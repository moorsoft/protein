﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Oracle.DataAccess.Client;

namespace Protein.Data
{
    public class DataConnection
    {
        private static string dbHost;
        private static string dbPort;
        private static string dbPwd;
        private static string dbUserId;
        private static string serviceName;
        private static string connectionString;

        public static IDbConnection GetConnection()
        {
            CheckConfigFile();
            IDbConnection connection = new OracleConnection(connectionString);
            SimpleCRUD.SetDialect(SimpleCRUD.Dialect.Oracle);
            connection.Open();
            connection.Execute("ALTER SESSION SET NLS_SORT = 'XTURKISH'");
            return connection;
        }
        private static void CheckConfigFile()
        {
            dbHost = ConfigurationManager.AppSettings["Oracle_DbHost"];
            dbPort = ConfigurationManager.AppSettings["Oracle_DbPort"];
            dbPwd = ConfigurationManager.AppSettings["Oracle_DbPwd"];
            dbUserId = ConfigurationManager.AppSettings["Oracle_DbUserId"];
            serviceName = ConfigurationManager.AppSettings["Oracle_ServiceName"];

            if (string.IsNullOrEmpty(dbHost) || string.IsNullOrEmpty(dbPwd) || string.IsNullOrEmpty(dbPort))
            {
                var currPath = Environment.CurrentDirectory + "\\app.config";
                var configMap = new ExeConfigurationFileMap { ExeConfigFilename = currPath };
                var configuration = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
                var settings = configuration.AppSettings.Settings;

                dbHost = settings["Oracle_DbHost"].Value;
                dbPort = settings["Oracle_DbPort"].Value;
                dbPwd = settings["Oracle_DbPwd"].Value;
                dbUserId = settings["Oracle_DbUserId"].Value;
                serviceName = settings["Oracle_ServiceName"].Value;
            }

            connectionString = $"Data Source=(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = {dbHost})(PORT = {dbPort}))(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = {serviceName})));User Id={dbUserId};Pooling=False;Password={dbPwd}";
        }
    }
}
