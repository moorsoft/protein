﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.Export.Model
{
    public class ExportColumn
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public int? Order_Num { get; set; }
    }
}
