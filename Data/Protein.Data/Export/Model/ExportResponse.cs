﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.Export.Model
{
    public class ExportResponse
    {
        public string Guid { get; set; }
        public MemoryStream memoryStream { get; set; }
        public bool IsDownloaded { get; set; } = false;
        public string FileName { get; set; }
        public ExportResponse()
        {
            memoryStream = new MemoryStream();
        }
    }
}
