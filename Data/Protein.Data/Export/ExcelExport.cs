﻿using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Protein.Data.Export.Model;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using Protein.Common.Entities;
using static Protein.Common.Enums.ProteinEnums;
using Protein.Common.Extensions;
using System.Globalization;
using Protein.Data.Repositories;
using Protein.Data.Helpers;

namespace Protein.Data.Export
{
    public class ExcelExport<T> : IExport<T> where T : class
    {
        public ExportResponse DoWork(List<T> lst, List<ExportColumn> columns, string FileName = "")
        {
            List<string> lstPropDisplayName = new List<string>();
            ExportResponse response = new ExportResponse();
            List<string> lstPropName = new List<string>();

            FileName = !string.IsNullOrEmpty(FileName) ? FileName : DateTime.Now.ToShortDateString() + ".xlsx";

            Type dg = lst[0].GetType();
            foreach (PropertyInfo prop in dg.GetProperties())
            {
                var attributes = prop.GetCustomAttributes(false);
                var columnMapping = attributes.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute));
                var realColumn = columnMapping as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute;


                ExportColumn findCol = columns.Where(x => x.Name == realColumn.Name).FirstOrDefault();
                if (findCol == null)
                    continue;

                lstPropDisplayName.Add(findCol.DisplayName);
                lstPropName.Add(prop.Name);
            }

            IWorkbook workbook;
            workbook = new XSSFWorkbook(); // for xlsx 
            ISheet sheet1 = workbook.CreateSheet("Sheet 1");
            IRow row1 = sheet1.CreateRow(0);

            int count = 0;
            int totalRow = 0;
            int columnCount = 0;

            foreach (string clmName in lstPropDisplayName)
            {
                ICell cell = row1.CreateCell(count);
                cell.SetCellValue(clmName);
                count++;
            }
            columnCount = count;
            count = 0;
            foreach (var item in lst)
            {
                IRow row = sheet1.CreateRow(count + 1);
                int cellCount = 0;
                foreach (string clmName in lstPropName)
                {
                    ICell cell = row.CreateCell(cellCount);
                    string val = "";
                    if (item.GetType().GetProperty(clmName).GetValue(item, null) != null)
                    {
                        string name = item.GetType().GetProperty(clmName).PropertyType.Name;
                        val = item.GetType().GetProperty(clmName).GetValue(item, null).ToString();
                    }
                    DateTime _out;
                    if (DateTime.TryParseExact(val,
                   "dd.MM.yyyy HH:mm:ss",
                   CultureInfo.InvariantCulture,
                   DateTimeStyles.None,
                   out _out) || DateTime.TryParseExact(val,
                   "d.MM.yyyy HH:mm:ss",
                   CultureInfo.InvariantCulture,
                   DateTimeStyles.None,
                   out _out))
                        val = Convert.ToDateTime(val).ToShortDateString();
                    cell.SetCellValue(val);
                    //sheet1.AutoSizeColumn(cellCount);
                    cellCount++;
                }

                count++;
                totalRow++;
            }
            CellRangeAddress cra = new CellRangeAddress(0, totalRow, 0, columnCount);
            string rAddr = cra.FormatAsString();
            sheet1.SetAutoFilter(CellRangeAddress.ValueOf(rAddr));

            using (var exportData = new MemoryStream())
            {
                workbook.Write(exportData);
                response.memoryStream = exportData;
                response.Guid = Guid.NewGuid().ToString();
                response.FileName = FileName;
                response.IsDownloaded = true;
            }
            return response;
        }

        public ExportResponse DoWorkOrder(List<T> lst, List<ExportColumn> DisplayColumns, string FileName = "")
        {
            Dictionary<long, string> lstPropDisplayName = new Dictionary<long, string>();
            ExportResponse response = new ExportResponse();
            Dictionary<long, string> lstPropName = new Dictionary<long, string>();
            try
            {

                FileName = !string.IsNullOrEmpty(FileName) ? FileName : DateTime.Now.ToShortDateString() + ".xlsx";

                Type dg = lst[0].GetType();

                foreach (PropertyInfo prop in dg.GetProperties())
                {
                    var attributes = prop.GetCustomAttributes(false);
                    var columnMapping = attributes.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute));
                    var realColumn = columnMapping as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute;


                    ExportColumn findCol = DisplayColumns.Where(x => x.Name == realColumn.Name).FirstOrDefault();
                    if (findCol == null) continue;
                    if (lstPropDisplayName.Where(x=>x.Key== findCol.Order_Num).Count()>0) continue;

                    lstPropDisplayName.Add((int)findCol.Order_Num, findCol.DisplayName);
                    lstPropName.Add((int)findCol.Order_Num, prop.Name);
                }

                var listPropDisplayKey = lstPropDisplayName.Keys.ToList();
                var listPropNameKey = lstPropDisplayName.Keys.ToList();

                IWorkbook workbook;
                workbook = new XSSFWorkbook(); // for xlsx 
                ISheet sheet1 = workbook.CreateSheet("Sheet 1");
                IRow row1 = sheet1.CreateRow(0);

                int count = 0;
                int totalRow = 0;
                int columnCount = 0;

                listPropDisplayKey.Sort();
                listPropNameKey.Sort();

                foreach (long order in listPropDisplayKey)
                {
                    ICell cell = row1.CreateCell(count);
                    cell.SetCellValue(lstPropDisplayName[order]);
                    count++;
                }
                columnCount = count;
                count = 0;
                foreach (var item in lst)
                {
                    IRow row = sheet1.CreateRow(count + 1);
                    int cellCount = 0;
                    foreach (long order in listPropNameKey)
                    {
                        ICell cell = row.CreateCell(cellCount);
                        string val = "";
                        if (item.GetType().GetProperty(lstPropName[order]).GetValue(item, null) != null)
                        {
                            string name = item.GetType().GetProperty(lstPropName[order]).PropertyType.Name;
                            val = item.GetType().GetProperty(lstPropName[order]).GetValue(item, null).ToString();
                        }
                        DateTime _out;
                        //if (DateTime.TryParse(val, out _out))
                        if (DateTime.TryParseExact(val,
                   "dd.MM.yyyy HH:mm:ss",
                   CultureInfo.InvariantCulture,
                   DateTimeStyles.None,
                   out _out) || DateTime.TryParseExact(val,
                   "d.MM.yyyy HH:mm:ss",
                   CultureInfo.InvariantCulture,
                   DateTimeStyles.None,
                   out _out))
                        {
                            val = Convert.ToDateTime(val).ToShortDateString();

                        }
                        //if (val.IsDateTime())
                        cell.SetCellValue(val);
                        //sheet1.AutoSizeColumn(cellCount);
                        cellCount++;
                    }

                    count++;
                    totalRow++;
                }
                CellRangeAddress cra = new CellRangeAddress(0, totalRow, 0, columnCount);
                string rAddr = cra.FormatAsString();
                sheet1.SetAutoFilter(CellRangeAddress.ValueOf(rAddr));

                using (var exportData = new MemoryStream())
                {
                    workbook.Write(exportData);
                    response.memoryStream = exportData;
                    response.Guid = Guid.NewGuid().ToString();
                    response.FileName = FileName;
                    response.IsDownloaded = true;
                }

            }
            catch (Exception ex)
            {

                throw;
            }
            return response;
        }

        public ExportResponse DoWorkRptProduction(List<V_RptProduction> lst, List<ExportColumn> DisplayColumns, string FileName = "")
        {
            Dictionary<long, string> lstPropDisplayName = new Dictionary<long, string>();
            ExportResponse response = new ExportResponse();
            Dictionary<long, string> lstPropName = new Dictionary<long, string>();

            FileName = !string.IsNullOrEmpty(FileName) ? FileName : DateTime.Now.ToShortDateString() + ".xlsx";

            Type dg = lst[0].GetType();

            foreach (PropertyInfo prop in dg.GetProperties())
            {
                var attributes = prop.GetCustomAttributes(false);
                var columnMapping = attributes.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute));
                var realColumn = columnMapping as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute;


                ExportColumn findCol = DisplayColumns.Where(x => x.Name == realColumn.Name).FirstOrDefault();
                if (findCol == null) continue;

                lstPropDisplayName.Add((int)findCol.Order_Num, findCol.DisplayName);
                lstPropName.Add((int)findCol.Order_Num, prop.Name);
            }

            var listPropDisplayKey = lstPropDisplayName.Keys.ToList();
            var listPropNameKey = lstPropDisplayName.Keys.ToList();

            IWorkbook workbook;
            workbook = new XSSFWorkbook(); // for xlsx 
            ISheet sheet1 = workbook.CreateSheet("Sheet 1");
            IRow row1 = sheet1.CreateRow(0);

            int count = 0;
            int totalRow = 0;
            int columnCount = 0;

            listPropDisplayKey.Sort();
            listPropNameKey.Sort();

            foreach (long order in listPropDisplayKey)
            {
                ICell cell = row1.CreateCell(count);
                cell.SetCellValue(lstPropDisplayName[order]);
                count++;
            }
            columnCount = count;
            count = 0;
            foreach (var item in lst)
            {
                IRow row = sheet1.CreateRow(count + 1);
                int cellCount = 0;
                foreach (long order in listPropNameKey)
                {
                    ICell cell = row.CreateCell(cellCount);
                    string val = "";
                    if (item.GetType().GetProperty(lstPropName[order]).GetValue(item, null) != null)
                    {
                        string name = item.GetType().GetProperty(lstPropName[order]).PropertyType.Name;
                        val = item.GetType().GetProperty(lstPropName[order]).GetValue(item, null).ToString();
                    }
                    DateTime _out;
                    //if (DateTime.TryParse(val, out _out))
                    if (DateTime.TryParseExact(val,
                   "dd.MM.yyyy HH:mm:ss",
                   CultureInfo.InvariantCulture,
                   DateTimeStyles.None,
                   out _out) || DateTime.TryParseExact(val,
                   "d.MM.yyyy HH:mm:ss",
                   CultureInfo.InvariantCulture,
                   DateTimeStyles.None,
                   out _out))
                    {
                        val = Convert.ToDateTime(val).ToShortDateString();

                    }
                    //if (val.IsDateTime())
                    cell.SetCellValue(val);
                    //sheet1.AutoSizeColumn(cellCount);
                    cellCount++;
                }

                count++;
                totalRow++;
            }
            CellRangeAddress cra = new CellRangeAddress(0, totalRow, 0, columnCount);
            string rAddr = cra.FormatAsString();
            sheet1.SetAutoFilter(CellRangeAddress.ValueOf(rAddr));

            ISheet sheet2 = workbook.CreateSheet("Özet");
            IRow row2 = sheet2.CreateRow(0);

            List<string> lstSumDisplayName = new List<string>();
            lstSumDisplayName.Add("Poliçe Adet");
            lstSumDisplayName.Add("Aktif_Sigortalı_Adedi");
            lstSumDisplayName.Add("Sigortalı_Adedi");
            lstSumDisplayName.Add("Kişi_Sayısı");
            lstSumDisplayName.Add("Zeyil_Adedi");
            lstSumDisplayName.Add("Prim");
            lstSumDisplayName.Add("Net_Risk_Primi");
            lstSumDisplayName.Add("Komisyon");
            lstSumDisplayName.Add("TPA_Payı");
            lstSumDisplayName.Add("Vergi");
            lstSumDisplayName.Add("Kazanılmış_Prim");

            count = 0;
            foreach (string clmName in lstSumDisplayName)
            {
                ICell celli = row2.CreateCell(count);
                celli.SetCellValue(clmName);
                count++;
            }

            IRow roww = sheet2.CreateRow(1);

            ICell celll = roww.CreateCell(0);
            celll.SetCellValue(lst.GroupBy(ls => ls.POLID).Count().ToString());

            celll = roww.CreateCell(1);
            celll.SetCellValue(lst.Where(ls => ls.INSURED_STATUS == ((int)Status.AKTIF).ToString() && ls.POLICY_STATUS == ((int)PolicyStatus.TANZIMLI).ToString()).GroupBy(ls => ls.INSURED_ID).Count().ToString());

            celll = roww.CreateCell(2);
            celll.SetCellValue(lst.GroupBy(ls => ls.INSURED_ID).Count().ToString());

            celll = roww.CreateCell(3);
            celll.SetCellValue(lst.GroupBy(ls => ls.PERSON_ID).Count().ToString());

            celll = roww.CreateCell(4);
            celll.SetCellValue(lst.GroupBy(ls => ls.ZEYLNO).Count().ToString());

            celll = roww.CreateCell(5);
            celll.SetCellValue(lst.Sum(ls => ls.PRIM).ToString());

            celll = roww.CreateCell(6);
            celll.SetCellValue(lst.Sum(ls => ls.NET_RISK_PRIM).ToString());

            celll = roww.CreateCell(7);
            celll.SetCellValue(lst.Sum(ls => ls.KOMISYON).ToString());

            celll = roww.CreateCell(8);
            celll.SetCellValue(lst.Sum(ls => ls.TPA_PAYI).ToString());

            celll = roww.CreateCell(9);
            celll.SetCellValue(lst.Sum(ls => ls.VERGI).ToString());

            celll = roww.CreateCell(10);
            celll.SetCellValue(lst.Sum(ls => ls.KAZANILMIS_PRIM).ToString());

            CellRangeAddress ceRan = new CellRangeAddress(0, 1, 0, 1);
            string rAdd = ceRan.FormatAsString();
            sheet2.SetAutoFilter(CellRangeAddress.ValueOf(rAdd));


            using (var exportData = new MemoryStream())
            {
                workbook.Write(exportData);
                response.memoryStream = exportData;
                response.Guid = Guid.NewGuid().ToString();
                response.FileName = FileName;
                response.IsDownloaded = true;
            }
            return response;
        }
    }
}
