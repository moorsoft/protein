﻿using Protein.Data.Export.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Data.Export
{
    public interface IExport<T> where T : class
    {
        ExportResponse DoWork(List<T> lst, List<ExportColumn> DisplayColumns, string FileName = "");
        ExportResponse DoWorkOrder(List<T> lst, List<ExportColumn> DisplayColumns, string FileName = "");
        ExportResponse DoWorkRptProduction(List<V_RptProduction> lst, List<ExportColumn> DisplayColumns, string FileName = "");
    }
}
