﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Dapper;
using Protein.Common.Dto;
using static Protein.Common.Entities.ProteinEntities;
using Protein.Data.Helpers;
using Protein.Common.Constants;
using Protein.Data.Workers;
using static Protein.Common.Enums.ProteinEnums;
using System.Web;
using Protein.Common.FunctionParams;
using Oracle.DataAccess.Client;

namespace Protein.Data.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T>
    {
        public class SpExecuteProp
        {
            public string ObjectName { get; set; }
            public string ObjectGuid { get; set; }
        }
        //For detailed help about Dapper.SimpleCRUD => https://github.com/ericdc1/Dapper.SimpleCRUD

        #region Standart repository methods

        //Find Operations

        /// <summary>
        /// Sample usage: entity.FindAll();
        /// </summary>
        /// <returns></returns>
        public List<T> FindAll()
        {
            List<T> data = null;
            string name = typeof(T).GetType().Name;
            using (IDbConnection cn = DataConnection.GetConnection())
            {
                data = cn.GetList<T>().ToList();
            }
            return data;
        }
        //public List<T> FindBySqlQuery(string sqlQuery)
        //{
        //    List<T> data = null;

        //    using (IDbConnection cn = DataConnection.GetConnection())
        //    {
        //        data = cn.GetList<T>(sqlQuery).ToList(); 
        //    }
        //    return data;
        //}
        /// <summary>
        /// Sample usage: entity.FindBy(0, 3, "MESSAGE LIKE '%SUCCESS%'");
        /// </summary>
        /// <param name="conditions"></param>
        /// <param name="orderby"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public List<T> FindBy(string conditions = "", bool fetchHistoricRows = false, bool fetchDeletedRows = false, string orderby = "ID", object parameters = null, string hint = "")
        {
            List<T> data = null;
            string name = typeof(T).Name;

            using (IDbConnection cn = DataConnection.GetConnection())
            {
                string sqlWhere = "";

                if (!fetchHistoricRows)
                {
                    sqlWhere = "WHERE NEW_VERSION_ID IS NULL";
                }
                if (!fetchDeletedRows)
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = $"WHERE STATUS != '{(int)Status.SILINDI}'";
                    }
                    else
                    {
                        sqlWhere += $" AND STATUS != '{(int)Status.SILINDI}'";
                    }
                }
                if (!string.IsNullOrEmpty(conditions))
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere += $"WHERE {conditions}";
                    }
                    else
                    {
                        sqlWhere += $" AND {conditions}";
                    }

                }

                if (orderby != "")
                    orderby = $" ORDER BY {orderby}";

                data = cn.GetList<T>(sqlWhere + orderby, parameters: parameters, hint: hint).ToList();
            }

            return data;
        }

        public List<T> FindBySf(SfParam param)
        {
            List<T> data = null;
            string name = typeof(T).Name;

            using (IDbConnection cn = DataConnection.GetConnection())
            {
                data = cn.GetList<T>(param).ToList();
            }

            return data;
        }
        /// <summary>
        /// Sample usage: entity.FindByPaged(0, 3, "MESSAGE LIKE :prm1", parameters: new { prm1 = "%SUCCESS%" });
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="rowsPerPage"></param>
        /// <param name="conditions"></param>
        /// <param name="orderby"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public ViewResultDto<List<T>> FindByPaged(int pageNumber = 0, int rowsPerPage = 10, string conditions = "", bool fetchHistoricRows = false, bool fetchDeletedRows = false, string orderby = "ID", object parameters = null, string hint = "")
        {
            ViewResultDto<List<T>> result = new ViewResultDto<List<T>>();
            Stopwatch watch = new Stopwatch();
            watch.Start();
            try
            {
                using (IDbConnection cn = DataConnection.GetConnection())
                {
                    string sqlWhere = "";
                    if (!fetchHistoricRows)
                    {
                        sqlWhere = "WHERE NEW_VERSION_ID IS NULL";
                    }
                    if (!fetchDeletedRows)
                    {
                        if (string.IsNullOrEmpty(sqlWhere))
                        {
                            sqlWhere = $"WHERE STATUS != {(int)Status.SILINDI}";
                        }
                        else
                        {
                            sqlWhere += $" AND STATUS != {(int)Status.SILINDI}";
                        }
                    }
                    if (!string.IsNullOrEmpty(conditions))
                    {
                        if (string.IsNullOrEmpty(sqlWhere))
                        {
                            sqlWhere += $"WHERE {conditions}";
                        }
                        else
                        {
                            sqlWhere += $" AND {conditions}";
                        }

                    }

                    if (orderby != "")
                        orderby = $" ORDER BY {orderby}";


                    var data = cn.GetListPaged<T>(pageNumber, rowsPerPage, sqlWhere, orderby, parameters: parameters, hint: hint).ToList();

                    result.Data = data;
                    result.TotalItemsCount = data.Count < rowsPerPage ? data.Count : cn.RecordCount<T>(sqlWhere, parameters);
                    result.StateCode = 1;
                    result.Conditions = !string.IsNullOrEmpty(sqlWhere) ? sqlWhere.Replace("WHERE", "") : "";
                    result.State = "OK";
                    result.Message = "";
                    result.Conditions = sqlWhere;
                }
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.TotalItemsCount = 0;
                result.StateCode = 0;
                result.Conditions = "";
                result.State = "ERROR";
                result.Message = $"{ex.Message}";
            }

            return result;
        }


        /// <summary>
        /// Sample usage: entity.FindById(1);
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T FindById(Int64 id)
        {
            T data;
            using (IDbConnection cn = DataConnection.GetConnection())
            {
                data = cn.GetList<T>($"WHERE ID=:id AND STATUS!=:status AND NEW_VERSION_ID IS NULL",
                                     parameters: new { id, status = new DbString { Value = ((int)Status.SILINDI).ToString(), Length = 3 } }).FirstOrDefault();
            }
            return data;
        }

        public bool CallSp(string SpName, ref OracleDynamicParameters parameters)
        {
            bool result = true;
            using (IDbConnection cn = DataConnection.GetConnection())
            {
                try
                {
                    var obj = cn.Execute(sql: SpName, param: parameters, commandType: CommandType.StoredProcedure);
                }
                catch (Exception ex)
                {
                    result = false;
                }
            }
            return result;
        }

        public T GetPreviousVersionData(Int64 newVersionId)
        {
            T data;
            using (IDbConnection cn = DataConnection.GetConnection())
            {
                data = cn.GetList<T>($"WHERE NEW_VERSION_ID = {newVersionId} ORDER BY ID DESC").FirstOrDefault();
            }
            return data;
        }
        //Insert Operations
        public SpResponse Insert(T entity, string CurrentToken = "")
        {
            List<T> list = new List<T>();
            list.Add(entity);
            return SPExecute(list, CurrentToken).FirstOrDefault();
        }
        public SpResponse UpdateForAll(T entity, string CurrentToken = "")
        {
            List<T> list = new List<T>();
            list.Add(entity);
            return SPExecuteForAll(list, CurrentToken).FirstOrDefault();
        }
        public List<SpResponse> InsertEntities(List<T> entityList, string CurrentToken = "")
        {
            return SPExecute(entityList, CurrentToken);
        }
        //public List<SpResponse<ClaimProcess>> InserClaimProcesstEntities(List<ClaimProcess> entityList, string CurrentToken = "")
        //{
        //    return SPClaimProcessExecute(entityList, CurrentToken);
        //}
        //Update Operations
        public SpResponse Update(T entity, string CurrentToken = "")
        {
            List<T> list = new List<T>();
            list.Add(entity);
            return SPExecute(list, CurrentToken).FirstOrDefault();
        }
        public List<SpResponse> UpdateEntities(List<T> entityList, string CurrentToken = "")
        {
            return SPExecute(entityList, CurrentToken);
        }
        //Delete Operations
        public SpResponse Delete(T entity, string CurrentToken = "")
        {
            List<T> list = new List<T>();
            list.Add(entity);
            return SPExecute(list, CurrentToken).FirstOrDefault();
        }
        public List<SpResponse> DeleteEntities(List<T> entityList, string CurrentToken = "")
        {
            return SPExecute(entityList, CurrentToken);
        }
        //Max Operation
        public T Max(string condition, string CurrentToken = "")
        {
            var classAttributes = typeof(T).GetCustomAttributes(false);
            var tableMapping = classAttributes.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.TableAttribute));
            var mapsToTable = tableMapping as System.ComponentModel.DataAnnotations.Schema.TableAttribute;
            T data;

            var whereCondition = "WHERE NEW_VERSION_ID IS NULL AND STATUS != 1";
            if (!string.IsNullOrEmpty(condition))
            {
                whereCondition += $" AND {condition}";
            }

            using (IDbConnection cn = DataConnection.GetConnection())
            {
                data = cn.Query<T>($"SELECT * FROM {mapsToTable.Name} WHERE ID IN (SELECT MAX(ID) FROM {mapsToTable.Name} {whereCondition})").FirstOrDefault();
            }

            return data;
        }
        #endregion
        //Other Operations
        private void ErrorHandler(ref List<SpResponse> spResponses)
        {
            if (spResponses.Count > 0)
            {
                foreach (SpResponse response in spResponses)
                {
                    if (response.Code != "100")
                    {
                        string errorMsg = LookupHelper.GetLookupTextByCode(Constants.LookupTypes.Error, response.Code);
                        response.Message = !string.IsNullOrEmpty(errorMsg) ? errorMsg : response.Message;
                    }
                }
            }
        }


        public List<SpResponse> InsertSpExecute3(List<T> entity, string CurrentToken = "")
        {
            return SpExecute3(entity, CurrentToken);
        }

        public List<SpResponse> InsertSpExecute3ForAll(List<T> entity, string CurrentToken = "")
        {
            return SpExecute3ForAll(entity, CurrentToken);
        }

        private List<SpResponse> SpExecute3(List<T> entityList, string CurrentToken = "")
        {
            List<SpResponse> result = new List<SpResponse>();
            #region Token

            #endregion

            List<SpExecuteProp> spExecuteProp = new List<SpExecuteProp>();
            List<string> RequestGuids = new List<string>();

            StringBuilder sb = new StringBuilder();

            sb.AppendLine($"<sp_request_list>");
            foreach (var entity in entityList)
            {
                var classAttributes = entity.GetType().GetCustomAttributes(false);
                var tableMapping = classAttributes.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.TableAttribute));
                var mapsToTable = tableMapping as System.ComponentModel.DataAnnotations.Schema.TableAttribute;

                string requestGuid = Guid.NewGuid().ToString("N");
                RequestGuids.Add(requestGuid);

                sb.AppendLine($"<sp_request>");
                sb.AppendLine($"  <id>{requestGuid}</id>");
                sb.AppendLine($"  <token>9999999999</token>");
                sb.AppendLine($"  <class_name>{mapsToTable.Name}</class_name>");
                sb.AppendLine($"  <object_list>"); //rows

                sb.AppendLine($"    <object>"); //row
                sb.AppendLine($"      <id>{mapsToTable.Name}</id>"); //objectid

                sb.AppendLine($"      <attribute_list>"); //column list

                Int64 ID = 0;
                Int64 pr_ID = 0;

                #region attribute_list
                var properties = entity.GetType().GetProperties();

                foreach (var property in entity.GetType().GetProperties())
                {
                    var attributes = property.GetCustomAttributes(false);
                    var columnMapping = attributes.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute));

                    if (columnMapping != null)
                    {

                        var mapsto = columnMapping as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute;
                        var val = property.GetValue(entity, null);

                        if (mapsto.TypeName == "BASE")
                        {
                            string findT_ = mapsToTable.Name.Substring(0, 2);
                            if (findT_ == "T_")
                                spExecuteProp.Add(new SpExecuteProp { ObjectName = mapsToTable.Name.Replace(findT_, "") + "_" + mapsto.Name, ObjectGuid = requestGuid });
                        }

                        string strVal = Convert.ToString(val);

                        if (val != null && strVal != null)
                        {
                            switch (mapsto.Name)
                            {
                                case "ID":
                                    ID = (Int64)val;
                                    break;

                                case "STATUS":
                                    if (!String.IsNullOrEmpty(strVal))
                                    {
                                        sb.AppendLine($"        <attribute>"); //row
                                        sb.AppendLine($"          <name>{mapsto.Name}</name>"); //ColumnName
                                        sb.AppendLine($"          <value>{strVal}</value>"); //ColumnValue
                                        sb.AppendLine($"        </attribute>");
                                    }
                                    break;

                                case "REASON_ID":
                                case "NEW_VERSION_ID":
                                case "PARENT_ID":
                                case "PREVIOUS_ID":
                                    pr_ID = (Int64)val;
                                    if (pr_ID > 0)
                                    {
                                        sb.AppendLine($"        <attribute>"); //row
                                        sb.AppendLine($"          <name>{mapsto.Name}</name>"); //ColumnName
                                        sb.AppendLine($"          <value>{pr_ID}</value>"); //ColumnValue
                                        sb.AppendLine($"        </attribute>");
                                    }
                                    break;

                                default:
                                    var existBaseID = spExecuteProp.Where(x => x.ObjectName == mapsto.Name).FirstOrDefault();
                                    sb.AppendLine($"        <attribute>"); //row
                                    sb.AppendLine($"          <name>{mapsto.Name}</name>"); //ColumnName
                                    sb.AppendLine($"          <value>{ (val.ToString() == "0" ? (existBaseID != null ? "#*#" + existBaseID.ObjectGuid + "#*#" : val) : val) }</value>"); //ColumnValue
                                    sb.AppendLine($"        </attribute>");
                                    break;
                            }
                        }
                    }
                }

                #endregion

                sb.AppendLine($"      </attribute_list>");

                sb.AppendLine(ID > 0 ? $"<pk_id>{ID}</pk_id>" : ""); //rowid

                sb.AppendLine($"    </object>");
                sb.AppendLine($"  </object_list>");
                sb.AppendLine($"</sp_request>");

            }
            sb.AppendLine($"</sp_request_list>");



            using (IDbConnection cn = DataConnection.GetConnection())
            {
                OracleDynamicParameters oraParams = new OracleDynamicParameters();

                oraParams.Add(name: "p_xml", oracleDbType: OracleDbType.Clob, obj: sb.ToString(), direction: System.Data.ParameterDirection.Input);

                Stopwatch watch = new Stopwatch();
                watch.Start();
                //Send XML to SP_EXECUTE Stored Procedure
                cn.Execute("SP_EXECUTE_MULTIPLE", oraParams, commandType: CommandType.StoredProcedure);



                //Get SP_EXECUTE Response results
                foreach (string guid in RequestGuids)
                {
                    string sql = "SELECT \"ID\" AS \"ID\",\"SP_REQUEST_ID\" AS \"SpRequestId\",\"OBJECT_ID\" AS \"ObjectId\","
                                + "\"PK_ID\" AS \"PkId\","
                                + "\"CODE\" AS \"Code\","
                                + "\"MESSAGE\" AS \"Message\","
                                + "\"SP_REQUEST_DATE\" AS \"SpRequestDate\","
                                + "\"SP_RESPONSE_DATE\" AS \"SpResponseDate\" "
                                + "FROM \"T_SP_RESPONSE\" "
                                + $"WHERE SP_REQUEST_ID = '{guid}' ";

                    var SubResult = cn.Query<SpResponse>(sql, commandType: CommandType.Text).ToList();

                    int counter = 1;
                    //while (!SubResult.Any())
                    //{
                    //    SubResult = cn.Query<SpResponse>(sql, commandType: CommandType.Text).ToList();
                    //    counter++;

                    //    if (counter == 100)
                    //    {
                    //        return result;
                    //    }
                    //}
                    result.Add(SubResult.FirstOrDefault());
                }
                watch.Stop();


                ////Get real data from mapsToTable.Name
                //var pkIds = result.Select(r => r.PkId).ToArray();
                //var resultPkIds = String.Join(",", pkIds.Select(p => p.ToString()).ToArray());
                //string dataSQL = $"SELECT * FROM {mapsToTable.Name} WHERE ID IN ({resultPkIds})";
                //System.Diagnostics.Debug.WriteLine(dataSQL);

                //List<T> dataList = cn.Query<T>(dataSQL, commandType: CommandType.Text).ToList();

                //List<dynamic> dataListDynamic = new List<dynamic>();
                //dataListDynamic.AddRange((IEnumerable<dynamic>)dataList);

                //if (dataListDynamic.Count() > 0)
                //{
                //    //Merge SP_EXECUTE Response results and real data
                //    foreach (var item in result)
                //    {
                //        int i = dataListDynamic.FindIndex(s => s.Id == item.PkId);
                //        var tmp = (T)dataListDynamic[i];

                //        item.Data = dataList[i];
                //    }
                //}

            }
            return result;
        }

        private List<SpResponse> SpExecute3ForAll(List<T> entityList, string CurrentToken = "")
        {
            List<SpResponse> result = new List<SpResponse>();
            #region Token

            //string Token = "";
            //if (CurrentToken != "9999999999" && string.IsNullOrEmpty(CurrentToken))
            //{
            //    int UserID = new UserWorker().GetUserID();

            //    #region Token validation (Burası taşınacak)
            //    Token = !(String.IsNullOrEmpty(CurrentToken)) ? CurrentToken : "";

            //    if (entityList.FirstOrDefault().GetType() == typeof(TSession))
            //        Token = typeof(T).GetProperty("Token").GetValue(entityList.FirstOrDefault()).ToString();
            //    else if (string.IsNullOrEmpty(Token))
            //    {
            //        if (string.IsNullOrEmpty(Token))
            //        {
            //            Token = new TokenWorker().GetValidToken(UserID);
            //            if (string.IsNullOrEmpty(Token))
            //            {
            //                Token = new TokenWorker().GenerateToken();
            //                new TokenWorker().SetToken(UserID, Token);
            //            }
            //        }
            //    }
            //    #endregion
            //}
            //else Token = CurrentToken;

            #endregion

            List<SpExecuteProp> spExecuteProp = new List<SpExecuteProp>();
            List<string> RequestGuids = new List<string>();

            StringBuilder sb = new StringBuilder();

            sb.AppendLine($"<sp_request_list>");
            foreach (var entity in entityList)
            {
                var classAttributes = entity.GetType().GetCustomAttributes(false);
                var tableMapping = classAttributes.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.TableAttribute));
                var mapsToTable = tableMapping as System.ComponentModel.DataAnnotations.Schema.TableAttribute;

                string requestGuid = Guid.NewGuid().ToString("N");
                RequestGuids.Add(requestGuid);

                sb.AppendLine($"<sp_request>");
                sb.AppendLine($"  <id>{requestGuid}</id>");
                sb.AppendLine($"  <token>9999999999</token>");
                sb.AppendLine($"  <class_name>{mapsToTable.Name}</class_name>");
                sb.AppendLine($"  <object_list>"); //rows

                sb.AppendLine($"    <object>"); //row
                sb.AppendLine($"      <id>{mapsToTable.Name}</id>"); //objectid

                sb.AppendLine($"      <attribute_list>"); //column list

                Int64 ID = 0;
                Int64 pr_ID = 0;

                #region attribute_list
                var properties = entity.GetType().GetProperties();

                foreach (var property in entity.GetType().GetProperties())
                {
                    var attributes = property.GetCustomAttributes(false);
                    var columnMapping = attributes.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute));

                    if (columnMapping != null)
                    {

                        var mapsto = columnMapping as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute;
                        var val = property.GetValue(entity, null);

                        if (mapsto.TypeName == "BASE")
                        {
                            string findT_ = mapsToTable.Name.Substring(0, 2);
                            if (findT_ == "T_")
                                spExecuteProp.Add(new SpExecuteProp { ObjectName = mapsToTable.Name.Replace(findT_, "") + "_" + mapsto.Name, ObjectGuid = requestGuid });
                        }
                        string strVal = Convert.ToString(val);

                        //if (val != null && strVal != null)
                        //{
                        switch (mapsto.Name)
                        {
                            case "ID":
                                ID = (Int64)val;
                                break;

                            case "STATUS":
                                if (!String.IsNullOrEmpty(strVal))
                                {
                                    sb.AppendLine($"        <attribute>"); //row
                                    sb.AppendLine($"          <name>{mapsto.Name}</name>"); //ColumnName
                                    sb.AppendLine($"          <value>{strVal}</value>"); //ColumnValue
                                    sb.AppendLine($"        </attribute>");
                                }
                                break;

                            case "REASON_ID":
                            case "NEW_VERSION_ID":
                            case "PARENT_ID":
                            case "PREVIOUS_ID":
                                if (!String.IsNullOrEmpty(strVal))
                                {
                                    pr_ID = (Int64)val;
                                    if (pr_ID > 0)
                                    {
                                        sb.AppendLine($"        <attribute>"); //row
                                        sb.AppendLine($"          <name>{mapsto.Name}</name>"); //ColumnName
                                        sb.AppendLine($"          <value>{pr_ID}</value>"); //ColumnValue
                                        sb.AppendLine($"        </attribute>");
                                    }
                                }
                                break;

                            default:
                                    var existBaseID = spExecuteProp.Where(x => x.ObjectName == mapsto.Name).FirstOrDefault();
                                    sb.AppendLine($"        <attribute>"); //row
                                    sb.AppendLine($"          <name>{mapsto.Name}</name>"); //ColumnName
                                if (val != null)
                                {
                                    sb.AppendLine($"          <value>{ (val.ToString() == "0" ? (existBaseID != null ? "#*#" + existBaseID.ObjectGuid + "#*#" : val) : val) }</value>"); //ColumnValue
                                }
                                else
                                {
                                    sb.AppendLine($"          <value>{val}</value>"); //ColumnValue
                                }
                           
                                sb.AppendLine($"        </attribute>");
                                break;
                        }
                        //}
                    }
                }

                #endregion

                sb.AppendLine($"      </attribute_list>");

                sb.AppendLine(ID > 0 ? $"<pk_id>{ID}</pk_id>" : ""); //rowid

                sb.AppendLine($"    </object>");
                sb.AppendLine($"  </object_list>");
                sb.AppendLine($"</sp_request>");

            }
            sb.AppendLine($"</sp_request_list>");



            using (IDbConnection cn = DataConnection.GetConnection())
            {
                OracleDynamicParameters oraParams = new OracleDynamicParameters();

                oraParams.Add(name: "p_xml", oracleDbType: OracleDbType.Clob, obj: sb.ToString(), direction: System.Data.ParameterDirection.Input);

                Stopwatch watch = new Stopwatch();
                watch.Start();
                //Send XML to SP_EXECUTE Stored Procedure
                cn.Execute("SP_EXECUTE_3", oraParams, commandType: CommandType.StoredProcedure);



                //Get SP_EXECUTE Response results
                foreach (string guid in RequestGuids)
                {
                    string sql = "SELECT \"ID\" AS \"ID\",\"SP_REQUEST_ID\" AS \"SpRequestId\",\"OBJECT_ID\" AS \"ObjectId\","
                                + "\"PK_ID\" AS \"PkId\","
                                + "\"CODE\" AS \"Code\","
                                + "\"MESSAGE\" AS \"Message\","
                                + "\"SP_REQUEST_DATE\" AS \"SpRequestDate\","
                                + "\"SP_RESPONSE_DATE\" AS \"SpResponseDate\" "
                                + "FROM \"T_SP_RESPONSE\" "
                                + $"WHERE SP_REQUEST_ID = '{guid}' ";

                    var SubResult = cn.Query<SpResponse>(sql, commandType: CommandType.Text).ToList();

                    int counter = 1;
                    //while (!SubResult.Any())
                    //{
                    //    SubResult = cn.Query<SpResponse>(sql, commandType: CommandType.Text).ToList();
                    //    counter++;

                    //    if (counter == 100)
                    //    {
                    //        return result;
                    //    }
                    //}
                    result.Add(SubResult.FirstOrDefault());
                }
                watch.Stop();


                ////Get real data from mapsToTable.Name
                //var pkIds = result.Select(r => r.PkId).ToArray();
                //var resultPkIds = String.Join(",", pkIds.Select(p => p.ToString()).ToArray());
                //string dataSQL = $"SELECT * FROM {mapsToTable.Name} WHERE ID IN ({resultPkIds})";
                //System.Diagnostics.Debug.WriteLine(dataSQL);

                //List<T> dataList = cn.Query<T>(dataSQL, commandType: CommandType.Text).ToList();

                //List<dynamic> dataListDynamic = new List<dynamic>();
                //dataListDynamic.AddRange((IEnumerable<dynamic>)dataList);

                //if (dataListDynamic.Count() > 0)
                //{
                //    //Merge SP_EXECUTE Response results and real data
                //    foreach (var item in result)
                //    {
                //        int i = dataListDynamic.FindIndex(s => s.Id == item.PkId);
                //        var tmp = (T)dataListDynamic[i];

                //        item.Data = dataList[i];
                //    }
                //}

            }
            return result;
        }

        private List<SpResponse> SPExecute(List<T> entityList, string CurrentToken = "")
        {
            List<SpResponse> result = new List<SpResponse>();
            string Token = "";
            if (string.IsNullOrEmpty(CurrentToken))
            {
                long UserID = new UserWorker().GetUserID();

                #region Token validation (Burası taşınacak)
                Token = !(String.IsNullOrEmpty(CurrentToken)) ? CurrentToken : "";

                if (entityList.FirstOrDefault().GetType() == typeof(TSession))
                    Token = typeof(T).GetProperty("Token").GetValue(entityList.FirstOrDefault()).ToString();
                else if (string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(Token))
                    {
                        Token = new TokenWorker().GetValidToken(UserID);
                        if (string.IsNullOrEmpty(Token))
                        {
                            Token = new TokenWorker().GenerateToken();
                            new TokenWorker().SetToken(UserID, Token);
                        }
                    }
                }
                #endregion
            }
            else if (CurrentToken == "9999999999")
            {
                Token = CurrentToken;
                //ws Kullanıcısının Id(73)si servisler için kullanılmıştır
                //HttpContext.Current.Response.Write("\n_Service Set Token" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");
                //new TokenWorker().SetToken(73, Token);
                //HttpContext.Current.Response.Write("\n_Service Generate XML Start" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");
            }

            var properties = typeof(T).GetProperties();

            var classAttributes = typeof(T).GetCustomAttributes(false);
            var tableMapping = classAttributes.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.TableAttribute));
            var mapsToTable = tableMapping as System.ComponentModel.DataAnnotations.Schema.TableAttribute;

            #region Generate XML 

            StringBuilder sb = new StringBuilder();

            string requestGuid = Guid.NewGuid().ToString("N");


            sb.AppendLine($"<sp_request>");
            sb.AppendLine($"  <id>{requestGuid}</id>");
            sb.AppendLine($"  <token>{Token}</token>");
            sb.AppendLine($"  <class_name>{mapsToTable.Name}</class_name>");
            sb.AppendLine($"  <object_list>"); //rows

            foreach (var entity in entityList)
            {
                //string objectGuid = Guid.NewGuid().ToString("N");

                sb.AppendLine($"    <object>"); //row
                sb.AppendLine($"      <id>{mapsToTable.Name}</id>"); //objectid

                sb.AppendLine($"      <attribute_list>"); //column list

                Int64 ID = 0;
                Int64 pr_ID = 0;

                #region attribute_list

                foreach (var property in properties)
                {
                    var attributes = property.GetCustomAttributes(false);
                    var columnMapping = attributes.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute));

                    if (columnMapping != null)
                    {
                        var mapsto = columnMapping as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute;
                        var val = property.GetValue(entity, null);

                        string strVal = Convert.ToString(val);

                        if (val != null && strVal != null)
                        {
                            switch (mapsto.Name)
                            {
                                case "ID":
                                    ID = (Int64)val;
                                    break;

                                case "STATUS":
                                    if (!String.IsNullOrEmpty(strVal))
                                    {
                                        sb.AppendLine($"        <attribute>"); //row
                                        sb.AppendLine($"          <name>{mapsto.Name}</name>"); //ColumnName
                                        sb.AppendLine($"          <value>{strVal}</value>"); //ColumnValue
                                        sb.AppendLine($"        </attribute>");
                                    }
                                    break;

                                case "REASON_ID":
                                case "NEW_VERSION_ID":
                                case "PARENT_ID":
                                case "PREVIOUS_ID":
                                    pr_ID = (Int64)val;
                                    if (pr_ID > 0)
                                    {
                                        sb.AppendLine($"        <attribute>"); //row
                                        sb.AppendLine($"          <name>{mapsto.Name}</name>"); //ColumnName
                                        sb.AppendLine($"          <value>{pr_ID}</value>"); //ColumnValue
                                        sb.AppendLine($"        </attribute>");
                                    }
                                    break;

                                default:
                                    sb.AppendLine($"        <attribute>"); //row
                                    sb.AppendLine($"          <name>{mapsto.Name}</name>"); //ColumnName
                                    sb.AppendLine($"          <value>{val}</value>"); //ColumnValue
                                    sb.AppendLine($"        </attribute>");
                                    break;
                            }
                        }
                    }
                }

                #endregion

                sb.AppendLine($"      </attribute_list>");

                sb.AppendLine(ID > 0 ? $"<pk_id>{ID}</pk_id>" : ""); //rowid

                sb.AppendLine($"    </object>");
            }

            sb.AppendLine($"  </object_list>");
            sb.AppendLine($"</sp_request>");

            #endregion

            using (IDbConnection cn = DataConnection.GetConnection())
            {

                OracleDynamicParameters oraParams = new OracleDynamicParameters();

                oraParams.Add(name: "p_xml", oracleDbType: OracleDbType.Clob, obj: sb.ToString(), direction: System.Data.ParameterDirection.Input);

                Stopwatch watch = new Stopwatch();
                watch.Start();

                //Send XML to SP_EXECUTE Stored Procedure
                if (CurrentToken == "9999999999")
                {
                    //HttpContext.Current.Response.Write("\n_Service Generate XML End" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");
                }
                cn.Execute("SP_EXECUTE_2", oraParams, commandType: CommandType.StoredProcedure);
                if (CurrentToken == "9999999999")
                {
                    //HttpContext.Current.Response.Write("\n_SP_EXECUTE_2 END_" + mapsToTable.Name + "__" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");
                }


                //Get SP_EXECUTE Response results
                string sql = "SELECT \"ID\" AS \"ID\",\"SP_REQUEST_ID\" AS \"SpRequestId\",\"OBJECT_ID\" AS \"ObjectId\","
                            + "\"PK_ID\" AS \"PkId\","
                            + "\"CODE\" AS \"Code\","
                            + "\"MESSAGE\" AS \"Message\","
                            + "\"SP_REQUEST_DATE\" AS \"SpRequestDate\","
                            + "\"SP_RESPONSE_DATE\" AS \"SpResponseDate\" "
                            + "FROM \"T_SP_RESPONSE\" "
                            + $"WHERE SP_REQUEST_ID = '{requestGuid}' ";

                result = cn.Query<SpResponse>(sql, commandType: CommandType.Text).ToList();

                int counter = 1;
                //while (!result.Any())
                //{
                //    result = cn.Query<SpResponse>(sql, commandType: CommandType.Text).ToList();
                //    counter++;
                //    if (counter == 100)
                //    {
                //        return result;
                //    }
                //}

                if (CurrentToken == "9999999999")
                {
                    //HttpContext.Current.Response.Write("\n_T_SP_RESPONSE_GET_RESPONSE_" + result[0].Id + "__" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");
                }
                ErrorHandler(ref result);
                watch.Stop();
                Debug.WriteLine("İşlem Gerçekleşme süresi: {0}  \nRequest: {1}\nSayaç: {2}", watch.Elapsed.ToString(), sb.ToString(), counter);
                //if (isReturnData)
                //{
                //    //Get real data from mapsToTable.Name
                //    var pkIds = result.Select(r => r.PkId).ToArray();
                //    var resultPkIds = String.Join(",", pkIds.Select(p => p.ToString()).ToArray());
                //    string dataSQL = $"SELECT * FROM {mapsToTable.Name} WHERE ID IN ({resultPkIds})";
                //    System.Diagnostics.Debug.WriteLine(dataSQL);

                //    List<T> dataList = cn.Query<T>(dataSQL, commandType: CommandType.Text).ToList();

                //    List<dynamic> dataListDynamic = new List<dynamic>();
                //    dataListDynamic.AddRange((IEnumerable<dynamic>)dataList);

                //    if (dataListDynamic.Count() > 0)
                //    {
                //        //Merge SP_EXECUTE Response results and real data
                //        foreach (var item in result)
                //        {
                //            int i = dataListDynamic.FindIndex(s => s.Id == item.PkId);
                //            var tmp = (T)dataListDynamic[i];

                //            item.Data = dataList[i];
                //        }
                //    }
                //}
            }

            return result;
        }

        //private List<SpResponse<ClaimProcess>> SPClaimProcessExecute(List<ClaimProcess> entityList, string CurrentToken = "")
        //{
        //    List<SpResponse<ClaimProcess>> result = new List<SpResponse<ClaimProcess>>();
        //    string Token = "";
        //    if (CurrentToken != "9999999999" && string.IsNullOrEmpty(CurrentToken))
        //    {
        //        int UserID = new UserWorker().GetUserID();

        //        #region Token validation (Burası taşınacak)
        //        Token = !(String.IsNullOrEmpty(CurrentToken)) ? CurrentToken : "";

        //        if (entityList.FirstOrDefault().GetType() == typeof(TSession))
        //            Token = typeof(ClaimProcess).GetProperty("Token").GetValue(entityList.FirstOrDefault()).ToString();
        //        else if (string.IsNullOrEmpty(Token))
        //        {
        //            if (string.IsNullOrEmpty(Token))
        //            {
        //                Token = new TokenWorker().GetValidToken(UserID);
        //                if (string.IsNullOrEmpty(Token))
        //                {
        //                    Token = new TokenWorker().GenerateToken();
        //                    new TokenWorker().SetToken(UserID, Token);
        //                }
        //            }
        //        }
        //        #endregion
        //    }
        //    else
        //    {
        //        Token = CurrentToken;
        //        //ws Kullanıcısının Id(73)si servisler için kullanılmıştır
        //        new TokenWorker().SetToken(73, Token);
        //    }

        //    var properties = typeof(ClaimProcess).GetProperties();

        //    var classAttributes = typeof(ClaimProcess).GetCustomAttributes(false);
        //    var tableMapping = classAttributes.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.TableAttribute));
        //    var mapsToTable = tableMapping as System.ComponentModel.DataAnnotations.Schema.TableAttribute;

        //    #region Generate XML 

        //    StringBuilder sb = new StringBuilder();

        //    string requestGuid = Guid.NewGuid().ToString("N");


        //    sb.AppendLine($"<sp_request>");
        //    sb.AppendLine($"  <id>{requestGuid}</id>");
        //    sb.AppendLine($"  <token>{Token}</token>");
        //    sb.AppendLine($"  <class_name>{mapsToTable.Name}</class_name>");
        //    sb.AppendLine($"  <object_list>"); //rows

        //    foreach (var entity in entityList)
        //    {
        //        //string objectGuid = Guid.NewGuid().ToString("N");

        //        sb.AppendLine($"    <object>"); //row
        //        sb.AppendLine($"      <id>{mapsToTable.Name}</id>"); //objectid

        //        sb.AppendLine($"      <attribute_list>"); //column list

        //        Int64 ID = 0;
        //        Int64 pr_ID = 0;

        //        #region attribute_list

        //        foreach (var property in properties)
        //        {
        //            var attributes = property.GetCustomAttributes(false);
        //            var columnMapping = attributes.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute));

        //            if (columnMapping != null)
        //            {
        //                var mapsto = columnMapping as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute;
        //                var val = property.GetValue(entity, null);

        //                string strVal = Convert.ToString(val);

        //                if (val != null && strVal != null)
        //                {
        //                    switch (mapsto.Name)
        //                    {
        //                        case "ID":
        //                            ID = (Int64)val;
        //                            break;

        //                        case "STATUS":
        //                            if (!String.IsNullOrEmpty(strVal))
        //                            {
        //                                sb.AppendLine($"        <attribute>"); //row
        //                                sb.AppendLine($"          <name>{mapsto.Name}</name>"); //ColumnName
        //                                sb.AppendLine($"          <value>{strVal}</value>"); //ColumnValue
        //                                sb.AppendLine($"        </attribute>");
        //                            }
        //                            break;

        //                        case "REASON_ID":
        //                        case "NEW_VERSION_ID":
        //                        case "PARENT_ID":
        //                        case "PREVIOUS_ID":
        //                            pr_ID = (Int64)val;
        //                            if (pr_ID > 0)
        //                            {
        //                                sb.AppendLine($"        <attribute>"); //row
        //                                sb.AppendLine($"          <name>{mapsto.Name}</name>"); //ColumnName
        //                                sb.AppendLine($"          <value>{pr_ID}</value>"); //ColumnValue
        //                                sb.AppendLine($"        </attribute>");
        //                            }
        //                            break;

        //                        default:
        //                            sb.AppendLine($"        <attribute>"); //row
        //                            sb.AppendLine($"          <name>{mapsto.Name}</name>"); //ColumnName
        //                            sb.AppendLine($"          <value>{val}</value>"); //ColumnValue
        //                            sb.AppendLine($"        </attribute>");
        //                            break;
        //                    }
        //                }
        //            }
        //        }

        //        #endregion

        //        sb.AppendLine($"      </attribute_list>");

        //        sb.AppendLine(ID > 0 ? $"<pk_id>{ID}</pk_id>" : ""); //rowid

        //        sb.AppendLine($"    </object>");
        //    }

        //    sb.AppendLine($"  </object_list>");
        //    sb.AppendLine($"</sp_request>");

        //    #endregion

        //    using (IDbConnection cn = DataConnection.GetConnection())
        //    {
        //        String xml = sb.ToString();
        //        //Send XML to SP_EXECUTE Stored Procedure

        //        cn.Execute("SP_EXECUTE_PROCESS", param: new { p_xml = new Dapper.DbString { Value = xml, Length = int.MaxValue } }, commandType: CommandType.StoredProcedure);



        //        //Get SP_EXECUTE Response results
        //        string sql = "SELECT \"ID\" AS \"ID\",\"SP_REQUEST_ID\" AS \"SpRequestId\",\"OBJECT_ID\" AS \"ObjectId\","
        //                    + "\"PK_ID\" AS \"PkId\","
        //                    + "\"CODE\" AS \"Code\","
        //                    + "\"MESSAGE\" AS \"Message\","
        //                    + "\"SP_REQUEST_DATE\" AS \"SpRequestDate\","
        //                    + "\"SP_RESPONSE_DATE\" AS \"SpResponseDate\" "
        //                    + "FROM \"T_SP_RESPONSE\" "
        //                    + $"WHERE SP_REQUEST_ID = '{requestGuid}' ";

        //        result = cn.Query<SpResponse<ClaimProcess>>(sql, commandType: CommandType.Text).ToList();

        //        while (!result.Any())
        //        {
        //            result = cn.Query<SpResponse<ClaimProcess>>(sql, commandType: CommandType.Text).ToList();
        //        }
        //       // ErrorHandler(ref result);

        //        //Get real data from mapsToTable.Name
        //        var pkIds = result.Select(r => r.PkId).ToArray();
        //        var resultPkIds = String.Join(",", pkIds.Select(p => p.ToString()).ToArray());
        //        string dataSQL = $"SELECT * FROM {mapsToTable.Name} WHERE ID IN ({resultPkIds})";
        //        System.Diagnostics.Debug.WriteLine(dataSQL);

        //        List<ClaimProcess> dataList = cn.Query<ClaimProcess>(dataSQL, commandType: CommandType.Text).ToList();

        //        List<dynamic> dataListDynamic = new List<dynamic>();
        //        dataListDynamic.AddRange((IEnumerable<dynamic>)dataList);

        //        if (dataListDynamic.Count() > 0)
        //        {
        //            //Merge SP_EXECUTE Response results and real data
        //            foreach (var item in result)
        //            {
        //                int i = dataListDynamic.FindIndex(s => s.Id == item.PkId);
        //                var tmp = (ClaimProcess)dataListDynamic[i];

        //                item.Data = dataList[i];
        //            }
        //        }

        //    }

        //    return result;
        //}

        private List<SpResponse> SPExecuteForAll(List<T> entityList, string CurrentToken = "")
        {
            List<SpResponse> result = new List<SpResponse>();
            string Token = "";
            if (CurrentToken != "9999999999" && string.IsNullOrEmpty(CurrentToken))
            {
                long UserID = new UserWorker().GetUserID();

                #region Token validation (Burası taşınacak)
                Token = !(String.IsNullOrEmpty(CurrentToken)) ? CurrentToken : "";

                if (entityList.FirstOrDefault().GetType() == typeof(TSession))
                    Token = typeof(T).GetProperty("Token").GetValue(entityList.FirstOrDefault()).ToString();
                else if (string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(Token))
                    {
                        Token = new TokenWorker().GetValidToken(UserID);
                        if (string.IsNullOrEmpty(Token))
                        {
                            Token = new TokenWorker().GenerateToken();
                            new TokenWorker().SetToken(UserID, Token);
                        }
                    }
                }
                #endregion
            }
            else if (CurrentToken == "9999999999")
            {
                Token = CurrentToken;
                //ws Kullanıcısının Id(73)si servisler için kullanılmıştır
                new TokenWorker().SetToken(73, Token);
            }

            var properties = typeof(T).GetProperties();

            var classAttributes = typeof(T).GetCustomAttributes(false);
            var tableMapping = classAttributes.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.TableAttribute));
            var mapsToTable = tableMapping as System.ComponentModel.DataAnnotations.Schema.TableAttribute;

            #region Generate XML 

            StringBuilder sb = new StringBuilder();

            string requestGuid = Guid.NewGuid().ToString("N");


            sb.AppendLine($"<sp_request>");
            sb.AppendLine($"  <id>{requestGuid}</id>");
            sb.AppendLine($"  <token>{Token}</token>");
            sb.AppendLine($"  <class_name>{mapsToTable.Name}</class_name>");
            sb.AppendLine($"  <object_list>"); //rows

            foreach (var entity in entityList)
            {
                //string objectGuid = Guid.NewGuid().ToString("N");

                sb.AppendLine($"    <object>"); //row
                sb.AppendLine($"      <id>{mapsToTable.Name}</id>"); //objectid

                sb.AppendLine($"      <attribute_list>"); //column list

                Int64 ID = 0;
                Int64 pr_ID = 0;

                #region attribute_list

                foreach (var property in properties)
                {
                    var attributes = property.GetCustomAttributes(false);
                    var columnMapping = attributes.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute));

                    if (columnMapping != null)
                    {
                        var mapsto = columnMapping as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute;
                        var val = property.GetValue(entity, null);

                        string strVal = Convert.ToString(val);

                        //if (val != null && strVal != null)
                        //{
                        switch (mapsto.Name)
                        {
                            case "ID":
                                ID = (Int64)val;
                                break;

                            case "STATUS":
                            case "NEW_VERSION_ID":
                                if (!String.IsNullOrEmpty(strVal))
                                {
                                    sb.AppendLine($"        <attribute>"); //row
                                    sb.AppendLine($"          <name>{mapsto.Name}</name>"); //ColumnName
                                    sb.AppendLine($"          <value>{strVal}</value>"); //ColumnValue
                                    sb.AppendLine($"        </attribute>");
                                }
                                break;

                            default:
                                sb.AppendLine($"        <attribute>"); //row
                                sb.AppendLine($"          <name>{mapsto.Name}</name>"); //ColumnName
                                sb.AppendLine($"          <value>{val}</value>"); //ColumnValue
                                sb.AppendLine($"        </attribute>");
                                break;
                        }
                        //}
                    }
                }

                #endregion

                sb.AppendLine($"      </attribute_list>");

                sb.AppendLine(ID > 0 ? $"<pk_id>{ID}</pk_id>" : ""); //rowid

                sb.AppendLine($"    </object>");
            }

            sb.AppendLine($"  </object_list>");
            sb.AppendLine($"</sp_request>");

            #endregion

            using (IDbConnection cn = DataConnection.GetConnection())
            {
                Stopwatch watch = new Stopwatch();
                watch.Start();
                //Send XML to SP_EXECUTE Stored Procedure
                cn.Execute("SP_EXECUTE_2", new { p_xml = sb.ToString() }, commandType: CommandType.StoredProcedure);



                //Get SP_EXECUTE Response results
                string sql = "SELECT \"ID\" AS \"ID\",\"SP_REQUEST_ID\" AS \"SpRequestId\",\"OBJECT_ID\" AS \"ObjectId\","
                            + "\"PK_ID\" AS \"PkId\","
                            + "\"CODE\" AS \"Code\","
                            + "\"MESSAGE\" AS \"Message\","
                            + "\"SP_REQUEST_DATE\" AS \"SpRequestDate\","
                            + "\"SP_RESPONSE_DATE\" AS \"SpResponseDate\" "
                            + "FROM \"T_SP_RESPONSE\" "
                            + $"WHERE SP_REQUEST_ID = '{requestGuid}' ";

                result = cn.Query<SpResponse>(sql, commandType: CommandType.Text).ToList();

                int counter = 1;
                //while (!result.Any())
                //{
                //    result = cn.Query<SpResponse>(sql, commandType: CommandType.Text).ToList();
                //    counter++;
                //    if (counter == 100)
                //    {
                //        return result;
                //    }
                //}
                ErrorHandler(ref result);
                watch.Stop();
                Debug.WriteLine("İşlem Gerçekleşme süresi: {0}  \nSelect Sorgusu Sayısı: {1}", watch.Elapsed.Milliseconds, counter);

                //Get real data from mapsToTable.Name
                //var pkIds = result.Select(r => r.PkId).ToArray();
                //var resultPkIds = String.Join(",", pkIds.Select(p => p.ToString()).ToArray());
                //string dataSQL = $"SELECT * FROM {mapsToTable.Name} WHERE ID IN ({resultPkIds})";
                //System.Diagnostics.Debug.WriteLine(dataSQL);

                //List<T> dataList = cn.Query<T>(dataSQL, commandType: CommandType.Text).ToList();

                //List<dynamic> dataListDynamic = new List<dynamic>();
                //dataListDynamic.AddRange((IEnumerable<dynamic>)dataList);

                //if (dataListDynamic.Count() > 0)
                //{
                //    //Merge SP_EXECUTE Response results and real data
                //    foreach (var item in result)
                //    {
                //        int i = dataListDynamic.FindIndex(s => s.Id == item.PkId);
                //        var tmp = (T)dataListDynamic[i];

                //        item.Data = dataList[i];
                //    }
                //}

            }

            return result;
        }
    }
}
