using Dapper;
using Oracle.DataAccess.Client;
using Protein.Data.Workers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Data.Repositories
{
    #region Generated code will paste here
    public class ContactRepository : GenericRepository<Contact>
    {
        public ContactRepository()
        {

        }
    }
    public class CountryRepository : GenericRepository<Country>
    {
        public CountryRepository()
        {
        }
    }
    public class PersonRepository : GenericRepository<Person>
    {
        public PersonRepository()
        {
        }
    }
    public class PackageNoteRepository : GenericRepository<PackageNote>
    {
        public PackageNoteRepository()
        {
        }
    }
    public class CorporateRepository : GenericRepository<Corporate>
    {
        public CorporateRepository()
        {
        }
    }
    public class AddressRepository : GenericRepository<Address>
    {
        public AddressRepository()
        {
        }
    }
    public class EmailRepository : GenericRepository<Email>
    {
        public EmailRepository()
        {
        }
    }
    public class LogRepository : GenericRepository<Log>
    {
        public LogRepository()
        {
        }
    }
    public class PhoneRepository : GenericRepository<Phone>
    {
        public PhoneRepository()
        {
        }
    }
    //public class MobilePhoneRepository : GenericRepository<MobilePhone>
    //{
    //    public MobilePhoneRepository()
    //    {
    //    }
    //}
    //public class ContactMobilePhoneRepository : GenericRepository<ContactMobilePhone>
    //{
    //    public ContactMobilePhoneRepository()
    //    {
    //    }
    //}
    public class ContactPhoneRepository : GenericRepository<ContactPhone>
    {
        public ContactPhoneRepository()
        {
        }
    }
    public class ContactAddressRepository : GenericRepository<ContactAddress>
    {
        public ContactAddressRepository()
        {
        }
    }
    public class ContactEmailRepository : GenericRepository<ContactEmail>
    {
        public ContactEmailRepository()
        {
        }
    }

    public class StateRepository : GenericRepository<State>
    {
        public StateRepository()
        {
        }
    }
    public class CityRepository : GenericRepository<City>
    {
        public CityRepository()
        {
        }
    }
    public class CountyRepository : GenericRepository<County>
    {
        public CountyRepository()
        {
        }
    }
    public class BankRepository : GenericRepository<Bank>
    {
        public BankRepository()
        {
        }
    }
    public class BankBranchRepository : GenericRepository<BankBranch>
    {
        public BankBranchRepository()
        {
        }
    }
    public class BankAccountRepository : GenericRepository<BankAccount>
    {
        public BankAccountRepository()
        {
        }
    }
    public class NoteRepository : GenericRepository<Note>
    {
        public NoteRepository()
        {
        }
    }
    public class MediaRepository : GenericRepository<Media>
    {
        public MediaRepository()
        {
        }
    }
    public class ReasonRepository : GenericRepository<Reason>
    {
        public ReasonRepository()
        {
        }
    }
    public class ApplicationRepository : GenericRepository<Application>
    {
        public ApplicationRepository()
        {
        }
    }
    public class CategoryRepository : GenericRepository<Category>
    {
        public CategoryRepository()
        {
        }
    }
    public class AssetRepository : GenericRepository<Asset>
    {
        public AssetRepository()
        {
        }
    }
    public class PrivilegeRepository : GenericRepository<Privilege>
    {
        public PrivilegeRepository()
        {
        }
    }
    public class OrganizationRepository : GenericRepository<Organization>
    {
        public OrganizationRepository()
        {
        }
    }
    public class UnitRepository : GenericRepository<Unit>
    {
        public UnitRepository()
        {
        }
    }
    public class CadreRepository : GenericRepository<Cadre>
    {
        public CadreRepository()
        {
        }
    }
    public class CadrePrivilegeRepository : GenericRepository<CadrePrivilege>
    {
        public CadrePrivilegeRepository()
        {
        }
    }
    public class UserRepository : GenericRepository<User>
    {
        public UserRepository()
        {
        }
    }
    public class PersonnelRepository : GenericRepository<Personnel>
    {
        public PersonnelRepository()
        {
        }
    }
    public class PersonnelCadreRepository : GenericRepository<PersonnelCadre>
    {
        public PersonnelCadreRepository()
        {
        }
    }
    public class DeputationRepository : GenericRepository<Deputation>
    {
        public DeputationRepository()
        {
        }
    }
    public class PriceListRepository : GenericRepository<PriceList>
    {
        public PriceListRepository()
        {
        }
    }
    public class PackageRepository : GenericRepository<Package>
    {
        public PackageRepository()
        {
        }
    }
    public class CompanyRepository : GenericRepository<Company>
    {
        public CompanyRepository()
        {
        }
    }
    public class AgencyRepository : GenericRepository<Agency>
    {
        public AgencyRepository()
        {
        }
    }
    public class PolicyGroupRepository : GenericRepository<PolicyGroup>
    {
        public PolicyGroupRepository()
        {
        }
    }
    public class PolicyRepository : GenericRepository<Policy>
    {
        public PolicyRepository()
        {
        }
    }
    public class EndorsementRepository : GenericRepository<Endorsement>
    {
        public EndorsementRepository()
        {
        }
    }
    public class InsuredRepository : GenericRepository<Insured>
    {
        public InsuredRepository()
        {
        }
    }
    public class InsuredTransferRepository : GenericRepository<InsuredTransfer>
    {
        public InsuredTransferRepository()
        {
        }
    }
    public class InsuredClaimReasonRepository : GenericRepository<InsuredClaimReason>
    {
        public InsuredClaimReasonRepository()
        {
        }
    }
    public class InstallmentRepository : GenericRepository<Installment>
    {
        public InstallmentRepository()
        {
        }
    }
    public class RuleCategoryRepository : GenericRepository<RuleCategory>
    {
        public RuleCategoryRepository()
        {
        }
    }
    public class RuleRepository : GenericRepository<Common.Entities.ProteinEntities.Rule>
    {
        public RuleRepository()
        {
        }
    }
    public class ExclusionRepository : GenericRepository<Exclusion>
    {
        public ExclusionRepository()
        {
        }
    }
    public class InsuredExclusionRepository : GenericRepository<InsuredExclusion>
    {
        public InsuredExclusionRepository()
        {
        }
    }
    public class ProcessListRepository : GenericRepository<ProcessList>
    {
        public ProcessListRepository()
        {
        }
    }
    public class ProcessRepository : GenericRepository<Process>
    {
        public ProcessRepository()
        {
        }
    }
    public class QueryRepository : GenericRepository<Query>
    {
        public QueryRepository()
        {
        }
    }
    public class QuestionRepository : GenericRepository<Question>
    {
        public QuestionRepository()
        {
        }
    }
    public class QueryQuestionRepository : GenericRepository<QueryQuestion>
    {
        public QueryQuestionRepository()
        {
        }
    }
    public class ChoiceRepository : GenericRepository<Choice>
    {
        public ChoiceRepository()
        {
        }
    }
    public class AnswerRepository : GenericRepository<Answer>
    {
        public AnswerRepository()
        {
        }
    }
    public class DeclarationRepository : GenericRepository<Declaration>
    {
        public DeclarationRepository()
        {
        }
    }
    public class NetworkRepository : GenericRepository<Network>
    {
        public NetworkRepository()
        {
        }
    }
    public class BranchRepository : GenericRepository<Branch>
    {
        public BranchRepository()
        {
        }
    }
    public class ProductRepository : GenericRepository<Product>
    {
        public ProductRepository()
        {
        }
    }
    public class SubproductRepository : GenericRepository<Subproduct>
    {
        public SubproductRepository()
        {
        }
    }
    public class PlanRepository : GenericRepository<Plan>
    {
        public PlanRepository()
        {
        }
    }
    public class CoverageRepository : GenericRepository<Coverage>
    {
        public CoverageRepository()
        {
        }
    }
    public class PlanCoverageRepository : GenericRepository<PlanCoverage>
    {
        public PlanCoverageRepository()
        {
        }
    }
    public class PlanCoverageVariationRepository : GenericRepository<PlanCoverageVariation>
    {
        public PlanCoverageVariationRepository()
        {
        }
    }
    public class PlanCoverageParameterRepository : GenericRepository<PlanCoverageParameter>
    {
        public PlanCoverageParameterRepository()
        {
        }
    }

    public class ProviderGroupRepository : GenericRepository<ProviderGroup>
    {
        public ProviderGroupRepository()
        {
        }
    }
    public class ProviderRepository : GenericRepository<Provider>
    {
        public ProviderRepository()
        {
        }
    }
    public class ProviderBankAccountRepository : GenericRepository<ProviderBankAccount>
    {
        public ProviderBankAccountRepository()
        {
        }
    }
    public class CityFactorGroupRepository : GenericRepository<CityFactorGroup>
    {
        public CityFactorGroupRepository()
        {
        }
    }
    public class CityFactorRepository : GenericRepository<CityFactor>
    {
        public CityFactorRepository()
        {
        }
    }
    public class ProcessCoverageRepository : GenericRepository<ProcessCoverage>
    {
        public ProcessCoverageRepository()
        {
        }
    }
    public class DoctorBranchRepository : GenericRepository<DoctorBranch>
    {
        public DoctorBranchRepository()
        {
        }
    }
    public class StaffRepository : GenericRepository<Staff>
    {
        public StaffRepository()
        {
        }
    }
    public class ContractRepository : GenericRepository<Contract>
    {
        public ContractRepository()
        {
        }
    }
    public class ContractReasonRepository : GenericRepository<ContractReason>
    {
        public ContractReasonRepository()
        {
        }
    }
    public class ContractDoctorBranchRepository : GenericRepository<ContractDoctorBranch>
    {
        public ContractDoctorBranchRepository()
        {
        }
    }
    public class ContractNetworkRepository : GenericRepository<ContractNetwork>
    {
        public ContractNetworkRepository()
        {
        }
    }
    public class ProcessGroupRepository : GenericRepository<ProcessGroup>
    {
        public ProcessGroupRepository()
        {
        }
    }
    public class ProcessGroupProcessRepository : GenericRepository<ProcessGroupProcess>
    {
        public ProcessGroupProcessRepository()
        {
        }
    }
    public class ContractProcessGroupRepository : GenericRepository<ContractProcessGroup>
    {
        public ContractProcessGroupRepository()
        {
        }
    }
    public class PayrollRepository : GenericRepository<Payroll>
    {
        public PayrollRepository()
        {
        }
    }
    public class TableColumnHeaderRepository : GenericRepository<TableColumnHeader>
    {
        public TableColumnHeaderRepository()
        {
        }
    }
    public class ClaimRepository : GenericRepository<Claim>
    {
        public ClaimRepository()
        {
        }

        public string[] ClaimStatusResult(Int64 claimId)
        {
            string[] result = new string[2];
            using (IDbConnection cn = DataConnection.GetConnection())
            {
                try
                {
                    OracleCommand objCmd = new OracleCommand();

                    objCmd.Connection = (OracleConnection)cn;

                    objCmd.CommandText = "SP_CLAIM_STATUS_RESULT";

                    objCmd.CommandType = CommandType.StoredProcedure;
                    objCmd.Parameters.Add("p_claim_id", OracleDbType.Int64).Value = claimId;
                    objCmd.Parameters.Add("v_result_code", OracleDbType.Varchar2).Direction = ParameterDirection.Output;
                    objCmd.Parameters["v_result_code"].Size = 4000;
                    objCmd.Parameters.Add("v_result_decription", OracleDbType.Varchar2).Direction = ParameterDirection.Output;
                    objCmd.Parameters["v_result_decription"].Size = 4000;

                    try
                    {
                        objCmd.ExecuteNonQuery();
                        result[0] = objCmd.Parameters["v_result_code"].Value.ToString();
                        result[1] = objCmd.Parameters["v_result_decription"].Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        result[0] = "999";
                        result[1] = ex.ToString();
                    }
                    cn.Close();
                }
                catch (Exception ex)
                {
                    result[0] = "999";
                    result[1] = ex.ToString();
                }
            }
            return result;
        }

        public List<SpResponse> ClaimProcessExecute(List<ClaimProcess> entityList, string CurrentToken = "")
        {
            List<SpResponse> result = new List<SpResponse>();
            using (IDbConnection cn = DataConnection.GetConnection())
            {
                string Token = "";
                if (CurrentToken != "9999999999" && string.IsNullOrEmpty(CurrentToken))
                {
                    long UserID = new UserWorker().GetUserID();

                    #region Token validation (Burası taşınacak)
                    Token = !(String.IsNullOrEmpty(CurrentToken)) ? CurrentToken : "";

                    if (entityList.FirstOrDefault().GetType() == typeof(TSession))
                        Token = typeof(ClaimProcess).GetProperty("Token").GetValue(entityList.FirstOrDefault()).ToString();
                    else if (string.IsNullOrEmpty(Token))
                    {
                        if (string.IsNullOrEmpty(Token))
                        {
                            Token = new TokenWorker().GetValidToken(UserID);
                            if (string.IsNullOrEmpty(Token))
                            {
                                Token = new TokenWorker().GenerateToken();
                                new TokenWorker().SetToken(UserID, Token);
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    Token = CurrentToken;
                    //ws Kullanıcısının Id(73)si servisler için kullanılmıştır
                    new TokenWorker().SetToken(73, Token);
                }

                var properties = typeof(ClaimProcess).GetProperties();

                var classAttributes = typeof(ClaimProcess).GetCustomAttributes(false);
                var tableMapping = classAttributes.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.TableAttribute));
                var mapsToTable = tableMapping as System.ComponentModel.DataAnnotations.Schema.TableAttribute;


                StringBuilder sb = new StringBuilder();

                string requestGuid = Guid.NewGuid().ToString("N");


                sb.AppendLine($"<sp_request>");
                sb.AppendLine($"  <id>{requestGuid}</id>");
                sb.AppendLine($"  <token>{Token}</token>");
                sb.AppendLine($"  <class_name>{mapsToTable.Name}</class_name>");
                sb.AppendLine($"  <object_list>"); //rows

                foreach (var entity in entityList)
                {
                    //string objectGuid = Guid.NewGuid().ToString("N");

                    sb.AppendLine($"    <object>"); //row
                    sb.AppendLine($"      <id>{mapsToTable.Name}</id>"); //objectid

                    sb.AppendLine($"      <attribute_list>"); //column list

                    Int64 ID = 0;
                    Int64 pr_ID = 0;

                    #region attribute_list

                    foreach (var property in properties)
                    {
                        var attributes = property.GetCustomAttributes(false);
                        var columnMapping = attributes.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute));

                        if (columnMapping != null)
                        {
                            var mapsto = columnMapping as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute;
                            var val = property.GetValue(entity, null);

                            string strVal = Convert.ToString(val);

                            if (val != null && strVal != null)
                            {
                                switch (mapsto.Name)
                                {
                                    case "ID":
                                        ID = (Int64)val;
                                        break;

                                    case "STATUS":
                                        if (!String.IsNullOrEmpty(strVal))
                                        {
                                            sb.AppendLine($"        <attribute>"); //row
                                            sb.AppendLine($"          <name>{mapsto.Name}</name>"); //ColumnName
                                            sb.AppendLine($"          <value>{strVal}</value>"); //ColumnValue
                                            sb.AppendLine($"        </attribute>");
                                        }
                                        break;

                                    case "REASON_ID":
                                    case "NEW_VERSION_ID":
                                    case "PARENT_ID":
                                    case "PREVIOUS_ID":
                                        pr_ID = (Int64)val;
                                        if (pr_ID > 0)
                                        {
                                            sb.AppendLine($"        <attribute>"); //row
                                            sb.AppendLine($"          <name>{mapsto.Name}</name>"); //ColumnName
                                            sb.AppendLine($"          <value>{pr_ID}</value>"); //ColumnValue
                                            sb.AppendLine($"        </attribute>");
                                        }
                                        break;

                                    default:
                                        sb.AppendLine($"        <attribute>"); //row
                                        sb.AppendLine($"          <name>{mapsto.Name}</name>"); //ColumnName
                                        sb.AppendLine($"          <value>{val}</value>"); //ColumnValue
                                        sb.AppendLine($"        </attribute>");
                                        break;
                                }
                            }
                        }
                    }

                    #endregion

                    sb.AppendLine($"      </attribute_list>");

                    sb.AppendLine(ID > 0 ? $"<pk_id>{ID}</pk_id>" : ""); //rowid

                    sb.AppendLine($"    </object>");
                }

                sb.AppendLine($"  </object_list>");
                sb.AppendLine($"</sp_request>");


                OracleCommand objCmd = new OracleCommand();

                objCmd.Connection = (OracleConnection)cn;

                objCmd.CommandText = "SP_EXECUTE_PROCESS";

                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("p_xml", OracleDbType.Clob).Value = sb.ToString();

                objCmd.ExecuteNonQuery();

                string sql = "SELECT \"ID\" AS \"ID\",\"SP_REQUEST_ID\" AS \"SpRequestId\",\"OBJECT_ID\" AS \"ObjectId\","
                    + "\"PK_ID\" AS \"PkId\","
                    + "\"CODE\" AS \"Code\","
                    + "\"MESSAGE\" AS \"Message\","
                    + "\"SP_REQUEST_DATE\" AS \"SpRequestDate\","
                    + "\"SP_RESPONSE_DATE\" AS \"SpResponseDate\" "
                    + "FROM \"T_SP_RESPONSE\" "
                    + $"WHERE SP_REQUEST_ID = '{requestGuid}' ";

                result = cn.Query<SpResponse>(sql, commandType: CommandType.Text).ToList();

                while (!result.Any())
                {
                    result = cn.Query<SpResponse>(sql, commandType: CommandType.Text).ToList();
                }
                // ErrorHandler(ref result);

                //Get real data from mapsToTable.Name
                //var pkIds = result.Select(r => r.PkId).ToArray();
                //var resultPkIds = String.Join(",", pkIds.Select(p => p.ToString()).ToArray());
                //string dataSQL = $"SELECT * FROM {mapsToTable.Name} WHERE ID IN ({resultPkIds})";
                //System.Diagnostics.Debug.WriteLine(dataSQL);

                //List<ClaimProcess> dataList = cn.Query<ClaimProcess>(dataSQL, commandType: CommandType.Text).ToList();

                //List<dynamic> dataListDynamic = new List<dynamic>();
                //dataListDynamic.AddRange((IEnumerable<dynamic>)dataList);

                //if (dataListDynamic.Count() > 0)
                //{
                //    //Merge SP_EXECUTE Response results and real data
                //    foreach (var item in result)
                //    {
                //        int i = dataListDynamic.FindIndex(s => s.Id == item.PkId);
                //        var tmp = (ClaimProcess)dataListDynamic[i];

                //        item.Data = dataList[i];
                //    }
                //}
                cn.Close();
            }
            return result;
        }
    }
    public class ClaimIcdRepository : GenericRepository<ClaimIcd>
    {
        public ClaimIcdRepository()
        {
        }
    }
    public class ClaimProcessIcdRepository : GenericRepository<ClaimProcessIcd>
    {
        public ClaimProcessIcdRepository()
        {
        }
    }
    public class ClaimProcessRepository : GenericRepository<ClaimProcess>
    {
        public ClaimProcessRepository()
        {
        }
    }
    public class ClaimHospitalizationRepository : GenericRepository<ClaimHospitalization>
    {
        public ClaimHospitalizationRepository()
        {
        }
    }
    public class ClaimBillRepository : GenericRepository<ClaimBill>
    {
        public ClaimBillRepository()
        {
        }
    }
    public class ClaimNoteRepository : GenericRepository<ClaimNote>
    {
        public ClaimNoteRepository()
        {
        }
    }
    public class ClaimReasonRepository : GenericRepository<ClaimReason>
    {
        public ClaimReasonRepository()
        {
        }
    }
    public class RuleGroupRepository : GenericRepository<RuleGroup>
    {
        public RuleGroupRepository()
        {
        }
    }
    public class RuleCategoryRuleRepository : GenericRepository<RuleCategoryRule>
    {
        public RuleCategoryRuleRepository()
        {
        }
    }
    public class CoverageRuleRepository : GenericRepository<CoverageRule>
    {
        public CoverageRuleRepository()
        {
        }
    }
    public class ProcessGroupRuleRepository : GenericRepository<ProcessGroupRule>
    {
        public ProcessGroupRuleRepository()
        {
        }
    }
    public class ProcessRuleRepository : GenericRepository<ProcessRule>
    {
        public ProcessRuleRepository()
        {
        }
    }
    public class ProviderGroupRuleRepository : GenericRepository<ProviderGroupRule>
    {
        public ProviderGroupRuleRepository()
        {
        }
    }
    public class ProviderRuleRepository : GenericRepository<ProviderRule>
    {
        public ProviderRuleRepository()
        {
        }
    }
    public class RuleGroupRuleRepository : GenericRepository<RuleGroupRule>
    {
        public RuleGroupRuleRepository()
        {
        }
    }
    public class RuleInsuredRepository : GenericRepository<RuleInsured>
    {
        public RuleInsuredRepository()
        {
        }
    }
    public class RulePolicyRepository : GenericRepository<RulePolicy>
    {
        public RulePolicyRepository()
        {
        }
    }
    public class RulePolicyGroupRepository : GenericRepository<RulePolicyGroup>
    {
        public RulePolicyGroupRepository()
        {
        }
    }
    public class RulePackageRepository : GenericRepository<RulePackage>
    {
        public RulePackageRepository()
        {
        }
    }
    public class RuleSubproductRepository : GenericRepository<RuleSubproduct>
    {
        public RuleSubproductRepository()
        {
        }
    }
    public class RuleProductRepository : GenericRepository<RuleProduct>
    {
        public RuleProductRepository()
        {
        }
    }
    public class ContractMediaRepository : GenericRepository<ContractMedia>
    {
        public ContractMediaRepository()
        {
        }
    }
    public class AppSettingRepository : GenericRepository<AppSetting>
    {
        public AppSettingRepository()
        {
        }
    }
    public class LookupRepository : GenericRepository<Lookup>
    {
        public LookupRepository()
        {
        }
    }
    public class IntegrationLogRepository : GenericRepository<IntegrationLog>
    {
        public IntegrationLogRepository()
        {
        }
    }
    public class IntegrationCompanyInfoRepository : GenericRepository<IntegrationCompanyInfo>
    {
        public IntegrationCompanyInfoRepository()
        {
        }
    }
    public class EquipmentRepository : GenericRepository<Equipment>
    {
        public EquipmentRepository()
        {
        }
    }
    public class TableConfigurationRepository : GenericRepository<TableConfiguration>
    {
        public TableConfigurationRepository()
        {
        }
    }
    public class PriceRepository : GenericRepository<Price>
    {
        public PriceRepository()
        {
        }
    }
    public class ParameterRepository : GenericRepository<Parameter>
    {
        public ParameterRepository()
        {
        }
    }
    public class CompanyParameterRepository : GenericRepository<CompanyParameter>
    {
        public CompanyParameterRepository()
        {
        }
    }
    public class CompanyNoteRepository : GenericRepository<CompanyNote>
    {
        public CompanyNoteRepository()
        {
        }
    }
    public class CompanyContactRepository : GenericRepository<CompanyContact>
    {
        public CompanyContactRepository()
        {
        }
    }
    public class CompanyPhoneRepository : GenericRepository<CompanyPhone>
    {
        public CompanyPhoneRepository()
        {
        }
    }
    public class CompanyEmailRepository : GenericRepository<CompanyEmail>
    {
        public CompanyEmailRepository()
        {
        }
    }
    public class CompanyBankAccountRepository : GenericRepository<CompanyBankAccount>
    {
        public CompanyBankAccountRepository()
        {
        }
    }
    public class RuleCompanyRepository : GenericRepository<RuleCompany>
    {
        public RuleCompanyRepository()
        {
        }
    }
    public class ProductParameterRepository : GenericRepository<ProductParameter>
    {
        public ProductParameterRepository()
        {
        }
    }
    public class ProductNoteRepository : GenericRepository<ProductNote>
    {
        public ProductNoteRepository()
        {
        }
    }
    public class ProductMediaRepository : GenericRepository<ProductMedia>
    {
        public ProductMediaRepository()
        {
        }
    }
    public class ClaimMediaRepository : GenericRepository<ClaimMedia>
    {
        public ClaimMediaRepository()
        {
        }
    }
    public class SubproductParameterRepository : GenericRepository<SubproductParameter>
    {
        public SubproductParameterRepository()
        {
        }
    }
    public class SubproductNoteRepository : GenericRepository<SubproductNote>
    {
        public SubproductNoteRepository()
        {
        }
    }
    public class SubproductMediaRepository : GenericRepository<SubproductMedia>
    {
        public SubproductMediaRepository()
        {
        }
    }
    public class CoverageParameterRepository : GenericRepository<CoverageParameter>
    {
        public CoverageParameterRepository()
        {
        }
    }
    public class PackageParameterRepository : GenericRepository<PackageParameter>
    {
        public PackageParameterRepository()
        {
        }
    }
    public class PackageTPAPriceRepository : GenericRepository<PackageTPAPrice>
    {
        public PackageTPAPriceRepository()
        {
        }
    }
    public class ProviderParameterRepository : GenericRepository<ProviderParameter>
    {
        public ProviderParameterRepository()
        {
        }
    }
    public class ProviderNoteRepository : GenericRepository<ProviderNote>
    {
        public ProviderNoteRepository()
        {
        }
    }
    public class ProviderMediaRepository : GenericRepository<ProviderMedia>
    {
        public ProviderMediaRepository()
        {
        }
    }
    public class PackageMediaRepository : GenericRepository<PackageMedia>
    {
        public PackageMediaRepository()
        {
        }
    }
    public class ProviderUserRepository : GenericRepository<ProviderUser>
    {
        public ProviderUserRepository()
        {
        }
    }
    public class AgencyUserRepository : GenericRepository<AgencyUser>
    {
        public AgencyUserRepository()
        {
        }
    }
    public class CompanyUserRepository : GenericRepository<CompanyUser>
    {
        public CompanyUserRepository()
        {
        }
    }
    public class ProviderEquipmentRepository : GenericRepository<ProviderEquipment>
    {
        public ProviderEquipmentRepository()
        {
        }
    }

    public class ProviderPhoneRepository : GenericRepository<ProviderPhone>
    {
        public ProviderPhoneRepository()
        {
        }
    }

    public class PolicyParameterRepository : GenericRepository<PolicyParameter>
    {
        public PolicyParameterRepository()
        {
        }
    }
    public class SagmerPackageTypeRepository : GenericRepository<SagmerPackageType>
    {
        public SagmerPackageTypeRepository()
        {
        }
    }
    public class SagmerCoverageCategoryRepository : GenericRepository<SagmerCoverageCategory>
    {
        public SagmerCoverageCategoryRepository()
        {
        }
    }
    public class SagmerCoverageTypeRepository : GenericRepository<SagmerCoverageType>
    {
        public SagmerCoverageTypeRepository()
        {
        }
    }
    public class SagmerCoverageRepository : GenericRepository<SagmerCoverage>
    {
        public SagmerCoverageRepository()
        {
        }
    }
    public class SagmerPlanRepository : GenericRepository<SagmerPlan>
    {
        public SagmerPlanRepository()
        {
        }
    }
    public class PeriodRepository : GenericRepository<Period>
    {
        public PeriodRepository()
        {
        }
    }
    public class IntervalRepository : GenericRepository<Interval>
    {
        public IntervalRepository()
        {
        }
    }
    public class SpResponseXmlRepository : GenericRepository<SpResponseXml>
    {
        public SpResponseXmlRepository()
        {
        }
    }
    public class ExchangeRateRepository : GenericRepository<ExchangeRate>
    {
        public ExchangeRateRepository()
        {

        }
    }
    public class TableColumnRepository : GenericRepository<V_TableColumn>
    {
        public TableColumnRepository()
        {

        }
    }
    public class ContractListRepository : GenericRepository<V_ContractList>
    {
        public ContractListRepository()
        {

        }
    }
    public class ExportProviderRepository : GenericRepository<V_ExportProvider>
    {
        public ExportProviderRepository()
        {

        }
    }
    public class ExportPackageRepository : GenericRepository<V_ExportPackage>
    {
        public ExportPackageRepository()
        {

        }
    }

    public class ExportNetworkRepository : GenericRepository<V_ExportNetwork>
    {
        public ExportNetworkRepository()
        {

        }
    }

    public class SessionRepository : GenericRepository<TSession>
    {
        public SessionRepository()
        {

        }
    }

    public class VCompanyParameterRepository : GenericRepository<V_CompanyParameter>
    {
        public VCompanyParameterRepository()
        {

        }
    }

    public class VInsuredEndorsementRepository : GenericRepository<V_InsuredEndorsement>
    {
        public VInsuredEndorsementRepository()
        {

        }
    }
    public class VInsuredPackageRemainingRepository : GenericRepository<V_InsuredPackageRemaining>
    {
        public VInsuredPackageRemainingRepository()
        {

        }
    }
    public class VContractProcessGroup : GenericRepository<V_ContractProcessGroup>
    {
        public VContractProcessGroup()
        {

        }
    }
    public class VContractNetworkRepository : GenericRepository<V_ContractNetwork>
    {
        public VContractNetworkRepository()
        {

        }
    }
    public class V_ProviderRepository : GenericRepository<V_Provider>
    {
        public V_ProviderRepository()
        {

        }
    }

    public class V_CoverageParameterRepository : GenericRepository<V_CoverageParameter>
    {
        public V_CoverageParameterRepository()
        {

        }
    }
    public class V_InsuredCoverageRuleRepository : GenericRepository<V_InsuredCoverageRule>
    {
        public V_InsuredCoverageRuleRepository()
        {

        }
    }

    public class V_PayrollRepository : GenericRepository<V_Payroll>
    {
        public V_PayrollRepository()
        {

        }
    }
    public class V_StaffRepository : GenericRepository<V_Staff>
    {
        public V_StaffRepository()
        {

        }
    }
    public class V_ProviderClaimListRepository : GenericRepository<V_ProviderClaimList>
    {
        public V_ProviderClaimListRepository()
        {

        }
    }

    #endregion

}
