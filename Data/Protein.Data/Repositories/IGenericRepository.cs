﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protein.Common.Dto;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Data.Repositories
{
    public interface IGenericRepository<T>
    {
        //Find Operations
        List<T> FindAll();
        List<T> FindBy(string conditions = "", bool fetchHistoricRows = false, bool fetchDeletedRows = false, string orderby = "", object parameters = null, string hint = "");
        ViewResultDto<List<T>> FindByPaged(int pageNumber = 0, int rowsPerPage = 10, string conditions = "", bool fetchHistoricRows = false, bool fetchDeletedRows = false, string orderby = "", object parameters = null, string hint = "");
        T FindById(Int64 id);

        //Insert Operations
        SpResponse Insert(T entity, string currentToken = "");
        List<SpResponse> InsertEntities(List<T> entityList, string currentToken = "");

        //Update Operations
        SpResponse Update(T entity, string currentToken = "");
        List<SpResponse> UpdateEntities(List<T> entityList, string currentToken = "");

        //Delete Operations
        SpResponse Delete(T entity, string currentToken = "");
        List<SpResponse> DeleteEntities(List<T> entityList, string currentToken = "");

    }
}
