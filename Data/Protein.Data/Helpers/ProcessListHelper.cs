﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Protein.Common.Helpers;
using Protein.Data.Repositories;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Data.Helpers
{
    public class ProcessListHelper
    {
        private static readonly string defaultEnvironment = ConfigurationManager.AppSettings["Environment"];
        private static readonly string configFile = $"{AppDomain.CurrentDomain.BaseDirectory}App_Data\\processlists.json";
        private static readonly string cacheName = "ProcessListCache";
        private static readonly string cacheKey = "ProcessList";

        public static void LoadProcessListDataFromDbToCache()
        {
            var cacheObject = CacheHelper.GetCache<List<ProcessList>>(cacheName, cacheKey);

            if (cacheObject == null)
            {
                ProcessListRepository repository = new ProcessListRepository();
                cacheObject = repository.FindBy();

                CacheHelper.AddCache(cacheName, cacheKey, cacheObject, 0, new List<string>() { configFile });
            }
        }

        private static List<ProcessList> LoadAppsettingsFromDb()
        {
            var cacheObject = CacheHelper.GetCache<List<ProcessList>>(cacheName, cacheKey);

            if (cacheObject == null)
            {
                ProcessListRepository repository = new ProcessListRepository();
                cacheObject = repository.FindBy();

                CacheHelper.AddCache(cacheName, cacheKey, cacheObject, 0, new List<string>() { configFile });
            }

            return cacheObject;
        }

        public static ProcessList GetProcessList(string name)
        {
            var result = new List<ProcessList>();
            foreach (var item in (from p in LoadAppsettingsFromDb()
                                  where p.Name == name
                                  select p))
                return item;
            return null;
        }

        public static List<ProcessList> GetProcessListbyType(string type)
        {
            var result = new List<ProcessList>();
            foreach (var item in (from p in LoadAppsettingsFromDb()
                                  where p.Type == type
                                  select p))
            {
                result.Add(item);
            }
            return result;
        }
    }
}
