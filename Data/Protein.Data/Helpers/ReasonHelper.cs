﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protein.Common.Entities;
using Protein.Common.Helpers;
using Protein.Data.Repositories;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Data.Helpers
{
    public class ReasonHelper
    {
        private static readonly string defaultEnvironment = ConfigurationManager.AppSettings["Environment"];
        private static readonly string configFile = $"{AppDomain.CurrentDomain.BaseDirectory}App_Data\\reasons.json";
        private static readonly string cacheName = "ReasonCache";
        private static readonly string cacheKey = "Reason";

        public static void LoadReasonDataFromDbToCache()
        {
            var cacheObject = CacheHelper.GetCache<List<Reason>>(cacheName, cacheKey);

            if (cacheObject == null)
            {
                ReasonRepository repository = new ReasonRepository();
                cacheObject = repository.FindAll();

                CacheHelper.AddCache(cacheName, cacheKey, cacheObject, 0, new List<string>() { configFile });
            }
        }

        private static List<Reason> LoadAppsettingsFromDb()
        {
            var cacheObject = CacheHelper.GetCache<List<Reason>>(cacheName, cacheKey);

            if (cacheObject == null)
            {
                ReasonRepository repository = new ReasonRepository();
                cacheObject = repository.FindAll();

                CacheHelper.AddCache(cacheName, cacheKey, cacheObject, 0, new List<string>() { configFile });
            }

            return cacheObject;
        }

        public static List<Reason> GetReasonData(string statusName, string statusOrdinal)
        {
            var result = new List<Reason>();

            foreach (var item in (from r in LoadAppsettingsFromDb()
                                  where r.StatusName == statusName && r.StatusOrdinal == statusOrdinal
                                  select r))
                result.Add(item);

            return result;
        }

        public static Reason GetReasonData(string statusName, string statusOrdinal, string ordinal)
        {
            var result = new List<Reason>();

            foreach (var item in (from r in LoadAppsettingsFromDb()
                                  where r.StatusName == statusName && r.StatusOrdinal == statusOrdinal && r.Ordinal == ordinal
                                  select r))
                result.Add(item);

            return result != null && result.Count > 0 ? result.ElementAt(0) : null;
        }

        public static Reason GetReasonbyId(long id)
        {
            return (from r in LoadAppsettingsFromDb()
                    where r.Id == id
                    select r).FirstOrDefault();
        }
    }
}
