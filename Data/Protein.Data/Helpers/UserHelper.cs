﻿using Protein.Common.Extensions;

namespace Protein.Data.Helpers
{
    public class UserHelper
    {
        public static string GenerateUserName(string Type, string Code)
        {
            return Type.Substring(0, 3).ReplaceTurkishCharacters() + Code;
        }

        public static string GeneratePassword(string Name, string Code)
        {
            return Name.Substring(0, 3).ReplaceTurkishCharacters().UppercaseFirstLetter() + Code;
        }
    }
}
