﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Protein.Common.Helpers;
using Protein.Data.Repositories;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Data.Helpers
{
    public class CityHelper
    {
        private static readonly string defaultEnvironment = ConfigurationManager.AppSettings["Environment"];
        private static readonly string configFile = $"{AppDomain.CurrentDomain.BaseDirectory}App_Data\\cities.json";
        private static readonly string cacheName = "CityCache";
        private static readonly string cacheKey = "City";

        public static void LoadCityDataFromDbToCache()
        {
            var cacheObject = CacheHelper.GetCache<List<City>>(cacheName, cacheKey);

            if (cacheObject == null)
            {
                GenericRepository<City> repository = new GenericRepository<City>();

                cacheObject = repository.FindBy();

                CacheHelper.AddCache(cacheName, cacheKey, cacheObject, 0, new List<string>() { configFile });
            }
        }

        private static List<City> LoadAppsettingsFromDb()
        {
            var cacheObject = CacheHelper.GetCache<List<City>>(cacheName, cacheKey);

            if (cacheObject == null)
            {
                GenericRepository<City> repository = new GenericRepository<City>();

                cacheObject = repository.FindBy();

                CacheHelper.AddCache(cacheName, cacheKey, cacheObject, 0, new List<string>() { configFile });
            }

            return cacheObject;
        }

        public static City GetCitybyCode(string cityCode)
        {
            var result = new City();

            var cacheObject = LoadAppsettingsFromDb();

            result = cacheObject.Where(c => c.Code == cityCode).FirstOrDefault();

            return result;
        }
    }
}
