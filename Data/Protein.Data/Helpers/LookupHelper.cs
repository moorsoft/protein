﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protein.Common.Entities;
using Protein.Common.Extensions;
using Protein.Common.Helpers;
using Protein.Data.Repositories;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Data.Helpers
{
    public class LookupHelper
    {
        private static readonly string defaultEnvironment = ConfigurationManager.AppSettings["Environment"];
        private static readonly string configFile = $"{AppDomain.CurrentDomain.BaseDirectory}App_Data\\lookups.json";
        private static readonly string cacheName = "LookupCache";
        private static readonly string cacheKey = "Lookup";

        public static void LoadLookupDataFromDbToCache()
        {
            var cacheObject = CacheHelper.GetCache<List<Lookup>>(cacheName, cacheKey);

            if (cacheObject == null)
            {
                LookupRepository repository = new LookupRepository();
                cacheObject = repository.FindBy();

                CacheHelper.AddCache(cacheName, cacheKey, cacheObject, 0, new List<string>() { configFile });
            }
        }

        private static List<Lookup> LoadAppsettingsFromDb()
        {
            var cacheObject = CacheHelper.GetCache<List<Lookup>>(cacheName, cacheKey);

            if (cacheObject == null)
            {
                LookupRepository repository = new LookupRepository();
                cacheObject = repository.FindBy();

                CacheHelper.AddCache(cacheName, cacheKey, cacheObject, 0, new List<string>() { configFile });
            }

            return cacheObject;
        }

        public static Dictionary<string, string> GetLookupData(string typeName, bool showChoose = false, bool showAll = false)
        {
            var result = new Dictionary<string, string>();

            if (showChoose)
                result.Add("", "SEÇİNİZ");

            if (showAll)
                result.Add("", "TÜMÜ");

            foreach (var item in (from r in LoadAppsettingsFromDb()
                                  where r.NetEnumTypeName == typeName && r.Status == ((int)Status.AKTIF).ToString()
                                  select new { key = r.Ordinal, value = r.Text, })
                     .ToDictionary(t => t.key, t => t.value))
                result.Add(item.Key, item.Value);

            return result;
        }
        public static Dictionary<string, string> GetLookupDataCode(string typeName)
        {
            var result = new Dictionary<string, string>();
            foreach (var item in (from r in LoadAppsettingsFromDb()
                                  where r.NetEnumTypeName == typeName && r.Status == ((int)Status.AKTIF).ToString()
                                  select new { key = r.Ordinal, value = r.Code, })
                     .ToDictionary(t => t.key, t => t.value))
                result.Add(item.Key, item.Value);

            return result;
        }
        public static Dictionary<string, string> GetLookupDataText(string typeName)
        {
            var result = new Dictionary<string, string>();
            foreach (var item in (from r in LoadAppsettingsFromDb()
                                  where r.NetEnumTypeName == typeName && r.Status == ((int)Status.AKTIF).ToString()
                                  select new { key = r.Ordinal, value = r.Text, })
                     .ToDictionary(t => t.key, t => t.value))
                result.Add(item.Key, item.Value);

            return result;
        }

        public static string GetLookupTextByCode(string typeName, string code)
        {
            if (code.IsNull()) return "";

            var result = (from r in LoadAppsettingsFromDb() where r.NetEnumTypeName == typeName && r.Code == code && r.Status == ((int)Status.AKTIF).ToString() select r).FirstOrDefault();
            return result != null ? result.Text.ToString() : "";
        }
        public static Lookup GetLookupByCode(string typeName, string code)
        {
            var result = (from r in LoadAppsettingsFromDb() where r.NetEnumTypeName == typeName && r.Code == code && r.Status == ((int)Status.AKTIF).ToString() select r).FirstOrDefault();
            return result;
        }
        public static string GetLookupCodeByOrdinal(string typeName, string ordinal)
        {
            if (ordinal.IsNull()) return "";

            var result = (from r in LoadAppsettingsFromDb() where r.NetEnumTypeName == typeName && r.Ordinal == ordinal && r.Status == ((int)Status.AKTIF).ToString() select r).FirstOrDefault();
            return result != null ? result.Code.ToString() : "";
        }

        public static string GetLookupTextByOrdinal(string typeName, string ordinal)
        {
            if (ordinal.IsNull()) return "";

            var result = (from r in LoadAppsettingsFromDb() where r.NetEnumTypeName == typeName && r.Ordinal == ordinal && r.Status == ((int)Status.AKTIF).ToString() select r).FirstOrDefault();
            return result != null ? result.Text.ToString() : "";
        }

        public static Lookup GetLookupByOrdinal(string typeName, string ordinal)
        {
            var result = (from r in LoadAppsettingsFromDb() where r.NetEnumTypeName == typeName && r.Ordinal == ordinal && r.Status==((int)Status.AKTIF).ToString() select r).FirstOrDefault();
            return result;
        }
    }
}
