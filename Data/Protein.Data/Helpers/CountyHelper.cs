﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Protein.Common.Helpers;
using Protein.Data.Repositories;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Data.Helpers
{
    public class CountyHelper
    {
        private static readonly string defaultEnvironment = ConfigurationManager.AppSettings["Environment"];
        private static readonly string configFile = $"{AppDomain.CurrentDomain.BaseDirectory}App_Data\\Counties.json";
        private static readonly string cacheName = "CountyCache";
        private static readonly string cacheKey = "County";

        public static void LoadCountyHelperDataFromDbToCache()
        {
            var cacheObject = CacheHelper.GetCache<List<County>>(cacheName, cacheKey);

            if (cacheObject == null)
            {
                GenericRepository<County> repository = new GenericRepository<County>();

                cacheObject = repository.FindBy();

                CacheHelper.AddCache(cacheName, cacheKey, cacheObject, 0, new List<string>() { configFile });
            }
        }

        private static List<County> LoadAppsettingsFromDb()
        {
            var cacheObject = CacheHelper.GetCache<List<County>>(cacheName, cacheKey);

            if (cacheObject == null)
            {
                GenericRepository<County> repository = new GenericRepository<County>();

                cacheObject = repository.FindBy();

                CacheHelper.AddCache(cacheName, cacheKey, cacheObject, 0, new List<string>() { configFile });
            }

            return cacheObject;
        }

        public static County GetCountybyCode(long cityId,int countyCode)
        {
            var result = new County();

            var cacheObject = LoadAppsettingsFromDb();

            result = cacheObject.Where(c => c.Type == "1" && c.CityId == cityId && c.Code == countyCode).FirstOrDefault();

            return result;
        }
    }
}
