﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Protein.Common.Helpers;
using Protein.Data.Repositories;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Data.Helpers
{
    public class CountryHelper
    {
        private static readonly string defaultEnvironment = ConfigurationManager.AppSettings["Environment"];
        private static readonly string configFile = $"{AppDomain.CurrentDomain.BaseDirectory}App_Data\\Countries.json";
        private static readonly string cacheName = "CountryCache";
        private static readonly string cacheKey = "Country";

        public static void LoadCountryHelperDataFromDbToCache()
        {
            var cacheObject = CacheHelper.GetCache<List<Country>>(cacheName, cacheKey);

            if (cacheObject == null)
            {
                GenericRepository<Country> repository = new GenericRepository<Country>();

                cacheObject = repository.FindBy();

                CacheHelper.AddCache(cacheName, cacheKey, cacheObject, 0, new List<string>() { configFile });
            }
        }

        private static List<Country> LoadAppsettingsFromDb()
        {
            var cacheObject = CacheHelper.GetCache<List<Country>>(cacheName, cacheKey);

            if (cacheObject == null)
            {
                GenericRepository<Country> repository = new GenericRepository<Country>();

                cacheObject = repository.FindBy();

                CacheHelper.AddCache(cacheName, cacheKey, cacheObject, 0, new List<string>() { configFile });
            }

            return cacheObject;
        }

        public static Country GetCountybyNumCode(string countryNumCode)
        {
            var result = new Country();

            var cacheObject = LoadAppsettingsFromDb();

            result = cacheObject.Where(c => c.NumCode == countryNumCode).FirstOrDefault();

            return result;
        }
   
        public static Country GetCountybyA2Code(string countryA2Code)
        {
            var result = new Country();

            var cacheObject = LoadAppsettingsFromDb();

            result = cacheObject.Where(c => c.A2Code == countryA2Code).FirstOrDefault();

            return result;
        }

        public static Country GetCountybyA3Code(string countryA3Code)
        {
            var result = new Country();

            var cacheObject = LoadAppsettingsFromDb();

            result = cacheObject.Where(c => c.A3Code == countryA3Code).FirstOrDefault();

            return result;
        }
    }
}
