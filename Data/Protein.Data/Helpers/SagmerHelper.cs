﻿    using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Data.Helpers
{
    public class SagmerHelper
    {
        public bool IsSagmerSendViaPolicy(long PolicyId,bool fetchDeletedRows=false)
        {
            return true;
            bool result = false;

            try
            {
                var _policy = new GenericRepository<V_Policy>().FindBy($"POLICY_ID = {PolicyId} and IS_SBM_TRANSFER = '1'", orderby: "", fetchDeletedRows: fetchDeletedRows);

                if (_policy.Count > 0)
                    result = true;
                else result = false;
            }
            catch(Exception ex) { result = false; }
            return result;
        }

        public bool IsSagmerSendViaClaim(long ClaimId)
        {
            bool result = false;

            try
            {
                var _policy = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID = {ClaimId} and IS_SBM_TRANSFER = '1'", orderby: "", hint: "/*+ USE_HASH(claim_id) ORDERED */");

                if (_policy.Count > 0)
                    result = true;
                else result = false;
            }
            catch (Exception ex) { result = false; }
            return result;
        }
    }
}
