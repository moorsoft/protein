﻿using Protein.Common.Constants;
using Protein.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Data.Helpers
{
    public class ErrorHelper<T>
    {
        public static void Handler(ref List<SpResponse> spResponses)
        {
            if (spResponses.Count > 0)
            {
                foreach (SpResponse response in spResponses)
                {
                    if (response.Code != "100")
                    {
                        string errorMsg = LookupHelper.GetLookupTextByCode(Constants.LookupTypes.Error, response.Code);
                        response.Message = !string.IsNullOrEmpty(errorMsg) ? errorMsg : response.Message;
                    }
                }
            }
        }
        //public static void Handler(ref ViewResultDto<dynamic> response)
        //{
        //    if (!string.IsNullOrEmpty(response.Message))
        //    {
        //        foreach (SpResponse<T> response in spResponses)
        //        {
        //            if (response.Code != "100")
        //            {
        //                string errorMsg = LookupHelper.GetLookupByCode(Constants.LookupTypes.Error, response.Code);
        //                response.Message = !string.IsNullOrEmpty(errorMsg) ? errorMsg : response.Message;
        //            }
        //        }
        //    }
        //}
    }
}
