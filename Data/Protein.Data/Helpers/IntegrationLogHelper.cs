﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Protein.Common.Messaging;
using Protein.Data.Repositories;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Data.Helpers
{
    public class IntegrationLogHelper
    {

        public static void AddToLog(IntegrationLog log, bool SendEmail = false)
        {
            if (SendEmail) SendEmail = false;
            try
            {
                var response = new GenericRepository<IntegrationLog>().Insert(log, CurrentToken: "9999999999");
                
            }
            catch(Exception ex) {
                MailSender msender = new MailSender();
                msender.SendMessage(
                    new EmailArgs
                    {
                        TO = "besim.oznalcin@mooryazilim.com",
                        IsHtml = false,
                        Subject = $"LogId = {log.Id}  -CorrelationId = {log.CorrelationId} - Description = {log.WsName}.{log.WsMethod} ",
                        Content = $"CatchMessage:{ex.Message} Req:{log.Request} <br><br> Res:{log.Response}"
                    });
            }
            if (SendEmail)
            {
                MailSender msender = new MailSender();
                msender.SendMessage(
                    new EmailArgs
                    {
                        TO = "besim.oznalcin@mooryazilim.com",
                        IsHtml = false,
                        Subject = $"LogId = {log.Id}  -CorrelationId = {log.CorrelationId} - Description = {log.WsName}.{log.WsMethod} ",
                        Content = $"Req:{log.Request} <br><br> Res:{log.Response}"
                    });
            }
        }

    }
}
