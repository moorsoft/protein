﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Dapper;
using Protein.Common.Dto;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Data.Helpers
{
    public static class ViewHelper
    {
        public static ViewResultDto<dynamic> GetView(string viewName, string whereCondititon = "", bool fetchHistoricRows = false, bool fetchDeletedRows = false, string orderby = "", int pageNumber = 0, int rowsPerPage = 0, object parameters = null, string selectCondititon = "*")
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();

            ViewResultDto<dynamic> result = new ViewResultDto<dynamic>();

            string sqlSelect = $"SELECT {selectCondititon} FROM {viewName}";
            string sqlWhere = !string.IsNullOrEmpty(whereCondititon) ? $"WHERE {whereCondititon}" : "";

            if (!fetchHistoricRows)
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = "WHERE NEW_VERSION_ID IS NULL";
                }
                else
                {
                    sqlWhere += " AND NEW_VERSION_ID IS NULL";
                }
            }
            if (!fetchDeletedRows)
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = $"WHERE STATUS != {(int)Status.SILINDI}";
                }
                else
                {
                    sqlWhere += $" AND STATUS != {(int)Status.SILINDI}";
                }
            }

            string sqlOrderBy = !string.IsNullOrEmpty(orderby) ? $"ORDER BY {orderby}" : "";
            string sqlOffset = rowsPerPage > 0 ? $"OFFSET {pageNumber} ROWS FETCH NEXT {rowsPerPage} ROWS ONLY" : "";

            string sql = $"{sqlSelect} {sqlWhere} {sqlOrderBy} {sqlOffset}";

            string sqlCount = $"SELECT COUNT(*) FROM {viewName} {sqlWhere}";

            try
            {
                using (IDbConnection cn = DataConnection.GetConnection())
                {
                    var data = cn.Query<dynamic>(sql, param: parameters, commandType: CommandType.Text).ToList();
                    watch.Stop();

                    try
                    {

                        if (viewName.ToUpper().StartsWith("V_"))
                        {
                            try
                            {
                                HttpContext.Current.Response.AppendToLog("_DURATIONFOR__" + viewName.ToUpper() + "_:_ " + watch.ElapsedMilliseconds.ToString() + "_ms._QUERY(" + sql + ")\n");
                            }
                            catch { }
                        }
                    }
                    catch { }

                    result.Data = data;
                    result.TotalItemsCount = cn.ExecuteScalar<int>(sqlCount, param: parameters, commandType: CommandType.Text);
                    result.StateCode = 1;
                    result.State = "OK";
                    result.Message = "";
                }
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.TotalItemsCount = 0;
                result.StateCode = 0;
                result.State = "ERROR";
                result.Message = $"{ex.Message}";
            }

            return result;
        }

    }
}
