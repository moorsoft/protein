﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Protein.Common.Helpers;
using Protein.Data.Repositories;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Data.Helpers
{
    public class ICD10StraightHelper
    {
        private static readonly string defaultEnvironment = ConfigurationManager.AppSettings["Environment"];
        private static readonly string configFile = $"{AppDomain.CurrentDomain.BaseDirectory}App_Data\\icd10straight.json";
        private static readonly string cacheName = "ICD10StraightCache";
        private static readonly string cacheKey = "ICD10Straight";

        public static List<Process> LoadAppsettingsFromDb()
        {
            var cacheObject = CacheHelper.GetCache<List<Process>>(cacheName, cacheKey);

            if (cacheObject == null)
            {
                ProcessRepository repository = new ProcessRepository();
                var icdProcessListId = ProcessListHelper.GetProcessList("ICD10").Id;

                cacheObject = repository.FindBy("PROCESS_LIST_ID = :icdProcessListId", orderby: "CODE", parameters: new { icdProcessListId });
                CacheHelper.AddCache(cacheName, cacheKey, cacheObject, 0, new List<string>() { configFile });
            }

            return cacheObject;
        }

        public static List<Process> GetICD10Data()
        {
            return LoadAppsettingsFromDb();
        }
    }
}