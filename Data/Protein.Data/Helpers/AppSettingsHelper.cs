﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Protein.Common.Helpers;
using Protein.Data.Repositories;

namespace Protein.Data.Helpers
{
    public class AppSettingsHelper
    {
        private static readonly string defaultEnvironment = ConfigurationManager.AppSettings["Environment"];
        private static readonly string configFile = $"{AppDomain.CurrentDomain.BaseDirectory}App_Data\\appsettings.json";
        private static readonly string cacheName = "ApplicationCache";
        private static readonly string cacheKey = "Settings";

        private static Dictionary<string, string> LoadAppsettingsFromDb()
        {
            var cacheObject = CacheHelper.GetCache<Dictionary<string, string>>(cacheName, cacheKey);

            if (cacheObject == null)
            {
                AppSettingRepository repository = new AppSettingRepository();

                cacheObject = repository.FindBy($"ENVIRONMENT = 'C' OR ENVIRONMENT = '{defaultEnvironment}'")
                                   .Select(l => new { l.Key, l.Value })
                                   .ToDictionary(d => d.Key, d => d.Value);

                CacheHelper.AddCache(cacheName, cacheKey, cacheObject, 0, new List<string>() { configFile });
            }

            return cacheObject;
        }

        public static object GetKey(string key)
        {
            var result = new object();

            var cacheObject = LoadAppsettingsFromDb();

            if (cacheObject.ContainsKey(key))
                result = cacheObject[key].ToString();
            else
                result = $"Key: {key} => not found!..";

            return result;
        }

    }
}
