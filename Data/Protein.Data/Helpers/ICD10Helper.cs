﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Protein.Common.Helpers;
using Protein.Data.Repositories;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Data.Helpers
{
    public class ICD10Helper
    {
        private static readonly string defaultEnvironment = ConfigurationManager.AppSettings["Environment"];
        private static readonly string configFile = $"{AppDomain.CurrentDomain.BaseDirectory}App_Data\\icd10s.json";
        private static readonly string cacheName = "ICD10Cache";
        private static readonly string cacheKey = "ICD10";

        public static void LoadICD10DataFromDbToCache()
        {
            var cacheObject = CacheHelper.GetCache<List<Process>>(cacheName, cacheKey);

            if (cacheObject == null)
            {
                ProcessRepository repository = new ProcessRepository();
                var icdProcessListId = ProcessListHelper.GetProcessList("ICD10").Id;

                cacheObject = repository.FindBy("PROCESS_LIST_ID = :icdProcessListId AND PARENT_ID IS NULL", orderby: "CODE", parameters: new { icdProcessListId });
                List<Process> childProcesses = repository.FindBy("PROCESS_LIST_ID = :icdProcessListId AND PARENT_ID IS NOT NULL", orderby: "CODE", parameters: new { icdProcessListId });

                foreach (Process process in cacheObject)
                {
                    foreach (Process childProcess in childProcesses)
                    {
                        if (childProcess.ParentId == process.Id)
                        {
                            process.ChildProcesses.Add(childProcess);
                        }
                    }
                }

                CacheHelper.AddCache(cacheName, cacheKey, cacheObject, 0, new List<string>() { configFile });
            }
        }

        private static List<Process> LoadAppsettingsFromDb()
        {
            var cacheObject = CacheHelper.GetCache<List<Process>>(cacheName, cacheKey);

            if (cacheObject == null)
            {
                ProcessRepository repository = new ProcessRepository();
                var icdProcessListId = ProcessListHelper.GetProcessList("ICD10").Id;

                cacheObject = repository.FindBy("PROCESS_LIST_ID = :icdProcessListId AND PARENT_ID IS NULL", orderby: "CODE", parameters: new { icdProcessListId });
                List<Process> childProcesses = repository.FindBy("PROCESS_LIST_ID = :icdProcessListId AND PARENT_ID IS NOT NULL", orderby: "CODE", parameters: new { icdProcessListId });

                foreach (Process process in cacheObject)
                {
                    foreach (Process childProcess in childProcesses)
                    {
                        if (childProcess.ParentId == process.Id)
                        {
                            process.ChildProcesses.Add(childProcess);
                        }
                    }
                }

                CacheHelper.AddCache(cacheName, cacheKey, cacheObject, 0, new List<string>() { configFile });
            }

            return cacheObject;
        }

        public static List<Process> GetICD10Data()
        {
            return LoadAppsettingsFromDb();
        }
    }
}
