﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Protein.Data.Workers
{
    public class UserWorker
    {
        public long GetUserID()
        {
            try
            {
                if (HttpContext.Current == null)
                    return 0;

                if (HttpContext.Current.Session["Personnel"] != null)
                {
                    dynamic sessionPersonnel = HttpContext.Current.Session["Personnel"];

                    return sessionPersonnel != null ? int.Parse(Convert.ToString(sessionPersonnel.USER_ID)) : 0;
                }
                else if (HttpContext.Current.Session["CompanyUser"] != null)
                {
                    dynamic sessionCompanyUser = HttpContext.Current.Session["CompanyUser"];

                    return sessionCompanyUser != null ? int.Parse(Convert.ToString(sessionCompanyUser.UserId)) : 0;
                }
                else
                {
                    return 0;
                }
            }
            catch { return 0; }
        }
    }
}
