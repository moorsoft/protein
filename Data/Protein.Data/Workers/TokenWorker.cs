﻿using Protein.Common.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Data.Workers
{
    public class TokenWorker
    {
        public string GenerateToken()
        { return Guid.NewGuid().ToString("N") + Guid.NewGuid().ToString("N"); }
        /// <summary>
        /// Token geçerli  mi ?
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="Token"></param>
        /// <returns></returns>
        public bool IsValidToken(int UserID, string Token)
        {
            bool result = false;
            List<TSession> session = new List<TSession>();
            try
            {
                session = new GenericRepository<TSession>().FindBy(conditions: $"USER_ID = {UserID} AND TOKEN = {Token} and START_DATE >= {DateTime.Now.ToString()} AND EXPIRED_DATE IS NULL", fetchHistoricRows: true);
                if (session == null) result = false;
                else if (session.Count < 1) result = false;
                else result = true;
            }
            catch { }
            finally { if (session != null) session = null; }
            return result;
        }
        /// <summary>
        /// Geçerli token'ı al. yoksa oluşturur
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public string GetValidToken(long UserID)
        {
            string _token = "";
            bool FindToken = false;
            List<TSession> session = new List<TSession>();
            try
            {
                session = new GenericRepository<TSession>().FindBy(conditions: $"USER_ID = {UserID} AND to_date(START_DATE,'DD.MM.YYYY HH24:MI:SS') <= to_date('{DateTime.Now.ToString()}','DD.MM.YYYY HH24:MI:SS') AND EXPIRED_DATE IS NULL", fetchHistoricRows: true);

                if (session == null) FindToken = false;
                else if (session.Count < 1) FindToken = false;
                else
                {
                    _token = session.FirstOrDefault().Token; FindToken = true;
                }

                if (!FindToken) //token yoksa oluştur.
                {
                   if(new TokenWorker().SetToken(UserID))
                    _token = new TokenWorker().GetValidToken(UserID);
                }
            }
            catch { }
            finally { if (session != null) session = null; }
            return _token;
        }
        /// <summary>
        /// Login olduğunda token'ı kullanıcıya set et
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public bool SetToken(long UserID, string Token = "")
        {
            bool result = false;
            TSession session = new TSession();
            try
            {
                session.Ip = ClientHelper.GetIpAddress();
                session.StartDate = DateTime.Now;
                session.Id = 0;
                session.Status = "0";
                session.Token = !string.IsNullOrEmpty(Token) ? Token : GenerateToken();
                session.UserID = UserID;
                session.ExpiredDate = null;
                SpResponse spResponse = new GenericRepository<TSession>().Insert(session);
                if (Token== "9999999999")
                {
                    //HttpContext.Current.Response.Write("\n_TSession Save" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");
                }

                if (spResponse.Code == "100")
                { FindAndDestroyExcludeToken(UserID: UserID, ExcludeToken: session.Token); session = null; spResponse = null; result = true; }
            }
            catch (Exception ex) { }
            finally { session = null; }
            return result;
        }
        /// <summary>
        /// Token oluşurken, User'a ait ExcludeToken dışındaki tüm expired_Date null olan tokenları Expired'Date'ini null'a çeker.
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="CurrentToken"></param>
        public void FindAndDestroyExcludeToken(long UserID, string ExcludeToken)
        {
            List<TSession> sessions = new List<TSession>();

            try
            {
                sessions = new GenericRepository<TSession>().FindBy(conditions: $"USER_ID = :UserID AND TOKEN != :ExcludeToken AND EXPIRED_DATE IS NULL", parameters: new { UserID, ExcludeToken = new Dapper.DbString { Value = ExcludeToken, Length = 100 } }, fetchHistoricRows: true);
                if (ExcludeToken == "9999999999")
                {
                    //HttpContext.Current.Response.Write("\n_TSession FindAndDestroyExcludeToken" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");
                }
                if (sessions == null) return;
                if (sessions.Count == 0) return;

                foreach (TSession session in sessions)
                {
                    if (!string.IsNullOrEmpty(session.Token))
                    {
                        session.ExpiredDate = DateTime.Now;
                        SpResponse spResponse = new GenericRepository<TSession>().Insert(session);
                        if (ExcludeToken == "9999999999")
                        {
                            //HttpContext.Current.Response.Write("\n_TSession Destroy" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");
                        }
                    }
                }
            }
            catch { }
            finally { sessions = null; }
        }
        /// <summary>
        /// IncludeToken'a ait kaydın expired'date'i now yapar.
        /// </summary>
        /// <param name="IncludeToken"></param>
        public bool FindAndDestroyIncludeToken(long UserID, string IncludeToken)
        {
            List<TSession> sessions = new List<TSession>();
            bool result = false;
            try
            {
                sessions = new GenericRepository<TSession>().FindBy(conditions: $"USER_ID = :UserID AND TOKEN != :IncludeToken AND EXPIRED_DATE IS NULL", parameters: new { UserID, ExcludeToken = new Dapper.DbString { Value = IncludeToken, Length = 100 } }, fetchHistoricRows: true);
                if (sessions == null) return false;
                if (sessions.Count == 0) return false;

                foreach (TSession session in sessions)
                {
                    if (!string.IsNullOrEmpty(session.Token))
                    {
                        session.ExpiredDate = DateTime.Now;
                        SpResponse spResponse = new GenericRepository<TSession>().Insert(session);
                        return spResponse.Code == "100" ? true : false;
                    }
                }
            }
            catch { }
            finally { sessions = null; }
            return result;
        }
    }
}
