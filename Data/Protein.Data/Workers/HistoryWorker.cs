﻿using Protein.Common.Attributes;
using Protein.Common.Entities;
using Protein.Data.Repositories;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.Workers
{

    public class HistoryResponse
    {
        //public string Source { get; set; }
        public int VersionNo { get; set; }
        public string ColumnName { get; set; }
        public string NewValue { get; set; }
        public string OldValue { get; set; }
        public string UserName { get; set; }
        public string ChangesDate { get; set; }
    }
    public class History<TEntity> where TEntity : class, new()
    {
        private void AnalyzeHistoryForSubContent(IList input, List<HistoryResponse> historyList)
        {
            int versionNo = 0;
            bool changedVersion = false;
            for (int i = 0; i < input.Count; i++)
            {
                var item = input[i];

                string entityName = ((item.GetType().GetCustomAttributes(false)).FirstOrDefault(a => a.GetType() == typeof(DisplayColumnAttribute)) as DisplayColumnAttribute).DisplayColumn;

                entityName = !(string.IsNullOrEmpty(entityName)) ? entityName : ((item.GetType().GetCustomAttributes(false)).FirstOrDefault(a => a.GetType() == typeof(TableAttribute)) as TableAttribute).Name;
                changedVersion = false;

                foreach (PropertyInfo prop in item.GetType().GetProperties())
                {
                    if (i == (input.Count - 1)) continue;

                    //if (i == 0)
                    //{
                    //    // Alt contentleri dolaş
                    //    if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                    //    {

                    //        //IList subList = (IList)prop.GetValue(input[i], null);


                    //        //Type type = prop.PropertyType.GetGenericArguments()[0];
                    //        //var listType = typeof(List<>);
                    //        //var constructedListType = listType.MakeGenericType(type);
                    //        //var instance = Activator.CreateInstance(constructedListType);
                    //    }
                    //}

                    bool findHistoricColumn = prop.GetCustomAttributes(typeof(HistoryAttribute), false).Any();
                    if (!findHistoricColumn) continue;

                    string ColumnName = prop.GetCustomAttributes(typeof(ColumnAttribute), false).Any() ?
                                                (prop.GetCustomAttributes(typeof(ColumnAttribute), false).FirstOrDefault() as ColumnAttribute).Name
                                                 : prop.Name;

                    string NewValue = "", OldValue = "";

                    if (prop.GetValue(input[i], null) != null)
                        NewValue = prop.GetValue(input[i], null).ToString();
                    if (input[i + 1].GetType().GetProperty(prop.Name).GetValue(input[i + 1], null) != null)
                        OldValue = input[i + 1].GetType().GetProperty(prop.Name).GetValue(input[i + 1], null).ToString();

                    if (NewValue != OldValue)
                    {
                        DateTime changesDate = new DateTime();
                        string userName = "";

                        PropertyInfo propResponseDate = item.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                         .FirstOrDefault(x => x.GetCustomAttribute<ColumnAttribute>() != null
                                            && x.GetCustomAttribute<ColumnAttribute>().Name.Equals("SP_RESPONSE_DATE", StringComparison.OrdinalIgnoreCase));
                        PropertyInfo propUserName = item.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                        .FirstOrDefault(x => x.GetCustomAttribute<ColumnAttribute>() != null
                                           && x.GetCustomAttribute<ColumnAttribute>().Name.Equals("USERNAME", StringComparison.OrdinalIgnoreCase));

                        changesDate = propResponseDate != null ? Convert.ToDateTime(propResponseDate.GetValue(input[i], null).ToString()) : new DateTime();
                        userName = propUserName != null ? (propUserName.GetValue(input[i], null) != null ? propUserName.GetValue(input[i], null).ToString() : "") : "";


                        if (!changedVersion)
                        {
                            versionNo++;
                            changedVersion = true;
                        }
                        historyList.Add(new HistoryResponse
                        {
                            //Source = entityName,
                            VersionNo = versionNo,
                            ChangesDate = changesDate.ToShortDateString() + " " + changesDate.ToShortTimeString(),
                            UserName = userName,
                            ColumnName = ColumnName,
                            OldValue = OldValue,
                            NewValue = NewValue
                        });
                    }
                }
            }
        }
        private List<HistoryResponse> AnalyzeHistory(List<TEntity> input)
        {
            List<HistoryResponse> HistoryList = new List<HistoryResponse>();

            int versionNo = 0;
            bool changedVersion = false;

            for (int i = 0; i < input.Count; i++)
            {
                TEntity item = input[i];

                foreach (PropertyInfo prop in item.GetType().GetProperties())
                {
                    if (i == (input.Count - 1)) continue;

                    if (i == 0)
                    {
                        // Alt contentleri dolaş
                        if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                        {
                            IList subContentlList = (IList)prop.GetValue(input[i], null);
                            AnalyzeHistoryForSubContent(subContentlList, HistoryList);
                            //IList subList = (IList)prop.GetValue(input[i], null);


                            //Type type = prop.PropertyType.GetGenericArguments()[0];
                            //var listType = typeof(List<>);
                            //var constructedListType = listType.MakeGenericType(type);
                            //var instance = Activator.CreateInstance(constructedListType);
                        }
                    }

                    bool findHistoricColumn = prop.GetCustomAttributes(typeof(HistoryAttribute), false).Any();
                    if (!findHistoricColumn) continue;

                    string ColumnName = prop.GetCustomAttributes(typeof(ColumnAttribute), false).Any() ?
                                                (prop.GetCustomAttributes(typeof(ColumnAttribute), false).FirstOrDefault() as ColumnAttribute).Name
                                                 : prop.Name;

                    string NewValue = "", OldValue = "";

                    PropertyInfo propNewVersionId = item.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                           .FirstOrDefault(x => x.GetCustomAttribute<ColumnAttribute>() != null
                                              && x.GetCustomAttribute<ColumnAttribute>().Name.Equals("NEW_VERSION_ID", StringComparison.OrdinalIgnoreCase));

                    string newVersionId_NEW = propNewVersionId != null ? (propNewVersionId.GetValue(input[i], null) != null ? propNewVersionId.GetValue(input[i], null).ToString() : "") : null;

                    string newVersionId_OLD = propNewVersionId != null ? (propNewVersionId.GetValue(input[i + 1], null) != null ? propNewVersionId.GetValue(input[i + 1], null).ToString() : "") : null;

                    if (!string.IsNullOrEmpty(newVersionId_OLD)) continue;

                    if (prop.GetValue(input[i], null) != null)
                        NewValue = prop.GetValue(input[i], null).ToString();
                    if (input[i + 1].GetType().GetProperty(prop.Name).GetValue(input[i + 1], null) != null)
                        OldValue = input[i + 1].GetType().GetProperty(prop.Name).GetValue(input[i + 1], null).ToString();

                    if (NewValue != OldValue)
                    {
                        DateTime changesDate = new DateTime();
                        string userName = "";

                        PropertyInfo propResponseDate = item.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                         .FirstOrDefault(x => x.GetCustomAttribute<ColumnAttribute>() != null
                                            && x.GetCustomAttribute<ColumnAttribute>().Name.Equals("SP_RESPONSE_DATE", StringComparison.OrdinalIgnoreCase));
                        PropertyInfo propUserName = item.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                        .FirstOrDefault(x => x.GetCustomAttribute<ColumnAttribute>() != null
                                           && x.GetCustomAttribute<ColumnAttribute>().Name.Equals("USERNAME", StringComparison.OrdinalIgnoreCase));

                        changesDate = propResponseDate != null ? Convert.ToDateTime(propResponseDate.GetValue(input[i], null).ToString()) : new DateTime();
                        userName = propUserName != null ? (propUserName.GetValue(input[i], null) != null ? propUserName.GetValue(input[i], null).ToString() : "") : "";

                        if (!changedVersion)
                        {
                            versionNo++;
                            changedVersion = true;
                        }
                        HistoryList.Add(new HistoryResponse
                        {
                            VersionNo = versionNo,
                            ChangesDate = changesDate.ToShortDateString() + " " + changesDate.ToShortTimeString(),
                            UserName = userName,
                            ColumnName = ColumnName,
                            OldValue = OldValue,
                            NewValue = NewValue
                        });
                    }
                }
            }
            if (HistoryList != null)
                HistoryList = HistoryList.OrderByDescending(x => Convert.ToDateTime(x.ChangesDate)).ToList();
            return HistoryList.OrderByDescending(x => Convert.ToDateTime(x.ChangesDate)).ToList();
        }
        private long objID { get; set; }
        public string baseColumnName { get; set; }
        public List<HistoryResponse> GetHistory(long Id)
        {
            try
            {
                this.objID = Id;
                List<TEntity> lstEntity = new List<TEntity>();
                List<HistoryResponse> returnResponse = new List<HistoryResponse>();
                this.baseColumnName = GetBaseIdName();
                //örn: V_CONTRACT_LIST İCİN   WHERE NEW_VERSION_ID = Id or CONTRACT_ID = Id
                lstEntity = new GenericRepository<TEntity>().FindBy(conditions: $"NEW_VERSION_ID = {Id} OR {this.baseColumnName} = {Id} ", orderby: "sp_response_date desc", fetchHistoricRows: true, fetchDeletedRows: true);
                SubProp(lstEntity[0]);
                if (lstEntity.Count > 0)
                    returnResponse = AnalyzeHistory(lstEntity);

                return returnResponse;
            }
            catch (Exception ex)
            {
                
            }
            return null;
        }
        private string GetBaseIdName()
        {
            string returnBaseId = "";
            //typename'i bulana kadar dön
            foreach (PropertyInfo prop in typeof(TEntity).GetProperties())
            {
                if (prop.GetCustomAttributes(typeof(ColumnAttribute), false).Any())
                {
                    if ((prop.GetCustomAttribute(typeof(ColumnAttribute), false) as ColumnAttribute).TypeName == "BASEID")
                        returnBaseId = (prop.GetCustomAttribute(typeof(ColumnAttribute), false) as ColumnAttribute).Name;
                }
            }
            return returnBaseId;
        }
        private object SubProp(TEntity input)
        {
            object SubContentList = null;
            try
            {
                foreach (PropertyInfo prop in input.GetType().GetProperties())
                {

                    string name = prop.Name;
                    //bool isgenerictype = prop.GetType().IsGenericType;

                    if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                    {
                        Type type = prop.PropertyType.GetGenericArguments()[0];
                        //object subInstance = Activator.CreateInstance(type);
                        //var classAttributes = type.GetCustomAttributes(false);
                        //var tableMapping = classAttributes.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.TableAttribute));
                        //var mapsToTable = tableMapping as System.ComponentModel.DataAnnotations.Schema.TableAttribute;

                        //var listType = typeof(List<>);
                        //var constructedListType = listType.MakeGenericType(type);
                        //var instance = Activator.CreateInstance(constructedListType);

                        Type[] GenericTypes = new Type[] { type };
                        Type listTypeGenericRepo = typeof(GenericRepository<>);
                        Type genericType = listTypeGenericRepo.MakeGenericType(GenericTypes);
                        string pkColumnName = "";

                        foreach (PropertyInfo propz in type.GetProperties())
                        {
                            bool findHim = propz.GetCustomAttributes(typeof(KeyAttribute), false).Any();
                            if (findHim)
                                pkColumnName = (propz.GetCustomAttributes(typeof(ColumnAttribute), false).FirstOrDefault() as ColumnAttribute).Name;
                        }

                        object instanceGenericRepo = Activator.CreateInstance(genericType);


                        object[] parametersArray = new object[] { this.baseColumnName + $" = '{this.objID}' OR NEW_VERSION_ID = {this.objID}", true, true,
                        $" NVL(new_version_id,{pkColumnName}) asc,new_version_id desc , {pkColumnName } asc  ",null};

                        SubContentList = genericType.GetMethod("FindBy").Invoke(instanceGenericRepo, parametersArray);
                        prop.SetValue(input, SubContentList);



                        //Type elementType = Type.GetType(type.FullName.Replace("+", "."));
                        //Type[] types = new Type[] { elementType };

                        //Type listType2 = typeof(GenericRepository<>);
                        //Type genericType = listType2.MakeGenericType(listType2);


                        //MethodInfo method = typeof(GenericRepository<>).GetMethod("FindAll");
                        //MethodInfo generic = method.MakeGenericMethod(type);


                        //generic.Invoke(this,nulll);
                        //IList subIlist = subInstance as IList;
                        // sub objeden List ürettik. bunu doldurup recursive dönmek kaldı
                    }
                }
            }
            catch (Exception ex) { }
            return SubContentList;
        }
    }
}
