﻿using Protein.Common.Dto;
using Protein.Common.Entities;
using Protein.Common.Resources;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Data.Services
{
    public class NetworkService
    {
        public GenericRepository<V_NETWORK> networkViewRepository = new GenericRepository<V_NETWORK>();
        public GenericRepository<Network> networkRepository = new GenericRepository<Network>();

        public ViewResultDto<List<V_NETWORK>> Find(NetworkResource networkResource)
        {
            string conditions = GenerateConditions(networkResource);
            return networkViewRepository.FindByPaged(pageNumber: networkResource.Offset, rowsPerPage: networkResource.Limit, conditions: conditions, orderby: !String.IsNullOrEmpty(networkResource.SortColumnName) ? $"{networkResource.SortColumnName} {networkResource.SortDirection}" : "NETWORK_ID ASC");
        }

        public Network FindById(long id)
        {
            return networkRepository.FindById(id);
        }

        public SpResponse Create(Network entity)
        {
            return networkRepository.Insert(entity);
        }

        public SpResponse Update(Network entity)
        {
            return networkRepository.Update(entity);
        }

        public SpResponse Delete(long id)
        {
            CheckNetworkIsUsedOnAContract(id);
            var network = FindById(id);
            network.Status = Convert.ToString((int)Status.SILINDI);
            return Update(network);
        }

        private string GenerateConditions(NetworkResource networkResource)
        {
            string conditions = "";
            if (networkResource.Id != null && networkResource.Id > 0)
            {
                if (!String.IsNullOrEmpty(conditions))
                {
                    conditions += " AND ";
                }
                conditions += $"NETWORK_ID = '{networkResource.Id}'";
            }
            if (!String.IsNullOrEmpty(networkResource.Name))
            {
                if (!String.IsNullOrEmpty(conditions))
                {
                    conditions += " AND ";
                }
                conditions += $"NETWORK_NAME LIKE '%{networkResource.Name}%'";
            }
            if (networkResource.Type != null)
            {
                if (!String.IsNullOrEmpty(conditions))
                {
                    conditions += " AND ";
                }
                conditions += $"NETWORK_TYPE = '{(int)networkResource.Type}'";
            }
            if (networkResource.Category != null)
            {
                if (!String.IsNullOrEmpty(conditions))
                {
                    conditions += " AND ";
                }
                conditions += $"NETWORK_CATEGORY = '{(int)networkResource.Category}'";
            }
            return conditions;
        }

        private void CheckNetworkIsUsedOnAContract(long Id)
        {
            string whereCondition = "NETWORK_ID = " + Id;
            var resultCheckContracts = new GenericRepository<V_ContractNetwork>().FindBy("NETWORK_ID=:networkId", orderby: "NETWORK_ID", parameters: new { networkId = Id }); 
            if (resultCheckContracts != null && resultCheckContracts.Count > 0)
            {
                string contractNos = string.Join(", ", resultCheckContracts.Select(cn => cn.CONTRACT_NO).ToList());

                //foreach (var contract in resultCheckContracts)
                //{
                //    string no = contract.ContractNo;
                //    contractNos += no + ", ";
                //}
                throw new Exception("Network sözleşmeye bağlı olduğundan silinemez! Sözleşme No(lar)ı : " + contractNos.Substring(0, contractNos.Length - 2));
            }
        }
    }
}
