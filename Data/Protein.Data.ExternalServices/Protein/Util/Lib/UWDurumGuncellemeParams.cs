﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Protein.Util.Lib
{
    public class UWDurumGuncellemeParams
    {
        public string ApiCode { get; set; }
        public long KisiId { get; set; }
        public long PoliceNo { get; set; }
        public long PolSiraNo { get; set; }
        public string IstisnaNot { get; set; }
        public long OBYG { get; set; }
        public DateTime IlkSigTar { get; set; }
        public DateTime GirisTar { get; set; }
    }
}
