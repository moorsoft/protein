﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Protein.Util.Lib
{
    public class Musteri
    {
        public string SonucKodu { get; set; }
        public string SonucAciklama { get; set; }
        public long MusteriId { get; set; }
        public long AileNo { get; set; }
    }
}
