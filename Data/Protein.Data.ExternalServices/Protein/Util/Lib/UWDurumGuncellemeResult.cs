﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Protein.Util.Lib
{
    public class UWDurumGuncellemeResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public SonucBilgi ResultInfo { get; set; }
    }
}
