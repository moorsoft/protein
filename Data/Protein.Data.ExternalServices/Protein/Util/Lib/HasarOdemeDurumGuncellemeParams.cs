﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Protein.Util.Lib
{
    public class HasarOdemeDurumGuncellemeParams
    {
        public string ApiCode { get; set; }
        public long ClaimId { get; set; }
        public DateTime OdemeTar { get; set; }
        public string OdemeKodu { get; set; }
        public string OdemeAciklama { get; set; }
    }
}
