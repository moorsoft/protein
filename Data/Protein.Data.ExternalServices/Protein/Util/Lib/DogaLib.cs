﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Protein.Util.Lib
{

    [Serializable]
    public class DogaPoliceAktarReq
    {
        public string PoliceNo;
        public string YenilemeNo;
        public string ZeylNo;
        public string AcenteNo;
        public string BransKod;
    }

    [Serializable]
    public class DogaPoliceAktarRes
    {
        public int ReqID { get; set; }
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public string[] ErrorList { get; set; }
    }
    [Serializable]
    public class DogaHasarAktarRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public string[] ErrorList { get; set; }
    }
    [Serializable]
    public class DogaIcmalAktarRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public string[] ErrorList { get; set; }
    }


    public class DogaTekPolReq
    {
        public string ErrorMessage { get; set; }
        public string AcenteNo { get; internal set; }
        public string DovizKuru { get; internal set; }
        public string DovizTipi { get; internal set; }
        public string KayitTipi { get; internal set; }
        public string MusteriAdi { get; internal set; }
        public string MusteriBabaAdi { get; internal set; }
        public string MusteriCepTelefonNo { get; internal set; }
        public string MusteriCinsiyet { get; internal set; }
        public string MusteriDogumTarihi { get; internal set; }
        public string MusteriDogumYeri { get; internal set; }
        public string MusteriEPosta { get; internal set; }
        public string MusteriEvApartmanAdi { get; internal set; }
        public string MusteriEvCadde { get; internal set; }
        public string MusteriEvDaire { get; internal set; }
        public string MusteriEvIlceKodu { get; internal set; }
        public string MusteriEvIlKodu { get; internal set; }
        public string MusteriEvKapiNo { get; internal set; }
        public string MusteriEvMahalle { get; internal set; }
        public string MusteriEvSemt { get; internal set; }
        public string MusteriEvSokak { get; internal set; }
        public string MusteriEvTelefonNo { get; internal set; }
        public string MusteriGercekTuzel { get; internal set; }
        public string MusteriMedeniHali { get; internal set; }
        public string MusteriNo { get; internal set; }
        public string MusteriSoyadi { get; internal set; }
        public string MusteriTcKimlikNo { get; internal set; }
        public string MusteriVergiNo { get; internal set; }
        public string MusteriPasaportNo { get; internal set; }
        public string PolicyPaymentType { get; internal set; }
        public string NetPrim { get; internal set; }
        public string PoliceGirisTarihi { get; internal set; }
        public string PoliceIptalTarihi { get; internal set; }
        public string PoliceNo { get; internal set; }
        public string SagmerPoliceNo { get; internal set; }
        public string TaksitDurumu { get; internal set; }
        public string TaksitTipi { get; internal set; }
        public string TaksitTipiAciklama { get; internal set; }
        public string PoliceTanzimTarihi { get; internal set; }
        public string ZeyilNo { get; internal set; }
        public string ZeyilKodu { get; internal set; }
        public string ZeyilAdi { get; internal set; }
        public string SigortaliAdi { get; internal set; }
        public string SigortaliAdres1 { get; internal set; }
        public string SigortaliBabaAdi { get; internal set; }
        public string SigortaliCepTelefonNo { get; internal set; }
        public string SigortaliCinsiyet { get; internal set; }
        public string SigortaliDogumTarihi { get; internal set; }
        public string SigortaliDogumYeri { get; internal set; }
        public string SigortaliEPosta { get; internal set; }
        public string SigortaliEvApartmanAdi { get; internal set; }
        public string SigortaliEvCadde { get; internal set; }
        public string SigortaliEvDaire { get; internal set; }
        public string SigortaliEvIlceKodu { get; internal set; }
        public string SigortaliEvIlKodu { get; internal set; }
        public string SigortaliEvKapiNo { get; internal set; }
        public string SigortaliEvMahalle { get; internal set; }
        public string SigortaliEvSemt { get; internal set; }
        public string SigortaliEvSokak { get; internal set; }
        public string SigortaliEvTelefonNo { get; internal set; }
        public string SigortaliMedeniHali { get; internal set; }
        public string SigortaliNo { get; internal set; }
        public string SigortaliPasaportNo { get; internal set; }
        public string SigortaliPostaKodu { get; internal set; }
        public string SigortaliSoyadi { get; internal set; }
        public string SigortaliTcKimlikNo { get; internal set; }
        public string SigortaliUyruk { get; internal set; }
        public string SigortaliVergiDairesi { get; internal set; }
        public string SigortaliVergiNo { get; internal set; }
        public List<Taksit> taksitList { get; set; }
        public List<EkSigortali> ekSigortaliList { get; set; }
        public string YenilemeNo { get; internal set; }
        public string GrupFerdi { get; internal set; }
        public string PaketNo { get; internal set; }
        public string OncekiPoliceNo { get; internal set; }
        public string YenilemeGorusu { get; internal set; }
        public string ZeyilTipi { get; internal set; }
        public string PolicyStatus { get; internal set; }
        public int PrimType { get; internal set; }
        public long? PoliceId { get; internal set; }
        public string  _SeyahatSaglikTarifeId { get; internal set; }
        public string  _TaksitSayisi{ get; internal set; }
        public string _OncekiPoliceBaslangicTarihi{ get; internal set; }
        public string _OncekiPoliceBitisTarihi{ get; internal set; }
        public string _OncekiPoliceNo{ get; internal set; }
        public string _OncekiSigortaSirketi{ get; internal set; }
        public string _OncekiSirketAdi{ get; internal set; }
        public string _OncekiSirketKodu{ get; internal set; }
        public string PoliceBaslangicTarihi{ get; internal set; }
        public string PoliceBitisTarihi{ get; internal set; }
        public string _SaglikTarifeId { get; internal set; }
        public string _Taksit_Sayisi{ get; internal set; }
        public string _VipKodu{ get; internal set; }
        public string _YenilemeGorusu{ get; internal set; }

        public List<Beyan> BeyanListesi { get; set; }
        public string _BOY { get; set; }
        public string _KILO{ get; set; }
        public string _OzelEkPrim{ get; set; }
        public string _RedNedeni{ get; set; }
        public string _SaglikTarifesiId{ get; set; }
        public string _SigortaliRiskEkPrimi{ get; set; }
        public string _SigortaliUwGorusu{ get; set; }

    }
    public class Beyan
    {
        public string BeyanDesc { get; set; }
    }
        public class Taksit
    {
        public string VadeTarihi { get; set; }
        public string Planlanan { get; set; }
    }

    public class EkSigortali
    {
        public string Adi { get; internal set; }
        public string AnnelikTeminatiBaslangicTarihi { get; internal set; }
        public string BabaAdi { get; internal set; }
        public string Cinsiyet { get; internal set; }
        public string IlkKayitTarihi { get; internal set; }
        public string DogumTarihi { get; internal set; }
        public string EvAdresi { get; internal set; }
        public string EvIlceKodu { get; internal set; }
        public string EvIlKodu { get; internal set; }
        public string IlkAyaktateminatBaslangicTarihi { get; internal set; }
        public string IlkSigortalilikTarihi { get; internal set; }
        public string EkSigortaliCepNo { get; internal set; }
        public string EPosta { get; internal set; }
        public string EkSigortaliIptalKayit { get; internal set; }
        public string GecisTipi { get; internal set; }
        public string OncekiSigortaSirketi { get; internal set; }
        public string ImeceKisiPaket { get; internal set; }
        public string MedeniHali { get; internal set; }
        public string SigortaliNo { get; internal set; }
        public string OmurBoyuYenilemeGarantisi { get; internal set; }
        public string OzelNotKisi { get; internal set; }
        public string PasaportNo { get; internal set; }
        public string Prim { get; internal set; }
        public string SicilNumarasi { get; internal set; }
        public string SigortaliHesapBankaKodu { get; internal set; }
        public string SigortaliIbanNo { get; internal set; }
        public string SigortaliUwGorusu { get; internal set; }
        public string Soyadi { get; internal set; }
        public string TcKimlikNo { get; internal set; }
        public string TelefonNo { get; internal set; }
        public string Uyruk { get; internal set; }
        public string VergiDairesi { get; internal set; }
        public string VergiNo { get; internal set; }
        public string Vip { get; internal set; }
        public string BireyTip { get; internal set; }
        public string YenilemeGarantisiTarihi { get; internal set; }
        public string YenilemeGarantisiTipi { get; internal set; }
        public string YenilemeGarantisiTuru { get; internal set; }
        public string DogumYeri { get; internal set; }
        public string SigortaliRiskEkPrimi { get; internal set; }
    }
}
