﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Protein.Util.Lib
{
    public class CreateMusteriParams
    {
        public string ApiCode { get; set; }
        public long Id { get; set; }
        public int MusteriTipi { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Tckn { get; set; }
        public string Vkn { get; set; }
        public string Ykn { get; set; }
        public string Pasaport { get; set; }
        public int Cinsiyet { get; set; }
        public DateTime DogTar { get; set; }
        public string Uyruk { get; set; }
        public int VIP { get; set; }
        public long AileNo { get; set; }
        public string Tel { get; set; }
        public string Adres { get; set; }
        public string IlKodu { get; set; }
        public string Email { get; set; }
        public string MedeniDurum { get; set; }
        public string Notlar { get; set; }

    }

    public enum MusteriTip
    {
        Kisi,
        Sirket
    }

    public enum CinsiyetTip
    {
        Kadın,
        Erkek
    }
}
