﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Protein
{
    public class ProteinSrvModels
    {
        // NOTE: Eski projedeki objelerdir. Mevcut kullanım için değiştirilmeyecektir.

        public class SONUC
        {
            public int SONUCKODU { get; set; }
            public string SONUCACIKLAMA { get; set; }
        }

        // MUSTERICREATEORUPDATE OBJECTS
        public class MUSTERI
        {
            public long ID { get; set; }
            public int MUSTERITIPI { get; set; }
            public string ADI { get; set; }
            public string SOYADI { get; set; }
            public string TCKN { get; set; }
            public string VKN { get; set; }
            public string YKN { get; set; }
            public string PASAPORTNO { get; set; }
            public int CINSIYET { get; set; }
            public DateTime? DOGUMTARIHI { get; set; }
            public string UYRUK { get; set; }
            public int VIP { get; set; }
            public long AILENO { get; set; }
            public string TEL { get; set; }
            public string ADRES { get; set; }
            public string ILKODU { get; set; }
            public string EMAIL { get; set; }
            public int MEDENIDURUM { get; set; }
            public string NOTLAR { get; set; }
        }
        public class MUSTERISTATUS : SONUC
        {
            public long MUSTERIID { get; set; }
            public long AILENO { get; set; }
        }

        // POLICECREATEUPDATE OBJECTS
        public class POLICE
        {
            public long POLICYNO { get; set; }
            public long POLICYORDER { get; set; }
            public long KAPAKPOLICYNO { get; set; }
            public long KAPAKPOLICYORDER { get; set; }
            public long SIGORTAETTIRENID { get; set; }
            public long POLICETIPI { get; set; }
            public DateTime? BASTARIHI { get; set; }
            public DateTime? BITISTARIHI { get; set; }
            public long ONCEKIPOLICENO { get; set; }
            public long ONCEKIPOLICEORDER { get; set; }
            public string ACENTEKODU { get; set; }
            public string ODEMEPLANI { get; set; }
            public string ODEMESEKLI { get; set; }
        }
        public class ZEYIL
        {
            public long ZEYILNO { get; set; }
            public string ZEYILTIPI { get; set; }
            public DateTime? ZEYILTANZIMTARIHI { get; set; }
            public DateTime? ZEYILMUHASEBETARIHI { get; set; }


        }
        public class SIGORTALILAR
        {
            public SIGORTALI[] ARRAYSIGORTALI { get; set; }
        }
        public class SIGORTALI
        {
            public DateTime? ZEYILGECERLILIKTARIHI { get; set; }
            public long KISIID { get; set; }
            public long RELATIONTYPEID { get; set; } //IndividualType
            public double PRIM { get; set; }
            public long PLANID { get; set; }
            public string ISTISNANOTU { get; set; }
            public long OBYG { get; set; } // RenewalGuaranteeType.YENILEME_GARANTISI_OMUR_BOYU (true:1/false:0)
            public DateTime? ILKSIGORTALANMATARIHI { get; set; }
            public DateTime? SIGORTASIRKETIGIRISTARIHI { get; set; }
            public long KISIPOLICENO { get; set; } //FamilyNo
        }
        public class POLICESTATUS : SONUC
        {
            public long POLICYNO { get; set; }
            public long POLICYORDER { get; set; }
        }

        // ODEMEDURUMU OBJECTS
        public class ODEMEDURUMU
        {
            public long CLAIMID { get; set; }
            public DateTime? ODEMETARIHI { get; set; }
            public string ODEMEKODU { get; set; }
            public string ODEMEACIKLAMA { get; set; }
        }
        public class ODEMEDURUMUSONUC : SONUC
        {
        }

        public class UWUPDATESONUC : SONUC
        {
        }


        public class UWGUNCELLE
        {
            public long KISIID { get; set; }
            public long POLICENO { get; set; }
            public long POLICESIRANO { get; set; }
            public string ISTISNANOTU { get; set; }
            public long OBYG { get; set; }
            public DateTime? ILKSIGORTALANMATARIHI { get; set; }
            public DateTime? SIGORTASIRKETIGIRISTARIHI { get; set; }
        }

        public class UWGUNCELLESONUC : SONUC
        {
            public long INSUREDID { get; set; }
            public long POLICYID { get; set; }
        }
    }
}
