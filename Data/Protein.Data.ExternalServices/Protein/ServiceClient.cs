﻿using Protein.Business.Abstract.Service;
using Protein.Business.Concrete.Services;
using Protein.Common.Constants;
using Protein.Common.Enums;
using Protein.Common.Dto.ImportObjects;
using Protein.Common.Entities;
using Protein.Common.Extensions;
using Protein.Data.ExternalServices.Protein.Util.Lib;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;
using static Protein.Data.ExternalServices.Protein.ProteinSrvModels;
using static Protein.Data.ExternalServices.Protein.ProxySrvModels;
using Protein.Data.ExternalServices.PortTSS.Util;
using Protein.Data.ExternalServices.PortTSS.Util.Lib;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.Log;
using System.Globalization;

namespace Protein.Data.ExternalServices.Protein
{

    public class ServiceClient
    {
        private long CompanyId = 5; // GUNES SIGORTA ID'SI BURAYA GELECEK

        //SIGORTALI.RELATIONTYPEID -> IndividualType
        //    1 -> FERT
        //    2 -> ES
        //    3 -> COCUK

        //POLICE.POLICETIPI -> PolicyType
        //    1 -> FERDI
        //    2 -> GRUP

        private bool ApiCodeIsValid(string ApiKod)
        {
            bool result = false;

            try
            {
                V_CompanyParameter resultCompanyParam = new GenericRepository<V_CompanyParameter>().FindBy($"KEY = '{AppKeys.ApiCode}' AND VALUE = '{ApiKod}' ", orderby: "").FirstOrDefault();
                if (resultCompanyParam != null)
                {
                    CompanyId = (long)resultCompanyParam.CompanyId;
                    return true;
                }
                else
                    return false;

            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
        private bool TcNoCheck(string TCNO)
        {
            bool result = false;

            string tckimlik;

            try
            {
                int index = 0;
                int toplam = 0;
                foreach (char n in TCNO)
                {
                    if (index < 10)
                    {
                        toplam += Convert.ToInt32(char.ToString(n));
                    }
                    index++;
                }
                if (toplam % 10 == Convert.ToInt32(TCNO[10].ToString()))
                    result = true;
                else
                    result = false;
            }
            catch
            {
                result = false;
            }

            return result;
        }
        private void CreateLog(IntegrationLog log,string responseXml, int code, string desc, string logType = "", string httpStatus = "")
        {
            log.HttpStatus = httpStatus.IsNull() ? ((int)HttpStatusCode.NotFound).ToString() : httpStatus;
            log.Type = logType.IsNull() ? ((int)LogType.FAILED).ToString() : logType;
            log.Response = responseXml;
            log.ResponseStatusCode = code.ToString();
            log.ResponseStatusDesc = desc;
            log.WsRequestDate = DateTime.Now;
            IntegrationLogHelper.AddToLog(log);
        }


        // NOTE: Oracle DB ile çalışan servis fonksiyonları
        #region ProteinSrv

        #region CustomerCreateOrUpdate
        public MUSTERISTATUS CustomerCreateOrUpdate(string apiKod, MUSTERI musteri)
        {
            MUSTERISTATUS result = new MUSTERISTATUS();

            var currentMethod = MethodBase.GetCurrentMethod();

            #region IncomingLog
            IntegrationLog log = new IntegrationLog();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.WsMethod = currentMethod.Name;
            log.Request = musteri.ToXML(true);
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.WsRequestDate = DateTime.Now;
            log.Type = ((int)LogType.INCOMING).ToString();
            IntegrationLogHelper.AddToLog(log);
            #endregion

            #region Api Control
            if (!ApiCodeIsValid(apiKod))
            {
                result.SONUCACIKLAMA = "Giriş başarısız";
                result.SONUCKODU = 1;

                CreateLog(log,result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA);

                return result;
            }
            #endregion

            #region Validation
            result = CustomerValidation(musteri);
            if (result.SONUCKODU > 0)
            {
                CreateLog(log, result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA);

                return result;
            }
            #endregion

            try
            {
                #region Waiting Log
                log.Type = ((int)LogType.WAITING).ToString();
                log.WsRequestDate = DateTime.Now;
                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                log.ResponseStatusCode = "200";
                IntegrationLogHelper.AddToLog(log);
                #endregion

                result = CustomerOpr(musteri);
                if (result.SONUCKODU != 0)
                {
                    CreateLog(log, result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA);

                    return result;
                }
            }
            catch (Exception ex)
            {
                result.SONUCKODU = 1;
                result.SONUCACIKLAMA = ex.Message;

                CreateLog(log, result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA);

                return result;
            }
            finally
            {
                CreateLog(log, result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA, ((int)LogType.COMPLETED).ToString(), ((int)HttpStatusCode.OK).ToString());
            }
            return result;
        }

        private MUSTERISTATUS CustomerValidation(MUSTERI musteri)
        {
            MUSTERISTATUS result = new MUSTERISTATUS();

            switch (musteri.UYRUK)
            {
                case "TC":
                case "TRK":
                case "TR":
                case "TÜRKİYE":
                case "9980":
                    if (!musteri.TCKN.IsInt64())
                    {
                        result.SONUCACIKLAMA = "TC BOŞ GEÇİLEMEZ!";
                        result.SONUCKODU = 1;
                        return result;
                    }
                    else
                    {
                        if (((long?)long.Parse(musteri.TCKN)).IsIdentityNoLength() == null)
                        {
                            result.SONUCACIKLAMA = "TCNO 11 HANE OLMALI!";
                            result.SONUCKODU = 1;
                            return result;
                        }
                        else
                        {
                            if (musteri.TCKN.Substring(0, 1) == "9")
                            {
                                result.SONUCACIKLAMA = "UYRUK TC, TCNO YABANCI. LUTFEN KONTROL EDİN!";
                                result.SONUCKODU = 1;
                                return result;
                            }
                            else
                            {
                                if (!TcNoCheck(musteri.TCKN.ToString()))
                                {
                                    result.SONUCACIKLAMA = "GEÇERSİZ TCNO!";
                                    result.SONUCKODU = 1;
                                    return result;
                                }
                            }
                        }
                    }
                    break;
                case "YBN":
                case "YABANCI":
                    if (!musteri.TCKN.IsInt64())
                    {
                        result.SONUCACIKLAMA = "TC BOŞ GEÇİLEMEZ!";
                        result.SONUCKODU = 1;
                        return result;
                    }
                    else
                    {
                        if (((long?)long.Parse(musteri.TCKN)).IsIdentityNoLength() == null)
                        {
                            result.SONUCACIKLAMA = "TCNO 11 HANE OLMALI!";
                            result.SONUCKODU = 1;
                            return result;
                        }
                        else
                        {
                            if (musteri.TCKN.Substring(0,1) == "9")
                            {
                                result.SONUCACIKLAMA = "YABANCI TCNO GİRMEDİNİZ. LUTFEN KONTROL EDİN!";
                                result.SONUCKODU = 1;
                                return result;
                            }
                            else
                            {
                                if (!TcNoCheck(musteri.TCKN.ToString()))
                                {
                                    result.SONUCACIKLAMA = "GEÇERSİZ TCNO!";
                                    result.SONUCKODU = 1;
                                    return result;
                                }
                            }
                        }
                    }
                    break;
                default: break;
            }
            if (musteri.VKN.IsInt64())
            {
                if (((long?)long.Parse(musteri.VKN)).IsTaxNumberLength()==null)
                {
                    result.SONUCACIKLAMA = "VKN 10 HANE OLMALI!";
                    result.SONUCKODU = 1;
                    return result;
                }
            }
            if (musteri.ADI.IsNull())
            {
                result.SONUCACIKLAMA = "MUSTERI ADI GIRMELISINIZ!";
                result.SONUCKODU = 1;
                return result;
            }
            if (musteri.SOYADI.IsNull())
            {
                result.SONUCACIKLAMA = "MUSTERI SOYADI GIRMELISINIZ!";
                result.SONUCKODU = 1;
                return result;
            }
            if(musteri.CINSIYET==1 || musteri.CINSIYET == 2) { }
            else
            {
                result.SONUCACIKLAMA = "CINSIYET BILGISI HATALI (0,1) GIRMELISINIZ!";
                result.SONUCKODU = 1;
                return result;
            }
            if (!musteri.ILKODU.IsNull())
            {
                if (musteri.ILKODU.Length != 3)
                {
                    musteri.ILKODU = musteri.ILKODU.Length == 2 ? ("0" + musteri.ILKODU) : ("00" + musteri.ILKODU);
                }
                City city = new GenericRepository<City>().FindBy($"CODE='{musteri.ILKODU}'").FirstOrDefault();
                if (city == null)
                {
                    result.SONUCACIKLAMA = "GEÇERSİZ İL KODU";
                    result.SONUCKODU = 1;
                    return result;
                }
            }

            V_ContactParameter findCustomerByGunesID = new GenericRepository<V_ContactParameter>().FindBy($"KEY = '{AppKeys.ContactCustomerID}' AND VALUE='{musteri.ID}' AND SYSTEM_TYPE='{((int)SystemType.VAKIF).ToString()}'", orderby: "").FirstOrDefault();

            if (findCustomerByGunesID?.CONTACT_ID == null)
            {
                if (musteri.TCKN.IsInt64() && ((long?)long.Parse(musteri.TCKN)).IsIdentityNoLength()!=null)
                {
                    V_Insured contact = new GenericRepository<V_Insured>().FindBy($"IDENTITY_NO={musteri.TCKN}",orderby:"").FirstOrDefault();
                    if (contact != null)
                    {
                        result.SONUCACIKLAMA = "BU TC KİMLİK NO İLE BAŞKA KAYIT BULUNMAKTADIR.";
                        result.SONUCKODU = 1;
                        return result;
                    }
                }
            }

            return result;
        }

        private MUSTERISTATUS CustomerOpr(MUSTERI musteri)
        {
            MUSTERISTATUS result = new MUSTERISTATUS();

            try
            {
                Contact contact = new Contact();

                V_ContactParameter findCustomerByGunesID = new GenericRepository<V_ContactParameter>().FindBy($"KEY = '{AppKeys.ContactCustomerID}' AND VALUE='{musteri.ID}' AND SYSTEM_TYPE='{((int)SystemType.VAKIF).ToString()}'", orderby: "").FirstOrDefault();

                if (findCustomerByGunesID?.CONTACT_ID != null)
                {
                    contact.Id = (long)findCustomerByGunesID.CONTACT_ID;
                }

                contact.IdentityNo = musteri.TCKN.IsInt64() ? long.Parse(musteri.TCKN) : long.Parse(musteri.YKN);
                contact.Status = ((int)Status.AKTIF).ToString();
                contact.Type = ((int)ContactType.GERCEK).ToString();
                contact.TaxNumber = long.Parse(musteri.VKN);
                contact.Title = musteri.ADI + " " + musteri.SOYADI;

                SpResponse spResponseContact = new GenericRepository<Contact>().Insert(contact);
                if (spResponseContact.Code == "100")
                {
                    result.MUSTERIID = spResponseContact.PkId;

                    Person person = new Person
                    {
                        Birthdate = musteri.DOGUMTARIHI,
                        ContactId = result.MUSTERIID,
                        LastName = musteri.SOYADI,
                        MaritalStatus = musteri.MEDENIDURUM.ToString(),
                        FirstName = musteri.ADI,
                        PassportNo = musteri.PASAPORTNO,
                        IsVip = musteri.VIP.ToString(),
                        Gender = musteri.CINSIYET.ToString()
                    };

                    long personId = 0;
                    try { personId = new GenericRepository<Person>().FindBy($"CONTACT_ID={result.MUSTERIID}").FirstOrDefault().Id; }
                    catch { }
                    person.Id = personId;
                    SpResponse spResponsePerson = new GenericRepository<Person>().Insert(person);
                    if (spResponsePerson.Code != "100")
                    {
                        result.SONUCACIKLAMA = "Kişi Bilgileri Oluşturulamadı.";
                        result.SONUCKODU = 1;
                        return result;

                    }
                    if (!musteri.TEL.IsNull())
                    {
                        Phone phone = new Phone
                        {
                            Type = ((int)PhoneType.MOBIL).ToString(),
                            No = musteri.TEL,
                            CountryId = 222,
                            IsPrimary = "1"
                        };
                        long phoneId = 0;
                        try { phoneId = new GenericRepository<ContactPhone>().FindBy($"CONTACT_ID={result.MUSTERIID}").FirstOrDefault().PhoneId; }
                        catch { }
                        phone.Id = phoneId;
                        SpResponse spResponsePhone = new GenericRepository<Phone>().Insert(phone);
                        if (spResponsePhone.Code != "100")
                        {
                            result.SONUCACIKLAMA = "Telefon Bilgisi Kaydedilemedi.";
                            result.SONUCKODU = 1;
                            return result;
                        }

                        if (phoneId <= 0)
                        {
                            ContactPhone contactPhone = new ContactPhone
                            {
                                PhoneId = spResponsePhone.PkId,
                                ContactId = result.MUSTERIID
                            };
                            SpResponse spResponseContactEmail = new GenericRepository<ContactPhone>().Insert(contactPhone);
                            if (spResponseContactEmail.Code != "100")
                            {
                                result.SONUCACIKLAMA = "Telefon Bilgisi Kaydedilemedi.";
                                result.SONUCKODU = 1;
                                return result;
                            }
                        }
                    }
                    if (!musteri.EMAIL.IsNull())
                    {
                        Email email = new Email
                        {
                            Type = ((int)EmailType.SAHSI).ToString(),
                            Details = musteri.EMAIL,
                            IsPrimary = "1"
                        };
                        long emailId = 0;
                        try { emailId = new GenericRepository<ContactEmail>().FindBy($"CONTACT_ID={result.MUSTERIID}").FirstOrDefault().EmailId; }
                        catch { }
                        email.Id = emailId;
                        SpResponse spResponseEmail = new GenericRepository<Email>().Insert(email);
                        if (spResponseEmail.Code != "100")
                        {
                            result.SONUCACIKLAMA = "Email Bilgisi Kaydedilemedi.";
                            result.SONUCKODU = 1;
                            return result;
                        }
                        if (emailId <= 0)
                        {
                            ContactEmail contactEmail = new ContactEmail
                            {
                                EmailId = spResponseEmail.PkId,
                                ContactId = result.MUSTERIID
                            };
                            SpResponse spResponseContactEmail = new GenericRepository<ContactEmail>().Insert(contactEmail);
                            if (spResponseContactEmail.Code != "100")
                            {
                                result.SONUCACIKLAMA = "Email Bilgisi Kaydedilemedi.";
                                result.SONUCKODU = 1;
                                return result;
                            }
                        }
                    }
                    if (!musteri.NOTLAR.IsNull())
                    {
                        Note note = new Note
                        {
                            Type = ((int)NoteType.OZEL).ToString(),
                            Description = musteri.NOTLAR
                        };
                        long noteId = 0;
                        try { noteId = new GenericRepository<ContactNote>().FindBy($"CONTACT_ID={result.MUSTERIID}").FirstOrDefault().NoteId; }
                        catch { }
                        note.Id = noteId;
                        SpResponse spResponseNote = new GenericRepository<Note>().Insert(note);
                        if (spResponseNote.Code != "100")
                        {
                            result.SONUCACIKLAMA = "Not Bilgisi Kaydedilemedi.";
                            result.SONUCKODU = 1;
                        }
                        if (noteId <= 0)
                        {
                            ContactNote contactNote = new ContactNote
                            {
                                NoteId = spResponseNote.PkId,
                                ContactId = result.MUSTERIID
                            };
                            SpResponse spResponseContactNote = new GenericRepository<ContactNote>().Insert(contactNote);
                            if (spResponseContactNote.Code != "100")
                            {
                                result.SONUCACIKLAMA = "Not Bilgisi Kaydedilemedi.";
                                result.SONUCKODU = 1;
                                return result;
                            }
                        }
                    }
                    if (!musteri.ILKODU.IsNull() && !musteri.ADRES.IsNull())
                    {
                        Address address = new Address
                        {
                            CityId = musteri.ILKODU.IsNull() ? null : new GenericRepository<City>().FindBy($"CODE='{musteri.ILKODU}'").FirstOrDefault()?.Id,
                            Details = musteri.ADRES,
                            Type = ((int)AddressType.EV).ToString(),
                            IsPrimary = "1"
                        };
                        long addressId = 0;
                        try { addressId = new GenericRepository<ContactAddress>().FindBy($"CONTACT_ID={result.MUSTERIID}").FirstOrDefault().AddressId; }
                        catch { }
                        address.Id = addressId;
                        SpResponse spResponseAddress = new GenericRepository<Address>().Insert(address);
                        if (spResponseAddress.Code != "100")
                        {
                            result.SONUCACIKLAMA = "Adres Bilgisi Kaydedilemedi.";
                            result.SONUCKODU = 1;
                        }
                        if (addressId <= 0)
                        {
                            ContactAddress ContactAddress = new ContactAddress
                            {
                                AddressId = spResponseAddress.PkId,
                                ContactId = result.MUSTERIID
                            };
                            SpResponse spResponseContactAddress = new GenericRepository<ContactAddress>().Insert(ContactAddress);
                            if (spResponseContactAddress.Code != "100")
                            {
                                result.SONUCACIKLAMA = "Adres Bilgisi Kaydedilemedi.";
                                result.SONUCKODU = 1;
                                return result;
                            }
                        }
                    }

                    if (contact.Id == 0) //New 
                    {
                        Parameter parameter = new Parameter
                        {
                            Key = AppKeys.ContactCustomerID,
                            Value = musteri.ID.ToString(),
                            SystemType = ((int)SystemType.VAKIF).ToString()
                        };
                        SpResponse spResponseParameter = new GenericRepository<Parameter>().Insert(parameter);
                        if (spResponseParameter.Code != "100")
                        {
                            result.SONUCACIKLAMA = "Parametre Kaydedilemedi.";
                            result.SONUCKODU = 1;
                            return result;

                        }
                        ContactParameter contactParameter = new ContactParameter
                        {
                            ContactId = result.MUSTERIID,
                            ParameterId = spResponseParameter.PkId,
                        };
                        SpResponse spResponseContactParameter = new GenericRepository<ContactParameter>().Insert(contactParameter);
                        if (spResponseContactParameter.Code != "100")
                        {
                            result.SONUCACIKLAMA = "Contact Parametre Kaydedilemedi.";
                            result.SONUCKODU = 1;
                            return result;
                        }
                    }
                }
                else
                {
                    result.SONUCACIKLAMA = "Müşteri Bilgisi Oluşturulamadı.";
                    result.SONUCKODU = 1;
                    return result;

                }
                //contact.tc
                result.SONUCACIKLAMA = "OK";
                result.SONUCKODU = 0;
            }
            catch (Exception ex) { result.SONUCACIKLAMA = ex.Message; result.SONUCKODU = 1; }
            return result;
        }
        #endregion

        #region PoliceCreateOrUpdate
        public POLICESTATUS PoliceCreateOrUpdate(string apiKod, POLICE POLICE, ZEYIL ZEYIL, SIGORTALILAR SIGORTALILAR)
        {
            POLICESTATUS result = new POLICESTATUS();

            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog();

            #region INCOMING Log
            log.WsName = currentMethod.DeclaringType.FullName;
            log.WsMethod = currentMethod.Name;
            log.Request = POLICE.ToXML(true) + ZEYIL.ToXML(true) + SIGORTALILAR.ToXML(true);
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.WsRequestDate = DateTime.Now;
            log.Type = ((int)LogType.INCOMING).ToString();
            IntegrationLogHelper.AddToLog(log);
            #endregion

            #region Api Control
            if (!ApiCodeIsValid(apiKod))
            {
                result.SONUCACIKLAMA = "Giriş başarısız";
                result.SONUCKODU = 1;

                CreateLog(log, result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA);

                return result;
            }
            #endregion

            #region Validation
            result = PoliceValidation(POLICE, ZEYIL, SIGORTALILAR);
            if (result.SONUCKODU != 0)
            {
                CreateLog(log, result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA);

                return result;
            }
            #endregion

            try
            {
                #region WAITING Log
                log.Type = ((int)LogType.WAITING).ToString();
                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                log.ResponseStatusCode = "200";
                IntegrationLogHelper.AddToLog(log);
                #endregion

                #region PoliceOpr
                result = PoliceOpr(POLICE, ZEYIL, SIGORTALILAR);
                if (result.SONUCKODU != 0)
                {
                    CreateLog(log, result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA);

                    return result;
                }
                #endregion
            }
            catch (Exception ex)
            {
                result.SONUCACIKLAMA = ex.Message;
                result.SONUCKODU = 1;

                CreateLog(log, result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA);

            }
            finally
            {
                CreateLog(log, result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA, ((int)LogType.COMPLETED).ToString(), ((int)HttpStatusCode.OK).ToString());
            }
            return result;
        }

        private POLICESTATUS PoliceValidation(POLICE POLICE, ZEYIL ZEYIL, SIGORTALILAR SIGORTALILAR)
        {
            POLICESTATUS result = new POLICESTATUS();

            #region Zorunlu Alan
            if ((!POLICE.KAPAKPOLICYNO.ToString().IsNumeric() || POLICE.KAPAKPOLICYNO <= 0) && (!POLICE.POLICYNO.ToString().IsNumeric() || POLICE.POLICYNO <= 0))
            {
                result.SONUCACIKLAMA = "POLICE NO GİRİLMESİ GEREKMEKTEDİR";
                result.SONUCKODU = 1;
                return result;
            }

            if ((!POLICE.KAPAKPOLICYORDER.ToString().IsNumeric() || POLICE.KAPAKPOLICYORDER < 0) && (!POLICE.POLICYORDER.ToString().IsNumeric() || POLICE.POLICYORDER < 0))
            {
                result.SONUCACIKLAMA = "POLICE ORDER GİRİLMESİ GEREKMEKTEDİR";
                result.SONUCKODU = 1;
                return result;
            }

            if (!POLICE.SIGORTAETTIRENID.ToString().IsNumeric())
            {
                result.SONUCACIKLAMA = "SIGORTA_ETTIREN_ID GİRİLMESİ GEREKMEKTEDİR";
                result.SONUCKODU = 1;
                return result;
            }

            if (!POLICE.ACENTEKODU.ToString().IsNumeric())
            {
                result.SONUCACIKLAMA = "ACENTE_KODU GİRİLMESİ GEREKMEKTEDİR";
                result.SONUCKODU = 1;
                return result;
            }

            if (!ZEYIL.ZEYILNO.ToString().IsNumeric())
            {
                result.SONUCACIKLAMA = "ZEYILNO GİRİLMESİ GEREKMEKTEDİR";
                result.SONUCKODU = 1;
                return result;
            }

            if (SIGORTALILAR.ARRAYSIGORTALI.Length <= 0)
            {
                result.SONUCACIKLAMA = "SIGORTALI GİRİLMESİ GEREKMEKTEDİR";
                result.SONUCKODU = 1;
                return result;
            }
            #endregion

            long policyNo = 0;
            long policyRenewalNo = 0;
            //bool isNewPolicy = false;

            if (POLICE.KAPAKPOLICYNO.ToString().IsNumeric() && POLICE.KAPAKPOLICYNO > 0)
            {
                policyNo = POLICE.KAPAKPOLICYNO;
                policyRenewalNo = POLICE.KAPAKPOLICYORDER < 0 ? 0 : POLICE.KAPAKPOLICYORDER;
            }
            else
            {
                policyNo = POLICE.POLICYNO;
                policyRenewalNo = POLICE.POLICYORDER < 0 ? 0 : POLICE.POLICYORDER;
            }

            Contact insurer = new GenericRepository<Contact>().FindById(POLICE.SIGORTAETTIRENID);
            if (insurer == null)
            {
                result.SONUCACIKLAMA = "SIGORTA_ETTIREN BULUNAMADI";
                result.SONUCKODU = 1;
                return result;
            }

            Agency agency = new GenericRepository<Agency>().FindBy($"NO='{POLICE.ACENTEKODU}' AND COMPANY_ID={CompanyId}").FirstOrDefault();
            if (agency == null)
            {
                result.SONUCACIKLAMA = "ACENTE BULUNAMADI";
                result.SONUCKODU = 1;
                return result;
            }

            V_Policy policy = new GenericRepository<V_Policy>().FindBy($"POLICY_NUMBER={policyNo} AND RENEWAL_NO={policyRenewalNo} AND COMPANY_ID={CompanyId}", orderby: "").FirstOrDefault();
            if (policy == null)
            {
                if (ZEYIL.ZEYILNO != 0 || ZEYIL.ZEYILTIPI != "G")
                {
                    result.SONUCACIKLAMA = "BAŞLANGIÇ ZEYİL GİRİLMESİ GEREKMEKTEDİR";
                    result.SONUCKODU = 1;
                    return result;
                }
            }
            else
            {
                if (ZEYIL.ZEYILNO == 0 || ZEYIL.ZEYILTIPI == "G")
                {
                    result.SONUCACIKLAMA = "BAŞLANGIÇ ZEYİL DAHA ÖNCE GÖNDERİLMİŞ";
                    result.SONUCKODU = 1;
                    return result;
                }
            }

            if (ZEYIL.ZEYILTIPI == "C" || ZEYIL.ZEYILTIPI == "PF")
            {
                foreach (var SIGORTALI in SIGORTALILAR.ARRAYSIGORTALI)
                {
                    var insured = new GenericRepository<V_Insured>().FindBy($"POLICY_ID={policy.POLICY_ID} AND CONTACT_ID={SIGORTALI.KISIID} AND STATUS='{((int)Status.AKTIF).ToString()}'", orderby: "", fetchDeletedRows: true);
                    if (insured == null || insured.Count < 1)
                    {
                        result.SONUCACIKLAMA = "KİŞİ BİLGİSİ BULUNAMADI";
                        result.SONUCKODU = 1;
                        return result;
                    }

                    V_Package package = new GenericRepository<V_Package>().FindBy($"PACKAGE_NO='{SIGORTALI.PLANID}' AND COMPANY_ID={CompanyId}", orderby: "").FirstOrDefault();
                    if (package == null)
                    {
                        result.SONUCACIKLAMA = "PAKET BİLGİSİ BULUNAMADI";
                        result.SONUCKODU = 1;
                        return result;
                    }
                }
            }
            else
            {
                foreach (var SIGORTALI in SIGORTALILAR.ARRAYSIGORTALI)
                {
                    Contact contact = new GenericRepository<Contact>().FindById(SIGORTALI.KISIID);
                    if (contact == null)
                    {
                        result.SONUCACIKLAMA = "KİŞİ BİLGİSİ BULUNAMADI";
                        result.SONUCKODU = 1;
                        return result;
                    }

                    var insured = new GenericRepository<V_Insured>().FindBy($"CONTACT_ID={contact.Id} AND STATUS='{((int)Status.AKTIF).ToString()}'",orderby:"", fetchDeletedRows: true);
                    if (insured.Count > 0)
                    {
                        result.SONUCACIKLAMA = "SIGORTALI DAHA ÖNCE KAYDEDİLMİŞ";
                        result.SONUCKODU = 1;
                        return result;
                    }

                    V_Package package = new GenericRepository<V_Package>().FindBy($"PACKAGE_NO='{SIGORTALI.PLANID}' AND COMPANY_ID={CompanyId}", orderby: "").FirstOrDefault();
                    if (package == null)
                    {
                        result.SONUCACIKLAMA = "PAKET BİLGİSİ BULUNAMADI";
                        result.SONUCKODU = 1;
                        return result;
                    }
                }
            }
            return result;
        }

        public POLICESTATUS PoliceOpr(POLICE POLICE, ZEYIL ZEYIL, SIGORTALILAR SIGORTALILAR)
        {
            POLICESTATUS result = new POLICESTATUS();

            PolicyDto policyDto = new PolicyDto();
            IService<PolicyDto> policyService = new PolicyService();

            long policyNo = 0;
            long policyRenewalNo = 0;
            //bool isNewPolicy = false;

            if (POLICE.KAPAKPOLICYNO.ToString().IsNumeric() && POLICE.KAPAKPOLICYNO > 0)
            {
                policyNo = POLICE.KAPAKPOLICYNO;
                policyRenewalNo = POLICE.KAPAKPOLICYORDER < 0 ? 0 : POLICE.KAPAKPOLICYORDER;
            }
            else
            {
                policyNo = POLICE.POLICYNO;
                policyRenewalNo = POLICE.POLICYORDER < 0 ? 0 : POLICE.POLICYORDER;
            }

            V_Policy policy = new GenericRepository<V_Policy>().FindBy($"POLICY_NUMBER={policyNo} AND RENEWAL_NO={policyRenewalNo} AND COMPANY_ID={CompanyId}", orderby: "").FirstOrDefault();
            if (policy == null)
            {
                policyDto.isPolicyChange = true;

                policyDto.PolicyId = 0;
                policyDto.PolicyType = POLICE.POLICETIPI == 1 ? ((int)PolicyType.FERDI).ToString() : ((int)PolicyType.GRUP).ToString();
                policyDto.PolicyNo = policyNo.ToString();
                policyDto.EndDate = POLICE.BITISTARIHI;
                policyDto.RenewalNo = (int)policyRenewalNo;
                policyDto.AgencyId = POLICE.ACENTEKODU;
                policyDto.InsurerId = POLICE.SIGORTAETTIRENID;
                policyDto.PolicyStatus = ((int)PolicyStatus.TANZIMLI).ToString();
                policyDto.CompanyId = CompanyId;
                var packageView = new GenericRepository<V_Package>().FindBy($"PACKAGE_NO='{SIGORTALILAR.ARRAYSIGORTALI[0].PLANID}' AND COMPANY_ID={CompanyId}",orderby:"").FirstOrDefault();
                if (packageView != null)
                    policyDto.SubProductId = packageView.SUBPRODUCT_ID;

                policyDto.policyIntegrationType = PolicyIntegrationType.WS;

                var response = policyService.ServiceInsert(policyDto);
                if (!response.IsSuccess)
                {
                    result.SONUCACIKLAMA = "POLİÇE KAYDEDİLEMEDİ";
                    result.SONUCKODU = 1;
                    return result;
                }

                policyDto.PolicyId = response.Id;

                policyDto.isPolicyChange = false;
                policyDto.isEndorsementChange = true;

                policyDto.EndorsementId = 0;
                policyDto.EndorsementNo = (int)ZEYIL.ZEYILNO;
                policyDto.EndorsementType = ((int)EndorsementType.BASLANGIC).ToString();
                policyDto.EndorsementStartDate = SIGORTALILAR.ARRAYSIGORTALI[0].ZEYILGECERLILIKTARIHI;
                policyDto.DateOfIssue = ZEYIL.ZEYILTANZIMTARIHI;
                policyDto.EndorsementRegistrationDate = ZEYIL.ZEYILMUHASEBETARIHI.ToString().IsDateTime() ? ZEYIL.ZEYILMUHASEBETARIHI : ZEYIL.ZEYILTANZIMTARIHI;

                response = policyService.ServiceInsert(policyDto);
                if (!response.IsSuccess)
                {
                    result.SONUCACIKLAMA = "ZEYİL KAYDEDİLEMEDİ";
                    result.SONUCKODU = 1;
                    return result;
                }
                policyDto.EndorsementId = response.Id;

                foreach (var SIGORTALI in SIGORTALILAR.ARRAYSIGORTALI)
                {
                    policyDto.isEndorsementChange = false;
                    policyDto.isInsuredChange = true;

                    policyDto.InsuredContactId = SIGORTALI.KISIID;

                    switch (SIGORTALI.RELATIONTYPEID)
                    {
                        case 1:
                            policyDto.InsuredIndividualType = ((int)IndividualType.FERT).ToString();
                            break;
                        case 2:
                            policyDto.InsuredIndividualType = ((int)IndividualType.ES).ToString();
                            break;
                        default:
                            policyDto.InsuredIndividualType = ((int)IndividualType.COCUK).ToString();
                            break;
                    }

                    policyDto.InsuredInitialPremium = (decimal)SIGORTALI.PRIM;
                    policyDto.InsuredPackageId = SIGORTALI.PLANID;

                    if (SIGORTALI.OBYG == 0)
                        policyDto.InsuredRenewalGuaranteeType = null;
                    else
                        policyDto.InsuredRenewalGuaranteeType = ((int)RenewalGuaranteeType.YENILEME_GARANTISI_OMUR_BOYU).ToString();

                    policyDto.InsuredFirstInsuredDate = SIGORTALI.ILKSIGORTALANMATARIHI;
                    policyDto.InsuredCompanyEntranceDate = SIGORTALI.SIGORTASIRKETIGIRISTARIHI;
                    policyDto.InsuredFamilyNo = SIGORTALI.KISIPOLICENO;

                    response = policyService.ServiceInsert(policyDto);
                    if (!response.IsSuccess)
                    {
                        result.SONUCACIKLAMA = "SIGORTALI KAYDEDİLEMEDİ";
                        result.SONUCKODU = 1;
                        return result;
                    }
                }
            }
            else
            {
                policyDto.isPolicyChange = true;

                policyDto.PolicyId = policy.POLICY_ID;
                policyDto.CompanyId = CompanyId;
                policyDto.PolicyType = POLICE.POLICETIPI == 1 ? ((int)PolicyType.FERDI).ToString() : ((int)PolicyType.GRUP).ToString();
                policyDto.PolicyNo = policyNo.ToString();
                policyDto.EndDate = POLICE.BITISTARIHI;
                policyDto.RenewalNo = (int)policyRenewalNo;
                policyDto.AgencyId = POLICE.ACENTEKODU;
                policyDto.InsurerId = POLICE.SIGORTAETTIRENID;
                policyDto.PolicyStatus = ((int)PolicyStatus.TANZIMLI).ToString();
                policyDto.CompanyId = CompanyId;
                policyDto.SubProductId = policy.SUBPRODUCT_ID;

                policyDto.policyIntegrationType = PolicyIntegrationType.WS;

                var response = policyService.ServiceInsert(policyDto);
                if (!response.IsSuccess)
                {
                    result.SONUCACIKLAMA = "POLİÇE KAYDEDİLEMEDİ";
                    result.SONUCKODU = 1;
                    return result;
                }

                policyDto.PolicyId = response.Id;

                policyDto.isPolicyChange = false;
                policyDto.isEndorsementChange = true;

                policyDto.EndorsementId = 0;
                policyDto.EndorsementNo = new GenericRepository<Endorsement>().FindBy($"POLICY_ID={policy.POLICY_ID}", orderby: "ID DESC").FirstOrDefault().No + 1;

                if (ZEYIL.ZEYILTIPI == "G")
                    policyDto.EndorsementType = ((int)EndorsementType.SIGORTALI_GIRISI).ToString();
                else if (ZEYIL.ZEYILTIPI == "C")
                    policyDto.EndorsementType = ((int)EndorsementType.SIGORTALI_CIKISI).ToString();
                else //ZEYIL.ZEYILTIPI == "PF"
                    policyDto.EndorsementType = ((int)EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI).ToString();

                policyDto.EndorsementStartDate = SIGORTALILAR.ARRAYSIGORTALI[0].ZEYILGECERLILIKTARIHI;
                policyDto.DateOfIssue = ZEYIL.ZEYILTANZIMTARIHI;
                policyDto.EndorsementRegistrationDate = ZEYIL.ZEYILMUHASEBETARIHI.ToString().IsDateTime() ? ZEYIL.ZEYILMUHASEBETARIHI : ZEYIL.ZEYILTANZIMTARIHI;

                response = policyService.ServiceInsert(policyDto);
                if (!response.IsSuccess)
                {
                    result.SONUCACIKLAMA = response.Message;
                    result.SONUCKODU = 1;
                    return result;
                }
                policyDto.EndorsementId = response.Id;

                foreach (var SIGORTALI in SIGORTALILAR.ARRAYSIGORTALI)
                {
                    if (ZEYIL.ZEYILTIPI.ToUpper() == "G")
                    {
                        policyDto.isEndorsementChange = false;
                        policyDto.isInsuredChange = true;

                        policyDto.InsuredContactId = SIGORTALI.KISIID;

                        switch (SIGORTALI.RELATIONTYPEID)
                        {
                            case 1:
                                policyDto.InsuredIndividualType = ((int)IndividualType.FERT).ToString();
                                break;
                            case 2:
                                policyDto.InsuredIndividualType = ((int)IndividualType.ES).ToString();
                                break;
                            default:
                                policyDto.InsuredIndividualType = ((int)IndividualType.COCUK).ToString();
                                break;
                        }

                        policyDto.InsuredInitialPremium = (decimal)SIGORTALI.PRIM;
                        policyDto.InsuredPackageId = SIGORTALI.PLANID;

                        if (SIGORTALI.OBYG == 0)
                            policyDto.InsuredRenewalGuaranteeType = null;
                        else
                            policyDto.InsuredRenewalGuaranteeType = ((int)RenewalGuaranteeType.YENILEME_GARANTISI_OMUR_BOYU).ToString();

                        policyDto.InsuredFirstInsuredDate = SIGORTALI.ILKSIGORTALANMATARIHI;
                        policyDto.InsuredCompanyEntranceDate = SIGORTALI.SIGORTASIRKETIGIRISTARIHI;
                        policyDto.InsuredFamilyNo = SIGORTALI.KISIPOLICENO;

                        response = policyService.ServiceInsert(policyDto);
                        if (!response.IsSuccess)
                        {
                            result.SONUCACIKLAMA = "SIGORTALI KAYDEDİLEMEDİ";
                            result.SONUCKODU = 1;
                            return result;
                        }
                    }
                    else
                    {
                        policyDto.isEndorsementChange = false;
                        policyDto.isInsuredChange = true;

                        policyDto.InsuredContactId = SIGORTALI.KISIID;

                        var insured = new GenericRepository<V_Insured>().FindBy($"POLICY_ID={policy.POLICY_ID} AND CONTACT_ID={SIGORTALI.KISIID} AND STATUS='{((int)Status.AKTIF).ToString()}'",orderby:"", fetchDeletedRows: true).FirstOrDefault();
                        string insuredId = insured?.INSURED_ID.ToString();
                        policyDto.InsuredId = insuredId.IsNumeric() ? long.Parse(insuredId) : 0;

                        switch (SIGORTALI.RELATIONTYPEID)
                        {
                            case 1:
                                policyDto.InsuredIndividualType = ((int)IndividualType.FERT).ToString();
                                break;
                            case 2:
                                policyDto.InsuredIndividualType = ((int)IndividualType.ES).ToString();
                                break;
                            default:
                                policyDto.InsuredIndividualType = ((int)IndividualType.COCUK).ToString();
                                break;
                        }

                        policyDto.InsuredInitialPremium = (decimal)SIGORTALI.PRIM;
                        policyDto.InsuredPackageId = SIGORTALI.PLANID;

                        if (SIGORTALI.OBYG == 0)
                            policyDto.InsuredRenewalGuaranteeType = null;
                        else
                            policyDto.InsuredRenewalGuaranteeType = ((int)RenewalGuaranteeType.YENILEME_GARANTISI_OMUR_BOYU).ToString();

                        policyDto.InsuredFirstInsuredDate = SIGORTALI.ILKSIGORTALANMATARIHI;
                        policyDto.InsuredCompanyEntranceDate = SIGORTALI.SIGORTASIRKETIGIRISTARIHI;
                        policyDto.InsuredFamilyNo = SIGORTALI.KISIPOLICENO;

                        if (ZEYIL.ZEYILTIPI.ToUpper() == "C")
                        {
                            policyDto.InsuredStatus = ((int)Status.SILINDI).ToString();
                        }

                        response = policyService.ServiceInsert(policyDto);
                        if (!response.IsSuccess)
                        {
                            result.SONUCACIKLAMA = response.Message;
                            result.SONUCKODU = 1;
                            return result;
                        }
                    }
                }
            }

            return result;
        }
        #endregion

        #region PaymentStatus
        public ODEMEDURUMUSONUC PaymentStatus(string apiKod, ODEMEDURUMU ODEMEDURUMU1)
        {
            ODEMEDURUMUSONUC result = new ODEMEDURUMUSONUC();

            var currentMethod = MethodBase.GetCurrentMethod();

            #region INCOMING Log
            IntegrationLog log = new IntegrationLog();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.WsMethod = currentMethod.Name;
            log.Request = ODEMEDURUMU1.ToXML(true);
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.WsRequestDate = DateTime.Now;
            log.Type = ((int)LogType.INCOMING).ToString();
            IntegrationLogHelper.AddToLog(log);
            #endregion

            #region Api Control
            if (!ApiCodeIsValid(apiKod))
            {
                result.SONUCACIKLAMA = "Giriş başarısız";
                result.SONUCKODU = 1;

                CreateLog(log, result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA);

                return result;
            }
            #endregion

            #region Validation
            result = PaymentStatusValidation(ODEMEDURUMU1);
            if (result.SONUCKODU != 0)
            {
                CreateLog(log, result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA);

                return result;
            }
            #endregion

            try
            {
                #region WAITING Log
                log.Type = ((int)LogType.WAITING).ToString();
                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                log.ResponseStatusCode = "200";
                log.WsRequestDate = DateTime.Now;
                IntegrationLogHelper.AddToLog(log);
                #endregion

                #region PaymentStatusOpr
                result = PaymentStatusOpr(ODEMEDURUMU1);
                if (result.SONUCKODU != 0)
                {
                    CreateLog(log, result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA);

                    return result;
                }
                #endregion
            }
            catch (Exception ex)
            {
                result.SONUCACIKLAMA = ex.Message;
                result.SONUCKODU = 1;

                CreateLog(log, result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA);
            }
            finally
            {
                CreateLog(log, result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA, ((int)LogType.COMPLETED).ToString(), ((int)HttpStatusCode.OK).ToString());
            }
            return result;
        }

        private ODEMEDURUMUSONUC PaymentStatusOpr(ODEMEDURUMU ODEMEDURUMU1)
        {
            ODEMEDURUMUSONUC result = new ODEMEDURUMUSONUC();

            Claim claim = new GenericRepository<Claim>().FindBy($"COMPANY_CLAIM_ID={ODEMEDURUMU1.CLAIMID} AND COMPANY_ID={CompanyId}").SingleOrDefault();

            if (claim.PayrollId.ToString().IsInt64())
            {
                Payroll payroll = new GenericRepository<Payroll>().FindBy($"ID={claim.PayrollId} AND COMPANY_ID={CompanyId}").SingleOrDefault();
                if (payroll == null)
                {
                    result.SONUCACIKLAMA = "PAYROLL BİLGİSİ BULUNAMADI";
                    result.SONUCKODU = 1;
                    return result;
                }
                if (ODEMEDURUMU1.ODEMETARIHI.ToString().IsDateTime())
                {
                    payroll.PaymentDate = ODEMEDURUMU1.ODEMETARIHI;
                    //payroll.Status = ((int)(PayrollStatus.Onay)).ToString();

                    claim.Status = ((int)ClaimStatus.ODENDI).ToString();

                }
                else
                {
                    payroll.PaymentDate = null;
                    //payroll.Status = ((int)(PayrollStatus.Onay_Bekler)).ToString();

                    claim.Status = ((int)ClaimStatus.ODENECEK).ToString();
                }
                SpResponse spResponsePayroll = new GenericRepository<Payroll>().Update(payroll);
                if (spResponsePayroll.Code != "100")
                {
                    result.SONUCACIKLAMA = "PAYROLL GÜNCELLEME İŞLEME YAPILAMADI";
                    result.SONUCKODU = 1;
                    return result;
                }
                SpResponse spResponseClaim = new GenericRepository<Claim>().Update(claim);
                if (spResponseClaim.Code != "100")
                {
                    result.SONUCACIKLAMA = "HASAR GÜNCELLEME İŞLEME YAPILAMADI";
                    result.SONUCKODU = 1;
                    return result;
                }
            }
            else
            {
                result.SONUCACIKLAMA = "HASARA AİT ZARF BİLGİSİ BULUNAMADI";
                result.SONUCKODU = 1;
                return result;
            }

            result.SONUCKODU = 0;
            result.SONUCACIKLAMA = "OK";
            return result;
        }

        private ODEMEDURUMUSONUC PaymentStatusValidation(ODEMEDURUMU ODEMEDURUMU1)
        {
            ODEMEDURUMUSONUC result = new ODEMEDURUMUSONUC();

            if (!ODEMEDURUMU1.CLAIMID.ToString().IsNumeric())
            {
                result.SONUCACIKLAMA = "CLAIM ID GİRİLMESİ GEREKMEKTEDİR";
                result.SONUCKODU = 1;
                return result;
            }

            Claim claim = new GenericRepository<Claim>().FindBy($"COMPANY_ID={CompanyId} AND COMPANY_CLAIM_ID={ODEMEDURUMU1.CLAIMID}").FirstOrDefault();
            if (claim == null)
            {
                result.SONUCACIKLAMA = "HASAR BİLGİSİ BULUNAMADI";
                result.SONUCKODU = 1;
                return result;
            }

            if (ODEMEDURUMU1.ODEMETARIHI.ToString().IsDateTime() && claim.Status == ((int)ClaimStatus.ODENECEK).ToString() && claim.PayrollId != null)
            {
                Payroll payroll = new GenericRepository<Payroll>().FindBy($"ID={claim.PayrollId} AND COMPANY_ID={CompanyId}").SingleOrDefault();
                if (payroll == null)
                {
                    result.SONUCACIKLAMA = "ZARF BİLGİSİ BULUNAMADI";
                    result.SONUCKODU = 1;
                    return result;
                }
                if (payroll.PaymentDate == ODEMEDURUMU1.ODEMETARIHI)
                {
                    result.SONUCACIKLAMA = "BU KAYIT DAHA ÖNCE GÖNDERİLMİŞ";
                    result.SONUCKODU = 1;
                    return result;
                }
            }
            else if (!ODEMEDURUMU1.ODEMETARIHI.ToString().IsDateTime() && claim.Status == ((int)ClaimStatus.ODENDI).ToString() && claim.PayrollId != null)
            {
                Payroll payroll = new GenericRepository<Payroll>().FindBy($"ID={claim.PayrollId} AND COMPANY_ID={CompanyId}").SingleOrDefault();
                if (payroll == null)
                {
                    result.SONUCACIKLAMA = "ZARF BİLGİSİ BULUNAMADI";
                    result.SONUCKODU = 1;
                    return result;
                }
                if (payroll.PaymentDate == null)
                {
                    result.SONUCACIKLAMA = "BU KAYIT DAHA ÖNCE GÖNDERİLMİŞ";
                    result.SONUCKODU = 1;
                    return result;
                }
            }
            else
            {
                result.SONUCACIKLAMA = "HASAR DURUMU UYGUN DEĞİL";
                result.SONUCKODU = 1;
                return result;
            }

            result.SONUCKODU = 0;
            return result;
        }
        #endregion

        #region UWUPDATE
        public UWUPDATESONUC UWUpdate(string apiKod, UWGUNCELLE UW)
        {
            var currentMethod = MethodBase.GetCurrentMethod();

            UWUPDATESONUC result = new UWUPDATESONUC();

            #region INCOMING Log
            IntegrationLog log = new IntegrationLog();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.WsMethod = currentMethod.Name;
            log.Request = UW.ToXML(true);
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.WsRequestDate = DateTime.Now;
            log.Type = ((int)LogType.INCOMING).ToString();
            IntegrationLogHelper.AddToLog(log);
            #endregion
            
            #region Api Control
            if (!ApiCodeIsValid(apiKod))
            {
                result.SONUCACIKLAMA = "Giriş başarısız";
                result.SONUCKODU = 1;

                CreateLog(log, result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA);

                return result;
            }
            #endregion

            #region Validation
            result = UWUpdateValidation(UW);
            if (result.SONUCKODU == 1)
            {
                CreateLog(log, result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA);

                return result;
            }
            #endregion

            try
            {
                #region WAITING Log
                log.Type = ((int)LogType.WAITING).ToString();
                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                log.ResponseStatusCode = "200";
                log.WsRequestDate = DateTime.Now;
                IntegrationLogHelper.AddToLog(log);
                #endregion

                #region UWOpr
                result = UWOpr(UW);
                if (result.SONUCKODU != 0)
                {
                    CreateLog(log, result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA);

                    return result;
                }
                #endregion
            }
            catch (Exception ex)
            {
                result.SONUCKODU = 1;
                result.SONUCACIKLAMA = ex.Message;

                CreateLog(log, result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA);
            }
            finally
            {
                CreateLog(log, result.ToXML(true), result.SONUCKODU, result.SONUCACIKLAMA, ((int)LogType.COMPLETED).ToString(), ((int)HttpStatusCode.OK).ToString());
            }

            return result;
        }

        private UWUPDATESONUC UWUpdateValidation(UWGUNCELLE UW)
        {
            UWUPDATESONUC result = new UWUPDATESONUC();

            #region Zorunlu Alan Kontrol
            if (!UW.POLICENO.ToString().IsNumeric())
            {
                result.SONUCACIKLAMA = "POLİÇE NO GİRİLMESİ GEREKMEKTEDİR";
                result.SONUCKODU = 1;
                return result;
            }

            if (!UW.POLICESIRANO.ToString().IsNumeric())
            {
                result.SONUCACIKLAMA = "POLİÇE SIRA NO GİRİLMESİ GEREKMEKTEDİR";
                result.SONUCKODU = 1;
                return result;
            }

            if (!UW.KISIID.ToString().IsNumeric())
            {
                result.SONUCACIKLAMA = "KİŞİ ID GİRİLMESİ GEREKMEKTEDİR";
                result.SONUCKODU = 1;
                return result;
            }
            #endregion
            
            result.SONUCKODU = 0;
            return result;
        }

        private UWUPDATESONUC UWOpr(UWGUNCELLE UW)
        {
            UWUPDATESONUC result = new UWUPDATESONUC();

            #region Data Kontrol
            V_Policy policy = new GenericRepository<V_Policy>().FindBy($"POLICY_NUMBER= {UW.POLICENO} AND RENEWAL_NO={UW.POLICESIRANO} AND COMPANY_ID={CompanyId}", orderby: "").FirstOrDefault();
            if (policy == null)
            {
                result.SONUCACIKLAMA = "POLİÇE BİLGİSİ BULUNAMADI";
                result.SONUCKODU = 1;
                return result;
            }

            Contact contact = new GenericRepository<Contact>().FindById(UW.KISIID);
            if (contact == null)
            {
                result.SONUCACIKLAMA = "KİŞİ BİLGİSİ BULUNAMADI";
                result.SONUCKODU = 1;
                return result;
            }

            var insuredView = new GenericRepository<V_Insured>().FindBy($"CONTACT_ID={contact.Id} AND POLICY_ID={policy.POLICY_ID}", orderby: "").FirstOrDefault();
            if (insuredView == null)
            {
                result.SONUCACIKLAMA = "SİGORTALI BİLGİSİ BULUNAMADI";
                result.SONUCKODU = 1;
                return result;
            }
            #endregion

            Insured insured = new GenericRepository<Insured>().FindById(insuredView.INSURED_ID);
            if (insured == null)
            {
                result.SONUCACIKLAMA = "SİGORTALI BİLGİSİ BULUNAMADI";
                result.SONUCKODU = 1;
                return result;
            }

            if (UW.OBYG.ToString().IsInt64() && UW.OBYG == 1)
            {
                insured.RenewalGuaranteeType = ((int)(RenewalGuaranteeType.YENILEME_GARANTISI_OMUR_BOYU)).ToString();
            }
            else
            {
                if (insured.RenewalGuaranteeType == ((int)(RenewalGuaranteeType.YENILEME_GARANTISI_OMUR_BOYU)).ToString())
                {
                    insured.RenewalGuaranteeType = null;
                }
            }

            if (UW.ILKSIGORTALANMATARIHI.ToString().IsDateTime())
            {
                insured.FirstInsuredDate = UW.ILKSIGORTALANMATARIHI;
            }
            else
            {
                insured.FirstInsuredDate = null;
            }

            if (UW.SIGORTASIRKETIGIRISTARIHI.ToString().IsDateTime())
            {
                insured.CompanyEntranceDate = UW.SIGORTASIRKETIGIRISTARIHI;
            }
            else
            {
                insured.CompanyEntranceDate = null;
            }

            var policyView = new GenericRepository<V_Policy>().FindBy($"POLICY_ID={policy.POLICY_ID}", orderby: "").FirstOrDefault();

            if (!UW.ISTISNANOTU.IsNull())
            {
                Exclusion exclusion = new GenericRepository<Exclusion>().FindBy($"CONTACT_ID={UW.KISIID} AND DESCRIPTION LIKE '%{UW.ISTISNANOTU}%'").FirstOrDefault();
                if (exclusion == null)
                {
                    Exclusion newExclusion = new Exclusion
                    {
                        ContactId = UW.KISIID,
                        Description = UW.ISTISNANOTU
                    };
                    SpResponse spResponse = new GenericRepository<Exclusion>().Insert(newExclusion);
                    if (spResponse.Code != "100")
                    {
                        result.SONUCACIKLAMA = "İSTİSNA NOTU KAYDEDİLEMEDİ.";
                        result.SONUCKODU = 1;
                        return result;
                    }
                    string startDate = Convert.ToString(policyView.POLICY_START_DATE);
                    string endDate = Convert.ToString(policyView.POLICY_END_DATE);

                    InsuredExclusion insuredExclusion = new InsuredExclusion
                    {
                        ExclusionId = spResponse.PkId,
                        InsuredId = insuredView.INSURED_ID,
                        StartDate = DateTime.Parse(startDate),
                        EndDate = endDate.IsDateTime() ? (DateTime?)DateTime.Parse(endDate) : null
                    };
                    SpResponse spResponseExclusion = new GenericRepository<InsuredExclusion>().Insert(insuredExclusion);
                    if (spResponseExclusion.Code != "100")
                    {
                        result.SONUCACIKLAMA = "SİGORTALIYA İSTİSNA NOTU EKLENEMEDİ.";
                        result.SONUCKODU = 1;
                        return result;
                    }
                }
                else
                {
                    exclusion.Description = UW.ISTISNANOTU;
                    SpResponse spResponse = new GenericRepository<Exclusion>().Insert(exclusion);
                    if (spResponse.Code != "100")
                    {
                        result.SONUCACIKLAMA = "İSTİSNA NOTU KAYDEDİLEMEDİ.";
                        result.SONUCKODU = 1;
                        return result;
                    }
                }
            }

            SpResponse spResponseInsured = new GenericRepository<Insured>().Update(insured);
            if (spResponseInsured.Code != "100")
            {
                result.SONUCACIKLAMA = "SİGORTALI BİLGİLERİ GÜNCELLENEMEDİ.";
                result.SONUCKODU = 1;
                return result;
            }

            result.SONUCKODU = 0;
            result.SONUCACIKLAMA = "OK";
            return result;
        }
        #endregion

        #endregion



        // NOTE: PUSULA servis ile çalışan servis metodları
        #region ProxySrv

        public Sonuc ProvisioningConformityInquiry(string sirketKod, long GrupPoliceNo, long MusteriId, int olayTarGun, int olayTarAy, int olayTarYil, long PoliceNo, long YenilemeNo)
        {
            Sonuc res = new Sonuc();
            CompanyParam companyParam = new Common.Helper.CompanyHelper().GetCompanyParameters(long.Parse(sirketKod));
            GunesHelper g = new GunesHelper(companyParam: companyParam);
            if (g == null || g._servicecDesc == null)
            {
                res.SonucKodu = "1";
                res.SonucAciklama = "Güneş Servisine Bağlantı Sağlanamadı! Tüm bilgilerin Sistemde kayıt edilmiş olduğunu kontrol ediniz...";
                return res;
            }
            SaglikProvizyonUygunlukKontrolParam p = new SaglikProvizyonUygunlukKontrolParam();
            p.GrupPoliceNo = GrupPoliceNo;
            p.MusteriId = MusteriId;
            p.OlayTarihi = new DateTime(olayTarYil, olayTarAy, olayTarGun);
            p.PoliceNo = PoliceNo;
            p.YenilemeNo = YenilemeNo;

            SaglikProvizyonUygunlukKontrolResult s = g.ProvKontrol(p);


            res.SonucAciklama = s.SonucAciklama;
            res.SonucKodu = s.SonucKodu;
            return res;
        } //hak sahipligi sorgula

        public string CreatePackage(string sirketKod, string cozumOrtagiKodu, long entegrasyonPaketId, string gelisTar_yil, string gelisTar_ay, string gelisTar_gun, long[] provIdList) //icmail aktarım
        {
            var currentMethod = MethodBase.GetCurrentMethod();

            CompanyParam companyParam = new Common.Helper.CompanyHelper().GetCompanyParameters(long.Parse(sirketKod));
            GunesHelper srv = new GunesHelper(companyParam: companyParam);
            if (srv == null || srv._servicecDesc == null)
            {
                return "Servise Bağlantı Sağlanamadı! Tüm bilgilerin Sistemde kayıt edilmiş olduğunu kontrol ediniz...";
            }
            string input = "";
            string output = "";

            IntegrationLog log = new IntegrationLog();

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:sag=\"http://saglik.ws.entegrasyon.pusula.gunessigorta.com/\">");
                sb.Append("<soapenv:Header>");
                sb.Append("<wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">");
                sb.Append("<wsse:UsernameToken xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">");
                sb.Append("<wsse:Username>" + srv._servicecDesc.UserName + "</wsse:Username>");
                sb.Append("<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">" + srv._servicecDesc.Pwd + "</wsse:Password>");
                sb.Append("</wsse:UsernameToken>");
                sb.Append("</wsse:Security>");
                sb.Append("</soapenv:Header>");

                sb.Append("<soapenv:Body>");
                sb.Append("<sag:paketOlustur>");
                sb.Append("<paketOlusturInput>");
                sb.Append("<cozumOrtagiKodu>" + cozumOrtagiKodu + "</cozumOrtagiKodu>");
                sb.Append("<entegrasyonPaketId>" + entegrasyonPaketId + "</entegrasyonPaketId>");
                sb.Append("<gelisTarihi>" + gelisTar_yil + "-" + gelisTar_ay + "-" + gelisTar_gun + "</gelisTarihi>");
                for (int i = 0; i < provIdList.Length; i++)
                {
                    sb.Append("<provizyonIdListesi>" + provIdList[i].ToString() + "</provizyonIdListesi>");
                }
                sb.Append("</paketOlusturInput>");
                sb.Append("</sag:paketOlustur>");
                sb.Append("</soapenv:Body>");
                sb.Append("</soapenv:Envelope>");

                input = sb.ToString();


                log.WsName = currentMethod.DeclaringType.FullName;
                log.WsMethod = currentMethod.Name;
                log.Request = input;
                //log.Request = MUSTERI.ToXML(true);
                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                log.WsRequestDate = DateTime.Now;
                log.Type = ((int)LogType.INCOMING).ToString();
                IntegrationLogHelper.AddToLog(log);


                log.WsName = currentMethod.DeclaringType.FullName;
                log.WsMethod = currentMethod.Name;
                log.Request = input;
                //log.Request = MUSTERI.ToXML(true);
                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                log.WsRequestDate = DateTime.Now;
                log.Type = ((int)LogType.WAITING).ToString();
                IntegrationLogHelper.AddToLog(log);

                string url = string.Format("{0}?paketOlustur", srv._servicecDesc.URL);
                output = srv.CallService(input, url);
                //output = srv.CallService(input, srv._servicecDesc.URL);
                log.Response = output;
                log.WsResponseDate = DateTime.Now;
                log.ResponseStatusCode = "100";
                log.ResponseStatusDesc = "OK";
            }
            catch (Exception ex)
            {
                log.WsName = currentMethod.DeclaringType.FullName;
                log.WsMethod = currentMethod.Name;
                log.Request = input;
                //log.Request = MUSTERI.ToXML(true);
                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                log.WsRequestDate = DateTime.Now;
                log.ResponseStatusCode = "999";
                log.ResponseStatusDesc = " : PAYROLL_ID: " + entegrasyonPaketId + "---" + ex.ToString();
                log.Type = ((int)LogType.FAILED).ToString();
                IntegrationLogHelper.AddToLog(log);
                throw ex;
            }
            finally
            {
                log.WsName = currentMethod.DeclaringType.FullName;
                log.WsMethod = currentMethod.Name;
                log.Request = input;
                //log.Request = MUSTERI.ToXML(true);
                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                log.WsRequestDate = DateTime.Now;
                log.ResponseStatusCode = "100";
                log.ResponseStatusDesc = " : PAYROLL_ID: " + entegrasyonPaketId;
                log.Type = ((int)LogType.COMPLETED).ToString();
                IntegrationLogHelper.AddToLog(log);
            }

            return output;
        }

        public SaglikProvizyonKaydetResult ProvisionSave(string sirketKod, SaglikProvizyonKaydetParam param) //hasar aktarım
        {
            SaglikProvizyonKaydetResult res = new SaglikProvizyonKaydetResult();
            IntegrationLog log = new IntegrationLog();
            log.CorrelationId = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString();
            var currentMethod = MethodBase.GetCurrentMethod();

            try
            {
                if (param.TaniListesi == null || param.TaniListesi.Count == 0)
                {
                    res.SonucAciklama = "Tanı listesi boş olamaz..";
                    return res;
                }

                if (param.IslemListesi == null || param.IslemListesi.Count == 0)
                {
                    res.SonucAciklama = "İşlem listesi boş olamaz..";
                    return res;
                }

                #region Incoming Log
                log.WsName = currentMethod.DeclaringType.FullName;
                log.WsMethod = currentMethod.Name;
                log.Request = param.ToXML(true);
                //log.Request = MUSTERI.ToXML(true);
                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                log.WsRequestDate = DateTime.Now;
                log.Type = ((int)LogType.INCOMING).ToString();
                IntegrationLogHelper.AddToLog(log);
                #endregion

                CompanyParam companyParam = new Common.Helper.CompanyHelper().GetCompanyParameters(long.Parse(sirketKod));
                GunesHelper helper = new GunesHelper(companyParam: companyParam);
                if (helper == null || helper._servicecDesc == null)
                {
                    res.SonucKodu = "1";
                    res.SonucAciklama = "Servise Bağlantı Sağlanamadı! Tüm bilgilerin Sistemde kayıt edilmiş olduğunu kontrol ediniz...";
                    return res;
                }
                #region Waiting Log
                log.WsName = currentMethod.DeclaringType.FullName;
                log.WsMethod = currentMethod.Name;
                log.Request = param.ToXML(true);
                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                log.WsRequestDate = DateTime.Now;
                log.Type = ((int)LogType.WAITING).ToString();
                IntegrationLogHelper.AddToLog(log);
                #endregion
                res = helper.ProvKaydet(param);

                if (res.SonucKodu!="0")
                {
                    #region error Log
                    log.WsName = currentMethod.DeclaringType.FullName;
                    log.WsMethod = currentMethod.Name;
                    log.Request = param.ToXML(true);
                    log.Response = res.ToXML(true);
                    log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                    log.ResponseStatusCode = "999";
                    log.ResponseStatusDesc = res.SonucAciklama;
                    log.WsResponseDate = DateTime.Now;
                    log.Type = ((int)LogType.FAILED).ToString();
                    IntegrationLogHelper.AddToLog(log);
                    #endregion
                    return res;
                }
                #region Completed Log
                log.WsName = currentMethod.DeclaringType.FullName;
                log.WsMethod = currentMethod.Name;
                log.Request = param.ToXML(true);
                log.Response = res.ToXML(true);
                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                log.ResponseStatusCode = "100";
                log.ResponseStatusDesc = "OK";
                log.WsResponseDate = DateTime.Now;
                log.Type = ((int)LogType.COMPLETED).ToString();
                IntegrationLogHelper.AddToLog(log);
                #endregion

                return res;
            }
            catch (Exception ex)
            {
                res.SonucAciklama = string.Format("Runtime Error: {0}", ex.Message);

                #region Completed Log
                log.WsName = currentMethod.DeclaringType.FullName;
                log.WsMethod = currentMethod.Name;
                log.Request = param.ToXML(true);
                log.Response = res.ToXML(true);
                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                log.ResponseStatusCode = "999";
                log.ResponseStatusDesc = ex.ToString();
                log.WsResponseDate = DateTime.Now;
                log.Type = ((int)LogType.FAILED).ToString();
                IntegrationLogHelper.AddToLog(log);
                #endregion
            }
            finally
            {
                //LogSrv _logSrv = new LogSrv();
                //LogSrv.LogItem logItem = new LogSrv.LogItem();
                //logItem.ServisAd = "SaglikWS.asmx.ProvKaydet";
                //logItem.Request = _logSrv.CaptureHttpReq(HttpContext.Current); ;
                //logItem.Response = JsonConvert.SerializeObject(res);


                //LogSrv.LogExtension logExItem = new LogSrv.LogExtension();
                //logExItem.SirketKod = sirketKod;
                //logExItem.ExtHasarID = res.ProvizyonId;
                //logExItem.ExtMusteriID = param.MusteriId;
                //logExItem.ExtPoliceNo = param.PoliceNo;
                //logExItem.KurumKod = param.CozumOrtagiId;
                //logExItem.ProteinProvNo = param.EntegrasyonProvizyonId;
                //logExItem.SonucKod = res.SonucKodu;
                //logExItem.SonucAciklama = res.SonucAciklama;

                //_logSrv.InsertLogItem(logItem, logExItem);

            }
            return res;
        }

        public CreateClaimResult CreateClaim(string sirketKod, CreateClaimParam param)
        {
            var currentMethod = MethodBase.GetCurrentMethod();
            CreateClaimResult result = new CreateClaimResult();

            CompanyParam companyParam = new Common.Helper.CompanyHelper().GetCompanyParameters(long.Parse(sirketKod));
            GunesHelper srv = new GunesHelper(companyParam: companyParam);
            if (srv == null || srv._servicecDesc == null)
            {
                result.ResultCode = 1;
                result.ResultDesc = "Servise Bağlantı Sağlanamadı! Tüm bilgilerin Sistemde kayıt edilmiş olduğunu kontrol ediniz...";
                return result;
            }
            string input = "";
            string output = "";

            IntegrationLog log = new IntegrationLog();
            log.CorrelationId = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString();

            try
            {
                StringBuilder sb = new StringBuilder();

                sb.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">");

                sb.Append(" <soapenv:Header/>");

                sb.Append(" <soapenv:Body>");

                sb.Append(" <tem:CreateClaim>");

                sb.Append(string.Format(" <tem:createClaimInputType>"));

                sb.Append(string.Format(" <tem:ClaimId>{0}</tem:ClaimId>", param.ClaimId));

                sb.Append(string.Format(" <tem:ClaimDetails>"));

                foreach (ClaimDetailInputType claim in param.ClaimDetails)

                {

                    sb.Append(string.Format(" <tem:ClaimDetailInputType>"));

                    sb.Append(string.Format(" <tem:BenefitCode>{0}</tem:BenefitCode>", claim.BenefitCode));

                    decimal invoiceAmount = Convert.ToDecimal(claim.InvoiceAmount);

                    decimal payableAmount = Convert.ToDecimal(claim.PayableAmount);

                    decimal coinsuranceAmount = Convert.ToDecimal(claim.Coinsurance);

                    sb.Append(string.Format(" <tem:InvoiceAmount>{0}</tem:InvoiceAmount>", invoiceAmount.ToString(new CultureInfo("en-US"))));

                    sb.Append(string.Format(" <tem:PayableAmount>{0}</tem:PayableAmount>", payableAmount.ToString(new CultureInfo("en-US"))));

                    sb.Append(string.Format(" <tem:Coinsurance>{0}</tem:Coinsurance>", coinsuranceAmount.ToString(new CultureInfo("en-US"))));

                    DateTime startDate = Convert.ToDateTime(claim.StartDate);

                    sb.Append(string.Format(" <tem:StartDate>{0}</tem:StartDate>", startDate));

                    DateTime finishDate = Convert.ToDateTime(claim.FinishDate);

                    sb.Append(string.Format(" <tem:FinishDate>{0}</tem:FinishDate>", finishDate));

                    sb.Append(string.Format(" <tem:InvoiceNumber>{0}</tem:InvoiceNumber>", claim.InvoiceNumber));

                    DateTime invoiceDate = Convert.ToDateTime(claim.InvoiceDate);

                    sb.Append(string.Format(" <tem:InvoiceDate>{0}</tem:InvoiceDate>", invoiceDate));

                    sb.Append(string.Format(" <tem:InvoiceExclusion>{0}</tem:InvoiceExclusion>", claim.InvoiceExclusion));

                    sb.Append(string.Format(" </tem:ClaimDetailInputType>"));

                }

                sb.Append(string.Format(" </tem:ClaimDetails>"));

                DateTime eventDate = Convert.ToDateTime(param.EventDate);

                sb.Append(string.Format(" <tem:EventDate>{0}</tem:EventDate>", eventDate));// _narHelper.GetDateWithOutTime(param.EventDate)));

                sb.Append(string.Format(" <tem:PolicyNumber>{0}</tem:PolicyNumber>", param.PolicyNumber));

                sb.Append(string.Format(" <tem:CustomerId>{0}</tem:CustomerId>", param.CustomerId));

                sb.Append(string.Format(" <tem:HealthCenterId>{0}</tem:HealthCenterId>", param.HealthCenterId));

                sb.Append(string.Format(" <tem:PlanId>{0}</tem:PlanId>", param.PlanId));

                sb.Append(string.Format(" <tem:Iban>{0}</tem:Iban>", param.Iban));

                DateTime payDueDate = Convert.ToDateTime(param.PayDueDate);

                sb.Append(string.Format(" <tem:PayDueDate>{0}</tem:PayDueDate>", payDueDate));

                sb.Append(string.Format(" <tem:SipClaimId>{0}</tem:SipClaimId>", param.SipClaimId));

                sb.Append(string.Format(" <tem:ClaimStatus>{0}</tem:ClaimStatus>", param.ClaimStatus));

                sb.Append(string.Format(" </tem:createClaimInputType>"));

                sb.Append(string.Format(" </tem:CreateClaim>"));

                sb.Append(string.Format(" </soapenv:Body>"));

                sb.Append(string.Format("</soapenv:Envelope>"));

                input = sb.ToString();


                log.WsName = currentMethod.DeclaringType.FullName;
                log.WsMethod = currentMethod.Name;
                log.Request = input;
                //log.Request = MUSTERI.ToXML(true);
                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                log.WsRequestDate = DateTime.Now;
                log.Type = ((int)LogType.INCOMING).ToString();
                IntegrationLogHelper.AddToLog(log);

                string url = string.Format("{0}?CreateClaim", "http://eacentewsdl.gunessigorta.com.tr/eAcenteWSDL/WebServices/AgitoClaimWs.asmx");
                output = srv.CallService(input, url);
                log.Response = output;
                log.WsResponseDate = DateTime.Now;
                log.ResponseStatusCode = "100";
                log.ResponseStatusDesc = "OK";
            }
            catch (Exception ex)
            {
                log.WsName = currentMethod.DeclaringType.FullName;
                log.WsMethod = currentMethod.Name;
                log.Request = input;
                //log.Request = MUSTERI.ToXML(true);
                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                log.WsRequestDate = DateTime.Now;
                log.ResponseStatusCode = "999";
                log.ResponseStatusDesc =  ex.ToString();
                log.Type = ((int)LogType.FAILED).ToString();
                IntegrationLogHelper.AddToLog(log);
                throw ex;
            }
            finally
            {
                log.WsName = currentMethod.DeclaringType.FullName;
                log.WsMethod = currentMethod.Name;
                log.Request = input;
                //log.Request = MUSTERI.ToXML(true);
                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                log.WsRequestDate = DateTime.Now;
                log.ResponseStatusCode = "100";
                log.ResponseStatusDesc = "OK";
                log.Type = ((int)LogType.COMPLETED).ToString();
                IntegrationLogHelper.AddToLog(log);
            }

            return result;
        }

        #endregion

    }
}
