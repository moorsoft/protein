﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Protein
{
    public class ProxySrvModels
    {
        // NOTE: Eski projedeki objelerdir. Mevcut kullanım için değiştirilmeyecektir.

        [Serializable]
        public class Sonuc
        {
            public string SonucAciklama { get; set; }
            public string SonucKodu { get; set; }
        }

        [Serializable]
        public class CreateClaimResult
        {
            public int ClaimId { get; set; }
            public int ResultCode { get; set; }
            public string ResultDesc { get; set; }
        }

        [Serializable]
        public class CreateClaimParam
        {
            public string ClaimId { get; set; }
            public List<ClaimDetailInputType> ClaimDetails { get; set; }
            public string EventDate { get; set; }
            public string PolicyNumber { get; set; }
            public string CustomerId { get; set; }
            public string HealthCenterId { get; set; }
            public string PlanId { get; set; }
            public string StatusReasonCode { get; set; }
            public string Iban { get; set; }
            public string AccountOwner { get; set; }
            public string PayDueDate { get; set; }
            public string Icd10Code { get; set; }
            public string Complaints { get; set; }
            public string DoctorDesc { get; set; }
            public string DoctorName { get; set; }
            public string DoctorSpecialityId { get; set; }
            public string Explain { get; set; }
            public string ConfidentialInfo { get; set; }
            public string SipClaimId { get; set; }
            public string ClaimStatus { get; set; }
        }

        [Serializable]
        public class ClaimDetailInputType
        {
            public string BenefitCode { get; set; }
            public string InvoiceAmount { get; set; }
            public string PayableAmount { get; set; }
            public string Coinsurance { get; set; }
            public string StartDate { get; set; }
            public string FinishDate { get; set; }
            public string SgkDesc { get; set; }
            public string InvoiceNumber { get; set; }
            public string InvoiceDate { get; set; }
            public string InvoiceExclusion { get; set; }
            public string SerialNumber { get; set; }
        }

        [Serializable]
        public class SaglikProvizyonKaydetResult : Sonuc
        {
            public string ProvizyonId { get; set; }
        }

        [Serializable]
        public class SaglikProvizyonKaydetParam
        {
            public string SirketKod { get; set; }
            public string CikisTarihi { get; set; }
            public string CozumOrtagiId { get; set; }
            public string DoktorAd { get; set; }
            public string DoktorDiplomaNo { get; set; }
            public string DoktorSaglikUnvan { get; set; }
            public string DoktorSoyad { get; set; }
            public string DoktorTCNo { get; set; }
            public string EntegrasyonProvizyonId { get; set; }
            public string GrupPoliceNo { get; set; }
            public string MusteriId { get; set; }
            public string OdemeYeri { get; set; }
            public string OlayTarihi { get; set; }
            public string OlayTarihiSaat { get; set; }
            public string PoliceNo { get; set; }
            public string ProvizyonDurumu { get; set; }
            public string ProvizyonId { get; set; }
            public string ProvizyonNedeni { get; set; }
            public string ProvizyonNotu { get; set; }
            public string RetNedeni { get; set; }
            public string SgkAnlasmaliMi { get; set; }
            public string Sikayet { get; set; }
            public string SikayetBaslangicTarihi { get; set; }
            public string TeminatGrubu { get; set; }
            public string YatisTarihi { get; set; }
            public string YenilemeNo { get; set; }

            public List<Islem> IslemListesi { get; set; }
            public List<Tani> TaniListesi { get; set; }
            public List<FaturaBilgisi> FaturaBilgiList { get; set; }
        }

        [Serializable]
        public class Islem
        {
            public string BrutTutar { get; set; }
            public string FaturaNo { get; set; }
            public string FaturaTutari { get; set; }
            public string IslemKodu { get; set; }
            public string KapsamDisiTutar { get; set; }
            public string KatilimTutari { get; set; }
            public string MuafiyetTutari { get; set; }
            public string OdenecekTutar { get; set; }
            public string SgkTutari { get; set; }
            public string TeminatKodu { get; set; }
        }

        [Serializable]
        public class Tani
        {
            public string KesinTaniMi { get; set; }
            public string TaniKodu { get; set; }
        }

        [Serializable]
        public class FaturaBilgisi
        {
            public string FaturaNo { get; set; }
            public string FaturaTarihi { get; set; }
            public string FaturaTutar { get; set; }
            public string StopajTutar { get; set; }
            public string MukellefTuru { get; set; }
        }

    }
}
