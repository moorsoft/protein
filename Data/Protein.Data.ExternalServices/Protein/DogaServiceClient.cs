﻿using Protein.Business.Abstract.Service;
using Protein.Business.Concrete.Responses;
using Protein.Business.Concrete.Services;
using Protein.Common.Constants;
using Protein.Common.Dto.ImportObjects;
using Protein.Common.Extensions;
using Protein.Common.Messaging;
using Protein.Data.ExternalServices.DogaClaimServiceProd;
using Protein.Data.ExternalServices.Log;
using Protein.Data.ExternalServices.Protein.Util.Lib;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Xml;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Data.ExternalServices.Protein
{
    public class DogaServiceClient
    {
        ClaimService claimServiceClient = new ClaimService();
        private long DogaCompanyId = 5;

        //Branch Code
        //606 -> OSS
        //303 -> SEY
        //610 -> TSS

        #region Create Log
        private void CreateFailedLog(IntegrationLog log, string responseXml, string desc)
        {
            log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
            log.Type = ((int)LogType.FAILED).ToString();
            log.Response = responseXml;
            log.ResponseStatusCode = "0";
            log.ResponseStatusDesc = desc;
            log.WsResponseDate = DateTime.Now;
            IntegrationLogHelper.AddToLog(log);
        }
        private void CreateOKLog(IntegrationLog log, string responseXml)
        {
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.Type = ((int)LogType.COMPLETED).ToString();
            log.Response = responseXml;
            log.ResponseStatusCode = "200";
            log.ResponseStatusDesc = "OK";
            log.WsResponseDate = DateTime.Now;
            IntegrationLogHelper.AddToLog(log);
        }
        private void CreateIncomingLog(IntegrationLog log)
        {
            log.HttpStatus = ((int)HttpStatusCode.Continue).ToString();
            log.Type = ((int)LogType.INCOMING).ToString();
            log.ResponseStatusCode = "100";
            log.ResponseStatusDesc = "INCOMING";
            IntegrationLogHelper.AddToLog(log);
        }
        private void CreateWaitingLog(IntegrationLog log)
        {
            log.HttpStatus = ((int)HttpStatusCode.Continue).ToString();
            log.Type = ((int)LogType.WAITING).ToString();
            log.ResponseStatusCode = "100";
            log.ResponseStatusDesc = "WAITING";
            IntegrationLogHelper.AddToLog(log);
        }
        #endregion

        #region PoliceAktar
        public DogaPoliceAktarRes PoliceAktar(string apiKod, DogaPoliceAktarReq req)
        {
            DogaPoliceAktarRes response = new DogaPoliceAktarRes();
            var currentMethod = MethodBase.GetCurrentMethod();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
                COMPANY_ID = DogaCompanyId
            };
            #endregion

            try
            {
                #region Auth
                if (ApiCodeIsValid(apiKod) <= 0)
                {
                    response.SonucAciklama = "HATA";
                    response.SonucKod = "Giriş Başarısız! APİCODE'u Kontrol Ediniz";

                    CreateFailedLog(log, response.ToXML(true), response.SonucAciklama);
                    return response;
                }
                #endregion

                #region Validation
                response = PoliceAktarValidation(req);
                if (response.SonucKod != "1")
                {
                    CreateFailedLog(log, response.ToXML(true), response.SonucAciklama);
                    return response;
                }
                #endregion

                #region Operation
                //AcenteBilgiServisleriSoapClient dogaClient = new AcenteBilgiServisleriSoapClient();
                //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                ////GeriyePoliceTransferCevap policyResult = dogaClient.TekPolice("ws_IMECE", "8Qdzp9vpMHkmfNwH", req.AcenteNo, req.BransKod, req.PoliceNo, req.YenilemeNo.ToString(), req.ZeylNo.ToString());

                //GeriyePoliceTransferCevap policyResult = dogaClient.TekPolice("ws_IMECE", "8Qdzp9vpMHkmfNwH", "302217", "303", "37945681", "", "");
                //string link = "https://portal.dogasigorta.com/WebServisleri/AcenteBilgiServisleri.asmx/TekPolice?kullaniciAdi=ws_IMECE&parola=8Qdzp9vpMHkmfNwH&acenteNo=302217&brans=600&policeNo=44633053&tecditNo=&zeyilNo=";

                if (req.YenilemeNo == "0")
                    req.YenilemeNo = "";
                else if (req.YenilemeNo.IsInt64())
                {
                    req.YenilemeNo = req.YenilemeNo.Trim().TrimStart('0');
                    req.YenilemeNo = (req.YenilemeNo.Length == 1 ? "00" : "") + req.YenilemeNo;
                    req.YenilemeNo = (req.YenilemeNo.Length == 2 ? "0" : "") + req.YenilemeNo;
                }

                if (req.ZeylNo == "0")
                    req.ZeylNo = "";
                else if (req.ZeylNo.IsInt64())
                {
                    req.ZeylNo = req.ZeylNo.Trim().TrimStart('0');
                    req.ZeylNo = (req.ZeylNo.Length == 1 ? "00" : "") + req.ZeylNo;
                    req.ZeylNo = (req.ZeylNo.Length == 2 ? "0" : "") + req.ZeylNo;
                }

                string link = "https://portal.dogasigorta.com/WebServisleri/AcenteBilgiServisleri.asmx/TekPolice?kullaniciAdi=ws_IMECE&parola=8Qdzp9vpMHkmfNwH&acenteNo=" + req.AcenteNo + "&brans=" + req.BransKod + "&policeNo=" + req.PoliceNo + "&tecditNo=" + req.YenilemeNo + "&zeyilNo=" + req.ZeylNo;

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(link);

                log.Response = $"<![CDATA[{xmlDoc.InnerXml.ToString().Replace("&amp;", " ")}]]>";
                CreateIncomingLog(log);

                log.Request = log.Response;

                response = PoliceAktarOpr(xmlDoc, req,log);
                if (response.SonucKod != "1")
                {
                    CreateFailedLog(log, response.ToXML(true), response.SonucAciklama);
                    return response;
                }
                #endregion

                #region OK
                response.SonucAciklama = "OK";
                response.SonucKod = "1";
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.SonucKod = "0";
                response.SonucAciklama = $"Bir Hata Oluştu - {ex.Message}";

                CreateFailedLog(log, response.ToXML(true), response.SonucAciklama);
                #endregion
            }
            return response;
        }

        private DogaPoliceAktarRes PoliceAktarValidation(DogaPoliceAktarReq req)
        {
            DogaPoliceAktarRes result = new DogaPoliceAktarRes();

            #region Zorunlu Alan
            if (!req.PoliceNo.IsNumeric())
            {
                result.SonucAciklama = "POLİÇE NO GİRİLMESİ GEREKMEKTEDİR";
                result.SonucKod = "0";
                return result;
            }

            if (!req.AcenteNo.IsNumeric())
            {
                result.SonucAciklama = "ACENTE NO GİRİLMESİ GEREKMEKTEDİR";
                result.SonucKod = "0";
                return result;
            }

            if (!req.BransKod.IsNumeric())
            {
                result.SonucAciklama = "BRANŞ KOD GİRİLMESİ GEREKMEKTEDİR";
                result.SonucKod = "0";
                return result;
            }

            #endregion

            #region Data Control

            Agency agency = new GenericRepository<Agency>().FindBy($"NO={req.AcenteNo} AND COMPANY_ID={DogaCompanyId}").FirstOrDefault();
            if (agency == null)
            {
                agency = new Agency
                {
                    No = req.AcenteNo,
                    Name = req.AcenteNo,
                    CompanyId = DogaCompanyId
                };
                var spRes = new GenericRepository<Agency>().Insert(agency);
                if (spRes.Code != "100")
                {
                    result.SonucAciklama = "ACENTE BİLGİSİ BULUNAMADI.";
                    result.SonucKod = "0";
                    return result;
                }
            }

            Policy policy = new GenericRepository<Policy>().FindBy($"COMPANY_ID={DogaCompanyId} AND NO={req.PoliceNo} AND RENEWAL_NO={(!req.YenilemeNo.IsInt() ? "0" : req.YenilemeNo)}", orderby: "").FirstOrDefault();
            if (!req.ZeylNo.IsInt() || int.Parse(req.ZeylNo) == 0)
            {
                if (policy != null)
                {
                    result.SonucAciklama = "POLİÇE BİLGİSİ DAHA ÖNCE AKTARILMIŞ. TEKRAR BAŞLANGIÇ ZEYL'İ AKTARILAMAZ.";
                    result.SonucKod = "0";
                    return result;
                }
            }
            else
            {
                if (policy == null)
                {
                    result.SonucAciklama = "POLİÇE BİLGİSİ BULUNAMADI.";
                    result.SonucKod = "0";
                    return result;
                }

                var endorsementNo = new GenericRepository<Endorsement>().FindBy($"POLICY_ID={policy.Id} AND NO={req.ZeylNo}", orderby: "ID DESC").FirstOrDefault();

                if (endorsementNo != null)
                {
                    result.SonucAciklama = "ZEYL NO HATALI! LÜTFEN KONTROL EDİNİZ.";
                    result.SonucKod = "0";
                    return result;
                }
            }

            #endregion

            result.SonucAciklama = "OK";
            result.SonucKod = "1";
            return result;
        }

        private DogaPoliceAktarRes PoliceAktarOpr(XmlDocument policyResult, DogaPoliceAktarReq req, IntegrationLog log)
        {
            DogaPoliceAktarRes result = new DogaPoliceAktarRes();

            PolicyDto policyDto = new PolicyDto();
            policyDto.policyIntegrationType = PolicyIntegrationType.WS;
            IService<PolicyDto> policyService = new PolicyService();

            DogaTekPolReq tekPolReq = TekPolXmlRead(policyResult.InnerXml);
            if (!tekPolReq.ErrorMessage.IsNull())
            {
                result.SonucAciklama = tekPolReq.ErrorMessage;
                result.SonucKod = "0";
                return result;
            }

            #region Validation
            result = TekPolValidation(tekPolReq);
            if (result.SonucKod != "1")
            {
                return result;
            }
            #endregion

            policyDto.policyIntegrationType = PolicyIntegrationType.WS;
            ServiceResponse response = new ServiceResponse();
            decimal exitPremium = 0;
            #region Insurer
            if (tekPolReq.ZeyilTipi == ((int)EndorsementType.BASLANGIC).ToString() || tekPolReq.ZeyilTipi == ((int)EndorsementType.SE_DEGISIKLIGI).ToString())
            {
                policyDto.isInsurerChange = true;
                policyDto.InsurerTcNo = tekPolReq.MusteriTcKimlikNo.IsInt64() ? (long?)long.Parse(tekPolReq.MusteriTcKimlikNo) : null;
                policyDto.InsurerVkn = tekPolReq.MusteriVergiNo.IsInt64() ? (long?)long.Parse(tekPolReq.MusteriVergiNo) : null;
                policyDto.InsurerTitle = tekPolReq.MusteriAdi + " " + tekPolReq.MusteriSoyadi;
                policyDto.InsurerType = (tekPolReq.MusteriGercekTuzel == "G" || tekPolReq.MusteriGercekTuzel == "Gerçek" || tekPolReq.MusteriGercekTuzel.IsNull()) ? ((int)ContactType.GERCEK).ToString() : ((int)ContactType.TUZEL).ToString();

                policyDto.InsurerName = tekPolReq.MusteriAdi;
                policyDto.InsurerCorporateName = policyDto.InsurerTitle;
                policyDto.InsurerLastName = tekPolReq.MusteriSoyadi;
                policyDto.InsurerGender = tekPolReq.MusteriCinsiyet;
                policyDto.InsurerBirthDate = tekPolReq.MusteriDogumTarihi.IsDateTime() ? (DateTime?)DateTime.Parse(tekPolReq.MusteriDogumTarihi) : null;
                policyDto.InsurerBirthPlace = tekPolReq.MusteriDogumYeri;
                policyDto.InsurerNameOfFather = tekPolReq.MusteriBabaAdi;

                policyDto.InsurerAddress = tekPolReq.MusteriEvCadde + " " + tekPolReq.MusteriEvMahalle + " " + tekPolReq.MusteriEvSokak + " " + tekPolReq.MusteriEvKapiNo + "/" + tekPolReq.MusteriEvDaire + " " + tekPolReq.MusteriEvSemt;
                policyDto.InsurerCityId = GetCityAndCountybyCode(cityCode: tekPolReq.MusteriEvIlKodu);
                policyDto.InsurerCountyId = GetCityAndCountybyCode(countyCode: tekPolReq.MusteriEvIlceKodu);

                policyDto.InsurerTelNo = tekPolReq.MusteriEvTelefonNo;
                policyDto.InsurerGsmNo = tekPolReq.MusteriCepTelefonNo;
                policyDto.InsurerEmail = tekPolReq.MusteriEPosta;

                response = policyService.ServiceInsert(policyDto);
                if (!response.IsSuccess)
                {
                    result.SonucAciklama = "SİGORTA ETTİREN KAYDEDİLEMEDİ";
                    result.SonucKod = "0";
                    return result;
                }
                policyDto.InsurerId = response.Id;
            }
            else
            {
                Policy policy = new GenericRepository<Policy>().FindById((Int64)tekPolReq.PoliceId);
                if (policy == null)
                {
                    result.SonucAciklama = "POLİÇE BİLGİSİ BULUNAMADI";
                    result.SonucKod = "0";
                    return result;
                }
                policyDto.PolicyId = policy.Id;

                policyDto.InsurerId = policy.Insurer;
                exitPremium = (tekPolReq.NetPrim.IsNumeric() ? decimal.Parse(tekPolReq.NetPrim.Replace(".", ",")) : 0) * tekPolReq.PrimType;
                tekPolReq.NetPrim = ((decimal)policy.Premium + exitPremium).ToString("#0.00");
            }
            #endregion

            decimal ForexBuying = 1;

            policyDto.isInsurerChange = false;
            if (tekPolReq.PrimType != 0)
            {
                #region Policy

                policyDto.isPolicyChange = true;

                policyDto.PolicyId = tekPolReq.PoliceId.ToString().IsNull() ? 0 : (long)tekPolReq.PoliceId;
                policyDto.CompanyId = DogaCompanyId;
                policyDto.AgencyId = tekPolReq.AcenteNo;
                policyDto.EndDate = tekPolReq.PoliceBitisTarihi.IsDateTime() ? (DateTime?)DateTime.Parse(tekPolReq.PoliceBitisTarihi) : null;
                policyDto.PolicyType = tekPolReq.GrupFerdi;

                policyDto.RenewalNo = tekPolReq.YenilemeNo.IsInt() ? int.Parse(tekPolReq.YenilemeNo) : 0;

                if (policyDto.RenewalNo > 0 && !tekPolReq.OncekiPoliceNo.IsNull())
                {
                    policyDto.PreviousPolicyNo = tekPolReq.OncekiPoliceNo;
                    policyDto.PreviousRenewalNo = policyDto.RenewalNo - 1;
                }

                policyDto.SbmPoliceNo = tekPolReq.SagmerPoliceNo;
                policyDto.PolicyNo = tekPolReq.PoliceNo;
                policyDto.PolicyInstallmentCount = tekPolReq.TaksitDurumu.IsInt() ? (int?)int.Parse(tekPolReq.TaksitDurumu) : 1;

                policyDto.SubProductId = GetPackagebyNo(tekPolReq.PaketNo).SUBPRODUCT_ID;

                policyDto.PolicyPaymentType = tekPolReq.PolicyPaymentType;

                if (tekPolReq.DovizTipi != "99" && tekPolReq.DovizKuru.IsNumeric())
                {
                    switch (tekPolReq.DovizTipi)
                    {
                        case "1":
                            tekPolReq.DovizTipi = ((int)CurrencyType.ABD_DOLARI).ToString();
                            break;
                        case "3":
                            tekPolReq.DovizTipi = ((int)CurrencyType.AVUSTRALYA_DOLARI).ToString();
                            break;
                        case "11":
                            tekPolReq.DovizTipi = ((int)CurrencyType.ISVEC_KRONU).ToString();
                            break;
                        case "12":
                            tekPolReq.DovizTipi = ((int)CurrencyType.ISVICRE_FRANGI).ToString();
                            break;
                        case "14":
                            tekPolReq.DovizTipi = ((int)CurrencyType.JAPON_YENI).ToString();
                            break;
                        case "15":
                            tekPolReq.DovizTipi = ((int)CurrencyType.KANADA_DOLARI).ToString();
                            break;
                        case "16":
                            tekPolReq.DovizTipi = ((int)CurrencyType.KUVEYT_DINARI).ToString();
                            break;
                        case "17":
                            tekPolReq.DovizTipi = ((int)CurrencyType.NORVEC_KRONU).ToString();
                            break;
                        case "18":
                            tekPolReq.DovizTipi = ((int)CurrencyType.INGLIZI_STERLINI).ToString();
                            break;
                        case "19":
                            tekPolReq.DovizTipi = ((int)CurrencyType.SUUDI_ARABISTAN_RIYALI).ToString();
                            break;
                        case "20":
                            tekPolReq.DovizTipi = ((int)CurrencyType.EURO).ToString();
                            break;
                        case "21":
                            tekPolReq.DovizTipi = ((int)CurrencyType.CIN_YUANI).ToString();
                            break;
                        default:
                            break;
                    }
                    ExchangeRate exchangeRate = new ExchangeRate
                    {
                        CurrencyType = tekPolReq.DovizTipi,
                        ForexBuying = Decimal.Parse(tekPolReq.DovizKuru.Replace(".", ",")),
                        RateDate = DateTime.Now
                    };
                    var spResponseExchange = new GenericRepository<ExchangeRate>().Insert(exchangeRate);
                    if (spResponseExchange.Code != "100")
                    {
                        result.SonucAciklama = "DÖVİZ BİLGİSİ KAYDEDİLEMEDİ";
                        result.SonucKod = "0";
                        return result;
                    }
                    policyDto.ExchangeRateId = spResponseExchange.PkId;
                    ForexBuying = Decimal.Parse(tekPolReq.DovizKuru.Replace(".", ","));
                }
                policyDto.PolicyPremium = tekPolReq.NetPrim.IsNumeric() ? ((Decimal?)Decimal.Parse(tekPolReq.NetPrim.Replace(".", ",")) * ForexBuying) : null;

                policyDto.PolicyStatus = tekPolReq.PolicyStatus;// ((int)PolicyStatus.TANZIMLI).ToString();

                response = policyService.ServiceInsert(policyDto);
                if (!response.IsSuccess)
                {
                    result.SonucAciklama = "POLİÇE KAYDEDİLEMEDİ";
                    result.SonucKod = "0";
                    return result;
                }
                policyDto.PolicyId = response.Id;
                #endregion
            }

            log.PolicyId = policyDto.PolicyId;
            #region Endorsement
            policyDto.isEndorsementChange = true;
            policyDto.isPolicyChange = false;

            policyDto.EndorsementType = tekPolReq.ZeyilTipi;
            policyDto.EndorsementTypeDescription = tekPolReq.ZeyilAdi;
            policyDto.EndorsementNo = tekPolReq.ZeyilNo.IsInt() ? int.Parse(tekPolReq.ZeyilNo) : 0;
            policyDto.EndorsementStartDate = tekPolReq.PoliceBaslangicTarihi.IsDateTime() ? (DateTime?)DateTime.Parse(tekPolReq.PoliceBaslangicTarihi) : null;
            policyDto.DateOfIssue = tekPolReq.PoliceTanzimTarihi.IsDateTime() ? (DateTime?)DateTime.Parse(tekPolReq.PoliceTanzimTarihi) : null;
            policyDto.EndorsementRegistrationDate = tekPolReq.PoliceTanzimTarihi.IsDateTime() ? (DateTime?)DateTime.Parse(tekPolReq.PoliceTanzimTarihi) : null;

            response = policyService.ServiceInsert(policyDto);
            if (!response.IsSuccess)
            {
                result.SonucAciklama = "ZEYL KAYDEDİLEMEDİ";
                result.SonucKod = "0";
                return result;
            }
            policyDto.EndorsementId = response.Id;
            #endregion

            #region Insured
            decimal insuredTotalPremium = 0;

            if (policyDto.EndorsementType == ((int)EndorsementType.BASLANGIC).ToString() || policyDto.EndorsementType == ((int)EndorsementType.SIGORTALI_GIRISI).ToString() || policyDto.EndorsementType == ((int)EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI).ToString())
            {
                policyDto.isEndorsementChange = false;
                policyDto.isInsuredContactChange = true;

                if (tekPolReq.ekSigortaliList.Count > 0)
                {
                    foreach (var sigortali in tekPolReq.ekSigortaliList)
                    {
                        policyDto.isEndorsementChange = false;
                        policyDto.isInsuredContactChange = true;
                        policyDto.isInsuredChange = false;
                        policyDto.isInsuredNoteChange = false;

                        policyDto.InsuredAddressId = 0;
                        policyDto.InsuredContactId = 0;
                        policyDto.InsuredPersonId = 0;
                        policyDto.InsuredPhoneId = 0;
                        policyDto.InsuredMobilePhoneId = 0;
                        policyDto.InsuredEmailId = 0;
                        policyDto.InsuredId = 0;
                        if (policyDto.EndorsementType == ((int)EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI).ToString())
                        {
                            var whereCon = $"POLICY_ID ={policyDto.PolicyId} ";
                            if (sigortali.TcKimlikNo.IsInt64() && ((long?)long.Parse(sigortali.TcKimlikNo)).IsIdentityNoLength() == null && !sigortali.PasaportNo.IsNull())
                            {
                                whereCon += $" AND (IDENTITY_NO={sigortali.TcKimlikNo} OR PASSPORT_NO={sigortali.PasaportNo})";
                            }
                            else if (sigortali.TcKimlikNo.IsInt64() && ((long?)long.Parse(sigortali.TcKimlikNo)).IsIdentityNoLength() == null)
                            {
                                whereCon += $"AND IDENTITY_NO = {sigortali.TcKimlikNo}";
                            }
                            else if (!sigortali.PasaportNo.IsNull())
                            {
                                whereCon += $"AND PASSPORT_NO = {sigortali.PasaportNo}";
                            }

                            var v_insured = new GenericRepository<V_Insured>().FindBy(whereCon, orderby: "").FirstOrDefault();
                            if (v_insured != null)
                            {
                                policyDto.InsuredId = v_insured.INSURED_ID;
                            }
                        }
                        policyDto.InsuredNoParameterId = 0;

                        #region Person
                        policyDto.InsuredTcNo = sigortali.TcKimlikNo.IsInt64() ? (long?)long.Parse(sigortali.TcKimlikNo) : null;
                        policyDto.InsuredName = sigortali.Adi;
                        policyDto.InsuredGender = sigortali.Cinsiyet;
                        policyDto.InsuredLastName = sigortali.Soyadi;
                        policyDto.InsuredBirthDate = sigortali.DogumTarihi.IsDateTime() ? (DateTime?)DateTime.Parse(sigortali.DogumTarihi) : null;
                        policyDto.InsuredNameOfFather = sigortali.BabaAdi;
                        policyDto.InsuredNationality = (sigortali.Uyruk == "1" || sigortali.Uyruk.IsNull()) ? "TR" : sigortali.Uyruk;
                        if (policyDto.InsuredNationality != "TR")
                        {
                            var a = new GenericRepository<Country>().FindBy($"NUM_CODE ='{policyDto.InsuredNationality}'").FirstOrDefault();
                            if (a != null)
                            {
                                policyDto.InsuredNationality = a.Id.ToString();
                            }
                            else
                            {
                                policyDto.InsuredNationality = null;
                            }
                        }
                        policyDto.InsuredPassport = sigortali.PasaportNo;

                        policyDto.InsuredMaritalStatus = sigortali.MedeniHali.ToUpper() == "EVLİ" ? ((int)MaritalStatus.EVLI).ToString() : null;
                        policyDto.InsuredVip = sigortali.Vip.IsNull() ? null : (sigortali.Vip.ToUpper() == "EVET" ? "1" : null);
                        if (tekPolReq._VipKodu != null && tekPolReq._VipKodu.ToUpper() == "EVET")
                        {
                            policyDto.InsuredVip = "1";
                        }
                        #endregion

                        #region Address
                        policyDto.InsuredCityId = GetCityAndCountybyCode(cityCode: sigortali.EvIlKodu);
                        policyDto.InsuredCountyId = GetCityAndCountybyCode(countyCode: sigortali.EvIlceKodu);

                        policyDto.InsuredAddress = sigortali.EvAdresi;

                        #endregion

                        #region Phone
                        policyDto.InsuredTelNo = sigortali.TelefonNo;
                        policyDto.InsuredGsmNo = sigortali.EkSigortaliCepNo;
                        #endregion

                        #region E-mail
                        policyDto.InsuredEmail = sigortali.EPosta;
                        #endregion

                        #region Bank
                        if (!sigortali.SigortaliHesapBankaKodu.IsNull())
                        {
                            var bank = new GenericRepository<Bank>().FindBy($"CODE={sigortali.SigortaliHesapBankaKodu}").SingleOrDefault();
                            if (bank != null)
                            {
                                policyDto.InsuredBankAccountOwnerName = sigortali.Adi + " " + sigortali.Soyadi;
                                policyDto.InsuredBankIbanNo = sigortali.SigortaliIbanNo;

                                policyDto.InsuredBankId = bank.Id;
                            }
                        }
                        #endregion

                        response = policyService.ServiceInsert(policyDto);
                        if (!response.IsSuccess)
                        {
                            result.SonucAciklama = "SİGORTALI (KİŞİ BİLGİLERİ) KAYDEDİLEMEDİ : " + response.Message;
                            result.SonucKod = "0";
                            return result;
                        }
                        policyDto.InsuredContactId = response.Id;

                        #region Insured
                        policyDto.isInsuredContactChange = false;
                        policyDto.isInsuredChange = true;

                        policyDto.InsuredFirstInsuredDate = sigortali.IlkSigortalilikTarihi.IsDateTime() ? (DateTime?)DateTime.Parse(sigortali.IlkSigortalilikTarihi) : null;
                        policyDto.InsuredFirstAmbulantCoverageDate = sigortali.IlkAyaktateminatBaslangicTarihi.IsDateTime() ? (DateTime?)DateTime.Parse(sigortali.IlkAyaktateminatBaslangicTarihi) : null;
                        policyDto.InsuredIndividualType = sigortali.BireyTip;
                        policyDto.InsuredFamilyNo = 1;

                        policyDto.InsuredPackageId = GetPackagebyNo(sigortali.ImeceKisiPaket).PACKAGE_ID;
                        insuredTotalPremium += (Decimal.Parse(sigortali.Prim.Replace(".", ",")) * ForexBuying);
                        policyDto.InsuredInitialPremium = sigortali.Prim.IsNumeric() ? (Decimal?)Decimal.Parse(sigortali.Prim.Replace(".", ",")) * ForexBuying : null;
                        policyDto.InsuredEndorsementPremium = sigortali.Prim.IsNumeric() ? (Decimal?)Decimal.Parse(sigortali.Prim.Replace(".", ",")) * ForexBuying : null;
                        policyDto.InsuredCompanyEntranceDate = sigortali.IlkKayitTarihi.IsDateTime() ? (DateTime?)DateTime.Parse(sigortali.IlkKayitTarihi) : null;
                        policyDto.InsuredBirthCoverageDate = sigortali.AnnelikTeminatiBaslangicTarihi.IsDateTime() ? (DateTime?)DateTime.Parse(sigortali.AnnelikTeminatiBaslangicTarihi) : null;
                        policyDto.InsuredRenewalGuaranteeType = sigortali.YenilemeGarantisiTipi.IsNull() ? null : ((int)RenewalGuaranteeType.YENILEME_GARANTISI_SARTLI).ToString();
                        policyDto.InsuredRenewalGuaranteeText = sigortali.YenilemeGarantisiTipi;

                        policyDto.InsuredrenewalDate = sigortali.YenilemeGarantisiTarihi.IsDateTime() ? (DateTime?)DateTime.Parse(sigortali.YenilemeGarantisiTarihi) : null;

                        #endregion

                        response = policyService.ServiceInsert(policyDto);
                        if (!response.IsSuccess)
                        {
                            result.SonucAciklama = "SİGORTALI KAYDEDİLEMEDİ : " + response.Message;
                            result.SonucKod = "0";
                            return result;
                        }
                        policyDto.InsuredId = response.Id;

                        if (!sigortali.OzelNotKisi.IsNull())
                        {
                            #region Note

                            policyDto.isInsuredNoteChange = true;
                            policyDto.isInsuredChange = false;

                            policyDto.InsuredNoteDetails = sigortali.OzelNotKisi;
                            policyDto.InsuredNoteType = ((int)NoteType.OZEL).ToString();

                            #endregion

                            response = policyService.ServiceInsert(policyDto);
                            if (!response.IsSuccess)
                            {
                                result.SonucAciklama = "SİGORTALI NOT(OzelNotKisi) KAYDEDİLEMEDİ";
                                result.SonucKod = "0";
                                return result;
                            }
                        }
                        if (tekPolReq.BeyanListesi.Count() > 0)
                        {
                            #region NoteBeyan

                            foreach (var noteBeyan in tekPolReq.BeyanListesi)
                            {
                                policyDto.isInsuredNoteChange = true;
                                policyDto.isInsuredChange = false;

                                policyDto.InsuredNoteDetails = noteBeyan.BeyanDesc;
                                policyDto.InsuredNoteType = ((int)InsuredNoteType.BEYAN).ToString();

                                #endregion

                                response = policyService.ServiceInsert(policyDto);
                                if (!response.IsSuccess)
                                {
                                    result.SonucAciklama = "BEYAN NOTU KAYDEDİLEMEDİ";
                                    result.SonucKod = "0";
                                    return result;
                                }
                            }


                        }
                        if (!tekPolReq.YenilemeGorusu.IsNull())
                        {
                            #region Note

                            policyDto.isInsuredNoteChange = true;
                            policyDto.isInsuredChange = false;

                            policyDto.InsuredNoteDetails = tekPolReq.YenilemeGorusu;
                            policyDto.InsuredNoteType = ((int)NoteType.OZEL).ToString();

                            #endregion

                            response = policyService.ServiceInsert(policyDto);
                            if (!response.IsSuccess)
                            {
                                result.SonucAciklama = "SİGORTALI NOT(YenilemeGorusu) KAYDEDİLEMEDİ";
                                result.SonucKod = "0";
                                return result;
                            }
                        }

                        if (!sigortali.GecisTipi.IsNull())
                        {
                            #region Insured Transfer
                            policyDto.isInsuredNoteChange = true;
                            policyDto.isInsuredChange = false;

                            switch (sigortali.GecisTipi.ToUpper())
                            {
                                case "BİREYSELDEN KURUMSALA":
                                    policyDto.InsuredTransferType = ((int)InsuredTransferType.FERDIDEN_GRUBA).ToString();
                                    break;
                                case "KURUMSALDAN BİREYSELE":
                                    policyDto.InsuredTransferType = ((int)InsuredTransferType.GRUPTAN_FERDIYE).ToString();
                                    break;
                                default:
                                    if (!sigortali.OncekiSigortaSirketi.IsNull())
                                    {
                                        policyDto.InsuredTransferType = ((int)InsuredTransferType.SIGORTA_SIRKETI).ToString();
                                        policyDto.InsuredTransferCompanyName = sigortali.OncekiSigortaSirketi;
                                    }
                                    break;
                            }

                            #endregion

                            response = policyService.ServiceInsert(policyDto);
                            if (!response.IsSuccess)
                            {
                                result.SonucAciklama = "SİGORTALI GECİŞİ(GecisTipi) KAYDEDİLEMEDİ";
                                result.SonucKod = "0";
                                return result;
                            }
                        }

                        if (!sigortali.SigortaliUwGorusu.IsNull())
                        {
                            #region Insured Exclusion

                            Exclusion newExclusion = new Exclusion
                            {
                                ContactId = policyDto.InsuredContactId,
                                Description = "SigortaliUwGorusu -> " + sigortali.SigortaliUwGorusu,
                                Status = ((int)Status.PASIF).ToString()
                            };
                            SpResponse spResponse = new GenericRepository<Exclusion>().Insert(newExclusion);
                            if (spResponse.Code != "100")
                            {
                                result.SonucAciklama = "SigortaliUwGorusu KAYDEDİLEMEDİ.";
                                result.SonucKod = "0";
                                return result;
                            }

                            InsuredExclusion insuredExclusion = new InsuredExclusion
                            {
                                ExclusionId = spResponse.PkId,
                                InsuredId = policyDto.InsuredId,
                                StartDate = policyDto.DateOfIssue.ToString().IsDateTime() ? (DateTime)policyDto.DateOfIssue : DateTime.Now,
                                EndDate = policyDto.EndDate
                            };
                            SpResponse spResponseExclusion = new GenericRepository<InsuredExclusion>().Insert(insuredExclusion);
                            if (spResponseExclusion.Code != "100")
                            {
                                result.SonucAciklama = "SİGORTALIYA SigortaliUwGorusu EKLENEMEDİ.";
                                result.SonucKod = "0";
                                return result;
                            }

                            MailSender msender = new MailSender();
                            msender.SendMessage(
                                new EmailArgs
                                {
                                    TO = "uretim@imecedestek.com;besim.oznalcin@mooryazilim.com;serdar@imecedestek.com",
                                    IsHtml = true,
                                    Subject = $"Web Servis SigortaliUwGorusu Aktarımı Uyarısı!",
                                    Content = $@"<b>Sigorta Şirketi:</b> <i>Doğa Sigorta</b><br>
                                                 <b>Poliçe No:</b> <i>{policyDto.PolicyNo}</b><br>
                                                 <b>TC Kimlik No:</b> <i>{sigortali.TcKimlikNo}</b><br>
                                                 <b>Sigortalı Adı ve Soyadı:</b> <i>{policyDto.InsuredName + " " + policyDto.InsuredLastName}</b><br>
                                                 <b>İstisna Açıklama:</b> SigortaliUwGorusu -> <i>{sigortali.SigortaliUwGorusu}</b><br>"
                                });
                            #endregion
                        }

                        if (!sigortali.SigortaliRiskEkPrimi.IsNull())
                        {
                            #region Insured Exclusion

                            Exclusion newExclusion = new Exclusion
                            {
                                ContactId = policyDto.InsuredContactId,
                                Description = "SigortaliRiskEkPrimi -> " + sigortali.SigortaliRiskEkPrimi,
                                Status = ((int)Status.PASIF).ToString()
                            };
                            SpResponse spResponse = new GenericRepository<Exclusion>().Insert(newExclusion);
                            if (spResponse.Code != "100")
                            {
                                result.SonucAciklama = "SigortaliRiskEkPrimi KAYDEDİLEMEDİ.";
                                result.SonucKod = "0";
                                return result;
                            }

                            InsuredExclusion insuredExclusion = new InsuredExclusion
                            {
                                ExclusionId = spResponse.PkId,
                                InsuredId = policyDto.InsuredId,
                                StartDate = policyDto.DateOfIssue.ToString().IsDateTime() ? (DateTime)policyDto.DateOfIssue : DateTime.Now,
                                EndDate = policyDto.EndDate
                            };
                            SpResponse spResponseExclusion = new GenericRepository<InsuredExclusion>().Insert(insuredExclusion);
                            if (spResponseExclusion.Code != "100")
                            {
                                result.SonucAciklama = "SİGORTALIYA SigortaliRiskEkPrimi EKLENEMEDİ.";
                                result.SonucKod = "0";
                                return result;
                            }

                            MailSender msender = new MailSender();
                            msender.SendMessage(
                                new EmailArgs
                                {
                                    TO = "uretim@imecedestek.com;besim.oznalcin@mooryazilim.com;serdar@imecedestek.com",
                                    IsHtml = true,
                                    Subject = $"Web Servis SigortaliRiskEkPrimi Aktarımı Uyarısı!",
                                    Content = $@"<b>Sigorta Şirketi:</b> <i>Doğa Sigorta</b><br>
                                                 <b>Poliçe No:</b> <i>{policyDto.PolicyNo}</b><br>
                                                 <b>TC Kimlik No:</b> <i>{sigortali.TcKimlikNo}</b><br>
                                                 <b>Sigortalı Adı ve Soyadı:</b> <i>{policyDto.InsuredName + " " + policyDto.InsuredLastName}</b><br>
                                                 <b>İstisna Açıklama:</b> SigortaliRiskEkPrimi -> <i>{sigortali.SigortaliRiskEkPrimi}</b><br>"
                                });
                            #endregion
                        }



                        //if (!tekpolreq._SigortaliUwGorusu.IsNull())
                        //{
                        //    #region Insured Exclusion 

                        //    Exclusion newExclusion = new Exclusion
                        //    {
                        //        ContactId = policyDto.InsuredContactId,
                        //        Description = tekPolReq._SigortaliUwGorusu
                        //    };
                        //    SpResponse<Exclusion> spResponse = new GenericRepository<Exclusion>().Insert(newExclusion);
                        //    if (spResponse.Code != "100")
                        //    {
                        //        result.SonucAciklama = "İSTİSNA NOTU KAYDEDİLEMEDİ.";
                        //        result.SonucKod = "0";
                        //        return result;
                        //    }

                        //    InsuredExclusion insuredExclusion = new InsuredExclusion
                        //    {
                        //        ExclusionId = spResponse.PkId,
                        //        InsuredId = policyDto.InsuredId,
                        //        StartDate = policyDto.DateOfIssue.ToString().IsDateTime() ? (DateTime)policyDto.DateOfIssue : DateTime.Now,
                        //        EndDate = policyDto.EndDate
                        //    };
                        //    SpResponse<InsuredExclusion> spResponseExclusion = new GenericRepository<InsuredExclusion>().Insert(insuredExclusion);
                        //    if (spResponseExclusion.Code != "100")
                        //    {
                        //        result.SonucAciklama = "SİGORTALIYA İSTİSNA NOTU EKLENEMEDİ.";
                        //        result.SonucKod = "0";
                        //        return result;
                        //    }

                        //    MailSender msender = new MailSender();
                        //    msender.SendMessage(
                        //        new EmailArgs
                        //        {
                        //            TO = "uretim@imecedestek.com",
                        //            IsHtml = true,
                        //            Subject = $"Web Servis İstisna Aktarımı Uyarısı!",
                        //            Content = $@"<b>Sigorta Şirketi:</b> <i>Doğa Sigorta</b><br>
                        //                         <b>Poliçe No:</b> <i>{policyDto.PolicyNo}</b><br>
                        //                         <b>TC Kimlik No:</b> <i>{sigortali.TcKimlikNo}</b><br>
                        //                         <b>Sigortalı Adı ve Soyadı:</b> <i>{policyDto.InsuredName + " " + policyDto.InsuredLastName}</b><br>
                        //                         <b>İstisna Açıklama:</b> <i>{tekPolReq._SigortaliUwGorusu}</b><br>"
                        //        });
                        //    #endregion
                        //}
                    }
                }
            }
            else if (policyDto.EndorsementType == ((int)EndorsementType.SIGORTALI_CIKISI).ToString())
            {

                if (tekPolReq.ekSigortaliList.Count > 0)
                {
                    List<string> IdentityNoList = new List<string>();
                    foreach (var sigortali in tekPolReq.ekSigortaliList)
                    {
                        if (sigortali.TcKimlikNo.IsInt64())
                        {
                            IdentityNoList.Add(sigortali.TcKimlikNo);
                        }
                    }

                    if (IdentityNoList.Count > 0)
                    {
                        List<V_Insured> v_Insured = new GenericRepository<V_Insured>().FindBy($"POLICY_ID={tekPolReq.PoliceId} AND IDENTITY_NO IN ({string.Join(",", IdentityNoList.ToArray())})", orderby: "");
                    }
                    List<Int64> insuredIdList = new List<long>();
                    foreach (var sigortali in tekPolReq.ekSigortaliList)
                    {
                        V_Insured v_Insured = new GenericRepository<V_Insured>().FindBy($"POLICY_ID={tekPolReq.PoliceId} AND IDENTITY_NO = {sigortali.TcKimlikNo}", orderby: "").FirstOrDefault();
                        if (v_Insured != null)
                        {
                            Insured insured = new GenericRepository<Insured>().FindBy($"ID={v_Insured.INSURED_ID}").FirstOrDefault();
                            insured.Status = ((int)Status.SILINDI).ToString();
                            insured.Premium = tekPolReq.PrimType * decimal.Parse(sigortali.Prim.Replace(".", ","));
                            insured.TotalPremium += insured.Premium;
                            insured.LastEndorsementId = policyDto.EndorsementId;

                            SpResponse spResponseInsured = new GenericRepository<Insured>().Insert(insured);

                            insuredTotalPremium += (decimal)insured.Premium;

                            EndorsementInsured endorsementInsured = new EndorsementInsured
                            {
                                EndorsementId = policyDto.EndorsementId,
                                InsuredId = v_Insured.INSURED_ID,
                                Status = ((int)Status.AKTIF).ToString(),
                                InsuredStatus = insured.Status,
                                PackageId = insured.PackageId,
                                Premium = insured.Premium,
                                TotalPremium = insured.TotalPremium
                            };
                            SpResponse spResponse = new GenericRepository<EndorsementInsured>().Insert(endorsementInsured);
                        }
                        policyDto.InsuredId = response.Id;
                    }
                    //foreach (var item in insuredIdList)
                    //{
                    //    EndorsementInsured endorsementInsured = new EndorsementInsured
                    //    {
                    //        EndorsementId = policyDto.EndorsementId,
                    //        InsuredId = item
                    //    };
                    //    SpResponse spResponse = new GenericRepository<EndorsementInsured>().Insert(endorsementInsured);

                    //}
                }
            }
            else if (policyDto.EndorsementType == ((int)EndorsementType.MEBDEINDEN_BASLANGICTAN_IPTAL).ToString())
            {
                List<Insured> insuredList = new GenericRepository<Insured>().FindBy($"POLICY_ID={tekPolReq.PoliceId}", orderby: "");
                foreach (var item in insuredList)
                {
                    item.Premium = -1 * item.TotalPremium;
                    item.TotalPremium = 0;
                    item.Status = ((int)Status.SILINDI).ToString();
                    item.LastEndorsementId = policyDto.EndorsementId;

                    new GenericRepository<Insured>().Insert(item);

                    EndorsementInsured endorsementInsured = new EndorsementInsured
                    {
                        EndorsementId = policyDto.EndorsementId,
                        InsuredId = item.Id,
                        InsuredStatus = item.Status,
                        PackageId = item.PackageId,
                        Premium = item.Premium,
                        TotalPremium = item.TotalPremium
                    };
                    SpResponse spResponse = new GenericRepository<EndorsementInsured>().Insert(endorsementInsured);

                    insuredTotalPremium += (decimal)item.Premium;
                }
            }
            else if (policyDto.EndorsementType == ((int)EndorsementType.GUN_ESASLI_IPTAL).ToString())
            {
                List<Insured> insuredList = new GenericRepository<Insured>().FindBy($"POLICY_ID={tekPolReq.PoliceId}", orderby: "");
                foreach (var item in insuredList)
                {
                    item.Premium = (exitPremium / insuredList.Count);
                    item.TotalPremium = (item.TotalPremium + (exitPremium / insuredList.Count));
                    item.Status = ((int)Status.SILINDI).ToString();
                    item.LastEndorsementId = policyDto.EndorsementId;

                    new GenericRepository<Insured>().Insert(item);

                    EndorsementInsured endorsementInsured = new EndorsementInsured
                    {
                        EndorsementId = policyDto.EndorsementId,
                        InsuredId = item.Id,
                        InsuredStatus = item.Status,
                        PackageId = item.PackageId,
                        Premium = item.Premium,
                        TotalPremium = item.TotalPremium
                    };
                    SpResponse spResponse = new GenericRepository<EndorsementInsured>().Insert(endorsementInsured);

                    insuredTotalPremium += (decimal)item.Premium;

                }
            }
            #endregion

            var endorsement = new GenericRepository<Endorsement>().FindById(policyDto.EndorsementId);
            if (endorsement != null)
            {
                endorsement.Premium = insuredTotalPremium;
                endorsement.Status = ((int)Status.AKTIF).ToString();
                var spResEndorsement = new GenericRepository<Endorsement>().Insert(endorsement);
                if (spResEndorsement.Code != "100")
                {
                    result.SonucAciklama = spResEndorsement.Message;
                    result.SonucKod = "0";

                    //RollBackProxySrv(policyId: policyDto.PolicyId, endorsementId: policyDto.EndorsementId, insurerId: (long)policyDto.InsurerId, insuredIdList: insuredIdList);

                    return result;
                }

                if (tekPolReq.taksitList != null && tekPolReq.taksitList.Count > 0)
                {
                    List<Installment> listInstallment = new List<Installment>();
                    foreach (var item in tekPolReq.taksitList)
                    {
                        var a = item.VadeTarihi.IsDateTime() ? DateTime.Parse(item.VadeTarihi) : DateTime.Now;

                        var b = item.Planlanan.Split('.')[0].Replace(",", ".").IsNumeric() ? Decimal.Parse(item.Planlanan.Split('.')[0].Replace(",", ".")) : 0;
                        b *= tekPolReq.PrimType;

                        Installment installment = new Installment
                        {
                            DueDate = a,
                            Amount = b,
                            EndorsementId = endorsement.Id,
                            PolicyId = endorsement.PolicyId
                        };
                        listInstallment.Add(installment);
                    }
                    new GenericRepository<Installment>().InsertEntities(listInstallment);
                }
                //if (endorsement.Premium != 0)
                //{
                //    var firstEndorsement = new Endorsement();
                //    if (endorsement.Type == ((int)EndorsementType.BASLANGIC).ToString())
                //    {
                //        firstEndorsement = endorsement;
                //    }
                //    else
                //    {
                //        firstEndorsement = new GenericRepository<Endorsement>().FindBy("POLICY_ID=:policyId AND TYPE=:type", orderby: "", parameters: new { policyId = endorsement.PolicyId, type = new Dapper.DbString { Value = ((int)EndorsementType.BASLANGIC).ToString(), Length = 3 } }).FirstOrDefault();
                //    }

                //    if (firstEndorsement != null)
                //    {
                //        if (endorsement.Type == ((int)EndorsementType.BASLANGIC).ToString())
                //        {
                //            List<Installment> listInstallment = new List<Installment>();

                //            for (int i = 0; i < policyDto.PolicyInstallmentCount; i++)
                //            {
                //                Installment installment = new Installment
                //                {
                //                    DueDate = DateTime.Now.AddMonths(i),
                //                    Amount = decimal.Parse(((decimal)endorsement.Premium / (int)policyDto.PolicyInstallmentCount).ToString("#0.00")),
                //                    EndorsementId = endorsement.Id,
                //                    PolicyId = endorsement.PolicyId
                //                };
                //                listInstallment.Add(installment);
                //            }
                //            new GenericRepository<Installment>().InsertEntities(listInstallment);

                //        }
                //        else
                //        {
                //            var instalmentList = new GenericRepository<Installment>().FindBy($"POLICY_ID=:policyId AND ENDORSEMENT_ID=:endorsementId AND DUE_DATE>= TO_DATE('{ (DateTime)endorsement.DateOfIssue}','DD.MM.YYYY HH24:MI:SS')", parameters: new { policyId = firstEndorsement.PolicyId, endorsementId = firstEndorsement.Id });
                //            if (instalmentList != null && instalmentList.Count > 0)
                //            {
                //                List<Installment> listInstallment = new List<Installment>();
                //                foreach (var item in instalmentList)
                //                {
                //                    Installment installment = new Installment
                //                    {
                //                        DueDate = item.DueDate,
                //                        Amount = (decimal)endorsement.Premium / instalmentList.Count,
                //                        EndorsementId = endorsement.Id,
                //                        PolicyId = endorsement.PolicyId
                //                    };
                //                    listInstallment.Add(installment);
                //                }
                //                new GenericRepository<Installment>().InsertEntities(listInstallment);
                //            }
                //            else
                //            {
                //                Installment installment = new Installment
                //                {
                //                    DueDate = (DateTime)endorsement.DateOfIssue,
                //                    Amount = (decimal)endorsement.Premium,
                //                    EndorsementId = endorsement.Id,
                //                    PolicyId = endorsement.PolicyId
                //                };
                //                new GenericRepository<Installment>().Insert(installment);
                //            }
                //        }
                //    }
                //}
            }

            result.SonucAciklama = "OK";
            result.SonucKod = "1";
            return result;


        }

        private DogaPoliceAktarRes TekPolValidation(DogaTekPolReq req)
        {
            DogaPoliceAktarRes response = new DogaPoliceAktarRes();

            #region Zorunlu Alan
            if (!req.PoliceNo.IsInt64())
            {
                response.SonucKod = "0";
                response.SonucAciklama = "POLİÇE NO GİRİLMESİ GEREKMEKTEDİR";
                return response;
            }
            if (!req.PoliceBaslangicTarihi.IsDateTime() || !req.PoliceBitisTarihi.IsDateTime() || !req.PoliceTanzimTarihi.ToString().IsDateTime())
            {
                response.SonucKod = "0";
                response.SonucAciklama = "POLİÇE TARİH BİLGİLERİ GİRİLMESİ GEREKMEKTEDİR. (Başlangıç, Bitiş ve Tanzim Tarihi)";
                return response;
            }
            if (DateTime.Parse(req.PoliceBitisTarihi) < DateTime.Parse(req.PoliceBaslangicTarihi))
            {
                response.SonucKod = "0";
                response.SonucAciklama = "POLİÇE BİTİŞ TARİHİ BAŞLANGIÇ TARİHİNDEN KÜÇÜK OLAMAZ";
                return response;
            }
            if (DateTime.Parse(req.PoliceTanzimTarihi) > DateTime.Parse(req.PoliceBitisTarihi))
            {
                response.SonucKod = "0";
                response.SonucAciklama = "POLİÇE TANZİM TARİHİ BİTİŞ TARİHİNDEN BÜYÜK OLAMAZ";
                return response;
            }
            if (!req.AcenteNo.IsInt64())
            {
                response.SonucKod = "0";
                response.SonucAciklama = "ACENTE NO GİRİLMESİ GEREKMEKTEDİR";
                return response;
            }
            if (req.PaketNo.IsNull())
            {
                response.SonucKod = "0";
                response.SonucAciklama = "PAKET BİLGİSİ BULUNAMADI!";
                return response;
            }

            #region Data Control
            V_Package package = new GenericRepository<V_Package>().FindBy($"PACKAGE_NO='{req.PaketNo}' AND COMPANY_ID={DogaCompanyId}", orderby: "").FirstOrDefault();
            if (package == null)
            {
                response.SonucKod = "0";
                response.SonucAciklama = $"SİGORTALI PAKET BİLGİSİ HATALI (PaketNo = {req.PaketNo})";
                return response;
            }

            Policy policy = new GenericRepository<Policy>().FindBy($"COMPANY_ID={DogaCompanyId} AND NO={req.PoliceNo} AND RENEWAL_NO={(req.YenilemeNo.IsNull() ? "0" : req.YenilemeNo)}", orderby: "").FirstOrDefault();
            if (policy != null)
            {
                if (req.ZeyilKodu.IsNull())
                {
                    response.SonucAciklama = "ZEYİL TİPİ BELİRLENEMEDİ! ZEYİL KODU GİRİNİZ.";
                    response.SonucKod = "0";
                    return response;
                }
                req.PoliceId = policy.Id;
                req.PolicyStatus = ((int)PolicyStatus.TANZIMLI).ToString();

                if (req.KayitTipi == "K")
                {
                    req.PrimType = 1;
                }
                else
                {
                    req.PrimType = -1;
                }

                switch (req.ZeyilKodu)
                {
                    case "11":
                        req.ZeyilTipi = ((int)EndorsementType.MEBDEINDEN_BASLANGICTAN_IPTAL).ToString();
                        req.PolicyStatus = ((int)PolicyStatus.IPTAL).ToString();

                        break;
                    case "13":
                        req.ZeyilTipi = ((int)EndorsementType.GUN_ESASLI_IPTAL).ToString();
                        req.PolicyStatus = ((int)PolicyStatus.IPTAL).ToString();

                        break;
                    case "05":
                        req.ZeyilTipi = ((int)EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI).ToString();
                        req.PrimType = 0;

                        break;
                    case "32":
                        req.ZeyilTipi = ((int)EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI).ToString();
                        req.PrimType = 0;
                        break;
                    case "90":
                        req.ZeyilTipi = ((int)EndorsementType.MERIYETE_DONUS_ZEYLI).ToString();

                        break;
                    case "S1":
                        req.ZeyilTipi = ((int)EndorsementType.SE_DEGISIKLIGI).ToString();
                        req.PrimType = 0;

                        break;
                    case "03":
                        req.ZeyilTipi = ((int)EndorsementType.PRIM_FARKI_ZEYLI).ToString();

                        break;
                    case "24":
                        req.ZeyilTipi = ((int)EndorsementType.SIGORTALI_GIRISI).ToString();
                        break;
                    case "25":
                        req.ZeyilTipi = ((int)EndorsementType.SIGORTALI_CIKISI).ToString();
                        break;
                    case "PD":
                        req.ZeyilTipi = ((int)EndorsementType.TEMINAT_PLAN_DEGISIKLIGI).ToString();
                        break;
                    default:
                        req.ZeyilTipi = ((int)EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI).ToString();
                        req.PrimType = 0;

                        break;
                }

            }
            else
            {
                if (req.KayitTipi == "K" && req.ZeyilKodu.IsNull())
                {
                    req.ZeyilTipi = ((int)EndorsementType.BASLANGIC).ToString();
                    req.PrimType = 1;
                    req.PolicyStatus = ((int)PolicyStatus.TANZIMLI).ToString();

                }
                else
                {
                    response.SonucAciklama = "POLİÇE BİLGİSİ BULUNAMADI!";
                    response.SonucKod = "0";
                    return response;
                }
            }

            #endregion

            if (req.ZeyilTipi != ((int)EndorsementType.SE_DEGISIKLIGI).ToString() && req.ZeyilTipi == ((int)EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI).ToString())
            {
                if (!req.NetPrim.IsNumeric())
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "NET PRİM DEĞERİ GİRİLMELİDİR!";
                    return response;
                }
            }
            if (req.ZeyilTipi == ((int)EndorsementType.BASLANGIC).ToString() || req.ZeyilTipi == ((int)EndorsementType.SIGORTALI_GIRISI).ToString() || req.ZeyilTipi == ((int)EndorsementType.SE_DEGISIKLIGI).ToString() || req.ZeyilTipi == ((int)EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI).ToString())
            {
                if (req.MusteriGercekTuzel.IsNull())
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "MusteriGercekTuzel Alanı Boş Geçilemez";
                    return response;
                }
                else
                {
                    switch (req.MusteriGercekTuzel)
                    {
                        case "Gerçek": //Gerçek
                            if (!req.MusteriTcKimlikNo.IsInt64() && req.MusteriPasaportNo.IsNull() && !req.MusteriVergiNo.IsInt64())
                            {
                                response.SonucKod = "0";
                                response.SonucAciklama = "MÜŞTERİ GERÇEK TİPİNDE OLDUĞUNDA MusteriTcKimlikNo, MusteriPasaportNo ve MusteriVergiNo EN AZ BİRİNİN DOLU OLMASI GEREKMEKTEDİR";
                                return response;
                            }
                            if (req.MusteriTcKimlikNo.IsInt64())
                            {
                                if (((long?)long.Parse(req.MusteriTcKimlikNo)).IsIdentityNoLength() == null)
                                {
                                    response.SonucKod = "0";
                                    response.SonucAciklama = "MusteriTcKimlikNo ALANI 11 HANELİ OLMALI!";
                                    return response;
                                }
                            }
                            if (req.MusteriAdi.IsNull() || req.MusteriSoyadi.IsNull())
                            {
                                response.SonucKod = "0";
                                response.SonucAciklama = "MÜŞTERİ GERÇEK TİPİNDE OLDUĞUNDA MusteriAdi VE MusteriSoyadi BİLGİSİ GİRİLMESİ GEREKMEKTEDİR";
                                return response;
                            }

                            if (req.MusteriCinsiyet.IsNull())
                            {
                                response.SonucKod = "0";
                                response.SonucAciklama = "MÜŞTERİ GERÇEK TİPİNDE OLDUĞUNDA MusteriCinsiyet BİLGİSİ GİRİLMESİ GEREKMEKTEDİR";
                                return response;
                            }
                            else
                            {
                                if (req.MusteriCinsiyet.ToUpper() != "K" && req.MusteriCinsiyet.ToUpper() != "E" && req.MusteriCinsiyet.ToUpper() != "0" && req.MusteriCinsiyet.ToUpper() != "1")
                                {
                                    response.SonucKod = "0";
                                    response.SonucAciklama = "MusteriCinsiyet BİLGİSİ HATALI (E,K) yada (0,1)";
                                    return response;
                                }
                            }

                            if (!req.MusteriDogumTarihi.IsDateTime())
                            {
                                response.SonucKod = "0";
                                response.SonucAciklama = "MÜŞTERİ GERÇEK TİPİNDE OLDUĞUNDA MusteriDogumTarihi BİLGİSİ GİRİLMESİ GEREKMEKTEDİR";
                                return response;
                            }
                            if (req.MusteriVergiNo.IsInt64())
                            {
                                if (((long?)long.Parse(req.MusteriVergiNo)).IsTaxNumberLength() == null)
                                {
                                    req.MusteriVergiNo = null;
                                    //response.SonucKod = "0";
                                    //response.SonucAciklama = "MusteriVergiNo ALANI 10 HANELİ OLMALI!";
                                    //return response;
                                }
                            }
                            break;
                        default: //Tüzel
                                 //if (req.SigEttiren.TK.SirketAd.IsNull())
                                 //{
                                 //    response.SonucKod = "0";
                                 //    response.SonucAciklama = "SİGORTA ETTİREN ŞİRKET ADI BİLGİSİ GİRİLMESİ GEREKMEKTEDİR";
                                 //    return response;
                                 //}

                            if (req.MusteriVergiNo.IsInt64())
                            {
                                if (((long?)long.Parse(req.MusteriVergiNo)).IsTaxNumberLength() == null)
                                {
                                    response.SonucKod = "0";
                                    response.SonucAciklama = "MusteriVergiNo ALANI 10 HANELİ OLMALI!";
                                    return response;
                                }
                            }
                            else
                            {
                                response.SonucKod = "0";
                                response.SonucAciklama = "MÜŞTERİ TÜZEL TİPİNDE OLDUĞUNDA MusteriVergiNo ALANI DOLU OLMASI GEREKMEKTEDİR";
                                return response;
                            }
                            break;
                    }
                }
            }
            if (req.ZeyilTipi == ((int)EndorsementType.BASLANGIC).ToString() || req.ZeyilTipi == ((int)EndorsementType.SIGORTALI_GIRISI).ToString() || req.ZeyilTipi == ((int)EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI).ToString())
            {
                if (req.ekSigortaliList.Count < 1)
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "EN AZ BİR SİGORTALI BİLGİSİ GİRİLMESİ GEREKMEKTEDİR";
                    return response;
                }
                else
                {
                    foreach (var sigortali in req.ekSigortaliList)
                    {
                        //if (sigortali.IlkKayitTarihi == null)
                        //{
                        //    response.SonucAciklama = "İlk Kayıt Tarihi Boş Geçilemez.";
                        //    response.SonucKod = "0";
                        //    errorArray.Add(response.SonucAciklama);
                        //}
                        //if (sigortali.IlkSigortalanmaTarihi == null)
                        //{
                        //    response.SonucAciklama = "İlk Sigortalanma Tarihi Boş Geçilemez.";
                        //    response.SonucKod = "0";
                        //    errorArray.Add(response.SonucAciklama);
                        //}

                        if (sigortali.BireyTip.IsNull())
                        {
                            sigortali.BireyTip = "F";
                        }

                        if (!sigortali.Prim.IsNumeric())
                        {
                            response.SonucKod = "0";
                            response.SonucAciklama = $"SİGORTALI PRİM GİRİLMESİ GEREKMEKTEDİR (SigortaliNo - {sigortali.SigortaliNo})";
                            return response;
                        }

                        if (!sigortali.TcKimlikNo.IsInt64() && sigortali.PasaportNo.IsNull() && !sigortali.VergiNo.IsInt64())
                        {
                            response.SonucKod = "0";
                            response.SonucAciklama = $"SİGORTALI TcKimlikNo, VergiNo ve PasaportNo EN AZ BİRİNİN DOLU OLMASI GEREKMEKTEDİR (SigortaliNo - {sigortali.SigortaliNo})";
                            return response;
                        }

                        if (sigortali.Uyruk.IsNull())
                        {
                            response.SonucKod = "0";
                            response.SonucAciklama = $"SİGORTALI UYRUK BİLGİSİ GİRİLMESİ GEREKMEKTEDİR (SigortaliNo - {sigortali.SigortaliNo})";
                            return response;
                        }
                        else
                        {
                            switch (sigortali.Uyruk)
                            {
                                case "1": //Türk
                                    if (!sigortali.TcKimlikNo.IsInt64())
                                    {
                                        response.SonucKod = "0";
                                        response.SonucAciklama = $"SİGORTALI TcKimlikNo ALANI BOŞ GEÇİLEMEZ (SigortaliNo - {sigortali.SigortaliNo})";
                                        return response;
                                    }
                                    else
                                    {
                                        if (((long?)long.Parse(sigortali.TcKimlikNo)).IsIdentityNoLength() == null)
                                        {
                                            response.SonucKod = "0";
                                            response.SonucAciklama = $"SİGORTALI TcKimlikNo 11 HANE OLMALI! (SigortaliNo - {sigortali.SigortaliNo})";
                                            return response;
                                        }
                                    }
                                    break;
                                case "2": //Yabancı
                                    if (sigortali.TcKimlikNo.IsInt64())
                                    {
                                        if (((long?)long.Parse(sigortali.TcKimlikNo)).IsIdentityNoLength() == null)
                                        {
                                            response.SonucKod = "0";
                                            response.SonucAciklama = $"SİGORTALI TcKimlikNo 11 HANE OLMALI! (SigortaliNo - {sigortali.SigortaliNo})";
                                            return response;
                                        }
                                    }
                                    if (sigortali.DogumYeri.IsNull())
                                    {
                                        response.SonucKod = "0";
                                        response.SonucAciklama = $"SİGORTALI YABANCI UYRUKLULAR İÇİN DOĞUM YERİ GİRİLMESİ GEREKMEKTEDİR. (SigortaliNo - {sigortali.SigortaliNo})";
                                        return response;
                                    }
                                    break;
                                default:
                                    break;
                            }

                            if (sigortali.Adi.IsNull() || sigortali.Soyadi.IsNull())
                            {
                                response.SonucKod = "0";
                                response.SonucAciklama = $"SİGORTALI AD - SOYAD BİLGİSİ GİRİLMESİ GEREKMEKTEDİR (SigortaliNo - {sigortali.SigortaliNo})";
                                return response;
                            }
                            if (sigortali.Cinsiyet.IsNull())
                            {
                                response.SonucKod = "0";
                                response.SonucAciklama = $"SİGORTALI CİNSİYET BİLGİSİ GİRİLMESİ GEREKMEKTEDİR (SigortaliNo - {sigortali.SigortaliNo})";
                                return response;
                            }
                            else
                            {
                                if (sigortali.Cinsiyet.ToUpper() != "K" && sigortali.Cinsiyet.ToUpper() != "E" && sigortali.Cinsiyet.ToUpper() != "0" && sigortali.Cinsiyet.ToUpper() != "1")
                                {
                                    response.SonucKod = "0";
                                    response.SonucAciklama = $"SİGORTALI CİNSİYET BİLGİSİ HATALI (E,K) yada (0,1)  (SigortaliNo - {sigortali.SigortaliNo})";
                                    return response;
                                }
                            }
                            if (!sigortali.DogumTarihi.ToString().IsDateTime())
                            {
                                response.SonucKod = "0";
                                response.SonucAciklama = $"SİGORTALI DOĞUM TARİHİ BİLGİSİ GİRİLMESİ GEREKMEKTEDİR (SigortaliNo - {sigortali.SigortaliNo})";
                                return response;
                            }

                            if (!sigortali.VergiNo.IsNull())
                            {
                                if (sigortali.VergiNo.Length != 10)
                                {
                                    sigortali.VergiNo = null;
                                    //response.SonucKod = "0";
                                    //response.SonucAciklama = $"SİGORTALI VERGİ NO 10 HANE OLMALI (SigortaliNo - {sigortali.SigortaliNo})";
                                    //return response;
                                }
                            }
                        }
                    }
                }
            }
            if (req.ZeyilTipi == ((int)EndorsementType.SIGORTALI_CIKISI).ToString())
            {
                if (req.ekSigortaliList.Count < 1)
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "EN AZ BİR SİGORTALI BİLGİSİ GİRİLMESİ GEREKMEKTEDİR";
                    return response;
                }
                else
                {
                    foreach (var sigortali in req.ekSigortaliList)
                    {
                        if (!sigortali.Prim.IsNumeric())
                        {
                            response.SonucKod = "0";
                            response.SonucAciklama = $"SİGORTALI PRİM GİRİLMESİ GEREKMEKTEDİR (SigortaliNo - {sigortali.SigortaliNo})";
                            return response;
                        }
                    }
                }
            }
            #endregion

            response.SonucAciklama = "OK";
            response.SonucKod = "1";

            return response;
        }

        private DogaTekPolReq TekPolXmlRead(string xml)
        {
            DogaTekPolReq tekPolReq = new DogaTekPolReq();
            DogaPoliceAktarRes result = new DogaPoliceAktarRes();
            XmlDocument xmlDoc = new XmlDocument();
            //XmlNamespaceManager namespaceManager = new XmlNamespaceManager(xmlDoc.NameTable);
            //namespaceManager.AddNamespace()
            xmlDoc.LoadXml(xml);


            try
            {
                var Statu = xmlDoc.LastChild.LastChild.InnerText;
                if (Statu == "false")
                {
                    tekPolReq.ErrorMessage = xmlDoc.LastChild.FirstChild.InnerText;
                    return tekPolReq;
                }

                #region Ek Alanlar
                if ((xmlDoc.SelectSingleNode("//_Grup") != null && xmlDoc.SelectSingleNode("//_Grup").InnerXml.ToString() == "E") || (xmlDoc.SelectSingleNode("//_GrupPolicesiMi") != null && xmlDoc.SelectSingleNode("//_GrupPolicesiMi").InnerXml.ToString() == "E"))
                {
                    tekPolReq.GrupFerdi = ((int)PolicyType.GRUP).ToString();
                }
                else
                {
                    tekPolReq.GrupFerdi = ((int)PolicyType.FERDI).ToString();
                }

                if (xmlDoc.SelectSingleNode("//_ImecePaket") != null)
                {
                    tekPolReq.PaketNo = xmlDoc.SelectSingleNode("//_ImecePaket").InnerXml;
                }
                if (xmlDoc.SelectSingleNode("//_SeyahatSaglikTarifeId") != null)
                {
                    tekPolReq._SeyahatSaglikTarifeId = xmlDoc.SelectSingleNode("//_SeyahatSaglikTarifeId").InnerXml;
                }
                if (xmlDoc.SelectSingleNode("//_TaksitSayisi") != null)
                {
                    tekPolReq._TaksitSayisi = xmlDoc.SelectSingleNode("//_TaksitSayisi").InnerXml;
                }
                if (xmlDoc.SelectSingleNode("//_OncekiPoliceBaslangicTarihi") != null)
                {
                    tekPolReq._OncekiPoliceBaslangicTarihi = xmlDoc.SelectSingleNode("//_OncekiPoliceBaslangicTarihi").InnerXml;
                }
                if (xmlDoc.SelectSingleNode("//_OncekiPoliceBitisTarihi") != null)
                {
                    tekPolReq._OncekiPoliceBitisTarihi = xmlDoc.SelectSingleNode("//_OncekiPoliceBitisTarihi").InnerXml;
                }
                if (xmlDoc.SelectSingleNode("//_OncekiPoliceNo") != null)
                {
                    tekPolReq._OncekiPoliceNo = xmlDoc.SelectSingleNode("//_OncekiPoliceNo").InnerXml;
                }
                if (xmlDoc.SelectSingleNode("//_OncekiSigortaSirketi") != null)
                {
                    tekPolReq._OncekiSigortaSirketi = xmlDoc.SelectSingleNode("//_OncekiSigortaSirketi").InnerXml;
                }
                if (xmlDoc.SelectSingleNode("//_OncekiSirketAdi") != null)
                {
                    tekPolReq._OncekiSirketAdi = xmlDoc.SelectSingleNode("//_OncekiSirketAdi").InnerXml;
                }
                if (xmlDoc.SelectSingleNode("//_OncekiSirketKodu") != null)
                {
                    tekPolReq._OncekiSirketKodu = xmlDoc.SelectSingleNode("//_OncekiSirketKodu").InnerXml;
                }
                if (xmlDoc.SelectSingleNode("//_PoliceBaslangicTarihi") != null && xmlDoc.SelectSingleNode("//_PoliceBaslangicTarihi").ToString().IsDateTime())
                {
                    tekPolReq.PoliceBaslangicTarihi = xmlDoc.SelectSingleNode("//_PoliceBaslangicTarihi").InnerXml;
                }
                if (xmlDoc.SelectSingleNode("//_PoliceBitisTarihi") != null && xmlDoc.SelectSingleNode("//_PoliceBitisTarihi").ToString().IsDateTime())
                {
                    tekPolReq.PoliceBitisTarihi = xmlDoc.SelectSingleNode("//_PoliceBitisTarihi").InnerXml;
                }
                if (xmlDoc.SelectSingleNode("//_SaglikTarifeId") != null)
                {
                    tekPolReq._SaglikTarifeId = xmlDoc.SelectSingleNode("//_SaglikTarifeId").InnerXml;
                }
                if (xmlDoc.SelectSingleNode("//_VipKodu") != null)
                {
                    tekPolReq._VipKodu = xmlDoc.SelectSingleNode("//_VipKodu").InnerXml;
                }
                if (xmlDoc.SelectSingleNode("//_BOY") != null)
                {
                    tekPolReq._BOY = xmlDoc.SelectSingleNode("//_BOY").InnerXml;
                }
                if (xmlDoc.SelectSingleNode("//_KILO") != null)
                {
                    tekPolReq._KILO = xmlDoc.SelectSingleNode("//_KILO").InnerXml;
                }
                if (xmlDoc.SelectSingleNode("//_OzelEkPrim") != null)
                {
                    tekPolReq._OzelEkPrim = xmlDoc.SelectSingleNode("//_OzelEkPrim").InnerXml;
                }
                if (xmlDoc.SelectSingleNode("//_RedNedeni") != null)
                {
                    tekPolReq._RedNedeni = xmlDoc.SelectSingleNode("//_RedNedeni").InnerXml;
                }
                if (xmlDoc.SelectSingleNode("//_SaglikTarifesiId") != null)
                {
                    tekPolReq._SaglikTarifesiId = xmlDoc.SelectSingleNode("//_SaglikTarifesiId").InnerXml;
                }
                if (xmlDoc.SelectSingleNode("//_SigortaliRiskEkPrimi") != null)
                {
                    tekPolReq._SigortaliRiskEkPrimi = xmlDoc.SelectSingleNode("//_SigortaliRiskEkPrimi").InnerXml;
                }
                if (xmlDoc.SelectSingleNode("//_SigortaliUwGorusu") != null)
                {
                    tekPolReq._SigortaliUwGorusu = xmlDoc.SelectSingleNode("//_SigortaliUwGorusu").InnerXml;
                }

                #region Beyan
                tekPolReq.BeyanListesi = new List<Beyan>();
                for (int i = 1; i <= 30; i++)
                {
                    Beyan beyan = new Beyan();
                    string BeyanNo = "//_BEYAN" + i;
                    if (xmlDoc.SelectSingleNode(BeyanNo) != null && !xmlDoc.SelectSingleNode(BeyanNo).InnerXml.IsNull() && xmlDoc.SelectSingleNode(BeyanNo).InnerXml != "H")
                    {
                        beyan.BeyanDesc = xmlDoc.SelectSingleNode(BeyanNo).InnerXml;
                        tekPolReq.BeyanListesi.Add(beyan);
                    }
                }

                if (xmlDoc.SelectSingleNode("//_BEYAN001") != null && !xmlDoc.SelectSingleNode("//_BEYAN001").InnerXml.IsNull() && xmlDoc.SelectSingleNode("//_BEYAN001").InnerXml != "H")
                {
                    Beyan beyan = new Beyan();
                    beyan.BeyanDesc = xmlDoc.SelectSingleNode("//_BEYAN001").InnerXml;
                    tekPolReq.BeyanListesi.Add(beyan);
                }
                if (xmlDoc.SelectSingleNode("//_BEYAN002") != null && !xmlDoc.SelectSingleNode("//_BEYAN002").InnerXml.IsNull() && xmlDoc.SelectSingleNode("//_BEYAN002").InnerXml != "H")
                {
                    Beyan beyan = new Beyan();
                    beyan.BeyanDesc = xmlDoc.SelectSingleNode("//_BEYAN002").InnerXml;
                    tekPolReq.BeyanListesi.Add(beyan);
                }
                if (xmlDoc.SelectSingleNode("//_BEYAN003") != null && !xmlDoc.SelectSingleNode("//_BEYAN003").InnerXml.IsNull() && xmlDoc.SelectSingleNode("//_BEYAN003").InnerXml != "H")
                {
                    Beyan beyan = new Beyan();
                    beyan.BeyanDesc = xmlDoc.SelectSingleNode("//_BEYAN003").InnerXml;
                    tekPolReq.BeyanListesi.Add(beyan);
                }
                #endregion

                #endregion

                tekPolReq.AcenteNo = xmlDoc.SelectSingleNode("//Alanlar/AcenteNo").InnerXml;
                tekPolReq.DovizKuru = xmlDoc.SelectSingleNode("//Alanlar/DovizKuru").InnerXml;
                tekPolReq.DovizTipi = xmlDoc.SelectSingleNode("//Alanlar/DovizTipi").InnerXml;
                tekPolReq.KayitTipi = xmlDoc.SelectSingleNode("//Alanlar/KayitTipi").InnerXml;


                tekPolReq.MusteriAdi = xmlDoc.SelectSingleNode("//Alanlar/MusteriAdi").InnerXml;
                tekPolReq.MusteriBabaAdi = xmlDoc.SelectSingleNode("//Alanlar/MusteriBabaAdi").InnerXml;
                tekPolReq.MusteriCepTelefonNo = xmlDoc.SelectSingleNode("//Alanlar/MusteriCepTelefonNo").InnerXml;
                tekPolReq.MusteriCinsiyet = xmlDoc.SelectSingleNode("//Alanlar/MusteriCinsiyet").InnerXml;
                tekPolReq.MusteriDogumTarihi = xmlDoc.SelectSingleNode("//Alanlar/MusteriDogumTarihi").InnerXml;
                tekPolReq.MusteriDogumYeri = xmlDoc.SelectSingleNode("//Alanlar/MusteriDogumYeri").InnerXml;
                tekPolReq.MusteriEPosta = xmlDoc.SelectSingleNode("//Alanlar/MusteriEPosta").InnerXml;
                tekPolReq.MusteriEvApartmanAdi = xmlDoc.SelectSingleNode("//Alanlar/MusteriEvApartmanAdi").InnerXml;
                tekPolReq.MusteriEvCadde = xmlDoc.SelectSingleNode("//Alanlar/MusteriEvCadde").InnerXml;
                tekPolReq.MusteriEvDaire = xmlDoc.SelectSingleNode("//Alanlar/MusteriEvDaire").InnerXml;
                tekPolReq.MusteriEvIlceKodu = xmlDoc.SelectSingleNode("//Alanlar/MusteriEvIlceKodu").InnerXml;
                tekPolReq.MusteriEvIlKodu = xmlDoc.SelectSingleNode("//Alanlar/MusteriEvIlKodu").InnerXml;
                tekPolReq.MusteriEvKapiNo = xmlDoc.SelectSingleNode("//Alanlar/MusteriEvKapiNo").InnerXml;
                tekPolReq.MusteriEvMahalle = xmlDoc.SelectSingleNode("//Alanlar/MusteriEvMahalle").InnerXml;
                tekPolReq.MusteriEvSemt = xmlDoc.SelectSingleNode("//Alanlar/MusteriEvSemt").InnerXml;
                tekPolReq.MusteriEvSokak = xmlDoc.SelectSingleNode("//Alanlar/MusteriEvSokak").InnerXml;
                tekPolReq.MusteriEvTelefonNo = xmlDoc.SelectSingleNode("//Alanlar/MusteriEvTelefonNo").InnerXml;
                tekPolReq.MusteriGercekTuzel = xmlDoc.SelectSingleNode("//Alanlar/MusteriGercekTuzel").InnerXml;
                tekPolReq.MusteriMedeniHali = xmlDoc.SelectSingleNode("//Alanlar/MusteriMedeniHali").InnerXml;
                tekPolReq.MusteriNo = xmlDoc.SelectSingleNode("//Alanlar/MusteriNo").InnerXml; //Contact Parameter
                tekPolReq.MusteriSoyadi = xmlDoc.SelectSingleNode("//Alanlar/MusteriSoyadi").InnerXml;
                tekPolReq.MusteriTcKimlikNo = xmlDoc.SelectSingleNode("//Alanlar/MusteriTcKimlikNo").InnerXml;
                tekPolReq.MusteriVergiNo = xmlDoc.SelectSingleNode("//Alanlar/MusteriVergiNo").InnerXml;
                tekPolReq.MusteriPasaportNo = xmlDoc.SelectSingleNode("//Alanlar/MusteriPasaportNo").InnerXml;


                tekPolReq.NetPrim = xmlDoc.SelectSingleNode("//Alanlar/NetPrim").InnerXml;
                tekPolReq.PoliceGirisTarihi = xmlDoc.SelectSingleNode("//Alanlar/PoliceGirisTarihi").InnerXml;
                tekPolReq.PoliceIptalTarihi = xmlDoc.SelectSingleNode("//Alanlar/PoliceIptalTarihi").InnerXml;
                tekPolReq.PoliceNo = xmlDoc.SelectSingleNode("//Alanlar/PoliceNo").InnerXml;
                tekPolReq.YenilemeNo = xmlDoc.SelectSingleNode("//Alanlar/TecditNo").InnerXml;
                tekPolReq.SagmerPoliceNo = xmlDoc.SelectSingleNode("//Alanlar/SagmerPoliceNo").InnerXml;

                tekPolReq.TaksitDurumu = xmlDoc.SelectSingleNode("//Alanlar/TaksitDurumu").InnerXml; //installmentcount
                tekPolReq.TaksitTipi = xmlDoc.SelectSingleNode("//Alanlar/TaksitTipi").InnerXml; //paymentType
                tekPolReq.TaksitTipiAciklama = xmlDoc.SelectSingleNode("//Alanlar/TaksitTipiAciklama").InnerXml; //paymentDescription

                tekPolReq.PoliceBaslangicTarihi = tekPolReq.PoliceBaslangicTarihi.IsDateTime() ? tekPolReq.PoliceBaslangicTarihi : xmlDoc.SelectSingleNode("//Alanlar/VadeBaslangic").InnerXml; //endorsement_start_date
                tekPolReq.PoliceBitisTarihi = tekPolReq.PoliceBitisTarihi.IsDateTime() ? tekPolReq.PoliceBitisTarihi : xmlDoc.SelectSingleNode("//Alanlar/VadeBitis").InnerXml; //t_policy end_date
                tekPolReq.PoliceTanzimTarihi = xmlDoc.SelectSingleNode("//Alanlar/TanzimTarihi").InnerXml; //endorsement_issue_date
                tekPolReq.ZeyilNo = xmlDoc.SelectSingleNode("//Alanlar/ZeyilNo").InnerXml; //boşsa sıfır yani baslangıc zeyli
                tekPolReq.ZeyilKodu = xmlDoc.SelectSingleNode("//Alanlar/ZeyilKodu").InnerXml;
                tekPolReq.ZeyilAdi = xmlDoc.SelectSingleNode("//Alanlar/ZeyilAdi").InnerXml;

                if (xmlDoc.SelectNodes("//TaksitArr/Taksit").Count > 0)
                {
                    List<Taksit> Taksitlist = new List<Taksit>();
                    foreach (XmlNode TaksitBilgi in xmlDoc.SelectNodes("//Taksit"))
                    {
                        Taksit taksit = new Taksit();

                        taksit.VadeTarihi = TaksitBilgi.ChildNodes[0].InnerText;//.SelectSingleNode("//VadeTarihi").InnerXml;
                        taksit.Planlanan = TaksitBilgi.ChildNodes[1].InnerText;


                        Taksitlist.Add(taksit);
                        tekPolReq.taksitList = Taksitlist;
                    }
                }

                if (xmlDoc.SelectNodes("//Odeme").Count > 0)
                {
                    var tipi = "";
                    foreach (XmlNode item in xmlDoc.SelectNodes("//Odeme"))
                    {
                        tipi = item.SelectSingleNode("Tipi").InnerXml;

                        if (tipi == "K")
                            tipi = ((int)PaymentType.KREDI_KARTI).ToString();
                        else if (tipi == "C")
                            tipi = ((int)PaymentType.CEK).ToString();
                        else if (tipi == "H")
                            tipi = ((int)PaymentType.HAVALE).ToString();
                        else
                            tipi = ((int)PaymentType.HAVALE).ToString();

                        break;
                    }
                    tekPolReq.PolicyPaymentType = tipi;
                }
                else
                {
                    tekPolReq.PolicyPaymentType = ((int)PaymentType.HAVALE).ToString();
                }

                if (xmlDoc.SelectNodes("//EkSigortali").Count > 0)
                {
                    List<EkSigortali> SigortaliList = new List<EkSigortali>();
                    foreach (XmlNode ekSigortaliBilgi in xmlDoc.SelectNodes("//EkSigortali"))
                    {
                        EkSigortali sigortali = new EkSigortali();

                        sigortali.Adi = ekSigortaliBilgi.SelectSingleNode("Adi").InnerXml;
                        sigortali.AnnelikTeminatiBaslangicTarihi = ekSigortaliBilgi.SelectSingleNode("AnnelikTeminatiBaslangicTarihi").InnerXml;
                        sigortali.BabaAdi = ekSigortaliBilgi.SelectSingleNode("BabaAdi").InnerXml;
                        sigortali.Cinsiyet = ekSigortaliBilgi.SelectSingleNode("Cinsiyet").InnerXml;
                        sigortali.DogumTarihi = ekSigortaliBilgi.SelectSingleNode("DogumTarihi").InnerXml;
                        sigortali.EvAdresi = ekSigortaliBilgi.SelectSingleNode("EvAdresi").InnerXml;
                        sigortali.EvIlceKodu = ekSigortaliBilgi.SelectSingleNode("EvIlceKodu").InnerXml;
                        sigortali.EvIlKodu = ekSigortaliBilgi.SelectSingleNode("EvIlKodu").InnerXml;

                        sigortali.IlkAyaktateminatBaslangicTarihi = ekSigortaliBilgi.SelectSingleNode("IlkAyaktateminatBaslangicTarihi").InnerXml;
                        sigortali.IlkSigortalilikTarihi = ekSigortaliBilgi.SelectSingleNode("IlkSigortalilikTarihi").InnerXml;
                        sigortali.IlkKayitTarihi = ekSigortaliBilgi.SelectSingleNode("DogaGirisTarihi").InnerXml;

                        sigortali.EkSigortaliCepNo = ekSigortaliBilgi.SelectSingleNode("EkSigortaliCepNo").InnerXml;
                        sigortali.EPosta = ekSigortaliBilgi.SelectSingleNode("EPosta").InnerXml;
                        sigortali.EkSigortaliIptalKayit = ekSigortaliBilgi.SelectSingleNode("EkSigortaliIptalKayit").InnerXml; //K aktif, p PASIF
                        sigortali.GecisTipi = ekSigortaliBilgi.SelectSingleNode("GecisTipi").InnerXml;
                        sigortali.OncekiSigortaSirketi = ekSigortaliBilgi.SelectSingleNode("OncekiSigortaSirketi").InnerXml;

                        if (tekPolReq.PaketNo.IsNull())
                        {
                            tekPolReq.PaketNo = ekSigortaliBilgi.SelectSingleNode("ImeceKisiPaket").InnerXml;
                        }

                        sigortali.ImeceKisiPaket = ekSigortaliBilgi.SelectSingleNode("ImeceKisiPaket").InnerXml.IsNull() ? tekPolReq.PaketNo : ekSigortaliBilgi.SelectSingleNode("ImeceKisiPaket").InnerXml;

                        sigortali.MedeniHali = ekSigortaliBilgi.SelectSingleNode("MedeniHali").InnerXml;
                        sigortali.OmurBoyuYenilemeGarantisi = ekSigortaliBilgi.SelectSingleNode("OmurBoyuYenilemeGarantisi").InnerXml;
                        sigortali.OzelNotKisi = ekSigortaliBilgi.SelectSingleNode("OzelNotKisi").InnerXml;
                        sigortali.PasaportNo = ekSigortaliBilgi.SelectSingleNode("PasaportNo").InnerXml;
                        sigortali.Prim = ekSigortaliBilgi.SelectSingleNode("Prim").InnerXml;
                        sigortali.SicilNumarasi = ekSigortaliBilgi.SelectSingleNode("SicilNumarasi").InnerXml;
                        sigortali.SigortaliHesapBankaKodu = ekSigortaliBilgi.SelectSingleNode("SigortaliHesapBankaKodu").InnerXml;
                        sigortali.SigortaliIbanNo = ekSigortaliBilgi.SelectSingleNode("SigortaliIbanNo").InnerXml;
                        sigortali.SigortaliUwGorusu = ekSigortaliBilgi.SelectSingleNode("SigortaliUwGorusu").InnerXml;
                        sigortali.SigortaliRiskEkPrimi = ekSigortaliBilgi.SelectSingleNode("SigortaliRiskEkPrimi").InnerXml;
                        sigortali.Soyadi = ekSigortaliBilgi.SelectSingleNode("Soyadi").InnerXml;
                        sigortali.TcKimlikNo = ekSigortaliBilgi.SelectSingleNode("TcKimlikNo").InnerXml;
                        sigortali.TelefonNo = ekSigortaliBilgi.SelectSingleNode("TelefonNo").InnerXml;
                        sigortali.Uyruk = ekSigortaliBilgi.SelectSingleNode("Uyruk").InnerXml.IsNull() ? "TR" : ekSigortaliBilgi.SelectSingleNode("Uyruk").InnerXml;
                        sigortali.VergiDairesi = ekSigortaliBilgi.SelectSingleNode("VergiDairesi").InnerXml;
                        sigortali.VergiNo = ekSigortaliBilgi.SelectSingleNode("VergiNo").InnerXml;

                        sigortali.Vip = ekSigortaliBilgi.SelectSingleNode("Vip").InnerXml;

                        if (tekPolReq.GrupFerdi == ((int)PolicyType.GRUP).ToString())
                        {
                            var yakinlik = ekSigortaliBilgi.SelectSingleNode("Yakinlik").InnerXml;
                            if (yakinlik == "Kendisi" || yakinlik == "Sigortali")
                            {
                                sigortali.BireyTip = ((int)IndividualType.FERT).ToString();
                            }
                            else if (yakinlik == "Eşi")
                            {
                                sigortali.BireyTip = ((int)IndividualType.ES).ToString();
                            }
                            else if (yakinlik == "Cocugu")
                            {
                                sigortali.BireyTip = ((int)IndividualType.COCUK).ToString();
                            }
                            else
                            {
                                sigortali.BireyTip = ((int)IndividualType.FERT).ToString();
                            }
                        }
                        else
                        {
                            var yakinlik = ekSigortaliBilgi.SelectSingleNode("Yakinlik").InnerXml;
                            if (yakinlik == "Kendisi" || yakinlik == "Sigortali")
                            {
                                sigortali.BireyTip = ((int)IndividualType.FERT).ToString();
                            }
                            else if (yakinlik == "Eşi")
                            {
                                sigortali.BireyTip = ((int)IndividualType.ES).ToString();
                            }
                            else
                            {
                                sigortali.BireyTip = ((int)IndividualType.COCUK).ToString();
                            }
                        }

                        sigortali.YenilemeGarantisiTarihi = ekSigortaliBilgi.SelectSingleNode("YenilemeGarantisiTarihi").InnerXml;
                        sigortali.YenilemeGarantisiTipi = ekSigortaliBilgi.SelectSingleNode("YenilemeGarantisiTipi").InnerXml;
                        sigortali.YenilemeGarantisiTuru = ekSigortaliBilgi.SelectSingleNode("YenilemeGarantisiTuru").InnerXml;
                        SigortaliList.Add(sigortali);
                        tekPolReq.ekSigortaliList = SigortaliList;
                    }
                }
                else
                {
                    List<EkSigortali> SigortaliList = new List<EkSigortali>();
                    EkSigortali sigortali = new EkSigortali();
                    sigortali.Adi = xmlDoc.SelectSingleNode("//SigortaliAdi").InnerXml;
                    sigortali.EvAdresi = xmlDoc.SelectSingleNode("//SigortaliAdres1").InnerXml;
                    sigortali.BabaAdi = xmlDoc.SelectSingleNode("//SigortaliBabaAdi").InnerXml;
                    sigortali.EkSigortaliCepNo = xmlDoc.SelectSingleNode("//SigortaliCepTelefonNo").InnerXml;
                    sigortali.Cinsiyet = xmlDoc.SelectSingleNode("//SigortaliCinsiyet").InnerXml;
                    sigortali.DogumTarihi = xmlDoc.SelectSingleNode("//SigortaliDogumTarihi").InnerXml;
                    sigortali.DogumYeri = xmlDoc.SelectSingleNode("//SigortaliDogumYeri").InnerXml;
                    sigortali.EPosta = xmlDoc.SelectSingleNode("//SigortaliEPosta").InnerXml;
                    sigortali.TelefonNo = xmlDoc.SelectSingleNode("//SigortaliEvTelefonNo").InnerXml;
                    sigortali.MedeniHali = xmlDoc.SelectSingleNode("//SigortaliMedeniHali").InnerXml;
                    sigortali.SigortaliNo = xmlDoc.SelectSingleNode("//SigortaliNo").InnerXml;
                    sigortali.PasaportNo = xmlDoc.SelectSingleNode("//SigortaliPasaportNo").InnerXml;
                    sigortali.Soyadi = xmlDoc.SelectSingleNode("//SigortaliSoyadi").InnerXml;
                    sigortali.TcKimlikNo = xmlDoc.SelectSingleNode("//SigortaliTcKimlikNo").InnerXml;
                    sigortali.Uyruk = xmlDoc.SelectSingleNode("//SigortaliUyrukDiger").InnerXml;
                    sigortali.VergiDairesi = xmlDoc.SelectSingleNode("//SigortaliVergiDairesi").InnerXml;
                    sigortali.VergiNo = xmlDoc.SelectSingleNode("//SigortaliVergiNo").InnerXml;
                    sigortali.BireyTip = ((int)IndividualType.FERT).ToString();
                    sigortali.Prim = xmlDoc.SelectSingleNode("//Alanlar/NetPrim").InnerXml;
                    sigortali.ImeceKisiPaket = xmlDoc.SelectSingleNode("//_ImecePaket").InnerXml;
                    //sigortali.Vip = xmlDoc.SelectSingleNode("police/Alanlar/_VipKodu").InnerXml;
                    SigortaliList.Add(sigortali);
                    tekPolReq.ekSigortaliList = SigortaliList;
                }
            }
            catch (Exception ex)
            { }
            return tekPolReq;
        }
        #endregion

        public DogaHasarAktarRes HasarAktar(string apiKod, long ClaimId)
        {
            DogaHasarAktarRes response = new DogaHasarAktarRes();
            var currentMethod = MethodBase.GetCurrentMethod();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                WsRequestDate = DateTime.Now,
                COMPANY_ID = DogaCompanyId,
                ClaimId = ClaimId
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Auth
                if (ApiCodeIsValid(apiKod) <= 0)
                {
                    response.SonucAciklama = "HATA";
                    response.SonucKod = "Giriş Başarısız! APİCODE'u Kontrol Ediniz";

                    CreateFailedLog(log, response.ToXML(true), response.SonucAciklama);
                    return response;
                }
                #endregion

                #region Fill Claim Dto
                ClaimDto cl = claimServiceClient.ClaimToDoga(ClaimId);
                if (cl == null)
                {
                    response.SonucAciklama = "HASAR BİLGİSİ BULUNAMADI";
                    response.SonucKod = "0";

                    CreateFailedLog(log, response.ToXML(true), response.SonucAciklama);
                    return response;
                }
                #endregion

                #region Mapping
                List<FaturaBilgisi> fatList = new List<FaturaBilgisi>();
                if (cl.Bill != null)
                {
                    FaturaBilgisi fatBilgi = new FaturaBilgisi
                    {
                        FaturaNo = cl.Bill.No,
                        FaturaTarihi = cl.Bill.Date.ToString(),
                        FaturaTutar = cl.Bill.TotalPaid.ToString(),
                        MukellefTuru = cl.Bill.CorporateTypeText,
                        StopajTutar = cl.Bill.TotalStoppage.ToString()
                    };
                    fatList.Add(fatBilgi);
                }
                List<Tani> taniList = new List<Tani>();
                if (cl.ICD != null && cl.ICD.Count > 0)
                {
                    foreach (var item in cl.ICD)
                    {
                        Tani taniBilgi = new Tani
                        {
                            TaniKodu = item.ICDCode,
                            KesinTaniMi = item.ICDType == ((int)ICDType.KESIN).ToString() ? "1" : "0"
                        };
                        taniList.Add(taniBilgi);
                    }
                }

                List<Islem> islemList = new List<Islem>();
                bool findYatarak = true;
                if (cl.Processes != null && cl.Processes.Count > 0)
                {
                    findYatarak = cl.Processes.Any(x => x.CoverageTypeText.ToLower() == "yatarak");
                    foreach (var item in cl.Processes)
                    {
                        Islem islem = new Islem
                        {
                            BrutTutar = item.Requested.ToString(),
                            FaturaNo = item.BillNo.ToString(),
                            FaturaTutari = item.BillTotalAmount.ToString(),
                            IslemKodu = item.ProcessCode,
                            KapsamDisiTutar = item.OutOfScopeAmount.ToString(),
                            KatilimTutari = item.ParticipationAmount.ToString(),
                            MuafiyetTutari = item.ExemptionAmount.ToString(),
                            OdenecekTutar = item.Paid.ToString(),
                            SgkTutari = item.SgkAmount.ToString(),
                            TeminatKodu = item.CoverageCode,
                            TeminatPaketNo = item.CoveragePackageNo,
                            TeminatVaryasyon = item.CoverageVariation
                        };
                        islemList.Add(islem);
                    }
                }

                SaglikProvizyonKaydetParam param = new SaglikProvizyonKaydetParam
                {
                    CikisTarihi = cl.ExitDate.ToString(),
                    DoktorAd = cl.DoctorName,
                    DoktorDiplomaNo = cl.DoctorDiplomaNo,
                    DoktorSaglikUnvan = cl.DoctorTitle,
                    DoktorSoyad = cl.DoctorLastName,
                    DoktorTCNo = cl.DoctorTCNO.ToString(),
                    EntegrasyonProvizyonId = cl.ClaimId.ToString(),
                    FaturaBilgiList = fatList.ToArray(),
                    GrupPoliceNo = cl.PolicyNo.ToString(),
                    HasarIlcesi = cl.ClaimCounty,
                    HasarIli = cl.ClaimCity,
                    IhbarTarihi = ((DateTime)cl.NoticeDate).ToString("dd/MM/yyyy"),
                    KurumAdi = cl.ProviderName,
                    IslemListesi = islemList.ToArray(),
                    KurumIlceKodu = cl.ProviderCountyCode,
                    KurumIlKodu = cl.ProviderCityCode,
                    KurumKimlikNo = cl.ProviderIdentityNo.ToString(),
                    MusteriId = cl.InsuredId.ToString(),
                    OdemeYeri = cl.PaymentMethodText,
                    OlayTarihi = ((DateTime)cl.ClaimDate).ToString("dd/MM/yyyy"),
                    PoliceNo = cl.PolicyNo.ToString(),
                    ProvizyonDurumu = cl.StatusText,
                    ProvizyonId = cl.CompanyClaimId,
                    ProvizyonNedeni = "HASTALIK",
                    ProvizyonNotu = cl.EpikrizNote,
                    RetNedeni = cl.ReasonText,
                    Sikayet = cl.Complaint,
                    TedaviGorenSigortaliAdi = cl.InsuredFirstName,
                    TedaviGorenSigortaliSoyadi = cl.InsuredLastName,
                    TedaviGorenSigortaliKimlikNo = cl.InsuredIdentityNo.ToString(),
                    TeminatGrubu = findYatarak ? "Yatarak" : "Ayakta",
                    YatisTarihi = cl.EntranceDate.ToString(),
                    YenilemeNo = cl.RenewalNo.ToString(),
                    TaniListesi = taniList.ToArray()
                };
                #endregion

                #region Operation
                log.Request = param.ToXML(true);
                CreateWaitingLog(log);

                HasarServisleri dogaClSrv = new HasarServisleri();

                SaglikProvizyonKaydetResult dogaResult = dogaClSrv.ProvKaydet(param);
                if (!dogaResult.ProvizyonId.IsInt64() || long.Parse(dogaResult.ProvizyonId) < 1)
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "HATA";
                    response.ErrorList = new string[] { dogaResult.SonucAciklama };

                    CreateFailedLog(log, response.ToXML(true), response.SonucAciklama);
                    return response;
                }
                #endregion

                #region OK
                Claim claim = new GenericRepository<Claim>().FindById(ClaimId);
                claim.CompanyClaimId = long.Parse(dogaResult.ProvizyonId);
                var spResClaim = new GenericRepository<Claim>().Insert(claim);


                response.SonucAciklama = "OK";
                response.SonucKod = "1";
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.SonucKod = "0";
                response.SonucAciklama = $"Bir Hata Oluştu - {ex.Message}";

                CreateFailedLog(log, response.ToXML(true), response.SonucAciklama);
                #endregion
            }
            return response;

        }

        public DogaIcmalAktarRes IcmalAktar(string apiKod, long PayrollId)
        {
            DogaIcmalAktarRes response = new DogaIcmalAktarRes();

            var currentMethod = MethodBase.GetCurrentMethod();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                WsRequestDate = DateTime.Now,
                COMPANY_ID = DogaCompanyId,
                PayrollId = PayrollId,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Auth
                if (ApiCodeIsValid(apiKod) <= 0)
                {
                    response.SonucAciklama = "HATA";
                    response.SonucKod = "Giriş Başarısız! APİCODE'u Kontrol Ediniz";

                    CreateFailedLog(log, response.ToXML(true), response.SonucAciklama);
                    return response;
                }
                #endregion

                #region Fill Data
                PayrollDto payroll = claimServiceClient.PayrollToDoga(PayrollId);
                if (payroll == null)
                {
                    response.SonucAciklama = "HATA";
                    response.SonucKod = "0";
                    response.ErrorList = new string[] { "İCMAL BİLGİSİ BULUNAMADI" };

                    CreateFailedLog(log, response.ToXML(true), response.SonucAciklama);
                    return response;
                }
                #endregion
                log.Request = payroll.ToXML(true);
                #region Mapping
                SaglikPaketKaydetParamV2 saglikPackageParam = new SaglikPaketKaydetParamV2
                {
                    IcmalNumarasi = payroll.PayrollId.ToString(),
                    Tarih = payroll.Date.ToString(),
                    KurumKimlikNo = payroll.ProviderIdentityNo.ToString(),
                    KurumTipi = payroll.ProviderTypeText,
                    Iban = payroll.ProviderIban
                };

                List<SaglikHasarBilgisiV2> hasarBilgiV2 = new List<SaglikHasarBilgisiV2>();
                if (payroll.ClaimList != null)
                {
                    foreach (ClaimDto claimDto in payroll.ClaimList)
                    {
                        SaglikHasarBilgisiV2 hsbv2 = new SaglikHasarBilgisiV2
                        {
                            FaturaNo = claimDto.Bill.No,
                            Adi = claimDto.InsuredFirstName,
                            Soyadi = claimDto.InsuredLastName,
                            Iban = claimDto.InsuredIban,
                            FaturaTarihi = claimDto.Bill.Date.ToString(),
                            FaturaTutari = claimDto.Bill.TotalPaid.ToString(),
                            KimlikNo = claimDto.InsuredIdentityNo.ToString(),
                            HasarNo = claimDto.CompanyClaimId,
                            OdemeTarihi = claimDto.PaymentDate.ToString()
                        };
                        hasarBilgiV2.Add(hsbv2);
                    }
                    saglikPackageParam.HasarListesi = hasarBilgiV2.ToArray();
                }


                #endregion
                log.Request = saglikPackageParam.ToXML(true);
                #region Operation
                HasarServisleri dogaClSrv = new HasarServisleri();
                dogaClSrv.Url = "https://portal.dogasigorta.com/WebServisleri/HasarServisleri.asmx";
                SaglikPaketKaydetResult PackageResult = dogaClSrv.PaketKaydetV2(saglikPackageParam);
                if (PackageResult.PaketId <= 0)
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "HATA";
                    response.ErrorList = new string[] { "Paket Doğa Servisine Aktarılamadı" };

                    CreateFailedLog(log, response.ToXML(true), response.SonucAciklama);
                    return response;
                }
                #endregion

                #region OK
                Payroll payrll = new GenericRepository<Payroll>().FindBy($"ID={PayrollId}", fetchDeletedRows: true).FirstOrDefault();
                payrll.CompanyPayrollNo = PackageResult.PaketId.ToString();
                new GenericRepository<Payroll>().Insert(payrll);

                response.SonucAciklama = "OK";
                response.SonucKod = "1";
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.SonucKod = "0";
                response.SonucAciklama = $"Bir Hata Oluştu - {ex.Message}";

                CreateFailedLog(log, response.ToXML(true), response.SonucAciklama);
                #endregion
            }
            return response;
        }

        public DogaIcmalAktarRes IcmalAktarList(string apiKod)
        {
            DogaIcmalAktarRes response = new DogaIcmalAktarRes();

            var currentMethod = MethodBase.GetCurrentMethod();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                WsRequestDate = DateTime.Now,
                COMPANY_ID = DogaCompanyId,
            };
            //CreateIncomingLog(log);
            #endregion

            try
            {
                #region Auth
                if (ApiCodeIsValid(apiKod) <= 0)
                {
                    response.SonucAciklama = "HATA";
                    response.SonucKod = "Giriş Başarısız! APİCODE'u Kontrol Ediniz";

                    CreateFailedLog(log, response.ToXML(true), response.SonucAciklama);
                    return response;
                }
                #endregion

                List<Payroll> payrollList = new GenericRepository<Payroll>().FindBy($"STATUS='{((int)PayrollStatus.Onay)}' AND COMPANY_ID={DogaCompanyId} AND COMPANY_PAYROLL_ID IS NULL AND COMPANY_PAYROLL_NO IS NULL AND OLD_PAYROLL_ID IS NULL ", fetchDeletedRows: true);
                foreach (var item in payrollList)
                {
                    #region Fill Data
                    PayrollDto payroll = claimServiceClient.PayrollToDoga(item.Id);
                    if (payroll == null)
                    {
                        response.SonucAciklama = "HATA";
                        response.SonucKod = "0";
                        response.ErrorList = new string[] { "İCMAL BİLGİSİ BULUNAMADI" };

                        CreateFailedLog(log, response.ToXML(true), response.SonucAciklama);
                        break;
                    }
                    #endregion

                    #region Mapping
                    SaglikPaketKaydetParamV2 saglikPackageParam = new SaglikPaketKaydetParamV2
                    {
                        IcmalNumarasi = payroll.PayrollId.ToString(),
                        Tarih = payroll.Date.ToString(),
                        KurumKimlikNo = payroll.ProviderIdentityNo.ToString(),
                        KurumTipi = payroll.ProviderTypeText,
                        Iban = payroll.ProviderIban
                    };

                    List<SaglikHasarBilgisiV2> hasarBilgiV2 = new List<SaglikHasarBilgisiV2>();
                    if (payroll.ClaimList != null)
                    {
                        foreach (ClaimDto claimDto in payroll.ClaimList)
                        {
                            SaglikHasarBilgisiV2 hsbv2 = new SaglikHasarBilgisiV2
                            {
                                FaturaNo = claimDto.Bill.No,
                                Adi = claimDto.InsuredFirstName,
                                Soyadi = claimDto.InsuredLastName,
                                Iban = claimDto.InsuredIban,
                                FaturaTarihi = claimDto.Bill.Date.ToString(),
                                FaturaTutari = claimDto.Bill.TotalPaid.ToString(),
                                KimlikNo = claimDto.InsuredIdentityNo.ToString(),
                                HasarNo = claimDto.CompanyClaimId,
                                OdemeTarihi = claimDto.PaymentDate.ToString()
                            };
                            hasarBilgiV2.Add(hsbv2);
                        }
                        saglikPackageParam.HasarListesi = hasarBilgiV2.ToArray();
                    }
                    #endregion

                    #region Operation
                    try
                    {
                        log.Request = saglikPackageParam.ToXML(true);
                        log.PayrollId = payroll.PayrollId;
                        HasarServisleri dogaClSrv = new HasarServisleri();
                        dogaClSrv.Url = "https://portal.dogasigorta.com/WebServisleri/HasarServisleri.asmx";
                        SaglikPaketKaydetResult PackageResult = dogaClSrv.PaketKaydetV2(saglikPackageParam);
                        if (PackageResult.PaketId <= 0)
                        {
                            response.SonucKod = "0";
                            response.SonucAciklama = "HATA";
                            response.ErrorList = new string[] { "Paket Doğa Servisine Aktarılamadı" };

                            CreateFailedLog(log, response.ToXML(true), response.SonucAciklama);
                            break;
                        }

                        #region OK
                        item.CompanyPayrollNo = PackageResult.PaketId.ToString();
                        new GenericRepository<Payroll>().Insert(item);

                        response.SonucAciklama = "OK";
                        response.SonucKod = "1";
                        CreateOKLog(log, response.ToXML(true));
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        response.SonucKod = "0";
                        response.SonucAciklama = "HATA";
                        response.ErrorList = new string[] { ex.Message };

                        CreateFailedLog(log, response.ToXML(true), response.SonucAciklama);
                        break;
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.SonucKod = "0";
                response.SonucAciklama = $"Bir Hata Oluştu - {ex.Message}";

                CreateFailedLog(log, response.ToXML(true), response.SonucAciklama);
                #endregion
            }
            return response;
        }

        #region Other Method
        private long ApiCodeIsValid(string apiCode)
        {
            long id = 0;

            try
            {
                string ApiCodeCompany = ConfigurationManager.AppSettings["ApiCode" + DogaCompanyId];

                if (ApiCodeCompany == apiCode)
                    id = DogaCompanyId;
                //string whereCondition = (DogaCompanyId > 0 ? $"COMPANY_ID = {DogaCompanyId} AND " : "") + ($" KEY = '{Constants.AppKeys.ApiCode}' AND VALUE = '{apiCode}' AND SYSTEM_TYPE='{((int)SystemType.WS).ToString()}'");
                //V_CompanyParameter resultCompanyParam = new GenericRepository<V_CompanyParameter>().FindBy(whereCondition, orderby: "").FirstOrDefault();
                //if (resultCompanyParam == null) return id;
                //if (resultCompanyParam.Value == apiCode)
                //    id = (long)resultCompanyParam.CompanyId;
            }
            catch
            {
                id = 0;
            }
            return id;
        }

        private V_Package GetPackagebyNo(string packageNo)
        {
            V_Package package = new GenericRepository<V_Package>().FindBy($"PACKAGE_NO={packageNo} AND COMPANY_ID={DogaCompanyId}", orderby: "").FirstOrDefault();
            return package;
        }

        private string GetIndividualType(string yakinlik)
        {
            string individualType = ((int)IndividualType.COCUK).ToString();
            if (!yakinlik.IsNull())
            {
                if (yakinlik.ToLower() == "eşi")
                {
                    individualType = ((int)IndividualType.ES).ToString();
                }
                else if (yakinlik.ToLower() == "kendisi" || yakinlik.ToLower() == "sigortali")
                {
                    individualType = ((int)IndividualType.FERT).ToString();
                }
            }
            return individualType;
        }

        private long? GetCityAndCountybyCode(string cityCode = "", string countyCode = "")
        {
            long? id = null;

            if (!cityCode.IsNull())
            {
                City city = new GenericRepository<City>().FindBy($"CODE={cityCode}").FirstOrDefault();
                id = city != null ? (long?)city.Id : null;
                return id;
            }

            if (!countyCode.IsNull())
            {
                County county = new GenericRepository<County>().FindBy($"CODE={countyCode}").FirstOrDefault();
                id = county != null ? (long?)county.Id : null;
                return id;
            }

            return id;
        }

        private string GetPackageNo(XmlDocument xmlDoc)
        {
            string imecePaketNo = xmlDoc.SelectSingleNode("police/Alanlar/_ImecePaket").InnerXml;

            if (imecePaketNo.IsNull())
            {
                string grup = xmlDoc.SelectSingleNode("police/Alanlar/_Grup").InnerXml;
                string paket = xmlDoc.SelectSingleNode("police/Alanlar/_Paket").InnerXml;
                string OpsiyonelTeminat = xmlDoc.SelectSingleNode("police/Alanlar/_OpsiyonelTeminat").InnerXml;
                if (paket.IsNull())
                {
                    return imecePaketNo = null;
                }
                else
                {
                    switch (paket)
                    {
                        case "YAY6":
                            if (!grup.IsNull() && grup != "0")
                            {
                                return imecePaketNo = "901";
                            }
                            else
                            {
                                if (OpsiyonelTeminat == "H" || OpsiyonelTeminat.IsNull())
                                {
                                    return imecePaketNo = "889";
                                }
                                else
                                {
                                    return imecePaketNo = "733";
                                }
                            }
                        case "YAY8":
                            if (!grup.IsNull() && grup != "0") //Grup
                            {
                                if (OpsiyonelTeminat == "H" || OpsiyonelTeminat.IsNull())
                                {
                                    return imecePaketNo = "897";
                                }
                                else
                                {
                                    return imecePaketNo = "893";
                                }
                            }
                            else //Ferdi
                            {
                                if (OpsiyonelTeminat == "H" || OpsiyonelTeminat.IsNull())
                                {
                                    return imecePaketNo = "890";
                                }
                                else
                                {
                                    return imecePaketNo = "887";
                                }
                            }
                        case "Yatarak":
                            if (!grup.IsNull() && grup != "0") //Grup
                            {
                                if (OpsiyonelTeminat == "H" || OpsiyonelTeminat.IsNull())
                                {
                                    return imecePaketNo = "899";
                                }
                                else
                                {
                                    return imecePaketNo = "895";
                                }
                            }
                            else //Ferdi
                            {
                                if (OpsiyonelTeminat == "H" || OpsiyonelTeminat.IsNull())
                                {
                                    return imecePaketNo = "891";
                                }
                                else
                                {
                                    return imecePaketNo = "888";
                                }
                            }
                        default:
                            return imecePaketNo = null;
                    }
                }
            }
            return imecePaketNo;
        }
        #endregion
    }
}
