﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.RxMedia.Result
{
    public class CommonResult/*<T> where T : class*/
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public bool Success { get; set; } = false;
    }
}
