﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.RxMedia.Result
{
    [Serializable]
    public class MedicineInfo : CommonResult
    {
        /// <summary>
        /// Barkod
        /// </summary>
        public string Barcode { get; set; }
        /// <summary>
        /// Karekod
        /// </summary>
        public string QRcode { get; set; } //karekod
        /// <summary>
        /// İlaç ismi
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// İlaç Tipi
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// Bileşim kod
        /// </summary>
        public string CompCode { get; set; }
        /// <summary>
        /// Adet
        /// </summary>
        public int Piece { get; set; }
        /// <summary>
        /// Miktar
        /// </summary>
        public decimal Quantity { get; set; }
        /// <summary>
        /// Birim
        /// </summary>
        public string Birim { get; set; }
        /// <summary>
        /// Toplam
        /// </summary>
        public decimal TotalPrice { get; set; }
        public string Guid { get; set; }
        /// <summary>
        /// Toplam Tutar
        /// </summary>
        public decimal GrandTotal { get; set; }
    }
}
