﻿using Protein.Data.ExternalServices.RxMedia.Result;
using Protein.Data.ExternalServices.RXMediaPharmaPROD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Services.Protocols;

namespace Protein.Data.ExternalServices.RxMedia
{
    public class Helper
    {
        private string WSUsername = Config.RxUserName;
        private string WSPassword = Config.RxPassword;


        public MedicineInfo GetMedicineInfoByBarcode(string barcode)
        {
            MedicineInfo mInfo = new MedicineInfo();

            try
            {
                RxMediaPharmaWS client = new RxMediaPharmaWS();
              
                userpass upass = new userpass
                {
                    username = WSUsername,
                    pass = WSPassword
                };

                mustahzarbul_barkod_Request reqIlacBul = new mustahzarbul_barkod_Request
                {
                    barkodlar = new string[] { barcode },
                    user = upass
                };

                mustahzarbul_barkod_Response respIlacBilgi = client.mustahzarbul_barkod(reqIlacBul);

                mustahzar[] respArray = respIlacBilgi.yanit;

                if (respArray == null)
                {
                    mInfo.Code = 99; //İlaç bulunamadı;
                    mInfo.Message = "İlaç bulunamadı!";
                    return mInfo;
                }

                //Fiyatı sorgulanıyor.
                mustahzar_fiyatlar_Request reqFiyat = new mustahzar_fiyatlar_Request
                {
                    user = upass,
                    tarih = string.Format("{0}-{1}-{2}", DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day),
                    ambalaj = respArray[0].ambalaj,
                    barkod = respArray[0].barkod
                };
                mustahzar_fiyatlar_Response response = client.mustahzar_fiyatlar(reqFiyat);
                mustahzar_fiyat[] fiyatRespArray = response.yanit;
                if (fiyatRespArray == null)
                {
                    mInfo.Code = 98; //İlaç bulunamadı;
                    mInfo.Message = "İlaç fiyatı bulunamadı!";
                    return mInfo;
                }

                mInfo.Name = respArray[0].tanim;
                mInfo.Barcode = barcode;
                mInfo.TotalPrice = Math.Round(Convert.ToDecimal(fiyatRespArray[0].fiyat), 2);
                mInfo.Message = "OK"; mInfo.Success = true;
                return mInfo;
            }
            catch (SoapHeaderException ex) { mInfo = new MedicineInfo { Message = ex.Code + " " + ex.Actor, Code = 10 }; return mInfo; }

        }
    }
}
