﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Protein.Data.ExternalServices.RxMedia
{
    public class Config
    {
        public static string RxUserName { get { return ConfigurationManager.AppSettings["RxUserName"]; } }
        public static string RxPassword { get { return ConfigurationManager.AppSettings["RxPassword"]; } }

    }
}
