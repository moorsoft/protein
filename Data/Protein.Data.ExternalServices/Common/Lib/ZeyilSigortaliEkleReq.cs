﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    [Serializable]
    public class ZeyilSigortaliEkleReq
    {
        public PolicePK PoliceNo { get; set; }
        public DateTime ZeylBaslamaTarih { get; set; }
        public DateTime ZeylBitisTarih { get; set; }
        public DateTime ZeylTanzimTarih { get; set; }
        public string ZeylNo { get; set; }
        public string DovizKod { get; set; }
        public decimal DovizKur { get; set; }
        public List<Sigortali> SigortaliListesi { get; set; }
    }
}
