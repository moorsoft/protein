﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class CompanyParam
    {
        public string ApiCode { get; set; }
        public string ApiUrl { get; set; }
        public string ApiCompanyCode { get; set; }
        public string ApiPassword { get; set; }
        public string ApiUserName { get; set; }
    }
}
