﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    [Serializable]
    public class PaketOdemeBildirimReq
    {
        public int Paket_No { get; set; }
        public DateTime? Odeme_Tarihi { get; set; }
    }
}
