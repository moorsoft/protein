﻿using Protein.Data.ExternalServices.InsuranceCompanies.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class ClaimUpdateReq 
    {
        public string ClaimCompanyId{ get; set; }
        public long ClaimId { get; set; }
        public string CompanyCode { get; set; }
    }
}
