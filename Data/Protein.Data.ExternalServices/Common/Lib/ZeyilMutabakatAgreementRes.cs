﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    class ZeyilMutabakatAgreementRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public int ToplamZeyilAdedi { get; set; }

        public List<ZeyilListTest> ZeyilListesi { get; set; }
    }
    public class ZeyilListTest
    {
        public int No { get; set; }
        public string Tipi { get; set; }
        public int SigortaliAdeti { get; set; }
        public decimal Prim { get; set; }

    }
}
