﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class mutabakatAgreementIcmalDetayRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public List<TazminatDetayAgreement> TazminatListesi { get; set; }
    }
    public class TazminatDetayAgreement
    {
        public string TazminatNo { get; set; }
        public decimal SirketTutar { get; set; }
        public string HasarDurum { get; set; }
    }
}
