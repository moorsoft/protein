﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class AcenteBilgi
    {
        public string SirketKod { get; set; }
        public string AcenteKod { get; set; }
        public string AcenteUnvan { get; set; }
        public string AcenteTip { get; set; }
        public string AcenteBolgeKod { get; set; }
        public string AcenteBolgeAd { get; set; }
        public DateTime? AcilisTarih { get; set; }
        public DateTime? KapanisTarih { get; set; }
        public string AcenteDurumu { get; set; }
        public string UstAcenteKod { get; set; }
        public string LevhaNo { get; set; }
        public Int64 TicaretSicilNo { get; set; }
        public string MersisNo { get; set; }
        public string WebAdres { get; set; }
        public string EMail { get; set; }
        public string Telefon { get; set; }
        public string YetkiliAd { get; set; }
        public string YetkiliSoyad { get; set; }
        public string YetkiliCepTel { get; set; }
        public Adres Adres { get; set; }
    }
}
