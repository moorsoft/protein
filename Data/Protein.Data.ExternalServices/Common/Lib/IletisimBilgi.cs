﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    [Serializable]
    public class IletisimBilgi
    {
        public string TelefonNo { get; set; }
        public string Email { get; set; }
        public string CepTel { get; set; }
    }
}
