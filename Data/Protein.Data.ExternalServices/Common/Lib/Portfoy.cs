﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Protein.Data.ExternalServices.Common.Lib
{
    [Serializable]
    public class GercekKisi
    {
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Cinsiyet { get; set; }
        public DateTime? DogumTarihi { get; set; }
        public string DogumYeri { get; set; }
        public string BabaAd { get; set; }
        public string Uyruk { get; set; }
        public string VKN { get; set; }
        public string PasaportNo { get; set; }
        public string TCKN { get; set; }


    }


    [Serializable]
    public class TuzelKisi
    {
        public string VKN { get; set; }
        public string SirketAd { get; set; }

    }
}
