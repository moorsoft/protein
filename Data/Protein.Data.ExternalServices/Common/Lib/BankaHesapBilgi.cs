﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    [Serializable]
    public class BankaHesapBilgi
    {
        public string BankaKod { get; set; }
        public string BankaAd { get; set; }
        public string SubeKod { get; set; }
        public string SubeAd { get; set; }
        public string HesapSahip { get; set; }
        public string HesapNo { get; set; }
        public string Iban { get; set; }
    }
}
