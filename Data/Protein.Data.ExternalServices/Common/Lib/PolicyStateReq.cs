﻿using Protein.Data.ExternalServices.InsuranceCompanies.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class PolicyStateReq 
    {
        public string AuthenticationKey { get; set; }
        public string InsuredNo { get; set; }
        public string PolicyNo { get; set; }
        public DateTime IncidentDate { get; set; }
        public string CompanyCode { get; set; }
        public long RenewalNo { get; set; }
    }
}
