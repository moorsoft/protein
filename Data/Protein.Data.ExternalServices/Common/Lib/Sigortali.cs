﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Protein.Data.ExternalServices.Common.Lib
{
    [Serializable]
    public class Sigortali
    {
        /// <summary>
        /// SirketMusteriKod
        /// </summary>
        public string SigortaliNo { get; set; }
        public DateTime? IlkKayitTarihi { get; set; }
        public DateTime? IlkSigortalanmaTarihi { get; set; }
        public string PaketNo { get; set; }
        public string BireyTip { get; set; }

        public decimal PrimTL { get; set; }
        public decimal PrimDoviz { get; set; }

        public string AileNo { get; set; }
        public GercekKisi KisiBilgi { get; set; }
        public AdresBilgi Adres { get; set; }
        public IletisimBilgi Iletisim { get; set; }
        public BankaHesapBilgi Banka { get; set; }
    }
}
