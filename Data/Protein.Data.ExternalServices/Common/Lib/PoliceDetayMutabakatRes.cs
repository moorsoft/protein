﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class PoliceDetayMutabakatRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public string SirketKod { get; set; }
        public Int64? PoliceNo { get; set; }
        public string YenilemeNo { get; set; }
        public DateTime TransferTarihi { get; set; }
        public decimal Prim { get; set; }
        public int ToplamSigortaliAdeti { get; set; }
        public List<SigortaliListesi> Sigortali {get;set;}

        
    }
    public class SigortaliListesi
    {
        public string TCKN { get; set; }
        public string BireyTipi { get; set; }
        public decimal Prim { get; set; }
        public string SigortaliNo { get; set; }
        public DateTime? IlkKayitTarihi { get; set; }
        public DateTime? IlkSigortalanmaTarihi { get; set; }
        public string PaketNo { get; set; }
        public GercekKisi KisiBilgi { get; set; }
    }
}
