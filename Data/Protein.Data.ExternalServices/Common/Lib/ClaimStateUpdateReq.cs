﻿using Protein.Data.ExternalServices.InsuranceCompanies.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class ClaimStateUpdateReq 
    {
        public long ClaimId { get; set; }
    }
}
