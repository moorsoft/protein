﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    [Serializable]
   public class ProvizyonMutabakatRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public int ToplamAdet { get; set; }
        public decimal ToplamSirketTutar { get; set; }
        public List<TazminatDurum> TazminatDurumListesi { get; set; }

    }

  
}
