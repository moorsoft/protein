﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class PolicyAgreementReq
    {
        public int SirketKod { get; set; }
        public DateTime TanzimBaslangic { get; set; }
        public DateTime TanzimBitis { get; set; }
    }
}
