﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
   public class ZeyilMutabakatDetayAgreementRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public string Tipi { get; set; }
        public int SigortaliAdedi { get; set; }
        public decimal Prim { get; set; }

        public List<SigortaliListTest> SigortaliListTest { get; set; }
    }
    public class SigortaliListTest
    {

        public Int64? TCKN { get; set; }
        public string BireyTipi { get; set; }
        public decimal Prim { get; set; }

    }

}
