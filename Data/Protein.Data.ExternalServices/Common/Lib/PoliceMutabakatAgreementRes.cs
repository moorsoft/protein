﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class PoliceMutabakatAgreementRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public int ToplamPoliceAdedi { get; set; }
        public decimal ToplamPolicePrimi { get; set; }
        public List<MutabakatPoliceListesiTest> MutabakatPoliceListesiTest { get; set; }
    }

    public class MutabakatPoliceListesiTest
    {
        public Int64? PoliceNo { get; set; }
        public string YenilemeNo { get; set; }
        public DateTime TanzimTarihi { get; set; }
        public string SigortaliAdedi { get; set; }
        public decimal Prim { get; set; }
    }
}
