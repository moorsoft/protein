﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    [Serializable]
    public class PoliceIptalReq
    {
        public PolicePK PoliceNo { get; set; }
        public DateTime? ZeylBaslamaTarih { get; set; }
        public string ZeylNo { get; set; }
        //public DateTime ZeylBitisTarih { get; set; }
        public DateTime? ZeylTanzimTarih { get; set; }
        public decimal? IptalPrimi { get; set; } ///toplam prim

        // MEB iptal? Kismi İptal?
        // IADE PRIM TUTAR??
    }
}
