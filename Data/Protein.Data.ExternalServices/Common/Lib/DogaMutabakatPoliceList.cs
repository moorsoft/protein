﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
   public class DogaMutabakatPoliceList
    {
        public string AcenteNo { get; set; }
        public string Brans { get; set; }
        public string policeNo { get; set; }
        public string TecditNo { get; set; }
        public string ZeyilNo { get; set; }
        public string IptalKayit { get; set; }
        public string TanzimTarihi { get; set; }
    }
}
