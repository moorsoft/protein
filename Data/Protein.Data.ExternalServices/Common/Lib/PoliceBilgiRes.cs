﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    [Serializable]
    public class PoliceBilgiRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public long TpaPoliceNo { get; set; }
        public string[] ValidationErrors { get; set; }
        public SigortaliKullanım[] KullanımBilgi { get; set; }
    }

    public class SigortaliKullanım
    {
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Yakinlik { get; set; }
        public string ExMusteriNo { get; set; }
        public SigortaliKullanımTeminat[] TeminatListesi { get; set; }
    }

    public class SigortaliKullanımTeminat
    {
        public string TeminatAd;
        public string Uygulama;
        public string Toplam;
        public string Kullanim;
        public string Kalan;
    }
}
