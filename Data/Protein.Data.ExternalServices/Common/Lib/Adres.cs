﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    [Serializable]
    public class Adres
    {
        public string IlKod { get; set; }
        public string IlceKod { get; set; }
        public string Mahalle { get; set; }
        public string Sokak { get; set; }
        public string BinaNo { get; set; }
        public string BinaAd { get; set; }
        public string DaireNo { get; set; }
        public string PostaKod { get; set; }
        public string AdresTarif { get; set; }
        public string UlkeKod { get; set; }
    }
}
