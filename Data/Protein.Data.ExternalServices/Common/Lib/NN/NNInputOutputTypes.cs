﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Protein.Data.ExternalServices.Common.Lib.NN
{
    public class NNInputOutputTypes
    {
        public class UserInfo
        {
            public Boolean userNameSpecified { get { return true; } }
            public string UserName { get { return "USERWSINTEGRATION"; }  }
            public Boolean passwordSpecified { get { return true; } }
            public string Password { get { return ConfigurationManager.AppSettings["NN_PASSWORD"]; } } //TEST : DUWS15CU  CANLI : FXHMN24XK04
        }

        public class NNHakSahiplikReq : UserInfo
        {
            #region Private
            private string _Kurum = "";
            private string _OlayTarihi = "";
            private string _TcKimlikNo = "";
            private string _PasaportNo = "";
            private string _AyaktaYatarak = "";
            #endregion
            
            public Boolean kurumSpecified { get; set; }
            public string Kurum { get { return _Kurum; } set { _Kurum = value; this.kurumSpecified = !string.IsNullOrEmpty(value) ? true : false; } }
            public Boolean olayTarihiSpecified { get; set; }
            public string OlayTarihi { get { return _OlayTarihi; } set { _OlayTarihi = value; this.olayTarihiSpecified = !string.IsNullOrEmpty(value) ? true : false; } }
            public Boolean tcKimlikNoSpecified { get; set; }
            public string TcKimlikNo { get { return _TcKimlikNo; } set { _TcKimlikNo = value; this.tcKimlikNoSpecified = !string.IsNullOrEmpty(value) ? true : false; } }
            public Boolean pasaportNoSpecified { get; set; }
            public string PasaportNo { get { return _PasaportNo; } set { _PasaportNo = value; this.pasaportNoSpecified = !string.IsNullOrEmpty(value) ? true : false; } }
            public Boolean ayaktanYatarakSpecified { get; set; }
            public string AyaktanYatarak { get { return _AyaktaYatarak; } set { _AyaktaYatarak = value; this.ayaktanYatarakSpecified = !string.IsNullOrEmpty(value) ? true : false; } }
        }

        public class NNSetProvisionStatusReq : UserInfo
        {
            #region Private
            private string _ProvisionNumber = "";
            private string _NewStatu = "";
            private string _ReasonCode = "";
            private string _ReasonDesc = "";
            #endregion

            public Boolean ProvisionNumberSpecified { get; set; }
            public string ProvisionNumber { get { return _ProvisionNumber; } set { _ProvisionNumber = value; this.ProvisionNumberSpecified = !string.IsNullOrEmpty(value) ? true : false; } }
            public Boolean NewStatuSpecified { get; set; }
            public string NewStatu { get { return _NewStatu; } set { _NewStatu = value; this.NewStatuSpecified = !string.IsNullOrEmpty(value) ? true : false; } }
            public Boolean ReasonCodeSpecified { get { return false; } }
            public string ReasonCode { get { return ""; }}
            public Boolean ReasonDescSpecified { get; set; }
            public string ReasonDesc { get { return _ReasonDesc; } set { _ReasonDesc = value; this.ReasonDescSpecified = !string.IsNullOrEmpty(value) ? true : false; } }
        }

        public class NNUpdateSagmerPolicyInfoReq : UserInfo
        {
            public string PolicyNumber { get; set; }
            public string PolicyOrder { get; set; }
            public string TPAPolicyNumber { get; set; }
            public string SagmerPolicyNumber { get; set; }
            public string SagmerSendDate { get; set; }
            public string SagmerErrorMessage { get; set; }
            public string SagmerSendStatus { get; set; }
        }

        public class NNTazminatGirisReq : UserInfo
        {
            public Int64 claimId { get; set; }
        }

        public class TaniBilgi
        {
            public string TaniTipi { get { return "2"; } }
            public string TaniKodu { get; set; }
            public string TaniAdi { get; set; }
        }

        public class TeminatTalep
        {
            public List<HizmetBlgs> HizmetBilgileri { get; set; }
            public string TeminatKodu { get; set; }
            public string TalepTutar { get; set; }
            public string OnayTutar { get; set; }
        }

        public class HizmetBlgs
        {
            public string HizmetKodu { get{ return "IMC"; } }
            public string HizmetAdi { get { return "IMECE"; } }
            public string HizmetSgkTutar { get; set; }
            public string HizmetTalepTutar { get; set; }
            public string HizmetOnayTutar { get; set; }
            public string SonucAciklama { get; set; }
            public string DrTescilNo { get { return ""; } }
            public string Brans { get { return ""; } }
        }

        //#region Output
        //public class OutputType
        //{
        //    public string SonucKodu { get; set; }
        //    public string SonucAciklama { get; set; }
        //}
        //public class ServiceResponse<T> : OutputType
        //{
        //    public bool IsSuccess { get { return (this.SonucKodu == "1" ? true : false); } }
        //    public T Data { get; set; }
        //}

        //#endregion

        //[DisplayName("SigortaliHakSorgulaResponse|SigortaliHakSorgulaResult")]
        //public class SigortaliHakSorgulaResult
        //{
        //    public string SonucAciklamaDetay { get; set; }
        //    public string SigortaliAdSoyad { get; set; }
        //    public List<Police> PoliceBilgileri { get; set; }
        //}

        //[Serializable]
        //public class Police
        //{
        //    public string PoliceNo { get; set; }
        //    public string PoliceBaslangicTarihi { get; set; }
        //    public string PoliceBitisTarihi { get; set; }
        //    public List<TeminatBilgileri> TanimliTeminatBilgileri { get; set; }
        //    public List<MuafiyetBilgileri> TanimliMuafiyetBilgileri { get; set; }
        //}
        //[Serializable]
        //public class MuafiyetBilgileri
        //{
        //    public string Test { get; set; }
        //}
        //[Serializable]
        //public class TeminatBilgileri
        //{
        //    public string TeminatKodu { get; set; }
        //    public string TeminatAdi { get; set; }
        //    public string TeminatAciklama { get; set; }
        //}
        //[Serializable]
        //public class CommonInput
        //{
        //    public string userNameSpecified { get; set; } = "1";
        //    [Required]
        //    public string UserName { get; set; }
        //    public string passwordSpecified { get; set; } = "1";
        //    [Required]
        //    public string Password { get; set; }
        //    public string kurumSpecified { get; set; } = "1";
        //    [Required]
        //    public string Kurum { get; set; }
        //}

        //[DisplayName("SigortaliHakSorgula|Sigortali")]
        //[Serializable]
        //public class HakSahiplikInput : CommonInput
        //{
        //    #region Private
        //    private string _tcNo = "";
        //    private string _olayTar = "";
        //    private string _pasaportNo = "";
        //    private string _aykYtrk = "";
        //    #endregion
        //    public string olayTarihiSpecified { get; set; }

        //    public string OlayTarihi { get { return _olayTar; } set { _olayTar = value; this.olayTarihiSpecified = !string.IsNullOrEmpty(value) ? true : false; } }
        //    public string tcKimlikNoSpecified { get; set; }

        //    public string TcKimlikNo { get { return _tcNo; } set { _tcNo = value; this.tcKimlikNoSpecified = !string.IsNullOrEmpty(value) ? true : false; } }
        //    public string pasaportNoSpecified { get; set; }
        //    public string PasaportNo { get { return _pasaportNo; } set { _pasaportNo = value; this.pasaportNoSpecified = !string.IsNullOrEmpty(value) ? true : false; } }
        //    public string ayaktanYatarakSpecified { get; set; }
        //    public string AyaktanYatarak
        //    {
        //        get { return _aykYtrk; }
        //        set { _aykYtrk = value; this.ayaktanYatarakSpecified = !string.IsNullOrEmpty(value) ? true : false; }
        //    }
        //}




        //[DisplayName("SigortaliHakSorgulaResponse|SigortaliHakSorgulaResult")]
        //public class HakSahiplikOutput : OutputType
        //{
        //    public string SonucAciklamaDetay { get; set; }
        //    public string SigortaliAdSoyad { get; set; }
        //    public List<PoliceOutput> PoliceBilgileri { get; set; }
        //}
        //[DisplayName("IcmalGirisResponse|IcmalGirisResult")]
        //public class IcmalGirisOutput : OutputType
        //{
        //    public string PaketNo { get; set; }
        //}
        //[DisplayName("TamamlayiciSaglikProvizyonIptalResponse|TamamlayiciSaglikProvizyonIptalResult")]
        //public class TamamlayiciSaglikProvizyonIptalOutput : OutputType
        //{
        //    public string ProvizyonNo { get; set; }
        //}
        //[DisplayName("IcmalTazminatCikarmaResponse|IcmalTazminatCikarmaResult")]
        //public class IcmalTazminatCikarmaOutput : OutputType
        //{

        //}
        //public class PoliceOutput
        //{
        //    public string PoliceNo { get; set; }
        //    public string PoliceBaslangicTarihi { get; set; }
        //    public string PoliceBitisTarihi { get; set; }
        //    //public string UrunKodu { get; set; }
        //    //public string Tarife { get; set; }
        //    //public string KalanMuayeneLimiti { get; set; }

        //}
        //[DisplayName("TamamlayiciSaglikTazminatGirisResponse|TamamlayiciSaglikTazminatGirisResult")]
        //public class TamamlayiciSaglikTazminatGirisOutput : OutputType
        //{
        //    public string ProvizyonNo { get; set; }
        //    public List<Teminat> TeminatTalepBilgileriCevap { get; set; }
        //}
        //public class Teminat
        //{
        //    public string TeminatKodu { get; set; }
        //    public string TalepTutar { get; set; }
        //    public string OnayTutar { get; set; }
        //    public string SigortaliTutar { get; set; }
        //}
        //#endregion

        //[Serializable]
        //#region Input
        //public class CommonInput
        //{
        //    public string userNameSpecified { get; set; } = "1";
        //    [Required]
        //    public string UserName { get; set; }
        //    public string passwordSpecified { get; set; } = "1";
        //    [Required]
        //    public string Password { get; set; }
        //    public string kurumSpecified { get; set; } = "1";
        //    [Required]
        //    public string Kurum { get; set; }
        //}
        //[DisplayName("SigortaliHakSorgula|Sigortali")]
        //[Serializable]
        //public class HakSahiplikInput : CommonInput
        //{
        //    #region Private
        //    private string _tcNo = "";
        //    private string _olayTar = "";
        //    private string _pasaportNo = "";
        //    private string _aykYtrk = "";
        //    #endregion
        //    public string olayTarihiSpecified { get; set; }

        //    public string OlayTarihi { get { return _olayTar; } set { _olayTar = value; this.olayTarihiSpecified = !string.IsNullOrEmpty(value) ? true : false; } }
        //    public string tcKimlikNoSpecified { get; set; }

        //    public string TcKimlikNo { get { return _tcNo; } set { _tcNo = value; this.tcKimlikNoSpecified = !string.IsNullOrEmpty(value) ? true : false; } }
        //    public string pasaportNoSpecified { get; set; }
        //    public string PasaportNo { get { return _pasaportNo; } set { _pasaportNo = value; this.pasaportNoSpecified = !string.IsNullOrEmpty(value) ? true : false; } }
        //    public string ayaktanYatarakSpecified { get; set; }
        //    public string AyaktanYatarak
        //    {
        //        get { return _aykYtrk; }
        //        set { _aykYtrk = value; this.ayaktanYatarakSpecified = !string.IsNullOrEmpty(value) ? true : false; }
        //    }
        //}
        //[DisplayName("IcmalGiris|Icmal")]
        //public class IcmalGirisInput : CommonInput
        //{
        //    public string IcmalNo { get; set; }
        //    public List<TazminatBilgisi> TazminatBilgileri { get; set; }
        //}
        //[DisplayName("TamamlayiciSaglikProvizyonIptal|ProvizyonIptal")]
        //public class TamamlayiciSaglikProvizyonIptalInput : CommonInput
        //{
        //    public string ProvizyonNo { get; set; }
        //}
        //public class TazminatBilgisi
        //{
        //    public string ProvizyonNo { get; set; }
        //    public string FaturaNo { get; set; }
        //    public string FaturaTarihi { get; set; }
        //}
        //public class TazminatBilgi
        //{
        //    public string ProvizyonNo { get; set; }
        //}
        //[DisplayName("IcmalTazminatCikarma|IcmalTazminat")]
        //public class IcmalTazminatCikarmaInput
        //{
        //    public string IcmalNo { get; set; }
        //    public List<TazminatBilgi> TazminatBilgileri { get; set; }
        //}
        //[DisplayName("TamamlayiciSaglikTazminatGiris|Tazminat")]
        //public class TamamlayiciSaglikTazminatGirisInput : CommonInput
        //{
        //    public string EpikrizNotu { get; set; }
        //    public string HasarNotu { get; set; }
        //    public string ProvizyonNo { get; set; }
        //    public string PoliceNo { get; set; }
        //    public string TCKimlikNo { get; set; }
        //    public string TakipNo { get; set; }
        //    public string TakipTarihi { get; set; }
        //    public List<Tani> Tanilar { get; set; }
        //    public List<TeminatTalepBilgileri> TeminatTalepBilgileri { get; set; }
        //}
        //public class TeminatTalepBilgileri
        //{
        //    public List<TeminatIn> Teminat { get; set; }
        //}
        //public class TeminatIn
        //{
        //    public List<TalepHizmetBilgileri> TalepHizmetBilgileri { get; set; }
        //}
        //public class TalepHizmetBilgileri
        //{
        //    public List<HizmetBilgisi> HizmetBilgisi { get; set; }
        //}

        //public class HizmetBilgisi
        //{
        //    public string HizmetKodu { get; set; }
        //    public string HizmetAdi { get; set; }
        //    public string HizmetSgkTutar { get; set; }
        //    public string HizmetTalepTutar { get; set; }
        //}
        //public class Tani
        //{
        //    public string TaniTipi { get; set; }
        //    public string TaniKodu { get; set; }
        //    public string TaniAdi { get; set; }

        //}
        //#endregion
        //#region InternalInput
        public class NNIcmalGirisRequest : UserInfo
        {
            public long PayrollId { get; set; }
        }
        public class NNProviderTransferRequest : UserInfo
        {
            public long ProviderId { get; set; }
        }
        //#endregion
    }
}

