﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    class PoliceDetayMutabakatAgreementRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public Int64? PoliceNo { get; set; }
        public string YenilemeNo { get; set; }
        public DateTime TanzimTarihi { get; set; }
        public decimal Prim { get; set; }
        public int ToplamSigortaliAdeti { get; set; }
        public List<SigortaliTestListesi> Sigortali { get; set; }
    }
    public class SigortaliTestListesi
    {
        public string TCKN { get; set; }
        public string BireyTipi { get; set; }
        public decimal Prim { get; set; }
    }
}
