﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
   public class mutabakatAgreementIcmalRes
    {

        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public int ToplamIcmalAdedi { get; set; }
        public decimal ToplamIcmalTutarı { get; set; }
        public List<IcmalDetayListAgreement> IcmalListesi { get; set; }
    }
    public class IcmalDetayListAgreement
    {
        public string IcmalNo { get; set; }
        public decimal ToplamTutar { get; set; }
        public string OdemeTarihi { get; set; }
        public string IcmalDurumu { get; set; }
    }
}
