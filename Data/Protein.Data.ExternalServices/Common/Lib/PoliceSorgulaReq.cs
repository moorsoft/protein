﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    [Serializable]
    public class PoliceSorgulaReq
    {
        public PolicePK PolPK;
        public int? ZeylNo;
    }
}
