﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class CommonProxyRes
    {
        public string CompanyObjectId { get; set; }
        public bool IsOk { get; set; }
        public string Code { get; set; } = "1";
        public string Description { get; set; }
        public string[] Error { get; set; }
    }
}
