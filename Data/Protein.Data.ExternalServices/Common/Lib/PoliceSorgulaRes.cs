﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    [Serializable]
    public class PoliceSorgulaRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public SonucListesi SonucListesi { get; set; }
    }

    public class SonucListesi
    {
        public List<ZeylSonuc> ZeylSonuc { get; set; }
    }

    public class ZeylSonuc
    {
        public Detay Detay { get; set; }
    }

    public class Detay
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public string[] HataListesi { get; set; }
        public long TpaPoliceNo { get; set; }
        public string SagmerNo { get; set; }
        public int ZeylNo { get; set; }
        public int? ZeylSigortaliAdet { get; set; }
        public decimal? ZeylPrim { get; set; }
        public decimal? PolicePrim { get; set; }
    }
}
