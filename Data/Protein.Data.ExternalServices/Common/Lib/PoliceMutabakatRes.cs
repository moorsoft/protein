﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class PoliceMutabakatRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public int ToplamPoliceAdedi { get; set; }
        public decimal ToplamPolicePrimi { get; set; }
        public int ToplamSigortaliAdedi { get; set; }

        public List<Police> PoliceListesi { get; set; }


    }
    public class Police
    {
        public Int64? PoliceNo { get; set; }
        public string YenilemeNo { get; set; }
        public DateTime TransferTarihi { get; set; }
        public int SigortaliAdedi { get; set; }
        public decimal Prim{ get; set; }
    }
}
