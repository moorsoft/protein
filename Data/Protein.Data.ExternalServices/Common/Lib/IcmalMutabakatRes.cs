﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class IcmalMutabakatRes
    {

        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public int ToplamIcmalAdedi { get; set; }
        public int ToplamTazminatAdedi { get; set; }
        public decimal ToplamFaturaTutari { get; set; }
        public decimal ToplamIcmalTutari { get; set; }
        public List<IcmalDetay> IcmalListesi { get; set; }
    }

    public class IcmalDetay
    {
        public string TazminatPaketNo { get; set; }
        public string KurumTazminatPaketNo { get; set; }
        public string SirketTazminatPaketNo { get; set; }
        public int ToplamProvizyonAdet { get; set; }
        public decimal ToplamFaturaTutar { get; set; }
        public string OdemeTarihi { get; set; }
        public string IcmalDurumu { get; set; }
    }
}
