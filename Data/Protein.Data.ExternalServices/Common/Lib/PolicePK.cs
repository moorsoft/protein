﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class PolicePK
    {
        public int SirketKod { get; set; }
        public string PoliceNo { get; set; }
        public int YenilemeNo { get; set; }
    }
}
