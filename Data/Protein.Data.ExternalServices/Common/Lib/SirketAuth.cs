﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class SirketAuth
    {
        private string _apiKod;
        private int _sirketKod;

        public SirketAuth(string apidKod, int sirketKod)
        {
            this._apiKod = apidKod;
            this._sirketKod = sirketKod;
        }

        public int SirketKod
        {
            get { return _sirketKod; }
        }
        public string ApiKod
        {
            get { return _apiKod; }
        }
    }
}
