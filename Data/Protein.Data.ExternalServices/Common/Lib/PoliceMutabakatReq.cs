﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
   public class PoliceMutabakatReq
    {
        public int SirketKod { get; set; }
        public DateTime TransferTarihiBaslangic { get; set; }
        public DateTime TransferTarihiBitis { get; set; }

    }
}
