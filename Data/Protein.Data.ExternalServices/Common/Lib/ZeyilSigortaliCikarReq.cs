﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class ZeyilSigortaliCikarReq
    {
        public PolicePK PoliceNo { get; set; }
        public DateTime ZeylBaslamaTarih { get; set; }
        public DateTime ZeylBitisTarih { get; set; }
        public DateTime ZeylTanzimTarih { get; set; }
        public string ZeylNo { get; set; }
        public List<Sigortali> SigortaliListesi { get; set; }
    }
}
