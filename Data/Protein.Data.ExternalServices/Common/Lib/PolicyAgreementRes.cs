﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class PolicyAgreementRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public string Description { get; set; }
        public string CorrelationId { get; set; }

        public string resultWs { get; set; }
    }
}
