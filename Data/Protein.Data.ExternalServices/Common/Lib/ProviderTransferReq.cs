﻿using Protein.Data.ExternalServices.InsuranceCompanies.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class ProviderTransferReq
    {
        public long ProviderId { get; set; }
        public long CompanyCode { get; set; }
    }
}
