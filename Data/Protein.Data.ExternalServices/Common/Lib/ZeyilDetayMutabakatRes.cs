﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
   public class ZeyilDetayMutabakatRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public string SirketKod { get; set; }
        public Int64? PoliceNo { get; set; }
        public int YenilemeNo { get; set; }
        public int ZeyilNo{ get; set; }
        public string Tipi { get; set; }
        public int SigortaliAdedi { get; set; }
        public decimal Prim { get; set; }

        public List<SigortaliList> Sigortali { get; set; }

        
    }
    public class SigortaliList
    {

        public Int64? TCKN { get; set; }
        public string BireyTipi { get; set; }
        public decimal Prim { get; set; }
        public string SigortaliNo { get; set; }
        public DateTime? IlkKayitTarihi { get; set; }
        public DateTime? IlkSigortalanmaTarihi { get; set; }
        public string PaketNo { get; set; }
        public GercekKisi KisiBilgi { get; set; }

    }


}
