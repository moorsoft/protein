﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
   public class IcmalMutabakatDetayReq
    {
        public int SirketKod { get; set; }
        public string icmalNo { get; set; }
    }
}
