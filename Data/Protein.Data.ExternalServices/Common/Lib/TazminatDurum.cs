﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class TazminatDurum
    {
        public string Durum { get; set; }
        public int Adet { get; set; }
        public decimal SirketTutar { get; set; }

    }
}
