﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib.SBM
{
    [Serializable()]
    public class SbmClaimReq
    {
        public long ClaimId { get; set; }
    }
}
