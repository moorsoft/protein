﻿using Protein.Business.SagmerIntWs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    [Serializable]
    public class SbmPolicyCreateReq
    {
        public long PolicyId { get; set; }
    }
}
