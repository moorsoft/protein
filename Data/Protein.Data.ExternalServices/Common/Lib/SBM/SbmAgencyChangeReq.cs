﻿using System;

namespace Protein.Data.ExternalServices.Common.Lib.SBM
{
    [Serializable()]
    public class SbmAgencyChangeReq
    {
        public long PolicyId { get; set; }
        public long EndorsementId { get; set; }
        public string Authorization { get; set; }
    }
}
