﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib.SBM
{
    [Serializable()]
    public class SbmInsuredAccrualReq
    {
        public long PolicyId { get; set; }
        public long EndorsementId { get; set; }
        public long InsuredId { get; set; }
        public string Authorization { get; set; }
    }
}
