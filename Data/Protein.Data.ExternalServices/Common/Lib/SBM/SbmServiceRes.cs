﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Protein.Data.ExternalServices.SagmerOnlineService.InputOutputTypes;

namespace Protein.Data.ExternalServices.Common.Lib
{
    [Serializable]
    public class SbmServiceRes
    {
        public int ResultCode { get; set; }
        public string ResultDesc { get; set; }
        public List<failure> FailedErrors { get; set; }
    }
}
