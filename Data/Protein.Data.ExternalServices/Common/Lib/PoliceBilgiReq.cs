﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    [Serializable]
    public class PoliceBilgiReq
    {
        public PolicePK PolPK;
        public string MusteriNo;
    }
}
