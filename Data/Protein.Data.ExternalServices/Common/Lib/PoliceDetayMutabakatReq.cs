﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
   public class PoliceDetayMutabakatReq
    {
        public int SirketKod { get; set; }
        public Int64? PoliceNo { get; set; }
        public string YenilemeNo { get; set; }
    }
}
