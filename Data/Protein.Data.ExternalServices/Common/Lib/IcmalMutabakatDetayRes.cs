﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
   public class IcmalMutabakatDetayRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public List<TazminatDetay> TazminatListesi { get; set; }

    }

    public class TazminatDetay
    {
        public string TazminatNo { get; set; }
        public string SirketProvizyon { get; set; }
        public int No { get; set; }
        public string TransferTarihi { get; set; }
        public decimal SirketTutar { get; set; }
        public string HasarDurum { get; set; }
    }
}
