﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class TazminatAgreementRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public string resultWs { get; set; }

    }
}
