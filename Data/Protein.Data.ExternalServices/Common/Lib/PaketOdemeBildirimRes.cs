﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    [Serializable]
    public class PaketOdemeBildirimRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public string[] ValidationErrors { get; set; }
        //public ValidationResult ValidationResult { get; set; }
        //public Exception SystemException { get; set; }
    }
}
