﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
   public class ProvizyonMutabakatDetayRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }

        public TazminatDurum TazminatDurum {get; set;}

       public List<Tazminat> TazminatListesi { get; set; }
     
    }

    public class Tazminat
    {
        public Int64 No { get; set; }
        public decimal SirketTutar { get; set; }
     // public  List<Teminat> TeminatListesi { get; set; }
    }

    //public class Teminat
    //{
    //    public string Kod { get; set; }
    //    public string Ad { get; set; }
    //    public decimal SirketTutar { get; set; }
    //}
}
