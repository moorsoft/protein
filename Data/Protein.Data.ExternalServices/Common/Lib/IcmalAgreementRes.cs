﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
    public class IcmalAgreementRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public int ToplamIcmalAdedi { get; set; }
        public decimal ToplamIcmalTutarı { get; set; }
        public string resultWs { get; set; }

        public List<IcmalDetayList> IcmalListesi { get; set; }
    }
    public class IcmalDetayList
    {
        public string IcmalNo { get; set; }
        public decimal ToplamTutar { get; set; }
        public string OdemeTarihi { get; set; }
        public string IcmalDurumu { get; set; }
    }
}
