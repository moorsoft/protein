﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Common.Lib
{
   public class ZeyilMutabakatRes
    {

        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public int ToplamSigortaliAdedi { get; set; }
        public decimal ToplamPrim { get; set; }
        public int ToplamZeyilAdedi { get; set;}

        public List<Zeyil> ZeyilListesi { get; set; }
    }

    public class Zeyil
    {
        public int No { get; set; }
        public string Tipi { get; set; }
        public int SigortaliAdeti { get; set; }
        public decimal Prim { get; set; }

    }
}
