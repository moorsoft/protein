﻿using Protein.Common.Constants;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Data.ExternalServices.Common.Helper
{
    public class CompanyHelper
    {
        public CompanyParam GetCompanyParameters(long CompanyID)
        {
            CompanyParam returnParam = new CompanyParam();
            try
            {
                string whereCondition = $" SYSTEM_TYPE = {(int)SystemType.WS} and COMPANY_ID = {CompanyID}";
                List<V_CompanyParameter> companyParams = new GenericRepository<V_CompanyParameter>().FindBy(conditions: whereCondition, orderby: "");

                if (companyParams != null)
                {
                    if (companyParams.Count > 0)
                    {
                        foreach (V_CompanyParameter cParams in companyParams)
                        {
                            switch (cParams.Key.ToUpper())
                            {
                                case Constants.AppKeys.ApiCode: returnParam.ApiCode = cParams.Value; break;
                                case Constants.AppKeys.ApiCompanyCode: returnParam.ApiCompanyCode = cParams.Value; break;
                                case Constants.AppKeys.ApiPassword: returnParam.ApiPassword = cParams.Value; break;
                                case Constants.AppKeys.ApiUrl: returnParam.ApiUrl = cParams.Value; break;
                                case Constants.AppKeys.ApiUserName: returnParam.ApiUserName = cParams.Value; break;
                                default: break;
                            }
                        }
                        companyParams = null;
                    }
                }
            }
            catch (Exception ex) { }
            return returnParam;
        }
    }
}
