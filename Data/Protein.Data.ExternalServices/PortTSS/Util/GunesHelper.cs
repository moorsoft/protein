﻿using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.PortTSS.Util.Lib;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using static Protein.Data.ExternalServices.Protein.ProxySrvModels;

namespace Protein.Data.ExternalServices.PortTSS.Util
{
    public class GunesHelper
    {


        //string GunesWSAddress; // = ConfigurationManager.AppSettings["GunesWSAddress"];
        //string GunesAgitoClaimWSAddress; // = ConfigurationManager.AppSettings["GunesAgitoClaimWSAddress"];

        //string ClaimUsername; // = "WS_PROVIZYON_IMECE";
        //string ClaimPassword; // = "Im83Fy1T";

        //private ProdType productType;



        public class ServiceDesc
        {
            public ServiceDesc(string sirketKod, string apiKod, string url, string userName, string pwd)
            {
                this.SirketKod = sirketKod;
                this.ApiKod = apiKod;
                this.URL = url;
                this.UserName = userName;
                this.Pwd = pwd;
            }
            public string SirketKod;
            public string ApiKod;
            public string URL;
            public string UserName;
            public string Pwd;
        }

        //public static string ApiCodeGunesProd = "A3B5C6C4D6BC44F2A610DF1AA4C64E86";
        //public static string ApiCodeVakifProd = "53ED0232A503454AA7248290D9E775A1";

        //public static string ApiCodeVakifTest = "8cf67ec6-01fc-4d53-ac0b-6f46170e8c40";
        //public static string ApiCodeGunesTest = "8284794d-0f7b-4ca1-87b3-6320a6bd6726";

        //public static ServiceDesc GuneProdServiceDesc = new ServiceDesc("2", ApiCodeGunesProd, "https://provizyon.gunessigorta.com.tr/gunessigorta/SaglikProvizyonWebService", "WS_PROVIZYON_IMECE", "Hy65Qt9Y");
        //public static ServiceDesc VakifProdServiceDesc = new ServiceDesc("9", ApiCodeVakifProd, "http://ve.gunessigorta.com.tr/gunessigorta/SaglikProvizyonWebService", "WS_PROVIZYON_IMECE", "Oz07Jz9H");

        //public static ServiceDesc VakifTestServiceDesc = new ServiceDesc("97", ApiCodeVakifTest, "http://vetest.gunessigorta.com.tr/gunessigorta/SaglikProvizyonWebService", "WS_PROVIZYON_IMECE", "Aa123456");
        //public static ServiceDesc GunesTestServiceDesc = new ServiceDesc("98", ApiCodeGunesTest, "http://pusulatest.gunessigorta.com.tr/gunessigorta/SaglikProvizyonWebService", "WS_PROVIZYON_IMECE", "Aa123456");


        //public static ServiceDesc GetServiceDescBySirketKod(int sirketKod)
        //{
        //    ServiceDesc desc = null;

        //    if (sirketKod.Equals(2))
        //    {
        //        desc = GuneProdServiceDesc;
        //    }
        //    if (sirketKod.Equals(98))
        //    {
        //        desc = GunesTestServiceDesc;
        //    }
        //    if (sirketKod.Equals(9))
        //    {
        //        desc = VakifProdServiceDesc;
        //    }
        //    if (sirketKod.Equals(97))
        //    {
        //        desc = VakifTestServiceDesc;
        //    }
        //    return desc;
        //}

        //public static ServiceDesc GetServiceDescApiKod(string apiKod)
        //{
        //    ServiceDesc desc = null;

        //    if (apiKod.Equals(GunesHelper.ApiCodeGunesProd))
        //    {
        //        desc = GunesHelper.GuneProdServiceDesc;
        //    }
        //    if (apiKod.Equals(GunesHelper.ApiCodeGunesTest))
        //    {
        //        desc = GunesHelper.GunesTestServiceDesc;
        //    }
        //    if (apiKod.Equals(GunesHelper.ApiCodeVakifProd))
        //    {
        //        desc = GunesHelper.VakifProdServiceDesc;
        //    }
        //    if (apiKod.Equals(GunesHelper.ApiCodeVakifTest))
        //    {
        //        desc = GunesHelper.VakifTestServiceDesc;
        //    }
        //    return desc;
        //}

        public ServiceDesc _servicecDesc { get; set; }

        //private LogSrv _logSrv = null;


        public GunesHelper(CompanyParam companyParam)
        {
            _servicecDesc = new ServiceDesc(companyParam.ApiCompanyCode, companyParam.ApiCode, companyParam.ApiUrl, companyParam.ApiUserName, companyParam.ApiPassword);
            //this._logSrv = new LogSrv();
        }
        //PUSULA
        public SaglikProvizyonUygunlukKontrolResult ProvKontrol(SaglikProvizyonUygunlukKontrolParam param)
        {
            SaglikProvizyonUygunlukKontrolResult res = new SaglikProvizyonUygunlukKontrolResult();

            //LogSrv.LogItem logItem = new LogSrv.LogItem();
            //LogSrv.LogExtension logExItem = new LogSrv.LogExtension();
            //logItem.ServisAd = "GunesHelper.ProvKontrol";

            //logExItem.SirketKod = this._servicecDesc.SirketKod;

            try
            {
                StringBuilder sb = new StringBuilder(1000);
                sb.AppendLine("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:sag=\"http://saglik.ws.entegrasyon.pusula.gunessigorta.com/\">");
                sb.AppendLine("<soapenv:Header>");
                sb.AppendLine("<wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">");
                sb.AppendLine("<wsse:UsernameToken xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">");
                sb.AppendLine(string.Format("<wsse:Username>{0}</wsse:Username>", this._servicecDesc.UserName));
                sb.AppendLine(string.Format("<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">{0}</wsse:Password>", _servicecDesc.Pwd));
                sb.AppendLine("</wsse:UsernameToken>");
                sb.AppendLine("</wsse:Security>");
                sb.AppendLine("</soapenv:Header>");
                sb.AppendLine("   <soapenv:Body>");
                sb.AppendLine("      <sag:provizyonUygunlukSorgula>");
                sb.AppendLine("         <uygunlukSorguInput>");
                sb.AppendLine(string.Format("            <grupPoliceNo>{0}</grupPoliceNo>", param.GrupPoliceNo));
                sb.AppendLine(string.Format("            <musteriId>{0}</musteriId>", param.MusteriId));
                sb.AppendLine(string.Format("            <olayTarihi>{0}-{1}-{2}T12:00:00</olayTarihi>", param.OlayTarihi.Year, param.OlayTarihi.Month.ToString().PadLeft(2, '0'), param.OlayTarihi.Day.ToString().PadLeft(2, '0')));
                sb.AppendLine(string.Format("            <policeNo>{0}</policeNo>", param.PoliceNo));
                sb.AppendLine(string.Format("            <yenilemeNo>{0}</yenilemeNo>", param.YenilemeNo));
                sb.AppendLine("         </uygunlukSorguInput>");
                sb.AppendLine("      </sag:provizyonUygunlukSorgula>");
                sb.AppendLine("   </soapenv:Body>");
                sb.AppendLine("</soapenv:Envelope>");


                //logItem.Request = sb.ToString();


                string url = string.Format("{0}?provizyonUygunlukSorgula", this._servicecDesc.URL);
                string serviceResult = CallService(sb.ToString(), url);

                //logItem.Response = serviceResult;

                string startKey = "<sonucAciklama>";
                int basIndex = serviceResult.IndexOf(startKey);
                int bitIndex = serviceResult.IndexOf("</sonucAciklama>");
                int startIndex = basIndex + startKey.Length;

                int len = (bitIndex - startIndex);

                if (basIndex < 0)
                    res.SonucAciklama = serviceResult;
                else
                    res.SonucAciklama = serviceResult.Substring(startIndex, len);

                res.SonucKodu = NarXmlParser("sonucKodu", serviceResult);
            }
            catch (Exception ex)
            {
                res.SonucAciklama = string.Format("Runtime Error: {0}", ex.Message);
                //logItem.Aciklama = "HATA:" + ex.Message;
                throw ex;
            }
            finally
            {
                //_logSrv.InsertLogItem(logItem, logExItem);
            }
            return res;
        }
        public SaglikProvizyonKaydetResult ProvKaydet(SaglikProvizyonKaydetParam param)
        {
            SaglikProvizyonKaydetResult res = new SaglikProvizyonKaydetResult();
            try
            {
                if (param.TaniListesi == null || param.TaniListesi.Count == 0)
                {
                    res.SonucAciklama = "Tanı listesi boş olamaz..";
                    return res;
                }

                if (param.IslemListesi == null || param.IslemListesi.Count == 0)
                {
                    res.SonucAciklama = "İşlem listesi boş olamaz..";
                    return res;
                }


                StringBuilder sb = new StringBuilder(2000);
                sb.AppendLine("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:sag=\"http://saglik.ws.entegrasyon.pusula.gunessigorta.com/\"> ");
                sb.AppendLine("  <soapenv:Header>");
                sb.AppendLine("      <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">");
                sb.AppendLine("        <wsse:UsernameToken xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">");
                sb.AppendLine(string.Format("           <wsse:Username>{0}</wsse:Username>", this._servicecDesc.UserName));
                sb.AppendLine(string.Format("                   <wsse:Password>{0}</wsse:Password>", this._servicecDesc.Pwd));
                sb.AppendLine("        </wsse:UsernameToken>");
                sb.AppendLine("     </wsse:Security>");
                sb.AppendLine("  </soapenv:Header>");
                sb.AppendLine("   <soapenv:Body>");
                sb.AppendLine("<provizyonKaydet xmlns=\"http://saglik.ws.entegrasyon.pusula.gunessigorta.com/\">");
                sb.AppendLine("            <provizyonKaydetInput xmlns=\"\">");
                sb.AppendLine(string.Format("          <provizyonId>{0}</provizyonId>", GetString(param.ProvizyonId)));
                sb.AppendLine(string.Format("          <musteriId>{0}</musteriId>", GetString(param.MusteriId)));
                sb.AppendLine(string.Format("          <policeNo>{0}</policeNo>", GetString(param.PoliceNo)));
                sb.AppendLine(string.Format("          <yenilemeNo>{0}</yenilemeNo>", GetString(param.YenilemeNo)));
                sb.AppendLine(string.Format("          <grupPoliceNo>{0}</grupPoliceNo>", GetString(param.GrupPoliceNo)));
                sb.AppendLine(string.Format("          <olayTarihi>{0}</olayTarihi>", GetDateWithTime(param.OlayTarihi, param.OlayTarihiSaat)));
                sb.AppendLine(string.Format("          <provizyonDurumu>{0}</provizyonDurumu>", GetString(param.ProvizyonDurumu)));
                sb.AppendLine(string.Format("          <cozumOrtagiId>{0}</cozumOrtagiId>", GetString(param.CozumOrtagiId)));
                sb.AppendLine(string.Format("          <sgkAnlasmaliMi>{0}</sgkAnlasmaliMi>", GetString(param.SgkAnlasmaliMi)));
                sb.AppendLine(string.Format("          <yatisTarihi>{0}</yatisTarihi>", GetDateWithOutTime(param.YatisTarihi)));
                sb.AppendLine(string.Format("          <cikisTarihi>{0}</cikisTarihi>", GetDateWithOutTime(param.CikisTarihi)));
                sb.AppendLine(string.Format("          <provizyonNotu>{0}</provizyonNotu>", GetString(param.ProvizyonNotu)));
                sb.AppendLine(string.Format("          <provizyonNedeni>{0}</provizyonNedeni>", GetString(param.ProvizyonNedeni)));
                sb.AppendLine("          <odemeYeri>KURUM</odemeYeri>");
                sb.AppendLine(string.Format("          <teminatGrubu>{0}</teminatGrubu>", GetString(param.TeminatGrubu)));
                sb.AppendLine(string.Format("          <sikayet>{0}</sikayet>", GetString(param.Sikayet)));
                sb.AppendLine(string.Format("          <doktorAd></doktorAd>", GetString(param.DoktorAd)));
                sb.AppendLine(string.Format("          <entegrasyonProvizyonId>{0}</entegrasyonProvizyonId>", GetString(param.EntegrasyonProvizyonId)));

                if (param.FaturaBilgiList == null || param.FaturaBilgiList.Count == 0)
                {
                    sb.AppendLine("          <faturaBilgisi>");
                    sb.AppendLine("              <faturaNo></faturaNo>");
                    sb.AppendLine("              <faturaTarihi></faturaTarihi>");
                    sb.AppendLine("              <faturaTutar></faturaTutar>");
                    sb.AppendLine("          </faturaBilgisi>");
                }
                else
                {
                    foreach (FaturaBilgisi fat in param.FaturaBilgiList)
                    {
                        sb.AppendLine("          <faturaBilgisi>");
                        sb.AppendLine(string.Format("              <faturaNo>{0}</faturaNo>", GetString(fat.FaturaNo)));
                        sb.AppendLine(string.Format("              <faturaTarihi>{0}</faturaTarihi>", GetDateWithOutTime(fat.FaturaTarihi)));
                        sb.AppendLine(string.Format("              <faturaTutar>{0}</faturaTutar>", GetString(fat.FaturaTutar).Replace(',', '.')));
                        sb.AppendLine("          </faturaBilgisi>");
                    }
                }


                foreach (Tani t in param.TaniListesi)
                {
                    sb.AppendLine("	            <taniListesi>");
                    sb.AppendLine("		            <kesinTaniMi>true</kesinTaniMi>");
                    sb.AppendLine(string.Format("		            <taniKodu>{0}</taniKodu>", GetString(t.TaniKodu)));
                    sb.AppendLine("	            </taniListesi>");
                }

                foreach (Islem i in param.IslemListesi)
                {
                    sb.AppendLine("          <islemListesi>");
                    sb.AppendLine(string.Format("              <islemKodu>{0}</islemKodu>", GetString(i.IslemKodu)));
                    sb.AppendLine(string.Format("              <faturaNo>{0}</faturaNo>", GetString(i.FaturaNo)));
                    sb.AppendLine(string.Format("              <faturaTutari>{0}</faturaTutari>", GetString(i.FaturaTutari).Replace(',', '.')));
                    sb.AppendLine(string.Format("              <brutTutar>{0}</brutTutar>", GetString(i.BrutTutar).Replace(',', '.')));
                    sb.AppendLine(string.Format("              <odenecekTutar>{0}</odenecekTutar>", GetString(i.OdenecekTutar).Replace(',', '.')));
                    sb.AppendLine(string.Format("              <katilimTutari>{0}</katilimTutari>", GetString(i.KatilimTutari).Replace(',', '.')));
                    sb.AppendLine(string.Format("              <kapsamDisiTutar>{0}</kapsamDisiTutar>", GetString(i.KapsamDisiTutar).Replace(',', '.')));
                    sb.AppendLine("          </islemListesi>");
                }

                sb.AppendLine("         </provizyonKaydetInput>");
                sb.AppendLine("      </provizyonKaydet>");
                sb.AppendLine("   </soapenv:Body>");
                sb.AppendLine("</soapenv:Envelope>");


                string url = string.Format("{0}?provizyonKaydet", this._servicecDesc.URL);

                string request = sb.ToString();
                string response = CallService(request, url);


                string startKey = "<sonucAciklama>";
                int basIndex = response.IndexOf(startKey);
                int bitIndex = response.IndexOf("</sonucAciklama>");
                int startIndex = basIndex + startKey.Length;

                int len = (bitIndex - startIndex);

                if (basIndex < 0)
                    res.SonucAciklama = response;
                else
                    res.SonucAciklama = response.Substring(startIndex, len);

                res.SonucKodu = NarXmlParser("sonucKodu", response);
                res.ProvizyonId = NarXmlParser("provizyonId", response);

                
                //20-09-2017 @MB mükerrer kayıt sorunu
                //10.02.2017#Güneş Hasar Update
                //LogHelper.GunesHasarUpdate(this._servicecDesc.SirketKod, "", param.EntegrasyonProvizyonId, res.ProvizyonId, out Durum);
                return res;
            }
            catch (Exception ex)
            {
                res.SonucAciklama = string.Format("Runtime Error: {0}", ex.Message);

            }

            return res;

        }



        /*
        //BT Human
        public CreateClaimResult CreateClaim(CreateClaimParam param, out string BTHumanGelenXML, out string BTHumanGidenXML, string TransactionRequestNo = "")
        {
            CreateClaimResult result = new CreateClaimResult();
            string MethodName = "CreateClaim";

            BTHumanGelenXML = "";
            BTHumanGidenXML = "";


            //result = this.ManuelCreateClaim(param, TransactionRequestNo);
            //return result;
            DBPlace dbPlace = DBPlace.Live;
     
            AgitoClaimWS.CreateClaimInputType prm = new AgitoClaimWS.CreateClaimInputType();
            prm.AccountOwner = param.AccountOwner;
            List<AgitoClaimWS.ClaimDetailInputType> claimDetailList = new List<AgitoClaimWS.ClaimDetailInputType>();
            foreach (ClaimDetailInputType det in param.ClaimDetails)
            {
                AgitoClaimWS.ClaimDetailInputType detail = new AgitoClaimWS.ClaimDetailInputType();
                if (!string.IsNullOrEmpty(det.BenefitCode))
                    detail.BenefitCode = Convert.ToInt32(det.BenefitCode); //Teminat kodu 
                if (!string.IsNullOrEmpty(det.Coinsurance))
                    detail.Coinsurance = Convert.ToDecimal(det.Coinsurance.Replace(',', '.'), new CultureInfo("en-US")); //Katılım oranı
                if (!string.IsNullOrEmpty(det.FinishDate))
                    detail.FinishDate = Convert.ToDateTime(det.FinishDate); //Şikayet bitiş tarihi
                if (!string.IsNullOrEmpty(det.InvoiceAmount))
                    detail.InvoiceAmount = Convert.ToDecimal(det.InvoiceAmount.Replace(',', '.'), new CultureInfo("en-US"));
                if (!string.IsNullOrEmpty(det.InvoiceDate))
                    detail.InvoiceDate = Convert.ToDateTime(det.InvoiceDate);
                if (!string.IsNullOrEmpty(det.InvoiceExclusion))
                    detail.InvoiceExclusion = Convert.ToDecimal(det.InvoiceExclusion.Replace(',', '.'), new CultureInfo("en-US"));
                if (!string.IsNullOrEmpty(det.InvoiceNumber))
                    detail.InvoiceNumber = Convert.ToInt64(det.InvoiceNumber);
                if (!string.IsNullOrEmpty(det.PayableAmount))
                    detail.PayableAmount = Convert.ToDecimal(det.PayableAmount.Replace(',', '.'), new CultureInfo("en-US"));
                detail.SerialNumber = det.SerialNumber;
                detail.SgkDesc = det.SgkDesc;
                if (!string.IsNullOrEmpty(det.StartDate))
                    detail.StartDate = Convert.ToDateTime(det.StartDate); //Şikayet bitiş tarihi
                claimDetailList.Add(detail);
            }
            prm.ClaimDetails = claimDetailList.ToArray();
            prm.ClaimId = Convert.ToInt32(param.ClaimId); //GUNES_HSR_ID
            prm.ClaimStatus = param.ClaimStatus;
            prm.Complaints = param.Complaints;
            prm.ConfidentialInfo = param.ConfidentialInfo;
            prm.CustomerId = Convert.ToInt32(param.CustomerId);
            prm.DoctorDesc = param.DoctorDesc;
            prm.DoctorName = param.DoctorName;
            if (!string.IsNullOrEmpty(param.DoctorSpecialityId))
                prm.DoctorSpecialityId = Convert.ToInt32(param.DoctorSpecialityId);
            if (!string.IsNullOrEmpty(param.EventDate))
                prm.EventDate = Convert.ToDateTime(param.EventDate);
            prm.Explain = param.Explain;
            if (!string.IsNullOrEmpty(param.HealthCenterId))
                prm.HealthCenterId = Convert.ToInt32(param.HealthCenterId);
            prm.Iban = param.Iban;
            prm.Icd10Code = param.Icd10Code;
            if (!string.IsNullOrEmpty(param.PayDueDate))
                prm.PayDueDate = Convert.ToDateTime(param.PayDueDate); //Hasar ödeme tarihi
            if (!string.IsNullOrEmpty(param.PlanId))
                prm.PlanId = Convert.ToInt32(param.PlanId); //hasar sorgusunun üzerinden alacağız
            if (!string.IsNullOrEmpty(param.PolicyNumber)) //POLICENO
                prm.PolicyNumber = Convert.ToInt64(param.PolicyNumber);
            if (!string.IsNullOrEmpty(param.SipClaimId)) //Kendi HAsar ID'miz MASTERID
                prm.SipClaimId = Convert.ToInt32(param.SipClaimId);
            if (!string.IsNullOrEmpty(param.StatusReasonCode)) //hasar durum sebep kodu sagmer kodu
                prm.StatusReasonCode = Convert.ToInt32(param.StatusReasonCode);

            AgitoClaimWS.AgitoClaimWSSoapClient client = new AgitoClaimWS.AgitoClaimWSSoapClient();

            //Context.Response.ContentEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");

            //client.Endpoint.ListenUri = new Uri("GunesAgitoClaimWSAddress");
            //client.ChannelFactory.Endpoint.Behaviors.Clear();
            //client.ChannelFactory.Endpoint.Behaviors.Add(new ClientViaBehavior(new Uri(ClaimPassword)));


            if (HttpContext.Current.Request.ContentType == "text/xml")
            {
                HttpContext.Current.Request.ContentType = "text/xml; charset=UTF-8";
            }

            AgitoClaimWS.CreateClaimResultType res = client.CreateClaim(prm);

            result.ClaimId = res.ClaimId;
            result.ResultCode = res.ResultCode;
            result.ResultDesc = res.ResultDesc;


            //10.02.2017#Güneş Hasar Update
            string Durum;
            LogHelper.GunesHasarUpdate("2", "", param.SipClaimId, result.ClaimId.ToString(), dbPlace, out Durum);


            return result;
        }




        private CreateClaimResult ManuelCreateClaim(CreateClaimParam param, string TransactionRequestNo)
        {
            CreateClaimResult result = new CreateClaimResult();
            try
            {
                StringBuilder sb = new StringBuilder(2000);
                sb.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">");
                sb.Append("   <soapenv:Header/>");
                sb.Append("   <soapenv:Body>");
                sb.Append("      <tem:CreateClaim>");
                sb.Append("         <tem:param>");
                sb.Append(string.Format("            <tem:ClaimId>{0}</tem:ClaimId>", GetString(param.ClaimId)));
                sb.Append(string.Format("            <tem:ClaimDetails>"));

                foreach (ClaimDetailInputType claim in param.ClaimDetails)
                {
                    sb.Append(string.Format("               <tem:ClaimDetailInputType>"));
                    sb.Append(string.Format("                  <tem:BenefitCode>{0}</tem:BenefitCode>", GetString(claim.BenefitCode)));
                    sb.Append(string.Format("                  <tem:InvoiceAmount>{0}</tem:InvoiceAmount>", GetString(claim.InvoiceAmount)));
                    sb.Append(string.Format("                  <tem:PayableAmount>{0}</tem:PayableAmount>", GetString(claim.PayableAmount)));
                    sb.Append(string.Format("                  <tem:Coinsurance>{0}</tem:Coinsurance>", GetString(claim.Coinsurance)));
                    sb.Append(string.Format("                  <tem:StartDate>{0}</tem:StartDate>", GetDateWithOutTime(claim.StartDate.Replace('/', '.'))));
                    sb.Append(string.Format("                  <tem:FinishDate>{0}</tem:FinishDate>", GetDateWithOutTime(claim.FinishDate.Replace('/', '.'))));
                    sb.Append(string.Format("                  <tem:SgkDesc>{0}</tem:SgkDesc>", GetString(claim.SgkDesc)));
                    sb.Append(string.Format("                  <tem:InvoiceNumber>{0}</tem:InvoiceNumber>", GetString(claim.InvoiceNumber)));
                    sb.Append(string.Format("                  <tem:InvoiceDate>09/03/2016</tem:InvoiceDate>", GetDateWithOutTime(claim.InvoiceDate.Replace('/', '.'))));
                    sb.Append(string.Format("                  <tem:InvoiceExclusion>{0}</tem:InvoiceExclusion>", GetString(claim.InvoiceExclusion)));
                    sb.Append(string.Format("                  <tem:SerialNumber></tem:SerialNumber>", GetString(claim.SerialNumber)));
                    sb.Append(string.Format("               </tem:ClaimDetailInputType>"));
                }

                sb.Append(string.Format("            </tem:ClaimDetails>"));
                sb.Append(string.Format("            <tem:EventDate>{0}</tem:EventDate>", GetDateWithOutTime(param.EventDate)));
                sb.Append(string.Format("            <tem:PolicyNumber>{0}</tem:PolicyNumber>", GetString(param.PolicyNumber)));
                sb.Append(string.Format("            <tem:CustomerId>{0}</tem:CustomerId>", GetString(param.CustomerId)));
                sb.Append(string.Format("            <tem:HealthCenterId>{0}</tem:HealthCenterId>", GetString(param.HealthCenterId)));
                sb.Append(string.Format("            <tem:PlanId>{0}</tem:PlanId>", GetString(param.PlanId)));
                sb.Append(string.Format("            <tem:StatusReasonCode>{0}</tem:StatusReasonCode>", GetString(param.StatusReasonCode)));
                sb.Append(string.Format("            <tem:Iban>{0}</tem:Iban>", GetString(param.Iban)));
                sb.Append(string.Format("            <tem:AccountOwner>{0}</tem:AccountOwner>", GetString(param.AccountOwner)));
                sb.Append(string.Format("            <tem:PayDueDate>{0}</tem:PayDueDate>", GetDateWithOutTime(param.PayDueDate.Replace('/', '.'))));
                sb.Append(string.Format("            <tem:Icd10Code>{0}</tem:Icd10Code>", GetString(param.Icd10Code)));
                sb.Append(string.Format("            <tem:Complaints>{0}</tem:Complaints>", GetString(param.Complaints)));
                sb.Append(string.Format("            <tem:DoctorDesc>{0}</tem:DoctorDesc>", GetString(param.DoctorDesc)));
                sb.Append(string.Format("            <tem:DoctorName>{0}</tem:DoctorName>", GetString(param.DoctorName)));
                sb.Append(string.Format("            <tem:DoctorSpecialityId>{0}</tem:DoctorSpecialityId>", GetString(param.DoctorSpecialityId)));
                sb.Append(string.Format("            <tem:Explain>{0}</tem:Explain>", GetString(param.Explain)));
                sb.Append(string.Format("            <tem:ConfidentialInfo>{0}</tem:ConfidentialInfo>", GetString(param.ConfidentialInfo)));
                sb.Append(string.Format("            <tem:SipClaimId>{0}</tem:SipClaimId>", GetString(param.SipClaimId)));
                sb.Append(string.Format("            <tem:ClaimStatus>{0}</tem:ClaimStatus>", GetString(param.ClaimStatus)));
                sb.Append(string.Format("         </tem:param>"));
                sb.Append(string.Format("      </tem:CreateClaim>"));
                sb.Append(string.Format("   </soapenv:Body>"));
                sb.Append(string.Format("</soapenv:Envelope>"));


                string url = string.Format("{0}?CreateClaim", GunesAgitoClaimWSAddress);

                string serviceResult = CallService(sb.ToString(), url);

                string startKey = "<sonucAciklama>";
                int basIndex = serviceResult.IndexOf(startKey);
                int bitIndex = serviceResult.IndexOf("</sonucAciklama>");
                int startIndex = basIndex + startKey.Length;

                int len = (bitIndex - startIndex);

                if (basIndex < 0)
                    result.ResultDesc = serviceResult;
                else
                    result.ResultDesc = serviceResult.Substring(startIndex, len);

                result.ResultDesc = NarXmlParser("ResultDesc", serviceResult);
                result.ResultCode = Convert.ToInt32(NarXmlParser("ResultCode", serviceResult));
                result.ClaimId = Convert.ToInt32(NarXmlParser("ClaimId", serviceResult));

                return result;

            }
            catch (Exception ex)
            {
                result.ResultCode = -99;
                result.ResultDesc = string.Format("Runtime Error: {0}", ex.Message);
            }

            return result;

        }
        public PaymentQueryResult PaymentQuery(PaymentQueryParam param)
        {
            PaymentQueryResult result = new PaymentQueryResult();
            try
            {
                AgitoClaimWS.AgitoClaimWSSoapClient client = new AgitoClaimWS.AgitoClaimWSSoapClient();
                AgitoClaimWS.PaymentQueryInputType prm = new AgitoClaimWS.PaymentQueryInputType();

                prm.CustomerId = Convert.ToInt32(param.CustomerId);
                prm.EventDate = (Convert.ToDateTime(param.EventDate));
                prm.PolicyNumber = Convert.ToInt32(param.PolicyNumber);

                AgitoClaimWS.PaymentQueryResultType res = client.PaymentQuery(prm);

                result.ResultCode = res.ResultCode;
                result.ResultDesc = res.ResultDesc;
            }
            catch (Exception exx)
            {
                result.ResultDesc = exx.Message;
            }
            return result;
        }

        public BenefitReturnType TeminatBenefitMapBTHuman(string TeminatId)
        {
            BenefitReturnType ret = new BenefitReturnType();

            switch (TeminatId)
            {
                case "55":
                    ret.GunesPlanId = 733;
                    ret.GunesTeminatId = 104;
                    break;
                case "117":
                    ret.GunesPlanId = 733;
                    ret.GunesTeminatId = 108;
                    break;
                case "348":
                    ret.GunesPlanId = 733;
                    ret.GunesTeminatId = 138;
                    break;
                case "286":
                    ret.GunesPlanId = 733;
                    ret.GunesTeminatId = 304;
                    break;
                case "169":
                    ret.GunesPlanId = 733;
                    ret.GunesTeminatId = 89;
                    break;
                case "333":
                    ret.GunesPlanId = 733;
                    ret.GunesTeminatId = 90;
                    break;
                case "70":
                    ret.GunesPlanId = 733;
                    ret.GunesTeminatId = 91;
                    break;
                case "349":
                    ret.GunesPlanId = 733;
                    ret.GunesTeminatId = 95;
                    break;
                case "339":
                    ret.GunesPlanId = 733;
                    ret.GunesTeminatId = 98;
                    break;
                case "429":
                    ret.GunesPlanId = 733;
                    ret.GunesTeminatId = 113;
                    break;
                case "256":
                    ret.GunesPlanId = 733;
                    ret.GunesTeminatId = 126;
                    break;
                case "328":
                    ret.GunesPlanId = 733;
                    ret.GunesTeminatId = 439;
                    break;
                case "368":
                    ret.GunesPlanId = 733;
                    ret.GunesTeminatId = 481;
                    break;


                //case "169":
                //    ret.GunesPlanId = 823;
                //    ret.GunesTeminatId = 169;
                //    break;
                //case "333":
                //    ret.GunesPlanId = 823;
                //    ret.GunesTeminatId = 333;
                //    break;
                //case "70":
                //    ret.GunesPlanId = 823;
                //    ret.GunesTeminatId = 70;
                //    break;
                //case "349":
                //    ret.GunesPlanId = 823;
                //    ret.GunesTeminatId = 349;
                //    break;
                //case "339":
                //    ret.GunesPlanId = 823;
                //    ret.GunesTeminatId = 339;
                //    break;
                //case "429":
                //    ret.GunesPlanId = 823;
                //    ret.GunesTeminatId = 429;
                //    break;
                //case "256":
                //    ret.GunesPlanId = 823;
                //    ret.GunesTeminatId = 256;
                //    break;
                //case "328":
                //    ret.GunesPlanId = 823;
                //    ret.GunesTeminatId = 328;
                //    break;
                //case "368":
                //    ret.GunesPlanId = 823;
                //    ret.GunesTeminatId = 368;
                //    break;



                default:
                    break;
            }

            return ret;
        }
        */
        public BenefitReturnType TeminatBenefitMapPusula(string TeminatId)
        {
            BenefitReturnType ret = new BenefitReturnType();
            switch (TeminatId)
            {
                case "":
                    ret.GunesPlanId = 0;
                    ret.GunesTeminatId = 0;
                    break;

                default:
                    break;
            }

            return ret;
        }

        public class BenefitReturnType
        {
            public int GunesPlanId { get; set; }
            public int GunesTeminatId { get; set; }
        }

        private string NarXmlParser(string SearchedTag, string SourceXML)
        {
            string result = "";

            try
            {
                string startKey = string.Format("<{0}>", SearchedTag);
                int basIndex = SourceXML.IndexOf(startKey);
                int bitIndex = SourceXML.IndexOf(string.Format("</{0}>", SearchedTag));
                int startIndex = basIndex + startKey.Length;

                int len = (bitIndex - startIndex);

                if (basIndex < 0)
                    result = "";
                else
                    result = SourceXML.Substring(startIndex, len);
            }
            catch (Exception exx)
            {
            }

            return result;

        }
        private string GetString(string str)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;
            return str;
        }


        private string GetDateWithTime(string date, string hour)
        {
            if (string.IsNullOrEmpty(date))
                return string.Empty;
            DateTime datedate;
            try
            {
                datedate = Convert.ToDateTime(date);
            }
            catch (Exception ex)
            {
                return string.Empty;
            }

            if (hour.Length.Equals(1))
            {
                hour = "0" + hour;
            }

            string retDate = string.Format("{0}-{1}-{2}T" + hour + ":00:00", datedate.Year, datedate.Month.ToString().PadLeft(2, '0'), datedate.Day.ToString().PadLeft(2, '0'));
            return retDate;
        }
        private string GetDecimal(string dcml)
        {
            if (string.IsNullOrEmpty(dcml))
                return string.Empty;
            decimal dec;
            if (!decimal.TryParse(dcml.Replace(',', '.'), NumberStyles.None, new CultureInfo("en-US"), out dec))
                return string.Empty;
            return dec.ToString();
        }

        private string GetDateWithOutTime(string date)
        {
            if (string.IsNullOrEmpty(date))
                return string.Empty;
            DateTime datedate;
            try
            {
                datedate = Convert.ToDateTime(date);
            }
            catch (Exception ex)
            {
                return string.Empty;
            }

            string retDate = string.Format("{0}-{1}-{2}T00:00:000", datedate.Year, datedate.Month.ToString().PadLeft(2, '0'), datedate.Day.ToString().PadLeft(2, '0'));
            return retDate;
        }
        public string CallService(string inputMsg, string url)
        {
            string soapResult = "";
            Stream requestStream = null;
            Stream responseStream = null;
            HttpWebRequest webRequest = null;
            WebResponse webResponse = null;
            StreamReader rd = null;
            try
            {
                webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.Proxy = new WebProxy("185.141.109.27", 808);
                webRequest.ContentType = "text/xml;charset=\"utf-8\"";
                webRequest.Accept = "text/xml";
                webRequest.Method = "POST";

                XmlDocument soapEnvelopeXml = new XmlDocument();
                soapEnvelopeXml.LoadXml(inputMsg);

                requestStream = webRequest.GetRequestStream();
                soapEnvelopeXml.Save(requestStream);

                // begin async call to web request.
                IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

                // suspend this thread until call is complete. You might want to
                // do something usefull here like update your UI.
                asyncResult.AsyncWaitHandle.WaitOne();

                // get the response from the completed web request.

                webResponse = webRequest.EndGetResponse(asyncResult);
                rd = new StreamReader(webResponse.GetResponseStream());

                soapResult = rd.ReadToEnd();
            }
            catch (WebException ex)
            {
                WebResponse errResp = ex.Response;
                using (Stream respStream = errResp.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(respStream);
                    soapResult = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                soapResult = ex.Message;
            }
            finally
            {
                if (requestStream != null)
                {
                    requestStream.Close();
                    requestStream = null;
                }
                if (webResponse != null)
                {
                    webResponse.Close();
                    webResponse = null;
                }
                if (responseStream != null)
                {
                    responseStream.Close();
                    responseStream = null;
                }
                if (rd != null)
                {
                    rd.Close();
                    rd = null;
                }
            }
            return soapResult;



        }

    }

    public enum ProdType
    {
        TEST,
        LIVE
    }

}

