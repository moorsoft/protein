﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.PortTSS.Util.Lib
{
    public class SaglikProvizyonUygunlukKontrolParam
    {
        public long GrupPoliceNo { get; set; }
        public long MusteriId { get; set; }
        public DateTime OlayTarihi { get; set; }
        public long PoliceNo { get; set; }
        public long YenilemeNo { get; set; }
        public string TCKN { get; set; }
    }
}
