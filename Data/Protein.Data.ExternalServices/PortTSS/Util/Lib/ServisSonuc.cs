﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.PortTSS.Util.Lib
{
    public class ServisSonuc
    {
        public string SonucKodu { get; set; }
        public string SonucAciklama { get; set; }
    }
}
