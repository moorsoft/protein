﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Protein.Data.ExternalServices.Protein.ProxySrvModels;

namespace Protein.Data.ExternalServices.PortTSS.Util.Lib
{
    public class ProvizyonGirisParams
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string EpikrizNotu { get; set; }
        public string HasarNotu { get; set; }
        public string Kurum { get; set; }
        public string PoliceNo { get; set; }
        public string TCKimlikNo { get; set; }
        public string TakipNo { get; set; }
        public string TakipTarihi { get; set; }
        public string TelefonNo { get; set; }
        public List<Tani> Tanilar { get; set; }
        public List<TeminatTalep> TeminatTalepleri { get; set; }
        public List<Hizmetler> Hizmetler { get; set; }

    }
}
