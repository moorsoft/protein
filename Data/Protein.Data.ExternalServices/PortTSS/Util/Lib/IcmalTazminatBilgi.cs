﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.PortTSS.Util.Lib
{
    public class IcmalTazminatBilgi
    {
        public string ProvizyonNo { get; set; }
        public string FaturaNo { get; set; }
        public string FaturaTarihi { get; set; }
        public decimal Tutar { get; set; }
    }
}
