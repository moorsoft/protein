﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.PortTSS.Util.Lib
{
    public class TeminatTalep
    {
        public string TeminatKodu { get; set; }
        public decimal TalepTutar { get; set; }
    }
}
