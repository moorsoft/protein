﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.PortTSS.Util.Lib
{
    public class ProvizyonGirisResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string TemTip { get; set; }
        public string ExMusId { get; set; }
        public List<TeminatCevap> TeminatCevaplari { get; set; }
        public ProvizyonGirisBilgi ProvizyonGirisBilgi { get; set; }
        public string AcenteNo { get; set; }
        public string TemPaketNo { get; set; }
    }
}
