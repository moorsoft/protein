﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.PortTSS.Util.Lib
{
    public class IcmalParams
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Kurum { get; set; }
        public string IcmalNo { get; set; }
        public string SgkTakipNo { get; set; }
        public List<IcmalTazminatBilgi> IcmalTazminatBilgiList { get; set; }

    }
}
