﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.PortTSS.Util.Lib
{
    public class TeminatCevap
    {
        public string TeminatKodu { get; set; }
        public decimal? TalepTutar { get; set; }
        public decimal? OnayTutar { get; set; }
        public decimal? SigortaliTutar { get; set; }
        public string DisSistemTemKod { get; set; }

    }
}
