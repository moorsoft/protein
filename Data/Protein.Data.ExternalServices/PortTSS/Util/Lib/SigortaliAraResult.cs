﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.PortTSS.Util.Lib
{
    public class SigortaliAraResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public List<SigortaliBilgi> SigortaliBilgiList { get; set; }
        public List<TeminatBilgi> TeminatBilgiList { get; set; }
    }
}
