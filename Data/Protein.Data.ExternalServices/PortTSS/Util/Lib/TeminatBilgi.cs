﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.PortTSS.Util.Lib
{
    public class TeminatBilgi
    {
        public string TeminatBilgiKey { get; set; }
        public string TeminatKodu { get; set; }
        public string TeminatAdi { get; set; }
        public string TeminatAciklama { get; set; }
    }
}
