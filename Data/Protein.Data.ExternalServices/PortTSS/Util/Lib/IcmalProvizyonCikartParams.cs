﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.PortTSS.Util.Lib
{
    public class IcmalProvizyonCikartParams
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string IcmalNo { get; set; }
        public List<IcmalTazCikartBilgi> IcmalTazCikartBilgi { get; set; }
    }
}
