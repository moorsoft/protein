﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.PortTSS.Util.Lib
{
    public class SigortaliBilgi
    {
        public string SonucKodu { get; set; }
        public string SonucAciklama { get; set; }
        public string SigortaliAdSoyad { get; set; }
        public string PoliceNo { get; set; }
        public string PoliceBitisTarihi { get; set; }
        public string Tarife { get; set; }
        public int VipType { get; set; }
        public string UrunKodu { get; set; }
        public Int32? KalanLimitAdedi { get; set; }
        public decimal? KalanLimitTutari { get; set; }
        public string TeminatBilgiKey { get; set; }
        public string GrupPoliceNo { get; set; }
        public string ExMusteriId { get; set; }
        public string InsuredNo { get; set; }
        public string PolicyIsOpenTheClaim { get; set; }
        public string InsuredIsOpenTheClaim { get; set; }
        public int YenilemeNo { get; set; }
        public long InsuredID { get; set; }
        public long PackageID { get; set; }
        public long PolicyID { get; set; }

    }
}
