﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.PortTSS.Util.Lib
{
    public class SigortaliAraParams
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Kurum { get; set; }
        public string TcKimlikNo { get; set; } //*** zorunlu
        public string AyaktaYatarak { get; set; }
        public string OlayTarihi { get; set; }
    }
}
