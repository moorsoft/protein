﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.PortTSS.Util.Lib
{
    public class Hizmetler
    {
        public string TemNo { get; set; }
        public string HizmetKodu { get; set; }
        public string HizmetAdi { get; set; }
        public decimal HizmetSgkTutar { get; set; }
        public decimal HizmetTalepTutar { get; set; }


        public string DrTescilNo { get; set; }

        public string Brans { get; set; }
    }
}
