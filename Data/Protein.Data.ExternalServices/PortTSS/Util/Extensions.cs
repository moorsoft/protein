﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.PortTSS.Util
{
    public static class Extensions
    {
        public static string FormatDate(this string Value)
        {
            string formattedDate = string.Empty;

            DateTime date;
            if (DateTime.TryParse(Value, out date))
            {
                formattedDate = string.Format("{0}-{1}-{2}", date.Year, date.Month.ToString().PadLeft(2, '0'), date.Day.ToString().PadLeft(2, '0'));
            }
            else
            {
                formattedDate = Value;
            }

            return formattedDate;
        }
    }
}
