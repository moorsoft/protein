﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Protein.Data.ExternalServices.PortTSS
{
    public class InputOutputTypes
    {
        public class SigortaliSorgulaCevap
        {
            public string SonucKodu { get; set; }

            public string SonucAciklama { get; set; }

            public string SigortaliAdSoyad { get; set; }

            public string PoliceNo { get; set; }

            public string PoliceBitisTarihi { get; set; }

            public string Tarife { get; set; }

            public string UrunKodu { get; set; }

            public decimal? KalanLimitAdedi { get; set; }

            public decimal? KalanLimitTutari { get; set; }
            public int? OnOnayVer { get; set; }

            [XmlElement]
            public List<TeminatBilgileri> TanimliTeminatBilgileri { get; set; }

            [XmlElement]
            public List<MuafiyetBilgileri> TanimliMuafiyetBilgileri { get; set; }
        }

        public class TeminatBilgileri
        {
            public string TeminatKodu { get; set; }

            public string TeminatAdi { get; set; }

            public string TeminatAciklama { get; set; }
        }

        public class MuafiyetBilgileri
        {
            public string MuafiyetKodu { get; set; }

            public string MuafiyetAdi { get; set; }
        }

        public class SigortaliSorgula
        {
            //UserName ve Password ile CompanyParameter dan Company Bilgisi Bulunur.
            [Required]
            public string UserName { get; set; }
            [Required]
            public string Password { get; set; }


            [Required]
            public string Kurum { get; set; } //ProviderId
            [Required]
            public string TcKimlikNo { get; set; }
            [Required]
            public string AyaktanYatarak { get; set; } //Ayaktan:A - Yatarak:Y
            [Required]
            public string OlayTarihi { get; set; }
        }

        public class TazminatHesaplaCevap
        {

            public string SonucKodu { get; set; }

            public string SonucAciklama { get; set; }
            public string EkBilgi { get; set; }

            public string ProvizyonNo { get; set; }

            public List<TeminatTalepCevap> TeminatTalepBilgileriCevap { get; set; }

        }

        public class TeminatTalepCevap
        {
            public string TeminatKodu { get; set; }

            public decimal TalepTutar { get; set; }
            public decimal SozlesmeTutar { get; set; }

            public decimal OnayTutar { get; set; }

            public decimal SigortaliTutar { get; set; }
        }

        public class TazminatHesapla
        {
            [Required]
            public string UserName { get; set; }
            [Required]
            public string Password { get; set; }

            public string EpikrizNotu { get; set; }

            public string HasarNotu { get; set; }
            [Required]
            public string Kurum { get; set; }
            [Required]
            public string PoliceNo { get; set; }
            [Required]
            public string TCKimlikNo { get; set; }
            [Required]
            public string TakipNo { get; set; }
            [Required]
            public string TakipTarihi { get; set; }

            public string TlfNo { get; set; }
            [Required]
            public List<Tani> Tanilar { get; set; }
            [Required]
            public List<TeminatTalep> TeminatTalepBilgileri { get; set; }
        }

        public class Tani
        {
            public string TaniTipi { get; set; }

            public string TaniKodu { get; set; }

            public string TaniAdi { get; set; }
        }

        public class TeminatTalep
        {
            public List<HizmetBilgisi> TalepHizmetBilgileri { get; set; }

            public string TeminatKodu { get; set; }

            public decimal TalepTutar { get; set; }
            public decimal SozlesmeTutar { get; set; }
        }

        public class HizmetBilgisi
        {
            public string HizmetKodu { get; set; }

            public string HizmetAdi { get; set; }

            public decimal? HizmetSgkTutar { get; set; }

            public decimal? HizmetTalepTutar { get; set; }
            public decimal? HizmetSozlesmeTutar { get; set; }

            public string DrTescilNo { get; set; }

            public string Brans { get; set; }
        }

        public class ProvizyonIptalCevap
        {
            public string SonucKodu { get; set; }

            public string SonucAciklama { get; set; }

            public string ProvizyonNo { get; set; }
        }

        public class ProvizyonIptal
        {
            [Required]
            public string UserName { get; set; }
            [Required]
            public string Password { get; set; }
            [Required]
            public string Kurum { get; set; }
            [Required]
            public string ProvizyonNo { get; set; }
        }

        public class IcmalGirisCevap
        {
            public string SonucKodu { get; set; }

            public string SonucAciklama { get; set; }
            public string EkBilgi { get; set; }

            public string PaketNo { get; set; }

        }

        public class IcmalBilgileri
        {
            [Required]
            public string UserName { get; set; }
            [Required]
            public string Password { get; set; }
            [Required]
            public string Kurum { get; set; }
            [Required]
            public string IcmalNo { get; set; }

            public string SGKTakipNo { get; set; }
            [Required]
            public List<TazminatBilgisi> TazminatBilgileri { get; set; }
        }

        public class TazminatBilgisi
        {
            public string ProvizyonNo { get; set; }

            public string FaturaNo { get; set; }

            public string FaturaTarihi { get; set; }

            public decimal? Tutar { get; set; }

        }

        public class IcmalTazminatCikarmaCevap
        {
            public string SonucKodu { get; set; }

            public string SonucAciklama { get; set; }
        }

        public class IcmalTazminatBilgileri
        {
            [Required]
            public string UserName { get; set; }
            [Required]
            public string Password { get; set; }
            [Required]
            public string IcmalNo { get; set; }
            [Required]
            public List<Tazminat> TazminatBilgileri { get; set; }

        }

        public class Tazminat
        {
            public string ProvizyonNo { get; set; }
        }

        public class RedDurumBildirimCevap
        {
            public string SonucKodu { get; set; }

            public string SonucAciklama { get; set; }

        }

        public class RedDurumBilgisi
        {
            [Required]
            public string UserName { get; set; }
            [Required]
            public string Password { get; set; }
            [Required]
            public string Kurum { get; set; }
            [Required]
            public string PoliceNo { get; set; }
            [Required]
            public string TCKimlikNo { get; set; }
            [Required]
            public string TakipNo { get; set; }
            public string TakipTarihi { get; set; }

            public string Aciklama { get; set; }

            public string ServisBransKodu { get; set; }

            public List<Tani> Tanilar { get; set; }

            public List<RedDurumHizmetBilgisi> HizmetBilgileri { get; set; }
        }

        public class RedDurumHizmetBilgisi
        {
            public string HizmetKodu { get; set; }

            public string HizmetAdi { get; set; }
        }

        public class RedDurumIptalCevap
        {
            public string SonucKodu { get; set; }

            public string SonucAciklama { get; set; }
        }

        public class RedDurumIptalBilgisi
        {
            [Required]
            public string UserName { get; set; }
            [Required]
            public string Password { get; set; }
            [Required]
            public string Kurum { get; set; }
            [Required]
            public string TakipNo { get; set; }

            public string Aciklama { get; set; }

        }

    }
}


