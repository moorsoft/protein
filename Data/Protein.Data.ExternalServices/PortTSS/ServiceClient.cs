﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Protein.Common.Constants;
using Protein.Common.Entities;
using Protein.Common.Enums;
using Protein.Common.Extensions;
using Protein.Common.Helpers;
using Protein.Common.Messaging;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.InsuranceCompanies;
using Protein.Data.ExternalServices.Log;
using Protein.Data.ExternalServices.PortTSS.Util;
using Protein.Data.ExternalServices.PortTSS.Util.Lib;
using Protein.Data.ExternalServices.Protein;
using Protein.Data.ExternalServices.Validation;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;
using static Protein.Data.ExternalServices.Common.Lib.NN.NNInputOutputTypes;
using static Protein.Data.ExternalServices.PortTSS.InputOutputTypes;

namespace Protein.Data.ExternalServices.PortTSS
{
    public class ServiceClient
    {
        //#region Service methods
        //string GunesApiCode = "A3B5C6C4D6BC44F2A610DF1AA4C64E86"; // ConfigurationManager.AppSettings["GunesApiCodeLive"];
        //string VakifApiCode = "53ED0232A503454AA7248290D9E775A1";
        public string Token { get; set; } = "9999999999";

        #region CreateLog
        private void CreateFailedLog(IntegrationLog log, string responseXml, string desc)
        {
            log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
            log.Type = ((int)LogType.FAILED).ToString();
            log.Response = responseXml;
            log.ResponseStatusCode = "0";
            log.ResponseStatusDesc = desc;
            log.WsResponseDate = DateTime.Now;
            IntegrationLogHelper.AddToLog(log);
        }
        private void CreateOKLog(IntegrationLog log, string responseXml)
        {
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.Type = ((int)LogType.COMPLETED).ToString();
            log.Response = responseXml;
            log.ResponseStatusCode = "200";
            log.ResponseStatusDesc = "SUCCESS";
            log.WsResponseDate = DateTime.Now;
            IntegrationLogHelper.AddToLog(log);
        }
        private void CreateIncomingLog(IntegrationLog log)
        {
            log.HttpStatus = ((int)HttpStatusCode.Continue).ToString();
            log.Type = ((int)LogType.INCOMING).ToString();
            log.ResponseStatusCode = "100";
            log.ResponseStatusDesc = "INCOMING";
            IntegrationLogHelper.AddToLog(log);
        }
        #endregion

        public SigortaliSorgulaCevap[] SigortaliHakSorgula(SigortaliSorgula Sigortali)
        {

            var currentMethod = MethodBase.GetCurrentMethod();
            long providerId = 0;
            long tssContractId = 0;
            long mdpContractId = 0;
            long? CompanyID = 0;

            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = Sigortali.ToXML(true),
                WsRequestDate = DateTime.Now
            };

            List<SigortaliSorgulaCevap> response = new List<SigortaliSorgulaCevap>();
            SigortaliAraResult result = new SigortaliAraResult();

            try
            {
                #region Validation
                ValidationResponse validationResponse = Validator.Worker(Sigortali);

                if (!validationResponse.IsValid)
                {
                    response.Add(new SigortaliSorgulaCevap
                    {
                        SonucKodu = "0",
                        SonucAciklama = validationResponse.Description
                    });

                    CreateFailedLog(log, response.ToXML(true), validationResponse.Description);

                    return response.ToArray();
                }
                #endregion

                log.ProviderId = Convert.ToInt64(Sigortali.Kurum);
                log.IdentityNo = Sigortali.TcKimlikNo.IsInt64() ? (long?)long.Parse(Sigortali.TcKimlikNo) : null;
            }
            catch (Exception ex)
            {
                response.Add(new SigortaliSorgulaCevap
                {
                    SonucKodu = "0",
                    SonucAciklama = PortTssErrors.ValidationInvalid.Split(':')[1]
                });

                CreateFailedLog(log, response.ToXML(true), ex.Message);

                return response.ToArray();
            }

            try
            {
                if (Sigortali.AyaktanYatarak.ToUpper() != "A" && Sigortali.AyaktanYatarak.ToUpper() != "Y")
                {
                    response.Add(new SigortaliSorgulaCevap
                    {
                        SonucKodu = "0",
                        SonucAciklama = "AyaktanYatarak değeri, A = Ayakta , Y = YATARAK olarak girilmeli!"
                    });
                    CreateFailedLog(log, response.ToXML(true), "AyaktanYatarak değeri, A = Ayakta ,  Y = YATARAK olarak girilmeli!");

                    return response.ToArray();
                }

                if (!Sigortali.OlayTarihi.IsDateTime() || !DateTime.TryParse(Sigortali.OlayTarihi, out DateTime dOut))
                {
                    response.Add(new SigortaliSorgulaCevap
                    {
                        SonucKodu = "0",
                        SonucAciklama = "Olay tarihi doğru formatta olmalıdır!"
                    });
                    CreateFailedLog(log, response.ToXML(true), "Olay tarihi doğru formatta olmalıdır!");

                    return response.ToArray();
                }
                else
                {
                    Sigortali.OlayTarihi = DateTime.Parse(Sigortali.OlayTarihi).ToString("yyyy-MM-dd");
                }

                #region User kontrolü
                if (!isValidUser(Sigortali.UserName, Sigortali.Password, ref CompanyID))
                {
                    response.Add(new SigortaliSorgulaCevap
                    {
                        SonucKodu = "0",
                        SonucAciklama = PortTssErrors.UserNameOrPasswordInvalid.Split(':')[1]
                    });
                    CreateFailedLog(log, response.ToXML(true), PortTssErrors.UserNameOrPasswordInvalid.Split(':')[1]);

                    return response.ToArray();
                }
                #endregion
                else
                {
                    log.COMPANY_ID = CompanyID;

                    #region Provider Kontrolü
                    Provider provider = new GenericRepository<Provider>().FindBy($"ID =:providerId", parameters: new { providerId = Sigortali.Kurum }, orderby: "").FirstOrDefault();
                    if (provider == null)
                    {
                        response.Add(new SigortaliSorgulaCevap
                        {
                            SonucKodu = "0",
                            SonucAciklama = PortTssErrors.ProviderNotFound.Split(':')[1]
                        });

                        CreateFailedLog(log, response.ToXML(true), PortTssErrors.ProviderNotFound.Split(':')[1]);

                        return response.ToArray();
                    }
                    providerId = provider.Id;
                    #endregion

                    #region ProviderContractIds
                    V_ProviderContractIds ProviderContractIds = new GenericRepository<V_ProviderContractIds>().FindBy($"PROVIDER_ID =:providerId", parameters: new { providerId = Sigortali.Kurum }, orderby: "",fetchDeletedRows:true,fetchHistoricRows:true).FirstOrDefault();
                    if (ProviderContractIds == null)
                    {
                        response.Add(new SigortaliSorgulaCevap
                        {
                            SonucKodu = "0",
                            SonucAciklama = "Kurum Sözleşme Bilgisi Bulunamadı!"
                        });

                        CreateFailedLog(log, response.ToXML(true), "Kurum Sözleşme Bilgisi Bulunamadı!");

                        return response.ToArray();
                    }
                    tssContractId = !string.IsNullOrEmpty(ProviderContractIds.TSS_CONTRACT_ID.ToString()) ? long.Parse(ProviderContractIds.TSS_CONTRACT_ID.ToString()) : 0;
                    mdpContractId = !string.IsNullOrEmpty(ProviderContractIds.MDP_CONTRACT_ID.ToString()) ? long.Parse(ProviderContractIds.MDP_CONTRACT_ID.ToString()) : 0;
                    if (tssContractId == 0)
                    {
                        response.Add(new SigortaliSorgulaCevap
                        {
                            SonucKodu = "0",
                            SonucAciklama = "Kuruma Ait Aktif Sözleşme bulunamadı!"
                        });

                        CreateFailedLog(log, response.ToXML(true), "Kuruma Ait TSS Sözleşmesi bulunamadı!");

                        return response.ToArray();
                    }
                    #endregion

                    #region Sigortalı Aktif mi ?
                    string whereCondition = $"product_type = '{((int)ProductType.TSS).ToString()}' and COMPANY_ID = {CompanyID} AND POLICY_START_DATE <= TO_DATE('{Sigortali.OlayTarihi} 12:00:00','YYYY-MM-DD HH24:MI:SS') and POLICY_END_DATE >= TO_DATE('{Sigortali.OlayTarihi} 12:00:00','YYYY-MM-DD HH24:MI:SS') AND ENDORSEMENT_START_DATE <= TO_DATE('{Sigortali.OlayTarihi} 12:00:00', 'YYYY-MM-DD HH24:MI:SS') and IDENTITY_NO = {Sigortali.TcKimlikNo} AND STATUS='{((int)Status.AKTIF).ToString()}'";

                    List<V_InsuredEndorsement> insuredList = new GenericRepository<V_InsuredEndorsement>().FindBy(conditions: whereCondition, orderby: "");
                    if (insuredList == null || insuredList.Count < 1)
                    {
                        whereCondition = $" product_type = '{((int)ProductType.TSS).ToString()}' and  insured_is_open_to_claim = '1' and policy_is_open_to_claim = '1' AND POLICY_START_DATE <= TO_DATE('{Sigortali.OlayTarihi} 12:00:00','YYYY-MM-DD HH24:MI:SS') and POLICY_END_DATE >= TO_DATE('{Sigortali.OlayTarihi} 12:00:00','YYYY-MM-DD HH24:MI:SS') and IDENTITY_NO = {Sigortali.TcKimlikNo} AND ENDORSEMENT_START_DATE <= TO_DATE('{Sigortali.OlayTarihi} 12:00:00', 'YYYY-MM-DD HH24:MI:SS') AND STATUS='{((int)Status.AKTIF).ToString()}' AND COMPANY_ID!={CompanyID}";

                        insuredList = new GenericRepository<V_InsuredEndorsement>().FindBy(conditions: whereCondition, orderby: "");
                        if (insuredList.Count > 0)
                        {
                            response.Add(new SigortaliSorgulaCevap
                            {
                                SonucKodu = "0",
                                SonucAciklama = "Şirket bilgilerini kontrol ediniz. Bu sigortalının başka bir sigorta şirketinde (" + insuredList[0].COMPANY_NAME + ") aktif poliçesi bulunmaktadır."
                            });
                        }
                        else
                        {
                            response.Add(new SigortaliSorgulaCevap
                            {
                                SonucKodu = "0",
                                SonucAciklama = Sigortali.TcKimlikNo + " TC kimlik numarasına ait aktif poliçe bulunmamaktadır"
                            });
                        }

                        CreateFailedLog(log, response.ToXML(true), PortTssErrors.InsuredNotFound.Split(':')[1]);

                        return response.ToArray();
                    }
                    result.SigortaliBilgiList = InsuredEndorsementToSigortaliAraResult(insuredList);

                    #endregion

                    #region Servis Kontrolü
                    var _Company = new CompanyHelper().WhichCompany((long)CompanyID);
                    foreach (SigortaliBilgi sigBilgi in result.SigortaliBilgiList)
                    {
                        sigBilgi.SonucKodu = "1";

                        //SaglikProvizyonUygunlukKontrolParam paramPusula = new SaglikProvizyonUygunlukKontrolParam();
                        //paramPusula.GrupPoliceNo;
                        //paramPusula.MusteriId;

                        //paramPusula.YenilemeNo = 2;
                        //paramPusula.GrupPoliceNo = 1433731;
                        //paramPusula.MusteriId = 20017745432;
                        //paramPusula.OlayTarihi = DateTime.Parse("2017-11-30");
                        //paramPusula.PoliceNo = 1433731;

                        //paramPusula.YenilemeNo = sigBilgi.YenilemeNo;
                        //paramPusula.TCKN = Sigortali.TcKimlikNo;
                        //paramPusula.OlayTarihi = Convert.ToDateTime(Sigortali.OlayTarihi);
                        //paramPusula.PoliceNo = long.Parse(sigBilgi.PoliceNo);
                        //paramPusula.MusteriId = long.Parse(new ContactWorker().GetCompanyContactIdByContactId(long.Parse(sigBilgi.ExMusteriId), CompanyEnum.GUNES_SIGORTA));
                        //paramPusula.GrupPoliceNo = paramPusula.PoliceNo;

                        #region CompanyCheck
                        //if (_Company == CompanyEnum.GUNES_SIGORTA) // Vakıf gelecek buraya
                        //{
                        //    #region GunesHelper
                        //    CompanyParam companyParam = new Common.Helper.CompanyHelper().GetCompanyParameters(long.Parse(CompanyID.ToString()));

                        //    GunesHelper gunesHelper = new GunesHelper(companyParam: companyParam);
                        //    #endregion

                        //    SaglikProvizyonUygunlukKontrolResult gunesResult = gunesHelper.ProvKontrol(paramPusula);

                        //    if (!gunesResult.SonucKodu.Equals("0")) //olumsuz ise 
                        //    {
                        //        sigBilgi.SonucKodu = "0";
                        //        sigBilgi.SonucAciklama += " " + gunesResult.SonucAciklama;
                        //        //hepsi olumsuz ise süreç bitir
                        //    }
                        //    else
                        //    {
                        //        sigBilgi.SonucKodu = "1";
                        //        sigBilgi.SonucAciklama += " " + gunesResult.SonucAciklama;
                        //    }
                        //}
                        if (sigBilgi.InsuredNo.IsInt64() && long.Parse(sigBilgi.InsuredNo) > 0)
                        {
                            if (_Company == CompanyEnum.NN)
                            {
                                NNHayatServiceClientV2 client = new NNHayatServiceClientV2();
                                NNHakSahiplikReq input = new NNHakSahiplikReq();
                                input.Kurum = Sigortali.Kurum;
                                input.OlayTarihi = Sigortali.OlayTarihi;
                                input.TcKimlikNo = Sigortali.TcKimlikNo.ToString();
                                input.AyaktanYatarak = Sigortali.AyaktanYatarak;

                                CommonProxyRes res = client.PolicyState(input);
                                if (!res.IsOk)
                                {
                                    sigBilgi.SonucKodu = "0";
                                    sigBilgi.SonucAciklama = res.Description;
                                }
                            }
                            else if (_Company == CompanyEnum.KATILIM_EMEKLILIK)
                            {
                                ProxyServiceClient katilimServiceClient = new ProxyServiceClient();
                                PolicyStateReq policyStateReq = new PolicyStateReq();

                                policyStateReq.IncidentDate = DateTime.Parse(Sigortali.OlayTarihi);
                                policyStateReq.PolicyNo = sigBilgi.PoliceNo.ToString();
                                policyStateReq.CompanyCode = CompanyID.ToString();
                                policyStateReq.RenewalNo = sigBilgi.YenilemeNo;
                                policyStateReq.InsuredNo = sigBilgi.InsuredNo;
                                CommonProxyRes commonProxyRes = katilimServiceClient.PolicyState(policyStateReq);
                                if (!commonProxyRes.IsOk)
                                {
                                    sigBilgi.SonucKodu = "0";
                                    sigBilgi.SonucAciklama += commonProxyRes.Error.Length > 0 ? string.Join(", ", commonProxyRes.Error) : commonProxyRes.Description;
                                }
                            }
                            else if (_Company == CompanyEnum.HDI || _Company == CompanyEnum.HDI_TEST)
                            {
                                ProxyServiceClient serviceClient = new ProxyServiceClient();
                                PolicyStateReq policyStateReq = new PolicyStateReq
                                {
                                    CompanyCode = CompanyID.ToString(),
                                    IncidentDate = DateTime.Parse(Sigortali.OlayTarihi),
                                    InsuredNo = sigBilgi.InsuredNo,
                                    PolicyNo = sigBilgi.PoliceNo.ToString(),
                                    RenewalNo = sigBilgi.YenilemeNo,
                                };
                                var policyStateRes = serviceClient.PolicyState(policyStateReq);
                                if (!policyStateRes.IsOk)
                                {
                                    sigBilgi.SonucKodu = "0";
                                    sigBilgi.SonucAciklama += policyStateRes.Error.Length > 0 ? string.Join(", ", policyStateRes.Error) : policyStateRes.Description;
                                }
                            }
                        }
                        else
                        {
                            sigBilgi.SonucKodu = "1";
                        }
                        #endregion
                    }
                    #endregion
                }


                #region InsuredPackageRemaining - ContractNetwork
                bool FindHim = false, findCompany = false, findInsuredOpenTheClaim = false, findCovarage = false;
                Sigortali.AyaktanYatarak = Sigortali.AyaktanYatarak.ToUpper() == "Y" ? "Yatarak" : "Ayakta";
                foreach (SigortaliBilgi sigBilgi in result.SigortaliBilgiList)
                {
                    if ((sigBilgi.InsuredIsOpenTheClaim != "1" || sigBilgi.PolicyIsOpenTheClaim != "1") && CompanyID == 1)
                    {
                        findInsuredOpenTheClaim = true;
                        response.Add(new SigortaliSorgulaCevap { SonucAciklama = " Provizyon işlemleri için 0212 978 14 40 numaralı provizyon merkezini arayınız. " + sigBilgi.SonucAciklama, SonucKodu = "0" });
                    }
                    else if (sigBilgi.SonucKodu == "1")
                    {
                        findCompany = true;
                        SigortaliSorgulaCevap retObj = new SigortaliSorgulaCevap();
                        Dictionary<string, string> CoverageType = LookupHelper.GetLookupData(LookupTypes.Coverage);
                        KeyValuePair<string, string> resultCoverageType = CoverageType.Where(x => x.Value == Sigortali.AyaktanYatarak).FirstOrDefault();

                        #region InsuredPackageRemaining
                        string whereCondition = $"PACKAGE_ID = {sigBilgi.PackageID} AND INSURED_ID = {sigBilgi.InsuredID} AND COVERAGE_TYPE = '{resultCoverageType.Key}'";
                        List<V_InsuredPackageRemaining> InsuredPackageRemaningList = new GenericRepository<V_InsuredPackageRemaining>().FindBy(conditions: whereCondition, orderby: "", fetchHistoricRows: true, fetchDeletedRows: true);
                        if (InsuredPackageRemaningList == null) continue;

                        V_InsuredPackageRemaining InsuredPackageRemaning = InsuredPackageRemaningList.Where(x => x.IsMainCoverage == 1).FirstOrDefault();
                        if (InsuredPackageRemaning == null) continue;
                        if (InsuredPackageRemaning.CoverageName.IsNull()) continue;
                        findCovarage = true;

                        #endregion

                        #region ContractNetwork
                        whereCondition = $"COMPANY_ID = {CompanyID} AND (CONTRACT_ID = {tssContractId} OR CONTRACT_ID={mdpContractId}) AND NETWORK_ID = {InsuredPackageRemaning.NetworkId}";
                        V_ContractNetwork ContractNetwork = new GenericRepository<V_ContractNetwork>().FindBy(conditions: whereCondition, orderby: "NETWORK_ID").FirstOrDefault();
                        if (ContractNetwork == null) continue;
                        if (string.IsNullOrEmpty(ContractNetwork.NETWORK_NAME)) continue;
                        #endregion

                        FindHim = true;
                        retObj.KalanLimitAdedi = InsuredPackageRemaning.AgrDayLimitRemaining != null ? decimal.Parse(InsuredPackageRemaning.AgrDayLimitRemaining.ToString()) : (InsuredPackageRemaning.AgrNumberLimitRemaining != null ? decimal.Parse(InsuredPackageRemaning.AgrNumberLimitRemaining.ToString()) : (InsuredPackageRemaning.AgrSessionCountRemaining != null ? (decimal?)decimal.Parse(InsuredPackageRemaning.AgrSessionCountRemaining.ToString()) : null));

                        retObj.KalanLimitTutari = InsuredPackageRemaning.AgrPriceLimitRemaining != null ? (decimal.Parse(InsuredPackageRemaning.AgrPriceLimitRemaining.ToString()) > 999999 ? null : (decimal?)decimal.Parse(InsuredPackageRemaning.AgrPriceLimitRemaining.ToString())) : 0;

                        retObj.OnOnayVer = sigBilgi.VipType;
                        retObj.PoliceBitisTarihi = sigBilgi.PoliceBitisTarihi.IsDateTime() ? (DateTime.Parse(sigBilgi.PoliceBitisTarihi).ToString("yyyy-MM-dd")) : "";
                        retObj.PoliceNo = sigBilgi.PoliceNo;
                        retObj.SigortaliAdSoyad = sigBilgi.SigortaliAdSoyad;
                        retObj.SonucKodu = "1";
                        if (InsuredPackageRemaning.PackageNo == "3060")
                        {
                            retObj.UrunKodu = "TKSZ";
                            retObj.Tarife = "4";
                            retObj.SonucAciklama = "TAKİPSİZ PROVİZYON";

                        }
                        else if (InsuredPackageRemaning.PackageNo == "930")
                        {
                            retObj.UrunKodu = "0"; //ISUOGR
                            retObj.Tarife = "0"; //2 Olmalı
                            retObj.SonucAciklama = "İSÜ ÖĞRENCİLERİ";
                        }
                        else if (InsuredPackageRemaning.PackageNo == "2001")
                        {
                            retObj.UrunKodu = "KDE";
                            retObj.Tarife = "3";
                            retObj.SonucAciklama = "EREĞLİ GRUBU";
                        }
                        else if (InsuredPackageRemaning.ProductCode == "NPN_MDP" || InsuredPackageRemaning.ProductCode == "HS_MDP" || InsuredPackageRemaning.ProductCode == "NPN_ALZ" || InsuredPackageRemaning.ProductCode == "NPN_HMO")
                        {
                            if (InsuredPackageRemaning.CompanyId == 3)
                            {
                                if (InsuredPackageRemaning.PackageNo == "3200")
                                {
                                    retObj.UrunKodu = "";
                                    retObj.Tarife = "6";
                                    retObj.SonucAciklama = "AİLEMİZE SAĞLIK";
                                    retObj.OnOnayVer = 1;
                                    retObj.KalanLimitAdedi = retObj.KalanLimitAdedi == null ? 99 : retObj.KalanLimitAdedi;
                                }
                                else if (InsuredPackageRemaning.PackageNo == "3202")
                                {
                                    retObj.UrunKodu = "";
                                    retObj.Tarife = "6";
                                    retObj.SonucAciklama = "GENİŞ AİLEM";
                                    retObj.OnOnayVer = 1;
                                    retObj.KalanLimitAdedi = retObj.KalanLimitAdedi == null ? 99 : retObj.KalanLimitAdedi;

                                }
                                else if (InsuredPackageRemaning.PackageNo == "3201")
                                {
                                    retObj.UrunKodu = "";
                                    retObj.Tarife = "7";
                                    retObj.SonucAciklama = "GENİŞ AİLEM";
                                    retObj.OnOnayVer = 1;
                                    retObj.KalanLimitAdedi = retObj.KalanLimitAdedi == null ? 99 : retObj.KalanLimitAdedi;

                                }
                                else
                                {
                                    retObj.UrunKodu = "";
                                    retObj.Tarife = "5"; //1 Olmalı
                                    retObj.SonucAciklama = "HMO";
                                }
                            }
                            else
                            {
                                retObj.UrunKodu = "";
                                retObj.Tarife = "5"; //1 Olmalı
                                retObj.SonucAciklama = "HMO";
                            }
                        }
                        else
                        {
                            retObj.UrunKodu = "";
                            retObj.Tarife = "0";
                            retObj.SonucAciklama = "";
                        }
                        retObj.KalanLimitAdedi = retObj.KalanLimitAdedi == null ? 0 : retObj.KalanLimitAdedi;

                        retObj.TanimliTeminatBilgileri = InsuredCoverageToTeminatBilgileri(InsuredPackageRemaningList);
                        retObj.TanimliMuafiyetBilgileri = InsuredCoverageToMuafiyetBilgileri(sigBilgi.InsuredID);
                        log.InsuredId = sigBilgi.InsuredID;
                        log.PolicyId = sigBilgi.PolicyID;
                        response.Add(retObj);
                    }
                    else
                    {
                        FindHim = true;
                        findCompany = false;

                        response.Add(new SigortaliSorgulaCevap { SonucAciklama = "Sigortalı Bağlı Bulunduğu Şirkette Aktif Değil! Sebebi = " + sigBilgi.SonucAciklama, SonucKodu = "0" });
                    }
                }

                if (findInsuredOpenTheClaim)
                {
                    CreateFailedLog(log, response.ToXML(true), "Sigortalı Hasara Kapalı");

                    return response.ToArray();
                }

                if (!findCompany)
                {
                    CreateFailedLog(log, response.ToXML(true), "Sigortalı Bağlı Bulunduğu Şirkette Aktif Değil!");

                    return response.ToArray();
                }

                if (!findCovarage)
                {
                    response.Add(new SigortaliSorgulaCevap { SonucAciklama = Sigortali.AyaktanYatarak + " tedavi teminatı bulunmamaktadır", SonucKodu = "0" });

                    CreateFailedLog(log, response.ToXML(true), Sigortali.AyaktanYatarak + " tedavi teminatı bulunmamaktadır");

                    return response.ToArray();
                }

                if (!FindHim)
                {
                    response.Add(new SigortaliSorgulaCevap { SonucAciklama = "Poliçe kurumunuzda geçerli değildir", SonucKodu = "0" });

                    CreateFailedLog(log, response.ToXML(true), "Poliçe kurumunuzda geçerli değildir");

                    return response.ToArray();
                }
                #endregion

                CreateOKLog(log, response.ToXML(true));
            }

            catch (Exception ex)
            {
                SigortaliSorgulaCevap cevap = new SigortaliSorgulaCevap
                {
                    SonucKodu = "0",
                    SonucAciklama = "HATA OLUŞTU! LÜTFEN TEKRAR DENEYİNİZ..."
                };
                response.Add(cevap);

                CreateFailedLog(log, response.ToXML(true), $"Error: {ex.Message} StackTrace: {ex.StackTrace}");
            }
            return response.ToArray();
        }
        public TazminatHesaplaCevap TamamlayiciSaglikTazminatGiris(TazminatHesapla Tazminat)
        {
            long providerId = 0;
            long? CompanyID = 0;
            long tssContractId = 0;
            long mdpContractId = 0;
            long claimId = 0;
            long processGroupId = 0;

            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = Tazminat.ToXML(true),
                WsRequestDate = DateTime.Now
            };

            TazminatHesaplaCevap result = new TazminatHesaplaCevap();
            ProvizyonGirisResult ProteinResult = new ProvizyonGirisResult();

            try
            {
                #region Validation
                ValidationResponse validationResponse = Validator.Worker(Tazminat);

                if (!validationResponse.IsValid)
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = validationResponse.Description;

                    CreateFailedLog(log, result.ToXML(true), validationResponse.Description);

                    return result;
                }
                #endregion

                log.ProviderId = Convert.ToInt64(Tazminat.Kurum);
                log.MedulaNo = Tazminat.TakipNo;
                log.IdentityNo = Tazminat.TCKimlikNo.IsInt64() ? (long?)long.Parse(Tazminat.TCKimlikNo) : null;
            }
            catch (Exception ex)
            {
                result.SonucKodu = "0";
                result.SonucAciklama = PortTssErrors.ValidationInvalid.Split(':')[1];

                CreateFailedLog(log, result.ToXML(true), ex.Message);

                return result;
            }

            try
            {
                string whereCondition = "";

                #region Validation

                #region User kontrolü
                if (!isValidUser(Tazminat.UserName, Tazminat.Password, ref CompanyID))
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = PortTssErrors.UserNameOrPasswordInvalid.Split(':')[1];

                    CreateFailedLog(log, result.ToXML(true), PortTssErrors.UserNameOrPasswordInvalid.Split(':')[1]);

                    return result;
                }
                log.COMPANY_ID = CompanyID;
                #endregion

                #region DateTimeKontrol
                Tazminat.TakipTarihi = DateTime.Parse(Tazminat.TakipTarihi).ToString("yyyy-MM-dd");
                #endregion

                #region Hizmet Bilgileri Dolu mu Kontrolü?
                if (Tazminat.TeminatTalepBilgileri == null || Tazminat.TeminatTalepBilgileri.Count < 1)
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = PortTssErrors.ProcessCoverageNotSent.Split(':')[1];

                    CreateFailedLog(log, result.ToXML(true), PortTssErrors.ProviderNotFound.Split(':')[1]);

                    return result;
                }
                #endregion

                #region Provider Kontrolü
                Provider provider = new GenericRepository<Provider>().FindBy($"ID =:providerId", parameters: new { providerId = Tazminat.Kurum }, orderby: "").FirstOrDefault();
                if (provider == null)
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = PortTssErrors.ProviderNotFound.Split(':')[1];

                    CreateFailedLog(log, result.ToXML(true), PortTssErrors.ProviderNotFound.Split(':')[1]);

                    return result;
                }
                providerId = provider.Id;
                #endregion

                #region ProviderContractIds
                V_ProviderContractIds ProviderContractIds = new GenericRepository<V_ProviderContractIds>().FindBy($"PROVIDER_ID =:providerId", parameters: new { providerId = Tazminat.Kurum }, orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (ProviderContractIds == null)
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = "Kuruma Ait Aktif Sözleşme bulunamadı!";

                    CreateFailedLog(log, result.ToXML(true), "Kuruma Ait Aktif Sözleşme bulunamadı!");

                    return result;
                }
                tssContractId = !string.IsNullOrEmpty(ProviderContractIds.TSS_CONTRACT_ID.ToString()) ? long.Parse(ProviderContractIds.TSS_CONTRACT_ID.ToString()) : 0;
                mdpContractId = !string.IsNullOrEmpty(ProviderContractIds.MDP_CONTRACT_ID.ToString()) ? long.Parse(ProviderContractIds.MDP_CONTRACT_ID.ToString()) : 0;
                if (tssContractId == 0)
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = "Kuruma Ait Aktif Sözleşme bulunamadı!";

                    CreateFailedLog(log, result.ToXML(true), "Kuruma Ait Aktif Sözleşme bulunamadı!");

                    return result;
                }
                processGroupId = new ClaimWorker().GetContractProcessGroupId(tssContractId);
                #endregion

                #region Sigortalı Aktif mi ? 
                whereCondition = $" product_type = '{((int)ProductType.TSS).ToString()}' AND COMPANY_ID = {CompanyID} AND POLICY_NUMBER = {Tazminat.PoliceNo} AND IDENTITY_NO = {Tazminat.TCKimlikNo} AND POLICY_START_DATE <= TO_DATE('{Tazminat.TakipTarihi} 12:00:00','YYYY-MM-DD HH24:MI:SS') AND POLICY_END_DATE >= TO_DATE('{Tazminat.TakipTarihi} 12:00:00', 'YYYY-MM-DD HH24:MI:SS') AND ENDORSEMENT_START_DATE <= TO_DATE('{Tazminat.TakipTarihi} 12:00:00', 'YYYY-MM-DD HH24:MI:SS') AND STATUS='{((int)Status.AKTIF).ToString()}'";

                V_InsuredEndorsement vInsured = new GenericRepository<V_InsuredEndorsement>().FindBy(conditions: whereCondition, orderby: "ENDORSEMENT_ID DESC").FirstOrDefault();
                if (vInsured == null)
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = PortTssErrors.InsuredNotFound.Split(':')[1];

                    CreateFailedLog(log, result.ToXML(true), PortTssErrors.InsuredNotFound.Split(':')[1]);

                    return result;
                }
                if ((vInsured.POLICY_IS_OPEN_TO_CLAIM != "1" || vInsured.INSURED_IS_OPEN_TO_CLAIM != "1") && CompanyID == 1)
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = "Provizyon işlemleri için 0212 978 14 40 numaralı provizyon merkezini arayınız.";

                    CreateFailedLog(log, result.ToXML(true), "Hasara Kapalı");

                    return result;
                }
                #endregion

                #region Claim var mı ?
                var claimWorker = new ClaimWorker();
                claimId = claimWorker.ExistsClaim(Tazminat.TakipNo);
                if (claimId > 0)
                {
                    var v_claim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID=:claimId", parameters: new { claimId }, orderby: "", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                    if (v_claim.PAYROLL_ID != null)
                    {
                        result.SonucKodu = "0";
                        result.SonucAciklama = "Zarflanmış Tazminat ile İşlem Yapılamaz.";

                        CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                        return result;
                    }
                    if (v_claim.STATUS == ((int)ClaimStatus.ODENECEK).ToString() || v_claim.STATUS == ((int)ClaimStatus.ODENDI).ToString())
                    {
                        result.SonucKodu = "0";
                        result.SonucAciklama = "Durumu ODENECEK/ODENDI olan tazminatta işlem yapılamaz.";

                        CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                        return result;
                    }
                    if (v_claim.PROVIDER_ID != long.Parse(Tazminat.Kurum))
                    {
                        result.SonucKodu = "0";
                        result.SonucAciklama = "Tazminat Daha önce Aynı No ile Farklı Kurum İçin Gönderilmiştir.";

                        CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                        return result;
                    }
                    if (v_claim.POLICY_NUMBER.ToString() != Tazminat.PoliceNo)
                    {
                        result.SonucKodu = "0";
                        result.SonucAciklama = "Tazminat Daha önce Aynı No ile Farklı Poliçe İçin Gönderilmiştir.";

                        CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                        return result;
                    }
                    if (v_claim.IDENTITY_NO.ToString() != Tazminat.TCKimlikNo)
                    {
                        result.SonucKodu = "0";
                        result.SonucAciklama = "Tazminat Daha önce Aynı No ile Farklı Sigortalı İçin Gönderilmiştir.";

                        CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                        return result;
                    }
                    if (v_claim.STATUS == ((int)ClaimStatus.PROZIYON_ONAY).ToString() || v_claim.STATUS == ((int)ClaimStatus.RET).ToString())
                    {
                        if (v_claim.PAID == Tazminat.TeminatTalepBilgileri.Select(t => t.TalepTutar).Sum())
                        {
                            int claimProcessCount = 0;
                            foreach (var item in Tazminat.TeminatTalepBilgileri)
                            {
                                claimProcessCount += item.TalepHizmetBilgileri.Count();
                            }
                            Tazminat.TeminatTalepBilgileri.Select(t => t.TalepHizmetBilgileri).Count();
                            List<ClaimProcess> claimProcesses = new GenericRepository<ClaimProcess>().FindBy($"CLAIM_ID={claimId}");
                            if (claimProcesses.Count == claimProcessCount)
                            {
                                List<ProcessList> processListExist = ProcessListHelper.GetProcessListbyType(((int)ProcessListType.SUT).ToString());
                                string processListId = "";
                                if (processListExist != null)
                                {
                                    processListId = string.Join(",", processListExist.Select(x => x.Id));
                                }

                                List<ClaimProcess> claimProcessListExist = new List<ClaimProcess>();

                                whereCondition = $"CLAIM_ID={claimId}";
                                List<V_ClaimProcess> responseProcessList = new GenericRepository<V_ClaimProcess>().FindBy(whereCondition, orderby: "");
                                var change = false;
                                foreach (InputOutputTypes.TeminatTalep coverage in Tazminat.TeminatTalepBilgileri)
                                {
                                    long CoverageId = new CoverageWorker().GetCoverageID(coverage.TeminatKodu, claimId);
                                    if (CoverageId != 0)
                                    {
                                        if (coverage.TalepHizmetBilgileri.Count > 0)
                                        {
                                            var sutCodeList = string.Join("','", coverage.TalepHizmetBilgileri.Select(t => t.HizmetKodu));

                                            foreach (HizmetBilgisi processInfo in coverage.TalepHizmetBilgileri)
                                            {
                                                if (!string.IsNullOrEmpty(processInfo.HizmetKodu))
                                                {
                                                    V_ClaimProcess responseProcess = responseProcessList.Where(p => p.PROCESS_CODE == processInfo.HizmetKodu).FirstOrDefault();
                                                    if (responseProcess != null)
                                                    {
                                                        var a = claimProcesses.Where(c => c.CoverageId == CoverageId && c.ProcessId == responseProcess.PROCESS_ID).FirstOrDefault();
                                                        if (a == null)
                                                        {
                                                            change = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        change = true;
                                                    }
                                                }
                                                else
                                                {
                                                    change = true;
                                                }

                                                if (change)
                                                {
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            change = true;
                                        }
                                    }
                                    else
                                    {
                                        change = true;
                                    }

                                    if (change)
                                    {
                                        break;
                                    }
                                }
                                if (!change)
                                {
                                    result.ProvizyonNo = claimId.ToString();
                                    result.SonucKodu = "1";
                                    result.SonucAciklama = "işlem başarıyla sonuçlandı";
                                    if (v_claim.COMPANY_ID == 3 && v_claim.PRODUCT_CODE == "NPN_MDP")
                                    {
                                        if (v_claim.POLICY_ISSUE_DATE != null && (DateTime)v_claim.POLICY_ISSUE_DATE < DateTime.Parse("01.05.2018"))
                                        {
                                            result.EkBilgi = "Lütfen bu provizyon için Faturanızı IMECE DESTEK ticari ünvanı adına kesiniz!";
                                        }
                                        else
                                        {
                                            result.EkBilgi = "Lütfen bu provizyon için faturanızı NİPPON SİGORTA ticari ünvanı adına kesiniz!";
                                        }
                                    }
                                    #region result Coverage 
                                    List<V_ClaimCoverage> claimCoverageeList = new List<V_ClaimCoverage>();
                                    claimCoverageeList = new GenericRepository<V_ClaimCoverage>().FindBy(conditions: $"CLAIM_ID = {claimId}", orderby: "");

                                    if (claimCoverageeList.Count > 0)
                                    {
                                        log.ClaimId = claimId;
                                        log.PolicyId = vInsured.POLICY_ID;
                                        log.InsuredId = vInsured.INSURED_ID;
                                        var coverageIdList = string.Join("','", claimCoverageeList.Select(t => t.CoverageId));
                                        result.TeminatTalepBilgileriCevap = new List<TeminatTalepCevap>();

                                        var coverParamList = new CoverageWorker().GetPortTssCoverageCode(coverageIdList);

                                        var idList = new List<long>();
                                        foreach (V_ClaimCoverage cover in claimCoverageeList)
                                        {
                                            if (!idList.Contains(cover.CoverageId))
                                            {
                                                var param = coverParamList.Where(c => c.CoverageId == cover.CoverageId).FirstOrDefault();
                                                var totalPaid = claimCoverageeList.Where(c => c.CoverageId == cover.CoverageId).Sum(t => t.Paid);
                                                var totalRequest = claimCoverageeList.Where(c => c.CoverageId == cover.CoverageId).Sum(t => t.Requested);
                                                var totalInsured = totalRequest - totalPaid;

                                                TeminatTalepCevap temTalepCevap = new TeminatTalepCevap
                                                {
                                                    OnayTutar = totalPaid,
                                                    SigortaliTutar = totalInsured,
                                                    TeminatKodu = param != null ? param.Value : "",
                                                    TalepTutar = totalRequest
                                                };
                                                result.TeminatTalepBilgileriCevap.Add(temTalepCevap);

                                                idList.Add(cover.CoverageId);
                                            }
                                        }
                                    }
                                    #endregion

                                    CreateOKLog(log, result.ToXML(true));

                                    return result;
                                }
                            }
                        }
                    }


                    new ClaimWorker().ClaimExistOperation(claimId);
                    //if (HttpContext.Current != null)
                    //    HttpContext.Current.Response.AppendToLog("_TamamlayiciSaglikTazminatGiris__Step6.2_DeleteOldClaimInfo__" + log.CorrelationId + "__" + DateTime.Now.ToString("dd-MM-yyyy HH:mm-ss fff") + "\n");
                }

                #endregion

                #endregion

                #region Worker

                #region InsertNewClaim
                Claim claim = new Claim
                {
                    Id = claimId,
                    NoticeDate = DateTime.Now,
                    ProviderId = providerId,
                    InsuredId = vInsured.INSURED_ID,
                    Type = ((int)ClaimType.TSS).ToString(),
                    Status = ((int)ClaimStatus.GIRIS).ToString(),
                    MedulaNo = Tazminat.TakipNo,
                    SourceType = ((int)ClaimSourceType.PORTTSS).ToString(),
                    PaymentMethod = ((int)ClaimPaymentMethod.KURUM).ToString(),
                    ClaimDate = Convert.ToDateTime(Tazminat.TakipTarihi),
                    StaffId = null,
                    PackageId = vInsured.PACKAGE_ID,
                    CompanyId = CompanyID,
                    PolicyId = vInsured.POLICY_ID
                };
                SpResponse spResponseClaim = new GenericRepository<Claim>().Insert(claim, this.Token);
                if (spResponseClaim.Code == "100")
                {
                    claimId = spResponseClaim.PkId;
                    claim.Id = claimId;
                    result.ProvizyonNo = claimId.ToString();
                }
                else
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = spResponseClaim.Message;
                    CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                    return result;
                }
                log.ClaimId = claimId;
                log.PolicyId = vInsured.POLICY_ID;
                log.InsuredId = vInsured.INSURED_ID;
                #endregion

                #region Insured Phone
                if (!Tazminat.TlfNo.IsNull())
                {
                    List<ISpExecute3> lst = new List<ISpExecute3>();

                    var VcontactPhone = new GenericRepository<V_ContactPhone>().FindBy($"CONTACT_ID={vInsured.CONTACT_ID} AND IS_PRIMARY='1' AND PHONE_TYPE='{((int)PhoneType.MOBIL).ToString()}'", orderby: "").FirstOrDefault();
                    var phone = new Phone
                    {
                        Id = VcontactPhone != null ? VcontactPhone.PHONE_ID : 0,
                        Type = ((int)PhoneType.MOBIL).ToString(),
                        CountryId = 222,
                        No = Tazminat.TlfNo,
                        IsPrimary = "1"
                    };
                    lst.Add(phone);
                    if (VcontactPhone == null)
                    {
                        var contactPhone = new ContactPhone
                        {
                            ContactId = vInsured.CONTACT_ID
                        };
                        lst.Add(contactPhone);
                    }
                    var spResponseISpExecute3 = new GenericRepository<ISpExecute3>().InsertSpExecute3(lst);

                }
                #endregion

                #region ClaimNotes
                List<string> epikrizNote = new ClaimWorker().ClaimNoteSplitter(Tazminat.EpikrizNotu);
                if (epikrizNote.Count > 0)
                {
                    List<ISpExecute3> lst = new List<ISpExecute3>();

                    foreach (string epikriz in epikrizNote)
                    {
                        Note note = new Note
                        {
                            Description = epikriz,
                            Status = ((int)Status.AKTIF).ToString(),
                            Type = ((int)ClaimNoteType.EPIKRIZ).ToString()
                        };
                        lst.Add(note);

                        ClaimNote clmNote = new ClaimNote
                        {
                            Id = 0,
                            Status = ((int)Status.AKTIF).ToString(),
                            ClaimId = claimId,
                            Type = ((int)ClaimNoteType.EPIKRIZ).ToString()
                        };
                        lst.Add(clmNote);

                        var spResponseISpExecute3 = new GenericRepository<ISpExecute3>().InsertSpExecute3(lst);
                    }

                }
                List<string> claimNotes = new ClaimWorker().ClaimNoteSplitter(Tazminat.HasarNotu);
                if (claimNotes.Count > 0)
                {
                    List<ISpExecute3> lst = new List<ISpExecute3>();

                    foreach (string claimNote in claimNotes)
                    {
                        Note note = new Note
                        {
                            Description = claimNote,
                            Status = ((int)Status.AKTIF).ToString(),
                            Type = ((int)ClaimNoteType.NORMAL).ToString()
                        };
                        lst.Add(note);

                        ClaimNote clmNote = new ClaimNote
                        {
                            Id = 0,
                            Status = ((int)Status.AKTIF).ToString(),
                            ClaimId = claimId,
                            Type = ((int)ClaimNoteType.NORMAL).ToString()
                        };
                        lst.Add(clmNote);

                        var spResponseISpExecute3 = new GenericRepository<ISpExecute3>().InsertSpExecute3(lst);
                    }
                }
                #endregion

                #region Tanılar ICD
                List<ProcessList> icdList = ProcessListHelper.GetProcessListbyType(((int)ProcessListType.TANI).ToString());
                string icdListIds = "";
                if (icdList!=null)
                {
                    icdListIds = string.Join(",", icdList.Select(x => x.Id));
                }

                if (Tazminat.Tanilar.Count > 0)
                {
                    var icdCodeList = string.Join("','", Tazminat.Tanilar.Select(t => t.TaniKodu));
                    whereCondition = $"CODE IN ('{icdCodeList}') AND PROCESS_LIST_ID IN ({icdListIds})";
                    List<Process> responseProcessList = new GenericRepository<Process>().FindBy(whereCondition);


                    List<ClaimIcd> ClaimIcdEntityList = new List<ClaimIcd>();
                    foreach (Tani tani in Tazminat.Tanilar)
                    {
                        if (!string.IsNullOrEmpty(tani.TaniKodu))
                        {
                            Int64 processId = 0;
                            Process responseProcess = responseProcessList.Where(p => p.Code == tani.TaniKodu).FirstOrDefault();
                            if (responseProcess == null)
                            {
                                Process process = new Process
                                {
                                    Id = 0,
                                    Amount = 0,
                                    Code = tani.TaniKodu,
                                    Name = tani.TaniAdi,
                                    ProcessListId = icdList[0].Id
                                };
                                SpResponse spResponseProcess = new GenericRepository<Process>().Insert(process, this.Token);
                                if (spResponseProcess.Code != "100")
                                {
                                    result.SonucKodu = "0";
                                    result.SonucAciklama = GetReturnMessageCompanyPhone((long)CompanyID);
                                    result.ProvizyonNo = null;

                                    CreateFailedLog(log, result.ToXML(true), "T_Process(TANI) - " + spResponseProcess.Message);

                                    return result;
                                }
                                process.Id= spResponseProcess.PkId;
                                responseProcessList.Add(process);
                                processId = spResponseProcess.PkId;
                            }
                            else
                            {
                                processId = responseProcess.Id;
                            }
                            ClaimIcd claimIcd = new ClaimIcd
                            {
                                Id = 0,
                                ClaimId = claimId,
                                IcdId = processId,
                                Status = ((int)Status.AKTIF).ToString(),
                                IcdType = tani.TaniTipi
                            };
                            ClaimIcdEntityList.Add(claimIcd);
                        }
                    }
                    List<SpResponse> responseClaimIcdList = new GenericRepository<ClaimIcd>().InsertSpExecute3(ClaimIcdEntityList, this.Token);
                }
                #endregion

                #region Hizmet
                List<ProcessList> processList = ProcessListHelper.GetProcessListbyType(((int)ProcessListType.SUT).ToString());
                string processListIds = "";
                if (processList != null)
                {
                    processListIds = string.Join(",", icdList.Select(x => x.Id));
                }

                List<ClaimProcess> claimProcessList = new List<ClaimProcess>();
                decimal fark = 0;
                decimal totalfark = 0;

                foreach (InputOutputTypes.TeminatTalep coverage in Tazminat.TeminatTalepBilgileri)
                {
                    long CoverageId = new CoverageWorker().GetCoverageID(coverage.TeminatKodu, claimId);
                    if (CoverageId == 0)
                    {
                        result.SonucKodu = "0";
                        result.SonucAciklama = GetReturnMessageCompanyPhone((long)CompanyID);
                        result.ProvizyonNo = null;

                        CreateFailedLog(log, result.ToXML(true), coverage.TeminatKodu + "Teminat kodu bulunamadı!");

                        return result;
                    }
                    if (coverage.TalepHizmetBilgileri.Count > 0)
                    {
                        var totalTalepTutar = coverage.TalepHizmetBilgileri.Where(x => x.HizmetTalepTutar != null).Select(a => a.HizmetTalepTutar).Sum();
                        bool isPlus = false;
                        if (totalTalepTutar != coverage.TalepTutar)
                        {
                            if ((decimal)totalTalepTutar > coverage.TalepTutar)
                            {
                                fark = (decimal)totalTalepTutar - coverage.TalepTutar;
                                isPlus = false;
                            }
                            else
                            {
                                fark = coverage.TalepTutar - (decimal)totalTalepTutar;
                                isPlus = true;
                            }

                            totalfark = fark;
                        }
                        var sutCodeList = string.Join("','", coverage.TalepHizmetBilgileri.Select(t => t.HizmetKodu));
                        whereCondition = $"CODE IN ('{sutCodeList}') AND PROCESS_LIST_ID IN ({processListIds}) and STATUS = '{((int)Status.AKTIF).ToString()}'";
                        List<Process> responseProcessList = new GenericRepository<Process>().FindBy(whereCondition);

                        foreach (HizmetBilgisi processInfo in coverage.TalepHizmetBilgileri)
                        {
                            if (!string.IsNullOrEmpty(processInfo.HizmetKodu))
                            {
                                long? staffId = 0;

                                Int64 processId = 0;
                                decimal processLimit = 0;
                                Process responseProcess = responseProcessList.Where(p => p.Code == processInfo.HizmetKodu).FirstOrDefault();
                                if (responseProcess == null)
                                {
                                    Process process = new Process
                                    {
                                        Id = 0,
                                        Amount = (decimal)processInfo.HizmetSgkTutar,
                                        Code = processInfo.HizmetKodu,
                                        Name = processInfo.HizmetAdi,
                                        ProcessListId = processList[0].Id
                                    };
                                    SpResponse spResponseProcess = new GenericRepository<Process>().Insert(process, this.Token);
                                    if (spResponseProcess.Code != "100")
                                    {
                                        result.SonucKodu = "0";
                                        result.SonucAciklama = GetReturnMessageCompanyPhone((long)CompanyID);
                                        result.ProvizyonNo = null;

                                        CreateFailedLog(log, result.ToXML(true), "T_Process(SUT) - " + spResponseProcess.Message);

                                        return result;
                                    }
                                    process.Id = spResponseProcess.PkId;
                                    responseProcessList.Add(process);
                                    processId = spResponseProcess.PkId;
                                    processLimit = (decimal)processInfo.HizmetTalepTutar;
                                }
                                else
                                {
                                    processId = responseProcess.Id;
                                    processLimit = responseProcess.Amount;
                                }

                                if (!processInfo.DrTescilNo.IsNull() && !processInfo.Brans.IsNull())
                                {
                                    staffId = new StaffWorker().DoctorCreator(providerId, processInfo.Brans, processInfo.DrTescilNo);
                                }

                                decimal exitPaid = 0;
                                if (fark > 0)
                                {
                                    exitPaid = decimal.Parse((totalfark / coverage.TalepHizmetBilgileri.Count).ToString("#0.00"));
                                    if (!isPlus && exitPaid > processInfo.HizmetTalepTutar)
                                    {
                                        exitPaid = (decimal)processInfo.HizmetTalepTutar;
                                    }
                                    fark -= exitPaid;
                                }

                                ClaimProcess claimProcess = new ClaimProcess
                                {
                                    ProcessId = processId,
                                    ClaimId = claimId,
                                    Status = ((int)Status.AKTIF).ToString(),
                                    StaffId = staffId > 0 ? staffId : null,
                                    AgreedAmount = isPlus ? processInfo.HizmetTalepTutar + exitPaid : processInfo.HizmetTalepTutar - exitPaid,
                                    ProcessLimit = processLimit,
                                    UnitAmount = (decimal)processInfo.HizmetSgkTutar,
                                    SgkAmount = (decimal)processInfo.HizmetSgkTutar,
                                    RequestedAmount = isPlus ? processInfo.HizmetTalepTutar + exitPaid : processInfo.HizmetTalepTutar - exitPaid,
                                    Accepted = isPlus ? processInfo.HizmetTalepTutar + exitPaid : processInfo.HizmetTalepTutar - exitPaid,
                                    ContractProcessGroupId = processGroupId > 0 ? (long?)processGroupId : null,
                                    Requested = isPlus ? processInfo.HizmetTalepTutar + exitPaid : processInfo.HizmetTalepTutar - exitPaid,
                                    CoverageId = CoverageId
                                };
                                claimProcessList.Add(claimProcess);

                                if (staffId > 0 && claim.StaffId == null)
                                {
                                    claim.StaffId = staffId > 0 ? staffId : null;
                                    new GenericRepository<Claim>().Update(claim);
                                }
                            }
                        }
                        if (fark > 0)
                        {
                            foreach (var item in claimProcessList.Where(c => c.CoverageId == CoverageId))
                            {
                                if (!isPlus && item.Requested >= fark)
                                {
                                    item.Requested -= fark;
                                    item.AgreedAmount -= fark;
                                    item.RequestedAmount -= fark;
                                    item.Accepted -= fark;
                                    break;
                                }
                                else if (isPlus)
                                {
                                    item.Requested += fark;
                                    item.AgreedAmount += fark;
                                    item.RequestedAmount += fark;
                                    item.Accepted += fark;
                                    break;
                                }
                            }
                        }

                        totalfark = 0;
                        fark = 0;
                    }
                }

                var responseClaimProcessList = new ClaimRepository().ClaimProcessExecute(claimProcessList, this.Token);
                #endregion

                //#region Hizmet
                //List<ProcessList> processList = ProcessListHelper.GetProcessListbyType(((int)ProcessListType.SUT).ToString());
                //string processListIds = "";
                //if (processList != null)
                //{
                //    processListIds = string.Join(",", icdList.Select(x => x.Id));
                //}

                //List<ClaimProcess> claimProcessList = new List<ClaimProcess>();

                //foreach (InputOutputTypes.TeminatTalep coverage in Tazminat.TeminatTalepBilgileri)
                //{
                //    long CoverageId = new CoverageWorker().GetCoverageID(coverage.TeminatKodu, claimId);
                //    if (CoverageId == 0)
                //    {
                //        result.SonucKodu = "0";
                //        result.SonucAciklama = GetReturnMessageCompanyPhone((long)CompanyID);
                //        result.ProvizyonNo = null;

                //        CreateFailedLog(log, result.ToXML(true), coverage.TeminatKodu + "Teminat kodu bulunamadı!");

                //        return result;
                //    }
                //    if (coverage.TalepHizmetBilgileri.Count > 0)
                //    {
                //        var sutCodeList = string.Join("','", coverage.TalepHizmetBilgileri.Select(t => t.HizmetKodu));
                //        whereCondition = $"CODE IN ('{sutCodeList}') AND PROCESS_LIST_ID IN ({processListIds}) and STATUS = '{((int)Status.AKTIF).ToString()}'";
                //        List<Process> responseProcessList = new GenericRepository<Process>().FindBy(whereCondition);

                //        foreach (HizmetBilgisi processInfo in coverage.TalepHizmetBilgileri)
                //        {
                //            if (!string.IsNullOrEmpty(processInfo.HizmetKodu))
                //            {
                //                long? staffId = 0;

                //                Int64 processId = 0;
                //                decimal processLimit = 0;
                //                Process responseProcess = responseProcessList.Where(p => p.Code == processInfo.HizmetKodu).FirstOrDefault();
                //                if (responseProcess == null)
                //                {
                //                    Process process = new Process
                //                    {
                //                        Id = 0,
                //                        Amount = (decimal)processInfo.HizmetSgkTutar,
                //                        Code = processInfo.HizmetKodu,
                //                        Name = processInfo.HizmetAdi,
                //                        ProcessListId = processList[0].Id
                //                    };
                //                    SpResponse spResponseProcess = new GenericRepository<Process>().Insert(process, this.Token);
                //                    if (spResponseProcess.Code != "100")
                //                    {
                //                        result.SonucKodu = "0";
                //                        result.SonucAciklama = GetReturnMessageCompanyPhone((long)CompanyID);
                //                        result.ProvizyonNo = null;

                //                        CreateFailedLog(log, result.ToXML(true), "T_Process(SUT) - " + spResponseProcess.Message);

                //                        return result;
                //                    }
                //                    process.Id = spResponseProcess.PkId;
                //                    responseProcessList.Add(process);
                //                    processId = spResponseProcess.PkId;
                //                    processLimit = (decimal)processInfo.HizmetTalepTutar;
                //                }
                //                else
                //                {
                //                    processId = responseProcess.Id;
                //                    processLimit = responseProcess.Amount;
                //                }

                //                if (!processInfo.DrTescilNo.IsNull() && !processInfo.Brans.IsNull())
                //                {
                //                    staffId = new StaffWorker().DoctorCreator(providerId, processInfo.Brans, processInfo.DrTescilNo);
                //                }

                //                ClaimProcess claimProcess = new ClaimProcess
                //                {
                //                    ProcessId = processId,
                //                    ClaimId = claimId,
                //                    Status = ((int)Status.AKTIF).ToString(),
                //                    StaffId = staffId > 0 ? staffId : null,
                //                    AgreedAmount = processInfo.HizmetTalepTutar,
                //                    ProcessLimit = processLimit,
                //                    UnitAmount = (decimal)processInfo.HizmetSgkTutar,
                //                    SgkAmount = (decimal)processInfo.HizmetSgkTutar,
                //                    RequestedAmount = processInfo.HizmetSozlesmeTutar,
                //                    Accepted = processInfo.HizmetTalepTutar,
                //                    ContractProcessGroupId = processGroupId > 0 ? (long?)processGroupId : null,
                //                    Requested = processInfo.HizmetSozlesmeTutar,
                //                    CoverageId = CoverageId
                //                };
                //                claimProcessList.Add(claimProcess);

                //                if (staffId > 0 && claim.StaffId == null)
                //                {
                //                    claim.StaffId = staffId > 0 ? staffId : null;
                //                    new GenericRepository<Claim>().Update(claim);
                //                }
                //            }
                //        }
                //    }
                //}

                //var responseClaimProcessList = new ClaimRepository().ClaimProcessExecute(claimProcessList, this.Token);
                //#endregion

                #region Provizyon Onay Kontrol

                var _Company = new CompanyHelper().WhichCompany((long)CompanyID);
                
                
                ProxyServiceClient proxyServiceClient = new ProxyServiceClient();
                NNHayatServiceClientV2 nNHayatServiceClient = new NNHayatServiceClientV2();

                ClaimStateUpdateReq stateUpdateReq = new ClaimStateUpdateReq
                {
                    ClaimId = claimId
                };
                SendCompanyClaim(claimId, (long)CompanyID);

                ClaimRepository claimRepository = new ClaimRepository();
                string[] spResult = claimRepository.ClaimStatusResult(claimId);
                if (!spResult[0].Equals("1"))
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = GetReturnMessageCompanyPhone((long)CompanyID);
                    result.ProvizyonNo = null;

                    CreateFailedLog(log, result.ToXML(true), spResult[0] + " : " + spResult[1] + " - HASAR DURUMU GÜNCELLENEMEDİ!");

                    return result;
                }

                if (result.SonucKodu.IsNull())
                {
                    var vClaim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID={claimId}", orderby: "CLAIM_ID DESC", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                    if (vClaim.STATUS == ((int)ClaimStatus.RET).ToString())
                    {
                        result.SonucKodu = "0";
                        result.SonucAciklama = vClaim.REASON_DESCRIPTION + ", Sebebiyle Tazminat RET olarak Kaydedildi.";
                        result.ProvizyonNo = null;
                        CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                        return result;
                    }
                    else if (vClaim.STATUS != ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                    {
                        result.SonucKodu = "0";
                        result.SonucAciklama = vClaim.REASON_DESCRIPTION.IsNull() ? "Hasar " + LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, vClaim.STATUS) + " olarak Kaydedildi." : vClaim.REASON_DESCRIPTION + " Sebebiyle Hasar " + LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, vClaim.STATUS) + " olarak Kaydedildi.";
                        result.ProvizyonNo = null;
                        CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                        return result;
                    }
                    if (vClaim.STATUS == ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                    {
                        decimal pSorgu = 10000;
                        if (vClaim.REQUESTED >= pSorgu)
                        {
                            MailSender msender = new MailSender();
                            msender.SendMessage(
                                new EmailArgs
                                {
                                    TO = "fulya@imecedestek.com;bilgiprovizyon@imecedestek",
                                    IsHtml = false,
                                    Subject = $"Yüksek Hasar/ Provizyon No:{vClaim.CLAIM_ID}",
                                    Content = $"{vClaim.CLAIM_ID} numaralı Hasarın Talep Tutarı {vClaim.REQUESTED} TL'dir. Detayına aşağıdaki bağlantı üzerinden erişebilirsiniz:<br/><br/>URL: http://protein.imecedestek.com:8080/Claim/Create?claimId=" +vClaim.CLAIM_ID
                                });
                        }
                    }
                    if (vClaim.COMPANY_ID == 3 && vClaim.PRODUCT_CODE == "NPN_MDP")
                    {
                        if (vClaim.POLICY_ISSUE_DATE != null && (DateTime)vClaim.POLICY_ISSUE_DATE < DateTime.Parse("01.05.2018"))
                        {
                            result.EkBilgi = "Lütfen bu provizyon için Faturanızı IMECE DESTEK ticari ünvanı adına kesiniz!";
                        }
                        else
                        {
                            result.EkBilgi = "Lütfen bu provizyon için faturanızı NİPPON SİGORTA ticari ünvanı adına kesiniz!";
                        }
                    }
                    SendCompanyClaimStatus(claimId, (long)CompanyID, vClaim);
                }
                else
                {
                    return result;
                }
                #endregion

                #endregion

                //Sigorta Şirketleri

                #region result Coverage 

                List<V_ClaimCoverage> claimCoverageList = new List<V_ClaimCoverage>();
                claimCoverageList = new GenericRepository<V_ClaimCoverage>().FindBy(conditions: $"CLAIM_ID = {claimId}", orderby: "");

                if (claimCoverageList.Count > 0)
                {
                    var coverageIdList = string.Join("','", claimCoverageList.Select(t => t.CoverageId));
                    result.TeminatTalepBilgileriCevap = new List<TeminatTalepCevap>();

                    var coverParamList = new CoverageWorker().GetPortTssCoverageCode(coverageIdList);

                    var idList = new List<long>();
                    foreach (V_ClaimCoverage cover in claimCoverageList)
                    {
                        if (!idList.Contains(cover.CoverageId))
                        {
                            var param = coverParamList.Where(c => c.CoverageId == cover.CoverageId).FirstOrDefault();
                            var totalPaid = claimCoverageList.Where(c => c.CoverageId == cover.CoverageId).Sum(t => t.Paid);
                            var totalRequest = claimCoverageList.Where(c => c.CoverageId == cover.CoverageId).Sum(t => t.Requested);
                            var totalInsured = totalRequest - totalPaid;

                            TeminatTalepCevap temTalepCevap = new TeminatTalepCevap
                            {
                                OnayTutar = totalPaid,
                                SigortaliTutar = totalInsured,
                                TeminatKodu = param != null ? param.Value : "",
                                TalepTutar = totalRequest
                            };
                            result.TeminatTalepBilgileriCevap.Add(temTalepCevap);

                            idList.Add(cover.CoverageId);
                        }
                    }
                }
                #endregion

                result.ProvizyonNo = claimId.ToString();
                result.SonucKodu = "1";
                result.SonucAciklama = "İşlem Başarıyla Sonuçlandı.";

                CreateOKLog(log, result.ToXML(true));
            }
            catch (Exception ex)
            {
                result.SonucKodu = "0";
                result.SonucAciklama = GetReturnMessageCompanyPhone((long)CompanyID);
                result.ProvizyonNo = null;

                CreateFailedLog(log, result.ToXML(true), ex.Message);
            }
            return result;
        }

        private Task<string> Task1(Int64 claimId, Int64 CompanyID)
        {
            return Task.Factory.StartNew(() => Function1(claimId, CompanyID));
        }
        private Task<string> Task2(Int64 claimId, Int64 CompanyID, V_Claim vClaim)
        {
            return Task.Factory.StartNew(() => Function2(claimId, CompanyID, vClaim));
        }
        private string Function1(Int64 claimId, Int64 CompanyID)
        {
            var _Company = new CompanyHelper().WhichCompany(CompanyID);
            ProxyServiceClient proxyServiceClient = new ProxyServiceClient();
            NNHayatServiceClientV2 nNHayatServiceClient = new NNHayatServiceClientV2();

            ClaimStateUpdateReq stateUpdateReq = new ClaimStateUpdateReq
            {
                ClaimId = claimId
            };

            if (_Company == CompanyEnum.HDI || _Company == CompanyEnum.HDI_TEST)
            {
                proxyServiceClient.ClaimNotice(stateUpdateReq);
            }

            if (_Company == CompanyEnum.NN || _Company == CompanyEnum.NN_TEST)
            {
                NNTazminatGirisReq nNTazminatGirisReq = new NNTazminatGirisReq
                {
                    claimId = claimId
                };
                nNHayatServiceClient.TazminatGiris(nNTazminatGirisReq);
            }

            Thread.Sleep(1000);
            return "Function1 End";
        }
        private string Function2(Int64 claimId, Int64 CompanyID,V_Claim vClaim)
        {
            var _Company = new CompanyHelper().WhichCompany(CompanyID);
            ProxyServiceClient proxyServiceClient = new ProxyServiceClient();
            NNHayatServiceClientV2 nNHayatServiceClient = new NNHayatServiceClientV2();

            ClaimStateUpdateReq stateUpdateReq = new ClaimStateUpdateReq
            {
                ClaimId = claimId
            };

            if (_Company == CompanyEnum.HDI || _Company == CompanyEnum.HDI_TEST)
            {
                if (vClaim.STATUS == ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                {
                    proxyServiceClient.ClaimApproval(stateUpdateReq);
                }
                else if (vClaim.STATUS == ((int)ClaimStatus.RET).ToString())
                {
                    proxyServiceClient.ClaimRejection(stateUpdateReq);
                }
                else if (vClaim.STATUS == ((int)ClaimStatus.IPTAL).ToString())
                {
                    proxyServiceClient.ClaimCancellation(stateUpdateReq);
                }
                else if (vClaim.STATUS == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || vClaim.STATUS == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                {
                    proxyServiceClient.ClaimWait(stateUpdateReq);
                }
            }
            if (_Company == CompanyEnum.NN || _Company == CompanyEnum.NN_TEST)
            {
                string newStatus = "";
                if (vClaim.STATUS == ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                {
                    newStatus = "1";
                }
                else if (vClaim.STATUS == ((int)ClaimStatus.RET).ToString())
                {
                    newStatus = "4";
                }
                else if (vClaim.STATUS == ((int)ClaimStatus.IPTAL).ToString())
                {
                    newStatus = "2";
                }
                else if (vClaim.STATUS == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || vClaim.STATUS == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                {
                    newStatus = "3";
                }

                if (newStatus.IsInt())
                {
                    NNSetProvisionStatusReq nNSetProvisionStatusReq = new NNSetProvisionStatusReq
                    {
                        ProvisionNumber = vClaim.COMPANY_CLAIM_ID.ToString(),
                        ProvisionNumberSpecified = true,
                        NewStatu = newStatus,
                        NewStatuSpecified = true,
                        ReasonDesc = vClaim.REASON_DESCRIPTION,
                        ReasonDescSpecified = true
                    };
                    nNHayatServiceClient.SetProvisionStatus(nNSetProvisionStatusReq);
                }
            }
            if (_Company == CompanyEnum.DOGA)
            {
                DogaServiceClient dogaServiceClient = new DogaServiceClient();
                dogaServiceClient.HasarAktar(ConfigurationManager.AppSettings["ApiCode" + ((int)CompanyEnum.DOGA).ToString()], vClaim.CLAIM_ID);
            }
            Thread.Sleep(1000);
            return "Function2 End";
        }

        public async Task<bool> SendCompanyClaim(Int64 claimId,Int64 CompanyID)
        {
            var t1 = Task1(claimId, CompanyID);
            await Task.WhenAll(t1);
            
            return true;
        }

        public async Task<bool> SendCompanyClaimStatus(Int64 claimId,Int64 CompanyID, V_Claim vClaim)
        {
            var t1 = Task2(claimId, CompanyID, vClaim);
            await Task.WhenAll(t1);
            
            return true;
        }

        private string GetReturnMessageCompanyPhone(long CompanyID)
        {
            string SonucAciklama = "Lütfen işleme devam edebilmek için lütfen provizyon merkezini arayınız.";

            var company = new GenericRepository<V_CompanyPhone>().FindBy($"COMPANY_ID={CompanyID} AND PHONE_TYPE='{((int)CompanyPhoneType.PROVIZYON)}'", orderby: "COMPANY_ID").FirstOrDefault();
            if (company != null)
            {
                SonucAciklama = "Lütfen işleme devam edebilmek için lütfen" + company.PHONE_NO + " numaralı provizyon merkezini arayınız.";
            }
            return SonucAciklama;
        }

        public ProvizyonIptalCevap TamamlayiciSaglikProvizyonIptal(ProvizyonIptal ProvizyonIptal)
        {
            var currentMethod = MethodBase.GetCurrentMethod();

            ProvizyonIptalCevap result = new ProvizyonIptalCevap();

            long providerId = 0;
            long claimId = 0;

            IntegrationLog log = new IntegrationLog();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.WsMethod = currentMethod.Name;
            log.Request = ProvizyonIptal.ToXML(true);
            log.WsRequestDate = DateTime.Now;

            long? CompanyID = 0;


            ValidationResponse validationResponse = Validator.Worker<ProvizyonIptal>(ProvizyonIptal);
            #region Validation
            if (!validationResponse.IsValid)
            {
                result.SonucKodu = validationResponse.Code;
                result.SonucAciklama = validationResponse.Description;

                try
                {
                    log.HttpStatus = ((int)HttpStatusCode.LengthRequired).ToString();
                    log.ResponseStatusCode = PortTssErrors.ValidationInvalid.Split(':')[0];
                    log.ResponseStatusDesc = PortTssErrors.ValidationInvalid.Split(':')[0];
                    log.Type = ((int)LogType.FAILED).ToString();
                    log.Response = result.ToXML(true);
                    log.ProviderId = (string.IsNullOrEmpty(ProvizyonIptal.Kurum)) ? 0 : Convert.ToInt64(ProvizyonIptal.Kurum);
                    log.ClaimId = long.Parse(ProvizyonIptal.Kurum);
                }
                catch { }
                IntegrationLogHelper.AddToLog(log, true);

                result.SonucAciklama = log.ResponseStatusDesc;
                result.SonucKodu = "0";

                return result;
            }
            #endregion
            #region Incoming Log
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.Type = ((int)LogType.INCOMING).ToString();
            log.ProviderId = Convert.ToInt64(ProvizyonIptal.Kurum);
            log.ClaimId = Convert.ToInt64(ProvizyonIptal.ProvizyonNo);
            IntegrationLogHelper.AddToLog(log);
            #endregion
            try
            {
                #region User kontrolü
                if (!isValidUser(ProvizyonIptal.UserName, ProvizyonIptal.Password, ref CompanyID))
                {
                    log.ResponseStatusCode = PortTssErrors.UserNameOrPasswordInvalid.Split(':')[0];
                    log.ResponseStatusDesc = PortTssErrors.UserNameOrPasswordInvalid.Split(':')[1];

                    result.SonucKodu = "0";
                    result.SonucAciklama = log.ResponseStatusDesc;

                    log.HttpStatus = ((int)HttpStatusCode.Unauthorized).ToString();
                    log.Type = ((int)LogType.FAILED).ToString();
                    log.WsResponseDate = DateTime.Now;
                    log.Response = result.ToXML(true);
                    IntegrationLogHelper.AddToLog(log, true);

                    return result;
                }
                log.COMPANY_ID = CompanyID;
                #endregion
                #region Provider Kontrolü
                V_Provider provider = new GenericRepository<V_Provider>().FindBy($"PROVIDER_ID = {ProvizyonIptal.Kurum}", orderby: "").FirstOrDefault();
                if (provider == null)
                {
                    log.ResponseStatusCode = PortTssErrors.ProviderNotFound.Split(':')[0];
                    log.ResponseStatusDesc = PortTssErrors.ProviderNotFound.Split(':')[1];

                    result.SonucKodu = "0";
                    result.SonucAciklama = log.ResponseStatusDesc;

                    log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                    log.Type = ((int)LogType.FAILED).ToString();
                    log.WsResponseDate = DateTime.Now;
                    log.Response = result.ToXML(true);
                    IntegrationLogHelper.AddToLog(log, true);

                    return result;
                }
                providerId = provider.PROVIDER_ID;
                #endregion
                #region Claim var mı ?
                string whereCondition = $" ID = {ProvizyonIptal.ProvizyonNo} ";
                Claim claim = new GenericRepository<Claim>().FindBy(whereCondition).FirstOrDefault();
                if (claim == null)
                {
                    log.ResponseStatusCode = PortTssErrors.ClaimNotFound.Split(':')[0];
                    log.ResponseStatusDesc = PortTssErrors.ClaimNotFound.Split(':')[1];

                    result.SonucKodu = "0";
                    result.SonucAciklama = log.ResponseStatusDesc;

                    log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                    log.Type = ((int)LogType.FAILED).ToString();
                    log.WsResponseDate = DateTime.Now;
                    log.Response = result.ToXML(true);
                    IntegrationLogHelper.AddToLog(log, true);

                    return result;
                }
                else
                {
                    claimId = claim.Id;
                    if (claim.PaymentMethod == ((int)ClaimStatus.RET).ToString() || claim.PaymentMethod == ((int)ClaimStatus.PROZIYON_ONAY).ToString() || claim.PaymentMethod == ((int)ClaimStatus.ODENDI).ToString())
                    {
                        log.ResponseStatusCode = PortTssErrors.ClaimNotFound.Split(':')[0];
                        log.ResponseStatusDesc = "Ret/Provizyon Onay/Odendi durumunda olan hasarlar iptal edilemez";

                        result.SonucKodu = "0";
                        result.SonucAciklama = log.ResponseStatusDesc;

                        log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                        log.Type = ((int)LogType.FAILED).ToString();
                        log.WsResponseDate = DateTime.Now;
                        log.Response = result.ToXML(true);
                        IntegrationLogHelper.AddToLog(log, true);

                        return result;
                    }
                    if (claim.PayrollId != null)
                    {
                        log.ResponseStatusCode = PortTssErrors.ClaimNotFound.Split(':')[0];
                        log.ResponseStatusDesc = "Zarflanmış hasarlar iptal edilemez";

                        result.SonucKodu = "0";
                        result.SonucAciklama = log.ResponseStatusDesc;

                        log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                        log.Type = ((int)LogType.FAILED).ToString();
                        log.WsResponseDate = DateTime.Now;
                        log.Response = result.ToXML(true);
                        IntegrationLogHelper.AddToLog(log, true);

                        return result;
                    }
                }

                #endregion
                claim.Status = ((int)ClaimStatus.IPTAL).ToString();
                SpResponse spResponseClaim = new GenericRepository<Claim>().Insert(claim, this.Token);
                if (spResponseClaim.Code != "100")
                {
                    log.ResponseStatusCode = PortTssErrors.ClaimNotFound.Split(':')[0];
                    log.ResponseStatusDesc = spResponseClaim.Message;

                    result.SonucKodu = "0";
                    result.SonucAciklama = "Hasar durumu değiştirilemedi.";

                    log.HttpStatus = ((int)HttpStatusCode.ExpectationFailed).ToString();
                    log.Type = ((int)LogType.FAILED).ToString();
                    log.WsResponseDate = DateTime.Now;
                    log.Response = result.ToXML(true);
                    IntegrationLogHelper.AddToLog(log, true);

                    return result;
                }

                var _Company = new CompanyHelper().WhichCompany((long)CompanyID);
                ProxyServiceClient proxyServiceClient = new ProxyServiceClient();
                ClaimStateUpdateReq stateUpdateReq = new ClaimStateUpdateReq
                {
                    ClaimId = claimId
                };

                if (_Company == CompanyEnum.HDI || _Company == CompanyEnum.HDI_TEST)
                {
                    proxyServiceClient.ClaimCancellation(stateUpdateReq);
                }

                //Sagmer

                //Sigorta Şirketleri

                result.ProvizyonNo = claimId.ToString();
                result.SonucKodu = "1";
                result.SonucAciklama = "işlem başarıyla sonuçlandı";

                log.ResponseStatusCode = result.SonucKodu;
                log.ResponseStatusDesc = result.SonucAciklama;
                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                log.Type = ((int)LogType.COMPLETED).ToString();
                log.WsResponseDate = DateTime.Now;
                log.Response = result.ToXML(true);
                IntegrationLogHelper.AddToLog(log);
            }
            catch (Exception ex)
            {
                result.SonucKodu = "0";
                result.SonucAciklama = "HATA";

                log.Response = result.ToXML(true);
                log.HttpStatus = ((int)HttpStatusCode.InternalServerError).ToString();
                log.ResponseStatusCode = "500";
                log.ResponseStatusDesc = $"Error: {ex.Message} StackTrace: {ex.StackTrace}";
                IntegrationLogHelper.AddToLog(log, true);
            }
            return result;
        }
        public IcmalGirisCevap IcmalGiris(IcmalBilgileri Icmal)
        {
            IcmalGirisCevap result = new IcmalGirisCevap();

            var currentMethod = MethodBase.GetCurrentMethod();
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = Icmal.ToXML(true),
                WsRequestDate = DateTime.Now
            };

            long? CompanyID = 0;
            long tssContractId = 0;
            long? PayrolID = 0;

            try
            {
                #region Validation
                ValidationResponse validationResponse = Validator.Worker(Icmal);

                if (!validationResponse.IsValid)
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = validationResponse.Description;

                    CreateFailedLog(log, result.ToXML(true), validationResponse.Description);

                    return result;
                }
                #endregion

                log.ProviderId = Convert.ToInt64(Icmal.Kurum);
                log.EXT_PROVIDER_NO = Icmal.IcmalNo;
            }
            catch (Exception ex)
            {
                result.SonucKodu = "0";
                result.SonucAciklama = PortTssErrors.ValidationInvalid.Split(':')[1];

                CreateFailedLog(log, result.ToXML(true), ex.Message);

                return result;
            }

            try
            {

                #region User kontrolü
                if (!isValidUser(Icmal.UserName, Icmal.Password, ref CompanyID))
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = PortTssErrors.UserNameOrPasswordInvalid.Split(':')[1];

                    CreateFailedLog(log, result.ToXML(true), PortTssErrors.UserNameOrPasswordInvalid.Split(':')[1]);

                    return result;
                }
                log.COMPANY_ID = CompanyID;
                #endregion

                #region WebValidations
                if (Icmal.TazminatBilgileri == null || Icmal.TazminatBilgileri.Count <= 0)
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = "Tazminat bilgileri dolu olmalıdır";

                    CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                    return result;
                }
                else
                {
                    if (!Icmal.IcmalNo.IsInt64())
                    {
                        result.SonucKodu = "0";
                        result.SonucAciklama = "IcmalNo dolu ve doğru formatta olmalıdır";

                        CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                        return result;
                    }
                    if (!Icmal.Kurum.IsInt64())
                    {
                        result.SonucKodu = "0";
                        result.SonucAciklama = "Kurum dolu ve doğru formatta olmalıdır";

                        CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                        return result;
                    }
                    if (Icmal.TazminatBilgileri.Count < 0)
                    {
                        result.SonucKodu = "0";
                        result.SonucAciklama = "İcmal içerisinde en az bir provizyon bulunması gerekmektedir.";

                        CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                        return result;

                    }
                    string claimIdList = string.Join(",", Icmal.TazminatBilgileri.Select(x => x.ProvizyonNo));
                    var claimList = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID IN ({claimIdList})", orderby: "", hint: "/*+ USE_HASH(claim_id) ORDERED */");

                    foreach (var tazBilg in Icmal.TazminatBilgileri)
                    {
                        if (!tazBilg.ProvizyonNo.IsInt64())
                        {
                            result.SonucKodu = "0";
                            result.SonucAciklama = "Provizyon no dolu ve doğru formatta olmalıdır";

                            CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                            return result;
                        }
                        if (!tazBilg.FaturaTarihi.IsDateTime())
                        {
                            result.SonucKodu = "0";
                            result.SonucAciklama = "Fatura tarihi dolu ve doğru formatta olmalıdır";

                            CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                            return result;
                        }
                        else
                        {
                            tazBilg.FaturaTarihi = DateTime.Parse(tazBilg.FaturaTarihi).ToString("yyyy-MM-dd");
                        }
                        if (string.IsNullOrEmpty(tazBilg.FaturaNo))
                        {
                            result.SonucKodu = "0";
                            result.SonucAciklama = "Fatura no dolu olmalıdır";

                            CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                            return result;
                        }
                        if (!tazBilg.Tutar.ToString().IsNumeric())
                        {
                            result.SonucKodu = "0";
                            result.SonucAciklama = "Tutar dolu ve doğru formatta olmalıdır";

                            CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                            return result;
                        }
                        V_Claim claim = claimList.Where(x => x.CLAIM_ID == long.Parse(tazBilg.ProvizyonNo)).ToList().FirstOrDefault();//new GenericRepository<V_Claim>().FindBy($"CLAIM_ID={tazBilg.ProvizyonNo}", orderby: "", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                        if (claim == null)
                        {
                            result.SonucKodu = "0";
                            result.SonucAciklama = tazBilg.ProvizyonNo + "Nolu Provizyon bilgisi bulunamadı.";

                            CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                            return result;
                        }
                        if (claim.PROVIDER_ID != long.Parse(Icmal.Kurum))
                        {
                            result.SonucKodu = "0";
                            result.SonucAciklama = tazBilg.ProvizyonNo + "Nolu Provizyon farklı bir Kurum için Oluşturulmuştur.";

                            CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                            return result;
                        }
                        if (claim.PAYROLL_ID != null && claim.EXT_PROVIDER_NO != Icmal.IcmalNo)
                        {
                            result.SonucKodu = "0";
                            result.SonucAciklama = tazBilg.ProvizyonNo + " Nolu Provizyon, " + claim.EXT_PROVIDER_NO + " nolu İcmal içerisinde Bulunmaktadır.";

                            CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                            return result;
                        }
                        if (claim.STATUS != ((int)ClaimStatus.PROZIYON_ONAY).ToString() && claim.STATUS != ((int)ClaimStatus.ODENECEK).ToString() && claim.STATUS != ((int)ClaimStatus.ODENDI).ToString())
                        {
                            result.SonucKodu = "0";
                            result.SonucAciklama = tazBilg.ProvizyonNo + " Nolu Provizyon, Onay Durumunda Olmalıdır.";

                            CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                            return result;
                        }
                        if (claim.PAID != tazBilg.Tutar)
                        {
                            result.SonucKodu = "0";
                            result.SonucAciklama = tazBilg.ProvizyonNo + " Nolu Provizyon, Tutarı Sistemdekiyle Eşleşmiyor.";

                            CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                            return result;
                        }
                    }
                }
                #endregion

                #region Provider Kontrolü
                V_ProviderContractIds contract = new GenericRepository<V_ProviderContractIds>().FindBy($"PROVIDER_ID = {Icmal.Kurum}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (contract == null)
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = PortTssErrors.ProviderNotFound.Split(':')[1];

                    CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                    return result;
                }
                tssContractId = contract.TSS_CONTRACT_ID != null ? (long)contract.TSS_CONTRACT_ID : 0;
                #endregion

                #region Worker
                IcmalParams param = new IcmalParams
                {
                    IcmalNo = Icmal.IcmalNo,
                    Kurum = Icmal.Kurum,
                    Password = Icmal.Password,
                    SgkTakipNo = Icmal.SGKTakipNo,
                    Username = Icmal.UserName,
                    IcmalTazminatBilgiList = new List<IcmalTazminatBilgi>()
                };
                foreach (TazminatBilgisi tazBil in Icmal.TazminatBilgileri)
                {
                    IcmalTazminatBilgi ic = new IcmalTazminatBilgi();
                    ic.FaturaNo = tazBil.FaturaNo;
                    ic.FaturaTarihi = tazBil.FaturaTarihi;
                    ic.ProvizyonNo = tazBil.ProvizyonNo;
                    if (tazBil.Tutar.HasValue)
                        ic.Tutar = Convert.ToDecimal(tazBil.Tutar);
                    else
                        ic.Tutar = 0;
                    param.IcmalTazminatBilgiList.Add(ic);

                }

                IcmalResult subResult = new IcmalResult();

                subResult = IcmalEntryWorker(icmalParams: param, CompanyID: CompanyID, PayrolID: ref PayrolID, TssContractID: int.Parse(tssContractId.ToString()), ProviderID: long.Parse(Icmal.Kurum));
                if (!subResult.Success)
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = subResult.Message;

                    CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                    return result;
                }
                if (subResult.IcmalBilgi == null)
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = "Icmal Servis sonuç null";

                    CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);

                    return result;
                }
                #endregion
                result.PaketNo = subResult.IcmalBilgi.PaketNo;
                result.SonucAciklama = subResult.IcmalBilgi.SonucAciklama;
                result.SonucKodu = "1";

                CreateOKLog(log, result.ToXML(true));
            }
            catch (Exception ex)
            {
                result.SonucKodu = "0";
                result.SonucAciklama = "HATA OLUŞTU! LÜTFEN TEKRAR DENEYİNİZ...";

                CreateFailedLog(log, result.ToXML(true), $"Error: {ex.Message} StackTrace: {ex.StackTrace}");

                return result;
            }
            return result;

        }
        public IcmalTazminatCikarmaCevap IcmalTazminatCikarma(IcmalTazminatBilgileri IcmalTazminat)
        {
            IcmalTazminatCikarmaCevap result = new IcmalTazminatCikarmaCevap();
            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                WsRequestDate = DateTime.Now,
                Request = IcmalTazminat.ToXML(true),
                EXT_PROVIDER_NO = IcmalTazminat.IcmalNo
            };

            long? CompanyID = 0;
            try
            {
                #region Validation
                ValidationResponse validationResponse = Validator.Worker<IcmalTazminatBilgileri>(IcmalTazminat);

                if (!validationResponse.IsValid)
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = validationResponse.Description;
                    CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);
                    return result;
                }
                #endregion
                #region User kontrolü
                if (!isValidUser(IcmalTazminat.UserName, IcmalTazminat.Password, ref CompanyID))
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = PortTssErrors.UserNameOrPasswordInvalid.Split(':')[1];
                    CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);
                    return result;
                }
                log.COMPANY_ID = CompanyID;
                #endregion

                var findPayroll = new GenericRepository<Payroll>().FindBy($"COMPANY_ID={CompanyID} AND EXT_PROVIDER_NO = '{IcmalTazminat.IcmalNo}'", orderby: "", fetchDeletedRows: true).FirstOrDefault();
                if (findPayroll == null)
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = "İcmal Bilgisi Bulunamadı.";
                    CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);
                    return result;
                }
                if (findPayroll.Status != ((int)PayrollStatus.Onay_Bekler).ToString())
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = "İcmal Durumu Uygun Değil Lütfen Onay Bekler Durumuna Alınız.";
                    CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);
                    return result;
                }
                if (IcmalTazminat.TazminatBilgileri == null || IcmalTazminat.TazminatBilgileri.Count <= 0)
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = "Tazminat bilgileri dolu olmalıdır";
                    CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);
                    return result;
                }
                else
                {
                    foreach (var tazBilg in IcmalTazminat.TazminatBilgileri)
                    {
                        if (!tazBilg.ProvizyonNo.IsInt64())
                        {
                            result.SonucKodu = "0";
                            result.SonucAciklama = "Provizyon no dolu olmalıdır";
                            CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);
                            return result;
                        }
                        var claim = new GenericRepository<Claim>().FindById(long.Parse(tazBilg.ProvizyonNo));
                        if (claim == null)
                        {
                            result.SonucKodu = "0";
                            result.SonucAciklama = tazBilg.ProvizyonNo + "Nolu Provizyon bilgisi bulunamadı.";
                            CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);
                            return result;
                        }
                        if (claim.PayrollId == null || claim.PayrollId != findPayroll.Id)
                        {
                            result.SonucKodu = "0";
                            result.SonucAciklama = tazBilg.ProvizyonNo + "Nolu Provizyon Bu İcmale Ait Değildir.";
                            CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);
                            return result;
                        }
                    }
                }

                IcmalProvizyonCikartParams param = new IcmalProvizyonCikartParams
                {
                    IcmalNo = IcmalTazminat.IcmalNo,
                    Password = IcmalTazminat.Password,
                    Username = IcmalTazminat.UserName,
                    IcmalTazCikartBilgi = new List<IcmalTazCikartBilgi>()
                };
                foreach (var tazBilg in IcmalTazminat.TazminatBilgileri)
                {
                    IcmalTazCikartBilgi bilgi = new IcmalTazCikartBilgi
                    {
                        ProvizyonNo = tazBilg.ProvizyonNo
                    };

                    param.IcmalTazCikartBilgi.Add(bilgi);
                }
                List<IcmalProvizyonCikartResult> subResult = new List<IcmalProvizyonCikartResult>();

                result = IcmalSubResult(param);
                if (result.SonucKodu == "0")
                {
                    result.SonucKodu = "0";
                    result.SonucAciklama = result.SonucAciklama;
                    CreateFailedLog(log, result.ToXML(true), result.SonucAciklama);
                    return result;
                }
                else
                {
                    CreateOKLog(log, result.ToXML(true));
                }
            }
            catch (Exception ex)
            {
                result.SonucKodu = "0";
                result.SonucAciklama = "HATA OLUŞTU! LÜTFEN TEKRAR DENEYİNİZ...";

                CreateFailedLog(log, result.ToXML(true), $"Error: {ex.Message} StackTrace: {ex.StackTrace}");

                return result;
            }
            return result;
        }
        public RedDurumBildirimCevap RedDurumBildirim(RedDurumBilgisi RedDurum)
        {
            RedDurumBildirimCevap result = new RedDurumBildirimCevap();
            IntegrationLog log = new IntegrationLog();
            var currentMethod = MethodBase.GetCurrentMethod();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.WsMethod = currentMethod.Name;
            log.WsRequestDate = DateTime.Now;

            long providerId = 0;
            long? CompanyID = 0;
            long tssContractId = 0;
            long contractId = 0;
            long claimId = 0;
            long processGroupId = 0;

            ValidationResponse validationResponse = Validator.Worker<RedDurumBilgisi>(RedDurum);
            #region Validation
            if (!validationResponse.IsValid)
            {
                result.SonucKodu = validationResponse.Code;
                result.SonucAciklama = validationResponse.Description;
                try
                {
                    log.HttpStatus = ((int)HttpStatusCode.LengthRequired).ToString();
                    log.ResponseStatusCode = PortTssErrors.ValidationInvalid.Split(':')[0];
                    log.ResponseStatusDesc = PortTssErrors.ValidationInvalid.Split(':')[1];
                    log.Type = ((int)LogType.FAILED).ToString();
                    log.WsResponseDate = DateTime.Now;
                    log.Request = RedDurum.ToXML(true);
                    log.Response = result.ToXML(true);
                }
                catch { }
                IntegrationLogHelper.AddToLog(log, true);
                log.ResponseStatusCode = "";
                log.ResponseStatusDesc = "";

                IcmalGirisCevap IcmalResult = new IcmalGirisCevap();
                IcmalResult.SonucKodu = "0";
                IcmalResult.SonucAciklama = log.ResponseStatusDesc;
                return result;
            }
            #endregion
            #region Incoming Log
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.Type = ((int)LogType.INCOMING).ToString();
            log.Request = RedDurum.ToXML(true);
            log.Response = result.ToXML(true);
            log.ProviderId = Convert.ToInt64(RedDurum.Kurum);
            IntegrationLogHelper.AddToLog(log);
            #endregion

            try
            {
                string whereCondition = "";
                #region Validation
                #region User kontrolü
                if (!isValidUser(RedDurum.UserName, RedDurum.Password, ref CompanyID))
                {
                    log.HttpStatus = ((int)HttpStatusCode.Unauthorized).ToString();
                    log.ResponseStatusCode = PortTssErrors.UserNameOrPasswordInvalid.Split(':')[0];
                    log.ResponseStatusDesc = PortTssErrors.UserNameOrPasswordInvalid.Split(':')[1];
                    log.Type = ((int)LogType.FAILED).ToString();

                    #region Invalid service user
                    result.SonucKodu = "0";
                    result.SonucAciklama = log.ResponseStatusDesc;
                    log.Response = result.ToXML(true);
                    IntegrationLogHelper.AddToLog(log, true);
                    return result;
                    #endregion
                }
                log.COMPANY_ID = CompanyID;
                #endregion
                #region Provider Kontrolü
                V_Provider provider = new GenericRepository<V_Provider>().FindBy($"PROVIDER_ID = {RedDurum.Kurum}", orderby: "").FirstOrDefault();
                if (provider == null)
                {
                    log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                    log.Type = ((int)LogType.FAILED).ToString();
                    log.ResponseStatusCode = PortTssErrors.ProviderNotFound.Split(':')[0];
                    log.ResponseStatusDesc = PortTssErrors.ProviderNotFound.Split(':')[1];

                    result.SonucKodu = "0";
                    result.SonucAciklama = log.ResponseStatusDesc;
                    log.Response = result.ToXML(true);
                    IntegrationLogHelper.AddToLog(log, true);
                    return result;
                }
                #endregion
                #region Sigortalı Aktif mi ? 
                providerId = provider.PROVIDER_ID;
                tssContractId = !string.IsNullOrEmpty(provider.TSS_CONTRACT_ID.ToString()) ? long.Parse(provider.TSS_CONTRACT_ID.ToString()) : 0;
                processGroupId = new ClaimWorker().GetContractProcessGroupId(tssContractId);

                whereCondition = $"COMPANY_ID = {CompanyID} AND POLICY_NUMBER = {RedDurum.PoliceNo} AND IDENTITY_NO = {RedDurum.TCKimlikNo} AND POLICY_STATUS='{(int)PolicyStatus.TANZIMLI}' AND POLICY_IS_OPEN_TO_CLAIM='1' AND IS_OPEN_TO_CLAIM='1'";

                List<V_Insured> InsuredEndorsement = new GenericRepository<V_Insured>().FindBy(conditions: whereCondition, orderby: "");
                if (!InsuredEndorsement.Any())
                {
                    log.ResponseStatusCode = PortTssErrors.InsuredNotFound.Split(':')[0];
                    log.ResponseStatusDesc = PortTssErrors.InsuredNotFound.Split(':')[1];

                    result.SonucKodu = "0";
                    result.SonucAciklama = log.ResponseStatusDesc;

                    log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                    log.Type = ((int)LogType.FAILED).ToString();
                    log.WsResponseDate = DateTime.Now;
                    log.Response = result.ToXML(true);
                    IntegrationLogHelper.AddToLog(log, true);

                    return result;
                }
                #endregion
                #region Claim var mı ?
                var claimWorker = new ClaimWorker();
                claimId = claimWorker.ExistsClaim(RedDurum.TakipNo);
                if (claimId > 0)
                {
                    new ClaimWorker().ClaimExistOperation(claimId);
                }
                #endregion
                #endregion

                #region Worker
                #region InsertNewClaim
                Thread.Sleep(100);
                Claim claim = new Claim
                {
                    Id = claimId,
                    NoticeDate = DateTime.Now,
                    ProviderId = providerId,
                    InsuredId = InsuredEndorsement[0].INSURED_ID,
                    Type = ((int)ClaimType.TSS).ToString(),
                    Status = ((int)ClaimStatus.GIRIS).ToString(),
                    MedulaNo = RedDurum.TakipNo,
                    SourceType = ((int)ClaimSourceType.PORTTSS).ToString(),
                    PaymentMethod = ((int)ClaimPaymentMethod.KURUM).ToString(),
                    ClaimDate = Convert.ToDateTime(RedDurum.TakipTarihi)
                };
                SpResponse spResponseClaim = new GenericRepository<Claim>().Insert(claim, this.Token);
                if (spResponseClaim.Code == "100")
                {
                    claimId = spResponseClaim.PkId;
                }
                else
                {
                    log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                    log.Type = ((int)LogType.FAILED).ToString();
                    log.ResponseStatusCode = PortTssErrors.ClaimNotCreated.Split(':')[0];
                    log.ResponseStatusDesc = PortTssErrors.ClaimNotCreated.Split(':')[1];

                    result.SonucKodu = "0";
                    result.SonucAciklama = log.ResponseStatusDesc;
                    log.Response = result.ToXML(true);
                    IntegrationLogHelper.AddToLog(log, true);
                    return result;
                }
                #endregion
                #region Tanılar ICD
                whereCondition = $"TYPE = '{((int)ProcessListType.TANI).ToString()}'";
                List<ProcessList> icdList = new GenericRepository<ProcessList>().FindBy(conditions: whereCondition);
                string icdListIds = "";
                if (icdList != null)
                    if (icdList.Count > 0)
                        for (int i = 0; i < icdList.Count; i++)
                            icdListIds += (i == 0 ? icdList[i].Id.ToString() : "," + icdList[i].Id.ToString());


                if (RedDurum.Tanilar.Count > 0)
                {
                    foreach (Tani tani in RedDurum.Tanilar)
                    {
                        if (!string.IsNullOrEmpty(tani.TaniKodu))
                        {
                            whereCondition = $"CODE = '{tani.TaniKodu}' AND PROCESS_LIST_ID IN ({icdListIds})";

                            Int64 processId = 0;
                            Process responseProcess = new GenericRepository<Process>().FindBy(whereCondition).FirstOrDefault();
                            if (responseProcess == null)
                            {
                                Process process = new Process
                                {
                                    Id = 0,
                                    Amount = 0,
                                    Code = tani.TaniKodu,
                                    Name = tani.TaniAdi,
                                    ProcessListId = icdList[0].Id
                                };
                                SpResponse spResponseProcess = new GenericRepository<Process>().Insert(process, this.Token);
                                if (spResponseProcess.Code != "100")
                                {
                                    log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                                    log.Type = ((int)LogType.FAILED).ToString();
                                    log.ResponseStatusCode = PortTssErrors.ProcessNotCreated.Split(':')[0];
                                    log.ResponseStatusDesc = PortTssErrors.ProcessNotCreated.Split(':')[1];

                                    result.SonucKodu = "0";
                                    result.SonucAciklama = log.ResponseStatusDesc;
                                    log.Response = result.ToXML(true);
                                    IntegrationLogHelper.AddToLog(log, true);
                                    return result;
                                }
                                processId = spResponseProcess.PkId;
                            }
                            else
                            {
                                processId = responseProcess.Id;
                            }

                            ClaimIcd claimIcd = new ClaimIcd
                            {
                                Id = 0,
                                ClaimId = claimId,
                                IcdId = processId,
                                Status = ((int)Status.AKTIF).ToString(),
                                IcdType = tani.TaniTipi
                            };
                            SpResponse responseClaim = new GenericRepository<ClaimIcd>().Insert(claimIcd, this.Token);
                            if (responseClaim.Code != "100")
                            {
                                log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                                log.Type = ((int)LogType.FAILED).ToString();
                                log.ResponseStatusCode = PortTssErrors.ClaimIcdNotCreated.Split(':')[0];
                                log.ResponseStatusDesc = PortTssErrors.ClaimIcdNotCreated.Split(':')[1];

                                result.SonucKodu = "0";
                                result.SonucAciklama = log.ResponseStatusDesc;
                                log.Response = result.ToXML(true);
                                IntegrationLogHelper.AddToLog(log, true);
                                return result;
                            }
                        }
                    }
                }
                #endregion
                #region Hizmet
                //whereCondition = $"TYPE = '{((int)ProcessListType.SUT).ToString()}'";
                //List<ProcessList> processList = new GenericRepository<ProcessList>().FindBy(conditions: whereCondition);
                //string processListIds = "";
                //if (processList == null)
                //    if (processList.Count > 0)
                //        for (int i = 0; i < processList.Count; i++)
                //            processListIds += (i == 0 ? processList[i].ToString() : "," + processList[i].ToString());

                //foreach (var processInfo in RedDurum.HizmetBilgileri)
                //{
                //    if (!string.IsNullOrEmpty(processInfo.HizmetKodu))
                //    {
                //        long? staffId = 0;

                //        whereCondition = $"CODE = '{processInfo.HizmetKodu}' AND PROCESS_LIST_ID IN ({processListIds}) and STATUS = '{((int)Status.AKTIF).ToString()}'";

                //        Int64 processId = 0;
                //        Process responseProcess = new GenericRepository<Process>().FindBy(whereCondition).FirstOrDefault();
                //        if (responseProcess == null)
                //        {
                //            Process process = new Process
                //            {
                //                Id = 0,
                //                Code = processInfo.HizmetKodu,
                //                Name = processInfo.HizmetAdi,
                //                ProcessListId = processList[0].Id
                //            };
                //            SpResponse<Process> spResponseProcess = new GenericRepository<Process>().Insert(process, this.Token);
                //            if (spResponseProcess.Code != "100")
                //            {
                //                log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                //                log.Type = ((int)LogType.FAILED).ToString();
                //                log.ResponseStatusCode = PortTssErrors.ProcessNotCreated.Split(':')[0];
                //                log.ResponseStatusDesc = PortTssErrors.ProcessNotCreated.Split(':')[1];
                //                IntegrationLogHelper.AddToLog(log, true);

                //                result.SonucKodu = "0";
                //                result.SonucAciklama = log.ResponseStatusDesc;
                //                return result;
                //            }
                //            processId = spResponseProcess.PkId;
                //        }
                //        //= long.Parse(processInfo.HizmetKodu);

                //        ClaimProcess claimProcess = new ClaimProcess
                //        {
                //            ProcessId = processId,
                //            ClaimId = claimId,
                //            Status = ((int)Status.AKTIF).ToString(),
                //            ContractProcessGroupId = processGroupId,
                //            CoverageId = new CoverageWorker().GetCoverageID(processInfo.HizmetKodu)
                //        };

                //        SpResponse<ClaimProcess> responseClaimProcess = new GenericRepository<ClaimProcess>().Insert(claimProcess, this.Token);
                //        if (responseClaimProcess.Code != "100")
                //        {
                //            log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                //            log.Type = ((int)LogType.FAILED).ToString();
                //            log.ResponseStatusCode = PortTssErrors.ProcessNotCreated.Split(':')[0];
                //            log.ResponseStatusDesc = PortTssErrors.ProcessNotCreated.Split(':')[1];
                //            IntegrationLogHelper.AddToLog(log, true);

                //            result.SonucKodu = "0";
                //            result.SonucAciklama = log.ResponseStatusDesc;
                //            return result;
                //        }
                //    }
                //}
                #endregion

                #region Provizyon Red
                var claimNew = new GenericRepository<Claim>().FindById(claimId);
                claimNew.Status = ((int)ClaimStatus.RET).ToString();
                SpResponse spResponse = new GenericRepository<Claim>().Insert(claimNew, Token);
                if (spResponse.Code != "100")
                {
                    log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                    log.Type = ((int)LogType.FAILED).ToString();
                    log.ResponseStatusCode = PortTssErrors.ClaimNotCreated.Split(':')[0];
                    log.ResponseStatusDesc = PortTssErrors.ClaimNotCreated.Split(':')[1];
                    result.SonucKodu = "0";
                    result.SonucAciklama = log.ResponseStatusDesc;
                    log.Response = result.ToXML(true);
                    IntegrationLogHelper.AddToLog(log, true);
                    return result;
                }

                var _Company = new CompanyHelper().WhichCompany((long)CompanyID);
                ProxyServiceClient proxyServiceClient = new ProxyServiceClient();
                ClaimStateUpdateReq stateUpdateReq = new ClaimStateUpdateReq
                {
                    ClaimId = claimId
                };

                if (_Company == CompanyEnum.HDI || _Company == CompanyEnum.HDI_TEST)
                {
                    proxyServiceClient.ClaimRejection(stateUpdateReq);
                }

                #endregion
                #endregion

                result.SonucAciklama = "işlem başarıyla sonuçlandı";
                result.SonucKodu = "1";
                return result;
            }
            catch (Exception ex)
            {
                log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                log.Type = ((int)LogType.FAILED).ToString();
                log.ResponseStatusCode = "999";
                log.ResponseStatusDesc = ex.Message;

                result.SonucKodu = "0";
                result.SonucAciklama = log.ResponseStatusDesc;
                log.Response = result.ToXML(true);
                IntegrationLogHelper.AddToLog(log, true);
            }

            return result;

        }
        public RedDurumIptalCevap RedDurumIptal(RedDurumIptalBilgisi RedDurumIptal)
        {
            RedDurumIptalCevap result = new RedDurumIptalCevap();
            IntegrationLog log = new IntegrationLog();
            var currentMethod = MethodBase.GetCurrentMethod();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.WsMethod = currentMethod.Name;
            log.WsRequestDate = DateTime.Now;

            long? CompanyID = 0;
            long claimId = 0;

            ValidationResponse validationResponse = Validator.Worker<RedDurumIptalBilgisi>(RedDurumIptal);
            #region Validation
            if (!validationResponse.IsValid)
            {
                result.SonucKodu = validationResponse.Code;
                result.SonucAciklama = validationResponse.Description;
                try
                {
                    log.HttpStatus = ((int)HttpStatusCode.LengthRequired).ToString();
                    log.ResponseStatusCode = PortTssErrors.ValidationInvalid.Split(':')[0];
                    log.ResponseStatusDesc = PortTssErrors.ValidationInvalid;
                    log.Type = ((int)LogType.FAILED).ToString();
                    log.WsResponseDate = DateTime.Now;
                    log.Request = RedDurumIptal.ToXML(true);
                    log.Response = result.ToXML(true);
                }
                catch { }
                IntegrationLogHelper.AddToLog(log, true);
                log.ResponseStatusCode = "";
                log.ResponseStatusDesc = "";

                IcmalGirisCevap IcmalResult = new IcmalGirisCevap();
                IcmalResult.SonucKodu = "0";
                IcmalResult.SonucAciklama = log.ResponseStatusDesc;
                return result;
            }
            #endregion
            #region Incoming Log
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.Type = ((int)LogType.INCOMING).ToString();
            log.Request = RedDurumIptal.ToXML(true);
            log.Response = result.ToXML(true);
            log.ProviderId = Convert.ToInt64(RedDurumIptal.Kurum);
            IntegrationLogHelper.AddToLog(log);
            #endregion

            try
            {
                #region User kontrolü
                if (!isValidUser(RedDurumIptal.UserName, RedDurumIptal.Password, ref CompanyID))
                {
                    log.HttpStatus = ((int)HttpStatusCode.Unauthorized).ToString();
                    log.ResponseStatusCode = PortTssErrors.UserNameOrPasswordInvalid.Split(':')[0];
                    log.ResponseStatusDesc = PortTssErrors.UserNameOrPasswordInvalid.Split(':')[1];
                    log.Type = ((int)LogType.FAILED).ToString();
                    log.Response = result.ToXML(true);

                    #region Invalid service user
                    RedDurumIptalCevap icmailCevap = new RedDurumIptalCevap();
                    icmailCevap.SonucKodu = "0";
                    icmailCevap.SonucAciklama = log.ResponseStatusDesc;
                    result = icmailCevap;
                    IntegrationLogHelper.AddToLog(log, true);
                    return result;
                    #endregion
                }
                log.COMPANY_ID = CompanyID;
                #endregion

                #region Validation
                #region User kontrolü
                if (!isValidUser(RedDurumIptal.UserName, RedDurumIptal.Password, ref CompanyID))
                {
                    log.HttpStatus = ((int)HttpStatusCode.Unauthorized).ToString();
                    log.ResponseStatusCode = PortTssErrors.UserNameOrPasswordInvalid.Split(':')[0];
                    log.ResponseStatusDesc = PortTssErrors.UserNameOrPasswordInvalid.Split(':')[1];
                    log.Type = ((int)LogType.FAILED).ToString();
                    log.Response = result.ToXML(true);
                    #region Invalid service user
                    result.SonucKodu = "0";
                    result.SonucAciklama = log.ResponseStatusDesc;
                    IntegrationLogHelper.AddToLog(log, true);
                    return result;
                    #endregion
                }
                #endregion
                #region Provider Kontrolü
                V_Provider provider = new GenericRepository<V_Provider>().FindBy($"PROVIDER_ID = {RedDurumIptal.Kurum}", orderby: "").FirstOrDefault();
                if (provider == null)
                {
                    log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                    log.Type = ((int)LogType.FAILED).ToString();
                    log.ResponseStatusCode = PortTssErrors.ProviderNotFound.Split(':')[0];
                    log.ResponseStatusDesc = PortTssErrors.ProviderNotFound.Split(':')[1];
                    log.Response = result.ToXML(true);
                    IntegrationLogHelper.AddToLog(log, true);

                    result.SonucKodu = "0";
                    result.SonucAciklama = log.ResponseStatusDesc;
                    return result;
                }
                #endregion
                #region Claim var mı ?
                var claimWorker = new ClaimWorker();
                claimId = claimWorker.ExistsCancelClaim(RedDurumIptal.TakipNo);
                if (claimId == 0)
                {
                    log.HttpStatus = ((int)HttpStatusCode.Unauthorized).ToString();
                    log.ResponseStatusCode = PortTssErrors.UserNameOrPasswordInvalid.Split(':')[0];
                    log.ResponseStatusDesc = RedDurumIptal.TakipNo + " Takip Nolu Provizyon bilgisi bulunamadı.";
                    log.Type = ((int)LogType.FAILED).ToString();
                    log.Response = result.ToXML(true);
                    IntegrationLogHelper.AddToLog(log, true);
                    result.SonucAciklama = RedDurumIptal.TakipNo + " Takip Nolu Provizyon bilgisi bulunamadı.";
                    result.SonucKodu = "0";
                    return result;
                }

                log.ClaimId = claimId;
                #endregion
                #endregion

                Claim claim = new GenericRepository<Claim>().FindById(claimId);
                claim.Status = ((int)ClaimStatus.GIRIS).ToString();
                SpResponse spResponseClaim = new GenericRepository<Claim>().Insert(claim, this.Token);
                if (spResponseClaim.Code != "100")
                {
                    log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                    log.Type = ((int)LogType.FAILED).ToString();
                    log.ResponseStatusCode = PortTssErrors.ClaimNotCreated.Split(':')[0];
                    log.ResponseStatusDesc = PortTssErrors.ClaimNotCreated.Split(':')[1];
                    log.Response = result.ToXML(true);
                    IntegrationLogHelper.AddToLog(log, true);

                    result.SonucKodu = "0";
                    result.SonucAciklama = log.ResponseStatusDesc;
                    return result;
                }

                result.SonucAciklama = "işlem başarıyla sonuçlandı";
                result.SonucKodu = "1";
                return result;
            }
            catch (Exception ex)
            {
                log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                log.Type = ((int)LogType.FAILED).ToString();
                log.ResponseStatusCode = "999";
                log.ResponseStatusDesc = ex.Message;
                IntegrationLogHelper.AddToLog(log, true);

                result.SonucKodu = "0";
                result.SonucAciklama = log.ResponseStatusDesc;
            }
            return result;
        }

        #region Private methods
        private bool isValidUser(string username, string password, ref long? CompanyId)
        {
            bool result = false;
            try
            {
                string ApiCodeCompany = ConfigurationManager.AppSettings["USER_NAME: " + username + " - PASSWORD: " + password];
                if (ApiCodeCompany.IsInt64())
                {
                    CompanyId = long.Parse(ApiCodeCompany);
                    result = true;
                }
                //if (ApiCodeCompany == apiCode)
                //    id = CompanyCode;

                //string whereCondition = $" SYSTEM_TYPE = '{(int)SystemType.PORTTSS}' and KEY = '{AppKeys.PortTSSUserName}' and VALUE = '{username}'";
                //V_CompanyParameter companyParam = new GenericRepository<V_CompanyParameter>().FindBy(conditions: whereCondition, orderby: "").FirstOrDefault();
                //if (companyParam != null)
                //{
                //    whereCondition = $"SYSTEM_TYPE = '{(int)SystemType.PORTTSS}' and COMPANY_ID = {companyParam.CompanyId} and KEY = '{AppKeys.PortTSSPassword}' and value = '{password}'";
                //    companyParam = new GenericRepository<V_CompanyParameter>().FindBy(conditions: whereCondition, orderby: "").FirstOrDefault();

                //    CompanyId = companyParam.CompanyId;
                //    result = true;

                //}
            }
            catch (Exception) { result = false; }

            return result;
        }
        private List<SigortaliBilgi> InsuredEndorsementToSigortaliAraResult(List<V_InsuredEndorsement> lst)
        {
            SigortaliAraResult returnLst = new SigortaliAraResult();
            returnLst.SigortaliBilgiList = new List<SigortaliBilgi>();
            foreach (V_InsuredEndorsement insured in lst)
            {
                var sigortaliBilgi = new SigortaliBilgi
                {
                    ExMusteriId = insured.CONTACT_ID.ToString(),
                    PolicyID = (long)insured.POLICY_ID,
                    Tarife = insured.TSS_SUBPRODUCT_CODE,
                    SigortaliAdSoyad = insured.FIRST_NAME + " " + insured.LAST_NAME,
                    UrunKodu = insured.TSS_PRODUCT_CODE,
                    PackageID = (long)insured.PACKAGE_ID,
                    InsuredID = insured.INSURED_ID,
                    InsuredNo = insured.COMPANY_INSURED_NO,
                    YenilemeNo = (int)insured.RENEWAL_NO,
                    PoliceNo = insured.POLICY_NUMBER.ToString(),
                    PoliceBitisTarihi = insured.POLICY_END_DATE.ToString(),
                    PolicyIsOpenTheClaim = insured.POLICY_IS_OPEN_TO_CLAIM,
                    InsuredIsOpenTheClaim = insured.INSURED_IS_OPEN_TO_CLAIM,
                    VipType = insured.TSS_VIP
                };

                returnLst.SigortaliBilgiList.Add(sigortaliBilgi);
            }
            return returnLst.SigortaliBilgiList;
        }
        private List<TeminatBilgileri> InsuredCoverageToTeminatBilgileri(List<V_InsuredPackageRemaining> PckRmngList)
        {
            List<TeminatBilgileri> returnLst = new List<TeminatBilgileri>();

            var packageRemaining = PckRmngList.Where(x => x.IsMainCoverage == 0).ToList();
            if (packageRemaining != null && packageRemaining.Count > 0)
            {
                var coverageIdList = string.Join(",", packageRemaining.Select(p => p.CoverageId).ToList());
                string whereCondition = $"COVERAGE_ID IN ({coverageIdList}) AND KEY =:key AND SYSTEM_TYPE =:systemType";
                List<V_CoverageParameter> coverageParameterList = new GenericRepository<V_CoverageParameter>().FindBy(conditions: whereCondition, orderby: "", parameters: new { key = new Dapper.DbString { Value = AppKeys.PortTSSCoverageCode, Length = 100 }, systemType = new Dapper.DbString { Value = ((int)SystemType.PORTTSS).ToString(), Length = 3 } });

                foreach (V_InsuredPackageRemaining insCoverageRule in packageRemaining)
                {
                    #region CoverageParameter
                    V_CoverageParameter coverageParameter = coverageParameterList.Where(c => c.CoverageId == insCoverageRule.CoverageId).ToList().FirstOrDefault();
                    if (coverageParameter == null) continue; if (string.IsNullOrEmpty(coverageParameter.CoverageName)) continue;

                    TeminatBilgileri temBilgi = new TeminatBilgileri();
                    temBilgi.TeminatAdi = coverageParameter.CoverageName;
                    temBilgi.TeminatAciklama = ""; // açıklama olacak mı ?
                    temBilgi.TeminatKodu = coverageParameter.Value;
                    returnLst.Add(temBilgi);
                    #endregion
                }
            }
            return returnLst;
        }
        private IcmalResult IcmalEntryWorker(IcmalParams icmalParams, long? CompanyID, ref long? PayrolID, int TssContractID, long ProviderID)
        {
            IcmalResult resultIcmal = new IcmalResult();
            resultIcmal.IcmalBilgi = new IcmalBilgi();

            if (icmalParams != null)
            {
                bool findPayroll = true;
                string whereCondition = $" COMPANY_ID = {CompanyID} AND PROVIDER_ID = {ProviderID} AND EXT_PROVIDER_NO = '{icmalParams.IcmalNo}' AND TYPE='{((int)PayrollType.KURUM).ToString()}' AND CATEGORY='{((int)PayrollCategory.TSS).ToString()}'";
                Payroll payroll = new GenericRepository<Payroll>().FindBy(whereCondition, orderby: "", fetchDeletedRows: true).FirstOrDefault();
                if (payroll == null) findPayroll = false;
                else findPayroll = true;

                //Payroll payroll = new Payroll();
                if (!findPayroll) //bulamazsa insert at
                {
                    payroll = new Payroll
                    {
                        CompanyId = long.Parse(CompanyID.ToString()),
                        PayrollDate = DateTime.Now,
                        ProviderId = ProviderID,
                        Category = ((int)PayrollCategory.TSS).ToString(),
                        Type = ((int)PayrollType.KURUM).ToString(),
                        ExtProviderNo = icmalParams.IcmalNo,
                        ExtProviderDate = DateTime.Now,
                        Status = ((int)PayrollStatus.Onay_Bekler).ToString()
                    };
                    SpResponse spResponsePayroll = new GenericRepository<Payroll>().Insert(payroll, this.Token);
                    if (spResponsePayroll.Code == "100")
                    {
                        PayrolID = spResponsePayroll.PkId;
                    }
                    else
                    {
                        resultIcmal.Message = "İcmal Oluşturulurken Hata Oluştu.";
                        resultIcmal.Success = false;
                        return resultIcmal;
                    }
                }
                else
                {
                    PayrolID = payroll.Id;
                }

                whereCondition = $"type = '{(int)ContractType.TSS}' and id = {TssContractID}";
                Contract conlist = new GenericRepository<Contract>().FindBy(conditions: whereCondition, orderby: "").FirstOrDefault();

                if (conlist.PaymentDay > 0)
                {
                    DateTime date = (DateTime)payroll.PayrollDate;
                    payroll.Due_Date = date.AddDays((double)conlist.PaymentDay);
                    payroll.Status = ((int)PayrollStatus.Onay_Bekler).ToString();
                    payroll.Id = (long)PayrolID;
                    new GenericRepository<Payroll>().Insert(payroll, this.Token);

                    string claimIdList = string.Join(",", icmalParams.IcmalTazminatBilgiList.Select(x => x.ProvizyonNo));

                    var providerankAccount = new GenericRepository<V_ProviderBankAccount>().FindBy($"PROVIDER_ID={conlist.ProviderId}", orderby: "PROVIDER_ID DESC").FirstOrDefault();

                    List<ClaimBill> claimBillList = new GenericRepository<ClaimBill>().FindBy($"CLAIM_ID IN ({claimIdList})");
                    List<ClaimPayment> claimPaymentList = new GenericRepository<ClaimPayment>().FindBy($"CLAIM_ID IN ({claimIdList})");
                    List<Claim> claimList = new GenericRepository<Claim>().FindBy($"ID IN ({claimIdList})");

                    List<ClaimBill> createClaimBillList = new List<ClaimBill>();
                    List<ClaimPayment> createClaimPaymentList = new List<ClaimPayment>();
                    List<Claim> createClaimList = new List<Claim>();

                    foreach (IcmalTazminatBilgi tazBilg in icmalParams.IcmalTazminatBilgiList)
                    {
                        ClaimBill claimBill = claimBillList.Where(x => x.ClaimId == long.Parse(tazBilg.ProvizyonNo)).FirstOrDefault();
                        claimBill = new ClaimBill
                        {
                            Id = claimBill != null ? claimBill.Id : 0,
                            ClaimId = long.Parse(tazBilg.ProvizyonNo),
                            BillNo = tazBilg.FaturaNo,
                            BillDate = DateTime.Parse(tazBilg.FaturaTarihi),
                            PaymentType = ((int)PaymentType.BANKAYA_ODEME).ToString(),
                            Status = ((int)BillStatus.ODEME_BEKLER).ToString()
                        };
                        createClaimBillList.Add(claimBill);


                        ClaimPayment claimPayment = claimPaymentList.Where(x => x.ClaimId == long.Parse(tazBilg.ProvizyonNo)).FirstOrDefault();
                        claimPayment = new ClaimPayment
                        {
                            Id = claimPayment != null ? claimPayment.Id : 0,
                            Amount = tazBilg.Tutar,
                            ClaimId = long.Parse(tazBilg.ProvizyonNo),
                            DueDate = payroll.Due_Date,
                            PaymentType = ((int)PaymentType.BANKAYA_ODEME).ToString(),
                        };
                        claimPayment.BankAccountId = providerankAccount != null ? providerankAccount.BANK_ACCOUNT_ID : null;
                        createClaimPaymentList.Add(claimPayment);


                        Claim claim = claimList.Where(x => x.Id == long.Parse(tazBilg.ProvizyonNo)).FirstOrDefault();
                        claim.PayrollId = PayrolID;
                        claim.Status = ((int)ClaimStatus.ODENECEK).ToString();
                        createClaimList.Add(claim);
                    }
                    bool isClaimBill = true, isClaimPayment = true, isClaim = true;

                    var spResponseClaimBill = new GenericRepository<ClaimBill>().InsertSpExecute3(createClaimBillList);
                    isClaimBill = spResponseClaimBill.Where(x => x.Code != "100").FirstOrDefault() == null ? true : false;

                    var spResponseClaimPayment = new GenericRepository<ClaimPayment>().InsertSpExecute3(createClaimPaymentList);
                    isClaimPayment = spResponseClaimPayment.Where(x => x.Code != "100").FirstOrDefault() == null ? true : false;

                    var spResponseClaim = new GenericRepository<Claim>().InsertSpExecute3(createClaimList);
                    isClaim = spResponseClaim.Where(x => x.Code != "100").FirstOrDefault() == null ? true : false;

                    if (!isClaimBill)
                    {
                        resultIcmal.Message = "Fatura Oluşturulurken Hata Oluştu.";
                        resultIcmal.Success = false;
                        return resultIcmal;
                    }

                    if (!isClaimPayment)
                    {
                        resultIcmal.Message = "Ödeme Bilgileri Kaydedilirken Hata Oluştu.";
                        resultIcmal.Success = false;
                        return resultIcmal;
                    }

                    if (!isClaim)
                    {
                        resultIcmal.Message = "Zarfa Hasar Eklenemedi Hata Oluştu.";
                        resultIcmal.Success = false;
                        return resultIcmal;
                    }

                    resultIcmal.IcmalBilgi.PaketNo = PayrolID.ToString();
                    resultIcmal.Success = true;
                    resultIcmal.IcmalBilgi.SonucAciklama = "işlem başarıyla sonuçlandı";
                    resultIcmal.IcmalBilgi.SonucKodu = "1";
                }
                else
                {
                    resultIcmal.Message = "Kurum Anlaşması Bulunamadı.";
                    resultIcmal.Success = false;
                    return resultIcmal;
                }
            }
            return resultIcmal;
        }
        private IcmalTazminatCikarmaCevap IcmalSubResult(IcmalProvizyonCikartParams param)
        {
            IcmalTazminatCikarmaCevap result = new IcmalTazminatCikarmaCevap();

            string claimIdList = string.Join(",", param.IcmalTazCikartBilgi.Select(x => x.ProvizyonNo));

            List<Claim> claimList = new GenericRepository<Claim>().FindBy($"ID IN ({claimIdList})");

            List<Claim> createClaimList = new List<Claim>();
            foreach (IcmalTazCikartBilgi tazBilgi in param.IcmalTazCikartBilgi)
            {
                Claim claim = claimList.Where(x => x.Id == long.Parse(tazBilgi.ProvizyonNo)).FirstOrDefault();
                claim.PayrollId = null;
                claim.Status = ((int)ClaimStatus.PROZIYON_ONAY).ToString();
                createClaimList.Add(claim);
            }
            var spResponseClaimList = new GenericRepository<Claim>().InsertSpExecute3ForAll(createClaimList);
            foreach (var item in spResponseClaimList)
            {
                if (item.Code != "100")
                {
                    result.SonucAciklama += item.PkId + " HATALI | ";
                    result.SonucKodu = "0";
                }
            }

            if (result.SonucKodu == "0")
            {
                return result;
            }
            result.SonucAciklama = "İşlem Başarıyla Sonuçlandı";
            result.SonucKodu = "1";
            return result;

        }
        private List<MuafiyetBilgileri> InsuredCoverageToMuafiyetBilgileri(long insuredID)
        {
            List<MuafiyetBilgileri> muafiyetBilgileriList = new List<MuafiyetBilgileri>();
            try
            {
                List<V_InsuredExclusion> exclusionList = new GenericRepository<V_InsuredExclusion>().FindBy($"INSURED_ID={insuredID}", orderby: "");

                if (exclusionList != null)
                {
                    foreach (var item in exclusionList)
                    {
                        var muafiyet = new MuafiyetBilgileri
                        {
                            MuafiyetAdi = item.DESCRIPTION,
                            MuafiyetKodu = item.EXCLUSION_ID.ToString()
                        };
                        muafiyetBilgileriList.Add(muafiyet);
                    }
                }
            }
            catch (Exception)
            {
            }
            return muafiyetBilgileriList;
        }
        private class ClaimWorker
        {
            public string Token { get; set; } = "9999999999";

            public Int64 ExistsClaim(string medulaNo)
            {
                Int64 id = 0;

                try
                {
                    var claimList = new GenericRepository<Claim>().FindBy($"MEDULA_NO=:medulaNo", parameters: new { medulaNo = new Dapper.DbString { Value = medulaNo, Length = 20 } }, orderby: "");
                    var claim = new Claim();
                    if (claimList != null)
                    {
                        if (claimList.Count == 1)
                        {
                            claim = claimList[0];
                        }
                        else
                        {
                            claim = claimList.Where(c => c.Status != ((int)ClaimStatus.IPTAL).ToString() && c.Status != ((int)ClaimStatus.RET).ToString()).FirstOrDefault();
                        }
                    }
                    id = claim != null ? claim.Id : 0;
                }
                catch { id = 0; }
                return id;
            }
            public Int64 ExistsCancelClaim(string medulaNo)
            {
                Int64 id = 0;

                try
                {
                    var claim = new GenericRepository<Claim>().FindBy($"MEDULA_NO=:medulaNo AND STATUS=:status", parameters: new { medulaNo = new Dapper.DbString { Value = medulaNo, Length = 20 }, status = new Dapper.DbString { Value = ((int)ClaimStatus.RET).ToString(), Length = 3 } }, orderby: "").FirstOrDefault();
                    id = claim != null ? (long)claim.Id : 0;
                }
                catch { id = 0; }
                return id;
            }
            public void ClaimExistOperation(long ClaimId)
            {
                try
                {
                    string whereCondition = $"CLAIM_ID = {ClaimId}";

                    #region claimICD
                    List<ClaimIcd> claimIcds = new GenericRepository<ClaimIcd>().FindBy(whereCondition);
                    if (claimIcds.Count > 0)
                    {
                        foreach (ClaimIcd clmIcd in claimIcds)
                        {
                            clmIcd.Status = ((int)Status.SILINDI).ToString();
                        }
                        new GenericRepository<ClaimIcd>().InsertSpExecute3(claimIcds, this.Token);

                    }
                    #endregion
                    #region claimProcess
                    List<ClaimProcess> claimProcessList = new GenericRepository<ClaimProcess>().FindBy(whereCondition);
                    if (claimProcessList.Count > 0)
                    {
                        foreach (ClaimProcess clmProc in claimProcessList)
                        {
                            clmProc.Status = ((int)Status.SILINDI).ToString();
                        }
                        var responseClaimProcessList = new ClaimRepository().ClaimProcessExecute(claimProcessList, this.Token);

                    }

                    #endregion
                    #region claimNote
                    List<ClaimNote> claimNotes = new GenericRepository<ClaimNote>().FindBy(whereCondition);
                    if (claimNotes.Count > 0)
                    {
                        foreach (ClaimNote clmNote in claimNotes)
                        {
                            clmNote.Status = ((int)Status.SILINDI).ToString();
                        }
                        new GenericRepository<ClaimNote>().InsertSpExecute3(claimNotes, this.Token);
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    MailSender msender = new MailSender();
                    msender.SendMessage(
                        new EmailArgs
                        {
                            TO = "besim.oznalcin@mooryazilim.com",
                            IsHtml = false,
                            Subject = $"Tekrar Gönderim Hatası",
                            Content = $"ClaimId={ClaimId}  -   Message= {ex.Message}"
                        });
                }
            }
            /// <summary>
            /// Epikriz notu 4000 karakterden buyukse parçalar. List döner. list sayısı kadar note insert
            /// </summary>
            /// <param name="Note"></param>
            /// <returns></returns>
            public List<string> ClaimNoteSplitter(string Note)
            {
                List<string> returnStrLst = new List<string>();
                char seperator = ' ';
                Note = Note.RemoveRepeatedWhiteSpace();

                if (Note.Length > 4000)
                {
                    int totalSplitCount = Note.Split(seperator).Count();

                    string noteText = "";

                    for (int i = 0; i < totalSplitCount; i++)
                    {
                        if (i < (totalSplitCount / 2))
                            noteText += Note.Split(seperator)[i];
                        else
                        {
                            if (i == (totalSplitCount / 2))
                            {
                                returnStrLst.Add(noteText);
                                noteText = "";
                            }
                            noteText += Note.Split(seperator)[i];
                        }
                    }
                    returnStrLst.Add(noteText);
                }
                else if (!Note.IsNull())
                {
                    returnStrLst.Add(Note);
                }
                return returnStrLst;
            }
            public long GetContractProcessGroupId(long ContractID)
            {
                long returnInt = 0;

                string whereCondition = $"CONTRACT_ID = {ContractID}";
                ContractProcessGroup contProcGroup = new GenericRepository<ContractProcessGroup>().FindBy(whereCondition).FirstOrDefault();
                if (contProcGroup != null) returnInt = contProcGroup.ProcessGroupId;
                return returnInt;
            }
        }
        private class CoverageWorker
        {
            public long GetCoverageID(string PortTssCoverCode, long claimId)
            {
                long returnCoverID = 0;
                try
                {
                    string whereCondition = $"CLAIM_ID={claimId} AND VALUE='{PortTssCoverCode}'";

                    V_ClaimCoveragePortTSSParameter coverParam = new GenericRepository<V_ClaimCoveragePortTSSParameter>().FindBy(whereCondition, orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                    if (coverParam != null) returnCoverID = coverParam.COVERAGE_ID;
                }
                catch (Exception e) { }
                return returnCoverID;
            }
            public List<V_CoverageParameter> GetPortTssCoverageCode(string CoverageIDList)
            {
                try
                {
                    if (!CoverageIDList.IsNull())
                    {
                        string whereCondition = $"SYSTEM_TYPE = '{((int)SystemType.PORTTSS).ToString()}' AND KEY='{AppKeys.PortTSSCoverageCode}' AND COVERAGE_ID IN ('{CoverageIDList}')";
                        List<V_CoverageParameter> coverParam = new GenericRepository<V_CoverageParameter>().FindBy(whereCondition, orderby: "");
                        if (coverParam != null) return coverParam;
                    }
                }
                catch (Exception e) { }
                return new List<V_CoverageParameter>();
            }

        }
        private class StaffWorker
        {
            public string Token { get; set; } = "9999999999";
            /// <summary>
            /// 
            /// </summary>
            /// <param name="providerId"></param>
            /// <param name="branch"></param>
            /// <param name="registerNo"></param>
            /// <returns>return: STAFF_ID</returns>
            public long DoctorCreator(long providerId, string branch, string registerNo)
            {
                long staffId = 0;
                long doctorBranchId = GetBranchId(branch);
                if (doctorBranchId > 0)
                {
                    V_Doctor v_Doctor = new GenericRepository<V_Doctor>().FindBy($"DOCTOR_BRANCH_ID={doctorBranchId} AND PROVIDER_ID={providerId} AND REGISTER_NO='{registerNo}'", orderby: "").FirstOrDefault();
                    if (v_Doctor != null)
                    {
                        return v_Doctor.STAFF_ID;
                    }
                    Contact contact = new Contact
                    {
                        Id = 0,
                        Type = ((int)ContactType.GERCEK).ToString(),
                        Status = ((int)Status.AKTIF).ToString()
                    };

                    SpResponse spResponseContact = new GenericRepository<Contact>().Insert(contact, this.Token);
                    if (spResponseContact.Code == "100")
                    {
                        Staff staff = new Staff
                        {
                            RegisterNo = registerNo,
                            DoctorBranchId = doctorBranchId,
                            ContactId = spResponseContact.PkId,
                            ProviderId = providerId,
                            Status = ((int)Status.AKTIF).ToString(),
                            Type = ((int)StaffType.DOKTOR).ToString(),
                            Id = 0
                        };

                        SpResponse responseStaff = new GenericRepository<Staff>().Insert(staff, this.Token);
                        if (responseStaff.Code == "100")
                            staffId = responseStaff.PkId;
                    }
                }
                return staffId;
            }
            public long GetBranchId(string Branch)
            {
                long branchId = 0;

                string whereCondition = $"KEY='CODE' AND VALUE = '{Branch}'";

                V_DoctorBranchParameter resDoctorBranch = new GenericRepository<V_DoctorBranchParameter>().FindBy(whereCondition, orderby: "").FirstOrDefault();
                if (resDoctorBranch != null)
                {
                    branchId = resDoctorBranch.DOCTOR_BRANCH_ID;
                }
                return branchId;
            }
        }
        private class ContactWorker
        {
            public string Token { get; set; } = "9999999999";

            public string GetCompanyContactIdByContactId(long ContactId, CompanyEnum companyEnum)
            {
                string returnStr = "";
                try
                {
                    ProteinEnums.SystemType systemType = new SystemType();
                    if (companyEnum == CompanyEnum.GUNES)
                        systemType = SystemType.PUSULA;
                    else if (companyEnum == CompanyEnum.KATILIM_EMEKLILIK)
                        systemType = SystemType.VAKIF;

                    var ContactParameter = new GenericRepository<V_ContactParameter>().FindBy($" KEY = 'COMPANYCONTACTID' AND CONTACT_ID = '{ContactId.ToString()}' AND SYSTEM_TYPE =  '{((int)systemType).ToString()}' ").FirstOrDefault();

                    if (ContactParameter == null)
                        returnStr = ContactParameter.VALUE;
                }
                catch { }
                return returnStr;
            }
        }
        #endregion
    }
}