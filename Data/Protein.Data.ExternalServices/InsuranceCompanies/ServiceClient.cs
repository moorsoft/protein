﻿using Protein.Business.Abstract.Service;
using Protein.Business.Concrete.Responses;
using Protein.Business.Concrete.Services;
using Protein.Common.Attributes;
using Protein.Common.Constants;
using Protein.Common.Dto.ImportObjects;
using Protein.Common.Enums;
using Protein.Common.Extensions;
using Protein.Common.Messaging;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.Common.Lib.SBM;
using Protein.Data.ExternalServices.InsuranceCompanies.Util.Lib;
using Protein.Data.ExternalServices.Log;
using Protein.Data.ExternalServices.SagmerOnlineService;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;
using static Protein.Data.ExternalServices.SagmerOnlineService.Enums;

namespace Protein.Data.ExternalServices.InsuranceCompanies
{
    public class ServiceClient
    {
        private string Token { get; set; } = "9999999999";
        //Stopwatch stopwatch = new Stopwatch();
        //Dictionary<string, string> reqTime = new Dictionary<string, string>();
        private bool TcNoCheck(string TCNO)
        {
            bool result = false;
            try
            {
                int index = 0;
                int toplam = 0;
                foreach (char n in TCNO)
                {
                    if (index < 10)
                    {
                        toplam += Convert.ToInt32(char.ToString(n));
                    }
                    index++;
                }
                if (toplam % 10 == Convert.ToInt32(TCNO[10].ToString()))
                    result = true;
                else
                    result = false;
            }
            catch
            {
                result = false;
            }

            return result;
        }
        private long ApiCodeIsValidParmeter(string apiCode, long CompanyCode = 0)
        {
            long id = 0;

            try
            {
                string whereCondition = (CompanyCode > 0 ? $"COMPANY_ID = {CompanyCode} AND " : "") + ($" KEY = '{Constants.AppKeys.ApiCode}' AND VALUE = '{apiCode}' AND SYSTEM_TYPE='{((int)SystemType.WS).ToString()}'");
                V_CompanyParameter resultCompanyParam = new GenericRepository<V_CompanyParameter>().FindBy(whereCondition, orderby: "").FirstOrDefault();

                if (resultCompanyParam == null) return id;
                if (resultCompanyParam.Value == apiCode)
                    id = (long)resultCompanyParam.CompanyId;
            }
            catch
            {
                id = 0;
            }
            return id;
        }
        private long ApiCodeIsValid(string apiCode, long CompanyCode = 0)
        {
            long id = 0;

            try
            {
                string ApiCodeCompany = ConfigurationManager.AppSettings["ApiCode" + CompanyCode];

                if (ApiCodeCompany == apiCode)
                    id = CompanyCode;
            }
            catch
            {
                id = 0;
            }
            return id;
        }
        private long CheckTCKNandPassportNo(string TCKN = "", string passportNo = "", long policyId = 0)
        {
            long Id = 0;
            try
            {
                if (TCKN.IsInt64() && ((long?)long.Parse(TCKN)).IsIdentityNoLength() == null)
                {
                    var contact = new GenericRepository<V_Insured>().FindBy($"IDENTITY_NO = {TCKN} AND POLICY_ID={policyId} AND STATUS='{((int)Status.AKTIF)}'", orderby: "").FirstOrDefault();

                    Id = contact == null ? 0 : (long)contact.CONTACT_ID;
                }

                if (!passportNo.IsNull() && Id != 0)
                {
                    var person = new GenericRepository<V_Insured>().FindBy($" PASSPORT_NO = '{passportNo}' AND POLICY_ID={policyId} AND STATUS='{((int)Status.AKTIF)}'", orderby: "").FirstOrDefault();

                    Id = person == null ? 0 : (long)person.CONTACT_ID;
                }

            }
            catch { return 0; }
            return Id;
        }
        private void CreateFailedLog(IntegrationLog log, string responseXml, string[] desc)
        {

            log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
            log.Type = ((int)LogType.FAILED).ToString();
            log.Response = responseXml;
            log.ResponseStatusCode = "0";
            log.ResponseStatusDesc = desc != null ? string.Join(" - ", desc) : "HATA";
            log.WsResponseDate = DateTime.Now;
            IntegrationLogHelper.AddToLog(log);

            MailSender msender = new MailSender();
            msender.SendMessage(
                new EmailArgs
                {
                    TO = "pmo@imecedestek.com",
                    IsHtml = true,
                    Subject = $"Servis Aktarım Hatası",
                    Content = $"Correlation ID: {log.CorrelationId.ToString()}<br />" +
                                $"Şirket Kodu: {log.COMPANY_ID.ToString()}<br />" +
                                $"Web Servis Adı: {log.WsName}<br />" +
                                $"WS Method: {log.WsMethod}<br />" +
                                $"WS İstek Tarihi: {log.WsRequestDate}<br />" +
                                $"WS Hata Mesajı: {log.ResponseStatusDesc}<br />"


                });


            //log.CorrelationId - log.COMPANY_ID - log.WsName - log.WsMethod - log.WsRequestDate - log.ResponseStatusDesc 
        }
        private void CreateOKLog(IntegrationLog log, string responseXml)
        {
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.Type = ((int)LogType.COMPLETED).ToString();
            log.Response = responseXml;
            log.ResponseStatusCode = "200";
            log.ResponseStatusDesc = "OK";
            log.WsResponseDate = DateTime.Now;
            IntegrationLogHelper.AddToLog(log);
        }
        private void CreateIncomingLog(IntegrationLog log)
        {
            log.HttpStatus = ((int)HttpStatusCode.Continue).ToString();
            log.Type = ((int)LogType.INCOMING).ToString();
            log.ResponseStatusCode = "100";
            log.ResponseStatusDesc = "INCOMING";
            IntegrationLogHelper.AddToLog(log);
        }

        #region PoliceAktar
        public PoliceAktarRes PoliceAktar(string apiCode, PoliceAktarReq req)
        {
            PoliceAktarRes response = new PoliceAktarRes();
            List<string> errorArray = new List<string>();
            var currentMethod = MethodBase.GetCurrentMethod();

            #region LOG - Correlation ID is set once the log created
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            #endregion

            try
            {
                #region Validation
                PoliceAktarInnerRes innerResponse = PoliceAktarValidation(req, apiCode, log);

                if (innerResponse.SonucKod != "1")
                {
                    response.SonucAciklama = innerResponse.SonucAciklama;
                    response.SonucKod = innerResponse.SonucKod;
                    response.ValidationErrors = innerResponse.ValidationErrors;

                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);

                    return response;
                }
                #endregion

                #region Operation
                if (req.Kontrol == null || !((bool)req.Kontrol))
                {
                    response = PoliceAktarOpr(req, log.CorrelationId, log, innerResponse.Package);
                    if (response.SonucKod != "1")
                    {
                        CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                        return response;
                    }
                }
                #endregion

                #region OK
                response.SonucAciklama = "OK";
                response.SonucKod = "1";
                CreateOKLog(log, response.ToXML(true));
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_OK_LOG_CREATE" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                #endregion

            }
            catch (Exception ex)
            {
                #region Catch Error
                response.SonucKod = "0";
                response.SonucAciklama = "HATA - ValidationErrors Listesini Kontrol Ediniz";
                response.ValidationErrors = new string[] { ex.Message };

                CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                #endregion
            }
            return response;
        }
        private PoliceAktarInnerRes PoliceAktarValidation(PoliceAktarReq req, string apiCode, IntegrationLog log)
        {
            PoliceAktarInnerRes response = new PoliceAktarInnerRes();
            List<string> errorArray = new List<string>();

            #region Incoming Validation
            if (req.PoliceBilgi == null)
            {
                response.SonucKod = "0";
                errorArray.Add("POLICE BİLGİ ALANI GİRİLMESİ GEREKMEKTEDİR");
            }
            else
            {
                if (!req.PoliceBilgi.SirketKod.ToString().IsNumeric())
                {
                    response.SonucKod = "0";
                    errorArray.Add("ŞİRKET KOD GİRİLMESİ GEREKMEKTEDİR");
                }
                if (!req.PoliceBilgi.PoliceNo.ToString().IsNumeric() || !req.PoliceBilgi.YenilemeNo.ToString().IsNumeric())
                {
                    response.SonucKod = "0";
                    errorArray.Add("POLİÇE NO VE YENİLEME NO GİRİLMESİ GEREKMEKTEDİR");
                }
            }
            if (response.SonucKod == "0")
            {
                response.ValidationErrors = errorArray.ToArray();
                response.SonucAciklama = "HATA";
                return response;
            }
            #endregion

            #region Auth
            if (ApiCodeIsValid(apiCode, req.PoliceBilgi.SirketKod) <= 0)
            {
                response.SonucAciklama = "HATA";
                response.SonucKod = "0";
                errorArray.Add("Giriş Başarısız! APİCODE'u kontrol ediniz");
                response.ValidationErrors = errorArray.ToArray();
                return response;
            }
            log.COMPANY_ID = req.PoliceBilgi.SirketKod;
            #endregion

            #region Zorunlu Alan
            if (!req.PolBaslamaTarih.ToString().IsDateTime() || !req.PolBitisTarih.ToString().IsDateTime() || !req.PolTanzimTarih.ToString().IsDateTime())
            {
                response.SonucKod = "0";
                errorArray.Add("POLİÇE TARİH BİLGİLERİ GİRİLMESİ GEREKMEKTEDİR");
            }

            if (req.PolBitisTarih < req.PolBaslamaTarih)
            {
                response.SonucKod = "0";
                errorArray.Add("POLİÇE BİTİŞ TARİHİ BAŞLANGIÇ TARİHİNDEN KÜÇÜK OLAMAZ");
            }
            if (req.PolTanzimTarih > req.PolBitisTarih)
            {
                response.SonucKod = "0";
                errorArray.Add("POLİÇE TANZİM TARİHİ BİTİŞ TARİHİNDEN BÜYÜK OLAMAZ");
            }
            if (req.GrupFerdi.IsNull())
            {
                response.SonucKod = "0";
                errorArray.Add("POLİÇE TİPİ BİLGİSİ GİRİLMESİ GEREKMEKTEDİR");
            }
            else
            {
                if (req.GrupFerdi.ToUpper() != "F" && req.GrupFerdi.ToUpper() != "G")
                {
                    response.SonucKod = "0";
                    errorArray.Add("GRUP FERDİ BİLGİSİ HATALI (G,F)");
                }
            }
            if (req.AcenteKod.IsNull())
            {
                response.SonucKod = "0";
                errorArray.Add("ACENTE BİLGİSİ GİRİLMESİ GEREKMEKTEDİR");
            }
            if (req.OdemeSekli.IsNull())
            {
                req.OdemeSekli = "1";
            }
            else
            {
                if (req.OdemeSekli.IsInt())
                {
                    req.OdemeSekli = LookupHelper.GetLookupCodeByOrdinal(LookupTypes.Payment, req.OdemeSekli);

                    if (req.OdemeSekli.IsNull())
                        req.OdemeSekli = "10";
                }
                else
                {
                    if (req.OdemeSekli == "N")
                        req.OdemeSekli = "1";
                    else if (req.OdemeSekli == "K")
                        req.OdemeSekli = "2";
                    else
                        req.OdemeSekli = "10";
                }
            }
            if (!req.VadeAdet.ToString().IsInt())
            {
                req.VadeAdet = 1;
            }
            else
            {
                if (int.Parse(req.VadeAdet.ToString()) < 0)
                {
                    response.SonucKod = "0";
                    errorArray.Add("VADE ADET 0'DAN KÜÇÜK OLAMAZ");
                }
            }
            if (req.SigEttiren == null)
            {
                response.SonucKod = "0";
                errorArray.Add("SİGORTA ETTİREN BİLGİSİ GİRİLMESİ GEREKMEKTEDİR");
            }
            else
            {
                switch (req.SigEttiren.GercekTuzel)
                {
                    case true: //Gerçek
                        if (req.SigEttiren.GK == null)
                        {
                            response.SonucKod = "0";
                            errorArray.Add("SİGORTA ETTİREN, GERÇEK KİŞİ İÇİN GK ALANI BOŞ GEÇİLEMEZ!");
                        }
                        else
                        {
                            if (req.SigEttiren.GK.Uyruk.IsNull())
                            {
                                response.SonucKod = "0";
                                errorArray.Add("SİGORTA ETTİREN UYRUK BİLGİSİ GİRİLMELİ!");
                            }
                            else
                            {
                                switch (req.SigEttiren.GK.Uyruk)
                                {
                                    case "TC":
                                    case "TRK":
                                    case "TR":
                                    case "TÜRKİYE":
                                    case "9980":
                                        if (!req.SigEttiren.GK.TCKN.IsInt64())
                                        {
                                            response.SonucKod = "0";
                                            errorArray.Add("SİGORTA ETTİREN TC BOŞ GEÇİLEMEZ!");
                                        }
                                        else
                                        {
                                            if (((long?)long.Parse(req.SigEttiren.GK.TCKN)).IsIdentityNoLength() == null)
                                            {
                                                response.SonucKod = "0";
                                                errorArray.Add("SİGORTA ETTİREN TCNO 11 HANE OLMALI!");
                                            }
                                            else
                                            {
                                                if (req.SigEttiren.GK.TCKN.Substring(0, 1) == "9")
                                                {
                                                    response.SonucKod = "0";
                                                    errorArray.Add("SİGORTA ETTİREN UYRUK TC, TCNO YABANCI. LUTFEN KONTROL EDİN!");
                                                }
                                                else
                                                {
                                                    if (!TcNoCheck(req.SigEttiren.GK.TCKN.ToString()))
                                                    {
                                                        response.SonucKod = "0";
                                                        errorArray.Add("SİGORTA ETTİREN GEÇERSİZ TCNO!");
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    case "YBN":
                                        break;
                                    //case "YABANCI":
                                    default:
                                        var checkNationalty = new GenericRepository<Country>().FindBy("A2_CODE=:code", parameters: new { code = new Dapper.DbString { Value = req.SigEttiren.GK.Uyruk.ToUpper().Trim(), Length = 3 } }).FirstOrDefault();
                                        if (checkNationalty == null)
                                        {
                                            response.SonucKod = "0";
                                            errorArray.Add("SİGORTA ETTİREN UYRUK BİLGİSİ HATALI!");
                                        }
                                        else
                                        {
                                            if (req.SigEttiren.GK.TCKN.IsInt64())
                                            {
                                                if (((long?)long.Parse(req.SigEttiren.GK.TCKN)).IsIdentityNoLength() == null)
                                                {
                                                    response.SonucKod = "0";
                                                    errorArray.Add("SİGORTA ETTİREN TCNO 11 HANE OLMALI!");
                                                }
                                                else
                                                {
                                                    if (req.SigEttiren.GK.TCKN.Substring(0, 1) == "9")
                                                    {
                                                        errorArray.Add("SİGORTA ETTİREN YABANCI TCNO GİRMEDİNİZ. LUTFEN KONTROL EDİN!");
                                                        response.SonucKod = "0";
                                                    }
                                                }
                                            }
                                            if (req.SigEttiren.GK.DogumYeri.IsNull())
                                            {
                                                response.SonucKod = "0";
                                                errorArray.Add("SİGORTA ETTİREN YABANCI UYRUKLULAR İÇİN DOĞUM YERİ GİRİLMESİ GEREKMEKTEDİR.");
                                            }
                                            if (req.SigEttiren.GK.BabaAd.IsNull())
                                            {
                                                response.SonucKod = "0";
                                                errorArray.Add("SİGORTA ETTİREN YABANCI UYRUKLULAR İÇİN BABA ADI GİRİLMESİ GEREKMEKTEDİR.");
                                            }
                                        }
                                        break;
                                }
                            }
                            if (req.SigEttiren.GK.Ad.IsNull() || req.SigEttiren.GK.Soyad.IsNull())
                            {
                                response.SonucKod = "0";
                                errorArray.Add("SİGORTA ETTİREN AD - SOYAD BİLGİSİ GİRİLMESİ GEREKMEKTEDİR");
                            }

                            if (req.SigEttiren.GK.Cinsiyet.IsNull())
                            {
                                response.SonucKod = "0";
                                errorArray.Add("SİGORTA ETTİREN CİNSİYET BİLGİSİ GİRİLMESİ GEREKMEKTEDİR");
                            }
                            else
                            {
                                if (req.SigEttiren.GK.Cinsiyet.ToUpper() != "K" && req.SigEttiren.GK.Cinsiyet.ToUpper() != "E" && req.SigEttiren.GK.Cinsiyet.ToUpper() != "0" && req.SigEttiren.GK.Cinsiyet.ToUpper() != "1")
                                {
                                    response.SonucKod = "0";
                                    errorArray.Add("SİGORTA ETTİREN CİNSİYET BİLGİSİ HATALI (E,K) yada (0,1)");
                                }
                            }

                            if (!req.SigEttiren.GK.DogumTarihi.ToString().IsDateTime())
                            {
                                response.SonucKod = "0";
                                errorArray.Add("SİGORTA ETTİREN DOĞUM TARİHİ BİLGİSİ GİRİLMESİ GEREKMEKTEDİR");
                            }

                            if (req.SigEttiren.GK.PasaportNo == "0")
                            {
                                req.SigEttiren.GK.PasaportNo = null;
                            }

                            if (req.SigEttiren.GK.VKN == "0")
                            {
                                req.SigEttiren.GK.VKN = null;
                            }

                            if (req.SigEttiren.GK.VKN.IsInt64())
                            {
                                if (((long?)long.Parse(req.SigEttiren.GK.VKN)).IsTaxNumberLength() == null)
                                {
                                    response.SonucKod = "0";
                                    errorArray.Add("SİGORTA ETTİREN VKN 10 HANE OLMALI!");
                                }
                            }
                        }
                        break;
                    case false: //Tüzel
                        if (req.SigEttiren.TK == null)
                        {
                            response.SonucKod = "0";
                            errorArray.Add("SİGORTA ETTİREN, TÜZEL KİŞİ İÇİN TK ALANI BOŞ GEÇİLEMEZ!");
                        }
                        else
                        {
                            if (req.SigEttiren.TK.SirketAd.IsNull())
                            {
                                response.SonucKod = "0";
                                errorArray.Add("SİGORTA ETTİREN ŞİRKET ADI BİLGİSİ GİRİLMESİ GEREKMEKTEDİR");
                            }

                            if (req.SigEttiren.TK.VKN.IsInt64())
                            {
                                if (((long?)long.Parse(req.SigEttiren.TK.VKN)).IsTaxNumberLength() == null)
                                {
                                    response.SonucKod = "0";
                                    errorArray.Add("SİGORTA ETTİREN VKN 10 HANE OLMALI!");
                                }
                            }

                            else
                            {
                                response.SonucKod = "0";
                                errorArray.Add("SİGORTA ETTİREN VKN BİLGİSİ GİRİLMESİ GEREKMEKTEDİR");
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            List<string> groupPolicyIndividual = new List<string>();

            if (req.SigortaliListesi == null || req.SigortaliListesi.Count <= 0)
            {
                response.SonucKod = "0";
                errorArray.Add("EN AZ BİR SİGORTALI BİLGİSİ GİRİLMESİ GEREKMEKTEDİR");
            }
            else
            {
                List<string> insuredCompanyNo = new List<string>();
                foreach (var sigortali in req.SigortaliListesi)
                {
                    if (sigortali.SigortaliNo.IsNull())
                    {
                        response.SonucKod = "0";
                        errorArray.Add("SİGORTALI NO GİRİLMESİ GEREKMEKTEDİR");
                    }
                    if (insuredCompanyNo.Contains(sigortali.SigortaliNo))
                    {
                        response.SonucKod = "0";
                        errorArray.Add("AYNI SİGORTALI NO İLE BİRDEN ÇOK SİGORTALI VAR. SİGORTALI NO TEKİL OLMALI.");
                    }
                    else
                    {
                        insuredCompanyNo.Add(sigortali.SigortaliNo);
                    }

                    if (!sigortali.PaketNo.IsNumeric())
                    {
                        response.SonucKod = "0";
                        errorArray.Add("SİGORTALI PAKET NO GİRİLMESİ GEREKMEKTEDİR");
                    }
                    //if (sigortali.IlkKayitTarihi == null)
                    //{
                    //    response.SonucAciklama = "İlk Kayıt Tarihi Boş Geçilemez.";
                    //    response.SonucKod = "0";
                    //    errorArray.Add(response.SonucAciklama);
                    //}
                    //if (sigortali.IlkSigortalanmaTarihi == null)
                    //{
                    //    response.SonucAciklama = "İlk Sigortalanma Tarihi Boş Geçilemez.";
                    //    response.SonucKod = "0";
                    //    errorArray.Add(response.SonucAciklama);
                    //}

                    if (sigortali.BireyTip.IsNull())
                    {
                        sigortali.BireyTip = "F";
                    }
                    else
                    {
                        List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.IndividualType>();
                        bool findHim = false;
                        foreach (EnumValue enVal in enumValuess)
                        {
                            if (enVal.Text == sigortali.BireyTip)
                            { findHim = true; break; }
                        }
                        if (!findHim)
                        {
                            if (sigortali.BireyTip == "K") { sigortali.BireyTip = "F"; }
                            else if (sigortali.BireyTip == "S") { sigortali.BireyTip = "E"; }
                            else
                            {
                                response.SonucKod = "0";
                                errorArray.Add("GEÇERSİZ BİREY TİPİ");
                            }
                        }
                    }
                    if (!sigortali.PrimTL.ToString().IsNumeric())
                    {
                        response.SonucKod = "0";
                        errorArray.Add("SİGORTALI PRİM TL GİRİLMESİ GEREKMEKTEDİR");
                    }
                    if (sigortali.KisiBilgi == null)
                    {
                        response.SonucKod = "0";
                        errorArray.Add("SİGORTALI KİŞİ BİLGİSİ GİRİLMESİ GEREKMEKTEDİR");
                    }
                    else
                    {
                        if (sigortali.KisiBilgi.Uyruk.IsNull())
                        {
                            response.SonucKod = "0";
                            errorArray.Add("SİGORTALI UYRUK BİLGİSİ GİRİLMELİ!");
                        }
                        else
                        {
                            switch (sigortali.KisiBilgi.Uyruk)
                            {
                                case "TC":
                                case "TRK":
                                case "TR":
                                case "TÜRKİYE":
                                case "9980":
                                    if (!sigortali.KisiBilgi.TCKN.IsInt64())
                                    {
                                        response.SonucKod = "0";
                                        errorArray.Add("SİGORTALI TC BOŞ GEÇİLEMEZ!");
                                    }
                                    else
                                    {
                                        if (((long?)long.Parse(sigortali.KisiBilgi.TCKN)).IsIdentityNoLength() == null)
                                        {
                                            response.SonucKod = "0";
                                            errorArray.Add("SİGORTALI TCNO 11 HANE OLMALI!");
                                        }
                                        else
                                        {
                                            if (sigortali.KisiBilgi.TCKN.Substring(0, 1) == "9")
                                            {
                                                response.SonucKod = "0";
                                                errorArray.Add("SİGORTALI UYRUK TC, TCNO YABANCI. LUTFEN KONTROL EDİN!");
                                            }
                                            else
                                            {
                                                if (!TcNoCheck(sigortali.KisiBilgi.TCKN.ToString()))
                                                {
                                                    response.SonucKod = "0";
                                                    errorArray.Add("SİGORTALI GEÇERSİZ TCNO!");
                                                }
                                            }
                                        }
                                    }
                                    break;
                                case "YBN":
                                    break;
                                default:
                                    var checkNationalty = new GenericRepository<Country>().FindBy("A2_CODE=:code", parameters: new { code = new Dapper.DbString { Value = sigortali.KisiBilgi.Uyruk.ToUpper().Trim(), Length = 3 } }).FirstOrDefault();
                                    if (checkNationalty == null)
                                    {
                                        response.SonucKod = "0";
                                        errorArray.Add("SİGORTALI UYRUK BİLGİSİ HATALI!");
                                    }
                                    else
                                    {
                                        if (sigortali.KisiBilgi.TCKN.IsInt64())
                                        {
                                            if (((long?)long.Parse(sigortali.KisiBilgi.TCKN)).IsIdentityNoLength() == null)
                                            {
                                                response.SonucKod = "0";
                                                errorArray.Add("SİGORTALI TCNO 11 HANE OLMALI!");
                                            }
                                            else
                                            {
                                                if (sigortali.KisiBilgi.TCKN.ToString().Substring(0, 1) == "9")
                                                {
                                                    response.SonucKod = "0";
                                                    errorArray.Add("SİGORTALI YABANCI TCNO GİRMEDİNİZ. LUTFEN KONTROL EDİN!");
                                                }
                                                else
                                                {
                                                    if (!TcNoCheck(sigortali.KisiBilgi.TCKN.ToString()))
                                                    {
                                                        response.SonucKod = "0";
                                                        errorArray.Add("SİGORTALI GEÇERSİZ TCNO!");
                                                    }
                                                }
                                            }
                                        }
                                        if (sigortali.KisiBilgi.DogumYeri.IsNull())
                                        {
                                            response.SonucKod = "0";
                                            errorArray.Add("SİGORTALI YABANCI UYRUKLULAR İÇİN DOĞUM YERİ GİRİLMESİ GEREKMEKTEDİR.");
                                        }
                                        if (sigortali.KisiBilgi.BabaAd.IsNull())
                                        {
                                            response.SonucKod = "0";
                                            errorArray.Add("SİGORTALI YABANCI UYRUKLULAR İÇİN BABA ADI GİRİLMESİ GEREKMEKTEDİR.");
                                        }
                                    }
                                    break;
                            }
                        }
                        if (sigortali.KisiBilgi.Ad.IsNull() || sigortali.KisiBilgi.Soyad.IsNull())
                        {
                            response.SonucKod = "0";
                            errorArray.Add("SİGORTALI AD - SOYAD BİLGİSİ GİRİLMESİ GEREKMEKTEDİR");
                        }
                        if (sigortali.KisiBilgi.Cinsiyet.IsNull())
                        {
                            response.SonucKod = "0";
                            errorArray.Add("SİGORTALI CİNSİYET BİLGİSİ GİRİLMESİ GEREKMEKTEDİR");
                        }
                        else
                        {
                            if (sigortali.KisiBilgi.Cinsiyet.ToUpper() != "K" && sigortali.KisiBilgi.Cinsiyet.ToUpper() != "E" && sigortali.KisiBilgi.Cinsiyet.ToUpper() != "0" && sigortali.KisiBilgi.Cinsiyet.ToUpper() != "1")
                            {
                                response.SonucKod = "0";
                                errorArray.Add("SİGORTALI CİNSİYET BİLGİSİ HATALI (E,K) yada (0,1)");
                            }
                        }
                        if (!sigortali.KisiBilgi.DogumTarihi.ToString().IsDateTime())
                        {
                            response.SonucKod = "0";
                            errorArray.Add("SİGORTALI DOĞUM TARİHİ BİLGİSİ GİRİLMESİ GEREKMEKTEDİR");
                        }
                        //zarfMutabakat.OdemeTarihi = payroll.PAYMENT_DATE != null ? ((DateTime)payroll.PAYMENT_DATE).ToString("yyyy-MM-dd") : null;

                        var birthDate = sigortali.KisiBilgi.DogumTarihi != null ? ((DateTime)sigortali.KisiBilgi.DogumTarihi).ToString("yyyyMMdd") : null;
                        var thisDay = DateTime.Now;
                        var fark = thisDay.Year - sigortali.KisiBilgi.DogumTarihi.Value.Year;
                        if (fark > 90)
                        {
                            response.SonucKod = "0";
                            errorArray.Add("SİGORTALI DOĞUM TARİHİ BİLGİSİ 90 YAŞ ÜZERİNDE OLAMAZ.");
                        }
                        if (sigortali.KisiBilgi.PasaportNo == "0")
                        {
                            sigortali.KisiBilgi.PasaportNo = null;
                        }

                        if (sigortali.KisiBilgi.VKN == "0")
                        {
                            sigortali.KisiBilgi.VKN = null;
                        }

                        if (sigortali.KisiBilgi.VKN.IsInt64())
                        {
                            if (((long?)long.Parse(sigortali.KisiBilgi.VKN)).IsTaxNumberLength() == null)
                            {
                                response.SonucKod = "0";
                                errorArray.Add("SİGORTALI VKN 10 HANE OLMALI!");
                            }
                        }
                    }
                }
            }
            #endregion
            if (response.SonucKod == "0")
            {
                response.ValidationErrors = errorArray.ToArray();
                response.SonucAciklama = "HATA - ValidationErrors Listesini Kontrol Ediniz";
                return response;
            }

            #region Data Control

            Agency agency = new GenericRepository<Agency>().FindBy($"COMPANY_ID =:CompanyId AND NO =:agencyNo", parameters: new { CompanyId = req.PoliceBilgi.SirketKod, agencyNo = new Dapper.DbString { Value = req.AcenteKod, Length = 20 } }).FirstOrDefault();

            if (agency == null)
            {
                response.SonucKod = "0";
                errorArray.Add("ACENTE BİLGİSİ BULUNAMADI.");
            }

            Policy policy = new GenericRepository<Policy>().FindBy($"COMPANY_ID={req.PoliceBilgi.SirketKod} AND NO={req.PoliceBilgi.PoliceNo} AND RENEWAL_NO={req.PoliceBilgi.YenilemeNo}", orderby: "").FirstOrDefault();

            if (policy != null)
            {
                log.PolicyId = policy.Id;
                response.SonucKod = "0";
                errorArray.Add("POLİÇE BİLGİSİ DAHA ÖNCE AKTARILMIŞ. TEKRAR AKTARILAMAZ.");
            }

            string packageNoList = string.Join("','", req.SigortaliListesi.Select(x => x.PaketNo));
            List<V_Package> packageList = new GenericRepository<V_Package>().FindBy($"PACKAGE_NO IN ('{packageNoList}') AND COMPANY_ID={req.PoliceBilgi.SirketKod}", orderby: "");
            if (packageList != null && packageList.Count > 0)
            {
                bool isFound = false, isContinue = true;
                foreach (var sigortali in req.SigortaliListesi)
                {
                    foreach (var package in packageList)
                    {
                        if (sigortali.PaketNo == package.PACKAGE_NO)
                        {
                            isFound = true;
                        }
                    }
                    if (!isFound)
                    {
                        response.SonucKod = "0";
                        errorArray.Add("SİGORTALI PAKET BİLGİSİ BULUNAMADI.");
                        isContinue = false;
                        break;
                    }
                    isFound = false;
                }
                if (isContinue)
                {
                    if (packageList[0].SUBPRODUCT_ID == null)
                    {
                        response.SonucKod = "0";
                        errorArray.Add($"PAKETİN ALT ÜRÜN BİLGİSİ BULUNAMADI. (PaketNo={packageList[0].PACKAGE_NO})");
                        isContinue = false;
                    }
                    if (isContinue)
                    {
                        long subProductId = (long)packageList[0].SUBPRODUCT_ID;
                        foreach (var item in packageList)
                        {
                            if (item.SUBPRODUCT_ID == null)
                            {
                                response.SonucKod = "0";
                                errorArray.Add($"PAKETİN ALT ÜRÜN BİLGİSİ BULUNAMADI. (PaketNo={item.PACKAGE_NO})");
                                isContinue = false;
                                break;
                            }
                            else if (item.SUBPRODUCT_ID != subProductId)
                            {
                                response.SonucKod = "0";
                                errorArray.Add($"SİGORTALI PAKET NO'LARI AYNI ALT ÜRÜN DEĞİL! (PaketNo={item.PACKAGE_NO})");
                                isContinue = false;
                                break;
                            }
                        }
                        response.Package = packageList[0];
                    }
                }
            }
            else
            {
                response.SonucKod = "0";
                errorArray.Add("SİGORTALI PAKET BİLGİSİ BULUNAMADI.");
            }

            #endregion
            if (response.SonucKod == "0")
            {
                response.SonucAciklama = "HATA - ValidationErrors Listesini Kontrol Ediniz";
                response.ValidationErrors = errorArray.ToArray();
            }
            else
            {
                response.SonucKod = "1";
                response.SonucAciklama = "OK";
            }

            return response;
        }

        private PoliceAktarRes PoliceAktarOpr(PoliceAktarReq req, string CorrelationId, IntegrationLog log, V_Package package)
        {
            PoliceAktarRes result = new PoliceAktarRes();

            List<string> errorArray = new List<string>();

            PolicyDto policyDto = new PolicyDto();
            policyDto.CorrelationId = CorrelationId;
            IService<PolicyDto> policyService = new PolicyService();

            policyDto.isPolicyTransfer = true;
            policyDto.policyIntegrationType = PolicyIntegrationType.WS;

            #region Insurer
            //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER START_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

            policyDto.isInsurerChange = true;
            policyDto.InsurerContactId = 0;
            switch (req.SigEttiren.GercekTuzel)
            {
                case true: //Gerçek
                    policyDto.InsurerTcNo = req.SigEttiren.GK.TCKN.IsInt64() ? (long?)long.Parse(req.SigEttiren.GK.TCKN) : null;
                    policyDto.InsurerVkn = req.SigEttiren.GK.VKN.IsInt64() ? (long?)long.Parse(req.SigEttiren.GK.VKN) : null;
                    policyDto.InsurerTitle = req.SigEttiren.GK.Ad + " " + req.SigEttiren.GK.Soyad;
                    policyDto.InsurerType = ((int)ContactType.GERCEK).ToString();

                    policyDto.InsurerName = req.SigEttiren.GK.Ad;
                    policyDto.InsurerLastName = req.SigEttiren.GK.Soyad;
                    policyDto.InsurerGender = req.SigEttiren.GK.Cinsiyet;
                    policyDto.InsurerBirthDate = req.SigEttiren.GK.DogumTarihi;
                    policyDto.InsurerBirthPlace = req.SigEttiren.GK.DogumYeri;
                    policyDto.InsurerNameOfFather = req.SigEttiren.GK.BabaAd;
                    policyDto.InsurerNationality = req.SigEttiren.GK.Uyruk;
                    policyDto.InsurerPassport = req.SigEttiren.GK.PasaportNo;

                    break;
                case false: //Tüzel
                    policyDto.InsurerType = ((int)ContactType.TUZEL).ToString();
                    policyDto.InsurerCorporateName = req.SigEttiren.TK.SirketAd;
                    policyDto.InsurerVkn = req.SigEttiren.TK.VKN.IsInt64() ? (long?)long.Parse(req.SigEttiren.TK.VKN) : null;
                    break;
                default:
                    break;
            }

            if (req.SigEttiren.Adres != null)
            {
                policyDto.InsurerAddress = "";
                if (req.SigEttiren.Adres.IlKod.IsInt64())
                {
                    req.SigEttiren.Adres.IlKod = req.SigEttiren.Adres.IlKod.Trim().TrimStart('0');
                    req.SigEttiren.Adres.IlKod = (req.SigEttiren.Adres.IlKod.Length == 1 ? "00" : "") + req.SigEttiren.Adres.IlKod;
                    req.SigEttiren.Adres.IlKod = (req.SigEttiren.Adres.IlKod.Length == 2 ? "0" : "") + req.SigEttiren.Adres.IlKod;

                    var city = CityHelper.GetCitybyCode(req.SigEttiren.Adres.IlKod);// new GenericRepository<City>().FindBy($"CODE='{req.SigEttiren.Adres.IlKod}'").FirstOrDefault();
                    policyDto.InsurerCityId = city != null ? (long?)city.Id : null;

                    County county = new County();
                    if (req.SigEttiren.Adres.IlceKod.IsInt64())
                    {
                        county = CountyHelper.GetCountybyCode(city.Id, int.Parse(req.SigEttiren.Adres.IlceKod)); // new GenericRepository<County>().FindBy($"CODE={req.SigEttiren.Adres.IlceKod}").FirstOrDefault();
                        policyDto.InsurerCountyId = county != null ? (long?)county.Id : null;
                    }
                }

                if (!req.SigEttiren.Adres.Mahalle.IsNull())
                {
                    policyDto.InsurerAddress += " Mah=" + req.SigEttiren.Adres.Mahalle;
                }
                if (!req.SigEttiren.Adres.Sokak.IsNull())
                {
                    policyDto.InsurerAddress += " Sok=" + req.SigEttiren.Adres.Sokak;
                }
                if (!req.SigEttiren.Adres.BinaAd.IsNull())
                {
                    policyDto.InsurerAddress += " Bina=" + req.SigEttiren.Adres.Mahalle;
                }
                if (!req.SigEttiren.Adres.BinaNo.IsNull())
                {
                    policyDto.InsurerAddress += " BinaNo=" + req.SigEttiren.Adres.BinaNo;
                }
                if (!req.SigEttiren.Adres.DaireNo.IsNull())
                {
                    policyDto.InsurerAddress += " Daire=" + req.SigEttiren.Adres.DaireNo;
                }
                if (!req.SigEttiren.Adres.AdresTarif.IsNull())
                {
                    policyDto.InsurerAddress += req.SigEttiren.Adres.AdresTarif;
                }

                policyDto.InsurerZipCode = req.SigEttiren.Adres.PostaKod;
            }
            #endregion
            //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER SAVE GO SERVICE_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

            var response = policyService.ServiceInsert(policyDto, this.Token);
            //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER Save End SERVICE_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

            if (!response.IsSuccess)
            {
                result.SonucAciklama = response.Message;
                result.SonucKod = "0";
                errorArray.Add(result.SonucAciklama);
                if (!string.IsNullOrEmpty(response.SagmerCode) && !string.IsNullOrEmpty(response.SagmerMessage))
                    errorArray.Add(response.SagmerCode + " " + response.SagmerMessage);
                result.ValidationErrors = errorArray.ToArray();
                return result;
            }
            policyDto.InsurerId = response.Id;

            #region Policy
            policyDto.isInsurerChange = false;
            policyDto.isPolicyChange = true;
            policyDto.PolicyId = 0;

            policyDto.SubProductId = package.SUBPRODUCT_ID;
            policyDto.AgencyId = req.AcenteKod;
            policyDto.CompanyId = req.PoliceBilgi.SirketKod;
            policyDto.EndDate = req.PolBitisTarih;
            policyDto.PolicyNo = req.PoliceBilgi.PoliceNo;
            policyDto.PolicyType = req.GrupFerdi;
            policyDto.RenewalNo = req.PoliceBilgi.YenilemeNo;
            policyDto.PreviousPolicyNo = req.EskiPoliceNo;
            policyDto.PreviousRenewalNo = (req.PoliceBilgi.YenilemeNo - 1);
            policyDto.EndDate = req.PolBitisTarih;
            policyDto.PolicyStatus = ((int)PolicyStatus.TEKLIF).ToString();
            policyDto.PolicyInstallmentCount = req.VadeAdet != null && req.VadeAdet > 0 ? req.VadeAdet : 1;
            policyDto.PolicyPaymentType = req.OdemeSekli;
            #endregion
            //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation POLICY Save START SERVICE_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

            response = policyService.ServiceInsert(policyDto, this.Token);
            //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation POLICY Save END SERVICE_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

            //HttpContext.Current.Response.Flush();
            //HttpContext.Current.Response.Clear();
            if (!response.IsSuccess)
            {
                result.SonucAciklama = response.Message;
                result.SonucKod = "0";
                errorArray.Add(result.SonucAciklama);
                result.ValidationErrors = errorArray.ToArray();

                RollBackProxySrv(insurerId: (long)policyDto.InsurerId);

                return result;
            }
            policyDto.PolicyId = response.Id;
            log.PolicyId = response.Id;

            #region Endorsement
            policyDto.isEndorsementChange = true;
            policyDto.isPolicyChange = false;
            policyDto.IsSagmerPolicySend = false;

            policyDto.EndorsementType = ((int)EndorsementType.BASLANGIC).ToString();
            policyDto.EndorsementNo = 0;
            policyDto.EndorsementStartDate = req.PolBaslamaTarih;
            policyDto.DateOfIssue = req.PolTanzimTarih;
            policyDto.EndorsementRegistrationDate = req.PolTanzimTarih;
            policyDto.EndorsementStatus = ((int)Status.PASIF).ToString();
            #endregion
            //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation ENDORSEMENT Save START SERVICE_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

            response = policyService.ServiceInsert(policyDto, this.Token);
            //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation ENDORSEMENT Save END SERVICE_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

            //HttpContext.Current.Response.Flush();
            //HttpContext.Current.Response.Clear();
            if (!response.IsSuccess)
            {
                result.SonucAciklama = response.Message;
                result.SonucKod = "0";
                errorArray.Add(result.SonucAciklama);
                result.ValidationErrors = errorArray.ToArray();

                RollBackProxySrv(policyId: policyDto.PolicyId, insurerId: (long)policyDto.InsurerId);

                return result;
            }
            policyDto.EndorsementId = response.Id;

            #region Insured Opr
            policyDto.isEndorsementChange = false;
            policyDto.isInsuredContactChange = true;


            List<Int64> insuredIdList = new List<long>();
            decimal insuredTotalPremium = 0;
            foreach (var sigortali in req.SigortaliListesi)
            {
                policyDto.isEndorsementChange = false;
                policyDto.isInsuredContactChange = true;
                policyDto.isInsuredChange = false;

                policyDto.InsuredAddressId = 0;
                policyDto.InsuredContactId = 0;
                policyDto.InsuredPersonId = 0;
                policyDto.InsuredPhoneId = 0;
                policyDto.InsuredMobilePhoneId = 0;
                policyDto.InsuredEmailId = 0;
                policyDto.InsuredId = 0;
                policyDto.InsuredNoParameterId = 0;


                #region Person
                policyDto.InsuredTcNo = sigortali.KisiBilgi.TCKN.IsInt64() ? (long?)long.Parse(sigortali.KisiBilgi.TCKN) : null;
                policyDto.InsuredName = sigortali.KisiBilgi.Ad;
                policyDto.InsuredGender = sigortali.KisiBilgi.Cinsiyet;
                policyDto.InsuredLastName = sigortali.KisiBilgi.Soyad;
                policyDto.InsuredBirthDate = sigortali.KisiBilgi.DogumTarihi;
                policyDto.InsuredBirthPlace = sigortali.KisiBilgi.DogumYeri;
                policyDto.InsuredNameOfFather = sigortali.KisiBilgi.BabaAd;
                policyDto.InsurerNationality = sigortali.KisiBilgi.Uyruk;
                policyDto.InsuredPassport = sigortali.KisiBilgi.PasaportNo;
                policyDto.InsuredFirstInsuredDate = sigortali.IlkSigortalanmaTarihi;
                policyDto.InsuredNo = sigortali.SigortaliNo;

                #endregion


                #region Address
                if (sigortali.Adres != null)
                {
                    policyDto.InsuredAddress = "";
                    if (!sigortali.Adres.IlKod.IsNull())
                    {
                        sigortali.Adres.IlKod = sigortali.Adres.IlKod.Trim().TrimStart('0');
                        sigortali.Adres.IlKod = (sigortali.Adres.IlKod.Length == 1 ? "00" : "") + sigortali.Adres.IlKod;
                        sigortali.Adres.IlKod = (sigortali.Adres.IlKod.Length == 2 ? "0" : "") + sigortali.Adres.IlKod;
                        var cityy = CityHelper.GetCitybyCode(sigortali.Adres.IlKod); //new GenericRepository<City>().FindBy($"CODE='{sigortali.Adres.IlKod}'").FirstOrDefault();
                        policyDto.InsuredCityId = cityy != null ? (long?)cityy.Id : null;
                        if (sigortali.Adres.IlceKod.IsInt())
                        {
                            var countyy = CountyHelper.GetCountybyCode(cityy.Id, int.Parse(sigortali.Adres.IlceKod));//new GenericRepository<County>().FindBy($"CODE={sigortali.Adres.IlceKod}").FirstOrDefault();
                            policyDto.InsuredCountyId = countyy != null ? (long?)countyy.Id : null;
                            if (countyy != null)
                            {
                                policyDto.InsuredCityId = countyy.CityId;
                            }
                        }
                    }

                    if (!sigortali.Adres.Mahalle.IsNull())
                    {
                        policyDto.InsuredAddress += " Mah=" + sigortali.Adres.Mahalle;
                    }
                    if (!sigortali.Adres.Sokak.IsNull())
                    {
                        policyDto.InsuredAddress += " Sok=" + sigortali.Adres.Sokak;
                    }
                    if (!sigortali.Adres.BinaAd.IsNull())
                    {
                        policyDto.InsuredAddress += " Bina=" + sigortali.Adres.Mahalle;
                    }
                    if (!sigortali.Adres.BinaNo.IsNull())
                    {
                        policyDto.InsuredAddress += " BinaNo=" + sigortali.Adres.BinaNo;
                    }
                    if (!sigortali.Adres.DaireNo.IsNull())
                    {
                        policyDto.InsuredAddress += " Daire=" + sigortali.Adres.DaireNo;
                    }
                    if (!sigortali.Adres.AdresTarif.IsNull())
                    {
                        policyDto.InsuredAddress += sigortali.Adres.AdresTarif;
                    }

                    policyDto.InsuredZipCode = sigortali.Adres.PostaKod;
                }
                #endregion

                if (sigortali.Iletisim != null)
                {
                    #region Phone
                    policyDto.InsuredTelNo = sigortali.Iletisim.TelefonNo;
                    policyDto.InsuredGsmNo = sigortali.Iletisim.CepTel;
                    #endregion

                    #region E-mail
                    policyDto.InsuredEmail = sigortali.Iletisim.Email;
                    #endregion
                }

                #region Bank
                if (sigortali.Banka != null && !sigortali.Banka.BankaKod.IsNull())
                {
                    var bank = new GenericRepository<Bank>().FindBy($"CODE='{sigortali.Banka.BankaKod}'").FirstOrDefault();
                    if (bank != null)
                    {
                        policyDto.InsuredBankAccountOwnerName = sigortali.Banka.HesapSahip;
                        policyDto.InsuredBankAccountNo = sigortali.Banka.HesapNo;
                        policyDto.InsuredBankIbanNo = sigortali.Banka.Iban;

                        policyDto.InsuredBankId = bank.Id;

                        var bankBranch = new GenericRepository<BankBranch>().FindBy($"CODE='{sigortali.Banka.SubeKod}'").FirstOrDefault();
                        policyDto.InsuredBankBranchId = bankBranch != null ? (long?)bankBranch.Id : null;
                    }
                }
                #endregion
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED_CONTACT Save SERVICE START_" + sigortali.SigortaliNo + "__" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                response = policyService.ServiceInsert(policyDto, this.Token);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED_CONTACT Save SERVICE END_" + sigortali.SigortaliNo + "__" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (!response.IsSuccess)
                {
                    result.SonucAciklama = response.Message;
                    result.SonucKod = "0";
                    errorArray.Add(result.SonucAciklama);
                    result.ValidationErrors = errorArray.ToArray();

                    RollBackProxySrv(policyId: policyDto.PolicyId, endorsementId: policyDto.EndorsementId, insurerId: (long)policyDto.InsurerId, insuredIdList: insuredIdList);
                    return result;
                }

                #region Insured
                policyDto.isInsuredContactChange = false;
                policyDto.isInsuredChange = true;

                policyDto.InsuredContactId = response.Id;
                policyDto.InsuredIndividualType = sigortali.BireyTip;
                policyDto.InsuredFamilyNo = sigortali.AileNo.IsNull() ? 1 : long.Parse(sigortali.AileNo);

                if (package.PACKAGE_NO != sigortali.PaketNo)
                    package = new GenericRepository<V_Package>().FindBy($"PACKAGE_NO='{sigortali.PaketNo}' AND COMPANY_ID={req.PoliceBilgi.SirketKod}", orderby: "PACKAGE_ID").FirstOrDefault();

                policyDto.InsuredPackageId = package.PACKAGE_ID;

                insuredTotalPremium += sigortali.PrimTL;
                policyDto.InsuredInitialPremium = sigortali.PrimTL;
                policyDto.InsuredEndorsementPremium = sigortali.PrimTL;
                policyDto.InsuredCompanyEntranceDate = sigortali.IlkKayitTarihi;
                policyDto.InsuredNo = sigortali.SigortaliNo;

                #endregion
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED Save SERVICE START_" + sigortali.SigortaliNo + "__" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                response = policyService.ServiceInsert(policyDto, this.Token);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED Save SERVICE END_" + sigortali.SigortaliNo + "__" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (!response.IsSuccess)
                {
                    result.SonucAciklama = response.Message;
                    result.SonucKod = "0";
                    errorArray.Add(result.SonucAciklama);
                    result.ValidationErrors = errorArray.ToArray();

                    RollBackProxySrv(policyId: policyDto.PolicyId, endorsementId: policyDto.EndorsementId, insurerId: (long)policyDto.InsurerId, insuredIdList: insuredIdList);

                    return result;
                }
                insuredIdList.Add(response.Id);
            }
            #endregion

            //HttpContext.Current.Response.Flush();
            //HttpContext.Current.Response.Clear();
            var endorsement = new GenericRepository<Endorsement>().FindById(policyDto.EndorsementId);
            //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation Endorsement FindById_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");
            if (endorsement != null)
            {
                endorsement.Premium = insuredTotalPremium;
                endorsement.Status = ((int)Status.AKTIF).ToString();
                var spResEndorsement = new GenericRepository<Endorsement>().Update(endorsement);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation Endorsement UPDATE_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");
                if (spResEndorsement.Code != "100")
                {
                    result.SonucAciklama = spResEndorsement.Message;
                    result.SonucKod = "0";
                    errorArray.Add(result.SonucAciklama);
                    result.ValidationErrors = errorArray.ToArray();

                    RollBackProxySrv(policyId: policyDto.PolicyId, endorsementId: policyDto.EndorsementId, insurerId: (long)policyDto.InsurerId, insuredIdList: insuredIdList);

                    return result;
                }
                if (endorsement.Premium != 0)
                {
                    var firstEndorsement = endorsement;

                    if (firstEndorsement != null)
                    {
                        List<Installment> listInstallment = new List<Installment>();
                        int month = 0;
                        for (int i = 0; i < policyDto.PolicyInstallmentCount; i++)
                        {
                            Installment installment = new Installment
                            {
                                DueDate = DateTime.Now.AddMonths(month),
                                Amount = (decimal)endorsement.Premium / (int)policyDto.PolicyInstallmentCount,
                                EndorsementId = endorsement.Id,
                                PolicyId = endorsement.PolicyId
                            };
                            listInstallment.Add(installment);
                            month++;
                        }
                        var spResIns = new GenericRepository<Installment>().InsertEntities(listInstallment);
                        //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation Installment INSERT_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");
                    }
                }
            }
            var policy = new GenericRepository<Policy>().FindById(policyDto.PolicyId);
            //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation Policy FindById_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");
            if (policy != null)
            {
                policy.LastEndorsementId = policyDto.EndorsementId;
                policy.Premium = insuredTotalPremium;
                policy.Status = ((int)PolicyStatus.TANZIMLI).ToString(); // TO-DO : Sagmer sonrası ara bir statu olabilir.

                var spResPolicy = new GenericRepository<Policy>().Update(policy);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation Policy UPDATE_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");
                if (spResPolicy.Code != "100")
                {
                    result.SonucAciklama = spResPolicy.Message;
                    result.SonucKod = "0";
                    errorArray.Add(result.SonucAciklama);
                    result.ValidationErrors = errorArray.ToArray();

                    RollBackProxySrv(policyId: policyDto.PolicyId, endorsementId: policyDto.EndorsementId, insurerId: (long)policyDto.InsurerId, insuredIdList: insuredIdList);

                    return result;
                }
            }

            //HttpContext.Current.Response.Flush();
            //HttpContext.Current.Response.Clear();
            //RollBackProxySrv(policyId: policyDto.PolicyId, endorsementId: policyDto.EndorsementId,  insuredIdList: insuredIdList);

            if (req.PoliceBilgi.SirketKod == 10)
            {
                bool isSgmOk = false;
                SgmServiceClient sgmServiceClient = new SgmServiceClient();
                SbmPolicyCreateReq sbmInsuredExitReq = new SbmPolicyCreateReq
                {
                    PolicyId = policyDto.PolicyId
                };
                var sbmResponse = sgmServiceClient.SbmPolicyCreate(sbmInsuredExitReq, SagmerServiceType.KONTROL);
                if (sbmResponse.ResultCode != 1)
                {
                    if (sbmResponse.FailedErrors.Count == 1)
                    {
                        foreach (var item in sbmResponse.FailedErrors)
                        {
                            if (item.code == "BRV-SGM-00001")
                            {
                                isSgmOk = true;
                            }
                        }
                    }

                    if (!isSgmOk)
                    {
                        RollBackProxySrv(policyId: policyDto.PolicyId, endorsementId: policyDto.EndorsementId, insuredIdList: insuredIdList, insurerId: (long)policyDto.InsurerId);

                        List<string> SbmErrorList = new List<string>();
                        foreach (var item in sbmResponse.FailedErrors)
                        {
                            SbmErrorList.Add(item.code + "  " + item.message);
                        }
                        result.SonucKod = "0";
                        result.SonucAciklama = "SAGMER HATA, ValidationErrors Kontrol Ediniz.";
                        result.ValidationErrors = SbmErrorList.ToArray();
                        return result;
                    }
                }
                else
                {
                    isSgmOk = true;
                }

                if (isSgmOk)
                {
                    sbmResponse = sgmServiceClient.SbmPolicyCreate(sbmInsuredExitReq, SagmerServiceType.CANLI);
                    if (sbmResponse.ResultCode != 1)
                    {
                        RollBackProxySrv(policyId: policyDto.PolicyId, endorsementId: policyDto.EndorsementId, insuredIdList: insuredIdList, insurerId: (long)policyDto.InsurerId);

                        List<string> SbmErrorList = new List<string>();
                        foreach (var item in sbmResponse.FailedErrors)
                        {
                            SbmErrorList.Add(item.code + "  " + item.message);
                        }
                        result.SonucKod = "0";
                        result.SonucAciklama = "SAGMER HATA, ValidationErrors Kontrol Ediniz.";
                        result.ValidationErrors = SbmErrorList.ToArray();
                        return result;
                    }
                }
            }
            result.SonucAciklama = "OK";
            result.SonucKod = "1";
            return result;
        }
        #endregion

        #region ZeyilSigortaliEkle
        public ZeyilSigortaliEkleRes ZeyilSigortaliEkle(string apiCode, ZeyilSigortaliEkleReq req)
        {
            ZeyilSigortaliEkleRes response = new ZeyilSigortaliEkleRes();
            List<string> errorArray = new List<string>();
            var currentMethod = MethodBase.GetCurrentMethod();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            #endregion

            try
            {
                #region Validation
                response = ZeyilSigortaliEkleValidation(req, apiCode, log);
                if (response.SonucKod != "1")
                {
                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                    return response;
                }
                #endregion

                #region Operation
                response = ZeyilSigortaliEkleOpr(req,log);
                if (response.SonucKod != "1")
                {
                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                    return response;
                }
                #endregion

                #region OK
                response.SonucAciklama = "OK";
                response.SonucKod = "1";
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.SonucKod = "0";
                response.SonucAciklama = "HATA - ValidationErrors Listesini Kontrol Ediniz";
                response.ValidationErrors = new string[] { ex.Message };

                CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                #endregion
            }
            return response;
        }

        private ZeyilSigortaliEkleRes ZeyilSigortaliEkleValidation(ZeyilSigortaliEkleReq req, string apiCode, IntegrationLog log)
        {
            ZeyilSigortaliEkleRes response = new ZeyilSigortaliEkleRes();
            List<string> errorArray = new List<string>();

            #region Incoming Validation
            if (req.PoliceNo == null)
            {
                response.SonucKod = "0";
                errorArray.Add("POLICE BİLGİ ALANI GİRİLMESİ GEREKMEKTEDİR");
            }
            else
            {
                if (!req.PoliceNo.SirketKod.ToString().IsNumeric())
                {
                    response.SonucKod = "0";
                    errorArray.Add("ŞİRKET KOD GİRİLMESİ GEREKMEKTEDİR");
                }
                if (!req.PoliceNo.PoliceNo.ToString().IsNumeric() || !req.PoliceNo.YenilemeNo.ToString().IsNumeric())
                {
                    response.SonucKod = "0";
                    errorArray.Add("POLİÇE NO VE YENİLEME NO GİRİLMESİ GEREKMEKTEDİR");
                }
            }
            if (response.SonucKod == "0")
            {
                response.ValidationErrors = errorArray.ToArray();
                response.SonucAciklama = "HATA";
                return response;
            }
            #endregion

            #region Auth
            if (ApiCodeIsValid(apiCode, req.PoliceNo.SirketKod) <= 0)
            {
                response.SonucAciklama = "HATA";
                response.SonucKod = "0";
                errorArray.Add("Giriş Başarısız! APİCODE'u kontrol ediniz");
                response.ValidationErrors = errorArray.ToArray();
                return response;
            }
            log.COMPANY_ID = req.PoliceNo.SirketKod;
            #endregion

            #region Zorunlu Alan

            if (req.ZeylNo.IsInt() && req.PoliceNo.SirketKod != 3)
            {
                var endorsementList = new GenericRepository<V_PolicyEndorsement>().FindBy(conditions: $"POLICY_NUMBER='{req.PoliceNo.PoliceNo}'", orderby: "ENDORSEMENT_NO desc").FirstOrDefault();
                int nextEndorsementNo = Convert.ToInt32(endorsementList.ENDORSEMENT_NO) + 1;
                if (Convert.ToInt32(req.ZeylNo) != nextEndorsementNo)
                {
                    response.SonucAciklama = "Zeyl No Son Zeyl Numarası ile ardışık ilerlemesi gerekir. Son Zeyl NO : " + endorsementList.ENDORSEMENT_NO;
                    response.SonucKod = "0";
                    errorArray.Add(response.SonucAciklama);
                }
            }
            if (req.PoliceNo == null)
            {
                response.SonucAciklama = "POLICE NO ALANININ GİRİLMESİ GEREKMEKTEDİR";
                response.SonucKod = "0";
                errorArray.Add(response.SonucAciklama);
            }
            else
            {
                if (!req.PoliceNo.SirketKod.ToString().IsNumeric())
                {
                    response.SonucAciklama = "ŞİRKET KOD GİRİLMESİ GEREKMEKTEDİR";
                    response.SonucKod = "0";
                    errorArray.Add(response.SonucAciklama);
                }
                if (!req.PoliceNo.PoliceNo.ToString().IsNumeric() || !req.PoliceNo.YenilemeNo.ToString().IsNumeric())
                {
                    response.SonucAciklama = "POLİÇE NO VE YENİLEME NO GİRİLMESİ GEREKMEKTEDİR";
                    response.SonucKod = "0";
                    errorArray.Add(response.SonucAciklama);
                }
            }
            if (!req.ZeylBaslamaTarih.ToString().IsDateTime() || !req.ZeylTanzimTarih.ToString().IsDateTime())
            {
                response.SonucAciklama = "ZEYL TARİH BİLGİLERİ GİRİLMESİ GEREKMEKTEDİR";
                response.SonucKod = "0";
                errorArray.Add(response.SonucAciklama);
            }
            if (req.SigortaliListesi == null || req.SigortaliListesi.Count <= 0)
            {
                response.SonucAciklama = "EN AZ BİR SİGORTALI GİRİLMESİ GEREKMEKTEDİR";
                response.SonucKod = "0";
                errorArray.Add(response.SonucAciklama);
            }
            else
            {
                foreach (var sigortali in req.SigortaliListesi)
                {
                    if (sigortali.SigortaliNo.IsNull())
                    {
                        response.SonucAciklama = "SİGORTALI NO GİRİLMESİ GEREKMEKTEDİR";
                        response.SonucKod = "0";
                        errorArray.Add(response.SonucAciklama);
                    }

                    if (!sigortali.PaketNo.IsNumeric())
                    {
                        response.SonucAciklama = "PAKET NO GİRİLMESİ GEREKMEKTEDİR";
                        response.SonucKod = "0";
                        errorArray.Add(response.SonucAciklama);
                    }

                    if (sigortali.IlkKayitTarihi == null)
                    {
                        response.SonucAciklama = "İlk Kayıt Tarihi Boş Geçilemez.";
                        response.SonucKod = "0";
                        errorArray.Add(response.SonucAciklama);
                    }
                    if (sigortali.IlkSigortalanmaTarihi == null)
                    {
                        response.SonucAciklama = "İlk Sigortalanma Tarihi Boş Geçilemez.";
                        response.SonucKod = "0";
                        errorArray.Add(response.SonucAciklama);
                    }

                    if (sigortali.BireyTip.IsNull())
                    {
                        sigortali.BireyTip = "F";
                    }
                    else
                    {
                        List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.IndividualType>();
                        bool findHim = false;
                        foreach (EnumValue enVal in enumValuess)
                        {
                            if (enVal.Text == sigortali.BireyTip)
                            { findHim = true; break; }
                        }
                        if (!findHim)
                        {
                            if (sigortali.BireyTip == "K") { sigortali.BireyTip = "F"; }
                            else if (sigortali.BireyTip == "S") { sigortali.BireyTip = "E"; }
                            else
                            {
                                response.SonucKod = "0";
                                response.SonucAciklama = "GEÇERSİZ BİREY TİPİ";
                                errorArray.Add(response.SonucAciklama);
                            }
                        }
                    }

                    if (!sigortali.PrimTL.ToString().IsNumeric())
                    {
                        response.SonucAciklama = "PRİM TL GİRİLMESİ GEREKMEKTEDİR";
                        response.SonucKod = "0";
                        errorArray.Add(response.SonucAciklama);
                    }

                    if (sigortali.KisiBilgi == null)
                    {
                        response.SonucAciklama = "SIGORTALI İÇİN KİŞİ BİLGİ ALANININ DOLDURULMASI GEREKMEKTEDİR";
                        response.SonucKod = "0";
                        errorArray.Add(response.SonucAciklama);
                    }
                    else
                    {
                        if (sigortali.KisiBilgi.Uyruk.IsNull())
                        {
                            response.SonucAciklama = "UYRUK BİLGİSİ GİRİLMELİ!";
                            response.SonucKod = "0";
                            errorArray.Add(response.SonucAciklama);
                        }
                        else
                        {
                            switch (sigortali.KisiBilgi.Uyruk)
                            {
                                case "TC":
                                case "TRK":
                                case "TR":
                                case "TÜRKİYE":
                                case "9980":
                                    if (!sigortali.KisiBilgi.TCKN.IsInt64())
                                    {
                                        response.SonucAciklama = "TC BOŞ GEÇİLEMEZ!";
                                        response.SonucKod = "0";
                                        errorArray.Add(response.SonucAciklama);
                                        response.ValidationErrors = errorArray.ToArray();
                                    }
                                    else
                                    {
                                        if (((long?)long.Parse(sigortali.KisiBilgi.TCKN)).IsIdentityNoLength() == null)
                                        {
                                            response.SonucAciklama = "TCNO 11 HANE OLMALI!";
                                            response.SonucKod = "0";
                                            errorArray.Add(response.SonucAciklama);
                                            response.ValidationErrors = errorArray.ToArray();
                                        }
                                        else
                                        {
                                            if (sigortali.KisiBilgi.TCKN.Substring(0, 1) == "9")
                                            {
                                                response.SonucAciklama = "UYRUK TC, TCNO YABANCI. LUTFEN KONTROL EDİN!";
                                                response.SonucKod = "0";
                                                errorArray.Add(response.SonucAciklama);
                                                response.ValidationErrors = errorArray.ToArray();
                                            }
                                            else
                                            {
                                                if (!TcNoCheck(sigortali.KisiBilgi.TCKN.ToString()))
                                                {
                                                    response.SonucAciklama = "GEÇERSİZ TCNO!";
                                                    response.SonucKod = "0";
                                                    errorArray.Add(response.SonucAciklama);
                                                    response.ValidationErrors = errorArray.ToArray();
                                                }
                                            }
                                        }
                                    }
                                    break;
                                case "YBN":
                                    break;
                                default:
                                    var checkNationalty = new GenericRepository<Country>().FindBy($" A2_CODE = '{sigortali.KisiBilgi.Uyruk.ToUpper().Trim()}'").FirstOrDefault();
                                    if (checkNationalty == null)
                                    {
                                        response.SonucAciklama = "UYRUK BİLGİSİ HATALI!";
                                        response.SonucKod = "0";
                                        errorArray.Add(response.SonucAciklama);
                                    }
                                    else
                                    {
                                        if (sigortali.KisiBilgi.TCKN.IsInt64())
                                        {
                                            //    result.SonucAciklama = "TC BOŞ GEÇİLEMEZ!";
                                            //    result.SonucKod = "0";
                                            //    errorArray.Add(result.SonucAciklama);
                                            //    result.ValidationErrors = errorArray.ToArray();
                                            //}
                                            //else
                                            //{
                                            if (((long?)long.Parse(sigortali.KisiBilgi.TCKN)).IsIdentityNoLength() == null)
                                            {
                                                response.SonucAciklama = "TCNO 11 HANE OLMALI!";
                                                response.SonucKod = "0";
                                                errorArray.Add(response.SonucAciklama);
                                                response.ValidationErrors = errorArray.ToArray();
                                                return response;
                                            }
                                            else
                                            {
                                                if (sigortali.KisiBilgi.TCKN.IndexOf("9") != 0)
                                                {
                                                    response.SonucAciklama = "YABANCI TCNO GİRMEDİNİZ. LUTFEN KONTROL EDİN!";
                                                    response.SonucKod = "0";
                                                    errorArray.Add(response.SonucAciklama);
                                                    response.ValidationErrors = errorArray.ToArray();
                                                }
                                                else
                                                {
                                                    if (!TcNoCheck(sigortali.KisiBilgi.TCKN.ToString()))
                                                    {
                                                        response.SonucAciklama = "GEÇERSİZ TCNO!";
                                                        response.SonucKod = "0";
                                                        errorArray.Add(response.SonucAciklama);
                                                        response.ValidationErrors = errorArray.ToArray();
                                                    }
                                                }
                                            }
                                        }
                                        if (sigortali.KisiBilgi.DogumYeri.IsNull())
                                        {
                                            response.SonucAciklama = "YABANCI UYRUKLULAR İÇİN DOĞUM YERİ GİRİLMESİ GEREKMEKTEDİR.";
                                            response.SonucKod = "0";
                                            errorArray.Add(response.SonucAciklama);
                                            response.ValidationErrors = errorArray.ToArray();
                                        }
                                        if (sigortali.KisiBilgi.BabaAd.IsNull())
                                        {
                                            response.SonucAciklama = "YABANCI UYRUKLULAR İÇİN BABA ADI GİRİLMESİ GEREKMEKTEDİR.";
                                            response.SonucKod = "0";
                                            errorArray.Add(response.SonucAciklama);
                                            response.ValidationErrors = errorArray.ToArray();
                                        }
                                    }
                                    break;
                            }
                        }
                        if (sigortali.KisiBilgi.Ad.IsNull() || sigortali.KisiBilgi.Soyad.IsNull())
                        {
                            response.SonucAciklama = "AD - SOYAD BİLGİSİ GİRİLMESİ GEREKMEKTEDİR";
                            response.SonucKod = "0";
                            errorArray.Add(response.SonucAciklama);
                            response.ValidationErrors = errorArray.ToArray();
                        }

                        if (sigortali.KisiBilgi.Cinsiyet.IsNull())
                        {
                            response.SonucAciklama = "CİNSİYET BİLGİSİ GİRİLMESİ GEREKMEKTEDİR";
                            response.SonucKod = "0";
                            errorArray.Add(response.SonucAciklama);
                            response.ValidationErrors = errorArray.ToArray();
                        }
                        else
                        {
                            if (sigortali.KisiBilgi.Cinsiyet.ToUpper() == "K" || sigortali.KisiBilgi.Cinsiyet.ToUpper() == "E" || sigortali.KisiBilgi.Cinsiyet.ToUpper() == "0" || sigortali.KisiBilgi.Cinsiyet.ToUpper() == "1")
                            { }
                            else
                            {
                                response.SonucAciklama = "CİNSİYET BİLGİSİ HATALI (E,K) yada (0,1)";
                                response.SonucKod = "0";
                                errorArray.Add(response.SonucAciklama);
                            }
                        }

                        if (!sigortali.KisiBilgi.DogumTarihi.ToString().IsDateTime())
                        {
                            response.SonucAciklama = "DOĞUM TARİHİ BİLGİSİ GİRİLMESİ GEREKMEKTEDİR";
                            response.SonucKod = "0";
                            errorArray.Add(response.SonucAciklama);
                            response.ValidationErrors = errorArray.ToArray();
                        }

                        if (sigortali.KisiBilgi.PasaportNo == "0")
                        {
                            sigortali.KisiBilgi.PasaportNo = null;
                        }

                        if (sigortali.KisiBilgi.VKN == "0")
                        {
                            sigortali.KisiBilgi.VKN = null;
                        }

                        if (sigortali.KisiBilgi.VKN.IsInt64())
                        {
                            if (((long?)long.Parse(sigortali.KisiBilgi.VKN)).IsTaxNumberLength() == null)
                            {
                                response.SonucAciklama = "VKN 10 HANE OLMALI!";
                                response.SonucKod = "0";
                                errorArray.Add(response.SonucAciklama);
                                response.ValidationErrors = errorArray.ToArray();
                            }
                        }
                    }
                }
            }
            #endregion

            if (response.SonucKod == "0")
            {
                response.SonucAciklama = "HATA - ValidationErrors Listesini Kontrol Ediniz";
                response.ValidationErrors = errorArray.ToArray();
                return response;
            }

            #region Data Control
            Policy policy = new GenericRepository<Policy>().FindBy($"NO ={req.PoliceNo.PoliceNo} AND RENEWAL_NO = {req.PoliceNo.YenilemeNo} AND COMPANY_ID = {req.PoliceNo.SirketKod}", orderby: "").FirstOrDefault();
            if (policy == null)
            {
                #region Hata
                response.SonucAciklama = "POLİÇE BİLGİSİ BULUNAMADI!";
                response.SonucKod = "0";
                errorArray.Add(response.SonucAciklama);
                #endregion
            }
            else
            {
                log.PolicyId = policy.Id;
                log.PolicyNumber = policy.No.ToString();
                if (policy.Status == ((int)PolicyStatus.TANZIMLI).ToString() || policy.Status == ((int)PolicyStatus.TEKLIF).ToString()) { }
                else
                {
                    #region Hata
                    response.SonucAciklama = "İPTAL ZEYİLİ DAHA ÖNCE GÖNDERİLMİŞTİR. TEKRAR GÖNDERİM YAPILAMAZ.";
                    response.SonucKod = "0";
                    errorArray.Add(response.SonucAciklama);
                    #endregion
                }

                Endorsement endorsement = new GenericRepository<Endorsement>().FindBy($"POLICY_ID={policy.Id}", orderby: "ID desc").FirstOrDefault();
                if (endorsement.Status == ((int)Status.PASIF).ToString())
                {
                    #region Hata
                    response.SonucAciklama = "POLİÇEDE TANZİMSİZ ZEYL BULUNMAKTA!";
                    response.SonucKod = "0";
                    errorArray.Add(response.SonucAciklama);
                    #endregion
                }

                List<long> identityNoList = new List<long>();
                List<string> passportNoList = new List<string>();
                List<string> companyInsuredNoList = new List<string>();
                foreach (var sigortali in req.SigortaliListesi)
                {
                    if (sigortali.KisiBilgi.TCKN.IsInt64())
                    {
                        long? identityNo = long.Parse(sigortali.KisiBilgi.TCKN);
                        if (identityNo.IsIdentityNoLength() != null)
                        {
                            identityNoList.Add((long)identityNo);
                        }
                    }

                    if (!sigortali.KisiBilgi.PasaportNo.IsNull() && sigortali.KisiBilgi.PasaportNo != "0")
                    {
                        passportNoList.Add(sigortali.KisiBilgi.PasaportNo);
                    }

                    if (!sigortali.SigortaliNo.IsNull())
                    {
                        companyInsuredNoList.Add(sigortali.SigortaliNo);
                    }
                }

                List<V_Insured> insuredList = new GenericRepository<V_Insured>().FindBy($"COMPANY_ID={req.PoliceNo.SirketKod} AND POLICY_ID={policy.Id}", orderby: "");

                foreach (var insured in insuredList)
                {
                    if (companyInsuredNoList.Count > 0 && companyInsuredNoList.Where(x => x == insured.CompanyInsuredNo).FirstOrDefault() != null)
                    {
                        response.SonucAciklama = "SİGORTALI NOSU = " + insured.CompanyInsuredNo + ", OLAN SİGORTALI SİSTEMDE BULUNMAKTADIR. TEKRAR GİRİŞİ YAPILAMAZ.";
                        response.SonucKod = "0";
                        errorArray.Add(response.SonucAciklama);
                        continue;
                    }
                    if (identityNoList.Count > 0 && insured.IDENTITY_NO != null && identityNoList.Where(x => x == insured.IDENTITY_NO).FirstOrDefault() != 0)
                    {
                        response.SonucAciklama = "TC KİMLİK NOSU = " + insured.IDENTITY_NO + ", OLAN SİGORTALI SİSTEMDE BULUNMAKTADIR. TEKRAR GİRİŞİ YAPILAMAZ.";
                        response.SonucKod = "0";
                        errorArray.Add(response.SonucAciklama);
                        continue;
                    }
                    if (passportNoList.Count > 0 && !insured.PASSPORT_NO.IsNull() && insured.PASSPORT_NO != "0" && passportNoList.Where(x => x == insured.PASSPORT_NO).FirstOrDefault() != null)
                    {
                        response.SonucAciklama = "PASAPORT NOSU = " + insured.PASSPORT_NO + ", OLAN SİGORTALI SİSTEMDE BULUNMAKTADIR. TEKRAR GİRİŞİ YAPILAMAZ.";
                        response.SonucKod = "0";
                        errorArray.Add(response.SonucAciklama);
                        continue;
                    }
                }

                string packageNoList = string.Join("','", req.SigortaliListesi.Select(x => x.PaketNo));
                List<V_Package> packageList = new GenericRepository<V_Package>().FindBy($"PACKAGE_NO IN ('{packageNoList}') AND COMPANY_ID={req.PoliceNo.SirketKod}", orderby: "");
                if (packageList != null && packageList.Count > 0)
                {
                    bool isFound = false, isContinue = true;
                    foreach (var sigortali in req.SigortaliListesi)
                    {
                        foreach (var package in packageList)
                        {
                            if (sigortali.PaketNo == package.PACKAGE_NO)
                            {
                                isFound = true;
                            }
                        }
                        if (!isFound)
                        {
                            response.SonucKod = "0";
                            errorArray.Add("SİGORTALI PAKET BİLGİSİ BULUNAMADI.");
                            isContinue = false;
                            break;
                        }
                        isFound = false;
                    }
                    if (isContinue)
                    {
                        if (packageList[0].SUBPRODUCT_ID == null)
                        {
                            response.SonucKod = "0";
                            errorArray.Add($"PAKETİN ALT ÜRÜN BİLGİSİ BULUNAMADI. (PaketNo={packageList[0].PACKAGE_NO})");
                            isContinue = false;
                        }
                        if (isContinue)
                        {
                            long subProductId = (long)packageList[0].SUBPRODUCT_ID;
                            foreach (var item in packageList)
                            {
                                if (item.SUBPRODUCT_ID == null)
                                {
                                    response.SonucKod = "0";
                                    errorArray.Add($"PAKETİN ALT ÜRÜN BİLGİSİ BULUNAMADI. (PaketNo={item.PACKAGE_NO})");
                                    isContinue = false;
                                    break;
                                }
                                else if (item.SUBPRODUCT_ID != subProductId)
                                {
                                    response.SonucKod = "0";
                                    errorArray.Add($"SİGORTALI PAKET NO'LARI AYNI ALT ÜRÜN DEĞİL! (PaketNo={item.PACKAGE_NO})");
                                    isContinue = false;
                                    break;
                                }
                                else if (insuredList.Count > 0 && item.SUBPRODUCT_ID != insuredList[0].SUBPRODUCT_ID)
                                {
                                    response.SonucKod = "0";
                                    errorArray.Add($"SİGORTALI PAKET NO'LARI AYNI ALT ÜRÜN DEĞİL! (PaketNo={item.PACKAGE_NO})");
                                    isContinue = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    response.SonucKod = "0";
                    errorArray.Add("SİGORTALI PAKET BİLGİSİ BULUNAMADI.");
                }
            }

            #endregion

            if (response.SonucKod == "0")
            {
                response.SonucAciklama = "HATA - ValidationErrors Listesini Kontrol Ediniz";
                response.ValidationErrors = errorArray.ToArray();
                return response;
            }
            else
            {
                response.SonucAciklama = "OK";
                response.SonucKod = "1";
            }
            return response;
        }

        private ZeyilSigortaliEkleRes ZeyilSigortaliEkleOpr(ZeyilSigortaliEkleReq req, IntegrationLog log)
        {
            ZeyilSigortaliEkleRes result = new ZeyilSigortaliEkleRes();
            result.SonucKod = "1";
            List<string> errorArray = new List<string>();
            Policy Vpolicy = new GenericRepository<Policy>().FindBy($"COMPANY_ID={req.PoliceNo.SirketKod} AND NO={req.PoliceNo.PoliceNo} AND RENEWAL_NO={req.PoliceNo.YenilemeNo} AND STATUS IN('{((int)PolicyStatus.TANZIMLI).ToString()}','{((int)PolicyStatus.TEKLIF).ToString()}')", orderby: "").FirstOrDefault();

            //TO-DO : Döviz İşlemleri

            PolicyDto policyDto = new PolicyDto();
            IService<PolicyDto> policyService = new PolicyService();
            policyDto.isPolicyTransfer = true;
            policyDto.policyIntegrationType = PolicyIntegrationType.WS;

            #region Endorsement
            policyDto.isEndorsementChange = true;

            policyDto.PolicyId = Vpolicy.Id;
            policyDto.EndorsementNo = new GenericRepository<Endorsement>().FindBy($"POLICY_ID={Vpolicy.Id}", orderby: "ID DESC").FirstOrDefault().No + 1;
            policyDto.EndorsementType = ((int)EndorsementType.SIGORTALI_GIRISI).ToString();
            policyDto.EndorsementStartDate = req.ZeylBaslamaTarih;
            policyDto.DateOfIssue = req.ZeylTanzimTarih;
            policyDto.EndorsementRegistrationDate = req.ZeylTanzimTarih;
            policyDto.EndorsementStatus = ((int)Status.PASIF).ToString();
            #endregion

            var response = policyService.ServiceInsert(policyDto, this.Token);
            if (!response.IsSuccess)
            {
                result.SonucAciklama = response.Message;
                result.SonucKod = "0";
                errorArray.Add(result.SonucAciklama);
                result.ValidationErrors = errorArray.ToArray();
                return result;
            }

            #region Insured Opr
            policyDto.EndorsementId = response.Id;

            List<Int64> insuredIdList = new List<long>();
            decimal endorsementPremium = 0;
            foreach (var sigortali in req.SigortaliListesi)
            {
                policyDto.isEndorsementChange = false;
                policyDto.isInsuredContactChange = true;
                policyDto.isInsuredChange = false;

                policyDto.InsuredAddressId = 0;
                policyDto.InsuredContactId = 0;
                policyDto.InsuredPersonId = 0;
                policyDto.InsuredPhoneId = 0;
                policyDto.InsuredMobilePhoneId = 0;
                policyDto.InsuredEmailId = 0;
                policyDto.InsuredId = 0;
                policyDto.InsuredNoParameterId = 0;

                #region Person
                policyDto.InsuredTcNo = sigortali.KisiBilgi.TCKN.IsInt64() ? (long?)long.Parse(sigortali.KisiBilgi.TCKN) : null;
                policyDto.InsuredName = sigortali.KisiBilgi.Ad;
                policyDto.InsuredGender = sigortali.KisiBilgi.Cinsiyet;
                policyDto.InsuredLastName = sigortali.KisiBilgi.Soyad;
                policyDto.InsuredBirthDate = sigortali.KisiBilgi.DogumTarihi;
                policyDto.InsuredBirthPlace = sigortali.KisiBilgi.DogumYeri;
                policyDto.InsuredNameOfFather = sigortali.KisiBilgi.BabaAd;
                policyDto.InsurerNationality = sigortali.KisiBilgi.Uyruk;
                policyDto.InsuredPassport = sigortali.KisiBilgi.PasaportNo;
                policyDto.InsuredFirstInsuredDate = sigortali.IlkSigortalanmaTarihi;
                policyDto.InsuredNo = sigortali.SigortaliNo;
                #endregion

                #region Address
                if (sigortali.Adres != null)
                {
                    policyDto.InsuredAddress = "";
                    if (sigortali.Adres.IlKod.IsInt64())
                    {
                        sigortali.Adres.IlKod = sigortali.Adres.IlKod.Trim().TrimStart('0');
                        sigortali.Adres.IlKod = (sigortali.Adres.IlKod.Length == 1 ? "00" : "") + sigortali.Adres.IlKod;
                        sigortali.Adres.IlKod = (sigortali.Adres.IlKod.Length == 2 ? "0" : "") + sigortali.Adres.IlKod;
                        var cityy = new GenericRepository<City>().FindBy($"CODE='{sigortali.Adres.IlKod}'").FirstOrDefault();
                        policyDto.InsuredCityId = cityy != null ? (long?)cityy.Id : null;
                        if (sigortali.Adres.IlceKod.IsInt64())
                        {

                            var countyy = new GenericRepository<County>().FindBy($"CODE={sigortali.Adres.IlceKod}").FirstOrDefault();
                            policyDto.InsuredCountyId = countyy != null ? (long?)countyy.Id : null;
                            if (countyy != null)
                            {
                                policyDto.InsuredCityId = countyy.CityId;
                            }
                        }
                    }

                    if (!sigortali.Adres.Mahalle.IsNull())
                    {
                        policyDto.InsuredAddress += " Mah=" + sigortali.Adres.Mahalle;
                    }
                    if (!sigortali.Adres.Sokak.IsNull())
                    {
                        policyDto.InsuredAddress += " Sok=" + sigortali.Adres.Sokak;
                    }
                    if (!sigortali.Adres.BinaAd.IsNull())
                    {
                        policyDto.InsuredAddress += " Bina=" + sigortali.Adres.Mahalle;
                    }
                    if (!sigortali.Adres.BinaNo.IsNull())
                    {
                        policyDto.InsuredAddress += " BinaNo=" + sigortali.Adres.BinaNo;
                    }
                    if (!sigortali.Adres.DaireNo.IsNull())
                    {
                        policyDto.InsuredAddress += " Daire=" + sigortali.Adres.DaireNo;
                    }
                    if (!sigortali.Adres.AdresTarif.IsNull())
                    {
                        policyDto.InsuredAddress += " Tarif=" + sigortali.Adres.AdresTarif;
                    }

                    policyDto.InsuredZipCode = sigortali.Adres.PostaKod;
                }
                #endregion
                if (sigortali.Iletisim != null)
                {
                    #region Phone
                    policyDto.InsuredTelNo = sigortali.Iletisim.TelefonNo;
                    policyDto.InsuredGsmNo = sigortali.Iletisim.CepTel;
                    #endregion
                    #region E-mail
                    policyDto.InsuredEmail = sigortali.Iletisim.Email;
                    #endregion
                }
                #region Bank
                if (sigortali.Banka != null)
                {
                    var bank = new GenericRepository<Bank>().FindBy($"CODE='{sigortali.Banka.BankaKod}'").FirstOrDefault();
                    if (bank != null)
                    {
                        policyDto.InsuredBankAccountOwnerName = sigortali.Banka.HesapSahip;
                        policyDto.InsuredBankAccountNo = sigortali.Banka.HesapNo;
                        policyDto.InsuredBankIbanNo = sigortali.Banka.Iban;

                        policyDto.InsuredBankId = bank.Id;

                        var bankBranch = new GenericRepository<BankBranch>().FindBy($"CODE='{sigortali.Banka.SubeKod}'").FirstOrDefault();
                        policyDto.InsuredBankBranchId = bankBranch != null ? (long?)bankBranch.Id : null;
                    }
                }
                #endregion

                response = policyService.ServiceInsert(policyDto, this.Token);
                if (!response.IsSuccess)
                {
                    result.SonucAciklama = response.Message;
                    result.SonucKod = "0";
                    errorArray.Add(result.SonucAciklama);
                    result.ValidationErrors = errorArray.ToArray();

                    RollBackProxySrv();

                    return result;
                }

                #region Insured
                policyDto.isInsuredContactChange = false;
                policyDto.isInsuredChange = true;

                policyDto.InsuredContactId = response.Id;
                policyDto.InsuredIndividualType = sigortali.BireyTip;
                policyDto.InsuredFamilyNo = sigortali.AileNo.IsNull() ? 1 : long.Parse(sigortali.AileNo);

                var v_Package = new GenericRepository<V_Package>().FindBy($"PACKAGE_NO='{sigortali.PaketNo}' AND COMPANY_ID={req.PoliceNo.SirketKod}", orderby: "PACKAGE_ID").FirstOrDefault();
                policyDto.InsuredPackageId = v_Package.PACKAGE_ID;

                endorsementPremium += sigortali.PrimTL;
                policyDto.InsuredInitialPremium = sigortali.PrimTL;
                policyDto.InsuredEndorsementPremium = sigortali.PrimTL;
                policyDto.InsuredCompanyEntranceDate = sigortali.IlkKayitTarihi;
                policyDto.InsuredNo = sigortali.SigortaliNo;

                #endregion

                response = policyService.ServiceInsert(policyDto, this.Token);
                if (!response.IsSuccess)
                {
                    result.SonucAciklama = response.Message;
                    result.SonucKod = "0";
                    errorArray.Add(result.SonucAciklama);
                    result.ValidationErrors = errorArray.ToArray();

                    RollBackProxySrv(endorsementId: policyDto.EndorsementId, insuredIdList: insuredIdList);

                    return result;
                }
                insuredIdList.Add(response.Id);
            }
            #endregion

            var endorsement = new GenericRepository<Endorsement>().FindById(policyDto.EndorsementId);
            if (endorsement != null)
            {
                endorsement.Premium = endorsementPremium;
                endorsement.Status = ((int)Status.AKTIF).ToString();
                var spResEndorsement = new GenericRepository<Endorsement>().Update(endorsement);
                if (spResEndorsement.Code != "100")
                {
                    result.SonucAciklama = spResEndorsement.Message;
                    result.SonucKod = "0";
                    errorArray.Add(result.SonucAciklama);
                    result.ValidationErrors = errorArray.ToArray();

                    RollBackProxySrv(policyId: policyDto.PolicyId, endorsementId: policyDto.EndorsementId, insurerId: (long)policyDto.InsurerId, insuredIdList: insuredIdList);

                    return result;
                }
                if (endorsement.Premium != 0)
                {
                    var firstEndorsement = new Endorsement();
                    if (endorsement.Type == ((int)EndorsementType.BASLANGIC).ToString())
                    {
                        firstEndorsement = endorsement;
                    }
                    else
                    {
                        firstEndorsement = new GenericRepository<Endorsement>().FindBy("POLICY_ID=:policyId AND TYPE=:type", orderby: "", parameters: new { policyId = endorsement.PolicyId, type = new Dapper.DbString { Value = ((int)EndorsementType.BASLANGIC).ToString(), Length = 3 } }).FirstOrDefault();
                    }

                    if (firstEndorsement != null)
                    {
                        var instalmentList = new GenericRepository<Installment>().FindBy($"POLICY_ID=:policyId AND ENDORSEMENT_ID=:endorsementId AND DUE_DATE>= TO_DATE('{ (DateTime)endorsement.DateOfIssue}','DD.MM.YYYY HH24:MI:SS')", parameters: new { policyId = firstEndorsement.PolicyId, endorsementId = firstEndorsement.Id });
                        if (instalmentList != null && instalmentList.Count > 0)
                        {
                            List<Installment> listInstallment = new List<Installment>();
                            foreach (var item in instalmentList)
                            {
                                Installment installment = new Installment
                                {
                                    DueDate = item.DueDate,
                                    Amount = (decimal)endorsement.Premium / instalmentList.Count,
                                    EndorsementId = endorsement.Id,
                                    PolicyId = endorsement.PolicyId
                                };
                                listInstallment.Add(installment);
                            }
                            new GenericRepository<Installment>().InsertEntities(listInstallment);
                        }
                        else
                        {
                            Installment installment = new Installment
                            {
                                DueDate = (DateTime)endorsement.DateOfIssue,
                                Amount = (decimal)endorsement.Premium,
                                EndorsementId = endorsement.Id,
                                PolicyId = endorsement.PolicyId
                            };
                            new GenericRepository<Installment>().Insert(installment);
                        }
                    }
                }
            }

            long? LastEndorsementId = 0;
            decimal? premium = 0;
            var policy = new GenericRepository<Policy>().FindById(policyDto.PolicyId);
            if (policy != null)
            {
                LastEndorsementId = policy.LastEndorsementId;
                premium = policy.Premium;
                policy.LastEndorsementId = policyDto.EndorsementId;
                policy.Premium += endorsementPremium;
                var spResPolicy = new GenericRepository<Policy>().Update(policy);
                if (spResPolicy.Code != "100")
                {
                    result.SonucAciklama = spResPolicy.Message;
                    result.SonucKod = "0";
                    errorArray.Add(result.SonucAciklama);
                    result.ValidationErrors = errorArray.ToArray();

                    RollBackProxySrv(endorsementId: policyDto.EndorsementId, insuredIdList: insuredIdList);

                    return result;
                }
            }

            if (req.PoliceNo.SirketKod == 10)
            {
                bool isSgmOk = false;
                SgmServiceClient sgmServiceClient = new SgmServiceClient();
                SbmInsuredEntranceReq sbmInsuredExitReq = new SbmInsuredEntranceReq
                {
                    EndorsementId = policyDto.EndorsementId,
                    PolicyId = policyDto.PolicyId
                };
                var sbmResponse = sgmServiceClient.SbmInsuredEntranceEndorsement(sbmInsuredExitReq, SagmerServiceType.KONTROL);
                if (sbmResponse.ResultCode != 1)
                {
                    RollBackProxySrv(policyId: policyDto.PolicyId, endorsementId: policyDto.EndorsementId, insuredIdList: insuredIdList, policyPremium: premium, lastEndorsementId: LastEndorsementId, policyIsActive: true);

                    List<string> SbmErrorList = new List<string>();
                    foreach (var item in sbmResponse.FailedErrors)
                    {
                        SbmErrorList.Add(item.code + "  " + item.message);
                    }
                    result.SonucKod = "0";
                    result.SonucAciklama = "SAGMER HATA, ValidationErrors Kontrol Ediniz.";
                    result.ValidationErrors = SbmErrorList.ToArray();
                    return result;
                }
                else
                {
                    isSgmOk = true;
                }

                if (isSgmOk)
                {
                    sbmResponse = sgmServiceClient.SbmInsuredEntranceEndorsement(sbmInsuredExitReq, SagmerServiceType.CANLI);
                    if (sbmResponse.ResultCode != 1)
                    {
                        RollBackProxySrv(policyId: policyDto.PolicyId, endorsementId: policyDto.EndorsementId, insuredIdList: insuredIdList, policyPremium: premium, lastEndorsementId: LastEndorsementId, policyIsActive: true);

                        List<string> SbmErrorList = new List<string>();
                        foreach (var item in sbmResponse.FailedErrors)
                        {
                            SbmErrorList.Add(item.code + "  " + item.message);
                        }
                        result.SonucKod = "0";
                        result.SonucAciklama = "SAGMER HATA, ValidationErrors Kontrol Ediniz.";
                        result.ValidationErrors = SbmErrorList.ToArray();
                        return result;
                    }
                }
            }

            result.SonucAciklama = "OK";
            result.SonucKod = "1";
            return result;
        }
        #endregion

        #region ZeyilSigortaliCikar
        public ZeyilSigortaliCikarRes ZeyilSigortaliCikar(string apiCode, ZeyilSigortaliCikarReq req)
        {
            ZeyilSigortaliCikarRes response = new ZeyilSigortaliCikarRes();
            List<string> errorArray = new List<string>();
            var currentMethod = MethodBase.GetCurrentMethod();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                response = ZeyilSigortaliCikarValidation(req, apiCode, log);
                if (response.SonucKod != "1")
                {
                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                    return response;
                }
                #endregion

                #region Operation
                response = ZeyilSigortaliCıkarOpr(req);
                if (response.SonucKod != "1")
                {
                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                    return response;
                }
                #endregion

                #region OK
                response.SonucAciklama = "OK";
                response.SonucKod = "1";
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.SonucKod = "0";
                response.SonucAciklama = "HATA - ValidationErrors Listesini Kontrol Ediniz";
                response.ValidationErrors = new string[] { ex.Message };

                CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                #endregion
            }

            return response;
        }
        private ZeyilSigortaliCikarRes ZeyilSigortaliCikarValidation(ZeyilSigortaliCikarReq req, string apiCode, IntegrationLog log)
        {
            ZeyilSigortaliCikarRes response = new ZeyilSigortaliCikarRes();
            List<string> errorArray = new List<string>();

            #region Incoming Validation
            if (req.PoliceNo == null)
            {
                response.SonucKod = "0";
                errorArray.Add("POLICE BİLGİ ALANI BOŞ OLAMAZ");
            }
            else
            {
                if (!req.PoliceNo.SirketKod.ToString().IsNumeric())
                {
                    response.SonucKod = "0";
                    errorArray.Add("ŞİRKET KOD GİRİLMESİ GEREKMEKTEDİR");
                }
                if (!req.PoliceNo.PoliceNo.ToString().IsNumeric() || !req.PoliceNo.YenilemeNo.ToString().IsNumeric())
                {
                    response.SonucKod = "0";
                    errorArray.Add("POLİÇE NO VE YENİLEME NO ALANLARI BOŞ GEÇİLEMEZ");
                }
            }
            if (response.SonucKod == "0")
            {
                response.ValidationErrors = errorArray.ToArray();
                response.SonucAciklama = "HATA";
                return response;
            }
            #endregion

            #region Auth
            if (ApiCodeIsValid(apiCode, req.PoliceNo.SirketKod) <= 0)
            {
                response.SonucAciklama = "HATA";
                response.SonucKod = "0";
                errorArray.Add("Giriş Başarısız! APİCODE'u kontrol ediniz");
                response.ValidationErrors = errorArray.ToArray();
                return response;
            }
            log.COMPANY_ID = req.PoliceNo.SirketKod;
            #endregion

            #region Zorunlu Alan
            if (req.ZeylNo.IsInt() && req.PoliceNo.SirketKod != 3)
            {
                var endorsementList = new GenericRepository<V_PolicyEndorsement>().FindBy(conditions: $"POLICY_NUMBER={req.PoliceNo.PoliceNo}", orderby: "ENDORSEMENT_NO desc").FirstOrDefault();
                int nextEndorsementNo = Convert.ToInt32(endorsementList.ENDORSEMENT_NO) + 1;
                if (Convert.ToInt32(req.ZeylNo) != nextEndorsementNo)
                {
                    response.SonucAciklama = "Zeyl No Son Zeyl Numarası ile ardışık ilerlemesi gerekir. Son Zeyl NO : " + endorsementList.ENDORSEMENT_NO;
                    response.SonucKod = "0";
                    errorArray.Add(response.SonucAciklama);
                }
            }
            if (req.PoliceNo == null)
            {
                response.SonucAciklama = "POLICE NO BİLGİLERİNİN GİRİLMESİ GEREKMEKTEDİR";
                response.SonucKod = "0";
                errorArray.Add(response.SonucAciklama);
            }
            else
            {
                if (!req.PoliceNo.SirketKod.ToString().IsNumeric())
                {
                    response.SonucAciklama = "ŞİRKET KOD GİRİLMESİ GEREKMEKTEDİR";
                    response.SonucKod = "0";
                    errorArray.Add(response.SonucAciklama);
                }

                if (!req.PoliceNo.PoliceNo.ToString().IsNumeric() || !req.PoliceNo.YenilemeNo.ToString().IsNumeric())
                {
                    response.SonucAciklama = "POLİÇE NO VE YENİLEME NO GİRİLMESİ GEREKMEKTEDİR";
                    response.SonucKod = "0";
                    errorArray.Add(response.SonucAciklama);
                }
            }
            if (!req.ZeylBaslamaTarih.ToString().IsDateTime() || !req.ZeylTanzimTarih.ToString().IsDateTime())
            {
                response.SonucAciklama = "ZEYL TARİH BİLGİLERİ GİRİLMESİ GEREKMEKTEDİR";
                response.SonucKod = "0";
                errorArray.Add(response.SonucAciklama);
            }

            if (req.SigortaliListesi == null || req.SigortaliListesi.Count <= 0)
            {
                response.SonucAciklama = "EN AZ BİR SİGORTALI GİRİLMESİ GEREKMEKTEDİR";
                response.SonucKod = "0";
                errorArray.Add(response.SonucAciklama);
            }
            else
            {
                foreach (var sigortali in req.SigortaliListesi)
                {
                    if (sigortali.SigortaliNo.IsNull())
                    {
                        response.SonucAciklama = "SİGORTALI NO GİRİLMESİ GEREKMEKTEDİR";
                        response.SonucKod = "0";
                        errorArray.Add(response.SonucAciklama);
                    }
                    if (sigortali.BireyTip.IsNull())
                    {
                        sigortali.BireyTip = "F";
                    }
                    else
                    {
                        List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.IndividualType>();
                        bool findHim = false;
                        foreach (EnumValue enVal in enumValuess)
                        {
                            if (enVal.Text == sigortali.BireyTip)
                            { findHim = true; break; }
                        }
                        if (!findHim)
                        {
                            if (sigortali.BireyTip == "K") { sigortali.BireyTip = "F"; }
                            else if (sigortali.BireyTip == "S") { sigortali.BireyTip = "E"; }
                            else
                            {
                                response.SonucKod = "0";
                                response.SonucAciklama = "GEÇERSİZ BİREY TİPİ";
                                errorArray.Add(response.SonucAciklama);
                            }
                        }
                    }

                    if (!sigortali.PrimTL.ToString().IsNumeric())
                    {
                        response.SonucAciklama = "PRİM TL GİRİLMESİ GEREKMEKTEDİR";
                        response.SonucKod = "0";
                        errorArray.Add(response.SonucAciklama);
                    }
                }
            }
            if (response.SonucKod == "0")
            {
                response.SonucAciklama = "ERROR";
                response.ValidationErrors = errorArray.ToArray();
                return response;
            }
            #endregion

            #region Data Control

            V_Policy policy = new GenericRepository<V_Policy>().FindBy($"POLICY_NUMBER ={req.PoliceNo.PoliceNo} AND RENEWAL_NO = {req.PoliceNo.YenilemeNo} AND COMPANY_ID = {req.PoliceNo.SirketKod}", orderby: "POLICY_ID").FirstOrDefault();
            if (policy == null)
            {
                #region Hata
                response.SonucAciklama = "POLİÇE BİLGİSİ BULUNAMADI!";
                response.SonucKod = "0";
                errorArray.Add(response.SonucAciklama);
                response.ValidationErrors = errorArray.ToArray();

                #endregion
                return response;
            }
            log.PolicyId = policy.POLICY_ID;
            //TODO
            if (policy.STATUS != ((int)PolicyStatus.TANZIMLI).ToString() && policy.STATUS != ((int)PolicyStatus.TEKLIF).ToString())
            {
                #region Hata
                response.SonucAciklama = "İPTAL ZEYLİ DAHA ÖNCE GÖNDERİLMİŞTİR. TEKRAR GÖNDERİM YAPILAMAZ.!";
                response.SonucKod = "0";
                errorArray.Add(response.SonucAciklama);
                response.ValidationErrors = errorArray.ToArray();
                #endregion
                return response;
            }

            Endorsement endorsement = new GenericRepository<Endorsement>().FindBy($"POLICY_ID={policy.POLICY_ID}", orderby: "ID desc").FirstOrDefault();
            if (endorsement.Status == ((int)Status.PASIF).ToString())
            {
                #region Hata
                response.SonucAciklama = "POLİÇEDE TANZİMSİZ ZEYL BULUNMAKTA!";
                response.SonucKod = "0";
                errorArray.Add(response.SonucAciklama);
                response.ValidationErrors = errorArray.ToArray();
                #endregion
                return response;
            }

            foreach (var sigortali in req.SigortaliListesi)
            {

                V_Insured insured = new GenericRepository<V_Insured>().FindBy($"POLICY_ID={policy.POLICY_ID} AND COMPANY_ID = {req.PoliceNo.SirketKod} AND COMPANY_INSURED_NO='{sigortali.SigortaliNo}' AND STATUS='{((int)Status.AKTIF)}'", orderby: "INSURED_ID").FirstOrDefault();
                if (insured == null)
                {
                    response.SonucAciklama = "SİGORTALI BULUNAMADI!";
                    response.SonucKod = "0";
                    errorArray.Add(response.SonucAciklama);
                    response.ValidationErrors = errorArray.ToArray();

                    return response;
                }
            }
            #endregion
            if (response.SonucKod == "0")
            {
                response.SonucAciklama = "ERROR";
                response.ValidationErrors = errorArray.ToArray();
            }
            else
            {
                response.SonucAciklama = "OK";
                response.SonucKod = "1";
            }
            return response;
        }
        private ZeyilSigortaliCikarRes ZeyilSigortaliCıkarOpr(ZeyilSigortaliCikarReq req)
        {
            ZeyilSigortaliCikarRes result = new ZeyilSigortaliCikarRes();
            result.SonucKod = "1";
            List<string> errorArray = new List<string>();
            V_Policy Vpolicy = new GenericRepository<V_Policy>().FindBy($"COMPANY_ID={req.PoliceNo.SirketKod} AND POLICY_NUMBER={req.PoliceNo.PoliceNo} AND RENEWAL_NO={req.PoliceNo.YenilemeNo}", orderby: "POLICY_ID").FirstOrDefault();

            //TO-DO : Döviz İşlemleri
            PolicyDto policyDto = new PolicyDto();
            IService<PolicyDto> policyService = new PolicyService();
            policyDto.isPolicyTransfer = true;
            #region Endorsement
            policyDto.policyIntegrationType = PolicyIntegrationType.WS;

            policyDto.isEndorsementChange = true;
            policyDto.PolicyId = Vpolicy.POLICY_ID;
            policyDto.EndorsementNo = new GenericRepository<Endorsement>().FindBy($"POLICY_ID={Vpolicy.POLICY_ID}", orderby: "ID DESC").FirstOrDefault().No + 1;
            policyDto.EndorsementType = ((int)EndorsementType.SIGORTALI_CIKISI).ToString();
            policyDto.EndorsementStartDate = req.ZeylBaslamaTarih;
            policyDto.DateOfIssue = req.ZeylTanzimTarih;
            policyDto.EndorsementRegistrationDate = req.ZeylTanzimTarih;
            #endregion

            var response = policyService.ServiceInsert(policyDto, this.Token);
            if (!response.IsSuccess)
            {
                result.SonucAciklama = response.Message;
                result.SonucKod = "0";
                errorArray.Add(result.SonucAciklama);
                result.ValidationErrors = errorArray.ToArray();
                return result;
            }
            policyDto.EndorsementId = response.Id;

            #region Insured Opr

            policyDto.isEndorsementChange = false;
            List<Int64> insuredIdList = new List<long>();
            decimal endorsementPremium = 0;
            foreach (var sigortali in req.SigortaliListesi)
            {
                #region Insured
                V_Insured insured = new GenericRepository<V_Insured>().FindBy($"POLICY_ID={Vpolicy.POLICY_ID} AND COMPANY_ID = {req.PoliceNo.SirketKod} AND COMPANY_INSURED_NO='{sigortali.SigortaliNo}' AND STATUS='{((int)Status.AKTIF)}'", orderby: "INSURED_ID").FirstOrDefault();

                policyDto.isInsuredContactChange = false;
                policyDto.isInsuredChange = true;
                policyDto.InsuredId = (long)insured.INSURED_ID;
                policyDto.InsuredContactId = (long)insured.CONTACT_ID;
                policyDto.InsuredIndividualType = insured.INDIVIDUAL_TYPE;
                policyDto.InsuredFamilyNo = (long)insured.FAMILY_NO;
                policyDto.InsuredPackageId = (long)insured.PACKAGE_ID;

                decimal exitPremium = (sigortali.PrimTL < 0 ? sigortali.PrimTL : (-1 * sigortali.PrimTL));
                endorsementPremium += exitPremium;
                policyDto.InsuredTotalPremium = (decimal)insured.TOTAL_PREMIUM + exitPremium;
                policyDto.InsuredEndorsementPremium = exitPremium;
                policyDto.InsuredCompanyEntranceDate = sigortali.IlkKayitTarihi;
                policyDto.InsuredStatus = ((int)Status.SILINDI).ToString();
                #endregion

                response = policyService.ServiceInsert(policyDto, this.Token);
                if (!response.IsSuccess)
                {
                    result.SonucAciklama = response.Message;
                    result.SonucKod = "0";
                    errorArray.Add(result.SonucAciklama);
                    result.ValidationErrors = errorArray.ToArray();
                    RollBackProxySrv(endorsementId: policyDto.EndorsementId, insuredIdList: insuredIdList);
                    return result;
                }
                insuredIdList.Add(insured.INSURED_ID);
            }
            #endregion

            var endorsement = new GenericRepository<Endorsement>().FindById(policyDto.EndorsementId);
            if (endorsement != null)
            {
                endorsement.Premium = endorsementPremium;
                endorsement.Status = ((int)Status.AKTIF).ToString();
                var spResEndorsement = new GenericRepository<Endorsement>().Update(endorsement);
                if (spResEndorsement.Code != "100")
                {
                    result.SonucAciklama = spResEndorsement.Message;
                    result.SonucKod = "0";
                    errorArray.Add(result.SonucAciklama);
                    result.ValidationErrors = errorArray.ToArray();

                    RollBackProxySrv(policyId: policyDto.PolicyId, endorsementId: policyDto.EndorsementId, insurerId: (long)policyDto.InsurerId, insuredIdList: insuredIdList);

                    return result;
                }
                if (endorsement.Premium != 0)
                {
                    var firstEndorsement = new Endorsement();
                    if (endorsement.Type == ((int)EndorsementType.BASLANGIC).ToString())
                    {
                        firstEndorsement = endorsement;
                    }
                    else
                    {
                        firstEndorsement = new GenericRepository<Endorsement>().FindBy("POLICY_ID=:policyId AND TYPE=:type", orderby: "", parameters: new { policyId = endorsement.PolicyId, type = new Dapper.DbString { Value = ((int)EndorsementType.BASLANGIC).ToString(), Length = 3 } }).FirstOrDefault();
                    }

                    if (firstEndorsement != null)
                    {
                        var instalmentList = new GenericRepository<Installment>().FindBy($"POLICY_ID=:policyId AND ENDORSEMENT_ID=:endorsementId AND DUE_DATE>= TO_DATE('{ (DateTime)endorsement.DateOfIssue}','DD.MM.YYYY HH24:MI:SS')", parameters: new { policyId = firstEndorsement.PolicyId, endorsementId = firstEndorsement.Id });
                        if (instalmentList != null && instalmentList.Count > 0)
                        {
                            List<Installment> listInstallment = new List<Installment>();
                            foreach (var item in instalmentList)
                            {
                                Installment installment = new Installment
                                {
                                    DueDate = item.DueDate,
                                    Amount = (decimal)endorsement.Premium / instalmentList.Count,
                                    EndorsementId = endorsement.Id,
                                    PolicyId = endorsement.PolicyId
                                };
                                listInstallment.Add(installment);
                            }
                            new GenericRepository<Installment>().InsertEntities(listInstallment);
                        }
                        else
                        {
                            Installment installment = new Installment
                            {
                                DueDate = (DateTime)endorsement.DateOfIssue,
                                Amount = (decimal)endorsement.Premium,
                                EndorsementId = endorsement.Id,
                                PolicyId = endorsement.PolicyId
                            };
                            new GenericRepository<Installment>().Insert(installment);
                        }
                    }
                }
            }

            long? LastEndorsementId = 0;
            decimal? premium = 0;
            var policy = new GenericRepository<Policy>().FindById(policyDto.PolicyId);
            if (policy != null)
            {
                LastEndorsementId = policy.LastEndorsementId;
                premium = policy.Premium;
                policy.LastEndorsementId = policyDto.EndorsementId;
                policy.Premium += endorsementPremium;
                var spResPolicy = new GenericRepository<Policy>().Update(policy);
                if (spResPolicy.Code != "100")
                {
                    result.SonucAciklama = spResPolicy.Message;
                    result.SonucKod = "0";
                    errorArray.Add(result.SonucAciklama);
                    result.ValidationErrors = errorArray.ToArray();

                    RollBackProxySrv(policyId: policyDto.PolicyId, endorsementId: policyDto.EndorsementId, insurerId: (long)policyDto.InsurerId, insuredIdList: insuredIdList, insuredIsActive: true);

                    return result;
                }
            }

            if (req.PoliceNo.SirketKod == 10)
            {
                bool isSgmOk = false;
                SgmServiceClient sgmServiceClient = new SgmServiceClient();
                SbmInsuredExitReq sbmInsuredExitReq = new SbmInsuredExitReq
                {
                    EndorsementId = policyDto.EndorsementId,
                    PolicyId = policyDto.PolicyId
                };
                var sbmResponse = sgmServiceClient.SbmInsuredExitEndorsement(sbmInsuredExitReq, SagmerServiceType.KONTROL);
                if (sbmResponse.ResultCode != 1)
                {
                    RollBackProxySrv(policyId: policyDto.PolicyId, endorsementId: policyDto.EndorsementId, insuredIdList: insuredIdList, policyPremium: premium, lastEndorsementId: LastEndorsementId, insuredIsActive: true, policyIsActive: true);

                    List<string> SbmErrorList = new List<string>();
                    foreach (var item in sbmResponse.FailedErrors)
                    {
                        SbmErrorList.Add(item.code + "  " + item.message);
                    }
                    result.SonucKod = "0";
                    result.SonucAciklama = "SAGMER HATA, ValidationErrors Kontrol Ediniz.";
                    result.ValidationErrors = SbmErrorList.ToArray();
                    return result;
                }
                else
                {
                    isSgmOk = true;
                }

                if (isSgmOk)
                {
                    sbmResponse = sgmServiceClient.SbmInsuredExitEndorsement(sbmInsuredExitReq, SagmerServiceType.CANLI);
                    if (sbmResponse.ResultCode != 1)
                    {
                        RollBackProxySrv(policyId: policyDto.PolicyId, endorsementId: policyDto.EndorsementId, insuredIdList: insuredIdList, policyPremium: premium, lastEndorsementId: LastEndorsementId, insuredIsActive: true, policyIsActive: true);

                        List<string> SbmErrorList = new List<string>();
                        foreach (var item in sbmResponse.FailedErrors)
                        {
                            SbmErrorList.Add(item.code + "  " + item.message);
                        }
                        result.SonucKod = "0";
                        result.SonucAciklama = "SAGMER HATA, ValidationErrors Kontrol Ediniz.";
                        result.ValidationErrors = SbmErrorList.ToArray();
                        return result;
                    }
                }
            }

            if (result.SonucKod == "0")
            {
                result.SonucAciklama = "ERROR";
                result.ValidationErrors = errorArray.ToArray();
                return result;
            }
            else
            {
                result.SonucAciklama = "OK";
                result.SonucKod = "1";
            }
            return result;
        }
        #endregion

        #region PoliceBilgiSorgula
        public PoliceBilgiRes PoliceBilgiSorgula(string apiCode, PoliceBilgiReq req)
        {
            PoliceBilgiRes response = new PoliceBilgiRes();
            List<string> errorArray = new List<string>();
            var currentMethod = MethodBase.GetCurrentMethod();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                response = PoliceBilgiSorgulaValidation(req, apiCode, log);
                if (response.SonucKod != "1")
                {
                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);

                    return response;
                }
                #endregion

                #region Operation
                response = PoliceBilgiSorgulaOpr(req);
                if (response.SonucKod != "1")
                {
                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);

                    return response;
                }
                #endregion

                #region OK
                response.SonucAciklama = "OK";
                response.SonucKod = "1";
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.SonucKod = "0";
                response.SonucAciklama = "HATA - ValidationErrors Listesini Kontrol Ediniz";
                response.ValidationErrors = new string[] { ex.Message };

                CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                #endregion
            }
            return response;
        }
        private PoliceBilgiRes PoliceBilgiSorgulaValidation(PoliceBilgiReq req, string apiCode, IntegrationLog log)
        {
            PoliceBilgiRes response = new PoliceBilgiRes();
            List<string> errorArray = new List<string>();

            #region Incoming Validation
            if (req.PolPK == null)
            {
                response.SonucKod = "0";
                errorArray.Add("POLICE BİLGİ ALANI GİRİLMESİ GEREKMEKTEDİR");
            }
            else
            {
                if (!req.PolPK.SirketKod.ToString().IsNumeric())
                {
                    response.SonucKod = "0";
                    errorArray.Add("ŞİRKET KOD GİRİLMESİ GEREKMEKTEDİR");
                }
                if (!req.PolPK.PoliceNo.ToString().IsNumeric() || !req.PolPK.YenilemeNo.ToString().IsNumeric())
                {
                    response.SonucKod = "0";
                    errorArray.Add("POLİÇE NO VE YENİLEME NO GİRİLMESİ GEREKMEKTEDİR");
                }
            }
            if (response.SonucKod == "0")
            {
                response.ValidationErrors = errorArray.ToArray();
                response.SonucAciklama = "HATA";
                return response;
            }
            #endregion

            #region Auth
            if (ApiCodeIsValid(apiCode, req.PolPK.SirketKod) <= 0)
            {
                response.SonucAciklama = "HATA";
                response.SonucKod = "0";
                errorArray.Add("Giriş Başarısız! APİCODE'u kontrol ediniz");
                response.ValidationErrors = errorArray.ToArray();
                return response;
            }
            log.COMPANY_ID = req.PolPK.SirketKod;
            #endregion

            #region Data Control
            V_Policy policyEndorsmentView = new GenericRepository<V_Policy>().FindBy($"COMPANY_ID={req.PolPK.SirketKod} AND POLICY_NUMBER={req.PolPK.PoliceNo} AND RENEWAL_NO={req.PolPK.YenilemeNo} AND STATUS IN('{((int)PolicyStatus.TANZIMLI).ToString()}','{((int)PolicyStatus.TEKLIF).ToString()}')", orderby: "POLICY_ID").FirstOrDefault();
            if (policyEndorsmentView == null)
            {
                response.SonucAciklama = "POLİÇE BİLGİSİ BULUNAMADI.";
                response.SonucKod = "0";
                errorArray.Add(response.SonucAciklama);
                response.ValidationErrors = errorArray.ToArray();
                return response;
            }
            log.PolicyId = policyEndorsmentView.POLICY_ID;
            if (req.MusteriNo.IsInt64())
            {
                var insuredParameter = new GenericRepository<V_InsuredParameter>().FindBy($"KEY='INSURED_NO' AND VALUE='{req.MusteriNo}'", orderby: "INSURED_ID");
                if (insuredParameter != null)
                {
                    bool findHim = false;
                    foreach (var insured in insuredParameter)
                    {
                        V_Insured _insured = new GenericRepository<V_Insured>().FindBy($"POLICY_ID={policyEndorsmentView.POLICY_ID} AND INSURED_ID={insured.InsuredId} AND STATUS='{((int)Status.AKTIF)}'", orderby: "INSURED_ID").FirstOrDefault();
                        if (_insured == null)
                        {

                        }
                        else
                        {
                            findHim = true;
                        }
                    }

                    if (!findHim)
                    {
                        response.SonucAciklama = "SİGORTALI BULUNAMADI!";
                        response.SonucKod = "0";
                        errorArray.Add(response.SonucAciklama);
                        response.ValidationErrors = errorArray.ToArray();
                        return response;
                    }
                }
                else
                {
                    response.SonucAciklama = "SİGORTALI BULUNAMADI!";
                    response.SonucKod = "0";
                    errorArray.Add(response.SonucAciklama);
                    response.ValidationErrors = errorArray.ToArray();
                    return response;
                }
            }
            #endregion

            response.SonucAciklama = "OK";
            response.SonucKod = "1";
            return response;
        }
        private PoliceBilgiRes PoliceBilgiSorgulaOpr(PoliceBilgiReq req)
        {
            PoliceBilgiRes result = new PoliceBilgiRes();

            List<V_Insured> insuredList = new List<V_Insured>();
            if (req.MusteriNo.IsInt64())
            {
                //var insuredParameter = new GenericRepository<V_InsuredParameter>().FindBy($"KEY='INSURED_NO' AND VALUE='{req.MusteriNo}'", orderby: "INSURED_ID");
                //if (insuredParameter != null)
                //{
                //    bool findHim = false;
                //    foreach (var insured in insuredParameter)
                //    {
                //        V_Insured _insured = new GenericRepository<V_Insured>().FindBy($"POLICY_ID={policyEndorsmentView.POLICY_ID} AND INSURED_ID={insured.InsuredId} AND STATUS='{((int)Status.AKTIF)}'", orderby: "INSURED_ID").FirstOrDefault();
                //        if (_insured == null)
                //        {

                //        }
                //        else
                //        {
                //            findHim = true;
                //        }
                //    }

                //    if (!findHim)
                //    {
                //        result.SonucAciklama = "SİGORTALI BULUNAMADI!";
                //        result.SonucKod = "0";
                //        errorArray.Add(result.SonucAciklama);
                //        result.ValidationErrors = errorArray.ToArray();
                //        return result;
                //    }
                //}
                var insuredParameter = new GenericRepository<V_InsuredParameter>().FindBy($"KEY='INSURED_NO' AND VALUE='{req.MusteriNo}'", orderby: "INSURED_ID");
                if (insuredParameter != null)
                {
                    var idns = insuredParameter.Select(i => i.InsuredId).ToList();
                    var idList = string.Join(", ", idns.ToArray());
                    V_Insured insured = new GenericRepository<V_Insured>().FindBy($"COMPANY_ID={req.PolPK.SirketKod} AND POLICY_NUMBER={req.PolPK.PoliceNo} AND RENEWAL_NO={req.PolPK.YenilemeNo} AND POLICY_STATUS IN('{((int)PolicyStatus.TANZIMLI).ToString()}','{((int)PolicyStatus.TEKLIF).ToString()}') AND INSURED_ID IN({idList}) AND STATUS='{((int)Status.AKTIF)}'", orderby: "INSURED_ID").FirstOrDefault();
                    if (insured != null)
                    {
                        insuredList.Add(insured);
                    }
                    else
                    {
                        insuredList = new GenericRepository<V_Insured>().FindBy($"COMPANY_ID={req.PolPK.SirketKod} AND POLICY_NUMBER={req.PolPK.PoliceNo} AND RENEWAL_NO={req.PolPK.YenilemeNo} AND POLICY_STATUS IN('{((int)PolicyStatus.TANZIMLI).ToString()}','{((int)PolicyStatus.TEKLIF).ToString()}') AND STATUS='{((int)Status.AKTIF)}'", orderby: "");
                    }
                }
                else
                {
                    insuredList = new GenericRepository<V_Insured>().FindBy($"COMPANY_ID={req.PolPK.SirketKod} AND POLICY_NUMBER={req.PolPK.PoliceNo} AND RENEWAL_NO={req.PolPK.YenilemeNo} AND POLICY_STATUS IN('{((int)PolicyStatus.TANZIMLI).ToString()}','{((int)PolicyStatus.TEKLIF).ToString()}') AND STATUS='{((int)Status.AKTIF)}'", orderby: "");
                }
            }
            else
            {
                insuredList = new GenericRepository<V_Insured>().FindBy($"COMPANY_ID={req.PolPK.SirketKod} AND POLICY_NUMBER={req.PolPK.PoliceNo} AND RENEWAL_NO={req.PolPK.YenilemeNo} AND POLICY_STATUS IN('{((int)PolicyStatus.TANZIMLI).ToString()}','{((int)PolicyStatus.TEKLIF).ToString()}') AND STATUS='{((int)Status.AKTIF)}'", orderby: "");
            }

            List<SigortaliKullanımTeminat> sigortaliKullanımTeminatList = new List<SigortaliKullanımTeminat>();
            List<SigortaliKullanım> SigortaliKullanımList = new List<SigortaliKullanım>();

            int j = 0;
            foreach (var insured in insuredList)
            {
                V_InsuredParameter insuredParam = new GenericRepository<V_InsuredParameter>().FindBy($"INSURED_ID={insured.INSURED_ID} AND KEY='INSURED_NO'", orderby: "INSURED_ID").FirstOrDefault();
                if (insuredParam != null)
                {
                    SigortaliKullanım sigortaliKullanım = new SigortaliKullanım
                    {
                        Ad = insured.FIRST_NAME,
                        Soyad = insured.LAST_NAME,
                        Yakinlik = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Individual, insured.INDIVIDUAL_TYPE),
                        ExMusteriNo = insuredParam.Value
                    };

                    List<SigortaliKullanımTeminat> teminatList = new List<SigortaliKullanımTeminat>();

                    List<V_InsuredPackageRemaining> insuredPackageRemainingList = new GenericRepository<V_InsuredPackageRemaining>().FindBy($"INSURED_ID={insured.INSURED_ID}", orderby: "INSURED_ID", fetchDeletedRows: true, fetchHistoricRows: true);
                    int i = 0;
                    foreach (var remaining in insuredPackageRemainingList)
                    {
                        SigortaliKullanımTeminat teminat = new SigortaliKullanımTeminat();

                        if (remaining.AgrAgreementType == ((int)AgreementType.ADETLIK).ToString())
                        {
                            teminat.TeminatAd = remaining.CoverageName;
                            teminat.Toplam = remaining.AgrNumberLimit.ToString();
                            teminat.Kullanim = remaining.AgrNumberLimitUse;
                            teminat.Kalan = remaining.AgrNumberLimitRemaining;
                            teminat.Uygulama = AgreementType.ADETLIK.ToString();
                        }
                        else if (remaining.AgrAgreementType == ((int)AgreementType.FATURA_BASINA).ToString())
                        {
                            teminat.TeminatAd = remaining.CoverageName;
                            teminat.Toplam = remaining.AgrPriceLimit.ToString();
                            teminat.Kullanim = remaining.AgrPriceLimitUse;
                            teminat.Kalan = remaining.AgrPriceLimitRemaining;
                            teminat.Uygulama = AgreementType.FATURA_BASINA.ToString();
                        }
                        else if (remaining.AgrAgreementType == ((int)AgreementType.GUNLUK).ToString())
                        {
                            teminat.TeminatAd = remaining.CoverageName;
                            teminat.Toplam = remaining.AgrDayLimit.ToString();
                            teminat.Kullanim = remaining.AgrDayLimitUse;
                            teminat.Kalan = remaining.AgrDayLimitRemaining;
                            teminat.Uygulama = AgreementType.GUNLUK.ToString();
                        }
                        else if (remaining.AgrAgreementType == ((int)AgreementType.HAVUZ_GRUP).ToString())
                        {
                            teminat.TeminatAd = remaining.CoverageName;
                            if (remaining.AgrDayLimit == null && remaining.AgrNumberLimit != null)
                            {
                                teminat.Toplam = remaining.AgrNumberLimit.ToString();
                                teminat.Kullanim = remaining.AgrNumberLimitUse;
                                teminat.Kalan = remaining.AgrNumberLimitRemaining;
                            }
                            else if (remaining.AgrDayLimit != null && remaining.AgrNumberLimit == null)
                            {
                                teminat.Toplam = remaining.AgrDayLimit.ToString();
                                teminat.Kullanim = remaining.AgrDayLimitUse;
                                teminat.Kalan = remaining.AgrDayLimitRemaining;
                            }
                            else
                            {
                                teminat.Toplam = remaining.AgrPriceLimit.ToString();
                                teminat.Kullanim = remaining.AgrPriceLimitUse;
                                teminat.Kalan = remaining.AgrPriceLimitRemaining;
                            }
                            teminat.Uygulama = AgreementType.HAVUZ_GRUP.ToString();
                        }
                        else if (remaining.AgrAgreementType == ((int)AgreementType.HAVUZ_HOLDING).ToString())
                        {
                            teminat.TeminatAd = remaining.CoverageName;
                            if (remaining.AgrDayLimit == null && remaining.AgrNumberLimit != null)
                            {
                                teminat.Toplam = remaining.AgrNumberLimit.ToString();
                                teminat.Kullanim = remaining.AgrNumberLimitUse;
                                teminat.Kalan = remaining.AgrNumberLimitRemaining;
                            }
                            else if (remaining.AgrDayLimit != null && remaining.AgrNumberLimit == null)
                            {
                                teminat.Toplam = remaining.AgrDayLimit.ToString();
                                teminat.Kullanim = remaining.AgrDayLimitUse;
                                teminat.Kalan = remaining.AgrDayLimitRemaining;
                            }
                            else
                            {
                                teminat.Toplam = remaining.AgrPriceLimit.ToString();
                                teminat.Kullanim = remaining.AgrPriceLimitUse;
                                teminat.Kalan = remaining.AgrPriceLimitRemaining;
                            }
                            teminat.Uygulama = AgreementType.HAVUZ_HOLDING.ToString();
                        }
                        else if (remaining.AgrAgreementType == ((int)AgreementType.RECETE_BASINA).ToString())
                        {
                            teminat.TeminatAd = remaining.CoverageName;
                            teminat.Toplam = remaining.AgrPriceLimit.ToString();
                            teminat.Kullanim = remaining.AgrPriceLimitUse;
                            teminat.Kalan = remaining.AgrPriceLimitRemaining;
                            teminat.Uygulama = AgreementType.RECETE_BASINA.ToString();
                        }
                        else if (remaining.AgrAgreementType == ((int)AgreementType.VIZITE_BASINA).ToString())
                        {
                            teminat.TeminatAd = remaining.CoverageName;
                            teminat.Toplam = remaining.AgrPriceLimit.ToString();
                            teminat.Kullanim = remaining.AgrPriceLimitUse;
                            teminat.Kalan = remaining.AgrPriceLimitRemaining;
                            teminat.Uygulama = AgreementType.VIZITE_BASINA.ToString();
                        }
                        else if (remaining.AgrAgreementType == ((int)AgreementType.YILLIK).ToString())
                        {
                            teminat.TeminatAd = remaining.CoverageName;
                            if (remaining.AgrDayLimit == null && remaining.AgrNumberLimit != null)
                            {
                                teminat.Toplam = remaining.AgrNumberLimit.ToString();
                                teminat.Kullanim = remaining.AgrNumberLimitUse;
                                teminat.Kalan = remaining.AgrNumberLimitRemaining;
                            }
                            else if (remaining.AgrDayLimit != null && remaining.AgrNumberLimit == null)
                            {
                                teminat.Toplam = remaining.AgrDayLimit.ToString();
                                teminat.Kullanim = remaining.AgrDayLimitUse;
                                teminat.Kalan = remaining.AgrDayLimitRemaining;
                            }
                            else
                            {
                                teminat.Toplam = remaining.AgrPriceLimit.ToString();
                                teminat.Kullanim = remaining.AgrPriceLimitUse;
                                teminat.Kalan = remaining.AgrPriceLimitRemaining;
                            }
                            teminat.Uygulama = AgreementType.YILLIK.ToString();
                        }
                        teminatList.Add(teminat);
                        i++;
                    }
                    sigortaliKullanım.TeminatListesi = teminatList.ToArray();

                    SigortaliKullanımList.Add(sigortaliKullanım);
                    j++;
                }
            }

            result.KullanımBilgi = SigortaliKullanımList.ToArray();
            result.TpaPoliceNo = (long)insuredList[0].POLICY_ID;
            result.SonucAciklama = "OK";
            result.SonucKod = "1";
            return result;
        }
        #endregion

        public PoliceSorgulaRes PoliceSorgula(string apiCode, PoliceSorgulaReq req)
        {
            PoliceSorgulaRes response = new PoliceSorgulaRes();
            List<string> errorArray = new List<string>();
            var currentMethod = MethodBase.GetCurrentMethod();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Incoming Log
                if (req.PolPK == null)
                {
                    response.SonucAciklama = "POLICE BİLGİ GİRİLMESİ GEREKMEKTEDİR";
                    response.SonucKod = "0";
                    errorArray.Add(response.SonucAciklama);
                }
                else
                {
                    if (!req.PolPK.SirketKod.ToString().IsNumeric())
                    {
                        response.SonucAciklama = "ŞİRKET KOD GİRİLMESİ GEREKMEKTEDİR";
                        response.SonucKod = "0";
                        CreateFailedLog(log, response.ToXML(true), new string[] { response.SonucAciklama });
                        return response;
                    }
                    if (!req.PolPK.PoliceNo.ToString().IsNumeric() || !req.PolPK.YenilemeNo.ToString().IsNumeric())
                    {
                        response.SonucAciklama = "POLİÇE NO VE YENİLEME NO GİRİLMESİ GEREKMEKTEDİR";
                        response.SonucKod = "0";
                        CreateFailedLog(log, response.ToXML(true), new string[] { response.SonucAciklama });
                        return response;
                    }
                }
                #endregion

                #region Auth
                if (ApiCodeIsValid(apiCode, req.PolPK.SirketKod) <= 0)
                {
                    response.SonucAciklama = "Giriş başarısız";
                    response.SonucKod = "0";

                    CreateFailedLog(log, response.ToXML(true), new string[] { response.SonucAciklama });

                    return response;
                }
                log.COMPANY_ID = req.PolPK.SirketKod;
                #endregion

                #region Operation
                V_Policy Vpolicy = new GenericRepository<V_Policy>().FindBy($"POLICY_NUMBER ={req.PolPK.PoliceNo} AND RENEWAL_NO = {req.PolPK.YenilemeNo} AND COMPANY_ID = {req.PolPK.SirketKod}", orderby: "POLICY_ID").FirstOrDefault();
                if (Vpolicy == null)
                {
                    response.SonucAciklama = "POLİÇE BİLGİSİ BULUNAMADI!";
                    response.SonucKod = "0";

                    CreateFailedLog(log, response.ToXML(true), new string[] { response.SonucAciklama });

                    return response;
                }
                log.PolicyId = Vpolicy.POLICY_ID;
                if (req.ZeylNo != null)
                {
                    Endorsement endorsement = new GenericRepository<Endorsement>().FindBy($"POLICY_ID={Vpolicy.POLICY_ID} AND NO={req.ZeylNo}", orderby: "").FirstOrDefault();
                    if (endorsement == null)
                    {
                        response.SonucAciklama = req.ZeylNo + " NOLU ZEYİL BİLGİSİ BULUNAMADI!";
                        response.SonucKod = "0";

                        CreateFailedLog(log, response.ToXML(true), new string[] { response.SonucAciklama });

                        return response;
                    }
                    else
                    {
                        response.SonucListesi = new SonucListesi();
                        response.SonucListesi.ZeylSonuc = new List<ZeylSonuc>();

                        List<V_InsuredEndorsement> insEndors = new GenericRepository<V_InsuredEndorsement>().FindBy($"ENDORSEMENT_ID={endorsement.Id}", orderby: "");
                        Detay detay = new Detay
                        {
                            SonucAciklama = "GÖNDERİM BAŞARILI",
                            SonucKod = "1",
                            TpaPoliceNo = Vpolicy.POLICY_ID,
                            ZeylNo = endorsement.No,
                            ZeylPrim = endorsement.Premium,
                            SagmerNo = Vpolicy.SBM_NO,
                            PolicePrim = Vpolicy.PREMIUM,
                            ZeylSigortaliAdet = insEndors.Count
                        };

                        ZeylSonuc zeylSonuc = new ZeylSonuc
                        {
                            Detay = detay
                        };

                        response.SonucListesi.ZeylSonuc.Add(zeylSonuc);
                    }
                }
                else
                {
                    response.SonucListesi = new SonucListesi();
                    response.SonucListesi.ZeylSonuc = new List<ZeylSonuc>();

                    List<Endorsement> endorsementList = new GenericRepository<Endorsement>().FindBy($"POLICY_ID={Vpolicy.POLICY_ID}", orderby: "");
                    if (endorsementList != null && endorsementList.Count > 0)
                    {
                        var idList = String.Join(",", endorsementList.Select(e => e.Id).ToArray());
                        List<V_InsuredEndorsement> insEndors = new GenericRepository<V_InsuredEndorsement>().FindBy($"ENDORSEMENT_ID IN ({idList})", orderby: "");
                        foreach (var item in endorsementList)
                        {
                            Detay detay = new Detay
                            {
                                SonucAciklama = "GÖNDERİM BAŞARILI",
                                SonucKod = "1",
                                TpaPoliceNo = Vpolicy.POLICY_ID,
                                ZeylNo = item.No,
                                ZeylPrim = item.Premium,
                                SagmerNo = Vpolicy.SBM_NO,
                                PolicePrim = Vpolicy.PREMIUM,
                                ZeylSigortaliAdet = insEndors.Where(i => i.ENDORSEMENT_ID == item.Id).ToList().Count,
                            };

                            ZeylSonuc zeylSonuc = new ZeylSonuc
                            {
                                Detay = detay
                            };

                            response.SonucListesi.ZeylSonuc.Add(zeylSonuc);
                        }
                    }
                }

                #endregion

                #region OK
                response.SonucAciklama = "OK";
                response.SonucKod = "1";
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.SonucKod = "0";
                response.SonucAciklama = ex.Message;

                CreateFailedLog(log, response.ToXML(true), new string[] { ex.Message });
                #endregion
            }
            return response;
        }

        public PoliceIptalRes PoliceIptal(string apiCode, PoliceIptalReq req)
        {
            PoliceIptalRes response = new PoliceIptalRes();
            var currentMethod = MethodBase.GetCurrentMethod();
            List<string> errorArray = new List<string>();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation

                #region Incoming Validation
                if (req.PoliceNo == null)
                {
                    response.SonucAciklama = "POLICE BİLGİ ALANI BOŞ OLAMAZ";
                    response.SonucKod = "0";
                    errorArray.Add(response.SonucAciklama);
                }
                else
                {
                    if (!req.PoliceNo.SirketKod.ToString().IsNumeric())
                    {
                        response.SonucAciklama = "ŞİRKET KOD BOŞ OLAMAZ";
                        response.SonucKod = "0";
                        errorArray.Add(response.SonucAciklama);
                    }
                    if (!req.PoliceNo.PoliceNo.ToString().IsNumeric() || !req.PoliceNo.YenilemeNo.ToString().IsNumeric())
                    {
                        response.SonucAciklama = "POLİÇE NO VE YENİLEME NO BOŞ OLAMAZ";
                        response.SonucKod = "0";
                        errorArray.Add(response.SonucAciklama);
                    }
                }
                if (response.SonucKod == "0")
                {
                    response.ValidationErrors = errorArray.ToArray();
                    response.SonucAciklama = "ERROR";
                    response.SonucKod = "0";
                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                    return response;
                }
                #endregion

                #region Auth
                if (ApiCodeIsValid(apiCode, req.PoliceNo.SirketKod) <= 0)
                {
                    response.SonucAciklama = "Giriş başarısız";
                    response.ValidationErrors = new string[] { response.SonucAciklama };
                    response.SonucKod = "0";

                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                    return response;
                }
                log.COMPANY_ID = req.PoliceNo.SirketKod;
                #endregion

                #region Zorunlu Alan
                if (req.ZeylNo.IsInt64() && req.PoliceNo.SirketKod != 3)
                {
                    var endorsementList = new GenericRepository<V_PolicyEndorsement>().FindBy(conditions: $"POLICY_NUMBER={req.PoliceNo.PoliceNo}", orderby: "ENDORSEMENT_NO desc").FirstOrDefault();
                    int nextEndorsementNo = Convert.ToInt32(endorsementList.ENDORSEMENT_NO) + 1;
                    if (Convert.ToInt32(req.ZeylNo) != nextEndorsementNo)
                    {
                        response.SonucAciklama = "Zeyl No Son Zeyl Numarası ile ardışık ilerlemesi gerekir. Son Zeyl NO : " + endorsementList.ENDORSEMENT_NO;
                        response.SonucKod = "0";
                        errorArray.Add(response.SonucAciklama);
                    }
                }


                if (!req.ZeylTanzimTarih.ToString().IsDateTime())
                {
                    response.SonucAciklama = "TANZİM TARİHİ GİRİLMESİ GEREKMEKTEDİR";
                    response.SonucKod = "0";
                    errorArray.Add(response.SonucAciklama);
                }
                else
                {
                    if (req.ZeylTanzimTarih == DateTime.MinValue)
                    {
                        response.SonucAciklama = "TANZİM TARİHİ GİRİLMESİ GEREKMEKTEDİR";
                        response.SonucKod = "0";
                        errorArray.Add(response.SonucAciklama);
                    }
                }
                if (!req.ZeylBaslamaTarih.ToString().IsDateTime())
                {
                    response.SonucAciklama = "BAŞLANGIÇ TARİHİ GİRİLMESİ GEREKMEKTEDİR";
                    response.SonucKod = "0";
                    errorArray.Add(response.SonucAciklama);
                }
                else
                {
                    if (req.ZeylBaslamaTarih == DateTime.MinValue)
                    {
                        response.SonucAciklama = "BAŞLAMA TARİHİ GİRİLMESİ GEREKMEKTEDİR";
                        response.SonucKod = "0";
                        errorArray.Add(response.SonucAciklama);
                    }
                }
                if (!req.IptalPrimi.ToString().IsNumeric())
                {
                    response.SonucAciklama = "iPTAL PİRİMİ GİRMELİSİNİZ!";
                    response.SonucKod = "0";
                    errorArray.Add(response.SonucAciklama);
                }

                if (req.IptalPrimi < 0)
                {
                    req.IptalPrimi = -1 * req.IptalPrimi;
                }

                if (response.SonucKod == "0")
                {
                    response.SonucAciklama = "ERROR";
                    response.ValidationErrors = errorArray.ToArray();
                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                    return response;
                }
                #endregion

                #region Data Control
                V_Policy Vpolicy = new GenericRepository<V_Policy>().FindBy($"POLICY_NUMBER ={req.PoliceNo.PoliceNo} AND RENEWAL_NO = {req.PoliceNo.YenilemeNo} AND COMPANY_ID = {req.PoliceNo.SirketKod}", orderby: "POLICY_ID").FirstOrDefault();
                if (Vpolicy == null)
                {
                    #region Hata
                    response.SonucAciklama = "POLİÇE BİLGİSİ BULUNAMADI!";
                    response.ValidationErrors = new string[] { response.SonucAciklama };
                    response.SonucKod = "0";
                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                    #endregion
                    return response;
                }
                else
                {
                    if (req.ZeylBaslamaTarih > Vpolicy.POLICY_END_DATE)
                    {
                        response.SonucAciklama = "ZEYL BAŞLAMA TARİHİ, POLİÇE BİTİŞ TARİHİNDEN BÜYÜK OLAMAZ";
                        response.SonucKod = "0";
                        response.ValidationErrors = new string[] { response.SonucAciklama };
                        CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                        return response;
                    }
                    else
                    {
                        req.ZeylTanzimTarih = DateTime.Parse(((DateTime)req.ZeylTanzimTarih).ToShortDateString()).AddHours(12);
                        req.ZeylBaslamaTarih = DateTime.Parse(((DateTime)req.ZeylBaslamaTarih).ToShortDateString()).AddHours(12);
                    }
                }
                if (Vpolicy.STATUS == ((int)PolicyStatus.TANZIMLI).ToString() || Vpolicy.STATUS == ((int)PolicyStatus.TEKLIF).ToString()) { }
                else
                {
                    #region Hata
                    response.SonucAciklama = "İPTAL ZEYİLİ DAHA ÖNCE GÖNDERİLMİŞTİR. TEKRAR GÖNDERİM YAPILAMAZ.";
                    response.SonucKod = "0";
                    response.ValidationErrors = new string[] { response.SonucAciklama };
                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                    #endregion
                    return response;
                }
                if (Vpolicy.PREMIUM < req.IptalPrimi)
                {
                    #region Hata
                    response.SonucAciklama = "İPTAL PRİMİ POLİÇE PRİMİNDEN YÜKSEK OLAMAZ!";
                    response.ValidationErrors = new string[] { response.SonucAciklama };
                    response.SonucKod = "0";
                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                    #endregion
                    return response;
                }
                log.PolicyId = Vpolicy.POLICY_ID;

                Endorsement endorsement = new GenericRepository<Endorsement>().FindBy($"POLICY_ID={Vpolicy.POLICY_ID}", orderby: "ID desc").FirstOrDefault();
                if (endorsement.Status == ((int)Status.PASIF).ToString())
                {
                    #region Hata
                    response.SonucAciklama = "POLİÇEDE TANZİMSİZ ZEYL BULUNMAKTA!";
                    response.ValidationErrors = new string[] { response.SonucAciklama };
                    response.SonucKod = "0";

                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                    #endregion
                    return response;
                }
                #endregion

                #endregion

                #region Operation
                Endorsement cancelEndorsement = new Endorsement
                {
                    Id = 0,
                    Premium = -1 * req.IptalPrimi,
                    DateOfIssue = req.ZeylTanzimTarih,
                    Status = ((int)Status.AKTIF).ToString(),
                    Type = ((int)EndorsementType.IPTAL).ToString(),
                    StartDate = req.ZeylBaslamaTarih,
                    No = endorsement.No + 1,
                    PolicyId = Vpolicy.POLICY_ID,
                    Category = ((int)EndorsementCategory.POLICE).ToString()
                };

                SpResponse spResponseEndo = new GenericRepository<Endorsement>().Insert(cancelEndorsement);
                if (spResponseEndo.Code != "100")
                {
                    #region Hata
                    response.SonucAciklama = spResponseEndo.Message;
                    response.ValidationErrors = new string[] { response.SonucAciklama };
                    response.SonucKod = "0";
                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);

                    #endregion
                    return response;
                }

                EndorsementPolicy endorsementPolicy = new EndorsementPolicy
                {
                    EndorsementId = spResponseEndo.PkId,
                    PolicyId = Vpolicy.POLICY_ID
                };
                SpResponse spResponseEndoPol = new GenericRepository<EndorsementPolicy>().Insert(endorsementPolicy, this.Token);
                if (spResponseEndoPol.Code != "100")
                {
                    #region Hata
                    response.SonucAciklama = spResponseEndoPol.Message;
                    response.ValidationErrors = new string[] { response.SonucAciklama };
                    response.SonucKod = "0";
                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);

                    #endregion
                    return response;
                }

                long? LastEndorsementId = 0;
                decimal? premium = 0;
                Policy policy = new GenericRepository<Policy>().FindById(Vpolicy.POLICY_ID);
                LastEndorsementId = policy.LastEndorsementId;
                premium = policy.Premium;

                policy.Status = ((int)PolicyStatus.IPTAL).ToString();
                policy.Premium = policy.Premium - req.IptalPrimi;
                policy.LastEndorsementId = spResponseEndo.PkId;

                SpResponse spResponsePolicy = new GenericRepository<Policy>().Insert(policy);
                if (spResponsePolicy.Code != "100")
                {
                    #region Hata
                    response.SonucAciklama = spResponsePolicy.Message;
                    response.ValidationErrors = new string[] { response.SonucAciklama };
                    response.SonucKod = "0";
                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                    #endregion
                    return response;
                }

                if (req.PoliceNo.SirketKod == 10)
                {
                    bool isSgmOk = false;
                    SgmServiceClient sgmServiceClient = new SgmServiceClient();
                    SbmPolicyCancelReq sbmInsuredExitReq = new SbmPolicyCancelReq
                    {
                        PolicyId = policy.Id,
                        EndorsementId = spResponseEndo.PkId
                    };
                    var sbmResponse = sgmServiceClient.SbmPolicyCancelEndorsement(sbmInsuredExitReq, SagmerServiceType.KONTROL);
                    if (sbmResponse.ResultCode != 1)
                    {
                        RollBackProxySrv(policyId: Vpolicy.POLICY_ID, endorsementId: spResponseEndo.PkId, policyPremium: premium, lastEndorsementId: LastEndorsementId, policyIsActive: true);

                        List<string> SbmErrorList = new List<string>();
                        foreach (var item in sbmResponse.FailedErrors)
                        {
                            SbmErrorList.Add(item.code + "  " + item.message);
                        }
                        response.SonucKod = "0";
                        response.SonucAciklama = "SAGMER HATA, ValidationErrors Kontrol Ediniz.";
                        response.ValidationErrors = SbmErrorList.ToArray();
                        return response;
                    }
                    else
                    {
                        isSgmOk = true;
                    }

                    if (isSgmOk)
                    {
                        sbmResponse = sgmServiceClient.SbmPolicyCancelEndorsement(sbmInsuredExitReq, SagmerServiceType.CANLI);
                        if (sbmResponse.ResultCode != 1)
                        {
                            RollBackProxySrv(policyId: Vpolicy.POLICY_ID, endorsementId: spResponseEndo.PkId, policyPremium: premium, lastEndorsementId: LastEndorsementId, policyIsActive: true);

                            List<string> SbmErrorList = new List<string>();
                            foreach (var item in sbmResponse.FailedErrors)
                            {
                                SbmErrorList.Add(item.code + "  " + item.message);
                            }
                            response.SonucKod = "0";
                            response.SonucAciklama = "SAGMER HATA, ValidationErrors Kontrol Ediniz.";
                            response.ValidationErrors = SbmErrorList.ToArray();
                            return response;
                        }
                    }
                }
                #endregion

                #region OK
                response.SonucAciklama = "OK";
                response.SonucKod = "1";
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.SonucKod = "0";
                response.SonucAciklama = "HATA - ValidationErrors Listesini Kontrol Ediniz";
                response.ValidationErrors = new string[] { ex.Message };

                CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                #endregion
            }
            return response;

        }

        public PaketOdemeBildirimRes PaketOdemeBildirim(string apiCode, PaketOdemeBildirimReq req)
        {
            PaketOdemeBildirimRes result = new PaketOdemeBildirimRes();

            var currentMethod = MethodBase.GetCurrentMethod();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Auth
                var CompanyId = ApiCodeIsValidParmeter(apiCode);
                if (CompanyId <= 0)
                {
                    #region ErrorLog
                    log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                    log.Type = ((int)LogType.FAILED).ToString();
                    result.SonucAciklama = "Giriş başarısız";
                    result.ValidationErrors = new string[] { result.SonucAciklama };
                    result.SonucKod = "0";
                    log.ResponseStatusDesc = result.SonucAciklama;
                    log.Response = result.ToXML(true);
                    IntegrationLogHelper.AddToLog(log);
                    #endregion
                    return result;
                }
                log.COMPANY_ID = CompanyId;
                #endregion

                if (!req.Paket_No.ToString().IsInt64())
                {
                    result.SonucAciklama = "PAKET NO GİRMELİSİNİZ!";
                    result.SonucKod = "0";
                    result.ValidationErrors = new string[] { result.SonucAciklama };
                    #region ErrorLog
                    log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                    log.Type = ((int)LogType.FAILED).ToString();
                    log.ResponseStatusDesc = result.SonucAciklama;
                    log.Response = result.ToXML(true);
                    IntegrationLogHelper.AddToLog(log);
                    #endregion
                    return result;
                }

                Payroll payroll = new GenericRepository<Payroll>().FindBy($" (ID = {req.Paket_No} OR OLD_PAYROLL_ID={req.Paket_No}) and STATUS != '{((int)PayrollStatus.Onay_Bekler).ToString()}' AND COMPANY_ID={CompanyId}", fetchDeletedRows: true).FirstOrDefault();
                if (payroll == null || payroll.Id < 1)
                {
                    #region ErrorLog
                    log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                    log.Type = ((int)LogType.FAILED).ToString();
                    log.ResponseStatusCode = "500";
                    log.ResponseStatusDesc = "Paket Bilgisi Bulunamadı!";

                    #endregion
                    result.SonucAciklama = log.ResponseStatusDesc;
                    result.ValidationErrors = new string[] { result.SonucAciklama };
                    result.SonucKod = "0";
                    log.Response = result.ToXML(true);
                    IntegrationLogHelper.AddToLog(log);
                    return result;
                }

                List<Claim> Claims = new GenericRepository<Claim>().FindBy($"PAYROLL_ID = {payroll.Id} AND COMPANY_ID={CompanyId}");
                if (Claims.Count > 0)
                {
                    List<string> errors = new List<string>();

                    foreach (Claim _claim in Claims)
                    {
                        if (req.Odeme_Tarihi == null)
                        {
                            if (_claim.Status != ((int)ClaimStatus.ODENDI).ToString())
                                errors.Add("Ödeme iptali Yapılamadı. Hasar Durumu Uygun Değil. Hasar No:" + _claim.Id);
                            else
                            {
                                var claimPayment = new GenericRepository<ClaimPayment>().FindBy($"CLAIM_ID={_claim.Id}").FirstOrDefault();
                                if (claimPayment == null) errors.Add("Ödeme iptali Yapılamadı. Ödeme Bilgisi Bulunamadı.");
                                else
                                {
                                    claimPayment.DueDate = null;
                                    var spResponseClaimPayment = new GenericRepository<ClaimPayment>().UpdateForAll(claimPayment);
                                    if (spResponseClaimPayment.Code != "100") errors.Add(spResponseClaimPayment.Message);
                                    else
                                    {
                                        _claim.Status = ((int)ClaimStatus.ODENECEK).ToString();
                                        SpResponse responseClaim = new GenericRepository<Claim>().Insert(_claim, this.Token);
                                        if (responseClaim.Code != "100") errors.Add(responseClaim.Message);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (_claim.Status != ((int)ClaimStatus.ODENECEK).ToString())
                                errors.Add("Ödeme Kaydı Yapılamadı. Hasar Durumu Uygun Değil. Hasar No:" + _claim.Id);
                            else
                            {
                                var claimPayment = new GenericRepository<ClaimPayment>().FindBy($"CLAIM_ID={_claim.Id}").FirstOrDefault();
                                if (claimPayment != null)
                                {
                                    claimPayment.PaymentDate = req.Odeme_Tarihi;
                                    SpResponse spResponseClaimPayment = new GenericRepository<ClaimPayment>().Insert(claimPayment, this.Token);
                                    if (spResponseClaimPayment.Code != "100") errors.Add(spResponseClaimPayment.Message);
                                    else
                                    {
                                        _claim.Status = ((int)ClaimStatus.ODENDI).ToString();
                                        SpResponse responseClaim = new GenericRepository<Claim>().Insert(_claim, this.Token);
                                        if (responseClaim.Code != "100") errors.Add(responseClaim.Message);
                                    }
                                }
                                else
                                {
                                    claimPayment = new ClaimPayment
                                    {
                                        ClaimId = _claim.Id,
                                        PaymentDate = req.Odeme_Tarihi,
                                        PaymentType = ((int)PaymentType.DIGER).ToString(),
                                        Status = ((int)Status.AKTIF).ToString(),
                                    };
                                    SpResponse spResponseClaimPayment = new GenericRepository<ClaimPayment>().Insert(claimPayment, this.Token);
                                    if (spResponseClaimPayment.Code != "100") errors.Add(spResponseClaimPayment.Message);
                                    else
                                    {
                                        _claim.Status = ((int)ClaimStatus.ODENDI).ToString();
                                        SpResponse responseClaim = new GenericRepository<Claim>().Insert(_claim, this.Token);
                                        if (responseClaim.Code != "100") errors.Add(responseClaim.Message);
                                    }
                                }
                            }
                        }
                    }
                    payroll.PaymentDate = req.Odeme_Tarihi;
                    payroll.Status = ((int)PayrollStatus.Odendi).ToString();
                    SpResponse spResponsePayroll = new GenericRepository<Payroll>().UpdateForAll(payroll);
                    if (errors.Count >= Claims.Count)
                    {
                        #region ErrorLog

                        result.SonucKod = "0";
                        result.SonucAciklama = "HATA";
                        result.ValidationErrors = errors.ToArray();

                        log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                        log.Type = ((int)LogType.FAILED).ToString();
                        result.SonucAciklama = "HATA";
                        result.ValidationErrors = errors.ToArray();
                        result.SonucKod = "0";
                        log.ResponseStatusDesc = result.SonucAciklama;
                        log.Response = result.ToXML(true);
                        log.PayrollId = payroll.Id;
                        IntegrationLogHelper.AddToLog(log);
                        #endregion
                        return result;
                    }
                    else
                    {
                        result.SonucKod = "1";
                        result.SonucAciklama = "işlem Başarıyla Sonuçlandı.";
                        result.ValidationErrors = errors.ToArray();
                        #region Completed Log
                        log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                        log.Type = ((int)LogType.COMPLETED).ToString();
                        log.ResponseStatusCode = "200";
                        log.ResponseStatusDesc = "OK";
                        log.Response = result.ToXML(true);
                        log.PayrollId = payroll.Id;
                        IntegrationLogHelper.AddToLog(log);
                        #endregion

                        return result;
                    }
                }
                else
                {
                    #region ErrorLog
                    log.Request = req.ToXML(true);
                    log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                    log.Type = ((int)LogType.FAILED).ToString();
                    log.ResponseStatusCode = "500";
                    log.ResponseStatusDesc = "HASAR BULUNAMADI";
                    result.SonucAciklama = log.ResponseStatusDesc;
                    result.ValidationErrors = new string[] { result.SonucAciklama };
                    result.SonucKod = "0";
                    log.Response = result.ToXML(true);
                    IntegrationLogHelper.AddToLog(log);
                    #endregion
                    return result;
                }
            }
            catch (Exception ex)
            {
                log.Request = req.ToXML(true);
                #region ErrorLog
                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                log.Type = ((int)LogType.FAILED).ToString();
                log.ResponseStatusCode = "500";
                log.ResponseStatusDesc = ex.ToString();
                log.Response = result.ToXML(true);
                IntegrationLogHelper.AddToLog(log);
                #endregion
                result.SonucAciklama = ex.Message;
                result.SonucKod = "0";
                return result;
            }
        }

        public AcenteTransferRes AcenteTransfer(string apiCode, AcenteTransferReq req)
        {
            AcenteTransferRes response = new AcenteTransferRes();
            var currentMethod = MethodBase.GetCurrentMethod();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Incoming Log
                if (!req.AcenteListesi[0].SirketKod.IsNumeric())
                {
                    response.SonucAciklama = "ŞİRKET KOD GİRİLMESİ GEREKMEKTEDİR";
                    response.SonucKod = "0";
                    response.ValidationErrors = new string[] { response.SonucAciklama };
                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);

                    return response;
                }

                if (req.AcenteListesi == null || req.AcenteListesi.Count < 1)
                {
                    response.SonucAciklama = "En az Bir Acente Bilgisi Gönderilmeli!";
                    response.ValidationErrors = new string[] { response.SonucAciklama };
                    response.SonucKod = "0";

                    CreateFailedLog(log, response.ToXML(true), new string[] { response.SonucAciklama });

                    return response;
                }
                #endregion

                #region Auth
                if (ApiCodeIsValid(apiCode, int.Parse(req.AcenteListesi[0].SirketKod)) <= 0)
                {
                    response.SonucAciklama = "Giriş başarısız";
                    response.SonucKod = "0";
                    response.ValidationErrors = new string[] { response.SonucAciklama };
                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);

                    return response;
                }
                #endregion

                #region Validation
                List<string> errors = new List<string>();
                int s1 = 0;
                foreach (AcenteBilgi agency in req.AcenteListesi)
                {
                    s1++;
                    response.SonucKod = "1";
                    if (string.IsNullOrEmpty(agency.AcenteKod))
                    {
                        response.SonucAciklama = $"ACENTE KOD GİRİLMESİ GEREKMEKTEDİR. Sıra No: {s1.ToString()}";
                        response.SonucKod = "0";
                        errors.Add(response.SonucAciklama);
                    }
                    if (string.IsNullOrEmpty(agency.AcenteUnvan))
                    {
                        response.SonucAciklama = $"ACENTE UNVAN GİRİLMESİ GEREKMEKTEDİR. Sıra No: {s1.ToString()}";
                        response.SonucKod = "0";
                        errors.Add(response.SonucAciklama);
                    }
                    if (string.IsNullOrEmpty(agency.AcenteTip))
                    {
                        response.SonucAciklama = $"ACENTE TİPİ GİRİLMESİ GEREKMEKTEDİR. Sıra No: {s1.ToString()}";
                        response.SonucKod = "0";
                        errors.Add(response.SonucAciklama);
                    }
                    if (LookupHelper.GetLookupTextByOrdinal(LookupTypes.Agency, agency.AcenteTip).IsNull())
                    {
                        agency.AcenteTip = ((int)AgencyType.ACENTE).ToString();
                    }
                    if (agency.Adres == null)
                    {
                        response.SonucAciklama = $"ACENTE ADRES BİLGİSİ GİRİLMESİ GEREKMEKTEDİR. Sıra No: {s1.ToString()}";
                        response.SonucKod = "0";
                        errors.Add(response.SonucAciklama);
                    }
                    if (string.IsNullOrEmpty(agency.Adres.IlKod))
                    {
                        response.SonucAciklama = $"ACENTE IL KODU BİLGİSİ GİRİLMESİ GEREKMEKTEDİR. Sıra No: {s1.ToString()}";
                        response.SonucKod = "0";
                        errors.Add(response.SonucAciklama);
                    }
                    if (response.SonucKod == "0")
                    {
                        response.SonucAciklama = $"ACENTE KAYDEDİLEMEDİ. Sıra No: {s1.ToString()}";
                        response.SonucKod = "0";
                        errors.Add(response.SonucAciklama);
                    }
                    else
                    {
                        IService<AgencyDto> agencyService = new AgencyService();
                        AgencyDto agencyDto = new AgencyDto
                        {
                            CompanyId = long.Parse(agency.SirketKod),
                            ActivePasive = agency.KapanisTarih != null ? ((int)ProteinEnums.Status.PASIF).ToString() : ((int)ProteinEnums.Status.AKTIF).ToString(),
                            Address = agency.Adres?.Mahalle + " " + agency.Adres?.Sokak + " " + agency.Adres?.PostaKod + " " + agency.Adres?.BinaAd + " " + agency.Adres?.BinaNo + " " + agency.Adres?.DaireNo + " " + agency.Adres?.AdresTarif,
                            AgencyTitle = agency.AcenteUnvan,
                            RegionCode = agency.Adres?.UlkeKod,
                            ZipCode = agency.Adres?.PostaKod,
                            No = agency.AcenteKod,
                            PlateNo = agency.LevhaNo,
                            EstablishmentDate = agency.AcilisTarih,
                            CloseDownDate = agency.KapanisTarih,
                            PhoneNo = agency.Telefon,
                            ContactFirstName = agency.YetkiliAd,
                            TaxNumber = agency.TicaretSicilNo,
                            Type = agency.AcenteTip,
                            ContactPhone = agency.YetkiliCepTel,
                            MobileNo = agency.YetkiliCepTel,
                            ContactLastName = agency.YetkiliSoyad,
                            City = agency.Adres?.IlKod,
                            County = agency.Adres?.IlceKod,
                            RegionName = agency.AcenteBolgeAd,
                        };
                        ServiceResponse serviceResponse = agencyService.ServiceInsert(agencyDto, this.Token);


                        if (serviceResponse.Code == "999")
                            errors.Add(serviceResponse.Message);
                    }
                }
                if (errors.Count >= req.AcenteListesi.Count)
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "Hata";
                    response.ValidationErrors = errors.ToArray();

                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);

                    return response;
                }
                else if (errors.Count == 0)
                {
                    #region OK
                    response.SonucAciklama = "OK";
                    response.SonucKod = "1";
                    response.ValidationErrors = errors.ToArray();

                    CreateOKLog(log, response.ToXML(true));
                    #endregion
                    return response;

                }
                else
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "Bazı Acenteler Aktarılamadı. Lütfen ValidationErrors'u kontrol ediniz.";
                    response.ValidationErrors = errors.ToArray();

                    CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);

                    return response;
                }
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.SonucKod = "0";
                response.SonucAciklama = "HATA - ValidationErrors Listesini Kontrol Ediniz";
                response.ValidationErrors = new string[] { ex.Message };

                CreateFailedLog(log, response.ToXML(true), response.ValidationErrors);
                #endregion
            }
            return response;
        }

        #region Mutabakat
        public ProvizyonMutabakatRes ProvizyonMutabakat(string apiCode, ProvizyonMutabakatReq req, string CorrelationId = null)
        {
            // incoming log için. Bütün servislerde sabit.
            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog
            {
                CorrelationId = CorrelationId != null ? CorrelationId : "",
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                COMPANY_ID = req.SirketKod,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);

            ProvizyonMutabakatRes response = new ProvizyonMutabakatRes();


            try
            {
                #region Auth
                var CompanyId = ApiCodeIsValid(apiCode, req.SirketKod);
                if (CompanyId <= 0)
                {
                    #region ErrorLog
                    response.SonucKod = "0";
                    response.SonucAciklama = "Giriş Başarısız! ApiCode'u Kontrol Ediniz.";
                    CreateFailedLog(log, response.ToXML(true), new string[] { response.SonucAciklama });
                    #endregion
                    return response;
                }
                #endregion

                #region Validation
                if (req.TransferTarihiBaslangic > req.TransferTarihiBitis)
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "Başlangıç tarihi bitiş tarihinden büyük olamaz.";
                    CreateFailedLog(log, response.ToXML(true), desc: new string[] { "Başlangıç tarihi bitiş tarihinden büyük olamaz." });
                    return response;
                }
                #endregion

                List<V_Claim> ClaimList = new GenericRepository<V_Claim>().FindBy(conditions: $"TO_DATE(Claim_Date,'DD.MM.YYYY') <= TO_DATE(TO_DATE('{req.TransferTarihiBitis.ToString("yyyy-MM-dd")}', 'YYYY-MM-DD'), 'DD.MM.YYYY') and TO_DATE(Claim_Date,'DD.MM.YYYY') >= TO_DATE(TO_DATE('{req.TransferTarihiBaslangic.ToString("yyyy-MM-dd")}', 'YYYY-MM-DD'), 'DD.MM.YYYY') and company_id={CompanyId}", orderby: "CLAIM_ID");

                if (ClaimList != null && ClaimList.Count() > 0)
                {
                    response.ToplamAdet = ClaimList.Count();
                    response.ToplamSirketTutar = (decimal)ClaimList.Where(x => x.PAID != null).Select(x => x.PAID).Sum();
                    List<int> statusList = new List<int>();
                    List<TazminatDurum> TazminatDurumList = new List<TazminatDurum>();
                    foreach (var claim in ClaimList)
                    {
                        if (!statusList.Contains(Convert.ToInt32(claim.STATUS)))
                        {
                            TazminatDurum tazminatDurum = new TazminatDurum
                            {
                                Durum = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, claim.STATUS),
                                Adet = ClaimList.Where(x => x.STATUS == claim.STATUS).ToList().Count(),
                                SirketTutar = (decimal)ClaimList.Where(x => x.STATUS == claim.STATUS && x.PAID != null).Select(x => x.PAID).Sum()
                            };
                            TazminatDurumList.Add(tazminatDurum);
                            statusList.Add(Convert.ToInt32(claim.STATUS));
                        }
                    }
                    response.TazminatDurumListesi = TazminatDurumList;
                }
                else
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "İki Tarih Arasında Kayıt Bulunamadı.";
                    return response;
                }

                #region OK
                response.SonucAciklama = "OK";
                response.SonucKod = "1";
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                response.SonucAciklama = "Hata oluştu tekrar deneyiniz.";
                response.SonucKod = "0";
                CreateFailedLog(log, response.ToXML(true), new string[] { ex.Message });
            }
            return response;
        }

        public ProvizyonMutabakatDetayRes ProvizyonMutabakatDetay(string apiCode, ProvizyonMutabakatDetayReq req, string CorrelationId = null)
        {
            // incoming log için. Bütün servislerde sabit.
            var currentMethod = MethodBase.GetCurrentMethod();
            IntegrationLog log = new IntegrationLog
            {
                CorrelationId = CorrelationId != null ? CorrelationId : "",
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                COMPANY_ID = req.SirketKod,
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            ProvizyonMutabakatDetayRes response = new ProvizyonMutabakatDetayRes();
            try
            {
                #region Auth
                var CompanyId = ApiCodeIsValid(apiCode, req.SirketKod);
                if (CompanyId <= 0)
                {
                    #region ErrorLog
                    response.SonucKod = "0";
                    response.SonucAciklama = "Giriş Başarısız! ApiCode'u Kontrol Ediniz.";
                    CreateFailedLog(log, response.ToXML(true), new string[] { response.SonucAciklama });
                    #endregion
                    return response;
                }
                #endregion

                #region Validation
                if (req.TransferTarihiBaslangic > req.TransferTarihiBitis)
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "Başlangıç tarihi bitiş tarihinden büyük olamaz.";
                    CreateFailedLog(log, response.ToXML(true), desc: new string[] { "Başlangıç tarihi bitiş tarihinden büyük olamaz." });
                    return response;
                }
                if (LookupHelper.GetLookupByOrdinal(LookupTypes.ClaimStatus, req.Durum) == null)
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "Hasar Durum Bilgisi Okunamadı. ";
                    CreateFailedLog(log, response.ToXML(true), desc: new string[] { response.SonucAciklama });
                    return response;

                }
                #endregion
                List<V_Claim> ClaimList = new GenericRepository<V_Claim>().FindBy(conditions: $"TO_DATE(CLAIM_DATE,'DD.MM.YYYY') <= TO_DATE(TO_DATE('{req.TransferTarihiBitis.ToString("yyyy-MM-dd")}', 'YYYY-MM-DD'), 'DD.MM.YYYY') and TO_DATE(Claim_Date, 'DD.MM.YYYY') >= TO_DATE(TO_DATE('{req.TransferTarihiBaslangic.ToString("yyyy-MM-dd")}', 'YYYY-MM-DD'), 'DD.MM.YYYY') and COMPANY_ID = {CompanyId} and status = '{req.Durum}'", orderby: "CLAIM_ID");

                if (ClaimList != null && ClaimList.Count() > 0)
                {
                    TazminatDurum tazminatDurumList = new TazminatDurum();
                    tazminatDurumList.Durum = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, req.Durum);
                    tazminatDurumList.Adet = ClaimList.Count();
                    tazminatDurumList.SirketTutar = (decimal)(ClaimList.Where(x => x.PAID != null).Select(x => x.PAID).Sum());

                    response.TazminatDurum = tazminatDurumList;

                    string claimIdList = string.Join(", ", ClaimList.Select(x => x.CLAIM_ID).ToArray());

                    List<V_ClaimCoverage> claimCoveregeList = new GenericRepository<V_ClaimCoverage>().FindBy(conditions: $"CLAIM_ID IN ({claimIdList})", orderby: "CLAIM_ID");
                    response.TazminatListesi = new List<Tazminat>();

                    foreach (var claim in ClaimList)
                    {
                        Tazminat tazminat = new Tazminat();

                        tazminat.No = claim.CLAIM_ID;
                        tazminat.SirketTutar = (decimal)claim.PAID;

                        response.TazminatListesi.Add(tazminat);
                    }

                    #region OK
                    response.SonucAciklama = "OK";
                    response.SonucKod = "1";
                    CreateOKLog(log, response.ToXML(true));
                    #endregion

                }
                else
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "İki Tarih Arasında Kayıt Bulunamadı.";
                    return response;
                }

            }
            catch (Exception ex)
            {
                response.SonucAciklama = "Hata oluştu tekrar deneyiniz.";
                response.SonucKod = "0";
                CreateFailedLog(log, response.ToXML(true), new string[] { ex.Message });
            }
            return response;
        }

        public IcmalMutabakatRes TazminatPaketMutabakat(string apiCode, IcmalMutabakatReq req, string CorrelationId = null)
        {
            // incoming log için. Bütün servislerde sabit.
            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog
            {
                CorrelationId = CorrelationId != null ? CorrelationId : "",
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                COMPANY_ID = req.SirketKod,
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);

            IcmalMutabakatRes response = new IcmalMutabakatRes();
            try
            {
                #region Auth
                var CompanyId = ApiCodeIsValid(apiCode, req.SirketKod);
                if (CompanyId <= 0)
                {
                    #region ErrorLog
                    response.SonucKod = "0";
                    response.SonucAciklama = "Giriş Başarısız! ApiCode'u Kontrol Ediniz.";
                    CreateFailedLog(log, response.ToXML(true), new string[] { response.SonucAciklama });
                    #endregion
                    return response;
                }
                #endregion


                #region Validation
                if (req.TransferTarihiBaslangic > req.TransferTarihiBitis)
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "Başlangıç tarihi bitiş tarihinden büyük olamaz.";
                    CreateFailedLog(log, response.ToXML(true), desc: new string[] { "Başlangıç tarihi bitiş tarihinden büyük olamaz." });
                    return response;
                }

                #endregion

                List<IntegrationLog> integrationLogList = new GenericRepository<IntegrationLog>().FindBy(conditions: $"ws_method = 'PayrollTransfer' and COMPANY_ID= {CompanyId} and TO_DATE(WS_REQUEST_DATE,'DD.MM.YYYY') >= TO_DATE(TO_DATE('{req.TransferTarihiBaslangic.ToString("yyyy-MM-dd")}', 'YYYY-MM-DD'), 'DD.MM.YYYY') and TO_DATE(WS_REQUEST_DATE,'DD.MM.YYYY') <= TO_DATE(TO_DATE('{req.TransferTarihiBitis.ToString("yyyy-MM-dd")}', 'YYYY-MM-DD'), 'DD.MM.YYYY') AND PAYROLL_ID IS NOT NULL", orderby: "ID", fetchHistoricRows: true);
                if (integrationLogList.Count() > 0)
                {
                    string PayrollIdList = string.Join(", ", integrationLogList.Select(x => x.PayrollId).ToArray());

                    List<V_Payroll> PayrollList = new GenericRepository<V_Payroll>().FindBy(conditions: $"PAYROLL_ID IN ({PayrollIdList}) AND STATUS!='{((int)PayrollStatus.Onay_Bekler).ToString()}'", orderby: "PAYROLL_ID", fetchDeletedRows: true);




                    if (PayrollList.Count() > 0 && PayrollList != null)
                    {
                        response.ToplamIcmalAdedi = PayrollList.Count();

                        response.ToplamIcmalTutari = (decimal)PayrollList.Where(p => p.TOTAL_AMOUNT != null).Select(p => p.TOTAL_AMOUNT).Sum();

                        List<IcmalDetay> zarfMutabakatList = new List<IcmalDetay>();
                        foreach (var payroll in PayrollList)
                        {
                            IcmalDetay zarfMutabakat = new IcmalDetay();

                            // Şimdilik payroll date kullandık ama termin tarihi alanının eklenmesi gerekmektedir.
                            response.ToplamTazminatAdedi += Convert.ToInt32(payroll.TOTAL_CLAIM);
                            zarfMutabakat.SirketTazminatPaketNo = payroll.COMPANY_PAYROLL_NO;
                            zarfMutabakat.KurumTazminatPaketNo = payroll.EXT_PROVIDER_NO;
                            zarfMutabakat.ToplamProvizyonAdet = Convert.ToInt32(payroll.TOTAL_CLAIM);
                            zarfMutabakat.TazminatPaketNo = payroll.PAYROLL_ID.ToString();
                            zarfMutabakat.ToplamFaturaTutar = payroll.TOTAL_AMOUNT != null ? (decimal)payroll.TOTAL_AMOUNT : 0;
                            zarfMutabakat.OdemeTarihi = payroll.PAYMENT_DATE != null ? ((DateTime)payroll.PAYMENT_DATE).ToString("yyyy-MM-dd") : null;
                            zarfMutabakat.IcmalDurumu = LookupHelper.GetLookupTextByOrdinal(LookupTypes.PayrollStatus, payroll.STATUS);
                            zarfMutabakatList.Add(zarfMutabakat);
                        }
                        response.IcmalListesi = zarfMutabakatList;
                    }
                    else
                    {
                        response.SonucKod = "0";
                        response.SonucAciklama = "İki Tarih Arasında Kayıt Bulunamadı.";
                        return response;
                    }
                }
                else
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "İki Tarih Arasında Kayıt Bulunamadı.";
                    return response;
                }
                #region OK
                response.SonucAciklama = "OK";
                response.SonucKod = "1";
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                response.SonucAciklama = "Hata oluştu tekrar deneyiniz.";
                response.SonucKod = "0";
                CreateFailedLog(log, response.ToXML(true), new string[] { ex.Message });
            }
            return response;
        }

        public IcmalMutabakatDetayRes TazminatPaketMutabakatDetay(string apiCode, IcmalMutabakatDetayReq req, string CorrelationId = null)
        {
            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog
            {
                CorrelationId = CorrelationId != null ? CorrelationId : "",
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                COMPANY_ID = req.SirketKod,
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);

            IcmalMutabakatDetayRes response = new IcmalMutabakatDetayRes();

            try
            {
                #region Auth
                var CompanyId = ApiCodeIsValid(apiCode, req.SirketKod);
                if (CompanyId <= 0)
                {
                    #region ErrorLog
                    response.SonucKod = "0";
                    response.SonucAciklama = "Giriş Başarısız! ApiCode'u Kontrol Ediniz.";
                    CreateFailedLog(log, response.ToXML(true), new string[] { response.SonucAciklama });
                    #endregion
                    return response;
                }
                #endregion


                #region Validation
                if (!req.icmalNo.IsInt64())
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "İcmal No Numerik Değer Olmalıdır.";
                    CreateFailedLog(log, response.ToXML(true), desc: new string[] { response.SonucAciklama });
                    return response;
                }

                #endregion
                List<V_Claim> ClaimList = new GenericRepository<V_Claim>().FindBy(conditions: $"COMPANY_ID={CompanyId} AND PAYROLL_ID={req.icmalNo}", orderby: "CLAIM_ID");

                if (ClaimList.Count() > 0 && ClaimList != null)
                {
                    List<TazminatDetay> HasarDetayList = new List<TazminatDetay>();
                    foreach (var claim in ClaimList)
                    {
                        TazminatDetay HasarDetay = new TazminatDetay();
                        HasarDetay.HasarDurum = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Claim, claim.STATUS);
                        HasarDetay.TazminatNo = claim.CLAIM_ID.ToString();
                        HasarDetay.No = Convert.ToInt32(claim.PAYROLL_ID);

                        HasarDetay.SirketTutar = (decimal)claim.PAID;
                        HasarDetayList.Add(HasarDetay);
                    }
                    response.TazminatListesi = HasarDetayList;
                }
                else
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "Zarf Mutabakat Detayı bulunamadı.";
                    return response;
                }

                #region OK
                response.SonucAciklama = "OK";
                response.SonucKod = "1";
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                response.SonucAciklama = "Hata oluştu tekrar deneyiniz.";
                response.SonucKod = "0";
                CreateFailedLog(log, response.ToXML(true), new string[] { ex.Message });
            }
            return response;



        }

        public PoliceMutabakatRes PoliceMutabakat(string apiCode, PoliceMutabakatReq req, string CorrelationId = null)
        {
            PoliceMutabakatRes response = new PoliceMutabakatRes();

            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog
            {
                CorrelationId = CorrelationId != null ? CorrelationId : Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString(),
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                COMPANY_ID = req.SirketKod,
                WsRequestDate = DateTime.Now,
                IS_IMECE_WS = 1
            };
            //CreateIncomingLog(log);


            try
            {
                #region Auth
                var CompanyId = ApiCodeIsValid(apiCode, req.SirketKod);
                if (CompanyId <= 0)
                {
                    #region ErrorLog
                    response.SonucKod = "0";
                    response.SonucAciklama = "Giriş Başarısız! ApiCode'u Kontrol Ediniz.";
                    CreateFailedLog(log, response.ToXML(true), new string[] { response.SonucAciklama });
                    #endregion
                    return response;
                }
                #endregion

                #region Validation
                if (req.TransferTarihiBaslangic > req.TransferTarihiBitis)
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "Başlangıç tarihi bitiş tarihinden büyük olamaz.";
                    CreateFailedLog(log, response.ToXML(true), desc: new string[] { "Başlangıç tarihi bitiş tarihinden büyük olamaz." });
                    return response;
                }

                #endregion

                //List<IntegrationLog> integrationLogList = new GenericRepository<IntegrationLog>().FindBy(conditions: $"ws_method = 'PoliceAktar' and COMPANY_ID= {CompanyId} and TO_DATE(WS_REQUEST_DATE,'DD.MM.YYYY') >= TO_DATE(TO_DATE('{req.TransferTarihiBaslangic.ToString("yyyy-MM-dd")}', 'YYYY-MM-DD'), 'DD.MM.YYYY') and TO_DATE(WS_REQUEST_DATE,'DD.MM.YYYY') <= TO_DATE(TO_DATE('{req.TransferTarihiBitis.ToString("yyyy-MM-dd")}', 'YYYY-MM-DD'), 'DD.MM.YYYY') AND POLICY_ID IS NOT NULL", orderby: "ID", fetchHistoricRows: true);

                //var policyIdList1 = string.Join(", ", integrationLogList.Select(p => p.PolicyId));
                //if (policyIdList1 != "")
                //{

                List<V_PolicyEndorsement> PolicyEndorsementList = new GenericRepository<V_PolicyEndorsement>().FindBy(
                    conditions: $"COMPANY_ID={CompanyId} AND " +
                    $"ENDORSEMENT_TYPE ='{((int)EndorsementType.BASLANGIC).ToString()}' AND " +
                    $"POLICY_TRANSFER_DATE >= TO_DATE('{req.TransferTarihiBaslangic.ToString("yyyy-MM-dd")}', 'YYYY-MM-DD') AND " +
                    $"POLICY_TRANSFER_DATE <= TO_DATE('{req.TransferTarihiBitis.ToString("yyyy-MM-dd")}', 'YYYY-MM-DD')",
                    orderby: "POLICY_ID");

                //TO_DATE(ENDORSEMENT_ISSUE_DATE,'DD.MM.YYYY') >= TO_DATE(TO_DATE('{req.TransferTarihiBaslangıc.ToString("yyyy-MM-dd")}', 'YYYY-MM-DD'), 'DD.MM.YYYY') AND  TO_DATE(ENDORSEMENT_ISSUE_DATE,'DD.MM.YYYY') <= TO_DATE(TO_DATE('{req.TransferTarihiBitis.ToString("yyyy-MM-dd")}', 'YYYY-MM-DD'), 'DD.MM.YYYY')
                if (PolicyEndorsementList.Count() > 0 && PolicyEndorsementList != null)
                {
                    var policyIdList = string.Join(", ", PolicyEndorsementList.Select(p => p.POLICY_ID));

                    List<V_InsuredEndorsement> InsuredEndorsementList = new GenericRepository<V_InsuredEndorsement>().FindBy(
                        conditions: $"ENDORSEMENT_TYPE ='{((int)EndorsementType.BASLANGIC).ToString()}' AND POLICY_ID IN({policyIdList})", orderby: "");

                    response.ToplamPoliceAdedi = PolicyEndorsementList.Count();
                    response.ToplamPolicePrimi = (decimal)PolicyEndorsementList.Where(x => x.ENDORSEMENT_PREMIUM != null).Select(p => p.ENDORSEMENT_PREMIUM).Sum();

                    List<Police> policeList = new List<Police>();
                    foreach (var policyEndorsement in PolicyEndorsementList)
                    {
                        Police police = new Police();
                        police.PoliceNo = policyEndorsement.POLICY_NUMBER;
                        if (policyEndorsement.ENDORSEMENT_PREMIUM == null)
                        {
                            police.Prim = 0;
                        }
                        else
                        {
                            police.Prim = (decimal)policyEndorsement.ENDORSEMENT_PREMIUM;
                        }

                        police.SigortaliAdedi = InsuredEndorsementList.Where(i => i.POLICY_ID == policyEndorsement.POLICY_ID).Count();
                        police.TransferTarihi = Convert.ToDateTime(policyEndorsement.ENDORSEMENT_ISSUE_DATE);
                        police.YenilemeNo = policyEndorsement.RENEWAL_NO.ToString();
                        response.ToplamSigortaliAdedi += police.SigortaliAdedi;
                        policeList.Add(police);
                    }
                    response.PoliceListesi = policeList;
                }
                //}
                else
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "Poliçe Mutabakat bulunamadı.";
                    return response;
                }


                #region OK
                response.SonucAciklama = "OK";
                response.SonucKod = "1";
                CreateOKLog(log, response.ToXML(true));
                #endregion

            }
            catch (Exception ex)
            {
                response.SonucAciklama = "Hata oluştu tekrar deneyiniz.";
                response.SonucKod = "0";
                CreateFailedLog(log, response.ToXML(true), new string[] { ex.Message });
            }
            return response;

        }

        public PoliceDetayMutabakatRes PoliceDetayMutabakat(string apiCode, PoliceDetayMutabakatReq req, string CorrelationId = null)
        {
            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog
            {
                CorrelationId = CorrelationId != null ? CorrelationId : "",
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                COMPANY_ID = req.SirketKod,
                PolicyId = req.PoliceNo,
                WsRequestDate = DateTime.Now,

            };
            CreateIncomingLog(log);

            PoliceDetayMutabakatRes response = new PoliceDetayMutabakatRes();

            try
            {

                #region Auth
                var CompanyId = ApiCodeIsValid(apiCode, req.SirketKod);
                if (CompanyId <= 0)
                {
                    #region ErrorLog
                    response.SonucKod = "0";
                    response.SonucAciklama = "Giriş Başarısız! ApiCode'u Kontrol Ediniz.";
                    CreateFailedLog(log, response.ToXML(true), new string[] { response.SonucAciklama });
                    #endregion
                    return response;
                }
                #endregion

                #region Validation
                if (req.PoliceNo == null)
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "Başlangıç tarihi bitiş tarihinden büyük olamaz.";
                    CreateFailedLog(log, response.ToXML(true), desc: new string[] { "Poliçe No Boş Olamaz." });
                    return response;
                }
                #endregion
                V_PolicyEndorsement PolicyEndorsement = new GenericRepository<V_PolicyEndorsement>().FindBy(conditions: $"COMPANY_ID={CompanyId} AND POLICY_NUMBER={req.PoliceNo} and RENEWAL_NO='{req.YenilemeNo}' AND ENDORSEMENT_TYPE ='1'", orderby: "POLICY_ID").FirstOrDefault();

                if (PolicyEndorsement != null)
                {
                    List<V_InsuredEndorsement> insuredEndorsementList = new GenericRepository<V_InsuredEndorsement>().FindBy(conditions: $"POLICY_ID = {PolicyEndorsement.POLICY_ID} and ENDORSEMENT_ID ={PolicyEndorsement.ENDORSEMENT_ID}", orderby: "POLICY_ID");


                    response.PoliceNo = PolicyEndorsement.POLICY_NUMBER;
                    response.YenilemeNo = PolicyEndorsement.RENEWAL_NO.ToString();
                    response.TransferTarihi = (DateTime)PolicyEndorsement.POLICY_ISSUE_DATE;
                    if (PolicyEndorsement.ENDORSEMENT_PREMIUM == null)
                    {
                        response.Prim = 0;
                    }
                    else
                    {
                        response.Prim = (decimal)PolicyEndorsement.ENDORSEMENT_PREMIUM;
                    }

                    response.ToplamSigortaliAdeti = insuredEndorsementList.Count();


                    List<SigortaliListesi> Sigortali = new List<SigortaliListesi>();
                    foreach (var insuredEndorsement in insuredEndorsementList)
                    {
                        SigortaliListesi sigortaliMutabakat = new SigortaliListesi();
                        sigortaliMutabakat.BireyTipi = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Individual, insuredEndorsement.INDIVIDUAL_TYPE);
                        sigortaliMutabakat.Prim = (decimal)insuredEndorsement.ENDORSEMENT_PREMIUM;
                        sigortaliMutabakat.IlkKayitTarihi = insuredEndorsement.REGISTRATION_DATE;
                        sigortaliMutabakat.IlkSigortalanmaTarihi = insuredEndorsement.FIRST_INSURED_DATE;
                        sigortaliMutabakat.PaketNo = insuredEndorsement.PACKAGE_NO;
                        sigortaliMutabakat.SigortaliNo = insuredEndorsement.COMPANY_INSURED_NO.ToString();
                        GercekKisi kisiBilgi = new GercekKisi();
                        kisiBilgi.Ad = insuredEndorsement.FIRST_NAME;
                        kisiBilgi.Soyad = insuredEndorsement.LAST_NAME;
                        kisiBilgi.TCKN = insuredEndorsement.IDENTITY_NO.ToString();
                        kisiBilgi.VKN = insuredEndorsement.TAX.ToString();
                        kisiBilgi.Cinsiyet = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Gender, insuredEndorsement.GENDER);
                        kisiBilgi.DogumTarihi = insuredEndorsement.BIRTHDATE;
                        kisiBilgi.BabaAd = insuredEndorsement.NAME_OF_FATHER;
                        // kisiBilgi.Uyruk = CountyHelper.GetCountybyCodeGetCountybyA2Code;
                        kisiBilgi.PasaportNo = insuredEndorsement.PASSPORT_NO;
                        kisiBilgi.DogumYeri = insuredEndorsement.BIRTHPLACE;
                        sigortaliMutabakat.KisiBilgi = kisiBilgi;
                        //Dogum yeri PASAPORT NO baba adı ve uyruk gelecek




                        Sigortali.Add(sigortaliMutabakat);
                    }
                    response.Sigortali = Sigortali;
                }
                else
                {

                    response.SonucKod = "0";
                    response.SonucAciklama = "Poliçe Mutabakat Detay bulunamadı.";
                    return response;
                }

                #region OK
                response.SonucAciklama = "OK";
                response.SonucKod = "1";
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                response.SonucAciklama = "Hata oluştu tekrar deneyiniz.";
                response.SonucKod = "0";
                CreateFailedLog(log, response.ToXML(true), new string[] { ex.Message });
            }
            return response;
        }

        public ZeyilMutabakatRes ZeyilMutabakat(string apiCode, ZeyilMutabakatReq req, string CorrelationId = null)
        {
            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog
            {
                CorrelationId = CorrelationId != null ? CorrelationId : "",
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
                PolicyId = long.Parse(req.PoliceNo),
                COMPANY_ID = req.SirketKod
            };
            CreateIncomingLog(log);

            ZeyilMutabakatRes response = new ZeyilMutabakatRes();

            try
            {

                #region Auth
                var CompanyId = ApiCodeIsValid(apiCode, req.SirketKod);
                if (CompanyId <= 0)
                {
                    #region ErrorLog
                    response.SonucKod = "0";
                    response.SonucAciklama = "Giriş Başarısız! ApiCode'u Kontrol Ediniz.";
                    CreateFailedLog(log, response.ToXML(true), new string[] { response.SonucAciklama });
                    #endregion
                    return response;
                }
                #endregion

                #region Validation
                if (req.PoliceNo == null)
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "Poliçe No Boş olamaz.";
                    CreateFailedLog(log, response.ToXML(true), desc: new string[] { "Poliçe No Boş Olamaz." });
                    return response;
                }
                #endregion




                List<V_PolicyEndorsement> policyEndorsement = new GenericRepository<V_PolicyEndorsement>().FindBy(conditions: $"COMPANY_ID={CompanyId} AND POLICY_NUMBER = {req.PoliceNo} and RENEWAL_NO ='{req.YenilemeNo}'", orderby: "ENDORSEMENT_ID");
                //var policyIdList1 = string.Join(", ", policyEndorsement.Select(p => p.POLICY_ID));
                //List<IntegrationLog> integrationLogList = new GenericRepository<IntegrationLog>().FindBy(conditions: $"(ws_method ='ZeyilSigortaliCikar' or ws_method ='ZeyilSigortaliEkle' or ws_method like '%PoliceIptal%') and COMPANY_ID= {CompanyId} and TO_DATE(WS_REQUEST_DATE,'DD.MM.YYYY') >= TO_DATE(TO_DATE('{req.TransferTarihiBaslangic.ToString("yyyy-MM-dd")}', 'YYYY-MM-DD'), 'DD.MM.YYYY') and TO_DATE(WS_REQUEST_DATE,'DD.MM.YYYY') <= TO_DATE(TO_DATE('{req.TransferTarihiBitis.ToString("yyyy-MM-dd")}', 'YYYY-MM-DD'), 'DD.MM.YYYY') AND POLICY_ID IN({policyIdList1})", orderby: "ID", fetchHistoricRows: true);


                if (policyEndorsement.Count() > 0 && policyEndorsement != null)
                {

                    var endorsementIdList = string.Join(", ", policyEndorsement.Select(p => p.ENDORSEMENT_ID));
                    List<V_InsuredEndorsement> insuredEndorsementList = new GenericRepository<V_InsuredEndorsement>().FindBy(conditions: $"POLICY_ID = {policyEndorsement[0].POLICY_ID} and ENDORSEMENT_ID IN ({endorsementIdList})", orderby: "POLICY_ID", fetchDeletedRows: true);

                    response.ToplamZeyilAdedi = policyEndorsement.Count();

                    List<Zeyil> zeyilList = new List<Zeyil>();
                    foreach (var zeyilMutabakat in policyEndorsement)
                    {
                        Zeyil ZeyilMutabakatList = new Zeyil();
                        ZeyilMutabakatList.No = Convert.ToInt32(zeyilMutabakat.ENDORSEMENT_NO);
                        ZeyilMutabakatList.Prim = (decimal)zeyilMutabakat.ENDORSEMENT_PREMIUM;
                        ZeyilMutabakatList.Tipi = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Endorsement, zeyilMutabakat.ENDORSEMENT_TYPE);
                        ZeyilMutabakatList.SigortaliAdeti = insuredEndorsementList.Where(x => x.ENDORSEMENT_ID == zeyilMutabakat.ENDORSEMENT_ID).Count();
                        response.ToplamSigortaliAdedi += ZeyilMutabakatList.SigortaliAdeti;
                        response.ToplamPrim += ZeyilMutabakatList.Prim;
                        zeyilList.Add(ZeyilMutabakatList);
                    }
                    response.ZeyilListesi = zeyilList;
                }

                else
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "Zeyil Mutabakat Bulunamadı.";
                    return response;
                }

                #region OK
                response.SonucAciklama = "OK";
                response.SonucKod = "1";
                CreateOKLog(log, response.ToXML(true));
                #endregion

            }
            catch (Exception ex)
            {
                response.SonucAciklama = "Hata oluştu tekrar deneyiniz.";
                response.SonucKod = "0";
                CreateFailedLog(log, response.ToXML(true), new string[] { ex.Message });
            }
            return response;
        }

        public ZeyilDetayMutabakatRes ZeyilDetayMutabakat(string apiCode, ZeyilDetayMutabakatReq req, string CorrelationId = null)
        {
            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog
            {
                CorrelationId = CorrelationId != null ? CorrelationId : "",
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
                COMPANY_ID = req.SirketKod,
                PolicyId = long.Parse(req.PoliceNo)
            };
            CreateIncomingLog(log);

            ZeyilDetayMutabakatRes response = new ZeyilDetayMutabakatRes();

            try
            {

                #region Auth
                var CompanyId = ApiCodeIsValid(apiCode, req.SirketKod);
                if (CompanyId <= 0)
                {
                    #region ErrorLog
                    response.SonucKod = "0";
                    response.SonucAciklama = "Giriş Başarısız! ApiCode'u Kontrol Ediniz.";
                    CreateFailedLog(log, response.ToXML(true), new string[] { response.SonucAciklama });
                    #endregion
                    return response;
                }
                #endregion

                #region Validation
                if (req.PoliceNo == null || req.ZeyilNo == null)
                {
                    response.SonucKod = "0";
                    response.SonucAciklama = "Poliçe No Veya ZeyilNo Boş olamaz.";
                    CreateFailedLog(log, response.ToXML(true), desc: new string[] { "Poliçe No Veya ZeyilNo Boş olamaz." });
                    return response;
                }

                #endregion
                V_PolicyEndorsement Endorsement = new GenericRepository<V_PolicyEndorsement>().FindBy(conditions: $"COMPANY_ID={CompanyId} AND POLICY_NUMBER = {req.PoliceNo} and RENEWAL_NO ='{req.YenilemeNo}' and ENDORSEMENT_NO='{req.ZeyilNo}'", orderby: "").FirstOrDefault();





                if (Endorsement != null)
                {
                    List<V_InsuredEndorsement> InsuredEndorsementList = new GenericRepository<V_InsuredEndorsement>().FindBy(conditions: $"POLICY_ID = {Endorsement.POLICY_ID} AND ENDORSEMENT_ID={Endorsement.ENDORSEMENT_ID}", orderby: "");

                    List<SigortaliList> Sigortalilar = new List<SigortaliList>();

                    response.Tipi = Endorsement.ENDORSEMENT_TYPE;
                    response.SigortaliAdedi = InsuredEndorsementList.Count();
                    response.Prim = (decimal)Endorsement.ENDORSEMENT_PREMIUM;
                    response.SirketKod = CompanyId.ToString();


                    if (Endorsement.ENDORSEMENT_TYPE == ((int)EndorsementType.SIGORTALI_CIKISI).ToString() || Endorsement.ENDORSEMENT_TYPE == ((int)EndorsementType.SIGORTALI_GIRISI).ToString())
                    {
                        foreach (var insuredEndorsment in InsuredEndorsementList)
                        {
                            SigortaliList zeyilDetayList = new SigortaliList();
                            zeyilDetayList.BireyTipi = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Individual, insuredEndorsment.INDIVIDUAL_TYPE);
                            zeyilDetayList.Prim = (decimal)insuredEndorsment.ENDORSEMENT_PREMIUM;
                            zeyilDetayList.TCKN = insuredEndorsment.IDENTITY_NO;
                            zeyilDetayList.IlkKayitTarihi = insuredEndorsment.REGISTRATION_DATE;
                            zeyilDetayList.IlkSigortalanmaTarihi = insuredEndorsment.FIRST_INSURED_DATE;
                            zeyilDetayList.PaketNo = insuredEndorsment.PACKAGE_NO;
                            zeyilDetayList.SigortaliNo = insuredEndorsment.CONTACT_ID.ToString();
                            GercekKisi kisiBilgi = new GercekKisi();
                            kisiBilgi.Ad = insuredEndorsment.FIRST_NAME;
                            kisiBilgi.Soyad = insuredEndorsment.LAST_NAME;
                            kisiBilgi.TCKN = insuredEndorsment.IDENTITY_NO.ToString();
                            kisiBilgi.Cinsiyet = insuredEndorsment.GENDER;
                            kisiBilgi.DogumTarihi = insuredEndorsment.BIRTHDATE;
                            kisiBilgi.VKN = insuredEndorsment.TAX.ToString();
                            kisiBilgi.BabaAd = insuredEndorsment.NAME_OF_FATHER;
                            // kisiBilgi.Uyruk = CountyHelper.GetCountybyCodeGetCountybyA2Code;
                            kisiBilgi.PasaportNo = insuredEndorsment.PASSPORT_NO;
                            kisiBilgi.DogumYeri = insuredEndorsment.BIRTHPLACE;


                            response.PoliceNo = insuredEndorsment.POLICY_NUMBER;
                            response.YenilemeNo = Convert.ToInt32(insuredEndorsment.RENEWAL_NO);
                            response.ZeyilNo = Convert.ToInt32(insuredEndorsment.ENDORSEMENT_NO);
                            Sigortalilar.Add(zeyilDetayList);
                        }

                        response.Sigortali = Sigortalilar;
                    }
                }

                else
                {

                    response.SonucKod = "0";
                    response.SonucAciklama = "Zeyil Detay Mutabakat Bulunamadı.";
                    return response;
                }

                #region OK
                response.SonucAciklama = "OK";
                response.SonucKod = "1";
                CreateOKLog(log, response.ToXML(true));
                #endregion

            }
            catch (Exception ex)
            {

                response.SonucAciklama = "Hata oluştu tekrar deneyiniz.";
                response.SonucKod = "0";
                CreateFailedLog(log, response.ToXML(true), new string[] { ex.Message });

            }
            return response;
        }

        public PolicyAgreementRes PolicyAgreement(PoliceMutabakatReq req)
        {

            var currentMethod = MethodBase.GetCurrentMethod();

            //IntegrationLog log = new IntegrationLog
            //{
            //    WsName = currentMethod.DeclaringType.FullName,
            //    WsMethod = currentMethod.Name,
            //    Request = req.ToXML(true),
            //    WsRequestDate = DateTime.Now,
            //    COMPANY_ID = req.SirketKod,
            //    IS_IMECE_WS = 1
            //};
            //CreateIncomingLog(log);

            PolicyAgreementRes response = new PolicyAgreementRes();
            PoliceDetayMutabakatReq policeDetayMutabakatReq = new PoliceDetayMutabakatReq();

            var companies = new GenericRepository<V_CompanyParameter>().FindBy($"KEY='APICODE' and COMPANY_ID != 1", orderby: "COMPANY_ID");




            Agreement agreement = new Agreement();
            req.TransferTarihiBaslangic = DateTime.Now;
            req.TransferTarihiBitis = DateTime.Now;

            foreach (var company in companies)
            {
                req.SirketKod = Convert.ToInt32(company.CompanyId);
                string corrId = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString();
                var result = PoliceMutabakat(company.Value, req, corrId);
                PoliceMutabakatAgreementRes mutabakatTest = new PoliceMutabakatAgreementRes();

                if (result.SonucKod == "1")
                {
                    #region Hatasız değer

                    mutabakatTest.MutabakatPoliceListesiTest = new List<MutabakatPoliceListesiTest>();
                    mutabakatTest.ToplamPoliceAdedi = result.ToplamPoliceAdedi;
                    mutabakatTest.ToplamPolicePrimi = result.ToplamPolicePrimi;
                    foreach (var policeList in result.PoliceListesi)
                    {
                        mutabakatTest.MutabakatPoliceListesiTest.Add(new MutabakatPoliceListesiTest
                        {
                            PoliceNo = policeList.PoliceNo,
                            Prim = policeList.Prim,
                            SigortaliAdedi = policeList.SigortaliAdedi.ToString(),
                            YenilemeNo = policeList.YenilemeNo
                        });
                    }



                    #endregion
                }



                if (result.SonucKod == "1")
                {
                    agreement.CorrelationId = corrId;
                    agreement.Description += company.CompanyId + " ID'li Şirket için : ";
                    agreement.Description += result.ToplamPoliceAdedi == mutabakatTest.ToplamPoliceAdedi ? "" : "Poliçe Adedi Farklı Imece Police Adedi : " + result.ToplamPoliceAdedi + " Diğer Poliçe Adedi : " + mutabakatTest.ToplamPoliceAdedi + " ";
                    agreement.Description += result.ToplamPolicePrimi == mutabakatTest.ToplamPolicePrimi ? "" : "Poliçe Primi Farklı Imece Prim : " + result.ToplamPolicePrimi + " Diğer Prim : " + mutabakatTest.ToplamPolicePrimi + " ";
                    agreement.CompanyId = company.CompanyId;
                    agreement.AgreementDate = DateTime.Now;
                    agreement.Status = "1";
                    agreement.Type = "5";

                    foreach (var policeList in result.PoliceListesi)
                    {
                        var mutabakatPoliceListesi = mutabakatTest.MutabakatPoliceListesiTest.Where(x => x.PoliceNo == policeList.PoliceNo).FirstOrDefault();
                        if (mutabakatPoliceListesi != null)
                        {
                            agreement.Description += mutabakatPoliceListesi.Prim == policeList.Prim ? "" : "Prim farklı Imece Prim : " + policeList.Prim + " Diğer Prim : " + mutabakatPoliceListesi.Prim + " ";
                            agreement.Description += mutabakatPoliceListesi.SigortaliAdedi == policeList.SigortaliAdedi.ToString() ? "" : "Sigortalı Adedi Farklı Imece Sigortalı Adet : " + policeList.SigortaliAdedi + " Diğer Sigortalı Adet : " + mutabakatPoliceListesi.SigortaliAdedi + " ";
                            agreement.Description += mutabakatPoliceListesi.YenilemeNo == policeList.YenilemeNo ? "" : "Yenileme No farklı Imece Yenileme No : " + policeList.YenilemeNo + " Diğer Yenileme No : " + mutabakatPoliceListesi.YenilemeNo + " ";
                        }
                        else
                        {
                            agreement.Description += "Diğer Şirkette bu poliçe bulunamadı : " + policeList.PoliceNo + " ";
                        }


                        if (agreement.Description != "" && agreement.Description != null)
                        {
                            policeDetayMutabakatReq.PoliceNo = policeList.PoliceNo;
                            policeDetayMutabakatReq.SirketKod = Convert.ToInt32(company.CompanyId);
                            policeDetayMutabakatReq.YenilemeNo = policeList.YenilemeNo;

                            var policeDetayResult = PoliceDetayMutabakat(company.Value, policeDetayMutabakatReq, corrId);

                            #region Hatasız DetayResponse
                            PoliceDetayMutabakatAgreementRes mutabakatDetayRes = new PoliceDetayMutabakatAgreementRes();
                            mutabakatDetayRes.Sigortali = new List<SigortaliTestListesi>();
                            mutabakatDetayRes.PoliceNo = policeDetayResult.PoliceNo;
                            mutabakatDetayRes.YenilemeNo = policeDetayResult.YenilemeNo;
                            mutabakatDetayRes.ToplamSigortaliAdeti = policeDetayResult.ToplamSigortaliAdeti;
                            mutabakatDetayRes.Prim = policeDetayResult.Prim;

                            foreach (var policeDetay in mutabakatDetayRes.Sigortali)
                            {
                                mutabakatDetayRes.Sigortali.Add(new SigortaliTestListesi
                                {
                                    TCKN = policeDetay.TCKN,
                                    BireyTipi = policeDetay.BireyTipi,
                                    Prim = policeDetay.Prim
                                });
                            }


                            #endregion


                            if (policeDetayResult.SonucKod == "1")
                            {
                                agreement.Description += mutabakatDetayRes.YenilemeNo == policeDetayResult.YenilemeNo ? "" : "Yenileme No Farklı : Imece Yenileme No : " + policeDetayResult.YenilemeNo + " Diğer Yenileme No : " + mutabakatDetayRes.YenilemeNo + " ";
                                agreement.Description += mutabakatDetayRes.ToplamSigortaliAdeti == policeDetayResult.ToplamSigortaliAdeti ? "" : " Sigortalı Adeti Farklı : Imece Sigortalı Adeti : " + policeDetayResult.ToplamSigortaliAdeti + " Diğer Sigortalı Adeti : " + mutabakatDetayRes.ToplamSigortaliAdeti + " ";

                                agreement.Description += mutabakatDetayRes.Prim == policeDetayResult.Prim ? "" : " Toplam Prim miktarı farklı : Imece Toplam Prim miktarı : " + policeDetayResult.Prim + " Diğer Toplam Prim miktarı  : " + mutabakatDetayRes.Prim + " ";


                                foreach (var SigortaliList in policeDetayResult.Sigortali)
                                {
                                    var mutabakatDetaySigortaliListesi = mutabakatDetayRes.Sigortali.Where(x => (x.TCKN == SigortaliList.TCKN || x.Prim == SigortaliList.Prim)).FirstOrDefault();
                                    if (mutabakatDetaySigortaliListesi != null)
                                    {
                                        agreement.Description += SigortaliList.Prim == mutabakatDetaySigortaliListesi.Prim ? "" : " Prim Farklı Imece Prim : " + SigortaliList.Prim + " Diğer Prim : " + mutabakatDetaySigortaliListesi.Prim + " ";
                                        agreement.Description += SigortaliList.BireyTipi == mutabakatDetaySigortaliListesi.BireyTipi ? "" : " Birey Tipi Farklı : Imece Birey Tipi : " + SigortaliList.BireyTipi + " Diğer Birey Tipi : " + mutabakatDetaySigortaliListesi.BireyTipi;
                                        agreement.Description += "TCKN Farklı Imece TCKN : " + SigortaliList.TCKN + " Diğer TCKN :" + mutabakatDetaySigortaliListesi.TCKN + " ";
                                    }
                                    else
                                    {
                                        agreement.Description += "Imece'de bulunan " + policeDetayResult.PoliceNo + " No'lu " + "Poliçe No'ya ait detay kaydı diğer şirkette bulunamamıştır. ";
                                    }
                                }
                            }
                            else
                            {
                                agreement.Description += "Poliçe No Farklı Imece Police No : " + policeDetayResult.PoliceNo + " Diğer Poliçe No : " + policeDetayResult.PoliceNo + " ";
                            }


                        }


                    }

                    var insertResult = new GenericRepository<Agreement>().Insert(agreement, this.Token);
                    if (agreement.Description == "" || agreement.Description == "Veriler Mutabıktır. ")
                    {
                        agreement.Description = "Veriler Mutabıktır. ";
                        response.resultWs += agreement.Description;
                    }
                    else
                    {
                        response.resultWs += agreement.Description;
                    }
                }
            }
            return response;
        }

        public ZeyilAgreementRes ZeyilAgreement(ZeyilMutabakatReq req)
        {
            var currentMethod = MethodBase.GetCurrentMethod();
            var companies = new GenericRepository<V_CompanyParameter>().FindBy($"KEY='APICODE'", orderby: "COMPANY_ID");
            ZeyilAgreementRes response = new ZeyilAgreementRes();


            Agreement agreement = new Agreement();
            req.PoliceNo = "22741570";
            req.YenilemeNo = 3;
            foreach (var company in companies)
            {
                req.SirketKod = Convert.ToInt32(company.CompanyId);
                string corrId = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString();
                var result = ZeyilMutabakat(company.Value, req, corrId);
                ZeyilMutabakatAgreementRes mutabakatTest = new ZeyilMutabakatAgreementRes();

                if (result.SonucKod == "1")
                {

                    #region Hazır zeyil
                    mutabakatTest.ZeyilListesi = new List<ZeyilListTest>();
                    mutabakatTest.ToplamZeyilAdedi = result.ToplamZeyilAdedi;
                    foreach (var zeyilMutabakat in result.ZeyilListesi)
                    {
                        mutabakatTest.ZeyilListesi.Add(new ZeyilListTest
                        {
                            No = zeyilMutabakat.No,
                            Prim = zeyilMutabakat.Prim,
                            SigortaliAdeti = zeyilMutabakat.SigortaliAdeti,
                            Tipi = zeyilMutabakat.Tipi
                        });
                    }

                    #endregion
                }

                if (result.SonucKod == "1")
                {
                    agreement.CorrelationId = corrId;
                    agreement.Description = company.CompanyId + " ID'li Şirket için : ";
                    agreement.Description += result.ToplamZeyilAdedi == mutabakatTest.ToplamZeyilAdedi ? "" : "Zeyil Adedi Farklı Imece Zeyil Adedi : " + result.ToplamZeyilAdedi + " Diğer Zeyil Adedi : " + mutabakatTest.ToplamZeyilAdedi;
                    agreement.CompanyId = company.CompanyId;
                    agreement.AgreementDate = DateTime.Now;
                    agreement.Status = "1";
                    agreement.Type = "1";
                    foreach (var zeyilList in result.ZeyilListesi)
                    {
                        var mutabakatList = mutabakatTest.ZeyilListesi.Where(x => (x.No == zeyilList.No)).FirstOrDefault();
                        if (mutabakatList != null)
                        {
                            agreement.Description += zeyilList.Prim == mutabakatList.Prim ? "" : " Prim Farklı Imece Prim : " + mutabakatList.Prim + " Diğer Prim : " + mutabakatList.Prim + " ";
                            agreement.Description += zeyilList.No == mutabakatList.No ? "" : " Zeyil No Farklı Imece Zeyil No :" + zeyilList.No + " Diğer Prim : " + mutabakatList.No + " ";
                            agreement.Description += zeyilList.SigortaliAdeti == mutabakatList.SigortaliAdeti ? "" : " Sigortalı Adedi Farklı Imece Sigortalı Adeti : " + zeyilList.SigortaliAdeti + " Diğer Sigortalı Adeti : " + mutabakatList.SigortaliAdeti;

                            agreement.Description += zeyilList.Tipi == mutabakatList.Tipi ? "" : " Zeyil Tipi Farklı Imece Tipi : " + mutabakatList.Tipi + " Diğer Zeyil Tipi: " + mutabakatList.Tipi;
                        }
                        else
                        {
                            //         agreement.Description += "Zeyil No diğer şirkette bulunamadı. Imece Zeyil No :" + zeyilList.No + " ";
                        }



                    }
                    if (agreement.Description != "" && agreement.Description != null)
                    {

                        ZeyilDetayMutabakatReq mutabakatTestZeyilDetayReq = new ZeyilDetayMutabakatReq();
                        mutabakatTestZeyilDetayReq.SirketKod = Convert.ToInt32(company.CompanyId);
                        mutabakatTestZeyilDetayReq.PoliceNo = req.PoliceNo;
                        mutabakatTestZeyilDetayReq.YenilemeNo = req.YenilemeNo.ToString();
                        mutabakatTestZeyilDetayReq.ZeyilNo = "1";

                        var ResultZeyilDetay = ZeyilDetayMutabakat(company.Value, mutabakatTestZeyilDetayReq, corrId);

                        #region hazır zeyil detay 

                        ZeyilMutabakatDetayAgreementRes mutabakatDetayRes = new ZeyilMutabakatDetayAgreementRes();
                        mutabakatDetayRes.SigortaliListTest = new List<SigortaliListTest>();
                        mutabakatDetayRes.Prim = ResultZeyilDetay.Prim;
                        mutabakatDetayRes.SigortaliAdedi = ResultZeyilDetay.SigortaliAdedi;
                        mutabakatDetayRes.Tipi = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Endorsement, ResultZeyilDetay.Tipi);

                        foreach (var item in ResultZeyilDetay.Sigortali)
                        {
                            mutabakatDetayRes.SigortaliListTest.Add(new SigortaliListTest
                            {
                                TCKN = item.TCKN,
                                BireyTipi = item.BireyTipi,
                                Prim = item.Prim
                            });
                        }

                        mutabakatDetayRes.SigortaliListTest.Add(new SigortaliListTest
                        {
                            TCKN = 123323123,
                            BireyTipi = "FERT",
                            Prim = 112.42M
                        });
                        #endregion

                        if (ResultZeyilDetay.SonucKod == "1")
                        {


                            agreement.Description += ResultZeyilDetay.Tipi == mutabakatDetayRes.Tipi ? "" : " Tipi farklıdır. Imece Tipi : " + ResultZeyilDetay.Tipi + " Diğer Şirket tipi : " + mutabakatDetayRes.Tipi + " ";
                            agreement.Description += ResultZeyilDetay.SigortaliAdedi == mutabakatDetayRes.SigortaliAdedi ? "" : " Sigortalı Adedi farklıdır. Imece Sigortalı adedi : " + ResultZeyilDetay.SigortaliAdedi + " Diğer Sigortalı Adedi " + mutabakatDetayRes.SigortaliAdedi;
                            agreement.Description += ResultZeyilDetay.Prim == mutabakatDetayRes.Prim ? "" : "Prim farklıdır. Imece Prim : " + ResultZeyilDetay.Prim + " Diğer Şirket Prim : " + mutabakatDetayRes.Prim;

                            if (ResultZeyilDetay.Sigortali != null)
                            {
                                foreach (var sigortali in ResultZeyilDetay.Sigortali)
                                {
                                    var mutabakatDetayResSelect = mutabakatDetayRes.SigortaliListTest.Where(x => x.TCKN == sigortali.TCKN).FirstOrDefault();
                                    if (mutabakatDetayResSelect != null)
                                    {
                                        agreement.Description += sigortali.Prim == mutabakatDetayResSelect.Prim ? "" : " Prim Farklı Imece Prim : " + sigortali.Prim + " Diğer Prim : " + mutabakatDetayResSelect.Prim + " ";
                                        agreement.Description += sigortali.BireyTipi == mutabakatDetayResSelect.BireyTipi ? "" : " Birey Tipi Farklı : Imece Birey Tipi : " + sigortali.BireyTipi + " Diğer Birey Tipi : " + mutabakatDetayResSelect.BireyTipi;
                                        agreement.Description += " TCKN Farklı Imece TCKN : " + sigortali.TCKN + " Diğer TCKN :" + mutabakatDetayResSelect.TCKN + " ";
                                    }
                                    else
                                    {
                                        agreement.Description += " Imecede bulunan " + sigortali.TCKN + " TCKN li kişi bulunamadı.";
                                    }

                                }
                            }

                        }
                    }



                    var insertResult = new GenericRepository<Agreement>().Insert(agreement, this.Token);
                    if (agreement.Description == "")
                    {
                        agreement.Description = "Veriler Mutabıktır.";
                        response.resultWs = agreement.Description;
                    }
                    else
                    {
                        response.resultWs = agreement.Description;
                    }
                }


            }

            return response;

        }

        public TazminatAgreementRes TazminatAgreement(ProvizyonMutabakatReq req)
        {
            var currentMethod = MethodBase.GetCurrentMethod();
            var companies = new GenericRepository<V_CompanyParameter>().FindBy($"KEY='APICODE'", orderby: "COMPANY_ID");
            TazminatAgreementRes response = new TazminatAgreementRes();

            Agreement agreement = new Agreement();
            req.TransferTarihiBaslangic = DateTime.Now;
            req.TransferTarihiBitis = DateTime.Now;
            foreach (var company in companies)
            {
                req.SirketKod = Convert.ToInt32(company.CompanyId);
                string corrId = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString();
                var result = ProvizyonMutabakat(company.Value, req, corrId);
                mutabakatAgreementTazminatRes mutabakatAgreementTazminatRes = new mutabakatAgreementTazminatRes();
                if (result.SonucKod == "1")
                {
                    #region mutabakatTazminatRes

                    mutabakatAgreementTazminatRes.ToplamAdet = result.ToplamAdet;
                    mutabakatAgreementTazminatRes.ToplamSirketTutar = result.ToplamSirketTutar;
                    mutabakatAgreementTazminatRes.TazminatDurumListesi = new List<TazminatDurum>();
                    foreach (var tazminatDurumList in result.TazminatDurumListesi)
                    {
                        mutabakatAgreementTazminatRes.TazminatDurumListesi.Add(new TazminatDurum
                        {
                            Adet = tazminatDurumList.Adet,
                            Durum = tazminatDurumList.Durum,
                            SirketTutar = tazminatDurumList.SirketTutar
                        });
                    }

                    #endregion
                }


                if (result.SonucKod == "1")
                {
                    agreement.CorrelationId = corrId;
                    agreement.Description += result.ToplamAdet == mutabakatAgreementTazminatRes.ToplamAdet ? "" : "Toplam Adet Farklı Imece Toplam Adedi : " + result.ToplamAdet + " Diğer Şirketin Toplam Adedi : " + mutabakatAgreementTazminatRes.ToplamAdet;
                    agreement.Description += result.ToplamSirketTutar == mutabakatAgreementTazminatRes.ToplamSirketTutar ? "" : "Toplam Şirket Tutarı Imece Zeyil Adedi : " + result.ToplamSirketTutar + " Diğer Toplam Şirket Tutarı Adedi : " + mutabakatAgreementTazminatRes.ToplamSirketTutar;

                    agreement.CompanyId = company.CompanyId;
                    agreement.AgreementDate = DateTime.Now;
                    agreement.Status = "1";
                    agreement.Type = "2";

                    foreach (var tazminatDurumListesi in result.TazminatDurumListesi)
                    {
                        var mutabakatTazminat = mutabakatAgreementTazminatRes.TazminatDurumListesi.Where(x => x.Durum == tazminatDurumListesi.Durum && x.Adet == tazminatDurumListesi.Adet && x.SirketTutar == tazminatDurumListesi.SirketTutar).FirstOrDefault();

                        if (mutabakatTazminat == null)
                        {
                            agreement.Description += "Diğer Şirkette Tazminat Durumun içinde şu değeler bulunamadı.  Durum : " + tazminatDurumListesi.Durum + " Adet : " + tazminatDurumListesi.Adet + " Şirket Tutar : " + tazminatDurumListesi.SirketTutar;
                        }
                    }

                }
                if (agreement.Description != "" && agreement.Description != null)
                {
                    ProvizyonMutabakatDetayReq mutabakatAgreementTazminatDetayReq = new ProvizyonMutabakatDetayReq();
                    mutabakatAgreementTazminatDetayReq.Durum = "2";
                    mutabakatAgreementTazminatDetayReq.TransferTarihiBaslangic = DateTime.Now;
                    mutabakatAgreementTazminatDetayReq.TransferTarihiBitis = DateTime.Now;
                    mutabakatAgreementTazminatDetayReq.SirketKod = Convert.ToInt32(company.CompanyId);

                    var tazminatMutabakatDetay = ProvizyonMutabakatDetay(company.Value, mutabakatAgreementTazminatDetayReq, corrId);


                    #region mutabakatTazminatDetayRes
                    mutabakatAgreementTazminatDetayRes mutabakatAgreementTazminatDetayRes = new mutabakatAgreementTazminatDetayRes();
                    mutabakatAgreementTazminatDetayRes.TazminatDurum = new TazminatDurum();

                    mutabakatAgreementTazminatDetayRes.TazminatDurum.SirketTutar = tazminatMutabakatDetay.TazminatDurum.SirketTutar;
                    mutabakatAgreementTazminatDetayRes.TazminatDurum.Durum = tazminatMutabakatDetay.TazminatDurum.Durum;
                    mutabakatAgreementTazminatDetayRes.TazminatDurum.Adet = tazminatMutabakatDetay.TazminatDurum.Adet;
                    mutabakatAgreementTazminatDetayRes.TazminatListesi = new List<TazminatAgreement>();
                    foreach (var tazminatListReq in tazminatMutabakatDetay.TazminatListesi)
                    {
                        mutabakatAgreementTazminatDetayRes.TazminatListesi.Add(new TazminatAgreement
                        {
                            No = tazminatListReq.No,
                            SirketTutar = tazminatListReq.SirketTutar,
                        });
                    }

                    #endregion

                    if (tazminatMutabakatDetay.SonucKod == "1")
                    {

                        foreach (var tazminatListesi in tazminatMutabakatDetay.TazminatListesi)
                        {
                            var mutabakatDetayTazminatDigerSirket = mutabakatAgreementTazminatDetayRes.TazminatListesi.Where(x => x.No == tazminatListesi.No && x.SirketTutar == tazminatListesi.SirketTutar).FirstOrDefault();
                            if (mutabakatDetayTazminatDigerSirket == null)
                            {
                                agreement.Description += "Diğer Şirkette No : " + tazminatListesi.No + " ve Şirket Tutarı : " + tazminatListesi.SirketTutar + " bulunamadı ";
                            }
                        }
                        foreach (var digerSirketTazminatListesi in mutabakatAgreementTazminatDetayRes.TazminatListesi)
                        {
                            var mutabakatDetayImece = tazminatMutabakatDetay.TazminatListesi.Where(x => x.No == digerSirketTazminatListesi.No && x.SirketTutar == digerSirketTazminatListesi.SirketTutar).FirstOrDefault();
                            if (mutabakatDetayImece == null)
                            {
                                agreement.Description += "Imece Şirketinde No : " + digerSirketTazminatListesi.No + " ve Şirket Tutarı : " + digerSirketTazminatListesi.SirketTutar + " bulunamadı ";
                            }
                        }
                    }
                }

            }
            var insertResult = new GenericRepository<Agreement>().Insert(agreement, this.Token);
            if (agreement.Description == "")
            {
                agreement.Description = "Veriler Mutabıktır.";
                response.resultWs = agreement.Description;
            }
            else
            {
                response.resultWs = agreement.Description;
            }
            return response;
        }

        public IcmalAgreementRes IcmalAgreement(IcmalMutabakatReq req)
        {
            var currentMethod = MethodBase.GetCurrentMethod();
            var companies = new GenericRepository<V_CompanyParameter>().FindBy($"KEY='APICODE'", orderby: "COMPANY_ID");

            IcmalAgreementRes response = new IcmalAgreementRes();




            Agreement agreement = new Agreement();
            req.TransferTarihiBaslangic = DateTime.Now;
            req.TransferTarihiBitis = DateTime.Now;

            foreach (var company in companies)
            {
                req.SirketKod = Convert.ToInt32(company.CompanyId);
                string corrId = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString();
                var result = TazminatPaketMutabakat(company.Value, req, corrId);
                mutabakatAgreementIcmalRes mutabakatAgreementIcmalRes = new mutabakatAgreementIcmalRes();
                if (result.SonucKod == "1")
                {

                    #region mutabakatIcmalRes
                    mutabakatAgreementIcmalRes.ToplamIcmalAdedi = result.ToplamIcmalAdedi;
                    mutabakatAgreementIcmalRes.ToplamIcmalTutarı = result.ToplamIcmalTutari;
                    mutabakatAgreementIcmalRes.IcmalListesi = new List<IcmalDetayListAgreement>();
                    foreach (var icmalListesiRes in result.IcmalListesi)
                    {
                        mutabakatAgreementIcmalRes.IcmalListesi.Add(new IcmalDetayListAgreement
                        {
                            IcmalDurumu = icmalListesiRes.IcmalDurumu,
                            IcmalNo = icmalListesiRes.TazminatPaketNo,
                            OdemeTarihi = icmalListesiRes.OdemeTarihi,
                            ToplamTutar = icmalListesiRes.ToplamFaturaTutar
                        });
                    }

                    #endregion

                }
                if (result.SonucKod == "1")
                {
                    agreement.CorrelationId = corrId;
                    agreement.Description += result.ToplamIcmalAdedi == mutabakatAgreementIcmalRes.ToplamIcmalAdedi ? "" : " Icmal Adetleri Farklı Imece Icmal Adet : " + result.ToplamIcmalAdedi + " Diger Sirket Icmal Adet : " + mutabakatAgreementIcmalRes.ToplamIcmalAdedi;
                    agreement.Description += result.ToplamIcmalTutari == mutabakatAgreementIcmalRes.ToplamIcmalTutarı ? "" : " Icmal Tutarları Farklı Imece Icmal Tutarı : " + result.ToplamIcmalTutari + " Diğer Şirket Icmal Tutarı : " + mutabakatAgreementIcmalRes.ToplamIcmalTutarı;

                    agreement.CompanyId = company.CompanyId;
                    agreement.AgreementDate = DateTime.Now;
                    agreement.Status = "1";
                    agreement.Type = "2";

                    foreach (var icmalListesi in result.IcmalListesi)
                    {
                        var mutabakatIcmal = mutabakatAgreementIcmalRes.IcmalListesi.Where(x => x.IcmalNo == icmalListesi.TazminatPaketNo).FirstOrDefault();
                        if (mutabakatIcmal != null)
                        {
                            agreement.Description += icmalListesi.IcmalDurumu == mutabakatIcmal.IcmalDurumu ? "" : " Icmal Durumu Farklı Imece Icmal Durumu : " + icmalListesi.IcmalDurumu + " Diger Sirket Icmal Durum : " + mutabakatIcmal.IcmalDurumu + " ";
                            agreement.Description += icmalListesi.OdemeTarihi == mutabakatIcmal.OdemeTarihi ? "" : " Odeme Tarihi Farklı Imecede Odeme Tarihi : " + icmalListesi.OdemeTarihi + " Diger Sirket Odeme Tarihi : " + mutabakatIcmal.OdemeTarihi + " ";
                            agreement.Description += icmalListesi.ToplamFaturaTutar == mutabakatIcmal.ToplamTutar ? "" : " Toplam Tutar Farklı Imecede Toplam Tutar : " + icmalListesi.ToplamFaturaTutar + " Diger Sirket Toplam Tutar : " + mutabakatIcmal.ToplamTutar + " ";
                        }
                        else
                        {
                            agreement.Description += "Diger Sirkette Bu Icmal No Bulunamamistir : " + icmalListesi.TazminatPaketNo;
                        }

                    }
                    foreach (var resIcmalListesi in mutabakatAgreementIcmalRes.IcmalListesi)
                    {
                        var mutabakatIcmalImece = result.IcmalListesi.Where(x => x.TazminatPaketNo == resIcmalListesi.IcmalNo).FirstOrDefault();
                        if (mutabakatIcmalImece != null)
                        {
                            agreement.Description += mutabakatIcmalImece.IcmalDurumu == resIcmalListesi.IcmalDurumu ? "" : " Icmal Durumu Farklı Imece Icmal Durumu : " + mutabakatIcmalImece.IcmalDurumu + " Diger Sirket Icmal Durum : " + resIcmalListesi.IcmalDurumu + " ";
                            agreement.Description += mutabakatIcmalImece.OdemeTarihi == resIcmalListesi.OdemeTarihi ? "" : " Odeme Tarihi Farklı Imecede Odeme Tarihi : " + mutabakatIcmalImece.OdemeTarihi + " Diger Sirket Odeme Tarihi : " + resIcmalListesi.OdemeTarihi + " ";
                            agreement.Description += mutabakatIcmalImece.ToplamFaturaTutar == resIcmalListesi.ToplamTutar ? "" : " Toplam Tutar Farklı Imecede Toplam Tutar : " + mutabakatIcmalImece.ToplamFaturaTutar + " Diger Sirket Toplam Tutar : " + resIcmalListesi.ToplamTutar + " ";
                        }
                        else
                        {
                            agreement.Description += "Imece Sirketinde Bu Icmal No Bulunamamistir : " + mutabakatIcmalImece.TazminatPaketNo;
                        }
                    }

                }

                if (agreement.Description != null)
                {
                    IcmalMutabakatDetayReq icmalMutabakatDetayReq = new IcmalMutabakatDetayReq();
                    icmalMutabakatDetayReq.icmalNo = "321";
                    icmalMutabakatDetayReq.SirketKod = Convert.ToInt32(company.CompanyId);

                    var resultIcmalDetay = TazminatPaketMutabakatDetay(company.Value, icmalMutabakatDetayReq, corrId);

                    #region mutabakatIcmalDetayRes
                    mutabakatAgreementIcmalDetayRes mutabakatAgreementIcmalDetayRes = new mutabakatAgreementIcmalDetayRes();
                    mutabakatAgreementIcmalDetayRes.TazminatListesi = new List<TazminatDetayAgreement>();
                    foreach (var tazminatListesiRes in resultIcmalDetay.TazminatListesi)
                    {

                        mutabakatAgreementIcmalDetayRes.TazminatListesi.Add(new TazminatDetayAgreement
                        {
                            HasarDurum = tazminatListesiRes.HasarDurum,
                            SirketTutar = tazminatListesiRes.SirketTutar,
                            TazminatNo = tazminatListesiRes.TazminatNo
                        });
                    }

                    #endregion


                    if (resultIcmalDetay.SonucKod == "1")
                    {
                        foreach (var tazminatListesi in resultIcmalDetay.TazminatListesi)
                        {
                            var mutabakatDetayIcmal = mutabakatAgreementIcmalDetayRes.TazminatListesi.Where(x => x.TazminatNo == tazminatListesi.TazminatNo).FirstOrDefault();
                            if (mutabakatDetayIcmal != null)
                            {
                                agreement.Description += tazminatListesi.SirketTutar == mutabakatDetayIcmal.SirketTutar ? "" : " Sirket Tutari Farklı Imece Sirket Tutarı : " + tazminatListesi.SirketTutar + " Diger Sirket Tutari : " + mutabakatDetayIcmal.SirketTutar;
                                agreement.Description += tazminatListesi.HasarDurum == mutabakatDetayIcmal.HasarDurum ? "" : " Hasar Durumu Farklı Imece Hasar Durumu : " + tazminatListesi.HasarDurum + " Diger Sirket Hasar Durumu : " + mutabakatDetayIcmal.HasarDurum + " ";
                            }
                            else
                            {
                                agreement.Description += "Diger Sirkette " + tazminatListesi.TazminatNo + " No'lu Tazminat No Bulunamamistir.";
                            }
                        }
                        foreach (var tazminatListesiRes in mutabakatAgreementIcmalDetayRes.TazminatListesi)
                        {
                            var mutabakatDetayIcmalImece = mutabakatAgreementIcmalDetayRes.TazminatListesi.Where(x => x.TazminatNo == tazminatListesiRes.TazminatNo).FirstOrDefault();
                            if (mutabakatDetayIcmalImece != null)
                            {
                                agreement.Description += mutabakatDetayIcmalImece.SirketTutar == mutabakatDetayIcmalImece.SirketTutar ? "" : " Sirket Tutari Farklı Imece Sirket Tutarı : " + mutabakatDetayIcmalImece.SirketTutar + " Diger Sirket Tutari : " + mutabakatDetayIcmalImece.SirketTutar;
                                agreement.Description += mutabakatDetayIcmalImece.HasarDurum == mutabakatDetayIcmalImece.HasarDurum ? "" : " Hasar Durumu Farklı Imece Hasar Durumu : " + mutabakatDetayIcmalImece.HasarDurum + " Diger Sirket Hasar Durumu : " + mutabakatDetayIcmalImece.HasarDurum + " ";
                            }
                            else
                            {
                                agreement.Description += "Imece Sirketinde " + tazminatListesiRes.TazminatNo + " No'lu Tazminat No Bulunamamistir.";
                            }
                        }
                    }
                }

            }

            var insertResult = new GenericRepository<Agreement>().Insert(agreement, this.Token);
            if (agreement.Description == "")
            {
                agreement.Description = "Veriler Mutabıktır.";
                response.resultWs = agreement.Description;
            }
            else
            {
                response.resultWs = agreement.Description;
            }

            return response;
        }
        #endregion

        private void RollBackProxySrv(Int64 policyId = 0, Int64 endorsementId = 0, List<Int64> insuredIdList = null, Int64 insurerId = 0, decimal? policyPremium = null, long? lastEndorsementId = null, bool insuredIsActive = false, bool policyIsActive = false)
        {
            try
            {
                if (policyId > 0)
                {
                    GenericRepository<Policy> repo = new GenericRepository<Policy>();
                    Policy policy = repo.FindById(policyId);
                    if (policy != null)
                    {
                        if (policyPremium == null && lastEndorsementId == null)
                        {
                            policy.Status = ((int)PolicyStatus.SILINDI).ToString();
                        }
                        else
                        {
                            policy.Status = policyIsActive ? ((int)PolicyStatus.TANZIMLI).ToString() : ((int)PolicyStatus.SILINDI).ToString();
                            policy.LastEndorsementId = lastEndorsementId;
                            policy.Premium = policyPremium;
                        }
                        repo.Update(policy);
                    }
                }
                if (endorsementId > 0)
                {
                    GenericRepository<Endorsement> repo = new GenericRepository<Endorsement>();

                    Endorsement endorsement = repo.FindById(endorsementId);
                    if (endorsement != null)
                    {
                        endorsement.Status = ((int)Status.SILINDI).ToString();
                        repo.Update(endorsement);
                    }
                }
                if (insuredIdList != null && insuredIdList.Count > 0)
                {
                    GenericRepository<Insured> repo = new GenericRepository<Insured>();

                    foreach (var insuredId in insuredIdList)
                    {

                        Insured insured = new GenericRepository<Insured>().FindById(insuredId);
                        if (insured != null)
                        {
                            insured.TotalPremium = null;
                            insured.Premium = null;
                            insured.InitialPremium = null;
                            insured.Status = ((int)Status.SILINDI).ToString();
                            repo.Update(insured);

                        }
                    }
                }
                if (insurerId > 0)
                {
                    GenericRepository<Contact> repo = new GenericRepository<Contact>();

                    Contact insurer = new GenericRepository<Contact>().FindById(insurerId);
                    if (insurer != null)
                    {
                        insurer.Status = insuredIsActive ? ((int)Status.AKTIF).ToString() : ((int)Status.SILINDI).ToString();
                        repo.Update(insurer);

                    }
                }
            }
            catch (Exception)
            {
            }
        }
    }
}

