﻿using Protein.Business.Concrete.Services;
using Protein.Common.Dto.ImportObjects;
using Protein.Common.Enums;
using Protein.Common.Extensions;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.InsuranceCompanies.Abstract;
using Protein.Data.ExternalServices.InsuranceCompanies.Concrete;

using Protein.Data.ExternalServices;
using Protein.Data.ExternalServices.KatilimSecurityService;

//using Protein.Data.ExternalServices.HDIHealthService;

//using Protein.Data.ExternalServices.KatilimHealthServicePROD;
//using Protein.Data.ExternalServices.KatilimSecurityServicePROD;
using Protein.Data.ExternalServices.Log;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Data.ExternalServices.InsuranceCompanies
{
    public class ProxyServiceClient
    {
        public string KatilimGetAuthenticationKey()
        {
            string authKey = "";
            SecurityServiceClient securityService = new SecurityServiceClient();
            var result = securityService.GetAuthenticationKey("9CDE40F6327BC1ED744BD1D2D09D79F4", "TPAWS", "TPAWS", ref authKey);
            //var result = securityService.GetAuthenticationKey("2F66C258DDE5EB33BC0433B9217BEF11", "TPAWS", "TPAWS", ref authKey);
            return authKey;
        }
        private void CreateLog(IntegrationLog log, string responseXml, string code, string desc, string logType = "", string HttpStatus = "")
        {
            log.HttpStatus = HttpStatus.IsNull() ? ((int)HttpStatusCode.NotFound).ToString() : HttpStatus;
            log.Type = logType.IsNull() ? ((int)LogType.FAILED).ToString() : logType;
            log.Response = responseXml;
            log.ResponseStatusCode = code;
            log.ResponseStatusDesc = desc;
            log.WsRequestDate = DateTime.Now;
            IntegrationLogHelper.AddToLog(log);
        }





        //Hasar Onay
        public CommonProxyRes ClaimApproval(ClaimStateUpdateReq req)
        {
            CommonProxyRes result = new CommonProxyRes();

            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.WsMethod = currentMethod.Name;
            log.ClaimId = req.ClaimId;
            log.Request = req.ToXML(true);
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.WsRequestDate = DateTime.Now;
            log.Type = ((int)LogType.INCOMING).ToString();
            IntegrationLogHelper.AddToLog(log);

            try
            {
                Claim existClaim = new GenericRepository<Claim>().FindBy($"ID = {req.ClaimId} AND STATUS='{((int)ClaimStatus.PROZIYON_ONAY).ToString()}'", fetchDeletedRows: true).FirstOrDefault();
                if (existClaim.CompanyClaimId == null || existClaim.CompanyClaimId < 1)
                {
                    result.Code = "0";
                    result.Error = new string[] { "Sigorta şirketi claim id bilgisi bulunamadı!" };
                    result.IsOk = false;

                    CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                    return result;
                }
                if (existClaim.Status != ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                {
                    result.Code = "0";
                    result.Error = new string[] { "Hasar Provizyon Olmalı!" };
                    result.IsOk = false;

                    CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                    return result;
                }

                ClaimDto claimDto = new ClaimDto();
                if ((CompanyEnum)existClaim.CompanyId == CompanyEnum.KATILIM_EMEKLILIK)
                {
                    string AuthenticationKey = KatilimGetAuthenticationKey();

                    if (!AuthenticationKey.IsNull())
                    {
                        ClaimService claimService = new ClaimService();
                        claimDto = claimService.ClaimToCommon(req.ClaimId, ((int)ClaimStatus.PROZIYON_ONAY).ToString());
                        if (claimDto == null)
                        {
                            result.Error = new string[] { "HASAR BİLGİSİ BULUNAMADI!" };
                            result.IsOk = false;
                            result.Code = "0";

                            CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                            return result;

                        }

                        KatilimHealthService.ClaimStateUpdateApprovalResult res = new KatilimHealthService.ClaimStateUpdateApprovalResult();
                        res = FillClaimStateUpdateApprovalInputKatilim(claimDto, AuthenticationKey);
                        result.Error = res.ServiceResult.ErrorList;
                        result.IsOk = res.ServiceResult.IsOk;
                        if (!result.IsOk)
                        {
                            result.Code = "0";

                            CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));

                            return result;
                        }
                    }
                    else
                    {
                        result.Code = "0";
                        result.Error = new string[] { "AuthenticationKey Alınamadı!" };
                        result.IsOk = false;

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));

                        return result;
                    }
                }
                else if ((CompanyEnum)existClaim.CompanyId == CompanyEnum.HDI || (CompanyEnum)existClaim.CompanyId == CompanyEnum.HDI_TEST)
                {
                    ClaimService claimService = new ClaimService();
                    claimDto = claimService.ClaimToCommon(req.ClaimId, ((int)ClaimStatus.PROZIYON_ONAY).ToString());
                    if (claimDto == null)
                    {
                        result.Error = new string[] { "HASAR BİLGİSİ BULUNAMADI!" };
                        result.IsOk = false;
                        result.Code = "0";

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                        return result;

                    }
                    HDIHealthService.ClaimStateUpdateApprovalResult res = new HDIHealthService.ClaimStateUpdateApprovalResult();
                    res = FillClaimStateUpdateApprovalInputHDI(claimDto, "1");
                    result.Error = res.ServiceResult.ErrorList.ToArray();
                    result.IsOk = res.ServiceResult.IsOk;
                    if (!result.IsOk)
                    {
                        result.Code = "0";

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));

                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = "0";
                result.Error = new string[] { ex.Message };
                result.IsOk = false;

                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));

                return result;
            }
            finally
            {
                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error), ((int)LogType.COMPLETED).ToString(), ((int)HttpStatusCode.OK).ToString());
            }
            return result;
        }
        //Hasar İptal
        public CommonProxyRes ClaimCancellation(ClaimStateUpdateReq req)
        {
            CommonProxyRes result = new CommonProxyRes();

            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog();
            log.CorrelationId = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.ClaimId = req.ClaimId;
            log.WsMethod = currentMethod.Name;
            log.Request = req.ToXML(true);
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.WsRequestDate = DateTime.Now;
            log.Type = ((int)LogType.INCOMING).ToString();
            IntegrationLogHelper.AddToLog(log);

            try
            {
                Claim existClaim = new GenericRepository<Claim>().FindBy($"ID = {req.ClaimId}").FirstOrDefault();
                if (existClaim == null || existClaim.CompanyClaimId < 1)
                {
                    result.Error = new string[] { "Sigorta şirketi claim id bilgisi bulunamadı!" };
                    result.IsOk = false;
                    result.Code = "0";

                    CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                    return result;
                }
                if (existClaim.Status != ((int)ClaimStatus.IPTAL).ToString())
                {
                    result.Error = new string[] { "Hasar Durumu İptal Olmalı!" };
                    result.IsOk = false;
                    result.Code = "0";

                    CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                    return result;

                }
                ClaimDto claimDto = new ClaimDto();

                if ((CompanyEnum)existClaim.CompanyId == CompanyEnum.KATILIM_EMEKLILIK)
                {
                    string AuthenticationKey = KatilimGetAuthenticationKey();

                    if (!AuthenticationKey.IsNull())
                    {
                        ClaimService claimService = new ClaimService();
                        claimDto = claimService.ClaimToCommon(req.ClaimId, ((int)ClaimStatus.IPTAL).ToString());
                        if (claimDto == null)
                        {
                            result.Error = new string[] { "HASAR BİLGİSİ BULUNAMADI!" };
                            result.IsOk = false;
                            result.Code = "0";

                            CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                            return result;
                        }

                        KatilimHealthService.ClaimStateUpdateCancellationResult res = new KatilimHealthService.ClaimStateUpdateCancellationResult();
                        res = FillClaimStateUpdateCancellationInputKatilim(claimDto, AuthenticationKey);
                        result.Error = res.ServiceResult.ErrorList;
                        result.IsOk = res.ServiceResult.IsOk;
                        if (!result.IsOk)
                        {
                            result.Code = "0";

                            CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                            return result;
                        }
                    }
                    else
                    {
                        result.Error = new string[] { "AuthenticationKey Alınamadı!" };
                        result.IsOk = false;
                        result.Code = "0";

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                        return result;
                    }
                }
                else if ((CompanyEnum)existClaim.CompanyId == CompanyEnum.HDI || (CompanyEnum)existClaim.CompanyId == CompanyEnum.HDI_TEST)
                {
                    ClaimService claimService = new ClaimService();
                    claimDto = claimService.ClaimToCommon(req.ClaimId, ((int)ClaimStatus.IPTAL).ToString());
                    if (claimDto == null)
                    {
                        result.Error = new string[] { "HASAR BİLGİSİ BULUNAMADI!" };
                        result.IsOk = false;
                        result.Code = "0";

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                        return result;
                    }

                    HDIHealthService.ClaimStateUpdateCancellationResult res = new HDIHealthService.ClaimStateUpdateCancellationResult();
                    res = FillClaimStateUpdateCancellationInputHDI(claimDto, "1");
                    result.Error = res.ServiceResult.ErrorList.ToArray();
                    result.IsOk = res.ServiceResult.IsOk;
                    if (!result.IsOk)
                    {
                        result.Code = "0";

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                        return result;
                    }
                }
                else
                {
                    result.Error = new string[] { "Şirket Bilgisi Bulunamadı!" };
                    result.IsOk = false;
                    result.Code = "0";

                    CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.Code = "0";
                result.Error = new string[] { ex.Message };
                result.IsOk = false;

                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));

                return result;
            }
            finally
            {
                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error), ((int)LogType.COMPLETED).ToString(), ((int)HttpStatusCode.OK).ToString());
            }
            return result;
        }
        //Hasar Güncelleme (Teminat)
        public CommonProxyRes ClaimUpdate(ClaimStateUpdateReq req)
        {
            CommonProxyRes result = new CommonProxyRes();
            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog();
            log.CorrelationId = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.WsMethod = currentMethod.Name;
            log.Request = req.ToXML(true);
            log.ClaimId = req.ClaimId;
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.WsRequestDate = DateTime.Now;
            log.Type = ((int)LogType.INCOMING).ToString();
            IntegrationLogHelper.AddToLog(log);

            try
            {

                log.Type = ((int)LogType.WAITING).ToString();
                log.WsRequestDate = DateTime.Now;
                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                log.ResponseStatusCode = "200";
                IntegrationLogHelper.AddToLog(log);

                Claim existClaim = new GenericRepository<Claim>().FindBy($"ID = {req.ClaimId}").FirstOrDefault();
                if (existClaim == null)
                {
                    result.Error = new string[] { "Claim bulunamadı!" };
                    result.IsOk = false;
                    result.Code = "0";

                    CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                    return result;
                }
                if (existClaim.CompanyClaimId == null || existClaim.CompanyClaimId < 1)
                {
                    result.Error = new string[] { "Sigorta şirketi claim id bilgisi bulunamadı!" };
                    result.IsOk = false;
                    result.Code = "0";

                    CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                    return result;
                }

                if ((CompanyEnum)existClaim.CompanyId == CompanyEnum.KATILIM_EMEKLILIK)
                {
                    string authKey = KatilimGetAuthenticationKey();
                    if (authKey.IsNull())
                    {
                        result.Error = new string[] { "AuthenticationKey Alınamadı!" };
                        result.IsOk = false;
                        result.Code = "0";

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                        return result;
                    }

                    KatilimHealthService.HealthTPAClient client = new KatilimHealthService.HealthTPAClient();
                    KatilimHealthService.ClaimAssuranceUpdateInput input = new KatilimHealthService.ClaimAssuranceUpdateInput();
                    input.FileNo = existClaim.CompanyClaimNo;
                    input.AuthenticationKey = authKey;
                    input.TEMINATLAR = new KatilimHealthService.TAB_HASAR_IHBAR_TEMINATLAR();
                    input.CompanyCode = existClaim.CompanyId.ToString();

                    List<V_ClaimCoverage> claimCoverage = new GenericRepository<V_ClaimCoverage>().FindBy($"CLAIM_ID = {req.ClaimId}", orderby: "");
                    if (claimCoverage.Count > 0)
                    {
                        List<KatilimHealthService.OBJ_HASAR_IHBAR_TEMINATLAR> lstObjHasar = new List<KatilimHealthService.OBJ_HASAR_IHBAR_TEMINATLAR>();

                        foreach (V_ClaimCoverage item in claimCoverage)
                        {
                            V_CoverageParameter coverParam = new GenericRepository<V_CoverageParameter>().FindBy($"COVERAGE_ID = {item.CoverageId} and  KEY = 'COVERAGE_CODE' AND SYSTEM_TYPE = {(int)SystemType.KATILIM_TEST}", orderby: "").FirstOrDefault();
                            V_ClaimBill claimBill = new GenericRepository<V_ClaimBill>().FindBy($"CLAIM_ID = {req.ClaimId}", orderby: "").FirstOrDefault();

                            KatilimHealthService.OBJ_HASAR_IHBAR_TEMINATLAR objHasarIhbarTem = new KatilimHealthService.OBJ_HASAR_IHBAR_TEMINATLAR
                            {
                                TEMINAT_KOD = coverParam.Value,
                                TUTAR = item.Paid,
                                FATURA_TARIH = claimBill.BILL_DATE,
                                DOVIZ_KOD = claimBill.EXCHANGE_RATE_ID.ToString()
                            };
                            lstObjHasar.Add(objHasarIhbarTem);
                        }
                        input.TEMINATLAR.Value = lstObjHasar.ToArray();
                        KatilimHealthService.ClaimAssuranceUpdateResult res = client.ClaimAssuranceUpdate(input);
                        result.IsOk = res.ServiceResult.IsOk;
                        result.Error = res.ServiceResult.ErrorList;
                        if (!result.IsOk)
                        {
                            result.Code = "0";

                            CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                            return result;
                        }
                    }
                }
                else if ((CompanyEnum)existClaim.CompanyId == CompanyEnum.HDI || (CompanyEnum)existClaim.CompanyId == CompanyEnum.HDI_TEST)
                {
                    HDIHealthService.HealthTPAServiceSoapClient client = new HDIHealthService.HealthTPAServiceSoapClient();
                    HDIHealthService.ClaimAssuranceUpdateInput input = new HDIHealthService.ClaimAssuranceUpdateInput
                    {
                        FileNo = req.ClaimId.ToString(),
                        AuthenticationKey = "1",
                        TEMINATLAR = new HDIHealthService.TAB_HASAR_IHBAR_TEMINATLAR(),
                        CompanyCode = existClaim.CompanyId.ToString()
                    };

                    List<V_ClaimCoverage> claimCoverage = new GenericRepository<V_ClaimCoverage>().FindBy($"CLAIM_ID = {req.ClaimId}", orderby: "");
                    if (claimCoverage.Count > 0)
                    {
                        List<HDIHealthService.OBJ_HASAR_IHBAR_TEMINATLAR> lstObjHasar = new List<HDIHealthService.OBJ_HASAR_IHBAR_TEMINATLAR>();

                        V_ClaimBill claimBill = new GenericRepository<V_ClaimBill>().FindBy($"CLAIM_ID = {req.ClaimId}", orderby: "").FirstOrDefault();

                        foreach (V_ClaimCoverage item in claimCoverage)
                        {
                            //V_CoverageParameter coverParam = new GenericRepository<V_CoverageParameter>().FindBy($"COVERAGE_ID = {item.CoverageId} and  KEY = 'COVERAGE_CODE' AND SYSTEM_TYPE = {(int)SystemType.KATILIM_TEST}", orderby: "").FirstOrDefault();

                            HDIHealthService.OBJ_HASAR_IHBAR_TEMINATLAR objHasarIhbarTem = new HDIHealthService.OBJ_HASAR_IHBAR_TEMINATLAR
                            {
                                TEMINAT_KOD = item.CoverageType == ((int)CoverageType.AYAKTA).ToString() ? "AT" : "YT",
                                TUTAR = item.Paid,
                                FATURA_TARIH = claimBill?.BILL_DATE,
                                DOVIZ_KOD = claimBill?.EXCHANGE_RATE_ID.ToString()
                            };
                            lstObjHasar.Add(objHasarIhbarTem);
                        }
                        input.TEMINATLAR.Value = lstObjHasar.ToArray();
                        HDIHealthService.ClaimAssuranceUpdateResult res = client.ClaimAssuranceUpdate(input);
                        result.IsOk = res.ServiceResult.IsOk;
                        result.Error = res.ServiceResult.ErrorList.ToArray();
                        log.Response = res.ToXML(true);
                        log.Request = input.ToXML(true);
                        if (!result.IsOk)
                        {
                            result.Code = "0";

                            CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                            return result;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.Code = "0";
                result.Error = new string[] { ex.Message };
                result.IsOk = false;

                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));

                return result;
            }
            finally
            {
                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error), ((int)LogType.COMPLETED).ToString(), ((int)HttpStatusCode.OK).ToString());
            }
            return result;
        }
        //Hasar Gönderim (Giriş)
        public CommonProxyRes ClaimNotice(ClaimStateUpdateReq req)
        {
            CommonProxyRes result = new CommonProxyRes();

            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.WsMethod = currentMethod.Name;
            log.ClaimId = req.ClaimId;
            log.Request = req.ToXML(true);
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.WsRequestDate = DateTime.Now;
            log.Type = ((int)LogType.INCOMING).ToString();
            //IntegrationLogHelper.AddToLog(log);

            try
            {
                V_Claim claim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID = {req.ClaimId}", orderby: "", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                if (claim == null)
                {
                    result.Error = new string[] { "Claim bulunamadı!" };
                    result.IsOk = false;
                    result.Code = "0";

                    CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                    return result;
                }
                else if (claim.COMPANY_CLAIM_ID != null)
                {
                    ProxyServiceClient proxyServiceClient = new ProxyServiceClient();
                    var res = proxyServiceClient.ClaimUpdate(req);

                    result = res;

                    return result;
                }
                if ((CompanyEnum)claim.COMPANY_ID == CompanyEnum.KATILIM_EMEKLILIK)
                {
                    string authKey = KatilimGetAuthenticationKey();
                    if (authKey.IsNull())
                    {
                        result.Error = new string[] { "AuthenticationKey Alınamadı!" };
                        result.IsOk = false;
                        result.Code = "0";

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                        return result;
                    }

                    KatilimHealthService.HealthTPAClient client = new KatilimHealthService.HealthTPAClient();
                    KatilimHealthService.ClaimNoticeInput input = new KatilimHealthService.ClaimNoticeInput();
                    input.AuthenticationKey = authKey;
                    input.ClaimDatas = new KatilimHealthService.OBJ_HASAR_IHBAR_TPA_WS();

                    input.ClaimDatas.IHBAR_TARIH = claim.NOTICE_DATE;
                    input.ClaimDatas.KURUM_NO = claim.PROVIDER_ID.ToString();
                    input.ClaimDatas.POLICE_NO = claim.POLICY_NUMBER==null ? null : (decimal?)decimal.Parse(claim.POLICY_NUMBER.ToString());
                    input.ClaimDatas.SIRKET_KOD = claim.COMPANY_ID.ToString();
                    input.ClaimDatas.MEDULA_TAKIP_NO = claim.MEDULA_NO;
                    input.ClaimDatas.OLAY_TARIH = claim.CLAIM_DATE;

                    input.ClaimDatas.SIGORTALI_NO = claim.INSURED_ID;
                    input.ClaimDatas.YENILEME_NO = claim.RENEWAL_NO;
                    input.ClaimDatas.TPA_HASAR_NO = claim.CLAIM_ID;
                    input.ClaimDatas.TEMINATLAR = new KatilimHealthService.TAB_HASAR_IHBAR_TEMINATLAR();

                    List<V_ClaimCoverage> claimCoverage = new GenericRepository<V_ClaimCoverage>().FindBy($"CLAIM_ID = {req.ClaimId}", orderby: "");
                    if (claimCoverage.Count > 0)
                    {
                        List<KatilimHealthService.OBJ_HASAR_IHBAR_TEMINATLAR> lstObjHasar = new List<KatilimHealthService.OBJ_HASAR_IHBAR_TEMINATLAR>();

                        foreach (V_ClaimCoverage item in claimCoverage)
                        {
                            V_CoverageParameter coverParam = new GenericRepository<V_CoverageParameter>().FindBy($"COVERAGE_ID = {item.CoverageId} and  KEY = 'COVERAGE_CODE' AND SYSTEM_TYPE = {(int)SystemType.PORTTSS}", orderby: "").FirstOrDefault();
                            V_ClaimBill claimBill = new GenericRepository<V_ClaimBill>().FindBy($"CLAIM_ID = {req.ClaimId}", orderby: "").FirstOrDefault();

                            KatilimHealthService.OBJ_HASAR_IHBAR_TEMINATLAR objHasarIhbarTem = new KatilimHealthService.OBJ_HASAR_IHBAR_TEMINATLAR
                            {
                                TEMINAT_KOD = coverParam?.Value,
                                TUTAR = item.Paid,
                                FATURA_TARIH = claimBill?.BILL_DATE,
                                DOVIZ_KOD = claimBill?.CURRENCY_TYPE.ToString()
                            };
                            lstObjHasar.Add(objHasarIhbarTem);
                        }
                        input.ClaimDatas.TEMINATLAR.Value = lstObjHasar.ToArray();
                    }

                    KatilimHealthService.ClaimNoticeResult res = client.ClaimNotice(input);
                    if (res != null)
                    {
                        result.Error = res.ServiceResult.ErrorList;
                        result.IsOk = res.ServiceResult.IsOk;
                        if (res.ServiceResult.IsOk)
                        {
                            Claim claimUpdate = new GenericRepository<Claim>().FindById(req.ClaimId);
                            claimUpdate.CompanyClaimId = res.FileNo;

                            SpResponse spResponseClaim = new GenericRepository<Claim>().Insert(claimUpdate);
                            if (spResponseClaim.Code != "100")
                            {
                                result.Error = new string[] { "Company Claim Id Kaydedilemedi!", spResponseClaim.Message };
                                result.IsOk = false;
                                result.Code = "0";

                                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                                return result;
                            }

                        }
                        else
                        {
                            result.Code = "0";

                            CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                            return result;
                        }
                    }
                    else
                    {
                        result.Error = new string[] { "Servis Cevabı Boş!" };
                        result.IsOk = false;
                        result.Code = "0";

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                        return result;
                    }
                }
                else if ((CompanyEnum)claim.COMPANY_ID == CompanyEnum.HDI || (CompanyEnum)claim.COMPANY_ID == CompanyEnum.HDI_TEST)
                {
                    HDIHealthService.HealthTPAServiceSoapClient client = new HDIHealthService.HealthTPAServiceSoapClient();
                    HDIHealthService.ClaimNoticeInput input = new HDIHealthService.ClaimNoticeInput();
                    input.AuthenticationKey = "1";
                    input.ClaimDatas = new HDIHealthService.OBJ_HASAR_IHBAR_TPA_WS
                    {
                        IHBAR_TARIH = claim.NOTICE_DATE,
                        KURUM_NO = claim.PROVIDER_ID.ToString(),
                        POLICE_NO = claim.POLICY_NUMBER == null ? null : (decimal?)decimal.Parse(claim.POLICY_NUMBER.ToString()),
                    SIRKET_KOD = claim.COMPANY_ID.ToString(),
                        MEDULA_TAKIP_NO = claim.MEDULA_NO,
                        OLAY_TARIH = claim.CLAIM_DATE,
                        SIGORTALI_NO = (claim.COMPANY_INSURED_NO.IsInt64() ? long.Parse(claim.COMPANY_INSURED_NO) : claim.INSURED_ID),
                        YENILEME_NO = claim.RENEWAL_NO,
                        TPA_HASAR_NO = claim.CLAIM_ID,
                        TEMINATLAR = new HDIHealthService.TAB_HASAR_IHBAR_TEMINATLAR()
                    };

                    List<V_ClaimCoverage> claimCoverage = new GenericRepository<V_ClaimCoverage>().FindBy($"CLAIM_ID = {req.ClaimId}", orderby: "");
                    if (claimCoverage.Count > 0)
                    {
                        List<HDIHealthService.OBJ_HASAR_IHBAR_TEMINATLAR> lstObjHasar = new List<HDIHealthService.OBJ_HASAR_IHBAR_TEMINATLAR>();
                        V_ClaimBill claimBill = new GenericRepository<V_ClaimBill>().FindBy($"CLAIM_ID = {req.ClaimId}", orderby: "").FirstOrDefault();

                        foreach (V_ClaimCoverage item in claimCoverage)
                        {
                            //V_CoverageParameter coverParam = new GenericRepository<V_CoverageParameter>().FindBy($"COVERAGE_ID = {item.CoverageId} and  KEY = 'COVERAGE_CODE' AND SYSTEM_TYPE = {(int)SystemType.PORTTSS}", orderby: "").FirstOrDefault();

                            HDIHealthService.OBJ_HASAR_IHBAR_TEMINATLAR objHasarIhbarTem = new HDIHealthService.OBJ_HASAR_IHBAR_TEMINATLAR
                            {
                                TEMINAT_KOD = item.CoverageType == ((int)CoverageType.AYAKTA).ToString() ? "AT" : "YT",
                                TUTAR = item.Paid,
                                FATURA_TARIH = claimBill?.BILL_DATE,
                                DOVIZ_KOD = claimBill?.EXCHANGE_RATE_ID.ToString()
                            };
                            lstObjHasar.Add(objHasarIhbarTem);
                        }
                        input.ClaimDatas.TEMINATLAR.Value = lstObjHasar.ToArray();
                    }

                    HDIHealthService.ClaimNoticeResult res = client.ClaimNotice(input);

                    log.Request = input.ToXML(true);
                    log.Response = res.ToXML(true);
                    if (res != null)
                    {
                        result.Error = res.ServiceResult.ErrorList.ToArray();
                        result.IsOk = res.ServiceResult.IsOk;
                        if (res.ServiceResult.IsOk || res.FileNo > 0)
                        {
                            Claim claimUpdate = new GenericRepository<Claim>().FindById(req.ClaimId);
                            claimUpdate.CompanyClaimId = res.FileNo;

                            SpResponse spResponseClaim = new GenericRepository<Claim>().Insert(claimUpdate);
                            if (spResponseClaim.Code != "100" && spResponseClaim.Code != "1")
                            {
                                result.Error = new string[] { "Company Claim Id Kaydedilemedi!", spResponseClaim.Message };
                                result.IsOk = false;
                                result.Code = "0";

                                CreateLog(log, res.ToXML(true), result.Code, string.Join(" - ", result.Error));
                                return result;
                            }

                        }
                        else
                        {
                            result.Code = "0";

                            CreateLog(log, res.ToXML(true), result.Code, string.Join(" - ", result.Error));
                            return result;
                        }
                    }
                    else
                    {
                        result.Error = new string[] { "Servis Cevabı Boş!" };
                        result.IsOk = false;
                        result.Code = "0";

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                        return result;
                    }
                }
                else
                {
                    result.Error = new string[] { "Sigortalı Şirketi Bilgisi Bulunamadı!" };
                    result.IsOk = false;
                    result.Code = "0";

                    CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                    return result;
                }
                CreateLog(log, log.Response, result.Code, (result.Error != null ? string.Join(" - ", result.Error) : ""), ((int)LogType.COMPLETED).ToString(), ((int)HttpStatusCode.OK).ToString());
            }
            catch (Exception ex)
            {
                result.Code = "0";
                result.Error = new string[] { ex.Message };
                result.IsOk = false;

                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));

                return result;
            }
            return result;
        }
        //Hasar Ret
        public CommonProxyRes ClaimRejection(ClaimStateUpdateReq req)
        {
            CommonProxyRes result = new CommonProxyRes();

            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog();
            log.CorrelationId = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.WsMethod = currentMethod.Name;
            log.ClaimId = req.ClaimId;
            log.Request = req.ToXML(true);
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.WsRequestDate = DateTime.Now;
            log.Type = ((int)LogType.INCOMING).ToString();
            IntegrationLogHelper.AddToLog(log);

            try
            {
                Claim existClaim = new GenericRepository<Claim>().FindBy($"ID = {req.ClaimId}").FirstOrDefault();
                if (existClaim == null || existClaim.CompanyClaimId < 1)
                {
                    result.Error = new string[] { "Sigorta şirketi claim id bilgisi bulunamadı!" };
                    result.IsOk = false;
                    result.Code = "0";

                    CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                    return result;
                }
                if (existClaim.Status != ((int)ClaimStatus.RET).ToString())
                {
                    result.Error = new string[] { "Hasar Durumu Red Olmalı!" };
                    result.IsOk = false;
                    result.Code = "0";

                    CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                    return result;

                }
                ClaimDto claimDto = new ClaimDto();

                if ((CompanyEnum)existClaim.CompanyId == CompanyEnum.KATILIM_EMEKLILIK)
                {
                    string AuthenticationKey = KatilimGetAuthenticationKey();

                    if (!AuthenticationKey.IsNull())
                    {
                        ClaimService claimService = new ClaimService();
                        claimDto = claimService.ClaimToCommon(req.ClaimId, ((int)ClaimStatus.RET).ToString());
                        if (claimDto == null)
                        {
                            result.Error = new string[] { "HASAR BİLGİSİ BULUNAMADI!" };
                            result.IsOk = false;
                            result.Code = "0";

                            CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                            return result;
                        }

                        KatilimHealthService.ClaimStateUpdateRejectionResult res = new KatilimHealthService.ClaimStateUpdateRejectionResult();
                        res = FillClaimStateUpdateRejectionInputKatilim(claimDto, AuthenticationKey);
                        result.Error = res.ServiceResult.ErrorList;
                        result.IsOk = res.ServiceResult.IsOk;
                        if (!result.IsOk)
                        {
                            result.Code = "0";

                            CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                            return result;
                        }
                    }
                    else
                    {
                        result.Error = new string[] { "AuthenticationKey Alınamadı!" };
                        result.IsOk = false;
                        result.Code = "0";

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                        return result;
                    }
                }
                else if ((CompanyEnum)existClaim.CompanyId == CompanyEnum.HDI || (CompanyEnum)existClaim.CompanyId == CompanyEnum.HDI_TEST)
                {
                    ClaimService claimService = new ClaimService();
                    claimDto = claimService.ClaimToCommon(req.ClaimId, ((int)ClaimStatus.RET).ToString());
                    if (claimDto == null)
                    {
                        result.Error = new string[] { "HASAR BİLGİSİ BULUNAMADI!" };
                        result.IsOk = false;
                        result.Code = "0";

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                        return result;
                    }

                    HDIHealthService.ClaimStateUpdateRejectionResult res = new HDIHealthService.ClaimStateUpdateRejectionResult();
                    res = FillClaimStateUpdateRejectionInputHDI(claimDto, "1");
                    result.Error = res.ServiceResult.ErrorList.ToArray();
                    result.IsOk = res.ServiceResult.IsOk;
                    if (!result.IsOk)
                    {
                        result.Code = "0";

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = "0";
                result.Error = new string[] { ex.Message };
                result.IsOk = false;

                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));

                return result;
            }
            finally
            {
                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error), ((int)LogType.COMPLETED).ToString(), ((int)HttpStatusCode.OK).ToString());
            }
            return result;
        }
        //Hasar Bekleme
        public CommonProxyRes ClaimWait(ClaimStateUpdateReq req)
        {
            CommonProxyRes result = new CommonProxyRes();

            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog();
            log.CorrelationId = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.WsMethod = currentMethod.Name;
            log.ClaimId = req.ClaimId;
            log.Request = req.ToXML(true);
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.WsRequestDate = DateTime.Now;
            log.Type = ((int)LogType.INCOMING).ToString();
            IntegrationLogHelper.AddToLog(log);

            try
            {
                Claim existClaim = new GenericRepository<Claim>().FindBy($"ID = {req.ClaimId}").FirstOrDefault();
                if (existClaim == null || existClaim.CompanyClaimId < 1)
                {
                    result.Error = new string[] { "Sigorta şirketi claim id bilgisi bulunamadı!" };
                    result.IsOk = false;
                    result.Code = "0";

                    CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                    return result;
                }
                if (existClaim.Status != ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() && existClaim.Status != ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                {
                    result.Error = new string[] { "Hasar Durumu Bekleme Olmalı!" };
                    result.IsOk = false;
                    result.Code = "0";

                    CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                    return result;

                }
                ClaimDto claimDto = new ClaimDto();

                if ((CompanyEnum)existClaim.CompanyId == CompanyEnum.HDI || (CompanyEnum)existClaim.CompanyId == CompanyEnum.HDI_TEST)
                {
                    ClaimService claimService = new ClaimService();
                    claimDto = claimService.ClaimToCommon(req.ClaimId,existClaim.Status);
                    if (claimDto == null)
                    {
                        result.Error = new string[] { "HASAR BİLGİSİ BULUNAMADI!" };
                        result.IsOk = false;
                        result.Code = "0";

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                        return result;
                    }

                    HDIHealthService.ClaimStateUpdateWaitResult res = new HDIHealthService.ClaimStateUpdateWaitResult();
                    res = FillClaimStateUpdateWaitInputHDI(claimDto, "1");
                    result.Error = res.ServiceResult.ErrorList.ToArray();
                    result.IsOk = res.ServiceResult.IsOk;
                    if (!result.IsOk)
                    {
                        result.Code = "0";
                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = "0";
                result.Error = new string[] { ex.Message };
                result.IsOk = false;

                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));

                return result;
            }
            finally
            {
                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error), ((int)LogType.COMPLETED).ToString(), ((int)HttpStatusCode.OK).ToString());
            }
            return result;
        }
        //Hak Sahipliği Sorgula
        public CommonProxyRes PolicyState(PolicyStateReq req)
        {
            CommonProxyRes result = new CommonProxyRes();

            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog();
            log.CorrelationId = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.WsMethod = currentMethod.Name;
            log.Request = req.ToXML(true);
            log.PolicyId = req.PolicyNo.IsInt64() ? (long?)long.Parse(req.PolicyNo) : null;
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.WsRequestDate = DateTime.Now;
            log.Type = ((int)LogType.INCOMING).ToString();
            //IntegrationLogHelper.AddToLog(log);

            try
            {

                #region Validation
                if (!req.InsuredNo.IsInt64() || long.Parse(req.InsuredNo) <= 0)
                {
                    result.IsOk = false;
                    result.Error = new string[] { "Insured No boş geçilemez!" };
                    result.Code = "0";

                    CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                    return result;
                }
                if (!req.PolicyNo.IsInt64())
                {
                    result.IsOk = false;
                    result.Error = new string[] { "Poliçe No boş geçilemez!" };
                    result.Code = "0";

                    CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                    return result;
                }
                //V_InsuredParameter insuredParam = new GenericRepository<V_InsuredParameter>().FindBy($"INSURED_ID");
                #endregion

                if ((CompanyEnum)long.Parse(req.CompanyCode) == CompanyEnum.KATILIM_EMEKLILIK)
                {
                    if (string.IsNullOrEmpty(req.AuthenticationKey))
                        req.AuthenticationKey = KatilimGetAuthenticationKey();

                    if (!req.AuthenticationKey.IsNull())
                    {
                        KatilimHealthService.HealthTPAClient client = new KatilimHealthService.HealthTPAClient();
                        KatilimHealthService.PolicyStateResult policyStateResult = new KatilimHealthService.PolicyStateResult();

                        KatilimHealthService.PolicyStateInput input = new KatilimHealthService.PolicyStateInput
                        {
                            AuthenticationKey = req.AuthenticationKey,
                            CompanyCode = req.CompanyCode,
                            IncidentDate = req.IncidentDate,
                            InsuredNo = long.Parse(req.InsuredNo),
                            PolicyNo = long.Parse(req.PolicyNo.ToString()),
                            RenewalNo = req.RenewalNo
                        };
                        policyStateResult = client.GetPolicyState(input);

                        result.Error = policyStateResult.ServiceResult.ErrorList;
                        result.IsOk = policyStateResult.ServiceResult.IsOk;

                        log.Response = policyStateResult.ToXML(true);
                        log.Request = input.ToXML(true);
                    }
                    else
                    {
                        result.Error = new string[] { "AuthenticationKey Alınamadı!" };
                        result.IsOk = false;
                        result.Code = "0";

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                        return result;
                    }
                }
                else if ((CompanyEnum)long.Parse(req.CompanyCode) == CompanyEnum.HDI || (CompanyEnum)long.Parse(req.CompanyCode) == CompanyEnum.HDI_TEST)
                {
                    HDIHealthService.HealthTPAServiceSoapClient client = new HDIHealthService.HealthTPAServiceSoapClient();
                    HDIHealthService.PolicyStateResult policyStateResult = new HDIHealthService.PolicyStateResult();

                    HDIHealthService.PolicyStateInput input = new HDIHealthService.PolicyStateInput
                    {
                        AuthenticationKey = "121",
                        CompanyCode = req.CompanyCode,
                        IncidentDate = req.IncidentDate.ToString(),
                        InsuredNo = long.Parse(req.InsuredNo),
                        PolicyNo = long.Parse(req.PolicyNo.ToString()),
                        RenewalNo = req.RenewalNo
                    };
                    policyStateResult = client.GetPolicyState(input);

                    result.Error = policyStateResult.ServiceResult.ErrorList.ToArray();
                    result.IsOk = policyStateResult.ServiceResult.IsOk;
                    log.Response = policyStateResult.ToXML(true);
                    log.Request = input.ToXML(true);
                }
                if (!result.IsOk)
                {
                    result.Code = "0";

                    CreateLog(log, log.Response, result.Code, string.Join(" - ", result.Error));
                    return result;
                }

                // CreateLog(log, result.ToXML(true), result.Code, "OK", ((int)LogType.COMPLETED).ToString(), ((int)HttpStatusCode.OK).ToString());
            }
            catch (Exception ex)
            {
                result.Code = "0";
                result.Error = new string[] { ex.Message };
                result.IsOk = false;

                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));

                return result;
            }
            return result;

        }
        //Zarf Aktarım
        public CommonProxyRes PayrollTransfer(PayrollTransferReq req)
        {
            CommonProxyRes result = new CommonProxyRes();

            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog();
            log.CorrelationId = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.WsMethod = currentMethod.Name;
            log.PayrollId = req.PayrollId;
            log.Request = req.ToXML(true);
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.WsRequestDate = DateTime.Now;
            log.Type = ((int)LogType.INCOMING).ToString();
            IntegrationLogHelper.AddToLog(log);

            try
            {
                V_Payroll payroll = new GenericRepository<V_Payroll>().FindBy("PAYROLL_ID =:payrollId", parameters: new { payrollId = req.PayrollId }, orderby: "PAYROLL_ID", fetchDeletedRows: true).FirstOrDefault();
                if (payroll == null)
                {
                    result.Code = "0";
                    result.Error = new string[] { "Zarf bilgisi bulunamadı!" };
                    result.IsOk = false;

                    CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                    return result;
                }
                log.PayrollId = req.PayrollId;
                log.COMPANY_ID = payroll.COMPANY_ID;
                PayrollDto payrollDto = new PayrollDto();

                if ((CompanyEnum)payroll.COMPANY_ID == CompanyEnum.KATILIM_EMEKLILIK)
                {
                    payrollDto.AuthenticationKey = KatilimGetAuthenticationKey();

                    if (!payrollDto.AuthenticationKey.IsNull())
                    {
                        ClaimService claimService = new ClaimService();
                        payrollDto = claimService.PayrollToKatilim(payroll);
                        if (payrollDto == null)
                        {
                            result.IsOk = false;
                            result.Error = new string[] { "Zarf bilgisi bulunamadı" };
                            result.Code = "0";

                            CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                            return result;
                        }

                        KatilimHealthService.PackageInstructionResult res = new KatilimHealthService.PackageInstructionResult();
                        res = FillPackageInstructionInputKatilim(payrollDto);

                        result.Error = res.ServiceResult.ErrorList;
                        result.IsOk = res.ServiceResult.IsOk;
                        if (!result.IsOk)
                        {
                            result.Code = "0";

                            CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                            return result;
                        }
                    }
                    else
                    {
                        result.Error = new string[] { "AuthenticationKey Alınamadı!" };
                        result.IsOk = false;
                        result.Code = "0";

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                        return result;
                    }
                }
                else if ((CompanyEnum)payroll.COMPANY_ID == CompanyEnum.HDI || (CompanyEnum)payroll.COMPANY_ID == CompanyEnum.HDI_TEST)
                {
                    ClaimService claimService = new ClaimService();
                    payrollDto = claimService.PayrollToKatilim(payroll);
                    if (payrollDto == null)
                    {
                        result.IsOk = false;
                        result.Error = new string[] { "Zarf bilgisi bulunamadı" };
                        result.Code = "0";

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                        return result;
                    }

                    HDIHealthService.PackageInstructionResult res = new HDIHealthService.PackageInstructionResult();
                    res = FillPackageInstructionInputHDI(payrollDto);

                    result.Error = res.ServiceResult.ErrorList.ToArray();
                    result.IsOk = res.ServiceResult.IsOk;
                    if (!result.IsOk)
                    {
                        result.Code = "0";

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = "0";
                result.Error = new string[] { ex.Message };
                result.IsOk = false;

                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));

                return result;
            }
            finally
            {
                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error), ((int)LogType.COMPLETED).ToString(), ((int)HttpStatusCode.OK).ToString());
            }

            return result;
        }
        //Kurum Aktarım
        public CommonProxyRes ProviderTransfer(ProviderTransferReq req)
        {
            CommonProxyRes result = new CommonProxyRes();

            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog();
            log.CorrelationId = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.ProviderId = req.ProviderId;
            log.WsMethod = currentMethod.Name;
            log.Request = req.ToXML(true);
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.WsRequestDate = DateTime.Now;
            log.Type = ((int)LogType.INCOMING).ToString();
            IntegrationLogHelper.AddToLog(log);

            try
            {
                ProviderDto providerDto = new ProviderDto();
                ProviderService providerService = new ProviderService();
                providerDto = providerService.FillProvider(req.ProviderId, req.CompanyCode);
                log.Request = providerDto.ToXML(true);
                if ((CompanyEnum)req.CompanyCode == CompanyEnum.KATILIM_EMEKLILIK)
                {
                    providerDto.AuthenticationKey = KatilimGetAuthenticationKey();
                    var a = providerDto.ToXML();

                    if (!providerDto.AuthenticationKey.IsNull())
                    {
                        if (providerDto == null)
                        {
                            result.Error = new string[] { "Kurum Bilgisi bulunamadı" };
                            result.IsOk = false;
                            result.Code = "0";

                            CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                            return result;
                        }
                        if (!providerDto.CompanyCode.ToString().IsInt64())
                        {
                            result.Error = new string[] { "Kurum Bilgisi bulunamadı" };
                            result.IsOk = false;
                            result.Code = "0";

                            CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                            return result;
                        }
                        KatilimHealthService.ContractedCompanyTransferResult res = new KatilimHealthService.ContractedCompanyTransferResult();
                        res = FillContractedCompanyTransferInputKatilim(providerDto);
                        result.Error = res.ServiceResult.ErrorList;
                        result.IsOk = res.ServiceResult.IsOk;
                        if (!result.IsOk)
                        {
                            result.Code = "0";

                            CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                            return result;
                        }
                    }
                    else
                    {
                        result.Error = new string[] { "AuthenticationKey Alınamadı!" };
                        result.IsOk = false;
                        result.Code = "0";

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                        return result;
                    }
                }
                else if ((CompanyEnum)req.CompanyCode == CompanyEnum.HDI || (CompanyEnum)req.CompanyCode == CompanyEnum.HDI_TEST)
                {
                    providerDto.AuthenticationKey = "1";
                    var a = providerDto.ToXML();

                    if (!providerDto.AuthenticationKey.IsNull())
                    {
                        if (providerDto == null)
                        {
                            result.Error = new string[] { "Kurum Bilgisi bulunamadı" };
                            result.IsOk = false;
                            result.Code = "0";

                            CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                            return result;
                        }
                        if (!providerDto.CompanyCode.ToString().IsInt64())
                        {
                            result.Error = new string[] { "Kurum Bilgisi bulunamadı" };
                            result.IsOk = false;
                            result.Code = "0";

                            CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                            return result;
                        }
                        HDIHealthService.ContractedCompanyTransferResult res = new HDIHealthService.ContractedCompanyTransferResult();
                        res = FillContractedCompanyTransferInputHDI(providerDto);
                        result.Error = res.ServiceResult.ErrorList.ToArray();
                        result.IsOk = res.ServiceResult.IsOk;
                        if (!result.IsOk)
                        {
                            result.Code = "0";

                            CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                            return result;
                        }
                        else
                        {
                            log.Response = result.ToXML(true);
                            log.WsResponseDate = DateTime.Now;
                            log.Type = ((int)LogType.COMPLETED).ToString();
                            log.Response = result.ToXML(true);
                            IntegrationLogHelper.AddToLog(log);
                        }
                    }
                    else
                    {
                        result.Error = new string[] { "AuthenticationKey Alınamadı!" };
                        result.IsOk = false;
                        result.Code = "0";

                        CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = "0";
                result.Error = new string[] { ex.Message };
                result.IsOk = false;

                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));

                return result;
            }
            finally
            {
                //CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error), ((int)LogType.COMPLETED).ToString(), ((int)HttpStatusCode.OK).ToString());
            }


            return result;
        }

        public CommonProxyRes ProviderListTransfer(ProviderListTransferReq req)
        {
            CommonProxyRes result = new CommonProxyRes();

            var currentMethod = MethodBase.GetCurrentMethod();

            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                HttpStatus = ((int)HttpStatusCode.OK).ToString(),
                WsRequestDate = DateTime.Now,
                Type = ((int)LogType.INCOMING).ToString(),
                COMPANY_ID = req.CompanyCode
            };
            IntegrationLogHelper.AddToLog(log);

            try
            {
                ProviderDto providerDto = new ProviderDto();
                ProviderService providerService = new ProviderService();
                List<V_ProviderWebSource> providers = new GenericRepository<V_ProviderWebSource>().FindBy($"COMPANY_ID={req.CompanyCode}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                foreach (var item in providers)
                {
                    providerDto = providerService.FillProvider(item.PROVIDER_ID, req.CompanyCode);

                    log.Request = providerDto.ToXML(true);
                    log.WsRequestDate = DateTime.Now;
                    log.Type = ((int)LogType.WAITING).ToString();
                    log.ProviderId = item.PROVIDER_ID;
                    IntegrationLogHelper.AddToLog(log);

                    if ((CompanyEnum)req.CompanyCode == CompanyEnum.KATILIM_EMEKLILIK)
                    {
                        providerDto.AuthenticationKey = KatilimGetAuthenticationKey();
                        var a = providerDto.ToXML();

                        if (!providerDto.AuthenticationKey.IsNull())
                        {
                            if (providerDto == null)
                            {
                                result.Error = new string[] { "Kurum Bilgisi bulunamadı" };
                                result.IsOk = false;
                                result.Code = "0";

                                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                                return result;
                            }
                            if (!providerDto.CompanyCode.ToString().IsInt64())
                            {
                                result.Error = new string[] { "Kurum Bilgisi bulunamadı" };
                                result.IsOk = false;
                                result.Code = "0";

                                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                                return result;
                            }
                            KatilimHealthService.ContractedCompanyTransferResult res = new KatilimHealthService.ContractedCompanyTransferResult();
                            res = FillContractedCompanyTransferInputKatilim(providerDto);
                            result.Error = res.ServiceResult.ErrorList;
                            result.IsOk = res.ServiceResult.IsOk;
                            if (!result.IsOk)
                            {
                                result.Code = "0";

                                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                                return result;
                            }
                        }
                        else
                        {
                            result.Error = new string[] { "AuthenticationKey Alınamadı!" };
                            result.IsOk = false;
                            result.Code = "0";

                            CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                            return result;
                        }
                    }
                    else if ((CompanyEnum)req.CompanyCode == CompanyEnum.HDI || (CompanyEnum)req.CompanyCode == CompanyEnum.HDI_TEST)
                    {
                        providerDto.AuthenticationKey = "1";
                        var a = providerDto.ToXML();

                        if (!providerDto.AuthenticationKey.IsNull())
                        {
                            if (providerDto == null)
                            {
                                result.Error = new string[] { "Kurum Bilgisi bulunamadı" };
                                result.IsOk = false;
                                result.Code = "0";

                                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                                return result;
                            }
                            if (!providerDto.CompanyCode.ToString().IsInt64())
                            {
                                result.Error = new string[] { "Kurum Bilgisi bulunamadı" };
                                result.IsOk = false;
                                result.Code = "0";

                                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                                return result;
                            }
                            HDIHealthService.ContractedCompanyTransferResult res = new HDIHealthService.ContractedCompanyTransferResult();
                            res = FillContractedCompanyTransferInputHDI(providerDto);
                            result.Error = res.ServiceResult.ErrorList.ToArray();
                            result.IsOk = res.ServiceResult.IsOk;
                            if (!result.IsOk)
                            {
                                result.Code = "0";

                                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                                return result;
                            }
                            else
                            {
                                log.WsResponseDate = DateTime.Now;
                                log.Type = ((int)LogType.COMPLETED).ToString();
                                log.Response = result.ToXML(true);
                                IntegrationLogHelper.AddToLog(log);
                            }
                        }
                        else
                        {
                            result.Error = new string[] { "AuthenticationKey Alınamadı!" };
                            result.IsOk = false;
                            result.Code = "0";

                            CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));
                            return result;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = "0";
                result.Error = new string[] { ex.Message };
                result.IsOk = false;

                CreateLog(log, result.ToXML(true), result.Code, string.Join(" - ", result.Error));

                return result;
            }
            return result;
        }


        private KatilimHealthService.PackageInstructionResult FillPackageInstructionInputKatilim(PayrollDto payrollDto)
        {
            KatilimHealthService.HealthTPAClient client = new KatilimHealthService.HealthTPAClient();

            KatilimHealthService.PackageInstructionInput input = new KatilimHealthService.PackageInstructionInput();
            input.AuthenticationKey = payrollDto.AuthenticationKey;
            input.ArriveDate = payrollDto.Date;
            input.ClaimPayDate = payrollDto.Date;
            input.PackageNo = payrollDto.PayrollId.ToString();
            //TO-DO: TestWS Yok. Ama Olması Lazım ---- input.ContractedCompanyCode = payrollDto.CompanyId;

            int i = 0;
            foreach (var item in payrollDto.ClaimList)
            {
                KatilimHealthService.Bill bill = new KatilimHealthService.Bill
                {
                    CurrencyCode = item.Bill.CurrencyCode,
                    FileNo = item.CompanyClaimId,
                    InvoiceAmount = item.Bill.TotalPaid,
                    InvoiceDate = (DateTime)item.Bill.PaymentDate,
                    InvoiceNo = item.Bill.No
                };

                input.Bills[i] = bill;
                i++;
            }
            if (payrollDto.PayrollTypeCode == "S")
            {
                input.CompanyIBAN = payrollDto.InsuredIban;
                input.PayeeNo = payrollDto.InsuredId;
                input.PayeeType = payrollDto.PayrollTypeCode;
            }
            else
            {
                input.CompanyIBAN = payrollDto.ProviderIban;
                input.PayeeNo = payrollDto.ProviderId;
                input.PayeeType = payrollDto.PayrollTypeCode;
            }

            return client.PackageInstruction(input);
        }
        private KatilimHealthService.ClaimStateUpdateApprovalResult FillClaimStateUpdateApprovalInputKatilim(ClaimDto claimDto, string AuthenticationKey)
        {
            var client = new KatilimHealthService.HealthTPAClient();

            var input = new KatilimHealthService.ClaimStateUpdateApprovalInput
            {
                AuthenticationKey = AuthenticationKey,
                CompanyCode = claimDto.CompanyId.ToString(),
                FileNo = claimDto.CompanyClaimId,
                Description = claimDto.ReasonText
            };

            return client.ClaimStateUpdateApproval(input);
        }
        private KatilimHealthService.ClaimStateUpdateCancellationResult FillClaimStateUpdateCancellationInputKatilim(ClaimDto claimDto, string authenticationKey)
        {
            var client = new KatilimHealthService.HealthTPAClient();

            var input = new KatilimHealthService.ClaimStateUpdateCancellationInput
            {
                AuthenticationKey = authenticationKey,
                CompanyCode = claimDto.CompanyId.ToString(),
                FileNo = claimDto.CompanyClaimId,
                Description = claimDto.ReasonText
            };

            return client.ClaimStateUpdateCancellation(input);
        }
        private KatilimHealthService.ClaimStateUpdateRejectionResult FillClaimStateUpdateRejectionInputKatilim(ClaimDto claimDto, string authenticationKey)
        {
            var client = new KatilimHealthService.HealthTPAClient();

            var input = new KatilimHealthService.ClaimStateUpdateRejectionInput
            {
                AuthenticationKey = authenticationKey,
                CompanyCode = claimDto.CompanyId.ToString(),
                FileNo = claimDto.CompanyClaimId,
                Description = claimDto.ReasonText
            };

            return client.ClaimStateUpdateRejection(input);
        }
        private KatilimHealthService.ContractedCompanyTransferResult FillContractedCompanyTransferInputKatilim(ProviderDto policyDto)
        {
            KatilimHealthService.HealthTPAClient client = new KatilimHealthService.HealthTPAClient();

            KatilimHealthService.ContractedCompanyTransferInput input = new KatilimHealthService.ContractedCompanyTransferInput
            {
                Address = new KatilimHealthService.AddressInfo
                {
                    Address = policyDto.AddressInfo?.Details,
                    CountryCode = policyDto.AddressInfo?.CountryCode,
                    DistrictCode = policyDto.AddressInfo?.DistrictCode,
                    PostCode = policyDto.AddressInfo?.PostCode,
                    ProvinceCode = policyDto.AddressInfo?.ProvinceCode,
                },
                AuthenticationKey = policyDto.AuthenticationKey,
                BankAccount = new KatilimHealthService.BankAccountInfo
                {
                    AccountHolderName = policyDto.BankAccountInfo?.AccountHolderName,
                    IBAN = policyDto.BankAccountInfo?.IBAN
                },
                CompanyCode = policyDto.CompanyCode, /////
                CompanyName = policyDto.CompanyName,
                CompanyNote = "-", /////////
                CompanyType = policyDto.CompanyType,
                Contact = new KatilimHealthService.ContactInfo
                {
                    EmailAddress = policyDto.ContactInfo?.EmailAddress,
                    LandPhone = policyDto.ContactInfo?.LandPhone,
                    MobilePhone = policyDto.ContactInfo?.MobilePhone,
                    WebSite = "www"
                },
                EInvoice = policyDto.EInvoice,
                LegalEntity = new KatilimHealthService.LegalEntityInfo
                {
                    Address = new KatilimHealthService.AddressInfo
                    {
                        Address = policyDto.AddressInfo?.Details,
                        CountryCode = policyDto.AddressInfo?.CountryCode,
                        DistrictCode = policyDto.AddressInfo?.DistrictCode,
                        PostCode = policyDto.AddressInfo?.PostCode,
                        ProvinceCode = policyDto.AddressInfo?.ProvinceCode,
                    },
                    AuthorizedPerson = new KatilimHealthService.Person
                    {
                        Address = new KatilimHealthService.AddressInfo
                        {
                            Address = policyDto.AddressInfo?.Details,
                            CountryCode = policyDto.AddressInfo?.CountryCode,
                            DistrictCode = policyDto.AddressInfo?.DistrictCode,
                            ProvinceCode = policyDto.AddressInfo?.ProvinceCode,
                            PostCode = policyDto.AddressInfo?.PostCode
                        },
                        BirthDate = DateTime.Now,   /////////
                        BirthPlace = "İstanbul",
                        Contact = new KatilimHealthService.ContactInfo
                        {
                            WebSite = "www",
                            EmailAddress = policyDto.ContactInfo?.EmailAddress,
                            LandPhone = policyDto.ContactInfo?.LandPhone,
                            MobilePhone = policyDto.ContactInfo?.MobilePhone
                        },
                        FatherName = "Father",
                        Gender = "E",
                        Name = "TEST",
                        Nation = "Nation",
                        PassportNo = "PassportNo",
                        Surname = "Surname",
                        TCKN = "1321233332123",
                        VKN = "123132313132"
                    },
                    CompanyTitle = policyDto.LegalEntityInfo?.CompanyTitle,
                    Contact = new KatilimHealthService.ContactInfo
                    {
                        WebSite = "www",
                        EmailAddress = policyDto.ContactInfo?.EmailAddress,
                        LandPhone = policyDto.ContactInfo?.LandPhone,
                        MobilePhone = policyDto.ContactInfo?.MobilePhone
                    },
                    MersisNo = "MersisNo",
                    MainCompanyCode = 1,  /////
                    TaxNo = policyDto.LegalEntityInfo?.TaxNo.ToString(),
                    TaxOffice = policyDto.LegalEntityInfo?.TaxOffice,
                },
                State = 5,   /////
            };
            var b = input.ToXML(true);
            return client.ContractedCompanyTransfer(input);
        }







        private HDIHealthService.ClaimStateUpdateApprovalResult FillClaimStateUpdateApprovalInputHDI(ClaimDto claimDto, string AuthenticationKey)
        {
            var client = new HDIHealthService.HealthTPAServiceSoapClient();

            var input = new HDIHealthService.ClaimStateUpdateApprovalInput
            {
                AuthenticationKey = AuthenticationKey,
                CompanyCode = claimDto.CompanyId.ToString(),
                FileNo = claimDto.ClaimId.ToString(),
                Description = claimDto.ReasonText
            };

            return client.ClaimStateUpdateApproval(input);
        }
        private HDIHealthService.ClaimStateUpdateCancellationResult FillClaimStateUpdateCancellationInputHDI(ClaimDto claimDto, string authenticationKey)
        {
            var client = new HDIHealthService.HealthTPAServiceSoapClient();

            var input = new HDIHealthService.ClaimStateUpdateCancellationInput
            {
                AuthenticationKey = authenticationKey,
                CompanyCode = claimDto.CompanyId.ToString(),
                FileNo = claimDto.ClaimId.ToString(),
                Description = claimDto.ReasonText
            };

            return client.ClaimStateUpdateCancellation(input);
        }
        private HDIHealthService.ClaimStateUpdateRejectionResult FillClaimStateUpdateRejectionInputHDI(ClaimDto claimDto, string authenticationKey)
        {
            var client = new HDIHealthService.HealthTPAServiceSoapClient();

            var input = new HDIHealthService.ClaimStateUpdateRejectionInput
            {
                AuthenticationKey = authenticationKey,
                CompanyCode = claimDto.CompanyId.ToString(),
                FileNo = claimDto.ClaimId.ToString(),
                Description = claimDto.ReasonText
            };

            return client.ClaimStateUpdateRejection(input);
        }
        private HDIHealthService.ClaimStateUpdateWaitResult FillClaimStateUpdateWaitInputHDI(ClaimDto claimDto, string authenticationKey)
        {
            var client = new HDIHealthService.HealthTPAServiceSoapClient();

            var input = new HDIHealthService.ClaimStateUpdateWaitInput
            {
                AuthenticationKey = authenticationKey,
                CompanyCode = claimDto.CompanyId.ToString(),
                FileNo = claimDto.ClaimId.ToString(),
                Description = claimDto.ReasonText
            };

            return client.ClaimStateUpdateWait(input);
        }
        private HDIHealthService.PackageInstructionResult FillPackageInstructionInputHDI(PayrollDto payrollDto)
        {
            HDIHealthService.HealthTPAServiceSoapClient client = new HDIHealthService.HealthTPAServiceSoapClient();

            HDIHealthService.PackageInstructionInput input = new HDIHealthService.PackageInstructionInput
            {
                AuthenticationKey = "121",
                ArriveDate = payrollDto.Date,
                ClaimPayDate = (DateTime)payrollDto.DueDate,
                PackageNo = payrollDto.PayrollId.ToString(),
                CompanyIBAN = payrollDto.ProviderIban,
                ContractedCompanyCode = payrollDto.ProviderId.ToString(),
            };

            int i = 0;
            input.Bills = new HDIHealthService.Bill[payrollDto.ClaimList.Count];
            foreach (var item in payrollDto.ClaimList)
            {
                HDIHealthService.Bill bill = new HDIHealthService.Bill
                {
                    CurrencyCode = item.Bill.CurrencyCode,
                    FileNo = item.ClaimId.ToString(),
                    InvoiceAmount = item.Bill.TotalPaid,
                    InvoiceDate = item.Bill.Date,
                    InvoiceNo = item.Bill.No
                };

                input.Bills[i] = bill;
                i++;
            }

            var a = input.ToXML(true);
            return client.PackageInstruction(input);
        }
        private HDIHealthService.ContractedCompanyTransferResult FillContractedCompanyTransferInputHDI(ProviderDto providerDto)
        {
            HDIHealthService.HealthTPAServiceSoapClient client = new HDIHealthService.HealthTPAServiceSoapClient();

            HDIHealthService.ContractedCompanyTransferInput input = new HDIHealthService.ContractedCompanyTransferInput
            {
                Address = new HDIHealthService.AddressInfo
                {
                    Address = providerDto.AddressInfo?.Details,
                    CountryCode = providerDto.AddressInfo?.CountryCode,
                    DistrictCode = providerDto.AddressInfo?.DistrictCode,
                    PostCode = providerDto.AddressInfo?.PostCode,
                    ProvinceCode = providerDto.AddressInfo?.ProvinceCode,
                },
                AuthenticationKey = providerDto.AuthenticationKey,
                BankAccount = new HDIHealthService.BankAccountInfo
                {
                    AccountHolderName = providerDto.BankAccountInfo?.AccountHolderName,
                    IBAN = providerDto.BankAccountInfo?.IBAN
                },
                CompanyCode = providerDto.CompanyCode, /////
                CompanyName = providerDto.CompanyName,
                CompanyNote = providerDto.CompanyNote, /////////
                CompanyType = providerDto.CompanyType,
                Contact = new HDIHealthService.ContactInfo
                {
                    EmailAddress = providerDto.ContactInfo?.EmailAddress,
                    LandPhone = providerDto.ContactInfo?.LandPhone,
                    MobilePhone = providerDto.ContactInfo?.MobilePhone,
                    WebSite = providerDto.ContactInfo?.WebSite
                },
                EInvoice = providerDto.EInvoice,
                LegalEntity = new HDIHealthService.LegalEntityInfo
                {
                    Address = new HDIHealthService.AddressInfo
                    {
                        Address = providerDto.LegalEntityInfo?.AddressInfo?.Details,
                        CountryCode = providerDto.LegalEntityInfo?.AddressInfo?.CountryCode,
                        DistrictCode = providerDto.LegalEntityInfo?.AddressInfo?.DistrictCode,
                        PostCode = providerDto.LegalEntityInfo?.AddressInfo?.PostCode,
                        ProvinceCode = providerDto.LegalEntityInfo?.AddressInfo?.ProvinceCode
                    },
                    AuthorizedPerson = new HDIHealthService.Person
                    {
                        Address = new HDIHealthService.AddressInfo
                        {
                            Address = providerDto.LegalEntityInfo?.AuthorizedPerson?.AddressInfo?.Details,
                            CountryCode = providerDto.LegalEntityInfo?.AuthorizedPerson?.AddressInfo?.CountryCode,
                            DistrictCode = providerDto.LegalEntityInfo?.AuthorizedPerson?.AddressInfo?.DistrictCode,
                            ProvinceCode = providerDto.LegalEntityInfo?.AuthorizedPerson?.AddressInfo?.ProvinceCode,
                            PostCode = providerDto.LegalEntityInfo?.AuthorizedPerson?.AddressInfo?.PostCode
                        },
                        //BirthDate = (DateTime)providerDto.LegalEntityInfo?.AuthorizedPerson?.BirthDate,  
                        BirthPlace = providerDto.LegalEntityInfo?.AuthorizedPerson?.BirthPlace,
                        Contact = new HDIHealthService.ContactInfo
                        {
                            WebSite = providerDto.LegalEntityInfo?.AuthorizedPerson?.ContactInfo?.WebSite,
                            EmailAddress = providerDto.LegalEntityInfo?.AuthorizedPerson?.ContactInfo?.EmailAddress,
                            LandPhone = providerDto.LegalEntityInfo?.AuthorizedPerson?.ContactInfo?.LandPhone,
                            MobilePhone = providerDto.LegalEntityInfo?.AuthorizedPerson?.ContactInfo?.MobilePhone
                        },
                        FatherName = providerDto.LegalEntityInfo?.AuthorizedPerson?.FatherName,
                        Gender = providerDto.LegalEntityInfo?.AuthorizedPerson?.Gender,
                        Name = providerDto.LegalEntityInfo?.AuthorizedPerson?.Name,
                        Nation = providerDto.LegalEntityInfo?.AuthorizedPerson?.Nation,
                        PassportNo = providerDto.LegalEntityInfo?.AuthorizedPerson?.PassportNo,
                        Surname = providerDto.LegalEntityInfo?.AuthorizedPerson?.Surname,
                        TCKN = providerDto.LegalEntityInfo?.AuthorizedPerson?.TCKN.ToString(),
                        VKN = providerDto.LegalEntityInfo?.AuthorizedPerson?.VKN.ToString()
                    },
                    CompanyTitle = providerDto.LegalEntityInfo?.CompanyTitle,
                    Contact = new HDIHealthService.ContactInfo
                    {
                        WebSite = providerDto.LegalEntityInfo?.ContactInfo?.WebSite,
                        EmailAddress = providerDto.LegalEntityInfo?.ContactInfo?.EmailAddress,
                        LandPhone = providerDto.LegalEntityInfo?.ContactInfo?.LandPhone,
                        MobilePhone = providerDto.LegalEntityInfo?.ContactInfo?.MobilePhone
                    },
                    MersisNo = providerDto.LegalEntityInfo?.MersisNo,
                    MainCompanyCode = providerDto.LegalEntityInfo?.MainCompanyCode,
                    TaxNo = providerDto.LegalEntityInfo?.TaxNo.ToString(),
                    TaxOffice = providerDto.LegalEntityInfo?.TaxOffice,
                },
                State = providerDto.State,   /////
            };
            var b = input.ToXML(true);
            return client.ContractedCompanyTransfer(input);
        }
    }
}
