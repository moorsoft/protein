﻿using Protein.Common.Constants;
using Protein.Common.Dto.ImportObjects;
using Protein.Common.Extensions;
using Protein.Common.Helpers;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.Log;
using Protein.Data.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Data.ExternalServices.Common.Lib.NN.NNInputOutputTypes;

namespace Protein.Data.ExternalServices.InsuranceCompanies
{
    public class NNHayatServiceClient
    {
        //    //private static CompanyParam companyParam = new Common.Helper.CompanyHelper().GetCompanyParameters(Constants.CompanyID.NNHayat);

        //    private string Url = "https://sagliktest.nnhayatemeklilik.com.tr/SaaSWebServices/IntegrationWebServices.asmx?wsdl";
        //    private static string Username = "USERWSINTEGRATION";
        //    private static string Password = "FXHMN24XK04";

        //    private static readonly string NNSoapEnvelopeTemp = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"
        //                                                          + " <soapenv:Header/>"
        //                                                          + "  <soapenv:Body>"
        //                                                          + "    <tem:{ComplexTypeName}>"
        //                                                          + "      <{Prefix}{ElementName}>"
        //                                                          + "        {InputFields}"
        //                                                          + "      </{Prefix}{ElementName}>"
        //                                                          + "    </tem:{ComplexTypeName}>"
        //                                                          + "  </soapenv:Body>"
        //                                                          + "</soapenv:Envelope>";
        //    #region NN Oprs 

        //    public ServiceResponse<SigortaliHakSorgulaResult> hakSahipligiSorgula(HakSahiplikInput input)
        //    {
        //        var result = GenerateXmlAndCallService<SigortaliHakSorgulaResult, HakSahiplikInput>(input);
        //        return result;
        //    }
        //    //public ServiceResponse<IcmalGirisOutput> icmalGiris(IcmalGirisInput input)
        //    //{
        //    //    //var currentMethod = MethodBase.GetCurrentMethod();

        //    //    //IntegrationLog log = new IntegrationLog();
        //    //    //log.CorrelationId = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString();
        //    //    //log.WsName = currentMethod.DeclaringType.FullName;
        //    //    //log.WsMethod = currentMethod.Name;
        //    //    //log.Request = input.ToXML(true);
        //    //    //log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
        //    //    //log.WsRequestDate = DateTime.Now;
        //    //    //log.Type = ((int)LogType.INCOMING).ToString();
        //    //    //IntegrationLogHelper.AddToLog(log);

        //    //    //try
        //    //    //{
        //    //    //    PayrollDto payrollDto = new PayrollDto();

        //    //    //}
        //    //    //catch { }
        //    //    //IcmalGirisInput input2 = new IcmalGirisInput();

        //    //    var result = GenerateXmlAndCallService<IcmalGirisOutput, IcmalGirisInput>(input);
        //    //    return result;
        //    //}

        //    //public ServiceResponse<TamamlayiciSaglikProvizyonIptalOutput> tamamlayiciSaglikProvizyonIptal(TamamlayiciSaglikProvizyonIptalInput input)
        //    //{
        //    //    var result = GenerateXmlAndCallService<TamamlayiciSaglikProvizyonIptalOutput, TamamlayiciSaglikProvizyonIptalInput>(input);
        //    //    return result;
        //    //}
        //    //public ServiceResponse<IcmalTazminatCikarmaOutput> tamamlayiciSaglikTazminatCikarma(IcmalTazminatCikarmaInput input)
        //    //{
        //    //    var result = GenerateXmlAndCallService<IcmalTazminatCikarmaOutput, IcmalTazminatCikarmaInput>(input);
        //    //    return result;
        //    //}
        //    //public ServiceResponse<TamamlayiciSaglikTazminatGirisOutput> tamamlayiciSaglikTazminatGiris(TamamlayiciSaglikTazminatGirisInput input)
        //    //{
        //    //    var result = GenerateXmlAndCallService<TamamlayiciSaglikTazminatGirisOutput, TamamlayiciSaglikTazminatGirisInput>(input);
        //    //    return result;
        //    //}
        //    #endregion
        //    #region Private Methods
        //    private ServiceResponse<TOut> GenerateXmlAndCallService<TOut, TIn>(TIn classObj, bool usePrefix = true)
        //    {
        //        ServiceResponse<TOut> returnObject = new ServiceResponse<TOut>();
        //        returnObject.Data = Activator.CreateInstance<TOut>();

        //        var classAttributes = typeof(TIn).GetCustomAttributes(false);
        //        var displayMapping = classAttributes.FirstOrDefault(a => a.GetType() == typeof(DisplayNameAttribute));
        //        var mapsToTable = displayMapping as DisplayNameAttribute;

        //        var OutputClassAttributes = typeof(TOut).GetCustomAttributes(false);
        //        var OutputDisplayMapping = OutputClassAttributes.FirstOrDefault(a => a.GetType() == typeof(DisplayNameAttribute));
        //        var OutputMapsToTable = OutputDisplayMapping as DisplayNameAttribute;

        //        string ComplexTypeName = mapsToTable.DisplayName.Split('|')[0];
        //        string ElementName = mapsToTable.DisplayName.Split('|')[1];
        //        string RootElement = OutputMapsToTable.DisplayName.Split('|')[1];

        //        if (classObj == null) return returnObject;

        //        #region Generate Soap XML 

        //        string generatedXml = string.Empty;

        //        XmlSerializer xmlSerializer = new XmlSerializer(typeof(TIn));
        //        XmlDocument soapEnvelopeXmlDoc = new XmlDocument();

        //        using (StringWriter stringWriter = new StringWriter())
        //        {
        //            using (var xmlWriter = new CustomDateTimeXmlWriter(stringWriter))
        //            {


        //                xmlSerializer.Serialize(xmlWriter, classObj);
        //                generatedXml = stringWriter.ToString();

        //                soapEnvelopeXmlDoc.LoadXml(generatedXml);
        //                XmlNodeList elemList = soapEnvelopeXmlDoc.GetElementsByTagName(typeof(TIn).Name.ToString());


        //                string elmList = "";

        //                foreach (XmlNode item in elemList)
        //                {
        //                    foreach (XmlNode node in item.ChildNodes)
        //                    {

        //                        elmList += "<tem:" + node.Name + ">";
        //                        elmList += node.InnerXml;
        //                        elmList += "</tem:" + node.Name + ">";
        //                    }
        //                }
        //                generatedXml = NNSoapEnvelopeTemp.Replace("{ComplexTypeName}", ComplexTypeName)
        //                                                     .Replace("{Prefix}", usePrefix ? "tem:" : "")
        //                                                     .Replace("{ElementName}", ElementName)
        //                                                     .Replace("{InputFields}", elmList)
        //                                                     .Replace("{Username}", Username)
        //                                                     .Replace("{Password}", Password);


        //                //generatedXml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"";
        //                //generatedXml += " xmlns:tem=\"http://tempuri.org/\">";
        //                //generatedXml += "<soapenv:Header/>";
        //                //generatedXml += "<soapenv:Body>";
        //                //generatedXml += "<tem:SigortaliHakSorgula>";
        //                //generatedXml += "<tem:Sigortali>";
        //                //generatedXml += "<tem:UserName>USERWSINTEGRATION</tem:UserName>";
        //                //generatedXml += "<tem:Password>FXHMN24XK04</tem:Password>";
        //                //generatedXml += "<tem:Kurum>1</tem:Kurum>";
        //                //generatedXml += "<tem:OlayTarihi>2018.07.17</tem:OlayTarihi>";
        //                //generatedXml += "<tem:TcKimlikNo>43174172166</tem:TcKimlikNo>";
        //                //generatedXml += "<tem:PasaportNo>0</tem:PasaportNo>";
        //                //generatedXml += "<tem:AyaktanYatarak>A</tem:AyaktanYatarak>";
        //                //generatedXml += " </tem:Sigortali>";
        //                //generatedXml += "</tem:SigortaliHakSorgula>";
        //                //generatedXml += " </soapenv:Body>";
        //                //generatedXml += " </soapenv:Envelope>";
        //                soapEnvelopeXmlDoc.LoadXml(generatedXml);

        //                //XmlNamespaceManager nsmgr = new XmlNamespaceManager(soapEnvelopeXmlDoc.NameTable);
        //                //nsmgr.AddNamespace("tem", "http://schemas.xmlsoap.org/soap/envelope/");


        //                //foreach (XmlNode node in soapEnvelopeXmlDoc.SelectNodes("//tem:"+ ElementName, nsmgr))
        //                //{
        //                //    node.Prefix = "ds";
        //                //}
        //            }
        //        }

        //        #endregion

        //        //Call service
        //        string responseXml = CallService(soapEnvelopeXmlDoc);

        //        //string responseXml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">" +
        //        //                       "<soapenv:Header />  " +
        //        //                        "<soapenv:Body>" +
        //        //                           " <IcmalGirisResponse>"+
        //        //                                "<IcmalGirisResult>" +
        //        //                                  "<SonucKodu>String</SonucKodu>" +
        //        //                                  "<SonucAciklama> String </SonucAciklama>" +
        //        //                                  "<PaketNo> String </PaketNo>" +
        //        //                               "</IcmalGirisResult >" +
        //        //                            "</IcmalGirisResponse>" +
        //        //                         "</soapenv:Body>" +
        //        //                       "</soapenv:Envelope>";

        //        //string responseXml = "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"" + " xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Header /> " +
        //        //                                                   "<soap:Body>" +
        //        //                                                    "<SigortaliHakSorgulaResponse>" +
        //        //                                                       "<SigortaliHakSorgulaResult>" +
        //        //                                                           "<SonucKodu> string </SonucKodu>" +
        //        //                                                           "<SonucAciklama> string </SonucAciklama>" +
        //        //                                                           "<SonucAciklamaDetay> string </SonucAciklamaDetay>" +
        //        //                                                           "<SigortaliAdSoyad> string</SigortaliAdSoyad>" +
        //        //                                                           "<PoliceBilgileri>" +
        //        //                                                               "<Police>" +
        //        //                                                                   "<PoliceNo> string </PoliceNo>" +
        //        //                                                                   "<PoliceBaslangicTarihi> string </PoliceBaslangicTarihi>" +
        //        //                                                                   "<PoliceBitisTarihi> string </PoliceBitisTarihi>" +
        //        //                                                                   "<UrunKodu> string </UrunKodu>" +
        //        //                                                                   "<KalanMuayeneLimiti> string </KalanMuayeneLimiti>" +
        //        //                                                               "</Police>" +
        //        //                                                               "<Police>" +
        //        //                                                                   "<PoliceNo> string </PoliceNo>" +
        //        //                                                                   "<PoliceBaslangicTarihi> string </PoliceBaslangicTarihi>" +
        //        //                                                                   "<PoliceBitisTarihi> string </PoliceBitisTarihi>" +
        //        //                                                                   "<UrunKodu> string </UrunKodu>" +
        //        //                                                                   "<Tarife> string </Tarife>" +
        //        //                                                                   "<KalanMuayeneLimiti> string </KalanMuayeneLimiti>" +
        //        //                                                                   "</Police>" +
        //        //                                                            "</PoliceBilgileri>" +
        //        //                                                        "</SigortaliHakSorgulaResult>" +
        //        //                                                     "</SigortaliHakSorgulaResponse>" +
        //        //                                                    "</soap:Body>" +
        //        //                                                  "</soap:Envelope>";

        //        //Parse xml result
        //        try
        //        {
        //            returnObject.Data = XmlHelper.ParseResponseXml<TOut>(responseXml, OutputMapsToTable.DisplayName.Split('|')[0]);
        //            //PropertyInfo propInfo = returnObject.Data.GetType().GetProperty("islemBasarili");
        //            //propInfo.SetValue(returnObject.Data, true, null);
        //        }
        //        catch (Exception ex)
        //        {
        //            //SbmServiceException soapEx = GetErrorDetail(responseXml);
        //            //PropertyInfo propInfo = returnObject.Data.GetType().GetProperty("islemBasarili");
        //            //propInfo.SetValue(returnObject.Data, false, null);
        //            //returnObject.ServiceException = soapEx;
        //        }

        //        return returnObject;
        //    }




        //    private string CallService(XmlDocument soapEnvelopeXmlDoc)
        //    {
        //        string soapResult = string.Empty;


        //        //System.Net.ServicePointManager.ServerCertificateValidationCallback +=
        //        // (sender, cert, chain, sslPolicyErrors) =>
        //        // {
        //        //     if (cert != null) System.Diagnostics.Debug.WriteLine(cert);
        //        //     return true;
        //        // };
        //        //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
        //        //byte[] bytes;

        //        //string soapStr = soapEnvelopeXmlDoc.InnerXml;

        //        //bytes = Encoding.UTF8.GetBytes(soapStr);
        //        //request.ContentType = "text/xml";
        //        //request.ContentLength = bytes.Length;
        //        //request.Method = "POST";

        //        //Stream requestStream = request.GetRequestStream();
        //        //requestStream.Write(bytes, 0, bytes.Length);
        //        //requestStream.Close();
        //        //HttpWebResponse response;
        //        //response = (HttpWebResponse)request.GetResponse();
        //        //if (response.StatusCode == HttpStatusCode.OK)
        //        //{
        //        //    Stream responseStream = response.GetResponseStream();
        //        //    string responseStr = new StreamReader(responseStream).ReadToEnd();
        //        //    soapResult = responseStr;
        //        //}


        //        Stream requestStream = null;
        //        Stream responseStream = null;
        //        HttpWebRequest webRequest = null;
        //        WebResponse webResponse = null;
        //        StreamReader rd = null;

        //        try
        //        {

        //            webRequest = (HttpWebRequest)WebRequest.Create(Url);
        //            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
        //            webRequest.Accept = "text/xml";
        //            webRequest.Method = "POST";

        //            //ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

        //            //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

        //            // Skip validation of SSL/TLS certificate
        //            //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };



        //            requestStream = webRequest.GetRequestStream();
        //            soapEnvelopeXmlDoc.Save(requestStream);

        //            // begin async call to web request.
        //            IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

        //            // suspend this thread until call is complete. You might want to
        //            // do something usefull here like update your UI.
        //            asyncResult.AsyncWaitHandle.WaitOne();

        //            // get the response from the completed web request.
        //            webResponse = webRequest.EndGetResponse(asyncResult);
        //            rd = new StreamReader(webResponse.GetResponseStream());

        //            soapResult = rd.ReadToEnd();
        //        }
        //        catch (WebException ex)
        //        {
        //            WebResponse errResp = ex.Response;
        //            using (Stream respStream = errResp.GetResponseStream())
        //            {
        //                StreamReader reader = new StreamReader(respStream);
        //                soapResult = reader.ReadToEnd();
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            soapResult = ex.Message;
        //        }
        //        finally
        //        {
        //            if (requestStream != null)
        //            {
        //                requestStream.Close();
        //                requestStream = null;
        //            }
        //            if (webResponse != null)
        //            {
        //                webResponse.Close();
        //                webResponse = null;
        //            }
        //            if (responseStream != null)
        //            {
        //                responseStream.Close();
        //                responseStream = null;
        //            }
        //            if (rd != null)
        //            {
        //                rd.Close();
        //                rd = null;
        //            }
        //        }

        //        return soapResult;
        //    }
        //    #endregion
        //}
    }
}
