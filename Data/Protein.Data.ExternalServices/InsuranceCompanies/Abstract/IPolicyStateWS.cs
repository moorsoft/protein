﻿using Protein.Data.ExternalServices.Common.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.InsuranceCompanies.Abstract
{
    public interface IPolicyStateWS
    {
        CommonProxyRes PolicyState(PolicyStateReq input);
    }
}
