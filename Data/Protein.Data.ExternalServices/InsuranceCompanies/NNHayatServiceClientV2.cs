﻿using Protein.Business.Concrete.Services;
using Protein.Common.Dto.ImportObjects;
using Protein.Common.Extensions;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.Common.Lib.NN;
using Protein.Data.ExternalServices.Log;
using Protein.Data.ExternalServices.NNHayatServicePROD;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;
using static Protein.Data.ExternalServices.Common.Lib.NN.NNInputOutputTypes;

namespace Protein.Data.ExternalServices.InsuranceCompanies
{
    public class NNHayatServiceClientV2
    {
        #region Create Generic Log
        private void CreateFailedLog(IntegrationLog log, string responseXml, string desc)
        {
            log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
            log.Type = ((int)LogType.FAILED).ToString();
            log.Response = responseXml;
            log.ResponseStatusCode = "0";
            log.ResponseStatusDesc = desc != null ? string.Join(" - ", desc) : "HATA";
            log.WsResponseDate = DateTime.Now;
            IntegrationLogHelper.AddToLog(log);
        }
        private void CreateOKLog(IntegrationLog log, string responseXml)
        {
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.Type = ((int)LogType.COMPLETED).ToString();
            log.Response = responseXml;
            log.ResponseStatusCode = "200";
            log.ResponseStatusDesc = "OK";
            log.WsResponseDate = DateTime.Now;
            IntegrationLogHelper.AddToLog(log);
        }
        private void CreateIncomingLog(IntegrationLog log)
        {
            log.HttpStatus = ((int)HttpStatusCode.Continue).ToString();
            log.Type = ((int)LogType.INCOMING).ToString();
            log.ResponseStatusCode = "100";
            log.ResponseStatusDesc = "INCOMING";
            IntegrationLogHelper.AddToLog(log);
        }
        private void CreateWaitingLog(IntegrationLog log)
        {
            log.HttpStatus = ((int)HttpStatusCode.Continue).ToString();
            log.Type = ((int)LogType.WAITING).ToString();
            log.ResponseStatusCode = "100";
            log.ResponseStatusDesc = "WAITING";
            IntegrationLogHelper.AddToLog(log);
        }

        public CommonProxyRes OldClaimTransfer()
        {
            List<Claim> claimList = new GenericRepository<Claim>().FindBy($"COMPANY_ID={NNCompanyCode} AND STATUS!='{((int)ClaimStatus.GIRIS).ToString()}' AND COMPANY_CLAIM_ID IS NULL");
            if (claimList!=null)
            {
                foreach (var item in claimList)
                {
                    var resTazGir = TazminatGiris(new NNTazminatGirisReq { claimId = item.Id });
                    if (resTazGir.IsOk)
                    {
                        string newStatus = "";
                        if (item.Status == ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                        {
                            newStatus = "1";
                        }
                        else if (item.Status == ((int)ClaimStatus.RET).ToString())
                        {
                            newStatus = "4";
                        }
                        else if (item.Status == ((int)ClaimStatus.IPTAL).ToString())
                        {
                            newStatus = "2";
                        }
                        else if (item.Status == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || item.Status == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                        {
                            newStatus = "3";
                        }
                        if (newStatus.IsInt())
                        {
                            NNSetProvisionStatusReq nNSetProvisionStatusReq = new NNSetProvisionStatusReq
                            {
                                ProvisionNumber = resTazGir.CompanyObjectId,
                                ProvisionNumberSpecified = true,
                                NewStatu = newStatus,
                                NewStatuSpecified = true,
                                ReasonDesc = item.ReasonId != null ? ReasonHelper.GetReasonbyId((long)item.ReasonId)?.Description : "",
                                ReasonDescSpecified = item.ReasonId != null ? true : false
                            };
                            var resStatusChange = SetProvisionStatus(nNSetProvisionStatusReq);
                        }
                    }
                }
            }
            return new CommonProxyRes { Code = "100", Description = "OK", IsOk = true, CompanyObjectId = claimList.Count.ToString() };
        }

        public CommonProxyRes ProviderListTransfer(NNProviderTransferRequest req)

        {
            CommonProxyRes result = new CommonProxyRes();
            var currentMethod = MethodBase.GetCurrentMethod();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
                COMPANY_ID = NNCompanyCode
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                ProviderDto providerDto = new ProviderDto();
                ProviderService providerService = new ProviderService();
                List<V_ProviderWebSource> providers = new GenericRepository<V_ProviderWebSource>().FindBy($"COMPANY_ID=322", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                foreach (var item in providers)
                {
                    #region Get Provider Data
                    providerDto = providerService.FillProvider(item.PROVIDER_ID, 322);
                    if (providerDto == null)
                    {
                        #region Error 
                        result.Code = "0";
                        result.Description = "HATA";
                        result.Error = new string[] { "Anlaşmalı Kurum Bilgisi Bulunamadı." };
                        result.IsOk = false;

                        CreateFailedLog(log, result.ToXML(true), "Anlaşmalı Kurum Bilgisi Bulunamadı.");
                        return result;
                        #endregion
                    }
                    #endregion

                    #region Mapping 
                    IntegrationWebServicesSoapClient client = new IntegrationWebServicesSoapClient();
                    HealthCenterInfo healthCenterInfo = new HealthCenterInfo
                    {
                        usernamespecified = true,
                        passwordspecified = true,
                        healthcenteridspecified = false,
                        descriptionspecified = true,
                        tradenamespecified = true,
                        typeidspecified = true,
                        subtypeidspecified = false,
                        contactpersonspecified = true,
                        addressspecified = true,
                        cityidspecified = true,
                        provinceidspecified = true,
                        districtidspecified = false,
                        phonespecified = true,
                        faxspecified = false,
                        gsmspecified = true,
                        emailspecified = true,
                        taxofficeidspecified = false,
                        taxnumberspecified = true,
                        chemistrecordnumberspecified = false,
                        licensenumberspecified = false,
                        idnumberspecified = false,
                        specialtyidspecified = false,
                        claimpaydayspecified = true,
                        webspecified = false,
                        reductionspecified = false,
                        eprolimitspecified = false,
                        eprodailylimitspecified = false,
                        reductiontypeidspecified = false,
                        glnspecified = false,
                        agreementtypeidspecified = false,
                        freelanceinvoicespecified = false,
                        externalrefspecified = true,
                        taxfreespecified = false,
                        hccompanyidspecified = false,
                        uplinkspecified = true,
                        upexternalrefspecified = false,
                        bankidspecified = false,
                        branchidspecified = false,
                        accountnumberspecified = false,
                        ibanspecified = true,
                        accountownernamespecified = true,
                        statusspecified = false,
                        vateratespecified = false,
                        notespecified = false,
                        seizurestatusidspecified = false,
                        seizedbankidspecified = false,
                        seizedbranchidspecified = false,
                        seizedaccountnumberspecified = false,
                        seizedibanspecified = false,
                        seizedaccountownamespecified = false,
                        username = "USERWSINTEGRATION",
                        password = "FXHMN24XK04",
                        externalref = providerDto.CompanyCode.ToString(),
                        description = providerDto.CompanyName,
                        tradename = providerDto.LegalEntityInfo.CompanyTitle,
                        typeid = providerDto.CompanyType.IsInt() ? GetNNTypeCode(providerDto.CompanyType) : "",
                        subtypeid = "",
                        contactperson = providerDto.LegalEntityInfo.AuthorizedPerson != null ? providerDto.LegalEntityInfo.AuthorizedPerson.Name + " " + providerDto.LegalEntityInfo.AuthorizedPerson.Surname : "",
                        addresss = providerDto.AddressInfo.Details,
                        cityid = providerDto.AddressInfo.ProvinceCode,
                        provinceid = "4",//providerDto.AddressInfo.DistrictCode,
                        phone = providerDto.ContactInfo.LandPhone,
                        gsm = providerDto.LegalEntityInfo.AuthorizedPerson != null ? providerDto.LegalEntityInfo.AuthorizedPerson.ContactInfo.MobilePhone : "",
                        email = providerDto.LegalEntityInfo.AuthorizedPerson != null ? providerDto.LegalEntityInfo.AuthorizedPerson.ContactInfo.EmailAddress : "",
                        taxnumber = providerDto.LegalEntityInfo.TaxNo.ToString(),
                        claimpayday = "30",
                        accountownername = providerDto.BankAccountInfo != null ? providerDto.BankAccountInfo.AccountHolderName : "",
                        iban = providerDto.BankAccountInfo != null ? providerDto.BankAccountInfo.IBAN : ""
                    };

                    #endregion

                    #region Operation
                    log.Request = healthCenterInfo.ToXML(true);
                    var cevap = client.CreateOrReplaceHealthCenter(healthCenterInfo);
                    if (cevap.error.ErrorCode == 0 && cevap.healthcenterid > 0)
                    {
                        //#region Parameter Save
                        //Parameter parameter = new Parameter
                        //{
                        //    Key = "PROVIDER_ID",
                        //    Value = cevap.healthcenterid.ToString(),
                        //    SystemType = "11"
                        //};
                        //SpResponse<Parameter> spResponseParameter = new GenericRepository<Parameter>().Insert(parameter);
                        //if (spResponseParameter.Code != "100")
                        //{
                        //    #region Error
                        //    result.Code = "0";
                        //    result.Description = "HATA - Error Listesini Kontrol Ediniz";
                        //    result.Error = new string[] { spResponseParameter.Message };
                        //    result.IsOk = false;

                        //    CreateFailedLog(log, result.ToXML(true), spResponseParameter.Message);
                        //    return result;
                        //    #endregion
                        //}
                        //ProviderParameter providerParameter = new ProviderParameter
                        //{
                        //    ParameterId = spResponseParameter.PkId,
                        //    ProviderId = req.ProviderId
                        //};
                        //SpResponse<ProviderParameter> spResponseProvider = new GenericRepository<ProviderParameter>().Insert(providerParameter);
                        //if (spResponseProvider.Code != "100")
                        //{
                        //    #region Error
                        //    result.Code = "0";
                        //    result.Description = "HATA - Error Listesini Kontrol Ediniz";
                        //    result.Error = new string[] { spResponseProvider.Message };
                        //    result.IsOk = false;

                        //    CreateFailedLog(log, result.ToXML(true), spResponseProvider.Message);
                        //    return result;
                        //    #endregion
                        //}
                        //#endregion

                        #region OK
                        result.Code = "1";
                        result.Description = "İşlem Başarılı";
                        result.IsOk = true;
                        result.CompanyObjectId = cevap.healthcenterid.ToString();
                        CreateOKLog(log, result.ToXML(true));
                        //return result;
                        #endregion
                    }
                    else
                    {
                        #region Error
                        result.Code = "0";
                        result.Description = "HATA - Error Listesini Kontrol Ediniz";
                        result.Error = new string[] { cevap.error.ErrorMessage };
                        result.IsOk = false;

                        CreateFailedLog(log, result.ToXML(true), cevap.error.ErrorMessage);
                        //return result;
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region Catch Error
                result.Code = "0";
                result.Description = "HATA - Error Listesini Kontrol Ediniz";
                result.Error = new string[] { ex.Message };
                result.IsOk = false;

                CreateFailedLog(log, result.ToXML(true), ex.Message);
                #endregion
            }
            return result;
        }
        #endregion

        long NNCompanyCode = 322;

        public string GetNNTypeCode(string IMCtypeCode)
        {
            int code = int.Parse(IMCtypeCode);

            switch (IMCtypeCode)
            {
                case "12":
                    code = 4;
                    break;
                case "4":
                    code = 6;
                    break;
                case "6":
                    code = 7;
                    break;
                case "22":
                    code = 8;
                    break;
                case "7":
                    code = 9;
                    break;
                case "14":
                    code = 10;
                    break;
                case "23":
                    code = 11;
                    break;
                case "16":
                    code = 13;
                    break;
                case "13":
                    code = 14;
                    break;
                case "21":
                    code = 15;
                    break;
                case "10":
                    code = 16;
                    break;
                default:
                    code = int.Parse(IMCtypeCode); //Diğer
                    break;
            }

            return code.ToString();
        }
        

        public CommonProxyRes PolicyState(NNHakSahiplikReq req)
        {
            CommonProxyRes result = new CommonProxyRes();
            result.IsOk = true;
            var currentMethod = MethodBase.GetCurrentMethod();

            #region IncomingLog
            IntegrationLog log = new IntegrationLog();
            log.CorrelationId = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.WsMethod = currentMethod.Name;
            log.Request = req.ToXML(true);
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.WsRequestDate = DateTime.Now;
            log.Type = ((int)LogType.INCOMING).ToString();
            log.IdentityNo = req.TcKimlikNo.IsInt64() ? (long?)long.Parse(req.TcKimlikNo) : null;
            log.ProviderId = req.Kurum.IsInt64() ? (long?)long.Parse(req.Kurum) : null;
            IntegrationLogHelper.AddToLog(log);
            #endregion

            try
            {
                //var companyProviderId = "";
                //V_ProviderParameter v_ProviderParameter = new GenericRepository<V_ProviderParameter>().FindBy($"PROVIDER_ID={req.Kurum} AND KEY='PROVIDER_ID' AND SYSTEM_TYPE='11'",orderby:"").FirstOrDefault();
                //if (v_ProviderParameter==null)
                //{
                //    NNProviderTransferRequest nNProviderTransferRequest = new NNProviderTransferRequest
                //    {
                //        ProviderId = long.Parse(req.Kurum)
                //    };
                //    var providerResponse = ProviderTransfer(nNProviderTransferRequest);
                //    if (!providerResponse.IsOk)
                //    {
                //        result.IsOk = false;
                //        result.Code = "0";
                //        result.Description = "Anlaşmalı Kurum Şirkete Gönderilemedi.";

                //        CreateFailedLog(log, result.ToXML(true), result.Description);
                //        return result;
                //    }
                //    else
                //    {
                //        companyProviderId = providerResponse.CompanyObjectId;
                //    }
                //}
                #region Mapping 
                IntegrationWebServicesSoapClient client = new IntegrationWebServicesSoapClient();
                SigortaliSorgula input = new SigortaliSorgula
                {
                    AyaktanYatarak = req.AyaktanYatarak,
                    ayaktanYatarakSpecified = req.ayaktanYatarakSpecified,

                    //Kurum = companyProviderId,
                    //kurumSpecified = true,

                    HariciKurum = req.Kurum,
                    HariciKurumSpecified = req.kurumSpecified,

                    OlayTarihi = req.OlayTarihi,
                    olayTarihiSpecified = req.olayTarihiSpecified,

                    PasaportNo = req.PasaportNo,
                    pasaportNoSpecified = req.pasaportNoSpecified,

                    Password = req.Password,
                    passwordSpecified = req.passwordSpecified,

                    TcKimlikNo = req.TcKimlikNo,
                    tcKimlikNoSpecified = req.tcKimlikNoSpecified,

                    UserName = req.UserName,
                    userNameSpecified = req.userNameSpecified
                };
                #endregion

                SigortaliSorgulaCevap cevap = client.SigortaliHakSorgula(input);
                if (cevap.SonucKodu == "1")
                {
                    result.Code = cevap.SonucKodu;
                    result.Description = cevap.SonucAciklama;
                    result.IsOk = cevap.SonucKodu == "1" ? true : false;
                    log.Request = input.ToXML(true);
                    log.Response = cevap.ToXML(true);
                    log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                    log.ResponseStatusCode = "1";
                    log.ResponseStatusDesc = "OK";
                    log.WsResponseDate = DateTime.Now;
                    log.Type = ((int)LogType.COMPLETED).ToString();
                    IntegrationLogHelper.AddToLog(log);
                }
                else
                {
                    result.Code = cevap.SonucKodu;
                    result.Description = cevap.SonucAciklama;
                    result.Error = new string[] { cevap.SonucAciklama };
                    result.IsOk = cevap.SonucKodu == "1" ? true : false;
                    log.Request = input.ToXML(true);
                    log.Response = cevap.ToXML(true);
                    log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                    log.ResponseStatusDesc = result.Description;
                    log.ResponseStatusCode = "0";
                    log.WsResponseDate = DateTime.Now;
                    log.Type = ((int)LogType.FAILED).ToString();
                    IntegrationLogHelper.AddToLog(log);
                }
            }
            catch (Exception ex)
            {
                result.Code = "0";
                result.Description = ex.Message;
                result.Error = new string[] { ex.Message };
                result.IsOk = false;

                log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                log.WsResponseDate = DateTime.Now;
                log.ResponseStatusDesc = "ERROR";
                log.Type = ((int)LogType.FAILED).ToString();
                IntegrationLogHelper.AddToLog(log);

                return result;
            }
            return result;
        }
        
        public CommonProxyRes ProviderTransfer(NNProviderTransferRequest req)
        {
            CommonProxyRes result = new CommonProxyRes();
            var currentMethod = MethodBase.GetCurrentMethod();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
                ProviderId = req.ProviderId,
                COMPANY_ID = NNCompanyCode
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Get Provider Data
                ProviderDto providerDto = new ProviderDto();
                ProviderService providerService = new ProviderService();
                providerDto = providerService.FillProvider(req.ProviderId, NNCompanyCode);
                if (providerDto==null)
                {
                    #region Error 
                    result.Code = "0";
                    result.Description = "HATA";
                    result.Error = new string[] { "Anlaşmalı Kurum Bilgisi Bulunamadı." };
                    result.IsOk = false;

                    CreateFailedLog(log, result.ToXML(true), "Anlaşmalı Kurum Bilgisi Bulunamadı.");
                    return result;
                    #endregion
                }
                #endregion

                #region Mapping 
                IntegrationWebServicesSoapClient client = new IntegrationWebServicesSoapClient();
                HealthCenterInfo healthCenterInfo = new HealthCenterInfo
                {
                    usernamespecified = true,
                    passwordspecified = true,
                    healthcenteridspecified = false,
                    descriptionspecified = true,
                    tradenamespecified = true,
                    typeidspecified = true,
                    subtypeidspecified = false,
                    contactpersonspecified = true,
                    addressspecified = true,
                    cityidspecified = true,
                    provinceidspecified = true,
                    districtidspecified = false,
                    phonespecified = true,
                    faxspecified = false,
                    gsmspecified = true,
                    emailspecified = true,
                    taxofficeidspecified = false,
                    taxnumberspecified = true,
                    chemistrecordnumberspecified = false,
                    licensenumberspecified = false,
                    idnumberspecified = false,
                    specialtyidspecified = false,
                    claimpaydayspecified = true,
                    webspecified = false,
                    reductionspecified = false,
                    eprolimitspecified = false,
                    eprodailylimitspecified = false,
                    reductiontypeidspecified = false,
                    glnspecified = false,
                    agreementtypeidspecified = false,
                    freelanceinvoicespecified = false,
                    externalrefspecified = true,
                    taxfreespecified = false,
                    hccompanyidspecified = false,
                    uplinkspecified = true,
                    upexternalrefspecified = false,
                    bankidspecified = false,
                    branchidspecified = false,
                    accountnumberspecified = false,
                    ibanspecified = true,
                    accountownernamespecified = true,
                    statusspecified = false,
                    vateratespecified = false,
                    notespecified = false,
                    seizurestatusidspecified = false,
                    seizedbankidspecified = false,
                    seizedbranchidspecified = false,
                    seizedaccountnumberspecified = false,
                    seizedibanspecified = false,
                    seizedaccountownamespecified = false,
                    username = req.UserName,
                    password = req.Password,
                    externalref = providerDto.CompanyCode.ToString(),
                    description = providerDto.CompanyName,
                    tradename = providerDto.LegalEntityInfo.CompanyTitle,
                    typeid = providerDto.CompanyType.IsInt() ? GetNNTypeCode(providerDto.CompanyType) : "",
                    subtypeid = "",
                    contactperson = providerDto.LegalEntityInfo.AuthorizedPerson != null ? providerDto.LegalEntityInfo.AuthorizedPerson.Name + " " + providerDto.LegalEntityInfo.AuthorizedPerson.Surname : "",
                    addresss = providerDto.AddressInfo.Details,
                    cityid = providerDto.AddressInfo.ProvinceCode,
                    provinceid = "4",//providerDto.AddressInfo.DistrictCode,
                    phone = providerDto.ContactInfo.LandPhone,
                    gsm = providerDto.LegalEntityInfo.AuthorizedPerson != null ? providerDto.LegalEntityInfo.AuthorizedPerson.ContactInfo.MobilePhone : "",
                    email = providerDto.LegalEntityInfo.AuthorizedPerson != null ? providerDto.LegalEntityInfo.AuthorizedPerson.ContactInfo.EmailAddress : "",
                    taxnumber = providerDto.LegalEntityInfo.TaxNo.ToString(),
                    claimpayday = "30",
                    accountownername = providerDto.BankAccountInfo != null ? providerDto.BankAccountInfo.AccountHolderName : "",
                    iban = providerDto.BankAccountInfo != null ? providerDto.BankAccountInfo.IBAN : ""
                };

                #endregion

                #region Operation
                log.Request = healthCenterInfo.ToXML(true);
                var cevap = client.CreateOrReplaceHealthCenter(healthCenterInfo);
                if (cevap.error.ErrorCode == 0 && cevap.healthcenterid > 0)
                {
                    #region Parameter Save
                    Parameter parameter = new Parameter
                    {
                        Key = "PROVIDER_ID",
                        Value = cevap.healthcenterid.ToString(),
                        SystemType = "11"
                    };
                    SpResponse spResponseParameter = new GenericRepository<Parameter>().Insert(parameter);
                    if (spResponseParameter.Code != "100")
                    {
                        #region Error
                        result.Code = "0";
                        result.Description = "HATA - Error Listesini Kontrol Ediniz";
                        result.Error = new string[] { spResponseParameter.Message };
                        result.IsOk = false;

                        CreateFailedLog(log, result.ToXML(true), spResponseParameter.Message);
                        return result;
                        #endregion
                    }
                    ProviderParameter providerParameter = new ProviderParameter
                    {
                        ParameterId = spResponseParameter.PkId,
                        ProviderId = req.ProviderId
                    };
                    SpResponse spResponseProvider = new GenericRepository<ProviderParameter>().Insert(providerParameter);
                    if (spResponseProvider.Code != "100")
                    {
                        #region Error
                        result.Code = "0";
                        result.Description = "HATA - Error Listesini Kontrol Ediniz";
                        result.Error = new string[] { spResponseProvider.Message };
                        result.IsOk = false;

                        CreateFailedLog(log, result.ToXML(true), spResponseProvider.Message);
                        return result;
                        #endregion
                    }
                    #endregion

                    #region OK
                    result.Code = "1";
                    result.Description = "İşlem Başarılı";
                    result.IsOk = true;
                    result.CompanyObjectId = cevap.healthcenterid.ToString();
                    CreateOKLog(log, result.ToXML(true));
                    return result;
                    #endregion
                }
                else
                {
                    #region Error
                    result.Code = "0";
                    result.Description = "HATA - Error Listesini Kontrol Ediniz";
                    result.Error = new string[] { cevap.error.ErrorMessage };
                    result.IsOk = false;

                    CreateFailedLog(log, result.ToXML(true), cevap.error.ErrorMessage);
                    return result;
                    #endregion
                }
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                result.Code = "0";
                result.Description = "HATA - Error Listesini Kontrol Ediniz";
                result.Error = new string[] { ex.Message };
                result.IsOk = false;

                CreateFailedLog(log, result.ToXML(true), ex.Message);
                #endregion
            }
            return result;
        }

        public CommonProxyRes IcmalGiris(NNIcmalGirisRequest req)
        {
            CommonProxyRes result = new CommonProxyRes();
            result.IsOk = true;
            var currentMethod = MethodBase.GetCurrentMethod();

            #region IncomingLog
            IntegrationLog log = new IntegrationLog();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.WsMethod = currentMethod.Name;
            log.Request = req.ToXML(true);
            log.PayrollId = req.PayrollId;
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.WsRequestDate = DateTime.Now;
            log.Type = ((int)LogType.INCOMING).ToString();
            IntegrationLogHelper.AddToLog(log);
            #endregion

            try
            {
                List<V_Claim> claimList = new GenericRepository<V_Claim>().FindBy($"PAYROLL_ID = {req.PayrollId}", orderby: "");
                if (claimList == null)
                {
                    result.Error = new string[] { "Claim bulunamadı!" };
                    result.IsOk = false;
                    result.Code = "0";

                    CreateFailedLog(log, result.ToXML(true), string.Join(" - ", result.Error));
                    return result;
                }


                #region Mapping 
                IntegrationWebServicesSoapClient client = new IntegrationWebServicesSoapClient();
                IcmalBilgileri icmalBilgileri = new IcmalBilgileri
                {
                    IBAN = claimList[0].IBAN,
                    IcmalNo = claimList[0].PAYROLL_ID.ToString(),
                    HariciKurum = claimList[0].PROVIDER_ID.ToString(),
                    OdemeTarihi = ((DateTime)claimList[0].PAYROLL_DUE_DATE).ToString("yyyy.MM.dd"),
                    UserName = req.UserName,
                    Password = req.Password
                };

                List<TazminatBilgisi> claimDetailsList = new List<TazminatBilgisi>();

                foreach (var item in claimList)
                {
                    TazminatBilgisi tazminatBilgisi = new TazminatBilgisi
                    {
                        FaturaNo = item.BILL_NO,
                        FaturaTarihi = item.BILL_DATE != null ? ((DateTime)item.BILL_DATE).ToString("yyyy.MM.dd") : "",
                        FaturaTutari = item.PAID.ToString(),
                        ProvizyonNo = item.COMPANY_CLAIM_ID.ToString()
                    };
                    claimDetailsList.Add(tazminatBilgisi);
                }

                icmalBilgileri.TazminatBilgileri = claimDetailsList.ToArray();

                #endregion
                log.Request = icmalBilgileri.ToXML(true);
                IcmalGirisCevap cevap = client.IcmalGiris(icmalBilgileri);
                if (cevap.SonucKodu == "1")
                {
                    Payroll payroll = new GenericRepository<Payroll>().FindBy($"ID={req.PayrollId}", fetchDeletedRows: true).FirstOrDefault();
                    payroll.CompanyPayrollNo = cevap.PaketNo;

                    SpResponse spResponsePayroll = new GenericRepository<Payroll>().Insert(payroll);
                    if (spResponsePayroll.Code != "100" && spResponsePayroll.Code != "1")
                    {
                        result.Error = new string[] { "Company Payroll No Kaydedilemedi!", spResponsePayroll.Message };
                        result.IsOk = false;
                        result.Code = "0";

                        CreateFailedLog(log, result.ToXML(true), string.Join(" - ", result.Error));
                        return result;
                    }

                    result.Code = cevap.SonucKodu;
                    result.Description = cevap.SonucAciklama;
                    result.IsOk = cevap.SonucKodu == "1" ? true : false;

                    log.Response = cevap.ToXML(true);
                    log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                    log.ResponseStatusDesc = "OK";
                    log.ResponseStatusCode = "1";
                    log.WsResponseDate = DateTime.Now;
                    log.Type = ((int)LogType.COMPLETED).ToString();
                    IntegrationLogHelper.AddToLog(log);
                }
                else
                {
                    result.Code = cevap.SonucKodu;
                    result.Description = cevap.SonucAciklama;
                    result.IsOk = cevap.SonucKodu == "1" ? true : false;

                    log.Response = cevap.ToXML(true);
                    log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                    log.ResponseStatusDesc = result.Description;
                    log.ResponseStatusCode = "0";
                    log.WsResponseDate = DateTime.Now;
                    log.Type = ((int)LogType.FAILED).ToString();
                    IntegrationLogHelper.AddToLog(log);
                }
            }
            catch (Exception ex)
            {
                result.Code = "0";
                result.Description = ex.Message;
                result.Error = new string[] { ex.Message };
                result.IsOk = false;

                log.WsName = currentMethod.DeclaringType.FullName;
                log.WsMethod = currentMethod.Name;
                log.Request = req.ToXML(true);
                log.Response = result.ToXML(true);
                log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                log.WsResponseDate = DateTime.Now;
                log.ResponseStatusDesc = "ERROR";
                log.Type = ((int)LogType.FAILED).ToString();
                IntegrationLogHelper.AddToLog(log);

                return result;
            }
            return result;
        }

        public CommonProxyRes TazminatGiris(NNTazminatGirisReq req)
        {
            CommonProxyRes result = new CommonProxyRes();
            result.IsOk = true;
            var currentMethod = MethodBase.GetCurrentMethod();

            #region IncomingLog
            IntegrationLog log = new IntegrationLog();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.WsMethod = currentMethod.Name;
            log.Request = req.ToXML(true);
            log.ClaimId = req.claimId;
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.WsRequestDate = DateTime.Now;
            log.Type = ((int)LogType.INCOMING).ToString();
            log.COMPANY_ID = NNCompanyCode;
            IntegrationLogHelper.AddToLog(log);
            #endregion

            try
            {
                V_Claim claim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID = {req.claimId}", orderby: "", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                if (claim == null)
                {
                    result.Error = new string[] { "Claim bulunamadı!" };
                    result.IsOk = false;
                    result.Code = "0";
                    CreateFailedLog(log, result.ToXML(true), string.Join(" - ", result.Error));
                    return result;
                }
                else if (claim.COMPANY_CLAIM_ID != null)
                {
                    //ProxyServiceClient proxyServiceClient = new ProxyServiceClient();
                    //var res = proxyServiceClient.ClaimUpdate(req);

                    //result = res;

                    //return result;
                }

                #region Mapping 
                IntegrationWebServicesSoapClient client = new IntegrationWebServicesSoapClient();
                TazminatHesapla input = new TazminatHesapla
                {
                    UserName = req.UserName,

                    userNameSpecified = true,

                    Password = req.Password,
                    
                    passwordSpecified = true,

                    hariciHasarNo = req.claimId.ToString(),

                    hariciHasarNoSpecified = true,

                    HariciKurum = claim.PROVIDER_ID.ToString(),

                    HariciKurumSpecified = true,

                    PoliceNo = claim.POLICY_NUMBER + "|" + claim.RENEWAL_NO.ToString() + "|" + claim.COMPANY_INSURED_NO,

                    PoliceNoSpecified = true,

                    TakipNo = claim.MEDULA_NO,

                    TakipNoSpecified = claim.MEDULA_NO.IsNull() ? false : true,

                    TakipTarihi = ((DateTime)claim.CLAIM_DATE).ToString("yyyy.MM.dd"),
                    
                    TakipTarihiSpecified = true,
                };

                List<V_ClaimNote> claimNote = new GenericRepository<V_ClaimNote>().FindBy($"CLAIM_ID = {req.claimId}", orderby: "");

                if (claimNote != null && claimNote.Count > 0)
                {
                    input.EpikrizNotu = claimNote.Where(cn => cn.CLAIM_NOTE_TYPE == ((int)ClaimNoteType.EPIKRIZ).ToString()).Select(cn => cn.NOTE_DECRIPTION).FirstOrDefault();
                    input.EpikrizNotuSpecified = input.EpikrizNotu.IsNull() ? false : true;

                    input.HasarNotu = claimNote.Where(cn => cn.CLAIM_NOTE_TYPE == ((int)ClaimNoteType.NORMAL).ToString()).Select(cn => cn.NOTE_DECRIPTION).FirstOrDefault();
                    input.HasarNotuSpecified = input.HasarNotu.IsNull() ? false : true;
                }

                List<V_ClaimIcd> claimIcdList = new GenericRepository<V_ClaimIcd>().FindBy($"CLAIM_ID = {req.claimId}", orderby: "");

                List<Tani> tanilar = new List<Tani>();
                foreach (var item in claimIcdList)
                {
                    Tani tani = new Tani
                    {
                        TaniTipi = "2",
                        TaniAdi = item.ICD_NAME,
                        TaniKodu = item.ICD_CODE
                    };

                    tanilar.Add(tani);
                }

                input.Tanilar = tanilar.ToArray();
                input.TanilarSpecified = tanilar != null && tanilar.Count > 0 ? true : false;

                List<V_ClaimProcess> claimProcessList = new GenericRepository<V_ClaimProcess>().FindBy($"CLAIM_ID = {req.claimId}", orderby: "");

                if (claimProcessList != null && claimProcessList.Count > 0)
                {
                    List<V_ClaimProcess> claimProcessAList = claimProcessList.Where(c => c.COVERAGE_TYPE == ((int)CoverageType.AYAKTA).ToString()).ToList();
                    List<V_ClaimProcess> claimProcessYList = claimProcessList.Where(c => c.COVERAGE_TYPE == ((int)CoverageType.YATARAK).ToString()).ToList();

                    List<NNHayatServicePROD.TeminatTalep> teminatBilgileri = new List<NNHayatServicePROD.TeminatTalep>();

                    if (claimProcessAList != null && claimProcessAList.Count > 0)
                    {
                        NNHayatServicePROD.TeminatTalep teminatTalep = new NNHayatServicePROD.TeminatTalep
                        {
                            TeminatKodu = "307",
                            OnayTutar = ((decimal)claimProcessAList.Where(ca => ca.PAID != null).Select(ca => ca.PAID).Sum()).ToString("#0.00"),
                            TalepTutar = ((decimal)claimProcessAList.Where(ca => ca.PAID != null).Select(ca => ca.PAID).Sum()).ToString("#0.00")
                        };
                        
                        decimal HizmetSgkTutar = 0, HizmetTalepTutar = 0, HizmetOnayTutar = 0;
                        foreach (var hzmtBilgi in claimProcessAList)
                        {
                            HizmetSgkTutar += (hzmtBilgi.SGK_AMOUNT != null ? (decimal)hzmtBilgi.SGK_AMOUNT : 0);
                            HizmetTalepTutar += (hzmtBilgi.PAID != null ? (decimal)hzmtBilgi.PAID : 0);
                            HizmetOnayTutar += (hzmtBilgi.PAID != null ? (decimal)hzmtBilgi.PAID : 0);
                        }

                        HizmetBilgisi hizmetBilgisi = new HizmetBilgisi
                        {
                            HizmetSgkTutar = HizmetSgkTutar.ToString("#0.00"),
                            HizmetTalepTutar = HizmetTalepTutar.ToString("#0.00"),
                            HizmetOnayTutar = HizmetOnayTutar.ToString("#0.00"),
                            HizmetAdi = "IMECE",
                            HizmetKodu = "IMC"
                        };
                        

                        teminatTalep.TalepHizmetBilgileri = new HizmetBilgisi[] { hizmetBilgisi };

                        teminatBilgileri.Add(teminatTalep);

                    }

                    if (claimProcessYList != null && claimProcessYList.Count > 0)
                    {
                        NNHayatServicePROD.TeminatTalep teminatTalep = new NNHayatServicePROD.TeminatTalep
                        {
                            TeminatKodu = "303",
                            OnayTutar = ((decimal)claimProcessYList.Where(ca => ca.PAID != null).Select(ca => ca.PAID).Sum()).ToString("#0.00"),
                            TalepTutar = ((decimal)claimProcessYList.Where(ca => ca.PAID != null).Select(ca => ca.PAID).Sum()).ToString("#0.00")
                        };
                        
                        decimal HizmetSgkTutar = 0, HizmetTalepTutar = 0, HizmetOnayTutar = 0;
                        foreach (var hzmtBilgi in claimProcessYList)
                        {
                            HizmetSgkTutar += (hzmtBilgi.SGK_AMOUNT != null ? (decimal)hzmtBilgi.SGK_AMOUNT : 0);
                            HizmetTalepTutar += (hzmtBilgi.PAID != null ? (decimal)hzmtBilgi.PAID : 0);
                            HizmetOnayTutar += (hzmtBilgi.PAID != null ? (decimal)hzmtBilgi.PAID : 0);
                        }

                        HizmetBilgisi hizmetBilgisi = new HizmetBilgisi
                        {
                            HizmetSgkTutar = HizmetSgkTutar.ToString("#0.00"),
                            HizmetTalepTutar = HizmetTalepTutar.ToString("#0.00"),
                            HizmetOnayTutar = HizmetOnayTutar.ToString("#0.00"),
                            HizmetAdi = "IMECE",
                            HizmetKodu = "IMC"
                        };

                        teminatTalep.TalepHizmetBilgileri = new HizmetBilgisi[] { hizmetBilgisi };

                        teminatBilgileri.Add(teminatTalep);

                    }
                    input.TeminatTalepBilgileri = teminatBilgileri.ToArray();
                    input.TeminatTalepBilgileriSpecified = teminatBilgileri != null && teminatBilgileri.Count > 0 ? true : false;
                }
                #endregion

                log.Request = input.ToXML(true);
                TazminatHesaplaCevap cevap = client.TazminatGiris(input);
                if (cevap.SonucKodu == "1")
                {
                    Claim claimUpdate = new GenericRepository<Claim>().FindById(req.claimId);
                    claimUpdate.CompanyClaimId = long.Parse(cevap.ProvizyonNo);

                    SpResponse spResponseClaim = new GenericRepository<Claim>().Insert(claimUpdate);
                    if (spResponseClaim.Code != "100" && spResponseClaim.Code != "1")
                    {
                        result.Error = new string[] { "Company Claim Id Kaydedilemedi!", spResponseClaim.Message };
                        result.IsOk = false;
                        result.Code = "0";
                        
                        CreateFailedLog(log, result.ToXML(true), string.Join(" - ", result.Error));
                        return result;
                    }

                    result.CompanyObjectId = cevap.ProvizyonNo;
                    result.Code = cevap.SonucKodu;
                    result.Description = cevap.SonucAciklama;
                    result.IsOk = cevap.SonucKodu == "1" ? true : false;

                    log.Response = cevap.ToXML(true);
                    log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                    log.ResponseStatusDesc = "OK";
                    log.ResponseStatusCode = "1";
                    log.WsResponseDate = DateTime.Now;
                    log.Type = ((int)LogType.COMPLETED).ToString();
                    IntegrationLogHelper.AddToLog(log);
                }
                else
                {
                    result.Code = cevap.SonucKodu;
                    result.Description = cevap.SonucAciklama;
                    result.IsOk = cevap.SonucKodu == "1" ? true : false;

                    log.Response = cevap.ToXML(true);
                    log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                    log.ResponseStatusDesc = result.Description;
                    log.ResponseStatusCode = "0";
                    log.WsResponseDate = DateTime.Now;
                    log.Type = ((int)LogType.FAILED).ToString();
                    IntegrationLogHelper.AddToLog(log);
                }
            }
            catch (Exception ex)
            {
                result.Code = "0";
                result.Description = ex.Message;
                result.Error = new string[] { ex.Message };
                result.IsOk = false;

                log.WsName = currentMethod.DeclaringType.FullName;
                log.WsMethod = currentMethod.Name;
                log.Request = req.ToXML(true);
                log.Response = result.ToXML(true);
                log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                log.WsResponseDate = DateTime.Now;
                log.ResponseStatusDesc = "ERROR";
                log.Type = ((int)LogType.FAILED).ToString();
                IntegrationLogHelper.AddToLog(log);

                return result;
            }
            return result;
        }

        public CommonProxyRes SetProvisionStatus(NNSetProvisionStatusReq req)
        {
            CommonProxyRes result = new CommonProxyRes();
            result.IsOk = true;
            var currentMethod = MethodBase.GetCurrentMethod();

            #region IncomingLog
            IntegrationLog log = new IntegrationLog();
            log.CorrelationId = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.WsMethod = currentMethod.Name;
            log.Request = req.ToXML(true);
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.WsRequestDate = DateTime.Now;
            log.Type = ((int)LogType.INCOMING).ToString();
            IntegrationLogHelper.AddToLog(log);
            #endregion

            try
            {
                log.WsName = currentMethod.DeclaringType.FullName;
                log.WsMethod = currentMethod.Name;
                log.Request = req.ToXML(true);
                log.Response = result.ToXML(true);
                log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                log.WsResponseDate = DateTime.Now;
                log.Type = ((int)LogType.WAITING).ToString();
                IntegrationLogHelper.AddToLog(log);

                #region Mapping 
                IntegrationWebServicesSoapClient client = new IntegrationWebServicesSoapClient();
                ProvisionStatus input = new ProvisionStatus();

                input.UserName = req.UserName;
                input.userNameSpecified = req.userNameSpecified;

                input.Password = req.Password;
                input.passwordSpecified = req.passwordSpecified;

                input.ProvisionNumber = req.ProvisionNumber;
                input.ProvisionNumberSpecified = req.ProvisionNumberSpecified;

                input.NewStatu = req.NewStatu;
                input.NewStatuSpecified = req.NewStatuSpecified;

                input.ReasonCode = req.ReasonCode;
                input.ReasonCodeSpecified = req.ReasonCodeSpecified;

                input.ReasonDesc = req.ReasonDesc;
                input.ReasonDescSpecified = req.ReasonDescSpecified;

                #endregion

                ErrorClass cevap = client.SetProvisionStatus(input);
                if (cevap.ErrorCode == 0)
                {
                    result.Code = cevap.ErrorCode.ToString();
                    result.Description = cevap.ErrorMessage;
                    result.IsOk = cevap.ErrorCode == 0 ? true : false;

                    log.WsName = currentMethod.DeclaringType.FullName;
                    log.WsMethod = currentMethod.Name;
                    log.Request = req.ToXML(true);
                    log.Response = cevap.ToXML(true);
                    log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                    log.ResponseStatusDesc = "OK";
                    log.WsResponseDate = DateTime.Now;
                    log.Type = ((int)LogType.COMPLETED).ToString();
                    IntegrationLogHelper.AddToLog(log);
                }
                else
                {
                    result.Code = cevap.ErrorCode.ToString();
                    result.Description = cevap.ErrorMessage;
                    result.IsOk = cevap.ErrorCode == 0 ? true : false;

                    log.WsName = currentMethod.DeclaringType.FullName;
                    log.WsMethod = currentMethod.Name;
                    log.Request = req.ToXML(true);
                    log.Response = cevap.ToXML(true);
                    log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                    log.ResponseStatusDesc = cevap.ErrorMessage;
                    log.WsResponseDate = DateTime.Now;
                    log.Type = ((int)LogType.FAILED).ToString();
                    IntegrationLogHelper.AddToLog(log);
                }
            }
            catch (Exception ex)
            {
                result.Code = "0";
                result.Error = new string[] { ex.Message };
                result.IsOk = false;

                log.WsName = currentMethod.DeclaringType.FullName;
                log.WsMethod = currentMethod.Name;
                log.Request = req.ToXML(true);
                log.Response = result.ToXML(true);
                log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                log.WsResponseDate = DateTime.Now;
                log.ResponseStatusDesc = "ERROR";
                log.Type = ((int)LogType.FAILED).ToString();
                IntegrationLogHelper.AddToLog(log);

                return result;
            }
            return result;
        }

        public CommonProxyRes UpdateSagmerPolicyInfo(NNUpdateSagmerPolicyInfoReq req)
        {
            CommonProxyRes result = new CommonProxyRes();
            result.IsOk = true;
            var currentMethod = MethodBase.GetCurrentMethod();

            #region IncomingLog
            IntegrationLog log = new IntegrationLog();
            log.CorrelationId = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString();
            log.WsName = currentMethod.DeclaringType.FullName;
            log.WsMethod = currentMethod.Name;
            log.Request = req.ToXML(true);
            log.PolicyId = long.Parse(req.PolicyNumber);
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.WsRequestDate = DateTime.Now;
            log.Type = ((int)LogType.INCOMING).ToString();
            IntegrationLogHelper.AddToLog(log);
            #endregion

            try
            {
                log.WsName = currentMethod.DeclaringType.FullName;
                log.WsMethod = currentMethod.Name;
                log.Request = req.ToXML(true);
                log.Response = result.ToXML(true);
                log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                log.WsResponseDate = DateTime.Now;
                log.Type = ((int)LogType.WAITING).ToString();
                IntegrationLogHelper.AddToLog(log);

                #region Mapping 
                IntegrationWebServicesSoapClient client = new IntegrationWebServicesSoapClient();
                SagmerInfo input = new SagmerInfo();

                input.UserName = req.UserName;

                input.Password = req.Password;

                input.PolicyNumber = req.PolicyNumber;

                input.PolicyOrder = req.PolicyOrder;

                input.TPAPolicyNumber = req.TPAPolicyNumber;

                input.SagmerPolicyNumber = req.SagmerPolicyNumber;

                input.SagmerSendDate = req.SagmerSendDate;

                input.SagmerErrorMessage = req.SagmerErrorMessage;

                input.SagmerSendStatus = req.SagmerSendStatus;
                #endregion

                log.Request = input.ToXML(true);
                ErrorClass cevap = client.UpdateSagmerPolicyInfo(input);
                if (cevap.ErrorCode == 0)
                {

                    result.Code = cevap.ErrorCode.ToString();
                    result.Description = cevap.ErrorMessage;
                    result.IsOk = cevap.ErrorCode == 0 ? true : false;

                    log.WsName = currentMethod.DeclaringType.FullName;
                    log.WsMethod = currentMethod.Name;
                    log.Request = req.ToXML(true);
                    log.Response = cevap.ToXML(true);
                    log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                    log.ResponseStatusDesc = "OK";
                    log.WsResponseDate = DateTime.Now;
                    log.Type = ((int)LogType.COMPLETED).ToString();
                    IntegrationLogHelper.AddToLog(log);
                }
                else
                {
                    result.Code = cevap.ErrorCode.ToString();
                    result.Description = cevap.ErrorMessage;
                    result.IsOk = cevap.ErrorCode == 0 ? true : false;

                    log.WsName = currentMethod.DeclaringType.FullName;
                    log.WsMethod = currentMethod.Name;
                    log.Request = req.ToXML(true);
                    log.Response = cevap.ToXML(true);
                    log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                    log.ResponseStatusDesc = "OK";
                    log.WsResponseDate = DateTime.Now;
                    log.Type = ((int)LogType.FAILED).ToString();
                    IntegrationLogHelper.AddToLog(log);
                }
            }
            catch (Exception ex)
            {
                result.Code = "0";
                result.Error = new string[] { ex.Message };
                result.IsOk = false;

                log.WsName = currentMethod.DeclaringType.FullName;
                log.WsMethod = currentMethod.Name;
                log.Request = req.ToXML(true);
                log.Response = result.ToXML(true);
                log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
                log.WsResponseDate = DateTime.Now;
                log.ResponseStatusDesc = "ERROR";
                log.Type = ((int)LogType.FAILED).ToString();
                IntegrationLogHelper.AddToLog(log);

                return result;
            }
            return result;
        }
    }
}
