﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Data.ExternalServices.InsuranceCompanies.Util.Lib
{

    [Serializable]
    public class PoliceAktarInnerRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public string[] ValidationErrors { get; set; }
        public V_Package Package { get; set; }
    }
}
