﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.InsuranceCompanies.Util.Lib
{

    [Serializable]
    public class PoliceAktarRes
    {
        public string SonucKod { get; set; }
        public string SonucAciklama { get; set; }
        public string[] ValidationErrors { get; set; }
    }
}
