﻿using Protein.Data.ExternalServices.Common.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Protein.Data.ExternalServices.InsuranceCompanies.Util.Lib
{
    [Serializable]
    public class PoliceAktarReq
    {
        public int ReqID { get; set; } // donusler bu ID ile yapılacak.. KE kuyruk No; Aktarım ID

        public PolicePK PoliceBilgi { get; set; }

        public bool? Kontrol { get; set; }

        [XmlElement(Form = XmlSchemaForm.None)]
        public DateTime? PolBaslamaTarih { get; set; }
        [XmlElement(Form = XmlSchemaForm.Qualified)]
        public DateTime? PolBitisTarih { get; set; }
        [XmlElement(Form = XmlSchemaForm.None)]
        public DateTime? PolTanzimTarih { get; set; }

        public string DovizKod { get; set; }
        public decimal? DovizKur { get; set; }
        //  public decimal BrutPrimTL { get; set; }
        //  public decimal BrutPrimDoviz { get; set; }
        /// <summary>
        /// G/F
        /// </summary>
        public string GrupFerdi { get; set; }
        public string EskiPoliceNo { get; set; }
        public string EskiTpaPoliceNo { get; set; } // SON 3 Karakter Yenileme No Olacak   123456001

        /// <summary>
        /// ZEYL Bilgi
        /// </summary>
        public string AcenteKod { get; set; }// Şirket Acente Kod, gelmeli.
        public SigortaEttiren SigEttiren { get; set; }
        public List<Sigortali> SigortaliListesi { get; set; }

        public string OdemeSekli { get; set; }
        public int? VadeAdet { get; set; }

        public string SeyahatEdilecekUlkeKod { get; set; }
    }
}
