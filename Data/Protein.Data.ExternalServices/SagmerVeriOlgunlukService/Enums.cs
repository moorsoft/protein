﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.SagmerVeriOlgunlukService
{
    public class Enums
    {
        public enum olgunlukHasarDurumType
        {
            /// <remarks/>
            SBM_DOSYA_FARKI,

            /// <remarks/>
            SIRKET_DOSYA_FARKI,

            /// <remarks/>
            TUTAR_FARKI,

            /// <remarks/>
            FARK_YOK,
        }
        public enum olgunlukHasarKalemType
        {

            /// <remarks/>
            TOPLAM_DOSYA_ADEDI,

            /// <remarks/>
            TOPLAM_MASRAF,

            /// <remarks/>
            TOPLAM_TAZMINAT,

            /// <remarks/>
            TOPLAM_RUCU,

            /// <remarks/>
            TOPLAM_SOVTAJ,
        }
    }
}
