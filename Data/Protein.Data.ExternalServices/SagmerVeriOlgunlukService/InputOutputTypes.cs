﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Protein.Data.ExternalServices.SagmerVeriOlgunlukService.Enums;

namespace Protein.Data.ExternalServices.SagmerVeriOlgunlukService
{
    public class InputOutputTypes
    {
        public class hasarFarkVeriSorguInput
        {
            public DateTime ihbarOdemeTarihi { get; set; }
            public int sayfaNumarasi { get; set; }
            public int urunKod { get; set; }
        }
        public class hasarFarkVeriSorguOutput
        {
            public hasarFarkVeriOutput[] hasarFarkVeriList
            { get; set; }
        }
        public class hasarFarkVeriOutput
        {
            public string hasarDosyaNo { get; set; }
            public olgunlukHasarDurumType hasarDurum { get; set; }
            public System.DateTime ihbarOdemeTarihi { get; set; }
            public olgunlukHasarKalemType kalemTipi { get; set; }
            public decimal sbmTutar { get; set; }
            public decimal tutar { get; set; }
        }
        public class hasarDosyaInput
        {
            public string hasarDosyaNo { get; set; }
            public decimal tutar { get; set; }
        }
        public class hasarGunBazliOlgunlukDetayInput
        {
            public hasarDosyaInput[] hasarDosyaList { get; set; }
            public string islemSiraNo { get; set; }
            public string kalemTipi { get; set; }
        }

        public class hasarGunBazliOlgunlukDetayOutput : output
        {

        }
        public class output
        {
            public string[] aciklamalar { get; set; }
            public bool islemBasarili { get; set; }
        }

        public class hasarGunBazliIcmalInput
        {
            public string islemSiraNo { get; set; }
            public long toplamDosyaAdedi { get; set; }
            public decimal toplamMasraf { get; set; }
            public long toplamMasrafDosyaAdedi { get; set; }
            public decimal toplamRucu { get; set; }
            public long toplamRucuDosyaAdedi { get; set; }
            public decimal toplamSovtaj { get; set; }
            public long toplamSovtajDosyaAdedi { get; set; }
            public decimal toplamTazminat { get; set; }
            public long toplamTazminatDosyaAdedi { get; set; }
        }
        public class hasarGunBazliOlgunlukIcmalInput
        {
            public hasarGunBazliIcmalInput[] olgunlukIcmalList { get; set; }
        }
    }
}
