﻿using Protein.Data.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Data.ExternalServices.SagmerOnlineService.Enums;
using static Protein.Data.ExternalServices.SagmerOnlineService.InputOutputTypes;

namespace Protein.Data.ExternalServices.SagmerOnlineService
{
    public class SgmrNonWs
    {
        ServiceClient serviceClient;
     
        public ServiceResponse<policeSonucType> police(long PolicyId, string ApiCode, SagmerServiceType ServiceType)
        {
            // Sigortalılar için... giriş zeyli çalışılıyor.
            serviceClient = new ServiceClient();
            ServiceResponse<policeSonucType> response = new ServiceResponse<policeSonucType>();

            #region ApiCode Kontrol
            if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            #endregion
            #region IsSagmerSendViaPolicyViaPolicy
            SagmerHelper sagmerHelper = new SagmerHelper();
            if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            {
                response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
                response.Data.islemBasarili = false;
                return response;
            }
            #endregion

            policeInputType input = new policeInputType();

            #region FillPolicyDetails
            V_SgmPolicy sgmPolicy = new GenericRepository<V_SgmPolicy>().FindBy($"POLICY_ID = {PolicyId}", fetchDeletedRows: true, fetchHistoricRows: true, orderby: "").FirstOrDefault();
            #endregion

            #region Policy Check
            if (sgmPolicy == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Poliçe detayı bulunamadı";
                return response;
            }
            #endregion
            #region Fill Fields
            try
            {
                input.branshKod = long.Parse(sgmPolicy.BranshCode);
                input.dovizCinsi = sgmPolicy.DovizCinsi;
                input.endirektSigortaSirketKod = sgmPolicy.EndirektSigortaSirketKod;
                input.eskiPoliceNo = sgmPolicy.EskiPoliceNo;
                input.eskiYenilemeNo = string.IsNullOrEmpty(sgmPolicy.EskiYenilemeNo) ? 0 : int.Parse(sgmPolicy.EskiYenilemeNo);
                input.ilKod = sgmPolicy.IlKod;
                input.odemeKod = string.IsNullOrEmpty(sgmPolicy.OdemeKod) ? 0 : long.Parse(sgmPolicy.OdemeKod);
                //input.otorizasyonKod = sgmPolicy.OtorizasyonKod;
                input.policeBaslamaTarihi = DateTime.Parse(sgmPolicy.PoliceBaslamaTarihi);
                input.policeBitisTarihi = DateTime.Parse(sgmPolicy.PoliceBitisTarihi);
                input.policeBrutPrimi = decimal.Parse(sgmPolicy.PoliceBrutPrimi);
                input.policeNetPrimi = decimal.Parse(sgmPolicy.PoliceNetPrimi);
                input.policeNo = sgmPolicy.PoliceNo;
                input.policeTanzimTarihi = DateTime.Parse(sgmPolicy.PoliceTanzimTarihi);
                input.policeTip = (policeTipiType)System.Enum.Parse(typeof(uyrukTuru), sgmPolicy.PoliceTip);
                input.sigortaEttirenType = new sigortaEttirenInputType();
                input.sigortaEttirenType.ad = sgmPolicy.SgtAd;
                input.sigortaEttirenType.adres = sgmPolicy.SgtAdres;
                input.sigortaEttirenType.babaAdi = sgmPolicy.SgtBabaAdi;
                input.sigortaEttirenType.cinsiyet = (cinsiyet)System.Enum.Parse(typeof(cinsiyet), sgmPolicy.SgtCinsiyet);
                input.sigortaEttirenType.dogumTarihi = DateTime.Parse(sgmPolicy.SgtDogumTarihi);
                input.sigortaEttirenType.dogumYeri = sgmPolicy.SgtDogumYeri;
                input.sigortaEttirenType.kimlikNo = sgmPolicy.SgtKimlikNo;
                input.sigortaEttirenType.kimlikTipKod = sgmPolicy.SgtKimlikTipKod;

                DateTime? kurulusTarihi = null;
                if (!string.IsNullOrEmpty(sgmPolicy.SgtKurulusTarihi))
                    kurulusTarihi = DateTime.Parse(sgmPolicy.SgtKurulusTarihi);

                input.sigortaEttirenType.kurulusTarihi = kurulusTarihi;
                input.sigortaEttirenType.kurulusYeri = sgmPolicy.SgtKurulusYeri;
                input.sigortaEttirenType.sigortaEttirenUyruk = (uyrukTuru)System.Enum.Parse(typeof(uyrukTuru), sgmPolicy.SgtSigortaEttirenUyruk);
                input.sigortaEttirenType.soyad = sgmPolicy.SgtSoyad;
                input.sigortaEttirenType.turKod = (turKod)System.Enum.Parse(typeof(turKod), sgmPolicy.SgtTurKod);
                input.sigortaEttirenType.ulkeKodu = int.Parse(sgmPolicy.SgtUlkeKodu);
                input.sigortaSirketKod = sgmPolicy.SigortaSirketKod;
                input.tarifeID = string.IsNullOrEmpty(sgmPolicy.TarifeId) ? 0 : long.Parse(sgmPolicy.TarifeId);
                input.uretimKaynakKod = string.IsNullOrEmpty(sgmPolicy.UretimKaynakKod) ? 0 : long.Parse(sgmPolicy.UretimKaynakKod);
                input.uretimKaynakKurumKod = sgmPolicy.UretimKaynakKurumKod;
                input.vadeSayisi = string.IsNullOrEmpty(sgmPolicy.VadeSayisi) ? 0 : int.Parse(sgmPolicy.VadeSayisi);
                input.yenilemeNo = string.IsNullOrEmpty(sgmPolicy.YenilemeNo) ? 0 : int.Parse(sgmPolicy.YenilemeNo);
                input.yeniYenilemeGecis = sgmPolicy.YeniYenilemeGecis;
                input.zeylNo = string.IsNullOrEmpty(sgmPolicy.ZeylNo) ? 0 : int.Parse(sgmPolicy.ZeylNo);
            }
            catch (Exception ex)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = ex.ToString();
                return response;
            }
            #endregion

            if (ServiceType == SagmerServiceType.KONTROL)
                return serviceClient.policeKontrol(input,long.Parse(sgmPolicy.CompanyId));
            else return serviceClient.police(input, long.Parse(sgmPolicy.CompanyId));
        }
       
        public ServiceResponse<policeSonucType> seyahatPolice(long PolicyId, string ApiCode, SagmerServiceType ServiceType)
        {
            serviceClient = new ServiceClient();
            ServiceResponse<policeSonucType> response = new ServiceResponse<policeSonucType>();
            #region ApiCode Kontrol
            if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            #endregion
            #region IsSagmerSendViaPolicy

            SagmerHelper sagmerHelper = new SagmerHelper();
            if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            {
                response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
                response.Data.islemBasarili = false;
                return response;
            }

            #endregion
            seyahatPoliceInputType input = new seyahatPoliceInputType();

            #region FillPolicyDetails
            V_SgmPolicy sgmPolicy = new GenericRepository<V_SgmPolicy>().FindBy($"POLICY_ID = {PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            #endregion
            #region Policy Check
            if (sgmPolicy == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Poliçe detayı bulunamadı";
                return response;
            }
            #endregion
            #region Fill Fields
            try
            {
                input.dovizCinsi = sgmPolicy.DovizCinsi;
                input.endirektSigortaSirketKod = sgmPolicy.EndirektSigortaSirketKod;
                input.eskiPoliceNo = sgmPolicy.EskiPoliceNo;
                input.eskiYenilemeNo = string.IsNullOrEmpty(sgmPolicy.EskiYenilemeNo) ? 0 : int.Parse(sgmPolicy.EskiYenilemeNo);
                input.ilKod = sgmPolicy.IlKod;
                input.odemeKod = string.IsNullOrEmpty(sgmPolicy.OdemeKod) ? 0 : long.Parse(sgmPolicy.OdemeKod);
                input.otorizasyonKodField = sgmPolicy.OtorizasyonKod;
                input.policeBaslamaTarihi = DateTime.Parse(sgmPolicy.PoliceBaslamaTarihi);
                input.policeBitisTarihi = DateTime.Parse(sgmPolicy.PoliceBitisTarihi);
                input.policeBrutPrimi = decimal.Parse(sgmPolicy.PoliceBrutPrimi);
                input.policeNetPrimi = decimal.Parse(sgmPolicy.PoliceNetPrimi);
                input.policeNo = sgmPolicy.PoliceNo;
                input.policeTanzimTarihi = DateTime.Parse(sgmPolicy.PoliceTanzimTarihi);
                input.policeTip = (policeTipiType)System.Enum.Parse(typeof(policeTipiType), sgmPolicy.PoliceTip);
                input.sigortaEttirenType = new sigortaEttirenInputType();
                input.sigortaEttirenType.ad = sgmPolicy.SgtAd;
                input.sigortaEttirenType.adres = sgmPolicy.SgtAdres;
                input.sigortaEttirenType.babaAdi = sgmPolicy.SgtBabaAdi;

                input.sigortaEttirenType.cinsiyet = (cinsiyet)System.Enum.Parse(typeof(cinsiyet), sgmPolicy.SgtCinsiyet);
                input.sigortaEttirenType.dogumTarihi = DateTime.Parse(sgmPolicy.SgtDogumTarihi);
                input.sigortaEttirenType.dogumYeri = sgmPolicy.SgtDogumYeri;
                input.sigortaEttirenType.kimlikNo = sgmPolicy.SgtKimlikNo;
                input.sigortaEttirenType.kimlikTipKod = sgmPolicy.SgtKimlikTipKod;
                input.sigortaEttirenType.kurulusTarihi = DateTime.Parse(sgmPolicy.SgtKurulusTarihi);
                input.sigortaEttirenType.kurulusYeri = sgmPolicy.SgtKurulusYeri;
                input.sigortaEttirenType.sigortaEttirenUyruk = (uyrukTuru)System.Enum.Parse(typeof(uyrukTuru), sgmPolicy.SgtSigortaEttirenUyruk);
                input.sigortaEttirenType.soyad = sgmPolicy.SgtSoyad;
                input.sigortaEttirenType.turKod = (turKod)System.Enum.Parse(typeof(turKod), sgmPolicy.SgtTurKod);
                input.sigortaEttirenType.ulkeKodu = int.Parse(sgmPolicy.SgtUlkeKodu);
                input.sigortaSirketKodField = sgmPolicy.SigortaSirketKod;
                input.tarifeID = string.IsNullOrEmpty(sgmPolicy.TarifeId) ? 0 : long.Parse(sgmPolicy.TarifeId);
                input.uretimKaynakKod = string.IsNullOrEmpty(sgmPolicy.UretimKaynakKod) ? 0 : long.Parse(sgmPolicy.UretimKaynakKod);
                input.uretimKaynakKurumKod = sgmPolicy.UretimKaynakKurumKod;
                input.vadeSayisi = string.IsNullOrEmpty(sgmPolicy.VadeSayisi) ? 0 : int.Parse(sgmPolicy.VadeSayisi);
                input.yenilemeNo = string.IsNullOrEmpty(sgmPolicy.YenilemeNo) ? 0 : int.Parse(sgmPolicy.YenilemeNo);
                input.yeniYenilemeGecis = sgmPolicy.YeniYenilemeGecis;
                input.zeylNo = string.IsNullOrEmpty(sgmPolicy.ZeylNo) ? 0 : int.Parse(sgmPolicy.ZeylNo);
            }
            catch (Exception ex)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = ex.ToString();
                return response;
            }
            #endregion
            if (ServiceType == SagmerServiceType.KONTROL)
                return serviceClient.seyahatPoliceKontrol(input, long.Parse(sgmPolicy.CompanyId));
            else return serviceClient.seyahatPolice(input, long.Parse(sgmPolicy.CompanyId));

        }
        public ServiceResponse<zeylSonucType> sigortaliGirisZeyli(long PolicyId, long EndorsementId, string ApiCode, SagmerServiceType ServiceType)
        {
            serviceClient = new ServiceClient();
            ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            #region ApiCode Kontrol
            if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            #endregion
            #region IsSagmerSendViaPolicy
            SagmerHelper sagmerHelper = new SagmerHelper();
            if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            {
                response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
                response.Data.islemBasarili = false;
                return response;
            }
            #endregion
            sigortaliGirisInputType input = new sigortaliGirisInputType();
            //input.policeNetPrimi
            #region FillEndorsDetails
            V_SgmIngressEndorsement sgmEndors = new GenericRepository<V_SgmIngressEndorsement>().FindBy($" ENDORSEMENT_ID = {EndorsementId} AND POLICY_ID = {PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            List<V_SgmIngressEndorsInsured> sgmEndorsInsured = new GenericRepository<V_SgmIngressEndorsInsured>().FindBy($" ENDORSEMENT_ID = {EndorsementId} AND POLICY_ID = {PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            #endregion
            #region Endors Check
            if (sgmEndors == null || sgmEndorsInsured.Count < 1)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Zeyl detayı bulunamadı";
                return response;
            }
            #endregion
            #region Fill Fields
            input.zeylTuru = long.Parse(sgmEndors.ZeylTuru);
            input.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);
            input.zeylNo = int.Parse(sgmEndors.ZeylNo);
            input.zeylNetPrimi = decimal.Parse(sgmEndors.ZeylNetPrimi);
            input.zeylBrutPrimi = decimal.Parse(sgmEndors.ZeylBrutPrimi);
            input.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
            input.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
            input.sigortaSirketKod = sgmEndors.SigortaSirketKod;
            input.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
            input.otorizasyonKod = sgmEndors.OtorizasyonKod;
            input.policeNo = sgmEndors.PoliceNo;
            input.policeNetPrimi = decimal.Parse(sgmEndors.PoliceNetPrimi);
            input.policeBrutPrimi = decimal.Parse(sgmEndors.PoliceBrutPrimi);
            List<sigortali> lstInsured = new List<sigortali>();
            foreach (V_SgmIngressEndorsInsured item in sgmEndorsInsured)
            {
                List<V_SgmIngressInsCover> sgmInsuredCover = new GenericRepository<V_SgmIngressInsCover>().FindBy($"INSURED_ID '{item.InsuredId}' and POLICY_ID = '{PolicyId}' and ENDORSEMENT_ID = '{EndorsementId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                V_SgmIngressInsHealthInfo healthInfo = new GenericRepository<V_SgmIngressInsHealthInfo>().FindBy($"INSURED_ID '{item.InsuredId}' and POLICY_ID = '{PolicyId}' and ENDORSEMENT_ID = '{EndorsementId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                List<V_SgmIngressInsHealthDet> healthInfoDet = new GenericRepository<V_SgmIngressInsHealthDet>().FindBy($"INSURED_ID '{item.InsuredId}' and POLICY_ID = '{PolicyId}' and ENDORSEMENT_ID = '{EndorsementId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                List<V_SgmIngressExtraPremium> ExtraPrm = new GenericRepository<V_SgmIngressExtraPremium>().FindBy($"INSURED_ID '{item.InsuredId}' and POLICY_ID = '{PolicyId}' and ENDORSEMENT_ID = '{EndorsementId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                List<V_SgmIngressInsExemption> sgmExemption = new GenericRepository<V_SgmIngressInsExemption>().FindBy($"INSURED_ID '{item.InsuredId}' and POLICY_ID = '{PolicyId}' and ENDORSEMENT_ID = '{EndorsementId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

                sigortaliSaglikBilgileriInputType sigSagBilg = new sigortaliSaglikBilgileriInputType();
                List<sigortaliSaglikBilgileriDetayInputType> sigSagBilgDetay = new List<sigortaliSaglikBilgileriDetayInputType>();

                List<sigortaliIndirimEkprimInputType> sigIndEkp = new List<sigortaliIndirimEkprimInputType>();

                List<sigortaliTeminatInputTypeList> sigTem = new List<sigortaliTeminatInputTypeList>();
                List<sigortaliUygulamaInputType> sigUyg = new List<sigortaliUygulamaInputType>();

                sigSagBilg.alkolKullaniyorMu = (Enums.evetHayirType)int.Parse(healthInfo.AlkolKullaniliyorMu);
                sigSagBilg.alkolKullanmaSuresi = healthInfo.AlkolKullanmaSuresi;
                sigSagBilg.alkolPeriyotTip = (Enums.aliskanlikPeriyodType)int.Parse(healthInfo.AlkolPeriyotTip);
                sigSagBilg.boy = int.Parse(healthInfo.Boy);
                sigSagBilg.kilo = int.Parse(healthInfo.Kilo);
                sigSagBilg.periyottakiAlkolMiktari = healthInfo.PeriyottakiAlkolMiktari;
                sigSagBilg.periyottakiSigaraAdedi = healthInfo.PeriyottakiSigaraAdedi;
                sigSagBilg.sigaraIciyorMu = (Enums.evetHayirType)int.Parse(healthInfo.SigaraIciyorMu);
                sigSagBilg.sigaraIcmeSuresi = healthInfo.SigaraIcmeSuresi;
                sigSagBilg.sigaraPeriyotTip = (Enums.aliskanlikPeriyodType)int.Parse(healthInfo.SigaraperiyotTip);

                foreach (V_SgmIngressInsHealthDet itemHealthDet in healthInfoDet)
                {
                    sigSagBilgDetay.Add(new sigortaliSaglikBilgileriDetayInputType
                    {
                        ameliyatVarmi = itemHealthDet.AmeliyatVarMi,
                        hastalikAciklamasi = itemHealthDet.HastalikAciklamasi,
                        hastalikAdi = itemHealthDet.HastalikAdi,
                        hastalikKod = itemHealthDet.HastalikKod,
                        hastalikUygulamaKod = long.Parse(itemHealthDet.HastalikUygulamaKod),
                        sonDurum = itemHealthDet.SonDurum,
                        sureAy = int.Parse(itemHealthDet.SureAy),
                        sureYil = int.Parse(itemHealthDet.SureYil),
                        uygulamaAciklamasi = itemHealthDet.UygulamaAciklamasi
                    });
                }
                sigSagBilg.saglikDetayBilgileri = sigSagBilgDetay.ToArray();
                foreach (V_SgmIngressExtraPremium itemExtraPrm in ExtraPrm)
                {
                    sigIndEkp.Add(new sigortaliIndirimEkprimInputType
                    {
                        aciklama = itemExtraPrm.Aciklama,
                        indirimEkprimDegeri = decimal.Parse(itemExtraPrm.IndirimEkPrimDegeri),
                        indirimEkprimKod = long.Parse(itemExtraPrm.IndirimEkPrimKod),
                        kayitNo = long.Parse(itemExtraPrm.KayitNo),
                        tutarMiOranMi = itemExtraPrm.TutarMiOranMi
                    });
                }
                foreach (V_SgmIngressInsCover itemInsCover in sgmInsuredCover)
                {
                    sigTem.Add(new sigortaliTeminatInputTypeList
                    {
                        aso = (Enums.evetHayirType)int.Parse(itemInsCover.Aso),
                        cografiKapsamKodu = long.Parse(itemInsCover.CografiKapsamKodu),
                        donemBaslangicTarihi = DateTime.Parse(itemInsCover.DonemBaslangicTarihi),
                        donemBitisTarihi = DateTime.Parse(itemInsCover.DonemBitisTarihi),
                        donemKodu = int.Parse(itemInsCover.DonemKodu),
                        dovizCinsi = itemInsCover.DovizCinsi,
                        teminatAdedi = int.Parse(itemInsCover.TeminatAdedi),
                        teminatKodu = long.Parse(itemInsCover.TeminatKodu),
                        teminatLimiti = decimal.Parse(itemInsCover.TeminatLimiti),
                        teminatLimitKodu = long.Parse(itemInsCover.TeminatLimitKodu),
                        teminatMuafiyeti =itemInsCover.TeminatMuafiyeti,
                        teminatMuafiyetKodu = itemInsCover.TeminatMuafiyetKodu,
                        teminatNetPrim = decimal.Parse(itemInsCover.TeminatNetPrim),
                        teminatOdemeYuzdesi = decimal.Parse(itemInsCover.TeminatOdemeYuzdesi),
                        teminatVaryasyonKodu = int.Parse(itemInsCover.TeminatVaryasyonKodu),
                        toplamDonemSayisi = int.Parse(itemInsCover.ToplamDonemSayisi)
                    });
                }
                foreach (V_SgmIngressInsExemption itemExemption in sgmExemption)
                {
                    sigUyg.Add(new sigortaliUygulamaInputType
                    {
                        aciklama = itemExemption.Aciklama,
                        hastalikKod = itemExemption.HastalikKod,
                        kayitNo = long.Parse(itemExemption.KayitNo),
                        muafiyetTutarLimiti = decimal.Parse(itemExemption.MuafiyetTutarLimiti),
                        sigortaliUygulamaKod = long.Parse(itemExemption.SigortaliUygulamaKod)
                    });
                }

                lstInsured.Add(new sigortali
                {
                    adres = item.Adres,
                    aileBagNo = item.AileBagNo,
                    babaAdi = item.BabaAdi,
                    bireySiraNo = item.BireySiraNo,
                    bireyTipKod = long.Parse(item.BireyTipKod),
                    brutPrim = decimal.Parse(item.BrutPrim),
                    cinsiyet = (Enums.cinsiyet)int.Parse(item.Cinsiyet),
                    dogumTarihi = DateTime.Parse(item.DogumTarihi),
                    dogumYeri = item.DogumYeri,
                    eskiPoliceNo = item.EskiPoliceNo,
                    eskiYenilemeNo = int.Parse(item.EskiYenilemeNo),
                    ilkOzelSigortalilikTarihi = DateTime.Parse(item.IlkOzelSigortalilikTarihi),
                    ilkOzelSirketKod = item.IlkOzelSirketKod,
                    kimlikNo = item.KimlikNo,
                    kimlikTipKod = item.KimlikTipKod,
                    meslekKod = long.Parse(item.MeslekKod),
                    netPrim = decimal.Parse(item.NetPrim),
                    sigortaliAd = item.SigortaliAd,
                    sigortaliSoyad = item.SigortaliAd,
                    sigortaliUyruk = (Enums.uyrukTuru)int.Parse(item.SigortaliUyruk),
                    ulkeKodu = int.Parse(item.UlkeKodu),
                    urunPlanKod = long.Parse(item.UrunPlanKod),
                    yasadigiIlceKod = item.YasadigiIlceKod,
                    yasadigiIlKod = item.YasadigiIlKod,
                    saglikBilgileriInputType = sigSagBilg,
                    sigortaliIndirimEkprimInputTypeList = sigIndEkp.ToArray(),
                    sigortaliTeminatInputTypeListNotXML = sigTem.ToArray(),
                    sigortaliUygulamaList = sigUyg.ToArray()
                });
            }
            input.sigortaliList = lstInsured.ToArray();
            #endregion
            if (ServiceType == SagmerServiceType.KONTROL)
                return serviceClient.sigortaliGirisZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
            else
                return serviceClient.sigortaliGirisZeyli(input, long.Parse(sgmEndors.CompanyId));

        }
        public ServiceResponse<seyahatSigortaliSonucType> seyahatSigortaliGirisZeyli(long PolicyId, long EndorsementId, string ApiCode, SagmerServiceType ServiceType)
        {
            serviceClient = new ServiceClient();
            ServiceResponse<seyahatSigortaliSonucType> response = new ServiceResponse<seyahatSigortaliSonucType>();
            #region ApiCode Kontrol
            if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            #endregion
            #region IsSagmerSendViaPolicy
            SagmerHelper sagmerHelper = new SagmerHelper();
            if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            {
                response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
                response.Data.islemBasarili = false;
                return response;
            }
            #endregion
            seyahatSigortaliGirisInputType input = new seyahatSigortaliGirisInputType();

            #region FillEndorsDetails
            V_SgmIngressEndorsement sgmEndors = new GenericRepository<V_SgmIngressEndorsement>().FindBy($" ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = {PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            List<V_SgmIngressEndorsInsured> sgmEndorsInsured = new GenericRepository<V_SgmIngressEndorsInsured>().FindBy($"ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            #endregion

            #region Fill Fields
            input.zeylTuru = long.Parse(sgmEndors.ZeylTuru);
            input.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);
            input.zeylNo = int.Parse(sgmEndors.ZeylNo);
            input.zeylNetPrimi = decimal.Parse(sgmEndors.ZeylNetPrimi);
            input.zeylBrutPrimi = decimal.Parse(sgmEndors.ZeylBrutPrimi);
            input.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
            input.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
            input.sigortaSirketKod = sgmEndors.SigortaSirketKod;
            input.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
            input.otorizasyonKod = sgmEndors.OtorizasyonKod;
            input.policeNo = sgmEndors.PoliceNo;
            input.policeNetPrimi = decimal.Parse(sgmEndors.PoliceNetPrimi);
            input.policeBrutPrimi = decimal.Parse(sgmEndors.PoliceBrutPrimi);
            List<seyahatSigortaliTeminatliInputType> lstInsured = new List<seyahatSigortaliTeminatliInputType>();
            foreach (V_SgmIngressEndorsInsured item in sgmEndorsInsured)
            {
                List<V_SgmIngressInsCover> sgmInsuredCover = new GenericRepository<V_SgmIngressInsCover>().FindBy($"INSURED_ID '{item.InsuredId}' and POLICY_ID = '{PolicyId}' and ENDORSEMENT_ID = '{EndorsementId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                List<V_SgmIngressExtraPremium> ExtraPrm = new GenericRepository<V_SgmIngressExtraPremium>().FindBy($"INSURED_ID '{item.InsuredId}' and POLICY_ID = '{PolicyId}' and ENDORSEMENT_ID = '{EndorsementId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

                //seyahatSigortaliTeminatliInputType seySigTem = new seyahatSigortaliTeminatliInputType();

                List<sigortaliIndirimEkprimInputType> sigIndEkp = new List<sigortaliIndirimEkprimInputType>();
                List<seyahatSigortaliTeminatInputType> sigTem = new List<seyahatSigortaliTeminatInputType>();

                foreach (V_SgmIngressExtraPremium itemExtraPrm in ExtraPrm)
                {
                    sigIndEkp.Add(new sigortaliIndirimEkprimInputType
                    {
                        aciklama = itemExtraPrm.Aciklama,
                        indirimEkprimDegeri = decimal.Parse(itemExtraPrm.IndirimEkPrimDegeri),
                        indirimEkprimKod = long.Parse(itemExtraPrm.IndirimEkPrimKod),
                        kayitNo = long.Parse(itemExtraPrm.KayitNo),
                        tutarMiOranMi = itemExtraPrm.TutarMiOranMi
                    });
                }
                foreach (V_SgmIngressInsCover itemInsCover in sgmInsuredCover)
                {
                    sigTem.Add(new seyahatSigortaliTeminatInputType
                    {
                        cografiKapsamKodu = long.Parse(itemInsCover.CografiKapsamKodu),
                        dovizCinsi = itemInsCover.DovizCinsi,
                        teminatAdedi = int.Parse(itemInsCover.TeminatAdedi),
                        teminatKodu = long.Parse(itemInsCover.TeminatKodu),
                        teminatLimiti = decimal.Parse(itemInsCover.TeminatLimiti),
                        teminatLimitKodu = long.Parse(itemInsCover.TeminatLimitKodu),
                        teminatMuafiyeti = decimal.Parse(itemInsCover.TeminatMuafiyeti),
                        teminatMuafiyetKodu = long.Parse(itemInsCover.TeminatMuafiyetKodu),
                        teminatNetPrim = decimal.Parse(itemInsCover.TeminatNetPrim),
                        teminatOdemeYuzdesi = decimal.Parse(itemInsCover.TeminatOdemeYuzdesi),
                        teminatVaryasyonKodu = int.Parse(itemInsCover.TeminatVaryasyonKodu)
                    });
                }

                lstInsured.Add(new seyahatSigortaliTeminatliInputType
                {
                    adres = item.Adres,
                    //aileBagNo = item.AileBagNo,
                    babaAdi = item.BabaAdi,
                    bireySiraNo = item.BireySiraNo,
                    bireyTipKod = long.Parse(item.BireyTipKod),
                    brutPrim = decimal.Parse(item.BrutPrim),
                    cinsiyet = (Enums.cinsiyet)int.Parse(item.Cinsiyet),
                    dogumTarihi = DateTime.Parse(item.DogumTarihi),
                    dogumYeri = item.DogumYeri,
                    eskiPoliceNo = item.EskiPoliceNo,
                    eskiYenilemeNo = int.Parse(item.EskiYenilemeNo),
                    //ilkOzelSigortalilikTarihi = DateTime.Parse(item.IlkOzelSigortalilikTarihi),
                    //ilkOzelSirketKod = item.IlkOzelSirketKod,
                    kimlikNo = item.KimlikNo,
                    kimlikTipKod = item.KimlikTipKod,
                    //meslekKod = long.Parse(item.MeslekKod),
                    netPrim = decimal.Parse(item.NetPrim),
                    sigortaliAd = item.SigortaliAd,
                    sigortaliSoyad = item.SigortaliAd,
                    sigortaliUyruk = (Enums.uyrukTuru)int.Parse(item.SigortaliUyruk),
                    ulkeKodu = int.Parse(item.UlkeKodu),
                    urunPlanKod = long.Parse(item.UrunPlanKod),
                    yasadigiIlceKod = item.YasadigiIlceKod,
                    yasadigiIlKod = item.YasadigiIlKod,
                    sigortaliIndirimEkprimInputTypeList = sigIndEkp.ToArray(),
                    sigortaliTeminatInputTypeListField = sigTem.ToArray()
                });
            }
            input.sigortali = lstInsured.ToArray();
            #endregion

            if (ServiceType == SagmerServiceType.KONTROL)
                return serviceClient.seyahatSigortaliGirisZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
            else
                return serviceClient.seyahatSigortaliGirisZeyli(input, long.Parse(sgmEndors.CompanyId));
        }
        public ServiceResponse<zeylSonucType> sigortaliCikisZeyli(long PolicyId, long EndorsementId, string ApiCode, SagmerServiceType ServiceType)
        {
            serviceClient = new ServiceClient();
            ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            #region ApiCode Kontrol
            if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            #endregion
            #region IsSagmerSendViaPolicy
            SagmerHelper sagmerHelper = new SagmerHelper();
            if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            {
                response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
                response.Data.islemBasarili = false;
                return response;
            }
            #endregion
            sigortaliCikisInputType input = new sigortaliCikisInputType();
            #region FillEndorsDetails
            V_SgmEgressEndorsement sgmEndors = new GenericRepository<V_SgmEgressEndorsement>().FindBy($"ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            #endregion
            #region Endors Check
            if (sgmEndors == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Zeyl detayı bulunamadı";
                return response;
            }
            #endregion
            #region FillFields
            input.brutPrim = decimal.Parse(sgmEndors.BrutPrim);
            input.kimlikNo = sgmEndors.KimlikNo;
            input.kimlikTipKod = sgmEndors.KimlikTipKod;
            input.netPrim = decimal.Parse(sgmEndors.NetPrim);
            input.otorizasyonKod = sgmEndors.OtorizasyonKod;
            input.policeBrutPrimi = decimal.Parse(sgmEndors.PoliceBrutPrimi);
            input.policeNetPrimi = decimal.Parse(sgmEndors.PoliceNetPrimi);
            input.policeNo = sgmEndors.PoliceNo;
            input.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
            input.sigortaSirketKod = sgmEndors.SigortaSirketKod;
            input.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
            input.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
            input.zeylBrutPrimi = decimal.Parse(sgmEndors.ZeylBrutPrimi);
            input.zeylNetPrimi = decimal.Parse(sgmEndors.ZeylNetPrimi);
            input.zeylNo = int.Parse(sgmEndors.ZeylNo);
            input.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);

            #endregion

            if (ServiceType == SagmerServiceType.KONTROL)
                return serviceClient.sigortaliCikisZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
            else
                return serviceClient.sigortaliCikisZeyli(input, long.Parse(sgmEndors.CompanyId));
        }
        public ServiceResponse<zeylSonucType> seyahatSigortaliCikisZeyli(long PolicyId, long EndorsementId, string ApiCode, SagmerServiceType ServiceType)
        {
            serviceClient = new ServiceClient();
            ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            #region ApiCode Kontrol
            if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            #endregion
            #region IsSagmerSendViaPolicy
            SagmerHelper sagmerHelper = new SagmerHelper();
            if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            {
                response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
                response.Data.islemBasarili = false;
                return response;
            }
            #endregion
            seyahatSigortaliCikisInputType input = new seyahatSigortaliCikisInputType();

            #region FillEndorsDetails
            V_SgmEgressEndorsement sgmEndors = new GenericRepository<V_SgmEgressEndorsement>().FindBy($"ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            #endregion
            #region Endors Check
            if (sgmEndors == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Zeyl detayı bulunamadı";
                return response;
            }
            #endregion
            #region FillFields
            input.brutPrim = decimal.Parse(sgmEndors.BrutPrim);
            input.kimlikNo = sgmEndors.KimlikNo;
            input.kimlikTipKod = sgmEndors.KimlikTipKod;
            input.netPrim = decimal.Parse(sgmEndors.NetPrim);
            input.otorizasyonKod = sgmEndors.OtorizasyonKod;
            input.policeBrutPrimi = decimal.Parse(sgmEndors.PoliceBrutPrimi);
            input.policeNetPrimi = decimal.Parse(sgmEndors.PoliceNetPrimi);
            input.policeNo = sgmEndors.PoliceNo;
            input.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
            input.sigortaSirketKod = sgmEndors.SigortaSirketKod;
            input.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
            input.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
            input.zeylBrutPrimi = decimal.Parse(sgmEndors.ZeylBrutPrimi);
            input.zeylNetPrimi = decimal.Parse(sgmEndors.ZeylNetPrimi);
            input.zeylNo = int.Parse(sgmEndors.ZeylNo);
            input.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);
            #endregion
            if (ServiceType == SagmerServiceType.KONTROL)
                return serviceClient.seyahatSigortaliCikisZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
            else
                return serviceClient.seyahatSigortaliCikisZeyli(input, long.Parse(sgmEndors.CompanyId));
        }
        public ServiceResponse<zeylSonucType> sigortaliTahakkukZeyli(long PolicyId, long EndorsementId, long InsuredId, string ApiCode, SagmerServiceType ServiceType)
        {
            serviceClient = new ServiceClient();
            ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            #region ApiCode Kontrol
            if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            #endregion
            #region IsSagmerSendViaPolicy
            SagmerHelper sagmerHelper = new SagmerHelper();
            if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            {
                response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
                response.Data.islemBasarili = false;
                return response;
            }
            #endregion
            sigortaliPrimFarkiInputType input = new sigortaliPrimFarkiInputType();

            #region FillEndorsDetails
            V_SgmEndorsInsuredPremium sgmEndors = new GenericRepository<V_SgmEndorsInsuredPremium>().FindBy($" INSURED_ID = '{InsuredId}' and ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            #endregion
            #region Endors Check
            if (sgmEndors == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Zeyl detayı bulunamadı";
                return response;
            }
            #endregion
            #region FillFields
            input.brutPrim = decimal.Parse(sgmEndors.BrutPrim);
            input.kimlikNo = sgmEndors.KimlikNo;
            input.kimlikTipKod = sgmEndors.KimlikTipi;
            input.netPrim = decimal.Parse(sgmEndors.NetPrim);
            input.otorizasyonKod = sgmEndors.OtorizasyonKod;
            input.policeBrutPrimi = decimal.Parse(sgmEndors.PoliceBrutPrimi);
            input.policeNetPrimi = decimal.Parse(sgmEndors.PoliceNetPrimi);
            input.policeNo = sgmEndors.PoliceNo;
            input.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
            input.sigortaSirketKod = sgmEndors.SigortaSirketKod;
            input.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
            input.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
            input.zeylBrutPrimi = decimal.Parse(sgmEndors.ZeylBrutPrimi);
            input.zeylNetPrimi = decimal.Parse(sgmEndors.ZeylNetPrimi);
            input.zeylNo = int.Parse(sgmEndors.ZeylNo);
            input.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);
            #endregion

            if (ServiceType == SagmerServiceType.KONTROL)
                return serviceClient.sigortaliTahakkukZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
            else
                return serviceClient.sigortaliTahakkukZeyli(input, long.Parse(sgmEndors.CompanyId));
        }
        public ServiceResponse<zeylSonucType> policeTahakkukVePrimFarkiZeyli(long PolicyId, long EndorsementId, string ApiCode, SagmerServiceType ServiceType)
        {
            serviceClient = new ServiceClient();
            ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            #region ApiCode Kontrol
            if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            #endregion
            #region IsSagmerSendViaPolicy
            SagmerHelper sagmerHelper = new SagmerHelper();
            if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            {
                response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
                response.Data.islemBasarili = false;
                return response;
            }
            #endregion
            primFarkiInputType input = new primFarkiInputType();

            #region FillEndorsDetails
            V_SgmEndorsPolicyPremium sgmPremium = new GenericRepository<V_SgmEndorsPolicyPremium>().FindBy($" ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            #endregion
            #region Endors Check
            if (sgmPremium == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Zeyl detayı bulunamadı";
                return response;
            }
            #endregion
            #region FillFields
            input.otorizasyonKod = sgmPremium.OtorizasyonKod;
            input.policeBrutPrimi = decimal.Parse(sgmPremium.PoliceBrutPrimi);
            input.policeNetPrimi = decimal.Parse(sgmPremium.PoliceNetPrimi);
            input.policeNo = sgmPremium.PoliceNo;
            input.sbmSagmerNo = long.Parse(sgmPremium.SbmSagmerNo);
            input.sigortaSirketKod = sgmPremium.SigortaSirketKod;
            input.yenilemeNo = int.Parse(sgmPremium.YenilemeNo);
            input.zeylBaslangicTarihi = DateTime.Parse(sgmPremium.ZeylBaslangicTarihi);
            input.zeylBrutPrimi = decimal.Parse(sgmPremium.ZeylBrutPrimi);
            input.zeylNetPrimi = decimal.Parse(sgmPremium.ZeylNetPrimi);
            input.zeylNo = int.Parse(sgmPremium.ZeylNo);
            input.zeylTanzimTarihi = DateTime.Parse(sgmPremium.ZeylTanzimTarihi);
            input.zeylTipi = int.Parse(sgmPremium.ZeylTipi);

            #endregion

            if (ServiceType == SagmerServiceType.KONTROL)
                return serviceClient.policeTahakkukVePrimFarkiZeyliKontrol(input, long.Parse(sgmPremium.CompanyId));
            else
                return serviceClient.policeTahakkukVePrimFarkiZeyli(input, long.Parse(sgmPremium.CompanyId));
        }
        public ServiceResponse<zeylSonucType> sigortaEttirenDegisiklikZeyli(long PolicyId, long EndorsementId, string ApiCode, SagmerServiceType ServiceType)
        {
            serviceClient = new ServiceClient();
            ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            #region ApiCode Kontrol
            if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            #endregion
            #region IsSagmerSendViaPolicy
            SagmerHelper sagmerHelper = new SagmerHelper();
            if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            {
                response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
                response.Data.islemBasarili = false;
                return response;
            }
            #endregion
            sigortaEttirenZeylInputType input = new sigortaEttirenZeylInputType();
            #region FillEndorsDetails
            V_SgmEndorsPolicyInsurer sgmInsurer = new GenericRepository<V_SgmEndorsPolicyInsurer>().FindBy($" ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            #endregion
            #region Endors Check
            if (sgmInsurer == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Zeyl detayı bulunamadı";
                return response;
            }
            #endregion
            #region FillFields
            input.ad = sgmInsurer.Ad;
            input.adres = sgmInsurer.Adres;
            input.babaAdi = sgmInsurer.BabaAdi;
            input.cinsiyet = ((Enums.cinsiyet)int.Parse(sgmInsurer.Cinsiyet));
            input.dogumTarihi = DateTime.Parse(sgmInsurer.DogumTarihi);
            input.dogumYeri = sgmInsurer.DogumYeri;
            input.kimlikNo = sgmInsurer.KimlikNo;
            input.kimlikTipKod = sgmInsurer.KimlikTipKod;
            input.kurulusTarihi = DateTime.Parse(sgmInsurer.KurulusTarihi);
            input.kurulusYeri = sgmInsurer.KurulusYeri;
            input.otorizasyonKod = sgmInsurer.OtorizasyonKod;
            input.policeBrutPrimi = decimal.Parse(sgmInsurer.PoliceBrutPrimi);
            input.policeNetPrimi = decimal.Parse(sgmInsurer.PoliceNetPrimi);
            input.policeNo = sgmInsurer.PoliceNo;
            input.sbmSagmerNo = long.Parse(sgmInsurer.SbmSagmerNo);
            input.sigortaSirketKod = sgmInsurer.SigortaSirketKod;
            input.turKod = ((Enums.turKod)int.Parse(sgmInsurer.TurKod));
            input.ulkeKodu = int.Parse(sgmInsurer.UlkeKodu);
            input.yenilemeNo = int.Parse(sgmInsurer.YenilemeNo);
            input.zeylBaslangicTarihi = DateTime.Parse(sgmInsurer.ZeylBaslangicTarihi);
            input.zeylBrutPrimi = decimal.Parse(sgmInsurer.ZeylBrutPrimi);
            input.zeylNetPrimi = decimal.Parse(sgmInsurer.ZeylNetPrimi);
            input.zeylNo = int.Parse(sgmInsurer.ZeylNo);
            input.zeylTanzimTarihi = DateTime.Parse(sgmInsurer.ZeylTanzimTarihi);
            #endregion
            if (ServiceType == SagmerServiceType.KONTROL)
                return serviceClient.sigortaEttirenDegisiklikZeyliKontrol(input, long.Parse(sgmInsurer.CompanyId));
            else
                return serviceClient.sigortaEttirenDegisiklikZeyli(input, long.Parse(sgmInsurer.CompanyId));
        }
        public ServiceResponse<zeylSonucType> uretimKaynakDegisiklikZeyliKontrol(long PolicyId, long EndorsementId, string ApiCode, SagmerServiceType ServiceType)
        {
            serviceClient = new ServiceClient();
            ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            #region ApiCode Kontrol
            if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            #endregion
            #region IsSagmerSendViaPolicy
            SagmerHelper sagmerHelper = new SagmerHelper();
            if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            {
                response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
                response.Data.islemBasarili = false;
                return response;
            }
            #endregion
            uretimKaynakInputType input = new uretimKaynakInputType();
            #region FillEndorsDetails
            V_SgmEndorsPolicyPremium sgmEndors = new GenericRepository<V_SgmEndorsPolicyPremium>().FindBy($" ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            #endregion
            #region Endors Check
            if (sgmEndors == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Zeyl detayı bulunamadı";
                return response;
            }
            #endregion
            input.otorizasyonKod = sgmEndors.OtorizasyonKod;
            input.policeBrutPrimi = decimal.Parse(sgmEndors.PoliceBrutPrimi);
            input.policeNetPrimi = decimal.Parse(sgmEndors.PoliceNetPrimi);
            input.policeNo = sgmEndors.PoliceNo;
            input.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
            input.sigortaSirketKod = sgmEndors.SigortaSirketKod;
            input.uretimKaynakKod = long.Parse(sgmEndors.UretimKaynakKod);
            input.uretimKaynakKurumKod = sgmEndors.UretimKaynakKod;
            input.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
            input.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
            input.zeylBrutPrimi = decimal.Parse(sgmEndors.ZeylBrutPrimi);
            input.zeylNetPrimi = decimal.Parse(sgmEndors.ZeylNetPrimi);
            input.zeylNo = int.Parse(sgmEndors.ZeylNo);
            input.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);


            if (ServiceType == SagmerServiceType.KONTROL)
                return serviceClient.uretimKaynakDegisiklikZeyliKontrol(input,long.Parse(sgmEndors.CompanyId));
            else
                return serviceClient.uretimKaynakDegisiklikZeyli(input, long.Parse(sgmEndors.CompanyId));
        }
        public ServiceResponse<zeylSonucType> genelIptalZeyli(long PolicyId, long EndorsementId, string ApiCode, SagmerServiceType ServiceType)
        {
            serviceClient = new ServiceClient();
            ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            #region ApiCode Kontrol
            if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            #endregion
            #region IsSagmerSendViaPolicy
            SagmerHelper sagmerHelper = new SagmerHelper();
            if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            {
                response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
                response.Data.islemBasarili = false;
                return response;
            }
            #endregion
            genelIptalInputType input = new genelIptalInputType();
            #region FillEndorsDetails
            V_SgmEndorsPolicyPremium sgmEndors = new GenericRepository<V_SgmEndorsPolicyPremium>().FindBy($" ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            #endregion
            #region Endors Check
            if (sgmEndors == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Police detayı bulunamadı";
                return response;
            }
            #endregion
            input.iptalNedenKod = long.Parse(sgmEndors.IptalNedenKod);
            input.iptalZeylTuru = long.Parse(sgmEndors.IptalZeylTuru);
            input.otorizasyonKod = sgmEndors.OtorizasyonKod;
            input.policeBrutPrimi = decimal.Parse(sgmEndors.PoliceBrutPrimi);
            input.policeNetPrimi = decimal.Parse(sgmEndors.PoliceNetPrimi);
            input.policeNo = sgmEndors.PoliceNo;
            input.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
            input.sigortaSirketKod = sgmEndors.SigortaSirketKod;
            input.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
            input.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
            input.zeylBrutPrimi = decimal.Parse(sgmEndors.ZeylBrutPrimi);
            input.zeylNetPrimi = decimal.Parse(sgmEndors.ZeylNetPrimi);
            input.zeylNo = int.Parse(sgmEndors.ZeylNo);
            input.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);

            if (ServiceType == SagmerServiceType.KONTROL)
                return serviceClient.genelIptalZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
            else
                return serviceClient.genelIptalZeyli(input, long.Parse(sgmEndors.CompanyId));
        }
        public ServiceResponse<zeylSonucType> meriyeteDonusZeyli(long PolicyId, long EndorsementId, string ApiCode, SagmerServiceType ServiceType)
        {
            serviceClient = new ServiceClient();
            ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            #region ApiCode Kontrol
            if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            #endregion
            #region IsSagmerSendViaPolicy
            SagmerHelper sagmerHelper = new SagmerHelper();
            if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            {
                response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
                response.Data.islemBasarili = false;
                return response;
            }
            #endregion
            policeYururlugeAlmaInputType input = new policeYururlugeAlmaInputType();
            #region FillEndorsDetails
            V_SgmEndorsPolicyPremium sgmEndors = new GenericRepository<V_SgmEndorsPolicyPremium>().FindBy($" ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            #endregion
            #region Endors Check
            if (sgmEndors == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Police detayı bulunamadı";
                return response;
            }
            #endregion

            input.otorizasyonKod = sgmEndors.OtorizasyonKod;
            input.policeBrutPrimi = decimal.Parse(sgmEndors.PoliceBrutPrimi);
            input.policeNetPrimi = decimal.Parse(sgmEndors.PoliceNetPrimi);
            input.policeNo = sgmEndors.PoliceNo;
            input.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
            input.sigortaSirketKod = sgmEndors.SigortaSirketKod;
            input.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
            input.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
            input.zeylBrutPrimi = decimal.Parse(sgmEndors.ZeylBrutPrimi);
            input.zeylNetPrimi = decimal.Parse(sgmEndors.ZeylNetPrimi);
            input.zeylNo = int.Parse(sgmEndors.ZeylNo);
            input.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);
            if (ServiceType == SagmerServiceType.KONTROL)
                return serviceClient.meriyeteDonusZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
            else
                return serviceClient.meriyeteDonusZeyli(input, long.Parse(sgmEndors.CompanyId));
        }
        public ServiceResponse<sonucType> policeSilme(long PolicyId, long EndorsementId, string ApiCode, SagmerServiceType ServiceType)
        {
            serviceClient = new ServiceClient();
            ServiceResponse<sonucType> response = new ServiceResponse<sonucType>();
            #region ApiCode Kontrol
            if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            #endregion
            #region IsSagmerSendViaPolicy
            SagmerHelper sagmerHelper = new SagmerHelper();
            if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            {
                response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
                response.Data.islemBasarili = false;
                return response;
            }
            #endregion
            policeSilmeInputType input = new policeSilmeInputType();
            #region FillEndorsDetails
            V_SgmPolicyDelete sgmPolDel = new GenericRepository<V_SgmPolicyDelete>().FindBy($" ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            #endregion
            #region Endors Check
            if (sgmPolDel == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Police detayı bulunamadı";
                return response;
            }
            #endregion
            input.otorizasyonKod = sgmPolDel.OtorizasyonKod;
            input.policeNo = sgmPolDel.PoliceNo;
            input.sbmSagmerNo = long.Parse(sgmPolDel.SbmSagmerNo);
            input.yenilemeNo = int.Parse(sgmPolDel.YenilemeNo);
            if (ServiceType == SagmerServiceType.KONTROL)
                return serviceClient.policeSilmeKontrol(input, long.Parse(sgmPolDel.CompanyId));
            else
                return serviceClient.policeSilme(input, long.Parse(sgmPolDel.CompanyId));
        }
        public ServiceResponse<zeylSonucType> teminatPlanDegisiklikZeyli(long PolicyId, long EndorsementId, long InsuredId, string ApiCode, SagmerServiceType ServiceType)
        {
            serviceClient = new ServiceClient();
            ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            #region ApiCode Kontrol
            if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            #endregion
            #region IsSagmerSendViaPolicy
            SagmerHelper sagmerHelper = new SagmerHelper();
            if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            {
                response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
                response.Data.islemBasarili = false;
                return response;
            }
            #endregion
            #region FillEndorsDetails
            V_SgmEndorsInsuredPremium sgmEndors = new GenericRepository<V_SgmEndorsInsuredPremium>().FindBy($" INSURED_ID = '{InsuredId}' and ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            List<V_SgmIngressInsCover> sgmCover = new GenericRepository<V_SgmIngressInsCover>().FindBy($"INSURED_ID '{InsuredId}' and POLICY_ID = '{PolicyId}' and ENDORSEMENT_ID = '{EndorsementId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            #endregion
            List<sigortaliTeminatInputTypeList> sigTem = new List<sigortaliTeminatInputTypeList>();

            foreach (V_SgmIngressInsCover itemInsCover in sgmCover)
            {
                sigTem.Add(new sigortaliTeminatInputTypeList
                {
                    aso = (Enums.evetHayirType)int.Parse(itemInsCover.Aso),
                    cografiKapsamKodu = long.Parse(itemInsCover.CografiKapsamKodu),
                    donemBaslangicTarihi = DateTime.Parse(itemInsCover.DonemBaslangicTarihi),
                    donemBitisTarihi = DateTime.Parse(itemInsCover.DonemBitisTarihi),
                    donemKodu = int.Parse(itemInsCover.DonemKodu),
                    dovizCinsi = itemInsCover.DovizCinsi,
                    teminatAdedi = int.Parse(itemInsCover.TeminatAdedi),
                    teminatKodu = long.Parse(itemInsCover.TeminatKodu),
                    teminatLimiti = decimal.Parse(itemInsCover.TeminatLimiti),
                    teminatLimitKodu = long.Parse(itemInsCover.TeminatLimitKodu),
                    teminatMuafiyeti = itemInsCover.TeminatMuafiyeti,
                    teminatMuafiyetKodu = itemInsCover.TeminatMuafiyetKodu,
                    teminatNetPrim = decimal.Parse(itemInsCover.TeminatNetPrim),
                    teminatOdemeYuzdesi = decimal.Parse(itemInsCover.TeminatOdemeYuzdesi),
                    teminatVaryasyonKodu = int.Parse(itemInsCover.TeminatVaryasyonKodu),
                    toplamDonemSayisi = int.Parse(itemInsCover.ToplamDonemSayisi)
                });
            }
            #region FillFields
            teminatInputType input = new teminatInputType();
            input.kimlikNo = sgmEndors.KimlikNo;
            input.kimlikTipKod = sgmEndors.KimlikTipi;
            input.otorizasyonKod = sgmEndors.OtorizasyonKod;
            input.policeBrutPrimi = decimal.Parse(sgmEndors.PoliceBrutPrimi);
            input.policeNetPrimi = decimal.Parse(sgmEndors.PoliceNetPrimi);
            input.policeNo = sgmEndors.PoliceNo;
            input.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
            input.sigortaSirketKod = sgmEndors.SigortaSirketKod;
            input.urunPlanKod = long.Parse(sgmEndors.UrunPlanKod);
            input.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
            input.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
            input.zeylBrutPrimi = decimal.Parse(sgmEndors.ZeylBrutPrimi);
            input.zeylNetPrimi = decimal.Parse(sgmEndors.ZeylNetPrimi);
            input.zeylNo = int.Parse(sgmEndors.ZeylNo);
            input.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);
            input.sigortaliTeminatInputTypeList = sigTem.ToArray();
            #endregion
            if (ServiceType == SagmerServiceType.KONTROL)
                return serviceClient.teminatPlanDegisiklikZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
            else
                return serviceClient.teminatPlanDegisiklikZeyli(input, long.Parse(sgmEndors.CompanyId));
        }
        public ServiceResponse<zeylSonucType> sigortaliBilgiDegisiklikZeyli(long PolicyId, long EndorsementId, string ApiCode, SagmerServiceType ServiceType)
        {
            serviceClient = new ServiceClient();
            ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            #region ApiCode Kontrol
            if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            #endregion
            #region IsSagmerSendViaPolicy
            SagmerHelper sagmerHelper = new SagmerHelper();
            if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            {
                response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
                response.Data.islemBasarili = false;
                return response;
            }
            #endregion
            #region FillEndorsDetails
            V_SgmIngressEndorsement sgmEndors = new GenericRepository<V_SgmIngressEndorsement>().FindBy($" ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            #endregion
            sigortaliBilgiDegisiklikInputType input = new sigortaliBilgiDegisiklikInputType();
            List<sigortaliBilgileriInputType> insuredInput = new List<sigortaliBilgileriInputType>();
            List<V_SgmIngressEndorsInsured> sgmInsured = new GenericRepository<V_SgmIngressEndorsInsured>().FindBy($"ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            foreach (V_SgmIngressEndorsInsured itemInsured in sgmInsured)
            {
                V_SgmIngressInsHealthInfo healthInfo = new GenericRepository<V_SgmIngressInsHealthInfo>().FindBy($"INSURED_ID '{itemInsured.InsuredId}' and POLICY_ID = '{PolicyId}' and ENDORSEMENT_ID = '{EndorsementId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                List<V_SgmIngressExtraPremium> ExtraPrm = new GenericRepository<V_SgmIngressExtraPremium>().FindBy($"INSURED_ID '{itemInsured.InsuredId}' and POLICY_ID = '{PolicyId}' and ENDORSEMENT_ID = '{EndorsementId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                List<V_SgmIngressInsExemption> sgmExemption = new GenericRepository<V_SgmIngressInsExemption>().FindBy($"INSURED_ID '{itemInsured.InsuredId}' and POLICY_ID = '{PolicyId}' and ENDORSEMENT_ID = '{EndorsementId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

                sigortaliSaglikBilgileriInputType sigSagBilg = new sigortaliSaglikBilgileriInputType();
                List<sigortaliIndirimEkprimInputType> sigIndEkp = new List<sigortaliIndirimEkprimInputType>();
                List<sigortaliUygulamaInputType> sigUyg = new List<sigortaliUygulamaInputType>();

                if (ExtraPrm.Count > 0)
                {
                    foreach (V_SgmIngressExtraPremium itemExtraPrm in ExtraPrm)
                    {
                        sigIndEkp.Add(new sigortaliIndirimEkprimInputType
                        {
                            aciklama = itemExtraPrm.Aciklama,
                            indirimEkprimDegeri = decimal.Parse(itemExtraPrm.IndirimEkPrimDegeri),
                            indirimEkprimKod = long.Parse(itemExtraPrm.IndirimEkPrimKod),
                            kayitNo = long.Parse(itemExtraPrm.KayitNo),
                            tutarMiOranMi = itemExtraPrm.TutarMiOranMi
                        });
                    }
                }
                if (healthInfo != null)
                {
                    sigSagBilg.alkolKullaniyorMu = (Enums.evetHayirType)int.Parse(healthInfo.AlkolKullaniliyorMu);
                    sigSagBilg.alkolKullanmaSuresi = healthInfo.AlkolKullanmaSuresi;
                    sigSagBilg.alkolPeriyotTip = (Enums.aliskanlikPeriyodType)int.Parse(healthInfo.AlkolPeriyotTip);
                    sigSagBilg.boy = int.Parse(healthInfo.Boy);
                    sigSagBilg.kilo = int.Parse(healthInfo.Kilo);
                    sigSagBilg.periyottakiAlkolMiktari = healthInfo.PeriyottakiAlkolMiktari;
                    sigSagBilg.periyottakiSigaraAdedi = healthInfo.PeriyottakiSigaraAdedi;
                    sigSagBilg.sigaraIciyorMu = (Enums.evetHayirType)int.Parse(healthInfo.SigaraIciyorMu);
                    sigSagBilg.sigaraIcmeSuresi = healthInfo.SigaraIcmeSuresi;
                    sigSagBilg.sigaraPeriyotTip = (Enums.aliskanlikPeriyodType)int.Parse(healthInfo.SigaraperiyotTip);

                    List<sigortaliSaglikBilgileriDetayInputType> sigSagBilgDetay = new List<sigortaliSaglikBilgileriDetayInputType>();

                    List<V_SgmIngressInsHealthDet> healthInfoDet = new GenericRepository<V_SgmIngressInsHealthDet>().FindBy($"INSURED_ID '{itemInsured.InsuredId}' and POLICY_ID = '{PolicyId}' and ENDORSEMENT_ID = '{EndorsementId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                    foreach (V_SgmIngressInsHealthDet itemHealthDet in healthInfoDet)
                    {
                        sigSagBilgDetay.Add(new sigortaliSaglikBilgileriDetayInputType
                        {
                            ameliyatVarmi = itemHealthDet.AmeliyatVarMi,
                            hastalikAciklamasi = itemHealthDet.HastalikAciklamasi,
                            hastalikAdi = itemHealthDet.HastalikAdi,
                            hastalikKod = itemHealthDet.HastalikKod,
                            hastalikUygulamaKod = long.Parse(itemHealthDet.HastalikUygulamaKod),
                            sonDurum = itemHealthDet.SonDurum,
                            sureAy = int.Parse(itemHealthDet.SureAy),
                            sureYil = int.Parse(itemHealthDet.SureYil),
                            uygulamaAciklamasi = itemHealthDet.UygulamaAciklamasi
                        });
                    }
                    sigSagBilg.saglikDetayBilgileri = sigSagBilgDetay.ToArray();

                }
                if (sigUyg.Count > 0)
                {
                    foreach (V_SgmIngressInsExemption itemExemption in sgmExemption)
                    {
                        sigUyg.Add(new sigortaliUygulamaInputType
                        {
                            aciklama = itemExemption.Aciklama,
                            hastalikKod = itemExemption.HastalikKod,
                            kayitNo = long.Parse(itemExemption.KayitNo),
                            muafiyetTutarLimiti = decimal.Parse(itemExemption.MuafiyetTutarLimiti),
                            sigortaliUygulamaKod = long.Parse(itemExemption.SigortaliUygulamaKod)
                        });
                    }
                }

                insuredInput.Add(new sigortaliBilgileriInputType
                {
                    brutPrim = decimal.Parse(itemInsured.BrutPrim),
                    eskiPoliceNo = itemInsured.EskiPoliceNo,
                    eskiYenilemeNo = int.Parse(itemInsured.EskiYenilemeNo),
                    ilkOzelSigortalilikTarihi = DateTime.Parse(itemInsured.IlkOzelSigortalilikTarihi),
                    ilkOzelSirketKod = itemInsured.IlkOzelSirketKod,
                    kimlikNo = itemInsured.KimlikNo,
                    kimlikTipKod = itemInsured.KimlikTipKod,
                    meslekKod = long.Parse(itemInsured.MeslekKod),
                    netPrim = decimal.Parse(itemInsured.NetPrim),
                    sigortaliUyruk = (Enums.uyrukTuru)int.Parse(itemInsured.SigortaliUyruk),
                    yasadigiIlceKod = itemInsured.YasadigiIlceKod,
                    yasadigiIlKod = itemInsured.YasadigiIlKod,
                    saglikBilgileriInputType = sigSagBilg,
                    sigortaliIndirimEkprimInputTypeList = sigIndEkp.ToArray(),
                    sigortaliUygulamaList = sigUyg.ToArray()
                });
            }
            #region FillFields
            input.otorizasyonKod = sgmEndors.OtorizasyonKod;
            input.policeBrutPrimi = decimal.Parse(sgmEndors.PoliceBrutPrimi);
            input.policeNetPrimi = decimal.Parse(sgmEndors.PoliceNetPrimi);
            input.policeNo = sgmEndors.PoliceNo;
            input.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
            input.sigortaSirketKod = sgmEndors.SigortaSirketKod;
            input.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
            input.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
            input.zeylBrutPrimi = decimal.Parse(sgmEndors.ZeylBrutPrimi);
            input.zeylNetPrimi = decimal.Parse(sgmEndors.ZeylNetPrimi);
            input.zeylNo = int.Parse(sgmEndors.ZeylNo);
            input.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);
            input.sigortali = insuredInput.ToArray();
            #endregion
            if (ServiceType == SagmerServiceType.KONTROL)
                return serviceClient.sigortaliBilgiDegisiklikZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
            else
                return serviceClient.sigortaliBilgiDegisiklikZeyli(input, long.Parse(sgmEndors.CompanyId));
        }
        public ServiceResponse<zeylSonucType> tazminatDosyaGiris(long ClaimId, string ApiCode, SagmerServiceType ServiceType)
        {
            serviceClient = new ServiceClient();
            ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            #region ApiCode Kontrol
            if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            #endregion
            #region IsSagmerSendViaPolicy
            SagmerHelper sagmerHelper = new SagmerHelper();
            if (!sagmerHelper.IsSagmerSendViaClaim(ClaimId))
            {
                response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
                response.Data.islemBasarili = false;
                return response;
            }
            #endregion
            dosyaInputType input = new dosyaInputType();
            #region FillClaim
            V_SgmClaimInsert claimIns = new GenericRepository<V_SgmClaimInsert>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            List<faturaType> bill = new List<faturaType>();
            List<V_SgmClaimBill> claimBill = new GenericRepository<V_SgmClaimBill>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            List<V_SgmClaimCover> claimCover = new GenericRepository<V_SgmClaimCover>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            List<V_SgmClaimIcd> claimIcd = new GenericRepository<V_SgmClaimIcd>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

            List<saglikKurumType> sagKur = new List<saglikKurumType>();
            List<teminatOdemeType> temOde = new List<teminatOdemeType>();
            List<icdType> icd = new List<icdType>();

            foreach (V_SgmClaimIcd _icd in claimIcd)
            {
                icd.Add(new icdType
                {
                    donemNo = int.Parse(_icd.DonemNo),
                    iCDNo = _icd.IcdNo,
                    iCDTip = int.Parse(_icd.IcdTip),
                    teminatNo = int.Parse(_icd.TeminatNo),
                    varyasyonNo = int.Parse(_icd.VaryasyonNo)
                });
            }
            foreach (V_SgmClaimCover clmCover in claimCover)
            {
                temOde.Add(new teminatOdemeType
                {
                    donemNo = int.Parse(clmCover.DonemNo),
                    muallakTutariTL = decimal.Parse(clmCover.MuallakTutariTL),
                    odemeSiraNo = long.Parse(clmCover.OdemeSiraNo),
                    odemeTipi = (Enums.odemeTipiType)int.Parse(clmCover.OdemeTipi),
                    odemeTutariTL = decimal.Parse(clmCover.OdemeTutariTL),
                    teminatNo = int.Parse(clmCover.TeminatNo),
                    varyasyonNo = int.Parse(clmCover.VaryasyonNo)
                });
            }
            sagKur.Add(new saglikKurumType
            {
                kurumIlcesi = claimIns.KurumIlcesi,
                kurumIli = claimIns.KurumIli,
                kurumUlke = claimIns.KurumUlke,
                saglikKurumNo = claimIns.SaglikKurumNo,
                saglikKurumNoTipi = claimIns.SaglikKurumNoTipi,
                teminatList = temOde.ToArray()
            });

            if (claimBill.Count > 0)
            {
                foreach (V_SgmClaimBill fat in claimBill)
                {
                    bill.Add(new faturaType
                    {
                        ettn = fat.Ettn,
                        faturaNo = fat.FaturaNo,
                        faturaTarihi = DateTime.Parse(fat.FaturaTarihi),
                        faturaTutariTL = decimal.Parse(fat.FaturaTutariTl),
                        faturaTutariTLOdenen = decimal.Parse(fat.FaturaTutariTlOdenen),
                        paraBirimi = fat.ParaBirimi
                    });
                }
            }
            #endregion
            #region  FillFields
            input.dosyaRedNedeni = long.Parse(claimIns.DosyaRedNedeni);
            input.ihbarTarihi = DateTime.Parse(claimIns.IhbarTarihi);
            input.kimlikNo = claimIns.KimlikNo;
            input.kimlikTipKod = claimIns.KimlikTipKodu;
            input.muallakOdemeTarihi = DateTime.Parse(claimIns.MuallakOdemeTarihi);
            input.muallakTutariTL = decimal.Parse(claimIns.MuallakTutariTL);
            input.odemeTutariTL = decimal.Parse(claimIns.OdemeTutarTL);
            input.odemeYeri = (Enums.odemeYeriType)int.Parse(claimIns.OdemeYeri);
            input.olayTarihi = DateTime.Parse(claimIns.OlayTarihi);
            input.otorizasyonKod = claimIns.OtorizasyonKodu;
            input.paraBirimi = claimIns.ParaBirimi;
            input.provizyonTarihi = DateTime.Parse(claimIns.ProvizyonTarihi);
            input.sbmSagmerNo = long.Parse(claimIns.SbmSagmerNo);
            input.tazminatDosyaDurumu = long.Parse(claimIns.TazminatDosyaDurumu);
            input.tazminatDosyaNo = claimIns.TazminatDosyaNo;
            input.tazminatSiraNo = long.Parse(claimIns.TazminatSiraNo);
            input.tedaviTipi = (Enums.tedaviTipiType)int.Parse(claimIns.TedaviTipi);
            input.faturaList = bill.ToArray();
            input.saglikKurumList = sagKur.ToArray();
            input.icdList = icd.ToArray();
            #endregion
            if (ServiceType == SagmerServiceType.KONTROL)
                return serviceClient.tazminatDosyaGirisKontrol(input, long.Parse(claimIns.CompanyId));
            else
                return serviceClient.tazminatDosyaGiris(input, long.Parse(claimIns.CompanyId));
        }
        public ServiceResponse<zeylSonucType> tazminatDosyaGuncelleme(long ClaimId, string ApiCode, SagmerServiceType ServiceType)
        {
            serviceClient = new ServiceClient();
            ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            #region ApiCode Kontrol
            if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            #endregion
            #region IsSagmerSendViaPolicy
            SagmerHelper sagmerHelper = new SagmerHelper();
            if (!sagmerHelper.IsSagmerSendViaClaim(ClaimId))
            {
                response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
                response.Data.islemBasarili = false;
                return response;
            }
            #endregion
            dosyaUpdateType input = new dosyaUpdateType();
            #region FillClaim
            V_SgmClaimInsert claimIns = new GenericRepository<V_SgmClaimInsert>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            List<faturaType> bill = new List<faturaType>();
            List<V_SgmClaimBill> claimBill = new GenericRepository<V_SgmClaimBill>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            List<V_SgmClaimCover> claimCover = new GenericRepository<V_SgmClaimCover>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            List<V_SgmClaimIcd> claimIcd = new GenericRepository<V_SgmClaimIcd>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

            List<saglikKurumUpdateType> sagKur = new List<saglikKurumUpdateType>();
            List<teminatOdemeType> temOde = new List<teminatOdemeType>();
            List<icdInputType> icd = new List<icdInputType>();

            foreach (V_SgmClaimIcd _icd in claimIcd)
            {
                icd.Add(new icdInputType
                {
                    islemTipi = (Enums.islemTipiType)int.Parse(_icd.IslemTipi),
                    donemNo = int.Parse(_icd.DonemNo),
                    iCDNo = _icd.IcdNo,
                    iCDTip = int.Parse(_icd.IcdTip),
                    teminatNo = int.Parse(_icd.TeminatNo),
                    varyasyonNo = int.Parse(_icd.VaryasyonNo)
                });
            }
            foreach (V_SgmClaimCover clmCover in claimCover)
            {
                temOde.Add(new teminatOdemeType
                {
                    donemNo = int.Parse(clmCover.DonemNo),
                    muallakTutariTL = decimal.Parse(clmCover.MuallakTutariTL),
                    odemeSiraNo = long.Parse(clmCover.OdemeSiraNo),
                    odemeTipi = (Enums.odemeTipiType)int.Parse(clmCover.OdemeTipi),
                    odemeTutariTL = decimal.Parse(clmCover.OdemeTutariTL),
                    teminatNo = int.Parse(clmCover.TeminatNo),
                    varyasyonNo = int.Parse(clmCover.VaryasyonNo)
                });
            }
            sagKur.Add(new saglikKurumUpdateType
            {
                kurumIlcesi = claimIns.KurumIlcesi,
                kurumIli = claimIns.KurumIli,
                kurumUlke = claimIns.KurumUlke,
                saglikKurumNo = claimIns.SaglikKurumNo,
                saglikKurumNoTipi = claimIns.SaglikKurumNoTipi,
                kurumIlcesiYeni = claimIns.KurumIlcesiYeni,
                kurumIliYeni = claimIns.KurumIliYeni,
                kurumUlkeYeni = claimIns.KurumUlkeYeni,
                saglikKurumNoTipiYeni = claimIns.SaglikKurumNoTipiYeni,
                saglikKurumNoYeni = claimIns.SaglikKurumNoYeni
            });
            if (claimBill.Count > 0)
            {
                foreach (V_SgmClaimBill fat in claimBill)
                {
                    bill.Add(new faturaType
                    {
                        ettn = fat.Ettn,
                        faturaNo = fat.FaturaNo,
                        faturaTarihi = DateTime.Parse(fat.FaturaTarihi),
                        faturaTutariTL = decimal.Parse(fat.FaturaTutariTl),
                        faturaTutariTLOdenen = decimal.Parse(fat.FaturaTutariTlOdenen),
                        paraBirimi = fat.ParaBirimi
                    });
                }
            }
            #endregion
            #region  FillFields
            input.dosyaRedNedeni = long.Parse(claimIns.DosyaRedNedeni);
            input.ihbarTarihi = DateTime.Parse(claimIns.IhbarTarihi);
            input.dosyaGuncellemeTarihi = DateTime.Parse(claimIns.DosyaGuncellemeTarihi);
            input.icdList = icd.ToArray();
            input.saglikKurumList = sagKur.ToArray();
            input.odemeYeri = (Enums.odemeYeriType)int.Parse(claimIns.OdemeYeri);
            input.olayTarihi = DateTime.Parse(claimIns.OlayTarihi);
            input.otorizasyonKod = claimIns.OtorizasyonKodu;
            input.paraBirimi = claimIns.ParaBirimi;
            input.provizyonTarihi = DateTime.Parse(claimIns.ProvizyonTarihi);
            input.sbmSagmerNo = long.Parse(claimIns.SbmSagmerNo);
            input.tazminatDosyaDurumu = long.Parse(claimIns.TazminatDosyaDurumu);
            input.tazminatDosyaNo = claimIns.TazminatDosyaNo;
            input.tedaviTipi = (Enums.tedaviTipiType)int.Parse(claimIns.TedaviTipi);
            #endregion
            if (ServiceType == SagmerServiceType.KONTROL)
                return serviceClient.tazminatDosyaGuncellemeKontrol(input, long.Parse(claimIns.CompanyId));
            else
                return serviceClient.tazminatDosyaGuncelleme(input, long.Parse(claimIns.CompanyId));
        }
        public ServiceResponse<zeylSonucType> tazminatFaturaGiris(long ClaimId, string ApiCode, SagmerServiceType ServiceType)
        {
            serviceClient = new ServiceClient();
            ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            #region ApiCode Kontrol
            if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            #endregion
            #region IsSagmerSendViaPolicy
            SagmerHelper sagmerHelper = new SagmerHelper();
            if (!sagmerHelper.IsSagmerSendViaClaim(ClaimId))
            {
                response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
                response.Data.islemBasarili = false;
                return response;
            }
            #endregion
            V_SgmClaimInsert claimIns = new GenericRepository<V_SgmClaimInsert>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            List<V_SgmClaimBill> claimBill = new GenericRepository<V_SgmClaimBill>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            List<faturaType> bill = new List<faturaType>();
            if (claimBill.Count > 0)
            {
                foreach (V_SgmClaimBill fat in claimBill)
                {
                    bill.Add(new faturaType
                    {
                        ettn = fat.Ettn,
                        faturaNo = fat.FaturaNo,
                        faturaTarihi = DateTime.Parse(fat.FaturaTarihi),
                        faturaTutariTL = decimal.Parse(fat.FaturaTutariTl),
                        faturaTutariTLOdenen = decimal.Parse(fat.FaturaTutariTlOdenen),
                        paraBirimi = fat.ParaBirimi
                    });
                }
            }
            dosyaFaturaInputType input = new dosyaFaturaInputType();
            input.sbmSagmerNo = long.Parse(claimIns.SbmSagmerNo);
            input.tazminatDosyaNo = claimIns.TazminatDosyaNo;
            input.faturaList = bill.ToArray();
            if (ServiceType == SagmerServiceType.KONTROL)
                return serviceClient.tazminatFaturaGirisKontrol(input, long.Parse(claimIns.CompanyId));
            else
                return serviceClient.tazminatFaturaGiris(input, long.Parse(claimIns.CompanyId));
        }
        public ServiceResponse<zeylSonucType> tazminatOdemeKontrol(long ClaimId, string ApiCode, SagmerServiceType ServiceType)
        {
            serviceClient = new ServiceClient();
            ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            #region ApiCode Kontrol
            if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            {
                response.Data.islemBasarili = false;
                response.Data.aciklama = "Api Code hatası";
                return response;
            }
            #endregion
            #region IsSagmerSendViaPolicy
            SagmerHelper sagmerHelper = new SagmerHelper();
            if (!sagmerHelper.IsSagmerSendViaClaim(ClaimId))
            {
                response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
                response.Data.islemBasarili = false;
                return response;
            }
            #endregion
            V_SgmClaimInsert claimIns = new GenericRepository<V_SgmClaimInsert>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();

            dosyaTazminatOdemeInputType input = new dosyaTazminatOdemeInputType();
            List<saglikKurumType> sagKur = new List<saglikKurumType>();
            List<V_SgmClaimCover> claimCover = new GenericRepository<V_SgmClaimCover>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            List<teminatOdemeType> temOde = new List<teminatOdemeType>();

            foreach (V_SgmClaimCover clmCover in claimCover)
            {
                temOde.Add(new teminatOdemeType
                {
                    donemNo = int.Parse(clmCover.DonemNo),
                    muallakTutariTL = decimal.Parse(clmCover.MuallakTutariTL),
                    odemeSiraNo = long.Parse(clmCover.OdemeSiraNo),
                    odemeTipi = (Enums.odemeTipiType)int.Parse(clmCover.OdemeTipi),
                    odemeTutariTL = decimal.Parse(clmCover.OdemeTutariTL),
                    teminatNo = int.Parse(clmCover.TeminatNo),
                    varyasyonNo = int.Parse(clmCover.VaryasyonNo)
                });
            }
            sagKur.Add(new saglikKurumType
            {
                kurumIlcesi = claimIns.KurumIlcesi,
                kurumIli = claimIns.KurumIli,
                kurumUlke = claimIns.KurumUlke,
                saglikKurumNo = claimIns.SaglikKurumNo,
                saglikKurumNoTipi = claimIns.SaglikKurumNoTipi,
                teminatList = temOde.ToArray()
            });

            input.dosyaRedNedeni = long.Parse(claimIns.DosyaRedNedeni);
            input.muallakOdemeTarihi = DateTime.Parse(claimIns.MuallakOdemeTarihi);
            input.muallakTutariTL = decimal.Parse(claimIns.MuallakTutariTL);
            input.odemeTutariTL = decimal.Parse(claimIns.OdemeTutarTL);
            input.otorizasyonKod = claimIns.OtorizasyonKodu;
            input.sbmSagmerNo = long.Parse(claimIns.SbmSagmerNo);
            input.tazminatDosyaDurumu = long.Parse(claimIns.TazminatDosyaDurumu);
            input.tazminatDosyaNo = claimIns.TazminatDosyaNo;
            input.saglikKurumList = sagKur.ToArray();
            if (ServiceType == SagmerServiceType.KONTROL)
                return serviceClient.tazminatOdemeKontrol(input, long.Parse(claimIns.CompanyId));
            else
                return serviceClient.tazminatOdeme(input, long.Parse(claimIns.CompanyId));
        }
    }
}
