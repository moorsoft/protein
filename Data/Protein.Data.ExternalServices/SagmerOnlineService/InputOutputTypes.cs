﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using static Protein.Data.ExternalServices.SagmerOnlineService.Enums;

namespace Protein.Data.ExternalServices.SagmerOnlineService
{
    public class InputOutputTypes
    {

        public class ServiceResponse<T>
        {
            public SbmServiceException ServiceException { get; set; }
            public T Data { get; set; } = Activator.CreateInstance<T>();
        }


        #region Sbm Service Exception

        public class SbmServiceException
        {
            public string code { get; set; }

            public List<failure> failureList { get; set; }

            public string message { get; set; }

        }

        public class failure
        {
            public string code { get; set; }

            public string message { get; set; }
        }

        #endregion

        #region Input Types

        public class policeInputType
        {
            public string otorizasyonKod { get; set; }
            public string sigortaSirketKod { get; set; }
            public long branshKod { get; set; }
            public string dovizCinsi { get; set; }
            public string endirektSigortaSirketKod { get; set; }
            public string eskiPoliceNo { get; set; }
            public int? eskiYenilemeNo { get; set; }
            public string ilKod { get; set; }
            public long odemeKod { get; set; }
            public DateTime policeBaslamaTarihi { get; set; }
            public DateTime policeBitisTarihi { get; set; }
            public decimal policeBrutPrimi { get; set; }
            public decimal policeNetPrimi { get; set; }
            public string policeNo { get; set; }
            public DateTime? policeTanzimTarihi { get; set; }
            public policeTipiType policeTip { get; set; }
            public sigortaEttirenInputType sigortaEttirenType { get; set; }
            public long tarifeID { get; set; }
            public long uretimKaynakKod { get; set; }
            public string uretimKaynakKurumKod { get; set; }
            public int vadeSayisi { get; set; }
            public string yeniYenilemeGecis { get; set; } //yeniYenilemeGecisType enum value
            public int yenilemeNo { get; set; }
            public int zeylNo { get; set; }
            //public string otorizasyonKod { get; set; }
        }

        public class icdInputType : icdType
        {
            public islemTipiType islemTipi { get; set; }
        }

        public class faturaInputType : faturaType
        {
            public DateTime faturaGuncellemeTarihi { get; set; }
            public DateTime iptalTarihi { get; set; }
            public islemTipiType islemTipi { get; set; }
        }

        public class seyahatSigortaliTeminatInputType
        {
            public long cografiKapsamKodu { get; set; }
            public string dovizCinsi { get; set; }
            public int teminatAdedi { get; set; }
            public long teminatKodu { get; set; }
            public long teminatLimitKodu { get; set; }
            public decimal teminatLimiti { get; set; }
            public long teminatMuafiyetKodu { get; set; }
            public decimal teminatMuafiyeti { get; set; }
            public decimal teminatNetPrim { get; set; }
            public decimal teminatOdemeYuzdesi { get; set; }
            public int teminatVaryasyonKodu { get; set; }
        }

        public class seyahatBireyselPoliceSigortaliInputType
        {
            public string adres { get; set; }
            public string babaAdi { get; set; }
            public long bireyTipKod { get; set; }
            public decimal brutPrim { get; set; }
            public cinsiyet cinsiyet { get; set; }
            public DateTime dogumTarihi { get; set; }
            public string dogumYeri { get; set; }
            public string eskiPoliceNo { get; set; }
            public int eskiYenilemeNo { get; set; }
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public decimal netPrim { get; set; }
            public string sigortaliAd { get; set; }
            public sigortaliIndirimEkprimInputType[] sigortaliIndirimEkprimInputTypeList { get; set; }
            public string sigortaliSoyad { get; set; }
            public seyahatSigortaliTeminatInputType[] sigortaliTeminatInputTypeList { get; set; }
            public uyrukTuru sigortaliUyruk { get; set; }
            public int ulkeKodu { get; set; }
            public long urunPlanKod { get; set; }
            public string yasadigiIlKod { get; set; }
            public string yasadigiIlceKod { get; set; }
        }

        public class sigortaliIndirimEkprimInputType
        {
            public string aciklama { get; set; }
            public decimal indirimEkprimDegeri { get; set; }
            public long indirimEkprimKod { get; set; }
            public long kayitNo { get; set; }
            public string tutarMiOranMi { get; set; }
        }

        public class sigortaliUygulamaInputType
        {
            public string aciklama { get; set; }
            public string hastalikKod { get; set; }
            public long kayitNo { get; set; }
            public decimal muafiyetTutarLimiti { get; set; }
            public long sigortaliUygulamaKod { get; set; }
        }

        public class sigortaliTeminatInputTypeList
        {
            public evetHayirType aso { get; set; }
            public long cografiKapsamKodu { get; set; }
            public DateTime donemBaslangicTarihi { get; set; }
            public DateTime donemBitisTarihi { get; set; }
            public int donemKodu { get; set; }
            public string dovizCinsi { get; set; }
            public int teminatAdedi { get; set; }
            public long teminatKodu { get; set; }
            public long? teminatLimitKodu { get; set; }
            public decimal teminatLimiti { get; set; }
            public string teminatMuafiyetKodu { get; set; }
            public string teminatMuafiyeti { get; set; }
            public decimal teminatNetPrim { get; set; }
            public decimal teminatOdemeYuzdesi { get; set; }
            public int teminatVaryasyonKodu { get; set; }
            public int toplamDonemSayisi { get; set; }
        }

        public class teminatDegisiklikInputType : sigortaliTeminatInputTypeList
        {
            public assuranceOperationType islemTuru { get; set; }
        }

        public class sigortaliSaglikBilgileriDetayInputType
        {
            public string ameliyatVarmi { get; set; }
            public string hastalikAciklamasi { get; set; }
            public string hastalikAdi { get; set; }
            public string hastalikKod { get; set; }
            public long hastalikUygulamaKod { get; set; }
            public string sonDurum { get; set; }
            public int sureAy { get; set; }
            public int sureYil { get; set; }
            public string uygulamaAciklamasi { get; set; }
        }

        public class saglikBireyselPoliceSaglikBilgileriInputType
        {
            public evetHayirType alkolKullaniyorMu { get; set; }
            public string alkolKullanmaSuresi { get; set; }
            public aliskanlikPeriyodType alkolPeriyotTip { get; set; }
            public int boy { get; set; }
            public int kilo { get; set; }
            public string periyottakiAlkolMiktari { get; set; }
            public string periyottakiSigaraAdedi { get; set; }
            public sigortaliSaglikBilgileriDetayInputType[] saglikDetayBilgileri { get; set; }
            public evetHayirType sigaraIciyorMu { get; set; }
            public string sigaraIcmeSuresi { get; set; }
            public aliskanlikPeriyodType sigaraPeriyotTip { get; set; }
        }

        public class saglikBireyselPoliceSigortaliInputType
        {
            public string adres { get; set; }
            public string aileBagNo { get; set; }
            public string babaAdi { get; set; }
            public long bireyTipKod { get; set; }
            public decimal brutPrim { get; set; }
            public cinsiyet cinsiyet { get; set; }
            public DateTime dogumTarihi { get; set; }
            public string dogumYeri { get; set; }
            public string eskiPoliceNo { get; set; }
            public int eskiYenilemeNo { get; set; }
            public DateTime ilkOzelSigortalilikTarihi { get; set; }
            public string ilkOzelSirketKod { get; set; }
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public long meslekKod { get; set; }
            public decimal netPrim { get; set; }
            public saglikBireyselPoliceSaglikBilgileriInputType saglikBilgileriInputType { get; set; }
            public string sigortaliAd { get; set; }
            public sigortaliIndirimEkprimInputType[] sigortaliIndirimEkprimInputTypeList { get; set; }
            public string sigortaliSoyad { get; set; }
            public sigortaliTeminatInputTypeList[] sigortaliTeminatInputTypeList { get; set; }
            public sigortaliUygulamaInputType[] sigortaliUygulamaList { get; set; }
            public uyrukTuru sigortaliUyruk { get; set; }
            public int ulkeKodu { get; set; }
            public long urunPlanKod { get; set; }
            public string yasadigiIlKod { get; set; }
            public string yasadigiIlceKod { get; set; }
        }

        public class sigortaEttirenInputType
        {
            public string ad { get; set; }
            public string adres { get; set; }
            public string babaAdi { get; set; }
            public cinsiyet? cinsiyet { get; set; }
            public DateTime? dogumTarihi { get; set; }
            public string dogumYeri { get; set; }
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public DateTime? kurulusTarihi { get; set; }
            public string kurulusYeri { get; set; }
            public uyrukTuru? sigortaEttirenUyruk { get; set; }
            public string soyad { get; set; }
            public turKod turKod { get; set; }
            public int? ulkeKodu { get; set; }
        }

        public class sigortaliBasvuruRedInputType
        {
            public string otorizasyonKodField { get; set; }
            public string sigortaSirketKodField { get; set; }
            public string aciklama { get; set; }
            public string basvuruNo { get; set; }
            public string bireySiraNo { get; set; }
            public long bireyTipKod { get; set; }
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public long redNedenKod { get; set; }
            public int yenilemeNo { get; set; }
        }
       
        public class seyahatPoliceInputType
        {
            public string otorizasyonKodField { get; set; }
            public string sigortaSirketKodField { get; set; }
            public string dovizCinsi { get; set; }
            public string endirektSigortaSirketKod { get; set; }
            public string eskiPoliceNo { get; set; }
            public int eskiYenilemeNo { get; set; }
            public string ilKod { get; set; }
            public long odemeKod { get; set; }
            public DateTime policeBaslamaTarihi { get; set; }
            public DateTime policeBitisTarihi { get; set; }
            public decimal policeBrutPrimi { get; set; }
            public decimal policeNetPrimi { get; set; }
            public string policeNo { get; set; }
            public DateTime policeTanzimTarihi { get; set; }
            public policeTipiType policeTip { get; set; }
            public sigortaEttirenInputType sigortaEttirenType { get; set; }
            public long tarifeID { get; set; }
            public long uretimKaynakKod { get; set; }
            public string uretimKaynakKurumKod { get; set; }
            public int vadeSayisi { get; set; }
            public string yeniYenilemeGecis { get; set; } //yeniYenilemeGecisType enum value
            public int yenilemeNo { get; set; }
            public int zeylNo { get; set; }
        }

        public class policeYenilenmemeInputType
        {
            public string otorizasyonKodField { get; set; }
            public string sigortaSirketKodField { get; set; }
            public string aciklama { get; set; }
            public int kayitNo { get; set; }
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public long sbmSagmerNo { get; set; }
            public long yenilenmemeNedenKod { get; set; }
        }

        public class seyahatPoliceBireyselInputType
        {
            public string otorizasyonKodField { get; set; }
            public string sigortaSirketKodField { get; set; }
            public string dovizCinsi { get; set; }
            public string endirektSigortaSirketKod { get; set; }
            public string eskiPoliceNo { get; set; }
            public int eskiYenilemeNo { get; set; }
            public string ilKod { get; set; }
            public long odemeKod { get; set; }
            public DateTime policeBaslamaTarihi { get; set; }
            public DateTime policeBitisTarihi { get; set; }
            public decimal policeBrutPrimi { get; set; }
            public decimal policeNetPrimi { get; set; }
            public string policeNo { get; set; }
            public DateTime policeTanzimTarihi { get; set; }
            public sigortaEttirenInputType sigortaEttiren { get; set; }
            public seyahatBireyselPoliceSigortaliInputType[] sigortali { get; set; }
            public long tarifeId { get; set; }
            public long uretimKaynakKod { get; set; }
            public string uretimKaynakKurumKod { get; set; }
            public int vadeSayisi { get; set; }
            public string yeniYenilemeGecis { get; set; } //yeniYenilemeGecisType enum value
            public int yenilemeNo { get; set; }
            public int zeylNo { get; set; }
        }

        public class saglikPoliceBireyselInputType
        {
            public string otorizasyonKodField { get; set; }
            public string sigortaSirketKodField { get; set; }
            public long bransKod { get; set; }
            public string dovizCinsi { get; set; }
            public string endirektSigortaSirketKod { get; set; }
            public string eskiPoliceNo { get; set; }
            public int eskiYenilemeNo { get; set; }
            public string ilKod { get; set; }
            public long odemeKod { get; set; }
            public DateTime policeBaslamaTarihi { get; set; }
            public DateTime policeBitisTarihi { get; set; }
            public decimal policeBrutPrimi { get; set; }
            public decimal policeNetPrimi { get; set; }
            public string policeNo { get; set; }
            public DateTime policeTanzimTarihi { get; set; }
            public sigortaEttirenInputType sigortaEttiren { get; set; }
            public saglikBireyselPoliceSigortaliInputType[] sigortali { get; set; }
            public long tarifeId { get; set; }
            public long uretimKaynakKod { get; set; }
            public string uretimKaynakKurumKod { get; set; }
            public int vadeSayisi { get; set; }
            public string yeniYenilemeGecis { get; set; } //yeniYenilemeGecisType enum value
            public int yenilemeNo { get; set; }
            public int zeylNo { get; set; }
        }

        public class kimlikDuzeltmeInputType
        {
            public string otorizasyonKodField { get; set; }
            public string sigortaSirketKodField { get; set; }
            public string eskiKimlikNo { get; set; }
            public string eskiKimlikTipKod { get; set; } //identityType enum value
            public string kimlikNo { get; set; }
            public string kimlikSahibiTuru { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public string policeNo { get; set; }
            public long sbmSagmerNo { get; set; }
            public int yenilemeNo { get; set; }
        }

        public class sgmPoliceGunlukMutabakatInputType
        {
            public long siraNo { get; set; }
            public DateTime tarih { get; set; }
            public tarihType tarihTipi { get; set; }
        }

        public class policeSorguInputType
        {
            public string policeNo { get; set; }
            public string sigortaSirketKod { get; set; }
            public int yenilemeNo { get; set; }
        }

        public class policeTarihDegisiklikInputType : zeylPrimInputType
        {
            public DateTime policeBaslamaTarihi { get; set; }
            public DateTime policeBitisTarihi { get; set; }
        }

        public class zeylPrimInputType : zeylInputType
        {
            public decimal policeBrutPrimi { get; set; }
            public decimal policeNetPrimi { get; set; }
            public decimal zeylBrutPrimi { get; set; }
            public decimal zeylNetPrimi { get; set; }
        }

        public class zeylInputType
        {
            public string otorizasyonKod { get; set; }
            public string policeNo { get; set; }
            public long sbmSagmerNo { get; set; }
            public string sigortaSirketKod { get; set; }
            public int yenilemeNo { get; set; }
            public DateTime zeylBaslangicTarihi { get; set; }
            public int zeylNo { get; set; }
            public DateTime zeylTanzimTarihi { get; set; }
        }

        public class sigortaliGirisInputType : zeylInputType
        {
            public decimal? policeBrutPrimi { get; set; }
            public decimal? policeNetPrimi { get; set; }
            public sigortali[] sigortaliList { get; set; }
            public decimal? zeylBrutPrimi { get; set; }
            public decimal? zeylNetPrimi { get; set; }
            public long zeylTuru { get; set; }
        }

        //public class sigortaliTeminatliInputType : sigortaliInputType
        //{
        //    public sigortaliTeminatInputType[] sigortaliTeminatInputTypeList { get; set; }
        //}

        public class sigortaliInputType
        {
            public string adres { get; set; }
            public string aileBagNo { get; set; }
            public string babaAdi { get; set; }
            public string bireySiraNo { get; set; }
            public long bireyTipKod { get; set; }
            public decimal brutPrim { get; set; }
            public cinsiyet? cinsiyet { get; set; }
            public DateTime? dogumTarihi { get; set; }
            public string dogumYeri { get; set; }
            public string eskiPoliceNo { get; set; }
            public int eskiYenilemeNo { get; set; }
            public DateTime ilkOzelSigortalilikTarihi { get; set; }
            public string ilkOzelSirketKod { get; set; }
            public string kimlikNo { get; set; }
            /// <summary>
            /// identityType enum value
            /// </summary>
            public string kimlikTipKod { get; set; } //identityType enum value
            public long? meslekKod { get; set; }
            public decimal netPrim { get; set; }
            public sigortaliSaglikBilgileriInputType saglikBilgileriInputType { get; set; }
            public string sigortaliAd { get; set; }
            public sigortaliIndirimEkprimInputType[] sigortaliIndirimEkprimInputTypeList { get; set; }
            public string sigortaliSoyad { get; set; }
            public sigortaliUygulamaInputType[] sigortaliUygulamaList { get; set; }
            public uyrukTuru sigortaliUyruk { get; set; }
            public int? ulkeKodu { get; set; }
            public long urunPlanKod { get; set; }
            public string yasadigiIlKod { get; set; }
            public string yasadigiIlceKod { get; set; }

        }

        public class sigortaliSaglikBilgileriInputType
        {
            public evetHayirType alkolKullaniyorMu { get; set; }
            public string alkolKullanmaSuresi { get; set; }
            public aliskanlikPeriyodType alkolPeriyotTip { get; set; }
            public int boy { get; set; }
            public int kilo { get; set; }
            public string periyottakiAlkolMiktari { get; set; }
            public string periyottakiSigaraAdedi { get; set; }
            public sigortaliSaglikBilgileriDetayInputType[] saglikDetayBilgileri { get; set; }
            public evetHayirType sigaraIciyorMu { get; set; }
            public string sigaraIcmeSuresi { get; set; }
            public aliskanlikPeriyodType sigaraPeriyotTip { get; set; }

        }

        public class seyahatSigortaliTeminatliInputType : seyahatSigortaliInputType
        {
            public seyahatSigortaliTeminatInputType[] sigortaliTeminatInputTypeListField;
        }

        public class seyahatSigortaliGirisInputType : zeylInputType
        {
            public decimal policeBrutPrimi { get; set; }
            public decimal policeNetPrimi { get; set; }
            public seyahatSigortaliTeminatliInputType[] sigortali { get; set; }
            public decimal zeylBrutPrimi { get; set; }
            public decimal zeylNetPrimi { get; set; }
            public long zeylTuru { get; set; }
        }

        public class seyahatSigortaliInputType
        {
            public string adres { get; set; }
            public string babaAdi { get; set; }
            public string bireySiraNo { get; set; }
            public long bireyTipKod { get; set; }
            public decimal brutPrim { get; set; }
            public cinsiyet cinsiyet { get; set; }
            public DateTime dogumTarihi { get; set; }
            public string dogumYeri { get; set; }
            public string eskiPoliceNo { get; set; }
            public int eskiYenilemeNo { get; set; }
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public decimal netPrim { get; set; }
            public string sigortaliAd { get; set; }
            public sigortaliIndirimEkprimInputType[] sigortaliIndirimEkprimInputTypeList { get; set; }
            public string sigortaliSoyad { get; set; }
            public uyrukTuru sigortaliUyruk { get; set; }
            public int ulkeKodu { get; set; }
            public long urunPlanKod { get; set; }
            public string yasadigiIlKod { get; set; }
            public string yasadigiIlceKod { get; set; }
        }

        public class uretimKaynakInputType : zeylPrimInputType
        {
            public long uretimKaynakKod { get; set; }
            public string uretimKaynakKurumKod { get; set; }
        }

        public class ulkeDegisiklikInputType : zeylPrimInputType
        {
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public long urunPlanKod { get; set; }
        }

        public class teminatZeyliInputType : zeylPrimInputType
        {
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public teminatDegisiklikInputType[] sigortaliTeminatInputTypeList { get; set; }
        }

        public class teminatInputType : zeylPrimInputType
        {
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public sigortaliTeminatInputTypeList[] sigortaliTeminatInputTypeList { get; set; }
            public long urunPlanKod { get; set; }
        }

        public class sigortaliPrimFarkiInputType : zeylPrimInputType
        {
            public decimal brutPrim { get; set; }
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public decimal netPrim { get; set; }
        }

        public class sigortaliCikisInputType : zeylPrimInputType
        {
            public decimal brutPrim { get; set; }
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public decimal netPrim { get; set; }
        }

        public class sigortaliBilgiDegisiklikInputType : zeylPrimInputType
        {
            public sigortaliBilgileriInputType[] sigortali { get; set; }
        }

        public class sigortaliBilgileriInputType
        {
            public decimal brutPrim { get; set; }
            public string eskiPoliceNo { get; set; }
            public int eskiYenilemeNo { get; set; }
            public DateTime ilkOzelSigortalilikTarihi { get; set; }
            public string ilkOzelSirketKod { get; set; }
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public long meslekKod { get; set; }
            public decimal netPrim { get; set; }
            public sigortaliSaglikBilgileriInputType saglikBilgileriInputType { get; set; }
            public sigortaliIndirimEkprimInputType[] sigortaliIndirimEkprimInputTypeList { get; set; }
            public sigortaliUygulamaInputType[] sigortaliUygulamaList { get; set; }
            public uyrukTuru sigortaliUyruk { get; set; }
            public string yasadigiIlKod { get; set; }
            public string yasadigiIlceKod { get; set; }
        }

        public class sigortaEttirenZeylInputType : zeylPrimInputType
        {
            public string ad { get; set; }
            public string adres { get; set; }
            public string babaAdi { get; set; }
            public cinsiyet cinsiyet { get; set; }
            public DateTime dogumTarihi { get; set; }
            public string dogumYeri { get; set; }
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public DateTime kurulusTarihi { get; set; }
            public string kurulusYeri { get; set; }
            //public uyrukTuru sigortaEttirenUyruk { get; set; }
            public string soyad { get; set; }
            public turKod turKod { get; set; }
            public int ulkeKodu { get; set; }
        }

        public class seyahatSigortaliCikisInputType : zeylPrimInputType
        {
            public decimal brutPrim { get; set; }
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public decimal netPrim { get; set; }
        }

        public class primFarkiInputType : zeylPrimInputType
        {
            public int zeylTipi { get; set; } //zeylType enum value
        }

        public class policeYururlugeAlmaInputType : zeylPrimInputType
        {

        }

        public class genelIptalInputType : zeylPrimInputType
        {
            public long? iptalNedenKod { get; set; }
            public long iptalZeylTuru { get; set; }
        }

        public class primGuncellemeInputType
        {
            public string otorizasyonKod { get; set; }
            public decimal policeBrutPrimi { get; set; }
            public decimal policeNetPrimi { get; set; }
            public string policeNo { get; set; }
            public long sbmSagmerNo { get; set; }
            public string sigortaSirketKod { get; set; }
            public int yenilemeNo { get; set; }
            public decimal zeylBrutPrimi { get; set; }
            public decimal zeylNetPrimi { get; set; }
            public int zeylNo { get; set; }
        }

        public class policeListSorguInputType
        {
            public DateTime baslangicTarihi { get; set; }
            public DateTime bitisTarihi { get; set; }
            public string sigortaSirketKod { get; set; }
        }

        public class policeSilmeInputType
        {
            public string otorizasyonKod { get; set; }
            public string policeNo { get; set; }
            public long sbmSagmerNo { get; set; }
            public int yenilemeNo { get; set; }
        }

        public class dosyaInputType : dosyaType
        {
            public long dosyaRedNedeni { get; set; }
            public faturaType[] faturaList { get; set; }
            public icdType[] icdList { get; set; }
            public DateTime ihbarTarihi { get; set; }
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public DateTime muallakOdemeTarihi { get; set; }
            public decimal muallakTutariTL { get; set; }
            public decimal odemeTutariTL { get; set; }
            public odemeYeriType odemeYeri { get; set; }
            public DateTime olayTarihi { get; set; }
            public string otorizasyonKod { get; set; }
            public string paraBirimi { get; set; }
            public DateTime provizyonTarihi { get; set; }
            public saglikKurumType[] saglikKurumList { get; set; }
            public long tazminatDosyaDurumu { get; set; }
            public long tazminatSiraNo { get; set; }
            public tedaviTipiType tedaviTipi { get; set; }
        }

        public class dosyaTazminatOdemeInputType : dosyaType
        {
            public long dosyaRedNedeni { get; set; }
            public DateTime muallakOdemeTarihi { get; set; }
            public decimal muallakTutariTL { get; set; }
            public decimal odemeTutariTL { get; set; }
            public string otorizasyonKod { get; set; }
            public saglikKurumType[] saglikKurumList { get; set; }
            public long tazminatDosyaDurumu { get; set; }
        }

        public class dosyaFaturaInputType : dosyaType
        {
            public faturaType[] faturaList { get; set; }
        }

        public class dosyaKurumInputType : dosyaType
        {
            public long dosyaRedNedeni { get; set; }
            public DateTime muallakOdemeTarihi { get; set; }
            public decimal muallakTutariTL { get; set; }
            public decimal odemeTutariTL { get; set; }
            public string otorizasyonKod { get; set; }
            public saglikKurumType[] saglikKurumList { get; set; }
            public long tazminatDosyaDurumu { get; set; }
        }

        public class policeBilgiGuncellemeInputType
        {
            public long branshKod { get; set; }
            public string dovizCinsi { get; set; }
            public string endirektSigortaSirketKod { get; set; }
            public string eskiPoliceNo { get; set; }
            public int eskiYenilemeNo { get; set; }
            public string ilKod { get; set; }
            public long odemeKod { get; set; }
            public string otorizasyonKod { get; set; }
            public string policeNo { get; set; }
            public string policeTip { get; set; }
            public long sbmSagmerNo { get; set; }
            public string sigortaSirketKod { get; set; }
            public long tarifeID { get; set; }
            public string uretimKaynakKurumKod { get; set; }
            public int vadeSayisi { get; set; }
            public int yenilemeNo { get; set; }
        }

        public class tazminatDosyaSilmeInputType
        {
            public string otorizasyonKod { get; set; }
            public long sbmSagmerNo { get; set; }
            public string tazminatDosyaNo { get; set; }
        }

        public class dosyaSorguInputType
        {
            public string otorizasyonKod { get; set; }
            public string tazminatDosyaNo { get; set; }
        }

        public class sigortaliSorguInputType
        {
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public long sbmSagmerNo { get; set; }
            public string sigortaSirketKod { get; set; }
        }

        public class digerSirketPoliceSorguInputType
        {
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
        }

        public class tanzimTarihiGuncellemeInputType
        {
            public string otorizasyonKod { get; set; }
            public DateTime policeTanzimTarihi { get; set; }
            public long sbmSagmerNo { get; set; }
            public int zeylNo { get; set; }
            public DateTime zeylTanzimTarihi { get; set; }
        }

        public class sigortaliBilgiInputType
        {
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public long sbmSagmerNo { get; set; }
        }

        public class policeOzetMutabakatInputType
        {
            public DateTime tarih { get; set; }
            public tarihType tarihTipi { get; set; }
        }

        public class sgmSigortaliGunlukMutabakatInputType
        {
            public long siraNo { get; set; }
            public DateTime tarih { get; set; }
            public tarihType tarihTipi { get; set; }
        }

        public class policeZeylListSorguInputType
        {
            public DateTime baslangicTarihiField;
            public DateTime bitisTarihiField;
            public string sigortaSirketKodField;
        }

        #endregion

        #region Output Types

        public class teminatOdemeType
        {
            public int donemNo { get; set; }
            public decimal muallakTutariTL { get; set; }
            public long odemeSiraNo { get; set; }
            public odemeTipiType odemeTipi { get; set; }
            public decimal odemeTutariTL { get; set; }
            public int teminatNo { get; set; }
            public int varyasyonNo { get; set; }
        }

        public class saglikKurumType
        {
            public string kurumIlcesi { get; set; }
            public string kurumIli { get; set; }
            public string kurumUlke { get; set; }
            public string saglikKurumNo { get; set; }
            public string saglikKurumNoTipi { get; set; } //saglikKurumNoType enum value
            public teminatOdemeType[] teminatList { get; set; }
        }

        public class faturaType
        {
            public string ettn { get; set; }
            public string faturaNo { get; set; }
            public DateTime faturaTarihi { get; set; }
            public decimal faturaTutariTL { get; set; }
            public decimal faturaTutariTLOdenen { get; set; }
            public string paraBirimi { get; set; }
        }

        public class icdType
        {
            public string iCDNo { get; set; }
            public int iCDTip { get; set; }
            public int donemNo { get; set; }
            public int teminatNo { get; set; }
            public int varyasyonNo { get; set; }
        }

        public class sonucType
        {
            public string aciklama { get; set; }
            public bool islemBasarili { get; set; }
        }

        public class tazminatDosyaSonucType : sonucType
        {
            public long dosyaRedNedeni { get; set; }
            public string ettn { get; set; }
            public faturaType[] faturaList { get; set; }
            public icdType[] icdList { get; set; }
            public DateTime ihbarTarihi { get; set; }
            public DateTime islemTarihi { get; set; }
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public odemeYeriType odemeYeri { get; set; }
            public DateTime olayTarihi { get; set; }
            public string paraBirimi { get; set; }
            public DateTime provizyonTarihi { get; set; }
            public saglikKurumType[] saglikKurumList { get; set; }
            public long sbmSagmerNo { get; set; }
            public long tazminatDosyaDurumu { get; set; }
            public tedaviTipiType tedaviTipi { get; set; }
        }

        public class sigortaliBasvuruRedSonucType : sonucType
        {

        }

        public class seyahatSigortaliSonucType : sonucType
        {
            public long sigortaliNo { get; set; }
        }

        public class policeYenilenmemeSonucType : sonucType
        {

        }

        public class policeSonucType : sonucType
        {
            public string policeNo { get; set; }
            public long sbmSagmerNo { get; set; }
            public int yenilemeNo { get; set; }
            public int zeylNo { get; set; }
        }

        public class policeBireyselSonucType : sonucType
        {
            public long sbmsagmerNo { get; set; }
            public System.Nullable<long>[] sigortaliNo { get; set; }
            public int yenilemeNo { get; set; }
            public int zeylNo { get; set; }
        }

        public class zeylSonucType : sonucType
        {

        }

        public class sgmPoliceGunlukMutabakatType
        {
            public long branchKod { get; set; }
            public string policeNo { get; set; }
            public policeTipiType policeTip { get; set; }
            public long sbmSagmerNo { get; set; }
            public string sigortaSirketKod { get; set; }
            public long siraNo { get; set; }
            public int yenilemeNo { get; set; }
            public decimal zeylNetPrim { get; set; }
            public int zeylNo { get; set; }
            public DateTime zeylTanzimTarihi { get; set; }
            public long zeylTuru { get; set; }
        }

        public class policeSorguSonucType
        {
            public policeType police { get; set; }
        }

        public class policeType
        {
            public int bransh { get; set; }
            public decimal brutPrim { get; set; }
            public decimal netPrim { get; set; }
            public DateTime policeBaslamaTarihi { get; set; }
            public DateTime policeBitisTarihi { get; set; }
            public string policeNo { get; set; }
            public DateTime policeTanzimTarihi { get; set; }
            public long sbmSagmerNo { get; set; }
            public sigortaEttiren sigortaEttiren { get; set; }
            public string sigortaSirketKod { get; set; }
            [XmlElement("sigortaliList")]
            public sigortali[] sigortaliList { get; set; }
            public long uretimKaynakKod { get; set; }
            public string uretimKaynakKurumKod { get; set; }
            public int yenilemeNo { get; set; }
            [XmlElement("zeylList")]
            public policeZeyl[] zeylList { get; set; }
            public int zeylNo { get; set; }
        }

        public class sigortaEttiren
        {
            public string kimlikNo { get; set; }
            public string kimlikTip { get; set; } //identityType enum value
            public turKod turKod { get; set; }
        }

        public class sigortali : sigortaliInputType
        {
           // public string kimlikTip { get; set; } //identityType enum value
            //public evetHayirType policedeMi { get; set; }
            public sigortaliTeminatInputTypeList[] sigortaliTeminatInputTypeListNotXML { get; set; }
        }

        public class policeZeyl
        {
            public DateTime policeBaslamaTarihi { get; set; }
            public DateTime policeBitisTarihi { get; set; }
            public decimal policeBrutPrim { get; set; }
            public decimal policeNetPrim { get; set; }
            public string policeNo { get; set; }
            public DateTime policeTanzimTarihi { get; set; }
            public long sbmSagmerNo { get; set; }
            public string sigortaSirketKod { get; set; }
            public int yenilemeNo { get; set; }
            public DateTime zeylBaslamaTarihi { get; set; }
            public decimal zeylBrutPrim { get; set; }
            public decimal zeylNetPrim { get; set; }
            public int zeylNo { get; set; }
            public DateTime zeylTanzimTarihi { get; set; }
            public long zeylTip { get; set; }
        }

        public class policeUretimKaynakKurumKodType
        {
            public string policeNo { get; set; }
            public long sbmSagmerNo { get; set; }
            public string sigortaSirketKod { get; set; }
            public long uretimKaynakKod { get; set; }
            public string uretimKaynakKurumKod { get; set; }
            public int yenilemeNo { get; set; }
        }

        public class dosyaType
        {
            public long sbmSagmerNo { get; set; }
            public string tazminatDosyaNo { get; set; }
        }

        public class dosyaUpdateType : dosyaType
        {
            public DateTime dosyaGuncellemeTarihi { get; set; }
            public long dosyaRedNedeni { get; set; }
            public icdInputType[] icdList { get; set; }
            public DateTime ihbarTarihi { get; set; }
            public odemeYeriType odemeYeri { get; set; }
            public DateTime olayTarihi { get; set; }
            public string otorizasyonKod { get; set; }
            public string paraBirimi { get; set; }
            public DateTime provizyonTarihi { get; set; }
            public saglikKurumUpdateType[] saglikKurumList { get; set; }
            public long tazminatDosyaDurumu { get; set; }
            public tedaviTipiType tedaviTipi { get; set; }
        }

        public class saglikKurumUpdateType
        {
            public string kurumIlcesi { get; set; }
            public string kurumIlcesiYeni { get; set; }
            public string kurumIli { get; set; }
            public string kurumIliYeni { get; set; }
            public string kurumUlke { get; set; }
            public string kurumUlkeYeni { get; set; }
            public string saglikKurumNo { get; set; }
            public string saglikKurumNoTipi { get; set; } //saglikKurumNoType enum value
            public string saglikKurumNoTipiYeni { get; set; } //saglikKurumNoType enum value
            public string saglikKurumNoYeni { get; set; }
        }

        public class digerSirketPoliceSorguSonucType
        {
            public digerSirketPoliceType[] digerSirketPoliceSorguList { get; set; }
        }

        public class digerSirketPoliceType
        {
            public string bireyTipKodAciklama { get; set; }
            public string bransKodAciklama { get; set; }
            public string policeTip { get; set; }
            public string sigortaSirketAdi { get; set; }
            public string sigortaliAd { get; set; }
            public DateTime sigortaliGirisTarihi { get; set; }
            public string sigortaliSoyad { get; set; }
            public digerSirketPoliceSorguTeminatListType[] teminatList { get; set; }
            public string urunPlanKodAciklama { get; set; }
        }

        public class digerSirketPoliceSorguTeminatListType
        {
            public string aso { get; set; }
            public string cografiKapsamKoduAciklama { get; set; }
            public DateTime donemBaslangicTarihi { get; set; }
            public DateTime donemBitisTarihi { get; set; }
            public string donemKoduAciklama { get; set; }
            public long teminatKodu { get; set; }
            public string toplamDonemSayisi { get; set; }
        }

        public class tanzimTarihiGuncellemeSonucType
        {

        }

        public class sigortaliBilgiSonucType
        {
            public long branchId { get; set; }
            public DateTime policeBaslamaTarihi { get; set; }
            public DateTime policeBitisTarihi { get; set; }
            public decimal policeNetPrim { get; set; }
            public string policeNo { get; set; }
            public DateTime policeTanzimTarihi { get; set; }
            public policeTipiType policeTip { get; set; }
            public long sbmSagmerNo { get; set; }
            public string sigortaSirketKod { get; set; }
            public sigortaliOutputType sigortali { get; set; }
            public long tarifeId { get; set; }
            public int yenilemeNo { get; set; }
        }

        public class sigortaliOutputType
        {
            public string adres { get; set; }
            public string aileBagNo { get; set; }
            public string babaAdi { get; set; }
            public string bireySiraNo { get; set; }
            public long bireyTipKod { get; set; }
            public decimal brutPrim { get; set; }
            public string cinsiyet { get; set; }
            public DateTime dogumTarihi { get; set; }
            public string dogumYeri { get; set; }
            public string eskiPoliceNo { get; set; }
            public int eskiYenilemeNo { get; set; }
            public DateTime ilkOzelSigortalilikTarihi { get; set; }
            public string ilkOzelSirketKod { get; set; }
            public sigortaliIndirimEkprimOutputType[] indirimEkPrimList { get; set; }
            public string kimlikNo { get; set; }
            public string kimlikTipKod { get; set; } //identityType enum value
            public long meslekKod { get; set; }
            public decimal netPrim { get; set; }
            public saglikBilgiOutputType[] saglikBilgiList { get; set; }
            public string sigortaliAd { get; set; }
            public string sigortaliSoyad { get; set; }
            public sigortaliTeminatOutputType[] teminatList { get; set; }
            public string ulkeKodu { get; set; }
            public long urunPlanKod { get; set; }
            public sigortaliUygulamaOutputType[] uygulamaList { get; set; }
            public string yasadigiIlKod { get; set; }
            public string yasadigiIlceKod { get; set; }
            public zeylOutputType[] zeylList { get; set; }
        }

        public class sigortaliIndirimEkprimOutputType
        {
            public string aciklama { get; set; }
            public decimal indirimEkprimDegeri { get; set; }
            public long indirimEkprimKod { get; set; }
            public long kayitNo { get; set; }
            public oranTutarType tutarMiOranMi { get; set; }
        }

        public class saglikBilgiOutputType
        {
            public evetHayirType alkolKullaniyorMu { get; set; }
            public string alkolKullanmaSuresi { get; set; }
            public string alkolPeriyotTip { get; set; }
            public int boy { get; set; }
            public int kilo { get; set; }
            public string periyottakiAlkolMiktari { get; set; }
            public string periyottakiSigaraAdedi { get; set; }
            public sigortaliDetayBilgileriOutputType[] saglikDetayBilgiler { get; set; }
            public evetHayirType sigaraIciyorMu { get; set; }
            public string sigaraIcmeSuresi { get; set; }
            public string sigaraPeriyotTip { get; set; }
        }

        public class sigortaliDetayBilgileriOutputType
        {
            public evetHayirType ameliyatVarmi { get; set; }
            public string hastalikAciklamasi { get; set; }
            public string hastalikAdi { get; set; }
            public string hastalikKod { get; set; }
            public long hastalikUygulamaKod { get; set; }
            public string sonDurum { get; set; }
            public int sureAy { get; set; }
            public int sureYil { get; set; }
            public string uygulamaAciklamasi { get; set; }
        }

        public class sigortaliTeminatOutputType
        {
            public evetHayirType aso { get; set; }
            public long cografiKapsamKodu { get; set; }
            public DateTime donemBaslangicTarihi { get; set; }
            public DateTime donemBitisTarihi { get; set; }
            public int donemKodu { get; set; }
            public string dovizCinsi { get; set; }
            public int teminatAdedi { get; set; }
            public long teminatKodu { get; set; }
            public long teminatLimitKodu { get; set; }
            public decimal teminatLimiti { get; set; }
            public long teminatMuafiyetKodu { get; set; }
            public decimal teminatMuafiyeti { get; set; }
            public decimal teminatNetPrim { get; set; }
            public decimal teminatOdemeYuzdesi { get; set; }
            public int teminatVaryasyonKodu { get; set; }
            public int toplamDonemSayisi { get; set; }
        }

        public class sigortaliUygulamaOutputType
        {
            public string aciklama { get; set; }
            public string hastalikKod { get; set; }
            public long kayitNo { get; set; }
            public decimal muafiyetTutarLimiti { get; set; }
            public long sigortaliUygulamaKod { get; set; }
        }

        public class zeylOutputType
        {
            public DateTime zeylBaslamaTarihi { get; set; }
            public decimal zeylNetPrim { get; set; }
            public int zeylNo { get; set; }
            public DateTime zeylTanzimTarihi { get; set; }
            public long zeylTuru { get; set; }
        }

        public class policeOzetMutabakatSonucType
        {
            public long saglikBireyselPoliceUretimAdet { get; set; }
            public decimal saglikBireyselPoliceUretimPrimTL { get; set; }
            public long saglikBireyselSigortaliGirisAdet { get; set; }
            public long saglikKurumsalPoliceUretimAdet { get; set; }
            public decimal saglikKurumsalPoliceUretimiPrimTL { get; set; }
            public long saglikKurumsalSigortaliGirisAdet { get; set; }
            public decimal seyahatBireyselPoliceUretimPrimTL { get; set; }
            public long seyahatBireyselPoliceUretimiAdet { get; set; }
            public long seyahatBireyselSigortaliGirisAdet { get; set; }
            public long seyahatKurumsalPoliceUretimAdet { get; set; }
            public decimal seyahatKurumsalPoliceUretimPrimTL { get; set; }
            public long seyahatKurumsalSigortaliGirisAdet { get; set; }
        }

        public class sgmSigortaliGunlukMutabakatType
        {
            public long branchKod { get; set; }
            public string policeNo { get; set; }
            public policeTipiType policeTip { get; set; }
            public long sbmSagmerNo { get; set; }
            public string sigortaSirketKod { get; set; }
            public sigortali[] sigortaliList { get; set; }
            public long siraNo { get; set; }
            public int yenilemeNo { get; set; }
            public decimal zeylNetPrim { get; set; }
            public int zeylNo { get; set; }
            public DateTime zeylTanzimTarihi { get; set; }
            public long zeylTuru { get; set; }
        }

        #endregion

        #region Custom Types For Array Objects

        //ServiceClient method name = policeListSorgu
        public class PoliceList
        {
            public PoliceList()
            {
                policeler = new List<policeType>();
            }

            [XmlElement("policeList")]
            public List<policeType> policeler { get; set; }
        }

        //ServiceClient method name = policeZeylListSorgu
        public class PoliceZeylList
        {
            public PoliceZeylList()
            {
                policeZeyilleri = new List<policeZeyl>();
            }

            [XmlElement("policeZeylList")]
            public List<policeZeyl> policeZeyilleri { get; set; }
        }

        //ServiceClient method name = policeGunlukMutabakat
        public class SgmPoliceGunlukMutabakatTypeList
        {
            public SgmPoliceGunlukMutabakatTypeList()
            {
                sgmPoliceGunlukMutabakatTypeListesi = new List<sgmPoliceGunlukMutabakatType>();
            }

            [XmlElement("list")]
            public List<sgmPoliceGunlukMutabakatType> sgmPoliceGunlukMutabakatTypeListesi { get; set; }
        }

        //ServiceClient method name = sigortaliGunlukMutabakat
        public class SgmSigortaliGunlukMutabakatTypeList
        {
            public SgmSigortaliGunlukMutabakatTypeList()
            {
                sgmSigortaliGunlukMutabakatTypeListesi = new List<sgmSigortaliGunlukMutabakatType>();
            }

            [XmlElement("list")]
            public List<sgmSigortaliGunlukMutabakatType> sgmSigortaliGunlukMutabakatTypeListesi { get; set; }
        }

        #endregion


    }//End class
}
