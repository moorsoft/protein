﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.SagmerOnlineService
{
    public class Enums
    {
        //Documented enums
        public enum SagmerServiceType
        {
            KONTROL = 1,
            CANLI = 2
        }
        public enum policeTipiType
        {
            G, //Grup
            F, //Ferdi
        }

        public enum yeniYenilemeGecisType
        {
            Item1 = 1, //Yeni Is
            Item2 = 2, //Yenileme
            Item3 = 3, //Sigortalı Geçis
        }

        public enum identityType
        {
            Item1 = 1, //TC Kimlik
            Item2 = 2, //Vergi Kimlik
            Item3 = 3, //Pasaport
            Item4 = 4, //Yabancı Kimlik
            Item9 = 9, //Kimliksiz
        }

        public enum aliskanlikPeriyodType
        {
            G, //Gün
            H, //Hafta
            A, //Ay
            Y, //Yıl
        }

        public enum zeylType
        {
            Item1 = 1, //Baslangıç
            Item2 = 2, //Iptal
            Item3 = 3, //Sigortalı Girisi
            Item4 = 4, //Sigortalı Çıkısı 4
            Item5 = 5, //Teminat / Plan Degisikligi 5
            Item6 = 6, //Üretim Kaynak Degisikligi 6
            Item7 = 7, //Sigortalı Bilgi Degisikligi 7
            Item8 = 8, //Sigorta Ettiren Degisikligi 8
            Item9 = 9, //Gidecegi Ülke Degisikligi 9
            Item10 = 10, //Prim Farkı Zeyli 10,
            Item11 = 11, //Kısa Dönem Esaslı Iptal 11,
            Item12 = 12, //Gün Esaslı Iptal 12,
            Item13 = 13, //Prim Iadesiz Iptal 13,
            Item14 = 14, //Dondurma Sonucu Iptal 14,
            Item15 = 15, //Mebdeinden-Baslangıçtan Iptal 15,
            Item16 = 16, //Meriyete Dönüs Zeyli 16,
            Item17 = 17, //Baslangıç Bitis Tarihi Degisikligi 17,
            Item18 = 18, //Poliçe Tahakkuk 18,
            Item19 = 19, //Sigortalı Tahakkuk 19,
            Item20 = 20, //Teminat Zeyli 20,
        }

        public enum uyrukTuru
        {
            TC, //TC Uyruk
            YABANCI, //Yabancı Uyruk
        }

        public enum evetHayirType
        {
            E, //Evet
            H, //Hayır
        }

        public enum cinsiyet
        {
            E, //Erkek
            K, //Kadın
        }

        public enum turKod
        {
            O, //Özel
            T, //Tüzel
        }

        public enum assuranceOperationType
        {
            I, //Teminat Ekleme
            D, //Teminat Çıkarma
            U, //teminat Güncelleme
        }


        //Undocumented enums

        public enum odemeTipiType
        {
            ODEME,
            IADE,
            RED,
        }

        public enum saglikKurumNoType
        {
            Item1 = 1,
            Item2 = 2,
            Item9 = 9,
        }

        public enum islemTipiType
        {
            EKLEME,
            SILME,
            GUNCELLEME,
        }

        public enum odemeYeriType
        {
            ANLASMALI,
            ELDEN,
        }

        public enum tedaviTipiType
        {
            AYAKTA,
            YATARAK,
        }

        public enum oranTutarType
        {
            O,
            T,
        }

        public enum tarihType
        {
            OLUSTURMA,
            TANZIM,
        }


    }
}
