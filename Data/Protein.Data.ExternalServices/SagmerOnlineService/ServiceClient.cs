﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Protein.Common.Extensions;
using Protein.Common.Helpers;
using Protein.Data.ExternalServices.Log;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;
using static Protein.Data.ExternalServices.SagmerOnlineService.InputOutputTypes;

namespace Protein.Data.ExternalServices.SagmerOnlineService
{
    /// <summary>
    /// PoliceOnlineServiceClient
    /// </summary>
    public class ServiceClient
    {
        string sagmerURL = ConfigurationManager.AppSettings["SagmerURL"];
        string dogaSagmerURL = ConfigurationManager.AppSettings["DogaSagmerURL"];
        private static readonly string Url = "https://www.sbm.org.tr/SagmerPoliceOnlineWS-prod-V1.0/PoliceOnlineService";  //AppSettingsHelper.GetKey(AppKeys.SagmerUrl).ToString();

        //private static readonly string Username = "105|katilim";
        //private static readonly string Password = "Cd5B2aTp";  //AppSettingsHelper.GetKey(AppKeys.SagmerKurumKod).ToString();

        private Int64 CompanyId = 0;
        private static readonly string SagmerSoapEnvelopeTemp = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pts=\"http://pts.sagmer.org.tr\">"
                                                              + "  <soapenv:Header>"
                                                              + "    <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
                                                              + "      <wsse:UsernameToken xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
                                                              + "        <wsse:Username>{Username}</wsse:Username>"
                                                              + "        <wsse:KurumKod>{Password}</wsse:KurumKod>"
                                                              + "      </wsse:UsernameToken>"
                                                              + "    </wsse:Security>"
                                                              + "  </soapenv:Header>"
                                                              + "  <soapenv:Body>"
                                                              + "    <pts:{ComplexTypeName}>"
                                                              + "      <{Prefix}{ElementName}>"
                                                              + "        {InputFields}"
                                                              + "      </{Prefix}{ElementName}>"
                                                              + "    </pts:{ComplexTypeName}>"
                                                              + "  </soapenv:Body>"
                                                              + "</soapenv:Envelope>";


        
        private string CallService(XmlDocument soapEnvelopeXmlDoc)
        {
            string soapResult = string.Empty;


            //System.Net.ServicePointManager.ServerCertificateValidationCallback +=
            // (sender, cert, chain, sslPolicyErrors) =>
            // {
            //     if (cert != null) System.Diagnostics.Debug.WriteLine(cert);
            //     return true;
            // };
            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            //byte[] bytes;

            //string soapStr = soapEnvelopeXmlDoc.InnerXml;

            //bytes = Encoding.UTF8.GetBytes(soapStr);
            //request.ContentType = "text/xml";
            //request.ContentLength = bytes.Length;
            //request.Method = "POST";

            //Stream requestStream = request.GetRequestStream();
            //requestStream.Write(bytes, 0, bytes.Length);
            //requestStream.Close();
            //HttpWebResponse response;
            //response = (HttpWebResponse)request.GetResponse();
            //if (response.StatusCode == HttpStatusCode.OK)
            //{
            //    Stream responseStream = response.GetResponseStream();
            //    string responseStr = new StreamReader(responseStream).ReadToEnd();
            //    soapResult = responseStr;
            //}

            Stream requestStream = null;
            Stream responseStream = null;
            HttpWebRequest webRequest = null;
            WebResponse webResponse = null;
            StreamReader rd = null;

            try
            {
                if (CompanyId == 10 || CompanyId == 95)
                {
                    string url = ConfigurationManager.AppSettings["HDISagmerURL"];
                    //webRequest = (HttpWebRequest)WebRequest.Create("http://portaltest.hdisigorta.com.tr/services/SagmerPoliceOnlineWSProxy?wsdl"); //TEST
                    //webRequest = (HttpWebRequest)WebRequest.Create("http://79.99.176.153/services/SagmerPoliceOnlineWSProxy"); //CANLI
                    webRequest = (HttpWebRequest)WebRequest.Create(url); //CANLI
                }
                else
                {
                    webRequest = (HttpWebRequest)WebRequest.Create(Url);
                }

                webRequest.ContentType = "text/xml;charset=\"utf-8\"";
                //webRequest.Proxy = new WebProxy("185.141.109.27", 808);
                webRequest.Accept = "text/xml";
                webRequest.Method = "POST";

                requestStream = webRequest.GetRequestStream();
                soapEnvelopeXmlDoc.Save(requestStream);

                // begin async call to web request.
                IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

                // suspend this thread until call is complete. You might want to
                // do something usefull here like update your UI.
                asyncResult.AsyncWaitHandle.WaitOne();

                // get the response from the completed web request.
                webResponse = webRequest.EndGetResponse(asyncResult);
                rd = new StreamReader(webResponse.GetResponseStream());

                soapResult = rd.ReadToEnd();
            }
            catch (WebException ex)
            {
                WebResponse errResp = ex.Response;
                using (Stream respStream = errResp.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(respStream);
                    soapResult = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                soapResult = ex.Message;
            }
            finally
            {
                if (requestStream != null)
                {
                    requestStream.Close();
                    requestStream = null;
                }
                if (webResponse != null)
                {
                    webResponse.Close();
                    webResponse = null;
                }
                if (responseStream != null)
                {
                    responseStream.Close();
                    responseStream = null;
                }
                if (rd != null)
                {
                    rd.Close();
                    rd = null;
                }
            }

            return soapResult;
        }

        #region Constructor

        public ServiceClient()
        {

        }

        #endregion
        #region Service functions

        public ServiceResponse<policeSonucType> police(policeInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<policeSonucType, policeInputType>(input, "police", "police", true);
            return result;
        }

        public ServiceResponse<zeylSonucType> sigortaliGirisZeyli(sigortaliGirisInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, sigortaliGirisInputType>(input, "sigortaliGirisZeyli", "sigortaliGiris");
            return result;
        }

        public ServiceResponse<zeylSonucType> sigortaliBilgiDegisiklikZeyli(sigortaliBilgiDegisiklikInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, sigortaliBilgiDegisiklikInputType>(input, "sigortaliBilgiDegisiklikZeyli", "sigortaliBilgiDegisiklik");
            return result;
        }

        public ServiceResponse<sigortaliBasvuruRedSonucType> sigortaliBasvuruRed(sigortaliBasvuruRedInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<sigortaliBasvuruRedSonucType, sigortaliBasvuruRedInputType>(input, "sigortaliBasvuruRed", "sigortaliBasvuruRed");
            return result;
        }

        public ServiceResponse<zeylSonucType> sigortaliTahakkukZeyli(sigortaliPrimFarkiInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, sigortaliPrimFarkiInputType>(input, "sigortaliTahakkukZeyli", "sigortaliTahakkukZeyli");
            return result;
        }

        public ServiceResponse<zeylSonucType> sigortaliCikisZeyli(sigortaliCikisInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, sigortaliCikisInputType>(input, "sigortaliCikisZeyli", "sigortaliCikis");
            return result;
        }

        public ServiceResponse<zeylSonucType> policeBaslamaBitisTarihDegisiklikZeyli(policeTarihDegisiklikInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, policeTarihDegisiklikInputType>(input, "policeBaslamaBitisTarihDegisiklikZeyli", "policeTarihDegisiklik");
            return result;
        }

        public ServiceResponse<policeYenilenmemeSonucType> policeYenilenmeme(policeYenilenmemeInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<policeYenilenmemeSonucType, policeYenilenmemeInputType>(input, "policeYenilenmeme", "policeYenilenmeme");
            return result;
        }

        public ServiceResponse<zeylSonucType> policeTahakkukVePrimFarkiZeyli(primFarkiInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, primFarkiInputType>(input, "policeTahakkukVePrimFarkiZeyli", "policeTahakkukVePrimFarkiZeyli");
            return result;
        }

        public ServiceResponse<zeylSonucType> teminatZeyli(teminatZeyliInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, teminatZeyliInputType>(input, "teminatZeyli", "teminatZeyli");
            return result;
        }

        public ServiceResponse<zeylSonucType> teminatPlanDegisiklikZeyli(teminatInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, teminatInputType>(input, "teminatPlanDegisiklikZeyli", "teminatPlanDegisiklik");
            return result;
        }

        public ServiceResponse<zeylSonucType> uretimKaynakDegisiklikZeyli(uretimKaynakInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, uretimKaynakInputType>(input, "uretimKaynakDegisiklikZeyli", "uretimKaynakDegisiklik");
            return result;
        }

        public ServiceResponse<zeylSonucType> ulkeDegisiklikZeyli(ulkeDegisiklikInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, ulkeDegisiklikInputType>(input, "ulkeDegisiklikZeyli", "ulkeDegisiklik");
            return result;
        }

        public ServiceResponse<zeylSonucType> sigortaEttirenDegisiklikZeyli(sigortaEttirenZeylInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, sigortaEttirenZeylInputType>(input, "sigortaEttirenDegisiklikZeyli", "sigortaEttirenDegisiklik");
            return result;
        }

        public ServiceResponse<zeylSonucType> genelIptalZeyli(genelIptalInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, genelIptalInputType>(input, "genelIptalZeyli", "genelIptal");
            return result;
        }

        public ServiceResponse<zeylSonucType> meriyeteDonusZeyli(policeYururlugeAlmaInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, policeYururlugeAlmaInputType>(input, "meriyeteDonusZeyli", "meriyeteDonus");
            return result;
        }

        public ServiceResponse<policeSorguSonucType> policeSorgu(policeSorguInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<policeSorguSonucType, policeSorguInputType>(input, "policeSorgu", "policeSorgu");
            return result;
        }

        public ServiceResponse<PoliceList> policeListSorgu(policeListSorguInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<PoliceList, policeListSorguInputType>(input, "policeListSorgu", "policeListSorgu");
            return result;
        }

        public ServiceResponse<PoliceZeylList> policeZeylListSorgu(policeZeylListSorguInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<PoliceZeylList, policeZeylListSorguInputType>(input, "policeZeylListSorgu", "policeZeylListSorgu");
            return result;
        }

        public ServiceResponse<zeylSonucType> kimlikDuzeltme(kimlikDuzeltmeInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, kimlikDuzeltmeInputType>(input, "kimlikDuzeltme", "kimlikDuzeltme");
            return result;
        }

        public ServiceResponse<policeSonucType> seyahatPolice(seyahatPoliceInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            //use prefix
            var result = GenerateXmlAndCallService<policeSonucType, seyahatPoliceInputType>(input, "seyahatPolice", "seyahatPolice", true);
            return result;
        }

        public ServiceResponse<seyahatSigortaliSonucType> seyahatSigortaliGirisZeyli(seyahatSigortaliGirisInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<seyahatSigortaliSonucType, seyahatSigortaliGirisInputType>(input, "seyahatSigortaliGirisZeyli", "seyahatSigortaliGiris");
            return result;
        }

        public ServiceResponse<zeylSonucType> seyahatSigortaliCikisZeyli(seyahatSigortaliCikisInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, seyahatSigortaliCikisInputType>(input, "seyahatSigortaliCikisZeyli", "seyahatSigortaliCikis");
            return result;
        }

        public ServiceResponse<sigortali> sigortaliSorgu(sigortaliSorguInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<sigortali, sigortaliSorguInputType>(input, "sigortaliSorgu", "sigortaliSorgu");
            return result;
        }

        public ServiceResponse<policeSonucType> seyahatPoliceKontrol(seyahatPoliceInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            //use prefix
            var result = GenerateXmlAndCallService<policeSonucType, seyahatPoliceInputType>(input, "seyahatPoliceKontrol", "seyahatPoliceKontrol", true);
            return result;
        }

        public ServiceResponse<seyahatSigortaliSonucType> seyahatSigortaliGirisZeyliKontrol(seyahatSigortaliGirisInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<seyahatSigortaliSonucType, seyahatSigortaliGirisInputType>(input, "seyahatSigortaliGirisZeyliKontrol", "seyahatSigortaliGirisKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> seyahatSigortaliCikisZeyliKontrol(seyahatSigortaliCikisInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, seyahatSigortaliCikisInputType>(input, "seyahatSigortaliCikisZeyliKontrol", "seyahatSigortaliCikisKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> policeBaslamaBitisTarihDegisiklikZeyliKontrol(policeTarihDegisiklikInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, policeTarihDegisiklikInputType>(input, "policeBaslamaBitisTarihDegisiklikZeyliKontrol", "policeTarihDegisiklikKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> policeTahakkukVePrimFarkiZeyliKontrol(primFarkiInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, primFarkiInputType>(input, "policeTahakkukVePrimFarkiZeyliKontrol", "policeTahakkukVePrimFarkiZeyliKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> teminatPlanDegisiklikZeyliKontrol(teminatInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, teminatInputType>(input, "teminatPlanDegisiklikZeyliKontrol", "teminatPlanDegisiklikKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> meriyeteDonusZeyliKontrol(policeYururlugeAlmaInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, policeYururlugeAlmaInputType>(input, "meriyeteDonusZeyliKontrol", "meriyeteDonusKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> genelIptalZeyliKontrol(genelIptalInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, genelIptalInputType>(input, "genelIptalZeyliKontrol", "genelIptalKontrol");
            return result;
        }

        public ServiceResponse<policeBireyselSonucType> policeBireyselSaglik(saglikPoliceBireyselInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<policeBireyselSonucType, saglikPoliceBireyselInputType>(input, "policeBireyselSaglik", "saglikPoliceBireysel");
            return result;
        }

        public ServiceResponse<policeBireyselSonucType> policeBireyselSeyahat(seyahatPoliceBireyselInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<policeBireyselSonucType, seyahatPoliceBireyselInputType>(input, "policeBireyselSeyahat", "seyahatPoliceBireysel");
            return result;
        }

        public ServiceResponse<policeBireyselSonucType> policeBireyselSaglikKontrol(saglikPoliceBireyselInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<policeBireyselSonucType, saglikPoliceBireyselInputType>(input, "policeBireyselSaglikKontrol", "saglikPoliceBireyselKontrol");
            return result;
        }

        public ServiceResponse<policeBireyselSonucType> policeBireyselSeyahatKontrol(seyahatPoliceBireyselInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<policeBireyselSonucType, seyahatPoliceBireyselInputType>(input, "policeBireyselSeyahatKontrol", "seyahatPoliceBireyselKontrol");
            return result;
        }

        public ServiceResponse<policeSonucType> policeKontrol(policeInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            //use prefix
            var result = GenerateXmlAndCallService<policeSonucType, policeInputType>(input, "policeKontrol", "policeKontrol", true);
            return result;
        }

        public ServiceResponse<policeYenilenmemeSonucType> policeYenilenmemeKontrol(policeYenilenmemeInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<policeYenilenmemeSonucType, policeYenilenmemeInputType>(input, "policeYenilenmemeKontrol", "policeYenilenmemeKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> sigortaEttirenDegisiklikZeyliKontrol(sigortaEttirenZeylInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, sigortaEttirenZeylInputType>(input, "sigortaEttirenDegisiklikZeyliKontrol", "sigortaEttirenDegisiklikKontrol");
            return result;
        }

        public ServiceResponse<sigortaliBasvuruRedSonucType> sigortaliBasvuruRedKontrol(sigortaliBasvuruRedInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<sigortaliBasvuruRedSonucType, sigortaliBasvuruRedInputType>(input, "sigortaliBasvuruRedKontrol", "sigortaliBasvuruRedKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> sigortaliBilgiDegisiklikZeyliKontrol(sigortaliBilgiDegisiklikInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, sigortaliBilgiDegisiklikInputType>(input, "sigortaliBilgiDegisiklikZeyliKontrol", "sigortaliBilgiDegisiklikKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> sigortaliCikisZeyliKontrol(sigortaliCikisInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, sigortaliCikisInputType>(input, "sigortaliCikisZeyliKontrol", "sigortaliCikisKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> sigortaliGirisZeyliKontrol(sigortaliGirisInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, sigortaliGirisInputType>(input, "sigortaliGirisZeyliKontrol", "sigortaliGirisKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> sigortaliTahakkukZeyliKontrol(sigortaliPrimFarkiInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, sigortaliPrimFarkiInputType>(input, "sigortaliTahakkukZeyliKontrol", "sigortaliTahakkukZeyliKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> teminatZeyliKontrol(teminatZeyliInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, teminatZeyliInputType>(input, "teminatZeyliKontrol", "teminatZeyliKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> ulkeDegisiklikZeyliKontrol(ulkeDegisiklikInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, ulkeDegisiklikInputType>(input, "ulkeDegisiklikZeyliKontrol", "ulkeDegisiklikKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> uretimKaynakDegisiklikZeyliKontrol(uretimKaynakInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, uretimKaynakInputType>(input, "uretimKaynakDegisiklikZeyliKontrol", "uretimKaynakDegisiklikKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> policeUretimKaynakKurumKod(policeUretimKaynakKurumKodType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, policeUretimKaynakKurumKodType>(input, "policeUretimKaynakKurumKod", "policeUretimKaynakKurumGuncelle");
            return result;
        }

        public ServiceResponse<sonucType> policeSilme(policeSilmeInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<sonucType, policeSilmeInputType>(input, "policeSilme", "policeSilme");
            return result;
        }

        public ServiceResponse<zeylSonucType> policeUretimKaynakKurumKodKontrol(policeUretimKaynakKurumKodType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, policeUretimKaynakKurumKodType>(input, "policeUretimKaynakKurumKodKontrol", "policeUretimKaynakKurumGuncelleKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> genelZeyl(zeylInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, zeylInputType>(input, "genelZeyl", "genelZeyil");
            return result;
        }

        public ServiceResponse<zeylSonucType> genelZeylKontrol(zeylInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, zeylInputType>(input, "genelZeylKontrol", "genelZeylKontrol");
            return result;
        }

        public ServiceResponse<sonucType> primGuncelleme(primGuncellemeInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<sonucType, primGuncellemeInputType>(input, "primGuncelleme", "primGuncelleme");
            return result;
        }

        public ServiceResponse<sonucType> primGuncellemeKontrol(primGuncellemeInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<sonucType, primGuncellemeInputType>(input, "primGuncellemeKontrol", "primGuncellemeKontrol");
            return result;
        }

        public ServiceResponse<sonucType> policeBilgiGuncelleme(policeBilgiGuncellemeInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<sonucType, policeBilgiGuncellemeInputType>(input, "policeBilgiGuncelleme", "policeBilgiGuncelleme");
            return result;
        }

        public ServiceResponse<sonucType> policeBilgiGuncellemeKontrol(policeBilgiGuncellemeInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<sonucType, policeBilgiGuncellemeInputType>(input, "policeBilgiGuncelleme", "policeBilgiGuncelleme");
            return result;
        }

        public ServiceResponse<SgmPoliceGunlukMutabakatTypeList> policeGunlukMutabakat(sgmPoliceGunlukMutabakatInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<SgmPoliceGunlukMutabakatTypeList, sgmPoliceGunlukMutabakatInputType>(input, "policeGunlukMutabakat", "policeGunlukMutabakat");
            return result;
        }

        public ServiceResponse<SgmSigortaliGunlukMutabakatTypeList> sigortaliGunlukMutabakat(sgmSigortaliGunlukMutabakatInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<SgmSigortaliGunlukMutabakatTypeList, sgmSigortaliGunlukMutabakatInputType>(input, "sigortaliGunlukMutabakat", "sigortaliGunlukMutabakat");
            return result;
        }

        public ServiceResponse<policeOzetMutabakatSonucType> gunlukPoliceOzetMutabakat(policeOzetMutabakatInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<policeOzetMutabakatSonucType, policeOzetMutabakatInputType>(input, "gunlukPoliceOzetMutabakat", "gunlukPoliceOzetMutabakat");
            return result;
        }

        public ServiceResponse<sigortaliBilgiSonucType> sigortaliBilgiSorgu(sigortaliBilgiInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<sigortaliBilgiSonucType, sigortaliBilgiInputType>(input, "sigortaliBilgiSorgu", "sigortaliBilgiSorgu");
            return result;
        }

        public ServiceResponse<tanzimTarihiGuncellemeSonucType> tanzimTarihiGuncelleme(tanzimTarihiGuncellemeInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<tanzimTarihiGuncellemeSonucType, tanzimTarihiGuncellemeInputType>(input, "tanzimTarihiGuncelleme", "tanzimTarihiGuncelleme");
            return result;
        }

        public ServiceResponse<tanzimTarihiGuncellemeSonucType> tanzimTarihiGuncellemeKontrol(tanzimTarihiGuncellemeInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<tanzimTarihiGuncellemeSonucType, tanzimTarihiGuncellemeInputType>(input, "tanzimTarihiGuncellemeKontrol", "tanzimTarihiGuncellemeKontrol");
            return result;
        }

        public ServiceResponse<digerSirketPoliceSorguSonucType> digerSirketPoliceSorgu(digerSirketPoliceSorguInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<digerSirketPoliceSorguSonucType, digerSirketPoliceSorguInputType>(input, "digerSirketPoliceSorgu", "digerSirketPoliceSorgu");
            return result;
        }


        //Undocumented fonctions

        public ServiceResponse<zeylSonucType> tazminatDosyaGirisKontrol(dosyaInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, dosyaInputType>(input, "tazminatDosyaGirisKontrol", "tazminatDosyaGirisKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> tazminatDosyaGiris(dosyaInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, dosyaInputType>(input, "tazminatDosyaGiris", "tazminatDosyaGiris");
            return result;
        }

        public ServiceResponse<sonucType> tazminatDosyaSilmeKontrol(tazminatDosyaSilmeInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<sonucType, tazminatDosyaSilmeInputType>(input, "tazminatDosyaSilmeKontrol", "tazminatDosyaSilmeKontrol");
            return result;
        }

        public ServiceResponse<tazminatDosyaSonucType> tazminatDosyaSorgu(dosyaSorguInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<tazminatDosyaSonucType, dosyaSorguInputType>(input, "tazminatDosyaSorgu", "tazminatDosyaSorgu");
            return result;
        }

        public ServiceResponse<zeylSonucType> tazminatDosyaKurumGirisKontrol(dosyaKurumInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, dosyaKurumInputType>(input, "tazminatDosyaKurumGirisKontrol", "tazminatDosyaKurumGirisKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> tazminatDosyaGuncelleme(dosyaUpdateType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, dosyaUpdateType>(input, "tazminatDosyaGuncelleme", "tazminatDosyaGuncelleme");
            return result;
        }

        public ServiceResponse<zeylSonucType> tazminatDosyaKurumGiris(dosyaKurumInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, dosyaKurumInputType>(input, "tazminatDosyaKurumGiris", "tazminatDosyaKurumGiris");
            return result;
        }

        public ServiceResponse<sonucType> tazminatDosyaSilme(tazminatDosyaSilmeInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<sonucType, tazminatDosyaSilmeInputType>(input, "tazminatDosyaSilme", "tazminatDosyaSilme");
            return result;
        }

        public ServiceResponse<sonucType> policeSilmeKontrol(policeSilmeInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<sonucType, policeSilmeInputType>(input, "policeSilmeKontrol", "policeSilmeKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> tazminatFaturaGirisKontrol(dosyaFaturaInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, dosyaFaturaInputType>(input, "tazminatFaturaGirisKontrol", "tazminatFaturaGirisKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> tazminatFaturaGiris(dosyaFaturaInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, dosyaFaturaInputType>(input, "tazminatFaturaGiris", "tazminatFaturaGiris");
            return result;
        }

        public ServiceResponse<zeylSonucType> tazminatOdemeKontrol(dosyaTazminatOdemeInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, dosyaTazminatOdemeInputType>(input, "tazminatOdemeKontrol", "tazminatOdemeKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> tazminatDosyaGuncellemeKontrol(dosyaUpdateType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, dosyaUpdateType>(input, "tazminatDosyaGuncellemeKontrol", "tazminatDosyaGuncellemeKontrol");
            return result;
        }

        public ServiceResponse<zeylSonucType> tazminatOdeme(dosyaTazminatOdemeInputType input, Int64 CompanyId)
        {
            this.CompanyId = CompanyId;
            var result = GenerateXmlAndCallService<zeylSonucType, dosyaTazminatOdemeInputType>(input, "tazminatOdeme", "tazminatOdeme");
            return result;
        }
        #endregion

        #region Private functions

        private ServiceResponse<TOut> GenerateXmlAndCallService<TOut, TIn>(TIn classObj, string complexTypeName, string elementName, bool usePrefix = false)
        {
            string Username = GetSgmUserName(CompanyId);
            string Password = GetSgmPassword(CompanyId);
            if (Username.IsNull() || Password.IsNull())
            {
                throw new Exception("");
            }
            ServiceResponse <TOut> returnObject = new ServiceResponse<TOut>();
            returnObject.Data = Activator.CreateInstance<TOut>();

            if (classObj == null) return returnObject;

            #region Generate Soap XML 

            string generatedXml = string.Empty;

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(TIn));
            XmlDocument soapEnvelopeXmlDoc = new XmlDocument();

            using (StringWriter stringWriter = new StringWriter())
            {
                using (var xmlWriter = new CustomDateTimeXmlWriter(stringWriter))
                {
                    xmlSerializer.Serialize(xmlWriter, classObj);
                    generatedXml = stringWriter.ToString().Replace("<sigortaliList>", "").Replace("</sigortaliList>", "").Replace("<sigortaliTeminatInputTypeListNotXML>", "").Replace("</sigortaliTeminatInputTypeListNotXML>", "");

                    soapEnvelopeXmlDoc.LoadXml(generatedXml);
                    XmlNodeList elemList = soapEnvelopeXmlDoc.GetElementsByTagName(typeof(TIn).Name.ToString());

                    generatedXml = SagmerSoapEnvelopeTemp.Replace("{ComplexTypeName}", complexTypeName)
                                                         .Replace("{Prefix}", usePrefix ? "pts:" : "")
                                                         .Replace("{ElementName}", elementName)
                                                         .Replace("{InputFields}", elemList.Item(0).InnerXml)
                                                         .Replace("{Username}", Username)
                                                         .Replace("{Password}", Password);

                    soapEnvelopeXmlDoc.LoadXml(generatedXml);

                }
            }

            #endregion

            //Call service
            string responseXml = CallService(soapEnvelopeXmlDoc);

            IntegrationLog log = new IntegrationLog
            {
                WsName = "SAGMER RESPONSE",
                WsMethod = "SAGMER RESPONSE",
                Request= $"<![CDATA[{generatedXml.ToString()}]]>",
                Response = $"<![CDATA[{responseXml.ToString()}]]>",
                WsRequestDate = DateTime.Now,
            };
            log.HttpStatus = ((int)HttpStatusCode.Continue).ToString();
            log.Type = ((int)LogType.INCOMING).ToString();
            log.ResponseStatusCode = "100";
            log.ResponseStatusDesc = "RESPONSE";
            IntegrationLogHelper.AddToLog(log);

            //Parse xml result
            try
            {
                returnObject.Data = XmlHelper.ParseResponseXml<TOut>(responseXml, "return");
                PropertyInfo propInfo = returnObject.Data.GetType().GetProperty("islemBasarili");
                propInfo.SetValue(returnObject.Data, true, null);
            }
            catch (Exception ex)
            {
                SbmServiceException soapEx = GetErrorDetail(responseXml);
                PropertyInfo propInfo = returnObject.Data.GetType().GetProperty("islemBasarili");
                propInfo.SetValue(returnObject.Data, false, null);
                returnObject.ServiceException = soapEx;
            }

            return returnObject;
        }

        private string GetSgmUserName(long companyId)
        {
            string username = "";
            try
            {
                if (companyId == 95 || companyId == 10)
                {
                    username = "hdisg";
                }
                else
                {
                    var companyParameter = new GenericRepository<V_CompanyParameter>().FindBy("COMPANY_ID=:companyId AND SYSTEM_TYPE=:systemType AND KEY IN ('SAGMER_USER','SAGMER_CODE')", orderby: "", parameters: new { companyId, systemType = new Dapper.DbString { Value = ((int)SystemType.SAGMER).ToString(), Length = 3 } });
                    if (companyParameter.Count > 0)
                    {
                        string code = "", user = "";
                        foreach (var item in companyParameter)
                        {
                            if (item.Key == "SAGMER_USER")
                                user = item.Value;

                            else if (item.Key == "SAGMER_CODE")
                                code = item.Value;
                        }

                        if (!code.IsNull() && !user.IsNull())
                            username = code + "|" + user;
                    }
                }
            }
            catch (Exception e) { }
            return username;
        }

        private string GetSgmPassword(long companyId)
        {
            string password = "";
            try
            {
                var companyParameter = new GenericRepository<V_CompanyParameter>().FindBy("COMPANY_ID=:companyId AND SYSTEM_TYPE=:systemType AND KEY ='SAGMER_PW'", orderby: "", parameters: new { companyId, systemType = new Dapper.DbString { Value = ((int)SystemType.SAGMER).ToString(), Length = 3 } }).FirstOrDefault();
                if (companyParameter != null)
                {
                    password = companyParameter.Value;
                }
            }
            catch (Exception e) { }
            return password;
        }


        private SbmServiceException GetErrorDetail(string responseXml)
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(responseXml);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xdoc.NameTable);
            nsmgr.AddNamespace("ns0", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("ns1", "http://www.w3.org/2003/05/soap-envelope");

            XmlNodeList xNodelst = xdoc.DocumentElement.SelectNodes("//ns0:Fault", nsmgr);

            var result = new SbmServiceException();

            foreach (XmlNode xn in xNodelst)
            {
                result.code = xn["faultcode"].InnerText;
                result.message = xn["faultstring"].InnerText;
                result.failureList = new List<failure>();

                XmlNodeList failureList = xn.SelectNodes("//failure");

                foreach (XmlNode fNode in failureList)
                {
                    result.failureList.Add(new failure()
                    {
                        code = fNode["code"].InnerText,
                        message = fNode["message"].InnerText
                    });
                }
            }

            return result;
        }


        #endregion

    }

}
