﻿using Protein.Common.Extensions;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.Common.Lib.SBM;
using Protein.Data.ExternalServices.Log;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;
using static Protein.Data.ExternalServices.SagmerOnlineService.Enums;
using static Protein.Data.ExternalServices.SagmerOnlineService.InputOutputTypes;

namespace Protein.Data.ExternalServices.SagmerOnlineService
{
    public class SgmServiceClient
    {
        #region Create Generic Log
        private void CreateFailedLog(IntegrationLog log, string responseXml, string desc)
        {
            log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
            log.Type = ((int)LogType.FAILED).ToString();
            log.Response = responseXml;
            log.ResponseStatusCode = "0";
            log.ResponseStatusDesc = desc != null ? string.Join(" - ", desc) : "HATA";
            log.WsResponseDate = DateTime.Now;
            IntegrationLogHelper.AddToLog(log);
        }
        private void CreateOKLog(IntegrationLog log, string responseXml)
        {
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.Type = ((int)LogType.COMPLETED).ToString();
            log.Response = responseXml;
            log.ResponseStatusCode = "200";
            log.ResponseStatusDesc = "OK";
            log.WsResponseDate = DateTime.Now;
            IntegrationLogHelper.AddToLog(log);
        }
        private void CreateIncomingLog(IntegrationLog log)
        {
            log.HttpStatus = ((int)HttpStatusCode.Continue).ToString();
            log.Type = ((int)LogType.INCOMING).ToString();
            log.ResponseStatusCode = "100";
            log.ResponseStatusDesc = "INCOMING";
            IntegrationLogHelper.AddToLog(log);
        }
        private void CreateWaitingLog(IntegrationLog log)
        {
            log.HttpStatus = ((int)HttpStatusCode.Continue).ToString();
            log.Type = ((int)LogType.WAITING).ToString();
            log.ResponseStatusCode = "100";
            log.ResponseStatusDesc = "WAITING";
            IntegrationLogHelper.AddToLog(log);
        }
        #endregion

        #region SBM Health Policy Service
        //Poliçe Kayıt
        public SbmServiceRes SbmPolicyCreate(SbmPolicyCreateReq req, Enums.SagmerServiceType ServiceType)
        {
            SbmServiceRes response = new SbmServiceRes();
            response.FailedErrors = new List<failure>();
            var currentMethod = MethodBase.GetCurrentMethod();
            ServiceClient serviceClient = new ServiceClient();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                SagmerHelper sagmerHelper = new SagmerHelper();
                if (!sagmerHelper.IsSagmerSendViaPolicy(req.PolicyId))
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Bu Poliçenin Sagmere Gönderilme Zorunluluğu Bulunmamaktadır. Altürününü Kontrol Ediniz." });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }

                Endorsement firstEndorsement = new GenericRepository<Endorsement>().FindBy("POLICY_ID=:policyId AND TYPE=:type", orderby: "", parameters: new { policyId = req.PolicyId, type = new Dapper.DbString { Value = ((int)EndorsementType.BASLANGIC).ToString(), Length = 3 } }).FirstOrDefault();
                if (firstEndorsement == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Başlangıç Zeyil Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region GetPolicyData
                V_SgmPolicy sgmPolicy = new GenericRepository<V_SgmPolicy>().FindBy($"POLICY_ID = {req.PolicyId}", fetchDeletedRows: true, fetchHistoricRows: true, orderby: "").FirstOrDefault();
                if (sgmPolicy == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Poliçe Detayı Bulunamadı" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                log.PolicyId = req.PolicyId;
                log.COMPANY_ID = long.Parse(sgmPolicy.CompanyId);
                #endregion

                #region GetInsuredData
                V_SgmIngressEndorsement sgmEndors = new GenericRepository<V_SgmIngressEndorsement>().FindBy($" ENDORSEMENT_ID = {firstEndorsement.Id} AND POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                List<V_SgmIngressEndorsInsured> sgmEndorsInsured = new GenericRepository<V_SgmIngressEndorsInsured>().FindBy($" ENDORSEMENT_ID = {firstEndorsement.Id} AND POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                if (sgmEndors == null || sgmEndorsInsured == null || sgmEndorsInsured.Count < 1)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Sigortalı Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region FillPolicyData
                policeInputType input = new policeInputType();

                input.branshKod = sgmPolicy.BranshCode.IsInt64() ? long.Parse(sgmPolicy.BranshCode) : 0;
                input.dovizCinsi = sgmPolicy.DovizCinsi;
                input.endirektSigortaSirketKod = sgmPolicy.EndirektSigortaSirketKod;
                input.eskiPoliceNo = sgmPolicy.EskiPoliceNo;
                input.eskiYenilemeNo = !sgmPolicy.EskiYenilemeNo.IsInt() ? null : (int?)int.Parse(sgmPolicy.EskiYenilemeNo);
                input.ilKod = sgmPolicy.IlKod;
                input.odemeKod = !sgmPolicy.OdemeKod.IsInt64() ? 0 : long.Parse(sgmPolicy.OdemeKod);
                input.otorizasyonKod = sgmPolicy.OtorizasyonKod;
                input.policeBaslamaTarihi = sgmPolicy.PoliceBaslamaTarihi.IsDateTime() ? DateTime.Parse(sgmPolicy.PoliceBaslamaTarihi) : throw new Exception("PoliceBaslamaTarihi Boş!");
                input.policeBitisTarihi = sgmPolicy.PoliceBitisTarihi.IsDateTime() ? DateTime.Parse(sgmPolicy.PoliceBitisTarihi) : throw new Exception("PoliceBitisTarihi Boş!");
                input.policeBrutPrimi = sgmPolicy.PoliceBrutPrimi.IsNumeric() ? decimal.Parse(sgmPolicy.PoliceBrutPrimi) : 0;
                input.policeNetPrimi = sgmPolicy.PoliceNetPrimi.IsNumeric() ? decimal.Parse(sgmPolicy.PoliceNetPrimi) : 0;
                input.policeNo = sgmPolicy.PoliceNo;
                input.policeTanzimTarihi = !sgmPolicy.PoliceTanzimTarihi.IsDateTime() ? null : (DateTime?)DateTime.Parse(sgmPolicy.PoliceTanzimTarihi);
                input.policeTip = sgmPolicy.PoliceTip.IsInt() ? (sgmPolicy.PoliceTip == ((int)PolicyType.FERDI).ToString() ? policeTipiType.F : policeTipiType.G) : throw new Exception("PoliceTip Boş!");

                input.sigortaEttirenType = new sigortaEttirenInputType();
                input.sigortaEttirenType.kimlikTipKod = sgmPolicy.SgtKimlikTipKod;
                if (input.sigortaEttirenType.kimlikTipKod != "1" && input.sigortaEttirenType.kimlikTipKod != "2" && input.sigortaEttirenType.kimlikTipKod != "4")
                {
                    input.sigortaEttirenType.ad = sgmPolicy.SgtAd;
                    input.sigortaEttirenType.soyad = sgmPolicy.SgtSoyad;
                    input.sigortaEttirenType.adres = sgmPolicy.SgtAdres;
                    input.sigortaEttirenType.babaAdi = sgmPolicy.SgtBabaAdi;
                    input.sigortaEttirenType.cinsiyet = !sgmPolicy.SgtCinsiyet.IsInt() ? null : (cinsiyet?)Enum.Parse(typeof(cinsiyet), sgmPolicy.SgtCinsiyet);
                    input.sigortaEttirenType.dogumTarihi = !sgmPolicy.SgtDogumTarihi.IsDateTime() ? null : (DateTime?)DateTime.Parse(sgmPolicy.SgtDogumTarihi);
                    input.sigortaEttirenType.dogumYeri = sgmPolicy.SgtDogumYeri;
                }
                input.sigortaEttirenType.kimlikNo = sgmPolicy.SgtKimlikNo;
                input.sigortaEttirenType.sigortaEttirenUyruk = sgmPolicy.SgtSigortaEttirenUyruk == null ? null : (uyrukTuru?)(uyrukTuru)Enum.Parse(typeof(uyrukTuru), sgmPolicy.SgtSigortaEttirenUyruk);

                DateTime? kurulusTarihi = null;
                if (!string.IsNullOrEmpty(sgmPolicy.SgtKurulusTarihi))
                    kurulusTarihi = DateTime.Parse(sgmPolicy.SgtKurulusTarihi);

                input.sigortaEttirenType.kurulusTarihi = kurulusTarihi;
                input.sigortaEttirenType.kurulusYeri = sgmPolicy.SgtKurulusYeri;
                input.sigortaEttirenType.turKod = sgmPolicy.SgtTurKod != null ? (turKod)Enum.Parse(typeof(turKod), sgmPolicy.SgtTurKod) : throw new Exception("SgtTurKod Boş");
                if (input.sigortaEttirenType.kimlikTipKod != "1")
                {
                    input.sigortaEttirenType.ulkeKodu = !sgmPolicy.SgtUlkeKodu.IsInt() ? null : (int?)int.Parse(sgmPolicy.SgtUlkeKodu);
                }
                input.sigortaSirketKod = sgmPolicy.SigortaSirketKod;
                input.tarifeID = !sgmPolicy.TarifeId.IsInt64() ? 0 : long.Parse(sgmPolicy.TarifeId);
                input.uretimKaynakKod = !sgmPolicy.UretimKaynakKod.IsInt64() ? 0 : long.Parse(sgmPolicy.UretimKaynakKod);
                input.uretimKaynakKurumKod = sgmPolicy.UretimKaynakKurumKod;
                input.vadeSayisi = !sgmPolicy.VadeSayisi.IsInt() ? 0 : int.Parse(sgmPolicy.VadeSayisi);
                input.yenilemeNo = !sgmPolicy.YenilemeNo.IsInt() ? 0 : int.Parse(sgmPolicy.YenilemeNo);
                input.yeniYenilemeGecis = sgmPolicy.YeniYenilemeGecis;
                input.zeylNo = !sgmPolicy.ZeylNo.IsInt() ? 0 : int.Parse(sgmPolicy.ZeylNo);
                #endregion

                long sbmNo = 0;
                if (ServiceType == Enums.SagmerServiceType.KONTROL)
                {
                    var responseSbmPolice = serviceClient.policeKontrol(input, long.Parse(sgmPolicy.CompanyId));
                    if (!responseSbmPolice.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmPolice.ServiceException.failureList;
                        log.Request = input.ToXML(true);

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                    log.Request = input.ToXML(true);
                    CreateWaitingLog(log);
                }
                else
                {
                    var responseSbmPolice = serviceClient.police(input, long.Parse(sgmPolicy.CompanyId));
                    if (!responseSbmPolice.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmPolice.ServiceException.failureList;
                        log.Request = sgmPolicy.ToXML(true);

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                    log.Request = input.ToXML(true);
                    CreateWaitingLog(log);

                    Policy policy = new GenericRepository<Policy>().FindById(req.PolicyId);
                    
                    policy.SbmNo = responseSbmPolice.Data.sbmSagmerNo.ToString();
                    new GenericRepository<Policy>().Insert(policy);

                    sbmNo = responseSbmPolice.Data.sbmSagmerNo;
                }

                #region FillInsuredData
                sigortaliGirisInputType inputInsured = new sigortaliGirisInputType();

                inputInsured.zeylTuru = sgmEndors.ZeylTuru.IsInt64() ? long.Parse(sgmEndors.ZeylTuru) : 0;
                inputInsured.zeylTanzimTarihi = sgmEndors.ZeylTanzimTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylTanzimTarihi) : DateTime.Now;
                inputInsured.zeylNo = sgmEndors.ZeylNo.IsInt() ? int.Parse(sgmEndors.ZeylNo) : 0;
                inputInsured.sbmSagmerNo = sbmNo;

                inputInsured.zeylBaslangicTarihi = sgmEndors.ZeylBaslangicTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylBaslangicTarihi) : DateTime.Now;
                inputInsured.yenilemeNo = sgmEndors.YenilemeNo.IsInt() ? int.Parse(sgmEndors.YenilemeNo) : 0;
                inputInsured.sigortaSirketKod = sgmEndors.SigortaSirketKod;
                inputInsured.otorizasyonKod = sgmEndors.OtorizasyonKod;
                inputInsured.policeNo = sgmEndors.PoliceNo;
                List<sigortali> lstInsured = new List<sigortali>();
                foreach (V_SgmIngressEndorsInsured item in sgmEndorsInsured)
                {
                    List<V_SgmIngressInsCover> sgmInsuredCover = new GenericRepository<V_SgmIngressInsCover>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {req.PolicyId} and ENDORSEMENT_ID = {firstEndorsement.Id}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                    V_SgmIngressInsHealthInfo healthInfo = new GenericRepository<V_SgmIngressInsHealthInfo>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {req.PolicyId} and ENDORSEMENT_ID = {firstEndorsement.Id}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                    List<V_SgmIngressInsHealthDet> healthInfoDet = new GenericRepository<V_SgmIngressInsHealthDet>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {req.PolicyId} and ENDORSEMENT_ID = {firstEndorsement.Id}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                    List<V_SgmIngressExtraPremium> ExtraPrm = new GenericRepository<V_SgmIngressExtraPremium>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {req.PolicyId} and ENDORSEMENT_ID = {firstEndorsement.Id}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                    List<V_SgmIngressInsExemption> sgmExemption = new GenericRepository<V_SgmIngressInsExemption>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {req.PolicyId} and ENDORSEMENT_ID = {firstEndorsement.Id}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

                    sigortaliSaglikBilgileriInputType sigSagBilg = new sigortaliSaglikBilgileriInputType();
                    List<sigortaliSaglikBilgileriDetayInputType> sigSagBilgDetay = new List<sigortaliSaglikBilgileriDetayInputType>();

                    List<sigortaliIndirimEkprimInputType> sigIndEkp = new List<sigortaliIndirimEkprimInputType>();

                    List<sigortaliTeminatInputTypeList> sigTem = new List<sigortaliTeminatInputTypeList>();
                    List<sigortaliUygulamaInputType> sigUyg = new List<sigortaliUygulamaInputType>();

                    if (healthInfo != null)
                    {
                        sigSagBilg.alkolKullaniyorMu = (Enums.evetHayirType)int.Parse(healthInfo.AlkolKullaniliyorMu);
                        sigSagBilg.alkolKullanmaSuresi = healthInfo.AlkolKullanmaSuresi;
                        sigSagBilg.alkolPeriyotTip = (Enums.aliskanlikPeriyodType)int.Parse(healthInfo.AlkolPeriyotTip);
                        sigSagBilg.boy = int.Parse(healthInfo.Boy);
                        sigSagBilg.kilo = int.Parse(healthInfo.Kilo);
                        sigSagBilg.periyottakiAlkolMiktari = healthInfo.PeriyottakiAlkolMiktari;
                        sigSagBilg.periyottakiSigaraAdedi = healthInfo.PeriyottakiSigaraAdedi;
                        sigSagBilg.sigaraIciyorMu = (Enums.evetHayirType)int.Parse(healthInfo.SigaraIciyorMu);
                        sigSagBilg.sigaraIcmeSuresi = healthInfo.SigaraIcmeSuresi;
                        sigSagBilg.sigaraPeriyotTip = (Enums.aliskanlikPeriyodType)int.Parse(healthInfo.SigaraperiyotTip);
                    }

                    foreach (V_SgmIngressInsHealthDet itemHealthDet in healthInfoDet)
                    {
                        sigSagBilgDetay.Add(new sigortaliSaglikBilgileriDetayInputType
                        {
                            ameliyatVarmi = itemHealthDet.AmeliyatVarMi,
                            hastalikAciklamasi = itemHealthDet.HastalikAciklamasi,
                            hastalikAdi = itemHealthDet.HastalikAdi,
                            hastalikKod = itemHealthDet.HastalikKod,
                            hastalikUygulamaKod = itemHealthDet.HastalikUygulamaKod.IsInt64() ? long.Parse(itemHealthDet.HastalikUygulamaKod) : 0,
                            sonDurum = itemHealthDet.SonDurum,
                            sureAy = itemHealthDet.SureAy.IsInt() ? int.Parse(itemHealthDet.SureAy) : 0,
                            sureYil = itemHealthDet.SureYil.IsInt() ? int.Parse(itemHealthDet.SureYil) : 0,
                            uygulamaAciklamasi = itemHealthDet.UygulamaAciklamasi
                        });
                    }
                    sigSagBilg.saglikDetayBilgileri = sigSagBilgDetay.ToArray();
                    foreach (V_SgmIngressExtraPremium itemExtraPrm in ExtraPrm)
                    {
                        sigIndEkp.Add(new sigortaliIndirimEkprimInputType
                        {
                            aciklama = itemExtraPrm.Aciklama,
                            indirimEkprimDegeri = itemExtraPrm.IndirimEkPrimDegeri.IsNumeric() ? decimal.Parse(itemExtraPrm.IndirimEkPrimDegeri) : 0,
                            indirimEkprimKod = itemExtraPrm.IndirimEkPrimKod.IsInt64() ? long.Parse(itemExtraPrm.IndirimEkPrimKod) : 0,
                            kayitNo = itemExtraPrm.KayitNo.IsInt64() ? long.Parse(itemExtraPrm.KayitNo) : 0,
                            tutarMiOranMi = itemExtraPrm.TutarMiOranMi
                        });
                    }
                    foreach (V_SgmIngressInsCover itemInsCover in sgmInsuredCover)
                    {
                        sigortaliTeminatInputTypeList sigortaliTeminatInputTypeList = new sigortaliTeminatInputTypeList
                        {
                            aso = itemInsCover.Aso.IsInt() ? (Enums.evetHayirType)int.Parse(itemInsCover.Aso) : evetHayirType.H,
                            cografiKapsamKodu = itemInsCover.CografiKapsamKodu.IsInt64() ? long.Parse(itemInsCover.CografiKapsamKodu) : 0,
                            donemBaslangicTarihi = itemInsCover.DonemBaslangicTarihi.IsDateTime() ? DateTime.Parse(itemInsCover.DonemBaslangicTarihi) : DateTime.Now,
                            donemBitisTarihi = itemInsCover.DonemBitisTarihi.IsDateTime() ? DateTime.Parse(itemInsCover.DonemBitisTarihi) : DateTime.Now,
                            donemKodu = itemInsCover.DonemKodu.IsInt() ? int.Parse(itemInsCover.DonemKodu) : 0,
                            dovizCinsi = itemInsCover.DovizCinsi,
                            teminatAdedi = itemInsCover.TeminatAdedi.IsInt() ? int.Parse(itemInsCover.TeminatAdedi) : 0,
                            teminatKodu = itemInsCover.TeminatKodu.IsInt64() ? long.Parse(itemInsCover.TeminatKodu) : 0,
                            teminatLimiti = itemInsCover.TeminatLimiti.IsNumeric() ? decimal.Parse(itemInsCover.TeminatLimiti) : 0,
                            teminatLimitKodu = itemInsCover.TeminatLimitKodu.IsInt64() ? long.Parse(itemInsCover.TeminatLimitKodu) : 0,
                            teminatNetPrim = itemInsCover.TeminatNetPrim.IsNumeric() ? decimal.Parse(itemInsCover.TeminatNetPrim) : 0,
                            teminatOdemeYuzdesi = itemInsCover.TeminatOdemeYuzdesi.IsNumeric() ? decimal.Parse(itemInsCover.TeminatOdemeYuzdesi) : 0,
                            teminatVaryasyonKodu = itemInsCover.TeminatVaryasyonKodu.IsInt() ? int.Parse(itemInsCover.TeminatVaryasyonKodu) : 0,
                            toplamDonemSayisi = itemInsCover.ToplamDonemSayisi.IsInt() ? int.Parse(itemInsCover.ToplamDonemSayisi) : 0,
                        };
                        if (itemInsCover.TeminatMuafiyetKodu.IsInt64() && long.Parse(itemInsCover.TeminatMuafiyetKodu) > 0)
                        {
                            sigortaliTeminatInputTypeList.teminatMuafiyeti = itemInsCover.TeminatMuafiyeti.IsNumeric() ? itemInsCover.TeminatMuafiyeti : "";
                            sigortaliTeminatInputTypeList.teminatMuafiyetKodu = itemInsCover.TeminatMuafiyetKodu;
                        }
                        sigTem.Add(sigortaliTeminatInputTypeList);

                    }
                    foreach (V_SgmIngressInsExemption itemExemption in sgmExemption)
                    {
                        sigUyg.Add(new sigortaliUygulamaInputType
                        {
                            aciklama = itemExemption.Aciklama,
                            hastalikKod = itemExemption.HastalikKod,
                            kayitNo = itemExemption.KayitNo.IsInt64() ? long.Parse(itemExemption.KayitNo) : 0,
                            muafiyetTutarLimiti = itemExemption.MuafiyetTutarLimiti.IsNumeric() ? decimal.Parse(itemExemption.MuafiyetTutarLimiti) : 0,
                            sigortaliUygulamaKod = itemExemption.SigortaliUygulamaKod.IsInt64() ? long.Parse(itemExemption.SigortaliUygulamaKod) : 0,
                        });
                    }


                    var sigInput = new sigortali
                    {
                        aileBagNo = item.AileBagNo,
                        bireySiraNo = item.BireySiraNo,
                        bireyTipKod = item.BireyTipKod.IsInt64() ? long.Parse(item.BireyTipKod) : 0,
                        brutPrim = item.BrutPrim.IsNumeric() ? decimal.Parse(item.BrutPrim) : 0,
                        eskiPoliceNo = item.EskiPoliceNo,
                        eskiYenilemeNo = item.EskiYenilemeNo.IsInt() ? int.Parse(item.EskiYenilemeNo) : 0,
                        ilkOzelSigortalilikTarihi = DateTime.Parse(item.IlkOzelSigortalilikTarihi),
                        ilkOzelSirketKod = item.IlkOzelSirketKod,
                        kimlikNo = item.KimlikNo,
                        kimlikTipKod = item.KimlikTipKod,
                        meslekKod = item.MeslekKod.IsInt64() ? (long?)long.Parse(item.MeslekKod) : null,
                        netPrim = item.NetPrim.IsNumeric() ? decimal.Parse(item.NetPrim) : 0,
                        sigortaliUyruk = item.SigortaliUyruk == "TC" ? uyrukTuru.TC : uyrukTuru.YABANCI,// (Enums.uyrukTuru)int.Parse(item.SigortaliUyruk),
                        urunPlanKod = item.UrunPlanKod.IsInt64() ? long.Parse(item.UrunPlanKod) : 0,
                        yasadigiIlceKod = item.YasadigiIlceKod,
                        yasadigiIlKod = "034",// item.YasadigiIlKod,
                        saglikBilgileriInputType = null,
                        sigortaliIndirimEkprimInputTypeList = sigIndEkp.ToArray(),
                        sigortaliTeminatInputTypeListNotXML = sigTem.ToArray(),
                        sigortaliUygulamaList = sigUyg.ToArray()
                    };

                    if (item.KimlikTipKod != "1" && item.KimlikTipKod != "2" && item.KimlikTipKod != "4")
                    {
                        sigInput.ulkeKodu = item.UlkeKodu.IsInt() ? (int?)int.Parse(item.UlkeKodu) : null;
                        sigInput.adres = item.Adres;
                        sigInput.babaAdi = item.BabaAdi;
                        sigInput.sigortaliAd = item.SigortaliAd;
                        sigInput.sigortaliSoyad = item.SigortaliSoyad;
                        sigInput.cinsiyet = (Enums.cinsiyet)int.Parse(item.Cinsiyet);
                        sigInput.dogumTarihi = DateTime.Parse(item.DogumTarihi);
                        sigInput.dogumYeri = item.DogumYeri;
                    }

                    lstInsured.Add(sigInput);
                    inputInsured.sigortaliList = lstInsured.ToArray();

                    log.Request = inputInsured.ToXML(true);

                    if (ServiceType == Enums.SagmerServiceType.KONTROL)
                    {
                        var responseSbmInsured = serviceClient.sigortaliGirisZeyliKontrol(inputInsured, long.Parse(sgmPolicy.CompanyId));
                        if (!responseSbmInsured.Data.islemBasarili)
                        {
                            bool isSgmOk = false;
                            if (responseSbmInsured.ServiceException.failureList.Count == 1)
                            {
                                foreach (var itemm in responseSbmInsured.ServiceException.failureList)
                                {
                                    if (itemm.code == "BRV-SGM-00001")
                                    {
                                        isSgmOk = true;
                                    }
                                }
                            }

                            if (!isSgmOk)
                            {
                                response.ResultCode = 0;
                                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                                response.FailedErrors = responseSbmInsured.ServiceException.failureList;
                                log.Request = inputInsured.ToXML(true);

                                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                                return response;
                            }
                        }
                    }
                    else
                    {
                        var responseSbmInsured = serviceClient.sigortaliGirisZeyli(inputInsured, long.Parse(sgmPolicy.CompanyId));
                        SpResponse spResponse = new SpResponse();
                        if (!responseSbmInsured.Data.islemBasarili)
                        {
                            response.ResultCode = 0;
                            response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                            response.FailedErrors = responseSbmInsured.ServiceException.failureList;
                            log.Request = inputInsured.ToXML(true);
                            firstEndorsement.SbmStatus = ((int)SagmerStatus.BASARISIZ).ToString();
                             spResponse = new GenericRepository<Endorsement>().Update(firstEndorsement);
                            CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                            return response;

                        }

                        firstEndorsement.SbmStatus = ((int)SagmerStatus.BASARILI).ToString();

                         spResponse = new GenericRepository<Endorsement>().Update(firstEndorsement);
                    }

                    CreateWaitingLog(log);
                    lstInsured = new List<sigortali>();
                }
                #endregion
                
                #region OK
                response.ResultCode = 1;
                response.ResultDesc = "İşlem Başarılı";
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.ResultCode = 0;
                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = ex.Message });

                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                #endregion
            }
            return response;
        }

        //Sigortalı Giriş Zeyili
        public SbmServiceRes SbmInsuredEntranceEndorsement(SbmInsuredEntranceReq req, Enums.SagmerServiceType ServiceType)
        {
            SbmServiceRes response = new SbmServiceRes();
            response.FailedErrors = new List<failure>();

            var currentMethod = MethodBase.GetCurrentMethod();
            ServiceClient serviceClient = new ServiceClient();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                SagmerHelper sagmerHelper = new SagmerHelper();
                if (!sagmerHelper.IsSagmerSendViaPolicy(req.PolicyId))
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Bu Poliçenin Sagmere Gönderilme Zorunluluğu Bulunmamaktadır. Altürününü Kontrol Ediniz." });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }

                Endorsement entranceEndorsement = new GenericRepository<Endorsement>().FindBy("ID=:endorsementId", orderby: "", parameters: new { endorsementId = req.EndorsementId }).FirstOrDefault();
                if (entranceEndorsement == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Zeyil Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region GetInsuredData
                V_SgmIngressEndorsement sgmEndors = new GenericRepository<V_SgmIngressEndorsement>().FindBy($" ENDORSEMENT_ID = {entranceEndorsement.Id} AND POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                List<V_SgmIngressEndorsInsured> sgmEndorsInsured = new GenericRepository<V_SgmIngressEndorsInsured>().FindBy($" ENDORSEMENT_ID = {entranceEndorsement.Id} AND POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                if (sgmEndors == null || sgmEndorsInsured == null || sgmEndorsInsured.Count < 1)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Sigortalı Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                log.PolicyId = req.PolicyId;
                log.COMPANY_ID = long.Parse(sgmEndors.CompanyId);
                #endregion

                #region FillInsuredData
                sigortaliGirisInputType inputInsured = new sigortaliGirisInputType();

                inputInsured.zeylTuru = sgmEndors.ZeylTuru.IsInt64() ? long.Parse(sgmEndors.ZeylTuru) : 0;
                inputInsured.zeylTanzimTarihi = sgmEndors.ZeylTanzimTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylTanzimTarihi) : DateTime.Now;
                inputInsured.zeylNo = sgmEndors.ZeylNo.IsInt() ? int.Parse(sgmEndors.ZeylNo) : 0;

                inputInsured.zeylBaslangicTarihi = sgmEndors.ZeylBaslangicTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylBaslangicTarihi) : DateTime.Now;
                inputInsured.yenilemeNo = sgmEndors.YenilemeNo.IsInt() ? int.Parse(sgmEndors.YenilemeNo) : 0;
                inputInsured.sigortaSirketKod = sgmEndors.SigortaSirketKod;
                inputInsured.sbmSagmerNo = sgmEndors.SbmSagmerNo.IsInt64() ? long.Parse(sgmEndors.SbmSagmerNo) : 0;
                inputInsured.otorizasyonKod = req.Authorization;
                inputInsured.policeNo = sgmEndors.PoliceNo;
                if (entranceEndorsement.Type != ((int)EndorsementType.BASLANGIC).ToString())
                {
                    //sgmEndors.PoliceNetPrimi = sgmEndors.PoliceBrutPrimi = "1.046,84";
                    inputInsured.zeylNetPrimi = sgmEndors.ZeylNetPrimi.IsNumeric() ? decimal.Parse(sgmEndors.ZeylNetPrimi) : 0;
                    inputInsured.zeylBrutPrimi = sgmEndors.ZeylBrutPrimi.IsNumeric() ? decimal.Parse(sgmEndors.ZeylBrutPrimi) : 0;
                    inputInsured.policeNetPrimi = sgmEndors.PoliceNetPrimi.IsNumeric() ? decimal.Parse(sgmEndors.PoliceNetPrimi) : 0;
                    inputInsured.policeBrutPrimi = sgmEndors.PoliceBrutPrimi.IsNumeric() ? decimal.Parse(sgmEndors.PoliceBrutPrimi) : 0;
                }
                List<sigortali> lstInsured = new List<sigortali>();
                foreach (V_SgmIngressEndorsInsured item in sgmEndorsInsured)
                {
                    List<V_SgmIngressInsCover> sgmInsuredCover = new GenericRepository<V_SgmIngressInsCover>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {req.PolicyId} and ENDORSEMENT_ID = {entranceEndorsement.Id}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                    V_SgmIngressInsHealthInfo healthInfo = new GenericRepository<V_SgmIngressInsHealthInfo>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {req.PolicyId} and ENDORSEMENT_ID = {entranceEndorsement.Id}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                    List<V_SgmIngressInsHealthDet> healthInfoDet = new GenericRepository<V_SgmIngressInsHealthDet>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {req.PolicyId} and ENDORSEMENT_ID = {entranceEndorsement.Id}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                    List<V_SgmIngressExtraPremium> ExtraPrm = new GenericRepository<V_SgmIngressExtraPremium>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {req.PolicyId} and ENDORSEMENT_ID = {entranceEndorsement.Id}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                    List<V_SgmIngressInsExemption> sgmExemption = new GenericRepository<V_SgmIngressInsExemption>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {req.PolicyId} and ENDORSEMENT_ID = {entranceEndorsement.Id}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

                    sigortaliSaglikBilgileriInputType sigSagBilg = new sigortaliSaglikBilgileriInputType();
                    List<sigortaliSaglikBilgileriDetayInputType> sigSagBilgDetay = new List<sigortaliSaglikBilgileriDetayInputType>();

                    List<sigortaliIndirimEkprimInputType> sigIndEkp = new List<sigortaliIndirimEkprimInputType>();

                    List<sigortaliTeminatInputTypeList> sigTem = new List<sigortaliTeminatInputTypeList>();
                    List<sigortaliUygulamaInputType> sigUyg = new List<sigortaliUygulamaInputType>();

                    if (healthInfo != null)
                    {
                        sigSagBilg.alkolKullaniyorMu = (Enums.evetHayirType)int.Parse(healthInfo.AlkolKullaniliyorMu);
                        sigSagBilg.alkolKullanmaSuresi = healthInfo.AlkolKullanmaSuresi;
                        sigSagBilg.alkolPeriyotTip = (Enums.aliskanlikPeriyodType)int.Parse(healthInfo.AlkolPeriyotTip);
                        sigSagBilg.boy = int.Parse(healthInfo.Boy);
                        sigSagBilg.kilo = int.Parse(healthInfo.Kilo);
                        sigSagBilg.periyottakiAlkolMiktari = healthInfo.PeriyottakiAlkolMiktari;
                        sigSagBilg.periyottakiSigaraAdedi = healthInfo.PeriyottakiSigaraAdedi;
                        sigSagBilg.sigaraIciyorMu = (Enums.evetHayirType)int.Parse(healthInfo.SigaraIciyorMu);
                        sigSagBilg.sigaraIcmeSuresi = healthInfo.SigaraIcmeSuresi;
                        sigSagBilg.sigaraPeriyotTip = (Enums.aliskanlikPeriyodType)int.Parse(healthInfo.SigaraperiyotTip);
                    }

                    foreach (V_SgmIngressInsHealthDet itemHealthDet in healthInfoDet)
                    {
                        sigSagBilgDetay.Add(new sigortaliSaglikBilgileriDetayInputType
                        {
                            ameliyatVarmi = itemHealthDet.AmeliyatVarMi,
                            hastalikAciklamasi = itemHealthDet.HastalikAciklamasi,
                            hastalikAdi = itemHealthDet.HastalikAdi,
                            hastalikKod = itemHealthDet.HastalikKod,
                            hastalikUygulamaKod = itemHealthDet.HastalikUygulamaKod.IsInt64() ? long.Parse(itemHealthDet.HastalikUygulamaKod) : 0,
                            sonDurum = itemHealthDet.SonDurum,
                            sureAy = itemHealthDet.SureAy.IsInt() ? int.Parse(itemHealthDet.SureAy) : 0,
                            sureYil = itemHealthDet.SureYil.IsInt() ? int.Parse(itemHealthDet.SureYil) : 0,
                            uygulamaAciklamasi = itemHealthDet.UygulamaAciklamasi
                        });
                    }
                    sigSagBilg.saglikDetayBilgileri = sigSagBilgDetay.ToArray();
                    foreach (V_SgmIngressExtraPremium itemExtraPrm in ExtraPrm)
                    {
                        sigIndEkp.Add(new sigortaliIndirimEkprimInputType
                        {
                            aciklama = itemExtraPrm.Aciklama,
                            indirimEkprimDegeri = itemExtraPrm.IndirimEkPrimDegeri.IsNumeric() ? decimal.Parse(itemExtraPrm.IndirimEkPrimDegeri) : 0,
                            indirimEkprimKod = itemExtraPrm.IndirimEkPrimKod.IsInt64() ? long.Parse(itemExtraPrm.IndirimEkPrimKod) : 0,
                            kayitNo = itemExtraPrm.KayitNo.IsInt64() ? long.Parse(itemExtraPrm.KayitNo) : 0,
                            tutarMiOranMi = itemExtraPrm.TutarMiOranMi
                        });
                    }
                    foreach (V_SgmIngressInsCover itemInsCover in sgmInsuredCover)
                    {
                        sigortaliTeminatInputTypeList sigortaliTeminatInputTypeList = new sigortaliTeminatInputTypeList
                        {
                            aso = itemInsCover.Aso.IsInt() ? (Enums.evetHayirType)int.Parse(itemInsCover.Aso) : evetHayirType.H,
                            cografiKapsamKodu = itemInsCover.CografiKapsamKodu.IsInt64() ? long.Parse(itemInsCover.CografiKapsamKodu) : 0,
                            donemBaslangicTarihi = itemInsCover.DonemBaslangicTarihi.IsDateTime() ? DateTime.Parse(itemInsCover.DonemBaslangicTarihi) : DateTime.Now,
                            donemBitisTarihi = itemInsCover.DonemBitisTarihi.IsDateTime() ? DateTime.Parse(itemInsCover.DonemBitisTarihi) : DateTime.Now,
                            donemKodu = itemInsCover.DonemKodu.IsInt() ? int.Parse(itemInsCover.DonemKodu) : 0,
                            dovizCinsi = itemInsCover.DovizCinsi,
                            teminatAdedi = itemInsCover.TeminatAdedi.IsInt() ? int.Parse(itemInsCover.TeminatAdedi) : 0,
                            teminatKodu = itemInsCover.TeminatKodu.IsInt64() ? long.Parse(itemInsCover.TeminatKodu) : 0,
                            teminatLimiti = itemInsCover.TeminatLimiti.IsNumeric() ? decimal.Parse(itemInsCover.TeminatLimiti) : 0,
                            teminatLimitKodu = itemInsCover.TeminatLimitKodu.IsInt64() ? long.Parse(itemInsCover.TeminatLimitKodu) : 0,
                            teminatNetPrim = itemInsCover.TeminatNetPrim.IsNumeric() ? decimal.Parse(itemInsCover.TeminatNetPrim) : 0,
                            teminatOdemeYuzdesi = itemInsCover.TeminatOdemeYuzdesi.IsNumeric() ? decimal.Parse(itemInsCover.TeminatOdemeYuzdesi) : 0,
                            teminatVaryasyonKodu = itemInsCover.TeminatVaryasyonKodu.IsInt() ? int.Parse(itemInsCover.TeminatVaryasyonKodu) : 0,
                            toplamDonemSayisi = itemInsCover.ToplamDonemSayisi.IsInt() ? int.Parse(itemInsCover.ToplamDonemSayisi) : 0,
                        };
                        if (itemInsCover.TeminatMuafiyetKodu.IsInt64() && long.Parse(itemInsCover.TeminatMuafiyetKodu) > 0)
                        {
                            sigortaliTeminatInputTypeList.teminatMuafiyeti = itemInsCover.TeminatMuafiyeti.IsNumeric() ? itemInsCover.TeminatMuafiyeti : "";
                            sigortaliTeminatInputTypeList.teminatMuafiyetKodu = itemInsCover.TeminatMuafiyetKodu;
                        }
                        sigTem.Add(sigortaliTeminatInputTypeList);

                    }
                    foreach (V_SgmIngressInsExemption itemExemption in sgmExemption)
                    {
                        sigUyg.Add(new sigortaliUygulamaInputType
                        {
                            aciklama = itemExemption.Aciklama,
                            hastalikKod = itemExemption.HastalikKod,
                            kayitNo = itemExemption.KayitNo.IsInt64() ? long.Parse(itemExemption.KayitNo) : 0,
                            muafiyetTutarLimiti = itemExemption.MuafiyetTutarLimiti.IsNumeric() ? decimal.Parse(itemExemption.MuafiyetTutarLimiti) : 0,
                            sigortaliUygulamaKod = itemExemption.SigortaliUygulamaKod.IsInt64() ? long.Parse(itemExemption.SigortaliUygulamaKod) : 0,
                        });
                    }


                    var sigInput = new sigortali
                    {
                        aileBagNo = item.AileBagNo,
                        bireySiraNo = item.BireySiraNo,
                        bireyTipKod = item.BireyTipKod.IsInt64() ? long.Parse(item.BireyTipKod) : 0,
                        brutPrim = item.BrutPrim.IsNumeric() ? decimal.Parse(item.BrutPrim) : 0,
                        eskiPoliceNo = item.EskiPoliceNo,
                        eskiYenilemeNo = item.EskiYenilemeNo.IsInt() ? int.Parse(item.EskiYenilemeNo) : 0,
                        ilkOzelSigortalilikTarihi = DateTime.Parse(item.IlkOzelSigortalilikTarihi),
                        ilkOzelSirketKod = item.IlkOzelSirketKod,
                        kimlikNo = item.KimlikNo,
                        kimlikTipKod = item.KimlikTipKod,
                        meslekKod = item.MeslekKod.IsInt64() ? (long?)long.Parse(item.MeslekKod) : null,
                        netPrim = item.NetPrim.IsNumeric() ? decimal.Parse(item.NetPrim) : 0,
                        sigortaliUyruk = item.SigortaliUyruk == "TC" ? uyrukTuru.TC : uyrukTuru.YABANCI,// (Enums.uyrukTuru)int.Parse(item.SigortaliUyruk),
                        urunPlanKod = item.UrunPlanKod.IsInt64() ? long.Parse(item.UrunPlanKod) : 0,
                        yasadigiIlceKod = item.YasadigiIlceKod,
                        yasadigiIlKod = "034",//item.YasadigiIlKod,
                        saglikBilgileriInputType = null,
                        sigortaliIndirimEkprimInputTypeList = sigIndEkp.ToArray(),
                        sigortaliTeminatInputTypeListNotXML = sigTem.ToArray(),
                        sigortaliUygulamaList = sigUyg.ToArray()
                    };

                    if (item.KimlikTipKod != "1" && item.KimlikTipKod != "2" && item.KimlikTipKod != "4")
                    {
                        sigInput.ulkeKodu = item.UlkeKodu.IsInt() ? (int?)int.Parse(item.UlkeKodu) : null;
                        sigInput.adres = item.Adres;
                        sigInput.babaAdi = item.BabaAdi;
                        sigInput.sigortaliAd = item.SigortaliAd;
                        sigInput.sigortaliSoyad = item.SigortaliSoyad;
                        sigInput.cinsiyet = (Enums.cinsiyet)int.Parse(item.Cinsiyet);
                        sigInput.dogumTarihi = DateTime.Parse(item.DogumTarihi);
                        sigInput.dogumYeri = item.DogumYeri;
                    }

                    lstInsured.Add(sigInput);
                    inputInsured.sigortaliList = lstInsured.ToArray();
                    #region Operation Policy And Insured
                    //if (ServiceType == Enums.SagmerServiceType.KONTROL)
                    //{
                    //    var responseSbmInsured = serviceClient.sigortaliGirisZeyliKontrol(inputInsured, long.Parse(sgmEndors.CompanyId));
                    //    if (!responseSbmInsured.Data.islemBasarili)
                    //    {
                    //        response.ResultCode = 0;
                    //        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    //        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                    //        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    //        return response;
                    //    }
                    //}
                    //else
                    //{
                    //    var responseSbmInsured = serviceClient.sigortaliGirisZeyli(inputInsured, long.Parse(sgmEndors.CompanyId));
                    //    if (!responseSbmInsured.Data.islemBasarili)
                    //    {
                    //        response.ResultCode = 0;
                    //        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    //        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                    //        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    //        return response;
                    //    }
                    //}
                    #endregion
                    inputInsured.sigortaliList = lstInsured.ToArray();

                    log.Request = inputInsured.ToXML(true);

                    if (ServiceType == Enums.SagmerServiceType.KONTROL)
                    {
                        var responseSbmInsured = serviceClient.sigortaliGirisZeyliKontrol(inputInsured, long.Parse(sgmEndors.CompanyId));
                        if (!responseSbmInsured.Data.islemBasarili)
                        {
                            bool isSgmOk = false;
                            if (responseSbmInsured.ServiceException.failureList.Count == 1)
                            {
                                foreach (var itemm in responseSbmInsured.ServiceException.failureList)
                                {
                                    if (itemm.code == "BRV-SGM-00001")
                                    {
                                        isSgmOk = true;
                                    }
                                }
                            }

                            if (!isSgmOk)
                            {
                                response.ResultCode = 0;
                                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                                response.FailedErrors = responseSbmInsured.ServiceException.failureList;
                                log.Request = inputInsured.ToXML(true);

                                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                                return response;

                            }
                        }
                    }
                    else
                    {
                        var responseSbmInsured = serviceClient.sigortaliGirisZeyli(inputInsured, long.Parse(sgmEndors.CompanyId));
                        SpResponse spResponse = new SpResponse();
                        if (!responseSbmInsured.Data.islemBasarili)
                        {
                            response.ResultCode = 0;
                            response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                            response.FailedErrors = responseSbmInsured.ServiceException.failureList;
                            log.Request = inputInsured.ToXML(true);
                            entranceEndorsement.SbmStatus = ((int)SagmerStatus.BASARISIZ).ToString();
                            spResponse = new GenericRepository<Endorsement>().Insert(entranceEndorsement);
                            CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                            return response;
                        }
                        entranceEndorsement.SbmStatus = ((int)SagmerStatus.BASARILI).ToString();
                        spResponse = new GenericRepository<Endorsement>().Insert(entranceEndorsement);
                    }
                    
                    CreateWaitingLog(log);
                    lstInsured = new List<sigortali>();
                }
                #endregion

                //log.Request = inputInsured.ToXML(true);

                

                #region OK
                response.ResultCode = 1;
                response.ResultDesc = "İşlem Başarılı";
                log.Request = inputInsured.ToXML(true);
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.ResultCode = 0;
                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = ex.Message });

                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                #endregion
            }
            return response;
        }

        //Sigortalı Çıkış Zeyili
        public SbmServiceRes SbmInsuredExitEndorsement(SbmInsuredExitReq req, Enums.SagmerServiceType ServiceType)
        {
            SbmServiceRes response = new SbmServiceRes();
            response.FailedErrors = new List<failure>();

            var currentMethod = MethodBase.GetCurrentMethod();
            ServiceClient serviceClient = new ServiceClient();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                SagmerHelper sagmerHelper = new SagmerHelper();
                if (!sagmerHelper.IsSagmerSendViaPolicy(req.PolicyId))
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Bu Poliçenin Sagmere Gönderilme Zorunluluğu Bulunmamaktadır. Altürününü Kontrol Ediniz." });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }

                Endorsement entranceEndorsement = new GenericRepository<Endorsement>().FindBy("ID=:endorsementId", orderby: "", parameters: new { endorsementId = req.EndorsementId }).FirstOrDefault();
                if (entranceEndorsement == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Zeyil Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region GetInsuredData
                List<V_SgmEgressEndorsement> sgmEndorsList = new GenericRepository<V_SgmEgressEndorsement>().FindBy($"ENDORSEMENT_ID = {req.EndorsementId} AND POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                if (sgmEndorsList == null || sgmEndorsList.Count < 1)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Sigortalı Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                log.PolicyId = req.PolicyId;
                log.COMPANY_ID = long.Parse(sgmEndorsList[0].CompanyId);
                #endregion

                foreach (var sgmEndors in sgmEndorsList)
                {
                    #region FillInsuredData
                    sigortaliCikisInputType input = new sigortaliCikisInputType
                    {
                        brutPrim = sgmEndors.BrutPrim.IsNumeric() ? decimal.Parse(sgmEndors.BrutPrim) : 0,
                        kimlikNo = sgmEndors.KimlikNo,
                        kimlikTipKod = sgmEndors.KimlikTipKod.IsInt() ? sgmEndors.KimlikTipKod : "",
                        netPrim = sgmEndors.NetPrim.IsNumeric() ? decimal.Parse(sgmEndors.NetPrim) : 0,
                        otorizasyonKod = req.AuthorizationCode,
                        policeBrutPrimi = sgmEndors.PoliceBrutPrimi.IsNumeric() ? decimal.Parse(sgmEndors.PoliceBrutPrimi) : 0,
                        policeNetPrimi = sgmEndors.PoliceNetPrimi.IsNumeric() ? decimal.Parse(sgmEndors.PoliceNetPrimi) : 0,
                        policeNo = sgmEndors.PoliceNo,
                        sbmSagmerNo = sgmEndors.SbmSagmerNo.IsInt64() ? long.Parse(sgmEndors.SbmSagmerNo) : 0,
                        sigortaSirketKod = sgmEndors.SigortaSirketKod,
                        yenilemeNo = sgmEndors.YenilemeNo.IsInt() ? int.Parse(sgmEndors.YenilemeNo) : 0,
                        zeylBaslangicTarihi = sgmEndors.ZeylBaslangicTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylBaslangicTarihi) : DateTime.Now,
                        zeylBrutPrimi = sgmEndors.ZeylBrutPrimi.IsNumeric() ? decimal.Parse(sgmEndors.ZeylBrutPrimi) : 0,
                        zeylNetPrimi = sgmEndors.ZeylNetPrimi.IsNumeric() ? decimal.Parse(sgmEndors.ZeylNetPrimi) : 0,
                        zeylNo = sgmEndors.ZeylNo.IsInt() ? int.Parse(sgmEndors.ZeylNo) : 0,
                        zeylTanzimTarihi = sgmEndors.ZeylTanzimTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylTanzimTarihi) : DateTime.Now,
                    };
                    #endregion

                    log.Request = input.ToXML(true);

                    #region Operation
                    if (ServiceType == Enums.SagmerServiceType.KONTROL)
                    {
                        var responseSbmInsured = serviceClient.sigortaliCikisZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
                        if (!responseSbmInsured.Data.islemBasarili)
                        {
                            response.ResultCode = 0;
                            response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                            response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                            CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                            return response;
                        }
                    }
                    else
                    {
                        var responseSbmInsured = serviceClient.sigortaliCikisZeyli(input, long.Parse(sgmEndors.CompanyId));
                        SpResponse spResponse = new SpResponse();
                        Endorsement endorsement = new GenericRepository<Endorsement>().FindBy($"ID={req.EndorsementId}").FirstOrDefault();
                        if (!responseSbmInsured.Data.islemBasarili)
                        {
                            response.ResultCode = 0;
                            response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                            response.FailedErrors = responseSbmInsured.ServiceException.failureList;
                            endorsement.SbmStatus = ((int)SagmerStatus.BASARISIZ).ToString();
                            spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                            CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                            return response;
                        }
                        endorsement.SbmStatus = ((int)SagmerStatus.BASARILI).ToString();
                        spResponse = new GenericRepository<Endorsement>().Insert(endorsement);

                    }
                    #endregion

                    #region OK
                    response.ResultCode = 1;
                    response.ResultDesc = "İşlem Başarılı";
                    log.Request = input.ToXML(true);
                    CreateOKLog(log, response.ToXML(true));
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.ResultCode = 0;
                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = ex.Message });

                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                #endregion
            }
            return response;
        }

        //Sigortalı Tahakkuk Zeyili
        public SbmServiceRes SbmInsuredAccrualEndorsement(SbmInsuredAccrualReq req, Enums.SagmerServiceType ServiceType)
        {
            SbmServiceRes response = new SbmServiceRes();
            response.FailedErrors = new List<failure>();

            var currentMethod = MethodBase.GetCurrentMethod();
            ServiceClient serviceClient = new ServiceClient();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                SagmerHelper sagmerHelper = new SagmerHelper();
                if (!sagmerHelper.IsSagmerSendViaPolicy(req.PolicyId))
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Bu Poliçenin Sagmere Gönderilme Zorunluluğu Bulunmamaktadır. Altürününü Kontrol Ediniz." });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }

                Endorsement entranceEndorsement = new GenericRepository<Endorsement>().FindBy("ID=:endorsementId", orderby: "", parameters: new { endorsementId = req.EndorsementId }).FirstOrDefault();
                if (entranceEndorsement == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Zeyil Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region GetInsuredData
                V_SgmEndorsInsuredPremium sgmEndors = new GenericRepository<V_SgmEndorsInsuredPremium>().FindBy($"INSURED_ID = {req.InsuredId} AND ENDORSEMENT_ID = {req.EndorsementId} AND POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (sgmEndors == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Sigortalı Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                log.PolicyId = req.PolicyId;
                log.COMPANY_ID = long.Parse(sgmEndors.CompanyId);
                #endregion

                #region FillInsuredData
                sigortaliPrimFarkiInputType input = new sigortaliPrimFarkiInputType
                {
                    brutPrim = sgmEndors.BrutPrim.IsNumeric() ? decimal.Parse(sgmEndors.BrutPrim) : 0,
                    kimlikNo = sgmEndors.KimlikNo,
                    kimlikTipKod = sgmEndors.KimlikTipi.IsInt() ? sgmEndors.KimlikTipi : "",
                    netPrim = sgmEndors.NetPrim.IsNumeric() ? decimal.Parse(sgmEndors.NetPrim) : 0,
                    otorizasyonKod = req.Authorization,
                    policeBrutPrimi = sgmEndors.PoliceBrutPrimi.IsNumeric() ? decimal.Parse(sgmEndors.PoliceBrutPrimi) : 0,
                    policeNetPrimi = sgmEndors.PoliceNetPrimi.IsNumeric() ? decimal.Parse(sgmEndors.PoliceNetPrimi) : 0,
                    policeNo = sgmEndors.PoliceNo,
                    sbmSagmerNo = sgmEndors.SbmSagmerNo.IsInt64() ? long.Parse(sgmEndors.SbmSagmerNo) : 0,
                    sigortaSirketKod = sgmEndors.SigortaSirketKod,
                    yenilemeNo = sgmEndors.YenilemeNo.IsInt() ? int.Parse(sgmEndors.YenilemeNo) : 0,
                    zeylBaslangicTarihi = sgmEndors.ZeylBaslangicTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylBaslangicTarihi) : DateTime.Now,
                    zeylBrutPrimi = sgmEndors.ZeylBrutPrimi.IsNumeric() ? decimal.Parse(sgmEndors.ZeylBrutPrimi) : 0,
                    zeylNetPrimi = sgmEndors.ZeylNetPrimi.IsNumeric() ? decimal.Parse(sgmEndors.ZeylNetPrimi) : 0,
                    zeylNo = sgmEndors.ZeylNo.IsInt() ? int.Parse(sgmEndors.ZeylNo) : 0,
                    zeylTanzimTarihi = sgmEndors.ZeylTanzimTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylTanzimTarihi) : DateTime.Now,
                };
                #endregion

                log.Request = input.ToXML(true);

                #region Operation
                if (ServiceType == Enums.SagmerServiceType.KONTROL)
                {
                    var responseSbmInsured = serviceClient.sigortaliTahakkukZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                else
                {
                    var responseSbmInsured = serviceClient.sigortaliTahakkukZeyli(input, long.Parse(sgmEndors.CompanyId));
                    SpResponse spResponse = new SpResponse();
                    Endorsement endorsement = new GenericRepository<Endorsement>().FindBy($"ID={req.EndorsementId}").FirstOrDefault();
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;
                        endorsement.SbmStatus = ((int)SagmerStatus.BASARILI).ToString();
                        spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }

                    endorsement.SbmStatus = ((int)SagmerStatus.BASARILI).ToString();
                    spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                }
                #endregion

                #region OK
                response.ResultCode = 1;
                response.ResultDesc = "İşlem Başarılı";
                log.Request = input.ToXML(true);
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.ResultCode = 0;
                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = ex.Message });

                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                #endregion
            }
            return response;
        }

        //Police Tahakkuk ve Prim Farkı Zeyili
        public SbmServiceRes SbmPolicyAccrualEndorsement(SbmPolicyAccrualReq req, Enums.SagmerServiceType ServiceType)
        {
            SbmServiceRes response = new SbmServiceRes();
            response.FailedErrors = new List<failure>();

            var currentMethod = MethodBase.GetCurrentMethod();
            ServiceClient serviceClient = new ServiceClient();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                SagmerHelper sagmerHelper = new SagmerHelper();
                if (!sagmerHelper.IsSagmerSendViaPolicy(req.PolicyId))
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Bu Poliçenin Sagmere Gönderilme Zorunluluğu Bulunmamaktadır. Altürününü Kontrol Ediniz." });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }

                Endorsement entranceEndorsement = new GenericRepository<Endorsement>().FindBy("ID=:endorsementId", orderby: "", parameters: new { endorsementId = req.EndorsementId }).FirstOrDefault();
                if (entranceEndorsement == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Zeyil Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region GetData
                V_SgmEndorsPolicyPremium sgmPremium = new GenericRepository<V_SgmEndorsPolicyPremium>().FindBy($"ENDORSEMENT_ID = {req.EndorsementId} AND POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (sgmPremium == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Zeyil Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                log.PolicyId = req.PolicyId;
                log.COMPANY_ID = long.Parse(sgmPremium.CompanyId);
                #endregion

                #region FillData
                primFarkiInputType input = new primFarkiInputType
                {
                    otorizasyonKod = req.Authorization,
                    policeBrutPrimi = sgmPremium.PoliceBrutPrimi.IsNumeric() ? decimal.Parse(sgmPremium.PoliceBrutPrimi) : 0,
                    policeNetPrimi = sgmPremium.PoliceNetPrimi.IsNumeric() ? decimal.Parse(sgmPremium.PoliceNetPrimi) : 0,
                    policeNo = sgmPremium.PoliceNo,
                    sbmSagmerNo = sgmPremium.SbmSagmerNo.IsInt64() ? long.Parse(sgmPremium.SbmSagmerNo) : 0,
                    sigortaSirketKod = sgmPremium.SigortaSirketKod,
                    yenilemeNo = sgmPremium.YenilemeNo.IsInt() ? int.Parse(sgmPremium.YenilemeNo) : 0,
                    zeylBaslangicTarihi = sgmPremium.ZeylBaslangicTarihi.IsDateTime() ? DateTime.Parse(sgmPremium.ZeylBaslangicTarihi) : DateTime.Now,
                    zeylBrutPrimi = sgmPremium.ZeylBrutPrimi.IsNumeric() ? decimal.Parse(sgmPremium.ZeylBrutPrimi) : 0,
                    zeylNetPrimi = sgmPremium.ZeylNetPrimi.IsNumeric() ? decimal.Parse(sgmPremium.ZeylNetPrimi) : 0,
                    zeylNo = sgmPremium.ZeylNo.IsInt() ? int.Parse(sgmPremium.ZeylNo) : 0,
                    zeylTanzimTarihi = sgmPremium.ZeylTanzimTarihi.IsDateTime() ? DateTime.Parse(sgmPremium.ZeylTanzimTarihi) : DateTime.Now,
                    zeylTipi = sgmPremium.ZeylTipi.IsInt() ? int.Parse(sgmPremium.ZeylTipi) : 0
                };
                #endregion

                log.Request = input.ToXML(true);

                #region Operation
                if (ServiceType == Enums.SagmerServiceType.KONTROL)
                {
                    var responseSbmInsured = serviceClient.policeTahakkukVePrimFarkiZeyliKontrol(input, long.Parse(sgmPremium.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                else
                {
                    var responseSbmInsured = serviceClient.policeTahakkukVePrimFarkiZeyli(input, long.Parse(sgmPremium.CompanyId));
                    SpResponse spResponse = new SpResponse();
                    Endorsement endorsement = new GenericRepository<Endorsement>().FindBy($"ID={req.EndorsementId}").FirstOrDefault();
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;
                        endorsement.SbmStatus = ((int)SagmerStatus.BASARISIZ).ToString();
                        spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                    endorsement.SbmStatus = ((int)SagmerStatus.BASARILI).ToString();
                    spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                }
                #endregion

                #region OK
                response.ResultCode = 1;
                response.ResultDesc = "İşlem Başarılı";
                log.Request = input.ToXML(true);
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.ResultCode = 0;
                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = ex.Message });

                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                #endregion
            }
            return response;
        }

        //Sigorta Ettiren Değişikliği Zeyili
        public SbmServiceRes SbmInsurerChangeEndorsement(SbmInsurerChangeReq req, Enums.SagmerServiceType ServiceType)
        {
            SbmServiceRes response = new SbmServiceRes();
            response.FailedErrors = new List<failure>();

            var currentMethod = MethodBase.GetCurrentMethod();
            ServiceClient serviceClient = new ServiceClient();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                SagmerHelper sagmerHelper = new SagmerHelper();
                if (!sagmerHelper.IsSagmerSendViaPolicy(req.PolicyId))
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Bu Poliçenin Sagmere Gönderilme Zorunluluğu Bulunmamaktadır. Altürününü Kontrol Ediniz." });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }

                Endorsement entranceEndorsement = new GenericRepository<Endorsement>().FindBy("ID=:endorsementId", orderby: "", parameters: new { endorsementId = req.EndorsementId }).FirstOrDefault();
                if (entranceEndorsement == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Zeyil Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region GetInsurerData
                V_SgmEndorsPolicyInsurer sgmInsurer = new GenericRepository<V_SgmEndorsPolicyInsurer>().FindBy($"ENDORSEMENT_ID = {req.EndorsementId} AND POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (sgmInsurer == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Sigorta Ettiren Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                log.PolicyId = req.PolicyId;
                log.COMPANY_ID = long.Parse(sgmInsurer.CompanyId);
                #endregion

                #region FillInsuredData
                sigortaEttirenZeylInputType input = new sigortaEttirenZeylInputType
                {
                    ad = sgmInsurer.Ad,
                    soyad = sgmInsurer.Soyad,
                    adres = sgmInsurer.Adres,
                    babaAdi = sgmInsurer.BabaAdi,
                    cinsiyet = ((Enums.cinsiyet)int.Parse(sgmInsurer.Cinsiyet)),
                    dogumTarihi = sgmInsurer.DogumTarihi.IsDateTime() ? DateTime.Parse(sgmInsurer.DogumTarihi) : DateTime.Now,
                    dogumYeri = sgmInsurer.DogumYeri,
                    kimlikNo = sgmInsurer.KimlikNo,
                    kimlikTipKod = sgmInsurer.KimlikTipKod,
                    kurulusTarihi = sgmInsurer.KurulusTarihi.IsDateTime() ? DateTime.Parse(sgmInsurer.KurulusTarihi) : DateTime.Now,
                    kurulusYeri = sgmInsurer.KurulusYeri,
                    otorizasyonKod = req.Authorization,
                    policeBrutPrimi = sgmInsurer.PoliceBrutPrimi.IsNumeric() ? decimal.Parse(sgmInsurer.PoliceBrutPrimi) : 0,
                    policeNetPrimi = sgmInsurer.PoliceBrutPrimi.IsNumeric() ? decimal.Parse(sgmInsurer.PoliceNetPrimi) : 0,
                    policeNo = sgmInsurer.PoliceNo,
                    sbmSagmerNo = sgmInsurer.SbmSagmerNo.IsInt64() ? long.Parse(sgmInsurer.SbmSagmerNo) : 0,
                    sigortaSirketKod = sgmInsurer.SigortaSirketKod,
                    turKod = ((Enums.turKod)int.Parse(sgmInsurer.TurKod)),
                    ulkeKodu = sgmInsurer.UlkeKodu.IsInt() ? int.Parse(sgmInsurer.UlkeKodu) : 0,
                    yenilemeNo = sgmInsurer.YenilemeNo.IsInt() ? int.Parse(sgmInsurer.YenilemeNo) : 0,
                    zeylBaslangicTarihi = sgmInsurer.ZeylBaslangicTarihi.IsDateTime() ? DateTime.Parse(sgmInsurer.ZeylBaslangicTarihi) : DateTime.Now,
                    zeylBrutPrimi = sgmInsurer.ZeylBrutPrimi.IsNumeric() ? decimal.Parse(sgmInsurer.ZeylBrutPrimi) : 0,
                    zeylNetPrimi = sgmInsurer.ZeylNetPrimi.IsNumeric() ? decimal.Parse(sgmInsurer.ZeylNetPrimi) : 0,
                    zeylNo = sgmInsurer.ZeylNo.IsInt() ? int.Parse(sgmInsurer.ZeylNo) : 0,
                    zeylTanzimTarihi = sgmInsurer.ZeylTanzimTarihi.IsDateTime() ? DateTime.Parse(sgmInsurer.ZeylTanzimTarihi) : DateTime.Now
                };
                #endregion

                log.Request = input.ToXML(true);

                #region Operation
                if (ServiceType == Enums.SagmerServiceType.KONTROL)
                {
                    var responseSbmInsured = serviceClient.sigortaEttirenDegisiklikZeyliKontrol(input, long.Parse(sgmInsurer.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                else
                {
                    var responseSbmInsured = serviceClient.sigortaEttirenDegisiklikZeyli(input, long.Parse(sgmInsurer.CompanyId));

                    SpResponse spResponse = new SpResponse();
                    Endorsement endorsement = new GenericRepository<Endorsement>().FindBy($"ID={req.EndorsementId}").FirstOrDefault();
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;
                        endorsement.SbmStatus = ((int)SagmerStatus.BASARISIZ).ToString();
                        spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                    endorsement.SbmStatus = ((int)SagmerStatus.BASARILI).ToString();
                    spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                }
                #endregion

                #region OK
                response.ResultCode = 1;
                response.ResultDesc = "İşlem Başarılı";
                log.Request = input.ToXML(true);
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.ResultCode = 0;
                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = ex.Message });

                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                #endregion
            }
            return response;
        }

        //Üretim Kaynağı(Acente) Değişikliği Zeyili
        public SbmServiceRes SbmAgencyChangeEndorsement(SbmAgencyChangeReq req, Enums.SagmerServiceType ServiceType)
        {
            SbmServiceRes response = new SbmServiceRes();
            response.FailedErrors = new List<failure>();

            var currentMethod = MethodBase.GetCurrentMethod();
            ServiceClient serviceClient = new ServiceClient();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                SagmerHelper sagmerHelper = new SagmerHelper();
                if (!sagmerHelper.IsSagmerSendViaPolicy(req.PolicyId))
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Bu Poliçenin Sagmere Gönderilme Zorunluluğu Bulunmamaktadır. Altürününü Kontrol Ediniz." });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }

                Endorsement endorsement = new GenericRepository<Endorsement>().FindBy("ID=:endorsementId AND TYPE=:type", orderby: "", parameters: new { endorsementId = req.EndorsementId, type = new Dapper.DbString { Value = ((int)EndorsementType.URETIM_KAYNAGI_DEGISIKLIGI).ToString(), Length = 3 } }).FirstOrDefault();
                if (endorsement == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Zeyil Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region GetData
                V_SgmEndorsPolicyPremium sgmEndors = new GenericRepository<V_SgmEndorsPolicyPremium>().FindBy($"ENDORSEMENT_ID = {req.EndorsementId} AND POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (sgmEndors == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Zeyil Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                log.PolicyId = req.PolicyId;
                log.COMPANY_ID = long.Parse(sgmEndors.CompanyId);
                #endregion

                #region FillData
                uretimKaynakInputType input = new uretimKaynakInputType
                {
                    otorizasyonKod = req.Authorization,
                    policeBrutPrimi = sgmEndors.PoliceBrutPrimi.IsNumeric() ? decimal.Parse(sgmEndors.PoliceBrutPrimi) : 0,
                    policeNetPrimi = sgmEndors.PoliceNetPrimi.IsNumeric() ? decimal.Parse(sgmEndors.PoliceNetPrimi) : 0,
                    policeNo = sgmEndors.PoliceNo,
                    sbmSagmerNo = sgmEndors.SbmSagmerNo.IsInt64() ? long.Parse(sgmEndors.SbmSagmerNo) : 0,
                    sigortaSirketKod = sgmEndors.SigortaSirketKod,
                    yenilemeNo = sgmEndors.YenilemeNo.IsInt() ? int.Parse(sgmEndors.YenilemeNo) : 0,
                    zeylBaslangicTarihi = sgmEndors.ZeylBaslangicTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylBaslangicTarihi) : DateTime.Now,
                    zeylBrutPrimi = sgmEndors.ZeylBrutPrimi.IsNumeric() ? decimal.Parse(sgmEndors.ZeylBrutPrimi) : 0,
                    zeylNetPrimi = sgmEndors.ZeylNetPrimi.IsNumeric() ? decimal.Parse(sgmEndors.ZeylNetPrimi) : 0,
                    zeylNo = sgmEndors.ZeylNo.IsInt() ? int.Parse(sgmEndors.ZeylNo) : 0,
                    zeylTanzimTarihi = sgmEndors.ZeylTanzimTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylTanzimTarihi) : DateTime.Now,
                    uretimKaynakKod = sgmEndors.UretimKaynakKod.IsInt64() ? long.Parse(sgmEndors.UretimKaynakKod) : 0,
                    uretimKaynakKurumKod = sgmEndors.UretimKaynakKod
                };
                #endregion

                log.Request = input.ToXML(true);

                #region Operation
                if (ServiceType == Enums.SagmerServiceType.KONTROL)
                {
                    var responseSbmInsured = serviceClient.uretimKaynakDegisiklikZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                else
                {
                    var responseSbmInsured = serviceClient.uretimKaynakDegisiklikZeyli(input, long.Parse(sgmEndors.CompanyId));
                    SpResponse spResponse = new SpResponse();
                    
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;
                        endorsement.SbmStatus = ((int)SagmerStatus.BASARILI).ToString();
                        spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                    endorsement.SbmStatus = ((int)SagmerStatus.BASARILI).ToString();
                    spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                }
                #endregion

                #region OK
                response.ResultCode = 1;
                response.ResultDesc = "İşlem Başarılı";
                log.Request = input.ToXML(true);
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.ResultCode = 0;
                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = ex.Message });

                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                #endregion
            }
            return response;
        }

        //Poliçe İptal Zeyili
        public SbmServiceRes SbmPolicyCancelEndorsement(SbmPolicyCancelReq req, Enums.SagmerServiceType ServiceType)
        {
            SbmServiceRes response = new SbmServiceRes();
            response.FailedErrors = new List<failure>();

            var currentMethod = MethodBase.GetCurrentMethod();
            ServiceClient serviceClient = new ServiceClient();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                SagmerHelper sagmerHelper = new SagmerHelper();
                if (!sagmerHelper.IsSagmerSendViaPolicy(req.PolicyId))
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Bu Poliçenin Sagmere Gönderilme Zorunluluğu Bulunmamaktadır. Altürününü Kontrol Ediniz." });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }

                Endorsement endorsement = new GenericRepository<Endorsement>().FindBy("ID=:endorsementId", orderby: "", parameters: new { endorsementId = req.EndorsementId }).FirstOrDefault();
                if (endorsement == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Zeyil Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region GetData
                V_SgmEndorsPolicyPremium sgmEndors = new GenericRepository<V_SgmEndorsPolicyPremium>().FindBy($"ENDORSEMENT_ID = {req.EndorsementId} AND POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (sgmEndors == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Zeyil Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                log.PolicyId = req.PolicyId;
                log.COMPANY_ID = long.Parse(sgmEndors.CompanyId);
                #endregion

                #region FillData
                genelIptalInputType input = new genelIptalInputType
                {
                    iptalNedenKod = sgmEndors.IptalNedenKod.IsInt64() ? long.Parse(sgmEndors.IptalNedenKod) : 2,
                    iptalZeylTuru = sgmEndors.IptalZeylTuru.IsInt64() ? long.Parse(sgmEndors.IptalZeylTuru) : 0,
                    otorizasyonKod = req.Authorization,
                    policeBrutPrimi = sgmEndors.PoliceBrutPrimi.IsNumeric() ? decimal.Parse(sgmEndors.PoliceBrutPrimi) : 0,
                    policeNetPrimi = sgmEndors.PoliceNetPrimi.IsNumeric() ? decimal.Parse(sgmEndors.PoliceNetPrimi) : 0,
                    policeNo = sgmEndors.PoliceNo,
                    sbmSagmerNo = sgmEndors.SbmSagmerNo.IsInt64() ? long.Parse(sgmEndors.SbmSagmerNo) : 0,
                    sigortaSirketKod = sgmEndors.SigortaSirketKod,
                    yenilemeNo = sgmEndors.YenilemeNo.IsInt() ? int.Parse(sgmEndors.YenilemeNo) : 0,
                    zeylBaslangicTarihi = sgmEndors.ZeylBaslangicTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylBaslangicTarihi) : DateTime.Now,
                    zeylBrutPrimi = sgmEndors.ZeylBrutPrimi.IsNumeric() ? decimal.Parse(sgmEndors.ZeylBrutPrimi) : 0,
                    zeylNetPrimi = sgmEndors.ZeylNetPrimi.IsNumeric() ? decimal.Parse(sgmEndors.ZeylNetPrimi) : 0,
                    zeylNo = sgmEndors.ZeylNo.IsInt() ? int.Parse(sgmEndors.ZeylNo) : 0,
                    zeylTanzimTarihi = sgmEndors.ZeylTanzimTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylTanzimTarihi) : DateTime.Now
                };
                #endregion

                log.Request = input.ToXML(true);

                #region Operation
                if (ServiceType == Enums.SagmerServiceType.KONTROL)
                {
                    var responseSbmInsured = serviceClient.genelIptalZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                else
                {
                    var responseSbmInsured = serviceClient.genelIptalZeyli(input, long.Parse(sgmEndors.CompanyId));
                    SpResponse spResponse = new SpResponse();
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;
                        endorsement.SbmStatus = ((int)SagmerStatus.BASARISIZ).ToString();
                        spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                    endorsement.SbmStatus = ((int)SagmerStatus.BASARILI).ToString();
                    spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                }
                #endregion

                #region OK
                response.ResultCode = 1;
                response.ResultDesc = "İşlem Başarılı";
                log.Request = input.ToXML(true);
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.ResultCode = 0;
                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = ex.Message });

                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                #endregion
            }
            return response;
        }

        //Poliçe Meriyete Dönüş Zeyili
        public SbmServiceRes SbmPolicyReturnStartEndorsement(SbmPolicyReturnStartReq req, Enums.SagmerServiceType ServiceType)
        {
            SbmServiceRes response = new SbmServiceRes();
            response.FailedErrors = new List<failure>();

            var currentMethod = MethodBase.GetCurrentMethod();
            ServiceClient serviceClient = new ServiceClient();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                SagmerHelper sagmerHelper = new SagmerHelper();
                if (!sagmerHelper.IsSagmerSendViaPolicy(req.PolicyId))
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Bu Poliçenin Sagmere Gönderilme Zorunluluğu Bulunmamaktadır. Altürününü Kontrol Ediniz." });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }

                Endorsement endorsement = new GenericRepository<Endorsement>().FindBy("ID=:endorsementId AND TYPE=:type", orderby: "", parameters: new { endorsementId = req.EndorsementId, type = new Dapper.DbString { Value = ((int)EndorsementType.MERIYETE_DONUS_ZEYLI).ToString(), Length = 3 } }).FirstOrDefault();
                if (endorsement == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Zeyil Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region GetData
                V_SgmEndorsPolicyPremium sgmEndors = new GenericRepository<V_SgmEndorsPolicyPremium>().FindBy($"ENDORSEMENT_ID = {req.EndorsementId} AND POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (sgmEndors == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Zeyil Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                log.PolicyId = req.PolicyId;
                log.COMPANY_ID = long.Parse(sgmEndors.CompanyId);
                #endregion

                #region FillData
                policeYururlugeAlmaInputType input = new policeYururlugeAlmaInputType
                {
                    otorizasyonKod = req.Authorization,
                    policeBrutPrimi = sgmEndors.PoliceBrutPrimi.IsNumeric() ? decimal.Parse(sgmEndors.PoliceBrutPrimi) : 0,
                    policeNetPrimi = sgmEndors.PoliceNetPrimi.IsNumeric() ? decimal.Parse(sgmEndors.PoliceNetPrimi) : 0,
                    policeNo = sgmEndors.PoliceNo,
                    sbmSagmerNo = sgmEndors.SbmSagmerNo.IsInt64() ? long.Parse(sgmEndors.SbmSagmerNo) : 0,
                    sigortaSirketKod = sgmEndors.SigortaSirketKod,
                    yenilemeNo = sgmEndors.YenilemeNo.IsInt() ? int.Parse(sgmEndors.YenilemeNo) : 0,
                    zeylBaslangicTarihi = sgmEndors.ZeylBaslangicTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylBaslangicTarihi) : DateTime.Now,
                    zeylBrutPrimi = sgmEndors.ZeylBrutPrimi.IsNumeric() ? decimal.Parse(sgmEndors.ZeylBrutPrimi) : 0,
                    zeylNetPrimi = sgmEndors.ZeylNetPrimi.IsNumeric() ? decimal.Parse(sgmEndors.ZeylNetPrimi) : 0,
                    zeylNo = sgmEndors.ZeylNo.IsInt() ? int.Parse(sgmEndors.ZeylNo) : 0,
                    zeylTanzimTarihi = sgmEndors.ZeylTanzimTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylTanzimTarihi) : DateTime.Now
                };
                #endregion

                log.Request = input.ToXML(true);

                #region Operation
                if (ServiceType == Enums.SagmerServiceType.KONTROL)
                {
                    var responseSbmInsured = serviceClient.meriyeteDonusZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                else
                {
                    var responseSbmInsured = serviceClient.meriyeteDonusZeyli(input, long.Parse(sgmEndors.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                #endregion

                #region OK
                response.ResultCode = 1;
                response.ResultDesc = "İşlem Başarılı";
                log.Request = input.ToXML(true);
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.ResultCode = 0;
                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = ex.Message });

                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                #endregion
            }
            return response;
        }

        //Poliçe Silme
        public SbmServiceRes SbmPolicyDelete(SbmPolicyDeleteReq req, Enums.SagmerServiceType ServiceType)
        {
            SbmServiceRes response = new SbmServiceRes();
            response.FailedErrors = new List<failure>();

            var currentMethod = MethodBase.GetCurrentMethod();
            ServiceClient serviceClient = new ServiceClient();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                SagmerHelper sagmerHelper = new SagmerHelper();
                if (!sagmerHelper.IsSagmerSendViaPolicy(req.PolicyId, true))
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Bu Poliçenin Sagmere Gönderilme Zorunluluğu Bulunmamaktadır. Altürününü Kontrol Ediniz." });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region GetData
                V_SgmPolicyDelete sgmPolDel = new GenericRepository<V_SgmPolicyDelete>().FindBy($" POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (sgmPolDel == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Poliçe Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                log.PolicyId = req.PolicyId;
                log.COMPANY_ID = long.Parse(sgmPolDel.CompanyId);
                #endregion

                #region FillData
                policeSilmeInputType input = new policeSilmeInputType
                {
                    otorizasyonKod = sgmPolDel.OtorizasyonKod,
                    policeNo = sgmPolDel.PoliceNo,
                    sbmSagmerNo = sgmPolDel.SbmSagmerNo.IsInt64() ? long.Parse(sgmPolDel.SbmSagmerNo) : 0,
                    yenilemeNo = sgmPolDel.YenilemeNo.IsInt() ? int.Parse(sgmPolDel.YenilemeNo) : 0
                };
                #endregion

                log.Request = input.ToXML(true);

                #region Operation
                if (ServiceType == Enums.SagmerServiceType.KONTROL)
                {
                    var responseSbmInsured = serviceClient.policeSilmeKontrol(input, long.Parse(sgmPolDel.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                else
                {
                    var responseSbmInsured = serviceClient.policeSilme(input, long.Parse(sgmPolDel.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                #endregion

                #region OK
                response.ResultCode = 1;
                response.ResultDesc = "İşlem Başarılı";
                log.Request = input.ToXML(true);
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.ResultCode = 0;
                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = ex.Message });

                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                #endregion
            }
            return response;
        }

        //Sigortalı Teminat Plan Değişikliği Zeyili
        public SbmServiceRes SbmInsuredPackageChangeEndorsement(SbmInsuredPackageChangeReq req, Enums.SagmerServiceType ServiceType)
        {
            SbmServiceRes response = new SbmServiceRes();
            response.FailedErrors = new List<failure>();

            var currentMethod = MethodBase.GetCurrentMethod();
            ServiceClient serviceClient = new ServiceClient();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                SagmerHelper sagmerHelper = new SagmerHelper();
                if (!sagmerHelper.IsSagmerSendViaPolicy(req.PolicyId))
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Bu Poliçenin Sagmere Gönderilme Zorunluluğu Bulunmamaktadır. Altürününü Kontrol Ediniz." });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }

                Endorsement entranceEndorsement = new GenericRepository<Endorsement>().FindBy("ID=:endorsementId", orderby: "", parameters: new { endorsementId = req.EndorsementId }).FirstOrDefault();
                if (entranceEndorsement == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Zeyil Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                List<V_InsuredEndorsement> v_InsuredEndorsementList = new GenericRepository<V_InsuredEndorsement>().FindBy($"ENDORSEMENT_ID = {req.EndorsementId} AND POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                foreach (var item in v_InsuredEndorsementList)
                {
                    #region GetInsuredData
                    V_SgmEndorsInsuredPremium sgmEndors = new GenericRepository<V_SgmEndorsInsuredPremium>().FindBy($" INSURED_ID = {item.INSURED_ID}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                    List<V_SgmIngressInsCover> sgmCover = new GenericRepository<V_SgmIngressInsCover>().FindBy($" INSURED_ID = {item.INSURED_ID}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                    if (sgmEndors == null || sgmCover == null || sgmCover.Count < 1)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Sigortalı Bilgisi Bulunamadı!" });

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                    log.PolicyId = req.PolicyId;
                    log.COMPANY_ID = long.Parse(sgmEndors.CompanyId);
                    #endregion

                    #region FillInsuredData
                    List<sigortaliTeminatInputTypeList> sigTem = new List<sigortaliTeminatInputTypeList>();
                    foreach (V_SgmIngressInsCover itemInsCover in sgmCover)
                    {
                        sigTem.Add(new sigortaliTeminatInputTypeList
                        {
                            aso = itemInsCover.Aso.IsInt() ? (Enums.evetHayirType)int.Parse(itemInsCover.Aso) : evetHayirType.H,
                            cografiKapsamKodu = itemInsCover.CografiKapsamKodu.IsInt64() ? long.Parse(itemInsCover.CografiKapsamKodu) : 0,
                            donemBaslangicTarihi = itemInsCover.DonemBaslangicTarihi.IsDateTime() ? DateTime.Parse(itemInsCover.DonemBaslangicTarihi) : DateTime.Now,
                            donemBitisTarihi = itemInsCover.DonemBitisTarihi.IsDateTime() ? DateTime.Parse(itemInsCover.DonemBitisTarihi) : DateTime.Now,
                            donemKodu = itemInsCover.DonemKodu.IsInt() ? int.Parse(itemInsCover.DonemKodu) : 0,
                            dovizCinsi = itemInsCover.DovizCinsi,
                            teminatAdedi = itemInsCover.TeminatAdedi.IsInt() ? int.Parse(itemInsCover.TeminatAdedi) : 0,
                            teminatKodu = itemInsCover.TeminatKodu.IsInt64() ? long.Parse(itemInsCover.TeminatKodu) : 0,
                            teminatLimiti = itemInsCover.TeminatLimiti.IsNumeric() ? decimal.Parse(itemInsCover.TeminatLimiti) : 0,
                            teminatLimitKodu = itemInsCover.TeminatLimitKodu.IsInt64() ? long.Parse(itemInsCover.TeminatLimitKodu) : 0,
                            teminatMuafiyeti = itemInsCover.TeminatMuafiyeti.IsNumeric() ? itemInsCover.TeminatMuafiyeti : "0",
                            teminatMuafiyetKodu = itemInsCover.TeminatMuafiyetKodu.IsInt64() ? itemInsCover.TeminatMuafiyetKodu : "0",
                            teminatNetPrim = itemInsCover.TeminatNetPrim.IsNumeric() ? decimal.Parse(itemInsCover.TeminatNetPrim) : 0,
                            teminatOdemeYuzdesi = itemInsCover.TeminatOdemeYuzdesi.IsNumeric() ? decimal.Parse(itemInsCover.TeminatOdemeYuzdesi) : 0,
                            teminatVaryasyonKodu = itemInsCover.TeminatVaryasyonKodu.IsInt() ? int.Parse(itemInsCover.TeminatVaryasyonKodu) : 0,
                            toplamDonemSayisi = itemInsCover.ToplamDonemSayisi.IsInt() ? int.Parse(itemInsCover.ToplamDonemSayisi) : 0,
                        });
                    }
                    teminatInputType input = new teminatInputType
                    {
                        kimlikNo = sgmEndors.KimlikNo,
                        kimlikTipKod = sgmEndors.KimlikTipi,
                        otorizasyonKod = req.Authorization,
                        policeBrutPrimi = sgmEndors.PoliceBrutPrimi.IsNumeric() ? decimal.Parse(sgmEndors.PoliceBrutPrimi) : 0,
                        policeNetPrimi = sgmEndors.PoliceNetPrimi.IsNumeric() ? decimal.Parse(sgmEndors.PoliceNetPrimi) : 0,
                        policeNo = sgmEndors.PoliceNo,
                        sbmSagmerNo = sgmEndors.SbmSagmerNo.IsInt64() ? long.Parse(sgmEndors.SbmSagmerNo) : 0,
                        sigortaSirketKod = sgmEndors.SigortaSirketKod,
                        urunPlanKod = sgmEndors.UrunPlanKod.IsInt64() ? long.Parse(sgmEndors.UrunPlanKod) : 0,
                        yenilemeNo = sgmEndors.YenilemeNo.IsInt() ? int.Parse(sgmEndors.YenilemeNo) : 0,
                        zeylBaslangicTarihi = sgmEndors.ZeylBaslangicTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylBaslangicTarihi) : DateTime.Now,
                        zeylBrutPrimi = sgmEndors.ZeylBrutPrimi.IsNumeric() ? decimal.Parse(sgmEndors.ZeylBrutPrimi) : 0,
                        zeylNetPrimi = sgmEndors.ZeylNetPrimi.IsNumeric() ? decimal.Parse(sgmEndors.ZeylNetPrimi) : 0,
                        zeylNo = sgmEndors.ZeylNo.IsInt() ? int.Parse(sgmEndors.ZeylNo) : 0,
                        zeylTanzimTarihi = sgmEndors.ZeylTanzimTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylTanzimTarihi) : DateTime.Now,
                        sigortaliTeminatInputTypeList = sigTem.ToArray()
                    };
                    #endregion

                    log.Request = input.ToXML(true);

                    #region Operation
                    if (ServiceType == Enums.SagmerServiceType.KONTROL)
                    {
                        var responseSbmInsured = serviceClient.teminatPlanDegisiklikZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
                        if (!responseSbmInsured.Data.islemBasarili)
                        {
                            response.ResultCode = 0;
                            response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                            response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                            CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                            return response;
                        }
                    }
                    else
                    {
                        var responseSbmInsured = serviceClient.teminatPlanDegisiklikZeyli(input, long.Parse(sgmEndors.CompanyId));
                        SpResponse spResponse = new SpResponse();
                        Endorsement endorsement = new GenericRepository<Endorsement>().FindBy($"ID={req.EndorsementId}").FirstOrDefault();
                        if (!responseSbmInsured.Data.islemBasarili)
                        {
                            response.ResultCode = 0;
                            response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                            response.FailedErrors = responseSbmInsured.ServiceException.failureList;
                            endorsement.SbmStatus = ((int)SagmerStatus.BASARISIZ).ToString();
                            spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                            CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                            return response;
                        }
                        endorsement.SbmStatus = ((int)SagmerStatus.BASARILI).ToString();
                        spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                    }
                    #endregion
                    log.Request = input.ToXML(true);
                    CreateOKLog(log, response.ToXML(true));
                }
                #region OK
                response.ResultCode = 1;
                response.ResultDesc = "İşlem Başarılı";
              
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.ResultCode = 0;
                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = ex.Message });

                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                #endregion
            }
            return response;
        }

        //Sigortalı Bilgi Değişikliği Zeyili
        public SbmServiceRes SbmInsuredInfoChangeEndorsement(SbmInsuredInfoChangeReq req, Enums.SagmerServiceType ServiceType)
        {
            SbmServiceRes response = new SbmServiceRes();
            response.FailedErrors = new List<failure>();

            var currentMethod = MethodBase.GetCurrentMethod();
            ServiceClient serviceClient = new ServiceClient();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                SagmerHelper sagmerHelper = new SagmerHelper();
                if (!sagmerHelper.IsSagmerSendViaPolicy(req.PolicyId))
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Bu Poliçenin Sagmere Gönderilme Zorunluluğu Bulunmamaktadır. Altürününü Kontrol Ediniz." });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }

                Endorsement entranceEndorsement = new GenericRepository<Endorsement>().FindBy("ID=:endorsementId", orderby: "", parameters: new { endorsementId = req.EndorsementId }).FirstOrDefault();
                if (entranceEndorsement == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Zeyil Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region GetInsuredData
                V_SgmIngressEndorsement sgmEndors = new GenericRepository<V_SgmIngressEndorsement>().FindBy($" ENDORSEMENT_ID = {entranceEndorsement.Id} AND POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                List<V_SgmIngressEndorsInsured> sgmEndorsInsured = new GenericRepository<V_SgmIngressEndorsInsured>().FindBy($" ENDORSEMENT_ID = {entranceEndorsement.Id} AND POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                if (sgmEndors == null || sgmEndorsInsured == null || sgmEndorsInsured.Count < 1)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Sigortalı Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                log.PolicyId = req.PolicyId;
                log.COMPANY_ID = long.Parse(sgmEndors.CompanyId);
                #endregion

                #region FillInsuredData
                List<sigortaliBilgileriInputType> lstInsured = new List<sigortaliBilgileriInputType>();
                foreach (V_SgmIngressEndorsInsured item in sgmEndorsInsured)
                {
                    V_SgmIngressInsHealthInfo healthInfo = new GenericRepository<V_SgmIngressInsHealthInfo>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {req.PolicyId} and ENDORSEMENT_ID = {entranceEndorsement.Id}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                    List<V_SgmIngressExtraPremium> ExtraPrm = new GenericRepository<V_SgmIngressExtraPremium>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {req.PolicyId} and ENDORSEMENT_ID = {entranceEndorsement.Id}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                    List<V_SgmIngressInsExemption> sgmExemption = new GenericRepository<V_SgmIngressInsExemption>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {req.PolicyId} and ENDORSEMENT_ID = {entranceEndorsement.Id}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

                    sigortaliSaglikBilgileriInputType sigSagBilg = new sigortaliSaglikBilgileriInputType();

                    List<sigortaliIndirimEkprimInputType> sigIndEkp = new List<sigortaliIndirimEkprimInputType>();

                    List<sigortaliTeminatInputTypeList> sigTem = new List<sigortaliTeminatInputTypeList>();
                    List<sigortaliUygulamaInputType> sigUyg = new List<sigortaliUygulamaInputType>();

                    if (healthInfo != null)
                    {
                        sigSagBilg.alkolKullaniyorMu = (Enums.evetHayirType)int.Parse(healthInfo.AlkolKullaniliyorMu);
                        sigSagBilg.alkolKullanmaSuresi = healthInfo.AlkolKullanmaSuresi;
                        sigSagBilg.alkolPeriyotTip = (Enums.aliskanlikPeriyodType)int.Parse(healthInfo.AlkolPeriyotTip);
                        sigSagBilg.boy = int.Parse(healthInfo.Boy);
                        sigSagBilg.kilo = int.Parse(healthInfo.Kilo);
                        sigSagBilg.periyottakiAlkolMiktari = healthInfo.PeriyottakiAlkolMiktari;
                        sigSagBilg.periyottakiSigaraAdedi = healthInfo.PeriyottakiSigaraAdedi;
                        sigSagBilg.sigaraIciyorMu = (Enums.evetHayirType)int.Parse(healthInfo.SigaraIciyorMu);
                        sigSagBilg.sigaraIcmeSuresi = healthInfo.SigaraIcmeSuresi;
                        sigSagBilg.sigaraPeriyotTip = (Enums.aliskanlikPeriyodType)int.Parse(healthInfo.SigaraperiyotTip);

                        List<V_SgmIngressInsHealthDet> healthInfoDet = new GenericRepository<V_SgmIngressInsHealthDet>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {req.PolicyId} and ENDORSEMENT_ID = {entranceEndorsement.Id}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

                        List<sigortaliSaglikBilgileriDetayInputType> sigSagBilgDetay = new List<sigortaliSaglikBilgileriDetayInputType>();

                        foreach (V_SgmIngressInsHealthDet itemHealthDet in healthInfoDet)
                        {
                            sigSagBilgDetay.Add(new sigortaliSaglikBilgileriDetayInputType
                            {
                                ameliyatVarmi = itemHealthDet.AmeliyatVarMi,
                                hastalikAciklamasi = itemHealthDet.HastalikAciklamasi,
                                hastalikAdi = itemHealthDet.HastalikAdi,
                                hastalikKod = itemHealthDet.HastalikKod,
                                hastalikUygulamaKod = itemHealthDet.HastalikUygulamaKod.IsInt64() ? long.Parse(itemHealthDet.HastalikUygulamaKod) : 0,
                                sonDurum = itemHealthDet.SonDurum,
                                sureAy = itemHealthDet.SureAy.IsInt() ? int.Parse(itemHealthDet.SureAy) : 0,
                                sureYil = itemHealthDet.SureYil.IsInt() ? int.Parse(itemHealthDet.SureYil) : 0,
                                uygulamaAciklamasi = itemHealthDet.UygulamaAciklamasi
                            });
                        }
                        sigSagBilg.saglikDetayBilgileri = sigSagBilgDetay.ToArray();
                    }


                    foreach (V_SgmIngressExtraPremium itemExtraPrm in ExtraPrm)
                    {
                        sigIndEkp.Add(new sigortaliIndirimEkprimInputType
                        {
                            aciklama = itemExtraPrm.Aciklama,
                            indirimEkprimDegeri = itemExtraPrm.IndirimEkPrimDegeri.IsNumeric() ? decimal.Parse(itemExtraPrm.IndirimEkPrimDegeri) : 0,
                            indirimEkprimKod = itemExtraPrm.IndirimEkPrimKod.IsInt64() ? long.Parse(itemExtraPrm.IndirimEkPrimKod) : 0,
                            kayitNo = itemExtraPrm.KayitNo.IsInt64() ? long.Parse(itemExtraPrm.KayitNo) : 0,
                            tutarMiOranMi = itemExtraPrm.TutarMiOranMi
                        });
                    }

                    foreach (V_SgmIngressInsExemption itemExemption in sgmExemption)
                    {
                        sigUyg.Add(new sigortaliUygulamaInputType
                        {
                            aciklama = itemExemption.Aciklama,
                            hastalikKod = itemExemption.HastalikKod,
                            kayitNo = itemExemption.KayitNo.IsInt64() ? long.Parse(itemExemption.KayitNo) : 0,
                            muafiyetTutarLimiti = itemExemption.MuafiyetTutarLimiti.IsNumeric() ? decimal.Parse(itemExemption.MuafiyetTutarLimiti) : 0,
                            sigortaliUygulamaKod = itemExemption.SigortaliUygulamaKod.IsInt64() ? long.Parse(itemExemption.SigortaliUygulamaKod) : 0,
                        });
                    }


                    var sigInput = new sigortaliBilgileriInputType
                    {
                        brutPrim = item.BrutPrim.IsNumeric() ? decimal.Parse(item.BrutPrim) : 0,
                        eskiPoliceNo = item.EskiPoliceNo,
                        eskiYenilemeNo = item.EskiYenilemeNo.IsInt() ? int.Parse(item.EskiYenilemeNo) : 0,
                        ilkOzelSigortalilikTarihi = DateTime.Parse(item.IlkOzelSigortalilikTarihi),
                        ilkOzelSirketKod = item.IlkOzelSirketKod,
                        kimlikNo = item.KimlikNo,
                        kimlikTipKod = item.KimlikTipKod,
                        meslekKod = item.MeslekKod.IsInt64() ? long.Parse(item.MeslekKod) : 0,
                        netPrim = item.NetPrim.IsNumeric() ? decimal.Parse(item.NetPrim) : 0,
                        sigortaliUyruk = item.SigortaliUyruk == "TC" ? uyrukTuru.TC : uyrukTuru.YABANCI,// (Enums.uyrukTuru)int.Parse(item.SigortaliUyruk),
                        yasadigiIlceKod = item.YasadigiIlceKod,
                        yasadigiIlKod = item.YasadigiIlKod,
                        saglikBilgileriInputType = sigSagBilg,
                        sigortaliIndirimEkprimInputTypeList = sigIndEkp.ToArray(),
                        sigortaliUygulamaList = sigUyg.ToArray()
                    };

                    lstInsured.Add(sigInput);

                }
                sigortaliBilgiDegisiklikInputType inputInsured = new sigortaliBilgiDegisiklikInputType
                {
                    zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi),
                    zeylNo = int.Parse(sgmEndors.ZeylNo),
                    zeylNetPrimi = decimal.Parse(sgmEndors.ZeylNetPrimi),
                    zeylBrutPrimi = decimal.Parse(sgmEndors.ZeylBrutPrimi),
                    zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi),
                    yenilemeNo = int.Parse(sgmEndors.YenilemeNo),
                    sigortaSirketKod = sgmEndors.SigortaSirketKod,
                    sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo),
                    otorizasyonKod = req.Authorization,
                    policeNo = sgmEndors.PoliceNo,
                    policeNetPrimi = decimal.Parse(sgmEndors.PoliceNetPrimi),
                    policeBrutPrimi = decimal.Parse(sgmEndors.PoliceBrutPrimi),
                    sigortali = lstInsured.ToArray()
                };
                #endregion

                log.Request = inputInsured.ToXML(true);

                #region Operation Insured
                if (ServiceType == Enums.SagmerServiceType.KONTROL)
                {
                    var responseSbmInsured = serviceClient.sigortaliBilgiDegisiklikZeyliKontrol(inputInsured, long.Parse(sgmEndors.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                else
                {
                    var responseSbmInsured = serviceClient.sigortaliBilgiDegisiklikZeyli(inputInsured, long.Parse(sgmEndors.CompanyId));
                    SpResponse spResponse = new SpResponse();
                    Endorsement endorsement = new GenericRepository<Endorsement>().FindBy($"ID={req.EndorsementId}").FirstOrDefault();
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;
                        endorsement.SbmStatus = ((int)SagmerStatus.BASARISIZ).ToString();
                        spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                    endorsement.SbmStatus = ((int)SagmerStatus.BASARILI).ToString();
                    spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                }
                #endregion

                #region OK
                response.ResultCode = 1;
                response.ResultDesc = "İşlem Başarılı";
                log.Request = inputInsured.ToXML(true);
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.ResultCode = 0;
                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = ex.Message });

                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                #endregion
            }
            return response;
        }
        #endregion

        #region SBM Travel Policy Service
        //Seyahat Poliçe Kayıt
        public SbmServiceRes SbmTravelPolicyCreate(SbmPolicyCreateReq req, Enums.SagmerServiceType ServiceType)
        {
            SbmServiceRes response = new SbmServiceRes();
            response.FailedErrors = new List<failure>();

            var currentMethod = MethodBase.GetCurrentMethod();
            ServiceClient serviceClient = new ServiceClient();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                SagmerHelper sagmerHelper = new SagmerHelper();
                if (!sagmerHelper.IsSagmerSendViaPolicy(req.PolicyId))
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Bu Poliçenin Sagmere Gönderilme Zorunluluğu Bulunmamaktadır. Altürününü Kontrol Ediniz." });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }

                Endorsement firstEndorsement = new GenericRepository<Endorsement>().FindBy("POLICY_ID=:policyId AND TYPE=:type", orderby: "", parameters: new { policyId = req.PolicyId, type = new Dapper.DbString { Value = ((int)EndorsementType.BASLANGIC).ToString(), Length = 3 } }).FirstOrDefault();
                if (firstEndorsement == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Başlangıç Zeyil Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region GetPolicyData
                V_SgmPolicy sgmPolicy = new GenericRepository<V_SgmPolicy>().FindBy($"POLICY_ID = {req.PolicyId}", fetchDeletedRows: true, fetchHistoricRows: true, orderby: "").FirstOrDefault();
                if (sgmPolicy == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Poliçe Detayı Bulunamadı" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                log.PolicyId = req.PolicyId;
                log.COMPANY_ID = long.Parse(sgmPolicy.CompanyId);
                #endregion

                #region GetInsuredData
                V_SgmIngressEndorsement sgmEndors = new GenericRepository<V_SgmIngressEndorsement>().FindBy($" ENDORSEMENT_ID = {firstEndorsement.Id} AND POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                List<V_SgmIngressEndorsInsured> sgmEndorsInsured = new GenericRepository<V_SgmIngressEndorsInsured>().FindBy($" ENDORSEMENT_ID = {firstEndorsement.Id} AND POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                if (sgmEndors == null || sgmEndorsInsured == null || sgmEndorsInsured.Count < 1)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Sigortalı Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region FillPolicyData
                seyahatPoliceInputType input = new seyahatPoliceInputType();

                input.dovizCinsi = sgmPolicy.DovizCinsi;
                input.endirektSigortaSirketKod = sgmPolicy.EndirektSigortaSirketKod;
                input.eskiPoliceNo = sgmPolicy.EskiPoliceNo;
                input.eskiYenilemeNo = !sgmPolicy.EskiYenilemeNo.IsInt() ? 0 : int.Parse(sgmPolicy.EskiYenilemeNo);
                input.ilKod = sgmPolicy.IlKod;
                input.odemeKod = !sgmPolicy.OdemeKod.IsInt64() ? 0 : long.Parse(sgmPolicy.OdemeKod);
                input.policeBaslamaTarihi = sgmPolicy.PoliceBaslamaTarihi.IsDateTime() ? DateTime.Parse(sgmPolicy.PoliceBaslamaTarihi) : throw new Exception("PoliceBaslamaTarihi Boş!");
                input.policeBitisTarihi = sgmPolicy.PoliceBitisTarihi.IsDateTime() ? DateTime.Parse(sgmPolicy.PoliceBitisTarihi) : throw new Exception("PoliceBitisTarihi Boş!");
                input.policeBrutPrimi = sgmPolicy.PoliceBrutPrimi.IsNumeric() ? decimal.Parse(sgmPolicy.PoliceBrutPrimi) : 0;
                input.policeNetPrimi = sgmPolicy.PoliceNetPrimi.IsNumeric() ? decimal.Parse(sgmPolicy.PoliceNetPrimi) : 0;
                input.policeNo = sgmPolicy.PoliceNo;
                input.policeTanzimTarihi = !sgmPolicy.PoliceTanzimTarihi.IsDateTime() ? DateTime.Now : DateTime.Parse(sgmPolicy.PoliceTanzimTarihi);
                input.policeTip = sgmPolicy.PoliceTip.IsInt() ? (sgmPolicy.PoliceTip == ((int)PolicyType.FERDI).ToString() ? policeTipiType.F : policeTipiType.G) : throw new Exception("PoliceTip Boş!");

                input.sigortaEttirenType = new sigortaEttirenInputType();
                input.sigortaEttirenType.kimlikTipKod = sgmPolicy.SgtKimlikTipKod;
                if (input.sigortaEttirenType.kimlikTipKod != "1" && input.sigortaEttirenType.kimlikTipKod != "2" && input.sigortaEttirenType.kimlikTipKod != "4")
                {
                    input.sigortaEttirenType.ad = sgmPolicy.SgtAd;
                    input.sigortaEttirenType.soyad = sgmPolicy.SgtSoyad;
                    input.sigortaEttirenType.adres = sgmPolicy.SgtAdres;
                    input.sigortaEttirenType.babaAdi = sgmPolicy.SgtBabaAdi;
                    input.sigortaEttirenType.cinsiyet = !sgmPolicy.SgtCinsiyet.IsInt() ? null : (cinsiyet?)Enum.Parse(typeof(cinsiyet), sgmPolicy.SgtCinsiyet);
                    input.sigortaEttirenType.dogumTarihi = !sgmPolicy.SgtDogumTarihi.IsDateTime() ? null : (DateTime?)DateTime.Parse(sgmPolicy.SgtDogumTarihi);
                    input.sigortaEttirenType.dogumYeri = sgmPolicy.SgtDogumYeri;
                }
                input.sigortaEttirenType.kimlikNo = sgmPolicy.SgtKimlikNo;
                input.sigortaEttirenType.sigortaEttirenUyruk = sgmPolicy.SgtSigortaEttirenUyruk == null ? null : (uyrukTuru?)(uyrukTuru)Enum.Parse(typeof(uyrukTuru), sgmPolicy.SgtSigortaEttirenUyruk);

                DateTime? kurulusTarihi = null;
                if (!string.IsNullOrEmpty(sgmPolicy.SgtKurulusTarihi))
                    kurulusTarihi = DateTime.Parse(sgmPolicy.SgtKurulusTarihi);

                input.sigortaEttirenType.kurulusTarihi = kurulusTarihi;
                input.sigortaEttirenType.kurulusYeri = sgmPolicy.SgtKurulusYeri;
                input.sigortaEttirenType.turKod = sgmPolicy.SgtTurKod != null ? (turKod)Enum.Parse(typeof(turKod), sgmPolicy.SgtTurKod) : throw new Exception("SgtTurKod Boş");
                if (input.sigortaEttirenType.kimlikTipKod != "1")
                {
                    input.sigortaEttirenType.ulkeKodu = !sgmPolicy.SgtUlkeKodu.IsInt() ? null : (int?)int.Parse(sgmPolicy.SgtUlkeKodu);
                }
                input.tarifeID = !sgmPolicy.TarifeId.IsInt64() ? 0 : long.Parse(sgmPolicy.TarifeId);
                input.uretimKaynakKod = !sgmPolicy.UretimKaynakKod.IsInt64() ? 0 : long.Parse(sgmPolicy.UretimKaynakKod);
                input.uretimKaynakKurumKod = sgmPolicy.UretimKaynakKurumKod;
                input.vadeSayisi = !sgmPolicy.VadeSayisi.IsInt() ? 0 : int.Parse(sgmPolicy.VadeSayisi);
                input.yenilemeNo = !sgmPolicy.YenilemeNo.IsInt() ? 0 : int.Parse(sgmPolicy.YenilemeNo);
                input.yeniYenilemeGecis = sgmPolicy.YeniYenilemeGecis;
                input.zeylNo = !sgmPolicy.ZeylNo.IsInt() ? 0 : int.Parse(sgmPolicy.ZeylNo);
                #endregion

                #region FillInsuredData
                seyahatSigortaliGirisInputType inputInsured = new seyahatSigortaliGirisInputType();

                inputInsured.zeylTuru = long.Parse(sgmEndors.ZeylTuru);
                inputInsured.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);
                inputInsured.zeylNo = int.Parse(sgmEndors.ZeylNo);
                inputInsured.zeylNetPrimi = decimal.Parse(sgmEndors.ZeylNetPrimi);
                inputInsured.zeylBrutPrimi = decimal.Parse(sgmEndors.ZeylBrutPrimi);
                inputInsured.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
                inputInsured.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
                inputInsured.sigortaSirketKod = sgmEndors.SigortaSirketKod;
                inputInsured.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
                inputInsured.otorizasyonKod = sgmEndors.OtorizasyonKod;
                inputInsured.policeNo = sgmEndors.PoliceNo;
                inputInsured.policeNetPrimi = decimal.Parse(sgmEndors.PoliceNetPrimi);
                inputInsured.policeBrutPrimi = decimal.Parse(sgmEndors.PoliceBrutPrimi);
                List<seyahatSigortaliTeminatliInputType> lstInsured = new List<seyahatSigortaliTeminatliInputType>();
                foreach (V_SgmIngressEndorsInsured item in sgmEndorsInsured)
                {
                    List<V_SgmIngressInsCover> sgmInsuredCover = new GenericRepository<V_SgmIngressInsCover>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {req.PolicyId} and ENDORSEMENT_ID = {firstEndorsement.Id}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                    List<V_SgmIngressExtraPremium> ExtraPrm = new GenericRepository<V_SgmIngressExtraPremium>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {req.PolicyId} and ENDORSEMENT_ID = {firstEndorsement.Id}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

                    List<sigortaliIndirimEkprimInputType> sigIndEkp = new List<sigortaliIndirimEkprimInputType>();
                    List<seyahatSigortaliTeminatInputType> sigTem = new List<seyahatSigortaliTeminatInputType>();

                    foreach (V_SgmIngressExtraPremium itemExtraPrm in ExtraPrm)
                    {
                        sigIndEkp.Add(new sigortaliIndirimEkprimInputType
                        {
                            aciklama = itemExtraPrm.Aciklama,
                            indirimEkprimDegeri = itemExtraPrm.IndirimEkPrimDegeri.IsNumeric() ? decimal.Parse(itemExtraPrm.IndirimEkPrimDegeri) : 0,
                            indirimEkprimKod = itemExtraPrm.IndirimEkPrimKod.IsInt64() ? long.Parse(itemExtraPrm.IndirimEkPrimKod) : 0,
                            kayitNo = itemExtraPrm.KayitNo.IsInt64() ? long.Parse(itemExtraPrm.KayitNo) : 0,
                            tutarMiOranMi = itemExtraPrm.TutarMiOranMi
                        });
                    }
                    foreach (V_SgmIngressInsCover itemInsCover in sgmInsuredCover)
                    {
                        sigTem.Add(new seyahatSigortaliTeminatInputType
                        {
                            cografiKapsamKodu = itemInsCover.CografiKapsamKodu.IsInt64() ? long.Parse(itemInsCover.CografiKapsamKodu) : 0,
                            dovizCinsi = itemInsCover.DovizCinsi,
                            teminatAdedi = itemInsCover.TeminatAdedi.IsInt() ? int.Parse(itemInsCover.TeminatAdedi) : 0,
                            teminatKodu = itemInsCover.TeminatKodu.IsInt64() ? long.Parse(itemInsCover.TeminatKodu) : 0,
                            teminatLimiti = itemInsCover.TeminatLimiti.IsNumeric() ? decimal.Parse(itemInsCover.TeminatLimiti) : 0,
                            teminatLimitKodu = itemInsCover.TeminatLimitKodu.IsInt64() ? long.Parse(itemInsCover.TeminatLimitKodu) : 0,
                            teminatMuafiyeti = itemInsCover.TeminatMuafiyeti.IsNumeric() ? decimal.Parse(itemInsCover.TeminatMuafiyeti) : 0,
                            teminatMuafiyetKodu = itemInsCover.TeminatMuafiyetKodu.IsInt64() ? long.Parse(itemInsCover.TeminatMuafiyetKodu) : 0,
                            teminatNetPrim = itemInsCover.TeminatNetPrim.IsNumeric() ? decimal.Parse(itemInsCover.TeminatNetPrim) : 0,
                            teminatOdemeYuzdesi = itemInsCover.TeminatOdemeYuzdesi.IsNumeric() ? decimal.Parse(itemInsCover.TeminatOdemeYuzdesi) : 0,
                            teminatVaryasyonKodu = itemInsCover.TeminatVaryasyonKodu.IsInt() ? int.Parse(itemInsCover.TeminatVaryasyonKodu) : 0
                        });
                    }


                    var sigInput = new seyahatSigortaliTeminatliInputType
                    {
                        bireySiraNo = item.BireySiraNo,
                        bireyTipKod = item.BireyTipKod.IsInt64() ? long.Parse(item.BireyTipKod) : 0,
                        brutPrim = item.BrutPrim.IsNumeric() ? decimal.Parse(item.BrutPrim) : 0,
                        eskiPoliceNo = item.EskiPoliceNo,
                        eskiYenilemeNo = item.EskiYenilemeNo.IsInt() ? int.Parse(item.EskiYenilemeNo) : 0,
                        kimlikNo = item.KimlikNo,
                        kimlikTipKod = item.KimlikTipKod,
                        netPrim = item.NetPrim.IsNumeric() ? decimal.Parse(item.NetPrim) : 0,
                        sigortaliUyruk = item.SigortaliUyruk == "TC" ? uyrukTuru.TC : uyrukTuru.YABANCI,// (Enums.uyrukTuru)int.Parse(item.SigortaliUyruk),
                        ulkeKodu = item.UlkeKodu.IsInt() ? int.Parse(item.UlkeKodu) : 0,
                        urunPlanKod = item.UrunPlanKod.IsInt64() ? long.Parse(item.UrunPlanKod) : 0,
                        yasadigiIlceKod = item.YasadigiIlceKod,
                        yasadigiIlKod = item.YasadigiIlKod,
                        sigortaliIndirimEkprimInputTypeList = sigIndEkp.ToArray(),
                        sigortaliTeminatInputTypeListField = sigTem.ToArray()
                    };

                    if (item.KimlikTipKod != "1" && item.KimlikTipKod != "2" && item.KimlikTipKod != "4")
                    {
                        sigInput.adres = item.Adres;
                        sigInput.babaAdi = item.BabaAdi;
                        sigInput.sigortaliAd = item.SigortaliAd;
                        sigInput.sigortaliSoyad = item.SigortaliSoyad;
                        sigInput.cinsiyet = (Enums.cinsiyet)int.Parse(item.Cinsiyet);
                        sigInput.dogumTarihi = DateTime.Parse(item.DogumTarihi);
                        sigInput.dogumYeri = item.DogumYeri;
                    }

                    lstInsured.Add(sigInput);

                }
                inputInsured.sigortali = lstInsured.ToArray();
                #endregion

                #region Operation Policy And Insured
                if (ServiceType == Enums.SagmerServiceType.KONTROL)
                {
                    var responseSbmPolice = serviceClient.seyahatPoliceKontrol(input, long.Parse(sgmPolicy.CompanyId));
                    if (!responseSbmPolice.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmPolice.ServiceException.failureList;
                        log.Request = input.ToXML(true);

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                    log.Request = input.ToXML(true);
                    CreateWaitingLog(log);

                    var responseSbmInsured = serviceClient.seyahatSigortaliGirisZeyliKontrol(inputInsured, long.Parse(sgmPolicy.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;
                        log.Request = inputInsured.ToXML(true);

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                else
                {
                    var responseSbmPolice = serviceClient.seyahatPolice(input, long.Parse(sgmPolicy.CompanyId));
                    SpResponse spResponse = new SpResponse();
                    Endorsement endorsement = new GenericRepository<Endorsement>().FindBy($"ID={firstEndorsement.Id}").FirstOrDefault();
                    if (!responseSbmPolice.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmPolice.ServiceException.failureList;
                        log.Request = input.ToXML(true);

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                    log.Request = input.ToXML(true);
                    CreateWaitingLog(log);

                    var responseSbmInsured = serviceClient.seyahatSigortaliGirisZeyli(inputInsured, long.Parse(sgmPolicy.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;
                        log.Request = inputInsured.ToXML(true);
                        endorsement.SbmStatus = ((int)SagmerStatus.BASARISIZ).ToString();
                        spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                    endorsement.SbmStatus = ((int)SagmerStatus.BASARILI).ToString();
                    spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                }
                #endregion

                #region OK
                response.ResultCode = 1;
                response.ResultDesc = "İşlem Başarılı";
                log.Request = inputInsured.ToXML(true);
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.ResultCode = 0;
                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = ex.Message });

                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                #endregion
            }
            return response;
        }

        //Seyahat Sigortalı Gİriş Zeyili
        public SbmServiceRes SbmTravelInsuredEntranceEndorsement(SbmInsuredEntranceReq req, Enums.SagmerServiceType ServiceType)
        {
            SbmServiceRes response = new SbmServiceRes();
            response.FailedErrors = new List<failure>();

            var currentMethod = MethodBase.GetCurrentMethod();
            ServiceClient serviceClient = new ServiceClient();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                SagmerHelper sagmerHelper = new SagmerHelper();
                if (!sagmerHelper.IsSagmerSendViaPolicy(req.PolicyId))
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Bu Poliçenin Sagmere Gönderilme Zorunluluğu Bulunmamaktadır. Altürününü Kontrol Ediniz." });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }

                Endorsement firstEndorsement = new GenericRepository<Endorsement>().FindBy("POLICY_ID=:policyId AND TYPE=:type", orderby: "", parameters: new { policyId = req.PolicyId, type = new Dapper.DbString { Value = ((int)EndorsementType.BASLANGIC).ToString(), Length = 3 } }).FirstOrDefault();
                if (firstEndorsement == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Başlangıç Zeyil Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region GetInsuredData
                V_SgmIngressEndorsement sgmEndors = new GenericRepository<V_SgmIngressEndorsement>().FindBy($" ENDORSEMENT_ID = {firstEndorsement.Id} AND POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                List<V_SgmIngressEndorsInsured> sgmEndorsInsured = new GenericRepository<V_SgmIngressEndorsInsured>().FindBy($" ENDORSEMENT_ID = {firstEndorsement.Id} AND POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                if (sgmEndors == null || sgmEndorsInsured == null || sgmEndorsInsured.Count < 1)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Sigortalı Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region FillInsuredData
                seyahatSigortaliGirisInputType inputInsured = new seyahatSigortaliGirisInputType();

                inputInsured.zeylTuru = long.Parse(sgmEndors.ZeylTuru);
                inputInsured.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);
                inputInsured.zeylNo = int.Parse(sgmEndors.ZeylNo);
                inputInsured.zeylNetPrimi = decimal.Parse(sgmEndors.ZeylNetPrimi);
                inputInsured.zeylBrutPrimi = decimal.Parse(sgmEndors.ZeylBrutPrimi);
                inputInsured.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
                inputInsured.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
                inputInsured.sigortaSirketKod = sgmEndors.SigortaSirketKod;
                inputInsured.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
                inputInsured.otorizasyonKod = sgmEndors.OtorizasyonKod;
                inputInsured.policeNo = sgmEndors.PoliceNo;
                inputInsured.policeNetPrimi = decimal.Parse(sgmEndors.PoliceNetPrimi);
                inputInsured.policeBrutPrimi = decimal.Parse(sgmEndors.PoliceBrutPrimi);
                List<seyahatSigortaliTeminatliInputType> lstInsured = new List<seyahatSigortaliTeminatliInputType>();
                foreach (V_SgmIngressEndorsInsured item in sgmEndorsInsured)
                {
                    List<V_SgmIngressInsCover> sgmInsuredCover = new GenericRepository<V_SgmIngressInsCover>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {req.PolicyId} and ENDORSEMENT_ID = {firstEndorsement.Id}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                    List<V_SgmIngressExtraPremium> ExtraPrm = new GenericRepository<V_SgmIngressExtraPremium>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {req.PolicyId} and ENDORSEMENT_ID = {firstEndorsement.Id}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

                    List<sigortaliIndirimEkprimInputType> sigIndEkp = new List<sigortaliIndirimEkprimInputType>();
                    List<seyahatSigortaliTeminatInputType> sigTem = new List<seyahatSigortaliTeminatInputType>();

                    foreach (V_SgmIngressExtraPremium itemExtraPrm in ExtraPrm)
                    {
                        sigIndEkp.Add(new sigortaliIndirimEkprimInputType
                        {
                            aciklama = itemExtraPrm.Aciklama,
                            indirimEkprimDegeri = itemExtraPrm.IndirimEkPrimDegeri.IsNumeric() ? decimal.Parse(itemExtraPrm.IndirimEkPrimDegeri) : 0,
                            indirimEkprimKod = itemExtraPrm.IndirimEkPrimKod.IsInt64() ? long.Parse(itemExtraPrm.IndirimEkPrimKod) : 0,
                            kayitNo = itemExtraPrm.KayitNo.IsInt64() ? long.Parse(itemExtraPrm.KayitNo) : 0,
                            tutarMiOranMi = itemExtraPrm.TutarMiOranMi
                        });
                    }
                    foreach (V_SgmIngressInsCover itemInsCover in sgmInsuredCover)
                    {
                        sigTem.Add(new seyahatSigortaliTeminatInputType
                        {
                            cografiKapsamKodu = itemInsCover.CografiKapsamKodu.IsInt64() ? long.Parse(itemInsCover.CografiKapsamKodu) : 0,
                            dovizCinsi = itemInsCover.DovizCinsi,
                            teminatAdedi = itemInsCover.TeminatAdedi.IsInt() ? int.Parse(itemInsCover.TeminatAdedi) : 0,
                            teminatKodu = itemInsCover.TeminatKodu.IsInt64() ? long.Parse(itemInsCover.TeminatKodu) : 0,
                            teminatLimiti = itemInsCover.TeminatLimiti.IsNumeric() ? decimal.Parse(itemInsCover.TeminatLimiti) : 0,
                            teminatLimitKodu = itemInsCover.TeminatLimitKodu.IsInt64() ? long.Parse(itemInsCover.TeminatLimitKodu) : 0,
                            teminatMuafiyeti = itemInsCover.TeminatMuafiyeti.IsNumeric() ? decimal.Parse(itemInsCover.TeminatMuafiyeti) : 0,
                            teminatMuafiyetKodu = itemInsCover.TeminatMuafiyetKodu.IsInt64() ? long.Parse(itemInsCover.TeminatMuafiyetKodu) : 0,
                            teminatNetPrim = itemInsCover.TeminatNetPrim.IsNumeric() ? decimal.Parse(itemInsCover.TeminatNetPrim) : 0,
                            teminatOdemeYuzdesi = itemInsCover.TeminatOdemeYuzdesi.IsNumeric() ? decimal.Parse(itemInsCover.TeminatOdemeYuzdesi) : 0,
                            teminatVaryasyonKodu = itemInsCover.TeminatVaryasyonKodu.IsInt() ? int.Parse(itemInsCover.TeminatVaryasyonKodu) : 0
                        });
                    }


                    var sigInput = new seyahatSigortaliTeminatliInputType
                    {
                        bireySiraNo = item.BireySiraNo,
                        bireyTipKod = item.BireyTipKod.IsInt64() ? long.Parse(item.BireyTipKod) : 0,
                        brutPrim = item.BrutPrim.IsNumeric() ? decimal.Parse(item.BrutPrim) : 0,
                        eskiPoliceNo = item.EskiPoliceNo,
                        eskiYenilemeNo = item.EskiYenilemeNo.IsInt() ? int.Parse(item.EskiYenilemeNo) : 0,
                        kimlikNo = item.KimlikNo,
                        kimlikTipKod = item.KimlikTipKod,
                        netPrim = item.NetPrim.IsNumeric() ? decimal.Parse(item.NetPrim) : 0,
                        sigortaliUyruk = item.SigortaliUyruk == "TC" ? uyrukTuru.TC : uyrukTuru.YABANCI,// (Enums.uyrukTuru)int.Parse(item.SigortaliUyruk),
                        ulkeKodu = item.UlkeKodu.IsInt() ? int.Parse(item.UlkeKodu) : 0,
                        urunPlanKod = item.UrunPlanKod.IsInt64() ? long.Parse(item.UrunPlanKod) : 0,
                        yasadigiIlceKod = item.YasadigiIlceKod,
                        yasadigiIlKod = item.YasadigiIlKod,
                        sigortaliIndirimEkprimInputTypeList = sigIndEkp.ToArray(),
                        sigortaliTeminatInputTypeListField = sigTem.ToArray()
                    };

                    if (item.KimlikTipKod != "1" && item.KimlikTipKod != "2" && item.KimlikTipKod != "4")
                    {
                        sigInput.adres = item.Adres;
                        sigInput.babaAdi = item.BabaAdi;
                        sigInput.sigortaliAd = item.SigortaliAd;
                        sigInput.sigortaliSoyad = item.SigortaliSoyad;
                        sigInput.cinsiyet = (Enums.cinsiyet)int.Parse(item.Cinsiyet);
                        sigInput.dogumTarihi = DateTime.Parse(item.DogumTarihi);
                        sigInput.dogumYeri = item.DogumYeri;
                    }

                    lstInsured.Add(sigInput);

                }
                inputInsured.sigortali = lstInsured.ToArray();
                #endregion

                log.Request = inputInsured.ToXML(true);

                #region Operation 
                if (ServiceType == Enums.SagmerServiceType.KONTROL)
                {
                    var responseSbmInsured = serviceClient.seyahatSigortaliGirisZeyliKontrol(inputInsured, long.Parse(sgmEndors.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                else
                {
                    var responseSbmInsured = serviceClient.seyahatSigortaliGirisZeyli(inputInsured, long.Parse(sgmEndors.CompanyId));
                    SpResponse spResponse = new SpResponse();
                    Endorsement endorsement = new GenericRepository<Endorsement>().FindBy($"ID={req.EndorsementId}").FirstOrDefault();
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;
                        endorsement.SbmStatus = ((int)SagmerStatus.BASARISIZ).ToString();
                        spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                    endorsement.SbmStatus = ((int)SagmerStatus.BASARILI).ToString();
                    spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                }
                #endregion

                #region OK
                response.ResultCode = 1;
                response.ResultDesc = "İşlem Başarılı";
                log.Request = inputInsured.ToXML(true);
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.ResultCode = 0;
                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = ex.Message });

                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                #endregion
            }
            return response;
        }

        //Seyahat Sigortalı Çıkış Zeyili
        public SbmServiceRes SbmTravelInsuredExitEndorsement(SbmInsuredExitReq req, Enums.SagmerServiceType ServiceType)
        {
            SbmServiceRes response = new SbmServiceRes();
            response.FailedErrors = new List<failure>();

            var currentMethod = MethodBase.GetCurrentMethod();
            ServiceClient serviceClient = new ServiceClient();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                SagmerHelper sagmerHelper = new SagmerHelper();
                if (!sagmerHelper.IsSagmerSendViaPolicy(req.PolicyId))
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Bu Poliçenin Sagmere Gönderilme Zorunluluğu Bulunmamaktadır. Altürününü Kontrol Ediniz." });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }

                Endorsement entranceEndorsement = new GenericRepository<Endorsement>().FindBy("ID=:endorsementId", orderby: "", parameters: new { endorsementId = req.EndorsementId }).FirstOrDefault();
                if (entranceEndorsement == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Zeyil Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region GetInsuredData
                V_SgmEgressEndorsement sgmEndors = new GenericRepository<V_SgmEgressEndorsement>().FindBy($"ENDORSEMENT_ID = {req.EndorsementId} AND POLICY_ID = {req.PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (sgmEndors == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Sigortalı Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                log.PolicyId = req.PolicyId;
                log.COMPANY_ID = long.Parse(sgmEndors.CompanyId);
                #endregion

                #region FillInsuredData
                seyahatSigortaliCikisInputType input = new seyahatSigortaliCikisInputType
                {
                    brutPrim = sgmEndors.BrutPrim.IsNumeric() ? decimal.Parse(sgmEndors.BrutPrim) : 0,
                    kimlikNo = sgmEndors.KimlikNo,
                    kimlikTipKod = sgmEndors.KimlikTipKod.IsInt() ? sgmEndors.KimlikTipKod : "",
                    netPrim = sgmEndors.NetPrim.IsNumeric() ? decimal.Parse(sgmEndors.NetPrim) : 0,
                    otorizasyonKod = sgmEndors.OtorizasyonKod,
                    policeBrutPrimi = sgmEndors.PoliceBrutPrimi.IsNumeric() ? decimal.Parse(sgmEndors.PoliceBrutPrimi) : 0,
                    policeNetPrimi = sgmEndors.PoliceNetPrimi.IsNumeric() ? decimal.Parse(sgmEndors.PoliceNetPrimi) : 0,
                    policeNo = sgmEndors.PoliceNo,
                    sbmSagmerNo = sgmEndors.SbmSagmerNo.IsInt64() ? long.Parse(sgmEndors.SbmSagmerNo) : 0,
                    sigortaSirketKod = sgmEndors.SigortaSirketKod,
                    yenilemeNo = sgmEndors.YenilemeNo.IsInt() ? int.Parse(sgmEndors.YenilemeNo) : 0,
                    zeylBaslangicTarihi = sgmEndors.ZeylBaslangicTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylBaslangicTarihi) : DateTime.Now,
                    zeylBrutPrimi = sgmEndors.ZeylBrutPrimi.IsNumeric() ? decimal.Parse(sgmEndors.ZeylBrutPrimi) : 0,
                    zeylNetPrimi = sgmEndors.ZeylNetPrimi.IsNumeric() ? decimal.Parse(sgmEndors.ZeylNetPrimi) : 0,
                    zeylNo = sgmEndors.ZeylNo.IsInt() ? int.Parse(sgmEndors.ZeylNo) : 0,
                    zeylTanzimTarihi = sgmEndors.ZeylTanzimTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylTanzimTarihi) : DateTime.Now,
                };
                #endregion

                log.Request = input.ToXML(true);

                #region Operation
                if (ServiceType == Enums.SagmerServiceType.KONTROL)
                {
                    var responseSbmInsured = serviceClient.seyahatSigortaliCikisZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                else
                {
                    var responseSbmInsured = serviceClient.seyahatSigortaliCikisZeyli(input, long.Parse(sgmEndors.CompanyId));
                    SpResponse spResponse = new SpResponse();
                    Endorsement endorsement = new GenericRepository<Endorsement>().FindBy($"ID={req.EndorsementId}").FirstOrDefault();
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;
                        endorsement.SbmStatus = ((int)SagmerStatus.BASARISIZ).ToString();
                        spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                    endorsement.SbmStatus = ((int)SagmerStatus.BASARILI).ToString();
                    spResponse = new GenericRepository<Endorsement>().Insert(endorsement);
                }
                #endregion

                #region OK
                response.ResultCode = 1;
                response.ResultDesc = "İşlem Başarılı";
                log.Request = input.ToXML(true);
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.ResultCode = 0;
                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = ex.Message });

                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                #endregion
            }
            return response;
        }
        #endregion

        #region SBM Claim Service
        //Hasar Giriş
        public SbmServiceRes SbmClaimCreate(SbmClaimReq req, Enums.SagmerServiceType ServiceType)
        {
            SbmServiceRes response = new SbmServiceRes();
            response.FailedErrors = new List<failure>();

            var currentMethod = MethodBase.GetCurrentMethod();
            ServiceClient serviceClient = new ServiceClient();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                SagmerHelper sagmerHelper = new SagmerHelper();
                if (!sagmerHelper.IsSagmerSendViaClaim(req.ClaimId))
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Bu Hasarın Sagmere Gönderilme Zorunluluğu Bulunmamaktadır." });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region Get Data
                V_SgmClaimInsert claimIns = new GenericRepository<V_SgmClaimInsert>().FindBy($"CLAIM_ID = {req.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                List<V_SgmClaimBill> claimBill = new GenericRepository<V_SgmClaimBill>().FindBy($"CLAIM_ID = {req.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                List<V_SgmClaimCover> claimCover = new GenericRepository<V_SgmClaimCover>().FindBy($"CLAIM_ID = {req.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                List<V_SgmClaimIcd> claimIcd = new GenericRepository<V_SgmClaimIcd>().FindBy($"CLAIM_ID = {req.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                if (claimIns == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Hasar Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region Fill Data
                List<faturaType> bill = new List<faturaType>();
                List<saglikKurumType> sagKur = new List<saglikKurumType>();
                List<teminatOdemeType> temOde = new List<teminatOdemeType>();
                List<icdType> icd = new List<icdType>();

                foreach (V_SgmClaimIcd _icd in claimIcd)
                {
                    icd.Add(new icdType
                    {
                        donemNo = _icd.DonemNo.IsInt() ? int.Parse(_icd.DonemNo) : 0,
                        iCDNo = _icd.IcdNo,
                        iCDTip = _icd.IcdTip.IsInt() ? int.Parse(_icd.IcdTip) : 0,
                        teminatNo = _icd.TeminatNo.IsInt() ? int.Parse(_icd.TeminatNo) : 0,
                        varyasyonNo = _icd.VaryasyonNo.IsInt() ? int.Parse(_icd.VaryasyonNo) : 0
                    });
                }
                foreach (V_SgmClaimCover clmCover in claimCover)
                {
                    temOde.Add(new teminatOdemeType
                    {
                        donemNo = clmCover.DonemNo.IsInt() ? int.Parse(clmCover.DonemNo) : 0,
                        muallakTutariTL = clmCover.MuallakTutariTL.IsNumeric() ? decimal.Parse(clmCover.MuallakTutariTL) : 0,
                        odemeSiraNo = clmCover.OdemeSiraNo.IsInt64() ? long.Parse(clmCover.OdemeSiraNo) : 0,
                        odemeTipi = clmCover.OdemeTipi.IsInt() ? (Enums.odemeTipiType)int.Parse(clmCover.OdemeTipi) : odemeTipiType.ODEME,
                        odemeTutariTL = clmCover.OdemeTutariTL.IsNumeric() ? decimal.Parse(clmCover.OdemeTutariTL) : 0,
                        teminatNo = clmCover.TeminatNo.IsInt() ? int.Parse(clmCover.TeminatNo) : 0,
                        varyasyonNo = clmCover.VaryasyonNo.IsInt() ? int.Parse(clmCover.VaryasyonNo) : 0
                    });
                }
                sagKur.Add(new saglikKurumType
                {
                    kurumIlcesi = claimIns.KurumIlcesi,
                    kurumIli = claimIns.KurumIli,
                    kurumUlke = claimIns.KurumUlke,
                    saglikKurumNo = claimIns.SaglikKurumNo,
                    saglikKurumNoTipi = claimIns.SaglikKurumNoTipi,
                    teminatList = temOde.ToArray()
                });

                if (claimBill.Count > 0)
                {
                    foreach (V_SgmClaimBill fat in claimBill)
                    {
                        bill.Add(new faturaType
                        {
                            ettn = fat.Ettn,
                            faturaNo = fat.FaturaNo,
                            faturaTarihi = fat.FaturaTarihi.IsDateTime() ? DateTime.Parse(fat.FaturaTarihi) : DateTime.Now,
                            faturaTutariTL = fat.FaturaTutariTl.IsNumeric() ? decimal.Parse(fat.FaturaTutariTl) : 0,
                            faturaTutariTLOdenen = fat.FaturaTutariTlOdenen.IsNumeric() ? decimal.Parse(fat.FaturaTutariTlOdenen) : 0,
                            paraBirimi = fat.ParaBirimi
                        });
                    }
                }

                dosyaInputType input = new dosyaInputType
                {
                    dosyaRedNedeni = claimIns.DosyaRedNedeni.IsInt64() ? long.Parse(claimIns.DosyaRedNedeni) : 0,
                    ihbarTarihi = claimIns.IhbarTarihi.IsDateTime() ? DateTime.Parse(claimIns.IhbarTarihi) : DateTime.Now,
                    kimlikNo = claimIns.KimlikNo,
                    kimlikTipKod = claimIns.KimlikTipKodu,
                    muallakOdemeTarihi = claimIns.MuallakOdemeTarihi.IsDateTime() ? DateTime.Parse(claimIns.MuallakOdemeTarihi) : DateTime.Now,
                    muallakTutariTL = claimIns.MuallakTutariTL.IsNumeric() ? decimal.Parse(claimIns.MuallakTutariTL) : 0,
                    odemeTutariTL = claimIns.OdemeTutarTL.IsNumeric() ? decimal.Parse(claimIns.OdemeTutarTL) : 0,
                    odemeYeri = claimIns.OdemeYeri.IsInt() ? (Enums.odemeYeriType)int.Parse(claimIns.OdemeYeri) : odemeYeriType.ANLASMALI,
                    olayTarihi = claimIns.OlayTarihi.IsDateTime() ? DateTime.Parse(claimIns.OlayTarihi) : DateTime.Now,
                    otorizasyonKod = claimIns.OtorizasyonKodu,
                    paraBirimi = claimIns.ParaBirimi,
                    provizyonTarihi = claimIns.ProvizyonTarihi.IsDateTime() ? DateTime.Parse(claimIns.ProvizyonTarihi) : DateTime.Now,
                    sbmSagmerNo = claimIns.SbmSagmerNo.IsInt64() ? long.Parse(claimIns.SbmSagmerNo) : 0,
                    tazminatDosyaDurumu = claimIns.TazminatDosyaDurumu.IsInt64() ? long.Parse(claimIns.TazminatDosyaDurumu) : 0,
                    tazminatDosyaNo = claimIns.TazminatDosyaNo,
                    tazminatSiraNo = claimIns.TazminatSiraNo.IsInt64() ? long.Parse(claimIns.TazminatSiraNo) : 0,
                    tedaviTipi = claimIns.TedaviTipi.IsInt() ? (Enums.tedaviTipiType)int.Parse(claimIns.TedaviTipi) : tedaviTipiType.AYAKTA,
                    faturaList = bill.ToArray(),
                    saglikKurumList = sagKur.ToArray(),
                    icdList = icd.ToArray()
                };
                #endregion

                log.Request = claimIns.ToXML(true);

                #region Operation
                if (ServiceType == Enums.SagmerServiceType.KONTROL)
                {
                    var responseSbmInsured = serviceClient.tazminatDosyaGirisKontrol(input, long.Parse(claimIns.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                else
                {
                    var responseSbmInsured = serviceClient.tazminatDosyaGiris(input, long.Parse(claimIns.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                #endregion

                #region OK
                response.ResultCode = 1;
                response.ResultDesc = "İşlem Başarılı";
                log.Request = input.ToXML(true);
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.ResultCode = 0;
                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = ex.Message });

                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                #endregion
            }
            return response;
        }

        //Hasar Güncelle
        public SbmServiceRes SbmClaimUpdate(SbmClaimReq req, Enums.SagmerServiceType ServiceType)
        {
            SbmServiceRes response = new SbmServiceRes();
            response.FailedErrors = new List<failure>();

            var currentMethod = MethodBase.GetCurrentMethod();
            ServiceClient serviceClient = new ServiceClient();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                SagmerHelper sagmerHelper = new SagmerHelper();
                if (!sagmerHelper.IsSagmerSendViaClaim(req.ClaimId))
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Bu Hasarın Sagmere Gönderilme Zorunluluğu Bulunmamaktadır." });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region Get Data
                V_SgmClaimInsert claimIns = new GenericRepository<V_SgmClaimInsert>().FindBy($"CLAIM_ID = {req.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                List<V_SgmClaimIcd> claimIcd = new GenericRepository<V_SgmClaimIcd>().FindBy($"CLAIM_ID = {req.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                if (claimIns == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Hasar Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region Fill Data
                List<saglikKurumUpdateType> sagKur = new List<saglikKurumUpdateType>();
                List<icdInputType> icd = new List<icdInputType>();

                foreach (V_SgmClaimIcd _icd in claimIcd)
                {
                    icd.Add(new icdInputType
                    {
                        islemTipi = _icd.IslemTipi.IsInt() ? (Enums.islemTipiType)int.Parse(_icd.IslemTipi) : islemTipiType.GUNCELLEME,
                        donemNo = _icd.DonemNo.IsInt() ? int.Parse(_icd.DonemNo) : 0,
                        iCDNo = _icd.IcdNo,
                        iCDTip = _icd.IcdTip.IsInt() ? int.Parse(_icd.IcdTip) : 0,
                        teminatNo = _icd.TeminatNo.IsInt() ? int.Parse(_icd.TeminatNo) : 0,
                        varyasyonNo = _icd.VaryasyonNo.IsInt() ? int.Parse(_icd.VaryasyonNo) : 0
                    });
                }
                sagKur.Add(new saglikKurumUpdateType
                {
                    kurumIlcesi = claimIns.KurumIlcesi,
                    kurumIli = claimIns.KurumIli,
                    kurumUlke = claimIns.KurumUlke,
                    saglikKurumNo = claimIns.SaglikKurumNo,
                    saglikKurumNoTipi = claimIns.SaglikKurumNoTipi,
                    kurumIlcesiYeni = claimIns.KurumIlcesiYeni,
                    kurumIliYeni = claimIns.KurumIliYeni,
                    kurumUlkeYeni = claimIns.KurumUlkeYeni,
                    saglikKurumNoTipiYeni = claimIns.SaglikKurumNoTipiYeni,
                    saglikKurumNoYeni = claimIns.SaglikKurumNoYeni
                });

                dosyaUpdateType input = new dosyaUpdateType
                {
                    dosyaRedNedeni = claimIns.DosyaRedNedeni.IsInt64() ? long.Parse(claimIns.DosyaRedNedeni) : 0,
                    ihbarTarihi = claimIns.IhbarTarihi.IsDateTime() ? DateTime.Parse(claimIns.IhbarTarihi) : DateTime.Now,
                    odemeYeri = claimIns.OdemeYeri.IsInt() ? (Enums.odemeYeriType)int.Parse(claimIns.OdemeYeri) : odemeYeriType.ANLASMALI,
                    olayTarihi = claimIns.OlayTarihi.IsDateTime() ? DateTime.Parse(claimIns.OlayTarihi) : DateTime.Now,
                    otorizasyonKod = claimIns.OtorizasyonKodu,
                    paraBirimi = claimIns.ParaBirimi,
                    provizyonTarihi = claimIns.ProvizyonTarihi.IsDateTime() ? DateTime.Parse(claimIns.ProvizyonTarihi) : DateTime.Now,
                    sbmSagmerNo = claimIns.SbmSagmerNo.IsInt64() ? long.Parse(claimIns.SbmSagmerNo) : 0,
                    tazminatDosyaDurumu = claimIns.TazminatDosyaDurumu.IsInt64() ? long.Parse(claimIns.TazminatDosyaDurumu) : 0,
                    tazminatDosyaNo = claimIns.TazminatDosyaNo,
                    tedaviTipi = claimIns.TedaviTipi.IsInt() ? (Enums.tedaviTipiType)int.Parse(claimIns.TedaviTipi) : tedaviTipiType.AYAKTA,
                    saglikKurumList = sagKur.ToArray(),
                    icdList = icd.ToArray(),
                    dosyaGuncellemeTarihi = claimIns.DosyaGuncellemeTarihi.IsDateTime() ? DateTime.Parse(claimIns.DosyaGuncellemeTarihi) : DateTime.Now
                };
                #endregion

                log.Request = claimIns.ToXML(true);

                #region Operation
                if (ServiceType == Enums.SagmerServiceType.KONTROL)
                {
                    var responseSbmInsured = serviceClient.tazminatDosyaGuncellemeKontrol(input, long.Parse(claimIns.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                else
                {
                    var responseSbmInsured = serviceClient.tazminatDosyaGuncelleme(input, long.Parse(claimIns.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                #endregion

                #region OK
                response.ResultCode = 1;
                response.ResultDesc = "İşlem Başarılı";
                log.Request = input.ToXML(true);
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.ResultCode = 0;
                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = ex.Message });

                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                #endregion
            }
            return response;
        }

        //Hasar Fatura Giriş
        public SbmServiceRes SbmClaimEntrancePayroll(SbmClaimReq req, Enums.SagmerServiceType ServiceType)
        {
            SbmServiceRes response = new SbmServiceRes();
            response.FailedErrors = new List<failure>();

            var currentMethod = MethodBase.GetCurrentMethod();
            ServiceClient serviceClient = new ServiceClient();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                SagmerHelper sagmerHelper = new SagmerHelper();
                if (!sagmerHelper.IsSagmerSendViaClaim(req.ClaimId))
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Bu Hasarın Sagmere Gönderilme Zorunluluğu Bulunmamaktadır." });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region Get Data
                V_SgmClaimInsert claimIns = new GenericRepository<V_SgmClaimInsert>().FindBy($"CLAIM_ID = {req.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                List<V_SgmClaimBill> claimBill = new GenericRepository<V_SgmClaimBill>().FindBy($"CLAIM_ID = {req.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                if (claimIns == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Hasar Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region Fill Data
                List<faturaType> bill = new List<faturaType>();

                if (claimBill.Count > 0)
                {
                    foreach (V_SgmClaimBill fat in claimBill)
                    {
                        bill.Add(new faturaType
                        {
                            ettn = fat.Ettn,
                            faturaNo = fat.FaturaNo,
                            faturaTarihi = fat.FaturaTarihi.IsDateTime() ? DateTime.Parse(fat.FaturaTarihi) : DateTime.Now,
                            faturaTutariTL = fat.FaturaTutariTl.IsNumeric() ? decimal.Parse(fat.FaturaTutariTl) : 0,
                            faturaTutariTLOdenen = fat.FaturaTutariTlOdenen.IsNumeric() ? decimal.Parse(fat.FaturaTutariTlOdenen) : 0,
                            paraBirimi = fat.ParaBirimi
                        });
                    }
                }

                dosyaFaturaInputType input = new dosyaFaturaInputType
                {
                    sbmSagmerNo = claimIns.SbmSagmerNo.IsInt64() ? long.Parse(claimIns.SbmSagmerNo) : 0,
                    tazminatDosyaNo = claimIns.TazminatDosyaNo,
                    faturaList = bill.ToArray()
                };
                #endregion

                log.Request = claimIns.ToXML(true);

                #region Operation
                if (ServiceType == Enums.SagmerServiceType.KONTROL)
                {
                    var responseSbmInsured = serviceClient.tazminatFaturaGirisKontrol(input, long.Parse(claimIns.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                else
                {
                    var responseSbmInsured = serviceClient.tazminatFaturaGiris(input, long.Parse(claimIns.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                #endregion

                #region OK
                response.ResultCode = 1;
                response.ResultDesc = "İşlem Başarılı";
                log.Request = input.ToXML(true);
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.ResultCode = 0;
                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = ex.Message });

                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                #endregion
            }
            return response;
        }

        //Hasar Ödeme
        public SbmServiceRes SbmClaimPayment(SbmClaimReq req, Enums.SagmerServiceType ServiceType)
        {
            SbmServiceRes response = new SbmServiceRes();
            response.FailedErrors = new List<failure>();

            var currentMethod = MethodBase.GetCurrentMethod();
            ServiceClient serviceClient = new ServiceClient();

            #region Incoming
            IntegrationLog log = new IntegrationLog
            {
                WsName = currentMethod.DeclaringType.FullName,
                WsMethod = currentMethod.Name,
                Request = req.ToXML(true),
                WsRequestDate = DateTime.Now,
            };
            CreateIncomingLog(log);
            #endregion

            try
            {
                #region Validation
                SagmerHelper sagmerHelper = new SagmerHelper();
                if (!sagmerHelper.IsSagmerSendViaClaim(req.ClaimId))
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Bu Hasarın Sagmere Gönderilme Zorunluluğu Bulunmamaktadır." });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region Get Data
                V_SgmClaimInsert claimIns = new GenericRepository<V_SgmClaimInsert>().FindBy($"CLAIM_ID = {req.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                List<V_SgmClaimCover> claimCover = new GenericRepository<V_SgmClaimCover>().FindBy($"CLAIM_ID = {req.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                if (claimIns == null)
                {
                    response.ResultCode = 0;
                    response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                    response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = "Hasar Bilgisi Bulunamadı!" });

                    CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                    return response;
                }
                #endregion

                #region Fill Data
                List<teminatOdemeType> temOde = new List<teminatOdemeType>();
                List<saglikKurumType> sagKur = new List<saglikKurumType>();

                if (claimCover.Count > 0)
                {
                    foreach (V_SgmClaimCover clmCover in claimCover)
                    {
                        temOde.Add(new teminatOdemeType
                        {
                            donemNo = clmCover.DonemNo.IsInt() ? int.Parse(clmCover.DonemNo) : 0,
                            muallakTutariTL = clmCover.MuallakTutariTL.IsNumeric() ? decimal.Parse(clmCover.MuallakTutariTL) : 0,
                            odemeSiraNo = clmCover.OdemeSiraNo.IsInt64() ? long.Parse(clmCover.OdemeSiraNo) : 0,
                            odemeTipi = clmCover.OdemeTipi.IsInt() ? (Enums.odemeTipiType)int.Parse(clmCover.OdemeTipi) : odemeTipiType.ODEME,
                            odemeTutariTL = clmCover.OdemeTutariTL.IsNumeric() ? decimal.Parse(clmCover.OdemeTutariTL) : 0,
                            teminatNo = clmCover.TeminatNo.IsInt() ? int.Parse(clmCover.TeminatNo) : 0,
                            varyasyonNo = clmCover.VaryasyonNo.IsInt() ? int.Parse(clmCover.VaryasyonNo) : 0
                        });
                    }
                }
                sagKur.Add(new saglikKurumType
                {
                    kurumIlcesi = claimIns.KurumIlcesi,
                    kurumIli = claimIns.KurumIli,
                    kurumUlke = claimIns.KurumUlke,
                    saglikKurumNo = claimIns.SaglikKurumNo,
                    saglikKurumNoTipi = claimIns.SaglikKurumNoTipi,
                    teminatList = temOde.ToArray()
                });

                dosyaTazminatOdemeInputType input = new dosyaTazminatOdemeInputType
                {
                    dosyaRedNedeni = claimIns.DosyaRedNedeni.IsInt64() ? long.Parse(claimIns.DosyaRedNedeni) : 0,
                    muallakOdemeTarihi = claimIns.MuallakOdemeTarihi.IsDateTime() ? DateTime.Parse(claimIns.MuallakOdemeTarihi) : DateTime.Now,
                    muallakTutariTL = claimIns.MuallakTutariTL.IsNumeric() ? decimal.Parse(claimIns.MuallakTutariTL) : 0,
                    odemeTutariTL = claimIns.OdemeTutarTL.IsNumeric() ? decimal.Parse(claimIns.OdemeTutarTL) : 0,
                    otorizasyonKod = claimIns.OtorizasyonKodu,
                    sbmSagmerNo = claimIns.SbmSagmerNo.IsInt64() ? long.Parse(claimIns.SbmSagmerNo) : 0,
                    tazminatDosyaDurumu = claimIns.TazminatDosyaDurumu.IsInt64() ? long.Parse(claimIns.TazminatDosyaDurumu) : 0,
                    tazminatDosyaNo = claimIns.TazminatDosyaNo,
                    saglikKurumList = sagKur.ToArray()
                };
                #endregion

                log.Request = claimIns.ToXML(true);

                #region Operation
                if (ServiceType == Enums.SagmerServiceType.KONTROL)
                {
                    var responseSbmInsured = serviceClient.tazminatOdemeKontrol(input, long.Parse(claimIns.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                else
                {
                    var responseSbmInsured = serviceClient.tazminatOdeme(input, long.Parse(claimIns.CompanyId));
                    if (!responseSbmInsured.Data.islemBasarili)
                    {
                        response.ResultCode = 0;
                        response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                        response.FailedErrors = responseSbmInsured.ServiceException.failureList;

                        CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                        return response;
                    }
                }
                #endregion

                #region OK
                response.ResultCode = 1;
                response.ResultDesc = "İşlem Başarılı";
                log.Request = input.ToXML(true);
                CreateOKLog(log, response.ToXML(true));
                #endregion
            }
            catch (Exception ex)
            {
                #region Catch Error
                response.ResultCode = 0;
                response.ResultDesc = "HATA - FailedErrors Listesini Kontrol Ediniz";
                response.FailedErrors.Add(new InputOutputTypes.failure { code = "999", message = ex.Message });

                CreateFailedLog(log, response.ToXML(true), response.ResultDesc);
                #endregion
            }
            return response;
        }
        #endregion
    }
}
