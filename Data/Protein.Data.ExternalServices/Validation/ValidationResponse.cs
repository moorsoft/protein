﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Validation
{
    public class ValidationResponse
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public bool IsValid { get; set; } = false;
    }
}
