﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Validation
{
    public class Validator
    {
        public static ValidationResponse Worker<T>(T input)
        {
            ValidationResponse response = new ValidationResponse();

            foreach (PropertyInfo prop in input.GetType().GetProperties())
            {
                var propValue = prop.GetValue(input, null);
                RequiredAttribute reqAttr = prop.GetCustomAttributes(false).FirstOrDefault(a => a.GetType().Name == typeof(RequiredAttribute).Name) as RequiredAttribute;

                if (prop.GetCustomAttributes(false).Any(attr => attr.GetType().Name == typeof(RequiredAttribute).Name))
                {
                    if (propValue == null)
                    {
                        response.Code = "999"; response.Description = $"Zorunlu alan: {prop.Name}";
                    }
                    else if (string.IsNullOrEmpty(propValue.ToString().Trim()))
                    {
                        response.Code = "999"; response.Description = $"Zorunlu alan: {prop.Name}";
                    }
                    else response.IsValid = true;
                    if (!response.IsValid) return response;
                }
            }
            return response;
        }
    }
}
