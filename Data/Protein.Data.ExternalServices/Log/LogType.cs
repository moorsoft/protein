﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Data.ExternalServices.Log
{
    public enum LogType
    {
        INCOMING = 0,
        WAITING = 1,
        COMPLETED = 2,
        FAILED = 3
    }
}
