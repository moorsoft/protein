﻿using Protein.Business.Concrete.Validator;
using Protein.Common.Dto.ImportObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
//using Excel = Microsoft.Office.Interop.Excel;
using ClosedXML.Excel;
using Protein.Common.Enums;
using Protein.Business.Abstract.Service;
using Protein.Business.Concrete.Services;
using Protein.Business.Concrete.Responses;
using Protein.Data.Repositories;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;
using Protein.Data.Helpers;
using static Protein.Common.Constants.Constants;

namespace PolicyBatch
{
    public partial class FormProcessStatus : Form
    {
        private static bool IsNullableType(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
        }
        private static CultureInfo trCulture = new CultureInfo("tr-TR");
        private string OutputFileDir = null;
        //private long PreviousPolicyId { get; set; } = 0;
        private List<PolicyDto> PolicyDtos = new List<PolicyDto>();
        private string PreviousPolicyKey { get; set; }


        private static IXLWorksheet mySheet = null;
        private static IXLRange myRange = null;
        private static XLWorkbook myWorkbook = null;

        //private static Excel.Application xlApp = null;
        //private static Excel.Workbook xlWorkbook = null;
        //private static Excel.Range xlUsedRange = null;
        //private static ProcessType CurrentProcess;
        private List<long> InstertedInstalmentIds = new List<long>();
        private static Dictionary<string, int> sonucTypeMap = null;
        private static SemaphoreSlim _pool;
        private static int maxthread = 0;
        private static Dictionary<string, int> policyTypeMap = null;
        private FormBase FormBase = new FormBase();
        private enum ActionType
        {
            Awaiting = 0,
            Running = 1,
            Pausing = 2,
            Finish = 3
        }
        //private enum ProcessType
        //{
        //    BaslangicZeyl = 0,
        //    IptalZeyl = 1,
        //    SigortaliGiris = 2,
        //    SigortaliCikis = 3
        //    //Type'lar eklenecek
        //}
        public FormProcessStatus(FormBase formBase)
        {
            InitializeComponent();
            FormBase = formBase;
        }
        private void SetActionTag(ActionType tag)
        {
            btnGo.Tag = tag;
            string label = btnGo.Text;
            switch (tag)
            {
                case ActionType.Awaiting:
                    label = "Başlat";
                    break;
                case ActionType.Pausing:
                    label = "Sürdür";
                    break;
                case ActionType.Running:
                    label = "Duraklat";
                    break;
                case ActionType.Finish:
                    label = "Çıkış";
                    break;
            }
            if (btnGo.InvokeRequired)
            {
                btnGo.Invoke(new Action(() =>
                {
                    btnGo.Text = label;
                    btnGo.Enabled = true;
                }));
            }
            else
            {
                btnGo.Text = label;
                btnGo.Enabled = true;
            }
        }
        private void btnGo_Click(object sender, EventArgs e)
        {
            switch ((ActionType)btnGo.Tag)
            {
                case ActionType.Awaiting:
                case ActionType.Pausing:
                    RunProcess();
                    break;
                case ActionType.Running:
                    //PauseProcess();
                    break;
                case ActionType.Finish:
                    this.Close();
                    break;
            }
        }
        private void RunProcess()
        { //Veri havuzunu işleme al
            SetActionTag(ActionType.Running);
            backgroundWorker1.RunWorkerAsync();
        }
        private void PauseProcess()
        { //pause operasyonları
            btnGo.Enabled = false; //bekleme moduna geçerken işlem yapamasın
            btnGo.Text = "Duraklatılıyor";

            Cursor.Current = Cursors.WaitCursor;
            backgroundWorker1.CancelAsync();
        }
        private void FormProcessStatus_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((ActionType)btnGo.Tag == ActionType.Running)
            {
                if (MessageBox.Show("Devam eden bir işlem var çıkmak istiyor musunuz?", null, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.OK)
                {
                    e.Cancel = true;
                    return;
                }
                else
                {
                    PauseProcess();
                    //Pause işlemi gelecek
                }
            }
            if (myWorkbook != null)
            {
                try
                {
                    myWorkbook.Save();
                }
                catch
                {
                    MessageBox.Show("Kayıt sırasında hata oluştu. Açılacak olan Excel ekranından dosyayı saklamayı deneyin.", null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //goto endpoint;
                }
                //finally
                //{
                //    myWorkbook = null;
                //    System.Diagnostics.Process.GetCurrentProcess().Kill();
                //}
            }
            myWorkbook = null;
            System.Diagnostics.Process.GetCurrentProcess().Kill();
            //if (xlApp != null)
            //{
            //    xlApp.Quit();
            //    xlApp = null;
            //}
            //endpoint:
            //if (xlUsedRange != null) xlUsedRange = null;

            //Program.FormBase.Show();
        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Dictionary<string, int> pMap = null;

            if (policyTypeMap == null)
            {
                policyTypeMap = new Dictionary<string, int>();
                pMap = policyTypeMap;
            }

            if (pMap != null)
            {
                foreach (DataGridViewRow row in FormBase.dataGvCols.Rows.Cast<DataGridViewRow>().Where(r => r.Tag != (object)"1"))
                {
                    pMap.Add(row.Cells[0].Value.ToString(), (int)row.Cells[1].Value);
                }
            }
            if (sonucTypeMap == null)
            {
                sonucTypeMap = new Dictionary<string, int>();
                foreach (DataGridViewRow row in FormBase.dataGvCols.Rows.Cast<DataGridViewRow>().Where(r => r.Tag == (object)"1"))
                {
                    sonucTypeMap.Add(row.Cells[0].Value.ToString(), (int)row.Cells[1].Value);
                }
            }

            maxthread = 1;
            _pool = new SemaphoreSlim(maxthread);

            int fStart = 0;
            int fFinish = PolicyDtos.Count;
            //int fStart = Math.Max(progressBar1.Value, 2);
            //int fFinish = progressBar1.Maximum + 1;

            for (int i = fStart; i < fFinish; i++)
            {
                if (backgroundWorker1.CancellationPending)
                {
                    while (_pool.CurrentCount < maxthread)
                    { //Mevcut processlerin bitmesini bekle
                        Thread.Sleep(1000);
                    }
                    Cursor.Current = Cursors.Default;
                    if ((ActionType)btnGo.Tag != ActionType.Finish) SetActionTag(ActionType.Pausing);
                    return;
                }
                _pool.Wait();

                try
                { //burada asıl process çalıştırılacak
                    BackgroundWorker bw = new BackgroundWorker();
                    bw.DoWork += new DoWorkEventHandler(this.PolicyCreator);
                    bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(ProcessRowComplete);
                    bw.RunWorkerAsync(i);
                }
                catch (Exception ex)
                {
                    _pool.Release();
                }
            }
        }
        private static object _syncExcel = new object();
        internal void WriteToExcel(int row, int col, string value)
        {
            if (col < 0) return;

            lock (_syncExcel)
            {
                try
                {
                    mySheet.Cell(row, col).Value = value;
                }
                catch { }
            }
        }
        private delegate void ProgressStepHandler();

        bool _IsSuccess = false;
        long _PolId = 0;
        long _EndorsId = 0;
        private void PolicyCreator(object sender, DoWorkEventArgs e)
        {
            PolicyDto _policy = new PolicyDto();
            lock (_syncExcel)
            {
                int index = (int)e.Argument;
                _policy = PolicyDtos[index];

                //eskiden excel'den okuyorduk. şimdi direk objeden....
                //alttaki kodlar kalsın. lazım olabilir


                //foreach (PropertyInfo prop in _policy.GetType().GetProperties())
                //{
                //    if (!prop.GetCustomAttributes(typeof(ColumnAttribute), false).Any())
                //        continue;
                //    else if (!((prop.GetCustomAttribute(typeof(ColumnAttribute), false) as ColumnAttribute).TypeName == "EXCEL"))
                //        continue;
                //    String columnName = prop.Name;
                //    columnName = (prop.GetCustomAttribute(typeof(ColumnAttribute), false) as ColumnAttribute).Name;
                //    if (policyTypeMap.ContainsKey(columnName) == false || policyTypeMap[columnName] < 0) continue;
                //    try
                //    {
                //        Type propertyType = prop.PropertyType;

                //        Type targetType = IsNullableType(propertyType) ? Nullable.GetUnderlyingType(propertyType) : propertyType;
                //        //prop null check

                //        object tmpvalue = mySheet.Cell((int)e.Argument, policyTypeMap[columnName]).Value; 
                //        /* ).Cells[policyTypeMap[columnName]].Value;*/
                //        if (tmpvalue == null) continue;
                //        if (string.IsNullOrEmpty(tmpvalue.ToString())) continue;
                //        object convertedObj = Convert.ChangeType(tmpvalue != null ? tmpvalue.ToString() : null, targetType);
                //        //if (targetType.Name == "String" && tmpvalue == null)
                //        //    tmpvalue = "";
                //        prop.SetValue(_policy, convertedObj, null);
                //    }
                //    catch (Exception ex)
                //    {
                //        if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel((int)e.Argument, sonucTypeMap["IsSuccess"], false.ToString());

                //        if (sonucTypeMap.ContainsKey("Description")) WriteToExcel((int)e.Argument, sonucTypeMap["Description"], ex.ToString());
                //        continue;
                //    }
                //}
            }

            bool OtherOpr = false;
            //string currentPolicyStatus = _policy.PolicyStatus;
            string currentEndorsementStatus = _policy.EndorsementStatus;

            _policy.CompanyId = Int64.Parse(FormBase.CompanySelectedValue);
            _policy.policyIntegrationType = PolicyIntegrationType.Excel;

            PolicyValidator validator = new PolicyValidator(_policy);
            PolicyValidatorResponse res;

            _policy.PolicyStatus = ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString();
            _policy.EndorsementStatus = ((int)ProteinEnums.Status.PASIF).ToString();

            try
            {


                if ((!string.IsNullOrEmpty(this.PreviousPolicyKey) && PreviousPolicyKey != _policy.BatchPolicyKey))
                {
                    long previousPolicyId = 0;
                    long previousEndorsId = 0;
                    string previousEndorsType = "";
                    string previousStatus = "";


                    PolicyDto previousPolicyDto = new PolicyDto();
                    previousPolicyDto.policyIntegrationType = PolicyIntegrationType.Excel;

                    //if (PolicyDtos.Count == (((int)e.Argument) + 1))
                    //    previousPolicyId = _PolId;
                    /*else*/
                    previousPolicyId = GetPolicyIdByPolicyKey(this.PreviousPolicyKey);
                    previousEndorsId = GetEndorsementIdByPolicyKey(this.PreviousPolicyKey);
                    previousEndorsType = GetEndorsementTypeByPolicyKey(this.PreviousPolicyKey);
                    var control = LookupHelper.GetLookupByCode(LookupTypes.Endorsement, previousEndorsType);
                    string previousEndorsementTypeCode = "";
                    if (control != null)
                    {
                         previousEndorsementTypeCode = ((int)(EnumHelper.GetValueFromText<ProteinEnums.EndorsementType>(previousEndorsType))).ToString();
                    }
                 
                    previousStatus = GetStatusByPolicyKey(this.PreviousPolicyKey);
                    if (previousPolicyId > 0)
                    {
                        if (previousEndorsementTypeCode == ((int)ProteinEnums.EndorsementType.BASLANGIC).ToString())
                        {
                            //bir önceki poliçeyi bul duruma göre tanzime çek => sagmer gönderimi => sigorta şirketi gönderimi

                            if (previousStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
                            {
                                var confirmedPolicy = new GenericRepository<Policy>().FindById(previousPolicyId);
                                confirmedPolicy.Status = previousStatus;
                                new GenericRepository<Policy>().Insert(confirmedPolicy);

                                previousStatus = previousStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() ? "0" : "2";
                                var confirmedEndorsement = new GenericRepository<Endorsement>().FindById(previousEndorsId);
                                confirmedEndorsement.Status = previousStatus;
                                new GenericRepository<Endorsement>().Insert(confirmedEndorsement);
                            }

                        }
                        else if (previousEndorsementTypeCode == ((int)ProteinEnums.EndorsementType.IPTAL).ToString())
                        {
                            if (previousStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
                            {
                                var confirmedPolicy = new GenericRepository<Policy>().FindById(previousPolicyId);
                                confirmedPolicy.Status = previousStatus;
                                new GenericRepository<Policy>().Insert(confirmedPolicy);

                                previousStatus = previousStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() ? "0" : "2";
                                var confirmedEndorsement = new GenericRepository<Endorsement>().FindById(previousEndorsId);
                                if (confirmedEndorsement != null)
                                {

                                    confirmedEndorsement.Status = previousStatus;
                                    new GenericRepository<Endorsement>().Insert(confirmedEndorsement);
                                }
                            }
                        }
                        else
                        {
                            if (previousStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
                            {
                                previousStatus = previousStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() ? "0" : "2";
                                var confirmedEndorsement = new GenericRepository<Endorsement>().FindById(previousEndorsId);
                                if (confirmedEndorsement != null)
                                {

                                    confirmedEndorsement.Status = previousStatus;
                                    new GenericRepository<Endorsement>().Insert(confirmedEndorsement);
                                }

                            }
                        }
                    }
                    else //policy bulunamadı!!!!
                    {
                        //error excel
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            #region Endorsement Opr.
            if (_policy.EndorsementType == EnumHelper.GetValues(EndorsementType.BASLANGIC)[1])
            {
                res = validator.BeginningEndorsement();
                if (res.ErrorCode == "100")
                {
                    _policy.isInsuredTransferChange = true;

                    if (_policy.InsuredIndividualType != "F")
                    {
                        _policy.isInsuredParameterChange = true;
                        _policy.isInsuredChange = true;
                        _policy.isInsuredContactChange = true;
                        //_policy.isInsurerChange = true;
                    }
                    else
                    {
                        //ilk poliçe oluşumu
                        if (_policy.PolicyId == 0)
                        {
                            #region Ödeme
                            _policy.isInsertInstallment = true;

                            #endregion

                            _policy.isEndorsementChange = true;
                            _policy.isPolicyChange = true;
                            _policy.IsAdditionalProtocolChange = true;
                            _policy.IsSpeacialCondition = true;
                            _policy.isInsurerChange = true;
                        }

                        _policy.isInsuredChange = true;
                        _policy.isInsuredContactChange = true;
                        //_policy.isInsurerChange = true;
                        _policy.isInsuredParameterChange = true;
                    }

                    Thread.Sleep(200);
                    IService<PolicyDto> policyService = new PolicyService();
                    _policy.PolicyStatus = ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString();
                    _policy.EndorsementStatus = ((int)ProteinEnums.Status.PASIF).ToString();
                    ServiceResponse response = policyService.ServiceInsert(_policy);

                    if (response.IsSuccess)
                    {
                        _PolId = response.PolicyId;
                        _EndorsId = response.EndorsementId;
                        _IsSuccess = true;
                    }
               
                    
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], response.Code == "100" ? true.ToString() : false.ToString());
                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], response.Code == "100" ? response.Message : response.Message);
                }
                else
                {
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], res.ErrorCode == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], res.ErrorCode == "100" ? true.ToString() : res.Description);
                }
            }
            else if (_policy.EndorsementType == EnumHelper.GetValues(EndorsementType.IPTAL)[1])
            {
                res = validator.PolicyCancellation();

                if (res.ErrorCode == "100")
                {
                    ServiceResponse response = new ServiceResponse();

                    #region Operation
                    Endorsement cancelEndorsement = new Endorsement
                    {
                        Id = 0,
                        Premium = _policy.PolicyPremium<0 ? _policy.PolicyPremium : (-1 * _policy.PolicyPremium),
                        DateOfIssue = _policy.DateOfIssue,
                        Status = ((int)Status.AKTIF).ToString(),
                        Type = ((int)EndorsementType.IPTAL).ToString(),
                        StartDate = _policy.EndorsementStartDate,
                        No = _policy.EndorsementNo,
                        PolicyId = _policy.PolicyId,
                        Category = ((int)EndorsementCategory.POLICE).ToString()
                    };

                    SpResponse spResponseEndo = new GenericRepository<Endorsement>().Insert(cancelEndorsement);
                    if (spResponseEndo.Code != "100")
                    {
                        #region Hata
                        response.Code = "0";
                        response.Message = "Zeyil Kayıt Edilemedi.";
                        #endregion
                    }
                    else
                    {
                        EndorsementPolicy endorsementPolicy = new EndorsementPolicy
                        {
                            EndorsementId = spResponseEndo.PkId,
                            PolicyId = _policy.PolicyId
                        };
                        SpResponse spResponseEndoPol = new GenericRepository<EndorsementPolicy>().Insert(endorsementPolicy);
                        if (spResponseEndoPol.Code != "100")
                        {
                            #region Hata
                            response.Code = "0";
                            response.Message = "Zeyil-Poliçe Kayıt Edilemedi.";
                            #endregion
                        }
                        else
                        {
                            long? LastEndorsementId = 0;
                            decimal? premium = 0;
                            Policy policy = new GenericRepository<Policy>().FindById(_policy.PolicyId);
                            LastEndorsementId = policy.LastEndorsementId;
                            premium = policy.Premium;

                            policy.Status = ((int)PolicyStatus.IPTAL).ToString();
                            policy.Premium = policy.Premium + (_policy.PolicyPremium < 0 ? _policy.PolicyPremium : (-1 * _policy.PolicyPremium));
                            policy.LastEndorsementId = spResponseEndo.PkId;

                            SpResponse spResponsePolicy = new GenericRepository<Policy>().Insert(policy);
                            if (spResponsePolicy.Code != "100")
                            {
                                #region Hata
                                response.Code = "0";
                                response.Message = "Poliçe Primi Güncellenemedi";
                                #endregion
                            }
                            else
                            {
                                response.Code = "100";
                                response.Message = "İşlem Başarılı";
                            }
                        }
                    }
                    #endregion

                    //IService<PolicyDto> policyService = new PolicyService();
                    //ServiceResponse response = policyService.ServiceInsert(_policy, "9999999999");

                    //if (response.IsSuccess)
                    //{
                    //    _PolId = response.PolicyId;
                    //    _EndorsId = response.EndorsementId;
                    //    _IsSuccess = true;
                    //}
                    //if ((!string.IsNullOrEmpty(this.PreviousPolicyKey) && PreviousPolicyKey != _policy.BatchPolicyKey) || PolicyDtos.Count == (((int)e.Argument) + 1))
                    //{
                    //    long previousPolicyId = 0;
                    //    PolicyDto previousPolicyDto = new PolicyDto();
                    //    previousPolicyDto.policyIntegrationType = PolicyIntegrationType.Excel;

                    //    if (PolicyDtos.Count == (((int)e.Argument) + 1))
                    //        previousPolicyId = response.PolicyId;
                    //    else previousPolicyId = GetPolicyIdByPolicyKey(this.PreviousPolicyKey);

                    //    if (currentPolicyStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
                    //    {
                    //        //bir önceki poliçeyi bul duruma göre tanzime çek => sagmer gönderimi => sigorta şirketi gönderimi

                    //        previousPolicyDto.PolicyStatus = ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString();
                    //        previousPolicyDto.PolicyId = previousPolicyId;
                    //        previousPolicyDto.isPolicyChange = true;
                    //        policyService.ServiceInsert(_policy, "9999999999");
                    //    }
                    //}


                    //string response = "OK";
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], response.IsSuccess ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], response.IsSuccess ? "İşlem Başarılı" : response.Message);
                }
                else
                {
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], res.ErrorCode == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], res.ErrorCode == "100" ? true.ToString() : res.Description);
                }
            }
            else if (_policy.EndorsementType == EnumHelper.GetValues(EndorsementType.SIGORTALI_GIRISI)[1])
            {
                res = validator.InsuredEntry();

                if (res.ErrorCode == "100")
                {
                    _policy.isInsuredTransferChange = true;
                    //if (_policy.InsuredIndividualType != "F")
                    //{
                    if (_policy.EndorsementId == 0)
                    {
                        _policy.isEndorsementChange = true;
                    }
                    _policy.isInsuredParameterChange = true;

                    _policy.isInsuredChange = true;
                    _policy.isInsuredContactChange = true;
                    //_policy.isInsurerChange = true;
                    //}
                    //else
                    //{
                    //    _policy.isInsuredChange = true;
                    //    _policy.isInsuredContactChange = true;
                    //    _policy.isInsurerChange = true;
                    //    _policy.isInsuredParameterChange = true;
                    //}
                    Thread.Sleep(200);
                    IService<PolicyDto> policyService = new PolicyService();
                    _policy.PolicyStatus = ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString();
                    _policy.EndorsementStatus = ((int)ProteinEnums.Status.PASIF).ToString();
                    ServiceResponse response = policyService.ServiceInsert(_policy);
                    if (response.IsSuccess)
                    {
                        _PolId = response.PolicyId;
                        _EndorsId = response.EndorsementId;
                        _IsSuccess = true;
                    }
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], response.Code == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], response.Code == "100" ? response.Id.ToString() : response.Message);
                }
                else
                {
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], res.ErrorCode == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], res.ErrorCode == "100" ? true.ToString() : res.Description);
                }
            }
            else if (_policy.EndorsementType == EnumHelper.GetValues(EndorsementType.SIGORTALI_CIKISI)[1])
            {
                res = validator.InsuredExit();

                if (res.ErrorCode == "100")
                {
                    _policy.isEndorsementChange = true;

                    IService<PolicyDto> policyService = new PolicyService();
                    _policy.PolicyStatus = ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString();
                    _policy.EndorsementStatus = ((int)ProteinEnums.Status.PASIF).ToString();
                    ServiceResponse response = policyService.ServiceInsert(_policy);

                    if (response.IsSuccess)
                    {

                        _PolId = response.PolicyId;
                        _EndorsId = response.EndorsementId;
                        _IsSuccess = true;

                        var result = new GenericRepository<Insured>().FindById(_policy.InsuredId);
                        result.Status = ((int)Status.SILINDI).ToString();
                        result.LastEndorsementId = _policy.EndorsementId;
                        result.InitialPremium = result.InitialPremium - _policy.InsuredInitialPremium;
                        var spResponse = new GenericRepository<Insured>().Insert(result);
                        if (spResponse.Code != "100")
                            throw new Exception(spResponse.Code + " : " + spResponse.Message);
                        var resultPol = new GenericRepository<Policy>().FindById(_policy.PolicyId);
                        resultPol.Premium = resultPol.Premium - _policy.InsuredInitialPremium;
                        var spPolResponse = new GenericRepository<Policy>().Insert(resultPol);

                        if (result.IndividualType == ((int)IndividualType.FERT).ToString())
                        {
                            List<Insured> insuredList = new GenericRepository<Insured>().FindBy("PARENT_ID=:parentId", parameters: new { parentId = _policy.InsuredId });
                            if (insuredList != null)
                            {
                                foreach (var item in insuredList)
                                {
                                    item.Status = ((int)Status.SILINDI).ToString();
                                    spResponse = new GenericRepository<Insured>().Insert(item);
                                    if (spResponse.Code != "100")
                                        throw new Exception(spResponse.Code + " : " + spResponse.Message);
                                }
                            }
                        }
                    }

                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], response.Code == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], response.Code == "100" ? response.Id.ToString() : response.Message);
                    //if (_policy.InsuredIndividualType == "F")
                    //{

                    //}
                }
                else
                {
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], res.ErrorCode == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], res.ErrorCode == "100" ? true.ToString() : res.Description);
                }
            }
            else if (_policy.EndorsementType == EnumHelper.GetValues(EndorsementType.TEMINAT_PLAN_DEGISIKLIGI)[1])
            {
                res = validator.PlanChangeEndorsement();

                if (res.ErrorCode == "100")
                {
                    _policy.isEndorsementChange = true;

                    Thread.Sleep(200);
                    IService<PolicyDto> policyService = new PolicyService();
                    _policy.PolicyStatus = ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString();
                    _policy.EndorsementStatus = ((int)ProteinEnums.Status.PASIF).ToString();
                    ServiceResponse response = policyService.ServiceInsert(_policy);
                    if (response.IsSuccess)
                    {
                        _PolId = response.PolicyId;
                        _EndorsId = response.EndorsementId;
                        _IsSuccess = true;

                        var insured = new GenericRepository<Insured>().FindById(_policy.InsuredId);
                        if (insured != null)
                        {
                            insured.PackageId = _policy.InsuredPackageId;
                            insured.LastEndorsementId = _policy.EndorsementId;
                            SpResponse spResponse = new GenericRepository<Insured>().Update(insured);
                            if (spResponse.Code != "100")
                            {
                                response.Code = spResponse.Code;
                                response.Message = spResponse.Message;
                            }
                        }
                    }


                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], response.Code == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], response.Code == "100" ? response.Id.ToString() : response.Message);
                }
                else
                {
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], res.ErrorCode == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], res.ErrorCode == "100" ? true.ToString() : res.Description);
                }
            }
            else if (_policy.EndorsementType == EnumHelper.GetValues(EndorsementType.URETIM_KAYNAGI_DEGISIKLIGI)[1])
            {
                res = validator.AgencyChangeEndorsement();

                if (res.ErrorCode == "100")
                {
                    _policy.isEndorsementChange = true;

                    Thread.Sleep(200);
                    IService<PolicyDto> policyService = new PolicyService();
                    _policy.PolicyStatus = ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString();
                    _policy.EndorsementStatus = ((int)ProteinEnums.Status.PASIF).ToString();
                    ServiceResponse response = policyService.ServiceInsert(_policy);
                    if (response.IsSuccess)
                    {
                        _PolId = response.PolicyId;
                        _EndorsId = response.EndorsementId;
                        _IsSuccess = true;

                        var policy = new GenericRepository<Policy>().FindById(_policy.PolicyId);
                        if (policy != null)
                        {
                            policy.AgencyId = long.Parse(_policy.AgencyId);
                            policy.LastEndorsementId = _policy.EndorsementId;
                            SpResponse spResponse = new GenericRepository<Policy>().Update(policy);
                            if (spResponse.Code != "100")
                            {
                                response.Code = spResponse.Code;
                                response.Message = spResponse.Message;
                            }
                        }
                    }


                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], response.Code == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], response.Code == "100" ? response.Id.ToString() : response.Message);
                }
                else
                {
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], res.ErrorCode == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], res.ErrorCode == "100" ? true.ToString() : res.Description);
                }
            }
            else if (_policy.EndorsementType == EnumHelper.GetValues(EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI)[1])
            {
                res = validator.InsuredChangeEndorsement();

                if (res.ErrorCode == "100")
                {
                    _policy.isInsuredTransferChange = true;
                    _policy.isEndorsementChange = true;
                    _policy.isInsuredParameterChange = true;
                    _policy.isInsuredChange = true;
                    _policy.isInsuredContactChange = true;

                    Thread.Sleep(200);
                    IService<PolicyDto> policyService = new PolicyService();
                    _policy.PolicyStatus = ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString();
                    _policy.EndorsementStatus = ((int)ProteinEnums.Status.PASIF).ToString();
                    ServiceResponse response = policyService.ServiceInsert(_policy);
                    if (response.Code == "100")
                    {
                        _PolId = response.PolicyId;
                        _EndorsId = response.EndorsementId;
                        _IsSuccess = true;
                    }
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], response.Code == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], response.Code == "100" ? response.Id.ToString() : response.Message);
                }
                else
                {
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], res.ErrorCode == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], res.ErrorCode == "100" ? true.ToString() : res.Description);
                }
            }
            else if (_policy.EndorsementType == EnumHelper.GetValues(EndorsementType.SE_DEGISIKLIGI)[1])
            {
                res = validator.InsurerChangeEndorsement();

                if (res.ErrorCode == "100")
                {
                    _policy.isEndorsementChange = true;
                    _policy.isInsurerChange = true;

                    Thread.Sleep(200);
                    IService<PolicyDto> policyService = new PolicyService();
                    _policy.PolicyStatus = ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString();
                    _policy.EndorsementStatus = ((int)ProteinEnums.Status.PASIF).ToString();
                    ServiceResponse response = policyService.ServiceInsert(_policy);
                    if (response.IsSuccess)
                    {
                        _PolId = response.PolicyId;
                        _EndorsId = response.EndorsementId;
                        _IsSuccess = true;

                        var policy = new GenericRepository<Policy>().FindById(_policy.PolicyId);
                        if (policy != null)
                        {
                            policy.Insurer = _policy.InsurerId;
                            policy.LastEndorsementId = _policy.EndorsementId;
                            SpResponse spResponse = new GenericRepository<Policy>().Update(policy);
                            if (spResponse.Code != "100")
                            {
                                response.Code = spResponse.Code;
                                response.Message = spResponse.Message;
                            }
                        }
                    }


                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], response.Code == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], response.Code == "100" ? response.Id.ToString() : response.Message);
                }
                else
                {
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], res.ErrorCode == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], res.ErrorCode == "100" ? true.ToString() : res.Description);
                }
            }
            else if (_policy.EndorsementType == EnumHelper.GetValues(EndorsementType.PRIM_FARKI_ZEYLI)[1] || _policy.EndorsementType == EnumHelper.GetValues(EndorsementType.POLICE_TAHAKKUK_ZEYLI)[1])
            {
                res = validator.PolicyPremiumChangeEndorsement();

                if (res.ErrorCode == "100")
                {
                    _policy.isEndorsementChange = true;
                    //_policy.isInsurerChange = true;

                    Thread.Sleep(200);
                    IService<PolicyDto> policyService = new PolicyService();
                    _policy.PolicyStatus = ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString();
                    _policy.EndorsementStatus = ((int)ProteinEnums.Status.PASIF).ToString();
                    ServiceResponse response = policyService.ServiceInsert(_policy);
                    if (response.IsSuccess)
                    {
                        _PolId = response.PolicyId;
                        _EndorsId = response.EndorsementId;
                        _IsSuccess = true;

                        var policy = new GenericRepository<Policy>().FindById(_policy.PolicyId);
                        if (policy != null)
                        {
                            policy.Premium = _policy.PolicyPremium;
                            policy.LastEndorsementId = _policy.EndorsementId;
                            SpResponse spResponse = new GenericRepository<Policy>().Update(policy);
                            if (spResponse.Code != "100")
                            {
                                response.Code = spResponse.Code;
                                response.Message = spResponse.Message;
                            }
                        }
                    }


                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], response.Code == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], response.Code == "100" ? response.Id.ToString() : response.Message);
                }
                else
                {
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], res.ErrorCode == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], res.ErrorCode == "100" ? true.ToString() : res.Description);
                }
            }
            else if (_policy.EndorsementType == EnumHelper.GetValues(EndorsementType.BASLANGIC_BITIS_TARIHI)[1])
            {
                res = validator.PolicyDateChangeEndorsement();

                if (res.ErrorCode == "100")
                {
                    _policy.isEndorsementChange = true;

                    Thread.Sleep(200);
                    IService<PolicyDto> policyService = new PolicyService();
                    _policy.PolicyStatus = ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString();
                    _policy.EndorsementStatus = ((int)ProteinEnums.Status.PASIF).ToString();
                    ServiceResponse response = policyService.ServiceInsert(_policy);
                    if (response.IsSuccess)
                    {
                        _PolId = response.PolicyId;
                        _EndorsId = response.EndorsementId;
                        _IsSuccess = true;

                        var policy = new GenericRepository<Policy>().FindById(_policy.PolicyId);
                        if (policy != null)
                        {
                            policy.EndDate = _policy.EndDate;
                            policy.LastEndorsementId = _policy.EndorsementId;
                            SpResponse spResponse = new GenericRepository<Policy>().Update(policy);
                            if (spResponse.Code != "100")
                            {
                                response.Code = spResponse.Code;
                                response.Message = spResponse.Message;
                            }

                            Endorsement endorsement = new GenericRepository<Endorsement>().FindBy("TYPE =:type AND POLICY_ID=:policyId", parameters: new { type = new Dapper.DbString { Value = ((int)EndorsementType.BASLANGIC).ToString(), Length = 3 }, policyId = policy.Id }).SingleOrDefault();
                            if (endorsement == null)
                            {
                                throw new Exception("Başlangıç Tarihi Değişikliği Gerçekleştirilemedi. Lütfen Tekrar Deneyiniz...");
                            }
                            endorsement.StartDate = _policy.StartDate;
                            SpResponse spResponseEndorsement = new GenericRepository<Endorsement>().Update(endorsement);
                            if (spResponseEndorsement.Code != "100")
                            {
                                response.Code = spResponse.Code;
                                response.Message = spResponse.Message;
                            }
                        }
                    }


                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], response.Code == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], response.Code == "100" ? response.Id.ToString() : response.Message);
                }
                else
                {
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], res.ErrorCode == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], res.ErrorCode == "100" ? true.ToString() : res.Description);
                }
            }
            else if (_policy.EndorsementType == EnumHelper.GetValues(EndorsementType.SIGORTALI_TAHAKKUK)[1])
            {
                res = validator.InsuredPremiumChangeEndorsement();

                if (res.ErrorCode == "100")
                {
                    _policy.isEndorsementChange = true;

                    Thread.Sleep(200);
                    IService<PolicyDto> policyService = new PolicyService();
                    _policy.PolicyStatus = ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString();
                    _policy.EndorsementStatus = ((int)ProteinEnums.Status.PASIF).ToString();
                    ServiceResponse response = policyService.ServiceInsert(_policy);
                    if (response.IsSuccess)
                    {
                        _PolId = response.PolicyId;
                        _EndorsId = response.EndorsementId;
                        _IsSuccess = true;

                        var insured = new GenericRepository<Insured>().FindById(_policy.InsuredId);
                        if (insured != null)
                        {
                            insured.InitialPremium = _policy.InsuredInitialPremium;
                            insured.LastEndorsementId = _policy.EndorsementId;
                            SpResponse spResponse = new GenericRepository<Insured>().Update(insured);
                            if (spResponse.Code != "100")
                            {
                                response.Code = spResponse.Code;
                                response.Message = spResponse.Message;
                            }
                        }
                    }


                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], response.Code == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], response.Code == "100" ? response.Id.ToString() : response.Message);
                }
                else
                {
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], res.ErrorCode == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], res.ErrorCode == "100" ? true.ToString() : res.Description);
                }
            }
            else if (_policy.EndorsementType == EnumHelper.GetValues(EndorsementType.GIDECEGI_ULKE_DEGISIKLIGI)[1])
            {
                res = validator.LocationChangeEndorsement();

                if (res.ErrorCode == "100")
                {
                    _policy.isEndorsementChange = true;

                    Thread.Sleep(200);
                    IService<PolicyDto> policyService = new PolicyService();
                    _policy.PolicyStatus = ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString();
                    _policy.EndorsementStatus = ((int)ProteinEnums.Status.PASIF).ToString();
                    ServiceResponse response = policyService.ServiceInsert(_policy);

                    if (response.IsSuccess)
                    {
                        _PolId = response.PolicyId;
                        _EndorsId = response.EndorsementId;
                        _IsSuccess = true;

                        var policy = new GenericRepository<Policy>().FindById(_policy.PolicyId);
                        if (policy != null)
                        {
                            policy.LocationType = _policy.LocationType;
                            policy.LastEndorsementId = _policy.EndorsementId;
                            SpResponse spResponse = new GenericRepository<Policy>().Update(policy);
                            if (spResponse.Code != "100")
                            {
                                response.Code = spResponse.Code;
                                response.Message = spResponse.Message;
                            }
                        }
                    }

                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], response.Code == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], response.Code == "100" ? response.Id.ToString() : response.Message);
                }
                else
                {
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], res.ErrorCode == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], res.ErrorCode == "100" ? true.ToString() : res.Description);
                }
            }
            else if (_policy.EndorsementType == EnumHelper.GetValues(EndorsementType.MERIYETE_DONUS_ZEYLI)[1])
            {
                res = validator.ValidityTurn();

                if (res.ErrorCode == "100")
                {
                    _policy.isEndorsementChange = true;

                    Thread.Sleep(200);
                    IService<PolicyDto> policyService = new PolicyService();
                    _policy.PolicyStatus = ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString();
                    _policy.EndorsementStatus = ((int)ProteinEnums.Status.PASIF).ToString();
                    ServiceResponse response = policyService.ServiceInsert(_policy);

                    if (response.IsSuccess)
                    {
                        _PolId = response.PolicyId;
                        _EndorsId = response.EndorsementId;
                        _IsSuccess = true;

                        var policy = new GenericRepository<Policy>().FindById(_policy.PolicyId);
                        if (policy != null)
                        {
                            //policy.Status = ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString();
                            policy.LastEndorsementId = _policy.EndorsementId;
                            SpResponse spResponse = new GenericRepository<Policy>().Update(policy);
                            if (spResponse.Code != "100")
                            {
                                response.Code = spResponse.Code;
                                response.Message = spResponse.Message;
                            }
                        }
                    }
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], response.Code == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], response.Code == "100" ? response.Id.ToString() : response.Message);
                }
                else
                {
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], res.ErrorCode == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], res.ErrorCode == "100" ? true.ToString() : res.Description);
                }
            }


            /// Zeyl turunden farklı operasyonlar
            else if (_policy.EndorsementType == "TAKSIT")
            {
                OtherOpr = true;
                res = validator.InstallmentOperation();

                if (res.ErrorCode == "100")
                {
                    _policy.isInsertInstallment = true;
                    _policy.InstertedInstalmentIds = InstertedInstalmentIds;
                    Thread.Sleep(200);
                    IService<PolicyDto> policyService = new PolicyService();
                    ServiceResponse response = policyService.ServiceInsert(_policy);
                    if (response.Id > 0)
                        InstertedInstalmentIds.Add(response.Id);
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], response.Code == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], response.Code == "100" ? response.Id.ToString() : response.Message);
                }
                else
                {
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], res.ErrorCode == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], res.ErrorCode == "100" ? true.ToString() : res.Description);
                }
            }
            else if (_policy.EndorsementType == "PRINT")
            {
                OtherOpr = true;
                res = validator.PrintOperation();

                if (res.ErrorCode == "100")
                {
                    _policy.IsPrint = true;
                    _policy.LocalSavePath = FormBase.folderBrowserDialog1.SelectedPath;
                    Thread.Sleep(200);

                    IService<PolicyDto> policyService = new PolicyService();
                    ServiceResponse response = policyService.ServiceInsert(_policy);

                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], response.Code == "100" ? true.ToString() : false.ToString());
                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], response.Code == "100" ? response.Id.ToString() : response.Message);
                }
                else
                {
                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["IsSuccess"], res.ErrorCode == "100" ? true.ToString() : false.ToString());

                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel(((int)e.Argument) + 2, sonucTypeMap["Description"], res.ErrorCode == "100" ? true.ToString() : res.Description);
                }
            }
            #endregion

            PreviousPolicyKey = _policy.BatchPolicyKey;

            if (PolicyDtos.Count == (((int)e.Argument) + 1) && !OtherOpr)
            {
                long previousPolicyId = 0;
                long previousEndorsId = 0;
                string previousEndorsType = "";
                string previousStatus = "";


                PolicyDto previousPolicyDto = new PolicyDto();
                previousPolicyDto.policyIntegrationType = PolicyIntegrationType.Excel;

                //if (PolicyDtos.Count == (((int)e.Argument) + 1))
                //    previousPolicyId = _PolId;
                /*else*/
                previousPolicyId = GetPolicyIdByPolicyKey(this.PreviousPolicyKey);
                previousEndorsId = GetEndorsementIdByPolicyKey(this.PreviousPolicyKey);
                previousEndorsType = GetEndorsementTypeByPolicyKey(this.PreviousPolicyKey);
                string previousEndorsementTypeCode = ((int)(EnumHelper.GetValueFromText<ProteinEnums.EndorsementType>(previousEndorsType))).ToString();
                previousStatus = GetStatusByPolicyKey(this.PreviousPolicyKey);
                if (previousPolicyId > 0 && previousEndorsId > 0)
                {
                    if (previousEndorsementTypeCode == ((int)ProteinEnums.EndorsementType.BASLANGIC).ToString())
                    {
                        //bir önceki poliçeyi bul duruma göre tanzime çek => sagmer gönderimi => sigorta şirketi gönderimi

                        if (previousStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
                        {
                            var confirmedPolicy = new GenericRepository<Policy>().FindById(previousPolicyId);
                            confirmedPolicy.Status = previousStatus;
                            new GenericRepository<Policy>().Insert(confirmedPolicy);

                            previousStatus = previousStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() ? "0" : "2";
                            var confirmedEndorsement = new GenericRepository<Endorsement>().FindById(previousEndorsId);
                            confirmedEndorsement.Status = previousStatus;
                            new GenericRepository<Endorsement>().Insert(confirmedEndorsement);
                        }

                    }
                    else if (previousEndorsementTypeCode == ((int)ProteinEnums.EndorsementType.IPTAL).ToString())
                    {
                        if (previousStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
                        {
                            var confirmedPolicy = new GenericRepository<Policy>().FindById(previousPolicyId);
                            confirmedPolicy.Status = previousStatus;
                            new GenericRepository<Policy>().Insert(confirmedPolicy);

                            previousStatus = previousStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() ? "0" : "2";
                            var confirmedEndorsement = new GenericRepository<Endorsement>().FindById(previousEndorsId);
                            confirmedEndorsement.Status = previousStatus;
                            new GenericRepository<Endorsement>().Insert(confirmedEndorsement);
                        }
                    }
                    else
                    {
                        if (previousStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
                        {
                            previousStatus = previousStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() ? "0" : "2";
                            var confirmedEndorsement = new GenericRepository<Endorsement>().FindById(previousEndorsId);
                            confirmedEndorsement.Status = previousStatus;
                            new GenericRepository<Endorsement>().Insert(confirmedEndorsement);
                        }
                    }
                }
                else //policy bulunamadı!!!!
                {
                    //error excel
                    if (previousStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() && previousPolicyId>0)
                    {
                        var confirmedPolicy = new GenericRepository<Policy>().FindById(previousPolicyId);
                        if (confirmedPolicy!=null)
                        {
                            confirmedPolicy.Status = ((int)ProteinEnums.PolicyStatus.IPTAL).ToString();
                            if (_policy.PolicyPremium > 0)
                            {
                                _policy.PolicyPremium += _policy.PolicyPremium * -1;
                            }
                            else
                            {
                                confirmedPolicy.Premium = confirmedPolicy.Premium + _policy.PolicyPremium;
                            }
                        }

                        previousStatus = previousStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() ? "0" : "3";
                        Endorsement endorsement = new Endorsement();
                        endorsement.PolicyId = previousPolicyId;
                        endorsement.Premium = _policy.PolicyPremium;
                        endorsement.Type = ((int)ProteinEnums.EndorsementType.GUN_ESASLI_IPTAL).ToString();
                        endorsement.No = 1; // BURAYA SON ZEYİLDEN +1 OLACAK
                        endorsement.StartDate = DateTime.Now;
                        endorsement.DateOfIssue = DateTime.Now;
                        var cancelEndorsement = new GenericRepository<Endorsement>().Insert(endorsement);

                        confirmedPolicy.LastEndorsementId = cancelEndorsement.PkId;
                        new GenericRepository<Policy>().Insert(confirmedPolicy);
                        // TAKSİT EKLENECEK
                    }
                }
            }
        }
        private void ProcessRowComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                Invoke(new ProgressStepHandler(ProgressStep));
                if (sender is BackgroundWorker) (sender as BackgroundWorker).Dispose();
            }
            finally
            {
                _pool.Release();
            }
        }
        private void ProgressStep()
        {
            progressBar1.PerformStep();
            lblProgress.Text = progressBar1.Value.ToString();
        }
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((ActionType)btnGo.Tag == ActionType.Running && _pool != null)
            {
                Cursor.Current = Cursors.WaitCursor;

                System.Windows.Forms.Application.DoEvents();
                while (_pool.CurrentCount < maxthread)
                { //Mevcut processlerin bitmesini bekle
                    Thread.Sleep(1000);
                    System.Windows.Forms.Application.DoEvents();
                }
                Cursor.Current = Cursors.Default;
                if ((ActionType)btnGo.Tag == ActionType.Running)
                {
                    SetActionTag(ActionType.Finish);
                    try
                    {
                        myWorkbook.Save();
                        myWorkbook = null;

                        //xlWorkbook.Save();
                        //xlWorkbook.Close();
                        //xlWorkbook = null;
                        //xlApp.Quit();
                        //xlApp = null;
                        //xlUsedRange = null;

                        MessageBox.Show("İşlem tamamladı.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch
                    {
                        MessageBox.Show("Kayıt sırasında hata oluştu. Açılacak olan Excel ekranından dosyayı saklamayı deneyin.", null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //xlApp.ScreenUpdating = true;
                        //xlApp.Visible = true;
                    }
                }
            }
        }
        private void SortExcel()
        {

            if (policyTypeMap == null)
            {
                policyTypeMap = new Dictionary<string, int>();
            }

            if (policyTypeMap != null)
            {
                foreach (DataGridViewRow row in FormBase.dataGvCols.Rows.Cast<DataGridViewRow>().Where(r => r.Tag != (object)"1"))
                {
                    if (row.Cells[1]!=null && row.Cells[0]!=null)
                    {
                        policyTypeMap.Add(row.Cells[0].Value.ToString(), (int)row.Cells[1].Value);
                    }
                }
            }


            //IXLRange range = mySheet.RangeUsed();
            //int s1 = 1;
            //foreach (KeyValuePair<string, int> item in policyTypeMap.OrderBy(x => x.Value))
            //{
            //    if (item.Value != s1)
            //    {
            //        string fdf = item.Key + "???" + item.Value;
            //    }
            //    s1++;
            //}

            var pol = policyTypeMap["KAPAK_POLICE_NO"];
            var renewal = policyTypeMap["YENILEME_NO"];
            var endorsNo = policyTypeMap["ZEYL_SIRA_NO"];
            var aileNo = policyTypeMap["SG_AILE_NO"];



            //var header = range.Row(1).InsertRowsAbove(1).First();
            //for (Int32 co = 1; co <= 1; co++)
            //{
            //    header.Cell(co).Value = "YAKINLIK_SAYISAL";
            //}

            //mySheet.Range(lastUsedCell.Address.ColumnLetter.ToString() + "1").Column(myRange.LastColumnUsed().ColumnNumber() + 5).Value = "YAKINLIK_SAYISAL";

            //string df = mySheet.RangeUsed().LastColumnUsed().ColumnLetter().ToString();
            //string gfg = mySheet.RangeUsed().ColumnCount().ToString(); //71
            //string werf = mySheet.RangeUsed().LastColumnUsed().CellCount().ToString(); // 47
            //string fdskjf = mySheet.RangeUsed().Columns().Count().ToString(); //71

            mySheet.Range(mySheet.RangeUsed().LastColumnUsed().ColumnLetter().ToString() + "1").Column(2).Value = "YAKINLIK_SAYISAL";
            //mySheet.Range(mySheet.RangeUsed().LastColumnUsed().ColumnLetter().ToString() + "1").Column(1).SetAutoFilter();

            //string sonmer = mySheet.RangeUsed().LastColumnUsed().ColumnLetter();
            string lastColCount = mySheet.RangeUsed().ColumnCount().ToString();
            policyTypeMap.Add("YAKINLIK_SAYISAL", int.Parse(lastColCount));



            for (int i = 2; i <= mySheet.RangeUsed().RowsUsed().Count(); i++)
            {
                string individualTypeText = mySheet.RangeUsed().Cell((int)i, policyTypeMap["SG_YAKINLIK"]).Value.ToString();
                if (!string.IsNullOrEmpty(individualTypeText))
                {
                    string individualTypeCode = ((int)(EnumHelper.GetValueFromText<ProteinEnums.IndividualType>(individualTypeText))).ToString();
                    mySheet.RangeUsed().Cell((int)i, policyTypeMap["YAKINLIK_SAYISAL"]).Value = individualTypeCode;
                }
            }

            mySheet.Columns().AdjustToContents();
            mySheet.Rows().AdjustToContents();


            var lastUsedCell = mySheet.LastCellUsed();
            //lastUsedCell.Address.RowNumber.ToString();
            //lastUsedCell.Address.ColumnLetter.ToString();
            string rangeAddress = "A2:" + lastUsedCell.Address.ColumnLetter.ToString() + lastUsedCell.Address.RowNumber.ToString();

            IXLRange range = mySheet.Range(rangeAddress);
            range.SortColumns.Add(pol, XLSortOrder.Ascending);
            range.SortColumns.Add(renewal, XLSortOrder.Ascending);
            range.SortColumns.Add(endorsNo, XLSortOrder.Ascending);
            range.SortColumns.Add(aileNo, XLSortOrder.Ascending);
            range.SortColumns.Add(lastColCount, XLSortOrder.Ascending);
            range.Sort();
            myRange = range;

            mySheet.ExpandColumns().SetAutoFilter();

            //myRange.Column(myRange.LastColumnUsed().ColumnNumber() + 2).Value = "YAKINLIK_SAYISAL";

            //mySheet.Sort($"{pol},{renewal},{aileNo} asc");
            //range.SortColumns.Add(fdf, XLSortOrder.Ascending, false, true);
            //range.SortColumns.Add(policyTypeMap["YENILEME_NO"], XLSortOrder.Ascending, false, true);
            //range.SortColumns.Add(policyTypeMap["SG_AILE_NO"], XLSortOrder.Ascending, false, true);
            //range.Sort();




            //int fStart = Math.Max(progressBar1.Value, 1);
            //int fFinish = progressBar1.Maximum;

            //for (int i = fStart; i <= fFinish; i++)
            //{
            //    PolicyDto _policy = new PolicyDto();

            //    foreach (PropertyInfo prop in _policy.GetType().GetProperties())
            //    {

            //        if (!prop.GetCustomAttributes(typeof(ColumnAttribute), false).Any())
            //            continue;
            //        else if (!((prop.GetCustomAttribute(typeof(ColumnAttribute), false) as ColumnAttribute).TypeName == "EXCEL"))
            //            continue;

            //        String columnName = prop.Name;

            //        columnName = (prop.GetCustomAttribute(typeof(ColumnAttribute), false) as ColumnAttribute).Name;


            //        if (policyTypeMap.ContainsKey(columnName) == false || policyTypeMap[columnName] < 0) continue;

            //        try
            //        {
            //            Type propertyType = prop.PropertyType;

            //            Type targetType = IsNullableType(propertyType) ? Nullable.GetUnderlyingType(propertyType) : propertyType;
            //            //prop null check
            //            if (columnName == "KAPAK_POLICE_NO" || columnName == "YENILEME_NO" || columnName == "SG_AILE_NO")
            //            {
            //                String sre = "fdfs";
            //            }
            //            var fdf = policyTypeMap[columnName];
            //            object tmpvalue = myRange.Cell((int)i, policyTypeMap[columnName]).Value;
            //            if (tmpvalue == null) continue;
            //            if (string.IsNullOrEmpty(tmpvalue.ToString())) continue;
            //            object convertedObj = Convert.ChangeType(tmpvalue != null ? tmpvalue.ToString() : null, targetType);
            //            //if (targetType.Name == "String" && tmpvalue == null)
            //            //    tmpvalue = "";
            //            prop.SetValue(_policy, convertedObj, null);
            //        }
            //        catch { }
            //    }
            //}

        }
        private void FormProcessStatus_Load(object sender, EventArgs e)
        {
            SetActionTag(ActionType.Awaiting);

            //threadCount.Value = 1;
            //threadCount.Maximum = Math.Min(threadCount.Value * 2, 8);

            OutputFileDir = null;

            sonucTypeMap = null;
            policyTypeMap = null;

            try
            {
                //xlApp = new Excel.Application();
                //xlApp.Visible = false;
                //xlApp.DisplayAlerts = false;
                //xlApp.ScreenUpdating = false;
                myWorkbook = new XLWorkbook(FormBase.txtExcelPath.Text);
                mySheet = myWorkbook.Worksheet(FormBase.cbxSheets.SelectedItem.ToString());
                // mySheet = workBook.Worksheet(cbxSheets.SelectedItem.ToString());



                //xlWorkbook = xlApp.Workbooks.Open(Program.FormBase.txtExcelPath.Text, IgnoreReadOnlyRecommended: true, ReadOnly: false, Notify: true);
                //xlWorkbook.CheckCompatibility = false;
                //xlWorkbook.DoNotPromptForConvert = true;

                //Excel.Worksheet xlWorksheet = xlWorkbook.Sheets.Cast<Excel.Worksheet>().Where(s => s.Name == Program.FormBase.cbxSheets.SelectedItem.ToString()).FirstOrDefault();
                if (mySheet != null && mySheet.Rows().Count() > 1)
                {
                    //xlUsedRange = xlWorksheet.UsedRange;

                    #region "Progress Ayarlamaları"

                    if (FormBase.chkbxSort.Checked)
                        SortExcel();
                    if (!CheckAndFillDto()) // zeyl'lerdeki (baslangıc zeyl dahil. status kontrolü... ardından fill list<policydto>....)
                    {
                        MessageBox.Show("Hatalar excel dosyanıza kaydedildi.", null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                    }

                    progressBar1.Maximum = PolicyDtos.Count; //1 satır başlık var
                    int wDiff = lblTotalPolicy.Width;
                    lblTotalPolicy.Text = String.Format(" / {0}", progressBar1.Maximum);
                    wDiff = lblTotalPolicy.Width - wDiff;
                    lblTotalPolicy.Left += wDiff;
                    lblProgress.Height = lblTotalPolicy.Height;
                    lblProgress.Width += wDiff;

                    #endregion
                }
                else
                {
                    MessageBox.Show(String.Format("Seçili veri sayfası \"{0}\" bulunamadı ya da kayıt yok.", FormBase.cbxSheets.SelectedValue), null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }
        private void FormProcessStatus_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }
        private bool CheckAndFillDto()
        {
            bool result = true;
            int errorRow = 0;
            Dictionary<string, int> pMap = null;

            if (policyTypeMap == null)
            {
                policyTypeMap = new Dictionary<string, int>();
                pMap = policyTypeMap;
            }
            if (pMap != null)
            {
                foreach (DataGridViewRow row in FormBase.dataGvCols.Rows.Cast<DataGridViewRow>().Where(r => r.Tag != (object)"1"))
                {
                    pMap.Add(row.Cells[0].Value.ToString(), (int)row.Cells[1].Value);
                }
            }
            if (sonucTypeMap == null)
            {
                sonucTypeMap = new Dictionary<string, int>();
                foreach (DataGridViewRow row in FormBase.dataGvCols.Rows.Cast<DataGridViewRow>().Where(r => r.Tag == (object)"1"))
                {
                    sonucTypeMap.Add(row.Cells[0].Value.ToString(), (int)row.Cells[1].Value);
                }
            }

            try
            {
                int fStart = Math.Max(progressBar1.Value, 2);
                int fFinish = progressBar1.Maximum + 1;

                for (int i = fStart; i <= fFinish; i++)
                {
                    errorRow = i;
                    PolicyDto _policy = new PolicyDto();
                    lock (_syncExcel)
                    {
                        foreach (PropertyInfo prop in _policy.GetType().GetProperties())
                        {
                            if (!prop.GetCustomAttributes(typeof(ColumnAttribute), false).Any())
                                continue;
                            else if (!((prop.GetCustomAttribute(typeof(ColumnAttribute), false) as ColumnAttribute).TypeName == "EXCEL"))
                                continue;
                            String columnName = prop.Name;
                            columnName = (prop.GetCustomAttribute(typeof(ColumnAttribute), false) as ColumnAttribute).Name;
                            if (policyTypeMap.ContainsKey(columnName) == false || policyTypeMap[columnName] < 0) continue;
                            try
                            {
                                Type propertyType = prop.PropertyType;

                                Type targetType = IsNullableType(propertyType) ? Nullable.GetUnderlyingType(propertyType) : propertyType;
                                //prop null check

                                object tmpvalue = mySheet.Cell((int)i, policyTypeMap[columnName]).Value; /* ).Cells[policyTypeMap[columnName]].Value;*/
                                if (tmpvalue == null) continue;
                                if (string.IsNullOrEmpty(tmpvalue.ToString())) continue;
                                object convertedObj = Convert.ChangeType(tmpvalue != null ? tmpvalue.ToString() : null, targetType);
                                //if (targetType.Name == "String" && tmpvalue == null)
                                //    tmpvalue = "";
                                prop.SetValue(_policy, convertedObj, null);

                            }
                            catch (Exception ex)
                            {
                                if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel((int)i, sonucTypeMap["IsSuccess"], false.ToString());

                                if (sonucTypeMap.ContainsKey("Description")) WriteToExcel((int)i, sonucTypeMap["Description"], ex.ToString());
                                result = false;
                                continue;
                            }
                        }
                        if (_policy.AgencyId != null)
                        {
                            _policy.PolicyNo = _policy.CoverPolicyNo != null ? _policy.CoverPolicyNo : _policy.PolicyNo;
                            PolicyDtos.Add(_policy);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel((int)errorRow, sonucTypeMap["IsSuccess"], false.ToString());

                if (sonucTypeMap.ContainsKey("Description")) WriteToExcel((int)errorRow, sonucTypeMap["Description"], ex.ToString());
            }
            if (result && PolicyDtos.Count > 0)
            {
                foreach (PolicyDto item in PolicyDtos)
                {
                    item.BatchPolicyKey = item.PolicyNo + "|" + FormBase.CompanySelectedValue + "|" + item.AgencyId + "|" + item.RenewalNo + "|" + item.EndorsementNo + "|" + item.EndorsementType + "|" + item.PolicyStatus;
                }
                bool diffrentEndors = false;

                string PreviousPolKey = "";
                string CurrentPolKey = "";

                string PreviousStatus = "";
                string CurrentStatus = "";
                int fStart = Math.Max(progressBar1.Value, 2);
                foreach (PolicyDto item in PolicyDtos)
                {
                    int index = PolicyDtos.IndexOf(item);

                    CurrentPolKey = item.BatchPolicyKey;
                    CurrentStatus = item.PolicyStatus;

                    item.BatchPolicyKey = CurrentPolKey;

                    if (index != 0)
                    {
                        if (PreviousPolKey == CurrentPolKey)
                        {
                            if (CurrentStatus != PreviousStatus)
                            {
                                //if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel((int)fStart, sonucTypeMap["IsSuccess"], false.ToString());
                                //if (sonucTypeMap.ContainsKey("Description")) WriteToExcel((int)fStart, sonucTypeMap["Description"], "Aynı Zeylin status'leri aynı olmak zorunda!");

                                var findPolicyKey = PolicyDtos.Where(x => x.BatchPolicyKey == CurrentPolKey);
                                foreach (var itemFindPolKey in findPolicyKey)
                                {
                                    int excelRowNmbr = PolicyDtos.IndexOf(itemFindPolKey) + 2;

                                    if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel((int)excelRowNmbr, sonucTypeMap["IsSuccess"], false.ToString());
                                    if (sonucTypeMap.ContainsKey("Description")) WriteToExcel((int)excelRowNmbr, sonucTypeMap["Description"], "Aynı Zeyl'in status'leri aynı olmak zorunda!");
                                }

                                item.BatchPolicyIsOk = false;
                                result = false;
                            }
                        }
                    }
                    PreviousPolKey = CurrentPolKey;
                    PreviousStatus = CurrentStatus;
                    fStart++;
                }

                foreach (var itemFNSPol in PolicyDtos.Where(x => x.BatchPolicyIsOk == false))
                {
                    var findPolicyKey = PolicyDtos.Where(x => x.BatchPolicyKey == itemFNSPol.BatchPolicyKey);
                    foreach (var itemFNDSetPol in findPolicyKey)
                        itemFNDSetPol.BatchPolicyIsOk = false;
                }
            }
            return result;
        }
        private long GetPolicyIdByPolicyKey(string PolicyKey)
        {
            long PolicyId = 0;

            try
            {
                string PolicyNo = PolicyKey.Split('|')[0];
                string CompanyId = PolicyKey.Split('|')[1];
                string AgencyId = PolicyKey.Split('|')[2];
                string RenewalNo = PolicyKey.Split('|')[3];
                string EndorsementNo = PolicyKey.Split('|')[4];
                string EndorsementType = PolicyKey.Split('|')[5];
                string Status = PolicyKey.Split('|')[6];
                var agencyId = GetAgencyID(long.Parse(AgencyId), long.Parse(CompanyId));

                //PolicyId = new GenericRepository<Policy>().FindBy($"AGENCY_ID = {GetAgencyID(long.Parse(AgencyId), long.Parse(CompanyId))} and STATUS in ('{((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString()}','{((int)ProteinEnums.PolicyStatus.TEKLIF).ToString()}') and RENEWAL_NO =  '{RenewalNo}' and NO = '{PolicyNo}' ", orderby: "", fetchDeletedRows: true).FirstOrDefault().Id;
                PolicyId = new GenericRepository<Policy>().FindBy("AGENCY_ID=:agencyId AND STATUS IN :statusList AND RENEWAL_NO=:renewalNo AND NO=:policyNo", fetchDeletedRows: true,
                    parameters: new { agencyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString(), ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString() }, renewalNo = RenewalNo, policyNo = PolicyNo }).FirstOrDefault().Id;

                new GenericRepository<Endorsement>().FindBy($"POLICY_ID =:PolicyId and NO =:EndorsementNo and TYPE =:EndorsementType", parameters: new { PolicyId, EndorsementNo, EndorsementType });
            }
            catch(Exception ex)
            {
            }
            return PolicyId;
        }
        private long GetEndorsementIdByPolicyKey(string PolicyKey)
        {
            long endorsId = 0;
            try
            {
                string PolicyNo = PolicyKey.Split('|')[0];
                string CompanyId = PolicyKey.Split('|')[1];
                string AgencyId = PolicyKey.Split('|')[2];
                string RenewalNo = PolicyKey.Split('|')[3];
                string EndorsementNo = PolicyKey.Split('|')[4];
                string EndorsementType = PolicyKey.Split('|')[5];
                string Status = PolicyKey.Split('|')[6];

                var agencyId = GetAgencyID(long.Parse(AgencyId), long.Parse(CompanyId));

                //PolicyId = new GenericRepository<Policy>().FindBy($"AGENCY_ID = {GetAgencyID(long.Parse(AgencyId), long.Parse(CompanyId))} and STATUS in ('{((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString()}','{((int)ProteinEnums.PolicyStatus.TEKLIF).ToString()}') and RENEWAL_NO =  '{RenewalNo}' and NO = '{PolicyNo}' ", orderby: "", fetchDeletedRows: true).FirstOrDefault().Id;
                long PolicyId = new GenericRepository<Policy>().FindBy("AGENCY_ID=:agencyId AND STATUS IN :statusList AND RENEWAL_NO=:renewalNo AND NO=:policyNo", fetchDeletedRows: true,
                    parameters: new { agencyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString(), ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString() }, renewalNo = int.Parse(RenewalNo), policyNo = new Dapper.DbString { Value = PolicyNo, Length = 20 } }).FirstOrDefault().Id;
                //endorsId = new GenericRepository<Endorsement>().FindBy($"POLICY_ID = {PolicyId} and NO = '{EndorsementNo}' and TYPE = '{((int)(EnumHelper.GetValueFromText<ProteinEnums.EndorsementType>(EndorsementType))).ToString()}'", orderby: "").FirstOrDefault().Id;
                endorsId = new GenericRepository<Endorsement>().FindBy($"POLICY_ID =:PolicyId and NO =:EndorsementNo and TYPE =:EndorsementType", parameters: new { PolicyId, EndorsementNo, EndorsementType = new Dapper.DbString { Value = ((int)(EnumHelper.GetValueFromText<ProteinEnums.EndorsementType>(EndorsementType))).ToString(), Length = 3 } }).FirstOrDefault().Id;
            }
            catch
            {

            }
            return endorsId;
        }
        private string GetEndorsementTypeByPolicyKey(string PolicyKey)
        {
            string endorsType = "";
            try
            {
                string PolicyNo = PolicyKey.Split('|')[0];
                string CompanyId = PolicyKey.Split('|')[1];
                string AgencyId = PolicyKey.Split('|')[2];
                string RenewalNo = PolicyKey.Split('|')[3];
                string EndorsementNo = PolicyKey.Split('|')[4];
                string EndorsementType = PolicyKey.Split('|')[5];
                string Status = PolicyKey.Split('|')[6];

                endorsType = EndorsementType;
            }
            catch
            {

            }
            return endorsType;
        }
        private string GetStatusByPolicyKey(string PolicyKey)
        {
            string status = "";
            try
            {
                string PolicyNo = PolicyKey.Split('|')[0];
                string CompanyId = PolicyKey.Split('|')[1];
                string AgencyId = PolicyKey.Split('|')[2];
                string RenewalNo = PolicyKey.Split('|')[3];
                string EndorsementNo = PolicyKey.Split('|')[4];
                string EndorsementType = PolicyKey.Split('|')[5];
                string Status = PolicyKey.Split('|')[6];

                status = Status;
            }
            catch
            {

            }
            return status;
        }
        private long? GetAgencyID(long? AgencyCode, long CompanyId)
        {
            long? Id = null;
            try
            {

                var resAgency = new GenericRepository<Agency>().FindBy("COMPANY_ID =:CompanyId and NO =:AgencyCode", parameters: new { CompanyId, AgencyCode = new Dapper.DbString { Value = AgencyCode.ToString(), Length = 20 } });

                if (resAgency.Count > 0)
                {
                    var resPackage = new GenericRepository<V_Package>().FindBy($"COMPANY_ID =:CompanyId", parameters: new { CompanyId }, orderby: "");
                    if (resPackage.Count > 0)
                        Id = resAgency.FirstOrDefault().Id;
                }
            }
            catch { }
            return Id;
        }
    }
}
