﻿namespace PolicyBatch
{
    partial class FormBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblExcelPath = new System.Windows.Forms.Label();
            this.txtExcelPath = new System.Windows.Forms.TextBox();
            this.btnExcelPath = new System.Windows.Forms.Button();
            this.lblExcelSheet = new System.Windows.Forms.Label();
            this.cbxSheets = new System.Windows.Forms.ComboBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.dataGvCols = new System.Windows.Forms.DataGridView();
            this.DB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnExcel = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.lblColumnMatch = new System.Windows.Forms.Label();
            this.btnShowList = new System.Windows.Forms.Button();
            this.lblCompany = new System.Windows.Forms.Label();
            this.cmbxCompany = new System.Windows.Forms.ComboBox();
            this.chkbxSort = new System.Windows.Forms.CheckBox();
            this.chkbxPrint = new System.Windows.Forms.CheckBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dataGvCols)).BeginInit();
            this.SuspendLayout();
            // 
            // lblExcelPath
            // 
            this.lblExcelPath.AutoSize = true;
            this.lblExcelPath.Location = new System.Drawing.Point(12, 39);
            this.lblExcelPath.Name = "lblExcelPath";
            this.lblExcelPath.Size = new System.Drawing.Size(85, 17);
            this.lblExcelPath.TabIndex = 0;
            this.lblExcelPath.Text = "Excel Dosya";
            // 
            // txtExcelPath
            // 
            this.txtExcelPath.Location = new System.Drawing.Point(149, 36);
            this.txtExcelPath.Name = "txtExcelPath";
            this.txtExcelPath.ReadOnly = true;
            this.txtExcelPath.Size = new System.Drawing.Size(522, 22);
            this.txtExcelPath.TabIndex = 1;
            // 
            // btnExcelPath
            // 
            this.btnExcelPath.Location = new System.Drawing.Point(677, 34);
            this.btnExcelPath.Name = "btnExcelPath";
            this.btnExcelPath.Size = new System.Drawing.Size(119, 27);
            this.btnExcelPath.TabIndex = 2;
            this.btnExcelPath.Text = "Seç";
            this.btnExcelPath.UseVisualStyleBackColor = true;
            this.btnExcelPath.Click += new System.EventHandler(this.btnExcelPath_Click);
            // 
            // lblExcelSheet
            // 
            this.lblExcelSheet.AutoSize = true;
            this.lblExcelSheet.Location = new System.Drawing.Point(15, 81);
            this.lblExcelSheet.Name = "lblExcelSheet";
            this.lblExcelSheet.Size = new System.Drawing.Size(82, 17);
            this.lblExcelSheet.TabIndex = 3;
            this.lblExcelSheet.Text = "Excel Sheet";
            // 
            // cbxSheets
            // 
            this.cbxSheets.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxSheets.FormattingEnabled = true;
            this.cbxSheets.Location = new System.Drawing.Point(149, 74);
            this.cbxSheets.Name = "cbxSheets";
            this.cbxSheets.Size = new System.Drawing.Size(522, 24);
            this.cbxSheets.TabIndex = 4;
            this.cbxSheets.SelectedIndexChanged += new System.EventHandler(this.cbxSheets_SelectedIndexChanged);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Excel Dosyası (*.xls, *.xlsx)|*.xls;*.xlsx";
            // 
            // dataGvCols
            // 
            this.dataGvCols.AllowUserToAddRows = false;
            this.dataGvCols.AllowUserToDeleteRows = false;
            this.dataGvCols.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGvCols.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DB,
            this.columnExcel});
            this.dataGvCols.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGvCols.Location = new System.Drawing.Point(147, 163);
            this.dataGvCols.MultiSelect = false;
            this.dataGvCols.Name = "dataGvCols";
            this.dataGvCols.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGvCols.RowTemplate.Height = 24;
            this.dataGvCols.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGvCols.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGvCols.Size = new System.Drawing.Size(649, 381);
            this.dataGvCols.TabIndex = 1;
            this.dataGvCols.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGvCols_CellContentClick);
            // 
            // DB
            // 
            this.DB.HeaderText = "DB";
            this.DB.Name = "DB";
            this.DB.ReadOnly = true;
            this.DB.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DB.Width = 190;
            // 
            // columnExcel
            // 
            this.columnExcel.HeaderText = "Excel";
            this.columnExcel.Name = "columnExcel";
            this.columnExcel.Width = 210;
            // 
            // lblColumnMatch
            // 
            this.lblColumnMatch.AutoSize = true;
            this.lblColumnMatch.Location = new System.Drawing.Point(12, 163);
            this.lblColumnMatch.Name = "lblColumnMatch";
            this.lblColumnMatch.Size = new System.Drawing.Size(129, 17);
            this.lblColumnMatch.TabIndex = 6;
            this.lblColumnMatch.Text = "Kolon Karşılaştırma";
            // 
            // btnShowList
            // 
            this.btnShowList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnShowList.Location = new System.Drawing.Point(651, 554);
            this.btnShowList.Name = "btnShowList";
            this.btnShowList.Size = new System.Drawing.Size(142, 28);
            this.btnShowList.TabIndex = 7;
            this.btnShowList.Text = "Devam >>>";
            this.btnShowList.UseVisualStyleBackColor = true;
            this.btnShowList.Click += new System.EventHandler(this.btnShowList_Click);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.Location = new System.Drawing.Point(53, 118);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(44, 17);
            this.lblCompany.TabIndex = 8;
            this.lblCompany.Text = "Şirket";
            // 
            // cmbxCompany
            // 
            this.cmbxCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxCompany.FormattingEnabled = true;
            this.cmbxCompany.Location = new System.Drawing.Point(147, 115);
            this.cmbxCompany.Name = "cmbxCompany";
            this.cmbxCompany.Size = new System.Drawing.Size(524, 24);
            this.cmbxCompany.TabIndex = 9;
            // 
            // chkbxSort
            // 
            this.chkbxSort.AutoSize = true;
            this.chkbxSort.Checked = true;
            this.chkbxSort.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkbxSort.Location = new System.Drawing.Point(147, 550);
            this.chkbxSort.Name = "chkbxSort";
            this.chkbxSort.Size = new System.Drawing.Size(485, 38);
            this.chkbxSort.TabIndex = 10;
            this.chkbxSort.Text = "Listeyi Sigortalıya Göre Sırala\r\n(Zeyl harici işlemlerde seçmeyiniz. Örn: Ödeme i" +
    "şlemlerinde seçilemez..)";
            this.chkbxSort.UseVisualStyleBackColor = true;
            this.chkbxSort.CheckedChanged += new System.EventHandler(this.chkbxSort_CheckedChanged);
            // 
            // chkbxPrint
            // 
            this.chkbxPrint.AutoSize = true;
            this.chkbxPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.chkbxPrint.Location = new System.Drawing.Point(677, 76);
            this.chkbxPrint.Name = "chkbxPrint";
            this.chkbxPrint.Size = new System.Drawing.Size(107, 21);
            this.chkbxPrint.TabIndex = 11;
            this.chkbxPrint.Text = "Basım İşlemi";
            this.chkbxPrint.UseVisualStyleBackColor = true;
            this.chkbxPrint.CheckedChanged += new System.EventHandler(this.chkbxPrint_CheckedChanged);
            // 
            // FormBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(805, 590);
            this.Controls.Add(this.chkbxPrint);
            this.Controls.Add(this.chkbxSort);
            this.Controls.Add(this.cmbxCompany);
            this.Controls.Add(this.lblCompany);
            this.Controls.Add(this.btnShowList);
            this.Controls.Add(this.lblColumnMatch);
            this.Controls.Add(this.dataGvCols);
            this.Controls.Add(this.cbxSheets);
            this.Controls.Add(this.lblExcelSheet);
            this.Controls.Add(this.btnExcelPath);
            this.Controls.Add(this.txtExcelPath);
            this.Controls.Add(this.lblExcelPath);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormBase";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Policy Transfer";
            ((System.ComponentModel.ISupportInitialize)(this.dataGvCols)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblExcelPath;
        protected internal System.Windows.Forms.TextBox txtExcelPath;
        private System.Windows.Forms.Button btnExcelPath;
        private System.Windows.Forms.Label lblExcelSheet;
        protected internal System.Windows.Forms.ComboBox cbxSheets;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        protected internal System.Windows.Forms.DataGridView dataGvCols;
        private System.Windows.Forms.Label lblColumnMatch;
        private System.Windows.Forms.Button btnShowList;
        private System.Windows.Forms.Label lblCompany;
        protected internal System.Windows.Forms.ComboBox cmbxCompany;
        protected internal System.Windows.Forms.CheckBox chkbxSort;
        private System.Windows.Forms.CheckBox chkbxPrint;
        protected internal System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.DataGridViewTextBoxColumn DB;
        private System.Windows.Forms.DataGridViewComboBoxColumn columnExcel;
    }
}

