﻿namespace PolicyBatch
{
    partial class FormClaimStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExcelPath = new System.Windows.Forms.Button();
            this.txtExcelPath = new System.Windows.Forms.TextBox();
            this.lblExcelPath = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.dataGvCols = new System.Windows.Forms.DataGridView();
            this.DB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnExcel = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.chkbxSort = new System.Windows.Forms.CheckBox();
            this.btnShowList = new System.Windows.Forms.Button();
            this.lblColumnMatch = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dataGvCols)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExcelPath
            // 
            this.btnExcelPath.Location = new System.Drawing.Point(673, 5);
            this.btnExcelPath.Name = "btnExcelPath";
            this.btnExcelPath.Size = new System.Drawing.Size(119, 27);
            this.btnExcelPath.TabIndex = 26;
            this.btnExcelPath.Text = "Seç";
            this.btnExcelPath.UseVisualStyleBackColor = true;
            this.btnExcelPath.Click += new System.EventHandler(this.btnExcelPath_Click);
            // 
            // txtExcelPath
            // 
            this.txtExcelPath.Location = new System.Drawing.Point(145, 7);
            this.txtExcelPath.Name = "txtExcelPath";
            this.txtExcelPath.ReadOnly = true;
            this.txtExcelPath.Size = new System.Drawing.Size(522, 22);
            this.txtExcelPath.TabIndex = 24;
            // 
            // lblExcelPath
            // 
            this.lblExcelPath.AutoSize = true;
            this.lblExcelPath.Location = new System.Drawing.Point(8, 10);
            this.lblExcelPath.Name = "lblExcelPath";
            this.lblExcelPath.Size = new System.Drawing.Size(85, 17);
            this.lblExcelPath.TabIndex = 23;
            this.lblExcelPath.Text = "Excel Dosya";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Excel Dosyası (*.xls, *.xlsx)|*.xls;*.xlsx";
            // 
            // dataGvCols
            // 
            this.dataGvCols.AllowUserToAddRows = false;
            this.dataGvCols.AllowUserToDeleteRows = false;
            this.dataGvCols.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGvCols.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DB,
            this.columnExcel});
            this.dataGvCols.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGvCols.Location = new System.Drawing.Point(144, 129);
            this.dataGvCols.MultiSelect = false;
            this.dataGvCols.Name = "dataGvCols";
            this.dataGvCols.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGvCols.RowTemplate.Height = 24;
            this.dataGvCols.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGvCols.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGvCols.Size = new System.Drawing.Size(649, 381);
            this.dataGvCols.TabIndex = 25;
            // 
            // DB
            // 
            this.DB.HeaderText = "DB";
            this.DB.Name = "DB";
            this.DB.ReadOnly = true;
            this.DB.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DB.Width = 190;
            // 
            // columnExcel
            // 
            this.columnExcel.HeaderText = "Excel";
            this.columnExcel.Name = "columnExcel";
            this.columnExcel.Width = 210;
            // 
            // chkbxSort
            // 
            this.chkbxSort.AutoSize = true;
            this.chkbxSort.Checked = true;
            this.chkbxSort.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkbxSort.Location = new System.Drawing.Point(144, 516);
            this.chkbxSort.Name = "chkbxSort";
            this.chkbxSort.Size = new System.Drawing.Size(485, 38);
            this.chkbxSort.TabIndex = 29;
            this.chkbxSort.Text = "Listeyi Sigortalıya Göre Sırala\r\n(Zeyl harici işlemlerde seçmeyiniz. Örn: Ödeme i" +
    "şlemlerinde seçilemez..)";
            this.chkbxSort.UseVisualStyleBackColor = true;
            // 
            // btnShowList
            // 
            this.btnShowList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnShowList.Location = new System.Drawing.Point(648, 520);
            this.btnShowList.Name = "btnShowList";
            this.btnShowList.Size = new System.Drawing.Size(142, 28);
            this.btnShowList.TabIndex = 28;
            this.btnShowList.Text = "Devam >>>";
            this.btnShowList.UseVisualStyleBackColor = true;
            this.btnShowList.Click += new System.EventHandler(this.btnShowList_Click);
            // 
            // lblColumnMatch
            // 
            this.lblColumnMatch.AutoSize = true;
            this.lblColumnMatch.Location = new System.Drawing.Point(9, 129);
            this.lblColumnMatch.Name = "lblColumnMatch";
            this.lblColumnMatch.Size = new System.Drawing.Size(129, 17);
            this.lblColumnMatch.TabIndex = 27;
            this.lblColumnMatch.Text = "Kolon Karşılaştırma";
            // 
            // FormClaimStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 569);
            this.Controls.Add(this.btnExcelPath);
            this.Controls.Add(this.txtExcelPath);
            this.Controls.Add(this.lblExcelPath);
            this.Controls.Add(this.dataGvCols);
            this.Controls.Add(this.chkbxSort);
            this.Controls.Add(this.btnShowList);
            this.Controls.Add(this.lblColumnMatch);
            this.Name = "FormClaimStatus";
            this.Text = "FormClaimStatus";
            this.Load += new System.EventHandler(this.FormClaimStatus_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGvCols)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExcelPath;
        protected internal System.Windows.Forms.TextBox txtExcelPath;
        private System.Windows.Forms.Label lblExcelPath;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        protected internal System.Windows.Forms.DataGridView dataGvCols;
        private System.Windows.Forms.DataGridViewTextBoxColumn DB;
        private System.Windows.Forms.DataGridViewComboBoxColumn columnExcel;
        protected internal System.Windows.Forms.CheckBox chkbxSort;
        private System.Windows.Forms.Button btnShowList;
        private System.Windows.Forms.Label lblColumnMatch;
        protected internal System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}