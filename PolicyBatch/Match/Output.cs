﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolicyBatch.Match
{
    public class Output
    {
        public bool IsSuccess { get; set; }
        public string Description { get; set; }
    }
}
