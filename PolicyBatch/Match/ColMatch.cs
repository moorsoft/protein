﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolicyBatch.Match
{
    //public class ColMatch
    //{
    //    [Column("Poliçe No"), Required]
    //    public string PolicyNo { get; set; }
    //    [Column("Acente No"), Required]
    //    public string AgencyNo { get; set; }
    //    [Column("Poliçe Tipi"), Required]
    //    public string PolicyType { get; set; }
    //    [Column("Yenileme No"), Required]
    //    public string RenewalNo { get; set; }
    //    [Column("Başlama Tarihi"), Required]
    //    public DateTime StartDate { get; set; }
    //    [Column("Bitiş Tarihi"), Required]
    //    public DateTime EndDate { get; set; }
    //    [Column("Tanzim Tarihi"), Required]
    //    public DateTime IssueDate { get; set; }
    //    [Column("Komisyon Oran"), Required]
    //    public string Commission { get; set; }
    //    [Column("Sigorta Ettiren Tip"), Required]
    //    public string InsurerType { get; set; }
    //    [Column("Sigorta Ettiren İsim"), Required]
    //    public string InsurerName { get; set; }
    //    [Column("Sigorta Ettiren Soyisim"), Required]
    //    public string InsurerLastName { get; set; }
    //    [Column("Sigorta Ettiren Cinsiyet"), Required]
    //    public string InsurerGender { get; set; }
    //    [Column("Sigorta Ettiren Doğum Yeri"), Required]
    //    public string InsurerBirthPlace { get; set; }
    //    [Column("Sigorta Ettiren Doğum Tarihi"), Required]
    //    public DateTime InsurerBirthDate { get; set; }

    //    [Column("Sigorta Ettiren Baba Adı")]
    //    public string InsurerFatherName { get; set; }
    //    [Column("Sigorta Ettiren Tc No"), Required]
    //    public string InsurerIdentityNo { get; set; }
    //    [Column("Sigorta Ettiren Vkn")]
    //    public string InsurerVkn { get; set; }
    //    [Column("Sigorta Ettiren Uyruk")]
    //    public string InsurerNationality { get; set; }
    //    [Column("Sigorta Ettiren Pasaport No")]
    //    public string InsurerPassportNo { get; set; }
    //    [Column("Sigorta Ettiren Adres"), Required]
    //    public string InsurerAddress { get; set; }
    //    [Column("Sigorta Ettiren Il Kodu"), Required]
    //    public string InsurerCityCode { get; set; }
    //    [Column("Sigorta Ettiren Tel No")]
    //    public string InsurerPhoneNo { get; set; }
    //    [Column("Sigorta Ettiren Gsm No")]
    //    public string InsurerGsmNo { get; set; }
    //    [Column("Sigorta Ettiren Email")]
    //    public string InsurerEmail { get; set; }
    //    [Column("Sigortalı No"), Required]
    //    public string InsuredNo { get; set; }
    //    [Column("Sigortalı Isim"), Required]
    //    public string InsuredName { get; set; }
    //    [Column("Sigortalı Soyad"), Required]
    //    public string InsuredLastName { get; set; }
    //    [Column("Sigortalı Cinsiyet"), Required]
    //    public string InsuredGender { get; set; }
    //    [Column("Sigortalı Doğum Tarihi")]
    //    public DateTime InsuredBirtDate { get; set; }
    //    [Column("Sigortalı Doğum Yer")]
    //    public DateTime InsuredBirtPlace { get; set; }
    //    [Column("Sigortalı Baba Adı"), Required]
    //    public string InsuredFatherName { get; set; }
    //    [Column("Sigortalı Şirkete Giriş Tarihi"), Required]
    //    public DateTime InsuredCompanyEntranceDate { get; set; }
    //    [Column("Sigortalı Plan Kodu"), Required]
    //    public string InsuredPlanCode { get; set; }
    //    [Column("Sigortalı Yakınlık"), Required]
    //    public string InsuredIndividual { get; set; }
    //    [Column("Sigortalı Prim"), Required]
    //    public string InsuredInitialPremium { get; set; }
    //    [Column("Sigortalı Poliçe Giriş Tarihi"), Required]
    //    public DateTime InsuredPolicyEntranceDate { get; set; }
    //    [Column("Sigortalı Tc No"), Required]
    //    public string InsuredIdentityNo { get; set; }
    //    [Column("Sigortalı Uyruk"), Required]
    //    public string InsuredNationality { get; set; }
    //    [Column("Sigortalı Pasaport")]
    //    public string InsuredPassport { get; set; }
    //    [Column("Sigortalı Aile No"), Required]
    //    public string InsuredFamilyNo { get; set; }
    //    [Column("Sigortalı Sicil No"), Required]
    //    public string InsuredRegistrationNo { get; set; }
    //    [Column("Sigortalı Istisna")]
    //    public string InsuredExclusion { get; set; }
    //    [Column("SG_OBYG")]
    //    public string InsuredRenewalGuaranteeType { get; set; }
    //    [Column("SG_VIP")]
    //    public string InsuredVip { get; set; }

    //    [Column("SG_KAZANILMIS_HAK")]
    //    [Required]
    //    public string InsuredKazanilmisHak { get; set; }
    //}
}
