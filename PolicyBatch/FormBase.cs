﻿using ClosedXML.Excel;
using PolicyBatch.Match;
using Protein.Common.Attributes;
using Protein.Common.Dto.ImportObjects;
using Protein.Common.Enums;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Protein.Common.Entities.ProteinEntities;
using static System.Windows.Forms.DataGridViewComboBoxCell;

namespace PolicyBatch
{
    public partial class FormBase : Form
    {
        private static String ConnString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 12.0 Xml;HDR=YES"";";
        public static OleDbConnection objConn = null;
        public string NotMatched = "(EŞLEŞMEDİ)";
        public string CompanySelectedValue { get; set; }
        public void Close()
        {
            if (objConn != null)
            {
                objConn.Close();
                objConn.Dispose();
            }
        }
        public FormBase()
        {
            InitializeComponent();

            //foreach (PropertyInfo prop in typeof(PolicyDto).GetProperties())
            //{
            //    if (!prop.GetCustomAttributes(typeof(ColumnAttribute), false).Any())
            //        continue;
            //    else if (!((prop.GetCustomAttribute(typeof(ColumnAttribute), false) as ColumnAttribute).TypeName == "EXCEL"))
            //        continue;

            //    String columnName = prop.Name;

            //    columnName = (prop.GetCustomAttribute(typeof(ColumnAttribute), false) as ColumnAttribute).Name;

            //    dataGvCols.Rows.Add();
            //    DataGridViewRow row = dataGvCols.Rows[dataGvCols.Rows.GetLastRow(DataGridViewElementStates.None)];
            //    row.Cells[0].Value = columnName;
            //    row.Cells[0].Tag = prop.Name;
            //}
        }
        private void btnExcelPath_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                cbxSheets.Items.Clear();
                cmbxCompany.DataSource = null;
                cmbxCompany.Items.Clear();
                //cmbxEndorType.DataSource = null;
                //cmbxEndorType.Items.Clear();
                dataGvCols.Rows.Clear();
                //Microsoft.Office.Interop.Excel.Application xlApp = null;
                //Microsoft.Office.Interop.Excel.Workbook xlWorkbook = null;
                bool OK = false;
                try
                {
                    List<Company> company = new GenericRepository<Company>().FindBy();


                    cmbxCompany.ValueMember = "Key";
                    cmbxCompany.DisplayMember = "Value";
                    cmbxCompany.DataSource = new BindingSource(company
                                            .Select(s => new KeyValuePair<int, string>(int.Parse(s.Id.ToString()), s.Name))
                                            .ToList(), null);

                    List<EnumValue> enumValues = EnumHelper.GetValues<ProteinEnums.EndorsementType>();

                    using (XLWorkbook workBook = new XLWorkbook(openFileDialog1.FileName))
                    {
                        cbxSheets.Items.AddRange(workBook.Worksheets.Cast<IXLWorksheet>().Select(s => s.Name).ToArray());
                        OK = true;
                    }

                    if (OK)
                    {
                        txtExcelPath.Text = openFileDialog1.FileName;
                       cbxSheets.SelectedIndex = 0;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void cbxSheets_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGvCols.Rows.Clear();
            dataGvCols.Refresh();
            DataGridViewComboBoxColumn sColumn = (dataGvCols.Columns["columnExcel"] as DataGridViewComboBoxColumn);
            if (sColumn.IsDataBound) sColumn.DataSource = null;

            //Microsoft.Office.Interop.Excel.Application xlApp = null;
            //Microsoft.Office.Interop.Excel.Workbook xlWorkbook = null;
            try
            {
                using (XLWorkbook workBook = new XLWorkbook(openFileDialog1.FileName))
                {
                    IXLWorksheet mySheet = workBook.Worksheet(cbxSheets.SelectedItem.ToString());

                    //    xlApp = new Microsoft.Office.Interop.Excel.Application();
                    //    xlWorkbook = xlApp.Workbooks.Open(txtExcelPath.Text, ReadOnly: true, Notify: false);

                    //Microsoft.Office.Interop.Excel.Worksheet mySheet = xlWorkbook.Sheets.Cast<Microsoft.Office.Interop.Excel.Worksheet>().Where(s => s.Name == cbxSheets.SelectedItem.ToString()).FirstOrDefault();

                    if (mySheet != null)
                    {
                        List<KeyValuePair<int, string>> lstColumn = new List<KeyValuePair<int, string>>();


                        bool firstRow = true;
                        foreach (IXLRow row in mySheet.Rows())
                        {

                            if (firstRow)
                            {
                                int colCnt = 1;

                                foreach (IXLCell cell in row.Cells())
                                {
                                    lstColumn.Add(new KeyValuePair<int, string>(colCnt, cell.Value.ToString()));
                                    colCnt++;
                                }
                                break;
                            }
                        }
                        lstColumn = lstColumn.OrderBy(t => t.Value).ToList();
                        lstColumn.Insert(0, new KeyValuePair<int, string>(-1, NotMatched));

                        sColumn.ValueMember = "Key";
                        sColumn.DisplayMember = "Value";
                        sColumn.DataSource = new BindingSource(lstColumn, null);
                        lstColumn = null;

                        foreach (DataGridViewRow row in dataGvCols.Rows)
                        {
                            row.Cells[1].Value = -1;
                            //row.Cells[0].Style.ForeColor = System.Drawing.Color.Red;
                        }
                        dataGvCols.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                //if (xlWorkbook != null) xlWorkbook.Close();
                //if (xlApp != null) xlApp.Quit();

                //xlWorkbook = null;
                //xlApp = null;
            }
            Type outParam = null;
            string[] paramNames = null;
            List<string> lstParams = new List<string>();
            foreach (PropertyInfo prop in typeof(PolicyDto).GetProperties())
            {
                if (!prop.GetCustomAttributes(typeof(ColumnAttribute), false).Any())
                    continue;
                else if (!((prop.GetCustomAttribute(typeof(ColumnAttribute), false) as ColumnAttribute).TypeName == "EXCEL"))
                    continue;

                String columnName = prop.Name;

                columnName = (prop.GetCustomAttribute(typeof(ColumnAttribute), false) as ColumnAttribute).Name;
                lstParams.Add(columnName);
                //dataGvCols.Rows.Add();
                //DataGridViewRow row = dataGvCols.Rows[dataGvCols.Rows.GetLastRow(DataGridViewElementStates.None)];
                //row.Cells[0].Value = columnName;
                //row.Cells[0].Tag = prop.Name;
            }
            paramNames = lstParams.ToArray();
            outParam = typeof(Output);

            foreach (string value in paramNames)
            {
                int inx = dataGvCols.Rows.Add(value, String.Empty);
                DataGridViewRow row = dataGvCols.Rows[inx];
                row.Cells[1].Value = -1;
            }
            if (outParam == null) return;
            foreach (string value in outParam.GetProperties().Select(p => p.Name).ToArray())
            {
                int inx = dataGvCols.Rows.Add(value, String.Empty);
                DataGridViewRow row = dataGvCols.Rows[inx];
                row.Tag = "1";
                row.Cells[0].Style.ForeColor = System.Drawing.Color.Blue;
                row.Cells[1].Value = -1;
            }

            for (int i = 0; i < dataGvCols.Rows.Count; i++)
            {
                DataGridViewComboBoxCell myCell = (DataGridViewComboBoxCell)dataGvCols.Rows[i].Cells[1];

                try
                {
                    bool matched = false;
                    foreach (var cellItem in myCell.Items)
                    {
                        var item = (KeyValuePair<Int32, string>)cellItem;

                        if (item.Value.ToString() == dataGvCols.Rows[i].Cells[0].Value.ToString())
                        {
                            myCell.Value = item.Key;
                            //dataGvCols.Rows[i].Cells[1].Value = dataGvCols.Rows[i].Cells[0].Value;
                            matched = true;

                        }
                    }
                    if (!matched)
                    {
                        myCell.Value = -1;
                        dataGvCols.Rows[i].Cells[0].Style.BackColor = System.Drawing.Color.Red;
                        dataGvCols.Rows[i].Cells[0].Style.ForeColor = System.Drawing.Color.White;
                        //myCell.Style = System.Drawing.Color.Red;
                    }
                }
                catch
                {
                    myCell.Value = -1;
                }
            }

        }
        private void btnShowList_Click(object sender, EventArgs e)
        {

            this.CompanySelectedValue = cmbxCompany.SelectedValue.ToString();

            int requiredParamCount = dataGvCols.Rows.Cast<DataGridViewRow>().Where(r => r.Tag != (object)"1").Count();
            int matchedParamCount = dataGvCols.Rows.Cast<DataGridViewRow>().Where(r => r.Tag != (object)"1" && r.Cells[1].Value != DBNull.Value && (int)r.Cells[1].Value > -1).Count();

            if (matchedParamCount == 0)
            {
                MessageBox.Show("Lütfen parametre eşleştirmelerini yapınız.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else if (matchedParamCount < requiredParamCount && MessageBox.Show("Eşleştirmediğiniz parametreler var.\nBu şekilde devam etmek istiyor musunuz?", null, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            if (dataGvCols.Rows.Cast<DataGridViewRow>().Where(r => r.Tag == (object)"1" && r.Cells[1].Value.ToString() != "-1").Count() == 0)
            {
                if (MessageBox.Show("Hiçbir sonuç parametresini eşleştirmediniz.\nİşlem sonuçlarını göremeyeceksiniz devam etmek istiyor musunuz?", null, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.OK) return;
            }

            if (chkbxPrint.Checked)
            {
                folderBrowserDialog1.Description = "Basımları kaydedecek klasörü seçiniz";
                folderBrowserDialog1.ShowNewFolderButton = true;

                if (folderBrowserDialog1.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    MessageBox.Show("Basım işlemi seçtiğiniz için basım klasörünü seçmeniz gerekiyordu", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }

            FormProcessStatus frmProcess = new FormProcessStatus(this);
            this.Hide();
            frmProcess.Show();
        }
        private void chkbxPrint_CheckedChanged(object sender, EventArgs e)
        {
            chkbxSort.Checked = chkbxPrint.Checked ? false : chkbxSort.Checked;
        }
        private void chkbxSort_CheckedChanged(object sender, EventArgs e)
        {
            chkbxSort.Checked = chkbxPrint.Checked ? false : chkbxSort.Checked;
        }

        private void dataGvCols_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
