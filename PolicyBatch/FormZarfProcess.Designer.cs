﻿using System;

namespace PolicyBatch
{
    partial class FormZarfProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGo = new System.Windows.Forms.Button();
            this.lblProgress = new System.Windows.Forms.Label();
            this.lblTotalPolicy = new System.Windows.Forms.Label();
            this.lblPolicyStatus = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // btnGo
            // 
            this.btnGo.Location = new System.Drawing.Point(949, 65);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(75, 29);
            this.btnGo.TabIndex = 11;
            this.btnGo.Tag = "0";
            this.btnGo.Text = "Başlat";
            this.btnGo.UseVisualStyleBackColor = true;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click_1);
            // 
            // lblProgress
            // 
            this.lblProgress.AutoSize = true;
            this.lblProgress.Location = new System.Drawing.Point(136, 16);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(16, 17);
            this.lblProgress.TabIndex = 10;
            this.lblProgress.Text = "0";
            // 
            // lblTotalPolicy
            // 
            this.lblTotalPolicy.AutoSize = true;
            this.lblTotalPolicy.Location = new System.Drawing.Point(152, 16);
            this.lblTotalPolicy.Name = "lblTotalPolicy";
            this.lblTotalPolicy.Size = new System.Drawing.Size(24, 17);
            this.lblTotalPolicy.TabIndex = 9;
            this.lblTotalPolicy.Text = "/ 0";
            // 
            // lblPolicyStatus
            // 
            this.lblPolicyStatus.AutoSize = true;
            this.lblPolicyStatus.Location = new System.Drawing.Point(12, 16);
            this.lblPolicyStatus.Name = "lblPolicyStatus";
            this.lblPolicyStatus.Size = new System.Drawing.Size(95, 17);
            this.lblPolicyStatus.TabIndex = 8;
            this.lblPolicyStatus.Text = "Poliçe İşlenen";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 36);
            this.progressBar1.Maximum = 500;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(1013, 23);
            this.progressBar1.Step = 1;
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 7;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // FormZarfProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1029, 102);
            this.Controls.Add(this.btnGo);
            this.Controls.Add(this.lblProgress);
            this.Controls.Add(this.lblTotalPolicy);
            this.Controls.Add(this.lblPolicyStatus);
            this.Controls.Add(this.progressBar1);
            this.Name = "FormZarfProcess";
            this.Text = "FormZarfProcess";
            this.Load += new System.EventHandler(this.FormZarfProcess_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGo;
        private System.Windows.Forms.Label lblProgress;
        private System.Windows.Forms.Label lblTotalPolicy;
        private System.Windows.Forms.Label lblPolicyStatus;
        public System.Windows.Forms.ProgressBar progressBar1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}