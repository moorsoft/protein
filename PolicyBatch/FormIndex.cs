﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolicyBatch
{
    public partial class FormIndex : Form
    {
        public FormIndex()
        {
            InitializeComponent();
        }

        private void FormIndex_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormBase formBase = new FormBase();
            formBase.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormPayrollStatus formPayrollStatus = new FormPayrollStatus();
            formPayrollStatus.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FormClaimStatus formClaimStatus = new FormClaimStatus();
            formClaimStatus.Show();
            this.Hide();
        }
    }
}
