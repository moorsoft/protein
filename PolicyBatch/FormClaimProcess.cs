﻿using ClosedXML.Excel;
using Protein.Common.Dto;
using Protein.Common.Dto.ImportObjects;
using Protein.Common.Enums;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace PolicyBatch
{
    public partial class FormClaimProcess : Form
    {
        private static bool IsNullableType(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
        }
        private static CultureInfo trCulture = new CultureInfo("tr-TR");
        private string OutputFileDir = null;
        //private long PreviousPolicyId { get; set; } = 0;
        private List<PayrollDto> PayrollDtos = new List<PayrollDto>();
        private List<ClaimDto> ClaimDtos = new List<ClaimDto>();
        private string PreviousPolicyKey { get; set; }


        Dictionary<long, string> errList = new Dictionary<long, string>();
        private static IXLWorksheet mySheet = null;
        private static IXLRange myRange = null;
        private static XLWorkbook myWorkbook = null;

        private static object _syncExcel = new object();
        //private static Excel.Application xlApp = null;
        //private static Excel.Workbook xlWorkbook = null;
        //private static Excel.Range xlUsedRange = null;
        //private static ProcessType CurrentProcess;
        private List<long> InstertedInstalmentIds = new List<long>();
        private static Dictionary<string, int> sonucTypeMap = null;
        private static SemaphoreSlim _pool;
        private static int maxthread = 0;
        private static Dictionary<string, int> policyTypeMap = null;
        private FormPayrollStatus formPayrollStatus = new FormPayrollStatus();
        private FormClaimStatus formClaimStatus = new FormClaimStatus();
        int number = 1;
        private enum ActionType
        {
            Awaiting = 0,
            Running = 1,
            Pausing = 2,
            Finish = 3
        }

        public FormClaimProcess(FormClaimStatus formClaimStatus)
        {
            InitializeComponent();
            this.formClaimStatus = formClaimStatus;
        }
        private void SetActionTag(ActionType tag)
        {
            btnGo.Tag = tag;
            string label = btnGo.Text;
            switch (tag)
            {
                case ActionType.Awaiting:
                    label = "Başlat";
                    break;
                case ActionType.Pausing:
                    label = "Sürdür";
                    break;
                case ActionType.Running:
                    label = "Duraklat";
                    break;
                case ActionType.Finish:
                    label = "Çıkış";
                    break;
            }
            if (btnGo.InvokeRequired)
            {
                btnGo.Invoke(new Action(() =>
                {
                    btnGo.Text = label;
                    btnGo.Enabled = true;
                }));
            }
            else
            {
                btnGo.Text = label;
                btnGo.Enabled = true;
            }
        }
        private void FormClaimProcess_Load(object sender, EventArgs e)
        {
            SetActionTag(ActionType.Awaiting);

            //threadCount.Value = 1;
            //threadCount.Maximum = Math.Min(threadCount.Value * 2, 8);

            OutputFileDir = null;

            sonucTypeMap = null;
            policyTypeMap = null;

            try
            {
                //xlApp = new Excel.Application();
                //xlApp.Visible = false;
                //xlApp.DisplayAlerts = false;
                //xlApp.ScreenUpdating = false;
                myWorkbook = new XLWorkbook(formClaimStatus.txtExcelPath.Text);
                mySheet = myWorkbook.Worksheet(1);



                //xlWorkbook = xlApp.Workbooks.Open(Program.formPayrollStatus.txtExcelPath.Text, IgnoreReadOnlyRecommended: true, ReadOnly: false, Notify: true);
                //xlWorkbook.CheckCompatibility = false;
                //xlWorkbook.DoNotPromptForConvert = true;

                //Excel.Worksheet xlWorksheet = xlWorkbook.Sheets.Cast<Excel.Worksheet>().Where(s => s.Name == Program.formPayrollStatus.cbxSheets.SelectedItem.ToString()).FirstOrDefault();

                //xlUsedRange = xlWorksheet.UsedRange;

                #region "Progress Ayarlamaları"


                if (!CheckAndFillDto()) // zeyl'lerdeki (baslangıc zeyl dahil. status kontrolü... ardından fill list<policydto>....)
                {
                    MessageBox.Show("Hatalar excel dosyanıza kaydedildi.", null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }

                progressBar1.Maximum = ClaimDtos.Count; //1 satır başlık var
                int wDiff = lblTotalPolicy.Width;
                lblTotalPolicy.Text = String.Format(" / {0}", progressBar1.Maximum);
                wDiff = lblTotalPolicy.Width - wDiff;
                lblTotalPolicy.Left += wDiff;
                lblProgress.Height = lblTotalPolicy.Height;
                lblProgress.Width += wDiff;

                #endregion

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        } // +

        private void setClaimStatus(object sender, DoWorkEventArgs e)
        {
            int rowCnt = 1;
            List<string> cols = new List<string>();
            int rowCount = ClaimDtos.Count();

            ClaimDtos = ClaimDtos.Where(l => l.ClaimId > 0).ToList();

            string claimIdList = string.Join(",", ClaimDtos.Where(l => l.ClaimId > 0).Select(x => x.ClaimId));
            List<Claim> claimList = new GenericRepository<Claim>().FindBy($"ID IN ({claimIdList})");

            string payrollIdList = string.Join(",", claimList.Where(x => x.PayrollId != null).Select(x => x.PayrollId));
            var payrollList = new GenericRepository<Payroll>().FindBy($"ID IN ({payrollIdList})", fetchDeletedRows: true);

            string correlation_id = Guid.NewGuid().ToString() + " - " + Guid.NewGuid().ToString();
            List<ClaimStatuChange> claimStatuChangeList = new List<ClaimStatuChange>();
            Dictionary<long, string> keyValuePairs = new Dictionary<long, string>();
            foreach (ClaimDto item in ClaimDtos.Where(l => l.ClaimId > 0))
            {
                long id = 0;
                Claim claim = claimList.Where(x => x.Id == item.ClaimId).FirstOrDefault();
                if (claim != null)
                {
                    id = claim.Id;
                }
                else
                    id = item.ClaimId;
                var status = item.ClaimStatus;

                ClaimStatuChange claimStatuChange = new ClaimStatuChange
                {
                    CORELATION_ID = correlation_id,
                    CLAIM_ID = id,
                    CLAIM_AMOUNT = item.Paid,
                    CLAIM_STATUS = status,
                };
                if (status == ((int)ClaimStatus.ODENDI).ToString())
                {
                    claimStatuChange.PAYMENT_DATE = item.PaymentDate;
                }
                claimStatuChangeList.Add(claimStatuChange);

            }
            List<SpResponse> spResponse = new GenericRepository<ClaimStatuChange>().InsertSpExecute3(claimStatuChangeList, CurrentToken: "9999999999");

            List<ClaimStatuChange> ClaimStatuChangeResultList = new GenericRepository<ClaimStatuChange>().FindBy($"CORELATION_ID='{correlation_id}'", orderby: "id asc");
            foreach (var item in ClaimStatuChangeResultList)
            {
                var result = new AjaxResultDto<ClaimStatusResult>();
                try
                {
                    ClaimStatuChange claimStatuChange = item;
                    while (claimStatuChange.RESULT == 2)
                    {
                        claimStatuChange = new GenericRepository<ClaimStatuChange>().FindById(claimStatuChange.ID);
                        Thread.Sleep(100);
                    }

                    if (claimStatuChange.RESULT == 0)
                    {
                        if (claimStatuChange.RESULT_DESC == "Ödeme kaydı atılmamış!")
                        {
                            V_Claim v_Claim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID={item.CLAIM_ID}", orderby: "", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                            if (v_Claim != null)
                            {
                                long bankId = 0;
                                if (v_Claim.PAYMENT_METHOD == ((int)ClaimPaymentMethod.KURUM).ToString())
                                {
                                    var bankAccount = new GenericRepository<V_ProviderBankAccount>().FindBy($"PROVIDER_ID={v_Claim.PROVIDER_ID}", orderby: "").FirstOrDefault();
                                    bankId = bankAccount != null ? (long)bankAccount.BANK_ACCOUNT_ID : 0;
                                }
                                else
                                {
                                    var insuredAccount = new GenericRepository<V_InsuredBankAccount>().FindBy($"INSURED_ID={v_Claim.INSURED_ID}", orderby: "").FirstOrDefault();
                                    bankId = insuredAccount != null ? (long)insuredAccount.BANK_ACCOUNT_ID : 0;
                                }

                                ClaimPayment claimPayment = new ClaimPayment
                                {
                                    Amount = v_Claim.PAID,
                                    ClaimId = v_Claim.CLAIM_ID,
                                    PaymentType = ((int)PaymentType.BANKAYA_ODEME).ToString(),
                                    DueDate = v_Claim.PAYROLL_DUE_DATE,
                                    BankAccountId = bankId > 0 ? (long?)bankId : null
                                };
                                var spResponseClaimPayment = new GenericRepository<ClaimPayment>().Insert(claimPayment, CurrentToken: "9999999999");

                            }

                            ClaimStatuChange claimStatuChangen = new ClaimStatuChange
                            {
                                CORELATION_ID = correlation_id,
                                CLAIM_ID = item.CLAIM_ID,
                                CLAIM_AMOUNT = item.CLAIM_AMOUNT,
                                CLAIM_STATUS = item.CLAIM_STATUS,
                                PAYMENT_DATE = item.PAYMENT_DATE
                            };
                            var spResponseClaimStatuChange = new GenericRepository<ClaimStatuChange>().Insert(claimStatuChangen, CurrentToken: "9999999999");
                            if (spResponseClaimStatuChange.Code != "100")
                            {
                                throw new Exception("İşlem Başlatılamadı. Tekrar Deneyiniz...");
                            }

                            claimStatuChange = new GenericRepository<ClaimStatuChange>().FindById(spResponseClaimStatuChange.PkId);
                            if (claimStatuChange.RESULT == 0)
                            {
                                throw new Exception(claimStatuChange.RESULT_DESC);
                            }
                            else
                            {
                                number++;
                                WriteToExcel((int)number, 5, "Başarılı");
                            }
                        }
                        else
                            throw new Exception(claimStatuChange.RESULT_DESC);
                    }
                    else
                    {
                        number++;
                        WriteToExcel((int)number, 5, "Başarılı");
                    }

                }
                catch (Exception ex)
                {
                    errList.Add((long)item.CLAIM_ID, ex.Message);
                    number++;
                    WriteToExcel((int)number, 5, "Hata");
                    WriteToExcel((int)number, 6, ex.Message);
                }
                double s = Convert.ToDouble(((double.Parse(rowCnt.ToString()) / rowCount) * 100));
                s = Math.Round(s, 2);
                Thread.Sleep(20);
                rowCnt++;

            }

            List<V_Claim> VclaimList = new GenericRepository<V_Claim>().FindBy($"PAYROLL_ID IN ({payrollIdList})", orderby: "CLAIM_ID", hint: "/*+ USE_HASH(claim_id) ORDERED */");
            foreach (var item in payrollList)
            {
                var claimLst = VclaimList.Where(x => x.PAYROLL_ID == item.Id).ToList();
                if (claimLst != null && claimLst.Count > 0)
                {
                    bool isFetchPayment = true;
                    foreach (var claim in claimLst)
                    {
                        if (claim.STATUS != ((int)ClaimStatus.ODENDI).ToString())
                        {
                            isFetchPayment = false;
                            break;
                        }
                    }
                    if (isFetchPayment)
                    {
                        item.PaymentDate = claimLst[0].PAYMENT_DATE;
                        item.Status = ((int)PayrollStatus.Odendi).ToString();
                        new GenericRepository<Payroll>().Insert(item);
                    }
                }
            }
        } // +

        internal void WriteToExcel(int row, int col, string value)
        {
            if (col < 0) return;

            lock (_syncExcel)
            {
                try
                {
                    mySheet.Cell(row, col).Value = value;
                }
                catch { }
            }
        } // +

        private bool CheckAndFillDto()
        {
            bool result = true;
            int errorRow = 0;
            Dictionary<string, int> pMap = null;

            if (policyTypeMap == null)
            {
                policyTypeMap = new Dictionary<string, int>();
                pMap = policyTypeMap;
            }
            if (pMap != null)
            {
                foreach (DataGridViewRow row in formClaimStatus.dataGvCols.Rows.Cast<DataGridViewRow>().Where(r => r.Tag != (object)"1"))
                {
                    pMap.Add(row.Cells[0].Value.ToString(), (int)row.Cells[1].Value);
                }
            }
            if (sonucTypeMap == null)
            {
                sonucTypeMap = new Dictionary<string, int>();
                foreach (DataGridViewRow row in formClaimStatus.dataGvCols.Rows.Cast<DataGridViewRow>().Where(r => r.Tag == (object)"1"))
                {
                    sonucTypeMap.Add(row.Cells[0].Value.ToString(), (int)row.Cells[1].Value);
                }
            }

            try
            {
                int fStart = Math.Max(progressBar1.Value, 2);
                int fFinish = progressBar1.Maximum + 1;

                for (int i = fStart; i <= fFinish; i++)
                {
                    errorRow = i;
                    ClaimDto _claimroll = new ClaimDto();
                    lock (_syncExcel)
                    {
                        foreach (PropertyInfo prop in _claimroll.GetType().GetProperties())
                        {
                            if (!prop.GetCustomAttributes(typeof(ColumnAttribute), false).Any())
                                continue;
                            String columnName = prop.Name;
                            columnName = (prop.GetCustomAttribute(typeof(ColumnAttribute), false) as ColumnAttribute).Name;
                            if (policyTypeMap.ContainsKey(columnName) == false || policyTypeMap[columnName] < 0) continue;
                            try
                            {
                                Type propertyType = prop.PropertyType;

                                Type targetType = IsNullableType(propertyType) ? Nullable.GetUnderlyingType(propertyType) : propertyType;
                                //prop null check

                                object tmpvalue = mySheet.Cell((int)i, policyTypeMap[columnName]).Value; /* ).Cells[policyTypeMap[columnName]].Value;*/
                                if (tmpvalue == null) continue;
                                if (string.IsNullOrEmpty(tmpvalue.ToString())) continue;
                                object convertedObj = Convert.ChangeType(tmpvalue != null ? tmpvalue.ToString() : null, targetType);
                                //if (targetType.Name == "String" && tmpvalue == null)
                                //    tmpvalue = "";
                                prop.SetValue(_claimroll, convertedObj, null);

                                if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel((int)i, sonucTypeMap["IsSuccess"], false.ToString());

                                if (sonucTypeMap.ContainsKey("Description")) WriteToExcel((int)i, sonucTypeMap["Description"], "hata");
                            }
                            catch (Exception ex)
                            {
                                if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel((int)i, sonucTypeMap["IsSuccess"], false.ToString());

                                if (sonucTypeMap.ContainsKey("Description")) WriteToExcel((int)i, sonucTypeMap["Description"], ex.ToString());
                                // Sonuçları hata olursa yazıyor
                                result = false;
                                continue;
                            }
                        }
                        ClaimDtos.Add(_claimroll);

                    }

                }
            }
            catch (Exception ex)
            {
                result = false;
                if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel((int)errorRow, sonucTypeMap["IsSuccess"], false.ToString());

                if (sonucTypeMap.ContainsKey("Description")) WriteToExcel((int)errorRow, sonucTypeMap["Description"], ex.ToString());
            }
            return result;
        }// +

        private void btnGo_Click(object sender, EventArgs e)
        {
            switch ((ActionType)btnGo.Tag)
            {
                case ActionType.Awaiting:
                case ActionType.Pausing:
                    RunProcess();
                    break;
                case ActionType.Running:
                    //PauseProcess();
                    break;
                case ActionType.Finish:
                    this.Close();
                    break;
            }
        } // +

        private void RunProcess()
        { //Veri havuzunu işleme al
            SetActionTag(ActionType.Running);
            backgroundWorker1.RunWorkerAsync();
        }
        private void PauseProcess()
        { //pause operasyonları
            btnGo.Enabled = false; //bekleme moduna geçerken işlem yapamasın
            btnGo.Text = "Duraklatılıyor";

            Cursor.Current = Cursors.WaitCursor;
            backgroundWorker1.CancelAsync();
        }


        private void FormProcessStatus_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((ActionType)btnGo.Tag == ActionType.Running)
            {
                if (MessageBox.Show("Devam eden bir işlem var çıkmak istiyor musunuz?", null, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.OK)
                {
                    e.Cancel = true;
                    return;
                }
                else
                {
                    PauseProcess();
                    //Pause işlemi gelecek
                }
            }
            if (myWorkbook != null)
            {
                try
                {
                    myWorkbook.Save();
                }
                catch
                {
                    MessageBox.Show("Kayıt sırasında hata oluştu. Açılacak olan Excel ekranından dosyayı saklamayı deneyin.", null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //goto endpoint;
                }
                //finally
                //{
                //    myWorkbook = null;
                //    System.Diagnostics.Process.GetCurrentProcess().Kill();
                //}
            }
            myWorkbook = null;
            System.Diagnostics.Process.GetCurrentProcess().Kill();
            //if (xlApp != null)
            //{
            //    xlApp.Quit();
            //    xlApp = null;
            //}
            //endpoint:
            //if (xlUsedRange != null) xlUsedRange = null;

            //Program.formPayrollStatus.Show();
        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            maxthread = 1;
            _pool = new SemaphoreSlim(maxthread);

            int fStart = 0;
            int fFinish = ClaimDtos.Count;
            //int fStart = Math.Max(progressBar1.Value, 2);
            //int fFinish = progressBar1.Maximum + 1;

            if (backgroundWorker1.CancellationPending)
            {
                while (_pool.CurrentCount < maxthread)
                { //Mevcut processlerin bitmesini bekle
                    Thread.Sleep(1000);
                }
                Cursor.Current = Cursors.Default;
                if ((ActionType)btnGo.Tag != ActionType.Finish) SetActionTag(ActionType.Pausing);
                return;
            }
            _pool.Wait();

            try
            { //burada asıl process çalıştırılacak
                BackgroundWorker bw = new BackgroundWorker();
                bw.DoWork += new DoWorkEventHandler(this.setClaimStatus);
                bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(ProcessRowComplete);
                bw.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                _pool.Release();
            }

        }
        private delegate void ProgressStepHandler();

        bool _IsSuccess = false;
        long _PolId = 0;
        long _EndorsId = 0;
        private void ProcessRowComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                Invoke(new ProgressStepHandler(ProgressStep));
                if (sender is BackgroundWorker) (sender as BackgroundWorker).Dispose();
            }
            finally
            {
                _pool.Release();
            }
        }
        private void ProgressStep()
        {
            progressBar1.PerformStep();
            lblProgress.Text = progressBar1.Value.ToString();
        }



        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((ActionType)btnGo.Tag == ActionType.Running && _pool != null)
            {
                Cursor.Current = Cursors.WaitCursor;

                System.Windows.Forms.Application.DoEvents();
                while (_pool.CurrentCount < maxthread)
                { //Mevcut processlerin bitmesini bekle
                    Thread.Sleep(1000);
                    System.Windows.Forms.Application.DoEvents();
                }
                Cursor.Current = Cursors.Default;
                if ((ActionType)btnGo.Tag == ActionType.Running)
                {
                    SetActionTag(ActionType.Finish);
                    try
                    {
                        myWorkbook.Save();
                        myWorkbook = null;

                        //xlWorkbook.Save();
                        //xlWorkbook.Close();
                        //xlWorkbook = null;
                        //xlApp.Quit();
                        //xlApp = null;
                        //xlUsedRange = null;

                        MessageBox.Show("İşlem tamamladı.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch
                    {
                        MessageBox.Show("Kayıt sırasında hata oluştu. Açılacak olan Excel ekranından dosyayı saklamayı deneyin.", null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //xlApp.ScreenUpdating = true;
                        //xlApp.Visible = true;
                    }
                }
            }
        }


        private void FormProcessStatus_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }




    }
}
