﻿using ClosedXML.Excel;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.ExtendedProperties;
using PolicyBatch.Match;
using Protein.Common.Attributes;
using Protein.Common.Dto.ImportObjects;
using Protein.Common.Enums;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolicyBatch
{
    public partial class FormPayrollStatus : Form
    {
        public string NotMatched = "(EŞLEŞMEDİ)";
        public FormPayrollStatus()
        {
            InitializeComponent();
        }

        private void btnExcelPath_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                dataGvCols.Rows.Clear();
                txtExcelPath.Text = openFileDialog1.FileName;

                dataGvCols.Rows.Clear();
                dataGvCols.Refresh();
                DataGridViewComboBoxColumn sColumn = (dataGvCols.Columns["columnExcel"] as DataGridViewComboBoxColumn);
                if (sColumn.IsDataBound) sColumn.DataSource = null;

                //Microsoft.Office.Interop.Excel.Application xlApp = null;
                //Microsoft.Office.Interop.Excel.Workbook xlWorkbook = null;
                try
                {
                    using (XLWorkbook workBook = new XLWorkbook(openFileDialog1.FileName))
                    {
                        IXLWorksheet mySheet = workBook.Worksheet(1);

                        //    xlApp = new Microsoft.Office.Interop.Excel.Application();
                        //    xlWorkbook = xlApp.Workbooks.Open(txtExcelPath.Text, ReadOnly: true, Notify: false);

                        //Microsoft.Office.Interop.Excel.Worksheet mySheet = xlWorkbook.Sheets.Cast<Microsoft.Office.Interop.Excel.Worksheet>().Where(s => s.Name == cbxSheets.SelectedItem.ToString()).FirstOrDefault();

                        if (mySheet != null)
                        {
                            List<KeyValuePair<int, string>> lstColumn = new List<KeyValuePair<int, string>>();


                            bool firstRow = true;
                            foreach (IXLRow row in mySheet.Rows())
                            {

                                if (firstRow)
                                {
                                    int colCnt = 1;

                                    foreach (IXLCell cell in row.Cells())
                                    {
                                        lstColumn.Add(new KeyValuePair<int, string>(colCnt, cell.Value.ToString()));
                                        colCnt++;
                                    }
                                    break;
                                }
                            }
                            lstColumn = lstColumn.OrderBy(t => t.Value).ToList();
                            lstColumn.Insert(0, new KeyValuePair<int, string>(-1, NotMatched));

                            sColumn.ValueMember = "Key";
                            sColumn.DisplayMember = "Value";
                            sColumn.DataSource = new BindingSource(lstColumn, null);
                            lstColumn = null;

                            foreach (DataGridViewRow row in dataGvCols.Rows)
                            {
                                row.Cells[1].Value = -1;
                                //row.Cells[0].Style.ForeColor = System.Drawing.Color.Red;
                            }
                            dataGvCols.Refresh();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    //if (xlWorkbook != null) xlWorkbook.Close();
                    //if (xlApp != null) xlApp.Quit();

                    //xlWorkbook = null;
                    //xlApp = null;
                }
                Type outParam = null;
                string[] paramNames = null;
                List<string> lstParams = new List<string>();
                foreach (PropertyInfo prop in typeof(PayrollDto).GetProperties())
                {
                    if (!prop.GetCustomAttributes(typeof(ColumnAttribute), false).Any())
                        continue;

                    String columnName = prop.Name;

                    columnName = (prop.GetCustomAttribute(typeof(ColumnAttribute), false) as ColumnAttribute).Name;
                    lstParams.Add(columnName);
                    //dataGvCols.Rows.Add();
                    //DataGridViewRow row = dataGvCols.Rows[dataGvCols.Rows.GetLastRow(DataGridViewElementStates.None)];
                    //row.Cells[0].Value = columnName;
                    //row.Cells[0].Tag = prop.Name;
                }
                paramNames = lstParams.ToArray();
                outParam = typeof(Output);

                foreach (string value in paramNames)
                {
                    int inx = dataGvCols.Rows.Add(value, String.Empty);
                    DataGridViewRow row = dataGvCols.Rows[inx];
                    row.Cells[1].Value = -1;
                }
                if (outParam == null) return;
                foreach (string value in outParam.GetProperties().Select(p => p.Name).ToArray())
                {
                    int inx = dataGvCols.Rows.Add(value, String.Empty);
                    DataGridViewRow row = dataGvCols.Rows[inx];
                    row.Tag = "1";
                    row.Cells[0].Style.ForeColor = System.Drawing.Color.Blue;
                    row.Cells[1].Value = -1;
                }

                for (int i = 0; i < dataGvCols.Rows.Count; i++)
                {
                    DataGridViewComboBoxCell myCell = (DataGridViewComboBoxCell)dataGvCols.Rows[i].Cells[1];

                    try
                    {
                        bool matched = false;
                        foreach (var cellItem in myCell.Items)
                        {
                            var item = (KeyValuePair<Int32, string>)cellItem;

                            if (item.Value.ToString() == dataGvCols.Rows[i].Cells[0].Value.ToString())
                            {
                                myCell.Value = item.Key;
                                //dataGvCols.Rows[i].Cells[1].Value = dataGvCols.Rows[i].Cells[0].Value;
                                matched = true;

                            }
                        }
                        if (!matched)
                        {
                            myCell.Value = -1;
                            dataGvCols.Rows[i].Cells[0].Style.BackColor = System.Drawing.Color.Red;
                            dataGvCols.Rows[i].Cells[0].Style.ForeColor = System.Drawing.Color.White;
                            //myCell.Style = System.Drawing.Color.Red;
                        }
                    }
                    catch
                    {
                        myCell.Value = -1;
                    }
                }
            }
        }

        private void btnShowList_Click(object sender, EventArgs e)
        {

            int requiredParamCount = dataGvCols.Rows.Cast<DataGridViewRow>().Where(r => r.Tag != (object)"1").Count();
            int matchedParamCount = dataGvCols.Rows.Cast<DataGridViewRow>().Where(r => r.Tag != (object)"1" && r.Cells[1].Value != DBNull.Value && (int)r.Cells[1].Value > -1).Count();

            

            FormZarfProcess frmProcess = new FormZarfProcess(this);

            frmProcess.Show();
            this.Hide();

        }
    }
}
