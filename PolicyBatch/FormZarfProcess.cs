﻿using ClosedXML.Excel;
using Protein.Common.Dto;
using Protein.Common.Dto.ImportObjects;
using Protein.Common.Enums;
using Protein.Common.Extensions;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace PolicyBatch
{
    public partial class FormZarfProcess : Form
    {
        private static bool IsNullableType(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
        }
        private static CultureInfo trCulture = new CultureInfo("tr-TR");
        private string OutputFileDir = null;
        private List<PayrollDto> PayrollDtos = new List<PayrollDto>();
        private List<ClaimDto> ClaimDtos = new List<ClaimDto>();

        private static IXLWorksheet mySheet = null;
        private static IXLRange myRange = null;
        private static XLWorkbook myWorkbook = null;

        private List<long> InstertedInstalmentIds = new List<long>();
        private static Dictionary<string, int> sonucTypeMap = null;
        private static SemaphoreSlim _pool;
        private static int maxthread = 0;
        private static Dictionary<string, int> policyTypeMap = null;
        private FormPayrollStatus formPayrollStatus = new FormPayrollStatus();

        private delegate void ProgressStepHandler();
        private static object _syncExcel = new object();

        bool _IsSuccess = false;
        long _PolId = 0;
        long _EndorsId = 0;
        private enum ActionType
        {
            Awaiting = 0,
            Running = 1,
            Pausing = 2,
            Finish = 3
        }
        public FormZarfProcess(FormPayrollStatus formPayrollStatus)
        {
            InitializeComponent();
            this.formPayrollStatus = formPayrollStatus;
        }
        private void SetActionTag(ActionType tag)
        {
            btnGo.Tag = tag;
            string label = btnGo.Text;
            switch (tag)
            {
                case ActionType.Awaiting:
                    label = "Başlat";
                    break;
                case ActionType.Pausing:
                    label = "Sürdür";
                    break;
                case ActionType.Running:
                    label = "Duraklat";
                    break;
                case ActionType.Finish:
                    label = "Çıkış";
                    break;
            }
            if (btnGo.InvokeRequired)
            {
                btnGo.Invoke(new Action(() =>
                {
                    btnGo.Text = label;
                    btnGo.Enabled = true;
                }));
            }
            else
            {
                btnGo.Text = label;
                btnGo.Enabled = true;
            }
        }
        private void btnGo_Click(object sender, EventArgs e)
        {
            switch ((ActionType)btnGo.Tag)
            {
                case ActionType.Awaiting:
                case ActionType.Pausing:
                    RunProcess();
                    break;
                case ActionType.Running:
                    //PauseProcess();
                    break;
                case ActionType.Finish:
                    this.Close();
                    break;
            }
        }
        private void RunProcess()
        { //Veri havuzunu işleme al
            SetActionTag(ActionType.Running);
            backgroundWorker1.RunWorkerAsync();
        }
        private void PauseProcess()
        { //pause operasyonları
            btnGo.Enabled = false; //bekleme moduna geçerken işlem yapamasın
            btnGo.Text = "Duraklatılıyor";

            Cursor.Current = Cursors.WaitCursor;
            backgroundWorker1.CancelAsync();
        }
        private void FormProcessStatus_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((ActionType)btnGo.Tag == ActionType.Running)
            {
                if (MessageBox.Show("Devam eden bir işlem var çıkmak istiyor musunuz?", null, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.OK)
                {
                    e.Cancel = true;
                    return;
                }
                else
                {
                    PauseProcess();
                    //Pause işlemi gelecek
                }
            }
            if (myWorkbook != null)
            {
                try
                {
                    myWorkbook.Save();
                }
                catch
                {
                    MessageBox.Show("Kayıt sırasında hata oluştu. Açılacak olan Excel ekranından dosyayı saklamayı deneyin.", null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            myWorkbook = null;
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            maxthread = 1;
            _pool = new SemaphoreSlim(maxthread);

            int fStart = 0;
            int fFinish = PayrollDtos.Count;
            //int fStart = Math.Max(progressBar1.Value, 2);
            //int fFinish = progressBar1.Maximum + 1;

            if (backgroundWorker1.CancellationPending)
            {
                while (_pool.CurrentCount < maxthread)
                { //Mevcut processlerin bitmesini bekle
                    Thread.Sleep(1000);
                }
                Cursor.Current = Cursors.Default;
                if ((ActionType)btnGo.Tag != ActionType.Finish) SetActionTag(ActionType.Pausing);
                return;
            }
            _pool.Wait();

            try
            { //burada asıl process çalıştırılacak
                BackgroundWorker bw = new BackgroundWorker();
                bw.DoWork += new DoWorkEventHandler(this.SetPayrollStatus);
                bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(ProcessRowComplete);
                bw.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                _pool.Release();
            }

        }
        private void SetPayrollStatus(object sender, DoWorkEventArgs e)
        {
            int rowCnt = 1;
            List<string> cols = new List<string>();
            int rowCount = PayrollDtos.Count;

            var payrollList = PayrollDtos.Where(l => l.PayrollId > 0).ToList();

            if (payrollList != null && payrollList.Count > 0)
            {
                string idList = string.Join(",", payrollList.Select(a => a.PayrollId));

                List<Payroll> payrollLst = new GenericRepository<Payroll>().FindBy($"ID IN ({idList}) OR OLD_PAYROLL_ID IN ({idList})", fetchDeletedRows: true);

                if (payrollLst != null && payrollLst.Count > 0)
                {
                    idList = String.Join(",", payrollLst.Select(p => p.Id));

                    Dictionary<long, string> errList = new Dictionary<long, string>();
                    int number = 1;
                    string correlation_id = Guid.NewGuid().ToString() + " - " + Guid.NewGuid().ToString();
                    List<ClaimStatuChange> claimStatuChangeList = new List<ClaimStatuChange>();
                    foreach (PayrollDto item in PayrollDtos.Where(l => l.PayrollId > 0))
                    {
                        long id = 0;
                        Payroll payroll = payrollLst.Where(p => p.Id == item.PayrollId || p.OldPayrollId == item.PayrollId).FirstOrDefault();
                        if (payroll == null)
                        {
                            id = item.PayrollId;
                        }
                        else
                            id = payroll.Id;
                        var changeStatus = item.PayrollStatus;

                        ClaimStatuChange claimStatuChange = new ClaimStatuChange
                        {
                            CORELATION_ID = correlation_id,
                            PAYROLL_ID = id,
                            PAYROLL_AMOUNT = item.TotalPaid,
                            PAYROLL_STATUS = changeStatus,
                        };
                        if (changeStatus == ((int)PayrollStatus.Odendi).ToString())
                        {
                            claimStatuChange.PAYMENT_DATE = item.Date;
                        }
                        claimStatuChangeList.Add(claimStatuChange);

                    }
                    List<SpResponse> spResponse = new GenericRepository<ClaimStatuChange>().InsertSpExecute3(claimStatuChangeList, CurrentToken: "9999999999");

                    List<ClaimStatuChange> ClaimStatuChangeResultList = new GenericRepository<ClaimStatuChange>().FindBy($"CORELATION_ID='{correlation_id}'", orderby: "id asc");
                    foreach (var item in ClaimStatuChangeResultList)
                    {
                        try
                        {
                            ClaimStatuChange claimStatuChange = item;
                            while (claimStatuChange.RESULT == 2)
                            {
                                claimStatuChange = new GenericRepository<ClaimStatuChange>().FindById(claimStatuChange.ID);
                                Thread.Sleep(100);
                            }

                            if (claimStatuChange.RESULT == 0)
                            {
                                if (claimStatuChange.RESULT_DESC.IndexOf(" nolu kaydın ödeme bilgisi yok!") > 0)
                                {
                                    var a = claimStatuChange.RESULT_DESC.Replace(" nolu kaydın ödeme bilgisi yok!", ",");
                                    var b = a.Replace("\n", " ");
                                    var c = b.Trim().Substring(0, b.Length - 2);
                                    List<ClaimPayment> claimPaymentList = new List<ClaimPayment>();
                                    foreach (var claimId in c.Split(','))
                                    {
                                        V_Claim v_Claim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID={claimId}", orderby: "", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                                        if (v_Claim != null)
                                        {
                                            long bankId = 0;
                                            if (v_Claim.PAYMENT_METHOD == ((int)ClaimPaymentMethod.KURUM).ToString())
                                            {
                                                var bankAccount = new GenericRepository<V_ProviderBankAccount>().FindBy($"PROVIDER_ID={v_Claim.PROVIDER_ID}", orderby: "").FirstOrDefault();
                                                bankId = bankAccount != null ? (long)bankAccount.BANK_ACCOUNT_ID : 0;
                                            }
                                            else
                                            {
                                                var insuredAccount = new GenericRepository<V_InsuredBankAccount>().FindBy($"INSURED_ID={v_Claim.INSURED_ID}", orderby: "").FirstOrDefault();
                                                bankId = insuredAccount != null ? (long)insuredAccount.BANK_ACCOUNT_ID : 0;
                                            }

                                            ClaimPayment claimPayment = new ClaimPayment
                                            {
                                                Amount = v_Claim.PAID,
                                                ClaimId = v_Claim.CLAIM_ID,
                                                PaymentType = ((int)PaymentType.BANKAYA_ODEME).ToString(),
                                                DueDate = v_Claim.PAYROLL_DUE_DATE,
                                                BankAccountId = bankId > 0 ? (long?)bankId : null
                                            };
                                            claimPaymentList.Add(claimPayment);
                                        }
                                    }
                                    var spResponseClaimPayment = new GenericRepository<ClaimPayment>().InsertSpExecute3(claimPaymentList, CurrentToken: "9999999999");

                                    ClaimStatuChange claimStatuChangen = new ClaimStatuChange
                                    {
                                        CORELATION_ID = correlation_id,
                                        PAYROLL_ID = item.PAYROLL_ID,
                                        PAYROLL_AMOUNT = item.PAYROLL_AMOUNT,
                                        PAYROLL_STATUS = item.PAYROLL_STATUS,
                                        PAYMENT_DATE = item.PAYMENT_DATE
                                    };
                                    var spResponseClaimStatuChange = new GenericRepository<ClaimStatuChange>().Insert(claimStatuChangen,CurrentToken: "9999999999");
                                    if (spResponseClaimStatuChange.Code != "100")
                                    {
                                        throw new Exception("İşlem Başlatılamadı. Tekrar Deneyiniz...");
                                    }

                                    claimStatuChange = new GenericRepository<ClaimStatuChange>().FindById(spResponseClaimStatuChange.PkId);
                                    if (claimStatuChange.RESULT == 0)
                                    {
                                        throw new Exception(claimStatuChange.RESULT_DESC);
                                    }
                                    else
                                    {
                                        number++;
                                        WriteToExcel((int)number, 5, "Başarılı");
                                    }
                                }
                                else
                                    throw new Exception(claimStatuChange.RESULT_DESC);
                            }
                            else
                            {
                                number++;
                                WriteToExcel((int)number, 5, "Başarılı");
                            }
                        }
                        catch (Exception ex)
                        {
                            errList.Add((long)item.PAYROLL_ID, ex.Message);
                            number++;
                            WriteToExcel((int)number, 5, "Hata");
                            WriteToExcel((int)number, 6, ex.Message);
                        }

                        double s = Convert.ToDouble(((double.Parse(rowCnt.ToString()) / rowCount) * 100));
                        s = Math.Round(s, 2);
                        Thread.Sleep(20);
                        rowCnt++;
                    }

                }

                //List<ClaimPayment> claimPaymentList = new GenericRepository<ClaimPayment>().FindBy($"CLAIM_ID IN ({claimIdList})");

                //List<ClaimBill> allClaimBillList = new GenericRepository<ClaimBill>().FindBy($"CLAIM_ID IN ({claimIdList})");

                //foreach (PayrollDto item in PayrollDtos.Where(l => l.PayrollId > 0))
                //{
                //    try
                //    {
                //        Payroll payroll = payrollLst.Where(p => p.Id == item.PayrollId || p.OldPayrollId == item.PayrollId).FirstOrDefault();
                //        if (payroll == null)
                //        {
                //            throw new Exception("Zarf No Hatalı - Zarf Bilgisi Bulunamadı!");
                //        }
                //        var changeStatus = item.PayrollStatus;
                //        if (payroll.Status == changeStatus)
                //        {
                //            throw new Exception($"Zarf Durumu Daha Önceden {LookupHelper.GetLookupTextByOrdinal(LookupTypes.PayrollStatus, payroll.Status)} Olarak Değiştirilmiş. Tekrar Aynı İşlem Yapılamaz... Lütfen Kontrol Ediniz...");
                //        }

                //        V_Payroll vPayroll = vPayrollList.Where(p => p.PAYROLL_ID == payroll.Id).FirstOrDefault();
                //        if (vPayroll.TOTAL_AMOUNT != item.TotalPaid)
                //        {
                //            throw new Exception("Sistemdeki Tutar ile Uyuşmazlık! Zarf içerisindeki Hasarları Kontrol Ediniz...");
                //        }

                //        if (changeStatus == ((int)PayrollStatus.Odendi).ToString())
                //        {
                //            if (payroll.Status != Convert.ToString((int)PayrollStatus.Onay))
                //            {
                //                throw new Exception("Zarf Durumu Onay olmadan zarf durumunu ÖDENDİ'ye çekemezsiniz. Lütfen Zarf Durumunu Güncelleyiniz...");
                //            }

                //            List<Claim> claimList = allClaimList.Where(x => x.PayrollId == payroll.Id).ToList(); //new GenericRepository<Claim>().FindBy("PAYROLL_ID=:payrollId", parameters: new { payrollId = payroll.Id });
                //            if (claimList == null)
                //            {
                //                throw new Exception("Hasarlar Bulunurken Hata Oluştu!");
                //            }
                //            if (claimList.Count < 1)
                //            {
                //                throw new Exception("Zarf İçerisinde Hasar Kaydı Bulunamadı.");
                //            }
                //            var result = new AjaxResultDto<ClaimStatusResult>();

                //            foreach (var claim in claimList)
                //            {
                //                if (claim.Status != ((int)ClaimStatus.ODENECEK).ToString() && claim.Status != ((int)ClaimStatus.ODENDI).ToString())
                //                {
                //                    result.ResultMessage += ($" {claim.Id} No'lu Hasar Durumu Uygun Değil!");
                //                }

                //                ClaimPayment claimPayment = claimPaymentList.Where(c => c.ClaimId == claim.Id).ToList().FirstOrDefault(); //= new GenericRepository<ClaimPayment>().FindBy($"CLAIM_ID ={claim.Id}").FirstOrDefault();
                //                if (claimPayment == null)
                //                {
                //                    V_Claim v_Claim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID={claim.Id}", orderby: "", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                //                    if (v_Claim != null)
                //                    {
                //                        long bankId = 0;
                //                        if (v_Claim.PAYMENT_METHOD == ((int)ClaimPaymentMethod.KURUM).ToString())
                //                        {
                //                            var bankAccount = new GenericRepository<V_ProviderBankAccount>().FindBy($"PROVIDER_ID={v_Claim.PROVIDER_ID}", orderby: "").FirstOrDefault();
                //                            bankId = bankAccount != null ? (long)bankAccount.BANK_ACCOUNT_ID : 0;
                //                        }
                //                        else
                //                        {
                //                            var insuredAccount = new GenericRepository<V_InsuredBankAccount>().FindBy($"INSURED_ID={v_Claim.INSURED_ID}", orderby: "").FirstOrDefault();
                //                            bankId = insuredAccount != null ? (long)insuredAccount.BANK_ACCOUNT_ID : 0;
                //                        }

                //                        claimPayment = new ClaimPayment
                //                        {
                //                            Amount = v_Claim.PAID,
                //                            ClaimId = claim.Id,
                //                            PaymentType = ((int)PaymentType.BANKAYA_ODEME).ToString(),
                //                            DueDate = v_Claim.PAYROLL_DUE_DATE,
                //                            BankAccountId = bankId > 0 ? (long?)bankId : null
                //                        };
                //                        var spRes = new GenericRepository<ClaimPayment>().Insert(claimPayment);
                //                        claimPayment.Id = spRes.PkId;
                //                        claimPaymentList.Add(claimPayment);
                //                    }

                //                    // throw new Exception("Hasar Ödeme Bilgisi Bulunamadı!");
                //                }

                //                var claimBillList = allClaimBillList.Where(x => x.ClaimId == claim.Id).ToList();// new GenericRepository<ClaimBill>().FindBy("CLAIM_ID =:claimId", parameters: new { claimId = claim.Id }, orderby: "CLAIM_ID");
                //                if (claimBillList == null || claimBillList.Count <= 0)
                //                {
                //                    result.ResultMessage += ($" {claim.Id} No'lu Hasara Ait Fatura Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                //                }
                //                else
                //                {
                //                    foreach (var claimBill in claimBillList)
                //                    {
                //                        if (claimBill.Status != Convert.ToString((int)BillStatus.ODENDI))
                //                        {
                //                            claimBill.Status = ((int)BillStatus.ODENDI).ToString();
                //                            new GenericRepository<ClaimBill>().Insert(claimBill);
                //                        }
                //                    }
                //                }
                //            }
                //            if (!result.ResultMessage.IsNull())
                //            {
                //                throw new Exception(result.ResultMessage);
                //            }
                //            foreach (var claim in claimList)
                //            {
                //                if (claim.Status != ((int)ClaimStatus.ODENDI).ToString())
                //                {
                //                    claim.Status = ((int)ClaimStatus.ODENDI).ToString();
                //                    new GenericRepository<Claim>().Insert(claim);
                //                }

                //                var claimPayment = claimPaymentList.Where(c => c.ClaimId == claim.Id).ToList().FirstOrDefault(); // new GenericRepository<ClaimPayment>().FindBy($"CLAIM_ID ={claim.Id}").FirstOrDefault();
                //                claimPayment.PaymentDate = item.Date;
                //                new GenericRepository<ClaimPayment>().Insert(claimPayment);
                //            }

                //            payroll.PaymentDate = item.Date;
                //        }
                //        else if (changeStatus == ((int)PayrollStatus.Onay_Bekler).ToString())
                //        {
                //            if (payroll.Status == ((int)PayrollStatus.Odendi).ToString())
                //            {
                //                var claimList = allClaimList.Where(x => x.PayrollId == payroll.Id).ToList(); // new GenericRepository<Claim>().FindBy($" PAYROLL_ID=:payrollId", parameters: new { payrollId = payroll.Id }, fetchDeletedRows: true);
                //                foreach (var claim in claimList)
                //                {
                //                    var claimPayment = claimPaymentList.Where(x => x.ClaimId == claim.Id).ToList().FirstOrDefault();// new GenericRepository<ClaimPayment>().FindBy($"CLAIM_ID ={claim.Id}").FirstOrDefault();
                //                    if (claimPayment != null)
                //                    {
                //                        claimPayment.PaymentDate = null;
                //                        new GenericRepository<ClaimPayment>().UpdateForAll(claimPayment);
                //                    }
                //                }
                //            }
                //            //TO_DO: Yetki Kontrolü
                //            //throw new Exception("Yetkili Kullanıcılar Dışında Zarfın Durumunu Onay Bekler'e Çekilemez. Lütfen Yetkilinize Danışın...");
                //        }
                //        else if (changeStatus == ((int)PayrollStatus.Onay).ToString())
                //        {
                //            var claimList = new List<Claim>();
                //            if (payroll.CompanyId == 10 || payroll.CompanyId == 17)
                //            {
                //                claimList = allClaimList.Where(x => x.PayrollId == payroll.Id && x.Status != ((int)ClaimStatus.ODENECEK).ToString() && x.Status != ((int)ClaimStatus.ODENDI).ToString() && x.Status != ((int)ClaimStatus.RET).ToString()).ToList();// new GenericRepository<Claim>().FindBy($"STATUS NOT IN :statusList AND PAYROLL_ID=:payrollId",parameters: new { payrollId = payroll.Id, statusList = new[] { ((int)ClaimStatus.ODENECEK).ToString(), ((int)ClaimStatus.ODENDI).ToString(), ((int)ClaimStatus.RET).ToString() } }, fetchDeletedRows: true);
                //                if (claimList.Count > 0)
                //                {
                //                    //TO_DO: Güneş Ret Hasarların Klonlanması
                //                    throw new Exception("Zarfı Onaylamak için İçerisindeki Hasarların ODENECEK veya RET Durumunda Olması Gerekmektedir. Lütfen Hasar Durumlarını Kontrol Ediniz...");
                //                }
                //            }
                //            else
                //            {
                //                claimList = allClaimList.Where(x => x.PayrollId == payroll.Id && x.Status != ((int)ClaimStatus.ODENECEK).ToString() && x.Status != ((int)ClaimStatus.ODENDI).ToString() && x.Status != ((int)ClaimStatus.RET).ToString()).ToList();// new GenericRepository<Claim>().FindBy($"STATUS NOT IN :statusList AND PAYROLL_ID=:payrollId", parameters: new { payrollId = payroll.Id, statusList = new[] { ((int)ClaimStatus.ODENECEK).ToString(), ((int)ClaimStatus.ODENDI).ToString(), ((int)ClaimStatus.RET).ToString() } }, fetchDeletedRows: true);
                //                if (claimList.Count > 0)
                //                {
                //                    throw new Exception("Zarfı Onaylamak için İçerisindeki Hasarların ODENECEK veya RET Durumunda Olması Gerekmektedir. Lütfen Hasar Durumlarını Kontrol Ediniz...");
                //                }
                //            }

                //            if (payroll.Status == ((int)PayrollStatus.Odendi).ToString())
                //            {
                //                foreach (var claim in claimList)
                //                {
                //                    var claimPayment = claimPaymentList.Where(c => c.ClaimId == claim.Id).ToList().FirstOrDefault(); // new GenericRepository<ClaimPayment>().FindBy($"CLAIM_ID ={claim.Id}").FirstOrDefault();
                //                    claimPayment.PaymentDate = null;
                //                    new GenericRepository<ClaimPayment>().UpdateForAll(claimPayment);
                //                }
                //            }
                //        }
                //        else
                //        {
                //            throw new Exception("Belirtilen Status Değeri Bulunamadı");
                //        }

                //        payroll.Status = changeStatus;
                //        SpResponse<Payroll> spResponse = new GenericRepository<Payroll>().Insert(payroll);
                //        if (spResponse.Code != "100")
                //        {
                //            throw new Exception("Zarf Durumu Değiştirilemedi");
                //        }


                //        number++;
                //        WriteToExcel((int)number, 5, "Başarılı");

                //    }
                //    catch (Exception ex)
                //    {
                //        errList.Add(item.PayrollId, ex.Message);
                //        number++;
                //        WriteToExcel((int)number, 5, "Hata");
                //        WriteToExcel((int)number, 6, ex.Message);
                //    }

                //    double s = Convert.ToDouble(((double.Parse(rowCnt.ToString()) / rowCount) * 100));
                //    s = Math.Round(s, 2);
                //    Thread.Sleep(20);
                //    rowCnt++;
                //}
                //}
            }
        
        
    }
    internal void WriteToExcel(int row, int col, string value)
    {
        if (col < 0) return;

        lock (_syncExcel)
        {
            try
            {
                mySheet.Cell(row, col).Value = value;
            }
            catch { }
        }
    }
    private void ProcessRowComplete(object sender, RunWorkerCompletedEventArgs e)
    {
        try
        {
            Invoke(new ProgressStepHandler(ProgressStep));
            if (sender is BackgroundWorker) (sender as BackgroundWorker).Dispose();
        }
        finally
        {
            _pool.Release();
        }
    }
    private void ProgressStep()
    {
        progressBar1.PerformStep();
        lblProgress.Text = progressBar1.Value.ToString();
    }
    private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
        if ((ActionType)btnGo.Tag == ActionType.Running && _pool != null)
        {
            Cursor.Current = Cursors.WaitCursor;

            System.Windows.Forms.Application.DoEvents();
            while (_pool.CurrentCount < maxthread)
            { //Mevcut processlerin bitmesini bekle
                Thread.Sleep(1000);
                System.Windows.Forms.Application.DoEvents();
            }
            Cursor.Current = Cursors.Default;
            if ((ActionType)btnGo.Tag == ActionType.Running)
            {
                SetActionTag(ActionType.Finish);
                try
                {
                    myWorkbook.Save();
                    myWorkbook = null;

                    MessageBox.Show("İşlem tamamladı.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch
                {
                    MessageBox.Show("Kayıt sırasında hata oluştu. Açılacak olan Excel ekranından dosyayı saklamayı deneyin.", null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
    private void FormZarfProcess_Load(object sender, EventArgs e)
    {
        SetActionTag(ActionType.Awaiting);

        OutputFileDir = null;

        sonucTypeMap = null;
        policyTypeMap = null;

        try
        {
            myWorkbook = new XLWorkbook(formPayrollStatus.txtExcelPath.Text);
            mySheet = myWorkbook.Worksheet(1);

            #region "Progress Ayarlamaları"


            if (!CheckAndFillDto()) // zeyl'lerdeki (baslangıc zeyl dahil. status kontrolü... ardından fill list<policydto>....)
            {
                MessageBox.Show("Hatalar excel dosyanıza kaydedildi.", null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }

            progressBar1.Maximum = PayrollDtos.Count; //1 satır başlık var
            int wDiff = lblTotalPolicy.Width;
            lblTotalPolicy.Text = String.Format(" / {0}", progressBar1.Maximum);
            wDiff = lblTotalPolicy.Width - wDiff;
            lblTotalPolicy.Left += wDiff;
            lblProgress.Height = lblTotalPolicy.Height;
            lblProgress.Width += wDiff;

            #endregion

        }
        catch (Exception ex)
        {
            MessageBox.Show(ex.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
            this.Close();
        }
    }
    private void FormProcessStatus_FormClosed(object sender, FormClosedEventArgs e)
    {
        Environment.Exit(1);
    }
    private bool CheckAndFillDto()
    {
        bool result = true;
        int errorRow = 0;
        Dictionary<string, int> pMap = null;

        if (policyTypeMap == null)
        {
            policyTypeMap = new Dictionary<string, int>();
            pMap = policyTypeMap;
        }
        if (pMap != null)
        {
            foreach (DataGridViewRow row in formPayrollStatus.dataGvCols.Rows.Cast<DataGridViewRow>().Where(r => r.Tag != (object)"1"))
            {
                pMap.Add(row.Cells[0].Value.ToString(), (int)row.Cells[1].Value);
            }
        }
        if (sonucTypeMap == null)
        {
            sonucTypeMap = new Dictionary<string, int>();
            foreach (DataGridViewRow row in formPayrollStatus.dataGvCols.Rows.Cast<DataGridViewRow>().Where(r => r.Tag == (object)"1"))
            {
                sonucTypeMap.Add(row.Cells[0].Value.ToString(), (int)row.Cells[1].Value);
            }
        }

        try
        {
            int fStart = Math.Max(progressBar1.Value, 2);
            int fFinish = progressBar1.Maximum + 1;

            for (int i = fStart; i <= fFinish; i++)
            {
                errorRow = i;
                PayrollDto _payroll = new PayrollDto();
                lock (_syncExcel)
                {
                    foreach (PropertyInfo prop in _payroll.GetType().GetProperties())
                    {
                        if (!prop.GetCustomAttributes(typeof(ColumnAttribute), false).Any())
                            continue;
                        String columnName = prop.Name;
                        columnName = (prop.GetCustomAttribute(typeof(ColumnAttribute), false) as ColumnAttribute).Name;
                        if (policyTypeMap.ContainsKey(columnName) == false || policyTypeMap[columnName] < 0) continue;
                        try
                        {
                            Type propertyType = prop.PropertyType;

                            Type targetType = IsNullableType(propertyType) ? Nullable.GetUnderlyingType(propertyType) : propertyType;
                            //prop null check

                            object tmpvalue = mySheet.Cell((int)i, policyTypeMap[columnName]).Value;
                            if (tmpvalue == null) continue;
                            if (string.IsNullOrEmpty(tmpvalue.ToString())) continue;
                            object convertedObj = Convert.ChangeType(tmpvalue != null ? tmpvalue.ToString() : null, targetType);

                            prop.SetValue(_payroll, convertedObj, null);

                            if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel((int)i, sonucTypeMap["IsSuccess"], false.ToString());

                            if (sonucTypeMap.ContainsKey("Description")) WriteToExcel((int)i, sonucTypeMap["Description"], "hata");
                        }
                        catch (Exception ex)
                        {
                            if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel((int)i, sonucTypeMap["IsSuccess"], false.ToString());

                            if (sonucTypeMap.ContainsKey("Description")) WriteToExcel((int)i, sonucTypeMap["Description"], ex.ToString());
                            // Sonuçları hata olursa yazıyor
                            result = false;
                            continue;
                        }
                    }
                    PayrollDtos.Add(_payroll);

                }

            }
        }
        catch (Exception ex)
        {
            result = false;
            if (sonucTypeMap.ContainsKey("IsSuccess")) WriteToExcel((int)errorRow, sonucTypeMap["IsSuccess"], false.ToString());

            if (sonucTypeMap.ContainsKey("Description")) WriteToExcel((int)errorRow, sonucTypeMap["Description"], ex.ToString());
        }
        return result;
    }
    private void btnGo_Click_1(object sender, EventArgs e)
    {
        switch ((ActionType)btnGo.Tag)
        {
            case ActionType.Awaiting:
            case ActionType.Pausing:
                RunProcess();
                break;
            case ActionType.Running:
                //PauseProcess();
                break;
            case ActionType.Finish:
                this.Close();
                break;
        }
    }
}
}
