﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Protein.Common.Extensions;
//using Protein.Data.Repositories;
//using Protein.Common.Entities;

using Protein.Common.Constants;
using System.Diagnostics;
using Protein.Data.Helpers;


using Protein.BesimTestConsole.ProteinSrvTestOnLocal;
using Protein.Common.Messaging;
using Protein.Data.Repositories;
using static Protein.Common.Entities.ProteinEntities;
//using Protein.BesimTestConsole.ProteinSrvProd;

namespace TestBesimConsole
{
    class Program
    {
        static void ProteinSrv()
        {
            ProteinSrvSoapClient client = new ProteinSrvSoapClient();
            client.PoliceAktar("dffsdf", new PoliceAktarReq());

        }
        static void Main(string[] args)
        {
            //ProteinSrv();
            SendMail();
            //CreateEntitiesAndRepositoriesFromSqlFile();

            //ViewHelper helper = new ViewHelper();
            //var result1 = helper.GetView("T_DOCTOR_BRANCH");
            //var result2 = helper.GetView("T_DOCTOR_BRANCH", whereCondititon: "", orderby: "ID DESC", pageNumber: 0, rowsPerPage: 25);

            //var result2 = spResponseRepository.FindBy("Where ID=@ID",parameters:new { ID=3 });
            //var result3 = spResponseRepository.FindById(3);

            //var result1 = Helper.GetLookupData(Constants.LookupTypes.DoctorBranch);
            //var result2 = Helper.GetLookupData(Constants.LookupTypes.Address);
            //var result3 = Helper.GetLookupData(Constants.LookupTypes.Gender);

            //var val1 = Helper.GetAppKey(Constants.AppKeys.SagmerUrl);
            //var val2 = Helper.GetAppKey(Constants.AppKeys.SagmerUsername);
            //var val3 = Helper.GetAppKey(Constants.AppKeys.SMTPServer);

        }

        static void SendMail()
        {
            var vClaim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID=811542", orderby: "", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();

            var dbIcds = new GenericRepository<V_ClaimIcd>().FindBy($"CLAIM_ID=811542", orderby: "");// ViewHelper.GetView(Views.ClaimICD, "CLAIM_ID =: id", parameters: new { id = claimId });
            var icdNameList = "";
            if (dbIcds != null && dbIcds.Count > 0)
            {
                icdNameList = string.Join(", ", dbIcds.Select(i => i.ICD_NAME));
            }

            long medicineProcessListId = 0;
            var medicineProcessList = ProcessListHelper.GetProcessList("İLAÇ");
            if (medicineProcessList != null)
            {
                medicineProcessListId = medicineProcessList.Id;
            }

            var dbClaimProcesses = new GenericRepository<V_ClaimProcess>().FindBy($"CLAIM_ID =811542 AND PROCESS_LIST_ID !={medicineProcessListId}"); //ViewHelper.GetView(Views.ClaimProcess, "", parameters: new { id = claimId, processListId = medicineProcessListId });
            var processNameList = "";
            if (dbClaimProcesses != null && dbClaimProcesses.Count > 0)
            {
                processNameList = string.Join(", ", dbIcds.Select(i => i.ICD_NAME));
            }

            MailSender msender = new MailSender();
            msender.SendMessage(
                new EmailArgs
                {
                    TO = "fulya@imecedestek.com",
                    IsHtml = true,
                    Subject = $"Yüksek Tutarlı Hasar Bilgilendirme",
                    Content = $"{vClaim.CLAIM_ID} nolu provizyon yuksek hasarlidir. Kontrol ediniz! </br></br> Hasar Statusu : PROV </br> Tutar : {vClaim.PAID} TL </br> Şirket : {vClaim.COMPANY_NAME} </br> Policeno : {vClaim.POLICY_NUMBER} </br> Provizyon No : {vClaim.CLAIM_ID} </br> Sigortali : {vClaim.FIRST_NAME} {vClaim.LAST_NAME} </br> Sigorta Ettiren : {vClaim.INSURER_NAME} </br> Kurum : {vClaim.PROVIDER_NAME} </br> Hasar Tarihi : {vClaim.CLAIM_DATE} </br> İşlemler : {processNameList} </br> Tanılar : {icdNameList}",
                });
        }


        public static void CreateEntitiesAndRepositoriesFromSqlFile()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            string sourceSqlFile = $@"{AppDomain.CurrentDomain.BaseDirectory}Utils\PROTEIN_DB_ALL._ORACLE.sql";
            string outFolder = @"C:\MoorProjects\";

            StringBuilder sbDropTables = new StringBuilder();
            StringBuilder sbTableConfig = new StringBuilder();

            StringBuilder sbEntities = new StringBuilder();
            StringBuilder sbFields = new StringBuilder();

            StringBuilder sbRepositories = new StringBuilder();
            StringBuilder sbLookupConstants = new StringBuilder();

            Dictionary<string, string> tables = new Dictionary<string, string>();
            Dictionary<string, string> lookup = new Dictionary<string, string>();
            string line = string.Empty;
            string currentTableName = string.Empty;
            string currentClassName = string.Empty;

            using (StreamReader sr = new StreamReader(sourceSqlFile))
            {
                while ((line = sr.ReadLine()) != null)
                {
                    //Remove whitespace chars from line
                    line = line.RemoveRepeatedWhiteSpace().Replace("\"", "");

                    #region Parse SQL File

                    //Split line value
                    string[] splitedLine = line.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                    //If empty line continue
                    if (splitedLine.Count() <= 0
                        || splitedLine[0] == "CONSTRAINT"
                        || splitedLine[0].StartsWith("/*")
                        || splitedLine[0] == ");"
                        || splitedLine[0] == "/*"
                        )
                    {
                        continue;
                    }
                    else if (splitedLine[0] == "alter")
                    {
                        break;
                    }


                    switch (splitedLine[0])
                    {
                        case "CREATE":
                            sbEntities.Replace("[FIELDS]", sbFields.ToString());
                            sbFields.Clear();

                            //New create table line
                            currentTableName = splitedLine[2];
                            currentClassName = currentTableName.Remove(0, 2).ToLower().ToTitleCase().Replace("_", "").ReplaceTurkishCharacters();
                            tables.Add(currentTableName, "0");

                            if (currentTableName != "T_SP_RESPONSE")
                            {
                                //Create repositories
                                sbRepositories.AppendLine($"public class {currentClassName}Repository : GenericRepository<{currentClassName}>");
                                sbRepositories.AppendLine("{");
                                sbRepositories.AppendLine($"    public {currentClassName}Repository()");
                                sbRepositories.AppendLine("    {");
                                sbRepositories.AppendLine("    }");
                                sbRepositories.AppendLine("}");

                                //Table Entity
                                sbEntities.AppendLine($"[Table(\"{currentTableName}\")]");
                                sbEntities.AppendLine($"public class {currentClassName}");
                                sbEntities.AppendLine("{");
                                sbEntities.AppendLine("[FIELDS]");
                                sbEntities.AppendLine("}");
                            }
                            else
                            {
                                //Table Entity
                                sbEntities.AppendLine($"[Table(\"{currentTableName}\")]");
                                sbEntities.AppendLine($"public class {currentClassName}<T>");
                                sbEntities.AppendLine("{");
                                sbEntities.AppendLine("[FIELDS]");
                                sbEntities.AppendLine("public T Data { get; set; }");
                                sbEntities.AppendLine("}");
                            }

                            //Create Table DROP SQL
                            sbDropTables.AppendLine($"DROP TABLE {currentTableName} CASCADE CONSTRAINTS PURGE;");
                            break;

                        case "SP_RESPONSE_ID":
                            //Set table historical value
                            tables[currentTableName] = "1";
                            break;

                        default:

                            string columnName = splitedLine[0];
                            string fieldName = columnName.ToLower().ToTitleCase().Replace("_", "").ReplaceTurkishCharacters();
                            fieldName = fieldName == currentClassName ? "f" + fieldName : fieldName;

                            sbFields.AppendLine($"[Column(\"{columnName}\")]");

                            string fieldtype = splitedLine[1].ToUpper();

                            if (fieldtype.IndexOf("VARCHAR") >= 0)
                            {
                                sbFields.AppendLine($"[MaxLength( {fieldtype.Split('(')[1].Split(')')[0]} , ErrorMessage = \"\")]");
                                sbFields.AppendLine($"public string {fieldName} {{ get; set; }}");
                            }
                            else if (fieldtype.IndexOf("CLOB") >= 0)
                            {
                                sbFields.AppendLine($"public string {fieldName} {{ get; set; }}");
                            }
                            else if (fieldtype.IndexOf("NUMBER") >= 0)
                            {
                                if (fieldtype.Split('(')[1].Split(')')[0].IndexOf(',') >= 0)
                                    sbFields.AppendLine($"public Decimal {fieldName} {{ get; set; }}");

                                else if (Convert.ToInt32(fieldtype.Split('(')[1].Split(')')[0]) < 10)
                                    sbFields.AppendLine($"public int {fieldName} {{ get; set; }}");

                                else if (Convert.ToInt32(fieldtype.Split('(')[1].Split(')')[0]) > 10)
                                    sbFields.AppendLine($"public Int64 {fieldName} {{ get; set; }}");
                            }
                            else if (fieldtype.IndexOf("DATE") >= 0)
                            {
                                sbFields.AppendLine($"public DateTime {fieldName} {{ get; set; }}");
                            }

                            //LookupTypes
                            if (line.IndexOf("-->") >= 0)
                            {
                                string lookupValue = line.Split(new string[] { "--> " }, StringSplitOptions.RemoveEmptyEntries)[1];
                                if (!lookup.Keys.Contains(lookupValue))
                                {
                                    string lookupName = lookupValue.Split(new string[] { "Type" }, StringSplitOptions.RemoveEmptyEntries)[0];
                                    sbLookupConstants.AppendLine($"public const string {lookupName} = \"{lookupValue}\";");
                                    lookup.Add(lookupValue, lookupName);
                                }
                            }
                            break;
                    }

                    #endregion

                }//End while


                foreach (var table in tables)
                {
                    //Create T_TABLE_CONFIGURATION Insert SQL
                    sbTableConfig.AppendLine($"INSERT INTO T_TABLE_CONFIGURATION (TABLE_NAME, KEY, VALUE) VALUES ('{table.Key}','Historical','{table.Value}');");
                    //sbTableConfig.AppendLine($"INSERT INTO T_TABLE_CONFIGURATION (TABLE_NAME, KEY, VALUE) VALUES ('{table.Key}','Alias','{table.Value}');");
                }

            }

            File.WriteAllText($"{outFolder}Repositories.txt", sbRepositories.ToString());
            File.WriteAllText($"{outFolder}Entities.txt", sbEntities.ToString().Replace("[FIELDS]", sbFields.ToString()));
            File.WriteAllText($"{outFolder}DropTables.sql", sbDropTables.ToString());
            File.WriteAllText($"{outFolder}InsertTablesConfig.sql", sbTableConfig.ToString());
            File.WriteAllText($"{outFolder}LookupConstants.txt", sbLookupConstants.ToString());

            sw.Stop();
            Console.WriteLine($"Elapsed miliseconds : {sw.ElapsedMilliseconds}");
        }





    }
}
