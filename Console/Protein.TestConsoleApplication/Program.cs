﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

//using Protein.Business.Entities;
using Protein.Common.Constants;
//using Protein.Common.Helpers;
using Protein.Data;
using Protein.Data.ExternalServices;
using Protein.Data.Helpers;

//using Protein.Data.ExternalServices.SagmerOnlineService;
//using static Protein.Data.ExternalServices.SagmerOnlineService.Enums;
//using static Protein.Data.ExternalServices.SagmerOnlineService.InputOutputTypes;

namespace Protein.TestConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {


            //SagmerTest();

            //SPEXECUTE_Test();

            //LookupDataTest();

            //PortTssTest();
        }

        //public static void PortTssTest()
        //{
        //    Protein.Data.ExternalServices.PortTSS.ServiceClient sc = new Data.ExternalServices.PortTSS.ServiceClient();

        //    var insured = new Data.ExternalServices.PortTSS.InputOutputTypes.SigortaliSorgula()
        //    {
        //        UserName = "testuser",
        //        Password = "*****",
        //        Kurum = "1",
        //        TcKimlikNo = "39064931364",
        //        AyaktanYatarak = "A",
        //        OlayTarihi = DateTime.Now.ToString()
        //    };

        //    sc.SigortaliHakSorgula(insured);

        //}
        //public static void LookupDataTest()
        //{
        //    var result1 = LookupHelper.GetLookupData(Constants.LookupTypes.DoctorBranch);
        //    var result2 = LookupHelper.GetLookupData(Constants.LookupTypes.Address);
        //    var result3 = LookupHelper.GetLookupData(Constants.LookupTypes.Gender);

        //    var val1 = AppSettingsHelper.GetKey(Constants.AppKeys.SagmerUrl);
        //    var val2 = AppSettingsHelper.GetKey(Constants.AppKeys.SagmerUsername);
        //    var val3 = AppSettingsHelper.GetKey(Constants.AppKeys.SMTPServer);

        //}

        public static void SagmerTest()
        {
            //ServiceClient sagmerService = new ServiceClient();


            //policeInputType input1 = new policeInputType()
            //{
            //    sigortaSirketKod = "105", //string
            //    branshKod = 0, //long
            //    dovizCinsi = "", //string
            //    uretimKaynakKod = 0, //long
            //    tarifeID = 0, //long
            //    yeniYenilemeGecis = ((int)yeniYenilemeGecisType.Item1).ToString(), //enum
            //    policeNo = "", //string
            //    yenilemeNo = 0, //int
            //    zeylNo = 0, //int
            //    policeNetPrimi = 0, //double
            //    policeBrutPrimi = 0, //double
            //    policeTanzimTarihi = DateTime.Now, //DatetimeDateTime
            //    policeBaslamaTarihi = DateTime.Now, //DateTime
            //    policeBitisTarihi = DateTime.Now, //DateTime
            //    endirektSigortaSirketKod = "", //string - OPTIONAL
            //    eskiPoliceNo = "", //string - OPTIONAL
            //    eskiYenilemeNo = 0, //int - OPTIONAL
            //    ilKod = "", //string - 
            //    odemeKod = 0, //long
            //    vadeSayisi = 0, //int
            //    otorizasyonKod = "", //string - OPTIONAL
            //    sigortaEttirenType = new sigortaEttirenInputType() //sigortaEttirenInputType
            //    {
            //        turKod = turKod.O,
            //        kimlikTipKod = ((int)identityType.Item1).ToString(),
            //        kimlikNo = "",
            //        ad = "", //string - OPTIONAL
            //        soyad = "", //string - OPTIONAL
            //        cinsiyet = cinsiyet.E, //string - OPTIONAL
            //        dogumTarihi = DateTime.Now, //string - OPTIONAL
            //        ulkeKodu = 090, //string - OPTIONAL
            //        babaAdi = "",
            //        adres = "",
            //        dogumYeri = "",
            //        kurulusYeri = "",
            //        kurulusTarihi = DateTime.Now,
            //        sigortaEttirenUyruk = uyrukTuru.TC

            //    },
            //    policeTip = policeTipiType.F, //enum
            //    uretimKaynakKurumKod = "", //string
            //};

            //service.police(input1);



            //policeListSorguInputType input2 = new policeListSorguInputType()
            //{
            //    baslangicTarihi = DateTime.Now.AddDays(-7),
            //    bitisTarihi = DateTime.Now.ToUniversalTime(),
            //    sigortaSirketKod = "105"
            //};

            //service.policeListSorgu(input2);

            //service.policeKontrol(input1);

            //XmlDocument xmlDoc = new XmlDocument();

            //xmlDoc.Load("C:\\MoorProjects\\policeGunlukMutabakat.xml");
            //var result1 = XmlHelper.ParseResponseXml<SgmPoliceGunlukMutabakatTypeList>(xmlDoc.OuterXml, "return");

            //xmlDoc.Load("C:\\MoorProjects\\policeList.xml");
            //var result2 = XmlHelper.ParseResponseXml<PoliceList>(xmlDoc.OuterXml, "return");

            //xmlDoc.Load("C:\\MoorProjects\\policeResponse.xml");
            //var result3 = XmlHelper.ParseResponseXml<policeSonucType>(xmlDoc.OuterXml, "return");

            //xmlDoc.Load("C:\\MoorProjects\\policeSorguSonucType.xml");
            //var result4 = XmlHelper.ParseResponseXml<policeSorguSonucType>(xmlDoc.OuterXml, "return");

            //xmlDoc.Load("C:\\MoorProjects\\policeZeylList.xml");
            //var result5 = XmlHelper.ParseResponseXml<PoliceZeylList>(xmlDoc.OuterXml, "return");

            //xmlDoc.Load("C:\\MoorProjects\\sigortaliGunlukMutabakat.xml");
            //var result6 = XmlHelper.ParseResponseXml<SgmSigortaliGunlukMutabakatTypeList>(xmlDoc.OuterXml, "return");

        }

        //Oracle SP_EXECUTE denemesi UDT olmadan
        //public static void SPEXECUTE_Test()
        //{
        //    Protein.Common.Entities.ProteinEntities.Network entity1 = new Common.Entities.ProteinEntities.Network();
        //    entity1.Category = "0";
        //    entity1.Name = "Test from console update1";
        //    entity1.Status = Common.Entities.ProteinEntities.Status.NotDeleted;
        //    entity1.ID = 1;

        //    Protein.Common.Entities.ProteinEntities.Network entity2 = new Common.Entities.ProteinEntities.Network();
        //    entity2.Category = "0";
        //    entity2.Name = "Test from console update2";
        //    entity2.Status = Common.Entities.ProteinEntities.Status.NotDeleted;
        //    entity2.ID = 2;

        //    Protein.Common.Entities.ProteinEntities.Network entity3 = new Common.Entities.ProteinEntities.Network();
        //    entity3.Category = "0";
        //    entity3.Name = "Test from console update3";
        //    entity3.Status = Common.Entities.ProteinEntities.Status.NotDeleted;
        //    entity3.ID = 3;

        //    List<Protein.Common.Entities.ProteinEntities.Network> updateList = new List<Common.Entities.ProteinEntities.Network>();
        //    updateList.Add(entity1);
        //    updateList.Add(entity2);
        //    updateList.Add(entity3);

        //    //Protein.Data.Repositories.NetworkRepository tmpRepo = new Data.Repositories.NetworkRepository();
        //    //tmpRepo.UpdateEntities(updateList);

        //}

        //Oracle SP_EXECUTE denemesi UDT ile
        public static RESPONSELIST_C SP_EXECUTE()
        {
            RESPONSELIST_C res = null;

            string conStr = ConfigurationManager.AppSettings["ProteinDbConnectionString"];

            OracleConnection con = new OracleConnection(conStr);

            try
            {

                string strXmlParam =
                  "<tablelist>"
                + "  <table>"
                + "    <name>NETWORK</name>"
                + "    <id></id>"
                + "    <fieldlist>"
                + "      <field>"
                + "        <name>CATEGORY</name>"
                + "        <value>1</value>"
                + "      </field>"
                + "      <field>"
                + "        <name>NAME</name>"
                + "        <value>Test Sigorta Şirketi</value>"
                + "      </field>"
                + "      <field>"
                + "        <name>STATUS</name>"
                + "        <value>1</value>"
                + "      </field>"
                + "    </fieldlist>"
                + "  </table>"
                + "</tablelist>";


                con.Open();

                OracleCommand cmd = con.CreateCommand();
                cmd.CommandText = "SP_EXECUTE";
                cmd.CommandType = CommandType.StoredProcedure;

                OracleParameter parameterInput = new OracleParameter();
                parameterInput.OracleDbType = OracleDbType.Clob;
                parameterInput.Direction = ParameterDirection.Input;
                parameterInput.ParameterName = "p_xml";
                parameterInput.Value = strXmlParam;
                cmd.Parameters.Add(parameterInput);

                OracleParameter parameterOutput = new OracleParameter();
                parameterOutput.OracleDbType = OracleDbType.Array;
                parameterOutput.Direction = System.Data.ParameterDirection.Output;
                parameterOutput.UdtTypeName = "RESPONSELIST_C";
                parameterOutput.ParameterName = "p_rsp";
                cmd.Parameters.Add(parameterOutput);

                cmd.ExecuteNonQuery();

                if (parameterOutput.Value != DBNull.Value)
                {
                    res = ConvertToResponseList(parameterOutput);
                }

            }
            catch (Exception ex)
            {
                string msg = $"{ex.Message}";
            }
            finally
            {
                con.Close();
            }

            return res;

        }

        public static RESPONSELIST_C ConvertToResponseList(OracleParameter param)
        {
            RESPONSELIST_C result = new RESPONSELIST_C();

            try
            {
                if (param.Value != DBNull.Value)
                {
                    result = param.Value as RESPONSELIST_C;
                }
            }
            catch (Exception ex)
            {
                string msg = $"{ex.Message}";
            }

            return result;
        }

    }//End class

    public class RESPONSELIST_C : IOracleCustomType
    {
        private RESPONSELIST_T[] _elm_Array;

        public RESPONSELIST_C()
        {

        }

        [OracleArrayMapping()]

        public RESPONSELIST_T[] ELM_ARRAY { get { return _elm_Array; } set { _elm_Array = value; } }

        public void FromCustomObject(OracleConnection con, System.IntPtr pUdt) { OracleUdt.SetValue(con, pUdt, 0, this._elm_Array); }

        public void ToCustomObject(OracleConnection con, System.IntPtr pUdt) { this._elm_Array = ((RESPONSELIST_T[])(OracleUdt.GetValue(con, pUdt, 0))); }

    }

    [OracleCustomTypeMapping("RESPONSELIST_C")]
    public class RESPONSELIST_C_Factory : IOracleCustomTypeFactory, IOracleArrayTypeFactory
    {
        public Array CreateArray(int numElems)
        {
            return new RESPONSELIST_T[numElems];
        }

        public IOracleCustomType CreateObject()
        {
            return new RESPONSELIST_C();
        }

        public Array CreateStatusArray(int numElems)
        {
            return new OracleUdtStatus[numElems];
        }
    }

    public class RESPONSELIST_T : IOracleCustomType
    {
        private int _id;
        private string _code;
        private string _message;

        [OracleObjectMapping("ID")]
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        [OracleObjectMapping("CODE")]
        public string CODE
        {
            get { return _code; }
            set { _code = value; }
        }

        [OracleObjectMapping("MESSAGE")]
        public string MESSAGE
        {
            get { return _message; }
            set { _message = value; }
        }

        void IOracleCustomType.FromCustomObject(OracleConnection con, IntPtr pUdt)
        {
            OracleUdt.SetValue(con, pUdt, "ID", this.ID);
            OracleUdt.SetValue(con, pUdt, "CODE", this.CODE);
            OracleUdt.SetValue(con, pUdt, "MESSAGE", this.MESSAGE);
        }

        void IOracleCustomType.ToCustomObject(OracleConnection con, IntPtr pUdt)
        {
            this.ID = ((int)(OracleUdt.GetValue(con, pUdt, "ID")));
            this.CODE = ((string)(OracleUdt.GetValue(con, pUdt, "CODE")));
            this.MESSAGE = ((string)(OracleUdt.GetValue(con, pUdt, "MESSAGE")));
        }

    }

    [OracleCustomTypeMapping("RESPONSELIST_T")]
    public class RESPONSELIST_T_Factory : IOracleCustomTypeFactory
    {
        public virtual IOracleCustomType CreateObject()
        {
            return new RESPONSELIST_T();
        }
    }

}
