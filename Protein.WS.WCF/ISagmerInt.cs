﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Protein.Data.ExternalServices;
using static Protein.Data.ExternalServices.SagmerOnlineService.InputOutputTypes;

namespace Protein.WS.WCF
{
    
    [ServiceContract]
    public interface ISagmerInt
    {
        [OperationContract]
        ServiceResponse<policeSonucType> police(policeInputType input);
        [OperationContract]
        ServiceResponse<zeylSonucType> sigortaliGirisZeyli(sigortaliGirisInputType input);
        [OperationContract]
        ServiceResponse<zeylSonucType> sigortaliBilgiDegisiklikZeyli(sigortaliBilgiDegisiklikInputType input);
        [OperationContract]
        ServiceResponse<sigortaliBasvuruRedSonucType> sigortaliBasvuruRed(sigortaliBasvuruRedInputType input);
        [OperationContract]
        ServiceResponse<zeylSonucType> sigortaliTahakkukZeyli(sigortaliPrimFarkiInputType input);
        [OperationContract]
        ServiceResponse<zeylSonucType> sigortaliCikisZeyli(sigortaliCikisInputType input);
        [OperationContract]
        ServiceResponse<zeylSonucType> policeBaslamaBitisTarihDegisiklikZeyli(policeTarihDegisiklikInputType input);
        [OperationContract]
        ServiceResponse<policeYenilenmemeSonucType> policeYenilenmeme(policeYenilenmemeInputType input);
        [OperationContract]
        ServiceResponse<zeylSonucType> policeYenilenmeme(primFarkiInputType input);
    }
}
