﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Protein.Data.ExternalServices.SagmerOnlineService;

namespace Protein.WS.WCF
{
    public class SagmerIntegration : ISagmerInt
    {
        public InputOutputTypes.ServiceResponse<InputOutputTypes.policeSonucType> police(InputOutputTypes.policeInputType input)
        {
            throw new NotImplementedException();
        }

        public InputOutputTypes.ServiceResponse<InputOutputTypes.zeylSonucType> policeBaslamaBitisTarihDegisiklikZeyli(InputOutputTypes.policeTarihDegisiklikInputType input)
        {
            throw new NotImplementedException();
        }

        public InputOutputTypes.ServiceResponse<InputOutputTypes.policeYenilenmemeSonucType> policeYenilenmeme(InputOutputTypes.policeYenilenmemeInputType input)
        {
            throw new NotImplementedException();
        }

        public InputOutputTypes.ServiceResponse<InputOutputTypes.zeylSonucType> policeYenilenmeme(InputOutputTypes.primFarkiInputType input)
        {
            throw new NotImplementedException();
        }

        public InputOutputTypes.ServiceResponse<InputOutputTypes.sigortaliBasvuruRedSonucType> sigortaliBasvuruRed(InputOutputTypes.sigortaliBasvuruRedInputType input)
        {
            throw new NotImplementedException();
        }

        public InputOutputTypes.ServiceResponse<InputOutputTypes.zeylSonucType> sigortaliBilgiDegisiklikZeyli(InputOutputTypes.sigortaliBilgiDegisiklikInputType input)
        {
            throw new NotImplementedException();
        }

        public InputOutputTypes.ServiceResponse<InputOutputTypes.zeylSonucType> sigortaliCikisZeyli(InputOutputTypes.sigortaliCikisInputType input)
        {
            throw new NotImplementedException();
        }

        public InputOutputTypes.ServiceResponse<InputOutputTypes.zeylSonucType> sigortaliGirisZeyli(InputOutputTypes.sigortaliGirisInputType input)
        {
            throw new NotImplementedException();
        }

        public InputOutputTypes.ServiceResponse<InputOutputTypes.zeylSonucType> sigortaliTahakkukZeyli(InputOutputTypes.sigortaliPrimFarkiInputType input)
        {
            throw new NotImplementedException();
        }
    }
}