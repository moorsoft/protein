﻿using Protein.Data.Repositories;
using Protein.WS.WS.Common;
using System;
using Protein.Data.ExternalServices;
using Protein.Data.ExternalServices.Common.Lib;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using Protein.Data.Helpers;
using System.Drawing;
using Protein.WS;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Protein.Common.Entities.ProteinEntities;
using System.Threading;
using Protein.Common.Messaging;
using System.Xml;
using Protein.Data.ExternalServices.Protein.Util.Lib;
using System.Reflection;
using System.Configuration;
using Protein.WS.WS.Doga;
using static Protein.Common.Constants.Constants;
using System.Net;
using Protein.Data.ExternalServices.Log;

namespace AgreementService
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void CreateFailedLog(IntegrationLog log, string responseXml, string[] desc)
        {
            log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
            log.Type = ((int)LogType.FAILED).ToString();
            log.Response = responseXml;

            log.WsResponseDate = DateTime.Now;
            IntegrationLogHelper.AddToLog(log);
        }

        private void CreateOKLog(IntegrationLog log, string responseXml)
        {
            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
            log.Type = ((int)LogType.COMPLETED).ToString();
            log.Response = responseXml;

            log.WsResponseDate = DateTime.Now;
            IntegrationLogHelper.AddToLog(log);
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            //string[] agreementServiceListArray = { "PolicyAgreement", "ZeyilAGreement", "TazminatAgreement", "IcmalAgreement" };
            //List<AgreementServiceList> agreementWsList = new List<AgreementServiceList>();
            //List<ServiceName> agreementServiceName = new List<ServiceName>();
            //for (int i = 0; i < agreementServiceListArray.Length; i++)
            //{
            //    agreementServiceName.Add(new ServiceName
            //    {
            //        servisAdi = agreementServiceListArray[i]
            //    });

            //}


            //var wsList = new GenericRepository<Lookup>().FindBy(conditions: $"NET_ENUM_TYPE_NAME='WsAgreementType'");


            //foreach (var wsListItem in agreementServiceName)
            //{
            //    wsListComboBox.Items.Add(wsListItem.servisAdi);
            //}

            string saat = DateTime.Now.ToLongTimeString();
            if (saat.Contains("20:00"))
            {
                DovizKuru();
            }
            else if (saat.Contains("02:00"))
            {
                DogaPoliceWs();
            }
        }

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    string slctWsListCombobox = wsListComboBox.SelectedItem.ToString();
        //    AgreementMatchSrv agreementMatchSrv = new AgreementMatchSrv();
        //    if (slctWsListCombobox == "PolicyAgreement")
        //    {
        //        PoliceMutabakatReq policyAgreementReq = new PoliceMutabakatReq();
        //        policyAgreementReq.SirketKod = 1;
        //        policyAgreementReq.TransferTarihiBaslangic = DateTime.Now;
        //        policyAgreementReq.TransferTarihiBitis = DateTime.Now;
        //        var agreementResult = agreementMatchSrv.PolicyAgreement(policyAgreementReq);
        //        listBox1.Items.Add(agreementResult.resultWs);
        //    }
        //    if (slctWsListCombobox == "ZeyilAGreement")
        //    {
        //        ZeyilMutabakatReq zeyilMutabakatReq = new ZeyilMutabakatReq();
        //        zeyilMutabakatReq.SirketKod = 1;
        //        zeyilMutabakatReq.TransferTarihiBaslangic = DateTime.Now;
        //        zeyilMutabakatReq.TransferTarihiBitis = DateTime.Now;
        //        zeyilMutabakatReq.YenilemeNo = 0;
        //        zeyilMutabakatReq.PoliceNo = "0";

        //        var agreementResult = agreementMatchSrv.ZeyilAGreement(zeyilMutabakatReq);
        //        listBox1.Items.Add(agreementResult.resultWs);

        //    }

        //    if (slctWsListCombobox == "TazminatAgreement")
        //    {
        //        ProvizyonMutabakatReq provizyonMutabakatReq = new ProvizyonMutabakatReq();
        //        provizyonMutabakatReq.SirketKod = 1;
        //        provizyonMutabakatReq.TransferTarihiBaslangic = DateTime.Now;
        //        provizyonMutabakatReq.TransferTarihiBitis = DateTime.Now;

        //        var agreementResult = agreementMatchSrv.TazminatAgreement(provizyonMutabakatReq);
        //        listBox1.Items.Add(agreementResult.resultWs);

        //    }
        //    if (slctWsListCombobox == "IcmalAgreement")
        //    {
        //        IcmalMutabakatReq icmalMutabakatReq = new IcmalMutabakatReq();
        //        icmalMutabakatReq.SirketKod = 1;
        //        icmalMutabakatReq.TransferTarihiBaslangic = DateTime.Now;
        //        icmalMutabakatReq.TransferTarihiBitis = DateTime.Now;

        //        var agreementResult = agreementMatchSrv.IcmalAgreement(icmalMutabakatReq);
        //        listBox1.Items.Add(agreementResult.resultWs);

        //    }

        //}

        private void button2_Click(object sender, EventArgs e)
        {
            DovizKur dovizKur = new DovizKur();
            dovizKur.Show();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            DogaPoliceMutabakat dogaPoliceMutabakat = new DogaPoliceMutabakat();
            dogaPoliceMutabakat.Show();
        }


        public void DogaPoliceWs()
        {
            XmlDocument xmlDoc = new XmlDocument();
            string baslangicBitisTarihi = DateTime.Now.AddDays(-1).ToString("dd-MM-yyyy");



            string link = "https://portal.dogasigorta.com/WebServisleri/AcenteBilgiServisleri.asmx/PoliceListesi?kullaniciAdi=ws_IMECE&parola=8Qdzp9vpMHkmfNwH&acenteNo=&baslangicBrans=600&bitisBrans=610&baslangicTarihi=" + baslangicBitisTarihi + "&bitisTarihi=" + baslangicBitisTarihi + "";
            xmlDoc.Load(link);
            var currentMethod = MethodBase.GetCurrentMethod();
            DogaPoliceAktarReq req = new DogaPoliceAktarReq();

            var company = new GenericRepository<V_CompanyParameter>().FindBy($"KEY='APICODE' and COMPANY_ID=5", orderby: "COMPANY_ID").FirstOrDefault();

            if (ApiCodeIsValid(company.Value, company.CompanyId) > 0)
            {
                try
                {
                    int sayi = 0;
                    int mutabikPoliceler = 0;
                    int aktarilanPoliceler = 0;
                    string aktarilanPoliceNo = "";
                    List<DogaMutabakatPoliceList> PoliceList = new List<DogaMutabakatPoliceList>();
                    foreach (XmlNode item in xmlDoc.SelectNodes("//Police"))
                    {
                        PoliceList.Add(new DogaMutabakatPoliceList
                        {
                            AcenteNo = item.ChildNodes[0].InnerXml,
                            Brans = item.ChildNodes[1].InnerXml,
                            policeNo = item.ChildNodes[2].InnerXml,
                            TecditNo = item.ChildNodes[3].InnerXml,
                            ZeyilNo = item.ChildNodes[4].InnerXml,
                            IptalKayit = item.ChildNodes[5].InnerXml,
                            TanzimTarihi = item.ChildNodes[6].InnerXml
                        });
                    }
                    progressBar1.Maximum = PoliceList.Count();
                    foreach (var PoliceDogaList in PoliceList)
                    {
                        DogaSigorta dogaSigorta = new DogaSigorta();

                        req.AcenteNo = PoliceDogaList.AcenteNo;
                        req.BransKod = PoliceDogaList.Brans;
                        req.PoliceNo = PoliceDogaList.policeNo;
                        req.ZeylNo = PoliceDogaList.ZeyilNo;
                        req.YenilemeNo = PoliceDogaList.TecditNo;
                        var result = dogaSigorta.PoliceAktar(company.Value, req);
                        if (result.SonucKod == "1")
                        {
                            aktarilanPoliceler++;
                            aktarilanPoliceNo = PoliceDogaList.policeNo + " ,";

                            IntegrationLog log = new IntegrationLog
                            {

                                WsName = currentMethod.DeclaringType.FullName,
                                WsMethod = currentMethod.Name,
                                WsRequestDate = DateTime.Now,
                                ResponseStatusDesc = $"HATA / Poliçe aktarılmamış. Poliçe İçeri aktarıldı. Poliçe No :  { PoliceDogaList.policeNo } . Mail Gönderildi.",
                                PolicyNumber = PoliceDogaList.policeNo,

                            };
                            CreateFailedLog(log, null, new string[] { log.ResponseStatusDesc });
                        }
                        else
                        {
                            mutabikPoliceler++;
                            IntegrationLog log = new IntegrationLog
                            {

                                WsName = currentMethod.DeclaringType.FullName,
                                WsMethod = currentMethod.Name,
                                WsRequestDate = DateTime.Now,
                                ResponseStatusDesc = "Başarılı / Mutabık",
                                PolicyNumber = PoliceDogaList.policeNo
                            };
                            CreateOKLog(log, null);
                        }
                        sayi++;
                        progressBar1.Value = sayi;


                    }

                    MailSender msender = new MailSender();
                    msender.SendMessage(
                        new EmailArgs
                        {
                            TO = "uretim@imecedestek.com;serdar@imecedestek.com;oguzhan.demir@mooryazilim.com",
                            IsHtml = true,
                            Subject = $"Doğa Poliçe Mutabakat / HATA",
                            Content = $"Mutabık poliçe sayısı : {mutabikPoliceler} , İçeri aktarılan Poliçe sayısı : {aktarilanPoliceler} <br /> Aktarılan Poliçe No'lar :  {aktarilanPoliceNo} "
                        });

                }
                catch (Exception ex)
                {
                    IntegrationLog log = new IntegrationLog
                    {

                        WsName = currentMethod.DeclaringType.FullName,
                        WsMethod = currentMethod.Name,
                        ResponseStatusCode = "0",
                        ResponseStatusDesc = $"Hata : {ex.Message}"
                    };
                    CreateFailedLog(log, null, new string[] { ex.Message });
                }
            }

            Environment.Exit(0);
        }

        public void DovizKuru()
        {
            DataTable dt = new DataTable();
            DataRow dr;

            dt.Columns.Add(new DataColumn("Adı", typeof(string)));
            dt.Columns.Add(new DataColumn("Kod", typeof(string)));
            dt.Columns.Add(new DataColumn("Döviz alış", typeof(string)));
            dt.Columns.Add(new DataColumn("Döviz satış", typeof(string)));
            dt.Columns.Add(new DataColumn("Efektif alış", typeof(string)));
            dt.Columns.Add(new DataColumn("Efektif Satış", typeof(string)));

            XmlTextReader rdr = new XmlTextReader("http://www.tcmb.gov.tr/kurlar/today.xml");
            XmlDocument myxml = new XmlDocument();
            myxml.Load(rdr);

            XmlNode tarih = myxml.SelectSingleNode("/Tarih_Date/@Tarih");
            XmlNodeList mylist = myxml.SelectNodes("/Tarih_Date/Currency");
            XmlNodeList adi = myxml.SelectNodes("/Tarih_Date/Currency/Isim");
            XmlNodeList kod = myxml.SelectNodes("/Tarih_Date/Currency/@Kod");
            XmlNodeList unit = myxml.SelectNodes("/Tarih_Date/Currency/Unit");
            XmlNodeList doviz_alis = myxml.SelectNodes("/Tarih_Date/Currency/ForexBuying");
            XmlNodeList doviz_satis = myxml.SelectNodes("/Tarih_Date/Currency/ForexSelling");
            XmlNodeList efektif_alis = myxml.SelectNodes("/Tarih_Date/Currency/BanknoteBuying");
            XmlNodeList efektif_satis = myxml.SelectNodes("/Tarih_Date/Currency/BanknoteSelling");

            // dataGridView1.= tarih.InnerText.ToString() + " tarihli Merkez Bankası kur bilgileri";
            List<ExchangeRate> exchangeRateList = new List<ExchangeRate>();
            int x = 19;
            int i;

            for (i = 0; i < x; i++)
            {
                var lookupData = LookupHelper.GetLookupByCode(LookupTypes.Currency, kod.Item(i).InnerText.ToString());
                if (lookupData != null)
                {
                    var lookupOrdinal = lookupData.Ordinal;
                    ExchangeRate exchangeRate = new ExchangeRate
                    {
                        RateDate = DateTime.Parse(tarih.InnerText.ToString()).AddDays(1),
                        CurrencyType = lookupOrdinal,
                        Unit = unit.Item(i).InnerText.ToString(),
                        ForexBuying = decimal.Parse(doviz_alis.Item(i).InnerText.ToString().Replace('.', ',')),
                        ForexSelling = decimal.Parse(doviz_satis.Item(i).InnerText.ToString().Replace('.', ',')),
                        BanknoteBuying = decimal.Parse(efektif_alis.Item(i).InnerText.ToString().Replace('.', ',')),
                        BanknoteSelling = decimal.Parse(efektif_satis.Item(i).InnerText.ToString().Replace('.', ',')),
                    };
                    exchangeRateList.Add(exchangeRate);
                    dr = dt.NewRow();
                    dr[0] = adi.Item(i).InnerText.ToString();
                    dr[1] = kod.Item(i).InnerText.ToString();

                    dr[2] = doviz_alis.Item(i).InnerText.ToString();
                    // Döviz Alış
                    dr[3] = doviz_satis.Item(i).InnerText.ToString();
                    // Döviz  Satış
                    dr[4] = efektif_alis.Item(i).InnerText.ToString();
                    // Efektif Alış
                    dr[5] = efektif_satis.Item(i).InnerText.ToString();
                    // Efektif Satış.
                    dt.Rows.Add(dr);
                }
            }
            var spResponse = new GenericRepository<ExchangeRate>().InsertSpExecute3(exchangeRateList);

            MailSender msender = new MailSender();
            msender.SendMessage(
                new EmailArgs
                {
                    TO = "oguzhan.demir@mooryazilim.com",
                    IsHtml = false,
                    Subject = $"DOVIZ KURU",
                    Content = $"Başarılı"
                });


            Environment.Exit(0);
        }

        private long ApiCodeIsValid(string apiCode, long CompanyCode = 0)
        {
            long id = 0;

            try
            {
                string ApiCodeCompany = ConfigurationManager.AppSettings["ApiCode" + CompanyCode];

                if (ApiCodeCompany == apiCode)
                    id = CompanyCode;

                //string whereCondition = (CompanyKod > 0 ? $"COMPANY_ID = {CompanyKod} AND " : "") + ($" KEY = '{Constants.AppKeys.ApiCode}' AND VALUE = '{apiCode}' AND SYSTEM_TYPE='{((int)SystemType.WS).ToString()}'");
                //V_CompanyParameter resultCompanyParam = new GenericRepository<V_CompanyParameter>().FindBy(whereCondition, orderby: "").FirstOrDefault();
                ////HttpContext.Current.Response.Write("\n_ApiCodeIsValid__" + apiCode + "__" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                //stopwatch.Stop();
                //reqTime.Add(whereCondition, stopwatch.Elapsed.ToString());
                //if (resultCompanyParam == null) return id;
                //if (resultCompanyParam.Value == apiCode)
                //    id = (long)resultCompanyParam.CompanyId;
            }
            catch
            {
                id = 0;
            }
            return id;
        }
    }
}
