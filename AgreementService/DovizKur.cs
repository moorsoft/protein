﻿using Protein.Data.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;

namespace AgreementService
{
    public partial class DovizKur : Form
    {
        public DovizKur()
        {
            InitializeComponent();
        }
        private void DovizKur_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = source();
            dataGridView1.Columns[0].HeaderText = "Count";
        }
        public DataTable source()
        {
            DataTable dt = new DataTable();
            DataRow dr;

            dt.Columns.Add(new DataColumn("Adı", typeof(string)));
            dt.Columns.Add(new DataColumn("Kod", typeof(string)));
            dt.Columns.Add(new DataColumn("Döviz alış", typeof(string)));
            dt.Columns.Add(new DataColumn("Döviz satış", typeof(string)));
            dt.Columns.Add(new DataColumn("Efektif alış", typeof(string)));
            dt.Columns.Add(new DataColumn("Efektif Satış", typeof(string)));

            XmlTextReader rdr = new XmlTextReader("http://www.tcmb.gov.tr/kurlar/today.xml");
            XmlDocument myxml = new XmlDocument();
            myxml.Load(rdr);

            XmlNode tarih = myxml.SelectSingleNode("/Tarih_Date/@Tarih");
            XmlNodeList mylist = myxml.SelectNodes("/Tarih_Date/Currency");
            XmlNodeList adi = myxml.SelectNodes("/Tarih_Date/Currency/Isim");
            XmlNodeList kod = myxml.SelectNodes("/Tarih_Date/Currency/@Kod");
            XmlNodeList unit = myxml.SelectNodes("/Tarih_Date/Currency/Unit");
            XmlNodeList doviz_alis = myxml.SelectNodes("/Tarih_Date/Currency/ForexBuying");
            XmlNodeList doviz_satis = myxml.SelectNodes("/Tarih_Date/Currency/ForexSelling");
            XmlNodeList efektif_alis = myxml.SelectNodes("/Tarih_Date/Currency/BanknoteBuying");
            XmlNodeList efektif_satis = myxml.SelectNodes("/Tarih_Date/Currency/BanknoteSelling");

            // dataGridView1.= tarih.InnerText.ToString() + " tarihli Merkez Bankası kur bilgileri";
            List<ExchangeRate> exchangeRateList = new List<ExchangeRate>();
            int x = 19;
            int i;

            for (i = 0; i < x; i++)
            {
                var lookupData = LookupHelper.GetLookupByCode(LookupTypes.Currency, kod.Item(i).InnerText.ToString());
                if (lookupData != null)
                {
                    var lookupOrdinal = lookupData.Ordinal;
                    ExchangeRate exchangeRate = new ExchangeRate
                    {
                        RateDate = DateTime.Parse(tarih.InnerText.ToString()),
                        CurrencyType = lookupOrdinal,
                        Unit = unit.Item(i).InnerText.ToString(),
                        ForexBuying = decimal.Parse(doviz_alis.Item(i).InnerText.ToString().Replace('.',',')),
                        ForexSelling = decimal.Parse(doviz_satis.Item(i).InnerText.ToString().Replace('.', ',')),
                        BanknoteBuying = decimal.Parse(efektif_alis.Item(i).InnerText.ToString().Replace('.', ',')),
                        BanknoteSelling = decimal.Parse(efektif_satis.Item(i).InnerText.ToString().Replace('.', ',')),
                    };
                    exchangeRateList.Add(exchangeRate);
                    dr = dt.NewRow();
                    dr[0] = adi.Item(i).InnerText.ToString();
                    dr[1] = kod.Item(i).InnerText.ToString();

                    dr[2] = doviz_alis.Item(i).InnerText.ToString();
                    // Döviz Alış
                    dr[3] = doviz_satis.Item(i).InnerText.ToString();
                    // Döviz  Satış
                    dr[4] = efektif_alis.Item(i).InnerText.ToString();
                    // Efektif Alış
                    dr[5] = efektif_satis.Item(i).InnerText.ToString();
                    // Efektif Satış.
                    dt.Rows.Add(dr);
                }
            }
            var spResponse = new GenericRepository<ExchangeRate>().InsertSpExecute3(exchangeRateList);
            return dt;
        }
    }
}

