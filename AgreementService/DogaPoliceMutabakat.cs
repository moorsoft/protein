﻿using Protein.Common.Messaging;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.Protein.Util.Lib;
using Protein.Data.Repositories;
using Protein.WS.WS.Doga;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using static Protein.Common.Entities.ProteinEntities;

namespace AgreementService
{
    public partial class DogaPoliceMutabakat : Form
    {
        public DogaPoliceMutabakat()
        {
            InitializeComponent();
        }

        private void DogaPoliceMutabakat_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
               }

        private long ApiCodeIsValid(string apiCode, long CompanyCode = 0)
        {
            long id = 0;

            try
            {
                string ApiCodeCompany = ConfigurationManager.AppSettings["ApiCode" + CompanyCode];

                if (ApiCodeCompany == apiCode)
                    id = CompanyCode;

                //string whereCondition = (CompanyKod > 0 ? $"COMPANY_ID = {CompanyKod} AND " : "") + ($" KEY = '{Constants.AppKeys.ApiCode}' AND VALUE = '{apiCode}' AND SYSTEM_TYPE='{((int)SystemType.WS).ToString()}'");
                //V_CompanyParameter resultCompanyParam = new GenericRepository<V_CompanyParameter>().FindBy(whereCondition, orderby: "").FirstOrDefault();
                ////HttpContext.Current.Response.Write("\n_ApiCodeIsValid__" + apiCode + "__" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                //stopwatch.Stop();
                //reqTime.Add(whereCondition, stopwatch.Elapsed.ToString());
                //if (resultCompanyParam == null) return id;
                //if (resultCompanyParam.Value == apiCode)
                //    id = (long)resultCompanyParam.CompanyId;
            }
            catch
            {
                id = 0;
            }
            return id;
        }
    }
}
