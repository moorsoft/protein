﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Enums.Media
{
    public enum MediaObjectType
    {
        T_COMPANY_MEDIA = 0,
        T_CONTRACT_MEDIA=1,
        T_PACKAGE_MEDIA=2,
        T_PRODUCT_MEDIA=3,
        T_PROVIDER_MEDIA=4,
        T_SUBPRODUCT_MEDIA=5,
        T_CLAIM_MEDIA=6
    }
}
