﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Enums.Import
{
    public enum ImportObjectType
    {
        ProviderDoctor,
        Policy,
        Agency,
        AgencyCompany,
        ClaimStatus,
        PayrollStatus
    }
}
