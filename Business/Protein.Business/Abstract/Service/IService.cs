﻿using Protein.Business.Concrete.Responses;
using Protein.Common.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Abstract.Service
{
    public interface IService<T> where T : class,
                                 IImportObject,
                                 new()
    {
        ServiceResponse ServiceInsert(T input,string Token= "9999999999");
    }
}
