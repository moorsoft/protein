﻿using Protein.Business.Print;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Abstract.Print
{
    public interface IPrint<TInput>
    {
        PrintResponse DoWork(TInput input);
    }
}
