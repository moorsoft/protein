﻿using Protein.Business.Concrete.Responses;
using Protein.Business.Enums.Import;
using Protein.Common.Abstract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Abstract.Import
{
    public interface IImport
    {
        ImportResponse DoWorkAsync(Stream stream, ImportFileType fileType,
                               ref IDictionary<Guid, double> tasks, Guid taskId,
                               ref IDictionary<Guid, string> errorList, Int64 CompanyId = 0);
    }
}
