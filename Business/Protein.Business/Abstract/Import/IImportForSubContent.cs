﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using Protein.Business.Concrete.Responses;
using Protein.Business.Enums.Import;
using Protein.Common.Abstract;

namespace Protein.Business.Abstract.Import
{
    public interface IImportForSubContent
    {
        ImportResponse DoWork(Int64 Id,string formId,Stream stream, ImportFileType fileType,
                              ref IDictionary<Guid, double> tasks,Guid taskId,
                              ref IDictionary<Guid,string> errorList);
    }
}
