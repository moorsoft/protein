﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Protein.Business.Abstract.Print;
using Protein.Common.Enums;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Business.Print.PolicyInsuredExit
{
    public class MyPageHeader : PdfPageEventHelper
    {
        public override void OnStartPage(PdfWriter writer, Document document)
        {
            base.OnStartPage(writer, document);
            BaseFont STF_Helvetica_Turkish = BaseFont.CreateFont("Helvetica", "CP1254", BaseFont.NOT_EMBEDDED);
            var HeaderTable = new PdfPTable(1);
            HeaderTable.TotalWidth = document.Right - document.Left;
            HeaderTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            //HeaderTable.WidthPercentage = 100;

            Font fontHeader = new Font(STF_Helvetica_Turkish, 11, Font.BOLD);
            var Headercell = new PdfPCell(new Phrase("GRUP TAMAMLAYICI SAĞLIK SİGORTASI ÇIKIŞ ZEYLİ", fontHeader));
            Headercell.HorizontalAlignment = Element.ALIGN_CENTER;
            Headercell.Border = Rectangle.NO_BORDER;
            Headercell.Colspan = 1;
            Headercell.Padding = 10f;
            HeaderTable.AddCell(Headercell);
            document.Add(HeaderTable);
            //HeaderTable.WriteSelectedRows(0, -1, 150, document.Top, writer.DirectContent);
        }
    }
    public class PolicyInsuredExit : PrintUtility, IPrint<PolicyInsuredExitReq>
    {
        public PrintResponse DoWork(PolicyInsuredExitReq request)
        {
            PrintResponse response = new PrintResponse();
            try
            {
                string InsuredIdForCoverageList = "";

                #region FillPrintData

                V_FormPolicyEndorsement formPolicyEndorsement = new GenericRepository<V_FormPolicyEndorsement>().FindBy($"POLICY_ID = {request.PolicyId} and ENDORSEMENT_ID = {request.EndorsementId}", orderby: "",
                     fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();

                List<V_FormEndorsementPayment> formEndorsementPayment = new GenericRepository<V_FormEndorsementPayment>().FindBy($"POLICY_ID = {request.PolicyId} and ENDORSEMENT_ID = {request.EndorsementId}", orderby: "",
                    fetchDeletedRows: true, fetchHistoricRows: true);

                List<V_FormEndorsementInsured> formEndorsementInsured = new GenericRepository<V_FormEndorsementInsured>().FindBy($"POLICY_ID = {request.PolicyId} and ENDORSEMENT_ID = {request.EndorsementId}", orderby: "",
                    fetchDeletedRows: true, fetchHistoricRows: true);

                List<V_FormInsuredCoverage> formInsuredCoverage = new GenericRepository<V_FormInsuredCoverage>().FindBy($" ENDORSEMENT_ID = {request.EndorsementId}", orderby: "",
                    fetchDeletedRows: true, fetchHistoricRows: true);
                if (formInsuredCoverage.Count > 0) InsuredIdForCoverageList = formInsuredCoverage[0].INSURED_ID;
                #endregion
                string _path = "";
                if (request.IsWebRequest)
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints")))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints"));

                    _path = HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints") + "\\" + Guid.NewGuid() + ".pdf";
                }
                else { _path = request.LocalPath + "\\" + (!string.IsNullOrEmpty(request.FileName) ? request.FileName + ".pdf" : "SAGLIK_SIGORTASI_CIKIS_ZEYL_" + request.PolicyId + ".pdf"); };

                int cnt = 0;
                while (true)
                {
                    cnt++;
                    if (File.Exists(_path))
                    {
                        _path = _path.Replace(".pdf", "") + "_" + cnt.ToString() + ".pdf";
                    }
                    else break;
                }

                System.IO.FileStream fs = new FileStream(_path, FileMode.Create);
                Document document = new Document(PageSize.A4, 10, 10, 10, 20);

                PdfWriter writer = PdfWriter.GetInstance(document, fs);

                document.AddAuthor("TEST");
                document.AddCreator("TEST");
                document.AddKeywords("TEST");
                document.AddSubject("TEST");
                document.AddTitle("TEST");

                BaseFont STF_Helvetica_Turkish = BaseFont.CreateFont("Helvetica", "CP1254", BaseFont.NOT_EMBEDDED);
                #region Header

                writer.PageEvent = new MyPageHeader();
                document.Open();
                //Paragraph paragraphHeader = new Paragraph("GRUP TAMAMLAYICI SAĞLIK SİGORTASI POLİÇESİ", fontHeader);
                //paragraphHeader.Alignment = 1;
                //document.Add(paragraphHeader);
                //Paragraph ds = new Paragraph();
                //Phrase phr = new Phrase(Environment.NewLine);
                //ds.Add(phr);
                //document.Add(HeaderTable);

                #endregion
                #region FillDetails

                var mainTable = new PdfPTable(1);
                mainTable.WidthPercentage = 100;
                mainTable.DefaultCell.BorderWidth = 0.5f;

                var table = new PdfPTable(6);
                float[] widths = new float[] { 30f, 30f, 30f, 30f, 30f, 30f };
                table.SetWidths(widths);

                table.DefaultCell.HorizontalAlignment = 0;

                table.WidthPercentage = 100;
                //table.DefaultCell.Border = Rectangle.NO_BORDER;
                table.DefaultCell.Border = Rectangle.TOP_BORDER;

                Font fontBold = new Font(STF_Helvetica_Turkish, 7, Font.BOLD);
                Font fontNormal = new Font(STF_Helvetica_Turkish, 7, Font.NORMAL);

                #region PolicyHeader

                #region Row1
                PdfPCell cell = new PdfPCell(new Phrase("Acente No:", fontBold));
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_NO, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Acente Adı:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Colspan = 1;
                cell.Border = Rectangle.NO_BORDER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_NAME, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Colspan = 3;
                cell.Border = Rectangle.NO_BORDER;
                table.AddCell(cell);
                #endregion

                #region Row2
                cell = new PdfPCell(new Phrase("Telefon:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Colspan = 1;
                cell.Border = Rectangle.NO_BORDER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_PHONE, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Fax:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_FAX, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Email:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_EMAIL, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);
                #endregion

                #region Row3
                cell = new PdfPCell(new Phrase("Web Adresi:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_WEB_ADDRESS, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Tic. Sicil No:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_TAX_NO, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Levha No:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_PLATE_NO, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);
                #endregion

                #region Row4
                cell = new PdfPCell(new Phrase("Acente Adres:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_ADDRESS, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 5;
                table.AddCell(cell);
                #endregion

                #region PolicyInfo

                var mainTable2 = new PdfPTable(1);
                mainTable2.SpacingBefore = 5;
                mainTable2.WidthPercentage = 100;
                mainTable2.DefaultCell.BorderWidth = 0.5f;

                var table2 = new PdfPTable(9);
                float[] widths2 = new float[] { 30f, 30f, 30f, 30f, 30f, 30f, 30f, 30f, 30f };
                table2.SetWidths(widths2);

                table2.DefaultCell.HorizontalAlignment = 0;

                table2.WidthPercentage = 100;
                //table.DefaultCell.Border = Rectangle.NO_BORDER;
                table2.DefaultCell.Border = Rectangle.TOP_BORDER;

                #region Row1
                cell = new PdfPCell(new Phrase("Poliçe No:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Sagmer Tarife No:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Sagmer Tarife Ad:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Poliçe Başlangıç Tarihi:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Poliçe Bitiş Tarihi:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Poliçe Süresi:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Yeni İş Yenileme:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Zeyl Tanzim Tarihi:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Zeyl No", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                #endregion

                #region Row2
                cell = new PdfPCell(new Phrase(formPolicyEndorsement.POLICY_NO, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.SBM_NO, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.SBM_NO, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase(DateTime.Parse(formPolicyEndorsement.POLICY_START_DATE).ToString("dd.MM.yyyy"), fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase(DateTime.Parse(formPolicyEndorsement.POLICY_END_DATE).ToString("dd.MM.yyyy"), fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.POLICY_PERIOD, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.IS_RENEWAL, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase(DateTime.Parse(formPolicyEndorsement.ENDORSEMENT_DATE_OF_ISSUE).ToString("dd.MM.yyyy"), fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.ENDORSEMET_NO, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);
                #endregion

                #endregion
                #region InsurerInfo
                var mainTable3 = new PdfPTable(1);
                mainTable3.SpacingBefore = 5;
                mainTable3.WidthPercentage = 100;
                mainTable3.DefaultCell.BorderWidth = 0.5f;


                var table3 = new PdfPTable(6);
                float[] widths3 = new float[] { 30f, 30f, 30f, 30f, 30f, 30f };
                table3.SetWidths(widths3);

                table3.DefaultCell.HorizontalAlignment = 0;

                table3.WidthPercentage = 100;
                //table.DefaultCell.Border = Rectangle.NO_BORDER;
                table3.DefaultCell.Border = Rectangle.TOP_BORDER;

                #region Row1
                cell = new PdfPCell(new Phrase("Adı Soyadı / Ünvanı :", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table3.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.INSURER_NAME, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 5;
                table3.AddCell(cell);

                #endregion

                #region Row2

                cell = new PdfPCell(new Phrase("Telefon", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table3.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.INSURER_PHONE, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table3.AddCell(cell);


                cell = new PdfPCell(new Phrase("Vergi No:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table3.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.INSURER_TAX_NO, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table3.AddCell(cell);

                cell = new PdfPCell(new Phrase("Tckn / Ykn:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table3.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.INSURER_IDENTITY_NO, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table3.AddCell(cell);

                #endregion

                #region Row3

                cell = new PdfPCell(new Phrase("Adres:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table3.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.INSURER_ADDRESS, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 5;
                table3.AddCell(cell);

                #endregion

                #endregion

                #region PaymentInfo

                var mainTable4 = new PdfPTable(2);
                float[] widthsMain4 = new float[] { 30f, 30f };
                mainTable4.SetWidths(widthsMain4);

                mainTable4.SpacingBefore = 5;
                mainTable4.WidthPercentage = 100;
                mainTable4.DefaultCell.Border = Rectangle.NO_BORDER;

                var table4 = new PdfPTable(3);
                var table5 = new PdfPTable(1);

                float[] widths4 = new float[] { 30f, 30f, 30f };
                table4.SetWidths(widths4);

                table4.DefaultCell.HorizontalAlignment = 0;
                table5.DefaultCell.HorizontalAlignment = 2;

                table4.WidthPercentage = 60;
                table5.WidthPercentage = 20;
                //table.DefaultCell.Border = Rectangle.NO_BORDER;
                table4.DefaultCell.Border = Rectangle.TOP_BORDER;
                table5.DefaultCell.Border = Rectangle.TOP_BORDER;

                #region Row1 Header
                cell = new PdfPCell(new Phrase("Ödeme Tarihi :", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Ödeme Şekli :", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Tutar (TL) :", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table4.AddCell(cell);
                #endregion

                #region Rows  foreach

                int totalInstallment = 0;
                decimal totalAmount = 0;

                //cell = new PdfPCell(new Phrase("234234", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BorderWidth = 0.5f;
                //cell.Colspan = 1;
                //table4.AddCell(cell);

                //cell = new PdfPCell(new Phrase("wdfsdf", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BorderWidth = 0.5f;
                //cell.Colspan = 1;
                //table4.AddCell(cell);

                //cell = new PdfPCell(new Phrase("dsfsdf", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BorderWidth = 0.5f;
                //cell.Colspan = 1;
                //table4.AddCell(cell);

                //cell = new PdfPCell(new Phrase("234234", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BorderWidth = 0.5f;
                //cell.Colspan = 1;
                //table4.AddCell(cell);

                //cell = new PdfPCell(new Phrase("wdfsdf", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BorderWidth = 0.5f;
                //cell.Colspan = 1;
                //table4.AddCell(cell);

                //cell = new PdfPCell(new Phrase("dsfsdf", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BorderWidth = 0.5f;
                //cell.Colspan = 1;
                //table4.AddCell(cell);

                //cell = new PdfPCell(new Phrase("234234", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BorderWidth = 0.5f;
                //cell.Colspan = 1;
                //table4.AddCell(cell);

                //cell = new PdfPCell(new Phrase("wdfsdf", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BorderWidth = 0.5f;
                //cell.Colspan = 1;
                //table4.AddCell(cell);

                //cell = new PdfPCell(new Phrase("dsfsdf", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BorderWidth = 0.5f;
                //cell.Colspan = 1;
                //table4.AddCell(cell);

                foreach (var itemPayment in formEndorsementPayment)
                {
                    totalInstallment++;
                    totalAmount += decimal.Parse(itemPayment.PAYMENT_AMOUNT);

                    cell = new PdfPCell(new Phrase(DateTime.Parse(itemPayment.PAYMENT_DATE).ToString("dd.MM.yyyy"), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidth = 0.5f;
                    cell.Colspan = 1;
                    table4.AddCell(cell);

                    cell = new PdfPCell(new Phrase(itemPayment.PAYMENT_TYPE, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidth = 0.5f;
                    cell.Colspan = 1;
                    table4.AddCell(cell);

                    cell = new PdfPCell(new Phrase(itemPayment.PAYMENT_AMOUNT, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidth = 0.5f;
                    cell.Colspan = 1;
                    table4.AddCell(cell);
                }
                #endregion

                #region TotalPaymentCol
                cell = new PdfPCell(new Phrase("Toplam", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table5.AddCell(cell);

                cell = new PdfPCell(new Phrase(totalAmount.ToString() + " TL", fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table5.AddCell(cell);

                cell = new PdfPCell(new Phrase(totalInstallment.ToString() + " TAKSİT", fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table5.AddCell(cell);
                #endregion

                #endregion

                #endregion
                #region Insured
                var mainTable5 = new PdfPTable(9);
                float[] widthsMain5 = new float[] { 30f, 30f, 30f, 30f, 30f, 30f, 30f, 30f, 30f };
                mainTable5.SetWidths(widthsMain5);

                mainTable5.SpacingBefore = 5;
                mainTable5.DefaultCell.HorizontalAlignment = 0;
                mainTable5.WidthPercentage = 100;
                mainTable5.DefaultCell.Border = Rectangle.TOP_BORDER;

                #region Row
                foreach (var Insured in formEndorsementInsured)
                {
                    #region Header
                    cell = new PdfPCell(new Phrase("Sigortalı No", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthTop = 0.5f;
                    cell.BorderWidthLeft = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Adı Soyadı", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthLeft = 0;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Yakınlık", fontBold));
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthLeft = 0;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Doğum Tarihi", fontBold));
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthLeft = 0;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Tckn", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Colspan = 1;
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthLeft = 0;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Cinsiyet", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthBottom = 0;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Plan Adı", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthBottom = 0;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);


                    cell = new PdfPCell(new Phrase("İlk Kayıt Tarihi", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.BorderWidthLeft = 0;
                    cell.Colspan = 1;
                    cell.BorderWidthBottom = 0;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Prim (TL)", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.BorderWidthLeft = 0;
                    cell.Colspan = 1; cell.BorderWidthBottom = 0;
                    cell.BorderWidthRight = 0;
                    mainTable5.AddCell(cell);
                    #endregion
                    #region Value
                    cell = new PdfPCell(new Phrase(Insured.REGISTRATION_NO, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthLeft = 0.5f;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Insured.NAME_SURNAME, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(((ProteinEnums.IndividualType)int.Parse(Insured.INDIVIDUAL_TYPE)).ToString(), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(DateTime.Parse(Insured.BIRTHDATE).ToString("dd.MM.yyyy"), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Insured.IDENTITY_NO, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Insured.GENDER, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Insured.PLAN_NAME, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(DateTime.Parse(Insured.FIRST_INSURED_DATE).ToString("dd.MM.yyyy"), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Insured.PREMIUM, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.BorderWidthLeft = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Sigortalı Prim Bilgileri Detayı", fontBold));
                    cell.PaddingTop = 10;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthLeft = 0.5f;
                    cell.Colspan = 9;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Yıllık Prim", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthLeft = 0.5f;
                    cell.BorderWidthTop = 0;
                    cell.BorderWidthRight = 0;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Insured.ANNUAL_PREMIUM, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthTop = 0;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthRight = 0;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("İndirim / Ek Prim Açıklama ", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthTop = 0;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthRight = 0;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Insured.EXTRA_PREMIUM, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthTop = 0;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthRight = 0;
                    cell.Colspan = 6;
                    mainTable5.AddCell(cell);
                    #endregion
                }
                //#region Header
                //cell = new PdfPCell(new Phrase("Sigortalı No", fontBold));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BorderWidthBottom = 0;
                //cell.BorderWidthTop = 0.5f;
                //cell.BorderWidthLeft = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Adı Soyadı", fontBold));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BorderWidthBottom = 0;
                //cell.BorderWidthLeft = 0;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Yakınlık", fontBold));
                //cell.BorderWidthBottom = 0;
                //cell.BorderWidthLeft = 0;
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Doğum Tarihi", fontBold));
                //cell.BorderWidthBottom = 0;
                //cell.BorderWidthLeft = 0;
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Tckn", fontBold));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Colspan = 1;
                //cell.BorderWidthBottom = 0;
                //cell.BorderWidthLeft = 0;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Cinsiyet", fontBold));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BorderWidthLeft = 0;
                //cell.BorderWidthBottom = 0;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Plan Adı", fontBold));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BorderWidthLeft = 0;
                //cell.BorderWidthBottom = 0;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);


                //cell = new PdfPCell(new Phrase("İlk Kayıt Tarihi", fontBold));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.BorderWidthLeft = 0;
                //cell.Colspan = 1;
                //cell.BorderWidthBottom = 0;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Prim (TL)", fontBold));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.BorderWidthLeft = 0;
                //cell.Colspan = 1; cell.BorderWidthBottom = 0;
                //cell.BorderWidthRight = 0;
                //mainTable5.AddCell(cell);
                //#endregion
                //#region Value
                //cell = new PdfPCell(new Phrase("adfsdf324", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.BorderWidthLeft = 0.5f;
                //cell.BorderWidthBottom = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("dsfsdfsdf", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.BorderWidthBottom = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("fdsfsdfsd", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.BorderWidthBottom = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("dsfsdfsdf", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.BorderWidthBottom = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("fdsf872163", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.BorderWidthBottom = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("fdsfsd7f6ye", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.BorderWidthBottom = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("fdsf27934", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.BorderWidthBottom = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("dffıut3o4423", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.BorderWidthBottom = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("324230784tf", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.BorderWidthBottom = 0.5f;
                //cell.BorderWidthLeft = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Sigortalı Prim Bilgileri Detayı", fontBold));
                //cell.PaddingTop = 10;
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.NO_BORDER;
                //cell.BorderWidthLeft = 0.5f;
                //cell.Colspan = 9;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Yıllık Prim", fontBold));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BorderWidthLeft = 0.5f;
                //cell.BorderWidthTop = 0;
                //cell.BorderWidthRight = 0;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Yıllık Prim tutarı", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BorderWidthTop = 0;
                //cell.BorderWidthLeft = 0;
                //cell.BorderWidthRight = 0;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("İndirim / Ek Prim Açıklama ", fontBold));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BorderWidthTop = 0;
                //cell.BorderWidthLeft = 0;
                //cell.BorderWidthRight = 0;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("İndirim / Ek Prim Açıklama  text", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BorderWidthTop = 0;
                //cell.BorderWidthLeft = 0;
                //cell.BorderWidthRight = 0;
                //cell.Colspan = 6;
                //mainTable5.AddCell(cell);
                //#endregion

                //#region Header
                //cell = new PdfPCell(new Phrase("Sigortalı No", fontBold));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.BorderWidthLeft = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Adı Soyadı", fontBold));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Yakınlık", fontBold));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Doğum Tarihi", fontBold));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Tckn", fontBold));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Cinsiyet", fontBold));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Plan Adı", fontBold));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);


                //cell = new PdfPCell(new Phrase("İlk Kayıt Tarihi", fontBold));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Prim (TL)", fontBold));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);
                //#endregion
                //#region Value
                //cell = new PdfPCell(new Phrase("adfsdf324", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.BorderWidthLeft = 0.5f;
                //cell.BorderWidthBottom = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("dsfsdfsdf", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.BorderWidthBottom = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("fdsfsdfsd", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.BorderWidthBottom = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("dsfsdfsdf", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.BorderWidthBottom = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("fdsf872163", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.BorderWidthBottom = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("fdsfsd7f6ye", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.BorderWidthBottom = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("fdsf27934", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.BorderWidthBottom = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("dffıut3o4423", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.BorderWidthBottom = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);

                //cell = new PdfPCell(new Phrase("324230784tf", fontNormal));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.Border = Rectangle.TOP_BORDER;
                //cell.BorderWidthBottom = 0.5f;
                //cell.Colspan = 1;
                //mainTable5.AddCell(cell);
                //#endregion

                //foreach (var itemInsured in formEndorsementInsured)
                //{
                //    #region Header
                //    cell = new PdfPCell(new Phrase("Sigortalı No", fontBold));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.Border = Rectangle.TOP_BORDER;
                //    cell.BorderWidthLeft = 0.5f;
                //    cell.Colspan = 1;
                //    mainTable5.AddCell(cell);

                //    cell = new PdfPCell(new Phrase("Adı Soyadı", fontBold));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.Border = Rectangle.TOP_BORDER;
                //    cell.Colspan = 1;
                //    mainTable5.AddCell(cell);

                //    cell = new PdfPCell(new Phrase("Yakınlık", fontBold));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.Border = Rectangle.TOP_BORDER;
                //    cell.Colspan = 1;
                //    mainTable5.AddCell(cell);

                //    cell = new PdfPCell(new Phrase("Doğum Tarihi", fontBold));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.Border = Rectangle.TOP_BORDER;
                //    cell.Colspan = 1;
                //    mainTable5.AddCell(cell);

                //    cell = new PdfPCell(new Phrase("Tckn", fontBold));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.Border = Rectangle.TOP_BORDER;
                //    cell.Colspan = 1;
                //    mainTable5.AddCell(cell);

                //    cell = new PdfPCell(new Phrase("Cinsiyet", fontBold));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.Border = Rectangle.TOP_BORDER;
                //    cell.Colspan = 1;
                //    mainTable5.AddCell(cell);

                //    cell = new PdfPCell(new Phrase("Plan Adı", fontBold));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.Border = Rectangle.TOP_BORDER;
                //    cell.Colspan = 1;
                //    mainTable5.AddCell(cell);


                //    cell = new PdfPCell(new Phrase("İlk Kayıt Tarihi", fontBold));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.Border = Rectangle.TOP_BORDER;
                //    cell.Colspan = 1;
                //    mainTable5.AddCell(cell);

                //    cell = new PdfPCell(new Phrase("Prim (TL)", fontBold));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.Border = Rectangle.TOP_BORDER;
                //    cell.Colspan = 1;
                //    mainTable5.AddCell(cell);
                //    #endregion
                //    #region Value
                //    cell = new PdfPCell(new Phrase(itemInsured.REGISTRATION_NO, fontNormal));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.Border = Rectangle.TOP_BORDER;
                //    cell.BorderWidthLeft = 0.5f;
                //    cell.BorderWidthBottom = 0.5f;
                //    cell.Colspan = 1;
                //    mainTable5.AddCell(cell);

                //    cell = new PdfPCell(new Phrase(itemInsured.NAME_SURNAME, fontNormal));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.Border = Rectangle.TOP_BORDER;
                //    cell.BorderWidthBottom = 0.5f;
                //    cell.Colspan = 1;
                //    mainTable5.AddCell(cell);

                //    cell = new PdfPCell(new Phrase(itemInsured.INDIVIDUAL_TYPE, fontNormal));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.Border = Rectangle.TOP_BORDER;
                //    cell.BorderWidthBottom = 0.5f;
                //    cell.Colspan = 1;
                //    mainTable5.AddCell(cell);

                //    cell = new PdfPCell(new Phrase(itemInsured.BIRTHDATE, fontNormal));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.Border = Rectangle.TOP_BORDER;
                //    cell.BorderWidthBottom = 0.5f;
                //    cell.Colspan = 1;
                //    mainTable5.AddCell(cell);

                //    cell = new PdfPCell(new Phrase(itemInsured.IDENTITY_NO, fontNormal));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.Border = Rectangle.TOP_BORDER;
                //    cell.BorderWidthBottom = 0.5f;
                //    cell.Colspan = 1;
                //    mainTable5.AddCell(cell);

                //    cell = new PdfPCell(new Phrase(itemInsured.GENDER, fontNormal));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.Border = Rectangle.TOP_BORDER;
                //    cell.BorderWidthBottom = 0.5f;
                //    cell.Colspan = 1;
                //    mainTable5.AddCell(cell);

                //    cell = new PdfPCell(new Phrase(itemInsured.PLAN_NAME, fontNormal));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.Border = Rectangle.TOP_BORDER;
                //    cell.BorderWidthBottom = 0.5f;
                //    cell.Colspan = 1;
                //    mainTable5.AddCell(cell);

                //    cell = new PdfPCell(new Phrase(itemInsured.FIRST_INSURED_DATE, fontNormal));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.Border = Rectangle.TOP_BORDER;
                //    cell.BorderWidthBottom = 0.5f;
                //    cell.Colspan = 1;
                //    mainTable5.AddCell(cell);

                //    cell = new PdfPCell(new Phrase(itemInsured.PREMIUM, fontNormal));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.Border = Rectangle.TOP_BORDER;
                //    cell.BorderWidthBottom = 0.5f;
                //    cell.Colspan = 1;
                //    mainTable5.AddCell(cell);
                //    #endregion
                //}
                #endregion

                #endregion
                #region Coverage Table

                var mainTable6 = new PdfPTable(8);
                float[] widthsMain6 = new float[] { 30f, 30f, 30f, 30f, 30f, 30f, 30f, 30f };
                mainTable6.SetWidths(widthsMain6);

                mainTable6.SpacingBefore = 5;
                mainTable6.DefaultCell.HorizontalAlignment = 0;
                mainTable6.WidthPercentage = 100;
                mainTable6.DefaultCell.Border = Rectangle.TOP_BORDER;

                #region Header

                #region Row1
                if (formInsuredCoverage != null && formInsuredCoverage.Count > 0)
                {
                    cell = new PdfPCell(new Phrase("Seçilen Plan", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthBottom = 0.5f;
                    cell.BorderWidthTop = 0.5f;
                    cell.BorderWidthLeft = 0.5f;
                    cell.Colspan = 1;
                    mainTable6.AddCell(cell);


                    cell = new PdfPCell(new Phrase(formInsuredCoverage[0].PACKAGE_NAME, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthBottom = 0.5f;
                    cell.BorderWidthTop = 0.5f;
                    cell.BorderWidthLeft = 0.5f;
                    cell.Colspan = 7;
                    mainTable6.AddCell(cell);
                }
                #endregion

                #region Row2

                cell = new PdfPCell(new Phrase("", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthBottom = 0.5f;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0.5f;
                cell.Colspan = 2;
                mainTable6.AddCell(cell);

                cell = new PdfPCell(new Phrase("Anlaşmalı Network ÖSS", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthBottom = 0.5f;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.Colspan = 3;
                mainTable6.AddCell(cell);

                cell = new PdfPCell(new Phrase("Anlaşmasız Network ÖSS", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthBottom = 0.5f;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.Colspan = 3;
                mainTable6.AddCell(cell);

                #endregion

                #region Row3

                cell = new PdfPCell(new Phrase("Teminat Adı", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthBottom = 0.5f;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0.5f;
                cell.Colspan = 2;
                mainTable6.AddCell(cell);

                cell = new PdfPCell(new Phrase("Uygulama", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidthBottom = 0.5f;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.Colspan = 1;
                mainTable6.AddCell(cell);

                cell = new PdfPCell(new Phrase("Yıllık Limit", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidthBottom = 0.5f;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.Colspan = 1;
                mainTable6.AddCell(cell);

                cell = new PdfPCell(new Phrase("Katılım", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidthBottom = 0.5f;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0.5f;
                cell.Colspan = 1;
                mainTable6.AddCell(cell);

                cell = new PdfPCell(new Phrase("Uygulama", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidthBottom = 0.5f;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.Colspan = 1;
                mainTable6.AddCell(cell);

                cell = new PdfPCell(new Phrase("Yıllık Limit", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidthBottom = 0.5f;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.Colspan = 1;
                mainTable6.AddCell(cell);

                cell = new PdfPCell(new Phrase("Katılım", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidthBottom = 0.5f;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.Colspan = 1;
                mainTable6.AddCell(cell);
                #endregion

                #region CoverageRows

                formInsuredCoverage = formInsuredCoverage.Where(x => x.INSURED_ID == InsuredIdForCoverageList).ToList();
                List<long> CoverageIDs = new List<long>();
                foreach (var MainCoverage in formInsuredCoverage.Where(x => x.IS_MAIN_COVERAGE == "1"))
                {
                    foreach (var coverageItem in formInsuredCoverage.Where(x => x.MAIN_COVERAGE_ID == MainCoverage.COVERAGE_ID || x.COVERAGE_ID == MainCoverage.COVERAGE_ID).OrderBy(x => x.COVERAGE_NAME).OrderByDescending(x => int.Parse(x.IS_MAIN_COVERAGE)))
                    {
                        if (CoverageIDs.Contains(long.Parse(coverageItem.COVERAGE_ID)))
                            continue;

                        CoverageIDs.Add(long.Parse(coverageItem.COVERAGE_ID));
                        Font font;

                        font = coverageItem.IS_MAIN_COVERAGE == "1" ? fontBold : fontNormal;

                        cell = new PdfPCell(new Phrase(coverageItem.COVERAGE_NAME, font));
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        cell.BorderWidthBottom = 0.5f;
                        cell.Padding = 5f;
                        cell.BorderWidthTop = 0;
                        cell.BorderWidthLeft = 0.5f;
                        cell.BorderWidthRight = 0;
                        cell.Colspan = 2;
                        mainTable6.AddCell(cell);

                        string aggrType = string.IsNullOrEmpty(coverageItem.AGR_AGREEMENT_TYPE) ? "" : ((ProteinEnums.AgreementType)int.Parse(coverageItem.AGR_AGREEMENT_TYPE)).ToString();

                        cell = new PdfPCell(new Phrase(aggrType, font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.BorderWidthBottom = 0.5f;
                        cell.BorderWidthTop = 0;
                        cell.BorderWidthLeft = 0;
                        cell.BorderWidthRight = 0;
                        cell.Colspan = 1;
                        mainTable6.AddCell(cell);

                        cell = new PdfPCell(new Phrase(coverageItem.AGREED_NETWORK_LIMIT == "9999999" ? "LİMİTSİZ" : coverageItem.AGREED_NETWORK_LIMIT, font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.BorderWidthBottom = 0.5f;
                        cell.BorderWidthTop = 0;
                        cell.BorderWidthLeft = 0;
                        cell.BorderWidthRight = 0;
                        cell.Colspan = 1;
                        mainTable6.AddCell(cell);

                        cell = new PdfPCell(new Phrase(coverageItem.AGREED_NETWORK_COINSURANCE, font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.BorderWidthBottom = 0.5f;
                        cell.BorderWidthTop = 0;
                        cell.BorderWidthLeft = 0;
                        cell.BorderWidthRight = 0;
                        cell.Colspan = 1;
                        mainTable6.AddCell(cell);

                        string nagType = string.IsNullOrEmpty(coverageItem.NOT_AGREEMENT_TYPE) ? "" : ((ProteinEnums.AgreementType)int.Parse(coverageItem.NOT_AGREEMENT_TYPE)).ToString();

                        cell = new PdfPCell(new Phrase(nagType, font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.BorderWidthBottom = 0.5f;
                        cell.BorderWidthTop = 0;
                        cell.BorderWidthLeft = 0;
                        cell.BorderWidthRight = 0;
                        cell.Colspan = 1;
                        mainTable6.AddCell(cell);

                        cell = new PdfPCell(new Phrase(coverageItem.NOT_AGREED_NETWORK_LIMIT == "9999999" ? "LİMİTSİZ" : coverageItem.NOT_AGREED_NETWORK_LIMIT, font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.BorderWidthBottom = 0.5f;
                        cell.BorderWidthTop = 0;
                        cell.BorderWidthLeft = 0;
                        cell.BorderWidthRight = 0;
                        cell.Colspan = 1;
                        mainTable6.AddCell(cell);

                        cell = new PdfPCell(new Phrase(coverageItem.NOT_AGREED_NETWORK_COINSURANCE, font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.BorderWidthBottom = 0.5f;
                        cell.BorderWidthTop = 0;
                        cell.BorderWidthLeft = 0;
                        cell.BorderWidthRight = 0.5f;
                        cell.Colspan = 1;
                        mainTable6.AddCell(cell);
                    }
                }
                //foreach (var coverageItem in formInsuredCoverage.Where(x => x.INSURED_ID == InsuredIdForCoverageList))
                //{

                //}
                #endregion

                #endregion

                #endregion
                #region CoverageInfo

                var mainTable7 = new PdfPTable(1);
                mainTable7.DefaultCell.HorizontalAlignment = 0;
                mainTable7.WidthPercentage = 100;
                mainTable7.DefaultCell.Border = Rectangle.TOP_BORDER;

                Paragraph parag = new Paragraph();
                Phrase phrase = new Phrase(@"1) Doktor Muayene, tahlil-röntgen, İleri Tanı Yöntemleri ve fizik tedaviye ilişkin giderler Ayakta Tedaviler kapsamında değerlendirilir. Ayakta Tedaviler kapsamında sadece Doktor Muayene tek başına kullanılabilir. Doktor muayenesi ile ilişkisi olmadan gerçekleşen tetkikler ve fizik tedaviler kapsam dışıdır. Poliçe dönemi içinde
ayakta tedavi teminatları yıllık maksimum 6 kez kullanım ile sınırlıdır.Fizik tedavi teminatı doktor muayene teminatı ile birlikte kullanılması durumunda poliçe döneminde maksimum 30 seans ile sınırlıdır.
", fontNormal);
                parag.Add(phrase);
                phrase = new Phrase(@"2) Anlaşmalı kurum bulunmayan illerde SGK kullanılarak gerçekleşen yatarak tedavi sağlık gideri fark ücretleri poliçe özel ve genel şartlarına uygun olması şartı  ile teminat limit ve ödeme oranı doğrultusunda 1 SUT ile limitli olarak ödenecektir. Ayrıca hayati bir tehlikeye neden olabilecek acil durumlarda anlaşma harici kurumlarda SGK kullanılarak gerçekleşen yatarak tedavi sağlık gideri fark ücretleri de Genel Sağlık Sigortası 'İlave Ücret Alınmayacak Sağlık Hizmetleri' maddesinde
belirtilen sağlık hizmetleri dışında, poliçe özel ve genel şartlarına uygun olması şartı ile teminat limit ve ödeme oranı doğrultusunda 1 SUT ile limitli olarak ödenecektir.
", fontNormal);
                parag.Add(phrase);

                phrase = new Phrase("Açıklamalar", fontNormal);
                parag.Add(phrase);
                parag.Add(Environment.NewLine);
                phrase = new Phrase(@"1) Sigortalının poliçe başlangıç tarihinden önce var olan şikayet ve hastalıkları ile ilgili her türlü sağlık harcamaları poliçe kapsamı dışındadır.", fontNormal);
                parag.Add(phrase);
                parag.Add(Environment.NewLine);
                phrase = new Phrase(@"2) Tamamlayıcı Sağlık Sigortası anlaşmalı kurum listesine www.dogasigorta.com adresinden ulaşılabilmektedir.", fontNormal);
                parag.Add(phrase);
                parag.Add(Environment.NewLine);
                phrase = new Phrase(@"3) Tamamlayıcı Sağlık Sigortası, hem SGK hem de ile anlaşmalı kurumlarda geçerli olup, Anlaşmasız kurumlardaki yatarak tedavi giderlerinin ödeme şartları ayrıca özel şartlarda tanımlıdır.", fontNormal);
                parag.Add(phrase);
                parag.Add(Environment.NewLine);
                phrase = new Phrase(@"4) Bu poliçe sadece Türkiye Cumhuriyeti sınırları içinde geçerli olup, yurtdışında gerçekleşen (K.K.T.C dahil) her türlü sağlık gideri poliçe kapsamı dışındadı", fontNormal);
                parag.Add(phrase);
                parag.Add(Environment.NewLine);
                parag.Add(Environment.NewLine);
                //ds.Add(phr);

                cell = new PdfPCell();
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthBottom = 0.5f;
                cell.BorderWidthTop = 0.5f;
                cell.BorderWidthLeft = 0.5f;
                cell.Colspan = 1;
                cell.AddElement(parag);
                mainTable7.AddCell(cell);


                #endregion
                #region InsuredInfo
                Font fontInsuredInfoBold = new Font(STF_Helvetica_Turkish, 9, Font.BOLD);
                Font fontInsuredInfoNormal = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL);

                var mainTable8 = new PdfPTable(1);
                mainTable8.DefaultCell.HorizontalAlignment = 0;
                mainTable8.WidthPercentage = 100;
                mainTable8.DefaultCell.Border = Rectangle.NO_BORDER;

                parag = new Paragraph();
                phrase = new Phrase(@"* Axa Sigorta geçiş olan sigortalıların ilk kayıt tarihleri korunmuştur", fontInsuredInfoNormal);
                parag.Add(phrase);
                parag.Add(Environment.NewLine);
                phrase = new Phrase(@"* Grubun aktif çalışanı ve/veya bağımlısı olmaması (yani gruptan çıkışı söz konusu oldu ise) durumunda, yenileme garantisi olsun, olmasın risk analizi yapılarak ferdi poliçeye alınacaktır.", fontInsuredInfoNormal);
                parag.Add(phrase);
                parag.Add(Environment.NewLine);
                phrase = new Phrase(@"7/24 Provizyon, Ambulans ve Tıbbi Danışma Hattı: İMECE DESTEK DANIŞMANLIK HİZMETLERİ A.Ş. Tel : 0 212 978 14 64 Faks : 0 212 978 14 66", fontBold);
                parag.Add(phrase);

                cell = new PdfPCell();
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.Colspan = 1;
                cell.AddElement(parag);
                mainTable8.AddCell(cell);


                #endregion

                #region Descriptions
                var mainTable9 = new PdfPTable(1);
                mainTable9.DefaultCell.HorizontalAlignment = 0;
                mainTable9.WidthPercentage = 100;
                mainTable9.PaddingTop = 10;
                mainTable9.DefaultCell.Border = Rectangle.NO_BORDER;

                parag = new Paragraph();
                parag.SetLeading(10, 0);
                phrase = new Phrase(@" Açıklamalar", fontBold);
                parag.Add(phrase);
                parag.Add(Environment.NewLine);
                parag.Add(Environment.NewLine);
                phrase = new Phrase(@"1) S.S.Doğa Sigorta Kooperatifi, Sigortalının / Sigorta Ettirenin başvuru formundaki beyanını esas alarak Türk Ticaret Kanunu hükümlerine  ve poliçenin ayrılmaz parçaları olan Sağlık Sigortası Genel ve Özel Şartlara bağlı kalarak bu sigortayı kabul ve poliçeyi tanzim etmiştir. Sağlık Genel Şartları ve teminatların açıklandığı Poliçe Kitapçığı ekte teslim edilmiştir.", fontBold);
                parag.Add(phrase);
                parag.Add(Environment.NewLine);
                parag.Add(Environment.NewLine);
                phrase = new Phrase(@"2) Poliçe teslim edilmiş olsa dahi, Sigortacının sorumluluğu; sigorta priminin peşin ödenmesi kararlaştırılmış ise tamamının, taksitle ödenmesi kararlaştırılmış ise peşinatın (ilk taksitin) ödenmesi ile başlar. Sigorta ettiren / Sigortalı prim peşinat ve taksitlerini poliçede belirtilen kesin vadelerle demekle yükümlüdür. Prim peşinat ve taksitlerden herhangi biri vadesinde ödenmemiş ise Sigorta ettiren / Sigortalı temerrüde düşer. Temerrüde düşülmesi halinde Borçlar Kanunu hükümleri uygulanır ve Borçlar Kanunu'nun 107.maddesinin 3. bendi uyarınca herhangi bir mehile ve ihtara gerek kalmaksızın sigorta sözleşmesinin derhal feshedilmiş olacağı taraflarca karşılıklı olarak kararlaştırılmıştır. Poliçede primlerin vadelerine ilişkin  bölümde belirtilen tarihler taraflarca müttefikan tayin edilmiş kesin vadeler olup, ödemelerin en geç bu tarihlerde ve mutlak sürette yapılması gerektiği konusunda taraflar karşılıklı olarak mutabakata varmışlardır. Rizikonun gerçekleşmesiyle henüz vadesi gelmemiş prim taksitlerinin Sigortacının ödemekle yükümlü olduğu tazminat miktarını aşmayan kısmı muaccel hale gelir.", fontBold);
                parag.Add(phrase);
                parag.Add(Environment.NewLine);
                parag.Add(Environment.NewLine);
                phrase = new Phrase(@"3) Sigorta Ettiren / Sigortalı'nın işbu sözleşmede yazılı adresi tebligat adresi olarak kabul edilmiş olup, Sigortacı'nın işbu sözleşmeye ilişkin olarak yapacağı her türlü bildirimin bu adrese yapılacağını taraflar kabul etmiştir.", fontBold);
                parag.Add(phrase);
                parag.Add(Environment.NewLine);
                parag.Add(Environment.NewLine);
                phrase = new Phrase(@"4) Sigortalılar tarafından başvuru aşamasında beyan edilmiş beyana konu olan sağlık bilgilerinin, Sağlık Sigortaları Bilgi ve Gözetim Merkezi (SAGMER) tarafından talep edilmesi halinde, Sağlık Sigortaları Bilgi ve Gözetim Merkezi'ne (SAGMER) Doğa Sigorta Kooperatifi tarafından iletilecek olup, gerektiğinde SAGMER'den bu bilgilerin temin edilmesi mümkün olacaktır.", fontBold);
                parag.Add(phrase);
                parag.Add(Environment.NewLine); parag.Add(Environment.NewLine);
                phrase = new Phrase(@"5) Sigorta Şirketi, sağlık sigorta sözleşmesinin düzenlenmesi aşamasında sigortalılardan almış olduğu her türlü bilgiyi (hasar (tazminat), teminat detayları, kişisel bilgiler vb.) yasal mevzuat gereğince Sigorta Bilgi Merkezi, Hazine Müsteşarlığı ,SAGMER ve talep edilmesi halinde her türlü devlet kurumuna ibraz etmekle yükümlüdür. Sağlık Sigortası satın alan her kişi bu bilgilerin resmi kurumlara ibraz edilmesini peşinen kabul etmektedir.", fontBold);
                parag.Add(phrase);
                parag.Add(Environment.NewLine); parag.Add(Environment.NewLine);
                phrase = new Phrase(@"6) S.S.Doğa Sigorta Kooperatifi sigortalının yazılı onayını alınması sonrası ve / veya beyan formunda yer alan onayına istinaden başka sigorta şirketine geçiş yapmak istemesi halinde sağlık profiline ilişkin geçiş bilgilerini ilgili Sigorta Şirketine gönderir.", fontBold);
                parag.Add(phrase);
                parag.Add(Environment.NewLine); parag.Add(Environment.NewLine);
                phrase = new Phrase(@"7) Poliçe özel şartlarında, bekleme sürelerinde, teminat dışı kalan hallerde, teminatlarında, teminat limitlerinde ve primlerinde Sigorta Şirketi tarafından değişiklik yapılabilir. Bu değişiklikler her bir sigortalı için poliçenin yenileme tarihinden itibaren geçerli olur.", fontBold);
                parag.Add(phrase);
                parag.Add(Environment.NewLine); parag.Add(Environment.NewLine);
                phrase = new Phrase(@"8) Madde: Sigorta Primleriniz; kişi bazında yaş, cinsiyet, ikamet adresi, sigorta süresi, geçmiş dönem kullanım oranı (sağlık gideri/prim oranı), teminat yapısı, teminat limiti ve ülkedeki sağlık enflasyonu, komisyon, genel giderler gibi kriterler dikkate alınarak belirlenmektedir.", fontBold);
                parag.Add(phrase);
                parag.Add(Environment.NewLine);


                cell = new PdfPCell();
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.Colspan = 1;
                cell.AddElement(parag);
                mainTable9.AddCell(cell);

                #endregion
                #region Signatures

                var mainTable10 = new PdfPTable(3);
                float[] widths10 = new float[] { 30f, 30f, 30f };
                mainTable10.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                mainTable10.WidthPercentage = 100;
                mainTable10.PaddingTop = 10;
                mainTable10.DefaultCell.Border = Rectangle.NO_BORDER;
                mainTable10.SpacingBefore = 20f;

                cell = new PdfPCell(new Phrase("SİGORTA ETTİREN", fontInsuredInfoBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.Colspan = 1;
                mainTable10.AddCell(cell);

                cell = new PdfPCell(new Phrase("ACENTE", fontInsuredInfoBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.Colspan = 1;
                mainTable10.AddCell(cell);

                cell = new PdfPCell(new Phrase("SİGORTACI", fontInsuredInfoBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.Colspan = 1;
                mainTable10.AddCell(cell);


                cell = new PdfPCell(new Phrase(formPolicyEndorsement.INSURER_NAME, fontInsuredInfoBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.Colspan = 1;
                mainTable10.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_NAME, fontInsuredInfoBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.Colspan = 1;
                mainTable10.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.COMPANY_NAME, fontInsuredInfoBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.Colspan = 1;
                mainTable10.AddCell(cell);

                #endregion

                #region SetDocument
                mainTable.AddCell(table);
                document.Add(mainTable);
                mainTable2.AddCell(table2);
                document.Add(mainTable2);
                document.Add(new Paragraph("Sigorta Ettiren", fontBold));
                mainTable3.AddCell(table3);
                document.Add(mainTable3);
                document.Add(new Paragraph("Ödeme Planları", fontBold));
                mainTable4.AddCell(table4);
                mainTable4.AddCell(table5);
                document.Add(mainTable4);
                document.Add(new Paragraph("POLİÇE KAPSAMINDA OLAN SİGORTALI / SİGORTALILAR", fontBold));
                document.Add(mainTable5);
                document.Add(new Paragraph("TEMİNAT TABLOSU", fontBold));
                document.Add(mainTable6);
                document.Add(mainTable7);
                document.Add(mainTable8);
                document.Add(mainTable9);
                document.Add(mainTable10);

                #endregion

                #endregion
                //table.SplitLate'e bak
                document.Close();
                writer.Close();
                fs.Close();

                if (request.IsWebRequest)
                {
                    request.FileName = !string.IsNullOrEmpty(request.FileName) ? request.FileName : ("SAGLIK_SIGORTASI_CIKIS_ZEYL_");
                    PrintOut(_path, request.FileName);
                }
                response.Url = _path;
                response.FileName = request.FileName;
                response.Code = "100";
            }
            catch (Exception ex) { response.Code = "999"; response.Message = ex.Message; }
            return response;
        }
    }
}
