﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Print.PolicyInsuredExit
{
    public class PolicyInsuredExitReq : CommonReq
    {
        public long PolicyId { get; set; }
        public long EndorsementId { get; set; }
    }
}
