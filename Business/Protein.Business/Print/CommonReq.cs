﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Print
{
    public class CommonReq
    {
        public string FileName { get; set; }
        public bool IsWebRequest { get; set; } = true;
        /// <summary>
        /// Web isteği dışındakilerde local path gönderimi zorunlu. Yoksa nereye kaydedecek...
        /// </summary>
        public string LocalPath { get; set; }
    }
}
