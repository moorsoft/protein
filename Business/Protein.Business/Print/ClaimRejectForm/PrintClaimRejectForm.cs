﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Protein.Business.Abstract.Print;
using Protein.Common.Extensions;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Business.Print.ClaimRejectForm
{
    public class PrintClaimRejectForm : PrintUtility, IPrint<PrintClaimRejectFormReq>
    {
        public PrintResponse DoWork(PrintClaimRejectFormReq request)
        {
            PrintResponse response = new PrintResponse();

            try
            {
                #region FillPrintData
                V_FormClaimReject reject = new GenericRepository<V_FormClaimReject>().FindBy($"CLAIM_ID = {request.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (reject == null) { response.Code = "999"; response.Message = "Kayıt Bulunamadı!"; return response; }
                #endregion

                string _path = "";
                if (request.IsWebRequest)
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints")))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints"));

                    _path = HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints") + "\\" + Guid.NewGuid() + ".pdf";
                }
                else { _path = request.LocalPath + "\\" + (!string.IsNullOrEmpty(request.FileName) ? request.FileName + ".pdf" : "_TAZMINAT_KURUM_IADE.pdf"); };

                int cnt = 0;
                while (true)
                {
                    cnt++;
                    if (File.Exists(_path))
                    {
                        _path = _path.Replace(".pdf", "") + "_" + cnt.ToString() + ".pdf";
                    }
                    else break;
                }

                FileStream fs = new FileStream(_path, FileMode.Create);
                Document document = new Document(PageSize.A4, 10, 10, 40, 20);

                PdfWriter writer = PdfWriter.GetInstance(document, fs);

                document.AddAuthor("TEST");
                document.AddCreator("TEST");
                document.AddKeywords("TEST");
                document.AddSubject("TEST");
                document.AddTitle("TEST");
                document.Open();

                Image image = Image.GetInstance(GetCompanyLogoPath(reject.COMPANY_NAME));

                image.ScalePercent(10f);
                document.Add(image);

                BaseFont STF_Helvetica_Turkish = BaseFont.CreateFont("Helvetica", "CP1254", BaseFont.NOT_EMBEDDED);

                #region Header
                Font fontHeader = new Font(STF_Helvetica_Turkish, 20, Font.BOLD);
                Paragraph paragraphHeader = new Paragraph("TAZMİNAT RED FORMU", fontHeader);
                paragraphHeader.Alignment = 1; // 0 left  1 center 2 right
                paragraphHeader.Font.Size = 20;
                document.Add(paragraphHeader);
                document.Add(new Paragraph());
                Paragraph ds = new Paragraph();
                Phrase phr = new Phrase(Environment.NewLine);
                ds.Add(phr);
                ds.Add(phr);
                ds.Add(phr);
                ds.Add(phr);
                document.Add(ds);
                #endregion

                #region FillDetails

                Font fontSubText = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL);
                Font fontSubValue = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL);

                var table2 = new PdfPTable(2);
                float[] widths2 = new float[] { 12f, 50f };
                table2.SetWidths(widths2);

                table2.DefaultCell.HorizontalAlignment = 0;

                table2.WidthPercentage = 100;
                table2.DefaultCell.Border = Rectangle.NO_BORDER;

                var cellDetail = new PdfPCell();
                cellDetail.Colspan = 2;
                cellDetail.PaddingTop = 30;
                cellDetail.Border = 0;
                cellDetail.PaddingLeft = 0;
                cellDetail.PaddingRight = 0;
                cellDetail.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                table2.AddCell(cellDetail);

                table2.AddCell(new Phrase("Tarih", fontSubText));
                table2.AddCell(new Phrase($":    {(reject.CLAIM_DATE.IsDateTime() ? DateTime.Parse(reject.CLAIM_DATE).ToString("dd.MM.yyyy") : "") }", fontSubValue));

                table2.AddCell(new Phrase("Sigortalı Adı - Soyadı", fontSubText));
                table2.AddCell(new Phrase($":    {reject.INSURED_NAME_SURNAME}", fontSubValue));

                table2.AddCell(new Phrase("Poliçe Numarası", fontSubText));
                table2.AddCell(new Phrase($":    {reject.POLICY_NUMBER}", fontSubValue));

                table2.AddCell(new Phrase("Müşteri Numarası", fontSubText));
                table2.AddCell(new Phrase($":    {reject.INSURED_CONTACT_ID}", fontSubValue));

                table2.AddCell(new Phrase("Ürün Adı", fontSubText));
                table2.AddCell(new Phrase($":    {reject.PRODUCT_NAME}", fontSubValue));

                table2.AddCell(new Phrase("Provizyon No", fontSubText));
                table2.AddCell(new Phrase($":    {reject.CLAIM_ID}", fontSubValue));

                table2.AddCell(new Phrase("Kurum Adı", fontSubText));
                table2.AddCell(new Phrase($":    {reject.PROVIDER_NAME}", fontSubValue));

                table2.AddCell(new Phrase("Acente Adı", fontSubText));
                table2.AddCell(new Phrase($":    {reject.AGENCY_NAME}", fontSubValue));

                table2.AddCell(new Phrase("Fatura Tutarı", fontSubText));
                table2.AddCell(new Phrase($":    {(reject.PAID.IsNumeric() ? decimal.Parse(reject.PAID).ToString("#0.00") : "")}", fontSubValue));

                table2.AddCell(new Phrase("Tanı ", fontSubText));
                int i = 0;
                if (!reject.ICD_LIST.IsNull())
                {
                    List<string> icdList = reject.ICD_LIST.Split('#').ToList();
                    foreach (var item in icdList)
                    {
                        table2.AddCell(new Phrase($"{(i == 0 ? ":" : " ")}    {item}", fontSubValue));
                        i++;
                        if (i < icdList.Count)
                            table2.AddCell((new Phrase("", fontSubValue)));
                    }
                }
                else
                {
                    table2.AddCell((new Phrase(":", fontSubValue)));
                }


                table2.AddCell(new Phrase("Tedavi ", fontSubText));
                i = 0;
                if (!reject.PROCESS_LIST.IsNull())
                {
                    List<string> processList = reject.PROCESS_LIST.Split('#').ToList();
                    foreach (var item in processList)
                    {
                        table2.AddCell(new Phrase($"{(i == 0 ? ":" : " ")}    {item}", fontSubValue));
                        i++;
                        if (i < processList.Count)
                            table2.AddCell((new Phrase("", fontSubValue)));
                    }
                }
                else
                {
                    table2.AddCell((new Phrase(":", fontSubValue)));
                }

                document.Add(table2);

                document.Add(ds);
                #endregion

                #region Paragraph

                Font fontBold = new Font(STF_Helvetica_Turkish, 10, Font.BOLD);
                Font fontNormal = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL);
                Font fontUnderline = new Font(STF_Helvetica_Turkish, 9, Font.UNDERLINE);
                Font fontNewLine = new Font(STF_Helvetica_Turkish, 4, Font.NORMAL);

                Phrase pr = new Phrase();
                Chunk chunk2 = new Chunk($"Sayın ", fontNormal);
                Chunk chunk = new Chunk($"{reject.INSURED_NAME_SURNAME}", fontBold);
                pr.Add(chunk2);
                pr.Add(chunk);
                document.Add(pr);
                document.Add(new Phrase(Environment.NewLine, fontNewLine));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                pr = new Phrase();
                chunk = new Chunk($"{reject.CLAIM_DATE}", new Font(STF_Helvetica_Turkish, 9, Font.BOLD));
                pr.Add(chunk);
                chunk = new Chunk($" tarihli sağlık giderleriniz poliçenizin özel-genel şartları, teminat ve limitleri dahilinde değerlendirilmiş ", fontNormal);
                pr.Add(chunk);
                chunk = new Chunk($"onaylanamamıştır.", fontUnderline);
                pr.Add(chunk);
                document.Add(pr);
                document.Add(new Phrase(Environment.NewLine, fontBold));

                document.Add(new Paragraph($"Red açıklaması aşağıda yazılı olduğu gibidir.", fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"{reject.REASON}", fontBold));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"Bilgilerinize sunar, sağlıklı günler dileriz.", fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"Saygılarımızla,", fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"{reject.COMPANY_NAME} ADINA", fontBold));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"Telefon   : {reject.COMPANY_PHONE}", fontBold));
                document.Add(new Paragraph($"Fax        : {reject.COMPANY_FAX}", fontBold));
                document.Add(new Phrase(Environment.NewLine, fontNormal));

                #endregion


                document.Close();
                writer.Close();
                fs.Close();
                if (request.IsWebRequest)
                {
                    request.FileName = !string.IsNullOrEmpty(request.FileName) ? request.FileName : (reject.INSURED_NAME_SURNAME.ToUpper() + "_TAZMINAT_KURUM_IADE" + reject.PROVIDER_NAME);
                    PrintOut(_path, request.FileName);
                }

                response.Url = _path;
                response.FileName = request.FileName;
                response.Code = "100";
            }
            catch (Exception ex) { response.Code = "999"; response.Message = ex.Message; }
            return response;
        }
    }
}
