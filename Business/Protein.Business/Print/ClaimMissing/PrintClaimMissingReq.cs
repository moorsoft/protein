﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Print.ClaimMissing
{
    public class PrintClaimMissingReq : CommonReq
    {
        public long ClaimId { get; set; }
    }
}
