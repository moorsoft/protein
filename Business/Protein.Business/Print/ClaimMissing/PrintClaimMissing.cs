﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Protein.Business.Abstract.Print;
using Protein.Common.Extensions;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Business.Print.ClaimMissing
{
    public class PrintClaimMissing : PrintUtility, IPrint<PrintClaimMissingReq>
    {
        public PrintResponse DoWork(PrintClaimMissingReq request)
        {
            PrintResponse response = new PrintResponse();
            try
            {
                #region FillPrintData
                V_FormClaimMissing claimMissing = new GenericRepository<V_FormClaimMissing>().FindBy($"CLAIM_ID = {request.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (claimMissing == null)
                {
                    response.Code = "999"; response.Message = "Kayıt Bulunamadı"; return response;
                }
                List<V_FormClaimMissingList> claimMissingLists = new GenericRepository<V_FormClaimMissingList>().FindBy($"CLAIM_ID = {request.ClaimId}", orderby: "COMPANY_ID", fetchDeletedRows: true, fetchHistoricRows: true);
                if (claimMissingLists.Count < 1)
                { response.Code = "999"; response.Message = "Kayıt Bulunamadı"; return response; }
                #endregion
                string _path = "";

                if (request.IsWebRequest)
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints")))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints"));

                    _path = HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints") + "\\" + Guid.NewGuid() + ".pdf";
                }
                else { _path = request.LocalPath + "\\" + (!string.IsNullOrEmpty(request.FileName) ? request.FileName + ".pdf" : "_TAZMINAT_KURUM_IADE.pdf"); };

                int cnt = 0;
                while (true)
                {
                    cnt++;
                    if (File.Exists(_path))
                    {
                        _path = _path.Replace(".pdf", "") + "_" + cnt.ToString() + ".pdf";
                    }
                    else break;
                }


                System.IO.FileStream fs = new FileStream(_path, FileMode.Create);
                Document document = new Document(PageSize.A4, 20, 20, 50, 20);

                PdfWriter writer = PdfWriter.GetInstance(document, fs);

                document.AddAuthor("TEST");
                document.AddCreator("TEST");
                document.AddKeywords("TEST");
                document.AddSubject("TEST");
                document.AddTitle("TEST");
                document.Open();

                Image image = Image.GetInstance(GetCompanyLogoPath(claimMissing.COMPANY_NAME));

                image.ScalePercent(10f);
                document.Add(image);

                iTextSharp.text.pdf.BaseFont STF_Helvetica_Turkish = iTextSharp.text.pdf.BaseFont.CreateFont("Helvetica", "CP1254", iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED);

                #region Header
                iTextSharp.text.Font fontHeader = new iTextSharp.text.Font(STF_Helvetica_Turkish, 20, iTextSharp.text.Font.BOLD);
                Paragraph paragraphHeader = new Paragraph("TAZMİNAT EKSİK EVRAK BİLDİRİMİ   ", fontHeader);
                paragraphHeader.Alignment = 1;
                paragraphHeader.Font.Size = 20;
                document.Add(paragraphHeader);
                document.Add(new Paragraph());
                Paragraph ds = new Paragraph();
                Phrase phr = new Phrase(Environment.NewLine);
                ds.Add(phr);
                ds.Add(phr);
                ds.Add(phr);
                ds.Add(phr);
                document.Add(ds);
                #endregion
                #region FillDetails
                var table2 = new PdfPTable(2);
                float[] widths2 = new float[] { 18f, 50f };
                table2.SetWidths(widths2);

                table2.DefaultCell.HorizontalAlignment = 0;

                table2.WidthPercentage = 100;
                table2.DefaultCell.Border = Rectangle.NO_BORDER;

                var cellDetail = new PdfPCell();
                cellDetail.Colspan = 2;
                cellDetail.PaddingTop = 30;
                cellDetail.Border = 0;
                cellDetail.PaddingLeft = 0;
                cellDetail.PaddingRight = 0;
                cellDetail.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                table2.AddCell(cellDetail);

                iTextSharp.text.Font fontSubText = new iTextSharp.text.Font(STF_Helvetica_Turkish, 9, iTextSharp.text.Font.BOLD);
                iTextSharp.text.Font fontSubValue = new iTextSharp.text.Font(STF_Helvetica_Turkish, 9, iTextSharp.text.Font.NORMAL);

                table2.AddCell(new Phrase("Tarih", fontSubText));
                table2.AddCell(new Phrase($":    {(claimMissing.MISSING_DATE.IsDateTime() ? DateTime.Parse(claimMissing.MISSING_DATE).ToString("dd.MM.yyyy") : "") }", fontSubValue));

                table2.AddCell(new Phrase("Sigortalı Adı - Soyadı", fontSubText));
                table2.AddCell(new Phrase($":    {claimMissing.INSURED_NAME_SURNAME}", fontSubValue));

                table2.AddCell(new Phrase("Sigorta Ettiren", fontSubText));
                table2.AddCell(new Phrase($":    {claimMissing.INSURER_NAME}", fontSubValue));

                table2.AddCell(new Phrase("Poliçe Numarası", fontSubText));
                table2.AddCell(new Phrase($":    {claimMissing.POLICY_NUMBER}", fontSubValue));

                table2.AddCell(new Phrase("Müşteri Numarası", fontSubText));
                table2.AddCell(new Phrase($":    {claimMissing.INSURED_CONTACT_ID}", fontSubValue));

                table2.AddCell(new Phrase("Ürün Adı", fontSubText));
                table2.AddCell(new Phrase($":    {claimMissing.PRODUCT_NAME}", fontSubValue));

                table2.AddCell(new Phrase("Provizyon No", fontSubText));
                table2.AddCell(new Phrase($":    {claimMissing.CLAIM_ID}", fontSubValue));

                table2.AddCell(new Phrase("Kurum Adı", fontSubText));
                table2.AddCell(new Phrase($":    {claimMissing.PROVIDER_NAME}", fontSubValue));

                table2.AddCell(new Phrase("Acente Adı", fontSubText));
                table2.AddCell(new Phrase($":    {claimMissing.AGENCY_NAME}", fontSubValue));

                table2.AddCell(new Phrase("Fatura Tutarı", fontSubText));
                table2.AddCell(new Phrase($":    {claimMissing.BILL_AMOUNT}", fontSubValue));

                table2.AddCell(new Phrase("Tanı ", fontSubText));
                int i = 0;
                if (!claimMissing.TANI.IsNull())
                {
                    List<string> icdList = claimMissing.TANI.Split('#').ToList();
                    foreach (var item in icdList)
                    {
                        table2.AddCell(new Phrase($"{(i == 0 ? ":" : " ")}    {item}", fontSubValue));
                        i++;
                        if (i < icdList.Count)
                            table2.AddCell((new Phrase("", fontSubValue)));
                    }
                }
                else
                {
                    table2.AddCell((new Phrase(":", fontSubValue)));
                }

                table2.AddCell(new Phrase("Tedavi ", fontSubText));
                i = 0;
                if (!claimMissing.TEDAVI.IsNull())
                {
                    List<string> processList = claimMissing.TEDAVI.Split('#').ToList();
                    foreach (var item in processList)
                    {
                        table2.AddCell(new Phrase($"{(i == 0 ? ":" : " ")}    {item}", fontSubValue));
                        i++;
                        if (i < processList.Count)
                            table2.AddCell((new Phrase("", fontSubValue)));
                    }
                }
                else
                {
                    table2.AddCell((new Phrase(":", fontSubValue)));
                }
                document.Add(table2);

                #endregion
                #region FillInfo
                iTextSharp.text.Font fontBold = new iTextSharp.text.Font(STF_Helvetica_Turkish, 9, iTextSharp.text.Font.BOLD);
                iTextSharp.text.Font fontNormal = new iTextSharp.text.Font(STF_Helvetica_Turkish, 9, iTextSharp.text.Font.NORMAL);

                Paragraph dsNewLine = new Paragraph();
                Phrase newLine = new Phrase(Environment.NewLine);
                dsNewLine.Add(newLine);
                document.Add(dsNewLine);

                Phrase pr = new Phrase();
                Chunk chunk = new Chunk($"{claimMissing.INSURED_NAME_SURNAME}", fontBold);
                Chunk chunk2 = new Chunk($"Sayın ", fontNormal);
                pr.Add(chunk2);
                pr.Add(chunk);
                document.Add(pr);

                document.Add(new Phrase(" tarafımıza iletmiş olduğunuz tazminat talebiniz değerlendirilmiş, aşağıda belirtilen belgelerin eksik olduğu tespit edilmiştir.", fontNormal));
                document.Add(dsNewLine);
                document.Add(dsNewLine);

                Paragraph parag = new Paragraph();
                pr.Clear();
                pr = new Phrase();
                Chunk chunk3 = new Chunk($"EKSİK EVRAKLAR", fontBold);
                pr.Add(chunk3);
                parag.Add(pr);
                document.Add(parag);

                int s1 = 1;
                foreach (V_FormClaimMissingList missingItem in claimMissingLists)
                {
                    parag.Clear();
                    parag = new Paragraph();
                    pr.Clear();
                    pr = new Phrase();
                    Chunk chunk4 = new Chunk($"{s1.ToString()}. ", fontBold);
                    Chunk chunk5 = new Chunk($"{missingItem.MISSING_TYPE_TEXT}", fontNormal);
                    pr.Add(chunk4);
                    pr.Add(chunk5);
                    parag.Add(pr);
                    document.Add(parag);
                    s1++;
                }
                
                document.Add(dsNewLine);
                document.Add(dsNewLine);
                document.Add(new Paragraph("Bilgilerinize sunar, sağlıklı günler dileriz.", fontNormal));
                document.Add(dsNewLine);
                document.Add(new Paragraph("Saygılarımızla, ", fontNormal));
                document.Add(dsNewLine);
                if (claimMissingLists.Where(cm=>cm.MISSING_TYPE=="45").ToList().Count>0)
                {
                    document.Add(new Paragraph("Evrak temini için hastane başvurusu yapılamıyor, belgeler fiziki olarak yok ise https://enabiz.gov.tr/Giris.aspx net sitesinden e-devlet şifresi/e- nabız ile giriş yapılır. Gelen ekran sol bölüm de açılan hastalıklarım - tahlillerim - raporlarım bölümlerinden ekran görüntülerini - bilgileri mail yoluyla iletebilirsiniz.", fontBold));
                }
                
                document.Add(dsNewLine);
                document.Add(new Phrase($"{claimMissing.COMPANY_NAME}  ADINA,", fontBold));
                #endregion

                document.Close();
                writer.Close();
                fs.Close();
                if (request.IsWebRequest)
                {
                    request.FileName = !string.IsNullOrEmpty(request.FileName) ? request.FileName : (claimMissing.CLAIM_ID.ToUpper() + "_TAZMİNAT_EKSİK_EVRAK_BİLDİRİMİ");
                    PrintOut(_path, request.FileName);
                }
                response.Url = _path;
                response.FileName = request.FileName;
                response.Code = "100";
            }
            catch (Exception ex) { response.Code = "999"; response.Message = ex.Message; }
            return response;
        }
    }
}
