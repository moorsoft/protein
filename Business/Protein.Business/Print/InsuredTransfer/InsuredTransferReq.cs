﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Print.InsuredTransfer
{
    public class InsuredTransferReq : CommonReq
    {
        public long InsuredId { get; set; }
    }
}
