﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Protein.Business.Abstract.Print;
using Protein.Common.Extensions;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Business.Print.ProvisionApproval
{
    public class PrintProvisionApproval : PrintUtility, IPrint<PrintProvisionApprovalReq>
    {
        public PrintResponse DoWork(PrintProvisionApprovalReq request)
        {
            PrintResponse response = new PrintResponse();
            try
            {
                #region FillPrintData
                V_FormProvisionApprove provisionApprove = new GenericRepository<V_FormProvisionApprove>().FindBy($"CLAIM_ID = {request.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (provisionApprove == null) { response.Code = "999"; return response; }
                //List<V_FormBillApproveList> billApproveList = new GenericRepository<V_FormBillApproveList>().FindBy($"CLAIM_ID = {request.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                //if (billApproveList.Count < 1)
                //{ response.Code = "999"; return response; }
                #endregion


                string _path = "";
                if (request.IsWebRequest)
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints")))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints"));

                    _path = HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints") + "\\" + Guid.NewGuid() + ".pdf";
                }
                else { _path = request.LocalPath + "\\" + (!string.IsNullOrEmpty(request.FileName) ? request.FileName + ".pdf" : "PROVIZYON_ON_ONAY.pdf"); };

                int cnt = 0;
                while (true)
                {
                    cnt++;
                    if (File.Exists(_path))
                    {
                        _path = _path.Replace(".pdf", "") + "_" + cnt.ToString() + ".pdf";
                    }
                    else break;
                }


                FileStream fs = new FileStream(_path, FileMode.Create);
                Document document = new Document(PageSize.A4, 20, 20, 50, 20);

                PdfWriter writer = PdfWriter.GetInstance(document, fs);

                document.AddAuthor("TEST");
                document.AddCreator("TEST");
                document.AddKeywords("TEST");
                document.AddSubject("TEST");
                document.AddTitle("TEST");
                document.Open();

                Image image = Image.GetInstance(GetCompanyLogoPath(provisionApprove.COMPANY_NAME));

                image.ScalePercent(10f);
                document.Add(image);

                BaseFont STF_Helvetica_Turkish = BaseFont.CreateFont("Helvetica", "CP1254", BaseFont.NOT_EMBEDDED);

                #region Header
                Font fontHeader = new Font(STF_Helvetica_Turkish, 16, Font.BOLD);
                Paragraph paragraphHeader = new Paragraph("PROVİZYON ÖN ONAY FORMU", fontHeader);
                paragraphHeader.Alignment = 1;
                document.Add(paragraphHeader);
                Paragraph ds = new Paragraph();
                Phrase phr = new Phrase(Environment.NewLine);
                ds.Add(phr);
                ds.Add(phr);
                document.Add(phr);

                //ds.Add(phr);
                //ds.Add(phr);
                #endregion

                #region FillDetails

                Font fontSubText = new Font(STF_Helvetica_Turkish, 9, Font.BOLD);
                Font fontSubValue = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL);

                var table2 = new PdfPTable(2);
                float[] widths2 = new float[] { 12f, 50f };
                table2.SetWidths(widths2);

                table2.DefaultCell.HorizontalAlignment = 0;

                table2.WidthPercentage = 100;
                table2.DefaultCell.Border = Rectangle.NO_BORDER;

                var cellDetail = new PdfPCell();
                cellDetail.Colspan = 2;
                cellDetail.PaddingTop = 30;
                cellDetail.Border = 0;
                cellDetail.PaddingLeft = 0;
                cellDetail.PaddingRight = 0;
                cellDetail.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                table2.AddCell(cellDetail);

                table2.AddCell(new Phrase("Tarih", fontSubText));
                table2.AddCell(new Phrase($":    {(provisionApprove.PROVISION_DATE.IsDateTime() ? DateTime.Parse(provisionApprove.PROVISION_DATE).ToString("dd.MM.yyyy") : "") }", fontSubValue));

                table2.AddCell(new Phrase("Sigortalı Adı - Soyadı", fontSubText));
                table2.AddCell(new Phrase($":    {provisionApprove.INSURED_NAME_SURNAME}", fontSubValue));

                table2.AddCell(new Phrase("Poliçe Numarası", fontSubText));
                table2.AddCell(new Phrase($":    {provisionApprove.POLICY_NUMBER}", fontSubValue));

                table2.AddCell(new Phrase("Müşteri Numarası", fontSubText));
                table2.AddCell(new Phrase($":    {provisionApprove.INSURED_CONTACT_ID}", fontSubValue));

                table2.AddCell(new Phrase("Ürün Adı", fontSubText));
                table2.AddCell(new Phrase($":    {provisionApprove.PRODUCT_NAME}", fontSubValue));

                table2.AddCell(new Phrase("Provizyon No", fontSubText));
                table2.AddCell(new Phrase($":    {provisionApprove.CLAIM_ID}", fontSubValue));

                table2.AddCell(new Phrase("Kurum Adı", fontSubText));
                table2.AddCell(new Phrase($":    {provisionApprove.PROVIDER_NAME}", fontSubValue));

                table2.AddCell(new Phrase("Tanı ", fontSubText));
                int i = 0;
                if (!provisionApprove.ICD_LIST.IsNull())
                {
                    List<string> icdList = provisionApprove.ICD_LIST.Split('#').ToList();
                    foreach (var item in icdList)
                    {
                        table2.AddCell(new Phrase($"{(i == 0 ? ":" : " ")}    {item}", fontSubValue));
                        i++;
                        if (i < icdList.Count)
                            table2.AddCell((new Phrase("", fontSubValue)));
                    }
                }
                else
                {
                    table2.AddCell((new Phrase("", fontSubValue)));
                }


                table2.AddCell(new Phrase("Tedavi ", fontSubText));
                i = 0;
                if (provisionApprove.PROCESS_LIST.IsNull() && provisionApprove.CLAIM_NOTE_TEDAVI.IsNull())
                {
                    table2.AddCell((new Phrase("", fontSubValue)));
                }
                else
                {
                    if (!provisionApprove.PROCESS_LIST.IsNull())
                    {
                        List<string> processList = provisionApprove.PROCESS_LIST.Split('#').ToList();
                        foreach (var item in processList)
                        {
                            table2.AddCell(new Phrase($"{(i == 0 ? ":" : " ")}    {item}", fontSubValue));
                            i++;
                            if (i < processList.Count)
                                table2.AddCell((new Phrase("", fontSubValue)));
                        }
                        if (!provisionApprove.CLAIM_NOTE_TEDAVI.IsNull())
                        {
                            table2.AddCell((new Phrase("", fontSubValue)));
                        }
                        i = 0;
                    }
                    if (!provisionApprove.CLAIM_NOTE_TEDAVI.IsNull())
                    {
                        List<string> noteList = provisionApprove.CLAIM_NOTE_TEDAVI.Split('#').ToList();
                        foreach (var item in noteList)
                        {
                            table2.AddCell(new Phrase($"{(i == 0 ? ":" : " ")}    {item}", fontSubValue));
                            i++;
                            if (i < noteList.Count)
                                table2.AddCell((new Phrase("", fontSubValue)));
                        }
                    }
                }

                document.Add(table2);

                document.Add(ds);
                #endregion

                Font fontBold = new Font(STF_Helvetica_Turkish, 10, Font.BOLD);
                Font fontNormal = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL);
                Font fontNewLine = new Font(STF_Helvetica_Turkish, 4, Font.NORMAL);

                document.Add(new Paragraph($"{provisionApprove.COMPANY_NAME} sigortalısı yukarıda poliçe bilgileri bulunan sigortalımıza ait provizyon talebi kuruluşunuz ile yapılan anlaşma gereğince sigortalımızın poliçesinin özel ve genel şartları, teminat ve limitleri dahilinde değerlendirilmiş ve onaylanmıştır.", fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"Bu onay yazısı özel sağlık sigortası hasta bilgi formunda ve /veya sigorta başvuru formunda ki beyanında eksik ya da hatalı bilgi bulunmaması kaydı ile geçerli olacaktır. İlgili belgelerde yanlış/eksik bilgi olduğunun saptanması halinde onayımız geçerli olmayacaktır.", fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"Provizyon geçerlilik süresi (7) gündür. Belirlenen sürenin aşılması durumunda tekrar onay alınması gerekmektedir.", fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"Yatış onayımız 1 gün ile sınırlıdır. Yatışın uzaması halinde gerekli tıbbi dokümanların tarafımıza gönderilmesi rica olunur.", fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"Sigortalımız taburcu edilmeden en az 2 saat önce fatura dökümleri ve medikal dosya dokümanlarının tarafımıza gönderilmesi rica olunur. 'FDA onayı olmayan malzemeler ve TTBAUT de yer almayan işlemler sigortalı katılımıdır.", fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"Bilgilerinize sunarız ", fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"Saygılarımızla", fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"{provisionApprove.COMPANY_NAME} ADINA", fontBold));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"Telefon   : {provisionApprove.COMPANY_PHONE}", fontNormal));
                document.Add(new Paragraph($"Fax       : {provisionApprove.COMPANY_FAX}", fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNormal));

                document.Add(new Paragraph($"'Bu provizyon çerçevesinde yapılan ödemelerde şirketi ibra ederim. Sigorta özel ve genel şartları dahilinde ödenmemesi gereken veya poliçemde belirtilen limitleri aşan ödemelerde sigorta şirketinin rücu hak ve yetkisini kabullendiğimi gayrıkabili rücu olmak kaydı ile kabul ve beyan ederim.'", fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"Sayın Yetkili, Sigortalı/sigorta ettiren sıfatı ile şahsım ve kanunen temsile yetkili olduğum diğer sigortalılara ait sağlık durumu ve tedavilerle ilgili olarak, ünvanı yukarıda yazılı olan Sigorta Şirketinin talep edeceği bütün bilgi ve belgeleri bu şirkete vermenizi; bu konuda bilgi ve onayım olduğunu ve Sigorta Şirketinin bu bilgi ve belgeleri sigorta mevzuatı ve sigorta sözleşmesi uyarınca öğrenme hakkının bulunduğunu bildiririm.", fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"Sigortalı/Sigorta Ettiren İmza :", fontBold));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"Dikkat: İşbu belgede sigortalının özel istisnaları, limitleri ve fatura tutarına iştirak oranları gösterilmektedir. Fatura tanzimini takiben bu bilgilere göre sigortalıdan tahsilini, faturanın ise bakiyenin kuruluşunuza ödenebilmesi için bu provizyon formuyla birlikte şirketimize gönderilmesini rica ederiz. Tereddüt halinde lütfen şirketimizden 'ödeme teyidi' alınız. Herhangi bir sebeple tedaviden vazgeçilirse bu provizyon numarası belirtilerek iptal edilmelidir.", fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"Sigortalı/sigorta ettiren tarafından imzalanmamış provizyon belgeleri işleme alınamayacaktır.", fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));


                document.NewPage();


                image.ScalePercent(10f);
                document.Add(image);
                
                document.Add(paragraphHeader);
                document.Add(phr);
                document.Add(phr);
                document.Add(phr);
                document.Add(phr);

                paragraphHeader = new Paragraph("TEMLİK BEYANI", fontBold);
                paragraphHeader.Alignment = 1;
                document.Add(paragraphHeader);

                document.Add(new Paragraph($"12599532 sayılı Sağlık Sigortası Poliçesi ile sigortalısı bulunduğum {provisionApprove.COMPANY_NAME} 'ye, 04/04/2018 tarihinde başvuruda bulunarak .................. TL tutarındaki tazminatı talep etmiş bulunuyorum. Bu tazminat bedeli tarafımdan tazmin edildiğinde {provisionApprove.COMPANY_NAME} 'den poliçe genel ve özel şartları çerçevesinde hiçbir alacağımın kalmamış olacağını ve {provisionApprove.COMPANY_NAME} 'yi gayrıkabilirücu olarak kesin şekilde ibra etmiş olacağımı kabul ve beyan ederim.", fontBold));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));
                
                document.Add(new Paragraph($"Sağlık Sigortası Poliçesi kapsamında gördüğüm tedavi dolayısıyla, tabi bulunduğum sosyal güvenlik kurumuna ve/veya üçüncü kişilere karşı sahip olduğum tüm talep ve dava haklarımı, {provisionApprove.COMPANY_NAME}	'nun ödeyeceği tutar ile sınırlı olmak üzere, {provisionApprove.COMPANY_NAME} 'ye, Borçlar Kanununun 162. ve devamındaki maddelerde düzenlenen alacağın temliki hükümleri çerçevesinde gayrikabil rücu olarak temlik ettiğimi beyan ve taahhüt ederim.", fontBold));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));
                
                document.Add(new Paragraph($"İşbu temlik işleminin herhangi bir sebeple hüküm doğurmaması halinde, {provisionApprove.COMPANY_NAME} , kendisinden talep etmiş olduğum tazminat tutarını bana ödediği anda TTK m. 1301 uyarınca, tabi bulunduğum Sosyal Güvenlik Kurumuna ve / veya üçüncü kişilere karşı sahip olduğum talep ve dava haklarına halef olacak ve Sosyal Güvenlik Kurumuna ve / veya üçüncü kişilere rücu edebilecektir.", fontBold));
                document.Add(new Phrase(Environment.NewLine, fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNormal));

                paragraphHeader = new Paragraph("TAZMİNAT MAKBUZU VE İBRANAME", fontBold);
                paragraphHeader.Alignment = 1;
                document.Add(paragraphHeader);

                document.Add(new Paragraph($"12599532 sayılı Sağlık Sigortası Poliçesi ile sigortalısı bulunduğum {provisionApprove.COMPANY_NAME} 'den, 04/04/2018 Tarihli başvurum ile talep ettiğim......................TL tutarındaki tazminatı nakden ve defaten tahsil ettim. Bu tahsilat yapmakla, {provisionApprove.COMPANY_NAME} 'den poliçe genel ve özel şartları çerçevesinde hiçbir alacağımın kalmamış olduğunu  ve  {provisionApprove.COMPANY_NAME} 'yi gayrikabil rücu olarak kesin şekilde ibra ettiğimi Kabul, beyan ve taahhüt ederim.", fontBold));
                document.Add(new Phrase(Environment.NewLine, fontNormal));

                table2 = new PdfPTable(2);
                widths2 = new float[] { 17f, 50f };
                table2.SetWidths(widths2);
                table2.DefaultCell.HorizontalAlignment = 0;
                table2.WidthPercentage = 100;
                table2.DefaultCell.Border = Rectangle.NO_BORDER;
                table2.AddCell(cellDetail);

                table2.AddCell(new Phrase("Sigortalı/Sigorta Ettiren", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("Adı Soyadı", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("Tarih", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("İmza", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("", fontBold));
                table2.AddCell(new Phrase($"    ", fontBold));

                table2.AddCell(new Phrase("KİMLİK BİLGİLERİ", fontBold));
                table2.AddCell(new Phrase($"    ", fontBold));

                table2.AddCell(new Phrase("Adı Soyadı", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("Doğum Yeri ve Tarihi", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("Baba Adı", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("Kimlik Türü", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("Kimlik No", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("İli/İlçesi", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("Mah.?Köyü", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("Cilt No: Sayfa No: Kütük No", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));
                document.Add(table2);

                document.Add(new Paragraph($"Sigortalı/sigorta ettiren tarafından imzalanmamış tazminat teyit belgeleri işleme alınmayacaktır.", fontBold));

                document.Close();
                writer.Close();
                fs.Close();

                if (request.IsWebRequest)
                {
                    request.FileName = !string.IsNullOrEmpty(request.FileName) ? request.FileName : ("PROVİZYON_ÖN_ONAY_FORMU_" + request.ClaimId);
                    PrintOut(_path, request.FileName);
                }
                response.Url = _path;
                response.FileName = request.FileName;
                response.Code = "100";
            }
            catch (Exception ex) { response.Code = "999"; response.Message = ex.Message; }
            return response;
        }
    }
}
