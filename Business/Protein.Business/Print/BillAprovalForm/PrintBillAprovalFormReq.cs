﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Print.BillAprovalForm
{
    public class PrintBillAprovalFormReq : CommonReq
    {
        public long ClaimId { get; set; }
    }
}
