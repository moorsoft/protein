﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Protein.Business.Abstract.Print;
using Protein.Common.Extensions;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Business.Print.BillAprovalForm
{
    public class PrintBillAprovalForm : PrintUtility, IPrint<PrintBillAprovalFormReq>
    {
        public PrintResponse DoWork(PrintBillAprovalFormReq request)
        {
            PrintResponse response = new PrintResponse();
            try
            {
                #region FillPrintData
                V_FormBillApprove billApprove = new GenericRepository<V_FormBillApprove>().FindBy($"CLAIM_ID = {request.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (billApprove == null) { response.Code = "999"; response.Message = "Kayıt Bulunamadı"; return response; }
                List<V_FormBillApproveList> billApproveList = new GenericRepository<V_FormBillApproveList>().FindBy($"CLAIM_ID = {request.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                if (billApproveList.Count < 1)
                { response.Code = "999"; response.Message = "Kayıt Bulunamadı"; return response; }
                #endregion

                string _path = "";
                if (request.IsWebRequest)
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints")))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints"));

                    _path = HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints") + "\\" + Guid.NewGuid() + ".pdf";
                }
                else { _path = request.LocalPath + "\\" + (!string.IsNullOrEmpty(request.FileName) ? request.FileName + ".pdf" : "FATURA_ONAY_FORMU.pdf"); };


                int cnt = 0;
                while (true)
                {
                    cnt++;
                    if (File.Exists(_path))
                    {
                        _path = _path.Replace(".pdf", "") + "_" + cnt.ToString() + ".pdf";
                    }
                    else break;
                }

                FileStream fs = new FileStream(_path, FileMode.Create);
                Document document = new Document(PageSize.A4, 20, 20, 50, 20);

                PdfWriter writer = PdfWriter.GetInstance(document, fs);

                document.AddAuthor("TEST");
                document.AddCreator("TEST");
                document.AddKeywords("TEST");
                document.AddSubject("TEST");
                document.AddTitle("TEST");
                document.Open();

                Image image = Image.GetInstance(GetCompanyLogoPath(billApprove.COMPANY_NAME));

                image.ScalePercent(10f);
                document.Add(image);

                BaseFont STF_Helvetica_Turkish = BaseFont.CreateFont("Helvetica", "CP1254", BaseFont.NOT_EMBEDDED);

                #region Header
                Font fontHeader = new Font(STF_Helvetica_Turkish, 20, Font.BOLD);
                Paragraph paragraphHeader = new Paragraph("FATURA ONAY FORMU", fontHeader);
                paragraphHeader.Alignment = 1;
                document.Add(paragraphHeader);
                Paragraph ds = new Paragraph();
                Phrase phr = new Phrase(Environment.NewLine);
                ds.Add(phr);
                ds.Add(phr);
                document.Add(phr);

                //ds.Add(phr);
                //ds.Add(phr);
                #endregion

                #region FillDetails

                Font fontSubText = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL);
                Font fontSubValue = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL);

                var table2 = new PdfPTable(2);
                float[] widths2 = new float[] { 12f, 50f };
                table2.SetWidths(widths2);

                table2.DefaultCell.HorizontalAlignment = 0;

                table2.WidthPercentage = 100;
                table2.DefaultCell.Border = Rectangle.NO_BORDER;

                var cellDetail = new PdfPCell();
                cellDetail.Colspan = 2;
                cellDetail.PaddingTop = 30;
                cellDetail.Border = 0;
                cellDetail.PaddingLeft = 0;
                cellDetail.PaddingRight = 0;
                cellDetail.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                table2.AddCell(cellDetail);

                table2.AddCell(new Phrase("Tarih", fontSubText));
                table2.AddCell(new Phrase($":    {(billApprove.PROVISION_DATE.IsDateTime() ? DateTime.Parse(billApprove.PROVISION_DATE).ToString("dd.MM.yyyy") : "") }", fontSubValue));

                table2.AddCell(new Phrase("Sigortalı Adı - Soyadı", fontSubText));
                table2.AddCell(new Phrase($":    {billApprove.INSURED_NAME_SURNAME}", fontSubValue));

                table2.AddCell(new Phrase("Poliçe Numarası", fontSubText));
                table2.AddCell(new Phrase($":    {billApprove.POLICY_NUMBER}", fontSubValue));

                table2.AddCell(new Phrase("Müşteri Numarası", fontSubText));
                table2.AddCell(new Phrase($":    {billApprove.INSURED_CONTACT_ID}", fontSubValue));

                table2.AddCell(new Phrase("Ürün Adı", fontSubText));
                table2.AddCell(new Phrase($":    {billApprove.PRODUCT_NAME}", fontSubValue));

                table2.AddCell(new Phrase("Provizyon No", fontSubText));
                table2.AddCell(new Phrase($":    {billApprove.CLAIM_ID}", fontSubValue));

                table2.AddCell(new Phrase("Kurum Adı", fontSubText));
                table2.AddCell(new Phrase($":    {billApprove.PROVIDER_NAME}", fontSubValue));

                table2.AddCell(new Phrase("Acente Adı", fontSubText));
                table2.AddCell(new Phrase($":    {billApprove.AGENCY_NAME}", fontSubValue));

                table2.AddCell(new Phrase("Fatura Tutarı", fontSubText));
                table2.AddCell(new Phrase($":    {billApprove.PAID}", fontSubValue));

                table2.AddCell(new Phrase("Tanı ", fontSubText));
                int i = 0;
                if (!billApprove.ICD_LIST.IsNull())
                {
                    List<string> icdList = billApprove.ICD_LIST.Split('#').ToList();
                    foreach (var item in icdList)
                    {
                        table2.AddCell(new Phrase($"{(i == 0 ? ":" : " ")}    {item}", fontSubValue));
                        i++;
                        if (i < icdList.Count)
                            table2.AddCell((new Phrase("", fontSubValue)));
                    }
                }

                table2.AddCell(new Phrase("Tedavi ", fontSubText));
                i = 0;
                if (!billApprove.PROCESS_LIST.IsNull())
                {
                    List<string> processList = billApprove.PROCESS_LIST.Split('#').ToList();
                    foreach (var item in processList)
                    {
                        table2.AddCell(new Phrase($"{(i == 0 ? ":" : " ")}    {item}", fontSubValue));
                        i++;
                        if (i < processList.Count)
                            table2.AddCell((new Phrase("", fontSubValue)));
                    }
                }
                document.Add(table2);

                document.Add(ds);
                #endregion

                #region FillInfo
                Font fontBold = new Font(STF_Helvetica_Turkish, 10, Font.BOLD);
                Font fontNormal = new Font(STF_Helvetica_Turkish, 10, Font.NORMAL);

                document.Add(new Paragraph($"{billApprove.COMPANY_NAME} sigortalısı sayın {billApprove.INSURED_NAME_SURNAME} .ait kuruluşunuzda gerçekleşen yukarıda yazılı tedaviye ait giderl kuruluşunuz ile yapılan anlaşma gereğince sigortalımızın poliçesinin özel ve genel şartları, teminat ve limitleri dahilinde aşağı belirtilen tutarlar üzerinden onaylanmıştır.", fontNormal));
                document.Add(new Phrase(Environment.NewLine));


                var table3 = new PdfPTable(4);
                table3.DefaultCell.HorizontalAlignment = 0;
                table3.WidthPercentage = 100;

                table3.AddCell(new Phrase("Teminat Adı", fontBold));
                table3.AddCell(new Phrase("Talep Edilen Tutar", fontBold));
                table3.AddCell(new Phrase("Şirket Tutarı", fontBold));
                table3.AddCell(new Phrase("Sigortalı Katılım Payı", fontBold));

                decimal totalACCEPTED = 0, totalREQUESTED = 0, totalPAID = 0, totalINSURED_AMOUNT = 0;
                foreach (var item in billApproveList)
                {
                    table3.AddCell(new Phrase(item.COVERAGE_NAME, fontNormal));
                    table3.AddCell(new Phrase(item.REQUESTED, fontNormal));
                    table3.AddCell(new Phrase(item.PAID, fontNormal));
                    table3.AddCell(new Phrase(item.INSURED_AMOUNT, fontNormal));

                    totalREQUESTED += decimal.Parse(item.REQUESTED.IsNumeric() ? item.REQUESTED : "0");
                    totalACCEPTED += decimal.Parse(item.ACCEPTED.IsNumeric() ? item.ACCEPTED : "0");
                    totalPAID += decimal.Parse(item.PAID.IsNumeric() ? item.PAID : "0");
                    totalINSURED_AMOUNT += decimal.Parse(item.INSURED_AMOUNT.IsNumeric() ? item.INSURED_AMOUNT : "0");
                }

                table3.AddCell(new Phrase("TOPLAM", fontBold));
                table3.AddCell(new Phrase(totalREQUESTED.ToString("#0.00"), fontBold));
                table3.AddCell(new Phrase(totalPAID.ToString("#0.00"), fontBold));
                table3.AddCell(new Phrase(totalINSURED_AMOUNT.ToString("#0.00"), fontBold));

                document.Add(table3);

                document.Add(ds);
                document.Add(ds);

                document.Add(new Phrase(new Chunk($"Bilgilerinize sunar, sağlıklı günler dileriz. ", fontNormal)));
                document.Add(new Phrase(Environment.NewLine));

                document.Add(new Phrase(new Chunk($"Saygılarımızla, ", fontNormal)));
                document.Add(ds);

                document.Add(new Phrase(new Chunk($"{billApprove.COMPANY_NAME} ADINA,", fontBold)));
                document.Add(new Phrase(Environment.NewLine));

                document.Add(new Paragraph($"Telefon   : {billApprove.COMPANY_PHONE}", fontBold));
                document.Add(new Paragraph($"Fax       : {billApprove.COMPANY_FAX}", fontBold));

                document.NewPage();
                document.Add(image);
                document.Add(ds);
                document.Add(ds);

                paragraphHeader = new Paragraph("TEMLİK BEYANI", fontBold);
                paragraphHeader.Alignment = 1;
                document.Add(paragraphHeader);

                document.Add(new Paragraph($"{billApprove.POLICY_NUMBER} sayılı Sağlık Sigortası Poliçesi ile sigortalısı bulunduğum {billApprove.COMPANY_NAME} 'ye, {(billApprove.PROVISION_DATE.IsDateTime() ? DateTime.Parse(billApprove.PROVISION_DATE).ToString("dd.MM.yyyy") : "")} tarihin başvuruda bulunarak {totalREQUESTED.ToString("#0.00")}TL tazminat talep etmiş bulunuyorum. Bu tazminat bedeli tarafımdan tazm edildiğinde {billApprove.COMPANY_NAME}'den poliçe genel ve özel şartları çerçevesinde hiçbir alacağımın kalma olacağını ve {billApprove.COMPANY_NAME}'yi gayrıkabilirücu olarak kesin şekilde ibra etmiş olacağımı kabul ve beyan ederim", fontBold));
                document.Add(new Phrase(Environment.NewLine));

                document.Add(new Paragraph($"Sağlık Sigortası Poliçesi kapsamında gördüğüm tedavi dolayısıyla, tabi bulunduğum sosyal güvenlik kurumuna ve/veya üçüncü kişilere karşı sahip olduğum tüm talep ve dava haklarımı, {billApprove.COMPANY_NAME}'nun ödeyeceği tutar ile sınırlı olmak üzere, {billApprove.COMPANY_NAME}'ye, Borçlar Kanununun 162. ve devamındaki maddelerde düzenlen alacağın temliki hükümleri çerçevesinde gayrikabil rücu olarak temlik ettiğimi beyan ve taahhüt ederim.", fontBold));
                document.Add(new Phrase(Environment.NewLine));

                document.Add(new Paragraph($"İşbu temlik işleminin herhangi bir sebeple hüküm doğurmaması halinde, {billApprove.COMPANY_NAME} , kendisinden talep etmiş olduğum tazminat tutarını bana ödediği anda TTK m. 1301 uyarınca, tabi bulunduğum Sosyal Güvenlik Kurumuna ve / veya üçüncü kişilere karşı sahip olduğum talep ve dava haklarına halef olacak ve Sosyal Güvenlik Kurumuna ve / veya üçüncü kişilere rücu edebilecektir.", fontBold));
                document.Add(ds);

                paragraphHeader = new Paragraph("TAZMİNAT MAKBUZU VE İBRANAME", fontBold);
                paragraphHeader.Alignment = 1;
                document.Add(paragraphHeader);

                document.Add(new Paragraph($"{billApprove.POLICY_NUMBER} sayılı Sağlık Sigortası Poliçesi ile sigortalısı bulunduğum {billApprove.COMPANY_NAME}'den, {(billApprove.PROVISION_DATE.IsDateTime() ? DateTime.Parse(billApprove.PROVISION_DATE).ToString("dd.MM.yyyy") : "")} tarihin başvuruda bulunarak {totalREQUESTED.ToString("#0.00")} Tarihli başvurum ile talep ettiğim {totalREQUESTED.ToString("#0.00")}TL tazminat nakden ve defaten tahsil ettim. Bu tahsilat yapmakla, {billApprove.COMPANY_NAME}'den poliçe genel ve özel şartları çerçevesinde hiçbir alacağımın kalmamı olduğunu ve {billApprove.COMPANY_NAME}'yi gayrikabil rücu olarak kesin şekilde ibra ettiğimi Kabul, beyan ve taah  ederim.", fontBold));
                document.Add(new Phrase(Environment.NewLine));

                table2 = new PdfPTable(2);
                widths2 = new float[] { 17f, 50f };
                table2.SetWidths(widths2);
                table2.DefaultCell.HorizontalAlignment = 0;
                table2.WidthPercentage = 100;
                table2.DefaultCell.Border = Rectangle.NO_BORDER;
                table2.AddCell(cellDetail);

                table2.AddCell(new Phrase("Sigortalı/Sigorta Ettiren", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("Adı Soyadı", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("Tarih", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("İmza", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("", fontBold));
                table2.AddCell(new Phrase($"    ", fontBold));

                table2.AddCell(new Phrase("KİMLİK BİLGİLERİ", fontBold));
                table2.AddCell(new Phrase($"    ", fontBold));

                table2.AddCell(new Phrase("Adı Soyadı", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("Doğum Yeri ve Tarihi", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("Baba Adı", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("Kimlik Türü", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("Kimlik No", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("İli/İlçesi", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("Mah.?Köyü", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));

                table2.AddCell(new Phrase("Cilt No: Sayfa No: Kütük No", fontBold));
                table2.AddCell(new Phrase($":    ", fontBold));
                document.Add(table2);

                document.Add(new Paragraph($"Sigortalı/sigorta ettiren tarafından imzalanmamış tazminat teyit belgeleri işleme alınmayacaktır.", fontBold));

                #endregion

                document.Close();
                writer.Close();
                fs.Close();

                if (request.IsWebRequest)
                {
                    request.FileName = !string.IsNullOrEmpty(request.FileName) ? request.FileName : (billApprove.INSURED_NAME_SURNAME.ToUpper() + "_FATURA_ONAY_FORMU" + billApprove.PROVIDER_NAME + "_" + billApprove.PROVISION_DATE);
                    PrintOut(_path, request.FileName);
                }
                response.Url = _path;
                response.FileName = request.FileName;
                response.Code = "100";
            }
            catch (Exception ex) { response.Code = "999"; response.Message = ex.Message; }
            return response;
        }
    }
}
