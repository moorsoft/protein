﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Protein.Business.Abstract.Print;
using Protein.Common.Enums;
using Protein.Common.Extensions;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Business.Print.PolicyCertificate
{
    public class PolicyCertificate : PrintUtility, IPrint<PolicyCertificateReq>
    {
        public class MyPageHeader : PdfPageEventHelper
        {
            public string SubProductName { get; set; }
            public MyPageHeader(string SubProductName = "")
            {
                this.SubProductName = SubProductName;
            }
            public override void OnStartPage(PdfWriter writer, Document document)
            {
                base.OnStartPage(writer, document);
                BaseFont STF_Helvetica_Turkish = BaseFont.CreateFont("Helvetica", "CP1254", BaseFont.NOT_EMBEDDED);
                var HeaderTable = new PdfPTable(1);
                HeaderTable.TotalWidth = document.Right - document.Left;
                HeaderTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                //HeaderTable.WidthPercentage = 100;

                Font fontHeader = new Font(STF_Helvetica_Turkish, 11, Font.BOLD);
                var Headercell = new PdfPCell(new Phrase($"{SubProductName} SERTİFİKASI", fontHeader));
                Headercell.HorizontalAlignment = Element.ALIGN_CENTER;
                Headercell.Border = Rectangle.NO_BORDER;
                Headercell.Colspan = 1;
                Headercell.Padding = 10f;
                Headercell.PaddingTop = 60f;
                HeaderTable.AddCell(Headercell);
                document.Add(HeaderTable);
                //HeaderTable.WriteSelectedRows(0, -1, 150, document.Top, writer.DirectContent);
            }
        }

        public PrintResponse DoWork(PolicyCertificateReq request)
        {
            PrintResponse response = new PrintResponse();
            try
            {
                string InsuredIdForCoverageList = "";

                #region FillPrintData

                V_FormPolicyEndorsement formPolicyEndorsement = new GenericRepository<V_FormPolicyEndorsement>().FindBy($"POLICY_ID = {request.PolicyId} and ENDORSEMENT_ID = {request.EndorsementId}", orderby: "",
                     fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();

                List<V_InsuredEndorsement> formEndorsementInsured = new List<V_InsuredEndorsement>();
                if (request.InsuredIdList.IsNull())
                {
                    formEndorsementInsured = new GenericRepository<V_InsuredEndorsement>().FindBy($"POLICY_ID = {request.PolicyId} and ENDORSEMENT_NO = {formPolicyEndorsement.ENDORSEMET_NO}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                }
                else
                {
                    formEndorsementInsured = new GenericRepository<V_InsuredEndorsement>().FindBy($"INSURED_ID IN ({request.InsuredIdList})", orderby: "",fetchDeletedRows: true, fetchHistoricRows: true);
                }

                string insuredIdLst = string.Join(",", formEndorsementInsured.Select(il => il.INSURED_ID));
                List<V_FormInsuredCoverage> formInsuredCoverage = new List<V_FormInsuredCoverage>();
                List<V_InsuredExclusion> exclusionList = new List<V_InsuredExclusion>();

                if (!insuredIdLst.IsNull())
                {
                    exclusionList = new GenericRepository<V_InsuredExclusion>().FindBy($"INSURED_ID IN ({insuredIdLst}) AND (IS_OPEN_TO_PRINT=1 OR IS_OPEN_TO_PRINT IS NULL)", orderby: "INSURED_ID");
                    List<string> packageIdList = new List<string>();
                    foreach (var item in formEndorsementInsured)
                    {
                        if (packageIdList.Where(x => x == item.PACKAGE_ID.ToString()).FirstOrDefault() == null)
                        {
                            packageIdList.Add(item.PACKAGE_ID.ToString());
                        }
                    }
                    string a = string.Join(",", packageIdList);
                    formInsuredCoverage =new GenericRepository<V_FormInsuredCoverage>().FindBy($"PACKAGE_ID IN ({a}) AND ENDORSEMENT_ID = {request.EndorsementId} AND (IS_OPEN_TO_PRINT=1 OR IS_OPEN_TO_PRINT IS NULL)", orderby: "COVERAGE_ID ASC", fetchDeletedRows: true, fetchHistoricRows: true);
                    if (formInsuredCoverage.Count > 0) InsuredIdForCoverageList = formInsuredCoverage[0].INSURED_ID;
                }
                 
                #endregion

                string _path = "";
                if (request.IsWebRequest)
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints")))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints"));

                    _path = HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints") + "\\" + Guid.NewGuid() + ".pdf";
                }
                else { _path = request.LocalPath + "\\" + (!string.IsNullOrEmpty(request.FileName) ? request.FileName + ".pdf" : "POLICE_SERTIFIKASI_" + request.PolicyId + ".pdf"); };

                int cnt = 0;
                while (true)
                {
                    cnt++;
                    if (File.Exists(_path))
                    {
                        _path = _path.Replace(".pdf", "") + "_" + cnt.ToString() + ".pdf";
                    }
                    else break;
                }


                System.IO.FileStream fs = new FileStream(_path, FileMode.Create);
                Document document = new Document(PageSize.A4, 10, 10, 10, 20);

                PdfWriter writer = PdfWriter.GetInstance(document, fs);

                document.AddAuthor("TEST");
                document.AddCreator("TEST");
                document.AddKeywords("TEST");
                document.AddSubject("TEST");
                document.AddTitle("TEST");

                writer.PageEvent = new MyPageHeader(formPolicyEndorsement.SUBPRODUCT_NAME);

                document.Open();

                BaseFont STF_Helvetica_Turkish = BaseFont.CreateFont("Helvetica", "CP1254", BaseFont.NOT_EMBEDDED);
                foreach (var Insured in formEndorsementInsured)
                {
                    #region Header


                    #endregion
                    #region FillDetails

                    var mainTable = new PdfPTable(1);
                    mainTable.WidthPercentage = 100;
                    mainTable.DefaultCell.BorderWidth = 0.5f;

                    Font fontBold = new Font(STF_Helvetica_Turkish, 7, Font.BOLD);
                    Font fontNormal = new Font(STF_Helvetica_Turkish, 7, Font.NORMAL);

                    #region PolicyHeader

                    #region PolicyInfo

                    var mainTable2 = new PdfPTable(1);
                    mainTable2.SpacingBefore = 5;
                    mainTable2.WidthPercentage = 100;
                    mainTable2.DefaultCell.BorderWidth = 0.5f;

                    var table2 = new PdfPTable(8);
                    float[] widths2 = new float[] { 30f, 30f, 30f, 30f, 30f, 30f, 30f, 30f };
                    table2.SetWidths(widths2);

                    table2.DefaultCell.HorizontalAlignment = 0;

                    table2.WidthPercentage = 100;
                    //table.DefaultCell.Border = Rectangle.NO_BORDER;
                    table2.DefaultCell.Border = Rectangle.TOP_BORDER;

                    #region Row1
                    PdfPCell cell = new PdfPCell(new Phrase("Poliçe No:", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table2.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Sagmer Tarife No:", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table2.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Sagmer Tarife Ad:", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table2.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Poliçe Başlangıç Tarihi:", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table2.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Poliçe Bitiş Tarihi:", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table2.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Poliçe Süresi:", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table2.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Yeni İş Yenileme:", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table2.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Poliçe Tanzim Tarihi:", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table2.AddCell(cell);
                    #endregion

                    #region Row2
                    cell = new PdfPCell(new Phrase(formPolicyEndorsement.POLICY_NO, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table2.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formPolicyEndorsement.SBM_TARIFF_NO, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table2.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formPolicyEndorsement.SBM_TARIFF_NO, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table2.AddCell(cell);

                    cell = new PdfPCell(new Phrase(DateTime.Parse(formPolicyEndorsement.POLICY_START_DATE).ToString("dd.MM.yyyy"), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table2.AddCell(cell);

                    cell = new PdfPCell(new Phrase(DateTime.Parse(formPolicyEndorsement.POLICY_END_DATE).ToString("dd.MM.yyyy"), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table2.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formPolicyEndorsement.POLICY_PERIOD == "365" ? "1 YIL" : (formPolicyEndorsement.POLICY_PERIOD + " GÜN"), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table2.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formPolicyEndorsement.IS_RENEWAL, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table2.AddCell(cell);

                    cell = new PdfPCell(new Phrase(DateTime.Parse(formPolicyEndorsement.POLICY_ISSUE_DATE).ToString("dd.MM.yyyy"), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table2.AddCell(cell);
                    #endregion

                    #endregion
                    #region InsurerInfo
                    var mainTable3 = new PdfPTable(1);
                    mainTable3.SpacingBefore = 5;
                    mainTable3.WidthPercentage = 100;
                    mainTable3.DefaultCell.BorderWidth = 0.5f;


                    var table3 = new PdfPTable(6);
                    float[] widths3 = new float[] { 30f, 30f, 30f, 30f, 30f, 30f };
                    table3.SetWidths(widths3);

                    table3.DefaultCell.HorizontalAlignment = 0;

                    table3.WidthPercentage = 100;
                    //table.DefaultCell.Border = Rectangle.NO_BORDER;
                    table3.DefaultCell.Border = Rectangle.TOP_BORDER;

                    #region Row1
                    cell = new PdfPCell(new Phrase("Adı Soyadı / Ünvanı :", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table3.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formPolicyEndorsement.INSURER_NAME, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 5;
                    table3.AddCell(cell);

                    #endregion

                    #region Row2

                    cell = new PdfPCell(new Phrase("Telefon", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table3.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formPolicyEndorsement.INSURER_PHONE, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table3.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Vergi No:", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table3.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formPolicyEndorsement.INSURER_TAX_NO, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table3.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Tckn / Ykn:", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table3.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formPolicyEndorsement.INSURER_IDENTITY_NO, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table3.AddCell(cell);

                    #endregion

                    #region Row3

                    cell = new PdfPCell(new Phrase("Adres:", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 1;
                    table3.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formPolicyEndorsement.INSURER_ADDRESS, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 5;
                    table3.AddCell(cell);

                    #endregion

                    #endregion

                    #endregion
                    #region Insured
                    var mainTable5 = new PdfPTable(9);
                    float[] widthsMain5 = new float[] { 30f, 30f, 30f, 30f, 30f, 30f, 30f, 30f, 30f };
                    mainTable5.SetWidths(widthsMain5);

                    mainTable5.SpacingBefore = 5;
                    mainTable5.DefaultCell.HorizontalAlignment = 0;
                    mainTable5.WidthPercentage = 100;
                    mainTable5.DefaultCell.Border = Rectangle.TOP_BORDER;

                    #region Row

                    #region Header
                    cell = new PdfPCell(new Phrase("Sigortalı No", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthTop = 0.5f;
                    cell.BorderWidthLeft = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Adı Soyadı", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthLeft = 0;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Yakınlık", fontBold));
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthLeft = 0;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Doğum Tarihi", fontBold));
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthLeft = 0;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Tckn", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Colspan = 1;
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthLeft = 0;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Cinsiyet", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthBottom = 0;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Plan Adı", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthBottom = 0;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("İlk Kayıt Tarihi", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.BorderWidthLeft = 0;
                    cell.Colspan = 1;
                    cell.BorderWidthBottom = 0;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Geçerlilik Tarihi", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.BorderWidthLeft = 0;
                    cell.Colspan = 1; cell.BorderWidthBottom = 0;
                    cell.BorderWidthRight = 0;
                    mainTable5.AddCell(cell);
                    #endregion
                    #region Value
                    cell = new PdfPCell(new Phrase(Insured.CONTACT_ID.ToString(), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthLeft = 0.5f;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Insured.FIRST_NAME + " " + Insured.LAST_NAME, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(LookupHelper.GetLookupCodeByOrdinal(LookupTypes.Individual, Insured.INDIVIDUAL_TYPE), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Insured.BIRTHDATE != null ? ((DateTime)Insured.BIRTHDATE).ToString("dd.MM.yyyy") : "", fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Insured.IDENTITY_NO.ToString(), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(LookupHelper.GetLookupCodeByOrdinal(LookupTypes.Gender, Insured.GENDER), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Insured.PACKAGE_NAME, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Insured.FIRST_INSURED_DATE != null ? ((DateTime)Insured.FIRST_INSURED_DATE).ToString("dd.MM.yyyy") : "", fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Insured.COMPANY_ENTRANCE_DATE != null ? ((DateTime)Insured.COMPANY_ENTRANCE_DATE).ToString("dd.MM.yyyy") : "", fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.BorderWidthLeft = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(LookupHelper.GetLookupTextByOrdinal(LookupTypes.RenewalGuarantee, Insured.RENEWAL_GUARANTEE_TYPE), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.BorderWidthLeft = 0.5f;
                    cell.Colspan = 9;
                    mainTable5.AddCell(cell);

                    if (exclusionList != null)
                    {
                        var insuredExcList = exclusionList.Where(e => e.INSURED_ID == Insured.INSURED_ID).ToList();
                        int i = 0;
                        foreach (var item in insuredExcList)
                        {
                            i++;
                            cell = new PdfPCell(new Phrase("Muafiyet", fontBold));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.Border = Rectangle.TOP_BORDER;
                            cell.BorderWidthBottom = 0.5f;
                            cell.BorderWidthLeft = 0.5f;
                            cell.Colspan = 1;
                            mainTable5.AddCell(cell);

                            cell = new PdfPCell(new Phrase(i.ToString(), fontBold));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.Border = Rectangle.TOP_BORDER;
                            cell.BorderWidthBottom = 0.5f;
                            cell.BorderWidthLeft = 0.5f;
                            cell.Colspan = 1;
                            mainTable5.AddCell(cell);

                            cell = new PdfPCell(new Phrase(item.DESCRIPTION, fontNormal));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.Border = Rectangle.TOP_BORDER;
                            cell.BorderWidthBottom = 0.5f;
                            cell.BorderWidthLeft = 0.5f;
                            cell.Colspan = 7;
                            mainTable5.AddCell(cell);
                        }

                    }
                    #endregion

                    #endregion

                    #endregion
                    #region Coverage Table

                    var mainTable6 = new PdfPTable(8);
                    float[] widthsMain6 = new float[] { 30f, 30f, 30f, 30f, 30f, 30f, 30f, 30f };
                    mainTable6.SetWidths(widthsMain6);

                    mainTable6.SpacingBefore = 5;
                    mainTable6.DefaultCell.HorizontalAlignment = 0;
                    mainTable6.WidthPercentage = 100;
                    mainTable6.DefaultCell.Border = Rectangle.TOP_BORDER;
                    Paragraph parag = new Paragraph();
                    Phrase phrase = new Phrase();

                    var mainTable7 = new PdfPTable(1);
                    mainTable7.DefaultCell.HorizontalAlignment = 0;
                    mainTable7.WidthPercentage = 100;
                    mainTable7.DefaultCell.Border = Rectangle.TOP_BORDER;

                    var mainTable8 = new PdfPTable(1);
                    mainTable8.DefaultCell.HorizontalAlignment = 0;
                    mainTable8.WidthPercentage = 100;
                    mainTable8.DefaultCell.Border = Rectangle.NO_BORDER;

                    Font fontInsuredInfoBold = new Font(STF_Helvetica_Turkish, 9, Font.BOLD);
                    Font fontInsuredInfoNormal = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL);
                    #endregion
                    if (formPolicyEndorsement.ENDORSEMENT_TYPE == ((int)EndorsementType.BASLANGIC).ToString() || formPolicyEndorsement.ENDORSEMENT_TYPE == ((int)EndorsementType.SIGORTALI_GIRISI).ToString() || formPolicyEndorsement.ENDORSEMENT_TYPE == ((int)EndorsementType.TEMINAT_PLAN_DEGISIKLIGI).ToString() || formPolicyEndorsement.ENDORSEMENT_TYPE == ((int)EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI).ToString())
                    {
                        #region Coverage Table
                        foreach (var package in formInsuredCoverage.Where(x => x.INSURED_ID == Insured.INSURED_ID.ToString()).GroupBy(ic => ic.PACKAGE_ID).Select(i => i.First()).ToList())
                        {
                            #region Row1
                            parag = new Paragraph();

                            cell = new PdfPCell(new Phrase($"SEÇİLEN PLAN : {package.PACKAGE_NAME}", fontBold));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BorderWidthBottom = 0.5f;
                            cell.BorderWidthTop = 0.5f;
                            cell.BorderWidthLeft = 0.5f;
                            cell.Colspan = 8;
                            mainTable6.AddCell(cell);

                            #endregion

                            #region CoverageRows

                            var formInsuredCoverageIns = formInsuredCoverage.Where(x => x.INSURED_ID == package.INSURED_ID).ToList();
                            List<long> CoverageIDs = new List<long>();
                            CoverageIDs.Add(321); // HAVUZ TEMİNATI çıktıda görülmemeli
                            foreach (var MainCoverage in formInsuredCoverageIns.Where(x => x.IS_MAIN_COVERAGE == "1"))
                            {
                                if (CoverageIDs.Contains(long.Parse(MainCoverage.COVERAGE_ID)))
                                    continue;

                                CoverageIDs.Add(long.Parse(MainCoverage.COVERAGE_ID));

                                #region Row2

                                cell = new PdfPCell(new Phrase("", fontBold));
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.BorderWidthBottom = 0.5f;
                                cell.BorderWidthTop = 0;
                                cell.BorderWidthLeft = 0.5f;
                                cell.Colspan = 2;
                                mainTable6.AddCell(cell);

                                cell = new PdfPCell(new Phrase("Anlaşmalı Network", fontBold));
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.BorderWidthBottom = 0.5f;
                                cell.BorderWidthTop = 0;
                                cell.BorderWidthLeft = 0;
                                cell.Colspan = 3;
                                mainTable6.AddCell(cell);

                                cell = new PdfPCell(new Phrase("Anlaşmasız Network", fontBold));
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.BorderWidthBottom = 0.5f;
                                cell.BorderWidthTop = 0;
                                cell.BorderWidthLeft = 0;
                                cell.Colspan = 3;
                                mainTable6.AddCell(cell);

                                #endregion

                                #region Row3

                                cell = new PdfPCell(new Phrase(MainCoverage.COVERAGE_NAME, fontBold));
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.BorderWidthBottom = 0.5f;
                                cell.BorderWidthTop = 0;
                                cell.BorderWidthLeft = 0.5f;
                                cell.Colspan = 2;
                                mainTable6.AddCell(cell);

                                cell = new PdfPCell(new Phrase("Uygulama", fontBold));
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.BorderWidthBottom = 0.5f;
                                cell.BorderWidthTop = 0;
                                cell.BorderWidthLeft = 0;
                                cell.Colspan = 1;
                                mainTable6.AddCell(cell);

                                cell = new PdfPCell(new Phrase("Yıllık Limit", fontBold));
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.BorderWidthBottom = 0.5f;
                                cell.BorderWidthTop = 0;
                                cell.BorderWidthLeft = 0;
                                cell.Colspan = 1;
                                mainTable6.AddCell(cell);

                                cell = new PdfPCell(new Phrase("Katılım %", fontBold));
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.BorderWidthBottom = 0.5f;
                                cell.BorderWidthTop = 0;
                                cell.BorderWidthLeft = 0.5f;
                                cell.Colspan = 1;
                                mainTable6.AddCell(cell);

                                cell = new PdfPCell(new Phrase("Uygulama", fontBold));
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.BorderWidthBottom = 0.5f;
                                cell.BorderWidthTop = 0;
                                cell.BorderWidthLeft = 0;
                                cell.Colspan = 1;
                                mainTable6.AddCell(cell);

                                cell = new PdfPCell(new Phrase("Yıllık Limit", fontBold));
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.BorderWidthBottom = 0.5f;
                                cell.BorderWidthTop = 0;
                                cell.BorderWidthLeft = 0;
                                cell.Colspan = 1;
                                mainTable6.AddCell(cell);

                                cell = new PdfPCell(new Phrase("Katılım %", fontBold));
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.BorderWidthBottom = 0.5f;
                                cell.BorderWidthTop = 0;
                                cell.BorderWidthLeft = 0;
                                cell.Colspan = 1;
                                mainTable6.AddCell(cell);
                                #endregion

                                foreach (var coverageItem in formInsuredCoverageIns.Where(x => x.MAIN_COVERAGE_ID == MainCoverage.COVERAGE_ID).OrderBy(x => x.COVERAGE_NAME))
                                {
                                    if (CoverageIDs.Contains(long.Parse(coverageItem.COVERAGE_ID)))
                                        continue;

                                    CoverageIDs.Add(long.Parse(coverageItem.COVERAGE_ID));
                                    Font font;

                                    font = coverageItem.IS_MAIN_COVERAGE == "1" ? fontBold : fontNormal;

                                    cell = new PdfPCell(new Phrase(coverageItem.COVERAGE_NAME, font));
                                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    cell.BorderWidthBottom = 0.5f;
                                    cell.Padding = 5f;
                                    cell.BorderWidthTop = 0;
                                    cell.BorderWidthLeft = 0.5f;
                                    cell.BorderWidthRight = 0;
                                    cell.Colspan = 2;
                                    mainTable6.AddCell(cell);

                                    string aggrType = string.IsNullOrEmpty(coverageItem.AGR_AGREEMENT_TYPE) ? "" : ((ProteinEnums.AgreementType)int.Parse(coverageItem.AGR_AGREEMENT_TYPE)).ToString();

                                    cell = new PdfPCell(new Phrase(aggrType, font));
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    cell.BorderWidthBottom = 0.5f;
                                    cell.BorderWidthTop = 0;
                                    cell.BorderWidthLeft = 0;
                                    cell.BorderWidthRight = 0;
                                    cell.Colspan = 1;
                                    mainTable6.AddCell(cell);

                                    Phrase phraseAgrLimit = new Phrase();

                                    if (coverageItem.AGREED_NETWORK_LIMIT.Replace(",", ".").IsNumeric())
                                    {
                                        phraseAgrLimit = new Phrase(coverageItem.AGREED_NETWORK_LIMIT + " " + LookupHelper.GetLookupCodeByOrdinal(LookupTypes.Currency, coverageItem.AGR_CURRENCY_TYPE), font);
                                    }
                                    else
                                    {
                                        phraseAgrLimit = new Phrase(coverageItem.AGREED_NETWORK_LIMIT, font);
                                    }

                                    cell = new PdfPCell(phraseAgrLimit);
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    cell.BorderWidthBottom = 0.5f;
                                    cell.BorderWidthTop = 0;
                                    cell.BorderWidthLeft = 0;
                                    cell.BorderWidthRight = 0;
                                    cell.Colspan = 1;
                                    mainTable6.AddCell(cell);

                                    cell = new PdfPCell(new Phrase(coverageItem.AGREED_NETWORK_COINSURANCE, font));
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    cell.BorderWidthBottom = 0.5f;
                                    cell.BorderWidthTop = 0;
                                    cell.BorderWidthLeft = 0;
                                    cell.BorderWidthRight = 0;
                                    cell.Colspan = 1;
                                    mainTable6.AddCell(cell);

                                    string nagType = string.IsNullOrEmpty(coverageItem.NOT_AGREEMENT_TYPE) ? "" : ((ProteinEnums.AgreementType)int.Parse(coverageItem.NOT_AGREEMENT_TYPE)).ToString();

                                    cell = new PdfPCell(new Phrase(nagType, font));
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    cell.BorderWidthBottom = 0.5f;
                                    cell.BorderWidthTop = 0;
                                    cell.BorderWidthLeft = 0;
                                    cell.BorderWidthRight = 0;
                                    cell.Colspan = 1;
                                    mainTable6.AddCell(cell);

                                    Phrase phraseNagLimit = new Phrase();

                                    if (coverageItem.NOT_AGREED_NETWORK_LIMIT.Replace(",", ".").IsNumeric())
                                    {
                                        phraseNagLimit = new Phrase(coverageItem.NOT_AGREED_NETWORK_LIMIT + " " + LookupHelper.GetLookupCodeByOrdinal(LookupTypes.Currency, coverageItem.AGR_CURRENCY_TYPE), font);
                                    }
                                    else
                                    {
                                        phraseNagLimit = new Phrase(coverageItem.NOT_AGREED_NETWORK_LIMIT, font);
                                    }

                                    cell = new PdfPCell(phraseNagLimit);
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    cell.BorderWidthBottom = 0.5f;
                                    cell.BorderWidthTop = 0;
                                    cell.BorderWidthLeft = 0;
                                    cell.BorderWidthRight = 0;
                                    cell.Colspan = 1;
                                    mainTable6.AddCell(cell);

                                    cell = new PdfPCell(new Phrase(coverageItem.NOT_AGREED_NETWORK_COINSURANCE, font));
                                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    cell.BorderWidthBottom = 0.5f;
                                    cell.BorderWidthTop = 0;
                                    cell.BorderWidthLeft = 0;
                                    cell.BorderWidthRight = 0.5f;
                                    cell.Colspan = 1;
                                    mainTable6.AddCell(cell);
                                }
                            }
                            #endregion

                            #region PackageNote

                            var packageNote = new GenericRepository<V_PackageNote>().FindBy($"PACKAGE_ID={package.PACKAGE_ID} AND PACKAGE_NOTE_TYPE='{((int)NoteType.BASIM).ToString()}'", orderby: "PACKAGE_NOTE_ID ASC");

                            foreach (var item in packageNote)
                            {
                                phrase = new Phrase(item.DESCRIPTION.RemoveRepeatedWhiteSpace(), fontNormal);
                                phrase.Add(Environment.NewLine);
                                parag.Add(phrase);
                            }

                            cell = new PdfPCell();
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.BorderWidthBottom = 0.5f;
                            cell.BorderWidthTop = 0.5f;
                            cell.BorderWidthLeft = 0.5f;
                            cell.Colspan = 1;
                            cell.AddElement(parag);
                            mainTable7.AddCell(cell);
                            #endregion
                        }
                        #endregion
                    }
                    #endregion

                    #region Descriptions
                    var mainTable9 = new PdfPTable(1);
                    mainTable9.DefaultCell.HorizontalAlignment = 0;
                    mainTable9.WidthPercentage = 100;
                    mainTable9.PaddingTop = 10;
                    mainTable9.DefaultCell.Border = Rectangle.NO_BORDER;

                    parag = new Paragraph();
                    phrase = new Phrase();

                    phrase = new Phrase($"Bu sertifika yukarıda belirtilen Sigorta Ettiren ile {formPolicyEndorsement.COMPANY_NAME} arasında imzalanan Grup Sağlık Sigortası Sözleşmesi şartlarına göre düzenlenmiştir", fontBold);
                    parag.Add(phrase);
                    parag.Add(Environment.NewLine);
                    parag.Add(Environment.NewLine);

                    cell = new PdfPCell();
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthTop = 0;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthRight = 0;
                    cell.Colspan = 1;
                    cell.AddElement(parag);
                    mainTable9.AddCell(cell);

                    #endregion
                    #region Signatures

                    var mainTable10 = new PdfPTable(3);
                    float[] widths10 = new float[] { 30f, 30f, 30f };
                    mainTable10.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    mainTable10.WidthPercentage = 100;
                    mainTable10.PaddingTop = 10;
                    mainTable10.DefaultCell.Border = Rectangle.NO_BORDER;
                    mainTable10.SpacingBefore = 20f;

                    cell = new PdfPCell(new Phrase("SİGORTA ETTİREN", fontInsuredInfoBold));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthTop = 0;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthRight = 0;
                    cell.Colspan = 1;
                    mainTable10.AddCell(cell);

                    cell = new PdfPCell(new Phrase("ACENTE", fontInsuredInfoBold));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthTop = 0;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthRight = 0;
                    cell.Colspan = 1;
                    mainTable10.AddCell(cell);

                    cell = new PdfPCell(new Phrase("SİGORTACI", fontInsuredInfoBold));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthTop = 0;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthRight = 0;
                    cell.Colspan = 1;
                    mainTable10.AddCell(cell);


                    cell = new PdfPCell(new Phrase(formPolicyEndorsement.INSURER_NAME, fontInsuredInfoBold));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthTop = 0;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthRight = 0;
                    cell.Colspan = 1;
                    mainTable10.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_NAME, fontInsuredInfoBold));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthTop = 0;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthRight = 0;
                    cell.Colspan = 1;
                    mainTable10.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formPolicyEndorsement.COMPANY_NAME, fontInsuredInfoBold));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthTop = 0;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthRight = 0;
                    cell.Colspan = 1;
                    mainTable10.AddCell(cell);

                    #endregion

                    #region SetDocument
                    //mainTable.AddCell(table);
                    document.Add(mainTable);
                    mainTable2.AddCell(table2);
                    document.Add(mainTable2);
                    document.Add(new Paragraph("Sigorta Ettiren", fontBold));
                    mainTable3.AddCell(table3);
                    document.Add(mainTable3);
                    document.Add(new Paragraph(Environment.NewLine));
                    document.Add(new Paragraph("POLİÇE KAPSAMINDA OLAN SİGORTALI / SİGORTALILAR", fontBold));
                    document.Add(mainTable5);
                    document.Add(new Paragraph("TEMİNAT TABLOSU", fontBold));
                    document.Add(mainTable6);
                    document.Add(mainTable7);
                    document.Add(mainTable8);
                    document.Add(mainTable9);
                    document.Add(mainTable10);

                    #endregion

                    document.NewPage();
                
                }
                //table.SplitLate'e bak
                document.Close();
                writer.Close();
                fs.Close();
                if (request.IsWebRequest)
                {
                    request.FileName = !string.IsNullOrEmpty(request.FileName) ? request.FileName : ("POLICE_SERTIFIKASI_");
                    PrintOut(_path, request.FileName);
                }
                response.Url = _path;
                response.FileName = request.FileName;
                response.Code = "100";
            }
            catch (Exception ex) { response.Code = "999"; response.Message = ex.Message; }
            return response;
        }
    }
}
