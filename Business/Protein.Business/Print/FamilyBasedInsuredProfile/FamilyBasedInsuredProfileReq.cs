﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Print.FamilyBasedInsuredProfile
{
    public class FamilyBasedInsuredProfileReq : CommonReq
    {
        public long PolicyId { get; set; }
        public string FamilyNo { get; set; }
    }
}
