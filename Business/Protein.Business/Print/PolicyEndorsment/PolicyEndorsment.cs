﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Protein.Business.Abstract.Print;
using Protein.Common.Enums;
using Protein.Common.Extensions;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Business.Print.PolicyEndorsment
{
    public class MyPageHeader : PdfPageEventHelper
    {
        public string SubProductName { get; set; }
        public string EndorsementTypeName { get; set; }
        public MyPageHeader(string SubProductName = "",string EndorsementType="")
        {
            this.SubProductName = SubProductName;
            this.EndorsementTypeName = GetEndorsementTypeName(EndorsementType);
        }

        private string GetEndorsementTypeName(string endorsementType)
        {
            string name = "";
            switch (endorsementType)
            {
                case "3":
                    name = "GİRİŞ";
                    break;
                case "4":
                    name = "ÇIKIŞ";
                    break;
                case "10":
                    name = "PRİM FARKI";
                    break;
                case "2":
                case "11":
                case "12":
                case "13":
                case "14":
                case "15":
                    name = "POLİÇE İPTAL";
                    break;
                case "16":
                    name = "TEKRAR GİRİŞ";
                    break;
                case "18":
                    name = "POLİÇE TAHAKKUK";
                    break;
                case "20":
                    name = "TEMİNAT DEĞİŞİKLİĞİ";
                    break;
                default:
                    name = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Endorsement, endorsementType);
                    break;
            }
            return name;
        }

        public override void OnStartPage(PdfWriter writer, Document document)
        {
            base.OnStartPage(writer, document);
            BaseFont STF_Helvetica_Turkish = BaseFont.CreateFont("Helvetica", "CP1254", BaseFont.NOT_EMBEDDED);
            var HeaderTable = new PdfPTable(1);
            HeaderTable.TotalWidth = document.Right - document.Left;
            HeaderTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            //HeaderTable.WidthPercentage = 100;

            Font fontHeader = new Font(STF_Helvetica_Turkish, 11, Font.BOLD);
            var Headercell = new PdfPCell(new Phrase($"{SubProductName} {EndorsementTypeName} ZEYLİ", fontHeader));
            Headercell.HorizontalAlignment = Element.ALIGN_CENTER;
            Headercell.Border = Rectangle.NO_BORDER;
            Headercell.Colspan = 1;
            Headercell.Padding = 10f;
            Headercell.PaddingTop = 60f;
            HeaderTable.AddCell(Headercell);
            document.Add(HeaderTable);
            //HeaderTable.WriteSelectedRows(0, -1, 150, document.Top, writer.DirectContent);
        }
    }
    public class PolicyEndorsment : PrintUtility, IPrint<PolicyEndorsmentReq>
    {
        public PrintResponse DoWork(PolicyEndorsmentReq request)
        {
            PrintResponse response = new PrintResponse();
            try
            {
                string InsuredIdForCoverageList = "";

                #region FillPrintData

                V_FormPolicyEndorsement formPolicyEndorsement = new GenericRepository<V_FormPolicyEndorsement>().FindBy($"POLICY_ID = {request.PolicyId} and ENDORSEMENT_ID = {request.EndorsementId}", orderby: "",
                     fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();

                List<V_InsuredEndorsement> formEndorsementInsured = new GenericRepository<V_InsuredEndorsement>().FindBy($"POLICY_ID = {request.PolicyId} and ENDORSEMENT_NO = {formPolicyEndorsement.ENDORSEMET_NO}", orderby: "INSURED_ID DESC", fetchDeletedRows:true, fetchHistoricRows: true);

                string insuredIdLst = string.Join(",", formEndorsementInsured.Select(il => il.INSURED_ID));

                List<V_FormInsuredCoverage> formInsuredCoverage = new List<V_FormInsuredCoverage>();
                List<V_InsuredExclusion> exclusionList = new List<V_InsuredExclusion>();

                if (!insuredIdLst.IsNull())
                {
                    exclusionList = new GenericRepository<V_InsuredExclusion>().FindBy($"INSURED_ID IN ({insuredIdLst}) AND (IS_OPEN_TO_PRINT=1 OR IS_OPEN_TO_PRINT IS NULL)", orderby: "INSURED_ID");

                    if (formPolicyEndorsement.ENDORSEMENT_TYPE == ((int)EndorsementType.BASLANGIC).ToString() || formPolicyEndorsement.ENDORSEMENT_TYPE == ((int)EndorsementType.SIGORTALI_GIRISI).ToString() || formPolicyEndorsement.ENDORSEMENT_TYPE == ((int)EndorsementType.TEMINAT_PLAN_DEGISIKLIGI).ToString())
                    {
                        List<string> packageIdList = new List<string>();
                        foreach (var item in formEndorsementInsured)
                        {
                            if (packageIdList.Where(x => x == item.PACKAGE_ID.ToString()).FirstOrDefault() == null)
                            {
                                packageIdList.Add(item.PACKAGE_ID.ToString());
                            }
                        }
                        string a = string.Join(",", packageIdList);
                        formInsuredCoverage = new GenericRepository<V_FormInsuredCoverage>().FindBy($"PACKAGE_ID IN ({a}) AND ENDORSEMENT_ID = {request.EndorsementId} AND (IS_OPEN_TO_PRINT=1 OR IS_OPEN_TO_PRINT IS NULL)", orderby: "COVERAGE_ID ASC",
                        fetchDeletedRows: true, fetchHistoricRows: true);
                        if (formInsuredCoverage.Count > 0) InsuredIdForCoverageList = formInsuredCoverage[0].INSURED_ID;
                    }
                }
                #endregion

                string _path = "";
                if (request.IsWebRequest)
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints")))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints"));

                    _path = HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints") + "\\" + Guid.NewGuid() + ".pdf";
                }
                else { _path = request.LocalPath + "\\" + (!string.IsNullOrEmpty(request.FileName) ? request.FileName + ".pdf" : "SAGLIK_SIGORTASI_GIRIS_ZEYL_" + request.PolicyId + ".pdf"); };

                int cnt = 0;
                while (true)
                {
                    cnt++;
                    if (File.Exists(_path))
                    {
                        _path = _path.Replace(".pdf", "") + "_" + cnt.ToString() + ".pdf";
                    }
                    else break;
                }
                System.IO.FileStream fs = new FileStream(_path, FileMode.Create);
                Document document = new Document(PageSize.A4, 10, 10, 10, 20);

                PdfWriter writer = PdfWriter.GetInstance(document, fs);

                document.AddAuthor("TEST");
                document.AddCreator("TEST");
                document.AddKeywords("TEST");
                document.AddSubject("TEST");
                document.AddTitle("TEST");

                BaseFont STF_Helvetica_Turkish = BaseFont.CreateFont("Helvetica", "CP1254", BaseFont.NOT_EMBEDDED);
                #region Header

                writer.PageEvent = new MyPageHeader(SubProductName: formPolicyEndorsement.SUBPRODUCT_NAME, EndorsementType: formPolicyEndorsement.ENDORSEMENT_TYPE);
                document.Open();

                #endregion
                #region FillDetails

                var mainTable = new PdfPTable(1);
                mainTable.WidthPercentage = 100;
                mainTable.DefaultCell.BorderWidth = 0.5f;

                var table = new PdfPTable(6);
                float[] widths = new float[] { 30f, 30f, 30f, 30f, 30f, 30f };
                table.SetWidths(widths);

                table.DefaultCell.HorizontalAlignment = 0;

                table.WidthPercentage = 100;
                //table.DefaultCell.Border = Rectangle.NO_BORDER;
                table.DefaultCell.Border = Rectangle.TOP_BORDER;

                Font fontBold = new Font(STF_Helvetica_Turkish, 7, Font.BOLD);
                Font fontNormal = new Font(STF_Helvetica_Turkish, 7, Font.NORMAL);

                #region PolicyHeader

                #region Row1
                PdfPCell cell = new PdfPCell(new Phrase("Acente No:", fontBold));
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_NO, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Acente Adı:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Colspan = 1;
                cell.Border = Rectangle.NO_BORDER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_NAME, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Colspan = 3;
                cell.Border = Rectangle.NO_BORDER;
                table.AddCell(cell);
                #endregion

                #region Row2
                cell = new PdfPCell(new Phrase("Telefon:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Colspan = 1;
                cell.Border = Rectangle.NO_BORDER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_PHONE, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Fax:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_FAX, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Email:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_EMAIL, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);
                #endregion

                #region Row3
                cell = new PdfPCell(new Phrase("Web Adresi:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_WEB_ADDRESS, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Tic. Sicil No:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_TAX_NO, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Levha No:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_PLATE_NO, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);
                #endregion

                #region Row4
                cell = new PdfPCell(new Phrase("Acente Adres:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_ADDRESS, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 5;
                table.AddCell(cell);
                #endregion

                #endregion
                #region PolicyInfo

                var mainTable2 = new PdfPTable(1);
                mainTable2.SpacingBefore = 5;
                mainTable2.WidthPercentage = 100;
                mainTable2.DefaultCell.BorderWidth = 0.5f;

                var table2 = new PdfPTable(10);
                float[] widths2 = new float[] { 30f, 30f, 30f, 30f, 30f, 30f, 30f, 30f, 30f,30f };
                table2.SetWidths(widths2);

                table2.DefaultCell.HorizontalAlignment = 0;

                table2.WidthPercentage = 100;
                //table.DefaultCell.Border = Rectangle.NO_BORDER;
                table2.DefaultCell.Border = Rectangle.TOP_BORDER;

                #region Row1
                cell = new PdfPCell(new Phrase("Poliçe No:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Sagmer Tarife No:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Sagmer Tarife Ad:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Zeyil Başlangıç Tarihi:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Poliçe Başlangıç Tarihi:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Poliçe Bitiş Tarihi:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Poliçe Süresi:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Yeni İş Yenileme:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Zeyl Tanzim Tarihi:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Zeyl No:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);
                #endregion

                #region Row2
                cell = new PdfPCell(new Phrase(formPolicyEndorsement.POLICY_NO, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.SBM_TARIFF_NO, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.SBM_TARIFF_NO, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase(DateTime.Parse(formPolicyEndorsement.ENDORSEMENT_START_DATE).ToString("dd.MM.yyyy"), fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase(DateTime.Parse(formPolicyEndorsement.POLICY_START_DATE).ToString("dd.MM.yyyy"), fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase(DateTime.Parse(formPolicyEndorsement.POLICY_END_DATE).ToString("dd.MM.yyyy"), fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.POLICY_PERIOD == "365" ? "1 YIL" : (formPolicyEndorsement.POLICY_PERIOD + " GÜN"), fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.IS_RENEWAL, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase(DateTime.Parse(formPolicyEndorsement.ENDORSEMENT_DATE_OF_ISSUE).ToString("dd.MM.yyyy"), fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.ENDORSEMET_NO, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table2.AddCell(cell);
                #endregion

                #endregion
                #region InsurerInfo
                var mainTable3 = new PdfPTable(1);
                mainTable3.SpacingBefore = 5;
                mainTable3.WidthPercentage = 100;
                mainTable3.DefaultCell.BorderWidth = 0.5f;


                var table3 = new PdfPTable(6);
                float[] widths3 = new float[] { 30f, 30f, 30f, 30f, 30f, 30f };
                table3.SetWidths(widths3);

                table3.DefaultCell.HorizontalAlignment = 0;

                table3.WidthPercentage = 100;
                //table.DefaultCell.Border = Rectangle.NO_BORDER;
                table3.DefaultCell.Border = Rectangle.TOP_BORDER;

                #region Row1
                cell = new PdfPCell(new Phrase("Adı Soyadı / Ünvanı :", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table3.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.INSURER_NAME, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 5;
                table3.AddCell(cell);

                #endregion

                #region Row2

                cell = new PdfPCell(new Phrase("Telefon", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table3.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.INSURER_PHONE, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table3.AddCell(cell);


                cell = new PdfPCell(new Phrase("Vergi No:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table3.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.INSURER_TAX_NO, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table3.AddCell(cell);

                cell = new PdfPCell(new Phrase("Tckn / Ykn:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table3.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.INSURER_IDENTITY_NO, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table3.AddCell(cell);

                #endregion

                #region Row3

                cell = new PdfPCell(new Phrase("Adres:", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table3.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.INSURER_ADDRESS, fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 5;
                table3.AddCell(cell);

                #endregion

                #endregion
                #region Insured
                var mainTable5 = new PdfPTable(9);
                float[] widthsMain5 = new float[] { 30f, 30f, 30f, 30f, 30f, 30f, 30f, 30f, 30f };
                mainTable5.SetWidths(widthsMain5);

                mainTable5.SpacingBefore = 5;
                mainTable5.DefaultCell.HorizontalAlignment = 0;
                mainTable5.WidthPercentage = 100;
                mainTable5.DefaultCell.Border = Rectangle.TOP_BORDER;
                decimal totalAmount = 0;

                #region Row
                var insuredIdList = new List<Int64>();
                foreach (var Insured in formEndorsementInsured)
                {
                    if (insuredIdList.Contains(Insured.INSURED_ID))
                        continue;

                    insuredIdList.Add(Insured.INSURED_ID);

                    #region Header
                    cell = new PdfPCell(new Phrase("Sigortalı No", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthTop = 0.5f;
                    cell.BorderWidthLeft = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Adı Soyadı", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthLeft = 0;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Yakınlık", fontBold));
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthLeft = 0;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Doğum Tarihi", fontBold));
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthLeft = 0;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Tckn", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Colspan = 1;
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthLeft = 0;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Cinsiyet", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthBottom = 0;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Plan Adı", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthBottom = 0;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);


                    cell = new PdfPCell(new Phrase("İlk Kayıt Tarihi", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.BorderWidthLeft = 0;
                    cell.Colspan = 1;
                    cell.BorderWidthBottom = 0;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Prim (TL)", fontBold));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT; cell.BorderWidthLeft = 0;
                    cell.Colspan = 1; cell.BorderWidthBottom = 0;
                    cell.BorderWidthRight = 0;
                    mainTable5.AddCell(cell);
                    #endregion
                    #region Value
                    cell = new PdfPCell(new Phrase(Insured.CONTACT_ID.ToString(), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthLeft = 0.5f;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Insured.FIRST_NAME + " " + Insured.LAST_NAME, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(LookupHelper.GetLookupCodeByOrdinal(LookupTypes.Individual, Insured.INDIVIDUAL_TYPE), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Insured.BIRTHDATE != null ? ((DateTime)Insured.BIRTHDATE).ToString("dd.MM.yyyy") : "", fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Insured.IDENTITY_NO.ToString(), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(LookupHelper.GetLookupCodeByOrdinal(LookupTypes.Gender, Insured.GENDER), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Insured.PACKAGE_NAME, fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Insured.FIRST_INSURED_DATE != null ? ((DateTime)Insured.FIRST_INSURED_DATE).ToString("dd.MM.yyyy") : "", fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Insured.PREMIUM != null ? ((decimal)Insured.PREMIUM).ToString("#0.00") : "0", fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.BorderWidthLeft = 0.5f;
                    cell.Colspan = 1;
                    mainTable5.AddCell(cell);

                    totalAmount += Insured.PREMIUM != null ? (decimal)Insured.PREMIUM : 0;

                    cell = new PdfPCell(new Phrase(LookupHelper.GetLookupTextByOrdinal(LookupTypes.RenewalGuarantee, Insured.RENEWAL_GUARANTEE_TYPE), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BorderWidthBottom = 0.5f;
                    cell.BorderWidthLeft = 0.5f;
                    cell.Colspan = 9;
                    mainTable5.AddCell(cell);

                    if (exclusionList != null)
                    {
                        var insuredExcList = exclusionList.Where(e => e.INSURED_ID == Insured.INSURED_ID).ToList();
                        int i = 0;
                        foreach (var item in insuredExcList)
                        {
                            i++;
                            cell = new PdfPCell(new Phrase("Muafiyet", fontBold));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.Border = Rectangle.TOP_BORDER;
                            cell.BorderWidthBottom = 0.5f;
                            cell.BorderWidthLeft = 0.5f;
                            cell.Colspan = 1;
                            mainTable5.AddCell(cell);

                            cell = new PdfPCell(new Phrase(i.ToString(), fontBold));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.Border = Rectangle.TOP_BORDER;
                            cell.BorderWidthBottom = 0.5f;
                            cell.BorderWidthLeft = 0.5f;
                            cell.Colspan = 1;
                            mainTable5.AddCell(cell);

                            cell = new PdfPCell(new Phrase(item.DESCRIPTION, fontNormal));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.Border = Rectangle.TOP_BORDER;
                            cell.BorderWidthBottom = 0.5f;
                            cell.BorderWidthLeft = 0.5f;
                            cell.Colspan = 7;
                            mainTable5.AddCell(cell);
                        }

                    }

                    if (formPolicyEndorsement.ENDORSEMENT_TYPE == ((int)EndorsementType.SIGORTALI_GIRISI).ToString())
                    {
                        cell = new PdfPCell(new Phrase("Sigortalı Prim Bilgileri Detayı", fontBold));
                        cell.PaddingTop = 10;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        cell.Border = Rectangle.NO_BORDER;
                        cell.BorderWidthLeft = 0.5f;
                        cell.Colspan = 9;
                        mainTable5.AddCell(cell);

                        cell = new PdfPCell(new Phrase("Yıllık Prim", fontBold));
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        cell.BorderWidthLeft = 0.5f;
                        cell.BorderWidthTop = 0;
                        cell.BorderWidthRight = 0;
                        cell.Colspan = 1;
                        mainTable5.AddCell(cell);

                        cell = new PdfPCell(new Phrase(Insured.TOTAL_PREMIUM != null ? ((decimal)Insured.TOTAL_PREMIUM).ToString("#0.00") : "", fontNormal));
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        cell.BorderWidthTop = 0;
                        cell.BorderWidthLeft = 0;
                        cell.BorderWidthRight = 0;
                        cell.Colspan = 1;
                        mainTable5.AddCell(cell);

                        cell = new PdfPCell(new Phrase("İndirim / Ek Prim Açıklama ", fontBold));
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        cell.BorderWidthTop = 0;
                        cell.BorderWidthLeft = 0;
                        cell.BorderWidthRight = 0;
                        cell.Colspan = 1;
                        mainTable5.AddCell(cell);

                        cell = new PdfPCell(new Phrase("", fontNormal));
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        cell.BorderWidthTop = 0;
                        cell.BorderWidthLeft = 0;
                        cell.BorderWidthRight = 0;
                        cell.Colspan = 6;
                        mainTable5.AddCell(cell);
                    }
                    
                    #endregion
                }
                #endregion

                #endregion
                #region PaymentInfo

                var mainTable4 = new PdfPTable(2);
                float[] widthsMain4 = new float[] { 30f, 30f };
                mainTable4.SetWidths(widthsMain4);

                mainTable4.SpacingBefore = 5;
                mainTable4.WidthPercentage = 100;
                mainTable4.DefaultCell.Border = Rectangle.NO_BORDER;

                var table4 = new PdfPTable(3);
                var table5 = new PdfPTable(1);

                float[] widths4 = new float[] { 30f, 30f, 30f };
                table4.SetWidths(widths4);

                table4.DefaultCell.HorizontalAlignment = 0;
                table5.DefaultCell.HorizontalAlignment = 2;

                table4.WidthPercentage = 60;
                table5.WidthPercentage = 20;
                //table.DefaultCell.Border = Rectangle.NO_BORDER;
                table4.DefaultCell.Border = Rectangle.TOP_BORDER;
                table5.DefaultCell.Border = Rectangle.TOP_BORDER;

                #region Row1 Header
                cell = new PdfPCell(new Phrase("Ödeme Tarihi :", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Ödeme Şekli :", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Tutar (TL) :", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table4.AddCell(cell);
                #endregion

                #region Rows  foreach

                List<Installment> formEndorsementPayment = new GenericRepository<Installment>().FindBy($"POLICY_ID = {request.PolicyId} and ENDORSEMENT_ID = {request.EndorsementId}", orderby: "DUE_DATE ASC");

                int totalInstallment = formEndorsementPayment.Count;

                foreach (var itemPayment in formEndorsementPayment)
                {
                    cell = new PdfPCell(new Phrase(itemPayment.DueDate.ToString("dd-MM-yyyy"), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidth = 0.5f;
                    cell.Colspan = 1;
                    table4.AddCell(cell);

                    cell = new PdfPCell(new Phrase((formPolicyEndorsement.POLICY_PAYMENT_TYPE != ((int)PaymentType.DIGER).ToString() ? LookupHelper.GetLookupTextByOrdinal(LookupTypes.Payment, formPolicyEndorsement.POLICY_PAYMENT_TYPE) : (formPolicyEndorsement.POLICY_PAYMENT_TYPE_TEXT.IsNull() ? "DİĞER" : formPolicyEndorsement.POLICY_PAYMENT_TYPE_TEXT)), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidth = 0.5f;
                    cell.Colspan = 1;
                    table4.AddCell(cell);

                    cell = new PdfPCell(new Phrase(itemPayment.Amount.ToString("#0.00"), fontNormal));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidth = 0.5f;
                    cell.Colspan = 1;
                    table4.AddCell(cell);
                }
                #endregion

                #region TotalPaymentCol
                cell = new PdfPCell(new Phrase("Toplam", fontBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table5.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.ENDORSEMENT_PREMIUM + " TL", fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table5.AddCell(cell);

                cell = new PdfPCell(new Phrase(totalInstallment.ToString() + " TAKSİT", fontNormal));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.Border = Rectangle.NO_BORDER;
                cell.Colspan = 1;
                table5.AddCell(cell);
                #endregion

                #endregion
                #region Coverage Table Info
                var mainTable6 = new PdfPTable(8);
                float[] widthsMain6 = new float[] { 30f, 30f, 30f, 30f, 30f, 30f, 30f, 30f };
                mainTable6.SetWidths(widthsMain6);

                mainTable6.SpacingBefore = 5;
                mainTable6.DefaultCell.HorizontalAlignment = 0;
                mainTable6.WidthPercentage = 100;
                mainTable6.DefaultCell.Border = Rectangle.TOP_BORDER;
                Paragraph parag = new Paragraph();
                Phrase phrase = new Phrase();

                var mainTable7 = new PdfPTable(1);
                mainTable7.DefaultCell.HorizontalAlignment = 0;
                mainTable7.WidthPercentage = 100;
                mainTable7.DefaultCell.Border = Rectangle.TOP_BORDER;

                var mainTable8 = new PdfPTable(1);
                mainTable8.DefaultCell.HorizontalAlignment = 0;
                mainTable8.WidthPercentage = 100;
                mainTable8.DefaultCell.Border = Rectangle.NO_BORDER;

                Font fontInsuredInfoBold = new Font(STF_Helvetica_Turkish, 9, Font.BOLD);
                Font fontInsuredInfoNormal = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL);
                #endregion
                if (formPolicyEndorsement.ENDORSEMENT_TYPE == ((int)EndorsementType.BASLANGIC).ToString() || formPolicyEndorsement.ENDORSEMENT_TYPE == ((int)EndorsementType.SIGORTALI_GIRISI).ToString() || formPolicyEndorsement.ENDORSEMENT_TYPE == ((int)EndorsementType.TEMINAT_PLAN_DEGISIKLIGI).ToString())
                {
                    #region Coverage Table
                    foreach (var package in formInsuredCoverage.GroupBy(ic => ic.PACKAGE_ID).Select(i => i.First()).ToList())
                    {
                        #region Row1

                        cell = new PdfPCell(new Phrase($"SEÇİLEN PLAN : {package.PACKAGE_NAME}", fontBold));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.BorderWidthBottom = 0.5f;
                        cell.BorderWidthTop = 0.5f;
                        cell.BorderWidthLeft = 0.5f;
                        cell.Colspan = 8;
                        mainTable6.AddCell(cell);

                        #endregion

                        #region CoverageRows

                        formInsuredCoverage = formInsuredCoverage.Where(x => x.INSURED_ID == package.INSURED_ID).ToList();
                        formInsuredCoverage = formInsuredCoverage.Where(x => x.INSURED_ID == package.INSURED_ID).ToList();
                        List<long> CoverageIDs = new List<long>();
                        CoverageIDs.Add(321); // HAVUZ TEMİNATI çıktıda görülmemeli

                        foreach (var MainCoverage in formInsuredCoverage.Where(x => x.IS_MAIN_COVERAGE == "1"))
                        {
                            if (CoverageIDs.Contains(long.Parse(MainCoverage.COVERAGE_ID)))
                                continue;

                            CoverageIDs.Add(long.Parse(MainCoverage.COVERAGE_ID));

                            #region Row2

                            cell = new PdfPCell(new Phrase("", fontBold));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.BorderWidthBottom = 0.5f;
                            cell.BorderWidthTop = 0;
                            cell.BorderWidthLeft = 0.5f;
                            cell.Colspan = 2;
                            mainTable6.AddCell(cell);

                            cell = new PdfPCell(new Phrase("Anlaşmalı Network", fontBold));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BorderWidthBottom = 0.5f;
                            cell.BorderWidthTop = 0;
                            cell.BorderWidthLeft = 0;
                            cell.Colspan = 3;
                            mainTable6.AddCell(cell);

                            cell = new PdfPCell(new Phrase("Anlaşmasız Network", fontBold));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BorderWidthBottom = 0.5f;
                            cell.BorderWidthTop = 0;
                            cell.BorderWidthLeft = 0;
                            cell.Colspan = 3;
                            mainTable6.AddCell(cell);

                            #endregion

                            #region Row3

                            cell = new PdfPCell(new Phrase(MainCoverage.COVERAGE_NAME, fontBold));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.BorderWidthBottom = 0.5f;
                            cell.BorderWidthTop = 0;
                            cell.BorderWidthLeft = 0.5f;
                            cell.Colspan = 2;
                            mainTable6.AddCell(cell);

                            cell = new PdfPCell(new Phrase("Uygulama", fontBold));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BorderWidthBottom = 0.5f;
                            cell.BorderWidthTop = 0;
                            cell.BorderWidthLeft = 0;
                            cell.Colspan = 1;
                            mainTable6.AddCell(cell);

                            cell = new PdfPCell(new Phrase("Yıllık Limit", fontBold));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BorderWidthBottom = 0.5f;
                            cell.BorderWidthTop = 0;
                            cell.BorderWidthLeft = 0;
                            cell.Colspan = 1;
                            mainTable6.AddCell(cell);

                            cell = new PdfPCell(new Phrase("Katılım %", fontBold));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BorderWidthBottom = 0.5f;
                            cell.BorderWidthTop = 0;
                            cell.BorderWidthLeft = 0.5f;
                            cell.Colspan = 1;
                            mainTable6.AddCell(cell);

                            cell = new PdfPCell(new Phrase("Uygulama", fontBold));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BorderWidthBottom = 0.5f;
                            cell.BorderWidthTop = 0;
                            cell.BorderWidthLeft = 0;
                            cell.Colspan = 1;
                            mainTable6.AddCell(cell);

                            cell = new PdfPCell(new Phrase("Yıllık Limit", fontBold));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BorderWidthBottom = 0.5f;
                            cell.BorderWidthTop = 0;
                            cell.BorderWidthLeft = 0;
                            cell.Colspan = 1;
                            mainTable6.AddCell(cell);

                            cell = new PdfPCell(new Phrase("Katılım %", fontBold));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BorderWidthBottom = 0.5f;
                            cell.BorderWidthTop = 0;
                            cell.BorderWidthLeft = 0;
                            cell.Colspan = 1;
                            mainTable6.AddCell(cell);
                            #endregion

                            foreach (var coverageItem in formInsuredCoverage.Where(x => x.MAIN_COVERAGE_ID == MainCoverage.COVERAGE_ID).OrderBy(x => x.COVERAGE_NAME))
                            {
                                if (CoverageIDs.Contains(long.Parse(coverageItem.COVERAGE_ID)))
                                    continue;

                                CoverageIDs.Add(long.Parse(coverageItem.COVERAGE_ID));
                                Font font;

                                font = coverageItem.IS_MAIN_COVERAGE == "1" ? fontBold : fontNormal;

                                cell = new PdfPCell(new Phrase(coverageItem.COVERAGE_NAME, font));
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.BorderWidthBottom = 0.5f;
                                cell.Padding = 5f;
                                cell.BorderWidthTop = 0;
                                cell.BorderWidthLeft = 0.5f;
                                cell.BorderWidthRight = 0;
                                cell.Colspan = 2;
                                mainTable6.AddCell(cell);

                                string aggrType = string.IsNullOrEmpty(coverageItem.AGR_AGREEMENT_TYPE) ? "" : ((ProteinEnums.AgreementType)int.Parse(coverageItem.AGR_AGREEMENT_TYPE)).ToString();

                                cell = new PdfPCell(new Phrase(aggrType, font));
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.BorderWidthBottom = 0.5f;
                                cell.BorderWidthTop = 0;
                                cell.BorderWidthLeft = 0;
                                cell.BorderWidthRight = 0;
                                cell.Colspan = 1;
                                mainTable6.AddCell(cell);

                                Phrase phraseAgrLimit = new Phrase();

                                if (coverageItem.AGREED_NETWORK_LIMIT.Replace(",", ".").IsNumeric())
                                {
                                    phraseAgrLimit = new Phrase(coverageItem.AGREED_NETWORK_LIMIT + " " + LookupHelper.GetLookupCodeByOrdinal(LookupTypes.Currency, coverageItem.AGR_CURRENCY_TYPE), font);
                                }
                                else
                                {
                                    phraseAgrLimit = new Phrase(coverageItem.AGREED_NETWORK_LIMIT, font);
                                }

                                cell = new PdfPCell(phraseAgrLimit);
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.BorderWidthBottom = 0.5f;
                                cell.BorderWidthTop = 0;
                                cell.BorderWidthLeft = 0;
                                cell.BorderWidthRight = 0;
                                cell.Colspan = 1;
                                mainTable6.AddCell(cell);

                                cell = new PdfPCell(new Phrase(coverageItem.AGREED_NETWORK_COINSURANCE, font));
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.BorderWidthBottom = 0.5f;
                                cell.BorderWidthTop = 0;
                                cell.BorderWidthLeft = 0;
                                cell.BorderWidthRight = 0;
                                cell.Colspan = 1;
                                mainTable6.AddCell(cell);

                                string nagType = string.IsNullOrEmpty(coverageItem.NOT_AGREEMENT_TYPE) ? "" : ((ProteinEnums.AgreementType)int.Parse(coverageItem.NOT_AGREEMENT_TYPE)).ToString();

                                cell = new PdfPCell(new Phrase(nagType, font));
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.BorderWidthBottom = 0.5f;
                                cell.BorderWidthTop = 0;
                                cell.BorderWidthLeft = 0;
                                cell.BorderWidthRight = 0;
                                cell.Colspan = 1;
                                mainTable6.AddCell(cell);

                                Phrase phraseNagLimit = new Phrase();

                                if (coverageItem.NOT_AGREED_NETWORK_LIMIT.Replace(",", ".").IsNumeric())
                                {
                                    phraseNagLimit = new Phrase(coverageItem.NOT_AGREED_NETWORK_LIMIT + " " + LookupHelper.GetLookupCodeByOrdinal(LookupTypes.Currency, coverageItem.AGR_CURRENCY_TYPE), font);
                                }
                                else
                                {
                                    phraseNagLimit = new Phrase(coverageItem.NOT_AGREED_NETWORK_LIMIT, font);
                                }

                                cell = new PdfPCell(phraseNagLimit);
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.BorderWidthBottom = 0.5f;
                                cell.BorderWidthTop = 0;
                                cell.BorderWidthLeft = 0;
                                cell.BorderWidthRight = 0;
                                cell.Colspan = 1;
                                mainTable6.AddCell(cell);

                                cell = new PdfPCell(new Phrase(coverageItem.NOT_AGREED_NETWORK_COINSURANCE, font));
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.BorderWidthBottom = 0.5f;
                                cell.BorderWidthTop = 0;
                                cell.BorderWidthLeft = 0;
                                cell.BorderWidthRight = 0.5f;
                                cell.Colspan = 1;
                                mainTable6.AddCell(cell);
                            }
                        }
                        #endregion

                        #region PackageNote

                        var packageNote = new GenericRepository<V_PackageNote>().FindBy($"PACKAGE_ID={package.PACKAGE_ID} AND PACKAGE_NOTE_TYPE='{((int)NoteType.BASIM).ToString()}'", orderby: "");

                        foreach (var item in packageNote)
                        {
                            phrase = new Phrase(item.DESCRIPTION.RemoveRepeatedWhiteSpace(), fontNormal);
                            phrase.Add(Environment.NewLine);
                            parag.Add(phrase);
                        }

                        cell = new PdfPCell();
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        cell.BorderWidthBottom = 0.5f;
                        cell.BorderWidthTop = 0.5f;
                        cell.BorderWidthLeft = 0.5f;
                        cell.Colspan = 1;
                        cell.AddElement(parag);
                        mainTable7.AddCell(cell);
                        #endregion
                    }

                    parag = new Paragraph();
                    phrase = new Phrase();
                    phrase.Add(new Chunk("NOT: ", fontBold));
                    phrase.Add(new Phrase("ACİL OLMAYAN HASTANE YATIŞLARINDA 24 SAAT ÖNCESİNDEN PROVİZYON ALINMASI, İŞLEMLERİNİZİN DAHA HIZLI YAPILMASINI SAĞLAYARAK YATIŞ SIRASINDA HASTANEDE PROVİZYON İÇİN BEKLEMENİZİ ORTADAN KALDIRACAKTIR. 24/365 KARA VE DENİZ AMBULANSI İLE ÜCRETSİZ TIBBI DANIŞMA TEMİNATA DAHİLDİR.", fontBold));
                    parag.Add(phrase);
                    parag.Add(Environment.NewLine);
                    parag.Add(Environment.NewLine);
                    cell = new PdfPCell();
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthTop = 0;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthRight = 0;
                    cell.Colspan = 1;
                    cell.AddElement(parag);
                    mainTable8.AddCell(cell);
                    #endregion
                }
                #region Descriptions
                var mainTable9 = new PdfPTable(1);
                mainTable9.DefaultCell.HorizontalAlignment = 0;
                mainTable9.WidthPercentage = 100;
                mainTable9.PaddingTop = 10;
                mainTable9.DefaultCell.Border = Rectangle.NO_BORDER;

                parag = new Paragraph();
                parag.SetLeading(10, 0);
                phrase = new Phrase(@" Açıklamalar", fontBold);
                parag.Add(phrase);
                parag.Add(Environment.NewLine);
                parag.Add(Environment.NewLine);

                var companyNote = new GenericRepository<V_CompanyNote>().FindBy($"COMPANY_ID={formPolicyEndorsement.COMPANY_ID} AND NOTE_TYPE='{((int)NoteType.BASIM).ToString()}'", orderby: "NOTE_ID ASC");

                foreach (var item in companyNote)
                {
                    phrase = new Phrase(item.NOTE_DESCRIPTION.RemoveRepeatedWhiteSpace(), fontBold);
                    phrase.Add(Environment.NewLine);
                    phrase.Add(Environment.NewLine);
                    parag.Add(phrase);
                }


                cell = new PdfPCell();
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.Colspan = 1;
                cell.AddElement(parag);
                mainTable9.AddCell(cell);

                #endregion
                #region Signatures

                var mainTable10 = new PdfPTable(3);
                float[] widths10 = new float[] { 30f, 30f, 30f };
                mainTable10.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                mainTable10.WidthPercentage = 100;
                mainTable10.PaddingTop = 10;
                mainTable10.DefaultCell.Border = Rectangle.NO_BORDER;
                mainTable10.SpacingBefore = 20f;

                cell = new PdfPCell(new Phrase("SİGORTA ETTİREN", fontInsuredInfoBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.Colspan = 1;
                mainTable10.AddCell(cell);

                cell = new PdfPCell(new Phrase("ACENTE", fontInsuredInfoBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.Colspan = 1;
                mainTable10.AddCell(cell);

                cell = new PdfPCell(new Phrase("SİGORTACI", fontInsuredInfoBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.Colspan = 1;
                mainTable10.AddCell(cell);


                cell = new PdfPCell(new Phrase(formPolicyEndorsement.INSURER_NAME, fontInsuredInfoBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.Colspan = 1;
                mainTable10.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.AGENCY_NAME, fontInsuredInfoBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.Colspan = 1;
                mainTable10.AddCell(cell);

                cell = new PdfPCell(new Phrase(formPolicyEndorsement.COMPANY_NAME, fontInsuredInfoBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 0;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.Colspan = 1;
                mainTable10.AddCell(cell);

                #endregion

                #region SetDocument
                mainTable.AddCell(table);
                document.Add(mainTable);
                mainTable2.AddCell(table2);
                document.Add(mainTable2);
                document.Add(new Paragraph("Sigorta Ettiren", fontBold));
                mainTable3.AddCell(table3);
                document.Add(mainTable3);
                document.Add(new Paragraph("Ödeme Planları", fontBold));
                mainTable4.AddCell(table4);
                mainTable4.AddCell(table5);
                document.Add(mainTable4);
                document.Add(new Paragraph("POLİÇE KAPSAMINDA OLAN SİGORTALI / SİGORTALILAR", fontBold));
                document.Add(mainTable5);
                if (formPolicyEndorsement.ENDORSEMENT_TYPE == ((int)EndorsementType.BASLANGIC).ToString() || formPolicyEndorsement.ENDORSEMENT_TYPE == ((int)EndorsementType.SIGORTALI_GIRISI).ToString() || formPolicyEndorsement.ENDORSEMENT_TYPE == ((int)EndorsementType.TEMINAT_PLAN_DEGISIKLIGI).ToString())
                {
                    document.Add(new Paragraph("TEMİNAT TABLOSU", fontBold));

                    document.Add(mainTable6);
                    document.Add(mainTable7);
                    document.Add(mainTable8);
                }
                document.Add(mainTable9);
                document.Add(mainTable10);

                #endregion

                #endregion
                //table.SplitLate'e bak
                document.Close();
                writer.Close();
                fs.Close();

                if (request.IsWebRequest)
                {
                    request.FileName = !string.IsNullOrEmpty(request.FileName) ? request.FileName : ($"{formPolicyEndorsement.SUBPRODUCT_NAME}_{formPolicyEndorsement.ENDORSEMET_NO.ToString()}_NOLU_{LookupHelper.GetLookupTextByOrdinal(LookupTypes.Endorsement,formPolicyEndorsement.ENDORSEMENT_TYPE)}_ZEYL");
                    PrintOut(_path, request.FileName);
                }
                response.Url = _path;
                response.FileName = request.FileName;
                response.Code = "100";
            }
            catch (Exception ex) { response.Code = "999"; response.Message = ex.Message; }
            return response;
        }
    }
}
