﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Protein.Business.Abstract.Print;
using Protein.Common.Extensions;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Business.Print.AcquittanceForm
{
    public class AcquittanceForm : PrintUtility, IPrint<PrintAcquittanceFormReq>
    {
        public PrintResponse DoWork(PrintAcquittanceFormReq request)
        {
            PrintResponse response = new PrintResponse();
            try
            {
                #region FillPrintData
                V_FormProvisionRelease acquittance = new GenericRepository<V_FormProvisionRelease>().FindBy($"CLAIM_ID = {request.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (acquittance == null) { response.Code = "999"; response.Message = "Kayıt Bulunamadı"; return response; }
                List<V_FormProvisiomnReleaseList> acquittanceLists = new GenericRepository<V_FormProvisiomnReleaseList>().FindBy($"CLAIM_ID = {request.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                if (acquittanceLists.Count < 1)
                { response.Code = "999"; response.Message = "Kayıt Bulunamadı"; return response; }
                #endregion

                string _path = "";
                if (request.IsWebRequest)
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints")))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints"));

                    _path = HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints") + "\\" + Guid.NewGuid() + ".pdf";
                }
                else { _path = request.LocalPath + "\\" + (!string.IsNullOrEmpty(request.FileName) ? request.FileName + ".pdf" : (acquittance.INSURED_NAME_SURNAME.ToUpper() + "_IBRANAME_FORMU" + acquittance.PROVIDER_NAME + "_" + acquittance.EVENT_DATE)); };

                int cnt = 0;
                while (true)
                {
                    cnt++;
                    if (File.Exists(_path))
                    {
                        _path = _path.Replace(".pdf", "") + "_" + cnt.ToString() + ".pdf";
                    }
                    else break;
                }

                FileStream fs = new FileStream(_path, FileMode.Create);
                Document document = new Document(PageSize.A4, 20, 20, 50, 20);

                PdfWriter writer = PdfWriter.GetInstance(document, fs);

                document.AddAuthor("İBRANAME");
                document.AddCreator("İBRANAME");
                document.AddKeywords("İBRANAME");
                document.AddSubject("İBRANAME");
                document.AddTitle("İBRANAME");
                document.Open();

                Image image = Image.GetInstance(GetCompanyLogoPath(acquittance.COMPANY_NAME));

                image.ScalePercent(10f);
                document.Add(image);

                BaseFont STF_Helvetica_Turkish = BaseFont.CreateFont("Helvetica", "CP1254", BaseFont.NOT_EMBEDDED);

                #region Header
                Font fontHeader = new Font(STF_Helvetica_Turkish, 20, Font.BOLD);
                Paragraph paragraphHeader = new Paragraph("İBRANAME FORMU", fontHeader);
                paragraphHeader.Alignment = 1;
                paragraphHeader.Font.Size = 20;
                document.Add(paragraphHeader);
                document.Add(new Paragraph());
                Paragraph ds = new Paragraph();
                Phrase phr = new Phrase(Environment.NewLine);
                ds.Add(phr);
                ds.Add(phr);
                //ds.Add(phr);
                //ds.Add(phr);
                document.Add(ds);
                #endregion

                #region FillDetails
                var table2 = new PdfPTable(2);
                float[] widths2 = new float[] { 12f, 50f };
                table2.SetWidths(widths2);

                table2.DefaultCell.HorizontalAlignment = 0;

                table2.WidthPercentage = 100;
                table2.DefaultCell.Border = Rectangle.NO_BORDER;

                var cellDetail = new PdfPCell();
                cellDetail.Colspan = 2;
                cellDetail.PaddingTop = 30;
                cellDetail.Border = 0;
                cellDetail.PaddingLeft = 0;
                cellDetail.PaddingRight = 0;
                cellDetail.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                table2.AddCell(cellDetail);

                Font fontSubText = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL);
                Font fontSubValue = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL);


                Phrase DateText = new Phrase("Olay Tarihi", fontSubText);
                Phrase ClaimText = new Phrase("Provizyon No", fontSubText);
                Phrase ProviderText = new Phrase("Kurum Adı", fontSubText);
                Phrase InsuredText = new Phrase("Sigortalı Adı Soyadı", fontSubText);
                Phrase RegistrationNoText = new Phrase("Müşteri Numarası", fontSubText);
                Phrase ProductNameText = new Phrase("Ürün Adı", fontSubText);
                Phrase PolicyNoText = new Phrase("Poliçe Numarası", fontSubText);
                Phrase ICDText = new Phrase("Tanı", fontSubText);

                Phrase DateValue = new Phrase($":    {(acquittance.EVENT_DATE.IsDateTime() ? DateTime.Parse(acquittance.EVENT_DATE).ToString("dd.MM.yyyy") : "") }", fontSubValue);
                Phrase ClaimValue = new Phrase($":    {acquittance.CLAIM_ID}", fontSubValue);
                Phrase ProviderValue = new Phrase($":    {acquittance.PROVIDER_NAME}", fontSubValue);
                Phrase InsuredValue = new Phrase($":    {acquittance.INSURED_NAME_SURNAME}", fontSubValue);
                Phrase RegistrationValue = new Phrase($":    {acquittance.INSURED_CONTACT_ID}", fontSubValue);
                Phrase ProductNameValue = new Phrase($":    {acquittance.PRODUCT_NAME}", fontSubValue);
                Phrase PolicyNoValue = new Phrase($":    {acquittance.POLICY_NUMBER}", fontSubValue);

                table2.AddCell(DateText);
                table2.AddCell(DateValue);

                table2.AddCell(ClaimText);
                table2.AddCell(ClaimValue);

                table2.AddCell(ProviderText);
                table2.AddCell(ProviderValue);

                table2.AddCell(InsuredText);
                table2.AddCell(InsuredValue);

                table2.AddCell(RegistrationNoText);
                table2.AddCell(RegistrationValue);

                table2.AddCell(ProductNameText);
                table2.AddCell(ProductNameValue);

                table2.AddCell(PolicyNoText);
                table2.AddCell(PolicyNoValue);

                table2.AddCell(ICDText);
                int i = 0;
                if (acquittance.ICD_LIST!=null)
                {
                    foreach (var item in acquittance.ICD_LIST.Split('#'))
                    {
                        Phrase ICDValue = new Phrase($"{(i == 0 ? ":" : " ")}    {item}", fontSubValue);
                        table2.AddCell(ICDValue);
                        table2.AddCell((new Phrase("", fontSubValue)));
                        i++;
                    }

                }

                document.Add(table2);
                #endregion

                #region FillInfo
                var table3 = new PdfPTable(7);
                table3.DefaultCell.HorizontalAlignment = 0;
                table3.WidthPercentage = 100;

                Font fontBold = new Font(STF_Helvetica_Turkish, 9, Font.BOLD);
                Font fontNormal = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL);

                document.Add(ds);

                table3.AddCell(new Phrase("Teminat Adı", fontBold));
                table3.AddCell(new Phrase("Katılım Oranı (%)", fontBold));
                table3.AddCell(new Phrase("Fatura No", fontBold));
                table3.AddCell(new Phrase("Fatura Tarihi", fontBold));
                table3.AddCell(new Phrase("Anlaşmalı Tutar", fontBold));
                table3.AddCell(new Phrase("Onay Tutar", fontBold));
                table3.AddCell(new Phrase("Sigortali Payı", fontBold));

                decimal totalCOINSURANCE = 0, totalREQUESTED = 0, totalCONFIRMED = 0;
                List<long> coverageIdList = new List<long>();
                foreach (var item in acquittanceLists)
                {
                    if (!coverageIdList.Contains(item.COVERAGE_ID))
                    {
                        table3.AddCell(new Phrase(item.COVERAGE_NAME, fontNormal));
                        table3.AddCell(new Phrase(item.COINSURANCE, fontNormal));
                        table3.AddCell(new Phrase(item.BILL_NO, fontNormal));
                        table3.AddCell(new Phrase(item.BILL_DATE.IsDateTime() ? DateTime.Parse(item.BILL_DATE).ToString("dd.MM.yyyy") : "", fontNormal));
                        table3.AddCell(new Phrase(acquittanceLists.Where(x => x.COVERAGE_ID == item.COVERAGE_ID && x.REQUESTED != null).Sum(x => decimal.Parse(x.REQUESTED)).ToString(), fontNormal));
                        table3.AddCell(new Phrase(acquittanceLists.Where(x => x.COVERAGE_ID == item.COVERAGE_ID && x.PAID != null).Sum(x => decimal.Parse(x.PAID)).ToString(), fontNormal));
                        table3.AddCell(new Phrase(acquittanceLists.Where(x => x.COVERAGE_ID == item.COVERAGE_ID && x.INSURED_AMOUNT != null).Sum(x => decimal.Parse(x.INSURED_AMOUNT)).ToString(), fontNormal));

                        totalCOINSURANCE += acquittanceLists.Where(x => x.COVERAGE_ID == item.COVERAGE_ID && x.REQUESTED != null).Sum(x => decimal.Parse(x.REQUESTED));
                        totalREQUESTED += acquittanceLists.Where(x => x.COVERAGE_ID == item.COVERAGE_ID && x.PAID != null).Sum(x => decimal.Parse(x.PAID));
                        totalCONFIRMED += acquittanceLists.Where(x => x.COVERAGE_ID == item.COVERAGE_ID && x.INSURED_AMOUNT != null).Sum(x => decimal.Parse(x.INSURED_AMOUNT));
                        coverageIdList.Add(item.COVERAGE_ID);
                    }
                }

                PdfPCell pCell1 = new PdfPCell();
                pCell1.Border = Rectangle.NO_BORDER;
                pCell1.AddElement(new Phrase("", fontNormal));
                table3.AddCell(pCell1);

                PdfPCell pCell2 = new PdfPCell();
                pCell2.AddElement(new Phrase("", fontNormal));
                pCell2.Border = Rectangle.NO_BORDER;
                table3.AddCell(pCell2);

                PdfPCell pCell3 = new PdfPCell();
                pCell3.AddElement(new Phrase("TOPLAM", fontNormal));
                pCell3.Border = Rectangle.NO_BORDER;
                table3.AddCell(pCell3);

                PdfPCell pCell4 = new PdfPCell();
                pCell4.AddElement(new Phrase("", fontNormal));
                pCell4.Border = Rectangle.NO_BORDER;
                table3.AddCell(pCell4);

                PdfPCell pCell5 = new PdfPCell();
                pCell5.AddElement(new Phrase(totalCOINSURANCE.ToString("#0.00"), fontNormal));
                pCell5.Border = Rectangle.NO_BORDER;
                table3.AddCell(pCell5);

                PdfPCell pCell6 = new PdfPCell();
                pCell6.AddElement(new Phrase(totalREQUESTED.ToString("#0.00"), fontNormal));
                pCell6.Border = Rectangle.NO_BORDER;
                table3.AddCell(pCell6);

                PdfPCell pCell7 = new PdfPCell();
                pCell7.AddElement(new Phrase(totalCONFIRMED.ToString("#0.00"), fontNormal));
                pCell7.Border = Rectangle.NO_BORDER;
                table3.AddCell(pCell7);

                document.Add(table3);

                document.Add(ds);
                document.Add(new Phrase(new Chunk($"{acquittance.COMPANY_NAME} ADINA", new Font(STF_Helvetica_Turkish, 12, Font.NORMAL))));
                document.Add(ds);

                document.Add(new Paragraph($"Telefon   : {acquittance.COMPANY_PHONE}", fontNormal));
                document.Add(new Paragraph($"Fax       : {acquittance.COMPANY_FAX}", fontNormal));
                document.Add(new Phrase(Environment.NewLine));

                document.Add(new Paragraph("'Bu provizyon çerçevesinde yapılan ödemelerde şirketi ibra ederim. Sigorta özel ve genel şartları dahilinde ödenmemesi gereken veya poliçemde belirtilen limitleri aşan ödemelerde sigorta şirketinin rücu hak ve yetkisini kabullendigimi gayrıkabili rücu olmak kaydı ile kabul ve beyan ederim.'", fontNormal));
                document.Add(new Phrase(Environment.NewLine));

                document.Add(new Paragraph("Sayın Yetkili, Sigortalı / sigorta ettiren sıfatı ile şahsım ve kanunen temsile yetkili oldugum diger sigortalılara ait saglık durumu ve tedavilerle ilgili olarak, unvanı yukarıda yazılı olan Sigorta Şirketinin talep edecegi bütün bilgi ve belgeleri bu şirkete vermenizi; bu konuda bilgi ve onayım oldugunu ve Sigorta Şirketinin bu bilgi ve belgelerle sigorta mevzuatı ve sigorta sözleşmesi uyarınca ögrenme hakkının bulundugunu bildiririm.", fontNormal));
                document.Add(new Phrase(Environment.NewLine));

                document.Add(new Phrase(new Chunk($"                                                                            Sigortalı / Sigorta Ettiren Imza:", new Font(STF_Helvetica_Turkish, 12, Font.NORMAL))));
                document.Add(ds);

                document.Add(new Paragraph("Dikkat: Işbu belgede sigortalının özel istisnaları, limitleri ve fatura tutarına iştirak oranları gösteirlmektedir. Fatura tanzimini takiben bu bilgilere göre sigortalıdan tahsilini, faturanın ise bakiyesinin kuruluşunuza ödenebilmesi için bu provizyon formuyla birlikte şirketimize gönderilmesini rica ederiz. Tereddüt halinde lütfen şirketimizden 'ödeme teyidi' alınız. Herhangi bir sebeple tedaviden vazgeçilirse bu provizyon numarası belirtilerek iptal edilmelidir.", fontNormal));
                document.Add(new Phrase(Environment.NewLine));

                document.Add(new Paragraph("Sigortalılsigorta ettiren tarafından imzalanmamış provizyon belgeleri işleme alınamayacaktır.", fontNormal));
                #endregion

                document.Close();
                writer.Close();
                fs.Close();
                if (request.IsWebRequest)
                {
                    request.FileName = !string.IsNullOrEmpty(request.FileName) ? request.FileName : (acquittance.INSURED_NAME_SURNAME.ToUpper() + "_IBRANAME_FORMU" + acquittance.PROVIDER_NAME + "_" + acquittance.EVENT_DATE);
                    PrintOut(_path, request.FileName);
                }

                response.Url = _path;
                response.FileName = request.FileName;
                response.Code = "100";
            }
            catch (Exception ex) { response.Code = "999"; response.Message = ex.Message; }
            return response;
        }
    }
}
