﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Print.AcquittanceForm
{
    public class PrintAcquittanceFormReq : CommonReq
    {
        public long ClaimId { get; set; }
    }
}
