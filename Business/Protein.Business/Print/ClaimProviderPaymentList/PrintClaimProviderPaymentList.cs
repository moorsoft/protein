﻿using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.pdf;
using Protein.Business.Abstract.Print;
using Protein.Common.Extensions;
using Protein.Common.Resources;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Business.Print.ClaimProviderPaymentList
{
    public class PrintClaimProviderPaymentList : PrintUtility, IPrint<PrintClaimProviderPaymentListReq>
    {
        public PrintResponse DoWork(PrintClaimProviderPaymentListReq request)
        {
            PrintResponse response = new PrintResponse();
            try
            {
                #region FillPrintData
                V_FormPayroll payroll = new GenericRepository<V_FormPayroll>().FindBy($"PAYROLL_ID = {request.PayrollId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (payroll == null) { response.Code = "500"; response.Message = "Zarf Bilgisi Bulunamadı!"; return response; }
                List<V_FormPayrollClaim> claimPayroll = new GenericRepository<V_FormPayrollClaim>().FindBy($"PAYROLL_ID = {request.PayrollId}", orderby: "CLAIM_ID", fetchDeletedRows: true, fetchHistoricRows: true);
                if (claimPayroll.Count < 1)
                { response.Code = "500"; response.Message = "Zarfa Ait Hasar Kaydı Bulunamadı!"; return response; }
                #endregion

                string _path = "";
                if (request.IsWebRequest)
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints")))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints"));

                    _path = HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints") + "\\" + Guid.NewGuid() + ".pdf";
                }
                else { _path = request.LocalPath + "\\" + (!string.IsNullOrEmpty(request.FileName) ? request.FileName + ".pdf" : "KURUM_TAZMINAT_ODEME_LISTESI.pdf"); };

                int cnt = 0;
                while (true)
                {
                    cnt++;
                    if (File.Exists(_path))
                    {
                        _path = _path.Replace(".pdf", "") + "_" + cnt.ToString() + ".pdf";
                    }
                    else break;
                }

                System.IO.FileStream fs = new FileStream(_path, FileMode.Create);
                Document document = new Document(PageSize.A4.Rotate(), 20, 20, 50, 20);
                PdfWriter writer = PdfWriter.GetInstance(document, fs);
                document.AddAuthor("TAZMINAT_ODEME_BORDROSU");
                document.AddCreator("TAZMINAT_ODEME_BORDROSU");
                document.AddKeywords("TAZMINAT_ODEME_BORDROSU");
                document.AddSubject("TAZMINAT_ODEME_BORDROSU");
                document.AddTitle("TAZMINAT_ODEME_BORDROSU");
                document.Open();


                iTextSharp.text.pdf.BaseFont STF_Helvetica_Turkish = iTextSharp.text.pdf.BaseFont.CreateFont("Helvetica", "CP1254", iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED);

                //Image image = Image.GetInstance(GetCompanyLogoPath("İMECE"));
                //image.ScalePercent(10f);
                //document.Add(image);
                #region SetImages
                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(GetCompanyLogoPath("İMECE"));
                image.ScaleAbsoluteWidth(10f);
                iTextSharp.text.Image image2 = iTextSharp.text.Image.GetInstance(GetCompanyLogoPath(payroll.COMPANY_NAME));
                image2.ScaleAbsoluteWidth(10f);

                var table = new PdfPTable(2);
                float[] widths = new float[] { 15f, 15f };
                table.SetWidths(widths);
                table.WidthPercentage = 100;
                //table.DefaultCell.PaddingRight = 70;
                //table.DefaultCell.PaddingLeft = 5;
                table.DefaultCell.Border = Rectangle.NO_BORDER;

                var cell1 = new PdfPCell();
                cell1.Colspan = 1;
                //cell.PaddingTop = 10;
                cell1.PaddingLeft = 5;
                cell1.PaddingRight = 70;
                cell1.Image = image;
                cell1.Border = 0;
                cell1.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                //cell.AddElement(image);
                table.AddCell(cell1);


                var cell2 = new PdfPCell();
                cell2.Colspan = 1;
                //cell.PaddingTop = 10;
                cell2.PaddingLeft = 40;

                cell2.Image = image2;
                cell2.Border = 0;
                cell2.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                //cell.AddElement(image);
                table.AddCell(cell2);

                //table.AddCell(image);
                //table.AddCell(image2);

                document.Add(table);
                #endregion
                #region Header
                iTextSharp.text.Font fontHeader = new iTextSharp.text.Font(STF_Helvetica_Turkish, 14, Font.NORMAL, BaseColor.WHITE);
                var table2 = new PdfPTable(1);
                table2.WidthPercentage = 100;
                table2.DefaultCell.Border = Rectangle.NO_BORDER;
                PdfPCell cell = new PdfPCell(new Phrase($"{payroll.COMPANY_NAME} {LookupHelper.GetLookupTextByOrdinal(LookupTypes.Payroll, payroll.PAYROLL_TYPE).ToUpper()} TAZMİNAT ODEME BORDROSU", fontHeader));
                cell.BackgroundColor = new BaseColor(80, 112, 240);//BaseColor.BLUE;
                cell.HorizontalAlignment = 1;
                table2.AddCell(cell);
                Paragraph ds = new Paragraph();
                Phrase phr = new Phrase(Environment.NewLine);
                ds.Add(phr);
                document.Add(ds);
                document.Add(table2);
                document.Add(new Paragraph());
                document.Add(ds);
                #endregion

                #region Info
                Font fontSubText = new Font(STF_Helvetica_Turkish, 9, Font.BOLD);
                Font fontSubValue = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL);

                var tableinfo = new PdfPTable(4);
                float[] widths2 = new float[] { 12f, 20f, 12f, 30f };
                tableinfo.SetWidths(widths2);

                tableinfo.DefaultCell.HorizontalAlignment = 0;

                tableinfo.WidthPercentage = 100;
                tableinfo.DefaultCell.Border = Rectangle.NO_BORDER;

                tableinfo.AddCell(new Phrase("Zarf No / Şirket Zarf No", fontSubText));
                tableinfo.AddCell(new Phrase($": {payroll.PAYROLL_ID} / {payroll.COMPANY_PAYROLL_NO}", fontSubValue));

                tableinfo.AddCell(new Phrase("Kurum/Sigortalı Adı", fontSubText));
                tableinfo.AddCell(new Phrase($": {(payroll.PAYROLL_TYPE == ((int)PayrollType.KURUM).ToString() ? payroll.PROVIDER_NAME : (claimPayroll != null && claimPayroll.Count > 0 ? claimPayroll[0].INSURED_NAME_LASTNAME : ""))}", fontSubValue));

                tableinfo.AddCell(new Phrase("Kurum Icmal No", fontSubText));
                tableinfo.AddCell(new Phrase($": {payroll.EXT_PROVIDER_NO}", fontSubValue));

                tableinfo.AddCell(new Phrase("Hesap Adı", fontSubText));
                tableinfo.AddCell(new Phrase($": {payroll.BANK_ACCOUNT_NAME}", fontSubValue));

                tableinfo.AddCell(new Phrase("Zarf Tipi", fontSubText));
                tableinfo.AddCell(new Phrase($": {LookupHelper.GetLookupTextByOrdinal(LookupTypes.Payroll, payroll.PAYROLL_TYPE)}", fontSubValue));

                tableinfo.AddCell(new Phrase("Banka", fontSubText));
                tableinfo.AddCell(new Phrase($": {payroll.BANK_NAME}", fontSubValue));

                tableinfo.AddCell(new Phrase("Gelis Tarihi", fontSubText));
                tableinfo.AddCell(new Phrase($": {(payroll.PAYROLL_DATE.IsDateTime() ? DateTime.Parse(payroll.PAYROLL_DATE).ToString("dd.MM.yyyy") : "")}", fontSubValue));

                tableinfo.AddCell(new Phrase("Şube Kodu / Adı", fontSubText));
                tableinfo.AddCell(new Phrase($": {payroll.BANK_BRANCH_ID} / {payroll.BANK_BRANCH_NAME}", fontSubValue));

                tableinfo.AddCell(new Phrase("Hasar Ödeme Tarihi", fontSubText));
                tableinfo.AddCell(new Phrase($": {(payroll.PAYROLL_DUE_DATE.IsDateTime() ? DateTime.Parse(payroll.PAYROLL_DUE_DATE).ToString("dd.MM.yyyy") : "")}", fontSubValue));

                tableinfo.AddCell(new Phrase("IBAN", fontSubText));
                tableinfo.AddCell(new Phrase($": {payroll.IBAN}", fontSubValue));
                document.Add(tableinfo);
                document.Add(ds);
                document.Add(ds);
                #endregion

                if (claimPayroll != null && claimPayroll.Count > 0)
                {
                    int colCount = payroll.PAYROLL_TYPE == ((int)PayrollType.KURUM).ToString() ? 14 : 15;
                    var tableClaimList = new PdfPTable(colCount);
                    tableClaimList.DefaultCell.HorizontalAlignment = 0;
                    tableClaimList.WidthPercentage = 100;

                    fontSubValue = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL, BaseColor.WHITE);

                    tableClaimList.AddCell(GetTableCell("Prov.No", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Policeno", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Urun", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Siget", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Sigortali", fontSubValue));
                    if (payroll.PAYROLL_TYPE == ((int)PayrollType.SIGORTALI).ToString())
                    {
                        tableClaimList.AddCell(GetTableCell("Kurum Adı", fontSubValue));
                    }
                    tableClaimList.AddCell(GetTableCell("Teminat", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Taztar", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Fattar", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Fatno", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Hsrstat", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Anatem", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Talep Ttr", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Sigpay", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Odetutar", fontSubValue));

                    fontSubValue = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL);
                    decimal REQUESTED = 0, INSURED_AMOUNT = 0, PAID = 0;

                    List<SimpleClaim> simpleClaimList = new List<SimpleClaim>();

                    foreach (var item in claimPayroll)
                    {
                        var isSimple = true;
                        foreach (var simpleClaim in simpleClaimList)
                        {
                            if (simpleClaim.CLAIM_ID == item.CLAIM_ID && simpleClaim.COVERAGE_NAME == item.COVERAGE_NAME)
                            {
                                isSimple = false;
                                break;
                            }
                        }
                        if (isSimple)
                        {
                            decimal claimREQUESTED = 0, claimINSURED_AMOUNT = 0, claimPAID = 0;
                            var totalPayroll = claimPayroll.Where(p => p.CLAIM_ID == item.CLAIM_ID && p.COVERAGE_NAME == item.COVERAGE_NAME).ToList();

                            claimREQUESTED = totalPayroll.Sum(t => t.REQUESTED.IsNumeric() ? decimal.Parse(t.REQUESTED) : 0);
                            claimINSURED_AMOUNT = totalPayroll.Sum(t => t.INSURED_AMOUNT.IsNumeric() ? decimal.Parse(t.INSURED_AMOUNT) : 0);
                            claimPAID = totalPayroll.Sum(t => t.PAID.IsNumeric() ? decimal.Parse(t.PAID) : 0);

                            tableClaimList.AddCell(new Phrase($"{item.CLAIM_ID}", fontSubValue));
                            tableClaimList.AddCell(new Phrase($"{item.POLICY_NUMBER}", fontSubValue));
                            tableClaimList.AddCell(new Phrase($"{item.PRODUCT_NAME}", fontSubValue));
                            tableClaimList.AddCell(new Phrase($"{item.INSURER_NAME}", fontSubValue));
                            tableClaimList.AddCell(new Phrase($"{item.INSURED_NAME_LASTNAME}", fontSubValue));
                            if (payroll.PAYROLL_TYPE == ((int)PayrollType.SIGORTALI).ToString())
                            {
                                tableClaimList.AddCell(new Phrase($"{item.PROVIDER_NAME}", fontSubValue));
                            }
                            tableClaimList.AddCell(new Phrase($"{item.COVERAGE_NAME}", fontSubValue));
                            tableClaimList.AddCell(new Phrase($"{(item.CLAIM_DATE.IsDateTime() ? DateTime.Parse(item.CLAIM_DATE).ToString("dd.MM.yyyy") : "")}", fontSubValue));
                            tableClaimList.AddCell(new Phrase($"{(item.BILL_DATE.IsDateTime() ? DateTime.Parse(item.BILL_DATE).ToString("dd.MM.yyyy") : "")}", fontSubValue));
                            tableClaimList.AddCell(new Phrase($"{item.BILL_NO}", fontSubValue));
                            tableClaimList.AddCell(new Phrase($"{LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, item.CLAIM_STATUS)}", fontSubValue));
                            tableClaimList.AddCell(new Phrase($"{item.MAIN_COVERAGE_NAME}", fontSubValue));
                            tableClaimList.AddCell(new Phrase($"{claimREQUESTED.ToString()}", fontSubValue));
                            tableClaimList.AddCell(new Phrase($"{claimINSURED_AMOUNT.ToString()}", fontSubValue));
                            tableClaimList.AddCell(new Phrase($"{claimPAID.ToString()}", fontSubValue));
                            REQUESTED += claimREQUESTED;
                            INSURED_AMOUNT += claimINSURED_AMOUNT;
                            PAID += claimPAID;

                            SimpleClaim simpleClaim = new SimpleClaim
                            {
                                CLAIM_ID = item.CLAIM_ID,
                                COVERAGE_NAME = item.COVERAGE_NAME,
                            };
                            simpleClaimList.Add(simpleClaim);
                        }
                    }
                    tableClaimList.DefaultCell.Border = Rectangle.NO_BORDER;
                    for (int i = 0; i < colCount - 4; i++)
                    {
                        tableClaimList.AddCell(new Phrase($"", fontSubValue));
                    }
                    tableClaimList.AddCell(new Phrase($"TOPLAM", fontSubText));
                    tableClaimList.AddCell(new Phrase($"{REQUESTED}", fontSubText));
                    tableClaimList.AddCell(new Phrase($"{INSURED_AMOUNT}", fontSubText));
                    tableClaimList.AddCell(new Phrase($"{PAID}", fontSubText));

                    document.Add(tableClaimList);
                }
                document.Close();
                writer.Close();
                fs.Close();
                if (request.IsWebRequest)
                {
                    request.FileName = !string.IsNullOrEmpty(request.FileName) ? request.FileName : ("TAZMINAT_ODEME_BORDROSU");
                    PrintOut(_path, request.FileName);
                }
                response.Url = _path;
                response.FileName = request.FileName;
                response.Code = "100";
            }
            catch (Exception ex) { response.Code = "999"; response.Message = ex.Message; }
            return response;
        }

        private PdfPCell GetTableCell(string v, Font font)
        {
            PdfPCell cellDetails = new PdfPCell(new Phrase(v, font));
            cellDetails.BackgroundColor = new BaseColor(80, 112, 240);//BaseColor.BLUE;
            cellDetails.HorizontalAlignment = 1;
            return cellDetails;
        }
    }
}
