﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Print.ClaimProviderPaymentList
{
    public class PrintClaimProviderPaymentListReq : CommonReq
    {
        public long PayrollId { get; set; }
    }
}
