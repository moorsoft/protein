﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Print.PolicyTransfer
{
    public class PolicyTransferReq : CommonReq
    {
        public long PolicyId { get; set; }
    }
}
