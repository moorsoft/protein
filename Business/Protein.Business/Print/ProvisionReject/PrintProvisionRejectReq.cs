﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Print.ProvisionReject
{
    public class PrintProvisionRejectReq : CommonReq
    {
        public long ClaimId { get; set; }
    }
}
