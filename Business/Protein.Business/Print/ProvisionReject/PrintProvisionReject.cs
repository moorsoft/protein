﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Protein.Business.Abstract.Print;
using Protein.Data.Repositories;
using static iTextSharp.text.Font;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Business.Print.ProvisionReject
{
    public class PrintProvisionReject : PrintUtility, IPrint<PrintProvisionRejectReq>
    {
        public PrintResponse DoWork(PrintProvisionRejectReq request)
        {
            PrintResponse response = new PrintResponse();

            try
            {
                #region FillPrintData
                V_FormProvisionReject reject = new GenericRepository<V_FormProvisionReject>().FindBy($"CLAIM_ID = {request.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (reject == null) { response.Code = "999"; return response; }
                #endregion

                string _path = "";
                if (request.IsWebRequest)
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints")))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints"));

                    _path = HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints") + "\\" + Guid.NewGuid() + ".pdf";
                }
                else { _path = request.LocalPath + "\\" + (!string.IsNullOrEmpty(request.FileName) ? request.FileName + ".pdf" : "PROVIZYON_RED_FORMU_.pdf"); };

                int cnt = 0;
                while (true)
                {
                    cnt++;
                    if (File.Exists(_path))
                    {
                        _path = _path.Replace(".pdf", "") + "_" + cnt.ToString() + ".pdf";
                    }
                    else break;
                }

                System.IO.FileStream fs = new FileStream(_path, FileMode.Create);
                Document document = new Document(PageSize.A4, 10, 10, 40, 20);

                PdfWriter writer = PdfWriter.GetInstance(document, fs);

                document.AddAuthor("TEST");
                document.AddCreator("TEST");
                document.AddKeywords("TEST");
                document.AddSubject("TEST");
                document.AddTitle("TEST");
                document.Open();

                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(GetCompanyLogoPath(reject.COMPANY_NAME));

                image.ScalePercent(10f);
                document.Add(image);

                iTextSharp.text.pdf.BaseFont STF_Helvetica_Turkish = iTextSharp.text.pdf.BaseFont.CreateFont("Helvetica", "CP1254", iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED);

                #region Header
                iTextSharp.text.Font fontHeader = new iTextSharp.text.Font(STF_Helvetica_Turkish, 20, iTextSharp.text.Font.BOLD);
                Paragraph paragraphHeader = new Paragraph("PROVİZYON RED FORMU", fontHeader);
                paragraphHeader.Alignment = 1; // 0 left  1 center 2 right
                paragraphHeader.Font.Size = 20;
                document.Add(paragraphHeader);
                document.Add(new Paragraph());
                Paragraph ds = new Paragraph();
                Phrase phr = new Phrase(Environment.NewLine);
                ds.Add(phr);
                ds.Add(phr);
                ds.Add(phr);
                ds.Add(phr);
                document.Add(ds);
                #endregion
                #region FillDetails
                var table = new PdfPTable(2);
                float[] widths = new float[] { 30f, 90f };
                table.SetWidths(widths);
                table.DefaultCell.Border = Rectangle.NO_BORDER;

                var cell = new PdfPCell();
                cell.Colspan = 2;
                cell.PaddingTop = 30;

                cell.Border = 0;
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                table.AddCell(cell);

                iTextSharp.text.Font fontSubText = new iTextSharp.text.Font(STF_Helvetica_Turkish, 9, iTextSharp.text.Font.BOLD);
                iTextSharp.text.Font fontSubValue = new iTextSharp.text.Font(STF_Helvetica_Turkish, 9, iTextSharp.text.Font.NORMAL);
                Phrase DateText = new Phrase("Tarih.....................: ", fontSubText);
                Phrase InsuredText = new Phrase("Sigortalı Adı - Soyadı...: ", fontSubText);
                Phrase PolicyNumberText = new Phrase("Poliçe Numarası....: ", fontSubText);
                Phrase CustomerNumberText = new Phrase("Müşteri Numarası...: ", fontSubText);
                Phrase ProductNameText = new Phrase("Ürün Adı...........: ", fontSubText);
                Phrase ProvisionNumberText = new Phrase("Provizyon Numarası..: ", fontSubText);
                Phrase ProviderNameText = new Phrase("Kurum Adı.......: ", fontSubText);
                Phrase IcdText = new Phrase("Tanı Adı..........: ", fontSubText);
                Phrase ProcessText = new Phrase("Tedavi Adı......: ", fontSubText);

                Phrase DateValue = new Phrase($"{reject.PROVISION_DATE}", fontSubValue);
                Phrase InsuredNameValue = new Phrase($"{ reject.INSURED_NAME_SURNAME}", fontSubValue);
                Phrase PolicyNumberValue = new Phrase($"{ reject.POLICY_NUMBER}", fontSubValue);
                Phrase CustomerNumberValue = new Phrase($"{ reject.INSURED_CONTACT_ID}", fontSubValue);
                Phrase ProductNameValue = new Phrase($"{ reject.PRODUCT_NAME}", fontSubValue);
                Phrase ProvisionNumberValue =
                    new Phrase($"{ (!string.IsNullOrEmpty(reject.CompanyClaimNo) ? reject.CompanyClaimNo : reject.CLAIM_ID) }", fontSubValue);

                Phrase ProviderNameValue = new Phrase($"{ reject.PROVIDER_NAME}", fontSubValue);
                Phrase IcdValue = new Phrase($"{ reject.ICD_LIST}", fontSubValue);
                Phrase ProcessValue = new Phrase($"{ reject.PROCESS_LIST}", fontSubValue);


                table.AddCell(DateText);
                table.AddCell(DateValue);

                table.AddCell(InsuredText);
                table.AddCell(InsuredNameValue);

                table.AddCell(PolicyNumberText);
                table.AddCell(PolicyNumberValue);

                table.AddCell(CustomerNumberText);
                table.AddCell(CustomerNumberValue);

                table.AddCell(ProductNameText);
                table.AddCell(ProductNameValue);

                table.AddCell(ProvisionNumberText);
                table.AddCell(ProvisionNumberValue);

                table.AddCell(ProviderNameText);
                table.AddCell(ProviderNameValue);

                table.AddCell(IcdText);
                table.AddCell(IcdValue);

                table.AddCell(ProcessText);
                table.AddCell(ProcessValue);

                document.Add(table);
                #endregion
                #region FillInfo
                iTextSharp.text.Font fontBold = new iTextSharp.text.Font(STF_Helvetica_Turkish, 9, iTextSharp.text.Font.BOLD);
                iTextSharp.text.Font fontNormal = new iTextSharp.text.Font(STF_Helvetica_Turkish, 9, iTextSharp.text.Font.NORMAL);

                Paragraph ds2 = new Paragraph();
                ds2.Add(phr);
                document.Add(ds2);
                document.Add(ds2);

                Phrase pr = new Phrase();
                Chunk chunk = new Chunk($"{reject.COMPANY_NAME}", fontBold);
                pr.Add(chunk);
                Chunk chunk2 = new Chunk(" sigortalısı yukarıda poliçe bilgileri bulunan sigortalımıza ait yapılmış/yapılması planlanan tanı, tetkik ve harcamalarının provizyon talebi kuruluşunuz ile yapılan anlaşma gereğince sigortalımızın poliçesinin özel ve genel şartları, tem limitleri dahilinde değerlendirilmiş ve onaylanamamıştır.", fontNormal);
                pr.Add(chunk2);

                Paragraph infoParag = new Paragraph();
                infoParag.Add(pr);
                document.Add(infoParag);
                document.Add(ds2);
                Paragraph infoParag2 = new Paragraph();

                Phrase prReasonInfo = new Phrase();
                Chunk chunkReasonInfo = new Chunk("Red açıklaması aşağıda yazılı olduğu gibidir.", fontNormal);
                prReasonInfo.Add(chunkReasonInfo);
                prReasonInfo.Add(Environment.NewLine);
                Chunk chunkReasonMessage = new Chunk($"{reject.REASON_NAME}", fontBold);
                prReasonInfo.Add(chunkReasonMessage);
                infoParag2.Add(prReasonInfo);

                Paragraph ds3 = new Paragraph();
                ds3.Add(phr);

                document.Add(infoParag2);
                document.Add(ds2);
                document.Add(ds2);
                Paragraph paragReason = new Paragraph();
                Phrase prReasonMessage = new Phrase();
                Chunk chunkCompanyCont = new Chunk(" ADINA,", fontBold);
                prReasonMessage.Add(chunk);
                prReasonMessage.Add(chunkCompanyCont);
                paragReason.Add(prReasonMessage);
                document.Add(paragReason);
                document.Add(ds2);

                Phrase prFinitoMessage = new Phrase("Bilgilerinize sunarız, ", fontNormal);
                Paragraph paragFinitoMessage = new Paragraph();
                paragFinitoMessage.Add(prFinitoMessage);
                document.Add(paragFinitoMessage);
                document.Add(ds2);

                Phrase prFinitoMessage2 = new Phrase("Saygılarımızla, ", fontNormal);
                Paragraph paragFinitoMessage2 = new Paragraph();
                paragFinitoMessage2.Add(prFinitoMessage2);
                document.Add(prFinitoMessage2);
                document.Add(ds2);
                document.Add(ds2);
                document.Add(ds2);


                Chunk chunkPhone = new Chunk($"Telefon : {reject.COMPANY_PHONE}");

                #endregion

                document.Close();
                writer.Close();
                fs.Close();
                response.Url = _path;
                response.Message = "OK";
                response.Code = "100";

                if (request.IsWebRequest)
                {
                    request.FileName = !string.IsNullOrEmpty(request.FileName) ? request.FileName : ("PROVIZYON_RED_FORMU_");
                    PrintOut(_path, request.FileName);
                }
                response.Url = _path;
                response.FileName = request.FileName;
                response.Code = "100";
            }
            catch (Exception ex) { response.Code = "999"; response.Message = ex.Message; }
            return response;
        }
    }
}
