﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Print
{
    public class PrintResponse
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string Url { get; set; }
        public string FileName { get; set; }
        public bool IsSuccess { get { return (this.Code == "100" ? true : false); } }
    }
}
