﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Protein.Business.Abstract.Print;
using Protein.Business.Print.FamilyBasedInsuredProfile;
using Protein.Common.Enums;
using Protein.Common.Extensions;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Business.Print.FamilyBasedTransferProfile
{
    public class FamilyBasedTransferProfile : PrintUtility, IPrint<FamilyBasedTransferProfileReq>
    {
        public class MyPageHeader : PdfPageEventHelper
        {
            public long CompanyId { get; set; }
            public string CompanyName { get; set; }
            public MyPageHeader(long companyId = 0, string CompanyName = "")
            {
                this.CompanyId = companyId;
                this.CompanyName = CompanyName;
            }

            public override void OnStartPage(PdfWriter writer, Document document)
            {
                base.OnStartPage(writer, document);
                BaseFont STF_Helvetica_Turkish = BaseFont.CreateFont("Helvetica", "CP1254", BaseFont.NOT_EMBEDDED);

                var HeaderTable = new PdfPTable(3);
                float[] widths = new float[] { 25f, 50f, 25f };
                HeaderTable.SetWidths(widths);
                HeaderTable.TotalWidth = document.Right - document.Left;
                HeaderTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                HeaderTable.DefaultCell.Border = Rectangle.NO_BORDER;
                HeaderTable.WidthPercentage = 100;
                Image image = Image.GetInstance(new FamilyBasedTransferProfile().GetCompanyLogoPath(CompanyName));

                image.ScalePercent(10);

                var HeaderImagecell = new PdfPCell();
                HeaderImagecell.HorizontalAlignment = Element.ALIGN_LEFT;
                HeaderImagecell.Border = Rectangle.NO_BORDER;
                HeaderImagecell.Colspan = 1;

                HeaderImagecell.Padding = 3f;

                HeaderTable.AddCell(image);
                //HeaderTable.WidthPercentage = 100;

                Font fontHeader = new Font(STF_Helvetica_Turkish, 15, Font.BOLD);
                Font fontHeaderNormal = new Font(STF_Helvetica_Turkish, 15);

                var HeaderTitlecell = new PdfPCell(new Phrase("SİGORTALI GEÇİŞ PROFİLİ", fontHeader));
                HeaderTitlecell.HorizontalAlignment = Element.ALIGN_CENTER;
                HeaderTitlecell.Border = Rectangle.NO_BORDER;
                HeaderTitlecell.Colspan = 1;
                HeaderTitlecell.PaddingTop = 25f;
                HeaderTable.AddCell(HeaderTitlecell);
                document.Add(HeaderTable);

                Paragraph parag = new Paragraph();
                parag.Add(new Phrase("Tarih:    " + DateTime.Now.ToShortDateString(), fontHeaderNormal));
                parag.Add(Environment.NewLine);
                parag.Add(new Phrase("Sayfa:    " + writer.PageNumber.ToString(), fontHeaderNormal));

                var HeaderRightcell = new PdfPCell(parag);
                HeaderRightcell.HorizontalAlignment = Element.ALIGN_LEFT;
                HeaderRightcell.Border = Rectangle.NO_BORDER;
                HeaderRightcell.Colspan = 1;
                HeaderRightcell.PaddingLeft = 40f;
                HeaderRightcell.PaddingTop = 10f;
                HeaderTable.AddCell(HeaderRightcell);
                document.Add(HeaderTable);
                //HeaderTable.WriteSelectedRows(0, -1, 150, document.Top, writer.DirectContent);
            }
        }

        public PrintResponse DoWork(FamilyBasedTransferProfileReq request)
        {
            PrintResponse response = new PrintResponse();

            try
            {
                #region FillPrintData

                List<V_Insured> insureds = new GenericRepository<V_Insured>().FindBy($" FAMILY_NO = '{request.FamilyNo}' and STATUS = '{((int)ProteinEnums.Status.AKTIF).ToString()}' and POLICY_ID = {request.PolicyId} AND POLICY_STATUS in ('{((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString()}','{((int)ProteinEnums.PolicyStatus.TEKLIF).ToString()}') ", orderby: "");

                #endregion

                string _path = "";
                if (request.IsWebRequest)
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints")))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints"));

                    _path = HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints") + "\\" + Guid.NewGuid() + ".pdf";
                }
                else { _path = request.LocalPath + "\\" + (!string.IsNullOrEmpty(request.FileName) ? request.FileName + ".pdf" : "AILE_BAZINDA_GECIS_PROFILI_" + request.PolicyId + ".pdf"); };

                int cnt = 0;
                while (true)
                {
                    cnt++;
                    if (File.Exists(_path))
                    {
                        _path = _path.Replace(".pdf", "") + "_" + cnt.ToString() + ".pdf";
                    }
                    else break;
                }


                System.IO.FileStream fs = new FileStream(_path, FileMode.Create);
                Document document = new Document(PageSize.A4, 10, 10, 10, 20);
                document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());

                PdfWriter writer = PdfWriter.GetInstance(document, fs);

                document.AddAuthor("TEST");
                document.AddCreator("TEST");
                document.AddKeywords("TEST");
                document.AddSubject("TEST");
                document.AddTitle("TEST");

                BaseFont STF_Helvetica_Turkish = BaseFont.CreateFont("Helvetica", "CP1254", BaseFont.NOT_EMBEDDED);
                writer.PageEvent = new MyPageHeader(companyId: (long)insureds[0].COMPANY_ID, CompanyName: insureds[0].COMPANY_NAME);
                document.Open();

                #region Fonts
                Font fontHeaderBold = new Font(STF_Helvetica_Turkish, 10, Font.BOLD);

                Font fontBoldBlack = new Font(STF_Helvetica_Turkish, 8, Font.BOLD);
                Font fontNormalBlack = new Font(STF_Helvetica_Turkish, 8, Font.NORMAL);

                Font fontBoldYellow = new Font(STF_Helvetica_Turkish, 8, Font.BOLD, BaseColor.BLACK);
                Font fontNormalYellow = new Font(STF_Helvetica_Turkish, 8, Font.NORMAL, BaseColor.BLACK);

                Font fontBoldBlue = new Font(STF_Helvetica_Turkish, 8, Font.BOLD, BaseColor.BLUE);
                Font fontNormalBlue = new Font(STF_Helvetica_Turkish, 8, Font.NORMAL, BaseColor.BLUE);
                #endregion
                #region Colors
                //BaseColor baseColorHeader = new BaseColor(8, 126, 131);
                //BaseColor baseColorContent = new BaseColor(192, 192, 192);

                BaseColor baseColorHeader = new BaseColor(255, 255, 255);
                BaseColor baseColorContent = new BaseColor(255, 255, 255);
                #endregion
                #region TableConfig

                Paragraph parag = new Paragraph();
                Phrase pr = new Phrase("Genel Müdürlüğüne,", fontBoldBlack);
                parag.Add(pr); parag.Add(Environment.NewLine);
                pr = new Phrase("Aşağıda bilgileri bulunan kişi/kişiler son durum kayıtları ekte açıklanmıştır.", fontBoldBlack);
                parag.Add(pr); parag.Add(Environment.NewLine);
                pr = new Phrase("Saygılarımızla", fontBoldBlack);
                parag.Add(pr); parag.Add(Environment.NewLine); parag.Add(Environment.NewLine);
                document.Add(parag);

                var mainTable = new PdfPTable(1);
                mainTable.WidthPercentage = 100;
                mainTable.DefaultCell.PaddingTop = 0;
                mainTable.DefaultCell.PaddingLeft = 0;
                mainTable.DefaultCell.PaddingRight = 0;
                mainTable.DefaultCell.PaddingBottom = 0;
                mainTable.DefaultCell.HorizontalAlignment = 0;
                mainTable.DefaultCell.Border = Rectangle.NO_BORDER;
                mainTable.DefaultCell.BackgroundColor = baseColorContent;

                foreach (var insured in insureds)
                {

                    V_FormInsuredProfile formInsuredProfile = new GenericRepository<V_FormInsuredProfile>().FindBy($"INSURED_ID = {insured.INSURED_ID}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();

                    List<V_FormContactPolicy> formInsuredOtherPolicy = new GenericRepository<V_FormContactPolicy>().FindBy($"CONTACT_ID = {formInsuredProfile.CONTACT_ID} and POLICY_ID != {formInsuredProfile.POLICY_ID}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

                    List<V_FormInsuredDecleration> formInsuredDecleration = new GenericRepository<V_FormInsuredDecleration>().FindBy($"INSURED_ID = {formInsuredProfile.INSURED_ID}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

                    List<V_FormInsuredNote> formInsuredNote = new GenericRepository<V_FormInsuredNote>().FindBy($"INSURED_ID = {formInsuredProfile.INSURED_ID}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

                    List<V_FormInsuredExclusion> formInsuredExclusion = new GenericRepository<V_FormInsuredExclusion>().FindBy($"INSURED_ID = {formInsuredProfile.INSURED_ID}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

                    List<V_FormPolicyCoverage> formPolicyCoverage = new GenericRepository<V_FormPolicyCoverage>().FindBy($"INSURED_ID = {formInsuredProfile.INSURED_ID} AND POLICY_ID = {formInsuredProfile.POLICY_ID}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

                    List<V_FormInsuredClaim> formInsuredClaim = new GenericRepository<V_FormInsuredClaim>().FindBy($"INSURED_ID = { formInsuredProfile.INSURED_ID} and POLICY_ID = {formInsuredProfile.POLICY_ID}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

                    List<V_FormInsuredClaim> formInsuredClaimOtherPolicy = new GenericRepository<V_FormInsuredClaim>().FindBy($"INSURED_ID = { formInsuredProfile.INSURED_ID} and POLICY_ID != {formInsuredProfile.POLICY_ID}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);



                    var HeaderTable = new PdfPTable(10);
                    HeaderTable.WidthPercentage = 100;
                    HeaderTable.DefaultCell.HorizontalAlignment = 0;
                    HeaderTable.PaddingTop = 0;
                    HeaderTable.DefaultCell.Padding = 0;
                    HeaderTable.DefaultCell.Border = Rectangle.TOP_BORDER;

                    var InsuredInfoTable = new PdfPTable(15);
                    InsuredInfoTable.WidthPercentage = 90;
                    InsuredInfoTable.PaddingTop = 0;
                    InsuredInfoTable.DefaultCell.Padding = 0;
                    InsuredInfoTable.DefaultCell.HorizontalAlignment = 0;
                    InsuredInfoTable.DefaultCell.Border = Rectangle.BOTTOM_BORDER;

                    var SubTableCols = new PdfPTable(2);
                    float[] widthsSubT = new float[] { 13f, 87f };
                    //SubTableCols.DefaultCell.PaddingTop = 10f;
                    SubTableCols.DefaultCell.HorizontalAlignment = 0;
                    SubTableCols.WidthPercentage = 100;
                    SubTableCols.DefaultCell.PaddingTop = 0;
                    SubTableCols.DefaultCell.PaddingLeft = 0;
                    SubTableCols.DefaultCell.PaddingRight = 0;
                    SubTableCols.DefaultCell.PaddingBottom = 0;
                    SubTableCols.SetWidths(widthsSubT);
                    SubTableCols.DefaultCell.Border = Rectangle.TOP_BORDER;

                    var OtherPolicyTable = new PdfPTable(6);
                    float[] widthsOtherPol = new float[] { 30f, 30f, 30f, 30f, 30f, 30f };
                    OtherPolicyTable.SetWidths(widthsOtherPol);
                    OtherPolicyTable.WidthPercentage = 100;
                    OtherPolicyTable.DefaultCell.HorizontalAlignment = 0;
                    OtherPolicyTable.DefaultCell.PaddingTop = 0;
                    OtherPolicyTable.DefaultCell.PaddingLeft = 0;
                    OtherPolicyTable.DefaultCell.PaddingRight = 0;
                    OtherPolicyTable.DefaultCell.PaddingBottom = 0;
                    OtherPolicyTable.DefaultCell.Border = Rectangle.TOP_BORDER;

                    var SubTableCols2 = new PdfPTable(2);
                    //SubTableCols.DefaultCell.PaddingTop = 10f;
                    SubTableCols2.DefaultCell.HorizontalAlignment = 0;
                    SubTableCols2.WidthPercentage = 100;
                    SubTableCols2.DefaultCell.PaddingTop = 0;
                    SubTableCols2.DefaultCell.PaddingLeft = 0;
                    SubTableCols2.DefaultCell.PaddingRight = 0;
                    SubTableCols2.DefaultCell.PaddingBottom = 0;
                    SubTableCols2.SetWidths(widthsSubT);
                    SubTableCols2.DefaultCell.Border = Rectangle.TOP_BORDER;

                    var DeclerationTable = new PdfPTable(2);
                    float[] widthsDecleration = new float[] { 20f, 80f };
                    DeclerationTable.SetWidths(widthsDecleration);
                    DeclerationTable.WidthPercentage = 100;
                    DeclerationTable.DefaultCell.HorizontalAlignment = 0;
                    DeclerationTable.DefaultCell.PaddingTop = 0;
                    DeclerationTable.DefaultCell.PaddingLeft = 0;
                    DeclerationTable.DefaultCell.PaddingRight = 0;
                    DeclerationTable.DefaultCell.PaddingBottom = 0;
                    DeclerationTable.DefaultCell.Border = Rectangle.TOP_BORDER;

                    var SubTableCols3 = new PdfPTable(2);
                    //SubTableCols.DefaultCell.PaddingTop = 10f;
                    SubTableCols3.DefaultCell.HorizontalAlignment = 0;
                    SubTableCols3.WidthPercentage = 100;
                    SubTableCols3.DefaultCell.PaddingTop = 0;
                    SubTableCols3.DefaultCell.PaddingLeft = 0;
                    SubTableCols3.DefaultCell.PaddingRight = 0;
                    SubTableCols3.DefaultCell.PaddingBottom = 0;
                    SubTableCols3.SetWidths(widthsSubT);
                    SubTableCols3.DefaultCell.Border = Rectangle.TOP_BORDER;

                    var NotesTable = new PdfPTable(2);
                    NotesTable.SetWidths(widthsDecleration);
                    NotesTable.WidthPercentage = 100;
                    NotesTable.DefaultCell.HorizontalAlignment = 0;
                    NotesTable.DefaultCell.PaddingTop = 0;
                    NotesTable.DefaultCell.PaddingLeft = 0;
                    NotesTable.DefaultCell.PaddingRight = 0;
                    NotesTable.DefaultCell.PaddingBottom = 0;
                    NotesTable.DefaultCell.Border = Rectangle.TOP_BORDER;

                    var SubTableCols4 = new PdfPTable(2);
                    //SubTableCols.DefaultCell.PaddingTop = 10f;
                    SubTableCols4.DefaultCell.HorizontalAlignment = 0;
                    SubTableCols4.WidthPercentage = 100;
                    SubTableCols4.DefaultCell.PaddingTop = 0;
                    SubTableCols4.DefaultCell.PaddingLeft = 0;
                    SubTableCols4.DefaultCell.PaddingRight = 0;
                    SubTableCols4.DefaultCell.PaddingBottom = 0;
                    SubTableCols4.SetWidths(widthsSubT);
                    SubTableCols4.DefaultCell.Border = Rectangle.TOP_BORDER;

                    var ExclusionTable = new PdfPTable(2);
                    ExclusionTable.SetWidths(widthsDecleration);
                    ExclusionTable.WidthPercentage = 100;
                    ExclusionTable.DefaultCell.HorizontalAlignment = 0;
                    ExclusionTable.DefaultCell.PaddingTop = 0;
                    ExclusionTable.DefaultCell.PaddingLeft = 0;
                    ExclusionTable.DefaultCell.PaddingRight = 0;
                    ExclusionTable.DefaultCell.PaddingBottom = 0;
                    ExclusionTable.DefaultCell.Border = Rectangle.TOP_BORDER;

                    var SubTableCols5 = new PdfPTable(2);
                    //SubTableCols.DefaultCell.PaddingTop = 10f;
                    SubTableCols5.DefaultCell.HorizontalAlignment = 0;
                    SubTableCols5.WidthPercentage = 100;
                    SubTableCols5.DefaultCell.PaddingTop = 0;
                    SubTableCols5.DefaultCell.PaddingLeft = 0;
                    SubTableCols5.DefaultCell.PaddingRight = 0;
                    SubTableCols5.DefaultCell.PaddingBottom = 0;
                    SubTableCols5.SetWidths(widthsSubT);
                    SubTableCols5.DefaultCell.Border = Rectangle.TOP_BORDER;

                    var InsuredCoverageTable = new PdfPTable(5);
                    float[] widthsInsuredCoverage = new float[] { 8f, 24f, 8f, 40f, 20f };
                    InsuredCoverageTable.SetWidths(widthsInsuredCoverage);
                    InsuredCoverageTable.WidthPercentage = 100;
                    InsuredCoverageTable.DefaultCell.HorizontalAlignment = 0;
                    InsuredCoverageTable.DefaultCell.PaddingTop = 0;
                    InsuredCoverageTable.DefaultCell.PaddingLeft = 0;
                    InsuredCoverageTable.DefaultCell.PaddingRight = 0;
                    InsuredCoverageTable.DefaultCell.PaddingBottom = 0;
                    InsuredCoverageTable.DefaultCell.Border = Rectangle.TOP_BORDER;

                    var InsuredClaimTable = new PdfPTable(11);
                    InsuredClaimTable.WidthPercentage = 100;
                    InsuredClaimTable.DefaultCell.PaddingTop = 0;
                    InsuredClaimTable.DefaultCell.PaddingLeft = 0;
                    InsuredClaimTable.DefaultCell.PaddingRight = 0;
                    InsuredClaimTable.DefaultCell.PaddingBottom = 0;
                    InsuredClaimTable.DefaultCell.HorizontalAlignment = 0;
                    InsuredClaimTable.DefaultCell.Border = Rectangle.TOP_BORDER;

                    var InsuredClaimOtherPolicyTable = new PdfPTable(11);
                    InsuredClaimOtherPolicyTable.WidthPercentage = 100;
                    InsuredClaimOtherPolicyTable.DefaultCell.PaddingTop = 0;
                    InsuredClaimOtherPolicyTable.DefaultCell.PaddingLeft = 0;
                    InsuredClaimOtherPolicyTable.DefaultCell.PaddingRight = 0;
                    InsuredClaimOtherPolicyTable.DefaultCell.PaddingBottom = 0;
                    InsuredClaimOtherPolicyTable.DefaultCell.HorizontalAlignment = 0;
                    InsuredClaimOtherPolicyTable.DefaultCell.Border = Rectangle.NO_BORDER;


                    #endregion
                    #region FillDetails

                    #region Row1 (InfoHeader) 10 cols

                    PdfPCell cell = new PdfPCell(new Phrase("Sigorta Şirketi", fontBoldYellow));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Padding = 0;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 18f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    HeaderTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Poliçe Numarası", fontBoldYellow));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    HeaderTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Başlangıç Tarihi", fontBoldYellow));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    HeaderTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Bitiş Tarihi", fontBoldYellow));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    HeaderTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Poliçe Tipi", fontBoldYellow));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    HeaderTable.AddCell(cell);

                    //cell = new PdfPCell(new Phrase("Acente Adı", fontBoldYellow));
                    //cell.Border = Rectangle.NO_BORDER;
                    //cell.BackgroundColor = baseColorHeader;
                    //cell.Colspan = 1;
                    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    //HeaderTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Sigorta Ettiren", fontBoldYellow));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    HeaderTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Sigorta Ettiren Tckn", fontBoldYellow));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    HeaderTable.AddCell(cell);

                    //string Setckn = formInsuredProfile.INSURED_IDENTITY_NO.ToString();
                    //string Seilkdört = Setckn.Substring(0, 4);
                    //string Sesondört = Setckn.Remove(0, 7);
                    //string SeSifreliTckn = Seilkdört + "***" + Sesondört;
                    //cell = new PdfPCell(new Phrase(SeSifreliTckn, fontNormalBlue));
                    //cell.Border = Rectangle.NO_BORDER;
                    //cell.BackgroundColor = baseColorContent;
                    //cell.Colspan = 1;
                    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    //HeaderTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Holding Adı", fontBoldYellow));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    HeaderTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Sigortalı Aktif mi ?", fontBoldYellow));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    HeaderTable.AddCell(cell);

                    #endregion
                    #region Row2 (InfoContent) 10 cols
                    cell = new PdfPCell(new Phrase(formInsuredProfile.COMPANY_NAME, fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 18f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    HeaderTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formInsuredProfile.POLICY_NUMBER, fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    HeaderTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formInsuredProfile.POLICY_START_DATE, fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    HeaderTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formInsuredProfile.POLICY_END_DATE, fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    HeaderTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formInsuredProfile.POLICY_TYPE, fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    HeaderTable.AddCell(cell);

                    //cell = new PdfPCell(new Phrase(formInsuredProfile.AGENCY_NAME, fontNormalBlue));
                    //cell.Border = Rectangle.NO_BORDER;
                    //cell.BackgroundColor = baseColorContent;
                    //cell.Colspan = 1;
                    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    //HeaderTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formInsuredProfile.INSURER_NAME, fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    HeaderTable.AddCell(cell);

                    //cell = new PdfPCell(new Phrase(formInsuredProfile.INSURER_IDENTITY_NO, fontNormalBlue));
                    //cell.Border = Rectangle.NO_BORDER;
                    //cell.BackgroundColor = baseColorContent;
                    //cell.Colspan = 1;
                    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    //HeaderTable.AddCell(cell);

                    string Setckn = formInsuredProfile.INSURED_IDENTITY_NO.ToString();
                    string Seilkdört = Setckn.Substring(0, 2);
                    string Sesondört = Setckn.Remove(0, 9);
                    string SeSifreliTckn = Seilkdört + "*******" + Sesondört;
                    cell = new PdfPCell(new Phrase(SeSifreliTckn, fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    HeaderTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("", fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    HeaderTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formInsuredProfile.INSURED_STATUS, fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    HeaderTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Sigortalı Bilgileri", fontHeaderBold));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 10;
                    cell.Padding = 7f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    HeaderTable.AddCell(cell);

                    #endregion

                    #region Row3 (InsuredInfoHeader) 14 cols

                    cell = new PdfPCell(new Phrase("Sigortalı No", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1; cell.Padding = 0;
                    cell.PaddingTop = 0;
                    cell.MinimumHeight = 18f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Başlangıç Tarihi", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 18f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Şirket Başlangıç Tar.", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 18f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("İlk Sigortalılık Tar.", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 18f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Doğum Tarihi", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 18f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Cinsiyet", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 18f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Sigortalı Tckn", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 18f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Sigortalı Adı", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 18f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Durum", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 18f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Çıkış Tarihi", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 18f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Prim", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 18f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Tazminat Tutarı", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 18f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("H/P/(%)", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 18f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("YG", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 18f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Surprim(%)", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 18f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    #endregion
                    #region Row4 (InsuredInfoContent) 14 cols

                    cell = new PdfPCell(new Phrase(formInsuredProfile.CONTACT_ID, fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 40f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    formInsuredProfile.INSURED_START_DATE = formInsuredProfile.INSURED_START_DATE == null ? "" : formInsuredProfile.INSURED_START_DATE;

                    cell = new PdfPCell(new Phrase(formInsuredProfile.INSURED_START_DATE.Replace("00:00:00", ""), fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;

                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    formInsuredProfile.INSURED_COMPANY_START_DATE = formInsuredProfile.INSURED_COMPANY_START_DATE == null ? "" : formInsuredProfile.INSURED_COMPANY_START_DATE;

                    cell = new PdfPCell(new Phrase(formInsuredProfile.INSURED_COMPANY_START_DATE.Replace("00:00:00", ""), fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    formInsuredProfile.FIRST_INSURED_DATE = formInsuredProfile.FIRST_INSURED_DATE == null ? "" : formInsuredProfile.FIRST_INSURED_DATE;

                    cell = new PdfPCell(new Phrase(formInsuredProfile.FIRST_INSURED_DATE.Replace("00:00:00", ""), fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    formInsuredProfile.INSURED_BIRTHDATE = formInsuredProfile.INSURED_BIRTHDATE == null ? "" : formInsuredProfile.INSURED_BIRTHDATE;

                    cell = new PdfPCell(new Phrase(formInsuredProfile.INSURED_BIRTHDATE.Replace("00:00:00", ""), fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formInsuredProfile.INSURED_GENDER, fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    //cell = new PdfPCell(new Phrase(formInsuredProfile.INSURED_IDENTITY_NO, fontNormalBlue));
                    //cell.Border = Rectangle.NO_BORDER;
                    //cell.BackgroundColor = baseColorContent;
                    //cell.Colspan = 1;
                    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    //InsuredInfoTable.AddCell(cell);

                    string Stckn = formInsuredProfile.INSURED_IDENTITY_NO.ToString();
                    string Silkdört = Stckn.Substring(0, 4);
                    string Ssondört = Stckn.Remove(0, 7);
                    string SSifreliTckn = Silkdört + "***" + Ssondört;
                    cell = new PdfPCell(new Phrase(SSifreliTckn, fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formInsuredProfile.INSURED_NAME_SURNAME, fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formInsuredProfile.INSURED_STATUS, fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formInsuredProfile.INSURED_EXIT_DATE, fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formInsuredProfile.INSURED_PREMIUM, fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formInsuredProfile.CLAIM_TOTAL_AMOUNT, fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    string Hp = string.IsNullOrEmpty(formInsuredProfile.CLAIM_TOTAL_AMOUNT) ? "" : (decimal.Parse(formInsuredProfile.CLAIM_TOTAL_AMOUNT) / decimal.Parse(formInsuredProfile.INSURED_PREMIUM)).ToString();

                    cell = new PdfPCell(new Phrase(Hp, fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formInsuredProfile.RENEWAL_GUARANTEE_TYPE.IsInt() ? "VAR" : "", fontNormalBlue));

                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(formInsuredProfile.INSURED_SURPREMIUM, fontNormalBlue));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredInfoTable.AddCell(cell);

                    #endregion

                    #region Row5 (OtherPolicy) 1 cols

                    cell = new PdfPCell(new Phrase("Sigortalı Diğer Poliçeleri", fontHeaderBold));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 30f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    SubTableCols.AddCell(cell);

                    #region OtherPolicyTable Header 6 cols

                    cell = new PdfPCell(new Phrase("Poliçe Numarası", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    OtherPolicyTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Başlangıç Tarihi", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    OtherPolicyTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Bitiş Tarihi", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    OtherPolicyTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Prim", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    OtherPolicyTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Tazminat Tutarı", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    OtherPolicyTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("H/P (%) ", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    OtherPolicyTable.AddCell(cell);

                    #endregion
                    #region OtherPolicyTable Content 6 cols

                    foreach (var itemOtherPolicy in formInsuredOtherPolicy)
                    {
                        cell = new PdfPCell(new Phrase(itemOtherPolicy.POLICY_NUMBER, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        OtherPolicyTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemOtherPolicy.POLICY_START_DATE, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        OtherPolicyTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemOtherPolicy.POLICY_END_DATE, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        OtherPolicyTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemOtherPolicy.INSURED_PREMIUM, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        OtherPolicyTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemOtherPolicy.CLAIM_TOTAL_AMOUNT, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        OtherPolicyTable.AddCell(cell);

                        string HpOtherPolicy = string.IsNullOrEmpty(itemOtherPolicy.CLAIM_TOTAL_AMOUNT) ? "" : (decimal.Parse(itemOtherPolicy.CLAIM_TOTAL_AMOUNT) / decimal.Parse(itemOtherPolicy.INSURED_PREMIUM)).ToString();

                        cell = new PdfPCell(new Phrase(HpOtherPolicy, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        OtherPolicyTable.AddCell(cell);
                    }

                    #endregion

                    SubTableCols.AddCell(OtherPolicyTable);

                    #endregion

                    #region Row6 (Decleration) 1 cols

                    cell = new PdfPCell(new Phrase("Beyanlar", fontHeaderBold));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 30f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    SubTableCols2.AddCell(cell);

                    #region Decleration Header
                    cell = new PdfPCell(new Phrase("Sıra No", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 0;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    DeclerationTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("İstisna", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    DeclerationTable.AddCell(cell);

                    #endregion
                    #region Decleration Content 2 cols
                    foreach (var decleration in formInsuredDecleration)
                    {
                        cell = new PdfPCell(new Phrase("", fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        DeclerationTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(decleration.DECLERATION, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        DeclerationTable.AddCell(cell);
                    }
                    SubTableCols2.AddCell(DeclerationTable);
                    #endregion

                    #endregion

                    #region Row7 Notes

                    cell = new PdfPCell(new Phrase("Notları", fontHeaderBold));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 30f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    SubTableCols3.AddCell(cell);

                    #region Notes Header
                    cell = new PdfPCell(new Phrase("Sıra No", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 0;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    NotesTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("İstisna", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    NotesTable.AddCell(cell);

                    #endregion
                    #region Notes Content 2 cols
                    foreach (var note in formInsuredNote)
                    {
                        cell = new PdfPCell(new Phrase("", fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        NotesTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(note.NOTE, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        NotesTable.AddCell(cell);
                    }
                    SubTableCols3.AddCell(DeclerationTable);

                    #endregion
                    #endregion

                    #region Row8 Exclusions

                    cell = new PdfPCell(new Phrase("Özel İstisnalar", fontHeaderBold));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 30f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    SubTableCols4.AddCell(cell);

                    #region Notes Header
                    cell = new PdfPCell(new Phrase("Sıra No", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 0;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    ExclusionTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("İstisna", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    ExclusionTable.AddCell(cell);

                    #endregion
                    #region Exclusion Content 2 cols
                    foreach (var exclusion in formInsuredExclusion)
                    {
                        cell = new PdfPCell(new Phrase("", fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        ExclusionTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(exclusion.EXCLUSION, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        ExclusionTable.AddCell(cell);
                    }
                    SubTableCols4.AddCell(ExclusionTable);

                    #endregion
                    #endregion

                    #region Row9 InsuredCoverage

                    cell = new PdfPCell(new Phrase("Poliçe Teminatları", fontHeaderBold));
                    cell.Border = Rectangle.TOP_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 30f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    SubTableCols5.AddCell(cell);

                    #region Insured Coverage Header

                    cell = new PdfPCell(new Phrase("Plan No", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 0;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    InsuredCoverageTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Plan Adı", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredCoverageTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Paket No", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredCoverageTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Paket Adı", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredCoverageTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Teminat Adı", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.MinimumHeight = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredCoverageTable.AddCell(cell);

                    #endregion
                    #region Insured Coverage Content

                    foreach (var itemPolCov in formPolicyCoverage)
                    {
                        cell = new PdfPCell(new Phrase(itemPolCov.PLAN_NO, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredCoverageTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemPolCov.PLAN_NAME, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        InsuredCoverageTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemPolCov.PACKAGE_NO, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        InsuredCoverageTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemPolCov.PACKAGE_NAME, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        InsuredCoverageTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemPolCov.COVERAGE_NAME, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        InsuredCoverageTable.AddCell(cell);
                    }

                    SubTableCols5.AddCell(InsuredCoverageTable);
                    #endregion
                    #endregion

                    #region InsuredClaims
                    #region InsuredClaims Header

                    cell = new PdfPCell(new Phrase("Sigortalı Tazminatları", fontHeaderBold));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.PaddingTop = 15f;
                    cell.Colspan = 11;
                    cell.MinimumHeight = 20f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    InsuredClaimTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Zarf No", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.PaddingTop = 0;
                    cell.MinimumHeight = 30f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Poliçe No", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Provizyon No", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Tazminat Tarihi", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Hasar Notu", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Tanı", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Tedavi", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Teminat", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Durum", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Talep Tutar", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Şirket Tutar", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimTable.AddCell(cell);

                    #endregion
                    #region InsuredClaims Content

                    foreach (var itemClaim in formInsuredClaim)
                    {
                        cell = new PdfPCell(new Phrase(itemClaim.PLAN_ID, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.POLICY_NO, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.CLAIM_ID, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.CLAIM_DATE, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.CLAIM_NOTE_LIST, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.ICD_LIST, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.PROCESS_LIST, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.COVERAGE_NAME, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.CLAIM_STATUS, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.REQUESTED, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.PAID, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimTable.AddCell(cell);
                    }
                    #endregion
                    #endregion

                    #region InsuredClaimsOtherPolicy

                    #region InsuredClaimsOtherPolicy Header

                    cell = new PdfPCell(new Phrase("Sigortalı Diğer Poliçe Tazminatları", fontHeaderBold));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.PaddingTop = 15f;
                    cell.Colspan = 11;
                    cell.MinimumHeight = 20f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    InsuredClaimOtherPolicyTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Zarf No", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.PaddingTop = 0;
                    cell.MinimumHeight = 30f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimOtherPolicyTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Poliçe No", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimOtherPolicyTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Provizyon No", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimOtherPolicyTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Tazminat Tarihi", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimOtherPolicyTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Hasar Notu", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimOtherPolicyTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Tanı", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimOtherPolicyTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Tedavi", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimOtherPolicyTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Teminat", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimOtherPolicyTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Durum", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimOtherPolicyTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Talep Tutar", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimOtherPolicyTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Şirket Tutar", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorHeader;
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    InsuredClaimOtherPolicyTable.AddCell(cell);

                    #endregion
                    #region InsuredClaimsOtherPolicy Content

                    foreach (var itemClaim in formInsuredClaimOtherPolicy)
                    {
                        cell = new PdfPCell(new Phrase(itemClaim.PLAN_ID, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimOtherPolicyTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.POLICY_NO, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimOtherPolicyTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.CLAIM_ID, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimOtherPolicyTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.CLAIM_DATE, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimOtherPolicyTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.CLAIM_NOTE_LIST, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimOtherPolicyTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.ICD_LIST, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimOtherPolicyTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.PROCESS_LIST, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimOtherPolicyTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.COVERAGE_NAME, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimOtherPolicyTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.CLAIM_STATUS, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimOtherPolicyTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.REQUESTED, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimOtherPolicyTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(itemClaim.PAID, fontNormalBlue));
                        cell.Border = Rectangle.TOP_BORDER;
                        cell.BackgroundColor = baseColorContent;
                        cell.Colspan = 1;
                        cell.MinimumHeight = 0;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        InsuredClaimOtherPolicyTable.AddCell(cell);
                    }
                    #endregion
                    #endregion


                    #region SetDocument


                    mainTable.AddCell(HeaderTable);
                    mainTable.AddCell(InsuredInfoTable);
                    mainTable.AddCell(SubTableCols);
                    mainTable.AddCell(SubTableCols2);
                    mainTable.AddCell(SubTableCols3);
                    mainTable.AddCell(SubTableCols4);
                    mainTable.AddCell(SubTableCols5);

                    mainTable.AddCell(InsuredClaimTable);
                    mainTable.AddCell(InsuredClaimOtherPolicyTable);


                    cell = new PdfPCell(new Phrase("", fontBoldYellow));
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BackgroundColor = baseColorContent;
                    cell.Colspan = 1; cell.Padding = 0;
                    cell.PaddingTop = 0;
                    cell.MinimumHeight = 35f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    mainTable.AddCell(cell);

                    #endregion

                    #endregion
                }
                document.Add(mainTable);
                document.Close();
                writer.Close();
                fs.Close();
                if (request.IsWebRequest)
                {
                    request.FileName = !string.IsNullOrEmpty(request.FileName) ? request.FileName : ("AILE_BAZINDA_GECIS_PROFILI");
                    PrintOut(_path, request.FileName);
                }
                response.Url = _path;
                response.FileName = request.FileName;
                response.Code = "100";
            }
            catch (Exception ex) { response.Code = "999"; response.Message = ex.Message; }
            return response;
        }
    }
}
