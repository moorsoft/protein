﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Print.FamilyBasedTransferProfile
{
    public class FamilyBasedTransferProfileReq : CommonReq
    {
        public string FamilyNo { get; set; }
        public long PolicyId { get; set; }
    }
}
