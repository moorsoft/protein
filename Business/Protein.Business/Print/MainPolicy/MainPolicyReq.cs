﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Print.MainPolicy
{
    public class MainPolicyReq : CommonReq
    {
        public long PolicyId { get; set; }
    }
}
