﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Print.PolicyProfile
{
    public class PolicyProfileReq : CommonReq
    {
        public long PolicyId { get; set; }
    }
}
