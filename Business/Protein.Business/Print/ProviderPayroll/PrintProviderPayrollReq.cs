﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Print.ProviderPayroll
{
    public class PrintProviderPayrollReq : CommonReq
    {
        public long PayrollId { get; set; }
    }
}
