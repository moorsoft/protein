﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Protein.Business.Abstract.Print;
using Protein.Common.Extensions;
using Protein.Common.Resources;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Business.Print.ProviderPayroll
{
    public class PrintProviderPayroll : PrintUtility, IPrint<PrintProviderPayrollReq>
    {
        public PrintResponse DoWork(PrintProviderPayrollReq request)
        {
            PrintResponse response = new PrintResponse();
            try
            {
                #region FillPrintData
                V_FormPayroll payroll = new GenericRepository<V_FormPayroll>().FindBy($"PAYROLL_ID = {request.PayrollId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (payroll == null) { response.Code = "500"; response.Message = "Zarf Bilgisi Bulunamadı!"; return response; }
                List<V_Claim> claimPayroll = new GenericRepository<V_Claim>().FindBy($"PAYROLL_ID = {request.PayrollId}", orderby: "CLAIM_ID");
                if (claimPayroll.Count < 1)
                { response.Code = "500"; response.Message = "Zarfa Ait Hasar Kaydı Bulunamadı!"; return response; }
                #endregion

                string _path = "";
                if (request.IsWebRequest)
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints")))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints"));

                    _path = HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints") + "\\" + Guid.NewGuid() + ".pdf";
                }
                else { _path = request.LocalPath + "\\" + (!string.IsNullOrEmpty(request.FileName) ? request.FileName + ".pdf" : "KURUM_TAZMINAT_ODEME_LISTESI.pdf"); };

                int cnt = 0;
                while (true)
                {
                    cnt++;
                    if (File.Exists(_path))
                    {
                        _path = _path.Replace(".pdf", "") + "_" + cnt.ToString() + ".pdf";
                    }
                    else break;
                }

                System.IO.FileStream fs = new FileStream(_path, FileMode.Create);
                Document document = new Document(PageSize.A4.Rotate(), 20, 20, 50, 20);
                PdfWriter writer = PdfWriter.GetInstance(document, fs);
                document.AddAuthor("FATURA_İCMALİ");
                document.AddCreator("FATURA_İCMALİ");
                document.AddKeywords("FATURA_İCMALİ");
                document.AddSubject("FATURA_İCMALİ");
                document.AddTitle("FATURA_İCMALİ");
                document.Open();


                iTextSharp.text.pdf.BaseFont STF_Helvetica_Turkish = iTextSharp.text.pdf.BaseFont.CreateFont("Helvetica", "CP1254", iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED);

                //Image image = Image.GetInstance(GetCompanyLogoPath("İMECE"));
                //image.ScalePercent(10f);
                //document.Add(image);
                #region SetImages
                Image image = Image.GetInstance(GetCompanyLogoPath("İMECE"));
                image.ScaleAbsoluteWidth(10f);
                //Image image2 = Image.GetInstance(GetCompanyLogoPath(payroll.COMPANY_NAME));
                //image2.ScaleAbsoluteWidth(10f);

                var table = new PdfPTable(2);
                float[] widths = new float[] { 15f, 15f };
                table.SetWidths(widths);
                table.WidthPercentage = 100;
                //table.DefaultCell.PaddingRight = 70;
                //table.DefaultCell.PaddingLeft = 5;
                table.DefaultCell.Border = Rectangle.NO_BORDER;

                var cell1 = new PdfPCell();
                cell1.Colspan = 1;
                //cell.PaddingTop = 10;
                cell1.PaddingLeft = 5;
                cell1.PaddingRight = 70;
                cell1.Image = image;
                cell1.Border = 0;
                cell1.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                //cell.AddElement(image);
                table.AddCell(cell1);


                var cell2 = new PdfPCell();
                cell2.Colspan = 1;
                //cell.PaddingTop = 10;
                cell2.PaddingLeft = 40;

                //cell2.Image = image2;
                cell2.Border = 0;
                cell2.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                //cell.AddElement(image);
                table.AddCell(cell2);

                //table.AddCell(image);
                //table.AddCell(image2);

                document.Add(table);
                #endregion
                #region Header
                iTextSharp.text.Font fontHeader = new iTextSharp.text.Font(STF_Helvetica_Turkish, 14, Font.NORMAL, BaseColor.WHITE);
                var table2 = new PdfPTable(1);
                table2.WidthPercentage = 100;
                table2.DefaultCell.Border = Rectangle.NO_BORDER;
                PdfPCell cell = new PdfPCell(new Phrase($"{payroll.COMPANY_NAME} FATURA İCMALİ", fontHeader));
                cell.BackgroundColor = new BaseColor(80, 112, 240);//BaseColor.BLUE;
                cell.HorizontalAlignment = 1;
                table2.AddCell(cell);
                Paragraph ds = new Paragraph();
                Phrase phr = new Phrase(Environment.NewLine);
                ds.Add(phr);
                document.Add(ds);
                document.Add(table2);
                document.Add(new Paragraph());
                document.Add(ds);
                #endregion

                #region Info
                Font fontSubText = new Font(STF_Helvetica_Turkish, 9, Font.BOLD);
                Font fontSubValue = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL);

                var tableinfo = new PdfPTable(4);
                float[] widths2 = new float[] { 12f, 20f, 12f, 30f };
                tableinfo.SetWidths(widths2);

                tableinfo.DefaultCell.HorizontalAlignment = 0;

                tableinfo.WidthPercentage = 100;
                tableinfo.DefaultCell.Border = Rectangle.NO_BORDER;

                tableinfo.AddCell(new Phrase("İcmal No", fontSubText));
                tableinfo.AddCell(new Phrase($": {payroll.PAYROLL_ID}", fontSubValue));

                tableinfo.AddCell(new Phrase("Kurum Adı", fontSubText));
                tableinfo.AddCell(new Phrase($": {payroll.PROVIDER_NAME}", fontSubValue));

                tableinfo.AddCell(new Phrase("İcmal Tarihi", fontSubText));
                tableinfo.AddCell(new Phrase($": {(payroll.PAYROLL_DATE.IsDateTime() ? DateTime.Parse(payroll.PAYROLL_DATE).ToString("dd.MM.yyyy") : "")}", fontSubValue));

                tableinfo.AddCell(new Phrase("Ticari Ünvan", fontSubText));
                tableinfo.AddCell(new Phrase($": {payroll.PROVIDER_NAME}", fontSubValue));

                document.Add(tableinfo);
                document.Add(ds);
                document.Add(ds);
                #endregion

                if (claimPayroll != null && claimPayroll.Count > 0)
                {
                    int colCount = 14;
                    var tableClaimList = new PdfPTable(colCount);
                    tableClaimList.DefaultCell.HorizontalAlignment = 0;
                    tableClaimList.WidthPercentage = 100;

                    fontSubValue = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL, BaseColor.WHITE);

                    tableClaimList.AddCell(GetTableCell("Sıra No", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Fatura No", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Fatura Tarihi", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Provizyon No", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Provizyon Tipi", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Medula No", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Provizyon Tarihi", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Provizyon Durumu", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Sigorta Şirketi", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Adı Soyadı", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("TCKN", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Anlaşmalı Tutar", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Onaylanan Tutar", fontSubValue));
                    tableClaimList.AddCell(GetTableCell("Sigortalı Payı", fontSubValue));

                    fontSubValue = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL);
                    decimal REQUESTED = 0, INSURED_AMOUNT = 0, PAID = 0;

                    List<SimpleClaim> simpleClaimList = new List<SimpleClaim>();
                    int v = 1;
                    foreach (var item in claimPayroll)
                    {
                        decimal claimREQUESTED = 0, claimINSURED_AMOUNT = 0, claimPAID = 0;
                        claimREQUESTED = item.REQUESTED == null ? 0 : Convert.ToDecimal(item.REQUESTED);
                        claimPAID = item.PAID == null ? 0 : Convert.ToDecimal(item.PAID);
                        claimINSURED_AMOUNT = claimREQUESTED - claimPAID;

                        tableClaimList.AddCell(new Phrase($"{v.ToString()}", fontSubValue));
                        tableClaimList.AddCell(new Phrase($"{item.BILL_NO}", fontSubValue));
                        tableClaimList.AddCell(new Phrase($"{(item.BILL_DATE != null ? ((DateTime)item.BILL_DATE).ToString("dd.MM.yyyy") : "")}", fontSubValue));
                        tableClaimList.AddCell(new Phrase($"{item.CLAIM_ID}", fontSubValue));
                        tableClaimList.AddCell(new Phrase($" {LookupHelper.GetLookupTextByOrdinal(LookupTypes.Claim, item.CLAIM_TYPE)}", fontSubValue));
                        tableClaimList.AddCell(new Phrase($"{item.MEDULA_NO}", fontSubValue));
                        tableClaimList.AddCell(new Phrase($"{(item.CLAIM_DATE != null ? ((DateTime)item.CLAIM_DATE).ToString("dd.MM.yyyy") : "")}", fontSubValue));
                        tableClaimList.AddCell(new Phrase($"{LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, item.STATUS)}", fontSubValue));
                        tableClaimList.AddCell(new Phrase($"{item.COMPANY_NAME}", fontSubValue));
                        tableClaimList.AddCell(new Phrase($"{(item.FIRST_NAME + " " + item.LAST_NAME)}", fontSubValue));
                        tableClaimList.AddCell(new Phrase($"{item.IDENTITY_NO}", fontSubValue));
                        tableClaimList.AddCell(new Phrase($"{(item.REQUESTED == null ? "0" : Convert.ToDecimal(item.REQUESTED).ToString("0.00"))}", fontSubValue));
                        tableClaimList.AddCell(new Phrase($"{(item.PAID == null ? "0" : Convert.ToDecimal(item.PAID).ToString("0.00"))}", fontSubValue));
                        tableClaimList.AddCell(new Phrase($"{claimINSURED_AMOUNT}", fontSubValue));
                        REQUESTED += claimREQUESTED;
                        INSURED_AMOUNT += claimINSURED_AMOUNT;
                        PAID += claimPAID;
                        v++;
                    }
                    tableClaimList.DefaultCell.Border = Rectangle.NO_BORDER;
                    for (int i = 0; i < colCount - 4; i++)
                    {
                        tableClaimList.AddCell(new Phrase($"", fontSubValue));
                    }
                    tableClaimList.AddCell(new Phrase($"TOPLAM", fontSubText));
                    tableClaimList.AddCell(new Phrase($"{REQUESTED}", fontSubText));
                    tableClaimList.AddCell(new Phrase($"{PAID}", fontSubText));
                    tableClaimList.AddCell(new Phrase($"{INSURED_AMOUNT}", fontSubText));

                    document.Add(tableClaimList);
                }
                document.Close();
                writer.Close();
                fs.Close();
                if (request.IsWebRequest)
                {
                    request.FileName = !string.IsNullOrEmpty(request.FileName) ? request.FileName : ("FATURA_İCMALİ");
                    PrintOut(_path, request.FileName);
                }
                response.Url = _path;
                response.FileName = request.FileName;
                response.Code = "100";
            }
            catch (Exception ex) { response.Code = "999"; response.Message = ex.Message; }
            return response;
        }

        private PdfPCell GetTableCell(string v, Font font)
        {
            PdfPCell cellDetails = new PdfPCell(new Phrase(v, font));
            cellDetails.BackgroundColor = new BaseColor(80, 112, 240);//BaseColor.BLUE;
            cellDetails.HorizontalAlignment = 1;
            return cellDetails;
        }
    }
}
