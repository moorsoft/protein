﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Protein.Business.Abstract.Print;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Business.Print.ClaimProviderReturn
{
    public class PrintClaimProviderReturn : PrintUtility, IPrint<PrintClaimProviderReturnReq>
    {
        public PrintResponse DoWork(PrintClaimProviderReturnReq request)
        {
            PrintResponse response = new PrintResponse();
            try
            {
                #region FillPrintData
                V_FormClaimProviderReturn reject = new GenericRepository<V_FormClaimProviderReturn>().FindBy($"CLAIM_ID = {request.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (reject == null) { response.Code = "999"; response.Message = "Kayıt Bulunamadı"; return response; }
                #endregion

                string _path = "";
                if (request.IsWebRequest)
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints")))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints"));

                    _path = HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints") + "\\" + Guid.NewGuid() + ".pdf";
                }
                else { _path = request.LocalPath + "\\" + (!string.IsNullOrEmpty(request.FileName) ? request.FileName + ".pdf" : "TAZMINAT_KURUM_IADE.pdf"); };

                int cnt = 0;
                while (true)
                {
                    cnt++;
                    if (File.Exists(_path))
                    {
                        _path = _path.Replace(".pdf", "") + "_" + cnt.ToString() + ".pdf";
                    }
                    else break;
                }

                System.IO.FileStream fs = new FileStream(_path, FileMode.Create);
                Document document = new Document(PageSize.A4, 10, 10, 20, 20);

                PdfWriter writer = PdfWriter.GetInstance(document, fs);

                document.AddAuthor("TAZMINAT_KURUM_IADE");
                document.AddCreator("TAZMINAT_KURUM_IADE");
                document.AddKeywords("TAZMINAT_KURUM_IADE");
                document.AddSubject("TAZMINAT_KURUM_IADE");
                document.AddTitle("TAZMINAT_KURUM_IADE");
                document.Open();

                #region SetImages
                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(GetCompanyLogoPath(reject.COMPANY_NAME));
                image.ScaleAbsoluteWidth(10f);
                iTextSharp.text.Image image2 = iTextSharp.text.Image.GetInstance(GetCompanyLogoPath("İMECE"));
                image2.ScaleAbsoluteWidth(10f);

                iTextSharp.text.pdf.BaseFont STF_Helvetica_Turkish = iTextSharp.text.pdf.BaseFont.CreateFont("Helvetica", "CP1254", iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED);

                var table = new PdfPTable(2);
                float[] widths = new float[] { 15f, 15f };
                table.SetWidths(widths);
                table.WidthPercentage = 100;
                //table.DefaultCell.PaddingRight = 70;
                //table.DefaultCell.PaddingLeft = 5;
                table.DefaultCell.Border = Rectangle.NO_BORDER;

                var cell = new PdfPCell();
                cell.Colspan = 1;
                //cell.PaddingTop = 10;
                cell.PaddingLeft = 5;
                cell.PaddingRight = 70;
                cell.Image = image;
                cell.Border = 0;
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                //cell.AddElement(image);
                table.AddCell(cell);


                var cell2 = new PdfPCell();
                cell2.Colspan = 1;
                //cell.PaddingTop = 10;
                cell2.PaddingLeft = 40;

                cell2.Image = image2;
                cell2.Border = 0;
                cell2.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                //cell.AddElement(image);
                table.AddCell(cell2);

                //table.AddCell(image);
                //table.AddCell(image2);

                document.Add(table);
                #endregion
                #region Header
                iTextSharp.text.Font fontHeader = new iTextSharp.text.Font(STF_Helvetica_Turkish, 20, iTextSharp.text.Font.BOLD);
                Paragraph paragraphHeader = new Paragraph("TAZMİNAT KURUM İADE FORMU", fontHeader);
                paragraphHeader.Alignment = 1;
                paragraphHeader.Font.Size = 20;
                document.Add(paragraphHeader);
                document.Add(new Paragraph());
                Paragraph ds = new Paragraph();
                Phrase phr = new Phrase(Environment.NewLine);
                ds.Add(phr);
                ds.Add(phr);
                ds.Add(phr);
                ds.Add(phr);
                document.Add(ds);
                #endregion
                #region FillDetails

                var table2 = new PdfPTable(2);
                float[] widths2 = new float[] { 18f, 50f };
                table2.SetWidths(widths2);

                table2.DefaultCell.HorizontalAlignment = 0;

                table2.WidthPercentage = 100;
                table2.DefaultCell.Border = Rectangle.NO_BORDER;

                var cellDetail = new PdfPCell();
                cellDetail.Colspan = 2;
                cellDetail.PaddingTop = 30;
                cellDetail.Border = 0;
                cellDetail.PaddingLeft = 0;
                cellDetail.PaddingRight = 0;
                cellDetail.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                table2.AddCell(cellDetail);

                iTextSharp.text.Font fontSubText = new iTextSharp.text.Font(STF_Helvetica_Turkish, 9, iTextSharp.text.Font.BOLD);
                iTextSharp.text.Font fontSubValue = new iTextSharp.text.Font(STF_Helvetica_Turkish, 9, iTextSharp.text.Font.NORMAL);

                Phrase DateText = new Phrase("İade Tarih..................: ", fontSubText);
                Phrase ProviderNameText = new Phrase("Kurum Adı.......: ", fontSubText);
                Phrase InsuredText = new Phrase("Sigortalı Adı - Soyadı...: ", fontSubText);
                Phrase CompanyNameText = new Phrase("Şirket adı........:", fontSubText);
                Phrase ProvisionNumberText = new Phrase("Provizyon Numarası..: ", fontSubText);
                Phrase PackageNoText = new Phrase("Paket No.............:", fontSubText);
                Phrase BillNoText = new Phrase("Fatura No.............:", fontSubText);
                Phrase BillDateText = new Phrase("Fatura Tarihi.............:", fontSubText);
                Phrase DescriptionText = new Phrase("Açıklama...............:", fontSubText);

                Phrase DateValue = new Phrase($"{reject.ReturnDate}", fontSubValue);
                Phrase ProviderNameValue = new Phrase($"{reject.PROVIDER_NAME}", fontSubValue);
                Phrase InsuredValue = new Phrase($"{reject.INSURED_NAME_SURNAME}", fontSubValue);
                Phrase CompanyNameValue = new Phrase($"{reject.COMPANY_NAME}", fontSubValue);
                Phrase ProvisionNumberValue = new Phrase($"{reject.CLAIM_ID}", fontSubValue);
                Phrase PackageNoValue = new Phrase($"{reject.PayrollId}", fontSubValue);
                Phrase BillNoValue = new Phrase($"{reject.BILL_NO}", fontSubValue);
                Phrase BillDateValue = new Phrase($"{reject.BILL_DATE}", fontSubValue);
                Phrase DescriptionValue = new Phrase($"{reject.REASON_NOTE}", fontSubValue);
            

                table2.AddCell(DateText);
                table2.AddCell(DateValue);

                table2.AddCell(ProviderNameText);
                table2.AddCell(ProviderNameValue);

                table2.AddCell(InsuredText);
                table2.AddCell(InsuredValue);

                table2.AddCell(CompanyNameText);
                table2.AddCell(CompanyNameValue);

                table2.AddCell(ProvisionNumberText);
                table2.AddCell(ProvisionNumberValue);

                table2.AddCell(PackageNoText);
                table2.AddCell(PackageNoValue);

                table2.AddCell(BillNoText);
                table2.AddCell(BillNoValue);

                table2.AddCell(BillDateText);
                table2.AddCell(BillDateValue);

                table2.AddCell(DescriptionText);
                table2.AddCell(DescriptionValue);

                document.Add(table2);
                #endregion

                #region FillInfo
                iTextSharp.text.Font fontBold = new iTextSharp.text.Font(STF_Helvetica_Turkish, 9, iTextSharp.text.Font.BOLD);
                iTextSharp.text.Font fontNormal = new iTextSharp.text.Font(STF_Helvetica_Turkish, 9, iTextSharp.text.Font.NORMAL);
                Paragraph ds2 = new Paragraph();
                ds2.Add(phr);
                document.Add(ds2);
                document.Add(ds2);

                Phrase pr = new Phrase();
                Chunk chunk = new Chunk(" Yukarıda Poliçe bilgileri bulunan sigortalımıza ait tanı, tetkik ve tedavi harcamaları sigortalımızın poliçesinin özel ve genel şartları, teminat ve limitleri dahilinde değerlendirilmiş olup aşağıda belirtilen sebepler nedeni ile tarafınıza iade edilmiştir.", fontBold);
                pr.Add(chunk);
                Paragraph infoParag = new Paragraph();
                infoParag.Add(pr);
                document.Add(infoParag);
                document.Add(ds2);
                chunk = new Chunk(" Kurumunuz ile yapılan sözleşme Şartları gereğince fatura ve evrakların belirtilen açıklamaya göre düzenlenip tarafımıza tekrar iletilmesi halinde değerlendirme/düzeltme yapılacaktır.", fontBold);
                infoParag.Clear();
                infoParag = new Paragraph();
                infoParag.Add(chunk);
                document.Add(infoParag);
                document.Add(ds2);

                //infoParag.Clear();
                //infoParag = new Paragraph();
                //chunk = new Chunk($"İade sebebimiz aşağıda ,açıklaması ise üst yazı da olduğu gibidir. ", fontBold);
                //infoParag.Add(chunk);
                //document.Add(infoParag);
                //document.Add(ds2);

                infoParag.Clear();
                infoParag = new Paragraph();
                chunk = new Chunk($"{reject.REASON_NAME}", fontBold);
                infoParag.Add(chunk);
                document.Add(infoParag);
                document.Add(ds2);
                
                //TO-DO : Eksik Evrak Var ise burada gösterilmeli

                infoParag.Clear();
                infoParag = new Paragraph();
                chunk = new Chunk($"Bilgilerinize sunarız, ", fontBold);
                infoParag.Add(chunk);
                document.Add(infoParag);
                document.Add(ds2);

                infoParag.Clear();
                infoParag = new Paragraph();
                chunk = new Chunk($"Saygılarımızla, ", fontBold);
                infoParag.Add(chunk);
                document.Add(infoParag);
                document.Add(ds2);
                document.Add(ds2);

                infoParag.Clear();
                infoParag = new Paragraph();
                chunk = new Chunk($"İMECE DESTEK DANIŞMANLIK A.Ş., ", fontBold);
                infoParag.Add(chunk);
                document.Add(infoParag);

                infoParag.Clear();
                infoParag = new Paragraph();
                chunk = new Chunk($"Yıldız Posta Cad. Cerrahoğulları İş Merkezi ", fontBold);
                infoParag.Add(chunk);
                document.Add(infoParag);

                infoParag.Clear();
                infoParag = new Paragraph();
                chunk = new Chunk($"No:17 Kat:1/A Esentepe ŞİŞLİ/İSTANBUL ", fontBold);
                infoParag.Add(chunk);
                document.Add(infoParag);

                infoParag.Clear();
                infoParag = new Paragraph();
                chunk = new Chunk($"tazminat@imecedestek.com ", fontBold);
                infoParag.Add(chunk);
                document.Add(infoParag);

                infoParag.Clear();
                infoParag = new Paragraph();
                chunk = new Chunk($"0212 585 13 31 ", fontBold);
                infoParag.Add(chunk);
                document.Add(infoParag);
                #endregion

                document.Close();
                writer.Close();
                fs.Close();
                if (request.IsWebRequest)
                {
                    request.FileName = !string.IsNullOrEmpty(request.FileName) ? request.FileName : (reject.INSURED_NAME_SURNAME.ToUpper() + "_TAZMINAT_KURUM_IADE" + reject.PROVIDER_NAME + "_" + reject.PROVISION_DATE);
                    PrintOut(_path, request.FileName);
                }
                response.Url = _path;
                response.FileName = request.FileName;
                response.Code = "100";
            }
            catch (Exception ex) { response.Code = "999"; response.Message = ex.Message; }
            return response;
        }
    }
}
