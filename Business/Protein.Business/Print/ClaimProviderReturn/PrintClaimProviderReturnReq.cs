﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Print.ClaimProviderReturn
{
    public class PrintClaimProviderReturnReq : CommonReq
    {
        public long ClaimId { get; set; }
    }
}
