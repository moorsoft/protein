﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Print.ProvisionMissingDocForm
{
    public class PrintProvisionMissingDocFormReq : CommonReq
    {
        public long ClaimId { get; set; }
    }
}
