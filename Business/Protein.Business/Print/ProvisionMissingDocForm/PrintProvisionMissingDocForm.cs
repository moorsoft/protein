﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Protein.Business.Abstract.Print;
using Protein.Common.Extensions;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Business.Print.ProvisionMissingDocForm
{
    public class PrintProvisionMissingDocForm : PrintUtility, IPrint<PrintProvisionMissingDocFormReq>
    {
        public PrintResponse DoWork(PrintProvisionMissingDocFormReq request)
        {
            PrintResponse response = new PrintResponse();
            try
            {
                #region FillPrintData
                V_FormClaimMissing claimMissing = new GenericRepository<V_FormClaimMissing>().FindBy($"CLAIM_ID = {request.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                if (claimMissing == null) { response.Code = "999"; response.Message = "Eksik Evrak Bulunamadı!"; return response; }
                List<V_FormClaimMissingList> claimMissingLists = new GenericRepository<V_FormClaimMissingList>().FindBy($"CLAIM_ID = {request.ClaimId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                if (claimMissingLists.Count < 1)
                { response.Code = "999"; return response; }
                #endregion

                string _path = "";
                if (request.IsWebRequest)
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints")))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints"));

                    _path = HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints") + "\\" + Guid.NewGuid() + ".pdf";
                }
                else { _path = request.LocalPath + "\\" + (!string.IsNullOrEmpty(request.FileName) ? request.FileName + ".pdf" : "TAZMINAT_KURUM_IADE.pdf"); };

                int cnt = 0;
                while (true)
                {
                    cnt++;
                    if (File.Exists(_path))
                    {
                        _path = _path.Replace(".pdf", "") + "_" + cnt.ToString() + ".pdf";
                    }
                    else break;
                }


                System.IO.FileStream fs = new FileStream(_path, FileMode.Create);
                Document document = new Document(PageSize.A4, 20, 20, 50, 20);

                PdfWriter writer = PdfWriter.GetInstance(document, fs);

                document.AddAuthor("TEST");
                document.AddCreator("TEST");
                document.AddKeywords("TEST");
                document.AddSubject("TEST");
                document.AddTitle("TEST");
                document.Open();

                Image image = Image.GetInstance(GetCompanyLogoPath(claimMissing.COMPANY_NAME));

                image.ScalePercent(10f);
                document.Add(image);

                iTextSharp.text.pdf.BaseFont STF_Helvetica_Turkish = iTextSharp.text.pdf.BaseFont.CreateFont("Helvetica", "CP1254", iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED);

                #region Header
                iTextSharp.text.Font fontHeader = new iTextSharp.text.Font(STF_Helvetica_Turkish, 20, iTextSharp.text.Font.BOLD);
                Paragraph paragraphHeader = new Paragraph("PROVİZYON EKSİK EVRAK FORMU", fontHeader);
                paragraphHeader.Alignment = 1;
                paragraphHeader.Font.Size = 20;
                document.Add(paragraphHeader);
                document.Add(new Paragraph());
                Paragraph ds = new Paragraph();
                Phrase phr = new Phrase(Environment.NewLine);
                ds.Add(phr);
                ds.Add(phr);
                ds.Add(phr);
                ds.Add(phr);
                document.Add(ds);
                #endregion

                Font fontSubText = new Font(STF_Helvetica_Turkish, 9, Font.BOLD);
                Font fontSubValue = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL);

                var table2 = new PdfPTable(2);
                float[] widths2 = new float[] { 12f, 50f };
                table2.SetWidths(widths2);

                table2.DefaultCell.HorizontalAlignment = 0;

                table2.WidthPercentage = 100;
                table2.DefaultCell.Border = Rectangle.NO_BORDER;

                var cellDetail = new PdfPCell();
                cellDetail.Colspan = 2;
                cellDetail.PaddingTop = 30;
                cellDetail.Border = 0;
                cellDetail.PaddingLeft = 0;
                cellDetail.PaddingRight = 0;
                cellDetail.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                table2.AddCell(cellDetail);

                table2.AddCell(new Phrase("Tarih", fontSubText));
                table2.AddCell(new Phrase($":    {(claimMissing.MISSING_DATE.IsDateTime() ? DateTime.Parse(claimMissing.MISSING_DATE).ToString("dd.MM.yyyy") : "") }", fontSubValue));

                table2.AddCell(new Phrase("Sigortalı Adı - Soyadı", fontSubText));
                table2.AddCell(new Phrase($":    {claimMissing.INSURED_NAME_SURNAME}", fontSubValue));

                table2.AddCell(new Phrase("Poliçe Numarası", fontSubText));
                table2.AddCell(new Phrase($":    {claimMissing.POLICY_NUMBER}", fontSubValue));

                table2.AddCell(new Phrase("Müşteri Numarası", fontSubText));
                table2.AddCell(new Phrase($":    {claimMissing.INSURED_CONTACT_ID}", fontSubValue));

                table2.AddCell(new Phrase("Ürün Adı", fontSubText));
                table2.AddCell(new Phrase($":    {claimMissing.PRODUCT_NAME}", fontSubValue));

                table2.AddCell(new Phrase("Provizyon No", fontSubText));
                table2.AddCell(new Phrase($":    {claimMissing.CLAIM_ID}", fontSubValue));

                table2.AddCell(new Phrase("Kurum Adı", fontSubText));
                table2.AddCell(new Phrase($":    {claimMissing.PROVIDER_NAME}", fontSubValue));

                table2.AddCell(new Phrase("Acente Adı", fontSubText));
                table2.AddCell(new Phrase($":    {claimMissing.AGENCY_NAME}", fontSubValue));

                table2.AddCell(new Phrase("Tanı ", fontSubText));
                int i = 0;
                if (!claimMissing.TANI.IsNull())
                {
                    List<string> icdList = claimMissing.TANI.Split('#').ToList();
                    foreach (var item in icdList)
                    {
                        table2.AddCell(new Phrase($"{(i == 0 ? ":" : " ")}    {item}", fontSubValue));
                        i++;
                        if (i < icdList.Count)
                            table2.AddCell((new Phrase("", fontSubValue)));
                    }
                }
                else
                {
                    table2.AddCell((new Phrase("", fontSubValue)));
                }

                document.Add(table2);
                document.Add(ds);

                #region Paragraph

                Font fontBold = new Font(STF_Helvetica_Turkish, 10, Font.BOLD);
                Font fontNormal = new Font(STF_Helvetica_Turkish, 9, Font.NORMAL);
                Font fontUnderline = new Font(STF_Helvetica_Turkish, 9, Font.UNDERLINE);
                Font fontNewLine = new Font(STF_Helvetica_Turkish, 4, Font.NORMAL);

                document.Add(new Paragraph($"{claimMissing.COMPANY_NAME} sigortalısı yukarıda poliçe bilgileri bulunan sigortalımıza ait yapılmış/yapılması planlanan tanı, tetkik ve tedavi harcamaların provizyon talebi kuruluşunuz ile yapılan anlaşma gereğince sigortalımızın poliçesinin özel ve genel şartları, teminat ve limitleri dahilinde değerlendirilmiş ve aşağıda belirtilen belgelerin eksik olduğu tespit edilmiştir.", fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"Belgelerin gönderilmesi halinde talebiniz tekrar değerlendirmeye alınabilecektir.", fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"EKSİK EVRAKLAR", fontBold));
                i = 1;
                foreach (var item in claimMissingLists)
                {
                    document.Add(new Paragraph($"{i}. {item.MISSING_TYPE_TEXT}", fontBold));
                    i++;
                }

                document.Add(new Phrase(Environment.NewLine, fontNewLine));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"Bilgilerinize sunarız.", fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"Saygılarımızla,", fontNormal));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                document.Add(new Paragraph($"{claimMissing.COMPANY_NAME} ADINA", fontBold));
                document.Add(new Phrase(Environment.NewLine, fontNewLine));

                //document.Add(new Paragraph($"Telefon   : {claimMissing.COMPANY_PHONE}", fontBold));
                //document.Add(new Paragraph($"Fax       : {claimMissing.COMPANY_FAX}", fontBold));
                //document.Add(new Phrase(Environment.NewLine, fontNormal));

                #endregion

                document.Close();
                writer.Close();
                fs.Close();

                if (request.IsWebRequest)
                {
                    request.FileName = !string.IsNullOrEmpty(request.FileName) ? request.FileName : ("PROVİZYON_EKSİK_EVRAK_FORMU");
                    PrintOut(_path, request.FileName);
                }
                response.Url = _path;
                response.FileName = request.FileName;
                response.Code = "100";
            }
            catch (Exception ex) { response.Code = "999"; response.Message = ex.Message; }
            return response;
        }
    }
}
