﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Print.InsuredProfile
{
    public class InsuredProfileReq : CommonReq
    {
        public long InsuredId { get; set; }
        public long PolicyId { get; set; }
    }
}
