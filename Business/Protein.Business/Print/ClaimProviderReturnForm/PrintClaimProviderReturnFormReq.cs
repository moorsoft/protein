﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Print.ClaimProviderReturnForm
{
    public class PrintClaimProviderReturnFormReq : CommonReq
    {
        public List<long> ClaimIdList { get; set; } = new List<long>();
        //public long PayrollId { get; set; }
    }
}
