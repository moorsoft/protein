﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Protein.Business.Abstract.Print;
using Protein.Common.Extensions;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Business.Print.ClaimProviderReturnForm
{
    public class PrintClaimProviderReturnForm : PrintUtility, IPrint<PrintClaimProviderReturnFormReq>
    {
        public PrintResponse DoWork(PrintClaimProviderReturnFormReq request)
        {
            PrintResponse response = new PrintResponse();
            try
            {
                #region FillPrintData
                var claimIds = string.Join(",", request.ClaimIdList.ToArray());
                if (claimIds.IsNull()) { response.Code = "500"; response.Message = "Zarfa Ait Hasar Kaydı Bulunamadı!"; return response; }

                List<V_FormClaimProviderBack> providerBackList = new GenericRepository<V_FormClaimProviderBack>().FindBy($"CLAIM_ID IN ({claimIds})", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                if (providerBackList == null || providerBackList.Count < 1) { response.Code = "500"; response.Message = "Zarfa Ait Hasar Kaydı Bulunamadı!"; return response; }
                #endregion


                string _path = "";
                if (request.IsWebRequest)
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints")))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints"));

                    _path = HttpContext.Current.Server.MapPath("~/Uploads/PdfPrints") + "\\" + Guid.NewGuid() + ".pdf";
                }
                else { _path = request.LocalPath + "\\" + (!string.IsNullOrEmpty(request.FileName) ? request.FileName + ".pdf" : "TAZMINAT_KURUM_IADE.pdf"); };


                int cnt = 0;
                while (true)
                {
                    cnt++;
                    if (File.Exists(_path))
                    {
                        _path = _path.Replace(".pdf", "") + "_" + cnt.ToString() + ".pdf";
                    }
                    else break;
                }

                System.IO.FileStream fs = new FileStream(_path, FileMode.Create);
                Document document = new Document(PageSize.A4.Rotate(), 10, 10, 20, 20);

                PdfWriter writer = PdfWriter.GetInstance(document, fs);

                document.AddAuthor("TEST");
                document.AddCreator("TEST");
                document.AddKeywords("TEST");
                document.AddSubject("TEST");
                document.AddTitle("TEST");
                document.Open();

                #region SetImages
                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(GetCompanyLogoPath(providerBackList[0].COMPANY_NAME));
                image.ScaleAbsoluteWidth(10f);
                iTextSharp.text.Image image2 = iTextSharp.text.Image.GetInstance(GetCompanyLogoPath("İMECE"));
                image2.ScaleAbsoluteWidth(10f);

                iTextSharp.text.pdf.BaseFont STF_Helvetica_Turkish = iTextSharp.text.pdf.BaseFont.CreateFont("Helvetica", "CP1254", iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED);

                var table = new PdfPTable(2);
                float[] widths = new float[] { 15f, 15f };
                table.SetWidths(widths);
                table.WidthPercentage = 100;
                //table.DefaultCell.PaddingRight = 70;
                //table.DefaultCell.PaddingLeft = 5;
                table.DefaultCell.Border = Rectangle.NO_BORDER;

                var cell = new PdfPCell();
                cell.Colspan = 1;
                //cell.PaddingTop = 10;
                cell.PaddingLeft = 5;
                cell.PaddingRight = 70;
                cell.Image = image;
                cell.Border = 0;
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                //cell.AddElement(image);
                table.AddCell(cell);


                var cell2 = new PdfPCell();
                cell2.Colspan = 1;
                //cell.PaddingTop = 10;
                cell2.PaddingLeft = 40;

                cell2.Image = image2;
                cell2.Border = 0;
                cell2.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                //cell.AddElement(image);
                table.AddCell(cell2);

                //table.AddCell(image);
                //table.AddCell(image2);

                document.Add(table);
                #endregion

                #region Header
                iTextSharp.text.Font fontHeader = new iTextSharp.text.Font(STF_Helvetica_Turkish, 16, iTextSharp.text.Font.BOLD);
                Paragraph paragraphHeader = new Paragraph("TAZMİNAT KURUM İADE FORMU", fontHeader);
                paragraphHeader.Alignment = 1;
                paragraphHeader.Font.Size = 20;
                document.Add(paragraphHeader);
                document.Add(new Paragraph());
                Paragraph ds = new Paragraph();
                Phrase phr = new Phrase(Environment.NewLine);
                ds.Add(phr);
                document.Add(ds);
                #endregion

                #region Table

                iTextSharp.text.Font fontBold = new iTextSharp.text.Font(STF_Helvetica_Turkish, 9, iTextSharp.text.Font.BOLD);
                iTextSharp.text.Font fontNormal = new iTextSharp.text.Font(STF_Helvetica_Turkish, 9, iTextSharp.text.Font.NORMAL);

                var table3 = new PdfPTable(11);
                table3.DefaultCell.HorizontalAlignment = 0;
                table3.WidthPercentage = 100;

                table3.AddCell(new Phrase("Sigorta Şirketi", fontBold));
                table3.AddCell(new Phrase("Paket Geliş Tarihi", fontBold));
                table3.AddCell(new Phrase("Paket No", fontBold));
                table3.AddCell(new Phrase("Provizyon No", fontBold));
                table3.AddCell(new Phrase("Sigortalı Adı Soyadı", fontBold));
                table3.AddCell(new Phrase("Kurum Paket No", fontBold));
                table3.AddCell(new Phrase("Fatura Tarihi", fontBold));
                table3.AddCell(new Phrase("Fatura No", fontBold));
                table3.AddCell(new Phrase("İade Sebebi", fontBold));
                table3.AddCell(new Phrase("Açıklama", fontBold));
                table3.AddCell(new Phrase("Şirket Tutar", fontBold));

                foreach (var providerBack in providerBackList)
                {
                    table3.AddCell(new Phrase(providerBack.COMPANY_NAME, fontNormal));
                    table3.AddCell(new Phrase((providerBack.PAYROLL_DATE.IsDateTime() ? DateTime.Parse(providerBack.PAYROLL_DATE).ToString("dd.MM.yyyy") : ""), fontNormal));
                    table3.AddCell(new Phrase(providerBack.PACKAGE_NO, fontNormal));
                    table3.AddCell(new Phrase(providerBack.CLAIM_ID, fontNormal));
                    table3.AddCell(new Phrase(providerBack.INSURED_NAME_SURNAME, fontNormal));
                    table3.AddCell(new Phrase(providerBack.PROVIDER_PACKAGE_NO, fontNormal));
                    table3.AddCell(new Phrase((providerBack.BILL_DATE.IsDateTime() ? DateTime.Parse(providerBack.BILL_DATE).ToString("dd.MM.yyyy") : ""), fontNormal));
                    table3.AddCell(new Phrase(providerBack.BILL_NO, fontNormal));
                    table3.AddCell(new Phrase(providerBack.REASON, fontNormal));
                    string note = "";
                    if (!providerBack.CLAIM_NOTE_LIST.IsNull())
                    {
                        List<string> icdList = providerBack.CLAIM_NOTE_LIST.Split('#').ToList();
                        foreach (var item in icdList)
                        {
                            note += item + " / ";
                        }
                    }
                    table3.AddCell(new Phrase(note.IsNull() ? note : note.Substring(0, note.Length - 2), fontNormal));
                    //table3.AddCell(new Phrase(providerBack.CLAIM_NOTE_LIST, fontNormal));
                    table3.AddCell(new Phrase(providerBack.PAID, fontNormal));
                }

                document.Add(table3);
                #endregion

                document.Close();
                writer.Close();
                fs.Close();

                if (request.IsWebRequest)
                {
                    request.FileName = !string.IsNullOrEmpty(request.FileName) ? request.FileName : (providerBackList[0].INSURED_NAME_SURNAME.ToUpper() + "_TAZMINAT_KURUM_IADE" + providerBackList[0].PAYROLL_DATE);
                    PrintOut(_path, request.FileName);
                }
                response.Url = _path;
                response.FileName = request.FileName;
                response.Code = "100";
            }
            catch (Exception ex) { response.Code = "999"; response.Message = ex.Message; }
            return response;
        }
    }
}
