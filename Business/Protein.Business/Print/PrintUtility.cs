﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Protein.Business.Print
{
    public class PrintUtility
    {
        public void PrintOut(string Path, string FileName)
        {
            try
            {
                //HttpContext.Current.Response.ClearHeaders();
                //HttpContext.Current.Response.ContentType = "application/pdf";
                //HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + FileName +
                //".Pdf");
                //HttpContext.Current.Response.TransmitFile(Path);
                //HttpContext.Current.Response.End();
            }
            catch
            {

            }
        }
        public string GetCompanyLogoPath(string CompanyName)
        {
            string returnStr = "";
            try
            {
                if (CompanyName.ToUpper().Contains("DOGA"))
                    returnStr = HttpContext.Current.Server.MapPath("~/Uploads/PrintLogo/dogaSigorta.jpg");
                else if (CompanyName.ToUpper().Contains("HALK"))
                    returnStr = HttpContext.Current.Server.MapPath("~/Uploads/PrintLogo/halksigortalogo.png");
                else if (CompanyName.ToUpper().Contains("HDI"))
                    returnStr = HttpContext.Current.Server.MapPath("~/Uploads/PrintLogo/hdisigortalogo.png");
                else if (CompanyName.ToUpper().Contains("HAYAT"))
                    returnStr = HttpContext.Current.Server.MapPath("~/Uploads/PrintLogo/NNHayat.jpg");
                else if (CompanyName.ToUpper().Contains("NİPPON"))
                    returnStr = HttpContext.Current.Server.MapPath("~/Uploads/PrintLogo/nipponlogo.jpg");
                else if (CompanyName.ToUpper().Contains("ETHICA"))
                    returnStr = HttpContext.Current.Server.MapPath("~/Uploads/PrintLogo/ethicalogo.png");
                else if (CompanyName.ToUpper().Contains("KATILIM"))
                    returnStr = HttpContext.Current.Server.MapPath("~/Uploads/PrintLogo/katilimlogo.png");
                else if (CompanyName.ToUpper().Contains("GÜNEŞ") || CompanyName.ToUpper().Contains("VAKIF"))
                    returnStr = HttpContext.Current.Server.MapPath("~/Uploads/PrintLogo/gunessigortalogo.png");
                else if (CompanyName.ToUpper().Contains("İMECE"))
                    returnStr = HttpContext.Current.Server.MapPath("~/Uploads/PrintLogo/imeceLogo.jpg");
                else returnStr = HttpContext.Current.Server.MapPath("~/Uploads/PrintLogo/dogaSigorta.jpg");
            }
            catch
            {
                if (CompanyName.ToUpper().Contains("DOGA"))
                    returnStr = Environment.CurrentDirectory + "\\Resources\\dogaSigorta.jpg";
                else if (CompanyName.ToUpper().Contains("HALK"))
                    returnStr = Environment.CurrentDirectory + "\\Resources\\halksigortalogo.png";
                else if (CompanyName.ToUpper().Contains("HDI"))
                    returnStr = HttpContext.Current.Server.MapPath("~/Uploads/PrintLogo/hdisigortalogo.png");
                else if (CompanyName.ToUpper().Contains("HAYAT"))
                    returnStr = HttpContext.Current.Server.MapPath("~/Uploads/PrintLogo/NNHayat.jpg");
                else if (CompanyName.ToUpper().Contains("NIPPON"))
                    returnStr = Environment.CurrentDirectory + "\\Resources\\nipponlogo.jpg";
                else if (CompanyName.ToUpper().Contains("ETHICA"))
                    returnStr = Environment.CurrentDirectory + "\\Resources\\ethicalogo.png";
                else if (CompanyName.ToUpper().Contains("KATILIM"))
                    returnStr = Environment.CurrentDirectory + "\\Resources\\katilimlogo.png";
                else if (CompanyName.ToUpper().Contains("GUNES") || CompanyName.ToUpper().Contains("VAKIF"))
                    returnStr = Environment.CurrentDirectory + "\\Resources\\gunessigortalogo.png";
                else if (CompanyName.ToUpper().Contains("İMECE"))
                    returnStr = Environment.CurrentDirectory + "\\Resources\\imeceLogo.jpg";
                else returnStr = Environment.CurrentDirectory + "\\Resources\\dogaSigorta.jpg";
            }
            return returnStr;
        }
    }
}
