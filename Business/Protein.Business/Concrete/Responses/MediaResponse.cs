﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Concrete.Responses
{
    public class MediaResponse
    {
        public string Message { get; set; }
        public bool IsSuccess { get; set; } = false;
        public Stream stream { get; set; }
    }
}
