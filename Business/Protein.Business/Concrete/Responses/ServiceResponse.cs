﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Concrete.Responses
{
    public class ServiceResponse
    {
        public Int64 Id { get; set; }
        public Int64 PolicyId { get; set; }
        public Int64 EndorsementId { get; set; }
        public string Message { get; set; }
        public string Code { get; set; }
        public string SagmerMessage { get; set; }
        public string SagmerCode { get; set; }
        public bool IsSuccess { get { return (this.Code == "100" ? true : false); } }
    }
}
