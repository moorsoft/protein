﻿using NPOI.SS.UserModel;
using Protein.Business.Abstract.Import;
using Protein.Common.Abstract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Concrete.Responses
{
    public class ImportResponse
    {
        public string Message { get; set; }
        public bool IsSuccess { get; set; } = false;
        public Stream stream { get; set; }
    }
}
