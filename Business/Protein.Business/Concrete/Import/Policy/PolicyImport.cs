﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Protein.Business.Abstract.Import;
using Protein.Business.Abstract.Service;
using Protein.Business.Concrete.Import.Util;
using Protein.Business.Concrete.Responses;
using Protein.Business.Concrete.Services;
using Protein.Business.Concrete.Validator;
using Protein.Business.Enums.Import;
using Protein.Common.Dto.ImportObjects;
using Protein.Data.Workers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Protein.Common.Constants.Constants;

namespace Protein.Business.Import.Policy
{
    public class PolicyImport : IImport
    {
        public List<PolicyDto> SortPolicyList(List<PolicyDto> list)
        {
            list = list.OrderBy(o => o.PolicyNo).ToList().OrderBy(o=>o.PolicyNo).OrderBy(o => o.InsuredFamilyNo).OrderBy(o => o.InsuredIndividualType).ToList();
            return list;
        }
        public ImportResponse DoWorkAsync(Stream stream, ImportFileType fileType, ref IDictionary<Guid, double> tasks, Guid taskId, ref IDictionary<Guid, string> errorList, Int64 CompanyId = 0)
        {
            ImportResponse response = new ImportResponse();
            MemoryStream stream2 = new MemoryStream();
            List<PolicyDto> PolicyList = new List<PolicyDto>();

            stream.CopyTo(stream2);
            stream.Position = 0;
            stream2.Position = 0;
            response = new ImportValidator<PolicyDto>().CheckValidFile(stream2, fileType);

            //if (response.IsSuccess)
            PolicyList = new StreamToObject<PolicyDto>().ConvertIt(stream, fileType);

            ImportNow(PolicyList, CompanyId, tasks, taskId, errorList);

            //if (response.IsSuccess && PolicyList != null)
            //{
            //Import işlemi başlayabilir ASYNC

            //}
            //response.Message = "Beklenmedik bir hata!!!";
            return response;
        }

        private ImportResponse ImportNow(List<PolicyDto> list, Int64 CompanyId, IDictionary<Guid, double> tasks, Guid taskId, IDictionary<Guid, string> errorList)
        {
            ImportResponse response = new ImportResponse();
            try
            {
                list = SortPolicyList(list);
                string currentToken = new TokenWorker().GetValidToken(new UserWorker().GetUserID());
                Task.Factory.StartNew(() =>
                {
                    int rowCnt = 1;
                    List<string> cols = new List<string>();
                    int rowCount = list.Count;

                    foreach (PolicyDto item in list)
                    {
                        //if (rowCnt == 0) { rowCnt++; continue; } //skip header

                        //if (rowCnt == 0) { rowCnt++; continue; }

                        //if (CheckSameDoctor(Id, data.DIPLOMA_NO))
                        //{
                        //    response.IsSuccess = false;
                        //    errorList[taskId] += "Mevcut doktoru tekrar ekleyemezsiniz! <b> " + data.DIPLOMA_NO + " - " + data.FIRST_NAME + " " + data.LAST_NAME + "</b> : (Satır No: " + rowCnt.ToString() + " )<br>";
                        //}
                        //else response.IsSuccess = true;

                        IService<PolicyDto> policyService = new PolicyService();
                        item.isEndorsementChange = true;
                        item.isInsuredChange = true;
                        item.isInsurerChange = true;
                        item.EndorsementType = "1";
                        item.EndorsementNo = 0;
                        item.isPolicyChange = true;
                        item.isInsuredContactChange = true;
                        item.CompanyId = CompanyId;
                        if (!string.IsNullOrEmpty(item.InsuredNo)) item.isInsuredParameterChange = true;
                        item.policyIntegrationType = PolicyIntegrationType.Excel;
                        ServiceResponse serviceResponse = policyService.ServiceInsert(item, Token: currentToken);

                        if (!serviceResponse.IsSuccess)
                        {
                            errorList[taskId] += serviceResponse.Message + " : (Satır No: " + rowCnt.ToString() + ") <br>";
                        }

                        double s = Convert.ToDouble(((double.Parse(rowCnt.ToString()) / rowCount) * 100));
                        s = Math.Round(s, 2);
                        tasks[taskId] = s;
                        Thread.Sleep(20);
                        rowCnt++;
                    }
                    tasks.Remove(taskId);
                });
            }
            catch (Exception ex) { response.Message += ex.ToString(); response.IsSuccess = false; }
            finally { }
            return response;
        }
    }
}
