﻿using Protein.Business.Abstract.Import;
using Protein.Business.Concrete.Import.Util;
using Protein.Business.Concrete.Responses;
using Protein.Business.Concrete.Validator;
using Protein.Business.Enums.Import;
using Protein.Common.Dto;
using Protein.Common.Dto.ImportObjects;
using Protein.Common.Entities;
using Protein.Common.Extensions;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Data.Workers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Business.Concrete.Import.Payroll
{
    public class PayrollStatusImport : IImport
    {
        public ImportResponse DoWorkAsync(Stream stream, ImportFileType fileType, ref IDictionary<Guid, double> tasks, Guid taskId, ref IDictionary<Guid, string> errorList, Int64 CompanyId = 0)
        {
            ImportResponse response = new ImportResponse();
            MemoryStream stream2 = new MemoryStream();
            List<PayrollDto> PayrollList = new List<PayrollDto>();
            stream.CopyTo(stream2);
            stream.Position = 0;
            stream2.Position = 0;
            response = new ImportValidator<PayrollDto>().CheckValidFile(stream2, fileType);

            if (response.IsSuccess)
                PayrollList = new StreamToObject<PayrollDto>().ConvertIt(stream, fileType);

            if (response.IsSuccess && PayrollList.Count > 0)
            {
                ImportNow(PayrollList, tasks, taskId, errorList);
            }
            return response;
        }


        private ImportResponse ImportNow(List<PayrollDto> list, IDictionary<Guid, double> tasks, Guid taskId, IDictionary<Guid, string> errorList)
        {
            ImportResponse response = new ImportResponse();
            try
            {

                string currentToken = new TokenWorker().GetValidToken(new UserWorker().GetUserID());
                Task.Factory.StartNew(() =>
                {
                    int rowCnt = 1;
                    List<string> cols = new List<string>();
                    int rowCount = list.Count;

                    var payrollList = list.Where(l => l.PayrollId > 0).ToList();

                    if (payrollList != null && payrollList.Count > 0)
                    {
                        string idList = string.Join(",", payrollList.Select(a => a.PayrollId));

                        List<ProteinEntities.Payroll> payrollLst = new GenericRepository<ProteinEntities.Payroll>().FindBy($"ID IN ({idList}) OR OLD_PAYROLL_ID IN ({idList})", fetchDeletedRows: true);

                        List<V_Payroll> vPayrollList = new GenericRepository<V_Payroll>().FindBy($"PAYROLL_ID IN ({idList}) OR OLD_PAYROLL_ID IN ({idList})", orderby: "", fetchDeletedRows: true);

                        Dictionary<long, string> errList = new Dictionary<long, string>();
                        foreach (PayrollDto item in list.Where(l => l.PayrollId > 0))
                        {
                            try
                            {
                                ProteinEntities.Payroll payroll = payrollLst.Where(p => p.Id == item.PayrollId || p.OldPayrollId == item.PayrollId).FirstOrDefault();
                                if (payroll == null)
                                {
                                    throw new Exception("Zarf No Hatalı - Zarf Bilgisi Bulunamadı!");
                                }
                                var changeStatus = item.PayrollStatus;
                                if (payroll.Status == changeStatus)
                                {
                                    throw new Exception($"Zarf Durumu Daha Önceden {LookupHelper.GetLookupTextByOrdinal(LookupTypes.PayrollStatus, payroll.Status)} Olarak Değiştirilmiş. Tekrar Aynı İşlem Yapılamaz... Lütfen Kontrol Ediniz...");
                                }

                                V_Payroll vPayroll = vPayrollList.Where(p => p.PAYROLL_ID == payroll.Id).FirstOrDefault();
                                if (vPayroll.TOTAL_AMOUNT != item.TotalPaid)
                                {
                                    throw new Exception("Sistemdeki Tutar ile Uyuşmazlık! Zarf içerisindeki Hasarları Kontrol Ediniz...");
                                }

                                if (changeStatus == ((int)PayrollStatus.Odendi).ToString())
                                {
                                    if (payroll.Status != Convert.ToString((int)PayrollStatus.Onay))
                                    {
                                        throw new Exception("Zarf Durumu Onay olmadan zarf durumunu ÖDENDİ'ye çekemezsiniz. Lütfen Zarf Durumunu Güncelleyiniz...");
                                    }

                                    List<ProteinEntities.Claim> claimList = new GenericRepository<ProteinEntities.Claim>().FindBy("PAYROLL_ID=:payrollId", parameters: new { payrollId = payroll.Id });
                                    if (claimList == null)
                                    {
                                        throw new Exception("Hasarlar Bulunurken Hata Oluştu!");
                                    }
                                    if (claimList.Count < 1)
                                    {
                                        throw new Exception("Zarf İçerisinde Hasar Kaydı Bulunamadı.");
                                    }
                                    var result = new AjaxResultDto<ClaimStatusResult>();

                                    foreach (var claim in claimList)
                                    {
                                        if (claim.Status != ((int)ClaimStatus.ODENECEK).ToString() && claim.Status != ((int)ClaimStatus.ODENDI).ToString())
                                        {
                                            result.ResultMessage += ($" {claim.Id} No'lu Hasar Statüsü Uygun Değil!");
                                        }

                                        var claimPayment = new GenericRepository<ClaimPayment>().FindBy($"CLAIM_ID ={claim.Id}").FirstOrDefault();
                                        if (claimPayment == null)
                                        {
                                            V_Claim v_Claim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID={claim.Id}", orderby: "", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                                            if (v_Claim != null)
                                            {
                                                long bankId = 0;
                                                if (v_Claim.PAYMENT_METHOD == ((int)ClaimPaymentMethod.KURUM).ToString())
                                                {
                                                    var bankAccount = new GenericRepository<V_ProviderBankAccount>().FindBy($"PROVIDER_ID={v_Claim.PROVIDER_ID}", orderby: "").FirstOrDefault();
                                                    bankId = bankAccount != null ? (long)bankAccount.BANK_ACCOUNT_ID : 0;
                                                }
                                                else
                                                {
                                                    var insuredAccount = new GenericRepository<V_InsuredBankAccount>().FindBy($"INSURED_ID={v_Claim.INSURED_ID}", orderby: "").FirstOrDefault();
                                                    bankId = insuredAccount != null ? (long)insuredAccount.BANK_ACCOUNT_ID : 0;
                                                }

                                                claimPayment = new ClaimPayment
                                                {
                                                    Amount = v_Claim.PAID,
                                                    ClaimId = claim.Id,
                                                    PaymentType = ((int)PaymentType.BANKAYA_ODEME).ToString(),
                                                    DueDate = v_Claim.PAYROLL_DUE_DATE,
                                                    BankAccountId = bankId > 0 ? (long?)bankId : null
                                                };
                                                var spRes = new GenericRepository<ClaimPayment>().Insert(claimPayment);
                                            }

                                            // throw new Exception("Hasar Ödeme Bilgisi Bulunamadı!");
                                        }

                                        var claimBillList = new GenericRepository<ClaimBill>().FindBy("CLAIM_ID =:claimId", parameters: new { claimId = claim.Id }, orderby: "CLAIM_ID");
                                        if (claimBillList == null || claimBillList.Count <= 0)
                                        {
                                            result.ResultMessage += ($" {claim.Id} No'lu Hasara Ait Fatura Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                                        }
                                        else
                                        {
                                            foreach (var claimBill in claimBillList)
                                            {
                                                if (claimBill.Status != Convert.ToString((int)BillStatus.ODENDI))
                                                {
                                                    claimBill.Status = ((int)BillStatus.ODENDI).ToString();
                                                    new GenericRepository<ClaimBill>().Update(claimBill);
                                                }
                                            }
                                        }
                                    }
                                    if (!result.ResultMessage.IsNull())
                                    {
                                        throw new Exception(result.ResultMessage);
                                    }
                                    foreach (var claim in claimList)
                                    {
                                        if (claim.Status != ((int)ClaimStatus.ODENDI).ToString())
                                        {
                                            claim.Status = ((int)ClaimStatus.ODENDI).ToString();
                                            new GenericRepository<ProteinEntities.Claim>().Update(claim);
                                        }

                                        var claimPayment = new GenericRepository<ClaimPayment>().FindBy($"CLAIM_ID ={claim.Id}").FirstOrDefault();
                                        claimPayment.PaymentDate = item.Date;
                                        new GenericRepository<ClaimPayment>().Insert(claimPayment);
                                    }

                                    payroll.PaymentDate = item.Date;
                                }
                                else if (changeStatus == ((int)PayrollStatus.Onay_Bekler).ToString())
                                {
                                    if (payroll.Status == ((int)PayrollStatus.Odendi).ToString())
                                    {
                                        var claimList = new GenericRepository<ProteinEntities.Claim>().FindBy($" PAYROLL_ID=:payrollId", parameters: new { payrollId = payroll.Id }, fetchDeletedRows: true);
                                        foreach (var claim in claimList)
                                        {
                                            var claimPayment = new GenericRepository<ClaimPayment>().FindBy($"CLAIM_ID ={claim.Id}").FirstOrDefault();
                                            if (claimPayment != null)
                                            {
                                                claimPayment.PaymentDate = null;
                                                new GenericRepository<ClaimPayment>().UpdateForAll(claimPayment);
                                            }
                                        }
                                    }
                                    //TO_DO: Yetki Kontrolü
                                    //throw new Exception("Yetkili Kullanıcılar Dışında Zarfın Durumunu Onay Bekler'e Çekilemez. Lütfen Yetkilinize Danışın...");
                                }
                                else if (changeStatus == ((int)PayrollStatus.Onay).ToString())
                                {
                                    var claimList = new List<ProteinEntities.Claim>();
                                    if (payroll.CompanyId == 10 || payroll.CompanyId == 17)
                                    {
                                        claimList = new GenericRepository<ProteinEntities.Claim>().FindBy($"STATUS NOT IN :statusList AND PAYROLL_ID=:payrollId",
                                            parameters: new { payrollId = payroll.Id, statusList = new[] { ((int)ClaimStatus.ODENECEK).ToString(), ((int)ClaimStatus.ODENDI).ToString(), ((int)ClaimStatus.RET).ToString() } }, fetchDeletedRows: true);
                                        if (claimList.Count > 0)
                                        {
                                            //TO_DO: Güneş Ret Hasarların Klonlanması
                                            throw new Exception("Zarfı Onaylamak için İçerisindeki Hasarların ODENECEK veya RET Durumunda Olması Gerekmektedir. Lütfen Hasar Durumlarını Kontrol Ediniz...");
                                        }
                                    }
                                    else
                                    {
                                        claimList = new GenericRepository<ProteinEntities.Claim>().FindBy($"STATUS NOT IN :statusList AND PAYROLL_ID=:payrollId",
                                            parameters: new { payrollId = payroll.Id, statusList = new[] { ((int)ClaimStatus.ODENECEK).ToString(), ((int)ClaimStatus.ODENDI).ToString(), ((int)ClaimStatus.RET).ToString() } }, fetchDeletedRows: true);
                                        if (claimList.Count > 0)
                                        {
                                            throw new Exception("Zarfı Onaylamak için İçerisindeki Hasarların ODENECEK veya RET Durumunda Olması Gerekmektedir. Lütfen Hasar Durumlarını Kontrol Ediniz...");
                                        }
                                    }

                                    if (payroll.Status == ((int)PayrollStatus.Odendi).ToString())
                                    {
                                        foreach (var claim in claimList)
                                        {
                                            var claimPayment = new GenericRepository<ClaimPayment>().FindBy($"CLAIM_ID ={claim.Id}").FirstOrDefault();
                                            claimPayment.PaymentDate = null;
                                            new GenericRepository<ClaimPayment>().UpdateForAll(claimPayment);
                                        }
                                    }
                                }
                                else
                                {
                                    throw new Exception("Belirtilen Status Değeri Bulunamadı");
                                }

                                payroll.Status = changeStatus;
                                SpResponse spResponse = new GenericRepository<ProteinEntities.Payroll>().Update(payroll);
                                if (spResponse.Code != "100")
                                {
                                    throw new Exception("Zarf Durumu Değiştirilemedi");
                                }
                            }
                            catch (Exception ex)
                            {
                                errorList[taskId] += ex.Message + " : (İcmal No: " + item.PayrollId.ToString() + ") <br>";
                                errList.Add(item.PayrollId, ex.Message);
                            }

                            double s = Convert.ToDouble(((double.Parse(rowCnt.ToString()) / rowCount) * 100));
                            s = Math.Round(s, 2);
                            tasks[taskId] = s;
                            Thread.Sleep(20);
                            rowCnt++;
                        }

                        //#region Excel Export 

                        //List<string> lstPropDisplayName = new List<string>();
                        //ExportResponse excelResponse = new ExportResponse();
                        //List<string> lstPropName = new List<string>();

                        //string FileName = "ImportHataListesi.xlsx";

                        //IWorkbook workbook;
                        //workbook = new XSSFWorkbook(); // for xlsx 
                        //ISheet sheet2 = workbook.CreateSheet("Hata Listesi");
                        //IRow row2 = sheet2.CreateRow(0);

                        //List<string> lstSumDisplayName = new List<string>();
                        //lstSumDisplayName.Add("Zarf No");
                        //lstSumDisplayName.Add("Sonuç");

                        //int count = 0;
                        //int totalRow = 0;
                        //int columnCount = 0;
                        //foreach (string clmName in lstSumDisplayName)
                        //{
                        //    ICell celli = row2.CreateCell(count);
                        //    celli.SetCellValue(clmName);
                        //    count++;
                        //}
                        //columnCount = count;
                        //count = 0;
                        //foreach (var item in errList)
                        //{
                        //    IRow row = sheet2.CreateRow(count + 1);
                        //    int cellCount = 0;

                        //    ICell cell1 = row.CreateCell(cellCount);
                        //    cell1.SetCellValue(item.Key);
                        //    cellCount++;

                        //    ICell cell2 = row.CreateCell(cellCount);
                        //    cell2.SetCellValue(item.Value);

                        //    count++;
                        //    totalRow++;
                        //}

                        //CellRangeAddress ceRan = new CellRangeAddress(0, totalRow, 0, columnCount);
                        //string rAdd = ceRan.FormatAsString();
                        //sheet2.SetAutoFilter(CellRangeAddress.ValueOf(rAdd));


                        //using (var exportData = new MemoryStream())
                        //{
                        //    workbook.Write(exportData);
                        //    excelResponse.memoryStream = exportData;
                        //    excelResponse.Guid = Guid.NewGuid().ToString();
                        //    excelResponse.FileName = FileName;
                        //    excelResponse.IsDownloaded = true;
                        //}

                        //ExcelExport excelExport = new ExcelExport();
                        //excelExport.DoWork();

                        //#endregion
                    }
                    tasks.Remove(taskId);
                });
            }
            catch (Exception ex) { response.Message += ex.ToString(); response.IsSuccess = false; }
            finally { }
            return response;
        }
    }
}
