﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Protein.Business.Enums.Import;
using Protein.Common.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Concrete.Import.Util
{

    public class StreamToObject<T> where T : class, IImportObject, new()
    {
        private static bool IsNullableType(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
        }

        public List<T> ConvertIt(Stream stream, ImportFileType fileType)
        {
            List<T> returnList = new List<T>();
            try
            {
                stream.Position = 0;
                ISheet sheet;
                #region File Type Assign 
                if (fileType == ImportFileType.Xlsx)
                {
                    XSSFWorkbook hssfwb = new XSSFWorkbook(stream);
                    sheet = hssfwb.GetSheetAt(0);
                }
                else
                {
                    HSSFWorkbook hssfwb = new HSSFWorkbook(stream);
                    sheet = hssfwb.GetSheetAt(0);
                }
                #endregion

                int rowCnt = 0;
                List<string> cols = new List<string>();
                int rowCount = sheet.LastRowNum;

                foreach (IRow row in sheet)
                {
                    T data = new T(); // prop.setValue için temp 

                    foreach (ICell cell in row)
                    {
                        if (rowCnt == 0)
                        {
                            cols.Add(cell.ToString());
                            continue;
                        }


                        PropertyInfo prop = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                         .FirstOrDefault(x => x.GetCustomAttribute<ColumnAttribute>() != null
                                            && x.GetCustomAttribute<ColumnAttribute>().Name.Equals(cols[cell.ColumnIndex].ToString(), StringComparison.OrdinalIgnoreCase));
                        if (prop == null) continue;

                        var displayColumn = prop.GetCustomAttribute<ColumnAttribute>(true).Name;

                        if (string.IsNullOrEmpty(cell.ToString()))
                            continue;

                        Type propertyType = prop.PropertyType;
                        
                        Type targetType = IsNullableType(propertyType) ? Nullable.GetUnderlyingType(propertyType) : propertyType;
                        object convertedObj = Convert.ChangeType(!string.IsNullOrEmpty(cell.ToString()) ? cell.ToString() : null, targetType);

                        prop.SetValue(data, convertedObj, null);


                    }
                    if (rowCnt == 0) { rowCnt++; continue; }
                    returnList.Add(data);
                    rowCnt++;
                }
            }
            catch (Exception ex) { return null; }
            return returnList;
        }
    }
}