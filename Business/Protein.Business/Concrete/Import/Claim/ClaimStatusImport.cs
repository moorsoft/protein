﻿using Protein.Business.Abstract.Import;
using Protein.Business.Abstract.Service;
using Protein.Business.Concrete.Import.Util;
using Protein.Business.Concrete.Responses;
using Protein.Business.Concrete.Validator;
using Protein.Business.Enums.Import;
using Protein.Common.Dto;
using Protein.Common.Dto.ImportObjects;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Data.Workers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Business.Concrete.Import.Claim
{
    public class ClaimStatusImport : IImport
    {
        public ImportResponse DoWorkAsync(Stream stream, ImportFileType fileType, ref IDictionary<Guid, double> tasks, Guid taskId, ref IDictionary<Guid, string> errorList, Int64 CompanyId = 0)
        {
            ImportResponse response = new ImportResponse();
            MemoryStream stream2 = new MemoryStream();
            List<ClaimDto> ClaimList = new List<ClaimDto>();
            stream.CopyTo(stream2);
            stream.Position = 0;
            stream2.Position = 0;
            response = new ImportValidator<ClaimDto>().CheckValidFile(stream2, fileType);

            if (response.IsSuccess)
                ClaimList = new StreamToObject<ClaimDto>().ConvertIt(stream, fileType);

            if (response.IsSuccess && ClaimList.Count > 0)
            {
                ImportNow(ClaimList, tasks, taskId, errorList);
            }
            return response;
        }

        private ImportResponse ImportNow(List<ClaimDto> list, IDictionary<Guid, double> tasks, Guid taskId, IDictionary<Guid, string> errorList)
        {
            ImportResponse response = new ImportResponse();
            try
            {

                string currentToken = new TokenWorker().GetValidToken(new UserWorker().GetUserID());
                Task.Factory.StartNew(() =>
                {
                    int rowCnt = 1;
                    List<string> cols = new List<string>();
                    int rowCount = list.Count;

                    foreach (ClaimDto item in list.Where(l=>l.ClaimId>0))
                    {

                        var result = new AjaxResultDto<ClaimStatusResult>();
                        try
                        {
                            Common.Entities.ProteinEntities.Claim claim = new GenericRepository<Common.Entities.ProteinEntities.Claim>().FindById(item.ClaimId);
                            if (claim != null)
                            {
                                var status = item.ClaimStatus;
                                if (status == ((int)ClaimStatus.GIRIS).ToString())
                                {
                                    if (claim.PayrollId != null)
                                    {
                                        throw new Exception("Zarfa Eklenmiş Bir Hasar Giriş Durumuna Getirilemez! Zarftan Çıkardıktan Sonra Tekrar Deneyiniz...");
                                    }

                                    if (claim.Status != ((int)ClaimStatus.PROZIYON_ONAY).ToString() && claim.Status != ((int)ClaimStatus.RET).ToString() && claim.Status != ((int)ClaimStatus.IPTAL).ToString() && claim.Status != ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                                    {
                                        throw new Exception("Hasar Durumu Uygun Değil");
                                    }

                                    if ((claim.Status == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || claim.Status == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString()) && claim.ReasonId == 3)
                                    {
                                        var missingDoc = new GenericRepository<ClaimMissingDoc>().FindBy("CLAIM_ID=:claimId AND INCOMING_DATE IS NULL", parameters: new { claimId = claim.Id });
                                        if (missingDoc.Count > 0)
                                        {
                                            throw new Exception("Tüm Eksik Evrakların Geliş Tarihlerinin Girilmesi Gerekmektedir.");
                                        }
                                    }
                                }
                                else if (status == ((int)ClaimStatus.SILINDI).ToString())
                                {
                                    if (claim.PayrollId != null)
                                    {
                                        throw new Exception("Zarfa Eklenmiş Bir Hasar Silinemez! Zarftan Çıkardıktan Sonra Tekrar Deneyiniz...");
                                    }
                                    if (claim.Status != ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                                    {
                                        throw new Exception("Hasar Durumu Uygun Değil");
                                    }
                                }
                                else if (status == ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                                {
                                    if (claim.PayrollId != null)
                                    {
                                        var payroll = new GenericRepository<Common.Entities.ProteinEntities.Payroll>().FindById((long)claim.PayrollId);
                                        if (payroll.Status != ((int)PayrollStatus.Onay_Bekler).ToString())
                                        {
                                            throw new Exception("Onay Bekler Durumundaki Zarf Haricinde, Onay veya Ödendi Durumundaki Zarftaki Hasar Durumu PROVIZYON'a Çekilemez!");
                                        }
                                    }
                                    if (claim.Status != ((int)ClaimStatus.GIRIS).ToString() && claim.Status != ((int)ClaimStatus.RET).ToString() && claim.Status != ((int)ClaimStatus.ODENECEK).ToString() && claim.Status != ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString() && claim.Status != ((int)ClaimStatus.IPTAL).ToString() && claim.Status != ((int)ClaimStatus.ODENDI).ToString())
                                    {
                                        throw new Exception("Hasar Durumu Uygun Değil");
                                    }

                                    var vClaim = new GenericRepository<V_Claim>().FindBy(conditions: "CLAIM_ID =: claim_Id", orderby: "CLAIM_ID", parameters: new { claim_Id = claim.Id }, hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                                    if (vClaim.PAID == null || vClaim.PAID <= 0)
                                    {
                                        throw new Exception("Hasarın Şirket Tutarı 0'dan Büyük Olmalıdır!");
                                    }

                                    if (claim.PaymentMethod == ((int)ClaimPaymentMethod.KURUM).ToString())
                                    {
                                        var IcdList = new GenericRepository<V_ClaimIcd>().FindBy(conditions: "CLAIM_ID =: claim_Id", orderby: "CLAIM_ID", parameters: new { claim_Id = claim.Id });
                                        
                                        if (IcdList.Count <= 0)
                                            throw new Exception("Hasara Ait Tanı Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                                        if (claim.StaffId == null)
                                        {
                                            throw new Exception("Hasara Ait Doktor Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                                        }
                                    }
                                    else if (claim.PaymentMethod != ((int)ClaimPaymentMethod.SIGORTALI).ToString())
                                    {
                                        throw new Exception("Hasar Ödeme Tipi Uygun Değil");
                                    }

                                    var processList = new GenericRepository<V_ClaimProcess>().FindBy(conditions: "CLAIM_ID =: claim_Id", orderby: "CLAIM_ID", parameters: new { claim_Id = claim.Id });
                                    if (processList == null || processList.Count <= 0)
                                    {
                                        throw new Exception("Hasara Ait İşlem ya da Teminat Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                                    }
                                    else
                                    {
                                        foreach (V_ClaimProcess claimProcess in processList)
                                        {
                                            if (claimProcess.STATUS != Convert.ToString((int)ClaimStatus.GIRIS))
                                            {
                                                throw new Exception("Hasara Ait İşlem/İlaç/Teminatlardan PROVİZYON Durumunda Olmayanlar Var. Lütfen Hasarı Güncelleyiniz...");
                                            }
                                        }
                                    }

                                    foreach (var process in processList)
                                    {
                                        if (process.COVERAGE_ID == null)
                                        {
                                            throw new Exception("Hasara Ait Tüm İşlemlerin Teminat Eşlemesi Yapılması Gerekmektedir. Lütfen Hasarı Güncelleyiniz...");
                                        }
                                        if (process.COVERAGE_TYPE == ((int)CoverageType.YATARAK).ToString())
                                        {
                                            var hospitalization = new GenericRepository<ClaimHospitalization>().FindBy(conditions: "CLAIM_ID =: claim_Id", orderby: "CLAIM_ID", parameters: new { claim_Id = claim.Id }).FirstOrDefault();
                                            if (hospitalization == null || hospitalization.EntranceDate == null)
                                            {
                                                throw new Exception("Yatarak Teminatı Bulunan Hasarın Yatış Tarihi Girilmesi Gerekmektedir.");
                                            }
                                        }
                                    }

                                    if (vClaim.PAID != null && vClaim.PAID >= 10000)
                                    {
                                    }
                                }
                                else if (status == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || status == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                                {
                                    throw new Exception("Lütfen Gerekçe Giriniz!!!");
                                }
                                else if (status == ((int)ClaimStatus.RET).ToString())
                                {
                                    throw new Exception("Lütfen Gerekçe Giriniz!!!");
                                }
                                else if (status == ((int)ClaimStatus.ODENECEK).ToString())
                                {
                                    if (claim.Status != ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                                    {
                                        throw new Exception("Hasar Durumu Uygun Değil");
                                    }
                                    var claimBillList = new GenericRepository<V_ClaimBill>().FindBy(conditions: "CLAIM_ID =: claim_Id", orderby: "CLAIM_ID", parameters: new { claim_Id = claim.Id });
                                    if (claimBillList == null || claimBillList.Count <= 0)
                                    {
                                        throw new Exception("Hasara Ait Fatura Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                                    }
                                    if (claim.PayrollId == null || claim.PayrollId <= 0)
                                    {
                                        throw new Exception("Hasara Ait Zarf Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                                    }
                                    else
                                    {
                                        var payrollList = new GenericRepository<V_Payroll>().FindBy("PAYROLL_ID =:payrollId", parameters: new { payrollId = claim.PayrollId }, orderby: "PAYROLL_ID");
                                        if (payrollList == null || payrollList.Count <= 0)
                                        {
                                            throw new Exception("Hasara Ait Zarf Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                                        }
                                    }

                                    if (claim.PaymentMethod == ((int)ClaimPaymentMethod.SIGORTALI).ToString())
                                    {
                                        // var insuredId = Request["insuredId"];
                                        if (claim.InsuredId==null || claim.InsuredId<1)
                                        {
                                            throw new Exception("Hasara Ait Sigortalı Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                                        }
                                        var insuredBankAccount = new GenericRepository<V_InsuredBankAccount>().FindBy("INSURED_ID=:insuredId", parameters: new { insuredId = claim.InsuredId }, orderby: "INSURED_ID");
                                        if (insuredBankAccount == null || insuredBankAccount.Count <= 0)
                                        {
                                            throw new Exception("Sigortalıya Ait Banka Hesap Bilgisi Bulunamadı. Lütfen Sigortalı Bilgilerini Güncelleyiniz...");
                                        }
                                    }
                                    else if (claim.PaymentMethod == ((int)ClaimPaymentMethod.KURUM).ToString())
                                    {
                                        var providerankAccount = new GenericRepository<V_ProviderBankAccount>().FindBy("PROVIDER_ID=:providerId", parameters: new { providerId = claim.ProviderId }, orderby: "PROVIDER_ID");
                                        if (providerankAccount == null || providerankAccount.Count <= 0)
                                        {
                                            throw new Exception("Kuruma Ait Banka Hesap Bilgisi Bulunamadı. Lütfen Kurum Bilgilerini Güncelleyiniz...");
                                        }
                                    }

                                }
                                else if (status == ((int)ClaimStatus.IPTAL).ToString())
                                {
                                    if (claim.PayrollId != null)
                                    {
                                        var payroll = new GenericRepository<Common.Entities.ProteinEntities.Payroll>().FindById((long)claim.PayrollId);
                                        if (payroll.Status != ((int)PayrollStatus.Onay_Bekler).ToString())
                                        {
                                            throw new Exception("Onay Bekler Durumundaki Zarf Haricinde, Onay veya Ödendi Durumundaki Zarftaki Hasar Durumu PROVIZYON'a Çekilemez!");
                                        }
                                    }
                                    if (claim.Status != ((int)ClaimStatus.RET).ToString() && claim.Status != ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                                    {
                                        throw new Exception("Hasar Durumu Uygun Değil");
                                    }
                                }
                                else if (status == ((int)ClaimStatus.ODENDI).ToString())
                                {
                                    if (claim.Status != ((int)ClaimStatus.ODENECEK).ToString())
                                    {
                                        throw new Exception("Hasar Durumu Uygun Değil");
                                    }
                                    var claimBillList = new GenericRepository<V_ClaimBill>().FindBy(conditions: "CLAIM_ID =: claim_Id", orderby: "CLAIM_ID", parameters: new { claim_Id = claim.Id });
                                    if (claimBillList == null || claimBillList.Count <= 0)
                                    {
                                        throw new Exception("Hasara Ait Fatura Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                                    }
                                    else
                                    {
                                        foreach (V_ClaimBill claimBill in claimBillList)
                                        {
                                            if (claimBill.STATUS != Convert.ToString((int)BillStatus.ODENDI))
                                            {
                                                throw new Exception("Hasara Ait Fatura Durumu ÖDENDİ olmadan hasar durumunu ÖDENDİ'ye çekemezsiniz. Lütfen Hasarı Güncelleyiniz...");
                                            }
                                        }
                                    }
                                    if (claim.PayrollId == null || claim.PayrollId <= 0)
                                    {
                                        throw new Exception("Hasara Ait Zarf Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                                    }
                                    else
                                    {
                                        var payrollList = new GenericRepository<V_Payroll>().FindBy("PAYROLL_ID =:payrollId", parameters: new { payrollId = claim.PayrollId }, orderby: "PAYROLL_ID");
                                        if (payrollList == null || payrollList.Count <= 0)
                                        {
                                            throw new Exception("Hasara Ait Zarf Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                                        }
                                        else
                                        {
                                            foreach (V_Payroll payroll in payrollList)
                                            {
                                                if (payroll.STATUS != Convert.ToString((int)PayrollStatus.Onay))
                                                {
                                                    throw new Exception("Hasara Ait Zarf Durumu Onay olmadan hasar durumunu ÖDENDİ'ye çekemezsiniz. Lütfen Hasarı Güncelleyiniz...");
                                                }
                                                if (payroll.PAYMENT_DATE == null)
                                                {
                                                    throw new Exception("Hasara Ait Zarf Vade Tarihi girilmediğinden hasar durumunu ÖDENDİ'ye çekemezsiniz. Lütfen Hasarı Güncelleyiniz...");
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    throw new Exception("Belirtilen Status Değeri Bulunamadı");
                                }
                            }
                            else
                            {
                                throw new Exception("Hasar No Hatalı!");
                            }
                        }
                        catch (Exception ex)
                        {
                            errorList[taskId] += ex.Message + " : (Satır No: " + rowCnt.ToString() + ") <br>";
                        }
                        //IService<AgencyDto> agencyService = new AgencyService();
                        //ServiceResponse serviceResponse = agencyService.ServiceInsert(item, Token: currentToken);

                        //if (!serviceResponse.IsSuccess)
                        //{
                        //    errorList[taskId] += serviceResponse.Message + " : (Satır No: " + rowCnt.ToString() + ") <br>";
                        //}
                        double s = Convert.ToDouble(((double.Parse(rowCnt.ToString()) / rowCount) * 100));
                        s = Math.Round(s, 2);
                        tasks[taskId] = s;
                        Thread.Sleep(20);
                        rowCnt++;
                    }
                    tasks.Remove(taskId);
                });
            }
            catch (Exception ex) { response.Message += ex.ToString(); response.IsSuccess = false; }
            finally { }
            return response;
        }
    }
}
