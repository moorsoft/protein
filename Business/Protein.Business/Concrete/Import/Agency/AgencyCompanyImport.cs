﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Protein.Business.Abstract.Import;
using Protein.Business.Abstract.Service;
using Protein.Business.Concrete.Import.Util;
using Protein.Business.Concrete.Responses;
using Protein.Business.Concrete.Services;
using Protein.Business.Concrete.Validator;
using Protein.Business.Enums.Import;
using Protein.Common.Dto.ImportObjects;
using Protein.Data.Repositories;
using Protein.Data.Workers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Business.Concrete.Import.Agency
{
    public class AgencyCompanyImport : IImport
    {
        public ImportResponse DoWorkAsync(Stream stream, ImportFileType fileType, ref IDictionary<Guid, double> tasks, Guid taskId, ref IDictionary<Guid, string> errorList, Int64 CompanyId = 0)
        {
            ImportResponse response = new ImportResponse();
            MemoryStream stream2 = new MemoryStream();
            List<AgencyDto> AgencyList = new List<AgencyDto>();
            stream.CopyTo(stream2);
            stream.Position = 0;
            stream2.Position = 0;
            response = new ImportValidator<AgencyDto>().CheckValidFile(stream2, fileType);

            if (response.IsSuccess || (!response.IsSuccess && response.Message == "<b>Şirket No</b> kolonu kaynak dosyada bulunamadı! <br>"))
            {
                AgencyList = new StreamToObject<AgencyDto>().ConvertIt(stream, fileType);
                if (!response.IsSuccess)
                {
                    foreach (var item in AgencyList)
                    {
                        item.CompanyId = CompanyId;
                    }
                }
                response.IsSuccess = true;
                response.Message = "";
            }

            if (response.IsSuccess && AgencyList.Count > 0)
            {
                ImportNow(AgencyList, tasks, taskId, errorList);
            }
            return response;
        }
        private ImportResponse ImportNow(List<AgencyDto> list, IDictionary<Guid, double> tasks, Guid taskId, IDictionary<Guid, string> errorList)
        {
            ImportResponse response = new ImportResponse();
            try
            {

                string currentToken = new TokenWorker().GetValidToken(new UserWorker().GetUserID());
                Task.Factory.StartNew(() =>
                {
                    int rowCnt = 1;
                    List<string> cols = new List<string>();
                    int rowCount = list.Count;

                    foreach (AgencyDto item in list)
                    {
                        IService<AgencyDto> agencyService = new AgencyService();
                        ServiceResponse serviceResponse = agencyService.ServiceInsert(item, Token: currentToken);

                        if (!serviceResponse.IsSuccess)
                        {
                            errorList[taskId] += serviceResponse.Message + " : (Satır No: " + rowCnt.ToString() + ") <br>";
                        }
                        double s = Convert.ToDouble(((double.Parse(rowCnt.ToString()) / rowCount) * 100));
                        s = Math.Round(s, 2);
                        tasks[taskId] = s;
                        Thread.Sleep(20);
                        rowCnt++;
                    }
                    tasks.Remove(taskId);
                });
            }
            catch (Exception ex) { response.Message += ex.ToString(); response.IsSuccess = false; }
            finally { }
            return response;
        }
    }
}
