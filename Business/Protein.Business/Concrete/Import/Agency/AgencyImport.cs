﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Protein.Business.Abstract.Import;
using Protein.Business.Abstract.Service;
using Protein.Business.Concrete.Import.Util;
using Protein.Business.Concrete.Responses;
using Protein.Business.Concrete.Services;
using Protein.Business.Concrete.Validator;
using Protein.Business.Enums.Import;
using Protein.Common.Dto.ImportObjects;
using Protein.Data.Repositories;
using Protein.Data.Workers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Business.Concrete.Import.Agency
{
    public class AgencyImport : IImport
    {
        public ImportResponse DoWorkAsync(Stream stream, ImportFileType fileType, ref IDictionary<Guid, double> tasks, Guid taskId, ref IDictionary<Guid, string> errorList, Int64 CompanyId = 0)
        {
            ImportResponse response = new ImportResponse();
            MemoryStream stream2 = new MemoryStream();
            List<AgencyDto> AgencyList = new List<AgencyDto>();
            stream.CopyTo(stream2);
            stream.Position = 0;
            stream2.Position = 0;
            response = new ImportValidator<AgencyDto>().CheckValidFile(stream2, fileType);

            if (response.IsSuccess)
                AgencyList = new StreamToObject<AgencyDto>().ConvertIt(stream, fileType);

            if (response.IsSuccess && AgencyList.Count > 0)
            {
                ImportNow(AgencyList, tasks, taskId, errorList);
            }
            return response;
        }
        private ImportResponse ImportNow(List<AgencyDto> list, IDictionary<Guid, double> tasks, Guid taskId, IDictionary<Guid, string> errorList)
        {
            ImportResponse response = new ImportResponse();
            try
            {

                string currentToken = new TokenWorker().GetValidToken(new UserWorker().GetUserID());
                Task.Factory.StartNew(() =>
                {
                    int rowCnt = 1;
                    List<string> cols = new List<string>();
                    int rowCount = list.Count;

                    foreach (AgencyDto item in list)
                    {
                        //if (rowCnt == 0) { rowCnt++; continue; } //skip header

                        //if (rowCnt == 0) { rowCnt++; continue; }

                        //if (CheckSameDoctor(Id, data.DIPLOMA_NO))
                        //{
                        //    response.IsSuccess = false;
                        //    errorList[taskId] += "Mevcut doktoru tekrar ekleyemezsiniz! <b> " + data.DIPLOMA_NO + " - " + data.FIRST_NAME + " " + data.LAST_NAME + "</b> : (Satır No: " + rowCnt.ToString() + " )<br>";
                        //}
                        //else response.IsSuccess = true;

                        IService<AgencyDto> agencyService = new AgencyService();
                        ServiceResponse serviceResponse = agencyService.ServiceInsert(item, Token: currentToken);

                        if (!serviceResponse.IsSuccess)
                        {
                            errorList[taskId] += serviceResponse.Message + " : (Satır No: " + rowCnt.ToString() + ") <br>";
                        }

                        //if (response.IsSuccess)
                        //{
                        //    var contact = new Contact
                        //    {
                        //        Id = 0,
                        //        Type = "0",
                        //        Title = data.TITLE,
                        //        IdentityNo = data.IDENTITY_NO,
                        //    };
                        //    var spResponseDoctorContact = new ContactRepository().Insert(contact, currentToken);
                        //    if (spResponseDoctorContact.Code == "100")
                        //    {
                        //        var contactId = spResponseDoctorContact.PkId;
                        //        var person = new Person
                        //        {
                        //            Id = 0,
                        //            ContactId = contactId,
                        //            ProfessionType = "4",
                        //            FirstName = data.FIRST_NAME,
                        //            LastName = data.LAST_NAME,
                        //            Status = "0"
                        //        };
                        //        var spResponsePerson = new PersonRepository().Insert(person, currentToken);
                        //        if (spResponsePerson.Code == "100")
                        //        {
                        //            DoctorBranch doctorBranch = new DoctorBranchRepository().FindBy(conditions: $"NAME='{data.NAME.Trim()}'").FirstOrDefault();
                        //            if (doctorBranch == null)
                        //            {
                        //                response.IsSuccess = false;
                        //                errorList[taskId] += "Doktor branşı eşleşemedi: <b> " + data.NAME + " </b> : (Satır No: " + rowCnt.ToString() + ") " + "<br>";

                        //            }
                        //            else response.IsSuccess = true;
                        //            if (response.IsSuccess)
                        //            {
                        //                var staff = new Staff
                        //                {
                        //                    Id = 0,
                        //                    //ProviderId = Id,
                        //                    Type = "0",
                        //                    ContactId = contactId,

                        //                    DoctorBranchId = doctorBranch.Id,
                        //                    DiplomaNo = data.DIPLOMA_NO,
                        //                    RegisterNo = data.REGISTER_NO,
                        //                    Status = "0"
                        //                };
                        //                var spResponseStaff = new StaffRepository().Insert(staff, currentToken);
                        //                if (spResponseStaff.Code == "100")
                        //                {
                        //                    response.Message = "İşlem başarılı";
                        //                    response.IsSuccess = true;
                        //                }
                        //                else
                        //                    errorList[taskId] += spResponseStaff.Message + " : (Satır No: " + rowCnt.ToString() + ") <br>";
                        //            }
                        //            else
                        //            {

                        //            }
                        //        }
                        //        else
                        //            errorList[taskId] += spResponsePerson.Message + " : (Satır No: " + rowCnt.ToString() + ") <br>";
                        //    }
                        //    else
                        //        errorList[taskId] += spResponseDoctorContact.Message + " : (Satır No: " + rowCnt.ToString() + ") <br>";
                        //}
                        double s = Convert.ToDouble(((double.Parse(rowCnt.ToString()) / rowCount) * 100));
                        s = Math.Round(s, 2);
                        tasks[taskId] = s;
                        Thread.Sleep(20);
                        rowCnt++;
                    }
                    tasks.Remove(taskId);
                });
            }
            catch (Exception ex) { response.Message += ex.ToString(); response.IsSuccess = false; }
            finally { }
            return response;
        }
        //private ImportResponse CheckValidFile(Stream stream, ImportFileType fileType)
        //{
        //    ImportResponse response = new ImportResponse();
        //    try
        //    {
        //        stream.Position = 0;
        //        //stream.Seek(0, SeekOrigin.Begin);
        //        if (stream == null) { response.Message = "Dosyayı kontrol edin"; return response; }
        //        ISheet sheet;
        //        if (fileType == ImportFileType.Xlsx)
        //        {
        //            XSSFWorkbook hssfwb = new XSSFWorkbook(stream);
        //            sheet = hssfwb.GetSheetAt(0);
        //        }
        //        else
        //        {
        //            HSSFWorkbook hssfwb = new HSSFWorkbook(stream);
        //            sheet = hssfwb.GetSheetAt(0);
        //        }

        //        IRow headerRow = sheet.GetRow(0);
        //        int cellCount = headerRow.LastCellNum;

        //        for (int j = 0; j < cellCount; j++)
        //        {
        //            ICell cell = headerRow.GetCell(j);
        //            var matchCol = typeof(ImportRules.Doctor.ColumnName).GetProperties().Where(x => x.Name == cell.StringCellValue.Trim()).FirstOrDefault();
        //            if (matchCol == null)
        //            {
        //                response.Message = "Eşleşemeyen kolon: <b>'" + cell.StringCellValue + "'</b> lütfen kontrol ediniz";
        //                response.IsSuccess = false;
        //                break;
        //            }
        //            response.IsSuccess = true;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        response.Message = ex.ToString();
        //    }
        //    finally { stream.Dispose(); }
        //    return response;
        //}
    }
}
