﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Protein.Business.Abstract.Import;
using Protein.Business.Concrete.Responses;
using Protein.Business.Concrete.Validator;
using Protein.Business.Enums.Import;
using Protein.Common.Dto.ImportObjects;
using Protein.Data.Repositories;
using Protein.Data.Workers;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Business.Import.Doctor
{
    public class DoctorImport : IImportForSubContent
    {
        public ImportResponse DoWork(Int64 Id, string formId, Stream stream, ImportFileType fileType, ref IDictionary<Guid, double> tasks, Guid taskId, ref IDictionary<Guid, string> errorList)
        {
            ImportResponse response = new ImportResponse();
            MemoryStream stream2 = new MemoryStream();
            stream.CopyTo(stream2);
            stream.Position = 0;
            stream2.Position = 0;
            response = new ImportValidator<DoctorImportDto>().CheckValidFile(stream2, fileType);

            if (response.IsSuccess)
            {
                ImportNow(Id, formId, stream, fileType, tasks, taskId, errorList);
            }
            return response;
        }
        private ImportResponse ImportNow(Int64 Id, string formId, Stream stream, ImportFileType fileType, IDictionary<Guid, double> tasks, Guid taskId, IDictionary<Guid, string> errorList)
        {
            ImportResponse response = new ImportResponse();
            try
            {
                stream.Position = 0;
                //stream.Seek(0, SeekOrigin.Begin);
                ISheet sheet;
                if (fileType == ImportFileType.Xlsx)
                {
                    XSSFWorkbook hssfwb = new XSSFWorkbook(stream);
                    sheet = hssfwb.GetSheetAt(0);
                }
                else
                {
                    HSSFWorkbook hssfwb = new HSSFWorkbook(stream);
                    sheet = hssfwb.GetSheetAt(0);
                }

                string currentToken = new TokenWorker().GetValidToken(new UserWorker().GetUserID());
                Task.Factory.StartNew(() =>
                {
                    int rowCnt = 0;
                    List<string> cols = new List<string>();
                    int rowCount = sheet.LastRowNum;

                    foreach (IRow row in sheet)
                    {
                        DoctorImportDto data = new DoctorImportDto();
                        //if (rowCnt == 0) { rowCnt++; continue; } //skip header
                        foreach (ICell cell in row)
                        {
                            if (rowCnt == 0)
                            {
                                cols.Add(cell.ToString());
                                continue;
                            }

                            PropertyInfo prop = typeof(DoctorImportDto).GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                    .FirstOrDefault(x => x.GetCustomAttribute<ColumnAttribute>() != null
                                       && x.GetCustomAttribute<ColumnAttribute>().Name.Equals(cols[cell.ColumnIndex].ToString(), StringComparison.OrdinalIgnoreCase));

                            var displayColumn = prop.GetCustomAttribute<ColumnAttribute>(true).Name;
                            string value = cell != null ? cell.ToString() : "";
                            prop.SetValue(data, value, null);
                        }
                        if (rowCnt == 0) { rowCnt++; continue; }

                        string condition = $"PROVIDER_ID =:ProviderId AND REGISTER_NO =:registerNo";
                        var v_staff = new GenericRepository<V_Staff>().FindBy(condition, orderby: "", parameters: new { ProviderId = Id, registerNo = new Dapper.DbString { Value = data.REGISTER_NO, Length = 20 } }).FirstOrDefault();


                        //    if (CheckSameDoctor(Id, data.DIPLOMA_NO))
                        //{
                        //    response.IsSuccess = false;
                        //    errorList[taskId] += "Mevcut doktoru tekrar ekleyemezsiniz! <b> " + data.DIPLOMA_NO + " - " + data.FIRST_NAME + " " + data.LAST_NAME + "</b> : (Satır No: " + rowCnt.ToString() + " )<br>";
                        //}
                        //else response.IsSuccess = true;

                        var contact = new Contact
                        {
                            Id = v_staff!=null ? v_staff.CONTACT_ID : 0,
                            Type = ((int)ContactType.GERCEK).ToString(),
                            Title = data.TITLE,
                            IdentityNo = data.IDENTITY_NO,
                        };
                        var spResponseDoctorContact = new ContactRepository().Insert(contact, currentToken);
                        if (spResponseDoctorContact.Code == "100")
                        {
                            var contactId = spResponseDoctorContact.PkId;
                            var person = new Person
                            {
                                Id = v_staff != null ? v_staff.PERSON_ID : 0,
                                ContactId = contactId,
                                ProfessionType = ((int)ProfessionType.VETERINER_HEKIM_).ToString(),
                                FirstName = data.FIRST_NAME,
                                LastName = data.LAST_NAME,
                                Status = "0"
                            };
                            var spResponsePerson = new PersonRepository().Insert(person, currentToken);
                            if (spResponsePerson.Code == "100")
                            {
                                DoctorBranch doctorBranch = new DoctorBranchRepository().FindBy(conditions: "NAME=:name", parameters: new { name = new Dapper.DbString { Value = data.NAME.Trim(), Length = 100 } }).FirstOrDefault();
                                if (doctorBranch == null)
                                {
                                    response.IsSuccess = false;
                                    errorList[taskId] += "Doktor branşı eşleşemedi: <b> " + data.NAME + " </b> : (Satır No: " + rowCnt.ToString() + ") " + "<br>";

                                }
                                else response.IsSuccess = true;
                                if (response.IsSuccess)
                                {
                                    var staff = new Staff
                                    {
                                        Id = v_staff != null ? v_staff.STAFF_ID : 0,
                                        ProviderId = Id,
                                        Type = ((int)StaffType.DOKTOR).ToString(),
                                        ContactId = contactId,

                                        DoctorBranchId = doctorBranch.Id,
                                        DiplomaNo = data.DIPLOMA_NO,
                                        RegisterNo = data.REGISTER_NO,
                                        Status = ((int)Status.AKTIF).ToString()
                                    };
                                    var spResponseStaff = new StaffRepository().Insert(staff, currentToken);
                                    if (spResponseStaff.Code == "100")
                                    {
                                        response.Message = "İşlem başarılı";
                                        response.IsSuccess = true;
                                    }
                                    else
                                        errorList[taskId] += spResponseStaff.Message + " : (Satır No: " + rowCnt.ToString() + ") <br>";
                                }
                                else
                                {

                                }
                            }
                            else
                                errorList[taskId] += spResponsePerson.Message + " : (Satır No: " + rowCnt.ToString() + ") <br>";
                        }
                        else
                            errorList[taskId] += spResponseDoctorContact.Message + " : (Satır No: " + rowCnt.ToString() + ") <br>";

                        double s = Convert.ToDouble(((double.Parse(rowCnt.ToString()) / rowCount) * 100));
                        s = Math.Round(s, 2);
                        tasks[taskId] = s;
                        Thread.Sleep(100);
                        rowCnt++;
                    }
                    tasks.Remove(taskId);
                });
            }
            catch (Exception ex) { response.Message += ex.ToString(); response.IsSuccess = false; }
            finally { stream.Dispose(); }
            return response;
        }
        //private ImportResponse CheckValidFile(Stream stream, ImportFileType fileType)
        //{
        //    ImportResponse response = new ImportResponse();
        //    try
        //    {
        //        stream.Position = 0;
        //        //stream.Seek(0, SeekOrigin.Begin);
        //        if (stream == null) { response.Message = "Dosyayı kontrol edin"; return response; }
        //        ISheet sheet;
        //        if (fileType == ImportFileType.Xlsx)
        //        {
        //            XSSFWorkbook hssfwb = new XSSFWorkbook(stream);
        //            sheet = hssfwb.GetSheetAt(0);
        //        }
        //        else
        //        {
        //            HSSFWorkbook hssfwb = new HSSFWorkbook(stream);
        //            sheet = hssfwb.GetSheetAt(0);
        //        }

        //        IRow headerRow = sheet.GetRow(0);
        //        int cellCount = headerRow.LastCellNum;

        //        for (int j = 0; j < cellCount; j++)
        //        {
        //            ICell cell = headerRow.GetCell(j);
        //            var matchCol = typeof(ImportRules.Doctor.ColumnName).GetProperties().Where(x => x.Name == cell.StringCellValue.Trim()).FirstOrDefault();
        //            if (matchCol == null)
        //            {
        //                response.Message = "Eşleşemeyen kolon: <b>'" + cell.StringCellValue + "'</b> lütfen kontrol ediniz";
        //                response.IsSuccess = false;
        //                break;
        //            }
        //            response.IsSuccess = true;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        response.Message = ex.ToString();
        //    }
        //    finally { stream.Dispose(); }
        //    return response;
        //}
        private bool CheckSameDoctor(long ProviderId, string DiplomaNo)
        {
            bool sameStaff = false;

            try
            {
                string condition = $"PROVIDER_ID =:ProviderId AND DIPLOMA_NO =:diplomaNo";
                if (new GenericRepository<V_Staff>().FindBy(condition, orderby: "", parameters: new { ProviderId, diplomaNo = new Dapper.DbString { Value = DiplomaNo, Length = 20 } }).Count > 0)
                    sameStaff = true;
            }
            catch (Exception ex) { }
            return sameStaff;
        }
    }
}
