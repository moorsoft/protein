﻿
using Protein.Business.Abstract.Service;
using Protein.Business.Concrete.Responses;
using Protein.Common.Abstract;
using Protein.Common.Dto.ImportObjects;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;
using Protein.Common.Extensions;
using Protein.Common.Enums;
using Protein.Data.Helpers;
using static Protein.Common.Constants.Constants;
using Protein.Business.SagmerIntWs;
using System.Configuration;
using Protein.Business.Abstract.Print;
using Protein.Business.Print.MainPolicy;
using Protein.Business.Print.PolicyProfile;
using System.Web;
using Protein.Common.Dto;
using Protein.Common.Entities;

namespace Protein.Business.Concrete.Services
{
    public class PolicyService : IService<PolicyDto>
    {
        private string Token { get; set; }
        string SagmerApiCode = ConfigurationManager.AppSettings[AppKeys.SagmerApiCode];
        SagmerIntWs.SagmerSrvSoapClient sagmerClient;

        private ServiceResponse PolicyPrint(PolicyDto input)
        {
            ServiceResponse response = new ServiceResponse();

            response.PolicyId = input.PolicyId > 0 ? input.PolicyId : 0;

            if (input.PolicyType == ((int)ProteinEnums.PolicyType.FERDI).ToString())
            {
                IPrint<MainPolicyReq> print = new MainPolicy();
                print.DoWork(new MainPolicyReq
                {
                    IsWebRequest = false,
                    LocalPath = input.LocalSavePath,
                    PolicyId = input.PolicyId
                });
            }
            else if (input.PolicyType == ((int)ProteinEnums.PolicyType.GRUP).ToString())
            {
                IPrint<MainPolicyReq> print = new MainPolicy();
                print.DoWork(new MainPolicyReq
                {
                    IsWebRequest = false,
                    LocalPath = input.LocalSavePath,
                    PolicyId = input.PolicyId
                });
            }

            response.Code = "100";
            response.Message = "SUCCESS";
            response.Id = input.PolicyId;
            return response;
        }
        public ServiceResponse ServiceInsert(PolicyDto input, string Token = "9999999999")
        {
            ServiceResponse response = new ServiceResponse();
            this.Token = Token;
            long returnId = 0;

            //SpecialRule
            if (input.IsSpeacialCondition)
            {
                response = InsertSpecialCondition(input);
                if (!response.IsSuccess)
                {
                    return response;
                }
                returnId = response.Id > 0 ? response.Id : returnId;
            }
            //AdditionalRule
            if (input.IsAdditionalProtocolChange)
            {
                response = InsertAdditionalProtocol(input);
                if (!response.IsSuccess)
                {
                    return response;
                }
                returnId = response.Id > 0 ? response.Id : returnId;
            }
            //Insurer
            if (input.isInsurerChange)
            {
                response = InsertInsurer(input);
                if (!response.IsSuccess)
                {
                    return response;
                }
                returnId = response.Id > 0 ? response.Id : returnId;
            }
            //Policy
            if (input.isPolicyChange)
            {
                response = InsertPolicy(input);
                if (!response.IsSuccess)
                {
                    return response;
                }
                returnId = response.Id > 0 ? response.Id : returnId;
            }
            //Endorsement
            if (input.isEndorsementChange)
            {
                response = InsertEndorsement(input);
                if (!response.IsSuccess)
                {
                    return response;
                }
                returnId = response.Id > 0 ? response.Id : returnId;

            }
            //Insured
            if (input.isInsuredContactChange)
            {
                response = InsertInsuredContact(input);
                if (!response.IsSuccess)
                {
                    return response;
                }
                returnId = response.Id > 0 ? response.Id : returnId;
            }
            //Insured
            if (input.isInsuredChange)
            {
                response = InsertInsured(input);
                if (!response.IsSuccess)
                {
                    return response;
                }
                returnId = response.Id > 0 ? response.Id : returnId;
            }
            //Insured Parameter
            if (input.isInsuredParameterChange)
            {
                response = InsertInsuredParameter(input);
                if (!response.IsSuccess)
                {
                    return response;
                }
                returnId = response.Id > 0 ? response.Id : returnId;
            }
            //Insured Parameter
            if (input.isInsuredExitChange)
            {
                response = InsertInsuredExit(input);
                if (!response.IsSuccess)
                {
                    return response;
                }
                returnId = response.Id > 0 ? response.Id : returnId;
            }
            //Insured Note
            if (input.isInsuredNoteChange)
            {
                response = InsertInsuredNote(input);
                if (!response.IsSuccess)
                {
                    return response;
                }
                returnId = response.Id > 0 ? response.Id : returnId;

            }
            //Insured Transfer
            if (input.isInsuredTransferChange)
            {
                response = InsertInsuredTransfer(input);
                if (!response.IsSuccess)
                {
                    return response;
                }
                returnId = response.Id > 0 ? response.Id : returnId;

            }
            response.Id = returnId;
            if (input.isInsertInstallment)
            {
                response = InsertInstallment(input);
                if (!response.IsSuccess)
                {
                    return response;
                }
                returnId = response.Id > 0 ? response.Id : returnId;
            }

            #region Sagmer Sending...
            //Policy sending
            if (input.IsSagmerPolicySend)
            {
                response = InsertSagmerPolicyControl(input);

                if (!response.IsSuccess)
                    return response;
                response.Code = "100";
                returnId = response.Id > 0 ? response.Id : returnId;
            }

            //Insured sending
            if (input.IsSagmerInsuredEntrySend)
            {
                response = InsertSagmerInsuredEntry(input);

                if (!response.IsSuccess)
                    return response;
                response.Code = "100";
                returnId = response.Id > 0 ? response.Id : returnId;
            }
            if (input.IsSagmerInsuredExitSend)
            {
                response = InsertSagmerInsuredExit(input);
                response.Code = "100";
                if (!response.IsSuccess)
                    return response;
                returnId = response.Id > 0 ? response.Id : returnId;
            }
            //Insurer Change
            if (input.IsSagmerInsurerChangeSend)
            {
                response = InsertSagmerInsurerChange(input);
                response.Code = "100";
                if (!response.IsSuccess)
                    return response;
                returnId = response.Id > 0 ? response.Id : returnId;
            }

            //Policy Sending (PROD)
            if (input.IsSagmerPolicySendForProd)
            {
                response = InsertSagmerPolicyProd(input);
                response.Code = "100";
                if (!response.IsSuccess)
                    return response;
                returnId = response.Id > 0 ? response.Id : returnId;
            }
            #endregion

            if (input.IsPrint)
            {
                response = PolicyPrint(input);
                if (!response.IsSuccess)
                {
                    return response;
                }
                returnId = response.Id > 0 ? response.Id : returnId;
            }

            return response;
        }

        private ServiceResponse InsertInsuredExit(PolicyDto input)
        {
            ServiceResponse response = new ServiceResponse();

            Insured insured = new GenericRepository<Insured>().FindById(input.InsuredId);
            if (insured == null)
            {
                response.Code = "999";
                response.Message = "Sigortalı Bilgisi Bulunamadı!";
                return response;
            }
            if (insured.TotalPremium == null && insured.InitialPremium == null)
            {
                response.Code = "999";
                response.Message = "Sigortalı Prim Bilgisi Bulunamadı!";
                return response;
            }
            if (!input.InsuredExitPremium.IsNumeric())
            {
                response.Code = "999";
                response.Message = "Çıkış Prim Bilgisi Girilmeli!";
                return response;
            }
            insured.TotalPremium = insured.TotalPremium != null ? insured.TotalPremium : insured.InitialPremium;

            decimal exitRate = decimal.Parse(input.InsuredExitPremium.Replace('.', ',')) / (decimal)insured.TotalPremium;
            decimal exitPremium = -decimal.Parse(input.InsuredExitPremium.Replace('.', ','));

            insured.Premium = exitPremium;
            insured.TotalPremium = (decimal)insured.TotalPremium - decimal.Parse(input.InsuredExitPremium.Replace('.', ','));
            insured.Status = ((int)Status.SILINDI).ToString();
            insured.LastEndorsementId = input.EndorsementId;
            var spResponse = new GenericRepository<Insured>().Update(insured);
            if (spResponse.Code != "100")
            {
                response.Code = spResponse.Code;
                response.Message = spResponse.Message;
                return response;
            }

            if (insured.IndividualType == ((int)IndividualType.FERT).ToString())
            {
                List<Insured> insuredList = new GenericRepository<Insured>().FindBy($"PARENT_ID=:parentId", parameters: new { parentId = insured.Id });
                if (insuredList != null)
                {
                    foreach (var item in insuredList)
                    {
                        item.TotalPremium = item.TotalPremium != null ? item.TotalPremium : item.InitialPremium;
                        input.InsuredExitPremium = ((decimal)item.TotalPremium * exitRate).ToString("#0.00");
                        exitPremium += decimal.Parse(input.InsuredExitPremium.Replace('.', ',')) - (decimal)item.TotalPremium;

                        item.Premium = exitPremium;
                        item.TotalPremium = (decimal)item.TotalPremium - decimal.Parse(input.InsuredExitPremium.Replace('.', ','));
                        item.Status = ((int)Status.SILINDI).ToString();
                        item.LastEndorsementId = input.EndorsementId;
                        spResponse = new GenericRepository<Insured>().Update(item);
                        if (spResponse.Code != "100")
                        {
                            response.Code = spResponse.Code;
                            response.Message = spResponse.Message;
                            return response;
                        }
                    }
                }
            }

            Policy policy = new GenericRepository<Policy>().FindById(input.PolicyId);
            policy.Premium += exitPremium;
            var spResponsePolicy = new GenericRepository<Policy>().Update(policy);
            if (spResponsePolicy.Code != "100")
            {
                response.Code = spResponsePolicy.Code;
                response.Message = spResponsePolicy.Message;
                return response;
            }

            //EndorsementInsured endorsementInsured = new EndorsementInsured
            //{
            //    EndorsementId = input.EndorsementId,
            //    InsuredId = insured.Id
            //};
            //SpResponse<EndorsementInsured> spResponseInsured = new GenericRepository<EndorsementInsured>().Insert(endorsementInsured, this.Token);
            //if (spResponseInsured.Code != "100")
            //{
            //    response.Code = spResponseInsured.Code;
            //    response.Message = spResponseInsured.Message;
            //    return response;
            //}

            response.Id = input.InsuredId;
            response.Code = "100";
            response.Message = "SUCCESS";
            return response;
        }
        private ServiceResponse InsertInsuredNote(PolicyDto input)
        {
            ServiceResponse response = new ServiceResponse();
            response.PolicyId = input.PolicyId > 0 ? input.PolicyId : 0;
            if (!input.InsuredNoteDetails.IsNull())
            {
                var note = new Note
                {
                    Id = input.InsuredNoteId,
                    Description = input.InsuredNoteDetails,
                    Type = input.InsuredNoteType.IsNull() ? ((int)InsuredNoteType.OZEL).ToString() : input.InsuredNoteType
                };
                SpResponse spResponseNote = new GenericRepository<Note>().Insert(note, this.Token);
                if (spResponseNote.Code == "100")
                {
                    input.InsuredNoteId = spResponseNote.PkId;
                    var insuredNote = new InsuredNote
                    {
                        NoteId = input.InsuredNoteId,
                        InsuredId = input.InsuredId
                    };
                    SpResponse spResponseInsuredNote = new GenericRepository<InsuredNote>().Insert(insuredNote, this.Token);
                    if (spResponseInsuredNote.Code != "100")
                    {
                        response.Code = spResponseInsuredNote.Code;
                        response.Message = spResponseInsuredNote.Message;
                        return response;
                    }
                }
                else
                {
                    response.Code = spResponseNote.Code;
                    response.Message = spResponseNote.Message;
                    return response;
                }
                response.Id = spResponseNote.PkId;
            }

            response.Code = "100";
            response.Message = "SUCCESS";
            return response;
        }
        private ServiceResponse InsertInsuredTransfer(PolicyDto input)
        {
            ServiceResponse response = new ServiceResponse();
            response.PolicyId = input.PolicyId > 0 ? input.PolicyId : 0;
            if (input.InsuredTransferType.IsInt())
            {
                var transfer = new InsuredTransfer
                {
                    Id = input.InsuredTransferId,
                    InsuredId = input.InsuredId,
                    Type = input.InsuredTransferType,
                    CompanyName = input.InsuredTransferCompanyName,

                };
                SpResponse spResponseTransfer = new GenericRepository<InsuredTransfer>().Insert(transfer, this.Token);
                if (spResponseTransfer.Code != "100")
                {
                    response.Code = spResponseTransfer.Code;
                    response.Message = spResponseTransfer.Message;
                    return response;
                }
                response.Id = spResponseTransfer.PkId;
            }

            response.Code = "100";
            response.Message = "SUCCESS";
            return response;
        }
        private ServiceResponse InsertInsuredContact(PolicyDto input)
        {
            ServiceResponse response = new ServiceResponse();
            response.PolicyId = input.PolicyId > 0 ? input.PolicyId : 0;
            if (!string.IsNullOrEmpty(input.InsuredName))
            {
                if (input.InsuredContactId <= 0)
                {
                    if (input.policyIntegrationType == PolicyIntegrationType.UI)
                    {
                        if (CheckTCKNandPassportNo(input.InsuredTcNo, input.InsuredPassport) > 0)
                        {
                            response.Message = "Bu TC Kimlik/Passport No ya ait kişi bulundu. TC Kimlik/Passport No kontrol ediniz.";
                            response.Code = "999";
                            return response;
                        }
                    }
                    else if (input.policyIntegrationType == PolicyIntegrationType.Excel || input.policyIntegrationType == PolicyIntegrationType.WS)
                    {
                        input.InsuredContactId = GetContactId(input.InsuredTcNo, input.InsuredPassport);
                        //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED_CONTACT Save GetContactId_"+ DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                        input.InsuredPersonId = GetPersonId(input.InsuredContactId);
                        //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED_CONTACT Save GetPersonId_"+ DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                    }
                }

                if (input.InsuredContactId > 0)
                {
                    input.InsuredContactAddressId = GetContactAddressId(input.InsuredContactId);
                    //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED_CONTACT Save GetContactAddressId_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                }

                var insuredContact = new Contact
                {
                    Id = input.InsuredContactId,
                    IdentityNo = input.InsuredTcNo.IsIdentityNoLength(),
                    Title = input.InsuredName + " " + input.InsuredLastName,
                    Type = ((int)ContactType.GERCEK).ToString(),
                    TaxNumber = input.InsuredTaxNumber
                };
                SpResponse spResponseContact = new GenericRepository<Contact>().Insert(insuredContact, this.Token);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED_CONTACT Save Contact Insert_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (spResponseContact.Code != "100")
                {
                    response.Code = spResponseContact.Code;
                    response.Message = spResponseContact.Message;
                    return response;
                }
                input.InsuredContactId = spResponseContact.PkId;

                if (input.InsuredGender != null)
                {
                    if (input.InsuredGender.Trim().ToUpper() == "K" || input.InsuredGender.Trim().ToUpper() == "E")
                        input.InsuredGender = (input.InsuredGender.Trim().ToUpper().IndexOf("K") == 0 ? "0" : "1");
                }
                var insuredPerson = new Person
                {
                    Id = input.InsuredPersonId,
                    ContactId = input.InsuredContactId,
                    FirstName = string.IsNullOrEmpty(input.InsuredName) ? null : input.InsuredName,
                    LastName = string.IsNullOrEmpty(input.InsuredLastName) ? null : input.InsuredLastName,
                    Gender = string.IsNullOrEmpty(input.InsuredGender) ? null : input.InsuredGender,
                    Birthdate = input.InsuredBirthDate,
                    Birthplace = string.IsNullOrEmpty(input.InsuredBirthPlace) ? null : input.InsuredBirthPlace,
                    NameOfFather = string.IsNullOrEmpty(input.InsuredNameOfFather) ? null : input.InsuredNameOfFather,
                    NationalityId = GetNationalityID(input.InsuredNationality),
                    PassportNo = string.IsNullOrEmpty(input.InsuredPassport) ? null : input.InsuredPassport,
                    MaritalStatus = string.IsNullOrEmpty(input.InsuredMaritalStatus) ? null : input.InsuredMaritalStatus,
                    IsVip = string.IsNullOrEmpty(input.InsuredVip) ? "0" : "1",
                    VipType = input.InsuredVipType,
                    VipText = input.InsuredVipText
                };
                SpResponse spResponsePerson = new GenericRepository<Person>().Insert(insuredPerson, this.Token);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED_CONTACT Save Person Insert_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (spResponsePerson.Code != "100")
                {
                    response.Code = spResponsePerson.Code;
                    response.Message = spResponsePerson.Message;
                    return response;
                }
            }
            if (!String.IsNullOrEmpty(input.InsuredAddress) && (input.InsuredCityId.ToString().IsNumeric() || input.InsuredCityCode.ToString().IsNumeric()))
            {
                if (input.policyIntegrationType == PolicyIntegrationType.Excel)
                    input.InsuredCityCode = (int)GetCityId(input.InsuredCityCode.ToString());
                var adress = new Address
                {
                    Id = input.InsuredAddressId,
                    Type = ((int)AddressType.EV).ToString(),
                    CityId = input.InsuredCityId.ToString().IsNumeric() ? input.InsuredCityId : null,
                    CountyId = input.InsuredCountyId.ToString().IsNumeric() ? input.InsuredCountyId : null,
                    Details = string.IsNullOrEmpty(input.InsuredAddress) ? "-" : input.InsuredAddress,
                    ZipCode = string.IsNullOrEmpty(input.InsuredZipCode) ? null : input.InsuredZipCode,
                };

                SpResponse spResponseAdress = new GenericRepository<Address>().Insert(adress, this.Token);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED_CONTACT Save Address Insert_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (spResponseAdress.Code == "100")
                {
                    var addressId = spResponseAdress.PkId;
                    if (input.InsuredAddressId <= 0)
                    {
                        var contactAddress = new ContactAddress
                        {
                            Id = input.InsuredContactAddressId,
                            AddressId = addressId,
                            ContactId = input.InsuredContactId,
                        };
                        SpResponse spResponseContactAddress = new GenericRepository<ContactAddress>().Insert(contactAddress, this.Token);
                        //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED_CONTACT Save ContactAddress Insert_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                        if (spResponseContactAddress.Code != "100")
                        {
                            response.Code = spResponseContactAddress.Code;
                            response.Message = spResponseContactAddress.Message;
                            return response;
                        }
                    }
                }
                else
                {
                    response.Code = spResponseAdress.Code;
                    response.Message = spResponseAdress.Message;
                    return response;
                }
            }

            if (!String.IsNullOrEmpty(input.InsuredTelNo))
            {
                var phone = new Phone
                {
                    Id = input.InsuredPhoneId,
                    Type = ((int)PhoneType.IS).ToString(),
                    CountryId = 222,
                    No = input.InsuredTelNo,
                    IsPrimary = "1"
                };
                SpResponse spResponsePhone = new GenericRepository<Phone>().Insert(phone, this.Token);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED_CONTACT Save Phone Insert_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (spResponsePhone.Code == "100")
                {
                    var phoneId = spResponsePhone.PkId;
                    if (input.InsuredPhoneId <= 0)
                    {
                        var contactPhone = new ContactPhone
                        {
                            PhoneId = phoneId,
                            ContactId = input.InsuredContactId
                        };
                        SpResponse spResponseContactPhone = new GenericRepository<ContactPhone>().Insert(contactPhone, this.Token);
                        //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED_CONTACT Save ContactPhone Insert_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                        if (spResponseContactPhone.Code != "100")
                        {
                            response.Code = spResponseContactPhone.Code;
                            response.Message = spResponseContactPhone.Message;
                            return response;
                        }
                    }
                }
                else
                {
                    response.Code = spResponsePhone.Code;
                    response.Message = spResponsePhone.Message;
                    return response;
                }
            }

            if (!String.IsNullOrEmpty(input.InsuredGsmNo))
            {
                var phone = new Phone
                {
                    Id = input.InsuredMobilePhoneId,
                    Type = ((int)PhoneType.MOBIL).ToString(),
                    CountryId = 222,
                    No = input.InsuredGsmNo,
                    IsPrimary = "1"
                };
                SpResponse spResponsePhone = new GenericRepository<Phone>().Insert(phone, this.Token);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED_CONTACT Save Phone InsertGSM_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (spResponsePhone.Code == "100")
                {
                    var phoneId = spResponsePhone.PkId;
                    if (input.InsuredMobilePhoneId <= 0)
                    {
                        var contactPhone = new ContactPhone
                        {
                            PhoneId = phoneId,
                            ContactId = input.InsuredContactId
                        };
                        SpResponse spResponseContactPhone = new GenericRepository<ContactPhone>().Insert(contactPhone, this.Token);
                        //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED_CONTACT Save ContactPhone InsertGSM_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                        if (spResponseContactPhone.Code != "100")
                        {
                            response.Code = spResponseContactPhone.Code;
                            response.Message = spResponseContactPhone.Message;
                            return response;
                        }
                    }
                }
                else
                {
                    response.Code = spResponsePhone.Code;
                    response.Message = spResponsePhone.Message;
                    return response;
                }
            }

            if (!String.IsNullOrEmpty(input.InsuredEmail))
            {
                var email = new Email
                {
                    Id = input.InsuredEmailId,
                    Type = ((int)EmailType.IS).ToString(),
                    Details = input.InsuredEmail,
                    IsPrimary = "1"
                };
                SpResponse spResponseEmail = new GenericRepository<Email>().Insert(email, this.Token);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED_CONTACT Save Email Insert_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (spResponseEmail.Code == "100")
                {
                    var emailId = spResponseEmail.PkId;
                    if (input.InsuredEmailId <= 0)
                    {
                        var contactEmail = new ContactEmail
                        {
                            EmailId = emailId,
                            ContactId = input.InsuredContactId
                        };
                        SpResponse spResponseContactEmail = new GenericRepository<ContactEmail>().Insert(contactEmail, this.Token);
                        //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED_CONTACT Save ContactEmail Insert_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                        if (spResponseContactEmail.Code != "100")
                        {
                            response.Code = spResponseContactEmail.Code;
                            response.Message = spResponseContactEmail.Message;
                            return response;
                        }
                    }
                }
                else
                {
                    response.Code = spResponseEmail.Code;
                    response.Message = spResponseEmail.Message;
                    return response;
                }
            }

            if (input.policyIntegrationType == PolicyIntegrationType.Excel)
            {
                if (!string.IsNullOrEmpty(input.InsuredBankCode) && !string.IsNullOrEmpty(input.InsuredBankBranchCode) && !string.IsNullOrEmpty(input.InsuredBankAccountNo) && !string.IsNullOrEmpty(input.InsuredBankIbanNo))
                {
                    input.InsuredBankCode = GetBankIdByCode(input.InsuredBankCode).ToString();
                    input.InsuredBankBranchCode = GetBankBranchIdByCode(input.InsuredBankBranchCode).ToString();
                    if (!String.IsNullOrEmpty(input.InsuredBankCode.ToString()) && input.InsuredBankCode != "0" && !String.IsNullOrEmpty(input.InsuredBankBranchCode.ToString()) && input.InsuredBankBranchCode != "0")
                    {
                        var bankAccount = new BankAccount
                        {
                            Name = input.InsuredBankAccountOwnerName,
                            CurrencyType = "1",
                            No = input.InsuredBankAccountNo,
                            Iban = input.InsuredBankIbanNo,
                            BankId = long.Parse(input.InsuredBankCode),
                            BankBranchId = long.Parse(input.InsuredBankBranchCode)
                        };
                        SpResponse spResponseBankAccount = new GenericRepository<BankAccount>().Insert(bankAccount, this.Token);

                        if (spResponseBankAccount.Code == "100")
                        {
                            var bankAccountId = spResponseBankAccount.PkId;
                            var providerBankAccount = new ContactBankAccount
                            {
                                ContactId = input.InsuredContactId,
                                BankAccountId = bankAccountId,
                            };
                            SpResponse spResponseContactBankAccount = new GenericRepository<ContactBankAccount>().Insert(providerBankAccount, this.Token);
                            if (spResponseContactBankAccount.Code != "100")
                            {
                                response.Code = spResponseContactBankAccount.Code;
                                response.Message = spResponseContactBankAccount.Message;
                                return response;
                            }
                        }
                        else
                        {
                            response.Code = spResponseBankAccount.Code;
                            response.Message = spResponseBankAccount.Message;
                            return response;
                        }
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(input.InsuredBankId.ToString()) && input.InsuredBankId > 0)
                    {
                        var bankAccount = new BankAccount
                        {
                            Name = input.InsuredBankAccountOwnerName,
                            CurrencyType = string.IsNullOrEmpty(input.InsuredBankCurrencyType) ? null : input.InsuredBankCurrencyType,
                            No = input.InsuredBankAccountNo,
                            Iban = input.InsuredBankIbanNo,
                            BankId = input.InsuredBankId,
                            BankBranchId = input.InsuredBankBranchId
                        };
                        SpResponse spResponseBankAccount = new GenericRepository<BankAccount>().Insert(bankAccount, this.Token);

                        if (spResponseBankAccount.Code == "100")
                        {
                            var bankAccountId = spResponseBankAccount.PkId;
                            var providerBankAccount = new ContactBankAccount
                            {
                                ContactId = input.InsuredContactId,
                                BankAccountId = bankAccountId,
                            };
                            SpResponse spResponseContactBankAccount = new GenericRepository<ContactBankAccount>().Insert(providerBankAccount, this.Token);
                            if (spResponseContactBankAccount.Code != "100")
                            {
                                response.Code = spResponseContactBankAccount.Code;
                                response.Message = spResponseContactBankAccount.Message;
                                return response;
                            }
                        }
                        else
                        {
                            response.Code = spResponseBankAccount.Code;
                            response.Message = spResponseBankAccount.Message;
                            return response;
                        }
                    }
                }
            }

            if (!String.IsNullOrEmpty(input.InsuredKazanilmisHak))
            {
                var note = new Note
                {
                    Type = string.IsNullOrEmpty(input.InsuredNoteType) ? ((int)InsuredNoteType.OZEL).ToString() : input.InsuredNoteType,
                    Description = input.InsuredKazanilmisHak,
                };
                SpResponse spResponseNote = new GenericRepository<Note>().Insert(note, this.Token);
                if (spResponseNote.Code == "100")
                {
                    var noteId = spResponseNote.PkId;
                    var contactNote = new ContactNote
                    {
                        ContactId = input.InsuredContactId,
                        NoteId = noteId
                    };
                    SpResponse spResponseContactNote = new GenericRepository<ContactNote>().Insert(contactNote, this.Token);
                    if (spResponseContactNote.Code != "100")
                    {
                        response.Code = spResponseContactNote.Code;
                        response.Message = spResponseContactNote.Message;
                        return response;
                    }
                }
                else
                {
                    response.Code = spResponseNote.Code;
                    response.Message = spResponseNote.Message;
                    return response;
                }
            }

            response.Code = "100";
            response.Message = "SUCCESS";
            response.Id = input.InsuredContactId;
            return response;
        }
        private ServiceResponse InsertInsured(PolicyDto input)
        {
            ServiceResponse response = new ServiceResponse();
            response.PolicyId = input.PolicyId > 0 ? input.PolicyId : 0;
            int tmp;
            if (!int.TryParse(input.InsuredIndividualType, out tmp))
                input.InsuredIndividualType = ((int)(EnumHelper.GetValueFromText<ProteinEnums.IndividualType>(input.InsuredIndividualType))).ToString();

            if (input.policyIntegrationType == PolicyIntegrationType.Excel)
                input.insuredParentId = GetInsuredParentId(input.EndorsementId, input.InsuredFamilyNo.ToString());

            if (input.InsuredIndividualType == ((int)IndividualType.ES).ToString())
            {
                if (input.InsuredId <= 0)
                {
                    V_InsuredClaim vInsured = new GenericRepository<V_InsuredClaim>().FindBy("POLICY_ID=:policyId AND FAMILY_NO=:familyNo AND INDIVIDUAL_TYPE=:INDIVIDUAL_TYPE", orderby: "POLICY_ID", parameters: new { policyId = input.PolicyId, familyNo = input.InsuredFamilyNo, INDIVIDUAL_TYPE = new Dapper.DbString { Value = ((int)IndividualType.ES).ToString(), Length = 3 } }).FirstOrDefault();
                    //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED Save V_InsuredClaim SelectES_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                    if (vInsured != null)
                    {
                        response.Code = "999";
                        response.Message = "Aynı Aile içinde Birden Çok Eş Olamaz!!!";
                        return response;
                    }
                }
                if (input.policyIntegrationType != PolicyIntegrationType.WS)
                {
                    V_Portfoy portfoy = new GenericRepository<V_Portfoy>().FindBy($"CONTACT_ID=:contactId", parameters: new { contactId = input.InsuredContactId }, orderby: "CONTACT_ID").FirstOrDefault();
                    if (portfoy.MARITAL_STATUS == null || portfoy.MARITAL_STATUS == ((int)MaritalStatus.BEKAR).ToString())
                    {
                        response.Code = "999";
                        response.Message = "Birey Tipi 'Eş' seçilen Kişilerin Medeni Durumu boş ya da bekar olamaz!!!";
                        return response;
                    }
                }
            }
            else if (input.InsuredIndividualType == ((int)IndividualType.BABASI).ToString() || input.InsuredIndividualType == ((int)IndividualType.ANNESI).ToString())
            {
                if (input.InsuredId <= 0)
                {
                    V_InsuredClaim vInsured = new GenericRepository<V_InsuredClaim>().FindBy("POLICY_ID=:policyId AND FAMILY_NO=:familyNo AND INDIVIDUAL_TYPE=:INDIVIDUAL_TYPE", orderby: "POLICY_ID", parameters: new { policyId = input.PolicyId, familyNo = input.InsuredFamilyNo, INDIVIDUAL_TYPE = new Dapper.DbString { Value = input.InsuredIndividualType, Length = 3 } }).FirstOrDefault();
                    //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED Save V_InsuredClaim SelectANNEBABA_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                    if (vInsured != null)
                    {
                        response.Code = "999";
                        response.Message = "Aynı Aile içinde Birden Çok Anne veya Baba Olamaz!!!";
                        return response;
                    }
                }
            }
            else if (input.InsuredId <= 0 && input.InsuredIndividualType == ((int)IndividualType.FERT).ToString())
            {
                Policy policy = new GenericRepository<Policy>().FindById(input.PolicyId);
                if (policy.Type == ((int)PolicyType.FERDI).ToString())
                {
                    V_InsuredClaim vInsured = new GenericRepository<V_InsuredClaim>().FindBy("POLICY_ID=:policyId AND  INDIVIDUAL_TYPE=:INDIVIDUAL_TYPE", orderby: "POLICY_ID", parameters: new { policyId = input.PolicyId, INDIVIDUAL_TYPE = new Dapper.DbString { Value = ((int)IndividualType.FERT).ToString(), Length = 3 } }).FirstOrDefault();
                    //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED Save V_InsuredClaim SelectFERT_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                    if (vInsured != null)
                    {
                        response.Code = "999";
                        response.Message = "Ferdi Poliçeye Birden Çok Fert Eklenemez!!!";
                        return response;
                    }
                }
            }

            if (input.InsuredGender == ((int)Gender.ERKEK).ToString())
            {
                var package = new GenericRepository<V_Package>().FindBy($"PACKAGE_ID=:packageId", parameters: new { packageId = input.InsuredPackageId }, orderby: "").FirstOrDefault();
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED Save V_Package SelectERKEK_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (package != null && package.PRODUCT_TYPE == ((int)ProductType.AVS).ToString())
                {
                    response.Code = "999";
                    response.Message = "Doğum Ürününe Sahip Poliçeye Erkek Eklenemez!";
                    return response;
                }
            }

            if (input.InsuredId <= 0)
            {
                List<V_InsuredClaim> vInsuredList = new GenericRepository<V_InsuredClaim>().FindBy($"POLICY_ID=:policyId AND CONTACT_ID=:contactId", parameters: new { policyId = input.PolicyId, contactId = input.InsuredContactId }, orderby: "INSURED_ID DESC", fetchDeletedRows: true);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED Save V_InsuredClaim SelectCONTACT_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (vInsuredList != null && vInsuredList.Count > 0)
                {
                    if (vInsuredList.Where(i => i.STATUS == ((int)Status.AKTIF).ToString()).FirstOrDefault() != null)
                    {
                        response.Code = "999";
                        response.Message = "Poliçeye Aynı Kişi Birden Fazla Kez Eklenemez!!!";
                        return response;
                    }
                    else if (vInsuredList.Where(i => i.STATUS == ((int)Status.SILINDI).ToString()).FirstOrDefault() != null)
                    {
                        input.InsuredId = vInsuredList.Where(i => i.STATUS == ((int)Status.SILINDI).ToString()).FirstOrDefault().INSURED_ID;
                    }
                }
            }

            var insured = new Insured
            {
                Id = input.InsuredId,
                ContactId = input.InsuredContactId,
                IndividualType = input.InsuredIndividualType,
                FamilyNo = !Extensions.IsInt64(input.InsuredFamilyNo.ToString()) ? 1 : input.InsuredFamilyNo,
                PackageId = input.InsuredPackageId > 0 ? input.InsuredPackageId : null,
                RegistrationNo = string.IsNullOrEmpty(input.InsuredRegistorNo) ? null : input.InsuredRegistorNo,
                InitialPremium = input.InsuredInitialPremium,
                Premium = input.InsuredEndorsementPremium,
                TotalPremium = input.InsuredTotalPremium == null ? input.InsuredInitialPremium : input.InsuredTotalPremium,
                Tax = input.InsuredTax,
                ParentId = !Extensions.IsInt64(input.insuredParentId.ToString()) ? null : input.insuredParentId,
                RenewalGuaranteeType = string.IsNullOrEmpty(input.InsuredRenewalGuaranteeType) ? null : input.InsuredRenewalGuaranteeType,
                RenewalGuaranteeText = input.InsuredRenewalGuaranteeText.IsNull() ? null : input.InsuredRenewalGuaranteeText,
                RenewalDate = input.RenewalDate,
                FirstAmbulantCoverageDate = input.InsuredFirstAmbulantCoverageDate,
                FirstInsuredDate = input.InsuredFirstInsuredDate,
                PriceId = !Extensions.IsInt64(input.insuredPackagePriceId.ToString()) ? null : input.insuredPackagePriceId,
                BirthCoverageDate = input.InsuredBirthCoverageDate.ToString().IsDateTime() ? input.InsuredBirthCoverageDate : null,
                CompanyEntranceDate = input.InsuredCompanyEntranceDate.ToString().IsDateTime() ? input.InsuredCompanyEntranceDate : null,
                ExchangeRateId = input.ExchangeRateId,
                Status = input.InsuredStatus,
                LastEndorsementId = input.EndorsementId,
                CompanyInsuredNo = input.InsuredNo,
                CompanyId = input.CompanyId > 0 ? (long?)input.CompanyId : GetCompanyIdbyPolicy(input.PolicyId),
                PolicyId = input.PolicyId > 0 ? (long?)input.PolicyId : null,
            };

            SpResponse spResponseInsured = new GenericRepository<Insured>().Insert(insured, this.Token);

            if (spResponseInsured.Code != "100")
            {
                response.Code = spResponseInsured.Code;
                response.Message = spResponseInsured.Message;
                return response;
            }

            if (input.EndorsementId > 0)
            {
                EndorsementInsured endorsementInsured = new GenericRepository<EndorsementInsured>().FindBy("ENDORSEMENT_ID=:endoId AND INSURED_ID=:insId", orderby: "", parameters: new { endoId = input.EndorsementId, insId = spResponseInsured.PkId }).FirstOrDefault();

                if (endorsementInsured == null)
                {
                    endorsementInsured = new EndorsementInsured
                    {
                        Id = 0,
                        EndorsementId = input.EndorsementId,
                        InsuredId = spResponseInsured.PkId,
                        Status = ((int)Status.AKTIF).ToString(),
                        InsuredStatus = insured.Status,
                        PackageId = input.InsuredPackageId > 0 ? input.InsuredPackageId : null,
                        Premium = insured.Premium,
                        TotalPremium = insured.TotalPremium
                    };
                    SpResponse spResponse = new GenericRepository<EndorsementInsured>().Insert(endorsementInsured, this.Token);

                    if (spResponse.Code != "100")
                    {
                        response.Code = spResponse.Code;
                        response.Message = spResponse.Message;
                        return response;
                    }
                }
            }

            if (input.PolicyId > 0 && !input.isPolicyTransfer)
            {
                var insuredList = new GenericRepository<Insured>().FindBy("POLICY_ID=:policy_id", orderby: "ID", parameters: new { policy_id = input.PolicyId }, fetchDeletedRows: true);

                if (insuredList != null && insuredList.Count > 0)
                {
                    decimal totalPremium = 0;
                    decimal totalTax = 0;

                    foreach (var item in insuredList)
                    {
                        if (item.TotalPremium != null)
                        {
                            totalPremium += (decimal)item.TotalPremium;
                        }
                        if (item.Tax != null)
                        {
                            totalTax += (decimal)item.Tax;
                        }
                    }

                    var policy = new GenericRepository<Policy>().FindById(input.PolicyId);
                    //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED Save pOLİCY SELECT_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                    if (policy != null)
                    {
                        policy.Premium = totalPremium;
                        if (input.policyIntegrationType == PolicyIntegrationType.Excel)
                        {
                            policy.CashPaymentType = input.CashPaymentType;
                            if (!string.IsNullOrEmpty(input.CashPaymentValue) && input.EndorsementType == ((int)EndorsementType.BASLANGIC).ToString())
                            {
                                if (((int)CashPaymentType.TUTAR).ToString() == policy.CashPaymentType)
                                {
                                    policy.CashAmount = input.CashAmount = (totalPremium - decimal.Parse(input.CashPaymentValue));
                                }
                                else if (((int)CashPaymentType.YUZDE).ToString() == policy.CashPaymentType)
                                {
                                    policy.CashAmount = input.CashAmount = totalPremium - ((totalPremium * decimal.Parse(input.CashPaymentValue)) / 100);
                                }

                            }
                        }
                        policy.Tax = totalTax;
                        var result = new GenericRepository<Policy>().Insert(policy, this.Token);
                        //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURED Save pOLİCY UPDATE_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                    }
                }
            }

            response.Code = "100";
            response.Message = "SUCCESS";
            response.Id = spResponseInsured.PkId;
            input.InsuredId = spResponseInsured.PkId;
            return response;
        }
        private ServiceResponse InsertAdditionalProtocol(PolicyDto input)
        {
            ServiceResponse response = new ServiceResponse();

            if (!string.IsNullOrEmpty(input.AdditionalProtocol) && input.PolicyId < 1)
            {
                response.PolicyId = input.PolicyId;
                RulePolicy rulePol = new RulePolicy
                {
                    PolicyId = input.PolicyId,
                    RuleId = long.Parse(input.AdditionalProtocol),
                    Status = ((int)ProteinEnums.Status.AKTIF).ToString(),
                    Type = ((int)ProteinEnums.RuleType.EK_PROTOKOL).ToString()
                };

                SpResponse spResponse = new GenericRepository<RulePolicy>().Insert(rulePol, this.Token);
                if (spResponse.Code != "100")
                {
                    response.Code = spResponse.Code;
                    response.Message = spResponse.Message;
                    return response;
                }
            }

            response.Code = "100";
            response.Message = "SUCCESS";
            return response;
        }
        private ServiceResponse InsertSpecialCondition(PolicyDto input)
        {
            ServiceResponse response = new ServiceResponse();

            if (!string.IsNullOrEmpty(input.AdditionalProtocol) && input.PolicyId < 1)
            {
                response.PolicyId = input.PolicyId > 0 ? input.PolicyId : 0;
                RulePolicy rulePol = new RulePolicy
                {
                    PolicyId = input.PolicyId,
                    RuleId = long.Parse(input.AdditionalProtocol),
                    Status = ((int)ProteinEnums.Status.AKTIF).ToString(),
                    Type = ((int)ProteinEnums.RuleType.OZEL_SART).ToString()
                };

                SpResponse spResponse = new GenericRepository<RulePolicy>().Insert(rulePol, this.Token);
                if (spResponse.Code != "100")
                {
                    response.Code = spResponse.Code;
                    response.Message = spResponse.Message;
                    return response;
                }
            }

            response.Code = "100";
            response.Message = "SUCCESS";
            return response;
        }
        private ServiceResponse InsertInsuredParameter(PolicyDto input)
        {
            ServiceResponse response = new ServiceResponse();

            if (!string.IsNullOrEmpty(input.InsuredNo))
            {
                var parameter = new Parameter
                {
                    Id = input.InsuredNoParameterId,
                    Key = "INSURED_NO",
                    Value = input.InsuredNo
                };
                SpResponse spResponseParameter = new GenericRepository<Parameter>().Insert(parameter, this.Token);
                if (spResponseParameter.Code == "100")
                {
                    var insuredParameter = new insuredParameter
                    {
                        InsuredId = input.InsuredId,
                        ParameterId = spResponseParameter.PkId
                    };
                    SpResponse spResponse = new GenericRepository<insuredParameter>().Insert(insuredParameter, this.Token);
                    if (spResponse.Code != "100")
                    {
                        response.Code = spResponseParameter.Code;
                        response.Message = spResponseParameter.Message;
                        return response;
                    }
                    response.Id = spResponseParameter.PkId;
                    input.InsuredNoParameterId = spResponseParameter.PkId;
                }
                else
                {
                    response.Code = spResponseParameter.Code;
                    response.Message = spResponseParameter.Message;
                    return response;
                }
            }

            response.Code = "100";
            response.Message = "SUCCESS";
            return response;
        }
        private ServiceResponse InsertEndorsement(PolicyDto input)
        {
            ServiceResponse response = new ServiceResponse();
            if (!input.EndorsementNo.ToString().IsInt() || string.IsNullOrEmpty(input.EndorsementType))
            {
                response.Code = "999";
                response.Message = "Zeyl No ve Zeyl Tipi Girilmesi Zorunludur.";
                return response;
            }
            if (input.EndorsementId <= 0)
            {
                var CheckEndorsementNo = new GenericRepository<Endorsement>().FindBy("POLICY_ID=:PolicyId AND NO=:EndorsementNo", orderby: "", parameters: new { input.PolicyId, input.EndorsementNo });
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation ENDORSEMENT Save CheckEndorsementNo_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (CheckEndorsementNo.Count > 0)
                {
                    response.Code = "999";
                    response.Message = "Bu Şirket Zeyl No Daha Önce Kullanılmış!!! Lütfen Farklı Bir Şirket Zeyl No Girerek Tekrar Deneyiniz...";
                    return response;
                }
            }

            if (input.EndorsementStartDate != null)
            {
                DateTime startDate = (DateTime)input.EndorsementStartDate;
                input.EndorsementStartDate = DateTime.Parse(startDate.ToShortDateString()).AddHours(12);
            }
            response.PolicyId = input.PolicyId;
            var endorsement = new Endorsement
            {
                Id = input.EndorsementId,
                PolicyId = input.PolicyId,
                Type = input.EndorsementType,
                No = input.EndorsementNo,
                StartDate = input.EndorsementStartDate,
                DateOfIssue = input.DateOfIssue,
                RegistrationDate = input.EndorsementRegistrationDate,
                TypeDescriptionType = input.EndorsementTypeDescriptionType,
                TypeDescription = input.EndorsementTypeDescription,
                Category = input.EndorsementCategory,
                ReasonId = input.EndorsementReasonId,
            };
            if (string.IsNullOrEmpty(input.EndorsementStatus))
            {
                input.EndorsementStatus = input.DateOfIssue != null ? ((int)Status.AKTIF).ToString() : ((int)Status.PASIF).ToString();
            }
            else
            {
                input.EndorsementStatus = input.EndorsementStatus;
            }

            if (input.EndorsementPremium != null && input.PolicyStatus.IsInt() && input.PolicyStatus == ((int)PolicyStatus.IPTAL).ToString())
            {
                endorsement.Premium = input.EndorsementPremium;
            }
            if (endorsement.Type == ((int)EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI).ToString())
            { endorsement.Premium = 0; }
            endorsement.Status = input.EndorsementStatus;
            SpResponse spResponseEndorsement = new GenericRepository<Endorsement>().Insert(endorsement, this.Token);
            //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation ENDORSEMENT Save INSERT_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

            if (spResponseEndorsement.Code != "100")
            {
                response.Code = spResponseEndorsement.Code;
                response.Message = spResponseEndorsement.Message;
                return response;
            }
            response.EndorsementId = spResponseEndorsement.PkId;
            endorsement.Id = spResponseEndorsement.PkId;
            if (input.EndorsementId <= 0)
            {
                EndorsementPolicy endorsementPolicy = new EndorsementPolicy
                {
                    EndorsementId = spResponseEndorsement.PkId,
                    PolicyId = input.PolicyId
                };
                SpResponse spResponse = new GenericRepository<EndorsementPolicy>().Insert(endorsementPolicy, this.Token);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation ENDORSEMENT Save EndorsementPolicy_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (spResponse.Code != "100")
                {
                    response.Code = spResponse.Code;
                    response.Message = spResponse.Message;
                    return response;
                }


                Policy policy = new GenericRepository<Policy>().FindById(input.PolicyId);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation ENDORSEMENT Save Policy-FindById_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                var lastEndorsementId = policy.LastEndorsementId;

                policy.LastEndorsementId = spResponseEndorsement.PkId;
                if (input.PolicyStatus.IsInt())
                {
                    policy.Status = input.PolicyStatus;
                    policy.Premium = input.PolicyPremium;
                }
                SpResponse spResponsePolicy = new GenericRepository<Policy>().Insert(policy, this.Token);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation ENDORSEMENT Save Policy-UPDATE_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (spResponsePolicy.Code != "100")
                {
                    response.Code = spResponsePolicy.Code;
                    response.Message = spResponsePolicy.Message;
                    return response;
                }
            }

            if (endorsement.Status == ((int)Status.AKTIF).ToString() && endorsement.Type != ((int)EndorsementType.DONDURMA_SONUCU_IPTAL).ToString() && endorsement.Type != ((int)EndorsementType.GUN_ESASLI_IPTAL).ToString() && endorsement.Type != ((int)EndorsementType.IPTAL).ToString() && endorsement.Type != ((int)EndorsementType.KISA_DONEM_ESASLI_IPTAL).ToString() && endorsement.Type != ((int)EndorsementType.MEBDEINDEN_BASLANGICTAN_IPTAL).ToString() && endorsement.Type != ((int)EndorsementType.PRIM_IADESIZ_IPTAL).ToString() && input.policyIntegrationType != PolicyIntegrationType.WS)
            {
                endorsement.Premium = 0;

                var insuredList = new GenericRepository<Insured>().FindBy($"LAST_ENDORSEMENT_ID=:lastEndorsementId", parameters: new { lastEndorsementId = endorsement.Id }, fetchDeletedRows: (endorsement.Type == ((int)EndorsementType.SIGORTALI_CIKISI).ToString() ? true : false));
                if (insuredList != null && insuredList.Count > 0)
                {
                    foreach (var insured in insuredList)
                    {
                        endorsement.Premium += insured.Premium;
                    }
                }
                new GenericRepository<Endorsement>().Update(endorsement);

            }

            input.EndorsementId = spResponseEndorsement.PkId;
            response.Code = "100";
            response.Message = "SUCCESS";
            if (!input.isPolicyChange)
                response.Id = spResponseEndorsement.PkId;
            return response;
        }
        private ServiceResponse InsertPolicy(PolicyDto input)
        {
            ServiceResponse response = new ServiceResponse();

            if (input.policyIntegrationType != PolicyIntegrationType.UI)
            {

                input.AgencyId = GetAgencyID(input.AgencyId, input.CompanyId);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation POLICY Save GetAgencyID_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (input.AgencyId.IsNull())
                {
                    response.Code = "999";
                    response.Message = "Acente bulunamadı"; return response;
                }
            }
            if (input.EndDate != null)
            {
                DateTime endDate = (DateTime)input.EndDate;
                input.EndDate = DateTime.Parse(endDate.ToShortDateString()).AddHours(12);
            }
            var policy = new Policy
            {
                Id = input.PolicyId,
                AgencyId = long.Parse(input.AgencyId),
                EndDate = input.EndDate,
                Type = input.PolicyType.IsInt() ? input.PolicyType : (input.PolicyType == "G" ? ((int)PolicyType.GRUP).ToString() : ((int)PolicyType.FERDI).ToString()),
                RenewalNo = input.RenewalNo,
                PreviousId = input.PolicyPreviousId,
                SbmNo = input.SbmPoliceNo,
                LocationType = input.LocationType,
                ParentId = input.PolicyParentId,
                PolicyGroupId = input.PolicyGroupId,
                No = input.PolicyNo,
                Insurer = input.InsurerId,
                Premium = input.PolicyPremium,
                Tax = input.PolicyTax,
                InstallmentCount = input.PolicyInstallmentCount == null || input.PolicyInstallmentCount < 1 ? null : input.PolicyInstallmentCount,
                CashPaymentType = input.CashPaymentType,
                CashAmount = input.CashAmount,
                SubproductId = input.SubProductId,
                PaymentType = string.IsNullOrEmpty(input.PolicyPaymentType) ? null : input.PolicyPaymentType,
                ExchangeRateId = input.ExchangeRateId <= 0 ? null : input.ExchangeRateId,
                Status = input.PolicyStatus,
                SbmAuthorizationCode = input.SbmAuthorizationCode,
                CompanyId = input.CompanyId <= 0 ? null : (long?)input.CompanyId
            };
            if (input.PreviousPolicyNo.IsInt64() && input.PreviousRenewalNo.ToString().IsInt())
            {
                Policy previousPolicy = new GenericRepository<Policy>().FindBy("NO=:no AND RENEWAL_NO=:renewalNo", parameters: new { no = new Dapper.DbString { Value = input.PreviousPolicyNo, Length = 20 }, renewalNo = input.PreviousRenewalNo }).FirstOrDefault();
                policy.PreviousId = previousPolicy != null ? (long?)previousPolicy.Id : null;
            }
            SpResponse spResponsePolicy = new GenericRepository<Policy>().Insert(policy, this.Token);
            //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation POLICY Save INSERT_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

            if (spResponsePolicy.Code != "100")
            {
                response.Code = spResponsePolicy.Code;
                response.Message = spResponsePolicy.Message;
                return response;
            }
            input.PolicyId = spResponsePolicy.PkId;
            response.PolicyId = spResponsePolicy.PkId;
            response.Code = "100";
            response.Message = "SUCCESS";
            response.Id = spResponsePolicy.PkId;
            return response;
        }
        private ServiceResponse InsertInstallment(PolicyDto input)
        {
            ServiceResponse response = new ServiceResponse();
            response.PolicyId = input.PolicyId > 0 ? input.PolicyId : 0;
            if (input.policyIntegrationType == PolicyIntegrationType.Excel || input.policyIntegrationType == PolicyIntegrationType.WS)
            {
                if (input.PolicyInstallmentCount < 1)
                {
                    response.Code = "-1";
                    response.Message = "Taksit sayısı 1'den kucuk olamaz";
                }
                var existPolicy = new GenericRepository<Policy>().FindById(input.PolicyId);

                var existInstallment = new GenericRepository<Installment>().FindBy($"POLICY_ID =:policyId", parameters: new { policyId = input.PolicyId }, fetchDeletedRows: true);
                List<long> oldInstallmentIds = new List<long>();
                if (existInstallment.Count > 0)
                {
                    foreach (Installment intlmnt in existInstallment)
                        oldInstallmentIds.Add(intlmnt.Id);
                }
                if (input.PolicyInstallmentCount > 0)
                {
                    for (int i = 0; i < input.PolicyInstallmentCount; i++)
                    {
                        var installment = new Installment
                        {
                            PolicyId = input.PolicyId,
                            Amount = (decimal.Parse(existPolicy.Premium.ToString().IsNumeric() ? existPolicy.Premium.ToString() : "0") - decimal.Parse(existPolicy.CashAmount.ToString().IsNumeric() ? existPolicy.CashAmount.ToString() : "0")) / int.Parse(input.PolicyInstallmentCount.ToString()),
                            DueDate = DateTime.Now.AddMonths(i),
                            Status = ((int)Status.AKTIF).ToString(),
                            EndorsementId = input.EndorsementId == 0 ? null : (long?)input.EndorsementId,
                        };
                        var spResponse = new GenericRepository<Installment>().Insert(installment, this.Token);
                        if (spResponse.Code != "100")
                        {
                            response.Code = spResponse.Code;
                            response.Message = spResponse.Message;
                            return response;
                        }
                    }
                }
                else
                {
                    var installment = new Installment
                    {
                        PolicyId = input.PolicyId,
                        Amount = input.InstallmentAmount,
                        DueDate = input.InstallmentDueDate,
                        Status = ((int)ProteinEnums.Status.AKTIF).ToString(),
                        EndorsementId = input.EndorsementId == 0 ? null : (long?)input.EndorsementId,
                    };
                    var spResponse = new GenericRepository<Installment>().Insert(installment, this.Token);
                    if (spResponse.Code != "100")
                    {
                        response.Code = spResponse.Code;
                        response.Message = spResponse.Message;
                        return response;
                    }
                    else
                    {
                        response.Id = spResponse.PkId;
                        if (oldInstallmentIds.Count > 0)
                        {
                            foreach (long oldInsId in oldInstallmentIds)
                            {
                                if (input.InstertedInstalmentIds.Contains(oldInsId))
                                    continue;
                                var oldInstallment = new Installment
                                {
                                    Id = oldInsId,
                                    PolicyId = input.PolicyId,
                                    Status = ((int)ProteinEnums.Status.SILINDI).ToString()
                                };
                                var spResponseOldInstallment = new GenericRepository<Installment>().Insert(oldInstallment, this.Token);
                                if (spResponseOldInstallment.Code != "100")
                                {
                                    response.Code = spResponse.Code;
                                    response.Message = spResponse.Message;
                                    return response;
                                }
                            }
                        }
                    }
                }

            }
            response.Code = "100";
            response.Message = "SUCCESS";
            return response;
        }
        private ServiceResponse InsertInsurer(PolicyDto input)
        {
            ServiceResponse response = new ServiceResponse();
            response.PolicyId = input.PolicyId > 0 ? input.PolicyId : 0;
            if (input.InsurerContactId < 1)
            {

                input.InsurerContactId = GetContactId(input.InsurerTcNo, input.InsurerPassport);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER GetContactId_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

            }

            if (input.InsurerContactId > 0)
            {
                input.InsurerPersonId = GetPersonId(input.InsurerContactId);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER GetPersonId_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                input.InsurerCorporateId = GetCorporateId(input.InsurerContactId);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER GetCorporateId_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                input.InsuredContactAddressId = GetContactAddressId(input.InsurerContactId);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER GetContactAddressId_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");
            }

            var insurerContact = new Contact
            {
                Id = input.InsurerContactId,
                IdentityNo = input.InsurerTcNo.IsIdentityNoLength(),
                TaxNumber = input.InsurerVkn.IsTaxNumberLength(),
                Title = input.InsurerTitle.IsNull() ? null : input.InsurerTitle,
                TaxOffice = input.InsurerTaxOffice.IsNull() ? null : input.InsurerTaxOffice,
                Type = input.InsurerType.IsNull() ? null : input.InsurerType
            };
            SpResponse spResponseContact = new GenericRepository<Contact>().Insert(insurerContact, this.Token);
            //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER InsertContact_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

            if (spResponseContact.Code != "100")
            {
                response.Code = spResponseContact.Code;
                response.Message = spResponseContact.Message;
                return response;
            }
            input.InsurerContactId = spResponseContact.PkId;

            if (input.InsurerGender != null)
            {
                if (input.InsurerGender.Trim().ToUpper() == "K" || input.InsurerGender.Trim().ToUpper() == "E")
                    input.InsurerGender = (input.InsurerGender.Trim().ToUpper().IndexOf("K") == 0 ? "0" : "1");
            }

            if (input.InsurerType == ((int)ContactType.GERCEK).ToString())
            {
                var insurerPerson = new Person
                {
                    Id = input.InsurerPersonId,
                    ContactId = input.InsurerContactId,
                    FirstName = string.IsNullOrEmpty(input.InsurerName) ? null : input.InsurerName,
                    LastName = string.IsNullOrEmpty(input.InsurerLastName) ? null : input.InsurerLastName,
                    Gender = string.IsNullOrEmpty(input.InsurerGender) ? null : input.InsurerGender,
                    Birthdate = input.InsurerBirthDate,
                    Birthplace = string.IsNullOrEmpty(input.InsurerBirthPlace) ? null : input.InsurerBirthPlace,
                    NameOfFather = string.IsNullOrEmpty(input.InsurerNameOfFather) ? null : input.InsurerNameOfFather,
                    NationalityId = GetNationalityID(input.InsurerNationality),
                    PassportNo = string.IsNullOrEmpty(input.InsurerPassport) ? null : input.InsurerPassport
                };
                SpResponse spResponsePerson = new GenericRepository<Person>().Insert(insurerPerson, this.Token);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER InsertPerson_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (spResponsePerson.Code != "100")
                {
                    response.Code = spResponsePerson.Code;
                    response.Message = spResponsePerson.Message;
                    return response;
                }
            }
            else if (input.InsurerType == ((int)ContactType.TUZEL).ToString())
            {
                var insurerCorporate = new Corporate
                {
                    Id = input.InsurerCorporateId,
                    ContactId = input.InsurerContactId,
                    Name = input.InsurerCorporateName,
                    Type = ((int)CorporateType.LIMITED).ToString(),
                };
                SpResponse spResponseCorporate = new GenericRepository<Corporate>().Insert(insurerCorporate, this.Token);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER InsertCorporate_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (spResponseCorporate.Code != "100")
                {
                    response.Code = spResponseCorporate.Code;
                    response.Message = spResponseCorporate.Message;
                    return response;
                }
            }

            if (!String.IsNullOrEmpty(input.InsurerAddress) && input.InsurerCityId != null)
            {
                var adress = new Address
                {
                    Id = input.InsurerAddressId,
                    Type = input.InsurerType == ((int)ContactType.TUZEL).ToString() ? ((int)AddressType.IS).ToString() : ((int)AddressType.EV).ToString(),
                    CityId = input.InsurerCityId,
                    CountyId = input.InsurerCountyId,
                    Details = string.IsNullOrEmpty(input.InsurerAddress) ? null : input.InsurerAddress,
                    ZipCode = string.IsNullOrEmpty(input.InsurerZipCode) ? null : input.InsurerZipCode,
                };

                SpResponse spResponseAdress = new GenericRepository<Address>().Insert(adress, this.Token);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER InsertAddress_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (spResponseAdress.Code == "100")
                {
                    var addressId = spResponseAdress.PkId;
                    if (input.InsurerAddressId <= 0)
                    {
                        var contactAddress = new ContactAddress
                        {
                            Id = input.InsuredContactAddressId,
                            AddressId = addressId,
                            ContactId = input.InsurerContactId,
                        };
                        SpResponse spResponseContactAddress = new GenericRepository<ContactAddress>().Insert(contactAddress, this.Token);
                        //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER InsertContactAddress_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                        if (spResponseContactAddress.Code != "100")
                        {
                            response.Code = spResponseContactAddress.Code;
                            response.Message = spResponseContactAddress.Message;
                            return response;
                        }
                    }
                }
                else
                {
                    response.Code = spResponseAdress.Code;
                    response.Message = spResponseAdress.Message;
                    return response;
                }
            }

            if (!String.IsNullOrEmpty(input.InsurerTelNo))
            {
                var phone = new Phone
                {
                    Id = input.InsurerPhoneId,
                    Type = ((int)PhoneType.IS).ToString(),
                    CountryId = 222,
                    No = input.InsurerTelNo
                };
                SpResponse spResponsePhone = new GenericRepository<Phone>().Insert(phone, this.Token);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER InsertPhone_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (spResponsePhone.Code == "100")
                {
                    var phoneId = spResponsePhone.PkId;
                    if (input.InsurerPhoneId <= 0)
                    {
                        var contactPhone = new ContactPhone
                        {
                            PhoneId = phoneId,
                            ContactId = input.InsurerContactId
                        };
                        SpResponse spResponseContactPhone = new GenericRepository<ContactPhone>().Insert(contactPhone, this.Token);
                        //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER InsertContactPhone_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                        if (spResponseContactPhone.Code != "100")
                        {
                            response.Code = spResponseContactPhone.Code;
                            response.Message = spResponseContactPhone.Message;
                            return response;
                        }
                    }
                }
                else
                {
                    response.Code = spResponsePhone.Code;
                    response.Message = spResponsePhone.Message;
                    return response;
                }
            }

            if (!String.IsNullOrEmpty(input.InsurerGsmNo))
            {
                var phone = new Phone
                {
                    Id = input.InsurerGsmPhoneId,
                    Type = ((int)PhoneType.MOBIL).ToString(),
                    CountryId = 222,
                    No = input.InsurerGsmNo
                };
                SpResponse spResponsePhone = new GenericRepository<Phone>().Insert(phone, this.Token);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER InsertPhoneGSM_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (spResponsePhone.Code == "100")
                {
                    var phoneId = spResponsePhone.PkId;
                    if (input.InsurerGsmPhoneId <= 0)
                    {
                        var contactPhone = new ContactPhone
                        {
                            PhoneId = phoneId,
                            ContactId = input.InsurerContactId
                        };
                        SpResponse spResponseContactPhone = new GenericRepository<ContactPhone>().Insert(contactPhone, this.Token);
                        //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER InsertContactPhoneGSM_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                        if (spResponseContactPhone.Code != "100")
                        {
                            response.Code = spResponseContactPhone.Code;
                            response.Message = spResponseContactPhone.Message;
                            return response;
                        }
                    }
                }
                else
                {
                    response.Code = spResponsePhone.Code;
                    response.Message = spResponsePhone.Message;
                    return response;
                }
            }

            if (!String.IsNullOrEmpty(input.InsurerEmail))
            {
                var email = new Email
                {
                    Id = input.InsurerEmailId,
                    Type = ((int)EmailType.IS).ToString(),
                    Details = input.InsurerEmail
                };
                SpResponse spResponseEmail = new GenericRepository<Email>().Insert(email, this.Token);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER InsertEmail_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (spResponseEmail.Code == "100")
                {
                    var emailId = spResponseEmail.PkId;
                    if (input.InsurerEmailId <= 0)
                    {
                        var contactEmail = new ContactEmail
                        {
                            EmailId = emailId,
                            ContactId = input.InsurerContactId
                        };
                        SpResponse spResponseContactEmail = new GenericRepository<ContactEmail>().Insert(contactEmail, this.Token);
                        //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER InsertContactEmail_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                        if (spResponseContactEmail.Code != "100")
                        {
                            response.Code = spResponseContactEmail.Code;
                            response.Message = spResponseContactEmail.Message;
                            return response;
                        }
                    }
                }
                else
                {
                    response.Code = spResponseEmail.Code;
                    response.Message = spResponseEmail.Message;
                    return response;
                }
            }

            //Banka
            if (input.InsurerBankId.ToString().IsInt64() && input.InsurerBankId > 0)
            {
                var bankAccount = new BankAccount
                {
                    Id = input.InsurerBankAccountId,
                    CurrencyType = input.InsurerBankCurrencyType,
                    BankId = input.InsurerBankId,
                    BankBranchId = input.InsurerBankBranchId,
                    Name = input.InsurerBankAccountName,
                    Iban = input.InsurerIban,
                    No = input.InsurerBankNo,
                    IsPrimary = "1"
                };
                SpResponse spResponseBankAccount = new GenericRepository<BankAccount>().Insert(bankAccount, this.Token);
                //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER InsertBankAccount_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                if (spResponseBankAccount.Code == "100")
                {
                    if (input.InsurerBankAccountId <= 0)
                    {
                        input.InsurerBankAccountId = spResponseBankAccount.PkId;
                        var contactBankAcoount = new ContactBankAccount
                        {
                            BankAccountId = input.InsurerBankAccountId,
                            ContactId = input.InsurerContactId
                        };
                        SpResponse spResponseContactBankAccount = new GenericRepository<ContactBankAccount>().Insert(contactBankAcoount, this.Token);
                        //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER InsertContactBankAccount_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

                        if (spResponseContactBankAccount.Code != "100")
                        {
                            response.Code = spResponseContactBankAccount.Code;
                            response.Message = spResponseContactBankAccount.Message;
                            return response;
                        }
                    }
                }
                else
                {
                    response.Code = spResponseBankAccount.Code;
                    response.Message = spResponseBankAccount.Message;
                    return response;
                }
            }
            response.Code = "100";
            response.Message = "SUCCESS";
            response.Id = spResponseContact.PkId;
            input.InsurerId = response.Id;
            //HttpContext.Current.Response.Write("\n_PoliceAktarService_Operation INSURER Save End_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "_\n");

            return response;
        }
        private long? GetCompanyIdbyPolicy(long policyId)
        {
            long? companyId = null;
            try
            {
                if (policyId < 1)
                    return companyId;

                companyId = new GenericRepository<V_Policy>().FindBy("POLICY_ID=:policyId", orderby: "", parameters: new { policyId }).FirstOrDefault().COMPANY_ID;
            }
            catch (Exception)
            {
                companyId = null;
            }
            return companyId;
        }



        public ServiceResponse InsertInsurer(CommonPolicyDto commonPolicyDto)
        {
            ServiceResponse response = new ServiceResponse();

            V_ContactDetailIds ContactDetailIds = new V_ContactDetailIds();
            List<ISpExecute3> ISpExecute3List = new List<ISpExecute3>();

            InsurerDto insurerDto = commonPolicyDto.insurer;
            if (insurerDto == null)
            {
                response.Code = "900";
                response.Message = "InsurerDto Boş!";
                return response;
            }

            ContactDto contactDto = insurerDto.contact;
            if (contactDto == null)
            {
                response.Code = "900";
                response.Message = "ContactDto Boş!";
                return response;
            }

            if (contactDto.type == ContactType.GERCEK)
            {
                if (insurerDto.person == null)
                {
                    response.Code = "900";
                    response.Message = "Gerçek Kişi Tipindeki Sigorta Ettiren İçin Kişi Bilgileri(PersonDto) Dolu Olmalıdır.";
                    return response;
                }

                if (insurerDto.contactId < 1)
                {
                    string whereConditions = "";
                    if (insurerDto.contact.identityNo.IsIdentityNoLength() != null)
                    {
                        whereConditions += $"IDENTITY_NO={insurerDto.contact.identityNo} OR ";
                    }
                    if (insurerDto.contact.taxNumber.IsTaxNumberLength() != null)
                    {
                        whereConditions += $"TAX_NUMBER={insurerDto.contact.taxNumber} OR ";
                    }
                    if (!insurerDto.person.passportNo.IsNull())
                    {
                        whereConditions += $"PASSPORT_NO='{insurerDto.person.passportNo}' OR ";
                    }
                    if (!whereConditions.IsNull())
                    {
                        whereConditions = "(" + whereConditions.Substring(0, whereConditions.Length - 3) + ")";
                        ContactDetailIds = new GenericRepository<V_ContactDetailIds>().FindBy(whereConditions, orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                        if (ContactDetailIds != null)
                        {
                            insurerDto.contactId = ContactDetailIds.CONTACT_ID;
                            insurerDto.personId = ContactDetailIds.PERSON_ID != null ? (long)ContactDetailIds.PERSON_ID : insurerDto.personId;
                            insurerDto.addressId = ContactDetailIds.ADDRESS_ID != null ? (long)ContactDetailIds.ADDRESS_ID : insurerDto.addressId;
                        }
                    }
                }
                else if (insurerDto.contactId > 0)
                {
                    ContactDetailIds = new GenericRepository<V_ContactDetailIds>().FindBy($"CONTACT_ID={insurerDto.contactId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                    if (ContactDetailIds != null)
                    {
                        insurerDto.contactId = ContactDetailIds.CONTACT_ID;
                        insurerDto.personId = ContactDetailIds.PERSON_ID != null ? (long)ContactDetailIds.PERSON_ID : insurerDto.personId;
                        insurerDto.addressId = ContactDetailIds.ADDRESS_ID != null ? (long)ContactDetailIds.ADDRESS_ID : insurerDto.addressId;
                    }
                }

                var insurerContact = new Contact
                {
                    Id = insurerDto.contactId,
                    IdentityNo = insurerDto.contact.identityNo.IsIdentityNoLength(),
                    TaxNumber = insurerDto.contact.taxNumber.IsTaxNumberLength(),
                    Title = insurerDto.contact.title.IsNull() ? insurerDto.person.firstName + " " + insurerDto.person.lastName : insurerDto.contact.title,
                    TaxOffice = insurerDto.contact.taxOffice,
                    Type = ((int)insurerDto.contact.type).ToString(),
                };
                ISpExecute3List.Add(insurerContact);

                var insurerPerson = new Person
                {
                    Id = insurerDto.personId,
                    ContactId = insurerDto.contactId,
                    FirstName = insurerDto.person.firstName,
                    LastName = insurerDto.person.lastName,
                    Gender = insurerDto.person.gender == null ? null : ((int)insurerDto.person.gender).ToString(),
                    Birthdate = insurerDto.person.birtdate,
                    Birthplace = insurerDto.person.birtplace,
                    NameOfFather = insurerDto.person.nameOfFather,
                    NationalityId = insurerDto.person.nationalityId,
                    PassportNo = insurerDto.person.passportNo
                };
                ISpExecute3List.Add(insurerContact);
            }

            if (contactDto.type == ContactType.TUZEL)
            {
                if (insurerDto.corporate == null)
                {
                    response.Code = "900";
                    response.Message = "Tüzel Kişi Tipindeki Sigorta Ettiren İçin Şirket Bilgileri(CorporateDto) Dolu Olmalıdır.";
                    return response;
                }

                if (insurerDto.contactId < 1)
                {
                    string whereConditions = "";
                    if (insurerDto.contact.identityNo.IsIdentityNoLength() != null)
                    {
                        whereConditions += $"IDENTITY_NO={insurerDto.contact.identityNo} OR ";
                    }
                    if (insurerDto.contact.taxNumber.IsTaxNumberLength() != null)
                    {
                        whereConditions += $"TAX_NUMBER={insurerDto.contact.taxNumber} OR ";
                    }
                    if (!whereConditions.IsNull())
                    {
                        whereConditions = "(" + whereConditions.Substring(0, whereConditions.Length - 3) + ")";
                        ContactDetailIds = new GenericRepository<V_ContactDetailIds>().FindBy(whereConditions, orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                        if (ContactDetailIds != null)
                        {
                            insurerDto.contactId = ContactDetailIds.CONTACT_ID;
                            insurerDto.corporateId = ContactDetailIds.CORPORATE_ID != null ? (long)ContactDetailIds.CORPORATE_ID : insurerDto.corporateId;
                            insurerDto.addressId = ContactDetailIds.ADDRESS_ID != null ? (long)ContactDetailIds.ADDRESS_ID : insurerDto.addressId;
                        }
                    }
                }
                else if (insurerDto.contactId > 0)
                {
                    ContactDetailIds = new GenericRepository<V_ContactDetailIds>().FindBy($"CONTACT_ID={insurerDto.contactId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
                    if (ContactDetailIds != null)
                    {
                        insurerDto.contactId = ContactDetailIds.CONTACT_ID;
                        insurerDto.corporateId = ContactDetailIds.CORPORATE_ID != null ? (long)ContactDetailIds.CORPORATE_ID : insurerDto.corporateId;
                        insurerDto.addressId = ContactDetailIds.ADDRESS_ID != null ? (long)ContactDetailIds.ADDRESS_ID : insurerDto.addressId;
                    }
                }

                var insurerContact = new Contact
                {
                    Id = insurerDto.contactId,
                    IdentityNo = insurerDto.contact.identityNo.IsIdentityNoLength(),
                    TaxNumber = insurerDto.contact.taxNumber.IsTaxNumberLength(),
                    Title = insurerDto.contact.title.IsNull() ? insurerDto.corporate.name : insurerDto.contact.title,
                    TaxOffice = insurerDto.contact.taxOffice,
                    Type = ((int)insurerDto.contact.type).ToString(),
                };
                ISpExecute3List.Add(insurerContact);

                var insurerCorporate = new Corporate
                {
                    Id = insurerDto.corporateId,
                    ContactId = insurerDto.contactId,
                    Name = insurerDto.corporate.name,
                    Type = insurerDto.corporate.type == null ? null : ((int)insurerDto.corporate.type).ToString()
                };

                ISpExecute3List.Add(insurerCorporate);
            }

            if (insurerDto.address != null && insurerDto.address.cityId > 0)
            {
                AddressDto addressDto = insurerDto.address;
                var address = new Address
                {
                    Id = insurerDto.addressId,
                    Type = contactDto.type == ContactType.TUZEL ? ((int)AddressType.IS).ToString() : ((int)AddressType.EV).ToString(),
                    CityId = addressDto.cityId,
                    CountyId = addressDto.countyId,
                    Details = addressDto.details,
                    ZipCode = addressDto.zip_code,
                    District = addressDto.district
                };
                ISpExecute3List.Add(address);

                if (insurerDto.addressId <= 0)
                {
                    var contactAddress = new ContactAddress
                    {
                        AddressId = insurerDto.addressId,
                        ContactId = insurerDto.contactId,
                    };
                    ISpExecute3List.Add(contactAddress);
                }

            }

            if (insurerDto.businessPhone != null)
            {
                PhoneDto phoneDto = insurerDto.businessPhone;
                var phone = new Phone
                {
                    Id = insurerDto.businessPhoneId,
                    Type = ((int)PhoneType.IS).ToString(),
                    CountryId = 222,
                    No = phoneDto.no
                };
                ISpExecute3List.Add(phone);

                if (insurerDto.businessPhoneId <= 0)
                {
                    var contactPhone = new ContactPhone
                    {
                        PhoneId = insurerDto.businessPhoneId,
                        ContactId = insurerDto.contactId
                    };
                    ISpExecute3List.Add(contactPhone);
                }
            }

            if (insurerDto.mobilePhone != null)
            {
                PhoneDto phoneDto = insurerDto.mobilePhone;
                var phone = new Phone
                {
                    Id = insurerDto.mobilePhoneId,
                    Type = ((int)PhoneType.MOBIL).ToString(),
                    CountryId = 222,
                    No = phoneDto.no
                };
                ISpExecute3List.Add(phone);

                if (insurerDto.mobilePhoneId <= 0)
                {
                    var contactPhone = new ContactPhone
                    {
                        PhoneId = insurerDto.mobilePhoneId,
                        ContactId = insurerDto.contactId
                    };
                    ISpExecute3List.Add(contactPhone);
                }
            }

            if (insurerDto.email != null)
            {
                EmailDto emailDto = insurerDto.email;
                var email = new Email
                {
                    Id = insurerDto.emailId,
                    Type = contactDto.type == ContactType.TUZEL ? ((int)EmailType.IS).ToString() : ((int)EmailType.SAHSI).ToString(),
                    Details = emailDto.details
                };
                ISpExecute3List.Add(email);

                if (insurerDto.emailId <= 0)
                {
                    var contactEmail = new ContactEmail
                    {
                        EmailId = insurerDto.emailId,
                        ContactId = insurerDto.contactId
                    };
                    ISpExecute3List.Add(contactEmail);
                }
            }

            if (insurerDto.bankAccount != null && insurerDto.bankAccount.bankId > 0)
            {
                BankAccountDto bankAccountDto = insurerDto.bankAccount;
                var bankAccount = new BankAccount
                {
                    Id = insurerDto.bankAccountId,
                    CurrencyType = bankAccountDto.currencyType != null ? ((int)bankAccountDto.currencyType).ToString() : null,
                    BankId = bankAccountDto.bankId,
                    BankBranchId = bankAccountDto.bankBranchId,
                    Name = bankAccountDto.name,
                    Iban = bankAccountDto.iban,
                    No = bankAccountDto.no,
                    IsPrimary = "1"
                };
                ISpExecute3List.Add(bankAccount);

                if (insurerDto.bankAccountId <= 0)
                {
                    var contactBankAcoount = new ContactBankAccount
                    {
                        BankAccountId = insurerDto.bankAccountId,
                        ContactId = insurerDto.contactId
                    };
                    ISpExecute3List.Add(contactBankAcoount);
                }
            }

            var spResponseList = new GenericRepository<ISpExecute3>().InsertSpExecute3(ISpExecute3List);
            foreach (var item in spResponseList)
            {
                if (item.Code != "100")
                {
                    response.Code = "900";
                    response.Message = item.Message;
                    return response;
                }
            }

            response.Code = "100";
            response.Message = "SUCCESS";

            return response;
        }

        #region SagmerProcess
        private ServiceResponse InsertSagmerInsuredExit(PolicyDto input)
        {
            ServiceResponse response = new ServiceResponse();

            #region SagmerClient
            if (input.PolicyId < 1) { response.Message = "Sagmer gönderimi için PolicyId dolu olmalı."; response.Code = "999"; return response; }
            if (input.EndorsementId < 1) { response.Message = "Sagmer gönderimi için EndorsementId dolu olmalı."; response.Code = "999"; return response; }

            response.PolicyId = input.PolicyId > 0 ? input.PolicyId : 0;
            V_Policy policy = new GenericRepository<V_Policy>().FindBy($" STATUS =:status and IS_SBM_TRANSFER =:isSbmTransfer and POLICY_ID =:PolicyId  AND INTEGRATION_STATUS =:integStatus",
                                                                       fetchDeletedRows: true,
                                                                       orderby: "",
                                                                       parameters: new
                                                                       {
                                                                           status = new Dapper.DbString { Value = ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), Length = 3 },
                                                                           isSbmTransfer = new Dapper.DbString { Value = "1", Length = 3 },
                                                                           input.PolicyId,
                                                                           integStatus = new Dapper.DbString { Value = ((int)ProteinEnums.IntegrationStatus.GONDERILMEDI).ToString(), Length = 3 }
                                                                       }).FirstOrDefault();
            if (policy == null)
            {
                response.Message = "Bu poliçe sagmer gönderime uygun değil (Status veya ürün kontrolü)"; response.Code = "100"; return response;
            }
            input.sagmerPolicyType = (policy.BRANCH_NAME.ToUpper().Contains("SAĞLIK") ? SagmerPolicyType.SAGLIK : SagmerPolicyType.SEYAHAT);
            sagmerClient = new SagmerSrvSoapClient();

            #endregion

            if (input.sagmerPolicyType == SagmerPolicyType.SAGLIK)
            {
                var SaglikResponse = sagmerClient.sigortaliCikisZeyli(input.PolicyId, input.EndorsementId, SagmerApiCode, SagmerServiceType.KONTROL);

                if (SaglikResponse.Data.islemBasarili == true) // Kontrol servis ok
                {
                    response.Code = "100";
                    response.Message = "Sagmer => sigortaliGirisZeyli OK";
                    response.SagmerMessage = SaglikResponse.ServiceException.message;
                    response.SagmerCode = SaglikResponse.ServiceException.code;

                    var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();

                    existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.KONTROL_SERVIS_OK).ToString();

                    SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);

                    if (spResponsePol.Code != "100")
                    {
                        response.Code = spResponsePol.Code;
                        response.Message = "Poliçe durumu değiştirilemedi (STATUS = KONTROL_SERVIS_OK)!";
                        return response;
                    }
                    response.PolicyId = spResponsePol.PkId;
                }
                else // Kontrol servis error
                {
                    response.Message = "Sagmer => sigortaliCikisZeyli Hatası";
                    response.Code = "999";
                    response.SagmerMessage = SaglikResponse.ServiceException.message;
                    response.SagmerCode = SaglikResponse.ServiceException.code;

                    var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();
                    existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.KONTROL_SERVIS_HATA).ToString();
                    SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);
                    if (spResponsePol.Code != "100")
                    {
                        response.Code = spResponsePol.Code;
                        response.Message = "Poliçe durumu değiştirilemedi (STATUS = KONTROL_SERVIS_HATA)!";
                        return response;
                    }
                    response.PolicyId = spResponsePol.PkId;
                }

            }
            else if (input.sagmerPolicyType == SagmerPolicyType.SEYAHAT)  // çıkış seyahat...
            {
                var SeyahatResponse = sagmerClient.seyahatSigortaliCikisZeyli(input.PolicyId, input.EndorsementId, SagmerApiCode, SagmerServiceType.KONTROL);
                if (SeyahatResponse.Data.islemBasarili == true) // Kontrol servis ok
                {
                    response.Code = "100";
                    response.Message = "Sagmer => seyahatSigortaliCikisZeyli OK";
                    response.SagmerMessage = SeyahatResponse.ServiceException.message;
                    response.SagmerCode = SeyahatResponse.ServiceException.code;

                    var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();

                    existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.KONTROL_SERVIS_OK).ToString();

                    SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);

                    if (spResponsePol.Code != "100")
                    {
                        response.Code = spResponsePol.Code;
                        response.Message = "Poliçe durumu değiştirilemedi (STATUS = KONTROL_SERVIS_OK)!";
                        return response;
                    }
                    response.PolicyId = spResponsePol.PkId;
                }
                else // Kontrol servis error
                {
                    response.Message = "Sagmer => seyahatSigortaliCikisZeyli Hatası";
                    response.Code = "999";
                    response.SagmerMessage = SeyahatResponse.ServiceException.message;
                    response.SagmerCode = SeyahatResponse.ServiceException.code;

                    var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();
                    existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.KONTROL_SERVIS_HATA).ToString();
                    SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);
                    if (spResponsePol.Code != "100")
                    {
                        response.Code = spResponsePol.Code;
                        response.Message = "Poliçe durumu değiştirilemedi (STATUS = KONTROL_SERVIS_HATA)!";
                        return response;
                    }
                    response.PolicyId = spResponsePol.PkId;
                }
            }
            return response;
        }
        private ServiceResponse InsertSagmerInsuredEntry(PolicyDto input)
        {
            ServiceResponse response = new ServiceResponse();
            response.PolicyId = input.PolicyId > 0 ? input.PolicyId : 0;
            #region SagmerClient
            if (input.PolicyId < 1) { response.Message = "Sagmer gönderimi için PolicyId dolu olmalı."; response.Code = "999"; return response; }
            if (input.EndorsementId < 1) { response.Message = "Sagmer gönderimi için EndorsementId dolu olmalı."; response.Code = "999"; return response; }

            V_Policy policy = new GenericRepository<V_Policy>().FindBy($" STATUS =:status and IS_SBM_TRANSFER =:isSbmTransfer and POLICY_ID =:PolicyId  AND INTEGRATION_STATUS =:integStatus",
                                                                       fetchDeletedRows: true,
                                                                       orderby: "",
                                                                       parameters: new
                                                                       {
                                                                           status = new Dapper.DbString { Value = ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), Length = 3 },
                                                                           isSbmTransfer = new Dapper.DbString { Value = "1", Length = 3 },
                                                                           input.PolicyId,
                                                                           integStatus = new Dapper.DbString { Value = ((int)ProteinEnums.IntegrationStatus.GONDERILMEDI).ToString(), Length = 3 }
                                                                       }).FirstOrDefault();

            //V_Policy policy = new GenericRepository<V_Policy>().FindBy($" STATUS = '{((int)ProteinEnums.PolicyStatus.TEKLIF).ToString()}' and IS_SBM_TRANSFER = '1' and POLICY_ID = {input.PolicyId}  AND INTEGRATION_STATUS in ('{((int)ProteinEnums.IntegrationStatus.GONDERILMEDI).ToString()}') ", fetchDeletedRows: true, orderby: "").FirstOrDefault();
            if (policy == null)
            {
                response.Message = "Bu poliçe sagmer gönderime uygun değil (Status veya ürün kontrolü)"; response.Code = "100"; return response;
            }
            input.sagmerPolicyType = (policy.BRANCH_NAME.ToUpper().Contains("SAĞLIK") ? SagmerPolicyType.SAGLIK : SagmerPolicyType.SEYAHAT);
            sagmerClient = new SagmerSrvSoapClient();
            #endregion

            if (input.sagmerPolicyType == SagmerPolicyType.SAGLIK)
            {
                var SaglikResponse = sagmerClient.sigortaliGirisZeyli(input.PolicyId, input.EndorsementId, SagmerApiCode, SagmerServiceType.KONTROL);

                if (SaglikResponse.Data.islemBasarili == true) // Kontrol servis ok
                {
                    response.Code = "100";
                    response.Message = "Sagmer => sigortaliGirisZeyli OK";
                    response.SagmerMessage = SaglikResponse.ServiceException.message;
                    response.SagmerCode = SaglikResponse.ServiceException.code;

                    var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();

                    existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.KONTROL_SERVIS_OK).ToString();

                    SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);


                    if (spResponsePol.Code != "100")
                    {
                        response.Code = spResponsePol.Code;
                        response.Message = "Poliçe durumu değiştirilemedi (STATUS = KONTROL_SERVIS_OK)!";
                        return response;
                    }
                }
                else // Kontrol servis ok
                {
                    response.Message = "Sagmer => sigortaliGirisZeyli Hatası";
                    response.Code = "999";
                    response.SagmerMessage = SaglikResponse.ServiceException.message;
                    response.SagmerCode = SaglikResponse.ServiceException.code;

                    var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();
                    existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.KONTROL_SERVIS_HATA).ToString();
                    SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);
                    if (spResponsePol.Code != "100")
                    {
                        response.Code = spResponsePol.Code;
                        response.Message = "Poliçe durumu değiştirilemedi (STATUS = KONTROL_SERVIS_HATA)!";
                        return response;
                    }
                    response.PolicyId = spResponsePol.PkId;
                }

            }
            else if (input.sagmerPolicyType == SagmerPolicyType.SEYAHAT)
            {
                var SeyahatResponse = sagmerClient.seyahatSigortaliGirisZeyli(input.PolicyId, input.EndorsementId, SagmerApiCode, SagmerServiceType.KONTROL);
                if (SeyahatResponse.Data.islemBasarili == true) // Kontrol servis ok
                {
                    response.Code = "100";
                    response.Message = "Sagmer => seyahatSigortaliGirisZeyli OK";
                    response.SagmerMessage = SeyahatResponse.ServiceException.message;
                    response.SagmerCode = SeyahatResponse.ServiceException.code;

                    var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();

                    existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.KONTROL_SERVIS_OK).ToString();

                    SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);


                    if (spResponsePol.Code != "100")
                    {
                        response.Code = spResponsePol.Code;
                        response.Message = "Poliçe durumu değiştirilemedi (STATUS = KONTROL_SERVIS_OK)!";
                        return response;
                    }
                }
                else // Kontrol servis error
                {

                    response.Message = "Sagmer => seyahatSigortaliGirisZeyli Hatası";
                    response.Code = "999";
                    response.SagmerMessage = SeyahatResponse.ServiceException.message;
                    response.SagmerCode = SeyahatResponse.ServiceException.code;

                    var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();
                    existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.KONTROL_SERVIS_HATA).ToString();
                    SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);
                    if (spResponsePol.Code != "100")
                    {
                        response.Code = spResponsePol.Code;
                        response.Message = "Poliçe durumu değiştirilemedi (STATUS = KONTROL_SERVIS_HATA)!";
                        return response;
                    }
                    response.PolicyId = spResponsePol.PkId;
                }

            }

            return response;
        }
        private ServiceResponse InsertSagmerInsurerChange(PolicyDto input)
        {
            ServiceResponse response = new ServiceResponse();
            #region SagmerClient
            if (input.PolicyId < 1) { response.Message = "Sagmer gönderimi için PolicyId dolu olmalı."; response.Code = "999"; return response; }
            if (input.EndorsementId < 1) { response.Message = "Sagmer gönderimi için EndorsementId dolu olmalı."; response.Code = "999"; return response; }

            response.PolicyId = input.PolicyId > 0 ? input.PolicyId : 0;
            V_Policy policy = new GenericRepository<V_Policy>().FindBy($" STATUS =:status and IS_SBM_TRANSFER =:isSbmTransfer and POLICY_ID =:PolicyId  AND INTEGRATION_STATUS =:integStatus",
                                                                       fetchDeletedRows: true,
                                                                       orderby: "",
                                                                       parameters: new
                                                                       {
                                                                           status = new Dapper.DbString { Value = ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), Length = 3 },
                                                                           isSbmTransfer = new Dapper.DbString { Value = "1", Length = 3 },
                                                                           input.PolicyId,
                                                                           integStatus = new Dapper.DbString { Value = ((int)ProteinEnums.IntegrationStatus.GONDERILMEDI).ToString(), Length = 3 }
                                                                       }).FirstOrDefault();
            if (policy == null)
            {
                response.Message = "Bu poliçe sagmer gönderime uygun değil (Status veya ürün kontrolü)"; response.Code = "100"; return response;
            }
            input.sagmerPolicyType = (policy.BRANCH_NAME.ToUpper().Contains("SAĞLIK") ? SagmerPolicyType.SAGLIK : SagmerPolicyType.SEYAHAT);
            sagmerClient = new SagmerSrvSoapClient();
            #endregion

            response.PolicyId = input.PolicyId > 0 ? input.PolicyId : 0;
            var SaglikResponse = sagmerClient.sigortaEttirenDegisiklikZeyli(input.PolicyId, input.EndorsementId, SagmerApiCode, SagmerServiceType.KONTROL);

            if (SaglikResponse.Data.islemBasarili == true) // Kontrol servis ok
            {

            }
            else // Kontrol servis hata
            {

            }
            return response;
        }
        private ServiceResponse InsertSagmerPolicyProd(PolicyDto input)
        {
            ServiceResponse response = new ServiceResponse();

            if (input.PolicyId < 1) { response.Message = "Sagmer gönderimi için PolicyId dolu olmalı."; response.Code = "999"; return response; }
            if (input.EndorsementId < 1) { response.Message = "Sagmer gönderimi için EndorsementId dolu olmalı"; response.Code = "999"; return response; }
            response.PolicyId = input.PolicyId > 0 ? input.PolicyId : 0;
            #region Policy Validation
            V_Policy policy = new GenericRepository<V_Policy>().FindBy($"IS_SBM_TRANSFER =:isSbmTransfer and POLICY_ID =:PolicyId  AND INTEGRATION_STATUS =:integStatus",
                                                                       fetchDeletedRows: true,
                                                                       orderby: "",
                                                                       parameters: new
                                                                       {
                                                                           isSbmTransfer = new Dapper.DbString { Value = "1", Length = 3 },
                                                                           input.PolicyId,
                                                                           integStatus = new Dapper.DbString { Value = ((int)ProteinEnums.IntegrationStatus.KONTROL_SERVIS_OK).ToString(), Length = 3 }
                                                                       }).FirstOrDefault();
            if (policy == null)
            {
                response.Code = "999";
                response.Message = "Bu poliçe sagmer gönderime uygun değil (Status veya ürün kontrolü)"; response.Code = "999"; return response;
            }
            #endregion
            input.sagmerPolicyType = (policy.BRANCH_NAME.ToUpper().Contains("SAĞLIK") ? SagmerPolicyType.SAGLIK : SagmerPolicyType.SEYAHAT);
            sagmerClient = new SagmerSrvSoapClient();

            #region Saglik
            if (input.sagmerPolicyType == SagmerPolicyType.SAGLIK)
            {
                #region Sagmer Police
                var SaglikResponse = sagmerClient.police(input.PolicyId, SagmerApiCode, SagmerServiceType.KONTROL);

                if (SaglikResponse.Data.islemBasarili) // Canlı servis ok
                {
                    #region Giriş Zeyli
                    var SaglikGirisZeylResponse = sagmerClient.sigortaliGirisZeyli(input.PolicyId, input.EndorsementId, SagmerApiCode, SagmerServiceType.KONTROL);
                    if (SaglikGirisZeylResponse.Data.islemBasarili) // Canlı giriş zeyli servis ok
                    {
                        response.Code = "100";
                        response.Message = "Sagmer => sigortaliGirisZeyli OK";
                        response.SagmerMessage = SaglikGirisZeylResponse.ServiceException.message;
                        response.SagmerCode = SaglikGirisZeylResponse.ServiceException.code;

                        var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();
                        existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.CANLI_SERVIS_OK).ToString();
                        SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);
                        if (spResponsePol.Code != "100")
                        {
                            response.Code = spResponsePol.Code;
                            response.Message = "Poliçe durumu değiştirilemedi (STATUS = CANLI_SERVIS_OK)!";
                            return response;
                        }
                        response.PolicyId = spResponsePol.PkId;
                        if (GetCompanyIdsForSortAction().Contains((long)policy.COMPANY_ID) && input.policyIntegrationType == PolicyIntegrationType.WS) // işlem sıraya alınacaksa
                        {
                            IntegrationPolicy intPolicy = new GenericRepository<IntegrationPolicy>().FindById(input.IntegrationId);
                            intPolicy.ResponseDate = DateTime.Now;
                            intPolicy.CorrelationId = input.CorrelationId;
                            SpResponse spResponseIntPol = new GenericRepository<IntegrationPolicy>().Insert(intPolicy, this.Token);
                            if (spResponseIntPol.Code != "100")
                            {
                                response.Code = spResponseIntPol.Code;
                                response.Message = "Kuyruk durumu degistirilemedi!";
                                return response;
                            }
                        }


                    }
                    else
                    {
                        response.Message = "Sagmer => sigortaliGirisZeyli Hatası";
                        response.Code = "999";
                        response.SagmerMessage = SaglikGirisZeylResponse.ServiceException.message;
                        response.SagmerCode = SaglikGirisZeylResponse.ServiceException.code;

                        var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();
                        existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.CANLI_SERVIS_HATA).ToString();
                        SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);
                        if (spResponsePol.Code != "100")
                        {
                            response.Code = spResponsePol.Code;
                            response.Message = "Poliçe durumu değiştirilemedi (STATUS = CANLI_SERVIS_HATA)!";
                            return response;
                        }
                        response.PolicyId = spResponsePol.PkId;
                        if (GetCompanyIdsForSortAction().Contains((long)policy.COMPANY_ID) && input.policyIntegrationType == PolicyIntegrationType.WS) // işlem sıraya alınacaksa
                        {
                            IntegrationPolicy intPolicy = new GenericRepository<IntegrationPolicy>().FindById(input.IntegrationId);
                            intPolicy.ResponseDate = DateTime.Now;
                            intPolicy.CorrelationId = input.CorrelationId;
                            SpResponse spResponseIntPol = new GenericRepository<IntegrationPolicy>().Insert(intPolicy, this.Token);
                            if (spResponseIntPol.Code != "100")
                            {
                                response.Code = spResponseIntPol.Code;
                                response.Message = "Kuyruk durumu degistirilemedi!";
                                return response;
                            }
                        }

                    }
                    #endregion
                }
                else // Canlı servis error
                {
                    response.Message = "Sagmer => policeKontrol Hatası";
                    response.Code = "999";
                    response.SagmerMessage = SaglikResponse.ServiceException.message;
                    response.SagmerCode = SaglikResponse.ServiceException.code;

                    var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();
                    existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.CANLI_SERVIS_HATA).ToString();
                    SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);
                    if (spResponsePol.Code != "100")
                    {
                        response.Code = spResponsePol.Code;
                        response.Message = "Poliçe durumu değiştirilemedi (STATUS = CANLI_SERVIS_HATA)!";
                        return response;
                    }
                    response.PolicyId = spResponsePol.PkId;
                    if (GetCompanyIdsForSortAction().Contains((long)policy.COMPANY_ID) && input.policyIntegrationType == PolicyIntegrationType.WS) // işlem sıraya alınacaksa
                    {
                        IntegrationPolicy intPolicy = new GenericRepository<IntegrationPolicy>().FindById(input.IntegrationId);
                        intPolicy.ResponseDate = DateTime.Now;
                        intPolicy.CorrelationId = input.CorrelationId;
                        SpResponse spResponseIntPol = new GenericRepository<IntegrationPolicy>().Insert(intPolicy, this.Token);
                        if (spResponseIntPol.Code != "100")
                        {
                            response.Code = spResponseIntPol.Code;
                            response.Message = "Kuyruk durumu degistirilemedi!";
                            return response;
                        }
                    }

                }
                #endregion
            }
            #endregion
            #region Seyahat
            else if (input.sagmerPolicyType == SagmerPolicyType.SEYAHAT)
            {
                #region Sagmer Police
                var SeyahatResponse = sagmerClient.seyahatPolice(input.PolicyId, SagmerApiCode, SagmerServiceType.KONTROL);
                if (SeyahatResponse.Data.islemBasarili == true) // Kontrol servis ok
                {
                    #region Giriş Zeyli
                    var SeyahatSigortaliGirisResponse = sagmerClient.seyahatSigortaliGirisZeyli(input.PolicyId, input.EndorsementId, SagmerApiCode, SagmerServiceType.KONTROL);
                    if (SeyahatSigortaliGirisResponse.Data.islemBasarili == true) // Canlı servis ok
                    {
                        var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();
                        existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.CANLI_SERVIS_OK).ToString();
                        SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);
                        if (spResponsePol.Code != "100")
                        {
                            response.Code = spResponsePol.Code;
                            response.Message = "Poliçe durumu değiştirilemedi (STATUS = CANLI_SERVIS_OK)!";
                            return response;
                        }
                        response.PolicyId = spResponsePol.PkId;
                        if (GetCompanyIdsForSortAction().Contains((long)policy.COMPANY_ID) && input.policyIntegrationType == PolicyIntegrationType.WS) // işlem sıraya alınacaksa
                        {
                            IntegrationPolicy intPolicy = new GenericRepository<IntegrationPolicy>().FindById(input.IntegrationId);
                            intPolicy.ResponseDate = DateTime.Now;
                            intPolicy.CorrelationId = input.CorrelationId;
                            SpResponse spResponseIntPol = new GenericRepository<IntegrationPolicy>().Insert(intPolicy, this.Token);
                            if (spResponseIntPol.Code != "100")
                            {
                                response.Code = spResponseIntPol.Code;
                                response.Message = "Kuyruk durumu degistirilemedi!";
                                return response;
                            }
                        }

                        response.Code = "100";
                        response.Message = "Sagmer => seyahatSigortaliGirisZeyli OK";
                        response.SagmerMessage = SeyahatSigortaliGirisResponse.ServiceException.message;
                        response.SagmerCode = SeyahatSigortaliGirisResponse.ServiceException.code;
                    }
                    else // sigprtali giriş servis error
                    {
                        response.Message = "Sagmer => seyahatSigortaliGirisZeyli Hatası";
                        response.Code = "999";
                        response.SagmerMessage = SeyahatSigortaliGirisResponse.ServiceException.message;
                        response.SagmerCode = SeyahatSigortaliGirisResponse.ServiceException.code;

                        var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();
                        existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.CANLI_SERVIS_HATA).ToString();
                        SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);
                        if (spResponsePol.Code != "100")
                        {
                            response.Code = spResponsePol.Code;
                            response.Message = "Poliçe durumu değiştirilemedi (STATUS = CANLI_SERVIS_HATA)!";
                            return response;
                        }
                        response.PolicyId = spResponsePol.PkId;
                        if (GetCompanyIdsForSortAction().Contains((long)policy.COMPANY_ID) && input.policyIntegrationType == PolicyIntegrationType.WS) // işlem sıraya alınacaksa
                        {
                            IntegrationPolicy intPolicy = new GenericRepository<IntegrationPolicy>().FindById(input.IntegrationId);
                            intPolicy.ResponseDate = DateTime.Now;
                            intPolicy.CorrelationId = input.CorrelationId;
                            SpResponse spResponseIntPol = new GenericRepository<IntegrationPolicy>().Insert(intPolicy, this.Token);
                            if (spResponseIntPol.Code != "100")
                            {
                                response.Code = spResponseIntPol.Code;
                                response.Message = "Kuyruk durumu degistirilemedi!";
                                return response;
                            }
                        }

                    }
                    #endregion
                }
                else // Kontrol servis error
                {
                    response.Message = "Sagmer => seyehatPoliceKontrol Hatası";
                    response.Code = "999";
                    response.SagmerMessage = SeyahatResponse.ServiceException.message;
                    response.SagmerCode = SeyahatResponse.ServiceException.code;

                    var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();
                    existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.CANLI_SERVIS_HATA).ToString();
                    SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);
                    if (spResponsePol.Code != "100")
                    {
                        response.Code = spResponsePol.Code;
                        response.Message = "Poliçe durumu değiştirilemedi (STATUS = CANLI_SERVIS_HATA)!";
                        return response;
                    }
                    response.PolicyId = spResponsePol.PkId;
                    if (GetCompanyIdsForSortAction().Contains((long)policy.COMPANY_ID) && input.policyIntegrationType == PolicyIntegrationType.WS) // işlem sıraya alınacaksa
                    {
                        IntegrationPolicy intPolicy = new GenericRepository<IntegrationPolicy>().FindById(input.IntegrationId);
                        intPolicy.ResponseDate = DateTime.Now;
                        intPolicy.CorrelationId = input.CorrelationId;
                        SpResponse spResponseIntPol = new GenericRepository<IntegrationPolicy>().Insert(intPolicy, this.Token);
                        if (spResponseIntPol.Code != "100")
                        {
                            response.Code = spResponseIntPol.Code;
                            response.Message = "Kuyruk durumu degistirilemedi!";
                            return response;
                        }
                    }

                }
                #endregion
            }
            #endregion

            return response;
        }
        private ServiceResponse InsertSagmerPolicyControl(PolicyDto input)
        {
            ServiceResponse response = new ServiceResponse();
            #region SagmerClient

            if (input.PolicyId < 1) { response.Message = "Sagmer gönderimi için PolicyId dolu olmalı."; response.Code = "999"; return response; }
            if (input.EndorsementId < 1) { response.Message = "Sagmer gönderimi için EndorsementId dolu olmalı"; response.Code = "999"; return response; }

            response.PolicyId = input.PolicyId > 0 ? input.PolicyId : 0;
            V_Policy policy = new GenericRepository<V_Policy>().FindBy($" STATUS =:status and IS_SBM_TRANSFER =:isSbmTransfer and POLICY_ID =:PolicyId  AND INTEGRATION_STATUS =:integStatus",
                                                                       fetchDeletedRows: true,
                                                                       orderby: "",
                                                                       parameters: new
                                                                       {
                                                                           status = new Dapper.DbString { Value = ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), Length = 3 },
                                                                           isSbmTransfer = new Dapper.DbString { Value = "1", Length = 3 },
                                                                           input.PolicyId,
                                                                           integStatus = new Dapper.DbString { Value = ((int)ProteinEnums.IntegrationStatus.GONDERILMEDI).ToString(), Length = 3 }
                                                                       }).FirstOrDefault();
            if (policy == null)
            {
                response.Message = "Bu poliçe sagmer gönderime uygun değil (Status veya ürün kontrolü)"; response.Code = "100"; return response;
            }
            input.sagmerPolicyType = (policy.BRANCH_NAME.ToUpper().Contains("SAĞLIK") ? SagmerPolicyType.SAGLIK : SagmerPolicyType.SEYAHAT);
            sagmerClient = new SagmerSrvSoapClient();

            if (input.sagmerPolicyType == SagmerPolicyType.SAGLIK)
            {
                var SaglikResponse = sagmerClient.police(input.PolicyId, SagmerApiCode, SagmerServiceType.KONTROL);

                if (SaglikResponse.Data.islemBasarili) // Kontrol servis ok
                {
                    var SaglikGirisZeylResponse = sagmerClient.sigortaliGirisZeyli(input.PolicyId, input.EndorsementId, SagmerApiCode, SagmerServiceType.KONTROL);

                    if (SaglikGirisZeylResponse.Data.islemBasarili) // Kontrol giriş zeyli servis ok
                    {
                        response.Code = "100";
                        response.Message = "Sagmer => sigortaliGirisZeyli OK";
                        response.SagmerMessage = response.Message = SaglikResponse.ServiceException.message;
                        response.SagmerCode = response.Code = SaglikResponse.ServiceException.code;

                        var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();

                        existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.KONTROL_SERVIS_OK).ToString();

                        SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);


                        if (spResponsePol.Code != "100")
                        {
                            response.Code = spResponsePol.Code;
                            response.Message = "Poliçe durumu değiştirilemedi (STATUS = KONTROL_SERVIS_OK)!";
                            return response;
                        }
                        response.PolicyId = spResponsePol.PkId;
                        if (GetCompanyIdsForSortAction().Contains((long)policy.COMPANY_ID) && input.policyIntegrationType == PolicyIntegrationType.WS) // işlem sıraya alınacaksa
                        {
                            IntegrationPolicy intPolicy = new IntegrationPolicy
                            {
                                CorrelationId = input.CorrelationId,
                                EndorsementId = input.EndorsementId,
                                PolicyID = input.PolicyId,
                                ReqId = input.ReqId,
                                RequestDate = DateTime.Now,
                                Status = ((int)ProteinEnums.Status.AKTIF).ToString()
                            };
                            SpResponse spResponseIntPol = new GenericRepository<IntegrationPolicy>().Insert(intPolicy, this.Token);
                            if (spResponseIntPol.Code != "100")
                            {
                                response.Code = spResponseIntPol.Code;
                                response.Message = "Kuyruga alınamadı!";
                                return response;
                            }
                        }
                        else // düz devam et
                        {
                            response = InsertSagmerPolicyProd(input);
                            if (response.Code == "100")
                                return response;
                            else
                            {
                                response.Code = "999";
                                response.Message = "Sagmer => sigortaliGirisZeyli Hata";
                                return response;
                            }
                        }


                    }
                    else
                    {
                        response.Message = "Sagmer => sigortaliGirisZeyli Hatası";
                        response.Code = "999";
                        response.SagmerMessage = SaglikGirisZeylResponse.ServiceException.message;
                        response.SagmerCode = SaglikGirisZeylResponse.ServiceException.code;

                        var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();
                        existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.KONTROL_SERVIS_HATA).ToString();
                        SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);
                        if (spResponsePol.Code != "100")
                        {
                            response.Code = spResponsePol.Code;
                            response.Message = "Poliçe durumu değiştirilemedi (STATUS = KONTROL_SERVIS_HATA)!";
                            return response;
                        }
                        response.PolicyId = spResponsePol.PkId;
                        //if (GetCompanyIdsForSortAction().Contains((long)policy.COMPANY_ID) && input.policyIntegrationType == PolicyIntegrationType.WS) // işlem sıraya alınacaksa
                        //{
                        //    IntegrationPolicy intPolicy = new IntegrationPolicy
                        //    {
                        //        CorrelationId = input.CorrelationId,
                        //        EndorsementId = input.EndorsementId,
                        //        PolicyID = input.PolicyId,
                        //        ReqId = input.ReqId,
                        //        RequestDate = DateTime.Now,
                        //        Status = ((int)ProteinEnums.Status.AKTIF).ToString()
                        //    };
                        //    SpResponse<IntegrationPolicy> spResponseIntPol = new GenericRepository<IntegrationPolicy>().Insert(intPolicy, this.Token);
                        //    if (spResponseIntPol.Code != "100")
                        //    {
                        //        response.Code = spResponseIntPol.Code;
                        //        response.Message = "Kuyruga alınamadı!";
                        //        return response;
                        //    }
                        //}

                    }
                }
                else // Kontrol servis error
                {
                    response.Message = "Sagmer => policeKontrol Hatası";
                    response.Code = "999";


                    response.SagmerMessage = response.Message = string.Join(", ", SaglikResponse.ServiceException.failureList.Select(x => x.message).ToList());
                    response.SagmerCode = response.Code = string.Join(", ", SaglikResponse.ServiceException.failureList.Select(x => x.code).ToList());

                    var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();
                    existPolicy.Status = ((int)Status.SILINDI).ToString();
                    existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.KONTROL_SERVIS_HATA).ToString();
                    SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);
                    if (spResponsePol.Code != "100")
                    {
                        response.Code = spResponsePol.Code;
                        response.Message = "Poliçe durumu değiştirilemedi (STATUS = KONTROL_SERVIS_HATA)!";
                        return response;
                    }
                    response.PolicyId = spResponsePol.PkId;
                    //if (GetCompanyIdsForSortAction().Contains((long)policy.COMPANY_ID) && input.policyIntegrationType == PolicyIntegrationType.WS) // işlem sıraya alınacaksa
                    //{
                    //    IntegrationPolicy intPolicy = new IntegrationPolicy
                    //    {
                    //        CorrelationId = input.CorrelationId,
                    //        EndorsementId = input.EndorsementId,
                    //        PolicyID = input.PolicyId,
                    //        ReqId = input.ReqId,
                    //        RequestDate = DateTime.Now,
                    //        Status = ((int)ProteinEnums.Status.AKTIF).ToString()
                    //    };
                    //    SpResponse<IntegrationPolicy> spResponseIntPol = new GenericRepository<IntegrationPolicy>().Insert(intPolicy, this.Token);
                    //    if (spResponseIntPol.Code != "100")
                    //    {
                    //        response.Code = spResponseIntPol.Code;
                    //        response.Message = "Kuyruga alınamadı!";
                    //        return response;
                    //    }
                    //}

                }

            }
            else if (input.sagmerPolicyType == SagmerPolicyType.SEYAHAT)
            {
                var SeyahatResponse = sagmerClient.seyahatPolice(input.PolicyId, SagmerApiCode, SagmerServiceType.KONTROL);
                if (SeyahatResponse.Data.islemBasarili == true) // Kontrol servis ok
                {
                    var SeyahatSigortaliGirisResponse = sagmerClient.seyahatSigortaliGirisZeyli(input.PolicyId, input.EndorsementId, SagmerApiCode, SagmerServiceType.KONTROL);
                    if (SeyahatSigortaliGirisResponse.Data.islemBasarili == true) // Kontrol giriş zeyli servis ok
                    {
                        response.Code = "100";
                        response.Message = "Sagmer => seyahatSigortaliGirisZeyli OK";
                        response.SagmerMessage = SeyahatSigortaliGirisResponse.ServiceException.message;
                        response.SagmerCode = SeyahatSigortaliGirisResponse.ServiceException.code;

                        var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();
                        existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.KONTROL_SERVIS_OK).ToString();
                        SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);
                        if (spResponsePol.Code != "100")
                        {
                            response.Code = spResponsePol.Code;
                            response.Message = "Poliçe durumu değiştirilemedi (STATUS = KONTROL_SERVIS_OK)!";
                            return response;
                        }
                        response.PolicyId = spResponsePol.PkId;
                        if (GetCompanyIdsForSortAction().Contains((long)policy.COMPANY_ID) && input.policyIntegrationType == PolicyIntegrationType.WS) // işlem sıraya alınacaksa
                        {
                            IntegrationPolicy intPolicy = new IntegrationPolicy
                            {
                                CorrelationId = input.CorrelationId,
                                EndorsementId = input.EndorsementId,
                                PolicyID = input.PolicyId,
                                ReqId = input.ReqId,
                                RequestDate = DateTime.Now,
                                Status = ((int)ProteinEnums.Status.AKTIF).ToString()
                            };
                            SpResponse spResponseIntPol = new GenericRepository<IntegrationPolicy>().Insert(intPolicy, this.Token);
                            if (spResponseIntPol.Code != "100")
                            {
                                response.Code = spResponseIntPol.Code;
                                response.Message = "Kuyruga alınamadı!";
                                return response;
                            }
                        }
                        else // düz devam et
                        {
                            response = InsertSagmerPolicyProd(input);
                            response.Code = "999";
                            response.Message = "Sagmer => sigortaliGirisZeyli Hata";
                            return response;
                        }

                    }
                    else // sigprtali giriş servis error
                    {
                        response.Message = "Sagmer => seyahatSigortaliGirisZeyli Hatası";
                        response.Code = "999";
                        response.SagmerMessage = SeyahatSigortaliGirisResponse.ServiceException.message;
                        response.SagmerCode = SeyahatSigortaliGirisResponse.ServiceException.code;

                        var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();
                        existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.KONTROL_SERVIS_HATA).ToString();
                        SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);
                        if (spResponsePol.Code != "100")
                        {
                            response.Code = spResponsePol.Code;
                            response.Message = "Poliçe durumu değiştirilemedi (STATUS = KONTROL_SERVIS_HATA)!";
                            return response;
                        }
                        response.PolicyId = spResponsePol.PkId;
                        //if (GetCompanyIdsForSortAction().Contains((long)policy.COMPANY_ID) && input.policyIntegrationType == PolicyIntegrationType.WS)
                        //{
                        //    IntegrationPolicy intPolicy = new IntegrationPolicy
                        //    {
                        //        CorrelationId = input.CorrelationId,
                        //        EndorsementId = input.EndorsementId,
                        //        PolicyID = input.PolicyId,
                        //        ReqId = input.ReqId,
                        //        RequestDate = DateTime.Now,
                        //        Status = ((int)ProteinEnums.Status.AKTIF).ToString()
                        //    };
                        //    SpResponse<IntegrationPolicy> spResponseIntPol = new GenericRepository<IntegrationPolicy>().Insert(intPolicy, this.Token);
                        //    if (spResponseIntPol.Code != "100")
                        //    {
                        //        response.Code = spResponseIntPol.Code;
                        //        response.Message = "Kuyruga alınamadı!";
                        //        return response;
                        //    }
                        //}


                    }
                }
                else // Kontrol servis error
                {
                    response.Message = "Sagmer => seyahatPoliceKontrol Hatası";
                    response.Code = "999";
                    response.SagmerMessage = SeyahatResponse.ServiceException.message;
                    response.SagmerCode = SeyahatResponse.ServiceException.code;

                    var existPolicy = new GenericRepository<Policy>().FindBy("ID =:id", parameters: new { id = policy.POLICY_ID }).FirstOrDefault();
                    existPolicy.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.KONTROL_SERVIS_HATA).ToString();
                    SpResponse spResponsePol = new GenericRepository<Policy>().Insert(existPolicy, this.Token);
                    if (spResponsePol.Code != "100")
                    {
                        response.Code = spResponsePol.Code;
                        response.Message = "Poliçe durumu değiştirilemedi (STATUS = KONTROL_SERVIS_HATA)!";
                        return response;
                    }
                    response.PolicyId = spResponsePol.PkId;
                    //if (GetCompanyIdsForSortAction().Contains((long)policy.COMPANY_ID) && input.policyIntegrationType == PolicyIntegrationType.WS) // işlem sıraya alınacaksa
                    //{
                    //    IntegrationPolicy intPolicy = new IntegrationPolicy
                    //    {
                    //        CorrelationId = input.CorrelationId,
                    //        EndorsementId = input.EndorsementId,
                    //        PolicyID = input.PolicyId,
                    //        ReqId = input.ReqId,
                    //        RequestDate = DateTime.Now,
                    //        Status = ((int)ProteinEnums.Status.AKTIF).ToString()
                    //    };
                    //    SpResponse<IntegrationPolicy> spResponseIntPol = new GenericRepository<IntegrationPolicy>().Insert(intPolicy, this.Token);
                    //    if (spResponseIntPol.Code != "100")
                    //    {
                    //        response.Code = spResponseIntPol.Code;
                    //        response.Message = "Kuyruga alınamadı!";
                    //        return response;
                    //    }
                    //}

                }

            }
            #endregion

            return response;
        }
        #endregion

        #region Private Methods
        private long? GetNationalityID(string CountryCode)
        {
            long? Id = null;
            try
            {
                if (CountryCode.IsNull())
                {
                    CountryCode = "TR";
                }

                if (CountryCode.ToUpper() == "TRK")
                    CountryCode = "TR";

                if (CountryCode.IsInt64())
                    return long.Parse(CountryCode);

                Id = new GenericRepository<Country>().FindBy($" A2_CODE =:code", parameters: new { code = new Dapper.DbString { Value = CountryCode.ToUpper().Trim(), Length = 3 } }).FirstOrDefault().Id;
            }
            catch { Id = null; }
            return Id;
        }

        private string GetAgencyID(string AgencyCode, long CompanyId)
        {
            string Id = "";
            try
            {

                var resAgency = new GenericRepository<Agency>().FindBy($"COMPANY_ID =:CompanyId and NO =:agencyNo", parameters: new { CompanyId, agencyNo = new Dapper.DbString { Value = AgencyCode, Length = 20 } });

                if (resAgency.Count > 0)
                {
                    var resPackage = new GenericRepository<V_Package>().FindBy($"COMPANY_ID =:CompanyId", parameters: new { CompanyId }, orderby: "");
                    if (resPackage.Count > 0)
                        Id = resAgency.FirstOrDefault().Id.ToString();
                }
            }
            catch { }
            return Id;
        }
        private long CheckTCKNandPassportNo(Int64? TCKN = 0, string passportNo = "")
        {
            long Id = 0;
            try
            {
                if (TCKN.IsIdentityNoLength() != null)
                {
                    var contact = new GenericRepository<Contact>().FindBy(" IDENTITY_NO =:TCKN", parameters: new { TCKN }).FirstOrDefault();

                    Id = contact == null ? 0 : contact.Id;
                }

                if (!passportNo.IsNull() && Id == 0)
                {
                    var person = new GenericRepository<Person>().FindBy(" PASSPORT_NO =:passportNo", parameters: new { passportNo = new Dapper.DbString { Value = passportNo, Length = 20 } }).FirstOrDefault();

                    Id = person == null ? 0 : person.Id;
                }

            }
            catch { return 0; }
            return Id;
        }

        private long GetContactAddressId(long insuredContactId)
        {
            long Id = 0;
            try
            {
                Id = new GenericRepository<ContactAddress>().FindBy("CONTACT_ID=:contactId", parameters: new { contactId = insuredContactId }).FirstOrDefault().Id;
            }
            catch { return 0; }
            return Id;
        }

        private long GetContactId(long? TCKN = 0, string passportNo = "")
        {
            long Id = 0;
            try
            {
                if (TCKN != null && TCKN.ToString().Length == 11)
                {
                    var contact = new GenericRepository<Contact>().FindBy($" IDENTITY_NO = :TCKN", parameters: new { TCKN }).FirstOrDefault();

                    Id = contact == null ? 0 : contact.Id;
                }
                if (!passportNo.IsNull() && Id == 0 && passportNo != "0")
                {
                    var person = new GenericRepository<Person>().FindBy($" PASSPORT_NO =:passportNo", parameters: new { passportNo = new Dapper.DbString { Value = passportNo, Length = 20 } }).FirstOrDefault();

                    Id = person == null ? 0 : person.ContactId;
                }
            }
            catch { return 0; }
            return Id;
        }
        private long GetPersonId(Int64 contactId)
        {
            long Id = 0;
            try
            {
                if (contactId > 0)
                {
                    var person = new GenericRepository<Person>().FindBy($"CONTACT_ID =:contactId", parameters: new { contactId }).FirstOrDefault();

                    Id = person == null ? 0 : person.Id;
                }

            }
            catch { return 0; }
            return Id;
        }
        private long GetCorporateId(Int64 contactId)
        {
            long Id = 0;
            try
            {
                if (contactId > 0)
                {
                    var contact = new GenericRepository<Corporate>().FindBy($" CONTACT_ID =:contactId", parameters: new { contactId }).FirstOrDefault();
                    Id = contact.Id;
                }
            }
            catch { return 0; }
            return Id;
        }
        private long GetInsuredParentId(long EndorsId, string FamilyNo)
        {
            long Id = 0;

            try
            {
                if (EndorsId > 0 && FamilyNo.IsInt64())
                {
                    V_InsuredEndorsement insuredEndors = new GenericRepository<V_InsuredEndorsement>().FindBy($"ENDORSEMENT_ID =:EndorsId AND INDIVIDUAL_TYPE =:indType AND FAMILY_NO =:FamilyNo ", orderby: "", parameters: new { EndorsId, indType = new Dapper.DbString { Value = ((int)IndividualType.FERT).ToString(), Length = 3 }, familyNo = long.Parse(FamilyNo) }).FirstOrDefault();
                    if (insuredEndors.INSURED_ID > 0)
                        Id = insuredEndors.INSURED_ID;
                }
            }
            catch { return 0; }
            return Id;
        }
        private List<long> GetCompanyIdsForSortAction()
        {
            return new List<long> { 5, 10, 9 };
        }
        private long GetCityId(string CityCode)
        {
            long Id = 0;

            try
            {
                CityCode = CityCode.Trim().TrimStart('0');
                CityCode = (CityCode.Length == 1 ? "00" : "") + CityCode;
                CityCode = (CityCode.Length == 2 ? "0" : "") + CityCode;
                var exitsCity = new GenericRepository<City>().FindBy("CODE =:code", parameters: new { code = new Dapper.DbString { Value = CityCode, Length = 3 } });
                if (exitsCity.Count > 0)
                    Id = exitsCity[0].Id;
            }
            catch
            {

            }
            return Id;
        }
        private long GetBankIdByCode(string BankCode)
        {
            long Id = 0;

            try
            {
                var existsBank = new GenericRepository<Bank>().FindBy($"CODE =:code", parameters: new { code = new Dapper.DbString { Value = BankCode, Length = 20 } });
                if (existsBank.Count > 0)
                    Id = existsBank[0].Id;
            }
            catch { }
            return Id;
        }
        private long GetBankBranchIdByCode(string BankCode)
        {
            long Id = 0;

            try
            {
                var existsBankBranch = new GenericRepository<BankBranch>().FindBy($"CODE =:code", parameters: new { code = new Dapper.DbString { Value = BankCode, Length = 20 } });
                if (existsBankBranch.Count > 0)
                    Id = existsBankBranch[0].Id;
            }
            catch { }
            return Id;
        }
        #endregion
    }
}
