﻿using Protein.Common.Constants;
using Protein.Common.Dto.ImportObjects;
using Protein.Common.Enums;
using Protein.Common.Extensions;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Business.Concrete.Services
{
    public class ClaimService
    {
        public ClaimDto ClaimToDoga(long ClaimID)
        {
            ClaimDto claimDto = new ClaimDto();
            try
            {
                V_Claim claim = new GenericRepository<V_Claim>().FindBy(" CLAIM_ID =:ClaimID", orderby: "CLAIM_ID", parameters: new { ClaimID }, fetchDeletedRows: true, hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                if (claim == null)
                    return null;

                #region hospitalization
                ClaimHospitalization hosp = new GenericRepository<ClaimHospitalization>().FindBy(" CLAIM_ID =:ClaimID", orderby: "CLAIM_ID", parameters: new { ClaimID }).FirstOrDefault();
                //if (hosp == null)
                //    return null;
                V_Staff staff = new GenericRepository<V_Staff>().FindBy("STAFF_ID=:staffId", orderby: "STAFF_ID", parameters: new { staffId = claim.STAFF_ID }).FirstOrDefault();
                //if (staff == null)
                //    return null;

                #endregion
                claimDto.ProviderId = (long)claim.PROVIDER_ID;
                claimDto.ExitDate = hosp?.ExitDate;
                claimDto.DoctorName = staff?.FIRST_NAME;// (!string.IsNullOrEmpty(staff.MIDDLE_NAME) ? " " + staff.LAST_NAME : "");
                claimDto.DoctorName = staff?.LAST_NAME;// (!string.IsNullOrEmpty(staff.MIDDLE_NAME) ? " " + staff.LAST_NAME : "");
                claimDto.DoctorTCNO = staff?.IDENTITY_NO;
                claimDto.DoctorDiplomaNo = staff?.DIPLOMA_NO;
                claimDto.DoctorTitle = staff?.STAFF_TITLE;
                claimDto.ClaimId = ClaimID;

                #region Insured
                V_Insured insured = new GenericRepository<V_Insured>().FindBy("INSURED_ID =:insuredId", orderby: "INSURED_ID", parameters: new { insuredId = claim.INSURED_ID }).FirstOrDefault();
                if (insured == null)
                    return null;
                #endregion
                #region Policy
                V_Policy policy = new GenericRepository<V_Policy>().FindBy(fetchDeletedRows: true, conditions: $"POLICY_ID =:policyId ", orderby: "", parameters: new { policyId = insured.POLICY_ID }).FirstOrDefault();
                if (policy == null)
                    return null;
                #endregion

                claimDto.InsuredId = insured.INSURED_ID;
                claimDto.PaymentMethodText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.ClaimPaymentMethod, claim.PAYMENT_METHOD);
                claimDto.ClaimDate = claim.CLAIM_DATE;
                claimDto.PolicyNo = policy.POLICY_NUMBER;
                claimDto.StatusText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.ClaimStatus, claim.STATUS);
                claimDto.CompanyClaimId = claim.COMPANY_CLAIM_ID.ToString();
                #region Epikriz Note
                List<V_ClaimNote> claimeEpikrizNote = new GenericRepository<V_ClaimNote>().FindBy($"CLAIM_ID =:ClaimID AND CLAIM_NOTE_TYPE =:claimNoteType", parameters: new { ClaimID, claimNoteType = new Dapper.DbString { Value = ((int)ProteinEnums.ClaimNoteType.EPIKRIZ).ToString(), Length = 3 } }, orderby: "");
                if (claimeEpikrizNote != null)
                {
                    if (claimeEpikrizNote.Count > 0)
                    {
                        foreach (V_ClaimNote note in claimeEpikrizNote)
                            claimDto.EpikrizNote += note.NOTE_DECRIPTION + " ";
                    }
                }
                #endregion
                #region Complaint Note
                List<V_ClaimNote> claimNote = new GenericRepository<V_ClaimNote>().FindBy($"CLAIM_ID =:ClaimID AND CLAIM_NOTE_TYPE =:claimNoteType", parameters: new { ClaimID, claimNoteType = new Dapper.DbString { Value = ((int)ProteinEnums.ClaimNoteType.NORMAL).ToString(), Length = 3 } }, orderby: "");
                if (claimNote != null)
                {
                    if (claimNote.Count > 0)
                    {
                        foreach (V_ClaimNote note in claimNote)
                            claimDto.Complaint += note.NOTE_DECRIPTION + " ";
                    }
                }
                #endregion

                claimDto.ReasonText = claim.REASON_DESCRIPTION;
                claimDto.InsuredIdentityNo = insured.IDENTITY_NO;
                claimDto.InsuredFirstName = insured.FIRST_NAME;
                claimDto.InsuredLastName = insured.LAST_NAME;

                #region Provider
                V_Provider provider = new GenericRepository<V_Provider>().FindBy($"PROVIDER_ID =:providerId", parameters: new { providerId = claim.PROVIDER_ID }, orderby: "").FirstOrDefault();
                if (provider == null)
                    return null;
                #endregion
                claimDto.ProviderIdentityNo = provider.IDENTITY_NO;
                claimDto.ProviderCityCode = provider.CITY_CODE;
                claimDto.ProviderCountyCode = provider.COUNTY_CODE.ToString();
                claimDto.EntranceDate = hosp?.EntranceDate;
                claimDto.RenewalNo = policy.RENEWAL_NO;
                claimDto.ClaimCity = provider.CITY_NAME;
                claimDto.ClaimCounty = provider.COUNTY_NAME;
                claimDto.NoticeDate = claim.NOTICE_DATE;

                #region ClaimCoverage
                List<V_ClaimCoverage> claimCoverage = new GenericRepository<V_ClaimCoverage>().FindBy(" CLAIM_ID =:ClaimID", orderby: "CLAIM_ID", parameters: new { ClaimID });
                decimal TotalPaid = 0;
                if (claimCoverage != null)
                {
                    if (claimCoverage.Count > 0)
                    {
                        claimDto.Processes = new List<ProcessListDto>();

                        foreach (V_ClaimCoverage cCoverage in claimCoverage)
                        {
                            ProcessListDto _process = new ProcessListDto
                            {
                                Requested = cCoverage.Requested,
                                SgkAmount = cCoverage.SgkAmount,
                                Paid = cCoverage.Paid,
                                OutOfScopeAmount = cCoverage.Requested - cCoverage.Confirmed,
                                ParticipationAmount = cCoverage.Confirmed - cCoverage.Paid,
                                ExemptionAmount = cCoverage.Exemption,
                                CoverageCode = cCoverage.CoverageId.ToString(),
                                CoveragePackageNo = claim.PACKAGE_NO.ToString(),
                                CoverageVariation = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.ClaimVariation, claim.VARIATION),
                                CoverageTypeText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Coverage, cCoverage.CoverageType),
                            };
                            claimDto.Processes.Add(_process);
                            TotalPaid += cCoverage.Paid;
                        }
                    }
                }
                #endregion
                #region Contact & Corporate & lookup
                //Contact contact = new GenericRepository<Contact>().FindById(provider.CONTACT_ID);
                //if (contact == null)
                //    return null;
                //Corporate corporate = new GenericRepository<Corporate>().FindBy($"CONTACT_ID =:contactId", parameters: new { contactId = provider.CONTACT_ID }).FirstOrDefault();
                //if (contact == null)
                //    return null;
                #endregion
                #region ClaimBill 
                V_ClaimBill claimBill = new GenericRepository<V_ClaimBill>().FindBy(" CLAIM_ID =:ClaimID", orderby: "CLAIM_ID", parameters: new { ClaimID }).FirstOrDefault();
                if (claimBill != null)
                {
                    claimDto.Bill = new BillDto();
                    //claimDto.Bill.CorporateTypeText = ((ProteinEnums.CorporateType)int.Parse(corporate.Type)).ToString();
                    claimDto.Bill.Date = (DateTime)claimBill.BILL_DATE;
                    claimDto.Bill.No = claimBill.BILL_NO;
                    claimDto.Bill.TotalPaid = TotalPaid;
                    claimDto.Bill.TotalStoppage = ((TotalPaid / 100) * decimal.Parse(provider.STOPPAGE_OVERWRITE.ToString()));
                }
                #endregion
                #region ClaimProcess
                List<V_ClaimIcd> claimIcd = new GenericRepository<V_ClaimIcd>().FindBy(" CLAIM_ID =:ClaimID", orderby: "CLAIM_ID", parameters: new { ClaimID });
                if (claimIcd != null)
                {
                    if (claimIcd.Count > 0)
                    {
                        claimDto.ICD = new List<ICDDto>();
                        foreach (V_ClaimIcd clIcd in claimIcd)
                        {
                            ICDDto _icd = new ICDDto();

                            _icd.ICDCode = clIcd.ICD_CODE;
                            _icd.ICDType = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.ICD, clIcd.ICD_TYPE);
                            _icd.ICDTypeCode = LookupHelper.GetLookupCodeByOrdinal(Constants.LookupTypes.ICD, clIcd.ICD_TYPE);
                            claimDto.ICD.Add(_icd);
                        }

                    }
                }
                #endregion


            }
            catch { }
            return claimDto;
        }

        public ClaimDto ClaimToCommon(long ClaimID, string ClaimStatus)
        {
            ClaimDto claimDto = new ClaimDto();
            try
            {
                V_Claim claim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID =:ClaimID AND STATUS=:status", parameters: new { ClaimID, status = new Dapper.DbString { Value = ClaimStatus, Length = 3 } }, fetchDeletedRows: true, orderby: "", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                if (claim == null)
                    return null;

                claimDto.ClaimId = (long)claim.CLAIM_ID;
                claimDto.CompanyId = (long)claim.COMPANY_ID;
                claimDto.CompanyClaimId = claim.COMPANY_CLAIM_ID.ToString();
                if (claim.REASON_ID.ToString().IsInt64())
                {
                    claimDto.ReasonText = claim.REASON_DESCRIPTION;
                }
                else
                {
                    claimDto.ReasonText = "OK";
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return claimDto;
        }

        public PayrollDto PayrollToDoga(long PayrollID)
        {
            PayrollDto payrollDto = new PayrollDto();

            try
            {
                V_Payroll payroll = new GenericRepository<V_Payroll>().FindBy($"PAYROLL_ID =:payrollId", parameters: new { payrollId = PayrollID }, orderby: "PAYROLL_ID", fetchDeletedRows: true).FirstOrDefault();
                if (payroll == null)
                    return null;
                
                V_Provider provider = new GenericRepository<V_Provider>().FindBy($"PROVIDER_ID =:providerId", parameters: new { providerId = payroll.PROVIDER_ID }, orderby: "PROVIDER_ID").FirstOrDefault();
                if (provider == null)
                    return null;

                V_ProviderBankAccount providerBank = new GenericRepository<V_ProviderBankAccount>().FindBy($"PROVIDER_ID =:providerId", parameters: new { providerId = provider.PROVIDER_ID }, orderby: "IS_PRIMARY DESC").FirstOrDefault();
                if (providerBank == null)
                    return null;

                List<V_Claim> claims = new GenericRepository<V_Claim>().FindBy($"PAYROLL_ID =:PayrollID AND COMPANY_CLAIM_ID IS NOT NULL", orderby: "", parameters: new { PayrollID });
                if (claims == null) return null;


                payrollDto.PayrollId = PayrollID;
                payrollDto.Date = (DateTime)payroll.PAYROLL_DATE;
                payrollDto.ProviderIdentityNo = provider.IDENTITY_NO;
                payrollDto.ProviderTypeText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Provider, provider.PROVIDER_TYPE);
                payrollDto.ProviderIban = providerBank.IBAN;
                

                payrollDto.ClaimList = new List<ClaimDto>();
                foreach (V_Claim claim in claims)
                {
                    ClaimDto _claimDto = new ClaimDto
                    {
                        InsuredFirstName = claim.FIRST_NAME,
                        InsuredLastName = claim.LAST_NAME,
                        InsuredIdentityNo = claim.IDENTITY_NO,
                        InsuredIban = claim.IBAN,
                        CompanyClaimId= claim.COMPANY_CLAIM_ID.ToString(),
                        ClaimId = claim.CLAIM_ID,
                        Bill = new BillDto
                        {
                            No = claim.BILL_NO,
                            Date = (DateTime)claim.BILL_DATE,
                            TotalPaid = (decimal)claim.PAID
                        },
                        PaymentDate = claim.PAYMENT_DATE
                    };
                    payrollDto.ClaimList.Add(_claimDto);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return payrollDto;
        }
        public PayrollDto PayrollToNN(long PayrollID)
        {
            PayrollDto dto = new PayrollDto();

            V_Payroll payroll = new GenericRepository<V_Payroll>().FindBy("PAYROLL_ID =:payrollId", parameters: new { payrollId = PayrollID }, orderby: "PAYROLL_ID").FirstOrDefault();
            if (payroll == null)
                return null;
            List<V_ClaimBill> claimBillList = new GenericRepository<V_ClaimBill>().FindBy("PAYROLL_ID =:payrollId", parameters: new { payrollId = PayrollID }, orderby: "PAYROLL_ID");
            if (claimBillList == null || claimBillList.Count <= 0)
                return null;
            dto.ClaimList = new List<ClaimDto>();
            foreach (var item in claimBillList)
            {
                ClaimDto claimDto = new ClaimDto();
                claimDto.Bill.Date = (DateTime)item.BILL_DATE;
                claimDto.Bill.No = item.BILL_NO;

                Claim claim = new GenericRepository<Claim>().FindById((long)item.CLAIM_ID);
                if (claim == null)
                    return null;
                claimDto.CompanyClaimId = claim.CompanyClaimId.ToString();

                dto.ClaimList.Add(claimDto);
            }


            return dto;
        }
        public PayrollDto PayrollToKatilim(V_Payroll payroll)
        {
            PayrollDto dto = new PayrollDto();


            dto.PayrollId = payroll.PAYROLL_ID;
            dto.CompanyId = (long)payroll.COMPANY_ID;
            dto.Date = (DateTime)payroll.PAYROLL_DATE;
            dto.DueDate = payroll.DUE_DATE;
            dto.PayrollTypeCode = LookupHelper.GetLookupCodeByOrdinal(Constants.LookupTypes.Payroll, payroll.PAYROLL_TYPE);

            List<V_ClaimBill> claimBillList = new GenericRepository<V_ClaimBill>().FindBy("PAYROLL_ID =:payrollId", parameters: new { payrollId = payroll.PAYROLL_ID }, orderby: "PAYROLL_ID");
            if (claimBillList == null || claimBillList.Count <= 0)
                return null;

            dto.ClaimList = new List<ClaimDto>();
            foreach (var item in claimBillList)
            {
                ClaimDto claimDto = new ClaimDto();
                claimDto.Bill = new BillDto();
                claimDto.Bill.CurrencyCode = LookupHelper.GetLookupCodeByOrdinal(Constants.LookupTypes.Currency, item.CURRENCY_TYPE);
                claimDto.Bill.Date = (DateTime)item.BILL_DATE;
                claimDto.Bill.No = item.BILL_NO;

                Claim claim = new GenericRepository<Claim>().FindById((long)item.CLAIM_ID);
                if (claim == null)
                    return null;
                claimDto.ClaimId = claim.Id;
                claimDto.CompanyClaimId = claim.CompanyClaimId.ToString();
                dto.InsuredId = (long)claim.InsuredId;

                List<V_ClaimCoverage> claimCoverage = new GenericRepository<V_ClaimCoverage>().FindBy("CLAIM_ID =:claimId", parameters: new { claimId = item.CLAIM_ID }, orderby: "CLAIM_ID");
                ClaimPayment claimPayment = new GenericRepository<ClaimPayment>().FindBy("CLAIM_ID =:claimId", parameters: new { claimId = item.CLAIM_ID }, orderby: "CLAIM_ID").FirstOrDefault();

                decimal TotalPaid = 0;
                foreach (V_ClaimCoverage _claimCoverage in claimCoverage)
                    TotalPaid += _claimCoverage.Paid;
                claimDto.Bill.TotalPaid = TotalPaid;

                dto.ClaimList.Add(claimDto);
            }

            if (payroll.PAYROLL_TYPE == ((int)PayrollType.KURUM).ToString())
            {
                V_ProviderBankAccount providerBankAccount = new GenericRepository<V_ProviderBankAccount>().FindBy("PROVIDER_ID =:providerId AND IBAN IS NOT NULL", parameters: new { providerId = payroll.PROVIDER_ID }, orderby: "IS_PRIMARY DESC").FirstOrDefault();
                if (providerBankAccount == null)
                    return null;
                dto.ProviderIban = providerBankAccount.IBAN;
                dto.ProviderId = (long)payroll.PROVIDER_ID;
            }
            else if (payroll.PAYROLL_TYPE == ((int)PayrollType.SIGORTALI).ToString())
            {
                V_InsuredBankAccount insuredBankAccount = new GenericRepository<V_InsuredBankAccount>().FindBy($"INSURED_ID =:insuredId AND IBAN IS NOT NULL", parameters: new { insuredId = dto.InsuredId }, orderby: "IS_PRIMARY DESC").FirstOrDefault();
                if (insuredBankAccount == null)
                    return null;
                dto.InsuredIban = insuredBankAccount.IBAN;
            }

            return dto;
        }
    }
}
