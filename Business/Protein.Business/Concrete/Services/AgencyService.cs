﻿using Protein.Business.Abstract.Service;
using Protein.Business.Concrete.Responses;
using Protein.Business.Concrete.Validator;
using Protein.Common.Dto.ImportObjects;
using Protein.Common.Enums;
using Protein.Common.Extensions;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Business.Concrete.Services
{
    public class AgencyService : IService<AgencyDto>
    {
        public ServiceResponse ServiceInsert(AgencyDto input, string Token = "9999999999")
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                long contractId = 0, addressId = 0, agencyId = 0;

                //=new ImportValidator<AgencyImportDto>().CheckValidFile();
                #region Find Company
                if (!FindCompany(input.CompanyId))
                {
                    response.Code = "999"; response.Message = "Belirsiz sigorta şirketi";
                    return response;
                }
                #endregion
                #region Same Agency Control
                input.AgencyId = CheckSameAgency(input.No, input.CompanyId);
                if (input.AgencyId>0)
                {
                    Agency checkAgency = new GenericRepository<Agency>().FindBy($"ID=:id",parameters: new {id= input.AgencyId }).FirstOrDefault();
                    input.ContactId = checkAgency.ContactId==null ? 0 : (long)checkAgency.ContactId;
                    input.AddressId = checkAgency.AddressId==null ? 0 : (long)checkAgency.AddressId;

                    V_AgencyPhone phone = new GenericRepository<V_AgencyPhone>().FindBy("AGENCY_ID=:id AND PHONE_TYPE=:type", orderby: "AGENCY_ID", parameters: new { id= input.AgencyId, type = new Dapper.DbString { Value = ((int)PhoneType.IS).ToString(), Length = 3 } }).FirstOrDefault();
                    if (phone != null)
                    {
                        input.PhoneId = (long)phone.PHONE_ID;
                        input.AgencyPhoneId = (long)phone.AGENCY_PHONE_ID;
                    }

                    V_AgencyPhone mobilePhone = new GenericRepository<V_AgencyPhone>().FindBy("AGENCY_ID=:id AND PHONE_TYPE=:type", orderby: "AGENCY_ID", parameters: new { id = input.AgencyId, type = new Dapper.DbString { Value = ((int)PhoneType.MOBIL).ToString(), Length = 3 } }).FirstOrDefault();
                    if (mobilePhone != null)
                    {
                        input.MobilePhoneId = (long)mobilePhone.PHONE_ID;
                        input.AgencyMobilePhoneId = (long)mobilePhone.AGENCY_PHONE_ID;
                    }

                    V_AgencyPhone faxPhone = new GenericRepository<V_AgencyPhone>().FindBy("AGENCY_ID=:id AND PHONE_TYPE=:type", orderby: "AGENCY_ID", parameters: new { id = input.AgencyId, type = new Dapper.DbString { Value = ((int)PhoneType.FAX).ToString(), Length = 3 } }).FirstOrDefault();
                    if (faxPhone != null)
                    {
                        input.FaxPhoneId = (long)faxPhone.PHONE_ID;
                        input.AgencyFaxPhoneId = (long)faxPhone.AGENCY_PHONE_ID;
                    }
                }
                #endregion

                #region Type Valid
                int outS1;

                if (!int.TryParse(input.Type, out outS1))
                {
                    response.Code = "999"; response.Message = "Acente tipi hatalı";
                    return response;
                }
                if (!(int.Parse(input.Type) > 0 && int.Parse(input.Type) < 12))
                {
                    response.Code = "999"; response.Message = "Acente tipi 1 ile 11 arasına girmelisiniz.";
                    return response;
                }
                #endregion
                if (!string.IsNullOrEmpty(input.City) || !input.CityCode.IsNull())
                {
                    if (input.City != null)
                    {
                        input.City = GetCityId(input.City).ToString();
                        input.County = GetCountyId(input.County, input.City).ToString();
                        if (input.City == "0" && input.County == "0")
                        {
                            response.Code = "999"; response.Message = $" İl/İlçe bilgisi bulunamadı";
                            RollBack(agencyId: agencyId, ContactId: contractId, Token: Token);
                            return response;
                        }
                    }
                }

                #region Agency insrt
                Agency agency = new Agency
                {
                    Id = input.AgencyId < 0 ? 0 : input.AgencyId,
                    Name = input.AgencyTitle,
                    Type = input.Type,
                    CompanyId = input.CompanyId,
                    Status = string.IsNullOrEmpty(input.ActivePasive) ? null : ((int)(ProteinEnums.Status)Enum.Parse(typeof(ProteinEnums.Status),
                string.Join("", input.ActivePasive.ToUpper().Normalize(NormalizationForm.FormD).Where(k => char.GetUnicodeCategory(k) != UnicodeCategory.NonSpacingMark)))).ToString(),
                    No = input.No,
                    CloseDownDate = input.CloseDownDate,
                    EstablishmentDate = input.EstablishmentDate,
                    PlateNo = input.PlateNo,
                    IsOpen = string.IsNullOrEmpty(input.IsOpen) ? null : (input.IsOpen.ToLower().Trim() == "açık" ? "1" : "0"),
                    RegionCode = input.RegionCode,
                    RegionName = input.RegionName,
                    ContactFirstName = input.ContactFirstName,
                    ContactLastName = input.ContactLastName,
                    ContactPhone = input.ContactPhone
                };
                SpResponse spResAgency = new GenericRepository<Agency>().Insert(agency, Token);
                if (spResAgency.Code != "100")
                {
                    response.Code = spResAgency.Code;
                    response.Message = spResAgency.Message;
                    return response;
                }
                else response.Id = agencyId = spResAgency.PkId;
                #endregion
                #region contact insrt
                Contact contact = new Contact
                {
                    Id = input.ContactId < 0 ? 0 : input.ContactId,
                    TaxNumber = input.TaxNumber,
                    TaxOffice = input.TaxOffice,
                    Status = ((int)ProteinEnums.Status.AKTIF).ToString(),
                    Title = input.AgencyTitle,
                    Type = ((int)ProteinEnums.ContactType.TUZEL).ToString(),
                };
                SpResponse spResContact = new GenericRepository<Contact>().Insert(contact, Token);
                if (spResContact.Code != "100")
                {
                    response.Code = spResContact.Code;
                    response.Message = spResContact.Message;
                    RollBack(agencyId: agencyId, Token: Token);
                    return response;
                }
                else
                {
                    agency.Id = spResAgency.PkId;
                    agency.ContactId = spResContact.PkId;
                    new GenericRepository<Agency>().Insert(agency, Token);
                    contractId = spResContact.PkId;
                }
                #endregion
                #region Address insert
                if (!string.IsNullOrEmpty(input.Address) || !string.IsNullOrEmpty(input.City) || !input.CityCode.IsNull())
                {
                    Address address = new Address
                    {
                        Id = input.AddressId < 0 ? 0 : input.AddressId,
                        CityId = input.City.IsInt64() && long.Parse(input.City) <= 0 ? null : (long?)long.Parse(input.City),
                        Status = ((int)ProteinEnums.Status.AKTIF).ToString(),
                        Type = ((int)ProteinEnums.AddressType.IS).ToString(),
                        ZipCode = input.ZipCode,
                        Details = input.Address.IsNull() ? "-" : input.Address,
                        CountyId = input.County.IsInt64() && long.Parse(input.County) <= 0 ? null : (long?)long.Parse(input.County)
                    };
                    SpResponse spResAddress = new GenericRepository<Address>().Insert(address, Token);
                    if (spResAddress.Code != "100")
                    {
                        response.Code = spResAddress.Code;
                        response.Message = spResAddress.Message;
                        RollBack(agencyId: agencyId, ContactId: contractId, AddressId: addressId, Token: Token);
                        return response;
                    }
                    else
                    {
                        addressId = spResAddress.PkId;
                        agency.Id = agencyId;
                        agency.AddressId = addressId;
                        new GenericRepository<Agency>().Insert(agency, Token);

                    }
                }
                #endregion
                #region Phone insrt

                if (!string.IsNullOrEmpty(input.FaxNo))
                {
                    if (!PhoneInsert(input.FaxPhoneId, input.AgencyFaxPhoneId, agencyId, input.FaxNo, ProteinEnums.PhoneType.FAX, Token))
                    {
                        response.Code = "999";
                        response.Message = "Fax numarası kayıt edilemedi!";
                        RollBack(agencyId: agencyId, ContactId: contractId, AddressId: addressId, Token: Token);
                        return response;
                    }
                }
                if (!string.IsNullOrEmpty(input.MobileNo))
                {
                    if (!PhoneInsert(input.MobilePhoneId, input.AgencyMobilePhoneId, agencyId, input.MobileNo, ProteinEnums.PhoneType.MOBIL, Token))
                    {
                        response.Code = "999";
                        response.Message = "Mobil numarası kayıt edilemedi!";
                        RollBack(agencyId: agencyId, ContactId: contractId, AddressId: addressId, Token: Token);
                        return response;
                    }
                }
                if (!string.IsNullOrEmpty(input.PhoneNo))
                {
                    if (!PhoneInsert(input.PhoneId, input.AgencyPhoneId, agencyId, input.PhoneNo, ProteinEnums.PhoneType.IS, Token))
                    {
                        response.Code = "999";
                        response.Message = "Telefon numarası kayıt edilemedi!";
                        RollBack(agencyId: agencyId, ContactId: contractId, AddressId: addressId, Token: Token);
                        return response;
                    }
                }
                response.Code = "100"; response.Message = "Başarılı kayıt";

                #endregion
            }
            catch (Exception ex) { response.Code = "999"; response.Message = ex.ToString(); }


            return response;
        }

        #region private methods
        private bool FindCompany(long CompanyId)
        {
            bool result = false;
            try
            {
                Company comp = new GenericRepository<Company>().FindBy($"ID =:id", parameters: new { id = CompanyId }).FirstOrDefault();

                if (!string.IsNullOrEmpty(comp.Name)) result = true;
                else result = false;
            }
            catch(Exception e) { }
            return result;
        }
        private long CheckSameAgency(string No,long companyId)
        {
            long id = 0;
            try
            {
                Agency agency = new GenericRepository<Agency>().FindBy($"NO =:No AND COMPANY_ID =:companyId", parameters: new { No = new Dapper.DbString { Value = No, Length = 20 }, companyId }).FirstOrDefault();
                if (!string.IsNullOrEmpty(agency.No)) id = agency.Id;
                else id = 0;
            }
            catch (Exception e) { }
            return id;
        }
        private Int64? GetCityId(string cityName)
        {
            long? cityId = 0;

            try
            {
                if (cityName.IsInt() && cityName.Length < 3)
                {
                    var zero = "";
                    for (int i = 0; i < 3 - cityName.Length; i++)
                    {
                        zero += "0";
                    }
                    cityName = zero + cityName;
                }
                cityId = new GenericRepository<City>().FindBy($"NAME=:city OR CODE=:city", parameters: new { city = cityName.ToUpper().Trim() }).FirstOrDefault().Id;
            }
            catch (Exception e) { }
            return cityId;
        }
        private Int64 GetCountyId(string countyName, string cityId)
        {
            long countyId = 0;
            try
            {
                if (cityId.IsInt64() && cityId != "0")
                    countyId = new GenericRepository<County>().FindBy($"(NAME='{countyName.ToUpper().Trim()}' OR CODE='{countyName.ToUpper().Trim()}') AND CITY_ID=:cityId AND TYPE='{((int)CountyType.SBM).ToString()}'", parameters: new { cityId }).FirstOrDefault().Id;
            }
            catch (Exception e) { }
            return countyId;
        }
        private bool PhoneInsert(long phoneId, long agencyPhoneId, long AgencyId, string PhoneNo, ProteinEnums.PhoneType type, string Token = "")
        {
            bool result = false;
            try
            {
                Phone phone = new Phone
                {
                    Id = phoneId < 0 ? 0 : phoneId,
                    Status = ((int)ProteinEnums.Status.AKTIF).ToString(),
                    Type = ((int)type).ToString(),
                    No = PhoneNo,
                    CountryId = 222
                };
                SpResponse spResPhone = new GenericRepository<Phone>().Insert(phone, Token);
                if (spResPhone.Code == "100")
                {
                    AgencyPhone agencyPhone = new AgencyPhone
                    {
                        Id = agencyPhoneId < 0 ? 0 : agencyPhoneId,
                        AgencyId = AgencyId,
                        PhoneId = spResPhone.PkId,
                        Status = ((int)ProteinEnums.Status.AKTIF).ToString()
                    };
                    SpResponse spResAgencyPhone = new GenericRepository<AgencyPhone>().Insert(agencyPhone, Token);
                    if (spResAgencyPhone.Code == "100")
                        result = true;
                    else RollBack(PhoneId: spResPhone.PkId);
                }
            }
            catch (Exception ex) { result = false; }
            return result;
        }
        private void RollBack(long agencyId = 0, long ContactId = 0, long AddressId = 0, long PhoneRelBaseId = 0, long PhoneId = 0, string Token = "", long companyId = 0)
        {
            try
            {
                if (agencyId > 0)
                {
                    new GenericRepository<Agency>().Insert(new Agency
                    {
                        Id = agencyId,
                        CompanyId = companyId,
                        Status = ((int)ProteinEnums.Status.SILINDI).ToString()
                    }, Token);
                }
                if (ContactId > 0)
                {
                    new GenericRepository<Contact>().Insert(new Contact
                    {
                        Id = ContactId,
                        Status = ((int)ProteinEnums.Status.SILINDI).ToString()
                    }, Token);
                }
                if (AddressId > 0)
                {
                    new GenericRepository<Address>().Insert(new Address
                    {
                        Id = AddressId,
                        Status = ((int)ProteinEnums.Status.SILINDI).ToString()
                    }, Token);
                }
                if (PhoneRelBaseId > 0)
                {
                    new GenericRepository<AgencyPhone>().Insert(new AgencyPhone
                    {
                        Id = PhoneRelBaseId,
                        Status = ((int)ProteinEnums.Status.SILINDI).ToString()
                    });
                }
                if (PhoneId > 0)
                {

                    new GenericRepository<Phone>().Insert(new Phone
                    {
                        Id = PhoneId,
                        Status = ((int)ProteinEnums.Status.SILINDI).ToString()
                    });
                }
            }
            catch (Exception e) { }
        }
        #endregion
    }
}
