﻿using Protein.Common.Dto.ImportObjects;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Business.Concrete.Services
{
    public class ProviderService
    {
        public ProviderDto FillProvider(long ProviderID,long companyId)
        {
            ProviderDto dto = new ProviderDto();

            V_Provider provider = new GenericRepository<V_Provider>().FindBy("PROVIDER_ID=:ProviderID", parameters: new { ProviderID }, orderby: "").FirstOrDefault();
            if (provider != null)
            {
                dto.AddressInfo = new ProviderAddress
                {
                    CountryCode = provider.COUNTRY_CODE,
                    DistrictCode = provider.COUNTY_CODE.ToString(),
                    ProvinceCode = provider.CITY_CODE,
                    Details = provider.ADDRESS,
                    PostCode = provider.ZIP_CODE
                };

                #region BankAccount
                V_ProviderBankAccount bankAccount = new GenericRepository<V_ProviderBankAccount>().FindBy($"PROVIDER_ID=:ProviderID", parameters: new { ProviderID }, orderby: "IS_PRIMARY DESC").FirstOrDefault();
                if (bankAccount != null)
                {
                    dto.BankAccountInfo = new ProviderBank
                    {
                        AccountHolderName = bankAccount.BANK_ACCOUNT_NAME,
                        IBAN = bankAccount.IBAN
                    };
                }
                #endregion

                dto.CompanyCode = provider.PROVIDER_ID;
                dto.CompanyName = provider.PROVIDER_NAME;

                #region CompanyNote
                List<V_ProviderNote> noteList = new GenericRepository<V_ProviderNote>().FindBy($"PROVIDER_ID=:ProviderID", parameters: new { ProviderID }, orderby: "PROVIDER_ID");
                if (noteList != null && noteList.Count > 0)
                {
                    var noteString = string.Join(" - ", noteList.Select(x => x.NOTE_DESCRIPTION).ToList());
                    dto.CompanyNote = noteString;
                }
                #endregion

                dto.ContactInfo = new ProviderContact
                {
                    LandPhone=provider.PHONE_NO
                };

                dto.CompanyType = provider.PROVIDER_TYPE;
                dto.EInvoice = provider.IS_E_BILL == "1" ? "E" : "H";
                
                #region LegalEntity
                dto.LegalEntityInfo = new LegalEntity
                {
                    AddressInfo = new ProviderAddress
                    {
                        CountryCode = provider.COUNTRY_CODE,
                        DistrictCode = provider.COUNTY_CODE.ToString(),
                        ProvinceCode = provider.CITY_CODE,
                        Details = provider.ADDRESS,
                        PostCode = provider.ZIP_CODE
                    },
                    CompanyTitle = provider.PROVIDER_TITLE,
                    MersisNo = provider.OFFICIAL_CODE,
                    MainCompanyCode = provider.PROVIDER_GROUP_ID.ToString(),
                    TaxNo = provider.TAX_NUMBER,
                    TaxOffice = provider.TAX_OFFICE
                };
                #endregion
                #region Staff
                V_Staff staff = new GenericRepository<V_Staff>().FindBy(conditions: $"PROVIDER_ID=:ProviderID", parameters: new { ProviderID, isGroupAdmin = new Dapper.DbString { Value = "1", Length = 3 } }, orderby: "IS_GROUP_ADMIN DESC").FirstOrDefault();
                if (staff != null)
                {
                    dto.LegalEntityInfo.AuthorizedPerson = new AuthorizedPerson
                    {
                        BirthDate = staff.BIRTHDATE,
                        BirthPlace=staff.BIRTHPLACE,
                        ContactInfo = new ProviderContact
                        {
                            EmailAddress = staff.EMAIL_ADDRESS,
                            LandPhone = staff.PHONE_NO,
                            MobilePhone = staff.MOBILE_PHONE_NO
                        },
                        FatherName=staff.NAME_OF_FATHER,
                        Gender=LookupHelper.GetLookupCodeByOrdinal(LookupTypes.Gender,staff.GENDER),
                        Name = staff.FIRST_NAME,
                        //Nation=staff.c,
                        PassportNo=staff.PASSPORT_NO,
                        Surname = staff.LAST_NAME,
                        TCKN = staff.IDENTITY_NO,
                        VKN=staff.TAX_NUMBER
                    };

                    V_ContactAddress staffAddress = new GenericRepository<V_ContactAddress>().FindBy($"CONTACT_ID={staff.CONTACT_ID}",orderby:"").FirstOrDefault();
                    if (staffAddress!=null)
                    {
                        dto.LegalEntityInfo.AuthorizedPerson.AddressInfo = new ProviderAddress
                        {
                            Details = staffAddress.DETAILS,
                            CountryCode=staffAddress.COUNTRY_NUM_CODE,
                            DistrictCode=staffAddress.COUNTY_CODE,
                            ProvinceCode=staffAddress.CITY_CODE,
                            PostCode = staffAddress.ZIP_CODE
                        };
                    }
                }
                #endregion

                dto.State = 0;

                V_ContractNetwork v_ContractNetwork = new GenericRepository<V_ContractNetwork>().FindBy($"PROVIDER_ID={ProviderID} AND COMPANY_ID={companyId}", orderby: "").FirstOrDefault();
                if (v_ContractNetwork!=null)
                {
                    if (v_ContractNetwork.CONTRACT_STATUS==((int)ContractStatus.AKTIF).ToString())
                    {
                        dto.State = 1;
                    }
                }
            }
            return dto;
        }
    }
}
