﻿using Protein.Common.Dto.MediaObjects;
using Protein.Common.Entities;
using Protein.Common.Enums;
using Protein.Common.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Business.Concrete.Media
{
    public class MediaWorker
    {
        public bool DeleteMedia(long MediaId, string ObjectType, long BaseId, long ObjectId)
        {
            bool result = false;
            try
            {
                var instance = Activator.CreateInstance(typeof(Protein.Common.Entities.ProteinEntities));
                var nestedClasses = instance.GetType().GetNestedTypes();

                foreach (var item in nestedClasses)
                {
                    var type = item.GetCustomAttributes(false);
                    var tableAttrName = type.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.TableAttribute)) as System.ComponentModel.DataAnnotations.Schema.TableAttribute;

                    if (ObjectType == tableAttrName.Name)
                    {
                        Type repositoryType = typeof(GenericRepository<>);
                        Type genericTypeSub = repositoryType.MakeGenericType(item);
                        var instanceGenericRepositorySub = Activator.CreateInstance(genericTypeSub);

                        var SubEntity = Activator.CreateInstance(item);

                        //string ObjectIdColName = "";
                        //string BaseIdColName = "";
                        //typename'i bulana kadar dön
                        foreach (PropertyInfo prop in item.GetProperties())
                        {
                            if (prop.GetCustomAttributes(typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute), false).Any())
                            {
                                if ((prop.GetCustomAttribute(typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute), false) as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute).Name == "ID")
                                {
                                    prop.SetValue(SubEntity, BaseId);
                                }

                                else if ((prop.GetCustomAttribute(typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute), false) as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute).Name == "STATUS")
                                {
                                    prop.SetValue(SubEntity, ((int)ProteinEnums.Status.SILINDI).ToString());
                                }
                                else if ((prop.GetCustomAttribute(typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute), false) as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute).Name == "MEDIA_ID")
                                {
                                    prop.SetValue(SubEntity, MediaId);
                                }

                                else if ((prop.GetCustomAttribute(typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute), false) as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute).TypeName == "OBJECTID")
                                {
                                    prop.SetValue(SubEntity, ObjectId);
                                }

                            }
                        }
                        //for (int i = 0; i < ObjectIdColName.Split('_').Count(); i++)
                        //{
                        //    if (i == 0)
                        //        BaseIdColName = ObjectIdColName.Split('_')[0] + "_MEDIA";
                        //    else
                        //        BaseIdColName += "_" + ObjectIdColName.Split('_')[i];
                        //}

                        object[] parametersArray = new object[] { SubEntity,"" };
                        var a = genericTypeSub.GetMethod("Insert");
                        object returnMediaList =a.Invoke(instanceGenericRepositorySub, parametersArray);

                        ProteinEntities.Media media = new ProteinEntities.Media();
                        media.Id = MediaId;
                        media.Status = ((int)ProteinEnums.Status.SILINDI).ToString();
                        SpResponse spResponseMedia = new GenericRepository<ProteinEntities.Media>().Insert(media);
                        if (spResponseMedia.Code == "100")
                            result = true;
                    }
                }
            }
            catch(Exception ex) { }
            return result;
        }

        public List<MediaListDto> FillMedia(long ObjectId, string ObjectType, string DiffrentObjectType = "")
        {
            List<MediaListDto> mediaList = new List<MediaListDto>();
            try
            {
                string viewName = "";
                for (int s1 = 0; s1 < ObjectType.Split('_').Count(); s1++)
                {
                    if (s1 == 0)
                        viewName = ObjectType.ToUpper().Split('_')[0].Replace("T", "V");
                    else viewName += "_" + ObjectType.ToUpper().Split('_')[s1];
                }

                var instance = Activator.CreateInstance(typeof(ProteinEntities));
                var nestedClasses = instance.GetType().GetNestedTypes();

                foreach (var item in nestedClasses)
                {
                    var type = item.GetCustomAttributes(false);
                    var tableAttrName = type.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.TableAttribute)) as System.ComponentModel.DataAnnotations.Schema.TableAttribute;

                    if (tableAttrName.Name == viewName.ToUpper())
                    {

                        Type repositoryType = typeof(GenericRepository<>);
                        Type genericType = repositoryType.MakeGenericType(item);
                        var instanceGenericRepository = Activator.CreateInstance(genericType);

                        string ObjectIdColName = ""; //örn: product_id
                        string BaseIdColName = ""; // media_id
                                                   //typename'i bulana kadar dön

                        foreach (PropertyInfo prop in item.GetProperties())
                        {
                            if (prop.GetCustomAttributes(typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute), false).Any())
                            {
                                if ((prop.GetCustomAttribute(typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute), false) as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute).TypeName == "OBJECTID")
                                {
                                    ObjectIdColName = (prop.GetCustomAttribute(typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute), false) as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute).Name;
                                    break;
                                }
                            }
                        }


                        for (int i = 0; i < ObjectIdColName.Split('_').Count(); i++)
                        {
                            if (i == 0)
                                BaseIdColName = ObjectIdColName.Split('_')[0] + "_MEDIA";
                            else
                                BaseIdColName += "_" + ObjectIdColName.Split('_')[i];
                        }

                        ObjectIdColName = (string.IsNullOrEmpty(DiffrentObjectType) ? ObjectIdColName : DiffrentObjectType);
                        object[] parametersArray = new object[] { ObjectIdColName + $" = {ObjectId}", false, false,
                        "",null,""};

                        object returnMediaList = genericType.GetMethod("FindBy").Invoke(instanceGenericRepository, parametersArray);
                        IList listt = (IList)returnMediaList;

                        foreach (var _media in listt)
                        {
                            MediaListDto md = new MediaListDto();
                            //md.Name = _media.MEDIA_NAME;
                            //md.ObjectId = ObjectId;
                            //md. = _media.FILE_CONTENT;

                            foreach (PropertyInfo prop in _media.GetType().GetProperties())
                            {

                                if ((prop.GetCustomAttribute(typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute), true) as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute).Name == "MEDIA_ID")
                                    md.MediaId = long.Parse(prop.GetValue(_media, null).ToString());
                                else if ((prop.GetCustomAttribute(typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute), true) as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute).Name == "MEDIA_NAME")
                                {
                                    try
                                    {
                                        md.Name = prop.GetValue(_media, null).ToString();
                                    }
                                    catch 
                                    {
                                        md.Name = "";
                                    }
                                }
                                else if ((prop.GetCustomAttribute(typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute), false) as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute).Name == BaseIdColName)
                                    md.BaseId = long.Parse(prop.GetValue(_media, null).ToString());
                            }
                            md.ObjectId = ObjectId;
                            mediaList.Add(md);
                        }
                        break;
                    }
                }
            }
            catch(Exception ex) { }
            return mediaList;
        }
        /// <summary>
        /// ilişkili media'ları döker. örnek  T_PRODUCT_MEDIA İÇİN  object_id = product_id
        /// </summary>
        /// <returns></returns>
        public List<MediaListDto> GetAllFindByObjectId<T>(long ObjectId) where T : class, new()
        {
            List<MediaListDto> returnList = new List<MediaListDto>();
            try
            {
                T instance = Activator.CreateInstance<T>();

                string ObjectIdColumnName = ((instance.GetType().GetCustomAttributes(false)).FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.TableAttribute)) as System.ComponentModel.DataAnnotations.Schema.TableAttribute).Name.Split('_')[1].ToString().Replace("_", "") + "_ID";
                string BaseIdColumnName = ((instance.GetType().GetCustomAttributes(false)).FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.TableAttribute)) as System.ComponentModel.DataAnnotations.Schema.TableAttribute).Name.Split('_')[1].ToString().Replace("_", "") + "_MEDIA_ID";

                List<T> result = new GenericRepository<T>().FindBy(conditions: ObjectIdColumnName + $" =:ObjectId", parameters: new { ObjectId }, orderby: "");

                if (result.Count < 1) return null;

                foreach (T item in result)
                {
                    MediaListDto media = new MediaListDto();

                    PropertyInfo prop_mediaName = PropHelper.FindPropByColumnAttrName("MEDIA_NAME", item.GetType());
                    PropertyInfo prop_mediaFileContent = PropHelper.FindPropByColumnAttrName("FILE_CONTENT", item.GetType());
                    PropertyInfo prop_mediaFileName = PropHelper.FindPropByColumnAttrName("FILENAME", item.GetType());
                    PropertyInfo prop_mediaId = PropHelper.FindPropByColumnAttrName("MEDIA_ID", item.GetType());
                    PropertyInfo prop_baseId = PropHelper.FindPropByColumnAttrName(BaseIdColumnName, item.GetType());

                    BinaryFormatter bf = new BinaryFormatter();
                    using (MemoryStream ms = new MemoryStream())
                    {
                        bf.Serialize(ms, prop_mediaFileContent.GetValue(item, null));
                        media.FileContent = ms.ToArray();
                    }

                    media.FileName = prop_mediaFileName.GetValue(item, null) != null ? prop_mediaFileName.GetValue(item, null).ToString() : "";
                    media.Name = prop_mediaName.GetValue(item, null) != null ? prop_mediaName.GetValue(item, null).ToString() : "";
                    media.MediaId = prop_mediaId.GetValue(item, null) != null ? long.Parse(prop_mediaId.GetValue(item, null).ToString()) : 0;
                    media.ObjectId = ObjectId;
                    media.BaseId = prop_baseId.GetValue(item, null) != null ? long.Parse(prop_baseId.GetValue(item, null).ToString()) : 0;
                    returnList.Add(media);
                }
            }
            catch { return null; }

            return returnList;
        }
    }
}
