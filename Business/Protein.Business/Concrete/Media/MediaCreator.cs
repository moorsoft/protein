﻿using Dapper;
using Oracle.DataAccess.Client;
using Protein.Business.Concrete.Responses;
using Protein.Common.Dto.MediaObjects;
using Protein.Common.Entities;
using Protein.Common.Enums;
using Protein.Common.Helpers;
using Protein.Data;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Business.Concrete.Media
{
    public class MediaCreator<T> where T : class, new()
    {
        public MediaResponse CreateIt(MediaDto input)
        {
            MediaResponse mediaResponse = new MediaResponse();

            try
            {
                ProteinEntities.Media media = new ProteinEntities.Media();

                if (input.BaseId < 1 || input.MEDIA_PATH != null)
                {
                    using (var binaryReader = new BinaryReader(input.MEDIA_PATH.InputStream))
                    {
                        media.FileContent = binaryReader.ReadBytes(input.MEDIA_PATH.ContentLength);
                    }
                }
                media.FileName = input.FileName;
                media.Id = input.MediaId;
                media.Status = ((int)ProteinEnums.Status.AKTIF).ToString();
                media.Name = input.Name;
                #region props,
                T instance = Activator.CreateInstance<T>();
                PropertyInfo mediaProperty = PropHelper.FindPropByColumnAttrName("MEDIA_ID", instance.GetType());
                PropertyInfo statusProperty = PropHelper.FindPropByColumnAttrName("STATUS", instance.GetType());
                PropertyInfo baseIdProperty = PropHelper.FindPropByColumnAttrName("ID", instance.GetType());

                string objectIdColName = ((instance.GetType().GetCustomAttributes(false)).FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.TableAttribute)) as System.ComponentModel.DataAnnotations.Schema.TableAttribute).Name.Split('_')[1].ToString().Replace("_", "") + "_ID";

                PropertyInfo objectIdProperty = PropHelper.FindPropByColumnAttrName(objectIdColName, instance.GetType());
                #endregion"

                if (mediaProperty == null || statusProperty == null || objectIdProperty == null)
                {
                    mediaResponse.IsSuccess = false;
                    mediaResponse.Message = "Media objesi, Object veya status (kolonu) bulunamadı!";
                    return mediaResponse;
                }

                if (input.BaseId < 1 || input.MEDIA_PATH != null)
                {
                    Stream stream = new MemoryStream(media.FileContent);
                    #region params
                    OracleDynamicParameters mediaParams = new OracleDynamicParameters();
                    //DynamicParameters mediaParams2 = new DynamicParameters();
                    mediaParams.Add(name: "p_FILE_CONTENT", oracleDbType: OracleDbType.Blob, obj: media.FileContent, direction: System.Data.ParameterDirection.Input);
                    mediaParams.Add(name: "p_NAME", oracleDbType: OracleDbType.Varchar2, obj: media.Name, direction: System.Data.ParameterDirection.Input);
                    mediaParams.Add(name: "P_FILENAME", oracleDbType: OracleDbType.Varchar2, obj: media.FileName, direction: System.Data.ParameterDirection.Input);
                    mediaParams.Add(name: "v_ID", oracleDbType: OracleDbType.Decimal, direction: System.Data.ParameterDirection.ReturnValue);
                    #endregion

                    if (!new GenericRepository<T>().CallSp("SF_MEDIA_UPLOAD", ref mediaParams))
                    {
                        mediaResponse.IsSuccess = false;
                        mediaResponse.Message = "Media objesi oluşturulamadı!";
                        return mediaResponse;
                    }
                    var v_ID = mediaParams.oracleParameters.Where(p => p.ParameterName == "v_ID").FirstOrDefault();
                    if (v_ID != null)
                    {
                        input.MediaId = long.Parse(v_ID.Value.ToString());
                        media.Id = input.MediaId;
                    }
                    else
                    {
                        mediaResponse.IsSuccess = false;
                        mediaResponse.Message = "Media objesi oluşturulamadı!";
                        return mediaResponse;
                    }
                }
                else
                {
                    SpResponse spResponseMedia = new GenericRepository<Protein.Common.Entities.ProteinEntities.Media>().Insert(media);
                    if (spResponseMedia.Code == "100")
                        input.MediaId = spResponseMedia.PkId;
                }

                mediaProperty.SetValue(instance, input.MediaId);
                statusProperty.SetValue(instance, ((int)ProteinEnums.Status.AKTIF).ToString());
                objectIdProperty.SetValue(instance, input.ObjectId);
                baseIdProperty.SetValue(instance, input.BaseId);

                SpResponse spResponseRelObject = new GenericRepository<T>().Insert(instance);
                if (spResponseRelObject.Code == "100")
                {
                    mediaResponse.Message = "Başarılı kayıt";
                    mediaResponse.IsSuccess = true;
                }

            }
            catch (Exception ex) { mediaResponse.IsSuccess = false; mediaResponse.Message = ex.ToString(); }
            return mediaResponse;
        }
        public MediaResponse Clone(MediaDto input)
        {
            MediaResponse mediaResponse = new MediaResponse();

            T instance = Activator.CreateInstance<T>();

            var existMedia = new GenericRepository<ProteinEntities.Media>().FindById(input.MediaId);
            if (existMedia != null)
            {
                PropertyInfo mediaProperty = PropHelper.FindPropByColumnAttrName("MEDIA_ID", instance.GetType());
                PropertyInfo statusProperty = PropHelper.FindPropByColumnAttrName("STATUS", instance.GetType());
                PropertyInfo baseIdProperty = PropHelper.FindPropByColumnAttrName("ID", instance.GetType());
                string objectIdColName = ((instance.GetType().GetCustomAttributes(false)).FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.TableAttribute)) as System.ComponentModel.DataAnnotations.Schema.TableAttribute).Name.Split('_')[1].ToString().Replace("_", "") + "_ID";
                PropertyInfo objectIdProperty = PropHelper.FindPropByColumnAttrName(objectIdColName, instance.GetType());

                if (mediaProperty == null || statusProperty == null || objectIdProperty == null)
                {
                    mediaResponse.IsSuccess = false;
                    mediaResponse.Message = "Media objesi, Object veya status (kolonu) bulunamadı!";
                    return mediaResponse;
                }


                OracleDynamicParameters mediaParams = new OracleDynamicParameters();
                //DynamicParameters mediaParams2 = new DynamicParameters();
                mediaParams.Add(name: "p_FILE_CONTENT", oracleDbType: OracleDbType.Blob, obj: existMedia.FileContent, direction: System.Data.ParameterDirection.Input);
                mediaParams.Add(name: "p_NAME", oracleDbType: OracleDbType.Varchar2, obj: existMedia.Name, direction: System.Data.ParameterDirection.Input);
                mediaParams.Add(name: "P_FILENAME", oracleDbType: OracleDbType.Varchar2, obj: existMedia.FileName, direction: System.Data.ParameterDirection.Input);
                mediaParams.Add(name: "v_ID", oracleDbType: OracleDbType.Decimal, direction: System.Data.ParameterDirection.ReturnValue);

                if (!new GenericRepository<T>().CallSp("SF_MEDIA_UPLOAD", ref mediaParams))
                {
                    mediaResponse.IsSuccess = false;
                    mediaResponse.Message = "Media objesi oluşturulamadı!";
                    return mediaResponse;

                }
                var v_ID = mediaParams.oracleParameters.Where(p => p.ParameterName == "v_ID").FirstOrDefault();
                if (v_ID != null)
                {
                    input.MediaId = long.Parse(v_ID.Value.ToString());
                }
                else
                {
                    mediaResponse.IsSuccess = false;
                    mediaResponse.Message = "Media objesi oluşturulamadı!";
                    return mediaResponse;
                }

                mediaProperty.SetValue(instance, input.MediaId);
                statusProperty.SetValue(instance, ((int)ProteinEnums.Status.AKTIF).ToString());
                objectIdProperty.SetValue(instance, input.ObjectId);
                baseIdProperty.SetValue(instance, input.BaseId);

                SpResponse spResponseRelObject = new GenericRepository<T>().Insert(instance);
                if (spResponseRelObject.Code == "100")
                {
                    mediaResponse.Message = "Başarılı kayıt";
                    mediaResponse.IsSuccess = true;
                }

            }

            return mediaResponse;
        }
    }
}
