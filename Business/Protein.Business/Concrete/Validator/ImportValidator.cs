﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Protein.Business.Concrete.Responses;
using Protein.Business.Enums.Import;
using Protein.Common.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Business.Concrete.Validator
{
    public class ImportValidator<T> where T : class, IImportObject, new()
    {
        public ImportResponse CheckValidFile(T input)
        {
            ImportResponse response = new ImportResponse();

            try
            {
                IDictionary<PropertyInfo, bool> PropList = FillProps(typeof(T).GetProperties());

                //foreach (var item in input.GetType().GetProperties())
                //{
                //    PropertyInfo matchCol = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance)
                //                            .FirstOrDefault(x => x.GetCustomAttribute<ColumnAttribute>() != null
                //                            && x.GetCustomAttribute<ColumnAttribute>().Name.Equals(item.GetValue(input, null).ToString(),                   StringComparison.OrdinalIgnoreCase));
                //}
            }
            catch { }
            return response;
        }
        public ImportResponse CheckValidFile(Stream stream, ImportFileType fileType)
        {
            ImportResponse response = new ImportResponse();
            try
            {
                stream.Position = 0;
                //stream.Seek(0, SeekOrigin.Begin);
                if (stream == null) { response.Message = "Dosyayı kontrol edin"; return response; }
                ISheet sheet;
                if (fileType == ImportFileType.Xlsx)
                {
                    XSSFWorkbook hssfwb = new XSSFWorkbook(stream);
                    sheet = hssfwb.GetSheetAt(0);
                }
                else
                {
                    HSSFWorkbook hssfwb = new HSSFWorkbook(stream);
                    sheet = hssfwb.GetSheetAt(0);
                }

                IRow headerRow = sheet.GetRow(0);
                int cellCount = headerRow.LastCellNum;

                IDictionary<PropertyInfo, bool> PropList = FillProps(typeof(T).GetProperties());

                for (int j = 0; j < cellCount; j++)
                {
                    ICell cell = headerRow.GetCell(j);

                    PropertyInfo matchCol = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                     .FirstOrDefault(x => x.GetCustomAttribute<ColumnAttribute>() != null
                                        && x.GetCustomAttribute<ColumnAttribute>().Name.Equals(cell.StringCellValue.Trim(), StringComparison.OrdinalIgnoreCase));
                    //PropertyInfo matchCol = typeof(T).GetProperties().Where(x => x.Name == cell.StringCellValue.Trim()).FirstOrDefault();
                    if (matchCol == null)
                    {
                        response.Message += "Eşleşemeyen kolon: <b>'" + cell.StringCellValue + "'</b> lütfen kontrol ediniz <br>";
                        response.IsSuccess = false; 
                    }
                    else
                    {
                        response.IsSuccess = true;
                        PropList[matchCol] = true;
                    }
                }
                response.Message += RemainingPropAnalyze(PropList);
                response.IsSuccess = PropList.Where(x => x.Value == false && x.Key.GetCustomAttribute<RequiredAttribute>(true) != null).Any() ? false : true;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }
            finally { stream.Dispose(); }
            return response;
        }
        private string RemainingPropAnalyze(IDictionary<PropertyInfo, bool> Props)
        {
            string errorMsg = "";
            foreach (KeyValuePair<PropertyInfo, bool> item in Props)
            {
                try
                {
                    if (!item.Value && item.Key.GetCustomAttribute<RequiredAttribute>(true) != null)
                        errorMsg += "<b>" + item.Key.GetCustomAttribute<ColumnAttribute>(true).Name + "</b> kolonu kaynak dosyada bulunamadı! <br>";
                }
                catch (Exception ex) { }
            }
            return errorMsg;
        }
        private IDictionary<PropertyInfo, bool> FillProps(PropertyInfo[] obj)
        {
            IDictionary<PropertyInfo, bool> propResult = new Dictionary<PropertyInfo, bool>();
            if (obj != null)
            {
                if (obj.Count() > 0)
                {
                    foreach (PropertyInfo prop in obj)
                    {
                        propResult.Add(prop, false);
                    }
                }
            }
            return propResult;
        }
    }
}
