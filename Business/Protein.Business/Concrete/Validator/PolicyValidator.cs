﻿using Dapper;
using Protein.Common.Attributes;
using Protein.Common.Constants;
using Protein.Common.Dto.ImportObjects;
using Protein.Common.Enums;
using Protein.Common.Extensions;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Business.Concrete.Validator
{
    public class PolicyValidator
    {
        PolicyDto policy;
        public PolicyValidator(PolicyDto policy)
        {
            this.policy = policy;
        }
        public PolicyValidatorResponse PrintOperation()
        {
            PolicyValidatorResponse response = new PolicyValidatorResponse();
            response.ErrorCode = "100";
            if (string.IsNullOrEmpty(this.policy.PolicyNo))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no bulunamadı";
                return response;
            }

            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else if (this.policy.EndorsementType != "PRINT")
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türünü PRINT YAZMALISINIZ";
                return response;
            }
            if (this.policy.AgencyId == null)
            {
                response.ErrorCode = "999";
                response.Description = "Acente no bulunamadı";
                return response;
            }
            if (this.policy.CompanyId == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Şirket bilgisi bulunamadı";
                return response;
            }
            if (this.policy.CompanyId > 0 && !string.IsNullOrEmpty(this.policy.AgencyId))
            {
                var agency = new GenericRepository<V_Agency>().FindBy("COMPANY_ID =:CompanyId and AGENCY_NO =:agencyNo", parameters: new { CompanyId = policy.CompanyId, agencyNo = new DbString { Value = policy.AgencyId, Length = 20 } }, orderby: "");
                if (agency.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Acente şirket eşleşmesi hatalı";
                    return response;
                }
            }
            if (this.policy.RenewalNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Yenileme no bulunamadı";
                return response;
            }
            var PolicyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });
            if (PolicyExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no sistemde bulunamadı.Basım için için önce poliçe oluşturmalısınız!";
                return response;
            }
            this.policy.PolicyId = (long)PolicyExists[0].POLICY_ID;
            this.policy.EndorsementId = (long)PolicyExists[0].LastEndorsementId;
            this.policy.PolicyType = PolicyExists[0].POLICY_TYPE;

            return response;
        }
        public PolicyValidatorResponse InstallmentOperation()
        {
            PolicyValidatorResponse response = new PolicyValidatorResponse();
            response.ErrorCode = "100";

            if (string.IsNullOrEmpty(this.policy.PolicyNo))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no bulunamadı";
                return response;
            }
            else
            {
                this.policy.PolicyNo = this.policy.PolicyNo;

                if (string.IsNullOrEmpty(this.policy.PolicyNo))
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçe no bulunamadı";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else if (this.policy.EndorsementType != "TAKSIT")
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türünü TAKSIT YAZMALISINIZ";
                return response;
            }
            if (this.policy.AgencyId == null)
            {
                response.ErrorCode = "999";
                response.Description = "Acente no bulunamadı";
                return response;
            }
            if (this.policy.CompanyId == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Şirket bilgisi bulunamadı";
                return response;
            }
            if (this.policy.CompanyId > 0 && string.IsNullOrEmpty(this.policy.AgencyId))
            {
                var agency = new GenericRepository<V_Agency>().FindBy("COMPANY_ID =:CompanyId and AGENCY_NO =:agencyNo", parameters: new { CompanyId = policy.CompanyId, agencyNo = new DbString { Value = policy.AgencyId, Length = 20 } }, orderby: "");
                if (agency.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Acente şirket eşleşmesi hatalı";
                    return response;
                }
            }
            if (this.policy.RenewalNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Yenileme no bulunamadı";
                return response;
            }
            if (this.policy.InstallmentAmount == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Taksit  tutarı 0'dan büyük olmalı";
                return response;
            }
            if (this.policy.InstallmentDueDate == Convert.ToDateTime("1.01.0001 00:00:00"))
            {
                response.ErrorCode = "999";
                response.Description = "Taksit son ödeme tarihi giriniz!";
                return response;
            }

            var PolicyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS =:status", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, status = new DbString { Value = ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), Length = 3 } });
            if (PolicyExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no sistemde bulunamadı.Taksit degisikligi için önce poliçe oluşturmalısınız!";
                return response;
            }
            this.policy.PolicyId = (long)PolicyExists[0].POLICY_ID;
            //response = EndorsementIssueCheck(this.policy.PolicyId);
            return response;
        }
        public PolicyValidatorResponse InsuredExit()
        {
            Thread.Sleep(100);
            PolicyValidatorResponse response = new PolicyValidatorResponse();
            response.ErrorCode = "100";

            #region Basic valid

            if (string.IsNullOrEmpty(this.policy.PolicyStatus))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe / Zeyl Durumunu belirtmelisiniz! ( 0 = TEKLIF, 2 = TANZIM)";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.PolicyStatus>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.PolicyStatus)
                    {
                        if (enVal.Text == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString()
                          || enVal.Text == ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString())
                            findHim = true;
                        break;
                    }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz poliçe status türü";
                    return response;
                }
            }
            if (this.policy.PolicyStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
            {
                if (this.policy.DateOfIssue == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Tanzim tarihi bulunamadı";
                    return response;
                }
            }

            //if (this.policy.DateOfIssue == null)
            //{
            //    response.ErrorCode = "999";
            //    response.Description = "Tanzim tarihi bulunamadı";
            //    return response;
            //}
            if (string.IsNullOrEmpty(this.policy.PolicyNo))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no bulunamadı";
                return response;
            }
            else
            {
                this.policy.PolicyNo = this.policy.PolicyNo;

                if (string.IsNullOrEmpty(this.policy.PolicyNo))
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçe no bulunamadı";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.EndorsementType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl türü";
                    return response;
                }
            }
            if (this.policy.AgencyId == null)
            {
                response.ErrorCode = "999";
                response.Description = "Acente no bulunamadı";
                return response;
            }
            if (this.policy.CompanyId == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Şirket bilgisi bulunamadı";
                return response;
            }
            if (this.policy.CompanyId > 0 && string.IsNullOrEmpty(this.policy.AgencyId))
            {
                var agency = new GenericRepository<V_Agency>().FindBy("COMPANY_ID =:CompanyId and AGENCY_NO =:agencyNo", parameters: new { CompanyId = policy.CompanyId, agencyNo = new DbString { Value = policy.AgencyId, Length = 20 } }, orderby: "");
                if (agency.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Acente şirket eşleşmesi hatalı";
                    return response;
                }
            }
            if (this.policy.RenewalNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Yenileme no bulunamadı";
                return response;
            }
            if (this.policy.InsuredFamilyNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Sigorta aile numarası bulunamadı";
                return response;
            }
            if (this.policy.InsuredInitialPremium == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı prim bilgisi bulunamadı";
                return response;
            }
            if (this.policy.InsuredTcNo == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı TcNo bulunamadı";
                return response;
            }
            else
            {
                if (this.policy.InsuredTcNo.IsIdentityNoLength()==null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigortalı TcNo 11 karakter olmak zorunda";
                    return response;
                }
            }
            if (this.policy.InsuredIndividualType.IsNull())
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı yakınlık bulunamadı ";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.IndividualType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.InsuredIndividualType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz yakınlık türü";
                    return response;
                }
            }
            if (this.policy.EndorsementNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl no 0 olmamalı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                string zeylCode = EnumHelper.GetValues(ProteinEnums.EndorsementType.SIGORTALI_CIKISI)[1];
                if (this.policy.EndorsementType != zeylCode)
                {
                    response.ErrorCode = "999";
                    response.Description = "Zeyl türü hatalı. Sigortalı çıkış zeyli olmalı";
                    return response;
                }
            }
            if (this.policy.EndorsementStartDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl başlama tarihi bulunamadı";
                return response;
            }
            #endregion

            #region Logic Valid

            var PolicyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });

            if (PolicyExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no sistemde bulunamadı.Sigortali çıkış için önce poliçe oluşturmalısınız!";
                return response;
            }
            this.policy.PolicyId = (long)PolicyExists[0].POLICY_ID;

            if (!response.IsSucces) return response;

            //if (this.policy.InsuredIndividualType == "F")
            //{
            var InsuredExists = new GenericRepository<V_InsuredEndorsement>().FindBy("POLICY_ID =:policyId and IDENTITY_NO =:tckn and FAMILY_NO =:familyNo", orderby: "POLICY_ID DESC,ENDORSEMENT_ID DESC,ENDORSEMENT_NO DESC", parameters: new { policyId = PolicyExists[0].POLICY_ID, tckn = policy.InsuredTcNo, familyNo = policy.InsuredFamilyNo });
            if (InsuredExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Bu birey bu poliçede bulunamadı!";
                return response;
            }
            //this.policy.EndorsementId = InsuredExists[0].EndorsementId;

            this.policy.InsuredId = InsuredExists[0].INSURED_ID;

            var EndorsementExistNonFamilyNo = new GenericRepository<V_PolicyEndorsement>().FindBy(conditions: $"POLICY_ID =:policyId", parameters: new { policyId = PolicyExists[0].POLICY_ID }, orderby: "POLICY_ID DESC,ENDORSEMENT_ID DESC,ENDORSEMENT_NO DESC");
            if (EndorsementExistNonFamilyNo.Count > 0)
            {
                if (this.policy.EndorsementNo == EndorsementExistNonFamilyNo[0].ENDORSEMENT_NO &&
                    EndorsementExistNonFamilyNo[0].ENDORSEMENT_TYPE == ((int)ProteinEnums.EndorsementType.SIGORTALI_CIKISI).ToString())
                    this.policy.EndorsementId = EndorsementExistNonFamilyNo[0].ENDORSEMENT_ID;
            }
            if (this.policy.EndorsementId == 0)
                response = EndorsementIssueCheck(this.policy.PolicyId);
            //    if (this.policy.EndorsementNo == InsuredExistNonFamilyNo[0].EndorsementNo && InsuredExistNonFamilyNo[0].EndorsementType == ((int)ProteinEnums.EndorsementType.SIGORTAL_GIRISI).ToString())
            //        this.policy.EndorsementId = InsuredExistNonFamilyNo[0].EndorsementId;
            //}
            //var InsuredExistNonFamilyNo = new GenericRepository<V_InsuredEndorsement>().FindBy(conditions: $"COMPANY_ID = {this.policy.CompanyId} and RENEWAL_NO = '{this.policy.RenewalNo}' and POLICY_NUMBER = '{this.policy.PolicyNo}' and INDIVIDUAL_TYPE ='{ LookupHelper.GetLookupByCode(Constants.LookupTypes.Individual, this.policy.InsuredIndividualType).Ordinal }' ", orderby: "POLICY_ID DESC,ENDORSEMENT_ID DESC,ENDORSEMENT_NO DESC");
            //if (InsuredExistNonFamilyNo.Count > 0)
            //{
            //    this.policy.PolicyId = (long)InsuredExistNonFamilyNo[0].PolicyID;
            //    if (this.policy.EndorsementNo == InsuredExistNonFamilyNo[0].EndorsementNo && InsuredExistNonFamilyNo[0].EndorsementType == ((int)ProteinEnums.EndorsementType.SIGORTAL_GIRISI).ToString())
            //        this.policy.EndorsementId = InsuredExistNonFamilyNo[0].EndorsementId;
            //}
            response = PolicyIsIssued(this.policy.PolicyId);
            if (!response.IsSucces) return response;
            #endregion

            #region Mapping
            this.policy.EndorsementType = ((int)ProteinEnums.EndorsementType.SIGORTALI_CIKISI).ToString();
            this.policy.EndorsementStartDate = this.policy.StartDate;
            #endregion

            return response;
        }
        public PolicyValidatorResponse PolicyCancellation()
        {
            PolicyValidatorResponse response = new PolicyValidatorResponse();
            response.ErrorCode = "100";
            #region Basic valid

            if (string.IsNullOrEmpty(this.policy.PolicyStatus))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe / Zeyl Durumunu belirtmelisiniz! ( 0 = TEKLIF, 2 = TANZIM)";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.PolicyStatus>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.PolicyStatus)
                    {
                        if (enVal.Text == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString()
                          || enVal.Text == ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString())
                            findHim = true;
                        break;
                    }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz poliçe status türü";
                    return response;
                }
            }
            //if (this.policy.PolicyStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
            //{
            //this.policy.EndorsementStatus = ((int)ProteinEnums.Status.AKTIF).ToString();
            if (this.policy.DateOfIssue == null)
            {
                response.ErrorCode = "999";
                response.Description = "Tanzim tarihi bulunamadı";
                return response;
            }
            //}
            //else
            //    this.policy.EndorsementStatus = ((int)ProteinEnums.Status.PASIF).ToString();
            if (string.IsNullOrEmpty(this.policy.PolicyNo))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no bulunamadı";
                return response;
            }
            else
            {
                this.policy.PolicyNo = this.policy.PolicyNo;

                if (string.IsNullOrEmpty(this.policy.PolicyNo))
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçe no bulunamadı";
                    return response;
                }
            }

            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.EndorsementType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl türü";
                    return response;
                }
            }
            if (this.policy.AgencyId == null)
            {
                response.ErrorCode = "999";
                response.Description = "Acente no bulunamadı";
                return response;
            }
            if (this.policy.CompanyId == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Şirket bilgisi bulunamadı";
                return response;
            }
            if (this.policy.CompanyId > 0 && !string.IsNullOrEmpty(this.policy.AgencyId))
            {
                var agency = new GenericRepository<V_Agency>().FindBy("COMPANY_ID =:CompanyId and AGENCY_NO =:agencyNo", parameters: new { CompanyId = policy.CompanyId, agencyNo = new DbString { Value = policy.AgencyId, Length = 20 } }, orderby: "");
                if (agency.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Acente şirket eşleşmesi hatalı";
                    return response;
                }
            }
            //if (this.policy.RenewalNo == 0)
            //{
            //    response.ErrorCode = "999";
            //    response.Description = "Yenileme no bulunamadı";
            //    return response;
            //}
            
            if (this.policy.EndorsementNo ==0)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl no 0  olmamalı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                string zeylCode = EnumHelper.GetValues(ProteinEnums.EndorsementType.IPTAL)[1];
                if (this.policy.EndorsementType != zeylCode)
                {
                    response.ErrorCode = "999";
                    response.Description = "Zeyl türü hatalı. İptal zeyli olmalı";
                    return response;
                }
            }
            if (this.policy.EndorsementStartDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl başlama tarihi bulunamadı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementCategory))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl kategorisi bulunamadı 0 = POLİÇE İÇİN , 1 = SİGORTALI İÇİN";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementCategory>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.EndorsementCategory)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl kategorisi ";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementTypeDescriptionType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl Tipinin türü bulunamadı. 0 = POLICE BASLANGICI, 1 = GUN ESASLI ";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementTypeDescriptionType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.EndorsementTypeDescriptionType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl tipinin türü";
                    return response;
                }
            }
            #endregion
            #region Logic valid

            var PolicyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });

            if (PolicyExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no sistemde bulunamadı. Önce poliçe oluşturmalısınız!";
                return response;
            }
            this.policy.PolicyId = (long)PolicyExists[0].POLICY_ID;

            if (!response.IsSucces) return response;
            if (this.policy.PolicyId < 1)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no sistemde bulunamadı.";
                return response;
            }

            var EndorsementExists = new GenericRepository<V_PolicyEndorsement>().FindBy(" POLICY_ID =:PolicyId  and POLICY_STATUS !=:polStatus", parameters: new { policy.PolicyId, polStatus = new DbString { Value = ((int)ProteinEnums.PolicyStatus.SILINDI).ToString(), Length = 3 } }, orderby: "");
            if (EndorsementExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Bu poliçenin mevcut zeyl bilgisi bulunamadı.";
                return response;
            }

            foreach (V_PolicyEndorsement policyEndors in EndorsementExists)
            {
                if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo)
                {
                    response.ErrorCode = "999";
                    response.Description = "Aynı zeyil no hatası";
                    return response;
                }
                else if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo)
                    this.policy.EndorsementId = policyEndors.ENDORSEMENT_ID;
            }
            response = PolicyIsIssued(this.policy.PolicyId);
            if (!response.IsSucces) return response;
            #endregion

            #region Mapping
            this.policy.EndorsementType = ((int)ProteinEnums.EndorsementType.IPTAL).ToString();
            this.policy.PolicyStatus = ((int)ProteinEnums.PolicyStatus.IPTAL).ToString();
            this.policy.EndorsementStartDate = this.policy.StartDate;
            //List<EnumValue> enumValues = EnumHelper.GetValues<ProteinEnums.IndividualType>();

            #endregion
            return response;
        }
        public PolicyValidatorResponse InsuredEntry()
        {
            Thread.Sleep(300);
            PolicyValidatorResponse response = new PolicyValidatorResponse();
            response.ErrorCode = "100";
            #region Basic Valid
            if (!string.IsNullOrEmpty(this.policy.InsuredTransferType))
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.InsuredTransferType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.InsuredTransferType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz sigortali transfer türü";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.PolicyStatus))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe / Zeyl Durumunu belirtmelisiniz! ( 0 = TEKLIF, 2 = TANZIM)";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.PolicyStatus>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.PolicyStatus)
                    {
                        if (enVal.Text == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString()
                          || enVal.Text == ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString())
                            findHim = true;
                        break;
                    }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz poliçe status türü";
                    return response;
                }
            }
            if (this.policy.PolicyStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
            {
                if (this.policy.DateOfIssue == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Tanzim tarihi bulunamadı";
                    return response;
                }
            }

            if (string.IsNullOrEmpty(this.policy.PolicyNo))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no bulunamadı";
                return response;
            }
            else
            {
                this.policy.PolicyNo = this.policy.PolicyNo;

                if (string.IsNullOrEmpty(this.policy.PolicyNo))
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçe no bulunamadı";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.EndorsementType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl türü";
                    return response;
                }
            }
            //if (this.policy.AgencyId == null)
            //{
            //    response.ErrorCode = "999";
            //    response.Description = "Acente no bulunamadı";
            //    return response;
            //}
            if (this.policy.CompanyId == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Şirket bilgisi bulunamadı";
                return response;
            }
            if (this.policy.CompanyId > 0 && string.IsNullOrEmpty(this.policy.AgencyId))
            {
                var agency = new GenericRepository<V_Agency>().FindBy("COMPANY_ID =:CompanyId and AGENCY_NO =:agencyNo", parameters: new { CompanyId = policy.CompanyId, agencyNo = new DbString { Value = policy.AgencyId, Length = 20 } }, orderby: "");
                if (agency.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Acente şirket eşleşmesi hatalı";
                    return response;
                }
            }
            if (this.policy.RenewalNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Yenileme no bulunamadı";
                return response;
            }
            if (this.policy.InsuredFamilyNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Sigorta aile numarası bulunamadı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.InsuredName))
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı isim kodu bulunamadı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.InsuredLastName))
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı soy isim kodu bulunamadı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.InsuredGender))
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı cinsiyet bulunamadı";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.Gender>();

                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.InsuredGender)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigortalı cinsiyet hatalı";
                    return response;
                }
            }
            if (this.policy.InsuredBirthDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı doğum tarihi bulunamadı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.InsuredNameOfFather))
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı baba adı bulunamadı";
                return response;
            }
            if (this.policy.InsuredCompanyEntranceDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı şirkete ilk giriş tarihi bulunamadı";
                return response;
            }
            if (this.policy.InsuredPackageId == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı plan kodu bulunamadı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.InsuredIndividualType))
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı yakınlık bulunamadı ";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.IndividualType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.InsuredIndividualType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz yakınlık türü";
                    return response;
                }
            }
            if (this.policy.InsuredInitialPremium == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı prim bilgisi bulunamadı";
                return response;
            }

            if (this.policy.InsuredPolicyEntryDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı poliçeye giriş tarihi bulunamadı";
                return response;
            }
            if (this.policy.InsuredTcNo == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı TcNo bulunamadı";
                return response;
            }
            else
            {
                if (this.policy.InsuredTcNo.IsIdentityNoLength()==null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigortalı TcNo 11 karakter olmak zorunda";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.InsuredNationality))
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı uyruk bulunamadı";
                return response;
            }
            else
            {
                if (this.policy.InsuredNationality.ToUpper() != "TRK")
                {
                    List<Country> lstCountry = new GenericRepository<Country>().FindBy("A2_CODE =:code", parameters: new { code = new DbString { Value = policy.InsuredNationality, Length = 3 } });
                    if (lstCountry.Count < 1)
                    {
                        response.ErrorCode = "999";
                        response.Description = "Sigortalı uyruk bulunamadı";
                        return response;
                    }
                }
            }
            if (!string.IsNullOrEmpty(this.policy.InsuredMaritalStatus))
            {
                if (this.policy.InsuredMaritalStatus == "B")
                { }
                else if (this.policy.InsuredMaritalStatus == "E") { }
                else
                {
                    response.ErrorCode = "999";
                    response.Description = "Medeni durum format hatası (E veya B)";
                    return response;
                }
            }
            //if (this.policy.InsuredFirstInsuredDate == null)
            //{
            //    response.ErrorCode = "999";
            //    response.Description = "Sigortalı ilk sigortalanma tarihi bilgisi bulunamadı!";
            //    return response;
            //}
            if (string.IsNullOrEmpty(this.policy.InsuredAddress))
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı adres bilgisi bulunamadı!";
                return response;
            }
            if (this.policy.InsuredCityCode == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı il kodu bulunamadı";
                return response;
            }
            else
            {
                string tmpCityCode = "";

                for (int s1 = 3; s1 > this.policy.InsuredCityCode.ToString().Length; s1--)
                    tmpCityCode += "0";
                tmpCityCode += this.policy.InsuredCityCode.ToString();

                List<City> lstCity = new GenericRepository<City>().FindBy($"CODE =:code", parameters: new { code = new DbString { Value = tmpCityCode, Length = 3 } });
                if (lstCity.Count < 1)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigortalı il kodu bulunamadı";
                    return response;
                }
            }
            if (this.policy.EndorsementNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl no 0 olmamalı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                string zeylCode = EnumHelper.GetValues(ProteinEnums.EndorsementType.SIGORTALI_GIRISI)[1];
                if (this.policy.EndorsementType != zeylCode)
                {
                    response.ErrorCode = "999";
                    response.Description = "Zeyl türü hatalı. Sigortalı giriş zeyli olmalı";
                    return response;
                }
            }
            if (this.policy.EndorsementStartDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl başlama tarihi bulunamadı";
                return response;
            }
            #endregion
            #region Logic Valid
            var PolicyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });

            if (this.policy.InsuredIndividualType != "F")
            {
                Thread.Sleep(100);
                //var PolicyExists = new GenericRepository<V_Policy>().FindBy($"RENEWAL_NO = '{this.policy.RenewalNo}' AND POLICY_NUMBER = '{this.policy.PolicyNo}' and COMPANY_ID = {this.policy.CompanyId} and STATUS IN ('{((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString()}','{((int)ProteinEnums.PolicyStatus.TEKLIF).ToString()}')", orderby: "POLICY_ID DESC");

                if (PolicyExists.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçe no sistemde bulunamadı. Önce poliçe oluşturmalısınız!";
                    return response;
                }
                this.policy.PolicyId = (long)PolicyExists[0].POLICY_ID;
            }
            if (this.policy.InsuredIndividualType == "F")
            {
                Thread.Sleep(100);

                if (PolicyExists.Count > 0)
                {
                    var InsuredExists = new GenericRepository<V_InsuredEndorsement>().FindBy("POLICY_ID =:policyId and INDIVIDUAL_TYPE =:indType and FAMILY_NO =:familyNo ", orderby: "POLICY_ID DESC,ENDORSEMENT_ID DESC,ENDORSEMENT_NO DESC", parameters: new { policyId = PolicyExists[0].POLICY_ID, indType = new DbString { Value = LookupHelper.GetLookupByCode(Constants.LookupTypes.Individual, this.policy.InsuredIndividualType).Ordinal, Length = 3 }, familyNo = policy.InsuredFamilyNo });
                    if (InsuredExists.Count > 0)
                    {
                        response.ErrorCode = "999";
                        response.Description = "Poliçede aynı ailede sadece bir fert olabilir";
                        return response;
                    }

                    Thread.Sleep(100);
                    var InsuredExistNonFamilyNo = new GenericRepository<V_InsuredEndorsement>().FindBy("POLICY_ID =:policyId and INDIVIDUAL_TYPE =:indType", orderby: "POLICY_ID DESC,ENDORSEMENT_ID DESC,ENDORSEMENT_NO DESC", parameters: new { policyId = PolicyExists[0].POLICY_ID, indType = new DbString { Value = LookupHelper.GetLookupByCode(Constants.LookupTypes.Individual, this.policy.InsuredIndividualType).Ordinal, Length = 3 } });
                    if (InsuredExistNonFamilyNo.Count > 0 && this.policy.PolicyType == ((int)ProteinEnums.PolicyType.GRUP).ToString())
                    {
                        this.policy.PolicyId = (long)InsuredExistNonFamilyNo[0].POLICY_ID;
                        if (this.policy.EndorsementNo == InsuredExistNonFamilyNo[0].ENDORSEMENT_NO && InsuredExistNonFamilyNo[0].ENDORSEMENT_TYPE == ((int)ProteinEnums.EndorsementType.SIGORTALI_GIRISI).ToString())
                            this.policy.EndorsementId = InsuredExistNonFamilyNo[0].ENDORSEMENT_ID;
                    }
                    else if (this.policy.PolicyType == ((int)ProteinEnums.PolicyType.FERDI).ToString())
                    {
                        //var policyExists = new GenericRepository<V_Policy>().FindBy($" RENEWAL_NO = '{(this.policy.RenewalNo).ToString()}' and POLICY_NUMBER = '{this.policy.PolicyNo}' and STATUS IN ('{((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString()}','{((int)ProteinEnums.PolicyStatus.TEKLIF).ToString()}') and COMPANY_ID = {this.policy.CompanyId}", orderby: "");
                        //if (policyExists.Count > 0)
                        //{
                        response.ErrorCode = "999";
                        response.Description = "Ferdi poliçede sadece 1 aile olabilir";
                        return response;
                        //}
                    }
                }
            }
            else if (this.policy.InsuredIndividualType == "A" || this.policy.InsuredIndividualType == "B" || this.policy.InsuredIndividualType == "E")
            {
                Thread.Sleep(100);
                //Fert kontrolü
                var InsuredExists = new GenericRepository<V_InsuredEndorsement>().FindBy("POLICY_ID =:policyId and INDIVIDUAL_TYPE =:indType and FAMILY_NO =:familyNo ", orderby: "POLICY_ID DESC,ENDORSEMENT_ID DESC,ENDORSEMENT_NO DESC", parameters: new { policyId = PolicyExists[0].POLICY_ID, indType = new DbString { Value = ((int)ProteinEnums.IndividualType.FERT).ToString(), Length = 3 }, familyNo = policy.InsuredFamilyNo });
                Thread.Sleep(100);
                //eş,cocuk,baba,anne kontrolü
                var InsuredExistsIndividual = new GenericRepository<V_InsuredEndorsement>().FindBy("POLICY_ID =:policyId and INDIVIDUAL_TYPE =:indType and FAMILY_NO =:familyNo ", orderby: "POLICY_ID DESC,ENDORSEMENT_ID DESC,ENDORSEMENT_NO DESC", parameters: new { policyId = PolicyExists[0].POLICY_ID, indType = new DbString { Value = LookupHelper.GetLookupByCode(Constants.LookupTypes.Individual, this.policy.InsuredIndividualType).Ordinal, Length = 3 }, familyNo = policy.InsuredFamilyNo });
                if (InsuredExists.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçeye eş, çocuk, anne,baba eklemek için önce fert eklemelisiniz!";
                    return response;
                }
                else if (InsuredExistsIndividual.Count > 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçede aynı ailede sadece bir adet anne,baba,eş içerebilir";
                    return response;
                }
                if (InsuredExists[0].ENDORSEMENT_NO == this.policy.EndorsementNo)
                    this.policy.EndorsementId = InsuredExists[0].ENDORSEMENT_ID;
                //this.policy.EndorsementNo = InsuredExists[0].EndorsementNo;
            }
            else if (this.policy.InsuredIndividualType == "C" || this.policy.InsuredIndividualType == "D")
            {
                Thread.Sleep(100);
                var InsuredExists = new GenericRepository<V_InsuredEndorsement>().FindBy("POLICY_ID =:policyId and INDIVIDUAL_TYPE =:indType and FAMILY_NO =:familyNo ", orderby: "POLICY_ID DESC,ENDORSEMENT_ID DESC,ENDORSEMENT_NO DESC", parameters: new { policyId = PolicyExists[0].POLICY_ID, indType = new DbString { Value = ((int)ProteinEnums.IndividualType.FERT).ToString(), Length = 3 }, familyNo = policy.InsuredFamilyNo });

                if (InsuredExists.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçeye çocuk veya diğer için önce fert eklemelisiniz!";
                    return response;
                }
                if (InsuredExists[0].ENDORSEMENT_NO == this.policy.EndorsementNo)
                    this.policy.EndorsementId = InsuredExists[0].ENDORSEMENT_ID;

                //this.policy.EndorsementNo = InsuredExists[0].EndorsementNo;
            }
            if (this.policy.RenewalNo > 1)
            {
                Thread.Sleep(100);
                var policyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });
                if (policyExists.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Bu yenileme poliçesinin kaydı bulunamadı";
                    return response;
                }
            }

            List<V_Package> lstpck = new GenericRepository<V_Package>().FindBy("PACKAGE_NO =:packageNo AND COMPANY_ID=:CompanyId", parameters: new { packageNo = new DbString { Value = policy.InsuredPackageId.ToString(), Length = 100 }, policy.CompanyId }, orderby: "");
            if (lstpck.Count < 1)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı plan kodu hatalı";
                return response;
            }
            else
            {
                if (lstpck.FirstOrDefault().SUBPRODUCT_ID == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Subproduct kodu bulunamadı.Plan kodunu kontrol ediniz!";
                    return response;
                }

                else
                {
                    this.policy.SubProductId = lstpck.FirstOrDefault().SUBPRODUCT_ID;
                    this.policy.InsuredPackageId = lstpck.FirstOrDefault().PACKAGE_ID;
                }
            }
            #endregion
            if (this.policy.PolicyId < 1)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe kaydı bulunamadı!";
                return response;
            }
            var EndorsementExists = new GenericRepository<V_PolicyEndorsement>().FindBy("POLICY_ID =:policyId", parameters: new { policyId = PolicyExists[0].POLICY_ID }, orderby: "");
            if (EndorsementExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Bu poliçenin mevcut zeyl bilgisi bulunamadı.";
                return response;
            }
            foreach (V_PolicyEndorsement policyEndors in EndorsementExists)
            {
                if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo &&
                    policyEndors.ENDORSEMENT_TYPE != ((int)ProteinEnums.EndorsementType.SIGORTALI_GIRISI).ToString())
                {
                    response.ErrorCode = "999";
                    response.Description = "Aynı zeyil no hatası";
                    return response;
                }
                else if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo)
                    this.policy.EndorsementId = policyEndors.ENDORSEMENT_ID;
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementCategory))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl kategorisi bulunamadı 0 = POLİÇE İÇİN , 1 = SİGORTALI İÇİN";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementCategory>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.EndorsementCategory)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl kategorisi ";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementTypeDescriptionType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl Tipinin türü bulunamadı. 0 = POLICE BASLANGICI, 1 = GUN ESASLI ";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementTypeDescriptionType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.EndorsementTypeDescriptionType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl tipinin türü";
                    return response;
                }
            }
            response = PolicyIsIssued(this.policy.PolicyId);
            if (!response.IsSucces) return response;
            if (this.policy.EndorsementId == 0)
                response = EndorsementIssueCheck(this.policy.PolicyId);
            if (!response.IsSucces) return response;
            response = InsuredSameBranch(this.policy);
            if (!response.IsSucces) return response;

            #region Mapping
            this.policy.EndorsementType = ((int)ProteinEnums.EndorsementType.SIGORTALI_GIRISI).ToString();
            this.policy.EndorsementStartDate = this.policy.StartDate;
            #endregion

            return response;
        }
        public PolicyValidatorResponse InsuredChangeEndorsement()
        {
            PolicyValidatorResponse response = new PolicyValidatorResponse();
            response.ErrorCode = "100";
            #region Basic Valid

            if (string.IsNullOrEmpty(this.policy.PolicyStatus))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe / Zeyl Durumunu belirtmelisiniz! ( 0 = TEKLIF, 2 = TANZIM)";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.PolicyStatus>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.PolicyStatus)
                    {
                        if (enVal.Text == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString()
                          || enVal.Text == ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString())
                            findHim = true;
                        break;
                    }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz poliçe status türü";
                    return response;
                }
            }
            if (this.policy.PolicyStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
            {
                if (this.policy.DateOfIssue == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Tanzim tarihi bulunamadı";
                    return response;
                }
            }


            if (!string.IsNullOrEmpty(this.policy.InsuredTransferType))
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.InsuredTransferType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.InsuredTransferType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz sigortali transfer türü";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.PolicyNo))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no bulunamadı";
                return response;
            }
            else
            {
                this.policy.PolicyNo = this.policy.PolicyNo;

                if (string.IsNullOrEmpty(this.policy.PolicyNo))
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçe no bulunamadı";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.EndorsementType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl türü";
                    return response;
                }
            }
            if (this.policy.RenewalNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Yenileme no bulunamadı";
                return response;
            }
            if (this.policy.CompanyId == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Şirket bilgisi bulunamadı";
                return response;
            }
            if (this.policy.InsuredFamilyNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Sigorta aile numarası bulunamadı";
                return response;
            }

            if (string.IsNullOrEmpty(this.policy.InsuredName))
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı isim kodu bulunamadı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.InsuredLastName))
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı soy isim kodu bulunamadı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.InsuredGender))
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı cinsiyet bulunamadı";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.Gender>();

                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.InsuredGender)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigortalı cinsiyet hatalı";
                    return response;
                }
            }
            if (this.policy.InsuredBirthDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı doğum tarihi bulunamadı";
                return response;
            }
            //if (string.IsNullOrEmpty(this.policy.InsuredNameOfFather))
            //{
            //    response.ErrorCode = "999";
            //    response.Description = "Sigortalı baba adı bulunamadı";
            //    return response;
            //}
            //if (this.policy.InsuredCompanyEntranceDate == null)
            //{
            //    response.ErrorCode = "999";
            //    response.Description = "Sigortalı şirkete ilk giriş tarihi bulunamadı";
            //    return response;
            //}
            if (this.policy.InsuredPackageId == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı plan kodu bulunamadı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.InsuredIndividualType))
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı yakınlık bulunamadı ";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.IndividualType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.InsuredIndividualType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz yakınlık türü";
                    return response;
                }
            }
            if (this.policy.InsuredInitialPremium == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı prim bilgisi bulunamadı";
                return response;
            }
            if (this.policy.InsuredPolicyEntryDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı poliçeye giriş tarihi bulunamadı";
                return response;
            }
            if (this.policy.InsuredTcNo == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı TcNo bulunamadı";
                return response;
            }
            else
            {
                if (this.policy.InsuredTcNo.IsIdentityNoLength()==null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigortalı TcNo 11 karakter olmak zorunda";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.InsuredNationality))
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı uyruk bulunamadı";
                return response;
            }
            else
            {
                if (this.policy.InsuredNationality.ToUpper() != "TRK")
                {
                    List<Country> lstCountry = new GenericRepository<Country>().FindBy("A2_CODE =:code", parameters: new { code = new DbString { Value = policy.InsuredNationality } });
                    if (lstCountry.Count < 1)
                    {
                        response.ErrorCode = "999";
                        response.Description = "Sigortalı uyruk bulunamadı";
                        return response;
                    }
                }
            }
            if (!string.IsNullOrEmpty(this.policy.InsuredMaritalStatus))
            {
                if (this.policy.InsuredMaritalStatus == "B")
                { }
                else if (this.policy.InsuredMaritalStatus == "E") { }
                else
                {
                    response.ErrorCode = "999";
                    response.Description = "Medeni durum format hatası (E veya B)";
                    return response;
                }
            }
            if (this.policy.InsuredFirstInsuredDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı ilk sigortalanma tarihi bilgisi bulunamadı!";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.InsuredAddress))
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı adres bilgisi bulunamadı!";
                return response;
            }
            if (this.policy.InsuredCityCode == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı il kodu bulunamadı";
                return response;
            }
            else
            {
                string tmpCityCode = "";

                for (int s1 = 3; s1 > this.policy.InsuredCityCode.ToString().Length; s1--)
                    tmpCityCode += "0";
                tmpCityCode += this.policy.InsuredCityCode.ToString();

                List<City> lstCity = new GenericRepository<City>().FindBy($"CODE =:code", parameters: new { code = new DbString { Value = tmpCityCode } });
                if (lstCity.Count < 1)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigortalı il kodu bulunamadı";
                    return response;
                }
            }
            if (this.policy.EndorsementNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl no 0 olmamalı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                string zeylCode = EnumHelper.GetValues(ProteinEnums.EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI)[1];
                if (this.policy.EndorsementType != zeylCode)
                {
                    response.ErrorCode = "999";
                    response.Description = "Zeyl türü hatalı. Sigortalı bilgi degisikligi zeyli olmalı";
                    return response;
                }
            }
            if (this.policy.EndorsementStartDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl başlama tarihi bulunamadı";
                return response;
            }
            #endregion
            #region Logic Valid

            var PolicyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });

            if (PolicyExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no sistemde bulunamadı.";
                return response;
            }
            this.policy.PolicyId = (long)PolicyExists[0].POLICY_ID;

            if (!response.IsSucces) return response;
            if (this.policy.PolicyId < 1)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe kaydı bulunamadı!";
                return response;
            }

            var InsuredExists = new GenericRepository<V_InsuredEndorsement>().FindBy("POLICY_ID =:policyId and IDENTITY_NO =:tckn and FAMILY_NO =:familyNo", orderby: "POLICY_ID DESC,ENDORSEMENT_ID DESC,ENDORSEMENT_NO DESC", parameters: new { policyId = policy.PolicyId, tckn =  policy.InsuredTcNo, familyNo = policy.InsuredFamilyNo });

            if (InsuredExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Bu birey bu poliçede bulunamadı!";
                return response;
            }
            this.policy.InsuredId = InsuredExists[0].INSURED_ID;

            var EndorsementExistNonFamilyNo = new GenericRepository<V_PolicyEndorsement>().FindBy(conditions: $"POLICY_ID =:PolicyId ", parameters: new { policy.PolicyId }, orderby: "POLICY_ID DESC,ENDORSEMENT_ID DESC,ENDORSEMENT_NO DESC");
            if (EndorsementExistNonFamilyNo.Count > 0)
            {
                if (this.policy.EndorsementNo == EndorsementExistNonFamilyNo[0].ENDORSEMENT_NO &&
                    EndorsementExistNonFamilyNo[0].ENDORSEMENT_TYPE == ((int)ProteinEnums.EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI).ToString())
                    this.policy.EndorsementId = EndorsementExistNonFamilyNo[0].ENDORSEMENT_ID;
            }

            List<V_Package> lstpck = new GenericRepository<V_Package>().FindBy("PACKAGE_NO =:packageNo AND COMPANY_ID=:CompanyId", parameters: new { packageNo = new DbString { Value = policy.InsuredPackageId.ToString(), Length = 100 }, policy.CompanyId }, orderby: "");
            if (lstpck.Count < 1)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı plan kodu hatalı";
                return response;
            }
            else
            {
                if (lstpck.FirstOrDefault().SUBPRODUCT_ID == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Subproduct kodu bulunamadı.Plan kodunu kontrol ediniz!";
                    return response;
                }

                else
                {
                    this.policy.SubProductId = lstpck.FirstOrDefault().SUBPRODUCT_ID;
                    this.policy.InsuredPackageId = lstpck.FirstOrDefault().PACKAGE_ID;
                }
            }

            if (this.policy.EndorsementId == 0)
                response = EndorsementIssueCheck(this.policy.PolicyId);
            response = PolicyIsIssued(this.policy.PolicyId);
            response = InsuredSameBranch(this.policy);
            if (!response.IsSucces) return response;
            #endregion
            #region Mapping
            this.policy.EndorsementType = ((int)ProteinEnums.EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI).ToString();
            this.policy.EndorsementStartDate = this.policy.StartDate;
            #endregion
            return response;
        }
        public PolicyValidatorResponse InsuredPremiumChangeEndorsement()
        {
            Thread.Sleep(300);
            PolicyValidatorResponse response = new PolicyValidatorResponse();
            response.ErrorCode = "100";
            #region Basic Valid
            if (string.IsNullOrEmpty(this.policy.PolicyStatus))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe / Zeyl Durumunu belirtmelisiniz! ( 0 = TEKLIF, 2 = TANZIM)";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.PolicyStatus>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.PolicyStatus)
                    {
                        if (enVal.Text == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString()
                          || enVal.Text == ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString())
                            findHim = true;
                        break;
                    }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz poliçe status türü";
                    return response;
                }
            }
            if (this.policy.PolicyStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
            {
                if (this.policy.DateOfIssue == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Tanzim tarihi bulunamadı";
                    return response;
                }
            }

            if (string.IsNullOrEmpty(this.policy.PolicyNo))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no bulunamadı";
                return response;
            }
            else
            {
                this.policy.PolicyNo = this.policy.PolicyNo;

                if (string.IsNullOrEmpty(this.policy.PolicyNo))
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçe no bulunamadı";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.EndorsementType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl türü";
                    return response;
                }
            }
            if (this.policy.RenewalNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Yenileme no bulunamadı";
                return response;
            }
            //if (this.policy.InsuredPackageId == null)
            //{
            //    response.ErrorCode = "999";
            //    response.Description = "Sigortalı plan kodu bulunamadı";
            //    return response;
            //}
            //else
            //{
            //    List<Package> lstpck = new GenericRepository<Package>().FindBy($"NO = '{this.policy.InsuredPackageId}'");
            //    if (lstpck.Count < 1)
            //    {
            //        response.ErrorCode = "999";
            //        response.Description = "Sigortalı plan kodu hatalı";
            //        return response;
            //    }
            //    else
            //    {
            //        this.policy.InsuredPackageId = lstpck.FirstOrDefault().Id;
            //        List<V_Package> lstPcks = new GenericRepository<V_Package>().FindBy($"PACKAGE_ID = {this.policy.InsuredPackageId}", orderby: "");
            //        if (lstPcks.Count < 1)
            //        {
            //            response.ErrorCode = "999";
            //            response.Description = "Subproduct kodu bulunamadı.Plan kodunu kontrol ediniz!";
            //            return response;
            //        }
            //        else
            //            this.policy.SubProductId = lstPcks.FirstOrDefault().SUBPRODUCT_ID;
            //    }
            //}
            if (this.policy.InsuredInitialPremium == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı Prim Bilgisi bulunamadı";
                return response;
            }
            if (this.policy.InsuredTcNo == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı TcNo bulunamadı";
                return response;
            }
            else
            {
                if (this.policy.InsuredTcNo.IsIdentityNoLength()==null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigortalı TcNo 11 karakter olmak zorunda";
                    return response;
                }
            }

            if (this.policy.EndorsementNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl no 0 olmamalı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                string zeylCode = EnumHelper.GetValues(ProteinEnums.EndorsementType.SIGORTALI_TAHAKKUK)[1];
                if (this.policy.EndorsementType != zeylCode)
                {
                    response.ErrorCode = "999";
                    response.Description = "Zeyl türü hatalı. Teminat/Plan Değişikliği zeyli olmalı";
                    return response;
                }
            }
            if (this.policy.EndorsementStartDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl başlama tarihi bulunamadı";
                return response;
            }
            #endregion
            #region Logic Valid
            Thread.Sleep(100);
            var PolicyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });

            if (PolicyExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no sistemde bulunamadı. Önce poliçe oluşturmalısınız!";
                return response;
            }
            this.policy.PolicyId = (long)PolicyExists[0].POLICY_ID;

            if (!response.IsSucces) return response;
            if (this.policy.RenewalNo > 1)
            {
                Thread.Sleep(100);
                var policyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });
                if (policyExists.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Bu yenileme poliçesinin kaydı bulunamadı";
                    return response;
                }
            }

            V_Insured insured = new GenericRepository<V_Insured>().FindBy($"IDENTITY_NO=:identityNo AND POLICY_ID=:policyId AND STATUS=:status", orderby: "", parameters: new { identityNo =policy.InsuredTcNo, policyId = policy.PolicyId, status = new DbString { Value = ((int)ProteinEnums.Status.AKTIF).ToString(), Length = 3 } }).FirstOrDefault();

            if (insured == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı sistemde bulunamadı. Önce sigortalıyı oluşturmalısınız!";
                return response;
            }
            policy.InsuredId = insured.INSURED_ID;

            #endregion
            if (this.policy.PolicyId < 1)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe kaydı bulunamadı!";
                return response;
            }

            var EndorsementExists = new GenericRepository<V_PolicyEndorsement>().FindBy("POLICY_ID =:policyId and POLICY_STATUS !=:polStatus", parameters: new { policyId = policy.PolicyId, polStatus = new DbString { Value = ((int)ProteinEnums.PolicyStatus.SILINDI).ToString(), Length = 3 } }, orderby: "");
            if (EndorsementExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Bu poliçenin mevcut zeyl bilgisi bulunamadı.";
                return response;
            }

            foreach (V_PolicyEndorsement policyEndors in EndorsementExists)
            {
                if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo && policyEndors.ENDORSEMENT_TYPE != ((int)ProteinEnums.EndorsementType.SIGORTALI_TAHAKKUK).ToString())
                {
                    response.ErrorCode = "999";
                    response.Description = "Aynı zeyil no hatası";
                    return response;
                }
                else if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo && policyEndors.ENDORSEMENT_TYPE == ((int)ProteinEnums.EndorsementType.SIGORTALI_TAHAKKUK).ToString())
                {
                    policy.EndorsementId = policyEndors.ENDORSEMENT_ID;
                }
            }
            if (this.policy.EndorsementId == 0)
                response = EndorsementIssueCheck(this.policy.PolicyId);
            if (string.IsNullOrEmpty(this.policy.EndorsementCategory))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl kategorisi bulunamadı 0 = POLİÇE İÇİN , 1 = SİGORTALI İÇİN";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementCategory>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.EndorsementCategory)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl kategorisi ";
                    return response;
                }
            }

            if (string.IsNullOrEmpty(this.policy.EndorsementTypeDescriptionType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl Tipinin türü bulunamadı. 0 = POLICE BASLANGICI, 1 = GUN ESASLI ";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementTypeDescriptionType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.EndorsementTypeDescriptionType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl tipinin türü";
                    return response;
                }
            }
            response = PolicyIsIssued(this.policy.PolicyId);
            if (!response.IsSucces) return response;
            #region Mapping
            this.policy.EndorsementType = ((int)ProteinEnums.EndorsementType.SIGORTALI_TAHAKKUK).ToString();
            //this.policy.EndorsementStartDate = this.policy.StartDate;
            #endregion

            return response;
        }
        public PolicyValidatorResponse PolicyDateChangeEndorsement()
        {
            Thread.Sleep(300);
            PolicyValidatorResponse response = new PolicyValidatorResponse();
            response.ErrorCode = "100";
            #region Basic Valid
            if (string.IsNullOrEmpty(this.policy.PolicyStatus))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe / Zeyl Durumunu belirtmelisiniz! ( 0 = TEKLIF, 2 = TANZIM)";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.PolicyStatus>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.PolicyStatus)
                    {
                        if (enVal.Text == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString()
                          || enVal.Text == ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString())
                            findHim = true;
                        break;
                    }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz poliçe status türü";
                    return response;
                }
            }
            if (this.policy.PolicyStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
            {
                if (this.policy.DateOfIssue == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Tanzim tarihi bulunamadı";
                    return response;
                }
            }

            if (string.IsNullOrEmpty(this.policy.PolicyNo))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no bulunamadı";
                return response;
            }
            else
            {
                this.policy.PolicyNo = this.policy.PolicyNo;

                if (string.IsNullOrEmpty(this.policy.PolicyNo))
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçe no bulunamadı";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.EndorsementType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl türü";
                    return response;
                }
            }
            if (this.policy.RenewalNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Yenileme no bulunamadı";
                return response;
            }
            if (this.policy.EndorsementNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl no 0 olmamalı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                string zeylCode = EnumHelper.GetValues(ProteinEnums.EndorsementType.BASLANGIC_BITIS_TARIHI)[1];
                if (this.policy.EndorsementType != zeylCode)
                {
                    response.ErrorCode = "999";
                    response.Description = "Zeyl türü hatalı. Baslangıc Bitis tarihi zeyli olmalı";
                    return response;
                }
            }
            if (this.policy.StartDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe başlama tarihi bulunamadı";
                return response;
            }
            if (this.policy.EndDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe bitiş tarihi bulunamadı";
                return response;
            }
            if (this.policy.EndorsementStartDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl başlama tarihi bulunamadı";
                return response;
            }

            #endregion
            #region Logic Valid
            Thread.Sleep(100);
            var PolicyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });

            if (PolicyExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no sistemde bulunamadı. Önce poliçe oluşturmalısınız!";
                return response;
            }
            this.policy.PolicyId = (long)PolicyExists[0].POLICY_ID;

            if (!response.IsSucces) return response;
            if (this.policy.RenewalNo > 1)
            {
                Thread.Sleep(100);
                var policyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });
                if (policyExists.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Bu yenileme poliçesinin kaydı bulunamadı";
                    return response;
                }
            }
            #endregion
            if (this.policy.PolicyId < 1)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe kaydı bulunamadı!";
                return response;
            }

            var EndorsementExists = new GenericRepository<V_PolicyEndorsement>().FindBy("POLICY_ID =:policyId and POLICY_STATUS !=:polStatus", parameters: new { policyId = policy.PolicyId, polStatus = new DbString { Value = ((int)ProteinEnums.PolicyStatus.SILINDI).ToString(), Length = 3 } }, orderby: "");
            if (EndorsementExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Bu poliçenin mevcut zeyl bilgisi bulunamadı.";
                return response;
            }

            foreach (V_PolicyEndorsement policyEndors in EndorsementExists)
            {
                if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo && policyEndors.ENDORSEMENT_TYPE != ((int)ProteinEnums.EndorsementType.BASLANGIC_BITIS_TARIHI).ToString())
                {
                    response.ErrorCode = "999";
                    response.Description = "Aynı zeyil no hatası";
                    return response;
                }
                else if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo && policyEndors.ENDORSEMENT_TYPE == ((int)ProteinEnums.EndorsementType.BASLANGIC_BITIS_TARIHI).ToString())
                {
                    policy.EndorsementId = policyEndors.ENDORSEMENT_ID;
                }
            }

            if (string.IsNullOrEmpty(this.policy.EndorsementCategory))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl kategorisi bulunamadı 0 = POLİÇE İÇİN , 1 = SİGORTALI İÇİN";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementCategory>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.EndorsementCategory)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl kategorisi ";
                    return response;
                }
            }

            if (string.IsNullOrEmpty(this.policy.EndorsementTypeDescriptionType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl Tipinin türü bulunamadı. 0 = POLICE BASLANGICI, 1 = GUN ESASLI ";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementTypeDescriptionType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.EndorsementTypeDescriptionType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl tipinin türü";
                    return response;
                }
            }
            response = PolicyIsIssued(this.policy.PolicyId);
            if (!response.IsSucces) return response;
            if (this.policy.EndorsementId == 0)
                response = EndorsementIssueCheck(this.policy.PolicyId);
            #region Mapping
            this.policy.EndorsementType = ((int)ProteinEnums.EndorsementType.BASLANGIC_BITIS_TARIHI).ToString();
            //this.policy.EndorsementStartDate = this.policy.StartDate;
            #endregion

            return response;
        }
        public PolicyValidatorResponse PolicyPremiumChangeEndorsement()
        {
            Thread.Sleep(300);
            PolicyValidatorResponse response = new PolicyValidatorResponse();
            response.ErrorCode = "100";
            #region Basic Valid
            if (string.IsNullOrEmpty(this.policy.PolicyStatus))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe / Zeyl Durumunu belirtmelisiniz! ( 0 = TEKLIF, 2 = TANZIM)";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.PolicyStatus>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.PolicyStatus)
                    {
                        if (enVal.Text == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString()
                          || enVal.Text == ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString())
                            findHim = true;
                        break;
                    }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz poliçe status türü";
                    return response;
                }
            }
            if (this.policy.PolicyStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
            {
                if (this.policy.DateOfIssue == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Tanzim tarihi bulunamadı";
                    return response;
                }
            }

            if (string.IsNullOrEmpty(this.policy.PolicyNo))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no bulunamadı";
                return response;
            }
            else
            {
                this.policy.PolicyNo = this.policy.PolicyNo;

                if (string.IsNullOrEmpty(this.policy.PolicyNo))
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçe no bulunamadı";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.EndorsementType)
                    { policy.EndorsementType = enVal.Code; findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl türü";
                    return response;
                }
            }
            if (this.policy.RenewalNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Yenileme no bulunamadı";
                return response;
            }
            if (this.policy.EndorsementNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl no 0 olmamalı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            //if (this.policy.PolicyPremium == null)
            //{
            //    response.ErrorCode = "999";
            //    response.Description = "Poliçe Prim Tutarı Bulunamadı";
            //    return response;
            //}
            if (this.policy.EndorsementStartDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl başlama tarihi bulunamadı";
                return response;
            }
            #endregion
            #region Logic Valid
            Thread.Sleep(100);
            var PolicyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });

            if (PolicyExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no sistemde bulunamadı. Önce poliçe oluşturmalısınız!";
                return response;
            }
            this.policy.PolicyId = (long)PolicyExists[0].POLICY_ID;

            if (!response.IsSucces) return response;
            if (this.policy.RenewalNo > 1)
            {
                Thread.Sleep(100);
                var policyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });
                if (policyExists.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Bu yenileme poliçesinin kaydı bulunamadı";
                    return response;
                }
            }
            #endregion
            if (this.policy.PolicyId < 1)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe kaydı bulunamadı!";
                return response;
            }

            var EndorsementExists = new GenericRepository<V_PolicyEndorsement>().FindBy("POLICY_ID =:policyId and POLICY_STATUS !=:polStatus", parameters: new { policyId = policy.PolicyId, polStatus = new DbString { Value = ((int)ProteinEnums.PolicyStatus.SILINDI).ToString(), Length = 3 } }, orderby: "");
            if (EndorsementExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Bu poliçenin mevcut zeyl bilgisi bulunamadı.";
                return response;
            }

            foreach (V_PolicyEndorsement policyEndors in EndorsementExists)
            {
                if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo && policyEndors.ENDORSEMENT_TYPE != ((int)ProteinEnums.EndorsementType.URETIM_KAYNAGI_DEGISIKLIGI).ToString())
                {
                    response.ErrorCode = "999";
                    response.Description = "Aynı zeyil no hatası";
                    return response;
                }
                else if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo && policyEndors.ENDORSEMENT_TYPE == ((int)ProteinEnums.EndorsementType.URETIM_KAYNAGI_DEGISIKLIGI).ToString())
                {
                    policy.EndorsementId = policyEndors.ENDORSEMENT_ID;
                }
            }

            if (string.IsNullOrEmpty(this.policy.EndorsementCategory))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl kategorisi bulunamadı 0 = POLİÇE İÇİN , 1 = SİGORTALI İÇİN";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementCategory>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.EndorsementCategory)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl kategorisi ";
                    return response;
                }
            }

            if (string.IsNullOrEmpty(this.policy.EndorsementTypeDescriptionType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl Tipinin türü bulunamadı. 0 = POLICE BASLANGICI, 1 = GUN ESASLI ";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementTypeDescriptionType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.EndorsementTypeDescriptionType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl tipinin türü";
                    return response;
                }
            }
            response = PolicyIsIssued(this.policy.PolicyId);
            if (!response.IsSucces) return response;
            if (this.policy.EndorsementId == 0)
                response = EndorsementIssueCheck(this.policy.PolicyId);
            #region Mapping
            //this.policy.EndorsementType = ((int)ProteinEnums.EndorsementType.URETIM_KAYNAGI_DEGISIKLIGI).ToString();
            this.policy.EndorsementStartDate = this.policy.StartDate;
            #endregion

            return response;
        }
        public PolicyValidatorResponse InsurerChangeEndorsement()
        {
            Thread.Sleep(300);
            PolicyValidatorResponse response = new PolicyValidatorResponse();
            response.ErrorCode = "100";
            #region Basic Valid
            if (string.IsNullOrEmpty(this.policy.PolicyStatus))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe / Zeyl Durumunu belirtmelisiniz! ( 0 = TEKLIF, 2 = TANZIM)";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.PolicyStatus>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.PolicyStatus)
                    {
                        if (enVal.Text == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString()
                          || enVal.Text == ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString())
                            findHim = true;
                        break;
                    }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz poliçe status türü";
                    return response;
                }
            }
            if (this.policy.PolicyStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
            {

                if (this.policy.DateOfIssue == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Tanzim tarihi bulunamadı";
                    return response;
                }
            }

            if (string.IsNullOrEmpty(this.policy.PolicyNo))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no bulunamadı";
                return response;
            }
            else
            {
                this.policy.PolicyNo = this.policy.PolicyNo;

                if (string.IsNullOrEmpty(this.policy.PolicyNo))
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçe no bulunamadı";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.EndorsementType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl türü";
                    return response;
                }
            }
            if (this.policy.RenewalNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Yenileme no bulunamadı";
                return response;
            }
            if (this.policy.EndorsementNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl no 0 olmamalı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                string zeylCode = EnumHelper.GetValues(ProteinEnums.EndorsementType.SE_DEGISIKLIGI)[1];
                if (this.policy.EndorsementType != zeylCode)
                {
                    response.ErrorCode = "999";
                    response.Description = "Zeyl türü hatalı. Sigorta Ettiren Değişikliği zeyli olmalı";
                    return response;
                }
            }

            if (string.IsNullOrEmpty(this.policy.InsurerType))
            {
                response.ErrorCode = "999";
                response.Description = "Sigorta ettiren tipi bulunamadı";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.ContactType>();

                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.InsurerType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren tipi hatalı";
                    return response;
                }
            }
            if (this.policy.InsurerType == ((int)ContactType.GERCEK).ToString())
            {
                if (string.IsNullOrEmpty(this.policy.InsurerName))
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren isim bulunamadı";
                    return response;
                }
                if (string.IsNullOrEmpty(this.policy.InsurerLastName))
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren soyisim bulunamadı";
                    return response;
                }
                if (string.IsNullOrEmpty(this.policy.InsurerName))
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren isim bulunamadı";
                    return response;
                }
                if (string.IsNullOrEmpty(this.policy.InsurerGender))
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren cinsiyet bulunamadı";
                    return response;
                }
                else
                {
                    List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.Gender>();

                    bool findHim = false;
                    foreach (EnumValue enVal in enumValuess)
                    {
                        if (enVal.Text == this.policy.InsurerGender)
                        { findHim = true; break; }
                    }
                    if (!findHim)
                    {
                        response.ErrorCode = "999";
                        response.Description = "Sigorta ettiren cinsiyet hatalı";
                        return response;
                    }
                }
                if (this.policy.InsurerBirthDate == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren doğum tarihi bulunamadı";
                    return response;
                }
                if (string.IsNullOrEmpty(this.policy.InsurerNameOfFather))
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren baba adı bulunamadı";
                    return response;
                }
                if (this.policy.InsurerTcNo == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren TcNo bulunamadı";
                    return response;
                }
                else
                {
                    if (this.policy.InsurerTcNo.IsIdentityNoLength()==null)
                    {
                        response.ErrorCode = "999";
                        response.Description = "Sigorta ettiren TcNo 11 karakter olmak zorunda";
                        return response;
                    }
                }
                if (string.IsNullOrEmpty(this.policy.InsurerNationality))
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren uyruk bulunamadı";
                    return response;
                }
                else
                {
                    if (this.policy.InsurerNationality.ToUpper() != "TRK")
                    {
                        List<Country> lstCountry = new GenericRepository<Country>().FindBy($"A2_CODE = :code", parameters: new { code = new DbString { Value = policy.InsuredNationality } });
                        if (lstCountry.Count < 1)
                        {
                            response.ErrorCode = "999";
                            response.Description = "Sigorta ettiren uyruk bulunamadı";
                            return response;
                        }
                    }
                }
                if (string.IsNullOrEmpty(this.policy.InsurerAddress))
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren adres bulunamadı";
                    return response;
                }
                if (this.policy.InsurerCityCode == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren il kodu bulunamadı";
                    return response;
                }
                else
                {
                    string tmpCityCode = "";

                    for (int s1 = 3; s1 > this.policy.InsurerCityCode.ToString().Length; s1--)
                        tmpCityCode += "0";
                    tmpCityCode += this.policy.InsurerCityCode.ToString();

                    List<City> lstCity = new GenericRepository<City>().FindBy($"CODE =:code", parameters: new { code = new DbString { Value = tmpCityCode } });
                    if (lstCity.Count < 1)
                    {
                        response.ErrorCode = "999";
                        response.Description = "Sigorta ettiren il kodu bulunamadı";
                        return response;
                    }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(this.policy.InsurerName))
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren isim bulunamadı";
                    return response;
                }
                if (this.policy.InsurerVkn.IsTaxNumberLength()==null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren VKN Bilgisi Bulunamadı";
                    return response;
                }
                if (string.IsNullOrEmpty(this.policy.InsurerAddress))
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren adres bulunamadı";
                    return response;
                }
                if (this.policy.InsurerCityCode == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren il kodu bulunamadı";
                    return response;
                }
                else
                {
                    string tmpCityCode = "";

                    for (int s1 = 3; s1 > this.policy.InsurerCityCode.ToString().Length; s1--)
                        tmpCityCode += "0";
                    tmpCityCode += this.policy.InsurerCityCode.ToString();

                    List<City> lstCity = new GenericRepository<City>().FindBy($"CODE =:code", parameters: new { code = new DbString { Value = tmpCityCode } });
                    if (lstCity.Count < 1)
                    {
                        response.ErrorCode = "999";
                        response.Description = "Sigorta ettiren il kodu bulunamadı";
                        return response;
                    }
                }
            }
            if (this.policy.EndorsementStartDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl başlama tarihi bulunamadı";
                return response;
            }
            #endregion
            #region Logic Valid
            Thread.Sleep(100);
            var PolicyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });

            if (PolicyExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no sistemde bulunamadı. Önce poliçe oluşturmalısınız!";
                return response;
            }
            this.policy.PolicyId = (long)PolicyExists[0].POLICY_ID;

            if (!response.IsSucces) return response;
            if (this.policy.RenewalNo > 1)
            {
                Thread.Sleep(100);
                var policyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });
                if (policyExists.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Bu yenileme poliçesinin kaydı bulunamadı";
                    return response;
                }
            }
            #endregion
            if (this.policy.PolicyId < 1)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe kaydı bulunamadı!";
                return response;
            }

            var EndorsementExists = new GenericRepository<V_PolicyEndorsement>().FindBy("POLICY_ID =:policyId and POLICY_STATUS !=:polStatus", parameters: new { policyId = policy.PolicyId, polStatus = new DbString { Value = ((int)ProteinEnums.PolicyStatus.SILINDI).ToString(), Length = 3 } }, orderby: "");
            if (EndorsementExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Bu poliçenin mevcut zeyl bilgisi bulunamadı.";
                return response;
            }

            foreach (V_PolicyEndorsement policyEndors in EndorsementExists)
            {
                if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo && policyEndors.ENDORSEMENT_TYPE != ((int)ProteinEnums.EndorsementType.SE_DEGISIKLIGI).ToString())
                {
                    response.ErrorCode = "999";
                    response.Description = "Aynı zeyil no hatası";
                    return response;
                }
                else if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo && policyEndors.ENDORSEMENT_TYPE == ((int)ProteinEnums.EndorsementType.SE_DEGISIKLIGI).ToString())
                {
                    policy.EndorsementId = policyEndors.ENDORSEMENT_ID;
                }
            }

            if (string.IsNullOrEmpty(this.policy.EndorsementCategory))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl kategorisi bulunamadı 0 = POLİÇE İÇİN , 1 = SİGORTALI İÇİN";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementCategory>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.EndorsementCategory)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl kategorisi ";
                    return response;
                }
            }

            if (string.IsNullOrEmpty(this.policy.EndorsementTypeDescriptionType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl Tipinin türü bulunamadı. 0 = POLICE BASLANGICI, 1 = GUN ESASLI ";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementTypeDescriptionType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.EndorsementTypeDescriptionType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl tipinin türü";
                    return response;
                }
            }
            response = PolicyIsIssued(this.policy.PolicyId);
            if (!response.IsSucces) return response;
            if (this.policy.EndorsementId == 0)
                response = EndorsementIssueCheck(this.policy.PolicyId);
            #region Mapping
            this.policy.EndorsementType = ((int)ProteinEnums.EndorsementType.SE_DEGISIKLIGI).ToString();
            //this.policy.EndorsementStartDate = this.policy.StartDate;
            #endregion

            return response;
        }
        public PolicyValidatorResponse AgencyChangeEndorsement()
        {
            Thread.Sleep(300);
            PolicyValidatorResponse response = new PolicyValidatorResponse();
            response.ErrorCode = "100";
            #region Basic Valid
            if (string.IsNullOrEmpty(this.policy.PolicyStatus))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe / Zeyl Durumunu belirtmelisiniz! ( 0 = TEKLIF, 2 = TANZIM)";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.PolicyStatus>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.PolicyStatus)
                    {
                        if (enVal.Text == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString()
                          || enVal.Text == ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString())
                            findHim = true;
                        break;
                    }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz poliçe status türü";
                    return response;
                }
            }
            if (this.policy.PolicyStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
            {
                if (this.policy.DateOfIssue == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Tanzim tarihi bulunamadı";
                    return response;
                }
            }

            if (string.IsNullOrEmpty(this.policy.PolicyNo))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no bulunamadı";
                return response;
            }
            else
            {
                this.policy.PolicyNo = this.policy.PolicyNo;

                if (string.IsNullOrEmpty(this.policy.PolicyNo))
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçe no bulunamadı";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.EndorsementType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl türü";
                    return response;
                }
            }
            if (this.policy.RenewalNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Yenileme no bulunamadı";
                return response;
            }
            if (this.policy.EndorsementNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl no 0 olmamalı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                string zeylCode = EnumHelper.GetValues(ProteinEnums.EndorsementType.URETIM_KAYNAGI_DEGISIKLIGI)[1];
                if (this.policy.EndorsementType != zeylCode)
                {
                    response.ErrorCode = "999";
                    response.Description = "Zeyl türü hatalı. Üretim Kaynağı Değişikliği zeyli olmalı";
                    return response;
                }
            }
            if (this.policy.AgencyId == null)
            {
                response.ErrorCode = "999";
                response.Description = "Acente no bulunamadı";
                return response;
            }
            if (this.policy.CompanyId == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Şirket bilgisi bulunamadı";
                return response;
            }
            if (this.policy.CompanyId > 0 && string.IsNullOrEmpty(this.policy.AgencyId))
            {
                var agency = new GenericRepository<V_Agency>().FindBy("COMPANY_ID =:CompanyId and AGENCY_NO =:agencyNo", parameters: new { CompanyId = policy.CompanyId, agencyNo = new DbString { Value = policy.AgencyId, Length = 20 } }, orderby: "");
                if (agency.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Acente şirket eşleşmesi hatalı";
                    return response;
                }
                policy.AgencyId = agency.First().AGENCY_ID.ToString();
            }
            if (this.policy.EndorsementStartDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl başlama tarihi bulunamadı";
                return response;
            }
            #endregion
            #region Logic Valid
            Thread.Sleep(100);
            var PolicyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });

            if (PolicyExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no sistemde bulunamadı. Önce poliçe oluşturmalısınız!";
                return response;
            }
            this.policy.PolicyId = (long)PolicyExists[0].POLICY_ID;

            if (!response.IsSucces) return response;
            if (this.policy.RenewalNo > 1)
            {
                Thread.Sleep(100);
                var policyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });
                if (policyExists.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Bu yenileme poliçesinin kaydı bulunamadı";
                    return response;
                }
            }
            #endregion
            if (this.policy.PolicyId < 1)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe kaydı bulunamadı!";
                return response;
            }

            var EndorsementExists = new GenericRepository<V_PolicyEndorsement>().FindBy("POLICY_ID =:policyId and POLICY_STATUS !=:polStatus", parameters: new { policyId = policy.PolicyId, polStatus = new DbString { Value = ((int)ProteinEnums.PolicyStatus.SILINDI).ToString(), Length = 3 } }, orderby: "");
            if (EndorsementExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Bu poliçenin mevcut zeyl bilgisi bulunamadı.";
                return response;
            }

            foreach (V_PolicyEndorsement policyEndors in EndorsementExists)
            {
                if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo && policyEndors.ENDORSEMENT_TYPE != ((int)ProteinEnums.EndorsementType.URETIM_KAYNAGI_DEGISIKLIGI).ToString())
                {
                    response.ErrorCode = "999";
                    response.Description = "Aynı zeyil no hatası";
                    return response;
                }
                else if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo && policyEndors.ENDORSEMENT_TYPE == ((int)ProteinEnums.EndorsementType.URETIM_KAYNAGI_DEGISIKLIGI).ToString())
                {
                    policy.EndorsementId = policyEndors.ENDORSEMENT_ID;
                }
            }

            if (string.IsNullOrEmpty(this.policy.EndorsementCategory))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl kategorisi bulunamadı 0 = POLİÇE İÇİN , 1 = SİGORTALI İÇİN";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementCategory>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.EndorsementCategory)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl kategorisi ";
                    return response;
                }
            }

            if (string.IsNullOrEmpty(this.policy.EndorsementTypeDescriptionType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl Tipinin türü bulunamadı. 0 = POLICE BASLANGICI, 1 = GUN ESASLI ";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementTypeDescriptionType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.EndorsementTypeDescriptionType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl tipinin türü";
                    return response;
                }
            }
            response = PolicyIsIssued(this.policy.PolicyId);
            if (!response.IsSucces) return response;
            if (this.policy.EndorsementId == 0)
                response = EndorsementIssueCheck(this.policy.PolicyId);
            #region Mapping
            this.policy.EndorsementType = ((int)ProteinEnums.EndorsementType.URETIM_KAYNAGI_DEGISIKLIGI).ToString();
            //this.policy.EndorsementStartDate = this.policy.StartDate;
            #endregion

            return response;
        }
        public PolicyValidatorResponse PlanChangeEndorsement()
        {
            Thread.Sleep(300);
            PolicyValidatorResponse response = new PolicyValidatorResponse();
            response.ErrorCode = "100";
            #region Basic Valid
            if (string.IsNullOrEmpty(this.policy.PolicyStatus))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe / Zeyl Durumunu belirtmelisiniz! ( 0 = TEKLIF, 2 = TANZIM)";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.PolicyStatus>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.PolicyStatus)
                    {
                        if (enVal.Text == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString()
                          || enVal.Text == ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString())
                            findHim = true;
                        break;
                    }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz poliçe status türü";
                    return response;
                }
            }
            if (this.policy.PolicyStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
            {
                if (this.policy.DateOfIssue == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Tanzim tarihi bulunamadı";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.PolicyNo))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no bulunamadı";
                return response;
            }
            else
            {
                this.policy.PolicyNo = this.policy.PolicyNo;

                if (string.IsNullOrEmpty(this.policy.PolicyNo))
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçe no bulunamadı";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.EndorsementType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl türü";
                    return response;
                }
            }
            if (this.policy.RenewalNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Yenileme no bulunamadı";
                return response;
            }
            if (this.policy.InsuredPackageId == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı plan kodu bulunamadı";
                return response;
            }
            if (this.policy.InsuredTcNo == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı TcNo bulunamadı";
                return response;
            }
            else
            {
                if (this.policy.InsuredTcNo.IsIdentityNoLength()==null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigortalı TcNo 11 karakter olmak zorunda";
                    return response;
                }
            }
            if (this.policy.EndorsementNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl no 0 olmamalı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                string zeylCode = EnumHelper.GetValues(ProteinEnums.EndorsementType.TEMINAT_PLAN_DEGISIKLIGI)[1];
                if (this.policy.EndorsementType != zeylCode)
                {
                    response.ErrorCode = "999";
                    response.Description = "Zeyl türü hatalı. Teminat/Plan Değişikliği zeyli olmalı";
                    return response;
                }
            }
            if (this.policy.EndorsementStartDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl başlama tarihi bulunamadı";
                return response;
            }

            #endregion
            #region Logic Valid
            Thread.Sleep(100);
            var PolicyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });

            if (PolicyExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no sistemde bulunamadı. Önce poliçe oluşturmalısınız!";
                return response;
            }
            this.policy.PolicyId = (long)PolicyExists[0].POLICY_ID;

            if (!response.IsSucces) return response;
            if (this.policy.RenewalNo > 1)
            {
                Thread.Sleep(100);
                var policyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });
                if (policyExists.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Bu yenileme poliçesinin kaydı bulunamadı";
                    return response;
                }
            }

            V_Insured insured = new GenericRepository<V_Insured>().FindBy($"IDENTITY_NO=:identityNo AND POLICY_ID=:policyId AND STATUS=:status", orderby: "", parameters: new { identityNo = policy.InsuredTcNo.ToString(), policyId = policy.PolicyId, status = new DbString { Value = ((int)ProteinEnums.Status.AKTIF).ToString(), Length = 3 } }).FirstOrDefault();
            if (insured == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı sistemde bulunamadı. Önce sigortalıyı oluşturmalısınız!";
                return response;
            }
            policy.InsuredId = insured.INSURED_ID;

            List<V_Package> lstpck = new GenericRepository<V_Package>().FindBy("PACKAGE_NO =:packageNo AND COMPANY_ID=:CompanyId", parameters: new { packageNo = new DbString { Value = policy.InsuredPackageId.ToString(), Length = 100 }, policy.CompanyId }, orderby: "");
            if (lstpck.Count < 1)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı plan kodu hatalı";
                return response;
            }
            else
            {
                if (lstpck.FirstOrDefault().SUBPRODUCT_ID == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Subproduct kodu bulunamadı.Plan kodunu kontrol ediniz!";
                    return response;
                }

                else
                {
                    this.policy.SubProductId = lstpck.FirstOrDefault().SUBPRODUCT_ID;
                    this.policy.InsuredPackageId = lstpck.FirstOrDefault().PACKAGE_ID;
                }
            }

            #endregion
            if (this.policy.PolicyId < 1)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe kaydı bulunamadı!";
                return response;
            }

            var EndorsementExists = new GenericRepository<V_PolicyEndorsement>().FindBy("POLICY_ID =:PolicyId", parameters: new { policy.PolicyId }, orderby: "");
            if (EndorsementExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Bu poliçenin mevcut zeyl bilgisi bulunamadı.";
                return response;
            }

            foreach (V_PolicyEndorsement policyEndors in EndorsementExists)
            {
                if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo && policyEndors.ENDORSEMENT_TYPE != ((int)ProteinEnums.EndorsementType.TEMINAT_PLAN_DEGISIKLIGI).ToString())
                {
                    response.ErrorCode = "999";
                    response.Description = "Aynı zeyil no hatası";
                    return response;
                }
                else if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo && policyEndors.ENDORSEMENT_TYPE == ((int)ProteinEnums.EndorsementType.TEMINAT_PLAN_DEGISIKLIGI).ToString())
                {
                    policy.EndorsementId = policyEndors.ENDORSEMENT_ID;
                }
            }

            if (string.IsNullOrEmpty(this.policy.EndorsementCategory))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl kategorisi bulunamadı 0 = POLİÇE İÇİN , 1 = SİGORTALI İÇİN";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementCategory>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.EndorsementCategory)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl kategorisi ";
                    return response;
                }
            }

            if (string.IsNullOrEmpty(this.policy.EndorsementTypeDescriptionType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl Tipinin türü bulunamadı. 0 = POLICE BASLANGICI, 1 = GUN ESASLI ";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementTypeDescriptionType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.EndorsementTypeDescriptionType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl tipinin türü";
                    return response;
                }
            }
            response = PolicyIsIssued(this.policy.PolicyId);
            if (!response.IsSucces) return response;
            if (this.policy.EndorsementId == 0)
                response = EndorsementIssueCheck(this.policy.PolicyId);
            #region Mapping
            this.policy.EndorsementType = ((int)ProteinEnums.EndorsementType.TEMINAT_PLAN_DEGISIKLIGI).ToString();
            //this.policy.EndorsementStartDate = this.policy.StartDate;
            #endregion

            return response;
        }
        public PolicyValidatorResponse BeginningEndorsement()
        {
            PolicyValidatorResponse response = new PolicyValidatorResponse();
            response.ErrorCode = "100";
            #region Basic valid

            bool findPolicyNo = false;
            //if (this.policy.DateOfIssue == null)
            //{
            //    response.ErrorCode = "999";
            //    response.Description = "Tanzim tarihi bulunamadı";
            //    return response;
            //}
            if (string.IsNullOrEmpty(this.policy.PolicyStatus))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe / Zeyl Durumunu belirtmelisiniz! ( 0 = TEKLIF, 2 = TANZIM)";
                return response;
            }
            else
            {
                var lookupData = LookupHelper.GetLookupByOrdinal(LookupTypes.PolicyStatus, policy.PolicyStatus);
                if (lookupData==null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz poliçe status türü";
                    return response;
                }
            }
            if (!this.policy.PolicyNo.IsInt64())
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no bulunamadı";
                return response;
            }
            if (!string.IsNullOrEmpty(this.policy.PreviousCoverPolicyNo) || !string.IsNullOrEmpty(this.policy.PreviousPolicyNo))
            {
                this.policy.PreviousPolicyNo = !string.IsNullOrEmpty(this.policy.PreviousCoverPolicyNo) ? this.policy.PreviousCoverPolicyNo.Trim() : this.policy.PreviousPolicyNo.Trim();

                if (!this.policy.PreviousRenewalNo.ToString().IsInt())
                {
                    response.ErrorCode = "999";
                    response.Description = "Önceki poliçe yenileme bilgisini girmelisiniz";
                    return response;
                }

                var policyExists = new GenericRepository<V_Policy>().FindBy($"RENEWAL_NO ={policy.PreviousRenewalNo} AND POLICY_NUMBER ={policy.PreviousPolicyNo} AND COMPANY_ID ={policy.CompanyId} ", orderby: "POLICY_ID DESC");
                if (policyExists.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Önceki poliçe bilgisi hatalı";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.PolicyType))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe tipi bulunamadı";
                return response;
            }
            else
            {
                var lookupData = LookupHelper.GetLookupByOrdinal(LookupTypes.Policy, policy.PolicyType);
                if (lookupData == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz poliçe tipi";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                var lookupData = LookupHelper.GetLookupByCode(LookupTypes.Endorsement, policy.EndorsementType);
                if (lookupData == null)
                {
                    lookupData = LookupHelper.GetLookupByOrdinal(LookupTypes.Endorsement, policy.EndorsementType);
                    if (lookupData == null)
                    {
                        response.ErrorCode = "999";
                        response.Description = "Geçersiz Zeyl tipi";
                        return response;
                    }
                }
            }
            if (this.policy.AgencyId == null)
            {
                response.ErrorCode = "999";
                response.Description = "Acente no bulunamadı";
                return response;
            }
            if (this.policy.CompanyId == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Şirket bilgisi bulunamadı";
                return response;
            }
            if (this.policy.CompanyId > 0 && !string.IsNullOrEmpty(this.policy.AgencyId))
            {
                var agency = new GenericRepository<V_Agency>().FindBy("COMPANY_ID =:CompanyId and AGENCY_NO =:agencyNo", parameters: new { CompanyId = policy.CompanyId, agencyNo = new DbString { Value = policy.AgencyId, Length = 20 } }, orderby: "");
                if (agency.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Acente şirket eşleşmesi hatalı";
                    return response;
                }
            }
            //if (this.policy.RenewalNo == 0)
            //{
            //    response.ErrorCode = "999";
            //    response.Description = "Yenileme no bulunamadı";
            //    return response;
            //}
            if (this.policy.StartDate==null)
            {
                response.ErrorCode = "999";
                response.Description = "Başlama tarihi bulunamadı";
                return response;
            }
            if (this.policy.EndDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Bitiş tarihi bulunamadı";
                return response;
            }
            //if (this.policy.DateOfIssue == null)
            //{
            //    response.ErrorCode = "999";
            //    response.Description = "Tanzim tarihi bulunamadı";
            //    return response;
            //}
            if (!string.IsNullOrEmpty(this.policy.PolicyCurrencyType))
            {
                var lookupData = LookupHelper.GetLookupByOrdinal(LookupTypes.Currency, policy.PolicyCurrencyType.ToUpper().Trim());
                if (lookupData == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz poliçe kur(döviz kodu) tipi";
                    return response;
                }
                if (this.policy.PolicyCurrencyType != ((int)ProteinEnums.CurrencyType.TURK_LIRASI).ToString())
                {
                    this.policy.ExchangeRateId = GetExchangeRateID(this.policy.PolicyCurrencyType);
                    if (this.policy.ExchangeRateId == 0)
                    {
                        response.ErrorCode = "999";
                        response.Description = "Kur bilgisi bulunamadı. Lütfen kur tablosunu kontrol ediniz!";
                        return response;
                    }
                }
            }

            if (this.policy.PolicyInstallmentCount == null)
            {
                response.ErrorCode = "999";
                response.Description = "Vade sayısı bulunamadı";
                return response;
            }
            else
            {
                if (this.policy.PolicyInstallmentCount < 1)
                {
                    response.ErrorCode = "999";
                    response.Description = "Vade sayısı 0'dan büyük olmalı";
                    return response;
                }
            }
            if (this.policy.PolicyStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
            {
                //this.policy.PolicyStatus = ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString();
                //this.policy.EndorsementStatus = ((int)ProteinEnums.Status.AKTIF).ToString();

                if (string.IsNullOrEmpty(this.policy.CashPaymentType))
                {
                    response.ErrorCode = "999";
                    response.Description = "Peşin ödeme tipi girmelisiniz";
                    return response;
                }
                if (this.policy.PolicyInstallmentCount == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Vade sayısını girmelisiniz";
                    return response;
                }
                if (this.policy.PolicyInstallmentCount == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Vade sayısını girmelisiniz";
                    return response;
                }
                if (string.IsNullOrEmpty(this.policy.CashPaymentValue))
                {
                    response.ErrorCode = "999";
                    response.Description = "Peşin ödeme değerini girmelisiniz";
                    return response;
                }
                if (this.policy.DateOfIssue == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Tanzim tarihi bulunamadı";
                    return response;
                }
            }
            else
            {
                this.policy.EndorsementStatus = ((int)ProteinEnums.Status.PASIF).ToString();
                this.policy.PolicyStatus = ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString();
            }
            if (string.IsNullOrEmpty(this.policy.CashPaymentType))
            {
                this.policy.CashPaymentType = "0";
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.CashPaymentType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.CashPaymentType)
                    {
                        findHim = true;
                        break;
                    }
                }
                if (findHim)
                {
                    if (!string.IsNullOrEmpty(this.policy.CashPaymentValue))
                    {
                        if (((int)ProteinEnums.CashPaymentType.TUTAR).ToString() == this.policy.CashPaymentType)
                        {
                            decimal Dout;
                            if (decimal.TryParse(this.policy.CashPaymentValue, out Dout))
                            {
                                //decimal.Parse(this.policy.CashPaymentValue) > 0 &&
                                if (decimal.Parse(this.policy.CashPaymentValue) <= this.policy.PolicyPremium)
                                {
                                    //this.policy.CashAmount = (this.policy.PolicyPremium - decimal.Parse(this.policy.CashPaymentValue));
                                }
                                else
                                {
                                    response.ErrorCode = "999";
                                    response.Description = "Peşin ödeme değeri, poliçe priminden yüksek olamaz!";
                                    return response;
                                }
                            }
                            else
                            {
                                response.ErrorCode = "999";
                                response.Description = "Peşin ödeme değerini ondalık değerde girmelisiniz";
                                return response;
                            }
                        }
                        else if (((int)ProteinEnums.CashPaymentType.YUZDE).ToString() == this.policy.CashPaymentType)
                        {
                            double Dout;

                            if (double.TryParse(this.policy.CashPaymentValue, out Dout))
                            {
                                if (decimal.Parse(this.policy.CashPaymentValue) > 0 && decimal.Parse(this.policy.CashPaymentValue) < 100)
                                {
                                    //this.policy.CashAmount = this.policy.PolicyPremium - ((this.policy.PolicyPremium * decimal.Parse(this.policy.CashPaymentValue)) / 100);

                                }
                                else
                                {
                                    response.ErrorCode = "999";
                                    response.Description = "Peşin ödeme değerinini %1 ile %99 arasında bir değer girmelisiniz!";
                                    return response;
                                }
                            }
                            else
                            {
                                response.ErrorCode = "999";
                                response.Description = "Peşin ödeme değerini ondalık değerde girmelisiniz";
                                return response;
                            }
                        }
                    }
                    else
                    {
                        response.ErrorCode = "999";
                        response.Description = "Peşin ödeme değerini girmelisiniz";
                        return response;
                    }

                }
                else
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz peşin ödeme türü";
                    return response;
                }
            }

            if (this.policy.EndDate <= this.policy.StartDate)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe bitiş tarihi başlangıç tarihinden önce olamaz";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.InsurerType))
            {
                response.ErrorCode = "999";
                response.Description = "Sigorta ettiren tipi bulunamadı";
                return response;
            }
            else
            {
                var lookupData = LookupHelper.GetLookupByOrdinal(LookupTypes.Contact, policy.InsurerType.Trim());
                if (lookupData == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren tipi hatalı";
                    return response;
                }
            }
            if (this.policy.InsurerType == ((int)ContactType.GERCEK).ToString())
            {
                if (string.IsNullOrEmpty(this.policy.InsurerName))
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren isim bulunamadı";
                    return response;
                }
                if (string.IsNullOrEmpty(this.policy.InsurerLastName))
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren soyisim bulunamadı";
                    return response;
                }
                policy.InsurerTitle = policy.InsurerName + " " + policy.InsurerLastName;
                if (string.IsNullOrEmpty(this.policy.InsurerGender))
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren cinsiyet bulunamadı";
                    return response;
                }
                else
                {
                    var lookupData = LookupHelper.GetLookupByCode(LookupTypes.Gender, policy.InsurerGender.Trim());
                    if (lookupData == null)
                    {
                        response.ErrorCode = "999";
                        response.Description = "Sigorta ettiren cinsiyet hatalı";
                        return response;
                    }
                }
                if (this.policy.InsurerBirthDate == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren doğum tarihi bulunamadı";
                    return response;
                }
                if (string.IsNullOrEmpty(this.policy.InsurerNameOfFather))
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren baba adı bulunamadı";
                    return response;
                }
                if (this.policy.InsurerTcNo == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren TcNo bulunamadı";
                    return response;
                }
                else
                {
                    if (this.policy.InsurerTcNo.IsIdentityNoLength()==null)
                    {
                        response.ErrorCode = "999";
                        response.Description = "Sigorta ettiren TcNo 11 karakter olmak zorunda";
                        return response;
                    }
                }
                if (string.IsNullOrEmpty(this.policy.InsurerNationality))
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren uyruk bulunamadı";
                    return response;
                }
                else
                {
                    if (this.policy.InsurerNationality.ToUpper() != "TRK")
                    {
                        List<Country> lstCountry = new GenericRepository<Country>().FindBy($"A2_CODE = :code", parameters: new { code = new DbString { Value = policy.InsuredNationality } });
                        if (lstCountry.Count < 1)
                        {
                            response.ErrorCode = "999";
                            response.Description = "Sigorta ettiren uyruk bulunamadı";
                            return response;
                        }
                    }
                }
                if (string.IsNullOrEmpty(this.policy.InsurerAddress))
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren adres bulunamadı";
                    return response;
                }
                if (this.policy.InsurerCityCode == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren il kodu bulunamadı";
                    return response;
                }
                else
                {
                    string tmpCityCode = "";

                    for (int s1 = 3; s1 > this.policy.InsurerCityCode.ToString().Length; s1--)
                        tmpCityCode += "0";
                    tmpCityCode += this.policy.InsurerCityCode.ToString();

                    List<City> lstCity = new GenericRepository<City>().FindBy($"CODE =:code", parameters: new { code = new DbString { Value = tmpCityCode } });
                    if (lstCity.Count < 1)
                    {
                        response.ErrorCode = "999";
                        response.Description = "Sigorta ettiren il kodu bulunamadı";
                        return response;
                    }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(this.policy.InsurerName))
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren isim bulunamadı";
                    return response;
                }
                policy.InsurerTitle = policy.InsurerName;

                if (this.policy.InsurerVkn.IsTaxNumberLength()==null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren VKN Bilgisi Bulunamadı";
                    return response;
                }
                if (string.IsNullOrEmpty(this.policy.InsurerAddress))
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren adres bulunamadı";
                    return response;
                }
                if (this.policy.InsurerCityCode == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigorta ettiren il kodu bulunamadı";
                    return response;
                }
                else
                {
                    string tmpCityCode = "";

                    for (int s1 = 3; s1 > this.policy.InsurerCityCode.ToString().Length; s1--)
                        tmpCityCode += "0";
                    tmpCityCode += this.policy.InsurerCityCode.ToString();

                    List<City> lstCity = new GenericRepository<City>().FindBy($"CODE =:code", parameters: new { code = new DbString { Value = tmpCityCode } });
                    if (lstCity.Count < 1)
                    {
                        response.ErrorCode = "999";
                        response.Description = "Sigorta ettiren il kodu bulunamadı";
                        return response;
                    }
                }
            }
            if (!string.IsNullOrEmpty(this.policy.AdditionalProtocol))
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.RuleType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.AdditionalProtocol)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz ek protokol";
                    return response;
                }
            }
            if (!string.IsNullOrEmpty(this.policy.SpecialCondition))
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.RuleType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.SpecialCondition)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz özel şart";
                    return response;
                }
            }
            if (this.policy.InsuredFamilyNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Sigorta aile numarası bulunamadı";
                return response;
            }
            
            if (string.IsNullOrEmpty(this.policy.InsuredName))
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı isim kodu bulunamadı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.InsuredLastName))
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı soy isim kodu bulunamadı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.InsuredGender))
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı cinsiyet bulunamadı";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.Gender>();

                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.InsuredGender)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigortalı cinsiyet hatalı";
                    return response;
                }
            }
            if (this.policy.InsuredBirthDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı doğum tarihi bulunamadı";
                return response;
            }
            if (this.policy.InsuredCompanyEntranceDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı şirkete ilk giriş tarihi bulunamadı";
                return response;
            }
            if (this.policy.InsuredPackageId == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı plan kodu bulunamadı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.InsuredIndividualType))
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı yakınlık bulunamadı ";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.IndividualType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.InsuredIndividualType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz yakınlık türü";
                    return response;
                }
            }
            if (this.policy.InsuredInitialPremium == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı prim bilgisi bulunamadı";
                return response;
            }
            policy.InsuredEndorsementPremium = policy.InsuredInitialPremium;
            if (this.policy.InsuredPolicyEntryDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı poliçeye giriş tarihi bulunamadı";
                return response;
            }
            if (this.policy.InsuredTcNo == null)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı TcNo bulunamadı";
                return response;
            }
            else
            {
                if (this.policy.InsuredTcNo.IsIdentityNoLength()==null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigortalı TcNo 11 karakter olmak zorunda";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.InsuredNationality))
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı uyruk bulunamadı";
                return response;
            }
            else
            {
                if (this.policy.InsuredNationality.ToUpper() != "TRK")
                {
                    List<Country> lstCountry = new GenericRepository<Country>().FindBy($"A2_CODE =:code", parameters: new { code = new DbString { Value = policy.InsuredNationality } });
                    if (lstCountry.Count < 1)
                    {
                        response.ErrorCode = "999";
                        response.Description = "Sigortalı uyruk bulunamadı";
                        return response;
                    }
                }
            }
            if (!string.IsNullOrEmpty(this.policy.InsuredMaritalStatus))
            {
                if (this.policy.InsuredMaritalStatus == "B")
                { }
                else if (this.policy.InsuredMaritalStatus == "E") { }
                else
                {
                    response.ErrorCode = "999";
                    response.Description = "Medeni durum format hatası (E veya B)";
                    return response;
                }
            }
            if (!string.IsNullOrEmpty(this.policy.InsuredTransferType))
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.InsuredTransferType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.InsuredTransferType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz sigortali transfer türü";
                    return response;
                }
            }
            //if (this.policy.InsuredFirstInsuredDate == null)
            //{
            //    response.ErrorCode = "999";
            //    response.Description = "Sigortalı ilk sigortalanma tarihi bilgisi bulunamadı!";
            //    return response;
            //}
            //if (string.IsNullOrEmpty(this.policy.InsuredAddress))
            //{
            //    response.ErrorCode = "999";
            //    response.Description = "Sigortalı adres bilgisi bulunamadı!";
            //    return response;
            //}
            //if (this.policy.InsuredCityCode == 0)
            //{
            //    response.ErrorCode = "999";
            //    response.Description = "Sigortalı il kodu bulunamadı";
            //    return response;
            //}
            if (this.policy.InsuredCityCode > 0)
            {
                string tmpCityCode = "";

                for (int s1 = 3; s1 > this.policy.InsuredCityCode.ToString().Length; s1--)
                    tmpCityCode += "0";
                tmpCityCode += this.policy.InsuredCityCode.ToString();

                List<City> lstCity = new GenericRepository<City>().FindBy($"CODE =:code", parameters: new { code = new DbString { Value = tmpCityCode } });
                if (lstCity.Count < 1)
                {
                    response.ErrorCode = "999";
                    response.Description = "Sigortalı il kodu bulunamadı";
                    return response;
                }
            }
            if (this.policy.EndorsementNo != 0)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl no 0 olmalı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                string zeylCode = EnumHelper.GetValues(ProteinEnums.EndorsementType.BASLANGIC)[1];
                if (this.policy.EndorsementType != zeylCode)
                {
                    response.ErrorCode = "999";
                    response.Description = "Zeyl türü hatalı. Başlangıc zeyli olmalı";
                    return response;
                }
            }
            if (this.policy.EndorsementStartDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl başlama tarihi bulunamadı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementCategory))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl kategorisi bulunamadı 0 = POLİÇE İÇİN , 1 = SİGORTALI İÇİN";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementCategory>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.EndorsementCategory)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl kategorisi ";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.EndorsementTypeDescriptionType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl Tipinin türü bulunamadı. 0 = POLICE BASLANGICI, 1 = GUN ESASLI ";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementTypeDescriptionType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Code == this.policy.EndorsementTypeDescriptionType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl tipinin türü";
                    return response;
                }
            }
            #endregion
            #region Logic Valid

            var PolicyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });

            if (this.policy.InsuredIndividualType != "F")
            {
                if (PolicyExists.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçe no sistemde bulunamadı. Önce poliçe oluşturmalısınız!";
                    return response;
                }
                this.policy.PolicyId = (long)PolicyExists[0].POLICY_ID;

            }
            if (this.policy.InsuredIndividualType == "F")
            {
                Thread.Sleep(100);
                if (PolicyExists.Count > 0)
                {
                    var InsuredExists = new GenericRepository<V_InsuredEndorsement>().FindBy("POLICY_ID =:policyId and INDIVIDUAL_TYPE =:indType and FAMILY_NO =:familyNo ", orderby: "POLICY_ID DESC,ENDORSEMENT_ID DESC,ENDORSEMENT_NO DESC", parameters: new { policyId = PolicyExists[0].POLICY_ID, indType = new DbString { Value = LookupHelper.GetLookupByCode(Constants.LookupTypes.Individual, this.policy.InsuredIndividualType).Ordinal, Length = 3 }, familyNo = policy.InsuredFamilyNo });
                    if (InsuredExists.Count > 0)
                    {
                        response.ErrorCode = "999";
                        response.Description = "Poliçede aynı ailede sadece bir fert olabilir";
                        return response;
                    }
                    Thread.Sleep(100);
                    var InsuredExistNonFamilyNo = new GenericRepository<V_InsuredEndorsement>().FindBy("POLICY_ID =:policyId and INDIVIDUAL_TYPE =:indType", orderby: "POLICY_ID DESC,ENDORSEMENT_ID DESC,ENDORSEMENT_NO DESC", parameters: new { policyId = PolicyExists[0].POLICY_ID, indType = new DbString { Value = LookupHelper.GetLookupByCode(Constants.LookupTypes.Individual, this.policy.InsuredIndividualType).Ordinal, Length = 3 }});
                    if (InsuredExistNonFamilyNo.Count > 0 && this.policy.PolicyType == ((int)ProteinEnums.PolicyType.GRUP).ToString())
                    {
                        this.policy.PolicyId = (long)InsuredExistNonFamilyNo[0].POLICY_ID;
                        this.policy.EndorsementId = InsuredExistNonFamilyNo[0].ENDORSEMENT_ID;
                    }
                    else if (this.policy.PolicyType == ((int)ProteinEnums.PolicyType.FERDI).ToString())
                    {
                        response.ErrorCode = "999";
                        response.Description = "Ferdi poliçede sadece 1 aile olabilir";
                        return response;

                    }
                }
            }
            else if (this.policy.InsuredIndividualType == "A" || this.policy.InsuredIndividualType == "B" || this.policy.InsuredIndividualType == "E")
            {
                //Fert kontrolü
                Thread.Sleep(100);
                var InsuredExists = new GenericRepository<V_InsuredEndorsement>().FindBy("POLICY_ID =:policyId and INDIVIDUAL_TYPE =:indType and FAMILY_NO =:familyNo ", orderby: "POLICY_ID DESC,ENDORSEMENT_ID DESC,ENDORSEMENT_NO DESC", parameters: new { policyId = PolicyExists[0].POLICY_ID, indType = new DbString { Value = ((int)ProteinEnums.IndividualType.FERT).ToString(), Length = 3 }, familyNo = policy.InsuredFamilyNo });
                Thread.Sleep(100);
                //eş,cocuk,baba,anne kontrolü
                var InsuredExistsIndividual = new GenericRepository<V_InsuredEndorsement>().FindBy("POLICY_ID =:policyId and INDIVIDUAL_TYPE =:indType and FAMILY_NO =:familyNo ", orderby: "POLICY_ID DESC,ENDORSEMENT_ID DESC,ENDORSEMENT_NO DESC", parameters: new { policyId = PolicyExists[0].POLICY_ID, indType = new DbString { Value = LookupHelper.GetLookupByCode(Constants.LookupTypes.Individual, this.policy.InsuredIndividualType).Ordinal, Length = 3 }, familyNo = policy.InsuredFamilyNo });
                if (InsuredExists.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçeye eş, çocuk, anne,baba eklemek için önce fert eklemelisiniz!";
                    return response;
                }
                else if (InsuredExistsIndividual.Count > 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçede aynı ailede sadece bir adet anne,baba,eş içerebilir";
                    return response;
                }

                this.policy.EndorsementId = InsuredExists[0].ENDORSEMENT_ID;
                this.policy.EndorsementNo = InsuredExists[0].ENDORSEMENT_NO;
            }
            else if (this.policy.InsuredIndividualType == "C" || this.policy.InsuredIndividualType == "D")
            {
                Thread.Sleep(100);
                var InsuredExists = new GenericRepository<V_InsuredEndorsement>().FindBy("POLICY_ID =:policyId and INDIVIDUAL_TYPE =:indType and FAMILY_NO =:familyNo ", orderby: "POLICY_ID DESC,ENDORSEMENT_ID DESC,ENDORSEMENT_NO DESC", parameters: new { policyId = PolicyExists[0].POLICY_ID, indType = new DbString { Value = ((int)ProteinEnums.IndividualType.FERT).ToString(), Length = 3 }, familyNo = policy.InsuredFamilyNo });

                if (InsuredExists.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçeye çocuk veya diğer için önce fert eklemelisiniz!";
                    return response;
                }
                this.policy.EndorsementId = InsuredExists[0].ENDORSEMENT_ID;
                this.policy.EndorsementNo = InsuredExists[0].ENDORSEMENT_NO;
            }
            //if (this.policy.RenewalNo > 1)
            //{
            //    Thread.Sleep(100);
            //    var policyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { RenewalNo=(policy.RenewalNo-1), PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });
            //    if (policyExists.Count == 0)
            //    {
            //        response.ErrorCode = "999";
            //        response.Description = "Bu yenileme poliçesinin kaydı bulunamadı";
            //        return response;
            //    }
            //}
            //response = InsuredSameBranch(this.policy);

            List<V_Package> lstpck = new GenericRepository<V_Package>().FindBy("PACKAGE_NO =:packageNo AND COMPANY_ID=:CompanyId", parameters: new { packageNo = new DbString { Value = policy.InsuredPackageId.ToString(), Length = 100 }, policy.CompanyId }, orderby: "");
            if (lstpck == null || lstpck.Count < 1)
            {
                response.ErrorCode = "999";
                response.Description = "Sigortalı plan kodu hatalı ";
                return response;
            }
            else
            {
                if (lstpck.FirstOrDefault().SUBPRODUCT_ID == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Subproduct kodu bulunamadı.Plan kodunu kontrol ediniz!";
                    return response;
                }
                else
                {
                    this.policy.SubProductId = lstpck.FirstOrDefault().SUBPRODUCT_ID;
                    this.policy.InsuredPackageId = lstpck.FirstOrDefault().PACKAGE_ID;
                }
            }
            #endregion
            //this.policy.IsSagmerPolicySend = true;
            #region Mapping
            this.policy.EndorsementType = ((int)ProteinEnums.EndorsementType.BASLANGIC).ToString();
            this.policy.EndorsementStartDate = this.policy.StartDate;
            //this.policy.EndorsementNo = 0;

            #endregion
            return response;
        }
        public PolicyValidatorResponse LocationChangeEndorsement()
        {
            PolicyValidatorResponse response = new PolicyValidatorResponse();
            response.ErrorCode = "100";

            #region Basic valid
            if (string.IsNullOrEmpty(this.policy.PolicyStatus))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe / Zeyl Durumunu belirtmelisiniz! ( 0 = TEKLIF, 2 = TANZIM)";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.PolicyStatus>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.PolicyStatus)
                    {
                        if (enVal.Text == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString()
                          || enVal.Text == ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString())
                            findHim = true;
                        break;
                    }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz poliçe status türü";
                    return response;
                }
            }
            if (this.policy.PolicyStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
            {

                if (this.policy.DateOfIssue == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Tanzim tarihi bulunamadı";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.PolicyNo))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no bulunamadı";
                return response;
            }
            else
            {
                this.policy.PolicyNo = this.policy.PolicyNo;

                if (string.IsNullOrEmpty(this.policy.PolicyNo))
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçe no bulunamadı";
                    return response;
                }
            }

            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.EndorsementType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl türü";
                    return response;
                }
            }
            if (this.policy.AgencyId == null)
            {
                response.ErrorCode = "999";
                response.Description = "Acente no bulunamadı";
                return response;
            }
            if (this.policy.CompanyId == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Şirket bilgisi bulunamadı";
                return response;
            }
            if (this.policy.CompanyId > 0 && string.IsNullOrEmpty(this.policy.AgencyId))
            {
                var agency = new GenericRepository<V_Agency>().FindBy("COMPANY_ID =:CompanyId and AGENCY_NO =:agencyNo", parameters: new { CompanyId = policy.CompanyId, agencyNo = new DbString { Value = policy.AgencyId, Length = 20 } }, orderby: "");
                if (agency.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Acente şirket eşleşmesi hatalı";
                    return response;
                }
            }
            if (this.policy.RenewalNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Yenileme no bulunamadı";
                return response;
            }
            if (this.policy.EndorsementStartDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl başlama tarihi bulunamadı";
                return response;
            }
            if (this.policy.DateOfIssue == null)
            {
                response.ErrorCode = "999";
                response.Description = "Tanzim tarihi bulunamadı";
                return response;
            }
            if (string.IsNullOrEmpty(this.policy.LocationType))
            {
                response.ErrorCode = "999";
                response.Description = "Cografi kapsam bulunamadı";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.LocationType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.LocationType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Cografi Kapsam: 1-YURTİÇİ, 2- YURTDIŞI/TÜM DÜNYA, 3-ABD KANADA HARİÇ TÜM DÜNYA, 4-TÜM AVRUPA ÜLKELERİ, 5-ABD VE KANADA, 6-SCHENGEN ÜLKELERİ,7-ABD KANADA JAPONYA HARİÇ TÜM DÜNYA";
                    return response;
                }
            }
            if (this.policy.EndorsementNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl no 0 olmamalı";
                return response;
            }
            #endregion
            Thread.Sleep(100);
            #region Logic valid
            var PolicyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString(), ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString() } });

            if (PolicyExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no sistemde bulunamadı. ";
                return response;
            }

            this.policy.PolicyId = (long)PolicyExists[0].POLICY_ID;


            if (!response.IsSucces) return response;
            if (this.policy.PolicyId < 1)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe kaydı bulunamadı!";
                return response;
            }
            if (!PolicyExists[0].BRANCH_NAME.ToUpper().Contains("SEYAHAT"))
            {
                response.ErrorCode = "999";
                response.Description = "Cografi kapsam degisikligi  sadece seyahat poliçeleri için geçerlidir!";
                return response;
            }
            var EndorsementExists = new GenericRepository<V_PolicyEndorsement>().FindBy("POLICY_ID =:PolicyId", parameters: new { policy.PolicyId }, orderby: "");
            if (EndorsementExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Bu poliçenin mevcut zeyl bilgisi bulunamadı.";
                return response;
            }

            foreach (V_PolicyEndorsement policyEndors in EndorsementExists)
            {
                if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo && policyEndors.ENDORSEMENT_TYPE != ((int)ProteinEnums.EndorsementType.GIDECEGI_ULKE_DEGISIKLIGI).ToString())
                {
                    response.ErrorCode = "999";
                    response.Description = "Aynı zeyil no hatası";
                    return response;
                }
                else if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo)
                    this.policy.EndorsementId = policyEndors.ENDORSEMENT_ID;
            }
            response = PolicyIsIssued(this.policy.PolicyId);
            if (!response.IsSucces) return response;
            if (this.policy.EndorsementId == 0)
                response = EndorsementIssueCheck(this.policy.PolicyId);
            #endregion

            #region Mapping
            this.policy.EndorsementType = ((int)ProteinEnums.EndorsementType.GIDECEGI_ULKE_DEGISIKLIGI).ToString();
            this.policy.EndorsementStartDate = this.policy.StartDate;
            #endregion
            return response;
        }
        public PolicyValidatorResponse ValidityTurn() //Meriyete dönüş
        {
            PolicyValidatorResponse response = new PolicyValidatorResponse();
            response.ErrorCode = "100";
            #region Basic valid
            if (string.IsNullOrEmpty(this.policy.PolicyStatus))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe / Zeyl Durumunu belirtmelisiniz! ( 0 = TEKLIF, 2 = TANZIM)";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.PolicyStatus>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.PolicyStatus)
                    {
                        if (enVal.Text == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString()
                          || enVal.Text == ((int)ProteinEnums.PolicyStatus.TEKLIF).ToString())
                            findHim = true;
                        break;
                    }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz poliçe status türü";
                    return response;
                }
            }
            if (this.policy.PolicyStatus == ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString())
            {
                if (this.policy.DateOfIssue == null)
                {
                    response.ErrorCode = "999";
                    response.Description = "Tanzim tarihi bulunamadı";
                    return response;
                }
            }
            if (string.IsNullOrEmpty(this.policy.PolicyNo))
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no bulunamadı";
                return response;
            }
            else
            {
                this.policy.PolicyNo = this.policy.PolicyNo;

                if (string.IsNullOrEmpty(this.policy.PolicyNo))
                {
                    response.ErrorCode = "999";
                    response.Description = "Poliçe no bulunamadı";
                    return response;
                }
            }

            if (string.IsNullOrEmpty(this.policy.EndorsementType))
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl türü bulunamadı";
                return response;
            }
            else
            {
                List<EnumValue> enumValuess = EnumHelper.GetValues<ProteinEnums.EndorsementType>();
                bool findHim = false;
                foreach (EnumValue enVal in enumValuess)
                {
                    if (enVal.Text == this.policy.EndorsementType)
                    { findHim = true; break; }
                }
                if (!findHim)
                {
                    response.ErrorCode = "999";
                    response.Description = "Geçersiz Zeyl türü";
                    return response;
                }
            }
            if (this.policy.AgencyId == null)
            {
                response.ErrorCode = "999";
                response.Description = "Acente no bulunamadı";
                return response;
            }
            if (this.policy.CompanyId == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Şirket bilgisi bulunamadı";
                return response;
            }
            if (this.policy.CompanyId > 0 && string.IsNullOrEmpty(this.policy.AgencyId))
            {
                var agency = new GenericRepository<V_Agency>().FindBy("COMPANY_ID =:CompanyId and AGENCY_NO =:agencyNo", parameters: new { CompanyId = policy.CompanyId, agencyNo = new DbString { Value = policy.AgencyId, Length = 20 } }, orderby: "");
                if (agency.Count == 0)
                {
                    response.ErrorCode = "999";
                    response.Description = "Acente şirket eşleşmesi hatalı";
                    return response;
                }
            }
            if (this.policy.RenewalNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Yenileme no bulunamadı";
                return response;
            }
            if (this.policy.EndorsementStartDate == null)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl başlama tarihi bulunamadı";
                return response;
            }
            if (this.policy.DateOfIssue == null)
            {
                response.ErrorCode = "999";
                response.Description = "Tanzim tarihi bulunamadı";
                return response;
            }
            if (this.policy.EndorsementNo == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Zeyl no 0 olmamalı";
                return response;
            }
            #endregion
            Thread.Sleep(100);
            #region Logic valid
            var PolicyExists = new GenericRepository<V_Policy>().FindBy("RENEWAL_NO =:RenewalNo AND POLICY_NUMBER =:PolicyNo and COMPANY_ID =:CompanyId and STATUS IN :statusList", orderby: "POLICY_ID DESC", parameters: new { policy.RenewalNo, PolicyNo = new DbString { Value = policy.PolicyNo, Length = 20 }, policy.CompanyId, statusList = new[] { ((int)ProteinEnums.PolicyStatus.IPTAL).ToString() } });

            if (PolicyExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe no sistemde bulunamadı veya poliçe iptal durumunda değil! ";
                return response;
            }
            this.policy.PolicyId = (long)PolicyExists[0].POLICY_ID;

            if (!response.IsSucces) return response;
            if (this.policy.PolicyId < 1)
            {
                response.ErrorCode = "999";
                response.Description = "Poliçe kaydı bulunamadı!";
                return response;
            }

            var EndorsementExists = new GenericRepository<V_PolicyEndorsement>().FindBy($"POLICY_ID =:PolicyId", parameters: new { policy.PolicyId }, orderby: "");
            if (EndorsementExists.Count == 0)
            {
                response.ErrorCode = "999";
                response.Description = "Bu poliçenin mevcut zeyl bilgisi bulunamadı.";
                return response;
            }

            foreach (V_PolicyEndorsement policyEndors in EndorsementExists)
            {
                if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo && policyEndors.ENDORSEMENT_TYPE != ((int)ProteinEnums.EndorsementType.MERIYETE_DONUS_ZEYLI).ToString())
                {
                    response.ErrorCode = "999";
                    response.Description = "Aynı zeyil no hatası";
                    return response;
                }
                else if (policyEndors.ENDORSEMENT_NO == this.policy.EndorsementNo)
                    this.policy.EndorsementId = policyEndors.ENDORSEMENT_ID;
            }
            response = PolicyIsIssued(this.policy.PolicyId);
            if (!response.IsSucces) return response;
            if (this.policy.EndorsementId == 0)
                response = EndorsementIssueCheck(this.policy.PolicyId);
            #endregion
            #region Mapping
            this.policy.EndorsementType = ((int)ProteinEnums.EndorsementType.MERIYETE_DONUS_ZEYLI).ToString();
            this.policy.EndorsementStartDate = this.policy.StartDate;

            #endregion
            return response;
        }
        #region Common 
        private PolicyValidatorResponse PolicyIsIssued(long PolicyId)
        {
            PolicyValidatorResponse response = new PolicyValidatorResponse();
            response.ErrorCode = "100";

            List<V_Policy> existPol = new GenericRepository<V_Policy>().FindBy($"POLICY_ID =:PolicyId and STATUS!=:status", parameters: new { PolicyId, status = new DbString { Value = ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString(), Length = 3 } }, orderby: "");

            if (existPol.Count > 0)
            {
                response.Description = "Tanzimsiz bir poliçeye başlangıç zeyili dışında zeyil ekleyemezsiniz";
                response.ErrorCode = "-1";
            }
            return response;
        }
        private PolicyValidatorResponse InsuredSameBranch(PolicyDto _policy)
        {
            PolicyValidatorResponse response = new PolicyValidatorResponse();
            response.ErrorCode = "100";
            var PolicyExists = new GenericRepository<V_Policy>().FindBy($"COMPANY_ID =:CompanyId and RENEWAL_NO =:RenewalNo and POLICY_NUMBER =:PolicyNo", orderby: "", parameters: new { _policy.CompanyId, _policy.RenewalNo, _policy.PolicyNo });
            if (PolicyExists.Count > 0)
            {
                var InsuredExists = new GenericRepository<V_InsuredEndorsement>().FindBy(conditions: $"COMPANY_ID =:CompanyId and RENEWAL_NO =:RenewalNo and POLICY_NUMBER =:PolicyNo and POLICY_STATUS !=:polStatus", parameters: new { _policy.CompanyId, _policy.RenewalNo, _policy.PolicyNo, polStatus = new DbString { Value = ((int)ProteinEnums.PolicyStatus.SILINDI).ToString(), Length = 3 } }, orderby: "POLICY_ID DESC,ENDORSEMENT_ID DESC,ENDORSEMENT_NO DESC");

                foreach (V_InsuredEndorsement _insEndors in InsuredExists)
                {
                    if (new GenericRepository<V_Package>().FindBy($"PACKAGE_ID =:packageId AND COMPANY_ID=:CompanyId", parameters: new { packageId = _insEndors.PACKAGE_ID, _policy.CompanyId }, orderby: "").FirstOrDefault().SUBPRODUCT_ID != PolicyExists.FirstOrDefault().SUBPRODUCT_ID)
                    {
                        response.Description = "Sigortalılar aynı branş olmalı ";
                        response.ErrorCode = "-1";
                        break;
                    }
                }
            }

            return response;
        }
        private PolicyValidatorResponse EndorsementIssueCheck(long PolicyId)
        {
            PolicyValidatorResponse response = new PolicyValidatorResponse();
            response.ErrorCode = "100";

            var PolicyExists = new GenericRepository<V_Policy>().FindBy("POLICY_ID =:PolicyId", parameters: new { PolicyId }, orderby: "");

            if (PolicyExists.Count > 0)
            {
                if (PolicyExists.FirstOrDefault().LastEndorsementId!=null && PolicyExists.FirstOrDefault().LastEndorsementId > 0)
                {
                    var endorsement = new GenericRepository<Endorsement>().FindById((long)PolicyExists.FirstOrDefault().LastEndorsementId);
                    if (endorsement!=null)
                    {
                        if (endorsement.Status != ((int)ProteinEnums.Status.AKTIF).ToString())
                        {
                            response.ErrorCode = "-1";
                            response.Description = "Bir önceki zeyl tanzimli olmadığından yeni bir zeyl ekleyemezsiniz";
                        }
                    }
                }
            }
            return response;
        }
        private long GetExchangeRateID(string CurrencyType) //örn: 1
        {
            long RateID = 0;

            ExchangeRate exchangeRate = new GenericRepository<ExchangeRate>().FindBy($"CURRENCY_TYPE =:curType", parameters: new { curType = new DbString { Value = CurrencyType, Length = 3 } }, orderby: "RATE_DATE DESC").FirstOrDefault();
            if (exchangeRate != null)
            {
                if (!string.IsNullOrEmpty(exchangeRate.CurrencyType))
                {
                    RateID = exchangeRate.Id;
                }
            }
            return RateID;
        }
        //private PolicyValidatorResponse FamilyNoCheck(long PolicyId, string FamilyNo, string IndividualType)
        //{
        //    PolicyValidatorResponse response = new PolicyValidatorResponse();
        //    response.ErrorCode = "100";

        //    List<V_Policy> existPol = new GenericRepository<V_Policy>().FindBy($"POLICY_ID = {PolicyId}", fetchHistoricRows: true, orderby: "");

        //    if (existPol.Count > 0)
        //    {
        //        if (IndividualType != EnumHelper.GetValues(ProteinEnums.IndividualType.FERT)[1])

        //    }
        //    return response;
        //}
        #endregion
    }

    public class PolicyValidatorResponse
    {
        public bool IsSucces { get { return (this.ErrorCode == "100" ? true : false); } }
        public string Description { get; set; }
        public string ErrorCode { get; set; }
    }
}
