﻿using Protein.Common.Dto.ImportObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Common.Dto
{
    public class CommonPolicyDto
    {
        public bool isPolicyChange { get; set; } = false;
        public bool isEndorsementChange { get; set; } = false;
        public bool isPolicyTransfer { get; set; } = false;
        public bool IsAdditionalProtocolChange { get; set; } = false;
        public bool IsSpeacialCondition { get; set; } = false;
        public bool IsSagmerPolicySend { get; set; } = false;
        public bool IsSagmerPolicySendForProd { get; set; } = false;
        public bool IsSagmerInsuredEntrySend { get; set; } = false;
        public bool IsSagmerInsuredExitSend { get; set; } = false;
        public bool IsSagmerInsurerChangeSend { get; set; } = false;
        public bool isInsurerChange { get; set; } = false;
        public bool isInsuredChange { get; set; } = false;
        public bool isInsuredNoteChange { get; set; } = false;
        public bool isInsuredContactChange { get; set; } = false;
        public bool isInsuredParameterChange { get; set; } = false;
        public bool isInsuredExitChange { get; set; } = false;
        public bool isInsuredTransferChange { get; set; } = false;
        public bool isInsertInstallment { get; set; } = false;

        public InsurerDto insurer { get; set; }
    }
    public class InsurerDto
    {
        public ContactDto contact { get; set; }
        public PersonDto person { get; set; }
        public CorporateDto corporate { get; set; }
        public AddressDto address { get; set; }
        public PhoneDto mobilePhone { get; set; }
        public PhoneDto businessPhone { get; set; }
        public EmailDto email { get; set; }
        public BankAccountDto bankAccount { get; set; }

        public Int64 contactId { get; set; }
        public Int64 personId { get; set; }
        public Int64 corporateId { get; set; }
        public Int64 mobilePhoneId { get; set; }
        public Int64 businessPhoneId { get; set; }
        public Int64 emailId { get; set; }
        public Int64 bankAccountId { get; set; }
        public Int64 addressId { get; set; }
    }
    public class ContactDto
    {
        public ContactType? type { get; set; }
        public string title { get; set; }
        public Int64? taxNumber { get; set; }
        public string taxOffice { get; set; }
        public Int64? identityNo { get; set; }
    }
    public class PersonDto
    {
        public Int64 id { get; set; }
        public ProfessionType? professionType { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public DateTime? birtdate { get; set; }
        public Gender? gender { get; set; }
        public Int64? nationalityId { get; set; }
        public string licenseNo { get; set; }
        public string birtplace { get; set; }
        public string nameOfFather { get; set; }
        public string passportNo { get; set; }
        public MaritalStatus? maritalStatus { get; set; }
        public string isVip { get; set; }
        public string vipType { get; set; }
        public string vipText { get; set; }
    }
    public class CorporateDto
    {
        public CorporateType? type { get; set; }
        public string name { get; set; }
    }
    public class AddressDto
    {
        public AddressType? type { get; set; }
        public Int64? cityId { get; set; }
        public Int64? countyId { get; set; }
        public string details { get; set; }
        public string zip_code { get; set; }
        public string district { get; set; }
    }
    public class BankAccountDto
    {
        public Int64? bankBranchId { get; set; }
        public Int64 bankId { get; set; }
        public CurrencyType? currencyType { get; set; }
        public string name { get; set; }
        public string iban { get; set; }
        public string no { get; set; }
    }
    public class PhoneDto
    {
        public PhoneType? type { get; set; }
        public Int64? countryId { get; set; }
        public string no { get; set; }
    }
    public class EmailDto
    {
        public EmailType? type { get; set; }
        public string details { get; set; }
    }
}
