﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Common.Dto.MediaObjects
{
    public class MediaDto
    {
        public HttpPostedFileBase MEDIA_PATH { get; set; }
        public string FileName { get; set; }

        public string Name { get; set; }
        /// <summary>
        /// örn: T_PRODUCT_MEDIA tablosu için  ID kolonu
        /// </summary>
        public long BaseId { get; set; }
        /// <summary>
        /// örn: T_PRODUCT_MEDIA için  CONTRACT_ID
        /// </summary>
        public long ObjectId { get; set; }
        /// <summary>
        /// örn: T_PRODUCT_MEDIA için MEDIA_ID
        /// </summary>
        public long MediaId { get; set; }
    }
}
