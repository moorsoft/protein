﻿using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Dto.MediaObjects
{
    class CustomDateTimeConverter : IsoDateTimeConverter
    {
        public CustomDateTimeConverter()
        {
            base.DateTimeFormat = "yyyy-MM-dd";
        }
    }
    [Serializable]
    public class MediaListDto
    {
        public long BaseId { get; set; } = 0;
        public long ObjectId { get; set; } = 0;
        public long MediaId { get; set; } = 0;
        public string Name { get; set; }
        public Byte[] FileContent { get; set; }
        public string FileName { get; set; }

    }
}
