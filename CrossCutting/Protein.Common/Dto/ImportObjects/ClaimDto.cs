﻿
using Protein.Common.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Dto.ImportObjects
{
    public class ProcessListDto
    {
        public decimal Requested { get; set; }
        public decimal BillNo { get; set; }
        public decimal BillDate { get; set; }
        public decimal BillTotalAmount { get; set; }
        public string ProcessCode { get; set; }
        public decimal OutOfScopeAmount { get; set; } //kapsam dışı
        public decimal ParticipationAmount { get; set; } // katılım tutarı
        public decimal ExemptionAmount { get; set; } //muafiyet tutarı
        public decimal Paid { get; set; } //ödenecek
        public decimal SgkAmount { get; set; }
        public string CoverageCode { get; set; }
        public string CoveragePackageNo { get; set; }
        public string CoverageVariation { get; set; }
        public string CoverageTypeText { get; set; }

    }
    public class ICDDto
    {
        public string ICDCode { get; set; }
        public string ICDType { get; set; }
        public string ICDTypeCode { get; set; }
    }
    public class BillDto
    {
        public string No { get; set; }
        public DateTime Date { get; set; }
        public string CurrencyCode { get; set; }
        public decimal TotalPaid { get; set; }
        public decimal TotalStoppage { get; set; }
        public string CorporateTypeText { get; set; }
        public DateTime? PaymentDate { get; set; }
    }

    public class ClaimDto : IImportObject
    {
        public List<ProcessListDto> Processes { get; set; }
        public List<ICDDto> ICD { get; set; }
        public BillDto Bill { get; set; }
        public Int64 ProviderId { get; set; }
        public DateTime? ExitDate { get; set; }
        public string DoctorName { get; set; }
        public string DoctorLastName { get; set; }
        public string DoctorTitle { get; set; }
        public Int64? DoctorTCNO { get; set; }
        public string DoctorDiplomaNo { get; set; }

        [Column("Hasar No")]
        [Required]
        public Int64 ClaimId { get; set; }
        public Int64 CompanyId { get; set; }
        public Int64 InsuredId { get; set; }
        public string PaymentMethodText { get; set; }
        public string PaymentMethodCode { get; set; }
        public string PaymentMethod { get; set; }
        [Column("Tarih")]
        public DateTime? PaymentDate { get; set; }
        public DateTime? ClaimDate { get; set; }
        public Int64? PolicyNo { get; set; }
        public string StatusText { get; set; }
        public string CompanyClaimId { get; set; }
        public string EpikrizNote { get; set; }
        public string ReasonText { get; set; }
        public string Complaint { get; set; }
        public Int64? InsuredIdentityNo { get; set; }
        public string InsuredFirstName { get; set; }
        public string InsuredLastName { get; set; }
        public Int64? ProviderIdentityNo { get; set; }
        public string ProviderName { get; set; }
        public string ProviderCityCode { get; set; }
        public string ProviderCountyCode { get; set; }
        public DateTime? EntranceDate { get; set; }
        public Int64? RenewalNo { get; set; }
        public string ClaimCity { get; set; }
        public string ClaimCounty { get; set; }
        public DateTime? NoticeDate { get; set; }
        public string InsuredIban { get; set; }
        [Column("Tutar")]
        public decimal Paid { get; set; }
        [Column("Statü")]
        [Required]
        public string ClaimStatus { get; set; }
    }
}
