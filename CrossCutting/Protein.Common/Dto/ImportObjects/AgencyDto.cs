﻿using Protein.Common.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Dto.ImportObjects
{
    public class AgencyDto : IImportObject
    {
        #region Besim
        [Column("Contact Id")]
        public Int64 ContactId { get; set; } = 0;
        [Column("Address Id")]
        public Int64 AddressId { get; set; } = 0;
        [Column("Agency Id")]
        public Int64 AgencyId { get; set; } = 0;

        [Column("Mobile Phone Id")]
        public Int64 MobilePhoneId { get; set; } = 0;
        [Column("Agency MobilePhone Id")]
        public Int64 AgencyMobilePhoneId { get; set; } = 0;

        [Column("Phone Id")]
        public Int64 PhoneId { get; set; } = 0;
        [Column("Agency Phone Id")]
        public Int64 AgencyPhoneId { get; set; } = 0;

        [Column("Fax Phone Id")]
        public Int64 FaxPhoneId { get; set; } = 0;
        [Column("Agency FaxPhone Id")]
        public Int64 AgencyFaxPhoneId { get; set; } = 0;

        [Column("Posta Kodu")]
        public string ZipCode { get; set; }

        [Column("Yetkili Adı")]
        public string ContactFirstName { get; set; }
        [Column("Yetkili Soyadı")]
        public string ContactLastName { get; set; }
        [Column("Yetkili Telefonu")]
        public string ContactPhone { get; set; }
        #endregion

        [Column("Acente No")]
        [Required]
        public string No { get; set; }
        [Column("Şirket No")]
        [Required]
        public Int64 CompanyId { get; set; }
        [Column("Tip")]
        [Required]
        public string Type { get; set; }
        [Column("Bölge Kod")]
        public string RegionCode { get; set; }
        [Column("Bölge Ad")]
        public string RegionName { get; set; }

        // Contact
        [Column("Acente Ünvanı")]
        public string AgencyTitle { get; set; }
        [Column("İl")]
        public string City { get; set; }
        [Column("İlçe")]
        public string County { get; set; }
        [Column("Levha No")]
        public string PlateNo { get; set; }
        [Column("Telefon")]
        public string PhoneNo { get; set; }
        [Column("Faks")]
        public string FaxNo { get; set; }
        [Column("Cep")]
        public string MobileNo { get; set; }
        [Column("Kuruluş")]
        public DateTime? EstablishmentDate { get; set; }
        [Column("Aktif/Pasif")]
        public string ActivePasive { get; set; }
        [Column("Açık/Kapalı")]
        public string IsOpen { get; set; }
        [Column("Kapanış Tarihi")]
        public DateTime? CloseDownDate { get; set; }
        [Column("Adres")]
        public string Address { get; set; }
        [Column("Vergi Kimlik No")]
        public Int64? TaxNumber { get; set; }
        [Column("Vergi Dairesi")]
        public string TaxOffice { get; set; }

        public string CityCode { get; set; }
        public string CountyCode { get; set; }

    }
}
