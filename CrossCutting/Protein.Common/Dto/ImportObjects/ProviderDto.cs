﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Dto.ImportObjects
{
    public class ProviderDto
    {
        public ProviderAddress AddressInfo { get; set; }
        public string AuthenticationKey { get; set; }
        public ProviderBank BankAccountInfo { get; set; }
        public long CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string CompanyNote { get; set; }
        public string CompanyType { get; set; }
        public ProviderContact ContactInfo { get; set; }
        public string EInvoice { get; set; } //E-Fatura : E-H
        public LegalEntity LegalEntityInfo { get; set; }
        public int State { get; set; }
    }

    public class LegalEntity
    {
        public ProviderAddress AddressInfo { get; set; }
        public AuthorizedPerson AuthorizedPerson { get; set; }
        public string CompanyTitle { get; set; }
        public ProviderContact ContactInfo { get; set; }
        public string MersisNo { get; set; }
        public string MainCompanyCode { get; set; }
        public Int64? TaxNo { get; set; }
        public string TaxOffice { get; set; }
    }

    public class ProviderContact
    {
        public string EmailAddress { get; set; }
        public string LandPhone { get; set; }
        public string MobilePhone { get; set; }
        public string WebSite { get; set; }
    }

    public class AuthorizedPerson
    {
        public ProviderAddress AddressInfo { get; set; }
        public DateTime? BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public ProviderContact ContactInfo { get; set; }
        public string FatherName { get; set; }
        public string Gender { get; set; }
        public string Name { get; set; }
        public string Nation { get; set; }
        public string PassportNo { get; set; }
        public string Surname { get; set; }
        public Int64? TCKN { get; set; }
        public Int64? VKN { get; set; }


    }

    public class ProviderBank
    {
        public string AccountHolderName { get; set; }
        public string IBAN { get; set; }

    }

    public class ProviderAddress
    {
        public string Details { get; set; }
        public string CountryCode { get; set; }
        public string DistrictCode { get; set; }
        public string PostCode { get; set; }
        public string ProvinceCode { get; set; }

    }
}
