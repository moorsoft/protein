﻿using Protein.Common.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Dto.ImportObjects
{
    public class DoctorImportDto : IImportObject
    {
        [Column("BOLUMBRANS")]
        [Required]
        public string NAME { get; set; }
        [Column("UNVAN")]
        [Required]
        public string TITLE { get; set; }
        [Column("DIPLOMANO")]
        [Required]
        public string DIPLOMA_NO { get; set; }
        [Column("SICILNO")]
        [Required]
        public string REGISTER_NO { get; set; }
        [Column("ADI")]
        [Required]
        public string FIRST_NAME { get; set; }
        [Required]
        [Column("SOYADI")]
        public string LAST_NAME { get; set; }
        [Column("TCKIMLIKNO")]
        [Required]
        public Int64? IDENTITY_NO { get; set; }

    }
}
