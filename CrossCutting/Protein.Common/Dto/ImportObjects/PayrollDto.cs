﻿using Protein.Common.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Dto.ImportObjects
{
    public class PayrollDto : IImportObject
    {
        public string AuthenticationKey { get; set; }

        [Column("Zarf No")]
        [Required]
        public Int64 PayrollId { get; set; }
        [Column("Tarih")]
        public DateTime Date { get; set; }
        [Column("Tutar")]
        public decimal TotalPaid { get; set; }
        [Column("Statü")]
        public string PayrollStatus { get; set; }
        public string PayrollTypeCode { get; set; }
        public Int64? ProviderIdentityNo { get; set; }
        public string ProviderTypeText { get; set; }
        public string ProviderIban { get; set; }
        public List<ClaimDto> ClaimList { get; set; }
        public long CompanyId { get; set; }
        public long ProviderId { get; set; }
        public long InsuredId { get; set; }
        public string InsuredIdentityNo { get; set; }
        public string InsuredName { get; set; }
        public string InsuredLastName { get; set; }
        public string InsuredIban { get; set; }
        public string ClaimNo { get; set; }
        public DateTime BillDate { get; set; }
        public string BillNo { get; set; }
        public DateTime PaymentDate { get; set; }
        public DateTime? DueDate { get; set; }
    }
}
