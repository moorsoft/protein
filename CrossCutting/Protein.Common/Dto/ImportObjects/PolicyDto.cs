﻿using Protein.Common.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Dto.ImportObjects
{
    #region PolicyImport
    public enum SagmerPolicyType
    {
        SAGLIK = 0,
        SEYAHAT = 1
    }
    public enum PolicyIntegrationType
    {
        UI = 0,
        Excel = 1,
        WS = 2
    }
    public class PolicyDto : IImportObject
    {
        #region Endorsement UI Fields
        [Column("ZEYL_KATEGORI", TypeName = "EXCEL")]
        public string EndorsementCategory { get; set; }
        [Column("ZEYL_TIPININ_TURU", TypeName = "EXCEL")]
        public string EndorsementTypeDescriptionType { get; set; }
        public string EndorsementTypeDescription { get; set; }
        [Column("STATUS", TypeName = "EXCEL")]
        public string PolicyStatus { get; set; }
        #endregion
        #region Besim
        [Column("IS_POLICY_CHANGE")]
        public bool isPolicyChange { get; set; } = false;
        [Column("IS_ENDORSEMENT_CHANGE")]
        public bool isEndorsementChange { get; set; } = false;
        public bool isPolicyTransfer { get; set; } = false;
        public bool IsAdditionalProtocolChange { get; set; } = false;
        public bool IsSpeacialCondition { get; set; } = false;
        public bool IsSagmerPolicySend { get; set; } = false;
        public bool IsSagmerPolicySendForProd { get; set; } = false;
        public bool IsSagmerInsuredEntrySend { get; set; } = false;
        public bool IsSagmerInsuredExitSend { get; set; } = false;
        public bool IsSagmerInsurerChangeSend { get; set; } = false;
        [Column("IS_INSURER_CHANGE")]
        public bool isInsurerChange { get; set; } = false;
        [Column("IS_INSURED_CHANGE")]
        public bool isInsuredChange { get; set; } = false;
        [Column("IS_INSURED__NOTE_CHANGE")]
        public bool isInsuredNoteChange { get; set; } = false;
        [Column("IS_INSURED_CONTACT_CHANGE")]
        public bool isInsuredContactChange { get; set; } = false;
        public bool isInsuredParameterChange { get; set; } = false;
        public bool isInsuredExitChange { get; set; } = false;
        public bool isInsuredTransferChange { get; set; } = false;
        public bool isInsertInstallment { get; set; } = false;
        public decimal? InsuredTotalPremium { get; set; }
        public decimal? InsuredEndorsementPremium { get; set; } = 0;

        //[Required]
        [Column("POLICY_ID")]
        public Int64 PolicyId { get; set; } = 0;
        //[Required]
        [Column("ENDORSEMENT_ID")]
        public Int64 EndorsementId { get; set; }
        [Column("SUBPRODUCT_ID")]
        public Int64? SubProductId { get; set; }
        [Column("ACENTE_NO", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 50, ErrorMessage = "Acente No maksimum 50 karakter olmalı")]
        public string AgencyId { get; set; }
        [Column("INSURER")]
        public Int64? InsurerId { get; set; }
        [Column("COGRAFI_KAPSAM", TypeName = "EXCEL")]
        [MaxLength(length: 1, ErrorMessage = "Lokasyon tipi maksimum 3 karakter olmalı")]
        public string LocationType { get; set; }
        [Column("POLICY_PARENT_ID")]
        public Int64? PolicyParentId { get; set; }
        [Column("POLICY_GROUP_ID")]
        public Int64? PolicyGroupId { get; set; }
        [Column("POLICY_PREVIOUS_ID")]
        public Int64? PolicyPreviousId { get; set; }
        [Column("POLICE_PRIMI", TypeName = "EXCEL")]
        public Decimal? PolicyPremium { get; set; }
        [Column("POLICY_TAX")]
        public Decimal? PolicyTax { get; set; }


        ////[Column("POLICY_INSTALLMENT_COUNT")]
        //public int? PolicyInstallmentCount { get; set; }
        [Column("ODEME_SEKLI", TypeName = "EXCEL")]
        [MaxLength(length: 1, ErrorMessage = "Fiyat tipi maksimum 3 karakter olmalı")]
        public string PolicyPaymentType { get; set; }

        public Int64? ExchangeRateId { get; set; }
        public string InsuredStatus { get; set; } = "0";


        //[Required]
        [Column("INSURER_CONTACT_ID")]
        public Int64 InsurerContactId { get; set; }
        [Column("INSURER_TAX_OFFICE")]
        public string InsurerTaxOffice { get; set; }
        [Column("INSURER_TITLE")]
        public string InsurerTitle { get; set; }
        [Column("INSURER_CORPORATE_NAME")]
        public string InsurerCorporateName { get; set; }
        [Column("INSURER_CITY_ID")]
        public Int64? InsurerCityId { get; set; }
        [Column("INSURER_COUNTY_ID")]
        public Int64? InsurerCountyId { get; set; }
        [Column("INSURER_ZIP_CODE")]
        public string InsurerZipCode { get; set; }
        [Column("SG_ILK_AYK_TEM_TARIH")]
        public DateTime? InsuredFirstAmbulantCoverageDate { get; set; }

        //[Required]
        [Column("INSURED_CONTACT_ID")]
        public Int64 InsuredContactId { get; set; }
        [Column("INSURED_CITY_ID")]
        public Int64? InsuredCityId { get; set; }
        [Column("INSURED_COUNTY_ID")]
        public Int64? InsuredCountyId { get; set; }
        [Column("INSURED_ZIP_CODE")]
        public string InsuredZipCode { get; set; }
        //[Required]
        [Column("INSURED_BANK_ID")]
        public Int64 InsuredBankId { get; set; }
        [Column("INSURED_BANK_BRANCH_ID")]
        public Int64? InsuredBankBranchId { get; set; }
        [Column("INSURED_BANK_CURRENCY_TYPE")]
        [MaxLength(length: 3, ErrorMessage = "Fiyat tipi maksimum 3 karakter olmalı")]
        public string InsuredBankCurrencyType { get; set; }
        [Column("INSURED_NOTE_TYPE")]
        [MaxLength(length: 3, ErrorMessage = "Fiyat tipi maksimum 3 karakter olmalı")]
        public string InsuredNoteType { get; set; }
        [Column("INSURED_TAX")]
        public decimal? InsuredTax { get; set; }
        [Column("INSURED_PARENT_ID")]
        public Int64? insuredParentId { get; set; }
        [Column("INSURED_PRICE_ID")]
        public Int64? insuredPackagePriceId { get; set; }
        [Column("INSURED_RENEWAL_GUARENTEE_TYPE_TEXT")]
        public string InsuredRenewalGuaranteeText { get; set; }

        [Column("ZEYL_MUHASEBELESTIRME_TARIH", TypeName = "EXCEL")]
        //[Required]
        public DateTime? EndorsementRegistrationDate { get; set; }
        [Column("ENDORSEMENT_REASON_ID")]
        public Int64? EndorsementReasonId { get; set; }
        [Column("ENDORSEMENT_STATUS")]
        public string EndorsementStatus { get; set; }


        public long InsuredPersonId { get; set; } = 0;
        public long InsuredContactEmailId { get; set; } = 0;
        public long InsuredEmailId { get; set; } = 0;
        public long InsuredContactMobilePhoneId { get; set; } = 0;
        public long InsuredMobilePhoneId { get; set; } = 0;
        public long InsuredContactPhoneId { get; set; } = 0;
        public long InsuredPhoneId { get; set; } = 0;
        public string InsuredVipType { get; set; }
        public long InsuredContactAddressId { get; set; } = 0;
        public long InsuredAddressId { get; set; } = 0;
        public long InsuredId { get; set; } = 0;
        public DateTime? InsuredBirthCoverageDate { get; set; }
        //public long? InsuredPackageId { get; set; } = null;
        public long InsuredNoParameterId { get; set; } = 0;
        public DateTime? InsuredrenewalDate { get; set; } = null;
        public string InsuredIsOpenToClaim { get; set; }
        public string InsuredVipText { get; set; }

        public long InsuredTransferId { get; set; } = 0;
        public decimal? CashAmount { get; set; }
        public decimal? Amount { get; set; }
        #endregion

        public bool IsPrint { get; set; } = false;
        public string LocalSavePath { get; set; }

        [Column("OTORIZASYON_KOD", TypeName = "EXCEL")]
        public string SbmAuthorizationCode { get; set; }
        public string BatchPolicyKey { get; set; }
        public bool BatchPolicyIsOk { get; set; }
        public long ReqId { get; set; }
        public long IntegrationId { get; set; }
        public string CorrelationId { get; set; }
        public SagmerPolicyType sagmerPolicyType { get; set; }
        public PolicyIntegrationType policyIntegrationType { get; set; }
        public List<long> InstertedInstalmentIds { get; set; } = new List<long>();
        [Column("TAKSIT_TUTARI", TypeName = "EXCEL")]
        public decimal InstallmentAmount { get; set; }
        [Column("TAKSIT_SON_ODEME_TARIHI", TypeName = "EXCEL")]
        public DateTime InstallmentDueDate { get; set; }
        [Column("ODEME_DEGER", TypeName = "EXCEL")]
        public string CashPaymentValue { get; set; }
        [Column("PESIN_ODEME_TIPI", TypeName = "EXCEL")]
        public string CashPaymentType { get; set; }
        [Column("SG_GECIS_TIPI", TypeName = "EXCEL")]
        public string InsuredTransferType { get; set; }
        [Column("SG_GECIS_SIRKETI", TypeName = "EXCEL")]
        public string InsuredTransferCompanyName { get; set; }
        [Column("EK_PROTOKOL", TypeName = "EXCEL")]
        public string AdditionalProtocol { get; set; }
        [Column("OZEL_SART", TypeName = "EXCEL")]
        public string SpecialCondition { get; set; }
        [Required]
        [Column("POLICE_TIPI", TypeName = "EXCEL")]
        [MaxLength(length: 1, ErrorMessage = "Poliçe tipi maksimum 1 karakter olmalı")]
        public string PolicyType { get; set; }
        [Column("KAPAK_POLICE_NO", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 20, ErrorMessage = "Kapak poliçe no maksimum 20 karakter olmalı")]
        public string CoverPolicyNo { get; set; }
        [Column("POLICE_NO", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 20, ErrorMessage = "Poliçe no maksimum 20 karakter olmalı")]
        public string PolicyNo { get; set; }
        [Column("YENILEME_NO", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 20, ErrorMessage = "Kapak poliçe no maksimum 20 karakter olmalı")]
        public int RenewalNo { get; set; }
        [Column("ONCEKI_KAPAK_POLICE_NO", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 20, ErrorMessage = "Önceki kapak poliçe no maksimum 20 karakter olmalı")]
        public string PreviousCoverPolicyNo { get; set; }
        [Column("ONCEKI_POLICE_NO", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 20, ErrorMessage = "Önceki poliçe no maksimum 20 karakter olmalı")]
        public string PreviousPolicyNo { get; set; }
        [Column("POLICE_KUR_TIPI", TypeName = "EXCEL")]
        public string PolicyCurrencyType { get; set; }
        [Column("ONCEKI_YENILEME_NO", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 20, ErrorMessage = "Önceki yenileme no maksimum 20 karakter olmalı")]
        public int? PreviousRenewalNo { get; set; }
        [Column("BASLAMA_TARIH", TypeName = "EXCEL")]
        [Required]
        public DateTime? StartDate { get; set; }
        [Column("BITIS_TARIH", TypeName = "EXCEL")]
        [Required]
        public DateTime? EndDate { get; set; }
        [Column("SBM_ODEME_KOD")]
        public string SbmPaymentCode { get; set; }
        [Column("KULLANICI")]
        public string User { get; set; }

        [Column("SBM_POLICE_NO")]
        [Required]
        [MaxLength(length: 20, ErrorMessage = "Sbm poliçe no maksimum 20 karakter olmalı")]
        public string SbmPoliceNo { get; set; }
        [Column("VADE_SAYI", TypeName = "EXCEL")]
        [Required]
        public int? PolicyInstallmentCount { get; set; }
        [Column("TANZIM_TARIH", TypeName = "EXCEL")]
        [Required]
        public DateTime? DateOfIssue { get; set; }

        [Column("SE_TIP", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 1, ErrorMessage = "Sigorta ettiren tipi maksimum 1 karakter olmalı")]
        public string InsurerType { get; set; }
        [Column("SE_ISIM", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 75, ErrorMessage = "Sigorta ettiren ismi 50 karakter olmalı")]
        public string InsurerName { get; set; }
        [Column("SE_SOYISIM", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 75, ErrorMessage = "Sigorta ettiren soyisim 50 karakter olmalı")]
        public string InsurerLastName { get; set; }

        [Column("SE_CINSIYET", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 5, ErrorMessage = "Sigorta ettiren cinsiyet 5 karakter olmalı")]
        public string InsurerGender { get; set; }

        [Column("SE_DOGUM_TARIH", TypeName = "EXCEL")]
        [Required]
        public DateTime? InsurerBirthDate { get; set; }
        [Column("SE_DOGUM_YER", TypeName = "EXCEL")]
        [Required]
        public string InsurerBirthPlace { get; set; }
        [Column("SE_BABA_ADI", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 75, ErrorMessage = "Sigorta ettiren baba adı 75 karakter olmalı")]
        public string InsurerNameOfFather { get; set; }

        [Column("SE_TC_KIMLIK_NO", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 11, ErrorMessage = "Sigorta ettiren tc no 11 karakter olmalı")]
        [MinLength(length: 11, ErrorMessage = "Sigorta ettiren tc no 11 karakter olmalı")]
        public Int64? InsurerTcNo { get; set; }
        [Column("SE_VKN", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 10, ErrorMessage = "Sigorta ettiren vkn no 10 karakter olmalı")]
        [MinLength(length: 10, ErrorMessage = "Sigorta ettiren vkn no 10 karakter olmalı")]
        public Int64? InsurerVkn { get; set; }
        [Column("SE_UYRUK", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 3, ErrorMessage = "Sigorta ettiren uyruğu 3 karakter olmalı")]
        public string InsurerNationality { get; set; }
        [Column("SE_PASAPORT", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 15, ErrorMessage = "Sigortalı ettiren pasaport maksimum 15 karakter olmalı")]
        public string InsurerPassport { get; set; }
        [Column("SE_ADRES", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 500, ErrorMessage = "Sigortalı ettiren adres maksimum 500 karakter olmalı")]
        public string InsurerAddress { get; set; }
        [Column("SE_IL_KODU", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 2, ErrorMessage = "Sigortalı ettiren il kodu maksimum 2 karakter olmalı")]
        public int InsurerCityCode { get; set; }
        [Column("SE_TEL_NO", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 15, ErrorMessage = "Sigortalı ettiren telefon no maksimum 15 karakter olmalı")]
        public string InsurerTelNo { get; set; }
        [Column("SE_GSM_NO", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 15, ErrorMessage = "Sigortalı ettiren gsm no maksimum 15 karakter olmalı")]
        public string InsurerGsmNo { get; set; }
        [Column("SE_EMAIL", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 75, ErrorMessage = "Sigortalı ettiren email maksimum 75 karakter olmalı")]
        public string InsurerEmail { get; set; }

        [Column("SG_SIGORTALI_NO", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 20, ErrorMessage = "Sigortalı no 20 karakter olmalı")]
        public string InsuredNo { get; set; }

        [Column("SG_ISIM", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 75, ErrorMessage = "Sigortalı isim 75 karakter olmalı")]
        public string InsuredName { get; set; }
        [Column("SG_SOYISIM", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 75, ErrorMessage = "Sigortalı soyisim 75 karakter olmalı")]
        public string InsuredLastName { get; set; }
        [Required]
        [Column("SG_TC_KIMLIK_NO", TypeName = "EXCEL")]
        [MaxLength(length: 11, ErrorMessage = "Sigortalı tc no 11 karakter olmalı")]
        [MinLength(length: 11, ErrorMessage = "Sigortalı tc no 11 karakter olmalı")]
        public Int64? InsuredTcNo { get; set; }
        [Column("SG_UYRUK", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 3, ErrorMessage = "Sigortalı uyruğu 3 karakter olmalı")]
        public string InsuredNationality { get; set; }
        [Required]
        [Column("SG_CINSIYET", TypeName = "EXCEL")]
        [MaxLength(length: 5, ErrorMessage = "Sigortalı cinsiyet 5 karakter olmalı")]
        public string InsuredGender { get; set; }
        [Required]
        [Column("SG_DOGUM_TARIH", TypeName = "EXCEL")]
        public DateTime? InsuredBirthDate { get; set; }
        [Column("SG_DOGUM_YER", TypeName = "EXCEL")]
        [Required]
        public string InsuredBirthPlace { get; set; }
        [Column("SG_BABA_ADI", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 75, ErrorMessage = "Sigortalı baba adı 75 karakter olmalı")]
        public string InsuredNameOfFather { get; set; }
        [Required]
        [Column("SG_SIG_SIR_GIRIS_TARIH", TypeName = "EXCEL")]
        public DateTime? InsuredCompanyEntranceDate { get; set; }
        [Column("SG_PLAN_KODU", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 10, ErrorMessage = "Sigortalı plan kodu maksimum 10 karakter olmalı")]
        public Int64? InsuredPackageId { get; set; } = null;
        [Column("SG_YAKINLIK", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 2, ErrorMessage = "Sigortalı yakınlık maksimum 10 karakter olmalı")]
        public string InsuredIndividualType { get; set; }
        [Column("SG_PRIM", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 20, ErrorMessage = "Sigortalı prim maksimum 10 karakter olmalı")]
        public decimal? InsuredInitialPremium { get; set; }
        [Column("SG_POLICEYE_GIRIS_TAR", TypeName = "EXCEL")]
        [Required]
        public DateTime InsuredPolicyEntryDate { get; set; }
        [Column("SG_CIKIS_PRIM")]
        public string InsuredExitPremium { get; set; } //gonderilen tutar - mevcut tutar  = insured_premium
        [Column("SG_PASAPORT_NO", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 15, ErrorMessage = "Sigortalı pasaport maksimum 15 karakter olmalı")]
        public string InsuredPassport { get; set; }
        [Required]
        [Column("SG_AILE_NO", TypeName = "EXCEL")]
        [MaxLength(length: 3, ErrorMessage = "Sigortalı aile no maksimum 15 karakter olmalı")]
        public Int64 InsuredFamilyNo { get; set; } = 0;
        [Required]
        [Column("SG_SICIL_NO", TypeName = "EXCEL")]
        [MaxLength(length: 3, ErrorMessage = "Sigortalı aile no maksimum 15 karakter olmalı")]
        public string InsuredRegistorNo { get; set; }
        [Column("SG_ISTISNA", TypeName = "EXCEL")]
        [Required]
        public string InsuredExclusion { get; set; }
        [Column("SG_OBYG", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 1, ErrorMessage = "Sigortalı obyg maksimum 1 karakter olmalı")]
        public string InsuredRenewalGuaranteeType { get; set; }
        [Column("SG_VIP", TypeName = "EXCEL")]
        [Required]
        public string InsuredVip { get; set; }
        [Column("KOMISYON_ORAN")]
        public string CommissionRate { get; set; }
        [Column("SG_KAZANILMIS_HAK", TypeName = "EXCEL")]
        [Required]
        public string InsuredKazanilmisHak { get; set; }
        [Column("SG_MEDENI_DURUM", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 3, ErrorMessage = "Sigortalı medeni durum maksimum 3 karakter olmalı")]
        public string InsuredMaritalStatus { get; set; }
        [Column("SG_ILK_SIG_TARIH", TypeName = "EXCEL")]
        public DateTime? InsuredFirstInsuredDate { get; set; }
        [Column("SG_BANKA_KODU", TypeName = "EXCEL")]
        [Required]
        public string InsuredBankCode { get; set; }
        [Column("SG_SUBE_KODU", TypeName = "EXCEL")]
        [Required]
        public string InsuredBankBranchCode { get; set; }
        [Column("SG_HESAP_NO", TypeName = "EXCEL")]
        [Required]
        public string InsuredBankAccountNo { get; set; }
        [Column("SG_IBAN_NO", TypeName = "EXCEL")]
        [Required]
        public string InsuredBankIbanNo { get; set; }
        [Column("SG_HESAP_SAHIBI_ADI", TypeName = "EXCEL")]
        [MaxLength(length: 75, ErrorMessage = "Sigortalı hesap sahibi adı maksimum 75 karakter olmalı")]
        [Required]
        public string InsuredBankAccountOwnerName { get; set; }
        [Column("SG_ADRES", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 500, ErrorMessage = "Sigortalı adres  maksimum 500 karakter olmalı")]
        public string InsuredAddress { get; set; }

        [Column("SG_IL_KODU", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 2, ErrorMessage = "Sigortalı il kodu maksimum 2 karakter olmalı")]
        public int InsuredCityCode { get; set; }
        [Column("SG_TEL_NO", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 15, ErrorMessage = "Sigortalı telefon no maksimum 15 karakter olmalı")]
        public string InsuredTelNo { get; set; }
        [Column("SG_GSM_NO", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 15, ErrorMessage = "Sigortalı gsm no maksimum 15 karakter olmalı")]
        public string InsuredGsmNo { get; set; }
        [Column("SG_EMAIL", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 75, ErrorMessage = "Sigortalı email maksimum 75 karakter olmalı")]
        public string InsuredEmail { get; set; }

        [Column("ZEYL_SIRA_NO", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 75, ErrorMessage = "Zeyl sıra no maksimum 75 karakter olmalı")]
        public int EndorsementNo { get; set; }
        [Column("ZEYL_TUR", TypeName = "EXCEL")]
        [Required]
        [MaxLength(length: 2, ErrorMessage = "Zeyl türü maksimum 2 karakter olmalı")]
        public string EndorsementType { get; set; }
        [Column("ZEYL_BASLAMA_TARIH", TypeName = "EXCEL")]
        [Required]
        public DateTime? EndorsementStartDate { get; set; }
        [Column("ZEYL_AD")]
        [Required]
        [MaxLength(length: 75, ErrorMessage = "Zeyl isim maksimum 75 karakter olmalı")]
        public string EndorsementName { get; set; }
        public DateTime? RenewalDate { get; set; }
        public string InsuredNoteDetails { get; set; }
        public long InsuredNoteId { get; set; } = 0;
        public long InsurerPersonId { get; set; } = 0;
        public long InsurerCorporateId { get; set; } = 0;
        public long InsurerAddressId { get; set; } = 0;
        public long InsurerPhoneId { get; set; } = 0;
        public long InsurerGsmPhoneId { get; set; } = 0;
        public long InsurerEmailId { get; set; } = 0;

        public Int64 CompanyId { get; set; } = 0;
        public Int64? InsuredTaxNumber { get; set; }
        public string InsurerBankAccountName { get; set; }
        public Int64 InsurerBankId { get; set; } = 0;
        public string InsurerBankCurrencyType { get; set; }
        public string InsurerIban { get; set; }
        public string InsurerBankNo { get; set; }
        public Int64 InsurerBankAccountId { get; set; } = 0;
        public long? InsurerBankBranchId { get; set; }
        public decimal? EndorsementPremium { get; set; }

        //public PolicyInsurer Insurer { get; set; }
        //public PolicyInsured Insured { get; set; }
        //public PolicyEndorsement Endorsement { get; set; }
        //public PolicyImportDto()
        //{
        //    Insured = new PolicyInsured();
        //    Insurer = new PolicyInsurer();
        //    Endorsement = new PolicyEndorsement();
        //}
    }
    #endregion

}
