﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Dto
{
    public class ViewResultDto<T>
    {
        public int TotalItemsCount { get; set; } = 0;
        public int StateCode { get; set; }
        public string State { get; set; }
        public string Message { get; set; }
        public string Conditions { get; set; }

        public T Data { get; set; }

    }
}
