﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Common.Dto
{
    public class AjaxResultDto<T>
    {
        public string ResultCode { get; set; } = "";
        public string ResultMessage { get; set; } = "";

        public T Data { get; set; }
    }

    public class ProviderSaveStep1Result
    {
        public Int64 providerContactId { get; set; } = 0;
        public Int64 providerAdressId { get; set; } = 0;
        public Int64 providerId { get; set; } = 0;
        public Int64 corporateId { get; set; } = 0;
        public Int64 UserId { get; set; } = 0;
        public Int64 providerUserId { get; set; } = 0;
        public Int64 phoneId { get; set; } = 0;
        public Int64 providerPhoneId { get; set; } = 0;

        public string stafs { get; set; }
        public string phones { get; set; }

    }

    public class PlanJsonResult
    {
        public string jsonData { get; set; }
    }

    public class ProviderSaveStep2Result
    {
        public string jsonData { get; set; }
    }

    public class ProviderSaveStep3Result
    {
        public string jsonData { get; set; }
    }

    public class ProviderSaveStep4Result
    {
        public string jsonData { get; set; }
    }

    public class ProviderSaveStep5Result
    {
        public string jsonData { get; set; }
    }

    public class ProviderSaveStep6Result
    {
        public string jsonData { get; set; }
    }

    public class QuestionSaveResult
    {
        public Int64 questionId { get; set; } = 0;
        public List<SpResponse> choiceList { get; set; }
    }

    public class Processes
    {
        public string jsonData { get; set; }
    }

    public class CompanySaveStep1Result
    {
        public Int64 companyContactId { get; set; } = 0;
        public Int64 companyAdressId { get; set; } = 0;
        public Int64 companyBillAdressId { get; set; } = 0;
        public Int64 companyActiveLogoId { get; set; } = 0;
        public Int64 companyPassiveLogoId { get; set; } = 0;
        public Int64 companyId { get; set; } = 0;
        public Int64 companyCorporateId { get; set; } = 0;
    }

    public class JSONResult
    {
        public string jsonData { get; set; }
    }

    public class CompanySaveStep2Result
    {
        public string jsonData { get; set; }
    }

    public class CompanySaveStep3Result
    {
        public string jsonData { get; set; }
    }

    public class CompanySaveStep4Result
    {
        public string jsonData { get; set; }
    }

    public class CompanySaveStep5Result
    {
        public string jsonData { get; set; }
    }

    public class CompanySaveStep6Result
    {
        public string jsonData { get; set; }
    }

    public class CompanySaveStep7Result
    {
        public string jsonData { get; set; }
    }

    public class ProductSaveStep1Result
    {
        public Int64 productId { get; set; } = 0;
        public Int64 sagmerRegistryDateParameterId { get; set; } = 0;
        public Int64 sagmerRegistryDateProductParameterId { get; set; } = 0;
        public Int64 sagmerNoParameterId { get; set; } = 0;
        public Int64 sagmerNoProductParameterId { get; set; } = 0;
    }

    public class ProductSaveStep2Result
    {
        public string jsonData { get; set; }
    }

    public class ProductSaveStep3Result
    {
        public string jsonData { get; set; }
    }

    public class ProductSaveStep4Result
    {
        public string jsonData { get; set; }
    }

    public class SubproductSaveResult
    {
        public Int64 subproductId { get; set; } = 0;
        public string subproductCode { get; set; } = "";
        public Int64 sagmerRegistryDateParameterId { get; set; } = 0;
        public Int64 sagmerRegistryDateSubproductParameterId { get; set; } = 0;
        public Int64 sagmerNoParameterId { get; set; } = 0;
        public Int64 sagmerNoSubproductParameterId { get; set; } = 0;
    }

    public class SubproductSaveStep2Result
    {
        public string subproductCode { get; set; } = "";
        public string jsonData { get; set; }
    }

    public class SubproductSaveStep3Result
    {
        public string subproductCode { get; set; } = "";
        public string jsonData { get; set; }
    }

    public class SubproductSaveStep4Result
    {
        public string subproductCode { get; set; } = "";
        public string jsonData { get; set; }
    }

    public class PackageSaveStep1Result
    {
        public Int64 packageId { get; set; } = 0;
        public Int64 priceListId { get; set; } = 0;
        public Int64 isSagmer { get; set; } = 0;
        public Int64 packageTypeParameterId { get; set; } = 0;
        public Int64 packageTypePackageParameterId { get; set; } = 0;
        public Int64 productParameterId { get; set; } = 0;
        public Int64 productPackageParameterId { get; set; } = 0;
        public Int64 packageTpaPrice { get; set; } = 0;
    }

    public class PackageSaveStep2Result
    {
        public string jsonData { get; set; }
    }

    public class PackageSaveStep3Result
    {
        public string jsonData { get; set; }
    }

    public class PackageSaveStep4Result
    {
        public string jsonData { get; set; }
    }

    public class PackageSaveStep6Result
    {
        public string jsonData { get; set; }
    }

    public class ClaimSaveStep1Result
    {
        public Int64 claimId { get; set; } = 0;
        public string claimStatus { get; set; }
        public int claimStatusOrdinal { get; set; }
        public int provisionStep { get; set; }
        public string providerName { get; set; }
        public string ReasonDesc { get; set; }
        public string CompanyName { get; set; }
        public string paymentMethod { get; set; }
        public Int64 CompanyId { get; set; }
        public Int64 InsuredId { get; set; }
    }

    public class ClaimStatusResult
    {
        public string claimStatus { get; set; }
        public int claimStatusOrdinal { get; set; }
        public string claimReason { get; set; }
        public string companyClaimId { get; set; }
    }

    public class ClaimSaveStep2Result
    {
        public string jsonData { get; set; }
    }

    public class BillSaveResult
    {
        public string claimBillId { get; set; }
    }

    public class PayrollSaveResult
    {
        public string payrollId { get; set; }
        public string payrollDescription { get; set; }
    }

    public class PayrollExitResult
    {
        public string payrollId { get; set; }
        public string jsonData { get; set; }
    }

    public class ClaimProcessListResult
    {
        public string jsonData { get; set; }
    }

    public class ClaimRejectResult
    {
        public string reason { get; set; } = "";
        public Int64 reasonId { get; set; } = 0;
    }

    public class ClaimSaveNoteResult
    {
        public string jsonData { get; set; }
    }

    public class ClaimSaveDoctorResult
    {
        public string jsonData { get; set; }
    }

    public class ClaimSaveEventResult
    {
        public string jsonData { get; set; }
    }

    public class ClaimSaveHospitalizationResult
    {
        public Int64 claimHospitalizationId;
    }

    public class ClaimSaveStep5Result
    {
        public Int64 claimHospitalizationId;
    }

    public class ClaimSaveStep6Result
    {
        public string jsonData { get; set; }
    }

    public class ClaimSaveStep7Result
    {
        public string jsonData { get; set; }
    }
    public class ClaimSaveStep8Result
    {
        public string jsonData { get; set; }
    }

    public class ClaimSaveStep9Result
    {
        public string jsonData { get; set; }
    }

    public class InsurerSaveResult
    {
        public Int64 contactId { get; set; } = 0;
        public string ContactTitle { get; set; } = string.Empty;
    }

    public class PolicySaveStep1Result
    {
        public Int64 policyId { get; set; } = 0;
        public Int64 endorsementId { get; set; } = 0;
    }
    public class PolicyGroupSaveResult
    {
        public Int64 policyGroupId { get; set; } = 0;
    }

    public class EndorsementSaveResult
    {
        public Int64 policyId { get; set; } = 0;
    }

    public class InsuredContactSaveResult
    {
        public Int64 insuredContactId { get; set; } = 0;
        public int AGE { get; set; } = 0;
        public string GENDER { get; set; } = string.Empty;
        public string ContactTitle { get; set; } = string.Empty;
    }

    public class InsuredNoteSaveResult
    {
        public string jsonNoteData { get; set; }
        public string jsonDeclarationData { get; set; }
        public string jsonTransferNoteData { get; set; }
    }

    public class InsuredBankSaveResult
    {
        public string jsonBankData { get; set; }
        public string claimAmount { get; set; }
    }

    public class ExclusionSaveResult
    {
        public string jsonExclusionData { get; set; }
    }
    public class LogSendResult
    {
        public string jsonLogSendData{ get; set; }
    }
    public class InsuredSaveResult
    {
        public Int64 insuredId { get; set; } = 0;
        public Int64 insuredContactId { get; set; } = 0;
        public Int64 parameterId { get; set; } = 0;
        public Int64 transferId { get; set; } = 0;
        public decimal Premium { get; set; } = 0;
    }

    public class StatusRejectorWaitSaveResult
    {
        public string reason { get; set; } = "";
        public Int64 reasonId { get; set; } = 0;
    }

    public class PolicySaveStep2Result
    {
        public string jsonData { get; set; }
    }

    public class PolicySaveStep3Result
    {
        public string jsonData { get; set; }
    }

    public class PolicySaveStep4Result
    {
        public string jsonData { get; set; }
    }

    public class PolicySaveStep5Result
    {
        public string jsonData { get; set; }
    }

    public class InsuredCheckResult
    {
        public string jsonData { get; set; }
    }

    public class ClaimProcessICDResult
    {
        public string jsonData { get; set; }
    }
}
