﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Constants
{
    public class Constants
    {
        public class ImportRules
        {
            public static long FileSizeLimit { get { return long.Parse(ConfigurationManager.AppSettings["ImportFileSizeLimit"].ToString()); } }

        }
        public class MediaRules
        {
            public static long FileSizeLimit { get { return long.Parse(ConfigurationManager.AppSettings["MediaFileSizeLimit"].ToString()); } }

        }
        public class AppKeys
        {
            public const string SMTPServer = "SMTP_Server";
            public const string SMTPPort = "SMTP_Port";
            public const string SMTPUseSSL = "SMTP_UseSSL";
            public const string SMTPUser = "SMTP_User";
            public const string SMTPPassword = "SMTP_Password";
            public const string SMTPDisplayName = "SMTP_DisplayName";

            public const string SagmerUrl = "Sagmer_Url";
            public const string SagmerUsername = "Sagmer_Username";
            public const string SagmerKurumKod = "Sagmer_KurumKod";
            public const string SagmerApiCode = "SagmerWsApiCode";
            public const string SagmerPlanId = "PLAN_ID";
            public const string SagmerPackageType = "PACKAGE_TYPE_ID";
            public const string SagmerCoverage = "COVERAGE";

            public const string GunesApiUrl = "WS_GunesApiUrl";
            public const string GunesApiCode = "WS_GunesApiCode";
            public const string GunesApiSirketKod = "WS_GunesApiSirketKod";
            public const string GunesApiPassword = "WS_GunesApiPassword";

            public const string VakifApiUrl = "WS_VakifApiUrl";
            public const string VakifApiCode = "WS_VakifApiCode";
            public const string VakifApiSirketKod = "WS_VakifApiSirketKod";
            public const string VakifApiPassword = "WS_VakifApiPassword";

            public const string ApiUrl = "APIURL";
            public const string ApiCode = "APICODE";
            public const string ApiCompanyCode = "APICOMPANYCODE";
            public const string ApiPassword = "APIPASSWORD";
            public const string ApiUserName = "APIUSERNAME";

            public const string PortTSSPassword = "PASSWORD";
            public const string PortTSSUserName = "USER_NAME";
            public const string PortTSSCoverageCode = "COVERAGE_CODE";


            public const string ContactCustomerID = "COMPANYCONTACTID";
        }

        public class LookupTypes
        {
            #region Generated code will paste here

            public const string PackageTpaPrice = "PackageTpaPriceType";
            public const string ClaimMissingDoc = "ClaimMissingDocType";
            public const string WsAgreementStatus = "WsAgreementStatus";
            public const string ProviderNote = "ProviderNoteType";
            public const string CompanyPhone = "CompanyPhoneType";
            public const string CompanyEmail = "CompanyEmailType";
            public const string InsuredNote = "InsuredNoteType";
            public const string Contact = "ContactType";
            public const string CashPayment = "CashPaymentType";
            public const string Status = "Status";
            public const string Profession = "ProfessionType";
            public const string Gender = "Gender";
            public const string MaritalStatus = "MaritalStatus";
            public const string ContractStatus = "ContractStatus";
            public const string PolicyStatus = "PolicyStatus";
            public const string Corporate = "CorporateType";
            public const string Address = "AddressType";
            public const string Bool = "BoolType";
            public const string Email = "EmailType";
            public const string WsAgreementType = "WsAgreementType";
            public const string Phone = "PhoneType";
            public const string County = "CountyType";
            public const string Currency = "CurrencyType";
            public const string Note = "NoteType";
            public const string Reason = "ReasonType";
            public const string UnitPrivilege = "UnitPrivilegeType";
            public const string Package = "PackageType";
            public const string Agency = "AgencyType";
            public const string Policy = "PolicyType";
            public const string Payment = "PaymentType";
            public const string Location = "LocationType";
            public const string Endorsement = "EndorsementType";
            public const string EndorsementTypeDescription = "EndorsementTypeDescriptionType";
            public const string EndorsementCategory = "EndorsementCategory";
            public const string Individual = "IndividualType";
            public const string RenewalGuarantee = "RenewalGuaranteeType";
            public const string InsuredTransfer = "InsuredTransferType";
            public const string RuleCategory = "RuleCategoryType";
            public const string RuleCategoryResult = "RuleCategoryResultType";
            public const string Rule = "RuleType";
            public const string Price = "PriceType";
            public const string ProcessList = "ProcessListType";
            public const string Network = "NetworkType";
            public const string NetworkCategory = "NetworkCategoryType";
            public const string Product = "ProductType";
            public const string SubProduct = "SubproductType";
            public const string Coverage = "CoverageType";
            public const string PlanCoverageVariation = "PlanCoverageVariationType";
            public const string Agreement = "AgreementType";
            public const string Exemption = "ExemptionType";
            public const string Provider = "ProviderType";
            public const string Staff = "StaffType";
            public const string StaffContract = "StaffContractType";
            public const string Contract = "ContractType";
            public const string Pricing = "PricingType";
            public const string Payroll = "PayrollType";
            public const string PayrollCategory = "PayrollCategory";
            public const string Claim = "ClaimType";
            public const string ClaimPaymentMethod = "ClaimPaymentMethod";
            public const string ClaimVariation = "ClaimVariationType";
            public const string ClaimStatus = "ClaimStatus";
            public const string ClaimSource = "ClaimSourceType";
            public const string BillStatus = "BillStatus";
            public const string PayrollStatus = "PayrollStatus";
            public const string Event = "EventType";
            public const string ClaimNote = "ClaimNoteType";
            public const string ClaimReason = "ClaimReasonType";
            public const string ICD = "ICDType";
            public const string Environment = "EnvironmentType";
            public const string System = "SystemType";
            public const string Selection = "SelectionType";
            public const string Interval = "IntervalType";
            public const string Error = "ErrorType";
            public const string VIP = "VipType";

            #endregion
        }


        public class Views
        {
            public const string Address = "V_ADDRESS";
            public const string Agency = "V_AGENCY";
            public const string AgencyPhone = "V_AGENCY_PHONE";
            public const string AgencyUser = "V_AGENCY_USER";
            public const string AggregateClaim = "V_AGGREGATE_CLAIM";
            public const string Asset = "V_ASSET";
            public const string BankAccount = "V_BANK_ACCOUNT";
            public const string Cadre = "V_CADRE";
            public const string CadrePrivilege = "V_CADRE_PRIVILEGE";
            public const string Category = "V_CATEGORY";
            public const string CityFactorGroup = "V_CITY_FACTOR_GROUP";
            public const string Claim = "V_CLAIM";
            public const string ClaimBill = "V_CLAIM_BILL";
            public const string ClaimCoverage = "V_CLAIM_COVERAGE";
            public const string ClaimHospitalization = "V_CLAIM_HOSPITALIZATION";
            public const string ClaimICD = "V_CLAIM_ICD";
            public const string ClaimMedicine = "V_CLAIM_MEDICINE";
            public const string ClaimNote = "V_CLAIM_NOTE";
            public const string ClaimPayment = "V_CLAIM_PAYMENT";
            public const string ClaimProcess = "V_CLAIM_PROCESS";
            public const string ClaimProcessList = "V_CLAIM_PROCESS_LIST";
            public const string PackageClaimProcess = "V_PACKAGE_CLAIM_PROCESS";
            public const string ClaimMedia = "V_CLAIM_MEDIA";
            public const string Company = "V_COMPANY";
            public const string CompanyBank = "V_COMPANY_BANK";
            public const string CompanyContact = "V_COMPANY_CONTACT";
            public const string CompanyEmail = "V_COMPANY_EMAIL";
            public const string CompanyNote = "V_COMPANY_NOTE";
            public const string CompanyParameter = "V_COMPANY_PARAMETER";
            public const string CompanyPhone = "V_COMPANY_PHONE";
            public const string CompanyUser = "V_COMPANY_USER";
            public const string ContactEmail = "V_CONTACT_EMAIL";
            public const string ContactAddress = "V_CONTACT_ADDRESS";
            public const string ContactBank = "V_CONTACT_BANK";
            public const string ContactPhone = "V_CONTACT_PHONE";
            public const string ContactNote = "V_CONTACT_NOTE";
            public const string ContractDoctorBranch = "V_CONTRACT_DOCTOR_BRANCH";
            public const string ContractList = "V_CONTRACT_LIST";
            public const string ContractListHist = "V_CONTRACT_LIST_HIST";
            public const string ContractNetwork = "V_CONTRACT_NETWORK";
            public const string ContractProcessGroup = "V_CONTRACT_PROCESS_GROUP";
            public const string Coverage = "V_COVERAGE";
            public const string CoverageParameter = "V_COVERAGE_PARAMETER";
            public const string CoverageRule = "V_COVERAGE_RULE";
            public const string ExportContractList = "V_EXPORT_CONTRACT_LIST";
            public const string ExportNetwork = "V_EXPORT_NETWORK";
            public const string ExportPackage = "V_EXPORT_PACKAGE";
            public const string ExportProvider = "V_EXPORT_PROVIDER";
            public const string Insured = "V_INSURED";
            public const string InsuredAddress = "V_INSURED_ADDRESS";
            public const string InsuredBankAccount = "V_INSURED_BANK_ACCOUNT";
            public const string InsuredClaimReason = "V_INSURED_CLAIM_REASON";
            public const string InsuredEndorsement = "V_INSURED_ENDORSEMENT";
            public const string InsuredExclusion = "V_INSURED_EXCLUSION";
            public const string InsuredMail = "V_INSURED_MAIL";
            public const string InsuredNote = "V_INSURED_NOTE";
            public const string InsuredPackage = "V_INSURED_PACKAGE";
            public const string InsuredPackageProduct = "V_INSURED_PACKAGE_PRODUCT";
            public const string InsuredPackageRemaining = "V_INSURED_PACKAGE_REMAINING";
            public const string InsuredPackageShort = "V_INSURED_PACKAGE_SHORT";
            public const string InsuredParent = "V_INSURED_PARENT";
            public const string InsuredPhone = "V_INSURED_PHONE";
            public const string InsuredPriceCalculate = "V_INSURED_PRICE_CALCULATE";
            public const string InsuredTransfer = "V_INSURED_TRANSFER";
            public const string Insurer = "V_INSURER";
            public const string LookupAddressType = "V_LOOKUP_ADDRESSTYPE";
            public const string LookupAgencyStatus = "V_LOOKUP_AGENCYSTATUS";
            public const string LookupAgencytype = "V_LOOKUP_AGENCYTYPE";
            public const string LookupAgreementType = "V_LOOKUP_AGREEMENTTYPE";
            public const string LookupBillStatus = "V_LOOKUP_BILLSTATUS";
            public const string LookupBoolType = "V_LOOKUP_BOOLTYPE";
            public const string LookupCityFactorGroupStatus = "V_LOOKUP_CITYFACTORGROUPSTATUS";
            public const string LookupClaimMissingDocType = "V_LOOKUP_CLAIMMISSINGDOCTYPE";
            public const string LookupClaimNoteType = "V_LOOKUP_CLAIMNOTETYPE";
            public const string LookupClaimPaymentMethod = "V_LOOKUP_CLAIMPAYMENTMETHOD";
            public const string LookupClaimSourceType = "V_LOOKUP_CLAIMSOURCETYPE";
            public const string LookupClaimStatus = "V_LOOKUP_CLAIMSTATUS";
            public const string LookupClaimStatusType = "V_LOOKUP_CLAIMSTATUSTYPE";
            public const string Lookup_ClaimType = "V_LOOKUP_CLAIMTYPE";
            public const string LookupClaimVariation = "V_LOOKUP_CLAIMVARIATION";
            public const string LookupContactNoteType = "V_LOOKUP_CONTACTNOTETYPE";
            public const string LookupContactType = "V_LOOKUP_CONTACTTYPE";
            public const string LookupContractStatus = "V_LOOKUP_CONTRACTSTATUS";
            public const string LookupContractStatusType = "V_LOOKUP_CONTRACTSTATUSTYPE";
            public const string LookupContractType = "V_LOOKUP_CONTRACTTYPE";
            public const string LookupCorporateType = "V_LOOKUP_CORPORATETYPE";
            public const string LookupCountyType = "V_LOOKUP_COUNTYTYPE";
            public const string LookupCoverageType = "V_LOOKUP_COVERAGETYPE";
            public const string LookupCurrencyType = "V_LOOKUP_CURRENCYTYPE";
            public const string LookupEmailType = "V_LOOKUP_EMAILTYPE";
            public const string LookupEndorsementType = "V_LOOKUP_ENDORSEMENTTYPE";
            public const string LookupExemptionType = "V_LOOKUP_EXEMPTIONTYPE";
            public const string LookupGender = "V_LOOKUP_GENDER";
            public const string LookupICDType = "V_LOOKUP_ICDTYPE";
            public const string LookupIdentityType = "V_LOOKUP_IDENTITYTYPE";
            public const string LookupIndividualType = "V_LOOKUP_INDIVIDUALTYPE";
            public const string LookupInsuredTransferType = "V_LOOKUP_INSUREDTRANSFERTYPE";
            public const string LookupIntervalType = "V_LOOKUP_INTERVALTYPE";
            public const string LookupLocationType = "V_LOOKUP_LOCATIONTYPE";
            public const string LookupMaritalStatus = "V_LOOKUP_MARITALSTATUS";
            public const string LookupNetworkCategoryType = "V_LOOKUP_NETWORKCATEGORYTYPE";
            public const string LookupNetworkStatus = "V_LOOKUP_NETWORKSTATUS";
            public const string LookupNetworkType = "V_LOOKUP_NETWORKTYPE";
            public const string LookupNoteType = "V_LOOKUP_NOTETYPE";
            public const string LookupPackageStatus = "V_LOOKUP_PACKAGESTATUS";
            public const string LookupPackagetPapriceType = "V_LOOKUP_PACKAGETPAPRICETYPE";
            public const string LookupPackageType = "V_LOOKUP_PACKAGETYPE";
            public const string LookupPaymentType = "V_LOOKUP_PAYMENTTYPE";
            public const string LookupPayrollCategory = "V_LOOKUP_PAYROLLCATEGORY";
            public const string LookupPayrollStatus = "V_LOOKUP_PAYROLLSTATUS";
            public const string LookupPayrollStatusType = "V_LOOKUP_PAYROLLSTATUSTYPE";
            public const string LookupPayrollType = "V_LOOKUP_PAYROLLTYPE";
            public const string LookupPhoneType = "V_LOOKUP_PHONETYPE";
            public const string LookupPlanStatus = "V_LOOKUP_PLANSTATUS";
            public const string LookupPolicyStatus = "V_LOOKUP_POLICYSTATUS";
            public const string LookupPolicyType = "V_LOOKUP_POLICYTYPE";
            public const string LookupPriceType = "V_LOOKUP_PRICETYPE";
            public const string LookupPricingType = "V_LOOKUP_PRICINGTYPE";
            public const string LookupPrintNoteType = "V_LOOKUP_PRINTNOTETYPE";
            public const string LookupProcessListStatus = "V_LOOKUP_PROCESSLISTSTATUS";
            public const string LookupProcessListType = "V_LOOKUP_PROCESSLISTTYPE";
            public const string LookupProductStatus = "V_LOOKUP_PRODUCTSTATUS";
            public const string LookupProductType = "V_LOOKUP_PRODUCTTYPE";
            public const string LookupProfessionType = "V_LOOKUP_PROFESSIONTYPE";
            public const string LookupProviderNoteType = "V_LOOKUP_PROVIDERNOTETYPE";
            public const string LookupProviderType = "V_LOOKUP_PROVIDERTYPE";
            public const string LookupRenewalGuaranteeType = "V_LOOKUP_RENEWALGUARANTEETYPE";
            public const string LookupRuleCategoryType = "V_LOOKUP_RULECATEGORYTYPE";
            public const string LookupRuleType = "V_LOOKUP_RULETYPE";
            public const string LookupSelectionType = "V_LOOKUP_SELECTIONTYPE";
            public const string LookupStaffType = "V_LOOKUP_STAFFTYPE";
            public const string LookupStatus = "V_LOOKUP_STATUS";
            public const string LookupSubproductStatus = "V_LOOKUP_SUBPRODUCTSTATUS";
            public const string LookupSubproductType = "V_LOOKUP_SUBPRODUCTTYPE";
            public const string LookupSystemType = "V_LOOKUP_SYSTEMTYPE";
            public const string LookupUnitPrivilegeType = "V_LOOKUP_UNITPRIVILEGETYPE";
            public const string LookupVariationType = "V_LOOKUP_VARIATIONTYPE";
            public const string Network = "V_NETWORK";
            public const string Organization = "V_ORGANIZATION";
            public const string Package = "V_PACKAGE";
            public const string PackageMedia = "V_PACKAGE_MEDIA";
            public const string PackageNote = "V_PACKAGE_NOTE";
            public const string PackageParameter = "V_PACKAGE_PARAMETER";
            public const string PackagePlanCoverage = "V_PACKAGE_PLAN_COVERAGE";
            public const string PackagePlanCoverageVariation = "V_PACKAGE_PLAN_COVERAGE_VAR";
            public const string PackagePrice = "V_PACKAGE_PRICE";
            public const string PackageTpaPrice = "V_PACKAGE_TPA_PRICE";
            public const string Parameter = "V_PARAMETER";
            public const string Payroll = "V_PAYROLL";
            public const string Personnel = "V_PERSONNEL";
            public const string PersonnelCadre = "V_PERSONNEL_CADRE";
            public const string Phone = "V_PHONE";
            public const string Plan = "V_PLAN";
            public const string PlanCoverage = "V_PLAN_COVERAGE";
            public const string PlanCoverageParameter = "V_PLAN_COVERAGE_PARAMETER";
            public const string PlanCoverageVariation = "V_PLAN_COVERAGE_VARIATION";
            public const string Policy = "V_POLICY";
            public const string PolicyInsured = "V_POLICY_INSURED";
            public const string PolicyClaimReason = "V_POLICY_CLAIM_REASON";
            public const string PolicyEndorsement = "V_POLICY_ENDORSEMENT";
            public const string PolicyGroup = "V_POLICY_GROUP";
            public const string PolicyPackageProduct = "V_POLICY_PACKAGE_PRODUCT";
            public const string PolicyParameter = "V_POLICY_PARAMETER";
            public const string Portfoy = "V_PORTFOY";
            public const string Privilege = "V_PRIVILEGE";
            public const string Process = "V_PROCESS";
            public const string ProcessCoverage = "V_PROCESS_COVERAGE";
            public const string ProcessGroup = "V_PROCESS_GROUP";
            public const string ProcessGroupProcess = "V_PROCESS_GROUP_PROCESS";
            public const string ProcessGroupRule = "V_PROCESS_GROUP_RULE";
            public const string ProcessList = "V_PROCESS_LIST";
            public const string ProcessRule = "V_PROCESS_RULE";
            public const string Product = "V_PRODUCT";
            public const string ProductMedia = "V_PRODUCT_MEDIA";
            public const string ProductNote = "V_PRODUCT_NOTE";
            public const string ProductParameter = "V_PRODUCT_PARAMETER";
            public const string Provider = "V_PROVIDER";
            public const string ProviderBank = "V_PROVIDER_BANK";
            public const string ProviderBankAccount = "V_PROVIDER_BANK_ACCOUNT";
            public const string ProviderEquipment = "V_PROVIDER_EQUIPMENT";
            public const string ProviderGroupRule = "V_PROVIDER_GROUP_RULE";
            public const string ProviderMedia = "V_PROVIDER_MEDIA";
            public const string ProviderNote = "V_PROVIDER_NOTE";
            public const string ProviderParameter = "V_PROVIDER_PARAMETER";
            public const string ProviderPhone = "V_PROVIDER_PHONE";
            public const string ProviderRule = "V_PROVIDER_RULE";
            public const string ProviderUser = "V_PROVIDER_USER";
            public const string Query = "V_QUERY";
            public const string Question = "V_QUESTION";
            public const string ResponseUser = "V_RESPONSE_USER";
            public const string Rule = "V_RULE";
            public const string RuleCategory = "V_RULE_CATEGORY";
            public const string RptClaimDetails = "V_RPT_CLAIM_DETAILS";
            public const string RptClaimPremium = "V_RPT_CLAIM_PREMIUM";
            public const string RptPayroll = "V_RPT_PAYROLL";
            public const string RptProduction = "V_RPT_PRODUCTION";
            public const string RuleCompany = "V_RULE_COMPANY";
            public const string RuleGroup = "V_RULE_GROUP";
            public const string RulePackage = "V_RULE_PACKAGE";
            public const string RulePolicy = "V_RULE_POLICY";
            public const string RulePolicyGroup = "V_RULE_POLICY_GROUP";
            public const string RuleProduct = "V_RULE_PRODUCT";
            public const string RuleSubproduct = "V_RULE_SUBPRODUCT";
            public const string Staff = "V_STAFF";
            public const string Subproduct = "V_SUBPRODUCT";
            public const string SubproductMedia = "V_SUBPRODUCT_MEDIA";
            public const string SubproductNote = "V_SUBPRODUCT_NOTE";
            public const string SubproductParameter = "V_SUBPRODUCT_PARAMETER";
            public const string TableColumn = "V_TABLE_COLUMN";
            public const string Unit = "V_UNIT";
            public const string User = "V_USER";
            public const string PackageCoverageList = "V_PACKAGE_COVERAGE_LIST";
            public const string ProviderClaimList = "V_PROVIDER_CLAIM_LIST";
        }

        public class PortTssErrors
        {
            public const string UserNameOrPasswordInvalid = "401:Kullanıcı adı veya şifre hatalıdır!...";
            public const string ValidationInvalid = "411:Zorunlu alanlar boş geçilemez!...";
            public const string ProviderNotFound = "404:Kurum bilgisi bulunamadı!...";
            public const string InsuredNotFound = "404:Sigortalı bilgisi bulunamadı!...";
            public const string ClaimNotFound = "404:Hasar bulunamadı!...";
            public const string ClaimNotCreated = "404:Hasar oluşturamadı!...";
            public const string ClaimIcdNotCreated = "404:Hasar Icd oluşturamadı!...";
            public const string ClaimNoteNotCreated = "404:Epikriz Kaydı oluşturamadı!...";
            public const string ProcessNotCreated = "404:İşlem oluşturamadı!...";
            public const string ProcessCoverageNotSent = "404:Hizmet ve Teminat bilgilerini gönderiniz!...";
        }

        public class WsErrors
        {
            public const string NationalityNotFound = "404:Uyruk bilgisi hatalı!...";
            public const string ContactNotFound = "404:Kişi bilgisi bulunamadı!...";
            public const string InsuredNotFound = "404:Sigortalı bilgisi bulunamadı!...";
            public const string PolicyNotFound = "404:Poliçe bilgisi bulunamadı!...";
            public const string PackageNoNotFound = "404:Paket No bilgisi bulunamadı!...";
            public const string IndividualTypeNotFound = "404:Birey Tipi bilgisi bulunamadı!...";
            public const string CompanyCodeNotFound = "404:Şirket Kodu bulunamadı!...";
            public const string InsurerNotFound = "404:Sigorta Ettiren bilgisi bulunamadı!...";
            public const string AgencyNotFound = "404:Acente bilgisi bulunamadı!...";
            public const string EndorsementNotFound = "404:Zeyl bilgisi bulunamadı!...";
            public const string FirstEndorsementNotFound = "404:Başlangıç Zeyl bilgisi bulunamadı!...";
            public const string FirstEndorsementReload = "404:Başlangıç Zeyl daha önce gönderilmiş!...";
            public const string ClaimNotFound = "404:Hasar bilgisi bulunamadı!...";
            public const string PackageNotFound = "404:Paket bilgisi bulunamadı!...";
            public const string PayrollNotFound = "404:Zarf bilgisi bulunamadı!...";
            public const string PayrollReload = "404:Bu kayıt daha önce gönderilmiş!...";
            public const string ExclusionNotCreated = "500:İstisna oluşturulamadı!...";
            public const string PayrollNotCreated = "500:Payroll oluşturulamadı!...";
            public const string PolicyNotCreated = "500:Poliçe oluşturulamadı!...";
            public const string EndorsementNotCreated = "500:Poliçe oluşturulamadı!...";
            public const string ClaimNotCreated = "500:Hasar güncellenemedi!...";
            public const string InsuredExclusionNotCreated = "500:Kişiye istisna eklenemedi!...";
            public const string InsuredNotCreated = "500:Sigortalı bilgileri kaydedilemedi!...";

        }
    }
}
