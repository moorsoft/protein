﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.FunctionParams
{
    public interface SfParam { }
    public class SF_PlanCoverageVarParams : SfParam
    {
        public string p_type { get; set; }
        public long p_package_id { get; set; }
        public string p_plan_coverage_id { get; set; }
        /// <summary>
        /// variationId
        /// </summary>
        public string p_cid { get; set; }
    }
}
