﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Protein.Common.Helpers
{
    public class ValidationHelper
    {
        public static bool isIPAddress(string url)
        {
            bool result = false;
            
            //create our match pattern
            string pattern = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
            //create our Regular Expression object
            Regex check = new Regex(pattern);
            
            //check to make sure an ip address was provided
            if (url == "")
            {
                //no address provided so return false
                result = false;
            }
            else
            {
                //address provided so use the IsMatch Method
                //of the Regular Expression object
                result = check.IsMatch(url, 0);
            }

            return result;
        }

        public static bool isValidEmail(string email)
        {
            bool result = false;

            string pattern = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                     @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                     @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            Regex check = new Regex(pattern, RegexOptions.IgnorePatternWhitespace);

            if (string.IsNullOrEmpty(email))
            {
                result = false;
            }
            else
            {
                result = check.IsMatch(email);
            }

            return result;

        }

        public static bool isValidTC(string tcIdentity)
        {
            bool result = false;

            if (tcIdentity.Length == 11)
            {
                Int64 ATCNO, BTCNO, TcNo;
                long C1, C2, C3, C4, C5, C6, C7, C8, C9, Q1, Q2;

                TcNo = Int64.Parse(tcIdentity);

                ATCNO = TcNo / 100;
                BTCNO = TcNo / 100;

                C1 = ATCNO % 10; ATCNO = ATCNO / 10;
                C2 = ATCNO % 10; ATCNO = ATCNO / 10;
                C3 = ATCNO % 10; ATCNO = ATCNO / 10;
                C4 = ATCNO % 10; ATCNO = ATCNO / 10;
                C5 = ATCNO % 10; ATCNO = ATCNO / 10;
                C6 = ATCNO % 10; ATCNO = ATCNO / 10;
                C7 = ATCNO % 10; ATCNO = ATCNO / 10;
                C8 = ATCNO % 10; ATCNO = ATCNO / 10;
                C9 = ATCNO % 10; ATCNO = ATCNO / 10;
                Q1 = ((10 - ((((C1 + C3 + C5 + C7 + C9) * 3) + (C2 + C4 + C6 + C8)) % 10)) % 10);
                Q2 = ((10 - (((((C2 + C4 + C6 + C8) + Q1) * 3) + (C1 + C3 + C5 + C7 + C9)) % 10)) % 10);

                result = ((BTCNO * 100) + (Q1 * 10) + Q2 == TcNo);
            }


            return result;
        }


    }
}
