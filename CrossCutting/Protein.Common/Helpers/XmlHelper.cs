﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Protein.Common.Helpers
{
    public static class XmlHelper
    {

        public static string DateTimeFormat { get; set; } = "yyyy-MM-ddTHH:mm:sszzz";

        public static T ParseResponseXml<T>(string responseXml, string rootElement)
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(responseXml);

            XmlNode node = xdoc.SelectSingleNode("//" + rootElement);

            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.ElementName = rootElement;
            xRoot.IsNullable = true;

            MemoryStream stm = new MemoryStream();

            StreamWriter stw = new StreamWriter(stm);
            stw.Write(node.OuterXml);
            stw.Flush();

            stm.Position = 0;

            XmlSerializer ser = new XmlSerializer(typeof(T), xRoot);
            T result = (T)ser.Deserialize(stm);

            return result;
        }

    }

    public class CustomDateTimeXmlWriter : XmlTextWriter
    {
        public CustomDateTimeXmlWriter(TextWriter writer) : base(writer) { }
        public CustomDateTimeXmlWriter(Stream stream, Encoding encoding) : base(stream, encoding) { }
        public CustomDateTimeXmlWriter(string filename, Encoding encoding) : base(filename, encoding) { }

        public override void WriteRaw(string data)
        {
            DateTime dt;
            decimal dc;
            if (!decimal.TryParse(data, out dc) &&DateTime.TryParse(data, out dt))
                base.WriteRaw(dt.ToString(XmlHelper.DateTimeFormat));
            else
                base.WriteRaw(data);
        }
    }

    public class CustomDateTimeXmlReader : XmlTextReader
    {
        public CustomDateTimeXmlReader(TextReader input) : base(input) { }
        // define other required constructors

        public override string ReadElementString()
        {
            string data = base.ReadElementString();
            DateTime dt;

            if (DateTime.TryParse(data, null, DateTimeStyles.AdjustToUniversal, out dt))
                return dt.ToString(XmlHelper.DateTimeFormat);
            else
                return data;
        }
    }



}
