﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Helpers
{
    public class CacheHelper
    {
        public static Dictionary<string, MemoryCache> cachesDictionary = new Dictionary<string, MemoryCache>();

        public static void AddCache(string cacheName, string cacheKey, object objectToCache, int cacheMinute, List<string> monitorFilePaths)
        {
            if (!cachesDictionary.ContainsKey(cacheName))
                cachesDictionary.Add(cacheName, new MemoryCache(cacheName));

            CacheItemPolicy policy = new CacheItemPolicy();

            /*if (monitorFilePaths != null && monitorFilePaths.Count > 0)
                policy.ChangeMonitors.Add(new HostFileChangeMonitor(monitorFilePaths));*/

            if (cacheMinute > 0)
                policy.AbsoluteExpiration = DateTime.Now.AddMinutes(cacheMinute);

            MemoryCache cache = cachesDictionary[cacheName];
            cache.Add(cacheKey, objectToCache, policy);
        }

        public static T GetCache<T>(string cacheName, string cacheKey) where T : class
        {
            if (!cachesDictionary.ContainsKey(cacheName))
            {
                cachesDictionary.Add(cacheName, new MemoryCache(cacheName));
                return null;
            }

            MemoryCache cache = cachesDictionary[cacheName];

            try
            {
                return (T)cache[cacheKey];
            }
            catch
            {
                return null;
            }
        }

    }
}
