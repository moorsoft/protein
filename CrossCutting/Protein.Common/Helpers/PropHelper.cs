﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Helpers
{
    public class PropHelper
    {
        public static PropertyInfo FindPropByColumnAttrName(string ColAttrName, Type type)
        {
            try
            {
                return type.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                           .FirstOrDefault(x => x.GetCustomAttribute<System.ComponentModel.DataAnnotations.Schema.ColumnAttribute>() != null
                                              && x.GetCustomAttribute<System.ComponentModel.DataAnnotations.Schema.ColumnAttribute>().Name.Equals(ColAttrName, StringComparison.OrdinalIgnoreCase));
            }
            catch { return null; }
        }
    }
}
