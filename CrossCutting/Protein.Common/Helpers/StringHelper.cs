﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Helpers
{
    public class StringHelper
    {
        public static string GenerateRandomStringWithNumeric(int _length)
        {
            Random _random = new Random();
            string _letters = "abcdefghijklmnoprstuvyzxwq1234567890_";
            //string _letters = "ABCDEFGHiJKLMNOPQRSTUVWXYZ1234567890_";
            char[] _buffer = new char[_length];
            for (int i = 0; i < _length; i++)
            {
                _buffer[i] = _letters[_random.Next(_letters.Length)];
            }
            return new string(_buffer);
        }
        public static string GenerateRandomStringOnlyLetters(int _length)
        {
            Random _random = new Random();
            string _letters = "ABCDEFGHiJKLMNOPQRSTUVWXYZ";
            char[] _buffer = new char[_length];
            for (int i = 0; i < _length; i++)
            {
                _buffer[i] = _letters[_random.Next(_letters.Length)];
            }
            return new string(_buffer);
        }
        public static string GenerateRandomDigitCode(int length)
        {
            Random random = new Random();
            string str = string.Empty;
            for (int i = 0; i < length; i++)
                str = String.Concat(str, random.Next(10).ToString());
            return str;
        }
    }
}
