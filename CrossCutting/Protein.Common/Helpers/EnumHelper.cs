﻿using Protein.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Enums
{
    public class EnumHelper
    {
        public static T GetValueFromText<T>(string description)
        {
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();
            foreach (var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field,
                    typeof(EnumValue)) as EnumValue;
                if (attribute != null)
                {
                    if (attribute.Text == description)
                        return (T)field.GetValue(null);
                }
                else
                {
                    if (field.Name == description)
                        return (T)field.GetValue(null);
                }
            }
            throw new ArgumentException("Not found.", "description");
        }
        public static string GetCode(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            EnumValue[] attributes = (EnumValue[])fi.GetCustomAttributes(typeof(EnumValue), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Code;
            else
                return "";
        }

        public static string GetText(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            EnumValue[] attributes = (EnumValue[])fi.GetCustomAttributes(typeof(EnumValue), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Text;
            else
                return "";
        }

        public static List<EnumValue> GetValues<T>()
        {
            List<EnumValue> values = new List<EnumValue>();

            Type enumType = typeof(T);
            if (enumType.BaseType != typeof(Enum))
                throw new ArgumentException("T must be of type System.Enum");

            foreach (var item in enumType.GetFields())
            {
                if (item.GetCustomAttributes().Count() > 0)
                    values.Add(item.GetCustomAttribute(typeof(EnumValue), false) as EnumValue);
            }
            return values;
        }
        public static string[] GetValues(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            EnumValue[] attributes = (EnumValue[])fi.GetCustomAttributes(typeof(EnumValue), false);

            string[] results = new string[2];
            if (attributes != null && attributes.Length > 0)
            {
                results[0] = attributes[0].Code;
                results[1] = attributes[0].Text;
            }
            return results;
        }

        public static IEnumerable<T> EnumToList<T>()
        {
            Type enumType = typeof(T);

            // Can't use generic type constraints on value types,
            // so have to do check like this
            if (enumType.BaseType != typeof(Enum))
                throw new ArgumentException("T must be of type System.Enum");

            Array enumValArray = Enum.GetValues(enumType);
            List<T> enumValList = new List<T>(enumValArray.Length);

            foreach (int val in enumValArray)
            {
                enumValList.Add((T)Enum.Parse(enumType, val.ToString()));
            }

            return enumValList;
        }
    }
}
