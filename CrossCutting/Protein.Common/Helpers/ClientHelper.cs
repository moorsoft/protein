﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
namespace Protein.Common.Helpers
{
    public class ClientHelper
    {
        public static string GetIpAddress()
        {
            if (HttpContext.Current == null)
                return "";
            return HttpContext.Current.Request.UserHostAddress;
        }
    }
}
