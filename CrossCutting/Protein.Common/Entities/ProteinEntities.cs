﻿using Newtonsoft.Json;
using Oracle.DataAccess.Types;
using Protein.Common.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Protein.Common.Entities
{
    public class ProteinEntities
    {
        #region Generated code will paste here
        //[Table("T_SP_RESPONSE")]
        //public class SpResponse<T>
        //{
        //    [Required]
        //    [Column("ID")]
        //    public Int64 Id { get; set; }
        //    [Required]
        //    [Column("SP_REQUEST_ID")]
        //    [MaxLength(100, ErrorMessage = "")]
        //    public string SpRequestId { get; set; }
        //    [Column("OBJECT_ID")]
        //    [MaxLength(100, ErrorMessage = "")]
        //    public string ObjectId { get; set; }
        //    [Column("PK_ID")]
        //    public Int64 PkId { get; set; }
        //    [Required]
        //    [Column("CODE")]
        //    [Display(Name = "CODE", Description = "Code")]
        //    [MaxLength(10, ErrorMessage = "")]
        //    public string Code { get; set; }
        //    [Required]
        //    [Column("MESSAGE")]
        //    [Display(Name = "MESSAGE", Description = "Mesaj")]
        //    [MaxLength(4000, ErrorMessage = "")]
        //    public string Message { get; set; }
        //    [Column("SP_REQUEST_DATE")]
        //    public DateTime SpRequestDate { get; set; }
        //    [Column("SP_RESPONSE_DATE")]
        //    public DateTime SpResponseDate { get; set; }

        //    //public T Data { get; set; }
        //}

        [Table("T_SP_RESPONSE")]
        public class SpResponse
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("SP_REQUEST_ID")]
            [MaxLength(100, ErrorMessage = "")]
            public string SpRequestId { get; set; }
            [Column("OBJECT_ID")]
            [MaxLength(100, ErrorMessage = "")]
            public string ObjectId { get; set; }
            [Column("PK_ID")]
            public Int64 PkId { get; set; }
            [Required]
            [Column("CODE")]
            [Display(Name = "CODE", Description = "Code")]
            [MaxLength(10, ErrorMessage = "")]
            public string Code { get; set; }
            [Required]
            [Column("MESSAGE")]
            [Display(Name = "MESSAGE", Description = "Mesaj")]
            [MaxLength(4000, ErrorMessage = "")]
            public string Message { get; set; }
            [Column("SP_REQUEST_DATE")]
            public DateTime SpRequestDate { get; set; }
            [Column("SP_RESPONSE_DATE")] 
            public DateTime SpResponseDate { get; set; }
        }
        [Table("T_CONTACT")]
        public class Contact : ISpExecute3
        {
            [Required]
            [Column("ID",TypeName ="BASE")]
            public Int64 Id { get; set; }
            [Display(Name = "Tipi", Description = "Contact Tipi")]
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Display(Name = "TITLE", Description = "Ünvan")]
            [Column("TITLE")]
            [MaxLength(200, ErrorMessage = "")]
            public string Title { get; set; }
            [Display(Name = "TAX NUMBER", Description = "Vergi Numarası")]
            [Column("TAX_NUMBER")]
            [MaxLength(100, ErrorMessage = "")]
            public Int64? TaxNumber { get; set; }
            [Display(Name = "TAX OFFICE", Description = "Vergi Dairesi")]
            [Column("TAX_OFFICE")]
            [MaxLength(100, ErrorMessage = "")]
            public string TaxOffice { get; set; }
            [Display(Name = "IDENTITY NO", Description = "T.C. Kimlik No")]
            [Column("IDENTITY_NO")]
            [MaxLength(20, ErrorMessage = "")]
            public Int64? IdentityNo { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Display(Name = "STATUS", Description = "Statüs")]
            [Column("STATUS")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_CONTACT_EMAIL")]
        public class ContactEmail : ISpExecute3
        {
            [Required]
            [Column("ID", TypeName = "BASE")]
            public Int64 Id { get; set; }
            [Column("CONTACT_ID", TypeName = "BASE")]
            public Int64 ContactId { get; set; }
            [Column("EMAIL_ID", TypeName = "BASE")]
            public Int64 EmailId { get; set; }
            [Display(Name = "STATUS", Description = "Statüs")]
            [Column("STATUS")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
            [Display(Name = "DETAILS", Description = "Detay")]
            [Column("DETAILS")]
            [MaxLength(100, ErrorMessage = "")]
            public string Details { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }

        }
        [Table("T_CONTACT_PHONE")]
        public class ContactPhone : ISpExecute3
        {
            [Required]
            [Column("ID", TypeName = "BASE")]
            public Int64 Id { get; set; }
            [Column("CONTACT_ID", TypeName = "BASE")]
            public Int64 ContactId { get; set; }
            [Column("PHONE_ID", TypeName = "BASE")]
            public Int64 PhoneId { get; set; }
            [Display(Name = "STATUS", Description = "Statüs")]
            [Column("STATUS")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_AGENCY_PHONE")]
        public class AgencyPhone
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("AGENCY_ID")]
            public Int64 AgencyId { get; set; }
            [Column("PHONE_ID")]
            public Int64 PhoneId { get; set; }
            [Display(Name = "STATUS", Description = "Statüs")]
            [Column("STATUS")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
            [Column("SP_RESPONSE_ID")]
            public Int64? SpResponseId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
        }
        [Table("T_CLAIM_STATU_CHANGE")]
        public class ClaimStatuChange
        {
            [Required]
            [Column("ID")]
            public Int64 ID { get; set; }
            [Column("CORELATION_ID")]
            public string CORELATION_ID { get; set; }
            [Column("CLAIM_ID")]
            public Int64? CLAIM_ID { get; set; }
            [Column("CLAIM_STATUS")]
            public string CLAIM_STATUS { get; set; }
            [Column("CLAIM_AMOUNT")]
            public decimal? CLAIM_AMOUNT { get; set; }
            [Column("PAYROLL_ID")]
            public Int64? PAYROLL_ID { get; set; }
            [Column("PAYROLL_STATUS")]
            public string PAYROLL_STATUS { get; set; }
            [Column("PAYROLL_AMOUNT")]
            public decimal? PAYROLL_AMOUNT { get; set; }
            [Column("CLAIM_ID")]
            public Int64? REASON_ID { get; set; }
            [Column("PAYMENT_DATE")]
            public DateTime? PAYMENT_DATE { get; set; }
            [Column("RESULT")]
            public int? RESULT { get; set; }
            [Column("RESULT_DESC")]
            public string RESULT_DESC { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NEW_VERSION_ID { get; set; }
            [Column("STATUS")]
            public string STATUS { get; set; }
        }
        [Table("T_CONTACT_ADDRESS")]
        public class ContactAddress :ISpExecute3
        {
            [Required]
            [Column("ID", TypeName = "BASE")]
            public Int64 Id { get; set; }
            [Column("CONTACT_ID", TypeName = "BASE")]
            public Int64 ContactId { get; set; }
            [Column("ADDRESS_ID", TypeName = "BASE")]
            public Int64 AddressId { get; set; }
            [Display(Name = "STATUS", Description = "Statüs")]
            [Column("STATUS")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_COUNTRY")]
        public class Country
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Display(Name = "A2 CODE", Description = "Ülke Adı Kısaltma")]
            [Column("A2_CODE")]
            [MaxLength(3, ErrorMessage = "")]
            public string A2Code { get; set; }
            [Column("A3_CODE")]
            [MaxLength(3, ErrorMessage = "")]
            public string A3Code { get; set; }
            [Required]
            [Display(Name = "NUM CODE", Description = "Ülke Kodu")]
            [Column("NUM_CODE")]
            [MaxLength(3, ErrorMessage = "")]
            public string NumCode { get; set; }
            [Required]
            [Display(Name = "NAME", Description = "Adı")]
            [Column("NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("PHONE_CODE")]
            [MaxLength(10, ErrorMessage = "")]
            public string PhoneCode { get; set; }
            [Display(Name = "STATUS", Description = "Statüs")]
            [Column("STATUS")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PERSON")]
        public class Person
        {
            [Required]
            [Column("ID", TypeName = "BASE")]
            public Int64 Id { get; set; }
            [Column("PROFESSION_TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string ProfessionType { get; set; }
            [Required]
            [Column("CONTACT_ID", TypeName = "BASE")]
            public Int64 ContactId { get; set; }
            [Column("FIRST_NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string FirstName { get; set; }
            [Column("MIDDLE_NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string MiddleName { get; set; }
            [Column("LAST_NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string LastName { get; set; }
            [Column("BIRTHDATE")]
            public DateTime? Birthdate { get; set; }
            [Column("GENDER")]
            [MaxLength(3, ErrorMessage = "")]
            public string Gender { get; set; }
            [Column("NATIONALITY_ID")]
            public Int64? NationalityId { get; set; }
            [Column("PHOTO")]
            [MaxLength(500, ErrorMessage = "")]
            public string Photo { get; set; }
            [Column("LICENSE_NO")]
            [MaxLength(20, ErrorMessage = "")]
            public string LicenseNo { get; set; }
            [Column("BIRTHPLACE")]
            [MaxLength(100, ErrorMessage = "")]
            public string Birthplace { get; set; }
            [Column("NAME_OF_FATHER")]
            [MaxLength(100, ErrorMessage = "")]
            public string NameOfFather { get; set; }
            [Column("PASSPORT_NO")]
            [MaxLength(20, ErrorMessage = "")]
            public string PassportNo { get; set; }
            [Column("MARITAL_STATUS")]
            [MaxLength(3, ErrorMessage = "")]
            public string MaritalStatus { get; set; }
            [Column("IS_VIP")]
            [MaxLength(1, ErrorMessage = "")]
            public string IsVip { get; set; }
            [Column("VIP_TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string VipType { get; set; }
            [Column("VIP_TEXT")]
            [MaxLength(100, ErrorMessage = "")]
            public string VipText { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Display(Name = "STATUS", Description = "Statüs")]
            [Column("STATUS")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_CORPORATE")]
        public class Corporate : ISpExecute3
        {
            [Required]
            [Column("ID", TypeName = "BASE")]
            public Int64 Id { get; set; }
            [Required]
            [Column("CONTACT_ID")]
            public Int64 ContactId { get; set; }
            [Display(Name = "Type", Description = "Tipi")]
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("NAME")]
            [MaxLength(200, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Display(Name = "STATUS", Description = "Statüs")]
            [Column("STATUS")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }


        }
        [Table("T_ADDRESS")]
        public class Address : ISpExecute3
        {
            [Required]
            [Column("ID", TypeName = "BASE")]
            public Int64 Id { get; set; }
            [Required]
            [Display(Name = "TYPE", Description = "Tipi")]
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("CITY_ID")]
            public Int64? CityId { get; set; }
            [Column("COUNTY_ID")]
            public Int64? CountyId { get; set; }
            [Required]
            [Display(Name = "DETAILS", Description = "Detay")]
            [Column("DETAILS")]
            [MaxLength(1000, ErrorMessage = "")]
            public string Details { get; set; }
            [Column("ZIP_CODE")]
            [MaxLength(20, ErrorMessage = "")]
            public string ZipCode { get; set; }
            [Column("DISTRICT")]
            [MaxLength(100, ErrorMessage = "")]
            public string District { get; set; }
            [Display(Name = "IS PRIMARY", Description = "Is Primary")]
            [Column("IS_PRIMARY")]
            [MaxLength(3, ErrorMessage = "")]
            public string IsPrimary { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Display(Name = "STATUS", Description = "Statüs")]
            [Column("STATUS")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_EMAIL")]
        public class Email:ISpExecute3
        {
            [Required]
            [Column("ID", TypeName = "BASE")]
            public Int64 Id { get; set; }
            [Required]
            [Display(Name = "DETAILS", Description = "Detay")]
            [Column("DETAILS")]
            [MaxLength(100, ErrorMessage = "")]
            public string Details { get; set; }
            [Required]
            [Display(Name = "TYPE", Description = "Tipi")]
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Display(Name = "IS PRIMARY", Description = "Is Primary")]
            [Column("IS_PRIMARY")]
            [MaxLength(3, ErrorMessage = "")]
            public string IsPrimary { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Display(Name = "STATUS", Description = "Statüs")]
            [Column("STATUS")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_LOG")]
        public class Log
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Display(Name = "CODE", Description = "Kod")]
            [Column("CODE")]
            [MaxLength(10, ErrorMessage = "")]
            public string Code { get; set; }
            [Required]
            [Display(Name = "PROCESS", Description = "Süreç")]
            [Column("PROCESS")]
            [MaxLength(45, ErrorMessage = "")]
            public string Process { get; set; }
            [Column("PROCESSOR_ID")]
            public Int64 ProcessorId { get; set; }
            [Column("PROCESSOR")]
            [MaxLength(45, ErrorMessage = "")]
            public string Processor { get; set; }
            [Column("PROCESSED_ID")]
            public Int64 ProcessedId { get; set; }
            [Column("PROCESSED")]
            [MaxLength(45, ErrorMessage = "")]
            public string Processed { get; set; }
            [Required]
            [Display(Name = "PROCESS TIME", Description = "İşlem Zamanı")]
            [Column("PROCESS_TIME")]
            public DateTime ProcessTime { get; set; }
            [Column("OLD_VALUE")]
            [MaxLength(400, ErrorMessage = "")]
            public string OldValue { get; set; }
            [Column("NEW_VALUE")]
            [MaxLength(400, ErrorMessage = "")]
            public string NewValue { get; set; }
            [Column("DESCRIPTION")]
            [MaxLength(400, ErrorMessage = "")]
            public string Description { get; set; }
            [Column("IP")]
            [MaxLength(100, ErrorMessage = "")]
            public string Ip { get; set; }

        }
        [Table("T_PHONE")]
        public class Phone : ISpExecute3
        {
            [Required]
            [Column("ID", TypeName = "BASE")]
            public Int64 Id { get; set; }
            [Required]
            [Column("COUNTRY_ID")]
            public Int64 CountryId { get; set; }
            [Required]
            [Display(Name = "NO", Description = "No")]
            [Column("NO")]
            [MaxLength(20, ErrorMessage = "")]
            public string No { get; set; }
            [Required]
            [Display(Name = "TYPE", Description = "Tipi")]
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("EXTENSION")]
            [MaxLength(10, ErrorMessage = "")]
            public string Extension { get; set; }
            [Display(Name = "IS PRIMARY", Description = "Is Primary")]
            [Column("IS_PRIMARY")]
            [MaxLength(3, ErrorMessage = "")]
            public string IsPrimary { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Display(Name = "STATUS", Description = "Statüs")]
            [Column("STATUS")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_STATE")]
        public class State
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Display(Name = "NAME", Description = "Adı")]
            [Column("NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Required]
            [Column("COUNTRY_ID")]
            public Int64 CountryId { get; set; }
            [Display(Name = "STATUS", Description = "Statüs")]
            [Column("STATUS")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_CITY")]
        public class City
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("CODE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Code { get; set; }
            [Required]
            [Display(Name = "NAME", Description = "Adı")]
            [Column("NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("STATE_ID")]
            public Int64 StateId { get; set; }
            [Display(Name = "COUNTRY ID", Description = "Ülke Id")]
            [Column("COUNTRY_ID")]
            public Int64 CountryId { get; set; }
            [Display(Name = "STATUS", Description = "Statüs")]
            [Column("STATUS")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_COUNTY")]
        public class County
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Display(Name = "NAME", Description = "Adı")]
            [Column("NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("CITY_ID")]
            public Int64 CityId { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("CODE")]
            public int Code { get; set; }
            [Display(Name = "STATUS", Description = "Statüs")]
            [Column("STATUS")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_BANK")]
        public class Bank
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Display(Name = "NAME", Description = "Adı")]
            [Column("NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("SWIFT_CODE")]
            [MaxLength(20, ErrorMessage = "")]
            public string SwiftCode { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
            [Column("CODE")]
            [MaxLength(20, ErrorMessage = "")]
            public string Code { get; set; }

        }
        [Table("T_BANK_BRANCH")]
        public class BankBranch
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("CITY_ID")]
            public Int64? CityId { get; set; }
            [Column("NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("CODE")]
            [MaxLength(20, ErrorMessage = "")]
            public string Code { get; set; }
            [Required]
            [Column("BANK_ID")]
            public Int64 BankId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_BANK_ACCOUNT")]
        public class BankAccount :ISpExecute3
        {
            [Required]
            [Column("ID", TypeName = "BASE")]
            public Int64 Id { get; set; }
            [Column("NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("IBAN")]
            [MaxLength(100, ErrorMessage = "")]
            public string Iban { get; set; }
            [Column("BANK_BRANCH_ID")]
            public Int64? BankBranchId { get; set; }
            [Required]
            [Column("CURRENCY_TYPE")]
            [Display(Name = "CURRENCY TYPE", Description = "Para Birimi Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string CurrencyType { get; set; }
            [Column("NO")]
            [MaxLength(100, ErrorMessage = "")]
            public string No { get; set; }
            [Required]
            [Column("BANK_ID")]
            public Int64 BankId { get; set; }
            [Column("IS_PRIMARY")]
            [MaxLength(3, ErrorMessage = "")]
            public string IsPrimary { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_NOTE")]
        public class Note : ISpExecute3
        {
            [Required]
            [Column("ID", TypeName = "BASE")]
            public Int64 Id { get; set; }
            [Required]
            [Column("TYPE")]
            [Display(Name = "TYPE", Description = "Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Required]
            [Column("DESCRIPTION")]
            [Display(Name = "DESCRIPTION", Description = "Açıklama")]
            [MaxLength(2000, ErrorMessage = "")]
            public string Description { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_MEDIA")]
        public class Media
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("NAME")]
            [MaxLength(200, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("FILENAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string FileName { get; set; }
            [Column("FILE_CONTENT")]
            [MaxLength(100, ErrorMessage = "")]
            public byte[] FileContent { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
        }
        [Table("T_REASON")]
        public class Reason
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("STATUS_NAME")]
            [MaxLength(400, ErrorMessage = "")]
            public string StatusName { get; set; }
            [Required]
            [Column("STATUS_ORDINAL")]
            [Display(Name = "STATUS ORDINAL", Description = "Statüs Sırası")]
            [MaxLength(3, ErrorMessage = "")]
            public string StatusOrdinal { get; set; }
            [Required]
            [Column("ORDINAL")]
            [Display(Name = "ORDINAL", Description = "Sıra")]
            [MaxLength(3, ErrorMessage = "")]
            public string Ordinal { get; set; }
            [Column("DESCRIPTION")]
            [MaxLength(1000, ErrorMessage = "")]
            public string Description { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_APPLICATION")]
        public class Application
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("CODE")]
            [MaxLength(50, ErrorMessage = "")]
            public string Code { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_CATEGORY")]
        public class Category
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("PARENT_ID")]
            public Int64? ParentId { get; set; }
            [Required]
            [Column("ORGANIZATION_ID")]
            public Int64? OrganizationId { get; set; }
            [Column("NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("CODE")]
            [MaxLength(50, ErrorMessage = "")]
            public string Code { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_ASSET")]
        public class Asset
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("CATEGORY_ID")]
            public Int64? CategoryId { get; set; }
            [Column("NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("CODE")]
            [MaxLength(50, ErrorMessage = "")]
            public string Code { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PRIVILEGE")]
        public class Privilege
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("ASSET_ID")]
            public Int64? AssetId { get; set; }
            [Column("NAME")]
            [MaxLength(200, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("CODE")]
            [MaxLength(50, ErrorMessage = "")]
            public string Code { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_ORGANIZATION")]
        public class Organization
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("CODE")]
            [MaxLength(50, ErrorMessage = "")]
            public string Code { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_UNIT")]
        public class Unit
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("ORGANIZATION_ID")]
            public Int64? OrganizationId { get; set; }
            [Column("PARENT_ID")]
            public Int64? ParentId { get; set; }
            [Column("NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("CODE")]
            [MaxLength(50, ErrorMessage = "")]
            public string Code { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_CADRE")]
        public class Cadre
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("UNIT_ID")]
            public Int64? UnitId { get; set; }
            [Column("NAME")]
            [MaxLength(200, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("CODE")]
            [MaxLength(50, ErrorMessage = "")]
            public string Code { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_CADRE_PRIVILEGE")]
        public class CadrePrivilege
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("PRIVILEGE_ID")]
            public Int64 PrivilegeId { get; set; }
            [Column("UNIT_PRIVILEGE_TYPE")]
            public Int64 UnitPrivilegeType { get; set; }
            [Column("CADRE_ID")]
            public Int64 CadreId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_USER")]
        public class User : ISpExecute3
        {
            [Required]
            [Column("ID", TypeName = "BASE")]
            public Int64 Id { get; set; }
            [Column("USERNAME")]
            [MaxLength(45, ErrorMessage = "")]
            public string Username { get; set; }
            [Column("PASSWORD")]
            [Display(Name = "PASSWORD", Description = "Şifre")]
            [MaxLength(45, ErrorMessage = "")]
            public string Password { get; set; }
            [Column("EMAIL")]
            [MaxLength(100, ErrorMessage = "")]
            public string Email { get; set; }
            [Column("MOBILE")]
            [MaxLength(20, ErrorMessage = "")]
            public string Mobile { get; set; }
            [Column("ACTIVATION_TOKEN")]
            [MaxLength(45, ErrorMessage = "")]
            public string ActivationToken { get; set; }
            [Column("TOKEN")]
            [MaxLength(45, ErrorMessage = "")]
            public string Token { get; set; }
            [Column("IS_REMEMBER_ME")]
            [Display(Name = "IS REMEMBER ME", Description = "Beni Hatırla")]
            [MaxLength(3, ErrorMessage = "")]
            public string IsRememberMe { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PERSONNEL")]
        public class Personnel
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("CONTACT_ID")]
            public Int64? ContactId { get; set; }
            [Column("USER_ID")]
            public Int64? UserId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PERSONNEL_CADRE")]
        public class PersonnelCadre
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("PERSONNEL_ID")]
            public Int64 PersonnelId { get; set; }
            [Column("CADRE_ID")]
            public Int64 CadreId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
        }
        [Table("T_DEPUTATION")]
        public class Deputation
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("PERSONNEL_CADRE_ID")]
            public Int64 PersonnelCadreId { get; set; }
            [Column("DEPUTY_ID")]
            public Int64 DeputyId { get; set; }
            [Column("START_DATE")]
            public DateTime StartDate { get; set; }
            [Column("END_DATE")]
            public DateTime EndDate { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PRICE_LIST")]
        public class PriceList
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("VALID_FROM")]
            public DateTime ValidFrom { get; set; }
            [Column("VALID_TO")]
            public DateTime ValidTo { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PACKAGE")]
        public class Package
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("NO")]
            [MaxLength(6, ErrorMessage = "")]
            public string No { get; set; }
            [Required]
            [Column("NAME")]
            [Display(Name = "NAME", Description = "Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Required]
            [Column("TYPE")]
            [Display(Name = "TYPE", Description = "Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("PRICE_LIST_ID")]
            public Int64? PriceListId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
            [Column("VALID_FROM")]
            public DateTime? ValidFrom { get; set; }
            [Column("VALID_TO")]
            public DateTime? ValidTo { get; set; }
            [Column("IS_OPEN_TO_WS")] public string IS_OPEN_TO_WS { get; set; }
        }
        [Table("T_COMPANY")]
        public class Company
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("NAME")]
            [Display(Name = "NAME", Description = "Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("ADDRESS_ID")]
            public Int64? AddressId { get; set; }
            [Column("CONTACT_ID")]
            public Int64 ContactId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_AGENCY")]
        public class Agency
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("NO")]
            [MaxLength(20, ErrorMessage = "")]
            public string No { get; set; }
            [Required]
            [Column("NAME")]
            [Display(Name = "NAME", Description = "Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Required]
            [Column("COMPANY_ID")]
            public Int64 CompanyId { get; set; }
            [Column("CONTACT_ID")]
            public Int64? ContactId { get; set; }
            [Column("ADDRESS_ID")]
            public Int64? AddressId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
            [Column("REGION_CODE")]
            public string RegionCode { get; set; }
            [Column("REGION_NAME")]
            public string RegionName { get; set; }
            [Column("PLATE_NO")]
            public string PlateNo { get; set; }
            [Column("CONTACT_FIRST_NAME")]
            public string ContactFirstName { get; set; }
            [Column("CONTACT_LAST_NAME")]
            public string ContactLastName { get; set; }
            [Column("CONTACT_PHONE")]
            public string ContactPhone { get; set; }
            [Column("ESTABLISHMENT_DATE")]
            public DateTime? EstablishmentDate { get; set; }
            [Column("CLOSE_DOWN_DATE")]
            public DateTime? CloseDownDate { get; set; }
            [Column("IS_OPEN")]
            public string IsOpen { get; set; }

        }
        [Table("T_POLICY_GROUP")]
        public class PolicyGroup
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("NAME")]
            [Display(Name = "NAME", Description = "Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("PARENT_ID")]
            public Int64? ParentId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_POLICY")]
        public class Policy
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("NO")]
            [MaxLength(20, ErrorMessage = "")]
            public string No { get; set; }
            [Column("END_DATE")]
            public DateTime? EndDate { get; set; }
            [Column("POLICY_GROUP_ID")]
            public Int64? PolicyGroupId { get; set; }
            [Column("PARENT_ID")]
            public Int64? ParentId { get; set; }
            [Column("PREVIOUS_ID")]
            public Int64? PreviousId { get; set; }
            [Column("RENEWAL_NO")]
            [Display(Name = "RENEWAL NO", Description = "Yenileme No")]
            public int RenewalNo { get; set; }
            [Column("PAYMENT_TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string PaymentType { get; set; }
            [Column("INSTALLMENT_COUNT")]
            public int? InstallmentCount { get; set; }
            [Column("SBM_NO")]
            [MaxLength(20, ErrorMessage = "")]
            public string SbmNo { get; set; }
            [Column("LOCATION_TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string LocationType { get; set; }
            [Column("CASH_PAYMENT_TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string CashPaymentType { get; set; }
            [Column("AGENCY_ID")]
            public Int64? AgencyId { get; set; }
            [Column("COMPANY_ID")]
            public Int64? CompanyId { get; set; }
            [Column("SUBPRODUCT_ID")]
            public Int64? SubproductId { get; set; }
            [Column("INSURER")]
            public Int64? Insurer { get; set; }
            [Column("PREMIUM")]
            public Decimal? Premium { get; set; }
            [Column("CASH_AMOUNT")]
            public Decimal? CashAmount { get; set; }
            [Column("TAX")]
            public Decimal? Tax { get; set; }
            [Column("EXCHANGE_RATE_ID")]
            public Int64? ExchangeRateId { get; set; }
            [Column("IS_OPEN_TO_CLAIM")]
            [Display(Name = "IS_OPEN_TO_CLAIM", Description = "Hasar Durumu")]
            [MaxLength(3, ErrorMessage = "")]
            public string IsOpenToClaim { get; set; }
            [Column("SBM_AUTHORIZATION_CODE")]
            [MaxLength(50, ErrorMessage = "")]
            public string SbmAuthorizationCode { get; set; }
            [Column("LAST_ENDORSEMENT_ID")]
            public Int64? LastEndorsementId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
            [Column("INTEGRATION_STATUS")]
            public string IntegrationStatus { get; set; }

        }
        [Table("T_ENDORSEMENT")]
        public class Endorsement
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("CATEGORY")]
            [Display(Name = "CATEGORY", Description = "Kategorisi")]
            [MaxLength(3, ErrorMessage = "")]
            public string Category { get; set; }
            [Required]
            [Column("NO")]
            [Display(Name = "NO", Description = "No")]
            public int No { get; set; }
            [Required]
            [Column("POLICY_ID")]
            public Int64 PolicyId { get; set; }
            [Required]
            [Column("TYPE")]
            [Display(Name = "TYPE", Description = "Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Required]
            [Column("TYPE_DESCRIPTION_TYPE")]
            [Display(Name = "TYPE_DESCRIPTION_TYPE", Description = "Tipinin Türü")]
            [MaxLength(3, ErrorMessage = "")]
            public string TypeDescriptionType { get; set; }
            [Required]
            [Column("TYPE_DESCRIPTION")]
            [Display(Name = "TYPE_DESCRIPTION", Description = "Tipinin Açıklaması")]
            [MaxLength(1000, ErrorMessage = "")]
            public string TypeDescription { get; set; }
            [Column("START_DATE")]
            public DateTime? StartDate { get; set; }
            [Column("TRANSFER_DATE")]
            public DateTime? TRANSFER_DATE { get; set; }
            [Column("DATE_OF_ISSUE")]
            public DateTime? DateOfIssue { get; set; }
            [Column("REGISTRATION_DATE")]
            public DateTime? RegistrationDate { get; set; }
            [Column("REASON_ID")]
            public Int64? ReasonId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
            [Column("SBM_AUTHORIZATION_CODE")]
            public string SBM_AUTHORIZATION_CODE { get; set; }
            [Column("SBM_STATUS")]
            public string SbmStatus { get; set; }
            [Column("PREMIUM")]
            public decimal? Premium { get; set; }

        }
        [Table("T_INSURED")]
        public class Insured
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("CONTACT_ID")]
            public Int64 ContactId { get; set; }
            [Required]
            [Column("INDIVIDUAL_TYPE")]
            [Display(Name = "INDIVIDUAL", Description = "Birey Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string IndividualType { get; set; }
            [Column("REGISTRATION_NO")]
            [MaxLength(20, ErrorMessage = "")]
            public string RegistrationNo { get; set; }
            [Column("PREMIUM")]
            public decimal? Premium { get; set; } //Zeyil Primi
            [Column("INITIAL_PREMIUM")]
            public decimal? InitialPremium { get; set; } //Ham Prim
            [Column("TOTAL_PREMIUM")]
            public decimal? TotalPremium { get; set; } //Toplam Prim
            [Column("TAX")]
            public decimal? Tax { get; set; }
            [Required]
            [Column("FAMILY_NO")]
            public Int64 FamilyNo { get; set; }
            [Column("PARENT_ID")]
            public Int64? ParentId { get; set; }
            [Column("RENEWAL_GUARANTEE_TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string RenewalGuaranteeType { get; set; }
            [Required]
            [Column("PACKAGE_ID")]
            public Int64? PackageId { get; set; }
            [Column("PRICE_ID")]
            public Int64? PriceId { get; set; }
            [Column("IS_OPEN_TO_CLAIM")]
            [Display(Name = "IS_OPEN_TO_CLAIM", Description = "Hasar Durumu")]
            [MaxLength(3, ErrorMessage = "")]
            public string IsOpenToClaim { get; set; }
            [Column("COMPANY_ENTRANCE_DATE")]
            public DateTime? CompanyEntranceDate { get; set; }
            [Column("RENEWAL_DATE")]
            public DateTime? RenewalDate { get; set; }
            [Column("FIRST_INSURED_DATE")]
            public DateTime? FirstInsuredDate { get; set; }
            [Column("FIRST_AMBULANT_COVERAGE_DATE")]
            public DateTime? FirstAmbulantCoverageDate { get; set; }
            [Column("BIRTH_COVERAGE_DATE")]
            public DateTime? BirthCoverageDate { get; set; }
            [Column("EXCHANGE_RATE_ID")]
            public Int64? ExchangeRateId { get; set; }
            [Column("RENEWAL_GUARANTEE_TEXT")]
            [MaxLength(100, ErrorMessage = "")]
            public string RenewalGuaranteeText { get; set; }
            [Column("LAST_ENDORSEMENT_ID")]
            public Int64? LastEndorsementId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("COMPANY_ID")]
            public Int64? CompanyId { get; set; }
            [Column("POLICY_ID")]
            public Int64? PolicyId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
            [Column("COMPANY_INSURED_NO")]
            public string CompanyInsuredNo { get; set; }

        }
        [Table("T_ENDORSEMENT_INSURED")]
        public class EndorsementInsured
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("ENDORSEMENT_ID")]
            public Int64 EndorsementId { get; set; }
            [Column("OLD_INSURED_ID")]
            public Int64? OldInsuredId { get; set; }
            [Required]
            [Column("INSURED_ID")]
            public Int64 InsuredId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
            [Column("RENEWAL_GUARANTEE_TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string RenewalGuaranteeType { get; set; }
            [Column("RENEWAL_GUARANTEE_TEXT")]
            [MaxLength(100, ErrorMessage = "")]
            public string RenewalGuaranteeText { get; set; }
            [Column("RENEWAL_DATE")]
            public DateTime? RenewalDate { get; set; }
            [Required]
            [Column("PACKAGE_ID")]
            public Int64? PackageId { get; set; }
            [Column("IS_OPEN_TO_CLAIM")]
            [Display(Name = "IS_OPEN_TO_CLAIM", Description = "Hasar Durumu")]
            [MaxLength(3, ErrorMessage = "")]
            public string IsOpenToClaim { get; set; }
            [Column("PREMIUM")]
            public decimal? Premium { get; set; } //Zeyil Primi
            [Column("TOTAL_PREMIUM")]
            public decimal? TotalPremium { get; set; } //Toplam Prim
            [Column("INSURED_STATUS")]
            [Display(Name = "INSURED_STATUS", Description = "Sigortalı Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string InsuredStatus { get; set; }

        }
        [Table("T_INSURED_TRANSFER")]
        public class InsuredTransfer
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("COMPANY_NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string CompanyName { get; set; }
            [Required]
            [Column("INSURED_ID")]
            public Int64 InsuredId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_INSTALLMENT")]
        public class Installment
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("POLICY_ID")]
            public Int64 PolicyId { get; set; }
            [Column("ENDORSEMENT_ID")]
            public Int64? EndorsementId { get; set; }
            [Column("AMOUNT")]
            [Display(Name = "AMOUNT", Description = "Miktar")]
            public Decimal Amount { get; set; }
            [Required]
            [Column("DUE_DATE")]
            [Display(Name = "DUE DATE", Description = "Due Tarihi")]
            public DateTime DueDate { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_RULE_CATEGORY")]
        public class RuleCategory
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("RESULT_TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string ResultType { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_RULE")]
        public class Rule
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("DATA")]
            [MaxLength(1000, ErrorMessage = "")]
            public string Data { get; set; }
            [Column("RESULT")]
            [MaxLength(100, ErrorMessage = "")]
            public string Result { get; set; }
            [Column("RESULT_SECOND")]
            [MaxLength(100, ErrorMessage = "")]
            public string ResultSecond { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("CATEGORY_ID")]
            public Int64? CategoryId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
            [Column("REASONID")]
            public Int64? ReasonId { get; set; }
            [Column("CLAIMSTATUS")]
            [MaxLength(3, ErrorMessage = "")]
            public string ClaimStatus { get; set; }

        }
        [Table("T_EXCLUSION")]
        public class Exclusion
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("CONTACT_ID")]
            public Int64 ContactId { get; set; }
            [Column("RULE_ID")]
            public Int64? RuleId { get; set; }
            [Column("ADDITIONAL_PREMIUM")]
            public int? AdditionalPremium { get; set; }
            [Required]
            [Column("DESCRIPTION")]
            [Display(Name = "DESCRIPTION", Description = "Açıklama")]
            [MaxLength(1000, ErrorMessage = "")]
            public string Description { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("IS_OPEN_TO_PRINT")]
            public int? IS_OPEN_TO_PRINT { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_INSURED_EXCLUSION")]
        public class InsuredExclusion
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("INSURED_ID")]
            public Int64 InsuredId { get; set; }
            [Required]
            [Column("EXCLUSION_ID")]
            public Int64 ExclusionId { get; set; }
            [Required]
            [Column("START_DATE")]
            [Display(Name = "START DATE", Description = "Başlangıç Tarihi")]
            public DateTime StartDate { get; set; }
            [Column("END_DATE")]
            public DateTime? EndDate { get; set; }
            [Column("REASON_ID")]
            public Int64? ReasonId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_INSURED_CLAIM_REASON")]
        public class InsuredClaimReason
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("INSURED_ID")]
            public Int64 InsuredId { get; set; }
            [Required]
            [Column("CLAIM_REASON_ID")]
            public Int64 ClaimReasonId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PROCESS_LIST")]
        public class ProcessList
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("IS_FACTOR_CALCULATED")]
            [Display(Name = "IS FACTOR CALCULATED", Description = "Faktor Hesaplama")]
            [MaxLength(3, ErrorMessage = "")]
            public string IsFactorCalculated { get; set; }
            [Required]
            [Column("NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Required]
            [Column("PRICE_TYPE")]
            [Display(Name = "PRICE TYPE", Description = "Para Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string PriceType { get; set; }
            [Required]
            [Column("TYPE")]
            [Display(Name = "TYPE", Description = "Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PROCESS")]
        public class Process
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("PROCESS_LIST_ID")]
            public Int64 ProcessListId { get; set; }
            [Column("PARENT_ID")]
            public Int64? ParentId { get; set; }
            [Required]
            [Column("CODE")]
            [Display(Name = "CODE", Description = "Kod")]
            [MaxLength(20, ErrorMessage = "")]
            public string Code { get; set; }
            [Required]
            [Column("NAME")]
            [Display(Name = "NAME", Description = "Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("AMOUNT")]
            [Display(Name = "AMOUNT", Description = "Miktar")]
            public Decimal Amount { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

            public List<Process> ChildProcesses = new List<Process>();

        }
        [Table("T_QUERY")]
        public class Query
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("NAME")]
            [Display(Name = "NAME", Description = "Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_QUESTION")]
        public class Question
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("IS_OPTIONAL")]
            [Display(Name = "IS OPTIONAL", Description = "İsteğe Bağlı")]
            [MaxLength(3, ErrorMessage = "")]
            public string IsOptional { get; set; }
            [Required]
            [Column("DESCRIPTION")]
            [Display(Name = "DESCRIPTION", Description = "Açıklama")]
            [MaxLength(1000, ErrorMessage = "")]
            public string Description { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_QUERY_QUESTION")]
        public class QueryQuestion
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("QUERY_ID")]
            public Int64 QueryId { get; set; }
            [Column("QUESTION_ID")]
            public Int64 QuestionId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_CHOICE")]
        public class Choice
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("QUESTION_ID")]
            public Int64 QuestionId { get; set; }
            [Required]
            [Column("DESCRIPTION")]
            [Display(Name = "DESCRIPTION", Description = "Açıklama")]
            [MaxLength(1000, ErrorMessage = "")]
            public string Description { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_ANSWER")]
        public class Answer
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("CHOICE_ID")]
            public Int64 ChoiceId { get; set; }
            [Required]
            [Column("DESCRIPTION")]
            [Display(Name = "DESCRIPTION", Description = "Açıklama")]
            [MaxLength(1000, ErrorMessage = "")]
            public string Description { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_DECLARATION")]
        public class Declaration
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("INSURED_ID")]
            public Int64 InsuredId { get; set; }
            [Column("QUESTION_ID")]
            public Int64 QuestionId { get; set; }
            [Column("ANSWER_ID")]
            public Int64 AnswerId { get; set; }
            [Required]
            [Column("DESCRIPTION")]
            [Display(Name = "DESCRIPTION", Description = "Açıklama")]
            [MaxLength(1000, ErrorMessage = "")]
            public string Description { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_NETWORK")]
        public class Network
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("TYPE")]
            [Display(Name = "TYPE", Description = "Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Required]
            [Column("CATEGORY")]
            [Display(Name = "CATEGORY", Description = "Kategori")]
            [MaxLength(3, ErrorMessage = "")]
            public string Category { get; set; }
            [Required]
            [Column("NAME")]
            [Display(Name = "NAME", Description = "Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_BRANCH")]
        public class Branch
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("NAME")]
            [Display(Name = "NAME", Description = "Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("PARENT_ID")]
            public Int64 ParentId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PRODUCT")]
        public class Product
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("CODE")]
            [Display(Name = "CODE", Description = "Kod")]
            [MaxLength(20, ErrorMessage = "")]
            public string Code { get; set; }
            [Required]
            [Column("BRANCH_ID")]
            public Int64 BranchId { get; set; }
            [Required]
            [Column("TYPE")]
            [Display(Name = "TYPE", Description = "Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Required]
            [Column("COMPANY_ID")]
            public Int64 CompanyId { get; set; }
            [Required]
            [Column("NAME")]
            [Display(Name = "NAME", Description = "Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("DESCRIPTION")]
            [MaxLength(1000, ErrorMessage = "")]
            public string Description { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_SUBPRODUCT")]
        public class Subproduct
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("CODE")]
            [Display(Name = "CODE", Description = "Kod")]
            [MaxLength(20, ErrorMessage = "")]
            public string Code { get; set; }
            [Required]
            [Column("TYPE")]
            [Display(Name = "TYPE", Description = "Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Required]
            [Column("PRODUCT_ID")]
            public Int64 ProductId { get; set; }
            [Required]
            [Column("NAME")]
            [Display(Name = "NAME", Description = "Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("DESCRIPTION")]
            [MaxLength(1000, ErrorMessage = "")]
            public string Description { get; set; }
            [Column("IS_SBM_TRANSFER")]
            [Display(Name = "IS_SBM_TRANSFER", Description = "Sbm Transfer")]
            [MaxLength(3, ErrorMessage = "")]
            public string IsSbmTransfer { get; set; }
            [Column("AGREED_AMOUNT")]
            [Display(Name = "AGREED_AMOUNT", Description = "Anlaşma Tutarı")]
            public Decimal? AgreedAmount { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PLAN")]
        public class Plan
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("SUBPRODUCT_ID")]
            public Int64 SubproductId { get; set; }
            [Required]
            [Column("NAME")]
            [Display(Name = "NAME", Description = "Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_COVERAGE")]
        public class Coverage
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("BRANCH_ID")]
            public Int64 BranchId { get; set; }
            [Required]
            [Column("TYPE")]
            [Display(Name = "TYPE", Description = "Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Required]
            [Column("LOCATION_TYPE")]
            [Display(Name = "LOCATION TYPE", Description = "Lokasyon Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string LocationType { get; set; }
            [Required]
            [Column("IS_TAX")]
            [Display(Name = "IS TAX", Description = "Vergi")]
            [MaxLength(3, ErrorMessage = "")]
            public string IsTax { get; set; }
            [Required]
            [Column("NAME")]
            [Display(Name = "NAME", Description = "Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PLAN_COVERAGE")]
        public class PlanCoverage
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("PARENT_ID")]
            public Int64? ParentId { get; set; }
            [Required]
            [Column("PLAN_ID")]
            public Int64 PlanId { get; set; }
            [Required]
            [Column("COVERAGE_ID")]
            public Int64 CoverageId { get; set; }
            [Column("COVERAGE_NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string CoverageName { get; set; }
            [Column("LOCATION_TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string LocationType { get; set; }
            [Required]
            [Column("NETWORK_ID")]
            public Int64 NetworkId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
            [Column("PACKAGE_ID")]
            public Int64? PackageId { get; set; }

            [Column("IS_OPEN_TO_PRINT")]
            public int? IS_OPEN_TO_PRINT { get; set; }

            [Column("IS_OPEN_TO_PROVIDER")]
            public int? IS_OPEN_TO_PROVIDER { get; set; }
        }
        [Table("T_PLAN_COVERAGE_VARIATION")]
        public class PlanCoverageVariation
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("TYPE")]
            [Display(Name = "TYPE", Description = "Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Required]
            [Column("PLAN_COVERAGE_ID")]
            public Int64 PlanCoverageId { get; set; }
            [Required]
            [Column("AGREEMENT_TYPE")]
            [Display(Name = "AGREEMENT TYPE", Description = "Anlaşma Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string AgreementType { get; set; }
            [Column("PRICE_LIMIT")]
            public Decimal? PriceLimit { get; set; }
            [Column("PRICE_FACTOR")]
            public Decimal? PriceFactor { get; set; }
            [Column("CURRENCY_TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string CurrencyType { get; set; }
            [Column("NUMBER_LIMIT")]
            public int? NumberLimit { get; set; }
            [Column("DAY_LIMIT")]
            public int? DayLimit { get; set; }
            [Column("COINSURANCE_RATIO")]
            public int? CoinsuranceRatio { get; set; }
            [Column("EXEMPTION_TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string ExemptionType { get; set; }
            [Column("EXEMPTION_LIMIT")]
            public Decimal? ExemptionLimit { get; set; }
            [Column("SESSION_COUNT")]
            public int? SessionCount { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_CONTACT_PARAMETER")]
        public class ContactParameter
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("CONTACT_ID")]
            public Int64? ContactId { get; set; }
            [Column("PARAMETER_ID")]
            public Int64? ParameterId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("SP_RESPONSE_ID")]
            public Int64? SpResponseId { get; set; }
            [Column("STATUS")]
            public string Status { get; set; }
        }
        [Table("V_CONTACT_PARAMETER")]
        public class V_ContactParameter
        {
            [Column("CONTACT_ID")] public long? CONTACT_ID { get; set; }
            [Key] [Column("CONTACT_PARAMETER_ID")] public long? CONTACT_PARAMETER_ID { get; set; }
            [Column("ENVIRONMENT")] public string ENVIRONMENT { get; set; }
            [Column("KEY")] public string KEY { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PARAMETER_ID")] public long? PARAMETER_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SYSTEM_TYPE")] public string SYSTEM_TYPE { get; set; }
            [Column("VALUE")] public string VALUE { get; set; }

        }
        [Table("T_PLAN_COVERAGE_PARAMETER")]
        public class PlanCoverageParameter
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("PARAMETER_ID")]
            public Int64 ParameterId { get; set; }
            [Column("PLAN_COVERAGE_ID")]
            public Int64 PlanCoverageId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("T_PROVIDER_GROUP")]
        public class ProviderGroup
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("NAME")]
            [Display(Name = "NAME", Description = "Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("TAX_NUMBER")]
            [MaxLength(100, ErrorMessage = "")]
            public Int64? TaxNumber { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PROVIDER")]
        public class Provider : ISpExecute3
        {
            [Required]
            [Column("ID", TypeName = "BASE")]
            public Int64 Id { get; set; }
            [Column("OFFICIAL_CODE")]
            [MaxLength(20, ErrorMessage = "")]
            public string OfficialCode { get; set; }
            [Required]
            [Column("TYPE")]
            [Display(Name = "TYPE", Description = "Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("PROVIDER_GROUP_ID")]
            public Int64? ProviderGroupId { get; set; }
            [Column("CONTACT_ID")]
            [FKType("T_CONTACT", "1toN")]
            public Int64? ContactId { get; set; }
            [Column("STOPPAGE_OVERWRITE")]
            public int? StoppageOverwrite { get; set; }
            [Column("ADDRESS_ID")]
            public Int64? AddressId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

            [Column("IS_VAT_EXEMPTION")]
            [Display(Name = "IS_VAT_EXEMPTION", Description = "IS_VAT_EXEMPTION")]
            [MaxLength(3, ErrorMessage = "")]
            public string IS_VAT_EXEMPTION { get; set; }

            [FKType("T_STAFF", "Nto1")]
            public List<Staff> StaffList { get; set; }

            [FKType("T_PROVIDER_BANK_ACCOUNT", "NtoN")]
            public List<ProviderBankAccount> ProviderBankAccountList { get; set; }
            [Column("REVISION_DATE")]
            public DateTime RevisionDate { get; set; }
            [Column("IS_E_BILL")]
            public string IsEBill { get; set; }

        }

        [Table("V_INSURED_MAIL")]
        public class V_InsuredEmail
        {
            [Column("CONTACT_ID")] public long? CONTACT_ID { get; set; }
            [Column("EMAIL_DETAILS")] public string EMAIL_DETAILS { get; set; }
            [Column("EMAIL_ID")] public long? EMAIL_ID { get; set; }
            [Column("EMAIL_TYPE")] public string EMAIL_TYPE { get; set; }
            [Key][Column("INSURED_EMAIL_ID")] public long? INSURED_EMAIL_ID { get; set; }
            [Column("INSURED_ID")] public long? INSURED_ID { get; set; }
            [Column("IS_PRIMARY")] public string IS_PRIMARY { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_INSURED_PHONE")]
        public class V_InsuredPhone
        {
            [Column("CONTACT_ID")] public long? CONTACT_ID { get; set; }
            [Column("EXTENSION")] public string EXTENSION { get; set; }
            [Column("INSURED_ID")] public long? INSURED_ID { get; set; }
            [Key][Column("INSURED_PHONE_ID")] public long? INSURED_PHONE_ID { get; set; }
            [Column("IS_PRIMARY")] public string IS_PRIMARY { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PHONE_ID")] public long? PHONE_ID { get; set; }
            [Column("PHONE_NO")] public string PHONE_NO { get; set; }
            [Column("PHONE_TYPE")] public string PHONE_TYPE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_INSURED_ADDRESS")]
        public class V_InsuredAddress
        {
            [Column("ADDRESS_ID")] public long? ADDRESS_ID { get; set; }
            [Column("ADDRESS_TYPE")] public string ADDRESS_TYPE { get; set; }
            [Column("CITY_ID")] public long? CITY_ID { get; set; }
            [Column("CITY_NAME")] public string CITY_NAME { get; set; }
            [Column("CONTACT_ID")] public long? CONTACT_ID { get; set; }
            [Column("COUNTRY_ID")] public long? COUNTRY_ID { get; set; }
            [Column("COUNTRY_NAME")] public string COUNTRY_NAME { get; set; }
            [Column("COUNTY_ID")] public long? COUNTY_ID { get; set; }
            [Column("COUNTY_NAME")] public string COUNTY_NAME { get; set; }
            [Column("DETAILS")] public string DETAILS { get; set; }
            [Key][Column("INSURED_ADDRESS_ID")] public long? INSURED_ADDRESS_ID { get; set; }
            [Column("INSURED_ID")] public long? INSURED_ID { get; set; }
            [Column("IS_PRIMARY")] public string IS_PRIMARY { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("ZIP_CODE")] public string ZIP_CODE { get; set; }

        }
        [Table("V_INSURED_BANK_ACCOUNT")]
        public class V_InsuredBankAccount
        {
            [Key] [Column("BANK_ACCOUNT_ID")] public long? BANK_ACCOUNT_ID { get; set; }
            [Column("BANK_ACCOUNT_NAME")] public string BANK_ACCOUNT_NAME { get; set; }
            [Column("BANK_ACCOUNT_NO")] public string BANK_ACCOUNT_NO { get; set; }
            [Column("BANK_BRANCH_ID")] public long? BANK_BRANCH_ID { get; set; }
            [Column("BANK_BRANCH_NAME")] public string BANK_BRANCH_NAME { get; set; }
            [Column("BANK_ID")] public long? BANK_ID { get; set; }
            [Column("BANK_NAME")] public string BANK_NAME { get; set; }
            [Column("CONTACT_ID")] public long? CONTACT_ID { get; set; }
            [Column("CURRENCY_TYPE")] public string CURRENCY_TYPE { get; set; }
            [Column("IBAN")] public string IBAN { get; set; }
            [Column("INSURED_BANK_ACCOUNT_ID")] public long? INSURED_BANK_ACCOUNT_ID { get; set; }
            [Column("INSURED_ID")] public long? INSURED_ID { get; set; }
            [Column("IS_PRIMARY")] public string IS_PRIMARY { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SWIFT_CODE")] public string SWIFT_CODE { get; set; }


            public string isClaimPayment = "0";

        }
        [Table("V_PROVIDER_PARAMETER")]
        public class V_ProviderParameter
        {
            [Column("ENVIRONMENT")] public string ENVIRONMENT { get; set; }
            [Column("KEY")] public string KEY { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PARAMETER_ID")] public long? PARAMETER_ID { get; set; }
            [Column("PROVIDER_ID")] public long? PROVIDER_ID { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Key] [Column("PROVIDER_PARAMETER_ID")] public long? PROVIDER_PARAMETER_ID { get; set; }
            [Column("PROVIDER_TITLE")] public string PROVIDER_TITLE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SYSTEM_TYPE")] public string SYSTEM_TYPE { get; set; }
            [Column("VALUE")] public string VALUE { get; set; }

        }
        [Table("V_PROVIDER")]
        public class V_Provider
        {
            [Column("ADDRESS")] public string ADDRESS { get; set; }
            [Column("ADDRESS_ID")] public long? ADDRESS_ID { get; set; }
            [Column("CITY_CODE")] public string CITY_CODE { get; set; }
            [Column("CITY_ID")] public long? CITY_ID { get; set; }
            [Column("CITY_NAME")] public string CITY_NAME { get; set; }
            [Column("CONTACT_ID")] public long CONTACT_ID { get; set; }
            [Column("CORPORATE_ID")] public long? CORPORATE_ID { get; set; }
            [Column("CORPORATE_TYPE")] public string CORPORATE_TYPE { get; set; }
            [Column("COUNTRY_CODE")] public string COUNTRY_CODE { get; set; }
            [Column("COUNTRY_ID")] public long? COUNTRY_ID { get; set; }
            [Column("COUNTRY_NAME")] public string COUNTRY_NAME { get; set; }
            [Column("COUNTY_CODE")] public long? COUNTY_CODE { get; set; }
            [Column("COUNTY_ID")] public long? COUNTY_ID { get; set; }
            [Column("COUNTY_NAME")] public string COUNTY_NAME { get; set; }
            [Column("DISTRICT")] public string DISTRICT { get; set; }
            [Column("DOGUM_CONTRACT_ID")] public long? DOGUM_CONTRACT_ID { get; set; }
            [Column("IDENTITY_NO")] public Int64? IDENTITY_NO { get; set; }
            [Column("IS_E_BILL")] public string IS_E_BILL { get; set; }
            [Column("IS_VAT_EXEMPTION")] public string IS_VAT_EXEMPTION { get; set; }
            [Column("MDP_CONTRACT_ID")] public long? MDP_CONTRACT_ID { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("OFFICIAL_CODE")] public string OFFICIAL_CODE { get; set; }
            [Column("OSS_CONTRACT_ID")] public long? OSS_CONTRACT_ID { get; set; }
            [Column("PHONE_ID")] public long? PHONE_ID { get; set; }
            [Column("PHONE_NO")] public string PHONE_NO { get; set; }
            [Column("PROVIDER_GROUP_ID")] public long? PROVIDER_GROUP_ID { get; set; }
            [Column("PROVIDER_GROUP_NAME")] public string PROVIDER_GROUP_NAME { get; set; }
            [Key] [Column("PROVIDER_ID")] public long PROVIDER_ID { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Column("PROVIDER_PHONE_ID")] public long? PROVIDER_PHONE_ID { get; set; }
            [Column("PROVIDER_TITLE")] public string PROVIDER_TITLE { get; set; }
            [Column("PROVIDER_TYPE")] public string PROVIDER_TYPE { get; set; }
            [Column("REVISION_DATE")] public DateTime? REVISION_DATE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("STOPPAGE_OVERWRITE")] public long? STOPPAGE_OVERWRITE { get; set; }
            [Column("TAX_NUMBER")] public Int64? TAX_NUMBER { get; set; }
            [Column("TAX_OFFICE")] public string TAX_OFFICE { get; set; }
            [Column("TSS_CONTRACT_ID")] public long? TSS_CONTRACT_ID { get; set; }
            [Column("ZIP_CODE")] public string ZIP_CODE { get; set; }

        }
        [Table("V_CONTACT_BANK")]
        public class V_ContactBank
        {
            [Column("BANK_ACCOUNT_ID")] public long? BANK_ACCOUNT_ID { get; set; }
            [Column("BANK_ACCOUNT_NAME")] public string BANK_ACCOUNT_NAME { get; set; }
            [Column("BANK_ACCOUNT_NO")] public string BANK_ACCOUNT_NO { get; set; }
            [Column("BANK_BRANCH_CODE")] public string BANK_BRANCH_CODE { get; set; }
            [Column("BANK_BRANCH_ID")] public long? BANK_BRANCH_ID { get; set; }
            [Column("BANK_BRANCH_NAME")] public string BANK_BRANCH_NAME { get; set; }
            [Column("BANK_ID")] public long? BANK_ID { get; set; }
            [Column("BANK_NAME")] public string BANK_NAME { get; set; }
            [Key][Column("CONTACT_BANK_ACCOUNT_ID")] public long? CONTACT_BANK_ACCOUNT_ID { get; set; }
            [Column("CONTACT_ID")] public long? CONTACT_ID { get; set; }
            [Column("CURRENCY_TYPE")] public string CURRENCY_TYPE { get; set; }
            [Column("IBAN")] public string IBAN { get; set; }
            [Column("IS_PRIMARY")] public string IS_PRIMARY { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SWIFT_CODE")] public string SWIFT_CODE { get; set; }

        }
        [Table("V_PROVIDER_BANK")]
        public class V_ProviderBank
        {
            [Column("BANK_ACCOUNT_ID")] public long? BANK_ACCOUNT_ID { get; set; }
            [Column("BANK_ACCOUNT_NAME")] public string BANK_ACCOUNT_NAME { get; set; }
            [Column("BANK_ACCOUNT_NO")] public string BANK_ACCOUNT_NO { get; set; }
            [Column("BANK_BRANCH_CODE")] public string BANK_BRANCH_CODE { get; set; }
            [Column("BANK_BRANCH_ID")] public long? BANK_BRANCH_ID { get; set; }
            [Column("BANK_BRANCH_NAME")] public string BANK_BRANCH_NAME { get; set; }
            [Column("BANK_ID")] public long? BANK_ID { get; set; }
            [Column("BANK_NAME")] public string BANK_NAME { get; set; }
            [Column("CURRENCY_TYPE")] public string CURRENCY_TYPE { get; set; }
            [Column("IBAN")] public string IBAN { get; set; }
            [Column("IS_PRIMARY")] public string IS_PRIMARY { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Key][Column("PROVIDER_BANK_ACCOUNT_ID")] public long? PROVIDER_BANK_ACCOUNT_ID { get; set; }
            [Column("PROVIDER_ID")] public long? PROVIDER_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SWIFT_CODE")] public string SWIFT_CODE { get; set; }

        }

        [Table("V_PROVIDER_BANK_ACCOUNT")]
        public class V_ProviderBankAccount
        {
            [Column("BANK_ACCOUNT_ID")] public long? BANK_ACCOUNT_ID { get; set; }
            [Column("BANK_ACCOUNT_NAME")] public string BANK_ACCOUNT_NAME { get; set; }
            [Column("BANK_ACCOUNT_NO")] public string BANK_ACCOUNT_NO { get; set; }
            [Column("BANK_BRANCH_ID")] public long? BANK_BRANCH_ID { get; set; }
            [Column("BANK_BRANCH_NAME")] public string BANK_BRANCH_NAME { get; set; }
            [Column("BANK_ID")] public long? BANK_ID { get; set; }
            [Column("BANK_NAME")] public string BANK_NAME { get; set; }
            [Column("IBAN")] public string IBAN { get; set; }
            [Column("IS_PRIMARY")] public string IS_PRIMARY { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Key] [Column("PROVIDER_BANK_ACCOUNT_ID")] public long? PROVIDER_BANK_ACCOUNT_ID { get; set; }
            [Column("PROVIDER_ID")] public long? PROVIDER_ID { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Column("PROVIDER_TITLE")] public string PROVIDER_TITLE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SWIFT_CODE")] public string SWIFT_CODE { get; set; }
        }

        [Table("T_PROVIDER_BANK_ACCOUNT")]
        public class ProviderBankAccount
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("PROVIDER_ID")]
            public Int64 ProviderId { get; set; }
            [Required]
            [Column("BANK_ACCOUNT_ID")]
            //[FKType("T_BANK_ACCOUNT", "1toN")]
            public Int64 BankAccountId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("T_CONTACT_BANK_ACCOUNT")]
        public class ContactBankAccount :ISpExecute3
        {
            [Required]
            [Column("ID", TypeName = "BASE")]
            public Int64 Id { get; set; }
            [Required]
            [Column("CONTACT_ID", TypeName = "BASE")]
            public Int64 ContactId { get; set; }
            [Required]
            [Column("BANK_ACCOUNT_ID", TypeName = "BASE")]
            //[FKType("T_BANK_ACCOUNT", "1toN")]
            public Int64 BankAccountId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_CITY_FACTOR_GROUP")]
        public class CityFactorGroup
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("PROCESS_LIST_ID")]
            public Int64 ProcessListId { get; set; }
            [Required]
            [Column("START_DATE")]
            [Display(Name = "START DATE", Description = "Başlangıç Tarihi")]
            public DateTime StartDate { get; set; }
            [Column("END_DATE")]
            public DateTime? EndDate { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_CITY_FACTOR")]
        public class CityFactor
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("CITY_FACTOR_GROUP_ID")]
            public Int64 CityFactorGroupId { get; set; }
            [Column("CITY_ID")]
            public Int64 CityId { get; set; }
            [Column("FACTOR")]
            [Display(Name = "FACTOR", Description = "Faktör")]
            public Decimal Factor { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PROCESS_COVERAGE")]
        public class ProcessCoverage
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("PROCESS_ID")]
            public Int64 ProcessId { get; set; }
            [Required]
            [Column("COVERAGE_ID")]
            public Int64 CoverageId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_DOCTOR_BRANCH")]
        public class DoctorBranch
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("NAME")]
            [Display(Name = "NAME", Description = "Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_STAFF")]
        public class Staff
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("PROVIDER_ID")]
            public Int64 ProviderId { get; set; }
            [Required]
            [Column("TYPE")]
            [Display(Name = "TYPE", Description = "Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Required]
            [Column("CONTACT_ID")]
            public Int64 ContactId { get; set; }
            [Column("DOCTOR_BRANCH_ID")]
            public Int64? DoctorBranchId { get; set; }
            [Column("DIPLOMA_NO")]
            [MaxLength(20, ErrorMessage = "")]
            public string DiplomaNo { get; set; }
            [Column("REGISTER_NO")]
            [MaxLength(20, ErrorMessage = "")]
            public string RegisterNo { get; set; }
            [Column("PHONE_ID")]
            public Int64? PhoneId { get; set; }
            [Column("MOBILE_PHONE_ID")]
            public Int64? MobilePhoneId { get; set; }
            [Column("EMAIL_ID")]
            public Int64? EmailId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
            [Column("IS_GROUP_ADMIN")]
            [MaxLength(1, ErrorMessage = "")]
            public string IsGroupAdmin { get; set; }
            [Column("CONTRACT_TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string ContractType { get; set; }

        }
        [Table("T_CONTRACT")]
        public class Contract
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("PROVIDER_ID")]
            public Int64 ProviderId { get; set; }
            [Required]
            [Column("NO")]
            [MaxLength(20, ErrorMessage = "")]
            [Display(Name = "NO", Description = "No")]
            public string No { get; set; }
            [Required]
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            [Display(Name = "TYPE", Description = "Tipi")]
            public string Type { get; set; }
            [Required]
            [Column("START_DATE")]
            [Display(Name = "START DATE", Description = "Başlangıç Tarihi")]
            public DateTime StartDate { get; set; }
            [Required]
            [Column("END_DATE")]
            [Display(Name = "END DATE", Description = "Bitiş Tarihi")]
            public DateTime? EndDate { get; set; }
            [Column("DISCOUNT_RATIO")]
            public decimal? DiscountRatio { get; set; }
            [Required]
            [Column("PAYMENT_DAY")]
            [Display(Name = "PAYMENT DAY", Description = "Ödeme Günü")]
            public int PaymentDay { get; set; }
            [Column("REASON_ID")]
            public Int64? ReasonId { get; set; }
            [Column("DESCRIPTION")]
            [MaxLength(3, ErrorMessage = "")]
            public string Description { get; set; }
            [Column("RESPONSE_DATE")]
            public DateTime? ResponseDate { get; set; }
            [Column("REVISION_DATE")]
            public DateTime? RevisionDate { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_CONTRACT_REASON")]
        public class ContractReason
        {
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("CONTRACT_ID")]
            public Int64 ContractId { get; set; }
            [Column("REASON_ID")]
            public Int64 ReasonId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_CONTRACT_DOCTOR_BRANCH")]
        public class ContractDoctorBranch
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("CONTRACT_ID")]
            public Int64 ContractId { get; set; }
            [Required]
            [Column("DOCTOR_BRANCH_ID")]
            public Int64 DoctorBranchId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_CONTRACT_NETWORK")]
        public class ContractNetwork
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("CONTRACT_ID")]
            public Int64 ContractId { get; set; }
            [Required]
            [Column("NETWORK_ID")]
            public Int64 NetworkId { get; set; }
            [Required]
            [Column("COMPANY_ID")]
            public Int64 CompanyId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PROCESS_GROUP")]
        public class ProcessGroup
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("PROCESS_LIST_ID")]
            public Int64 ProcessListId { get; set; }
            [Required]
            [Column("NAME")]
            [Display(Name = "NAME", Description = "Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PROCESS_GROUP_PROCESS")]
        public class ProcessGroupProcess
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("PROCESS_GROUP_ID")]
            public Int64 ProcessGroupId { get; set; }
            [Required]
            [Column("PROCESS_ID")]
            public Int64 ProcessId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_CONTRACT_PROCESS_GROUP")]
        public class ContractProcessGroup
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("CONTRACT_ID")]
            public Int64 ContractId { get; set; }
            [Required]
            [Column("PROCESS_GROUP_ID")]
            public Int64 ProcessGroupId { get; set; }
            [Required]
            [Column("PRICING_TYPE")]
            [Display(Name = "PRICING TYPE", Description = "Fiyatlandırma Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string PricingType { get; set; }
            [Required]
            [Column("AMOUNT")]
            [Display(Name = "AMOUNT", Description = "Miktar")]
            public Decimal Amount { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PAYROLL")]
        public class Payroll
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("COMPANY_ID")]
            public Int64? CompanyId { get; set; }
            [Column("OLD_PAYROLL_ID")]
            public Int64? OldPayrollId { get; set; }
            [Column("PAYROLL_DATE")]
            public DateTime? PayrollDate { get; set; }
            [Column("DUE_DATE")]
            public DateTime? Due_Date { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("CATEGORY")]
            [MaxLength(3, ErrorMessage = "")]
            public string Category { get; set; }
            [Column("PROVIDER_ID")]
            public Int64? ProviderId { get; set; }
            [Column("EXT_PROVIDER_NO")]
            [MaxLength(20, ErrorMessage = "")]
            public string ExtProviderNo { get; set; }
            [Column("EXT_PROVIDER_DATE")]
            public DateTime? ExtProviderDate { get; set; }
            [Column("PAYMENT_DATE")]
            public DateTime? PaymentDate { get; set; }
            [Column("VERIFICATION_DATE")]
            public DateTime? VerificationDate { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
            [Column("COMPANY_PAYROLL_ID")]
            public string CompanyPayrollId { get; set; }
            [Column("COMPANY_PAYROLL_NO")]
            public string CompanyPayrollNo { get; set; }

        }

        [Table("T_CLAIM_MISSING_DOC")]
        public class ClaimMissingDoc
        {
            [Required] [Column("CLAIM_ID")] public Int64 CLAIM_ID { get; set; }
            [Required] [Column("ID")] public Int64 Id { get; set; }
            [Column("INCOMING_DATE")] public DateTime? INCOMING_DATE { get; set; }
            [Column("RETURN_DATE")] public DateTime? RETURN_DATE { get; set; }
            [Required] [Column("MISSING_DATE")] public DateTime MISSING_DATE { get; set; }
            [Required] [Column("STATUS")] public string STATUS { get; set; }
            [Required] [Column("TYPE")] public string TYPE { get; set; }
        }

        [Table("T_CLAIM")]
        public class Claim
        {
            [Required]
            [Column("ID", TypeName = "BASE")]
            public Int64 Id { get; set; }
            [Required]
            [Column("INSURED_ID")]
            public Int64? InsuredId { get; set; }
            [Required]
            [Column("PROVIDER_ID")]
            public Int64? ProviderId { get; set; }
            [Required]
            [Column("NOTICE_DATE")]
            [Display(Name = "NOTICE DATE", Description = "İhbar  Tarihi")]
            public DateTime? NoticeDate { get; set; }
            [Column("CLAIM_DATE")]
            public DateTime? ClaimDate { get; set; }
            [Required]
            [Column("TYPE")]
            [Display(Name = "TYPE", Description = "Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("VARIATION")]
            [MaxLength(3, ErrorMessage = "")]
            public string Variation { get; set; }
            [Column("EVENT_DATE")]
            public DateTime? EventDate { get; set; }
            [Column("EVENT_TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string EventType { get; set; }
            [Column("EVENT_DESCRIPTION")]
            [MaxLength(2000, ErrorMessage = "")]
            public string EventDescription { get; set; }
            [Column("REASON_ID")]
            public Int64? ReasonId { get; set; }
            [Column("STAFF_ID")]
            public Int64? StaffId { get; set; }
            [Column("COMPANY_CLAIM_ID")]
            public Int64? CompanyClaimId { get; set; }
            [Column("COMPANY_CLAIM_NO")]
            [MaxLength(20, ErrorMessage = "")]
            public string CompanyClaimNo { get; set; }
            [Column("MEDULA_NO")]
            [MaxLength(20, ErrorMessage = "")]
            public string MedulaNo { get; set; }
            [Column("PAYROLL_ID")]
            public Int64? PayrollId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
            [Required]
            [Column("PAYMENT_METHOD")]
            [Display(Name = "PAYMENT METHOD", Description = "Ödeme Şekli")]
            [MaxLength(3, ErrorMessage = "")]
            public string PaymentMethod { get; set; }
            [Column("SOURCE_TYPE")]
            public string SourceType { get; set; }
            [Column("PACKAGE_ID")]
            public Int64? PackageId { get; set; }
            [Column("COMPANY_ID")]
            public Int64? CompanyId { get; set; }
            [Column("PRODUCT_ID")]
            public Int64? ProductId { get; set; }
            [Column("SUBPRODUCT_ID")]
            public Int64? SubproductId { get; set; }
            [Column("POLICY_ID")]
            public Int64? PolicyId { get; set; }
            [Column("POLICY_GROUP_ID")]
            public Int64? PolicyGroupId { get; set; }
            [Column("PROVIDER_GROUP_ID")]
            public Int64? ProviderGroupId { get; set; }
        }
        //[Table("T_CLAIM_COVERAGE")]
        //public class ClaimCoverage
        //{
        //    [Required]
        //    [Column("ID")]
        //    public Int64 Id { get; set; }
        //    [Required]
        //    [Column("CLAIM_ID")]
        //    public Int64 ClaimId { get; set; }
        //    [Required]
        //    [Column("COVERAGE_ID")]
        //    public Int64 CoverageId { get; set; }
        //    [Column("SESSION_COUNT")]
        //    public int SessionCount { get; set; }
        //    [Column("COUNT")]
        //    public int Count { get; set; }
        //    [Column("DAY")]
        //    public int Day { get; set; }
        //    [Column("PROCESS_LIMIT")]
        //    public Decimal ProcessLimit { get; set; }
        //    [Column("REQUESTED")]
        //    public Decimal Requested { get; set; }
        //    [Column("ACCEPTED")]
        //    public Decimal Accepted { get; set; }
        //    [Column("CONFIRMED")]
        //    public Decimal Confirmed { get; set; }
        //    [Column("PAID")]
        //    public Decimal Paid { get; set; }
        //    [Column("EXGRACIA")]
        //    public Decimal Exgracia { get; set; }
        //    [Column("SGK_AMOUNT")]
        //    public Decimal SgkAmount { get; set; }
        //    [Column("COINSURANCE")]
        //    public int Coinsurance { get; set; }
        //    [Column("REASON_ID")]
        //    public Int64 ReasonId { get; set; }
        //    [Column("RESULT")]
        //    [MaxLength(2000, ErrorMessage = "")]
        //    public string Result { get; set; }
        //    [Column("NEW_VERSION_ID")]
        //    public Int64 NewVersionId { get; set; }
        //    [Column("STATUS")]
        //    [Display(Name = "STATUS", Description = "Statüs")]
        //    [MaxLength(3, ErrorMessage = "")]
        //    public string Status { get; set; }

        //}
        [Table("V_CLAIM_PAYMENT")]
        public class V_ClaimPayment
        {
            [Column("AMOUNT")] public decimal? AMOUNT { get; set; }
            [Column("BANK_ACCOUNT_ID")] public long? BANK_ACCOUNT_ID { get; set; }
            [Column("BANK_ACCOUNT_NAME")] public string BANK_ACCOUNT_NAME { get; set; }
            [Column("BANK_ACCOUNT_NO")] public string BANK_ACCOUNT_NO { get; set; }
            [Column("BANK_BRANCH_ID")] public long? BANK_BRANCH_ID { get; set; }
            [Column("BANK_BRANCH_NAME")] public string BANK_BRANCH_NAME { get; set; }
            [Column("BANK_ID")] public long? BANK_ID { get; set; }
            [Column("BANK_NAME")] public string BANK_NAME { get; set; }
            [Column("CLAIM_ID")] public long? CLAIM_ID { get; set; }
            [Key][Column("CLAIM_PAYMENT_ID")] public long? CLAIM_PAYMENT_ID { get; set; }
            [Column("CURRENCY_TYPE")] public string CURRENCY_TYPE { get; set; }
            [Column("DUE_DATE")] public DateTime? DUE_DATE { get; set; }
            [Column("IBAN")] public string IBAN { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PAYMENT_DATE")] public DateTime? PAYMENT_DATE { get; set; }
            [Column("PAYMENT_TYPE")] public string PAYMENT_TYPE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SWIFT_CODE")] public string SWIFT_CODE { get; set; }

        }
        [Table("V_CLAIM_ICD")]
        public class V_ClaimIcd
        {
            [Key] [Column("CLAIM_ICD_ID")] public long? CLAIM_ICD_ID { get; set; }
            [Column("CLAIM_ID")] public long? CLAIM_ID { get; set; }
            [Column("ICD_CODE")] public string ICD_CODE { get; set; }
            [Column("ICD_ID")] public long? ICD_ID { get; set; }
            [Column("ICD_NAME")] public string ICD_NAME { get; set; }
            [Column("ICD_TYPE")] public string ICD_TYPE { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
        }

        [Table("T_CLAIM_ICD")]
        public class ClaimIcd
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("CLAIM_ID")]
            public Int64? ClaimId { get; set; }
            [Required]
            [Column("ICD_ID")]
            public Int64? IcdId { get; set; }
            [Column("ICD_TYPE")]
            [Display(Name = "ICD TYPE", Description = "Icd Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string IcdType { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("T_CLAIM_PROCESS_ICD")]
        public class ClaimProcessIcd
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("CLAIM_PROCESS_ID")]
            public Int64? ClaimProcessId { get; set; }
            [Required]
            [Column("ICD_ID")]
            public Int64? IcdId { get; set; }
            [Column("ICD_TYPE")]
            [Display(Name = "ICD TYPE", Description = "Icd Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string IcdType { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("V_CLAIM_PROCESS_LIST")]
        public class V_ClaimProcessList
        {
            [Column("CITY_FACTOR")] public decimal? CITY_FACTOR { get; set; }
            [Column("CITY_ID")] public long? CITY_ID { get; set; }
            [Column("CLAIM_DATE")] public DateTime? CLAIM_DATE { get; set; }
            [Key] [Column("CLAIM_ID")] public long? CLAIM_ID { get; set; }
            [Column("CONTRACT_AMOUNT")] public decimal? CONTRACT_AMOUNT { get; set; }
            [Column("CONTRACT_PRICE")] public decimal? CONTRACT_PRICE { get; set; }
            [Column("INSURED_ID")] public long? INSURED_ID { get; set; }
            [Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Column("PARENT_ID")] public long? PARENT_ID { get; set; }
            [Column("PARENT_NAME")] public string PARENT_NAME { get; set; }
            [Column("PRICING_TYPE")] public string PRICING_TYPE { get; set; }
            [Column("PROCESS_AMOUNT")] public decimal? PROCESS_AMOUNT { get; set; }
            [Column("PROCESS_CODE")] public string PROCESS_CODE { get; set; }
            [Column("PROCESS_GROUP_ID")] public long? PROCESS_GROUP_ID { get; set; }
            [Column("PROCESS_GROUP_NAME")] public string PROCESS_GROUP_NAME { get; set; }
            [Column("PROCESS_ID")] public long? PROCESS_ID { get; set; }
            [Column("PROCESS_LIST_ID")] public long? PROCESS_LIST_ID { get; set; }
            [Column("PROCESS_LIST_NAME")] public string PROCESS_LIST_NAME { get; set; }
            [Column("PROCESS_LIST_TYPE")] public string PROCESS_LIST_TYPE { get; set; }
            [Column("PROCESS_NAME")] public string PROCESS_NAME { get; set; }
            [Column("PROCESS_PRICE")] public decimal? PROCESS_PRICE { get; set; }
            [Column("PROVIDER_ID")] public long? PROVIDER_ID { get; set; }
        }

        [Table("V_CLAIM_PROCESS")]
        public class V_ClaimProcess
        {
            [Column("ACCEPTED")] public decimal? ACCEPTED { get; set; }
            [Column("AGREED_AMOUNT")] public decimal? AGREED_AMOUNT { get; set; }
            [Column("CLAIM_ID")] public long? CLAIM_ID { get; set; }
            [Key] [Column("CLAIM_PROCESS_ID")] public long? CLAIM_PROCESS_ID { get; set; }
            [Column("COINSURANCE")] public long? COINSURANCE { get; set; }
            [Column("CONFIRMED")] public decimal? CONFIRMED { get; set; }
            [Column("CONTRACT_PROCESS_GROUP_ID")] public long? CONTRACT_PROCESS_GROUP_ID { get; set; }
            [Column("COUNT")] public long? COUNT { get; set; }
            [Column("COVERAGE_ID")] public long? COVERAGE_ID { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Column("COVERAGE_TYPE")] public string COVERAGE_TYPE { get; set; }
            [Column("DAY")] public long? DAY { get; set; }
            [Column("DIPLOMA_NO")] public string DIPLOMA_NO { get; set; }
            [Column("DOCTOR_BRANCH_ID")] public long? DOCTOR_BRANCH_ID { get; set; }
            [Column("DOCTOR_BRANCH_NAME")] public string DOCTOR_BRANCH_NAME { get; set; }
            [Column("EXEMPTION")] public decimal? EXEMPTION { get; set; }
            [Column("EXGRACIA")] public decimal? EXGRACIA { get; set; }
            [Column("FIRST_NAME")] public string FIRST_NAME { get; set; }
            [Column("LAST_NAME")] public string LAST_NAME { get; set; }
            [Column("MIDDLE_NAME")] public string MIDDLE_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PAID")] public decimal? PAID { get; set; }
            [Column("PARTLY_REJECT_DESC")] public string PARTLY_REJECT_DESC { get; set; }
            [Column("PROCESS_CODE")] public string PROCESS_CODE { get; set; }
            [Column("PROCESS_ID")] public long? PROCESS_ID { get; set; }
            [Column("PROCESS_LIMIT")] public decimal? PROCESS_LIMIT { get; set; }
            [Column("PROCESS_PRICE")] public decimal? PROCESS_PRICE { get; set; }
            [Column("PROCESS_LIST_ID")] public long? PROCESS_LIST_ID { get; set; }
            [Column("PROCESS_NAME")] public string PROCESS_NAME { get; set; }
            [Column("PROVIDER_ID")] public long? PROVIDER_ID { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Column("PROVIDER_TITLE")] public string PROVIDER_TITLE { get; set; }
            [Column("REASON_ID")] public long? REASON_ID { get; set; }
            [Column("REQUESTED")] public decimal? REQUESTED { get; set; }
            [Column("REQUESTED_AMOUNT")] public decimal? REQUESTED_AMOUNT { get; set; }
            [Column("RESULT")] public string RESULT { get; set; }
            [Column("SESSION_COUNT")] public long? SESSION_COUNT { get; set; }
            [Column("SGK_AMOUNT")] public decimal? SGK_AMOUNT { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STAFF_ID")] public long? STAFF_ID { get; set; }
            [Column("STAFF_IDENTITY_NO")] public string STAFF_IDENTITY_NO { get; set; }
            [Column("STAFF_TITLE")] public string STAFF_TITLE { get; set; }
            [Column("STAFF_TYPE")] public string STAFF_TYPE { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("STOPAJ")] public decimal? STOPAJ { get; set; }
            [Column("UNIT_AMOUNT")] public decimal? UNIT_AMOUNT { get; set; }
        }

        [Table("T_CLAIM_PROCESS")]
        public class ClaimProcess
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("CLAIM_ID")]
            public Int64? ClaimId { get; set; }
            [Required]
            [Column("PROCESS_ID")]
            public Int64? ProcessId { get; set; }
            [Column("COVERAGE_ID")]
            public Int64? CoverageId { get; set; }
            [Required]
            [Display(Name = "UNIT AMOUNT", Description = "Birim Miktar")]
            [Column("UNIT_AMOUNT")]
            public Decimal UnitAmount { get; set; }
            [Column("REQUESTED_AMOUNT")]
            public Decimal? RequestedAmount { get; set; }
            [Required]
            [Display(Name = "AGREED AMOUNT", Description = "Kararlaştırışan Miktar")]
            [Column("AGREED_AMOUNT")]
            public Decimal? AgreedAmount { get; set; }
            [Column("CONTRACT_PROCESS_GROUP_ID")]
            public Int64? ContractProcessGroupId { get; set; }
            [Column("STAFF_ID")]
            public Int64? StaffId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
            [Column("COUNT")]
            public Int32? Count { get; set; }
            [Column("DAY")]
            public Int32? Day { get; set; }
            [Column("SESSION_COUNT")]
            public Int32? SessionCount { get; set; }
            [Column("PROCESS_LIMIT")]
            public Decimal? ProcessLimit { get; set; }
            [Column("REQUESTED")]
            public Decimal? Requested { get; set; }
            [Column("ACCEPTED")]
            public Decimal? Accepted { get; set; }
            [Column("CONFIRMED")]
            public Decimal? Confirmed { get; set; }
            [Column("PAID")]
            public Decimal? Paid { get; set; }
            [Column("EXGRACIA")]
            public Decimal? Exgracia { get; set; }
            [Column("SGK_AMOUNT")]
            public Decimal? SgkAmount { get; set; }
            [Column("COINSURANCE")]
            public Int32? Coinsurance { get; set; }
            [Column("REASON_ID")]
            public Int64? ReasonId { get; set; }
            [Column("RESULT")]
            public string Result { get; set; }
            [Column("EXEMPTION")]
            public Decimal? Exemption { get; set; }
            [Column("STOPAJ")]
            public Decimal? Stopaj { get; set; }
            [Column("PARTLY_REJECT_DESC")]
            public string PartlyRejectDesc { get; set; }
        }
        [Table("T_CLAIM_HOSPITALIZATION")]
        public class ClaimHospitalization
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("CLAIM_ID")]
            public Int64 ClaimId { get; set; }
            [Required]
            [Column("ENTRANCE_DATE")]
            [Display(Name = "ENTRANCE DATE", Description = "Giriş Tarihi")]
            public DateTime EntranceDate { get; set; }
            [Column("EXIT_DATE")]
            public DateTime ExitDate { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
        }

        [Table("V_CLAIM_BILL")]
        public class V_ClaimBill
        {
            [Column("BILL_DATE")] public DateTime? BILL_DATE { get; set; }
            [Column("BILL_NO")] public string BILL_NO { get; set; }
            [Key] [Column("CLAIM_BILL_ID")] public long? CLAIM_BILL_ID { get; set; }
            [Column("CLAIM_ID")] public long? CLAIM_ID { get; set; }
            [Column("CURRENCY_RATE_DATE")] public DateTime? CURRENCY_RATE_DATE { get; set; }
            [Column("CURRENCY_TYPE")] public string CURRENCY_TYPE { get; set; }
            [Column("EXCHANGE_RATE_ID")] public long? EXCHANGE_RATE_ID { get; set; }
            [Column("FOREX_SELLING")] public long? FOREX_SELLING { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PAYMENT_TYPE")] public string PAYMENT_TYPE { get; set; }
            [Column("PAYROLL_ID")] public long? PAYROLL_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("CLAIM_STATUS")] public string CLAIM_STATUS { get; set; }
        }

        [Table("T_CLAIM_BILL")]
        public class ClaimBill
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("CLAIM_ID")]
            public Int64 ClaimId { get; set; }
            [Required]
            [Column("BILL_NO")]
            [Display(Name = "BILL NO", Description = "Fatura  No")]
            [MaxLength(20, ErrorMessage = "")]
            public string BillNo { get; set; }
            [Required]
            [Column("BILL_DATE")]
            [Display(Name = "BILL DATE", Description = "Fatura Tarihi")]
            public DateTime BillDate { get; set; }
            [Column("PAYMENT_TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string PaymentType { get; set; }
            [Column("EXCHANGE_RATE_ID")]
            public Int64? ExchangeRateId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }


        }
        [Table("T_CONTACT_NOTE")]
        public class ContactNote
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("CONTACT_ID")]
            public Int64 ContactId { get; set; }
            [Required]
            [Column("TYPE")]
            [Display(Name = "TYPE", Description = "Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Required]
            [Column("NOTE_ID")]
            public Int64 NoteId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("T_INSURED_NOTE")]
        public class InsuredNote
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("INSURED_ID")]
            public Int64 InsuredId { get; set; }
            [Required]
            [Column("TYPE")]
            [Display(Name = "TYPE", Description = "Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Required]
            [Column("NOTE_ID")]
            public Int64 NoteId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_CLAIM_PAYMENT")]
        public class ClaimPayment
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("CLAIM_ID")]
            public Int64 ClaimId { get; set; }
            [Column("PAYMENT_TYPE")]
            public string PaymentType { get; set; }
            [Column("DUE_DATE")]
            public DateTime? DueDate { get; set; }
            [Column("PAYMENT_DATE")]
            public DateTime? PaymentDate { get; set; }
            [Column("AMOUNT")]
            public Decimal? Amount { get; set; }
            [Column("BANK_ACCOUNT_ID")]
            public Int64? BankAccountId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
        }
        [Table("V_CLAIM_NOTE")]
        public class V_ClaimNote
        {
            [Column("CLAIM_ID")] public long? CLAIM_ID { get; set; }
            [Key] [Column("CLAIM_NOTE_ID")] public long? CLAIM_NOTE_ID { get; set; }
            [Column("CLAIM_NOTE_TYPE")] public string CLAIM_NOTE_TYPE { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("NOTE_DECRIPTION")] public string NOTE_DECRIPTION { get; set; }
            [Column("NOTE_ID")] public long? NOTE_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
        }

        [Table("T_CLAIM_NOTE")]
        public class ClaimNote : ISpExecute3
        {
            [Required]
            [Column("ID", TypeName = "BASE")]
            public Int64 Id { get; set; }
            [Required]
            [Column("CLAIM_ID")]
            public Int64 ClaimId { get; set; }
            [Required]
            [Column("TYPE")]
            [Display(Name = "TYPE", Description = "Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Required]
            [Column("NOTE_ID")]
            public Int64 NoteId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_CLAIM_REASON")]
        public class ClaimReason
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("REASON_ID")]
            public Int64 ReasonId { get; set; }
            [Required]
            [Column("TYPE")]
            [Display(Name = "TYPE", Description = "Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("DESCRIPTION")]
            [MaxLength(4000, ErrorMessage = "")]
            public string Description { get; set; }
            [Column("START_DATE")]
            public DateTime StartDate { get; set; }
            [Column("END_DATE")]
            public DateTime? EndDate { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("T_PACKAGE_NOTE")]
        public class PackageNote
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("PACKAGE_ID")]
            public Int64 PackageId { get; set; }
            [Required]
            [Column("NOTE_ID")]
            public Int64 NoteId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("T_PACKAGE_TPA_PRICE")]
        public class PackageTPAPrice
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("PACKAGE_ID")]
            public Int64 PackageId { get; set; }
            [Required]
            [Column("AMOUNT")]
            [Display(Name = "AMOUNT", Description = "Miktar")]
            public Decimal Amount { get; set; }
            [Required]
            [Column("TYPE")]
            [Display(Name = "TYPE", Description = "Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Required]
            [Column("START_DATE")]
            [Display(Name = "START DATE", Description = "Başlangıç Tarihi")]
            public DateTime StartDate { get; set; }
            [Column("CASH_PAYMENT_TYPE")]
            [Display(Name = "CASH PAYMENT TYPE", Description = "Nakit Ödeme Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string CashPaymentType { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("T_RULE_GROUP")]
        public class RuleGroup
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_RULE_CATEGORY_RULE")]
        public class RuleCategoryRule
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("RULE_CATEGORY_ID")]
            public Int64 RuleCategoryId { get; set; }
            [Column("RULE_ID")]
            public Int64 RuleId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
        }

        [Table("T_RULE_GROUP_RULE")]
        public class RuleGroupRule
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("RULE_GROUP_ID")]
            public Int64 RuleGroupId { get; set; }
            [Column("RULE_ID")]
            public Int64 RuleId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("T_PROVIDER_RULE")]
        public class ProviderRule
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("PROVIDER_ID")]
            public Int64 ProviderId { get; set; }
            [Column("RULE_ID")]
            public Int64 RuleId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("T_PROVIDER_GROUP_RULE")]
        public class ProviderGroupRule
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("PROVIDER_GROUP_ID")]
            public Int64 ProviderGroupId { get; set; }
            [Column("RULE_ID")]
            public Int64 RuleId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("T_COVERAGE_RULE")]
        public class CoverageRule
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("COVERAGE_ID")]
            public Int64 CoverageId { get; set; }
            [Column("RULE_ID")]
            public Int64 RuleId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("T_PROCESS_GROUP_RULE")]
        public class ProcessGroupRule
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("PROCESS_GROUP_ID")]
            public Int64 ProcessGroupId { get; set; }
            [Column("RULE_ID")]
            public Int64 RuleId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("T_PROCESS_RULE")]
        public class ProcessRule
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("PROCESS_ID")]
            public Int64 ProcessId { get; set; }
            [Column("RULE_ID")]
            public Int64 RuleId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("T_RULE_INSURED")]
        public class RuleInsured
        {
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("RULE_ID")]
            public Int64? RuleId { get; set; }
            [Column("RULE_GROUP_ID")]
            public Int64? RuleGroupId { get; set; }
            [Column("INSURED_ID")]
            public Int64? InsuredId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_RULE_POLICY")]
        public class RulePolicy
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("RULE_ID")]
            public Int64? RuleId { get; set; }
            [Column("RULE_GROUP_ID")]
            public Int64? RuleGroupId { get; set; }
            [Column("POLICY_ID")]
            public Int64? PolicyId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_RULE_POLICY_GROUP")]
        public class RulePolicyGroup
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("RULE_ID")]
            public Int64 RuleId { get; set; }
            [Column("RULE_GROUP_ID")]
            public Int64? RuleGroupId { get; set; }
            [Column("POLICY_GROUP_ID")]
            public Int64 PolicyGroupId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_RULE_PACKAGE")]
        public class RulePackage
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("RULE_ID")]
            public Int64 RuleId { get; set; }
            [Column("RULE_GROUP_ID")]
            public Int64? RuleGroupId { get; set; }
            [Column("PACKAGE_ID")]
            public Int64 PackageId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_RULE_SUBPRODUCT")]
        public class RuleSubproduct
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("RULE_ID")]
            public Int64 RuleId { get; set; }
            [Column("RULE_GROUP_ID")]
            public Int64? RuleGroupId { get; set; }
            [Column("SUBPRODUCT_ID")]
            public Int64 SubproductId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_RULE_PRODUCT")]
        public class RuleProduct
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("RULE_ID")]
            public Int64? RuleId { get; set; }
            [Column("RULE_GROUP_ID")]
            public Int64? RuleGroupId { get; set; }
            [Column("PRODUCT_ID")]
            public Int64? ProductId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_CONTRACT_MEDIA")]
        public class ContractMedia
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("CONTRACT_ID")]
            public Int64 ContractId { get; set; }
            [Required]
            [Column("MEDIA_ID")]
            public Int64 MediaId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_APP_SETTING")]
        public class AppSetting
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("ENVIRONMENT")]
            [MaxLength(3, ErrorMessage = "")]
            public string Environment { get; set; }
            [Required]
            [Column("KEY")]
            [Display(Name = "KEY", Description = "Anahtar")]
            [MaxLength(100, ErrorMessage = "")]
            public string Key { get; set; }
            [Required]
            [Column("VALUE")]
            [Display(Name = "VALUE", Description = "Değer")]
            [MaxLength(4000, ErrorMessage = "")]
            public string Value { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_LOOKUP")]
        public class Lookup
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("NET_ENUM_TYPE_NAME")]
            [MaxLength(400, ErrorMessage = "")]
            public string NetEnumTypeName { get; set; }
            [Required]
            [Column("CODE")]
            [Display(Name = "CODE", Description = "Kod")]
            [MaxLength(10, ErrorMessage = "")]
            public string Code { get; set; }
            [Required]
            [Column("ORDINAL")]
            [Display(Name = "ODINAL", Description = "Sıra")]
            [MaxLength(3, ErrorMessage = "")]
            public string Ordinal { get; set; }
            [Required]
            [Column("TEXT")]
            [Display(Name = "TEXT", Description = "Metin")]
            [MaxLength(100, ErrorMessage = "")]
            public string Text { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_INTEGRATION_LOG")]
        public class IntegrationLog
        {
            public IntegrationLog()
            {
                this.CorrelationId = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString();
            }
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("TYPE")]
            [Display(Name = "Tipi", Description = "Log Tipi")]
            public string Type { get; set; }
            [Column("PARENT_ID")]
            public Int64? ParentId { get; set; }
            [Column("PAYROLL_ID")]
            public Int64? PayrollId { get; set; }
            [Required]
            [Column("WS_NAME")]
            [Display(Name = "WS NAME", Description = "WS Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string WsName { get; set; }
            [Required]
            [Column("WS_METHOD")]
            [Display(Name = "WS METHOD", Description = "WS Yöntem")]
            [MaxLength(100, ErrorMessage = "")]
            public string WsMethod { get; set; }
            [Column("REQUEST")]
            public string Request { get; set; }
            [Column("RESPONSE")]
            public string Response { get; set; }
            [Column("WS_REQUEST_DATE")]
            public DateTime? WsRequestDate { get; set; }
            [Column("WS_RESPONSE_DATE")]
            public DateTime? WsResponseDate { get; set; }
            [Required]
            [Column("HTTP_STATUS")]
            [Display(Name = "HTTPS STATUS", Description = "HTTP Statüs")]
            [MaxLength(10, ErrorMessage = "")]
            public string HttpStatus { get; set; }
            [Required]
            [Column("RESPONSE_STATUS_CODE")]
            [Display(Name = "RESPONSE STATUS CODE", Description = "Yanıt Statüs Kod")]
            [MaxLength(10, ErrorMessage = "")]
            public string ResponseStatusCode { get; set; }
            [Required]
            [Column("RESPONSE_STATUS_DESC")]
            [Display(Name = "RESPONSE STATU DESCS", Description = " Yanıt Statüs Desc")]
            [MaxLength(4000, ErrorMessage = "")]
            public string ResponseStatusDesc { get; set; }
            [Column("PROVIDER_ID")]
            public Int64? ProviderId { get; set; }
            [Column("CLAIM_ID")]
            public Int64? ClaimId { get; set; }
            [Column("POLICY_ID")]
            public Int64? PolicyId { get; set; }
            [Column("POLICY_NUMBER")]
            public string PolicyNumber { get; set; }
            [Column("INSURED_ID")]
            public Int64? InsuredId { get; set; }
            [Column("IDENTITY_NO")]
            public Int64? IdentityNo { get; set; }
            [Column("INSURED_NATIONALITY_ID")]
            public Int64? InsuredNationalityId { get; set; }
            [Column("MEDULA_NO")]
            [MaxLength(20, ErrorMessage = "")]
            public string MedulaNo { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

            [Column("CORRELATION_ID")]
            public string CorrelationId { get; set; }
            [Column("EXT_PROVIDER_NO")] public string EXT_PROVIDER_NO { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("IS_IMECE_WS")] public Int64? IS_IMECE_WS { get; set; }
        }
        [Table("T_INTEGRATION_COMPANY_INFO")]
        public class IntegrationCompanyInfo
        {
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("PROVIDER_ID")]
            public Int64 ProviderId { get; set; }
            [Column("USERNAME")]
            [MaxLength(45, ErrorMessage = "")]
            public string Username { get; set; }
            [Column("PASSWORD")]
            [MaxLength(45, ErrorMessage = "")]
            public string Password { get; set; }
            [Column("STATUS")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_EQUIPMENT")]
        public class Equipment
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("NAME")]
            [Display(Name = "NAME", Description = "Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("DOCTOR_BRANCH_ID")]
            public Int64 DoctorBranchId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_TABLE_CONFIGURATION")]
        public class TableConfiguration
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("TABLE_NAME")]
            [MaxLength(30, ErrorMessage = "")]
            public string TableName { get; set; }
            [Required]
            [Column("KEY")]
            [Display(Name = "KEY", Description = "Anahtar")]
            [MaxLength(100, ErrorMessage = "")]
            public string Key { get; set; }
            [Required]
            [Column("VALUE")]
            [Display(Name = "VALUE", Description = "Değer")]
            [MaxLength(4000, ErrorMessage = "")]
            public string Value { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PRICE")]
        public class Price
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("AGE_START")]
            [Display(Name = "AGE START", Description = "Başlangıç Yaşı")]
            public int AgeStart { get; set; }
            [Required]
            [Column("AGE_END")]
            [Display(Name = "AGE END", Description = "Bitiş Yaşı")]
            public int AgeEnd { get; set; }
            [Required]
            [Column("GENDER")]
            [Display(Name = "GENDER", Description = "Cinsiyet")]
            [MaxLength(3, ErrorMessage = "")]
            public string Gender { get; set; }
            [Required]
            [Column("INDIVIDUAL_TYPE")]
            [Display(Name = "INDIVIDUAL TYPE", Description = "Birey Tipi")]
            [MaxLength(3, ErrorMessage = "")]
            public string IndividualType { get; set; }
            [Column("PREMIUM")]
            public Decimal? Premium { get; set; }
            [Column("DAY_COUNT")]
            public int DayCount { get; set; }
            [Column("PRICE_LIST_ID")]
            public Int64 PriceListId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_ENDORSEMENT_POLICY")]
        public class EndorsementPolicy
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("ENDORSEMENT_ID")]
            public Int64 EndorsementId { get; set; }
            [Column("OLD_POLICY_ID")]
            public Int64? OldPolicyId { get; set; }
            [Required]
            [Column("POLICY_ID")]
            public Int64 PolicyId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PARAMETER")]
        public class Parameter
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("SYSTEM_TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string SystemType { get; set; }
            [Column("ENVIRONMENT")]
            [Display(Name = "ENVIRORMENT", Description = "Çevre")]
            [MaxLength(3, ErrorMessage = "")]
            public string Environment { get; set; }
            [Required]
            [Column("KEY")]
            [Display(Name = "KEY", Description = "Anahtar")]
            [MaxLength(100, ErrorMessage = "")]
            public string Key { get; set; }
            [Required]
            [Column("VALUE")]
            [Display(Name = "VALUE", Description = "Değer")]
            [MaxLength(4000, ErrorMessage = "")]
            public string Value { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_COMPANY_PARAMETER")]
        public class CompanyParameter
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("COMPANY_ID")]
            public Int64 CompanyId { get; set; }
            [Column("PARAMETER_ID")]
            public Int64 ParameterId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_COMPANY_NOTE")]
        public class CompanyNote
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("COMPANY_ID")]
            public Int64 CompanyId { get; set; }
            [Column("NOTE_ID")]
            public Int64 NoteId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_COMPANY_CONTACT")]
        public class CompanyContact
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("COMPANY_ID")]
            public Int64 CompanyId { get; set; }
            [Column("CONTACT_ID")]
            public Int64 ContactId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_COMPANY_PHONE")]
        public class CompanyPhone
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("COMPANY_ID")]
            public Int64 CompanyId { get; set; }
            [Column("PHONE_ID")]
            public Int64 PhoneId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_COMPANY_EMAIL")]
        public class CompanyEmail
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("COMPANY_ID")]
            public Int64 CompanyId { get; set; }
            [Column("EMAIL_ID")]
            public Int64 EmailId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_COMPANY_BANK_ACCOUNT")]
        public class CompanyBankAccount
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("COMPANY_ID")]
            public Int64 CompanyId { get; set; }
            [Column("BANK_ACCOUNT_ID")]
            public Int64 BankAccountId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_RULE_COMPANY")]
        public class RuleCompany
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("COMPANY_ID")]
            public Int64? CompanyId { get; set; }
            [Column("RULE_ID")]
            public Int64? RuleId { get; set; }
            [Column("RULE_GROUP_ID")]
            public Int64? RuleGroupId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PRODUCT_PARAMETER")]
        public class ProductParameter
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("PRODUCT_ID")]
            public Int64 ProductId { get; set; }
            [Column("PARAMETER_ID")]
            public Int64 ParameterId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PRODUCT_NOTE")]
        public class ProductNote
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("PRODUCT_ID")]
            public Int64 ProductId { get; set; }
            [Column("NOTE_ID")]
            public Int64 NoteId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PRODUCT_MEDIA")]
        public class ProductMedia
        {
            [Required]
            [Key]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("PRODUCT_ID", TypeName = "OBJECTID")]
            public Int64 ProductId { get; set; }
            [Column("MEDIA_ID")]
            public Int64 MediaId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("T_CLAIM_MEDIA")]
        public class ClaimMedia
        {
            [Required]
            [Key]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("CLAIM_ID", TypeName = "OBJECTID")]
            public Int64 ClaimId { get; set; }
            [Column("MEDIA_ID")]
            public Int64 MediaId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
        }

        [Table("T_SUBPRODUCT_PARAMETER")]
        public class SubproductParameter
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("SUBPRODUCT_ID")]
            public Int64 SubproductId { get; set; }
            [Column("PARAMETER_ID")]
            public Int64 ParameterId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_SUBPRODUCT_NOTE")]
        public class SubproductNote
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("SUBPRODUCT_ID")]
            public Int64 SubproductId { get; set; }
            [Column("NOTE_ID")]
            public Int64 NoteId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_SUBPRODUCT_MEDIA")]
        public class SubproductMedia
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("SUBPRODUCT_ID", TypeName = "OBJECTID")]
            public Int64 SubproductId { get; set; }
            [Column("MEDIA_ID")]
            public Int64 MediaId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_COVERAGE_PARAMETER")]
        public class CoverageParameter
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("COVERAGE_ID")]
            public Int64 CoverageId { get; set; }
            [Column("PARAMETER_ID")]
            public Int64 ParameterId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PACKAGE_PARAMETER")]
        public class PackageParameter
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("PACKAGE_ID")]
            public Int64 PackageId { get; set; }
            [Column("PARAMETER_ID")]
            public Int64 ParameterId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PROVIDER_PARAMETER")]
        public class ProviderParameter
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("PROVIDER_ID")]
            public Int64 ProviderId { get; set; }
            [Column("PARAMETER_ID")]
            public Int64 ParameterId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("T_INSURED_PARAMETER")]
        public class insuredParameter
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("INSURED_ID")]
            public Int64 InsuredId { get; set; }
            [Column("PARAMETER_ID")]
            public Int64 ParameterId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("T_PROVIDER_NOTE")]
        public class ProviderNote
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("PROVIDER_ID")]
            public Int64 ProviderId { get; set; }
            [Column("NOTE_ID")]
            public Int64 NoteId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PROVIDER_MEDIA")]
        public class ProviderMedia
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("PROVIDER_ID", TypeName = "OBJECTID")]
            public Int64 ProviderId { get; set; }
            [Column("MEDIA_ID")]
            public Int64 MediaId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("T_PACKAGE_MEDIA")]
        public class PackageMedia
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("PACKAGE_ID", TypeName = "OBJECTID")]
            public Int64 PackageId { get; set; }
            [Column("MEDIA_ID")]
            public Int64 MediaId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("T_PROVIDER_USER")]
        public class ProviderUser : ISpExecute3
        {
            [Required]
            [Column("ID", TypeName = "BASE")]
            public Int64 Id { get; set; }
            [Column("PROVIDER_ID")]
            public Int64 ProviderId { get; set; }
            [Column("USER_ID")]
            public Int64 UserId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }

        [Table("T_PROVIDER_PHONE")]
        public class ProviderPhone : ISpExecute3
        {
            [Required]
            [Column("ID", TypeName = "BASE")]
            public Int64 Id { get; set; }
            [Column("PROVIDER_ID")]
            public Int64 ProviderId { get; set; }
            [Column("PHONE_ID")]
            public Int64 PhoneId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_AGENCY_USER")]
        public class AgencyUser
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("AGENCY_ID")]
            public Int64 AgencyId { get; set; }
            [Column("USER_ID")]
            public Int64 UserId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_COMPANY_USER")]
        public class CompanyUser
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("COMPANY_ID")]
            public Int64 CompanyId { get; set; }
            [Column("USER_ID")]
            public Int64 UserId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PROVIDER_EQUIPMENT")]
        public class ProviderEquipment
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("PROVIDER_ID")]
            public Int64 ProviderId { get; set; }
            [Column("EQUIPMENT_ID")]
            public Int64 EquipmentId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_POLICY_PARAMETER")]
        public class PolicyParameter
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("POLICY_ID")]
            public Int64 PolicyId { get; set; }
            [Column("PARAMETER_ID")]
            public Int64 ParameterId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_POLICY_CLAIM_REASON")]
        public class PolicyClaimReason
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("POLICY_ID")]
            public Int64 PolicyId { get; set; }
            [Column("CLAIM_REASON_ID")]
            public Int64 ClaimReasonId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_SAGMER_PACKAGE_TYPE")]
        public class SagmerPackageType
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("BRANCH_ID")]
            public Int64 BranchId { get; set; }
            [Required]
            [Column("NAME")]
            [Display(Name = "NAME", Description = "Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("IS_BIRTH")]
            [MaxLength(1, ErrorMessage = "")]
            public string IsBirth { get; set; }
            [Column("DESCRIPTION")]
            [MaxLength(1000, ErrorMessage = "")]
            public string Description { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_SAGMER_COVERAGE_CATEGORY")]
        public class SagmerCoverageCategory
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("BRANCH_ID")]
            public Int64 BranchId { get; set; }
            [Column("COVERAGE_TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string CoverageType { get; set; }
            [Required]
            [Column("NAME")]
            [Display(Name = "NAME", Description = "Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string Name { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_SAGMER_COVERAGE_TYPE")]
        public class SagmerCoverageType
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("COVERAGE_NO")]
            [MaxLength(3, ErrorMessage = "")]
            public string CoverageNo { get; set; }
            [Required]
            [Column("COVERAGE_NAME")]
            [Display(Name = "CAVERAGE NAME", Description = "Teminat Adı")]
            [MaxLength(100, ErrorMessage = "")]
            public string CoverageName { get; set; }
            [Column("PARENT_ID")]
            public Int64 ParentId { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_SAGMER_COVERAGE")]
        public class SagmerCoverage
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("SAGMER_COVERAGE_CATEGORY_ID")]
            public Int64 SagmerCoverageCategoryId { get; set; }
            [Required]
            [Column("SAGMER_COVERAGE_TYPE_ID")]
            public Int64 SagmerCoverageTypeId { get; set; }
            [Column("DESCRIPTION")]
            [MaxLength(1000, ErrorMessage = "")]
            public string Description { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_SAGMER_PLAN")]
        public class SagmerPlan
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("SAGMER_PACKAGE_TYPE_ID")]
            public Int64 SagmerPackageTypeId { get; set; }
            [Column("SELECTION_TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string SelectionType { get; set; }
            [Column("LOCATION_TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string LocationType { get; set; }
            [Column("NETWORK_TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string NetworkType { get; set; }
            [Column("DESCRIPTION")]
            [MaxLength(1000, ErrorMessage = "")]
            public string Description { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }

        }
        [Table("T_PERIOD")]
        public class Period
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("SHORT_DATE")]
            public DateTime ShortDate { get; set; }
            [Column("PERIOD")]
            public int fPeriod { get; set; }

        }
        [Table("T_INTERVAL")]
        public class Interval
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("TYPE")]
            [MaxLength(3, ErrorMessage = "")]
            public string Type { get; set; }
            [Column("INTERVAL")]
            [MaxLength(20, ErrorMessage = "")]
            public string fInterval { get; set; }
            [Column("FIRST")]
            public Int64 First { get; set; }
            [Column("LAST")]
            public Int64 Last { get; set; }

        }
        [Table("T_SP_RESPONSE_XML")]
        public class SpResponseXml
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Required]
            [Column("SP_REQUEST_ID")]
            [MaxLength(100, ErrorMessage = "")]
            public string SpRequestId { get; set; }
            [Column("XML")]
            public string Xml { get; set; }
            [Column("SP_REQUEST_DATE")]
            public DateTime SpRequestDate { get; set; }

        }
        [Table("V_PRODUCT_MEDIA")]
        public class V_ProductMedia
        {
            [Key]
            [Column("PRODUCT_MEDIA_ID")]
            public Int64 ProductMediaId { get; set; }
            [Column("PRODUCT_ID", TypeName = "OBJECTID")]
            public Int64 ProductId { get; set; }
            [Column("PRODUCT_CODE")]
            public string ProductCode { get; set; }

            [Column("PRODUCT_DESCRIPTION")]
            public string ProductDescription { get; set; }
            [Column("MEDIA_ID")]
            public Int64 MediaId { get; set; }
            [Column("MEDIA_NAME")]
            public string MediaName { get; set; }
            [Column("FILENAME")]
            public string FileName { get; set; }
            [Column("FILE_CONTENT")]
            [MaxLength(100, ErrorMessage = "")]
            public OracleBlob FileContent { get; set; }
            [Column("STATUS")]
            public string Status { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("SP_RESPONSE_ID")]
            public Int64? SpResponseId { get; set; }

        }

        [Table("V_CLAIM_MEDIA")]
        public class V_ClaimMedia
        {
            [Key]
            [Column("CLAIM_MEDIA_ID")]
            public Int64 ClaimMediaId { get; set; }
            [Column("INSURED_ID")]
            public Int64 InsuredId { get; set; }
            [Column("CLAIM_ID", TypeName = "OBJECTID")]
            public Int64 ClaimId { get; set; }
            [Column("MEDIA_ID")]
            public Int64 MediaId { get; set; }
            [Column("MEDIA_NAME")]
            public string MediaName { get; set; }
            [Column("FILENAME")]
            public string FileName { get; set; }
            [Column("FILE_CONTENT")]
            [MaxLength(100, ErrorMessage = "")]
            public OracleBlob FileContent { get; set; }
            [Column("STATUS")]
            public string Status { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("SP_RESPONSE_ID")]
            public Int64? SpResponseId { get; set; }

        }

        [Table("V_PACKAGE_MEDIA")]
        public class V_PackageMedia
        {
            [Key]
            [Column("PACKAGE_MEDIA_ID")]
            public Int64 PackageMediaId { get; set; }
            [Column("PACKAGE_ID", TypeName = "OBJECTID")]
            public Int64 PackageId { get; set; }
            [Column("MEDIA_ID")]
            public Int64 MediaId { get; set; }
            [Column("MEDIA_NAME")]
            public string MediaName { get; set; }
            [Column("FILENAME")]
            public string FileName { get; set; }
            [Column("FILE_CONTENT")]
            [MaxLength(100, ErrorMessage = "")]
            public OracleBlob FileContent { get; set; }
            [Column("STATUS")]
            public string Status { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("SP_RESPONSE_ID")]
            public Int64? SpResponseId { get; set; }

        }

        [Table("V_SUBPRODUCT_MEDIA")]
        public class V_SubproductMedia
        {
            [Key]
            [Column("SUBPRODUCT_MEDIA_ID")]
            public Int64 SubproductMediaId { get; set; }
            [Column("SUBPRODUCT_ID", TypeName = "OBJECTID")]
            public Int64 SubproductId { get; set; }
            [Column("PRODUCT_ID")]
            public Int64 ProductId { get; set; }
            [Column("COMPANY_ID")]
            public Int64 CompanyId { get; set; }
            [Column("COMPANY_NAME")]
            public string CompanyName { get; set; }
            [Column("PRODUCT_CODE")]
            public string ProductCode { get; set; }
            [Column("SUBPRODUCT_CODE")]
            public string SubproductCode { get; set; }
            [Column("SUBPRODUCT_NAME")]
            public string SubproductName { get; set; }

            [Column("MEDIA_ID")]
            public Int64 MediaId { get; set; }
            [Column("MEDIA_NAME")]
            public string MediaName { get; set; }
            [Column("FILENAME")]
            public string FileName { get; set; }
            [Column("FILE_CONTENT")]
            [MaxLength(100, ErrorMessage = "")]
            public OracleBlob FileContent { get; set; }
            [Column("STATUS")]
            public string Status { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }


            [Column("SP_RESPONSE_ID")]
            public Int64? SpResponseId { get; set; }
        }

        [Table("V_PROVIDER_MEDIA")]
        public class V_ProviderMedia
        {
            [Key]
            [Column("PROVIDER_MEDIA_ID")]
            public Int64 ProviderMediaId { get; set; }
            [Column("PROVIDER_ID", TypeName = "OBJECTID")]
            public Int64 ProviderId { get; set; }
            [Column("PROVIDER_NAME")]
            public string ProviderName { get; set; }
            [Column("PROVIDER_TITLE")]
            public string ProviderTitle { get; set; }
            [Column("MEDIA_ID")]
            public Int64 MediaId { get; set; }
            [Column("MEDIA_NAME")]
            public string MediaName { get; set; }
            [Column("FILENAME")]
            public string FileName { get; set; }
            [Column("FILE_CONTENT")]
            [MaxLength(100, ErrorMessage = "")]
            public OracleBlob FileContent { get; set; }
            [Column("STATUS")]
            public string Status { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("SP_RESPONSE_ID")]
            public Int64? SpResponseId { get; set; }
        }

        [Table("V_PROVIDER_CONTRACT_IDS")]
        public class V_ProviderContractIds
        {
            [Key] [Column("PROVIDER_ID")] public long? PROVIDER_ID { get; set; }
            [Column("DOGUM_CONTRACT_ID")] public long? DOGUM_CONTRACT_ID { get; set; }
            [Column("MDP_CONTRACT_ID")] public long? MDP_CONTRACT_ID { get; set; }
            [Column("OSS_CONTRACT_ID")] public long? OSS_CONTRACT_ID { get; set; }
            [Column("TSS_CONTRACT_ID")] public long? TSS_CONTRACT_ID { get; set; }

        }


        [Table("V_TABLE_COLUMN")]
        public class V_TableColumn
        {
            [Key][Column("ID")]
            public Int64 Id { get; set; }
            [Column("TABLE_NAME")]
            public string TableName { get; set; }
            [Column("COLUMN_NAME")]
            public string ColumnName { get; set; }
            [Column("HEADER_NAME")]
            public string HeaderName { get; set; }
            [Column("DATA_TYPE")]
            public string DataType { get; set; }
            [Column("DATA_LENGTH")]
            public string DataLength { get; set; }
            [Column("COLUMN_ORDER")]
            public string ColumnOrder { get; set; }
        }
        [Table("V_CONTRACT_LIST")]
        public class V_ContractList
        {
            [Key][Column("CONTRACT_ID")] public long? CONTRACT_ID { get; set; }
            [Column("CONTRACT_NO")] public string CONTRACT_NO { get; set; }
            [Column("CONTRACT_TYPE")] public string CONTRACT_TYPE { get; set; }
            [Column("DESCRIPTION")] public string DESCRIPTION { get; set; }
            [Column("DISCOUNT_RATIO")] public long? DISCOUNT_RATIO { get; set; }
            [Column("END_DATE")] public DateTime? END_DATE { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PAYMENT_DAY")] public long? PAYMENT_DAY { get; set; }
            [Column("PROVIDER_ID")] public long? PROVIDER_ID { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Column("REASON_ID")] public long? REASON_ID { get; set; }
            [Column("RESPONSE_DATE")] public DateTime? RESPONSE_DATE { get; set; }
            [Column("REVISION_DATE")] public DateTime? REVISION_DATE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("START_DATE")] public DateTime? START_DATE { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_CONTRACT_LIST_HIST")]
        public class V_ContractListHist : IEntity
        {
            [Key][Column("CONTRACT_ID", TypeName = "BASEID")] public long? CONTRACT_ID { get; set; }
            [History] [Column("CONTRACT_NO")] public string CONTRACT_NO { get; set; }
            [History] [Column("CONTRACT_TYPE")] public string CONTRACT_TYPE { get; set; }
            [Column("DESCRIPTION")] public string DESCRIPTION { get; set; }
            [History] [Column("DISCOUNT_RATIO")] public long? DISCOUNT_RATIO { get; set; }
            [Column("END_DATE")] public DateTime? END_DATE { get; set; }
            [Column("IP")] public string IP { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [History] [Column("PAYMENT_DAY")] public long? PAYMENT_DAY { get; set; }
            [Column("PROVIDER_ID")] public long? PROVIDER_ID { get; set; }
            [History] [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Column("REASON_ID")] public long? REASON_ID { get; set; }
            [Column("RESPONSE_DATE")] public DateTime? RESPONSE_DATE { get; set; }
            [History] [Column("REVISION_DATE")] public DateTime? REVISION_DATE { get; set; }
            [Column("SP_REQUEST_DATE")] public DateTime? SP_REQUEST_DATE { get; set; }
            [Column("SP_RESPONSE_DATE")] public DateTime? SP_RESPONSE_DATE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("START_DATE")] public DateTime? START_DATE { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("USERNAME")] public string USERNAME { get; set; }
            [Column("USER_ID")] public long? USER_ID { get; set; }
            [Column("PRICING_LIST")]
            public List<V_ContractProcessGroupHist> ContractProcessGroup { get; set; } = new List<V_ContractProcessGroupHist>();
            [Column("NETWORK_LIST")]
            public List<V_ContractNetworkHist> ContractNetwork { get; set; } = new List<V_ContractNetworkHist>();
        }

        [Table("V_CONTRACT_NETWORK_HIST")]
        [DisplayColumn("NETWORK")]
        public class V_ContractNetworkHist : IEntity
        {
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("CONTRACT_ID", TypeName = "BASEID")] public long? CONTRACT_ID { get; set; }
            [Key][Column("CONTRACT_NETWORK_ID")] public long? CONTRACT_NETWORK_ID { get; set; }
            [Column("CONTRACT_NO")] public string CONTRACT_NO { get; set; }
            [Column("CONTRACT_STATUS")] public string CONTRACT_STATUS { get; set; }
            [Column("END_DATE")] public DateTime? END_DATE { get; set; }
            [Column("IP")] public string IP { get; set; }
            [Column("NETWORK_CATEGORY")] public string NETWORK_CATEGORY { get; set; }
            [Column("NETWORK_ID")] public long? NETWORK_ID { get; set; }
            [History] [Column("NETWORK_NAME")] public string NETWORK_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PROVIDER_ID")] public long? PROVIDER_ID { get; set; }
            [Column("SP_REQUEST_DATE")] public DateTime? SP_REQUEST_DATE { get; set; }
            [Column("SP_RESPONSE_DATE")] public DateTime? SP_RESPONSE_DATE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("START_DATE")] public DateTime? START_DATE { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("USERNAME")] public string USERNAME { get; set; }
            [Column("USER_ID")] public long? USER_ID { get; set; }

        }

        [Table("V_CONTRACT_PROCESS_GROUP_HIST")]
        [DisplayColumn("ÜCRETLENDİRME")]
        public class V_ContractProcessGroupHist : IEntity
        {
            [History] [Column("AMOUNT")] public long? AMOUNT { get; set; }
            [Column("CONTRACT_ID", TypeName = "BASEID")] public long? CONTRACT_ID { get; set; }
            [Key][Column("CONTRACT_PROCESS_GROUP_ID")] public long? CONTRACT_PROCESS_GROUP_ID { get; set; }
            [Column("IP")] public string IP { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [History] [Column("PRICING_TYPE")] public string PRICING_TYPE { get; set; }
            [Column("PROCESS_GROUP_ID")] public long? PROCESS_GROUP_ID { get; set; }
            [Column("PROCESS_GROUP_NAME")] public string PROCESS_GROUP_NAME { get; set; }
            [Column("PROCESS_LIST_ID")] public long? PROCESS_LIST_ID { get; set; }
            [Column("PROCESS_LIST_NAME")] public string PROCESS_LIST_NAME { get; set; }
            [Column("SP_REQUEST_DATE")] public DateTime? SP_REQUEST_DATE { get; set; }
            [Column("SP_RESPONSE_DATE")] public DateTime? SP_RESPONSE_DATE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("USERNAME")] public string USERNAME { get; set; }
            [Column("USER_ID")] public long? USER_ID { get; set; }
        }

        [Table("V_PERSONNEL")]
        public class V_Personnel
        {
            [Column("ACTIVATION_TOKEN")] public string ACTIVATION_TOKEN { get; set; }
            [Column("BIRTHDATE")] public DateTime? BIRTHDATE { get; set; }
            [Column("CONTACT_ID")] public long? CONTACT_ID { get; set; }
            [Column("EMAIL")] public string EMAIL { get; set; }
            [Column("FIRST_NAME")] public string FIRST_NAME { get; set; }
            [Column("GENDER")] public string GENDER { get; set; }
            [Column("IDENTITY_NO")] public Int64? IDENTITY_NO { get; set; }
            [Column("IS_REMEMBER_ME")] public string IS_REMEMBER_ME { get; set; }
            [Column("LAST_NAME")] public string LAST_NAME { get; set; }
            [Column("MOBILE")] public string MOBILE { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Key][Column("PERSONNEL_ID")] public long? PERSONNEL_ID { get; set; }
            [Column("PERSON_ID")] public long? PERSON_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("TOKEN")] public string TOKEN { get; set; }
            [Column("USERNAME")] public string USERNAME { get; set; }
            [Column("USER_ID")] public long? USER_ID { get; set; }

        }

        [Table("V_AGENCY_PHONE")]
        public class V_AgencyPhone
        {
            [Column("AGENCY_ID")] public long? AGENCY_ID { get; set; }
            [Key][Column("AGENCY_PHONE_ID")] public long? AGENCY_PHONE_ID { get; set; }
            [Column("COUNTRY_ID")] public long? COUNTRY_ID { get; set; }
            [Column("COUNTRY_PHONE_CODE")] public string COUNTRY_PHONE_CODE { get; set; }
            [Column("EXTENSION")] public string EXTENSION { get; set; }
            [Column("IS_PRIMARY")] public string IS_PRIMARY { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PHONE_ID")] public long? PHONE_ID { get; set; }
            [Column("PHONE_NO")] public string PHONE_NO { get; set; }
            [Column("PHONE_TYPE")] public string PHONE_TYPE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_AGENCY")]
        public class V_Agency
        {
            [Column("ADDRESS_ID")] public long? ADDRESS_ID { get; set; }
            [Key][Column("AGENCY_ID")] public long AGENCY_ID { get; set; }
            [Column("AGENCY_NAME")] public string AGENCY_NAME { get; set; }
            [Column("AGENCY_NO")] public string AGENCY_NO { get; set; }
            [Column("CITY_ID")] public long? CITY_ID { get; set; }
            [Column("CITY_NAME")] public string CITY_NAME { get; set; }
            [Column("CLOSE_DOWN_DATE")] public DateTime? CLOSE_DOWN_DATE { get; set; }
            [Column("COMPANY_ID")] public long COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("CONTACT_FIRST_NAME")] public string CONTACT_FIRST_NAME { get; set; }
            [Column("CONTACT_ID")] public long? CONTACT_ID { get; set; }
            [Column("CONTACT_LAST_NAME")] public string CONTACT_LAST_NAME { get; set; }
            [Column("CONTACT_PHONE")] public string CONTACT_PHONE { get; set; }
            [Column("COUNTRY_ID")] public long? COUNTRY_ID { get; set; }
            [Column("COUNTRY_NAME")] public string COUNTRY_NAME { get; set; }
            [Column("COUNTY_ID")] public long? COUNTY_ID { get; set; }
            [Column("COUNTY_NAME")] public string COUNTY_NAME { get; set; }
            [Column("DETAILS")] public string DETAILS { get; set; }
            [Column("ESTABLISHMENT_DATE")] public DateTime? ESTABLISHMENT_DATE { get; set; }
            [Column("IS_OPEN")] public string IS_OPEN { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PLATE_NO")] public string PLATE_NO { get; set; }
            [Column("REGION_CODE")] public string REGION_CODE { get; set; }
            [Column("REGION_NAME")] public string REGION_NAME { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("TAX_NUMBER")] public Int64? TAX_NUMBER { get; set; }
            [Column("TAX_OFFICE")] public string TAX_OFFICE { get; set; }
            [Column("TITLE")] public string TITLE { get; set; }
            [Column("TYPE")] public string TYPE { get; set; }
            [Column("ZIP_CODE")] public string ZIP_CODE { get; set; }

        }

        [Table("V_PACKAGE")]
        public class V_Package
        {
            [Column("BRANCH_ID")] public long? BRANCH_ID { get; set; }
            [Column("BRANCH_NAME")] public string BRANCH_NAME { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("DUMMY_COUNT")] public long? DUMMY_COUNT { get; set; }
            [Column("IS_SBM_TRANSFER")] public string IS_SBM_TRANSFER { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Key][Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Column("PACKAGE_NAME")] public string PACKAGE_NAME { get; set; }
            [Column("PACKAGE_NO")] public string PACKAGE_NO { get; set; }
            [Column("PACKAGE_TYPE")] public string PACKAGE_TYPE { get; set; }
            [Column("PRICE_LIST_ID")] public long? PRICE_LIST_ID { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("PRODUCT_TYPE")] public string PRODUCT_TYPE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SUBPRODUCT_CODE")] public string SUBPRODUCT_CODE { get; set; }
            [Column("SUBPRODUCT_ID")] public long? SUBPRODUCT_ID { get; set; }
            [Column("SUBPRODUCT_NAME")] public string SUBPRODUCT_NAME { get; set; }
            [Column("IS_OPEN_TO_WS")] public string IS_OPEN_TO_WS { get; set; }
        }

        [Table("V_COMPANY")]
        public class V_Company
        {
            [Column("ADDRESS_DETAILS")] public string ADDRESS_DETAILS { get; set; }
            [Column("ADDRESS_ID")] public long? ADDRESS_ID { get; set; }
            [Column("CITY_ID")] public long? CITY_ID { get; set; }
            [Column("CITY_NAME")] public string CITY_NAME { get; set; }
            [Key][Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("CONTACT_ID")] public long? CONTACT_ID { get; set; }
            [Column("CORPORATE_ID")] public long? CORPORATE_ID { get; set; }
            [Column("CORP_NAME")] public string CORP_NAME { get; set; }
            [Column("CORP_TYPE")] public string CORP_TYPE { get; set; }
            [Column("COUNTRY_ID")] public long? COUNTRY_ID { get; set; }
            [Column("COUNTRY_NAME")] public string COUNTRY_NAME { get; set; }
            [Column("COUNTY_ID")] public long? COUNTY_ID { get; set; }
            [Column("COUNTY_NAME")] public string COUNTY_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("TAX_NUMBER")] public Int64? TAX_NUMBER { get; set; }
            [Column("TAX_OFFICE")] public string TAX_OFFICE { get; set; }
            [Column("TITLE")] public string TITLE { get; set; }
            [Column("ZIP_CODE")] public string ZIP_CODE { get; set; }

        }

        [Table("V_COMPANY_EMAIL")]
        public class V_CompanyEmail
        {
            [Key][Column("COMPANY_EMAIL_ID")] public long? COMPANY_EMAIL_ID { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("EMAIL")] public string EMAIL { get; set; }
            [Column("EMAIL_ID")] public long? EMAIL_ID { get; set; }
            [Column("EMAIL_TYPE")] public string EMAIL_TYPE { get; set; }
            [Column("IS_PRIMARY")] public string IS_PRIMARY { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_COMPANY_PHONE")]
        public class V_CompanyPhone
        {
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Key][Column("COMPANY_PHONE_ID")] public long? COMPANY_PHONE_ID { get; set; }
            [Column("COUNTRY_ID")] public long? COUNTRY_ID { get; set; }
            [Column("EXTENSION")] public string EXTENSION { get; set; }
            [Column("IS_PRIMARY")] public string IS_PRIMARY { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("NUM_CODE")] public string NUM_CODE { get; set; }
            [Column("PHONE_ID")] public long? PHONE_ID { get; set; }
            [Column("PHONE_NO")] public string PHONE_NO { get; set; }
            [Column("PHONE_TYPE")] public string PHONE_TYPE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_COMPANY_USER")]
        public class V_CompanyUser
        {
            [Column("COMPANY_ID")] public long COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Key][Column("COMPANY_USER_ID")] public long COMPANY_USER_ID { get; set; }
            [Column("PASSWORD")] public string PASSWORD { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("USERNAME")] public string USERNAME { get; set; }
            [Column("USER_ID")] public long USER_ID { get; set; }

        }

        [Table("V_COMPANY_BANK")]
        public class V_CompanyBank
        {
            [Column("BANK_ACCOUNT_ID")] public long? BANK_ACCOUNT_ID { get; set; }
            [Column("BANK_ACCOUNT_NAME")] public string BANK_ACCOUNT_NAME { get; set; }
            [Column("BANK_ACCOUNT_NO")] public string BANK_ACCOUNT_NO { get; set; }
            [Column("BANK_BRANCH_CODE")] public string BANK_BRANCH_CODE { get; set; }
            [Column("BANK_BRANCH_ID")] public long? BANK_BRANCH_ID { get; set; }
            [Column("BANK_BRANCH_NAME")] public string BANK_BRANCH_NAME { get; set; }
            [Column("BANK_ID")] public long? BANK_ID { get; set; }
            [Column("BANK_IS_PRIMARY")] public string BANK_IS_PRIMARY { get; set; }
            [Column("BANK_NAME")] public string BANK_NAME { get; set; }
            [Key][Column("COMPANY_BANK_ID")] public long? COMPANY_BANK_ID { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("CURRENCY_TYPE")] public string CURRENCY_TYPE { get; set; }
            [Column("IBAN")] public string IBAN { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SWIFT_CODE")] public string SWIFT_CODE { get; set; }

        }

        [Table("V_COMPANY_CONTACT")]
        public class V_CompanyContact
        {
            [Key][Column("COMPANY_CONTACT_ID")] public long? COMPANY_CONTACT_ID { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("CONTACT_EMAIL_ID")] public long? CONTACT_EMAIL_ID { get; set; }
            [Column("CONTACT_ID")] public long? CONTACT_ID { get; set; }
            [Column("CONTACT_MOBILE_PHONE_ID")] public long? CONTACT_MOBILE_PHONE_ID { get; set; }
            [Column("CONTACT_PHONE_ID")] public long? CONTACT_PHONE_ID { get; set; }
            [Column("EMAIL")] public string EMAIL { get; set; }
            [Column("EMAIL_ID")] public long? EMAIL_ID { get; set; }
            [Column("EXTENSION")] public string EXTENSION { get; set; }
            [Column("FIRST_NAME")] public string FIRST_NAME { get; set; }
            [Column("IDENTITY_NO")] public string IDENTITY_NO { get; set; }
            [Column("LAST_NAME")] public string LAST_NAME { get; set; }
            [Column("MIDDLE_NAME")] public string MIDDLE_NAME { get; set; }
            [Column("MOBILE_PHONE_ID")] public long? MOBILE_PHONE_ID { get; set; }
            [Column("MOBILE_PHONE_NO")] public string MOBILE_PHONE_NO { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PERSON_ID")] public long? PERSON_ID { get; set; }
            [Column("PHONE_ID")] public long? PHONE_ID { get; set; }
            [Column("PHONE_NO")] public string PHONE_NO { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("TITLE")] public string TITLE { get; set; }

        }

        [Table("V_COMPANY_NOTE")]
        public class V_CompanyNote
        {
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Key][Column("COMPANY_NOTE_ID")] public long? COMPANY_NOTE_ID { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("NOTE_DESCRIPTION")] public string NOTE_DESCRIPTION { get; set; }
            [Column("NOTE_ID")] public long? NOTE_ID { get; set; }
            [Column("NOTE_TYPE")] public string NOTE_TYPE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_PACKAGE_TPA_PRICE")]
        public class V_PackageTpaPrice
        {
            [Column("AMOUNT")] public decimal? AMOUNT { get; set; }
            [Column("CASH_PAYMENT_TYPE")] public string CASH_PAYMENT_TYPE { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Key][Column("PACKAGE_TPA_PRICE_ID")] public long? PACKAGE_TPA_PRICE_ID { get; set; }
            [Column("PACKAGE_TPA_PRICE_TYPE")] public string PACKAGE_TPA_PRICE_TYPE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("START_DATE")] public DateTime? START_DATE { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_PACKAGE_PARAMETER")]
        public class V_PackageParameter
        {
            [Column("ENVIRONMENT")] public string ENVIRONMENT { get; set; }
            [Column("KEY")] public string KEY { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Column("PACKAGE_NAME")] public string PACKAGE_NAME { get; set; }
            [Column("PACKAGE_NO")] public string PACKAGE_NO { get; set; }
            [Key][Column("PACKAGE_PARAMETER_ID")] public long? PACKAGE_PARAMETER_ID { get; set; }
            [Column("PARAMETER_ID")] public long? PARAMETER_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SYSTEM_TYPE")] public string SYSTEM_TYPE { get; set; }
            [Column("VALUE")] public string VALUE { get; set; }

        }

        [Table("V_DOCTOR_BRANCH_PARAMETER")]
        public class V_DoctorBranchParameter
        {
            [Column("ENVIRONMENT")] public string ENVIRONMENT { get; set; }
            [Column("KEY")] public string KEY { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("DOCTOR_BRANCH_ID")] public long DOCTOR_BRANCH_ID { get; set; }
            [Key][Column("DOCTOR_BRANCH_PARAMETER_ID")] public long DOCTOR_BRANCH_PARAMETER_ID { get; set; }
            [Column("PARAMETER_ID")] public long PARAMETER_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SYSTEM_TYPE")] public string SYSTEM_TYPE { get; set; }
            [Column("VALUE")] public string VALUE { get; set; }

        }

        [Table("V_PACKAGE_NOTE")]
        public class V_PackageNote
        {
            [Column("DESCRIPTION")] public string DESCRIPTION { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("NOTE_ID")] public long? NOTE_ID { get; set; }
            [Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Key][Column("PACKAGE_NOTE_ID")] public long? PACKAGE_NOTE_ID { get; set; }
            [Column("PACKAGE_NOTE_TYPE")] public string PACKAGE_NOTE_TYPE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_PACKAGE_PLAN_COVERAGE_VAR")]
        public class V_PackagePlanCoverageVar
        {
            [Column("AGR_AGREEMENT_TYPE")] public string AGR_AGREEMENT_TYPE { get; set; }
            [Column("AGR_AGREEMENT_TYPE_CODE")] public string AGR_AGREEMENT_TYPE_CODE { get; set; }
            [Column("AGR_AGREEMENT_TYPE_TEXT")] public string AGR_AGREEMENT_TYPE_TEXT { get; set; }
            [Column("AGR_COINSURANCE_RATIO")] public long? AGR_COINSURANCE_RATIO { get; set; }
            [Column("AGR_CURRENCY_ID")] public string AGR_CURRENCY_ID { get; set; }
            [Column("AGR_CURRENCY_TYPE_CODE")] public string AGR_CURRENCY_TYPE_CODE { get; set; }
            [Column("AGR_CURRENCY_TYPE_TEXT")] public string AGR_CURRENCY_TYPE_TEXT { get; set; }
            [Column("AGR_DAY_LIMIT")] public long? AGR_DAY_LIMIT { get; set; }
            [Column("AGR_EXEMPTION_LIMIT")] public long? AGR_EXEMPTION_LIMIT { get; set; }
            [Column("AGR_EXEMPTION_TYPE")] public string AGR_EXEMPTION_TYPE { get; set; }
            [Column("AGR_EXEMPTION_TYPE_CODE")] public string AGR_EXEMPTION_TYPE_CODE { get; set; }
            [Column("AGR_EXEMPTION_TYPE_TEXT")] public string AGR_EXEMPTION_TYPE_TEXT { get; set; }
            [Column("AGR_ID")] public long? AGR_ID { get; set; }
            [Column("AGR_NEW_VERSION_ID")] public long? AGR_NEW_VERSION_ID { get; set; }
            [Column("AGR_NUMBER_LIMIT")] public long? AGR_NUMBER_LIMIT { get; set; }
            [Column("AGR_PRICE_FACTOR")] public long? AGR_PRICE_FACTOR { get; set; }
            [Column("AGR_PRICE_LIMIT")] public long? AGR_PRICE_LIMIT { get; set; }
            [Column("AGR_SESSION_COUNT")] public long? AGR_SESSION_COUNT { get; set; }
            [Column("AGR_SP_RESPONSE_ID")] public long? AGR_SP_RESPONSE_ID { get; set; }
            [Column("AGR_STATUS")] public string AGR_STATUS { get; set; }
            [Column("AGR_STATUS_CODE")] public string AGR_STATUS_CODE { get; set; }
            [Column("AGR_STATUS_TEXT")] public string AGR_STATUS_TEXT { get; set; }
            [Column("BRANCH_ID")] public long? BRANCH_ID { get; set; }
            [Column("BRANCH_NAME")] public string BRANCH_NAME { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("COVERAGE_ID")] public long? COVERAGE_ID { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Column("COVERAGE_TYPE")] public string COVERAGE_TYPE { get; set; }
            [Column("DSP_AGREEMENT_TYPE")] public string DSP_AGREEMENT_TYPE { get; set; }
            [Column("DSP_AGREEMENT_TYPE_CODE")] public string DSP_AGREEMENT_TYPE_CODE { get; set; }
            [Column("DSP_AGREEMENT_TYPE_TEXT")] public string DSP_AGREEMENT_TYPE_TEXT { get; set; }
            [Column("DSP_COINSURANCE_RATIO")] public long? DSP_COINSURANCE_RATIO { get; set; }
            [Column("DSP_CURRENCY_ID")] public string DSP_CURRENCY_ID { get; set; }
            [Column("DSP_CURRENCY_TYPE_CODE")] public string DSP_CURRENCY_TYPE_CODE { get; set; }
            [Column("DSP_CURRENCY_TYPE_TEXT")] public string DSP_CURRENCY_TYPE_TEXT { get; set; }
            [Column("DSP_DAY_LIMIT")] public long? DSP_DAY_LIMIT { get; set; }
            [Column("DSP_EXEMPTION_LIMIT")] public long? DSP_EXEMPTION_LIMIT { get; set; }
            [Column("DSP_EXEMPTION_TYPE")] public string DSP_EXEMPTION_TYPE { get; set; }
            [Column("DSP_EXEMPTION_TYPE_CODE")] public string DSP_EXEMPTION_TYPE_CODE { get; set; }
            [Column("DSP_EXEMPTION_TYPE_TEXT")] public string DSP_EXEMPTION_TYPE_TEXT { get; set; }
            [Column("DSP_ID")] public long? DSP_ID { get; set; }
            [Column("DSP_NEW_VERSION_ID")] public long? DSP_NEW_VERSION_ID { get; set; }
            [Column("DSP_NUMBER_LIMIT")] public long? DSP_NUMBER_LIMIT { get; set; }
            [Column("DSP_PRICE_FACTOR")] public long? DSP_PRICE_FACTOR { get; set; }
            [Column("DSP_PRICE_LIMIT")] public long? DSP_PRICE_LIMIT { get; set; }
            [Column("DSP_SESSION_COUNT")] public long? DSP_SESSION_COUNT { get; set; }
            [Column("DSP_SP_RESPONSE_ID")] public long? DSP_SP_RESPONSE_ID { get; set; }
            [Column("DSP_STATUS")] public string DSP_STATUS { get; set; }
            [Column("DSP_STATUS_CODE")] public string DSP_STATUS_CODE { get; set; }
            [Column("DSP_STATUS_TEXT")] public string DSP_STATUS_TEXT { get; set; }
            [Column("EMG_AGREEMENT_TYPE")] public string EMG_AGREEMENT_TYPE { get; set; }
            [Column("EMG_AGREEMENT_TYPE_CODE")] public string EMG_AGREEMENT_TYPE_CODE { get; set; }
            [Column("EMG_AGREEMENT_TYPE_TEXT")] public string EMG_AGREEMENT_TYPE_TEXT { get; set; }
            [Column("EMG_COINSURANCE_RATIO")] public long? EMG_COINSURANCE_RATIO { get; set; }
            [Column("EMG_CURRENCY_ID")] public string EMG_CURRENCY_ID { get; set; }
            [Column("EMG_CURRENCY_TYPE_CODE")] public string EMG_CURRENCY_TYPE_CODE { get; set; }
            [Column("EMG_CURRENCY_TYPE_TEXT")] public string EMG_CURRENCY_TYPE_TEXT { get; set; }
            [Column("EMG_DAY_LIMIT")] public long? EMG_DAY_LIMIT { get; set; }
            [Column("EMG_EXEMPTION_LIMIT")] public long? EMG_EXEMPTION_LIMIT { get; set; }
            [Column("EMG_EXEMPTION_TYPE")] public string EMG_EXEMPTION_TYPE { get; set; }
            [Column("EMG_EXEMPTION_TYPE_CODE")] public string EMG_EXEMPTION_TYPE_CODE { get; set; }
            [Column("EMG_EXEMPTION_TYPE_TEXT")] public string EMG_EXEMPTION_TYPE_TEXT { get; set; }
            [Column("EMG_ID")] public long? EMG_ID { get; set; }
            [Column("EMG_NEW_VERSION_ID")] public long? EMG_NEW_VERSION_ID { get; set; }
            [Column("EMG_NUMBER_LIMIT")] public long? EMG_NUMBER_LIMIT { get; set; }
            [Column("EMG_PRICE_FACTOR")] public long? EMG_PRICE_FACTOR { get; set; }
            [Column("EMG_PRICE_LIMIT")] public long? EMG_PRICE_LIMIT { get; set; }
            [Column("EMG_SESSION_COUNT")] public long? EMG_SESSION_COUNT { get; set; }
            [Column("EMG_SP_RESPONSE_ID")] public long? EMG_SP_RESPONSE_ID { get; set; }
            [Column("EMG_STATUS")] public string EMG_STATUS { get; set; }
            [Column("EMG_STATUS_CODE")] public string EMG_STATUS_CODE { get; set; }
            [Column("EMG_STATUS_TEXT")] public string EMG_STATUS_TEXT { get; set; }
            [Column("IS_MAIN_COVERAGE")] public long? IS_MAIN_COVERAGE { get; set; }
            [Column("MAIN_COVERAGE_ID")] public long? MAIN_COVERAGE_ID { get; set; }
            [Column("NAG_AGREEMENT_TYPE")] public string NAG_AGREEMENT_TYPE { get; set; }
            [Column("NAG_AGREEMENT_TYPE_CODE")] public string NAG_AGREEMENT_TYPE_CODE { get; set; }
            [Column("NAG_AGREEMENT_TYPE_TEXT")] public string NAG_AGREEMENT_TYPE_TEXT { get; set; }
            [Column("NAG_COINSURANCE_RATIO")] public long? NAG_COINSURANCE_RATIO { get; set; }
            [Column("NAG_CURRENCY_ID")] public string NAG_CURRENCY_ID { get; set; }
            [Column("NAG_CURRENCY_TYPE_CODE")] public string NAG_CURRENCY_TYPE_CODE { get; set; }
            [Column("NAG_CURRENCY_TYPE_TEXT")] public string NAG_CURRENCY_TYPE_TEXT { get; set; }
            [Column("NAG_DAY_LIMIT")] public long? NAG_DAY_LIMIT { get; set; }
            [Column("NAG_EXEMPTION_LIMIT")] public long? NAG_EXEMPTION_LIMIT { get; set; }
            [Column("NAG_EXEMPTION_TYPE")] public string NAG_EXEMPTION_TYPE { get; set; }
            [Column("NAG_EXEMPTION_TYPE_CODE")] public string NAG_EXEMPTION_TYPE_CODE { get; set; }
            [Column("NAG_EXEMPTION_TYPE_TEXT")] public string NAG_EXEMPTION_TYPE_TEXT { get; set; }
            [Column("NAG_ID")] public long? NAG_ID { get; set; }
            [Column("NAG_NEW_VERSION_ID")] public long? NAG_NEW_VERSION_ID { get; set; }
            [Column("NAG_NUMBER_LIMIT")] public long? NAG_NUMBER_LIMIT { get; set; }
            [Column("NAG_PRICE_FACTOR")] public long? NAG_PRICE_FACTOR { get; set; }
            [Column("NAG_PRICE_LIMIT")] public long? NAG_PRICE_LIMIT { get; set; }
            [Column("NAG_SESSION_COUNT")] public long? NAG_SESSION_COUNT { get; set; }
            [Column("NAG_SP_RESPONSE_ID")] public long? NAG_SP_RESPONSE_ID { get; set; }
            [Column("NAG_STATUS")] public string NAG_STATUS { get; set; }
            [Column("NAG_STATUS_CODE")] public string NAG_STATUS_CODE { get; set; }
            [Column("NAG_STATUS_TEXT")] public string NAG_STATUS_TEXT { get; set; }
            [Column("NETWORK_ID")] public long? NETWORK_ID { get; set; }
            [Column("NETWORK_NAME")] public string NETWORK_NAME { get; set; }
            [Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Column("PACKAGE_NAME")] public string PACKAGE_NAME { get; set; }
            [Column("PACKAGE_NO")] public string PACKAGE_NO { get; set; }
            [Column("PACKAGE_TYPE")] public string PACKAGE_TYPE { get; set; }
            [Key][Column("PLAN_COVERAGE_ID")] public long? PLAN_COVERAGE_ID { get; set; }
            [Column("PLAN_ID")] public long? PLAN_ID { get; set; }
            [Column("PLAN_NAME")] public string PLAN_NAME { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("SUBPRODUCT_CODE")] public string SUBPRODUCT_CODE { get; set; }
            [Column("SUBPRODUCT_ID")] public long? SUBPRODUCT_ID { get; set; }

        }

        [Table("V_PACKAGE_PLAN_COVERAGE")]
        public class V_PackagePlanCoverage
        {
            [Column("BRANCH_ID")] public long? BRANCH_ID { get; set; }
            [Column("BRANCH_NAME")] public string BRANCH_NAME { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("COVERAGE_ID")] public long? COVERAGE_ID { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Column("COVERAGE_TYPE")] public string COVERAGE_TYPE { get; set; }
            [Column("IS_MAIN_COVERAGE")] public long? IS_MAIN_COVERAGE { get; set; }
            [Column("MAIN_COVERAGE_ID")] public long? MAIN_COVERAGE_ID { get; set; }
            [Column("NETWORK_ID")] public long? NETWORK_ID { get; set; }
            [Column("NETWORK_NAME")] public string NETWORK_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Key][Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Column("PACKAGE_NAME")] public string PACKAGE_NAME { get; set; }
            [Column("PACKAGE_NO")] public string PACKAGE_NO { get; set; }
            [Column("PACKAGE_TYPE")] public string PACKAGE_TYPE { get; set; }
            [Column("PLAN_COVERAGE_ID")] public long? PLAN_COVERAGE_ID { get; set; }
            [Column("PLAN_ID")] public long? PLAN_ID { get; set; }
            [Column("PLAN_NAME")] public string PLAN_NAME { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SUBPRODUCT_CODE")] public string SUBPRODUCT_CODE { get; set; }
            [Column("SUBPRODUCT_ID")] public long? SUBPRODUCT_ID { get; set; }
            [Column("SUBPRODUCT_NAME")] public string SUBPRODUCT_NAME { get; set; }



        }

        [Table("V_PACKAGE_CLAIM_PROCESS")]
        public class V_PackageClaimProcess
        {
            [Column("COVERAGE_ID")] public long? COVERAGE_ID { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Column("COVERAGE_TYPE")] public string COVERAGE_TYPE { get; set; }
            [Key][Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Column("PACKAGE_NAME")] public string PACKAGE_NAME { get; set; }
            [Column("PACKAGE_NO")] public string PACKAGE_NO { get; set; }
            [Column("PROCESS_ID")] public long? PROCESS_ID { get; set; }

        }

        [Table("V_PACKAGE_COVERAGE_LIST")]
        public class V_PackageCoverageList
        {
            [Column("COVERAGE_ID")] public long? COVERAGE_ID { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Column("COVERAGE_TYPE")] public string COVERAGE_TYPE { get; set; }
            [Column("ID")] public long? ID { get; set; }
            [Column("MAIN_COVERAGE_ID")] public long? MAIN_COVERAGE_ID { get; set; }
            [Column("MAIN_COVERAGE_NAME")] public string MAIN_COVERAGE_NAME { get; set; }
            [Column("MAIN_COVERAGE_TYPE")] public string MAIN_COVERAGE_TYPE { get; set; }
            [Key][Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Column("PARENT_ID")] public long? PARENT_ID { get; set; }

        }

        [Table("V_PACKAGE_PRICE")]
        public class V_PackagePrice
        {
            [Column("PACKAGE_NO")]
            public string PACKAGE_NO { get; set; }
            [Column("INDIVIDUAL_TYPE")]
            public string INDIVIDUAL_TYPE { get; set; }
            [Column("PRICE_ID")]
            public long? PRICE_ID { get; set; }
            [Column("NEW_VERSION_ID")]
            public long? NEW_VERSION_ID { get; set; }
            [Column("STATUS")]
            public string STATUS { get; set; }
            [Column("AGE_END")]
            public long? AGE_END { get; set; }
            [Column("PRICE_LIST_ID")]
            public long? PRICE_LIST_ID { get; set; }
            [Column("VALID_FROM")]
            public DateTime? VALID_FROM { get; set; }
            [Key]
            [Column("PACKAGE_ID")]
            public long PACKAGE_ID { get; set; }
            [Column("SP_RESPONSE_ID")]
            public long? SP_RESPONSE_ID { get; set; }
            [Column("DAY_COUNT")]
            public long? DAY_COUNT { get; set; }
            [Column("GENDER")]
            public string GENDER { get; set; }
            [Column("PACKAGE_NAME")]
            public string PACKAGE_NAME { get; set; }
            [Column("PREMIUM")]
            public decimal? PREMIUM { get; set; }
            [Column("VALID_TO")]
            public DateTime? VALID_TO { get; set; }
            [Column("AGE_START")]
            public long? AGE_START { get; set; }

        }

        [Table("V_EXPORT_PACKAGE")]
        public class V_ExportPackage
        {
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("BRANCH_ID")]
            public Int64 BranchId { get; set; }
            [Column("BRANCH_NAME")]
            public string BranchName { get; set; }
            [Column("COMPANY_ID")]
            public Int64 CompanyId { get; set; }
            [Column("COMPANY_NAME")]
            public string CompanyName { get; set; }
            [Column("COVERAGE_ID")]
            public Int64 CoverageId { get; set; }
            [Column("COVERAGE_TYPE")]
            public string CoverageType { get; set; }
            [Column("IS_MAIN_COVERAGE")]
            public int IsMainCoverage { get; set; }
            [Column("MAIN_COVERAGE_ID")]
            public Int64 MainCoverageId { get; set; }
            [Column("NETWORK_ID")]
            public Int64 NetworkId { get; set; }
            [Column("NETWORK_NAME")]
            public string NetworkName { get; set; }
            [Column("NEW_VERSION_ID")]
            public string NEW_VERSION_ID { get; set; }
            [Column("PACKAGE_ID")]
            public Int64 PackageId { get; set; }
            [Column("PACKAGE_NAME")]
            public string PackageName { get; set; }
            [Column("PACKAGE_NO")]
            public string PackageNo { get; set; }
            [Column("PACKAGE_TYPE")]
            public string PackageType { get; set; }
            [Column("PLAN_COVERAGE_ID")]
            public Int64 PlanCoverageId { get; set; }
            [Column("PLAN_ID")]
            public Int64 PlanId { get; set; }
            [Column("PLAN_NAME")]
            public string PlanName { get; set; }
            [Column("PRODUCT_CODE")]
            public string ProductCode { get; set; }
            [Column("PRODUCT_ID")]
            public Int64 ProductId { get; set; }
            [Column("SAGMER_PACKAGE_ID")]
            public Int64 SagmerPackageId { get; set; }
            [Column("SAGMER_PACKAGE_NAME")]
            public string SagmerPackageName { get; set; }
            [Column("SAGMER_PLAN_ID")]
            public Int64 SagmerPlanId { get; set; }
            [Column("SAGMER_PLAN_NAME")]
            public string SagmerPlanName { get; set; }
            [Column("SAGMER_REGISTRY_DATE")]
            public string SagmerRegistryDate { get; set; }
            [Column("SBM_NO")]
            public string SbmNo { get; set; }
            [Column("SP_RESPONSE_ID")]
            public string SP_RESPONSE_ID { get; set; }
            [Column("STATUS")]
            public string STATUS { get; set; }
            [Column("SUBPRODUCT_CODE")]
            public string SubProductCode { get; set; }
            [Column("SUBPRODUCT_ID")]
            public Int64 SubProductId { get; set; }
        }
        [Table("V_EXPORT_PROVIDER")]
        public class V_ExportProvider
        {
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("REVISION_DATE")]
            public string REVISION_DATE { get; set; }
            [Column("IS_E_BILL")]
            public string IS_E_BILL { get; set; }
            [Column("CORPORATE_ID")]
            public string CORPORATE_ID { get; set; }
            [Column("CORPORATE_TYPE")]
            public string CORPORATE_TYPE { get; set; }
            [Column("ZIP_CODE")]
            public string ZIP_CODE { get; set; }
            [Column("ADDRESS")]
            public string ADDRESS { get; set; }
            [Column("PROVIDER_GROUP_NAME")]
            public string PROVIDER_GROUP_NAME { get; set; }
            [Column("PROVIDER_GROUP_ID")]
            public string PROVIDER_GROUP_ID { get; set; }
            [Column("STOPPAGE_OVERWRITE")]
            public string STOPPAGE_OVERWRITE { get; set; }
            [Column("OFFICIAL_CODE")]
            public string OFFICIAL_CODE { get; set; }
            [Column("ADDRESS_ID")]
            public string ADDRESS_ID { get; set; }
            [Column("CONTACT_ID")]
            public string CONTACT_ID { get; set; }
            [Column("SP_RESPONSE_ID")]
            public string SP_RESPONSE_ID { get; set; }
            [Column("NEW_VERSION_ID")]
            public string NEW_VERSION_ID { get; set; }
            [Column("STATUS")]
            public string STATUS { get; set; }
            [Column("MDP_CONTRACT_ID")]
            public string MDP_CONTRACT_ID { get; set; }
            [Column("DOGUM_CONTRACT_ID")]
            public string DOGUM_CONTRACT_ID { get; set; }
            [Column("OSS_CONTRACT_ID")]
            public string OSS_CONTRACT_ID { get; set; }
            [Column("TSS_CONTRACT_ID")]
            public string TSS_CONTRACT_ID { get; set; }
            [Column("COUNTY_NAME")]
            public string COUNTY_NAME { get; set; }
            [Column("COUNTY_CODE")]
            public string COUNTY_CODE { get; set; }
            [Column("COUNTY_ID")]
            public string COUNTY_ID { get; set; }
            [Column("CITY_NAME")]
            public string CITY_NAME { get; set; }
            [Column("CITY_ID")]
            public string CITY_ID { get; set; }
            [Column("COUNTRY_NAME")]
            public string COUNTRY_NAME { get; set; }
            [Column("COUNTRY_CODE")]
            public string COUNTRY_CODE { get; set; }
            [Column("PROVIDER_TYPE")]
            public string PROVIDER_TYPE { get; set; }
            [Column("TAX_OFFICE")]
            public string TAX_OFFICE { get; set; }
            [Column("TAX_NUMBER")]
            public string TAX_NUMBER { get; set; }
            [Column("PROVIDER_TITLE")]
            public string PROVIDER_TITLE { get; set; }
            [Column("PROVIDER_NAME")]
            public string PROVIDER_NAME { get; set; }
            [Column("PROVIDER_ID")]
            public string PROVIDER_ID { get; set; }
        }
        [Table("T_SESSION")]
        public class TSession
        {
            [Required]
            [Column("ID")]
            public Int64? Id { get; set; }
            [Required]
            [Column("TOKEN")]
            [Display(Name = "TOKEN", Description = "Token")]
            public string Token { get; set; }
            [Required]
            [Column("USER_ID")]
            [Display(Name = "USER ID", Description = "User Id")]
            public long? UserID { get; set; }
            [Column("IP")]
            public string Ip { get; set; }
            [Required]
            [Column("START_DATE")]
            [Display(Name = "START DATE", Description = "Başlangıç Tarihi")]
            public DateTime StartDate { get; set; }
            [Column("EXPIRED_DATE")]
            public DateTime? ExpiredDate { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            public string Status { get; set; }
        }
        [Table("T_EXCHANGE_RATE")]
        public class ExchangeRate
        {
            [Required]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("RATE_DATE")]
            public DateTime? RateDate { get; set; }
            [Column("CURRENCY_TYPE")]
            public string CurrencyType { get; set; }
            [Column("UNIT")]
            public string Unit { get; set; }
            [Column("FOREX_BUYING")]
            public Decimal? ForexBuying { get; set; }
            [Column("FOREX_SELLING")]
            public Decimal? ForexSelling { get; set; }
            [Column("BANKNOTE_BUYING")]
            public Decimal? BanknoteBuying { get; set; }
            [Column("BANKNOTE_SELLING")]
            public Decimal? BanknoteSelling { get; set; }
            [Column("STATUS")]
            [Display(Name = "STATUS", Description = "Statüs")]
            [MaxLength(3, ErrorMessage = "")]
            public string Status { get; set; }
        }
        #endregion
        [Table("T_WS_QUEUE")]
        public class WsQueue
        {
            [Column("REQ_ID")]
            public string ReqId { get; set; }
            [Column("COMPANY_ID")]
            public Int64 CompanyId { get; set; }
            [Column("REQUEST_DATE")]
            public DateTime? RequestDate { get; set; }
            [Column("RESPONSE_DATE")]
            public DateTime? ResponseDate { get; set; }
            [Column("REQUEST_XML")]
            public string RequestXml { get; set; }
            [Column("RESPONSE_XML")]
            public string ResponseXml { get; set; }
            [Column("OBJECT_TYPE")]
            public string ObjectType { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("STATUS")]
            public string Status { get; set; }

        }
        [Table("T_INTEGRATION_POLICY")]
        public class IntegrationPolicy
        {
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("CORRELATION_ID")]
            public string CorrelationId { get; set; }
            [Column("POLICY_ID")]
            public Int64 PolicyID { get; set; }
            [Column("ENDORSEMENT_ID")]
            public Int64 EndorsementId { get; set; }
            [Column("REQID")]
            public Int64 ReqId { get; set; }
            [Column("REQUEST_DATE")]
            public DateTime? RequestDate { get; set; }
            [Column("RESPONSE_DATE")]
            public DateTime? ResponseDate { get; set; }
            [Column("SP_RESPONSE_ID")]
            public Int64? SpResponseId { get; set; }
            [Column("STATUS")]
            public string Status { get; set; }
        }
        [Table("T_WSAGREEMENT")]
        public class Agreement
        {
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("CORRELATION_ID")]
            public string CorrelationId { get; set; }
            [Column("COMPANY_ID")]
            public Int64 CompanyId { get; set; }
            [Column("AGREEMENT_DATE")]
            public DateTime AgreementDate { get; set; }
            [Column("DESCRIPTION")]
            public string Description { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("SP_RESPONSE_ID")]
            public Int64? SpResponseId { get; set; }
            [Column("STATUS")]
            public string Status { get; set; }
            [Column("TYPE")]
            public string Type { get; set; }

            public string CompanyName  = "";
            public string TypeText  = "";
            public string StatusText  = "";


        }
        [Table("V_COMPANY_PARAMETER")]
        public class V_CompanyParameter
        {
            
            [Column("COMPANY_ID")]
            public Int64 CompanyId { get; set; }
            [Column("COMPANY_NAME")]
            public string CompanyName { get; set; }
            [Key]
            [Column("COMPANY_PARAMETER_ID")]
            public Int64? CompanyParameterId { get; set; }
            [Column("ENVIRONMENT")]
            public string Environment { get; set; }
            [Column("KEY")]
            public string Key { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("PARAMETER_ID")]
            public Int64? ParameterId { get; set; }
            [Column("SP_RESPONSE_ID")]
            public Int64? SpResponseId { get; set; }
            [Column("STATUS")]
            public string Status { get; set; }
            [Column("SYSTEM_TYPE")]
            public string SystemType { get; set; }
            [Column("VALUE")]
            public string Value { get; set; }
        }

        [Table("V_INSURED_NOTE")]
        public class V_InsuredNote
        {
            [Column("DESCRIPTION")] public string DESCRIPTION { get; set; }
            [Column("INSURED_ID")] public long? INSURED_ID { get; set; }
            [Key][Column("INSURED_NOTE_ID")] public long? INSURED_NOTE_ID { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("NOTE_ID")] public long? NOTE_ID { get; set; }
            [Column("NOTE_TYPE")] public string NOTE_TYPE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_INSURED_CLAIM")]
        public class V_InsuredClaim
        {
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_INSURED_NO")] public string COMPANY_INSURED_NO { get; set; }
            [Column("CONTACT_ID")] public long CONTACT_ID { get; set; }
            [Column("FIRST_INSURED_DATE")] public DateTime? FIRST_INSURED_DATE { get; set; }
            [Column("FIRST_NAME")] public string FIRST_NAME { get; set; }
            [Column("IDENTITY_NO")] public Int64? IDENTITY_NO { get; set; }
            [Key][Column("INSURED_ID")] public long INSURED_ID { get; set; }
            [Column("LAST_NAME")] public string LAST_NAME { get; set; }
            [Column("PERSON_ID")] public long PERSON_ID { get; set; }
            [Column("POLICY_END_DATE")] public DateTime? POLICY_END_DATE { get; set; }
            [Column("POLICY_GROUP_ID")] public long? POLICY_GROUP_ID { get; set; }
            [Column("POLICY_ID")] public long POLICY_ID { get; set; }
            [Column("POLICY_ISSUE_DATE")] public DateTime? POLICY_ISSUE_DATE { get; set; }
            [Column("POLICY_NUMBER")] public string POLICY_NUMBER { get; set; }
            [Column("POLICY_START_DATE")] public DateTime? POLICY_START_DATE { get; set; }
            [Column("POLICY_TYPE")] public string POLICY_TYPE { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_ID")] public long PRODUCT_ID { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("PRODUCT_TYPE")] public string PRODUCT_TYPE { get; set; }
            [Column("PACKAGE_TYPE")] public string PACKAGE_TYPE { get; set; }
            [Column("RENEWAL_NO")] public int RENEWAL_NO { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
        }

        [Table("V_INSURED")]
        public class V_Insured
        {
            //[Column("ADDRESS_ID")] public long? ADDRESS_ID { get; set; }
            //[Column("ADDRESS_TYPE")] public string ADDRESS_TYPE { get; set; }
            [Column("AGENCY_ID")] public long? AGENCY_ID { get; set; }
            [Column("AGENCY_NAME")] public string AGENCY_NAME { get; set; }
            [Column("AGENCY_NO")] public string AGENCY_NO { get; set; }
            [Column("BIRTHDATE")] public DateTime? BIRTHDATE { get; set; }
            [Column("BIRTHPLACE")] public string BIRTHPLACE { get; set; }
            [Column("BIRTH_COVERAGE_DATE")] public DateTime? BIRTH_COVERAGE_DATE { get; set; }
            //[Column("CITY_ID")] public long? CITY_ID { get; set; }
            //[Column("CITY_NAME")] public string CITY_NAME { get; set; }
            [Column("COMPANY_ENTRANCE_DATE")] public DateTime? COMPANY_ENTRANCE_DATE { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("CONTACT_ID")] public long CONTACT_ID { get; set; }
            //[Column("COUNTRY_ID")] public long? COUNTRY_ID { get; set; }
            //[Column("COUNTRY_NAME")] public string COUNTRY_NAME { get; set; }
            //[Column("COUNTY_ID")] public long? COUNTY_ID { get; set; }
            //[Column("COUNTY_NAME")] public string COUNTY_NAME { get; set; }
            [Column("CURRENCY_TYPE")] public string CURRENCY_TYPE { get; set; }
            //[Column("DETAILS")] public string DETAILS { get; set; }
            [Column("ENDORSEMENT_END_DATE")] public DateTime? ENDORSEMENT_END_DATE { get; set; }
            [Column("ENDORSEMENT_ID")] public long? ENDORSEMENT_ID { get; set; }
            [Column("ENDORSEMENT_ISSUE_DATE")] public DateTime? ENDORSEMENT_ISSUE_DATE { get; set; }
            [Column("ENDORSEMENT_NO")] public long? ENDORSEMENT_NO { get; set; }
            [Column("ENDORSEMENT_START_DATE")] public DateTime? ENDORSEMENT_START_DATE { get; set; }
            [Column("ENDORSEMENT_TYPE")] public string ENDORSEMENT_TYPE { get; set; }
            [Column("ENTRANCE_DATE")] public DateTime? ENTRANCE_DATE { get; set; }
            [Column("EXCHANGE_RATE_ID")] public long? EXCHANGE_RATE_ID { get; set; }
            [Column("FAMILY_NO")] public long? FAMILY_NO { get; set; }
            [Column("FIRST_AMBULANT_COVERAGE_DATE")] public DateTime? FIRST_AMBULANT_COVERAGE_DATE { get; set; }
            [Column("FIRST_INSURED_DATE")] public DateTime? FIRST_INSURED_DATE { get; set; }
            [Column("INSURED_CANCELED_DATE")] public DateTime? INSURED_CANCELED_DATE { get; set; }
            [Column("FIRST_NAME")] public string FIRST_NAME { get; set; }
            [Column("FOREX_SELLING")] public long? FOREX_SELLING { get; set; }
            [Column("GENDER")] public string GENDER { get; set; }
            [Column("IDENTITY_NO")] public Int64? IDENTITY_NO { get; set; }
            [Column("INDIVIDUAL_TYPE")] public string INDIVIDUAL_TYPE { get; set; }
            //[Column("INSURED_ADDRESS_ID")] public long? INSURED_ADDRESS_ID { get; set; }
            [Key] [Column("INSURED_ID")] public long INSURED_ID { get; set; }
            [Column("INSURED_TRANSFER_COMPANY_NAME")] public string INSURED_TRANSFER_COMPANY_NAME { get; set; }
            [Column("INSURED_TRANSFER_ID")] public long? INSURED_TRANSFER_ID { get; set; }
            [Column("INSURED_TRANSFER_TYPE")] public string INSURED_TRANSFER_TYPE { get; set; }
            [Column("INSURER")] public long? INSURER { get; set; }
            [Column("INSURER_NAME")] public string INSURER_NAME { get; set; }
            [Column("IS_OPEN_TO_CLAIM")] public string IS_OPEN_TO_CLAIM { get; set; }
            [Column("IS_VIP")] public string IS_VIP { get; set; }
            [Column("LAST_NAME")] public string LAST_NAME { get; set; }
            [Column("LICENSE_NO")] public string LICENSE_NO { get; set; }
            [Column("MARITAL_STATUS")] public string MARITAL_STATUS { get; set; }
            [Column("MIDDLE_NAME")] public string MIDDLE_NAME { get; set; }
            [Column("NAME_OF_FATHER")] public string NAME_OF_FATHER { get; set; }
            [Column("NATIONALITY_ID")] public long? NATIONALITY_ID { get; set; }
            [Column("NATIONALITY_NAME")] public string NATIONALITY_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Column("PACKAGE_NAME")] public string PACKAGE_NAME { get; set; }
            [Column("PACKAGE_NO")] public string PACKAGE_NO { get; set; }
            [Column("PARENT_ID")] public long? PARENT_ID { get; set; }
            [Column("PARENT_POLICY_ID")] public long? PARENT_POLICY_ID { get; set; }
            [Column("PASSPORT_NO")] public string PASSPORT_NO { get; set; }
            [Column("PERSON_ID")] public long? PERSON_ID { get; set; }
            [Column("POLICY_END_DATE")] public DateTime? POLICY_END_DATE { get; set; }
            [Column("POLICY_GROUP_ID")] public long? POLICY_GROUP_ID { get; set; }
            [Column("POLICY_ID")] public long? POLICY_ID { get; set; }
            [Column("POLICY_ISSUE_DATE")] public DateTime? POLICY_ISSUE_DATE { get; set; }
            [Column("POLICY_NUMBER")] public Int64? POLICY_NUMBER { get; set; }
            [Column("POLICY_START_DATE")] public DateTime? POLICY_START_DATE { get; set; }
            [Column("POLICY_STATUS")] public string POLICY_STATUS { get; set; }
            [Column("POLICY_TYPE")] public string POLICY_TYPE { get; set; }
            [Column("PREMIUM")] public decimal? PREMIUM { get; set; }
            [Column("TOTAL_PREMIUM")] public decimal? TOTAL_PREMIUM { get; set; }
            [Column("PRICE_ID")] public long? PRICE_ID { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("PRODUCT_TYPE")] public string PRODUCT_TYPE { get; set; }
            [Column("REGISTRATION_DATE")] public DateTime? REGISTRATION_DATE { get; set; }
            [Column("REGISTRATION_NO")] public string REGISTRATION_NO { get; set; }
            [Column("RENEWAL_DATE")] public DateTime? RENEWAL_DATE { get; set; }
            [Column("RENEWAL_GUARANTEE_TEXT")] public string RENEWAL_GUARANTEE_TEXT { get; set; }
            [Column("RENEWAL_GUARANTEE_TYPE")] public string RENEWAL_GUARANTEE_TYPE { get; set; }
            [Column("RENEWAL_NO")] public long? RENEWAL_NO { get; set; }
            [Column("SBM_NO")] public string SBM_NO { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SUBPRODUCT_CODE")] public string SUBPRODUCT_CODE { get; set; }
            [Column("SUBPRODUCT_ID")] public long? SUBPRODUCT_ID { get; set; }
            [Column("SUBPRODUCT_NAME")] public string SUBPRODUCT_NAME { get; set; }
            [Column("TAX")] public long? TAX { get; set; }
            [Column("TAX_NUMBER")] public Int64? TAX_NUMBER { get; set; }
            [Column("TAX_OFFICE")] public string TAX_OFFICE { get; set; }
            [Column("VIP_TYPE")] public string VIP_TYPE { get; set; }
            //[Column("ZIP_CODE")] public string ZIP_CODE { get; set; }
            [Column("COMPANY_INSURED_NO")] public string CompanyInsuredNo { get; set; }
        }

        [Table("V_PORTFOY")]
        public class V_Portfoy
        {
            [Column("BIRTHDATE")] public DateTime? BIRTHDATE { get; set; }
            [Column("BIRTHPLACE")] public string BIRTHPLACE { get; set; }
            [Key] [Column("CONTACT_ID")] public long? CONTACT_ID { get; set; }
            [Column("CONTACT_TITLE")] public string CONTACT_TITLE { get; set; }
            [Column("CONTACT_TYPE")] public string CONTACT_TYPE { get; set; }
            [Column("CORPORATE_ID")] public long? CORPORATE_ID { get; set; }
            [Column("CORP_NAME")] public string CORP_NAME { get; set; }
            [Column("CORP_TYPE")] public string CORP_TYPE { get; set; }
            [Column("FIRST_NAME")] public string FIRST_NAME { get; set; }
            [Column("GENDER")] public string GENDER { get; set; }
            [Column("IDENTITY_NO")] public Int64? IDENTITY_NO { get; set; }
            [Column("IS_VIP")] public string IS_VIP { get; set; }
            [Column("LAST_NAME")] public string LAST_NAME { get; set; }
            [Column("LICENSE_NO")] public string LICENSE_NO { get; set; }
            [Column("MARITAL_STATUS")] public string MARITAL_STATUS { get; set; }
            [Column("MIDDLE_NAME")] public string MIDDLE_NAME { get; set; }
            [Column("NAME_OF_FATHER")] public string NAME_OF_FATHER { get; set; }
            [Column("NATIONALITY_ID")] public long? NATIONALITY_ID { get; set; }
            [Column("NATIONALITY_NAME")] public string NATIONALITY_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public string NEW_VERSION_ID { get; set; }
            [Column("PASSPORT_NO")] public string PASSPORT_NO { get; set; }
            [Column("PERSON_ID")] public long? PERSON_ID { get; set; }
            [Column("PHOTO")] public string PHOTO { get; set; }
            [Column("SP_RESPONSE_ID")] public string SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("TAX_NUMBER")] public Int64? TAX_NUMBER { get; set; }
            [Column("TAX_OFFICE")] public string TAX_OFFICE { get; set; }
            [Column("VIP_TEXT")] public string VIP_TEXT { get; set; }
            [Column("VIP_TYPE")] public string VIP_TYPE { get; set; }

        }

        [Table("V_INSURED_PACKAGE")]
        public class V_InsuredPackage
        {
            [Column("AGR_AGREEMENT_TYPE")] public string AGR_AGREEMENT_TYPE { get; set; }
            [Column("AGR_AGREEMENT_TYPE_CODE")] public string AGR_AGREEMENT_TYPE_CODE { get; set; }
            [Column("AGR_AGREEMENT_TYPE_TEXT")] public string AGR_AGREEMENT_TYPE_TEXT { get; set; }
            [Column("AGR_COINSURANCE_RATIO")] public decimal? AGR_COINSURANCE_RATIO { get; set; }
            [Column("AGR_CURRENCY_ID")] public string AGR_CURRENCY_ID { get; set; }
            [Column("AGR_CURRENCY_TYPE_CODE")] public string AGR_CURRENCY_TYPE_CODE { get; set; }
            [Column("AGR_CURRENCY_TYPE_TEXT")] public string AGR_CURRENCY_TYPE_TEXT { get; set; }
            [Column("AGR_DAY_LIMIT")] public decimal? AGR_DAY_LIMIT { get; set; }
            [Column("AGR_EXEMPTION_LIMIT")] public decimal? AGR_EXEMPTION_LIMIT { get; set; }
            [Column("AGR_EXEMPTION_TYPE")] public string AGR_EXEMPTION_TYPE { get; set; }
            [Column("AGR_EXEMPTION_TYPE_CODE")] public string AGR_EXEMPTION_TYPE_CODE { get; set; }
            [Column("AGR_EXEMPTION_TYPE_TEXT")] public string AGR_EXEMPTION_TYPE_TEXT { get; set; }
            [Column("AGR_NUMBER_LIMIT")] public decimal? AGR_NUMBER_LIMIT { get; set; }
            [Column("AGR_PRICE_FACTOR")] public decimal? AGR_PRICE_FACTOR { get; set; }
            [Column("AGR_PRICE_LIMIT")] public decimal? AGR_PRICE_LIMIT { get; set; }
            [Column("AGR_SESSION_COUNT")] public decimal? AGR_SESSION_COUNT { get; set; }
            [Column("BIRTHDATE")] public DateTime? BIRTHDATE { get; set; }
            [Column("BRANCH_ID")] public long? BRANCH_ID { get; set; }
            [Column("BRANCH_NAME")] public string BRANCH_NAME { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("COVERAGE_ID")] public long? COVERAGE_ID { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Column("COVERAGE_TYPE")] public string COVERAGE_TYPE { get; set; }
            [Column("DSP_AGREEMENT_TYPE")] public string DSP_AGREEMENT_TYPE { get; set; }
            [Column("DSP_AGREEMENT_TYPE_CODE")] public string DSP_AGREEMENT_TYPE_CODE { get; set; }
            [Column("DSP_AGREEMENT_TYPE_TEXT")] public string DSP_AGREEMENT_TYPE_TEXT { get; set; }
            [Column("DSP_COINSURANCE_RATIO")] public decimal? DSP_COINSURANCE_RATIO { get; set; }
            [Column("DSP_CURRENCY_ID")] public string DSP_CURRENCY_ID { get; set; }
            [Column("DSP_CURRENCY_TYPE_CODE")] public string DSP_CURRENCY_TYPE_CODE { get; set; }
            [Column("DSP_CURRENCY_TYPE_TEXT")] public string DSP_CURRENCY_TYPE_TEXT { get; set; }
            [Column("DSP_DAY_LIMIT")] public decimal? DSP_DAY_LIMIT { get; set; }
            [Column("DSP_EXEMPTION_LIMIT")] public decimal? DSP_EXEMPTION_LIMIT { get; set; }
            [Column("DSP_EXEMPTION_TYPE")] public string DSP_EXEMPTION_TYPE { get; set; }
            [Column("DSP_EXEMPTION_TYPE_CODE")] public string DSP_EXEMPTION_TYPE_CODE { get; set; }
            [Column("DSP_EXEMPTION_TYPE_TEXT")] public string DSP_EXEMPTION_TYPE_TEXT { get; set; }
            [Column("DSP_NUMBER_LIMIT")] public decimal? DSP_NUMBER_LIMIT { get; set; }
            [Column("DSP_PRICE_FACTOR")] public decimal? DSP_PRICE_FACTOR { get; set; }
            [Column("DSP_PRICE_LIMIT")] public decimal? DSP_PRICE_LIMIT { get; set; }
            [Column("DSP_SESSION_COUNT")] public decimal? DSP_SESSION_COUNT { get; set; }
            [Column("EMG_AGREEMENT_TYPE")] public string EMG_AGREEMENT_TYPE { get; set; }
            [Column("EMG_AGREEMENT_TYPE_CODE")] public string EMG_AGREEMENT_TYPE_CODE { get; set; }
            [Column("EMG_AGREEMENT_TYPE_TEXT")] public string EMG_AGREEMENT_TYPE_TEXT { get; set; }
            [Column("EMG_COINSURANCE_RATIO")] public decimal? EMG_COINSURANCE_RATIO { get; set; }
            [Column("EMG_CURRENCY_ID")] public string EMG_CURRENCY_ID { get; set; }
            [Column("EMG_CURRENCY_TYPE_CODE")] public string EMG_CURRENCY_TYPE_CODE { get; set; }
            [Column("EMG_CURRENCY_TYPE_TEXT")] public string EMG_CURRENCY_TYPE_TEXT { get; set; }
            [Column("EMG_DAY_LIMIT")] public decimal? EMG_DAY_LIMIT { get; set; }
            [Column("EMG_EXEMPTION_LIMIT")] public decimal? EMG_EXEMPTION_LIMIT { get; set; }
            [Column("EMG_EXEMPTION_TYPE")] public string EMG_EXEMPTION_TYPE { get; set; }
            [Column("EMG_EXEMPTION_TYPE_CODE")] public string EMG_EXEMPTION_TYPE_CODE { get; set; }
            [Column("EMG_EXEMPTION_TYPE_TEXT")] public string EMG_EXEMPTION_TYPE_TEXT { get; set; }
            [Column("EMG_NUMBER_LIMIT")] public decimal? EMG_NUMBER_LIMIT { get; set; }
            [Column("EMG_PRICE_FACTOR")] public decimal? EMG_PRICE_FACTOR { get; set; }
            [Column("EMG_PRICE_LIMIT")] public decimal? EMG_PRICE_LIMIT { get; set; }
            [Column("EMG_SESSION_COUNT")] public decimal? EMG_SESSION_COUNT { get; set; }
            [Column("FAMILY_NO")] public long? FAMILY_NO { get; set; }
            [Column("FIRST_NAME")] public string FIRST_NAME { get; set; }
            [Column("GENDER")] public string GENDER { get; set; }
            [Column("IDENTITY_NO")] public Int64? IDENTITY_NO { get; set; }
            [Key][Column("INSURED_ID")] public long? INSURED_ID { get; set; }
            [Column("IS_MAIN_COVERAGE")] public int? IS_MAIN_COVERAGE { get; set; }
            [Column("LAST_NAME")] public string LAST_NAME { get; set; }
            [Column("MAIN_COVERAGE_ID")] public long? MAIN_COVERAGE_ID { get; set; }
            [Column("MAIN_COVERAGE_NAME")] public string MAIN_COVERAGE_NAME { get; set; }
            [Column("MIDDLE_NAME")] public string MIDDLE_NAME { get; set; }
            [Column("NAG_AGREEMENT_TYPE")] public string NAG_AGREEMENT_TYPE { get; set; }
            [Column("NAG_AGREEMENT_TYPE_CODE")] public string NAG_AGREEMENT_TYPE_CODE { get; set; }
            [Column("NAG_AGREEMENT_TYPE_TEXT")] public string NAG_AGREEMENT_TYPE_TEXT { get; set; }
            [Column("NAG_COINSURANCE_RATIO")] public decimal? NAG_COINSURANCE_RATIO { get; set; }
            [Column("NAG_CURRENCY_ID")] public string NAG_CURRENCY_ID { get; set; }
            [Column("NAG_CURRENCY_TYPE_CODE")] public string NAG_CURRENCY_TYPE_CODE { get; set; }
            [Column("NAG_CURRENCY_TYPE_TEXT")] public string NAG_CURRENCY_TYPE_TEXT { get; set; }
            [Column("NAG_DAY_LIMIT")] public decimal? NAG_DAY_LIMIT { get; set; }
            [Column("NAG_EXEMPTION_LIMIT")] public decimal? NAG_EXEMPTION_LIMIT { get; set; }
            [Column("NAG_EXEMPTION_TYPE")] public string NAG_EXEMPTION_TYPE { get; set; }
            [Column("NAG_EXEMPTION_TYPE_CODE")] public string NAG_EXEMPTION_TYPE_CODE { get; set; }
            [Column("NAG_EXEMPTION_TYPE_TEXT")] public string NAG_EXEMPTION_TYPE_TEXT { get; set; }
            [Column("NAG_NUMBER_LIMIT")] public decimal? NAG_NUMBER_LIMIT { get; set; }
            [Column("NAG_PRICE_FACTOR")] public decimal? NAG_PRICE_FACTOR { get; set; }
            [Column("NAG_PRICE_LIMIT")] public decimal? NAG_PRICE_LIMIT { get; set; }
            [Column("NAG_SESSION_COUNT")] public decimal? NAG_SESSION_COUNT { get; set; }
            [Column("NETWORK_ID")] public long? NETWORK_ID { get; set; }
            [Column("NETWORK_NAME")] public string NETWORK_NAME { get; set; }
            [Column("NETWORK_TYPE")] public string NETWORK_TYPE { get; set; }
            [Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Column("PACKAGE_NAME")] public string PACKAGE_NAME { get; set; }
            [Column("PACKAGE_NO")] public string PACKAGE_NO { get; set; }
            [Column("PACKAGE_TYPE")] public string PACKAGE_TYPE { get; set; }
            [Column("PASSPORT_NO")] public string PASSPORT_NO { get; set; }
            [Column("PLAN_COVERAGE_ID")] public long? PLAN_COVERAGE_ID { get; set; }
            [Column("PLAN_ID")] public long? PLAN_ID { get; set; }
            [Column("PLAN_NAME")] public string PLAN_NAME { get; set; }
            [Column("POLICY_ID")] public long? POLICY_ID { get; set; }
            [Column("POLICY_NUMBER")] Int64? POLICY_NUMBER { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("RENEWAL_NO")] public long? RENEWAL_NO { get; set; }
            [Column("SUBPRODUCT_CODE")] public string SUBPRODUCT_CODE { get; set; }
            [Column("SUBPRODUCT_ID")] public long? SUBPRODUCT_ID { get; set; }
            [Column("TAX_NUMBER")] public Int64? TAX_NUMBER { get; set; }
            [Column("TAX_OFFICE")] public string TAX_OFFICE { get; set; }

        }

        [Table("V_INSURED_PARAMETER")]
        public class V_InsuredParameter
        {
            [Column("INSURED_ID")]
            public Int64 InsuredId { get; set; }
            [Key]
            [Column("INSURED_PARAMETER_ID")]
            public Int64 InsuredParameterId { get; set; }
            [Column("ENVIRONMENT")]
            public string Environment { get; set; }
            [Column("KEY")]
            public string Key { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("PARAMETER_ID")]
            public Int64? ParameterId { get; set; }
            [Column("SP_RESPONSE_ID")]
            public Int64? SpResponseId { get; set; }
            [Column("STATUS")]
            public string Status { get; set; }
            [Column("SYSTEM_TYPE")]
            public string SystemType { get; set; }
            [Column("VALUE")]
            public string Value { get; set; }
        }

        [Table("V_POLICY_GROUP")]
        public class V_PolicyGroup
        {
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PARENT_ID")] public long? PARENT_ID { get; set; }
            [Column("PARENT_NAME")] public string PARENT_NAME { get; set; }
            [Key][Column("POLICY_GROUP_ID")] public long? POLICY_GROUP_ID { get; set; }
            [Column("POLICY_GROUP_NAME")] public string POLICY_GROUP_NAME { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_POLICY")]
        public class V_Policy
        {
            [Column("LAST_ENDORSEMENT_ID")]
            public Int64? LastEndorsementId { get; set; }
            [Key]
            [Column("AGENCY_ID")] public long? AGENCY_ID { get; set; }
            [Column("AGENCY_NAME")] public string AGENCY_NAME { get; set; }
            [Column("AGENCY_NO")] public string AGENCY_NO { get; set; }
            [Column("BRANCH_ID")] public long? BRANCH_ID { get; set; }
            [Column("BRANCH_NAME")] public string BRANCH_NAME { get; set; }
            [Column("CASH_AMOUNT")] public decimal? CASH_AMOUNT { get; set; }
            [Column("CASH_PAYMENT_TYPE")] public string CASH_PAYMENT_TYPE { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("CURRENCY_TYPE")] public string CURRENCY_TYPE { get; set; }
            [Column("EXCHANGE_RATE_ID")] public long? EXCHANGE_RATE_ID { get; set; }
            [Column("FOREX_SELLING")] public decimal? FOREX_SELLING { get; set; }
            [Column("INSTALLMENT_COUNT")] public long? INSTALLMENT_COUNT { get; set; }
            [Column("INSURER")] public long? INSURER { get; set; }
            [Column("INSURER_IDENTITY_NO")] public string INSURER_IDENTITY_NO { get; set; }
            [Column("INSURER_NAME")] public string INSURER_NAME { get; set; }
            [Column("INSURER_TAX_NUMBER")] public string INSURER_TAX_NUMBER { get; set; }
            [Column("IS_OPEN_TO_CLAIM")] public string IS_OPEN_TO_CLAIM { get; set; }
            [Column("LOCATION_TYPE")] public string LOCATION_TYPE { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PARENT_ID")] public long? PARENT_ID { get; set; }
            [Column("PARENT_POLICY_NO")] public string PARENT_POLICY_NO { get; set; }
            [Column("PARENT_RENEWAL_NO")] public long? PARENT_RENEWAL_NO { get; set; }
            [Column("PAYMENT_TYPE")] public string PAYMENT_TYPE { get; set; }
            [Column("POLICY_END_DATE")] public DateTime? POLICY_END_DATE { get; set; }
            [Column("POLICY_GROUP_ID")] public long? POLICY_GROUP_ID { get; set; }
            [Column("POLICY_GROUP_NAME")] public string POLICY_GROUP_NAME { get; set; }
            [Column("POLICY_ID")] public long POLICY_ID { get; set; }
            [Column("POLICY_ISSUE_DATE")] public DateTime? POLICY_ISSUE_DATE { get; set; }
            [Column("POLICY_NUMBER")] public Int64? POLICY_NUMBER { get; set; }
            [Column("POLICY_START_DATE")] public DateTime? POLICY_START_DATE { get; set; }
            [Column("POLICY_TYPE")] public string POLICY_TYPE { get; set; }
            [Column("PREMIUM")] public decimal? PREMIUM { get; set; }
            [Column("PREVIOUS_ID")] public long? PREVIOUS_ID { get; set; }
            [Column("PREVIOUS_POLICY_NO")] public string PREVIOUS_POLICY_NO { get; set; }
            [Column("PREVIOUS_RENEWAL_NO")] public long? PREVIOUS_RENEWAL_NO { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("RENEWAL_NO")] public int RENEWAL_NO { get; set; }
            [Column("SBM_NO")] public string SBM_NO { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SUBPRODUCT_CODE")] public string SUBPRODUCT_CODE { get; set; }
            [Column("SUBPRODUCT_ID")] public long? SUBPRODUCT_ID { get; set; }
            [Column("TAX")] public decimal? TAX { get; set; }
            [Column("IS_SBM_TRANSFER")]
            public string IsSbmTransfer { get; set; }
            [Column("INTEGRATION_STATUS")]
            public string IntegrationStatus { get; set; }
        }

        [Table("V_POLICY_INSURED")]
        public class V_PolicyInsured
        {
            [Column("LAST_ENDORSEMENT_ID")]
            public Int64? LastEndorsementId { get; set; }
            [Key]
            [Column("AGENCY_ID")] public long? AGENCY_ID { get; set; }
            [Column("AGENCY_NAME")] public string AGENCY_NAME { get; set; }
            [Column("AGENCY_NO")] public string AGENCY_NO { get; set; }
            [Column("BRANCH_ID")] public long? BRANCH_ID { get; set; }
            [Column("BRANCH_NAME")] public string BRANCH_NAME { get; set; }
            [Column("CASH_AMOUNT")] public long? CASH_AMOUNT { get; set; }
            [Column("CASH_PAYMENT_TYPE")] public string CASH_PAYMENT_TYPE { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("CURRENCY_TYPE")] public string CURRENCY_TYPE { get; set; }
            [Column("EXCHANGE_RATE_ID")] public long? EXCHANGE_RATE_ID { get; set; }
            [Column("FOREX_SELLING")] public long? FOREX_SELLING { get; set; }
            [Column("INSTALLMENT_COUNT")] public long? INSTALLMENT_COUNT { get; set; }
            [Column("INSURER")] public long? INSURER { get; set; }
            [Column("INSURER_IDENTITY_NO")] public string INSURER_IDENTITY_NO { get; set; }
            [Column("INSURER_NAME")] public string INSURER_NAME { get; set; }
            [Column("LAST_NAME")] public string LAST_NAME { get; set; }
            [Column("INSURER_TAX_NUMBER")] public string INSURER_TAX_NUMBER { get; set; }
            [Column("IS_OPEN_TO_CLAIM")] public string IS_OPEN_TO_CLAIM { get; set; }
            [Column("LOCATION_TYPE")] public string LOCATION_TYPE { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PARENT_ID")] public long? PARENT_ID { get; set; }
            [Column("PARENT_POLICY_NO")] public string PARENT_POLICY_NO { get; set; }
            [Column("PARENT_RENEWAL_NO")] public long? PARENT_RENEWAL_NO { get; set; }
            [Column("PAYMENT_TYPE")] public string PAYMENT_TYPE { get; set; }
            [Column("POLICY_END_DATE")] public DateTime? POLICY_END_DATE { get; set; }
            [Column("POLICY_GROUP_ID")] public long? POLICY_GROUP_ID { get; set; }
            [Column("POLICY_GROUP_NAME")] public string POLICY_GROUP_NAME { get; set; }
            [Column("POLICY_ID")] public long POLICY_ID { get; set; }
            [Column("POLICY_ISSUE_DATE")] public DateTime? POLICY_ISSUE_DATE { get; set; }
            [Column("POLICY_NUMBER")] public string POLICY_NUMBER { get; set; }
            [Column("POLICY_START_DATE")] public DateTime? POLICY_START_DATE { get; set; }
            [Column("POLICY_TYPE")] public string POLICY_TYPE { get; set; }
            [Column("PREMIUM")] public long? PREMIUM { get; set; }
            [Column("PREVIOUS_ID")] public long? PREVIOUS_ID { get; set; }
            [Column("PREVIOUS_POLICY_NO")] public string PREVIOUS_POLICY_NO { get; set; }
            [Column("PREVIOUS_RENEWAL_NO")] public long? PREVIOUS_RENEWAL_NO { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("RENEWAL_NO")] public long? RENEWAL_NO { get; set; }
            [Column("SBM_NO")] public string SBM_NO { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SUBPRODUCT_CODE")] public string SUBPRODUCT_CODE { get; set; }
            [Column("SUBPRODUCT_ID")] public long? SUBPRODUCT_ID { get; set; }
            [Column("TAX")] public long? TAX { get; set; }
            [Column("IS_SBM_TRANSFER")]
            public string IsSbmTransfer { get; set; }
            [Column("INTEGRATION_STATUS")]
            public string IntegrationStatus { get; set; }
            [Column("INSURED_FIRST_NAME")]
            public string InsuredFirstName { get; set; }
            [Column("INSURED_LAST_NAME")]
            public string InsuredLastName { get; set; }
            [Column("INSURED_IDENTITY_NO")]
            public string InsuredIdentityNo { get; set; }
            [Column("INSURED_TAX_NUMBER")]
            public string InsuredTaxNumber { get; set; }
            [Column("INSURED_PASSPORT_NO")]
            public string InsuredPassportNo { get; set; }
        }

        [Table("V_POLICY_ENDORSEMENT")]
        public class V_PolicyEndorsement
        {
            [Column("AGENCY_ADDRESS_ID")] public long? AGENCY_ADDRESS_ID { get; set; }
            [Column("AGENCY_ID")] public long? AGENCY_ID { get; set; }
            [Column("AGENCY_NAME")] public string AGENCY_NAME { get; set; }
            [Column("AGENCY_NO")] public string AGENCY_NO { get; set; }
            [Column("AGENCY_TYPE")] public string AGENCY_TYPE { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("ENDORSEMENT_END_DATE")] public DateTime? ENDORSEMENT_END_DATE { get; set; }
            [Column("ENDORSEMENT_ID")] public long ENDORSEMENT_ID { get; set; }
            [Column("ENDORSEMENT_ISSUE_DATE")] public DateTime? ENDORSEMENT_ISSUE_DATE { get; set; }
            [Column("ENDORSEMENT_NO")] public long? ENDORSEMENT_NO { get; set; }
            [Column("ENDORSEMENT_PREMIUM")] public decimal? ENDORSEMENT_PREMIUM { get; set; }
            [Column("ENDORSEMENT_START_DATE")] public DateTime? ENDORSEMENT_START_DATE { get; set; }
            [Column("ENDORSEMENT_TYPE")] public string ENDORSEMENT_TYPE { get; set; }
            [Column("INSURER")] public long? INSURER { get; set; }
            [Column("INSURER_NAME")] public string INSURER_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("POLICY_END_DATE")] public DateTime? POLICY_END_DATE { get; set; }
            [Column("POLICY_GROUP_ID")] public long? POLICY_GROUP_ID { get; set; }
            [Key][Column("POLICY_ID")] public long POLICY_ID { get; set; }
            [Column("POLICY_ISSUE_DATE")] public DateTime? POLICY_ISSUE_DATE { get; set; }
            [Column("POLICY_IS_OPEN_TO_CLAIM")] public string POLICY_IS_OPEN_TO_CLAIM { get; set; }
            [Column("POLICY_NUMBER")] public Int64? POLICY_NUMBER { get; set; }
            [Column("POLICY_PREMIUM")] public decimal? POLICY_PREMIUM { get; set; }
            [Column("POLICY_START_DATE")] public DateTime? POLICY_START_DATE { get; set; }
            [Column("POLICY_STATUS")] public string POLICY_STATUS { get; set; }
            [Column("POLICY_TYPE")] public string POLICY_TYPE { get; set; }
            [Column("PREVIOUS_ID")] public long? PREVIOUS_ID { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("REASON_DESCRIPTION")] public string REASON_DESCRIPTION { get; set; }
            [Column("REASON_ID")] public long? REASON_ID { get; set; }
            [Column("REGISTRATION_DATE")] public DateTime? REGISTRATION_DATE { get; set; }
            [Column("RENEWAL_NO")] public long? RENEWAL_NO { get; set; }
            [Column("SBM_AUTHORIZATION_CODE")] public string SBM_AUTHORIZATION_CODE { get; set; }
            [Column("SBM_NO")] public string SBM_NO { get; set; }
            [Column("SBM_STATUS")] public string SBM_STATUS { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
        }
        [Table("V_INSURED_ENDORSEMENT")]
        public class V_InsuredEndorsement
        {
            [Column("AGENCY_ID")] public long? AGENCY_ID { get; set; }
            [Column("AGENCY_NAME")] public string AGENCY_NAME { get; set; }
            [Column("AGENCY_NO")] public string AGENCY_NO { get; set; }
            [Column("BIRTHDATE")] public DateTime? BIRTHDATE { get; set; }
            [Column("BIRTHPLACE")] public string BIRTHPLACE { get; set; }
            [Column("PASSPORT_NO")] public string PASSPORT_NO { get; set; }
            [Column("NATIONALITY_ID")] public int NATIONALITY_ID { get; set; }
            [Column("NAME_OF_FATHER")] public string NAME_OF_FATHER { get; set; }
            [Column("BIRTH_COVERAGE_DATE")] public DateTime? BIRTH_COVERAGE_DATE { get; set; }
            [Column("COMPANY_ENTRANCE_DATE")] public DateTime? COMPANY_ENTRANCE_DATE { get; set; }
            [Column("COMPANY_ID")] public long COMPANY_ID { get; set; }
            [Column("COMPANY_INSURED_NO")] public string COMPANY_INSURED_NO { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("CONTACT_ID")] public long CONTACT_ID { get; set; }
            [Column("ENDORSEMENT_END_DATE")] public DateTime? ENDORSEMENT_END_DATE { get; set; }
            [Column("ENDORSEMENT_ID")] public long ENDORSEMENT_ID { get; set; }
            [Column("ENDORSEMENT_ISSUE_DATE")] public DateTime? ENDORSEMENT_ISSUE_DATE { get; set; }
            [Column("ENDORSEMENT_NO")] public int ENDORSEMENT_NO { get; set; }
            [Column("ENDORSEMENT_PREMIUM")] public decimal? ENDORSEMENT_PREMIUM { get; set; }
            [Column("ENDORSEMENT_START_DATE")] public DateTime? ENDORSEMENT_START_DATE { get; set; }
            [Column("ENDORSEMENT_TYPE")] public string ENDORSEMENT_TYPE { get; set; }
            [Column("FAMILY_NO")] public long? FAMILY_NO { get; set; }
            [Column("FIRST_AMBULANT_COVERAGE_DATE")] public DateTime? FIRST_AMBULANT_COVERAGE_DATE { get; set; }
            [Column("FIRST_INSURED_DATE")] public DateTime? FIRST_INSURED_DATE { get; set; }
            [Column("FIRST_NAME")] public string FIRST_NAME { get; set; }
            [Column("GENDER")] public string GENDER { get; set; }
            [Column("IDENTITY_NO")] public Int64? IDENTITY_NO { get; set; }
            [Column("INDIVIDUAL_TYPE")] public string INDIVIDUAL_TYPE { get; set; }
            [Key][Column("INSURED_ID")] public long INSURED_ID { get; set; }
            [Column("INSURED_IS_OPEN_TO_CLAIM")] public string INSURED_IS_OPEN_TO_CLAIM { get; set; }
            [Column("INSURED_NAME_LASTNAME")] public string INSURED_NAME_LASTNAME { get; set; }
            [Column("INSURED_NEW_VERSION_ID")] public long? INSURED_NEW_VERSION_ID { get; set; }
            [Column("INSURER")] public long? INSURER { get; set; }
            [Column("INSURER_NAME")] public string INSURER_NAME { get; set; }
            [Column("LAST_NAME")] public string LAST_NAME { get; set; }
            [Column("MIDDLE_NAME")] public string MIDDLE_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Column("PACKAGE_NO")] public string PACKAGE_NO { get; set; }
            [Column("PACKAGE_NAME")] public string PACKAGE_NAME { get; set; }
            [Column("PARENT_ID")] public long? PARENT_ID { get; set; }
            [Column("POLICY_END_DATE")] public DateTime? POLICY_END_DATE { get; set; }
            [Column("POLICY_ID")] public long POLICY_ID { get; set; }
            [Column("POLICY_ISSUE_DATE")] public DateTime? POLICY_ISSUE_DATE { get; set; }
            [Column("POLICY_IS_OPEN_TO_CLAIM")] public string POLICY_IS_OPEN_TO_CLAIM { get; set; }
            [Column("POLICY_NUMBER")] public Int64? POLICY_NUMBER { get; set; }
            [Column("POLICY_PREMIUM")] public decimal? POLICY_PREMIUM { get; set; }
            [Column("POLICY_START_DATE")] public DateTime? POLICY_START_DATE { get; set; }
            [Column("POLICY_STATUS")] public string POLICY_STATUS { get; set; }
            [Column("PRODUCT_TYPE")] public string PRODUCT_TYPE { get; set; }
            [Column("PREMIUM")] public decimal? PREMIUM { get; set; }
            [Column("PREVIOUS_ID")] public long? PREVIOUS_ID { get; set; }
            [Column("PRICE_ID")] public long? PRICE_ID { get; set; }
            [Column("REASON_DESCRIPTION")] public string REASON_DESCRIPTION { get; set; }
            [Column("REASON_ID")] public long? REASON_ID { get; set; }
            [Column("REGISTRATION_DATE")] public DateTime? REGISTRATION_DATE { get; set; }
            [Column("REGISTRATION_NO")] public string REGISTRATION_NO { get; set; }
            [Column("RENEWAL_DATE")] public DateTime? RENEWAL_DATE { get; set; }
            [Column("RENEWAL_GUARANTEE_TYPE")] public string RENEWAL_GUARANTEE_TYPE { get; set; }
            [Column("RENEWAL_NO")] public long? RENEWAL_NO { get; set; }
            [Column("SBM_AUTHORIZATION_CODE")] public string SBM_AUTHORIZATION_CODE { get; set; }
            [Column("SBM_NO")] public string SBM_NO { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("TAX")] public Int64? TAX { get; set; }
            [Column("TOTAL_PREMIUM")] public decimal? TOTAL_PREMIUM { get; set; }
            [Column("TSS_DESC")] public string TSS_DESC { get; set; }
            [Column("TSS_VIP")] public int TSS_VIP { get; set; }
            [Column("TSS_PRODUCT_CODE")] public string TSS_PRODUCT_CODE { get; set; }
            [Column("TSS_SUBPRODUCT_CODE")] public string TSS_SUBPRODUCT_CODE { get; set; }

        }
        [Table("V_INSURED_PACKAGE_REMAINING")]
        public class V_InsuredPackageRemaining
        {
            [Column("AGR_AGREEMENT_TYPE")]
            public string AgrAgreementType { get; set; }
            [Column("AGR_COINSURANCE_RATIO")]
            public Int64? AgrCoinsuranceRatio { get; set; }
            [Column("AGR_CURRENCY_ID")]
            public string AgrCurrencyId { get; set; }
            [Column("AGR_DAY_LIMIT")]
            public decimal? AgrDayLimit { get; set; }
            [Column("AGR_DAY_LIMIT_REMAINING")]
            public string AgrDayLimitRemaining { get; set; }
            [Column("AGR_DAY_LIMIT_USE")]
            public string AgrDayLimitUse { get; set; }
            [Column("AGR_EXEMPTION_LIMIT")]
            public decimal? AgrExemptionLimit { get; set; }
            [Column("AGR_EXEMPTION_LIMIT_REMAINING")]
            public string AgrExemptionLimitRemaining { get; set; }
            [Column("AGR_EXEMPTION_LIMIT_USE")]
            public string AgrExemptionLimitUse { get; set; }
            [Column("AGR_EXEMPTION_TYPE")]
            public string AgrExemptionType { get; set; }
            [Column("AGR_NUMBER_LIMIT")]
            public decimal? AgrNumberLimit { get; set; }
            [Column("AGR_NUMBER_LIMIT_REMAINING")]
            public string AgrNumberLimitRemaining { get; set; }
            [Column("AGR_NUMBER_LIMIT_USE")]
            public string AgrNumberLimitUse { get; set; }
            [Column("AGR_PRICE_FACTOR")]
            public decimal? AgrPriceFactor { get; set; }
            [Column("AGR_PRICE_LIMIT")]
            public decimal? AgrPriceLimit { get; set; }
            [Column("AGR_PRICE_LIMIT_REMAINING")]
            public string AgrPriceLimitRemaining { get; set; }
            [Column("AGR_PRICE_LIMIT_TL")]
            public string AgrPriceLimitTL { get; set; }
            [Column("AGR_PRICE_LIMIT_USE")]
            public string AgrPriceLimitUse { get; set; }
            [Column("AGR_SESSION_COUNT")]
            public decimal? AgrSessionCount { get; set; }
            [Column("AGR_SESSION_COUNT_REMAINING")]
            public string AgrSessionCountRemaining { get; set; }
            [Column("AGR_SESSION_COUNT_USE")]
            public string AgrSessionCountUse { get; set; }
            [Column("BRANCH_ID")]
            public Int64 BranchId { get; set; }
            [Column("BRANCH_NAME")]
            public string BranchName { get; set; }
            [Column("COMPANY_ID")]
            public Int64 CompanyId { get; set; }
            [Column("COMPANY_NAME")]
            public string CompanyName { get; set; }
            [Column("COVERAGE_ID")]
            public long CoverageId { get; set; }
            [Column("COVERAGE_NAME")]
            public string CoverageName { get; set; }
            [Column("COVERAGE_TYPE")]
            public string CoverageType { get; set; }


            [Column("DSP_AGREEMENT_TYPE")]
            public string DspAgreementType { get; set; }
            [Column("DSP_COINSURANCE_RATIO")]
            public Int64? DspCoinsuranceRatio { get; set; }
            [Column("DSP_CURRENCY_ID")]
            public string DspCurrencyId { get; set; }
            [Column("DSP_DAY_LIMIT")]
            public decimal? DspDayLimit { get; set; }
            [Column("DSP_DAY_LIMIT_REMAINING")]
            public string DspDayLimitRemaining { get; set; }
            [Column("DSP_DAY_LIMIT_USE")]
            public string DspDayLimitUse { get; set; }
            [Column("DSP_SESSION_COUNT")]
            public decimal? DspSessionCount { get; set; }
            [Column("DSP_SESSION_COUNT_REMAINING")]
            public string DspSessionCountRemaining { get; set; }
            [Column("DSP_SESSION_COUNT_USE")]
            public string DspSessionCountUse { get; set; }
            [Column("DSP_EXEMPTION_LIMIT")]
            public decimal? DspExemptionLimit { get; set; }
            [Column("DSP_EXEMPTION_LIMIT_REMAINING")]
            public string DspExemptionLimitRemaining { get; set; }
            [Column("DSP_EXEMPTION_LIMIT_USE")]
            public string DspExemptionLimitUse { get; set; }
            [Column("DSP_EXEMPTION_TYPE")]
            public string DspExemptionType { get; set; }
            [Column("DSP_NUMBER_LIMIT")]
            public decimal? DspNumberLimit { get; set; }
            [Column("DSP_NUMBER_LIMIT_REMAINING")]
            public string DspNumberLimitRemaining { get; set; }
            [Column("DSP_NUMBER_LIMIT_USE")]
            public string DspNumberLimitUse { get; set; }
            [Column("DSP_PRICE_FACTOR")]
            public decimal? DspPriceFactor { get; set; }
            [Column("DSP_PRICE_LIMIT")]
            public decimal? DspPriceLimit { get; set; }
            [Column("DSP_PRICE_LIMIT_REMAINING")]
            public string DspPriceLimitRemaining { get; set; }
            [Column("DSP_PRICE_LIMIT_TL")]
            public string DspPriceLimitTL { get; set; }
            [Column("DSP_PRICE_LIMIT_USE")]
            public string DspPriceLimitUse { get; set; }




            [Column("EMG_AGREEMENT_TYPE")]
            public string EmgAgreementType { get; set; }
            [Column("EMG_COINSURANCE_RATIO")]
            public Int64? EmgCoinsuranceRatio { get; set; }
            [Column("EMG_CURRENCY_ID")]

            public string EmgCurrencyId { get; set; }
            [Column("EMG_DAY_LIMIT")]
            public decimal? EmgDayLimit { get; set; }
            [Column("EMG_DAY_LIMIT_REMAINING")]
            public string EmgDayLimitRemaining { get; set; }
            [Column("EMG_DAY_LIMIT_USE")]
            public string EmgDayLimitUse { get; set; }
            [Column("EMG_EXEMPTION_LIMIT")]
            public decimal? EmgExemptionLimit { get; set; }
            [Column("EMG_EXEMPTION_LIMIT_REMAINING")]
            public string EmgExemptionLimitRemaining { get; set; }
            [Column("EMG_EXEMPTION_LIMIT_USE")]
            public string EmgExemptionLimitUse { get; set; }
            [Column("EMG_EXEMPTION_TYPE")]
            public string EmgExemptionType { get; set; }
            [Column("EMG_NUMBER_LIMIT")]
            public decimal? EmgNumberLimit { get; set; }
            [Column("EMG_NUMBER_LIMIT_REMAINING")]
            public string EmgNumberLimitRemaining { get; set; }
            [Column("EMG_NUMBER_LIMIT_USE")]
            public string EmgNumberLimitUse { get; set; }
            [Column("EMG_PRICE_FACTOR")]
            public decimal? EmgPriceFactor { get; set; }
            [Column("EMG_PRICE_LIMIT")]
            public decimal? EmgPriceLimit { get; set; }
            [Column("EMG_PRICE_LIMIT_REMAINING")]
            public string EmgPriceLimitRemaining { get; set; }
            [Column("EMG_PRICE_LIMIT_TL")]
            public string EmgPriceLimitTL { get; set; }
            [Column("EMG_PRICE_LIMIT_USE")]
            public string EmgPriceLimitUse { get; set; }
            [Column("EMG_SESSION_COUNT")]
            public decimal? EmgSessionCount { get; set; }
            [Column("EMG_SESSION_COUNT_REMAINING")]
            public string EmgSessionCountRemaining { get; set; }
            [Column("EMG_SESSION_COUNT_USE")]
            public string EmgSessionCountUse { get; set; }
            [Column("FAMILY_NO")]
            public Int64 FamilyNo { get; set; }
            [Column("FIRST_NAME")]
            public string FirstName { get; set; }
            [Key]
            [Column("INSURED_ID")]
            public Int64 InsuredId { get; set; }
            [Column("IS_MAIN_COVERAGE")]
            public int IsMainCoverage { get; set; }
            [Column("LAST_NAME")]
            public string LastName { get; set; }
            [Column("MAIN_COVERAGE_ID")]
            public Int64? MainCoverageId { get; set; }
            [Column("MIDDLE_NAME")]
            public string MiddleName { get; set; }
            
            [Column("NAG_AGREEMENT_TYPE")]
            public string NagAgreementType { get; set; }
            [Column("NAG_COINSURANCE_RATIO")]
            public Int64? NagCoinsuranceRatio { get; set; }
            [Column("NAG_CURRENCY_ID")]

            public string NagCurrencyId { get; set; }
            [Column("NAG_DAY_LIMIT")]
            public decimal? NagDayLimit { get; set; }
            [Column("NAG_DAY_LIMIT_REMAINING")]
            public string NagDayLimitRemaining { get; set; }
            [Column("NAG_DAY_LIMIT_USE")]
            public string NagDayLimitUse { get; set; }
            [Column("NAG_EXEMPTION_LIMIT")]
            public decimal? NagExemptionLimit { get; set; }
            [Column("NAG_EXEMPTION_LIMIT_REMAINING")]
            public string NagExemptionLimitRemaining { get; set; }
            [Column("NAG_EXEMPTION_LIMIT_USE")]
            public string NagExemptionLimitUse { get; set; }
            [Column("NAG_EXEMPTION_TYPE")]
            public string NagExemptionType { get; set; }
            [Column("NAG_NUMBER_LIMIT")]
            public decimal? NagNumberLimit { get; set; }
            [Column("NAG_NUMBER_LIMIT_REMAINING")]
            public string NagNumberLimitRemaining { get; set; }
            [Column("NAG_NUMBER_LIMIT_USE")]
            public string NagNumberLimitUse { get; set; }
            [Column("NAG_PRICE_FACTOR")]
            public decimal? NagPriceFactor { get; set; }
            [Column("NAG_PRICE_LIMIT")]
            public decimal? NagPriceLimit { get; set; }
            [Column("NAG_PRICE_LIMIT_REMAINING")]
            public string NagPriceLimitRemaining { get; set; }
            [Column("NAG_PRICE_LIMIT_TL")]
            public string NagPriceLimitTL { get; set; }
            [Column("NAG_PRICE_LIMIT_USE")]
            public string NagPriceLimitUse { get; set; }
            [Column("NAG_SESSION_COUNT")]
            public decimal? NagSessionCount { get; set; }
            [Column("NAG_SESSION_COUNT_REMAINING")]
            public string NagSessionCountRemaining { get; set; }
            [Column("NAG_SESSION_COUNT_USE")]
            public string NagSessionCountUse { get; set; }
            [Column("NETWORK_ID")]
            public Int64 NetworkId { get; set; }

            [Column("NETWORK_NAME")]
            public string NetworkName { get; set; }
            [Column("PACKAGE_ID")]
            public Int64 PackageId { get; set; }
            [Column("PACKAGE_NO")]
            public string PackageNo { get; set; }
            [Column("PACKAGE_TYPE")]
            public string PackageType { get; set; }
            [Column("PLAN_COVERAGE_ID")]
            public Int64 PlanCoverageId { get; set; }
            [Column("PLAN_ID")]
            public Int64? PlanId { get; set; }
            [Column("PLAN_NAME")]
            public string PlanName { get; set; }
            [Column("POLICY_ID")]
            public Int64 PolicyId { get; set; }
            [Column("POLICY_NUMBER")]
            public string PolicyNumber { get; set; }
            [Column("PRODUCT_CODE")]
            public string ProductCode { get; set; }
            [Column("PRODUCT_ID")]
            public Int64 ProductId { get; set; }
            [Column("RENEWAL_NO")]
            public decimal? RenewalNo { get; set; }
            [Column("SUBPRODUCT_CODE")]
            public string SubProductCode { get; set; }
            [Column("SUBPRODUCT_ID")]
            public Int64? SubProductId { get; set; }

        }
        [Table("V_CONTRACT_PROCESS_GROUP")]
        [DisplayColumn("ÜCRETLENDİRME")]
        public class V_ContractProcessGroup
        {

            [Column("CONTRACT_ID", TypeName = "BASEID")]
            public Int64 CONTRACT_ID { get; set; }
            [Column("AMOUNT")]
            [History]
            public Decimal AMOUNT { get; set; }
            [Key]
            [Column("CONTRACT_PROCESS_GROUP_ID")]
            public Int64 CONTRACT_PROCESS_GROUP_ID { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NEW_VERSION_ID { get; set; }
            [Column("PRICING_TYPE")]
            [History]
            public string PRICING_TYPE { get; set; }
            [Column("PROCESS_GROUP_ID")]
            public Int64 PROCESS_GROUP_ID { get; set; }
            [Column("PROCESS_GROUP_NAME")]
            public string PROCESS_GROUP_NAME { get; set; }
            [Column("PROCESS_LIST_ID")]
            public Int64 PROCESS_LIST_ID { get; set; }
            [Column("PROCESS_LIST_NAME")]
            public string PROCESS_LIST_NAME { get; set; }
            [Column("SP_RESPONSE_ID")]
            public Int64? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")]
            public string STATUS { get; set; }

        }
        [Table("V_CONTRACT_NETWORK")]
        [DisplayColumn("NETWORK")]
        public class V_ContractNetwork
        {
            [Column("COMPANY_ID")]
            public Int64 COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")]
            public string COMPANY_NAME { get; set; }

            [Column("CONTRACT_ID")]
            public Int64 CONTRACT_ID { get; set; }
            [Key]
            [Column("CONTRACT_NETWORK_ID")]
            public Int64 CONTRACT_NETWORK_ID { get; set; }
            [Column("CONTRACT_NO")]
            public string CONTRACT_NO { get; set; }

            [Column("NETWORK_ID")]
            public Int64 NETWORK_ID { get; set; }
            [Column("NETWORK_NAME")]
            [History]
            public string NETWORK_NAME { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NEW_VERSION_ID { get; set; }

            [Column("SP_RESPONSE_ID")]
            public Int64? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")]
            public string STATUS { get; set; }
            [Column("CONTRACT_STATUS")]
            public string CONTRACT_STATUS { get; set; }



        }

        [Table("V_PLAN_COVERAGE_PARAMETER")]
        public class V_PlanCoverageParameter
        {
            [Column("BRANCH_ID")] public long? BRANCH_ID { get; set; }
            [Column("BRANCH_NAME")] public string BRANCH_NAME { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("COVERAGE_ID")] public long? COVERAGE_ID { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Column("COVERAGE_TYPE")] public string COVERAGE_TYPE { get; set; }
            [Column("ENVIRONMENT")] public string ENVIRONMENT { get; set; }
            [Column("IS_MAIN_COVERAGE")] public long? IS_MAIN_COVERAGE { get; set; }
            [Column("KEY")] public string KEY { get; set; }
            [Column("MAIN_COVERAGE_ID")] public long? MAIN_COVERAGE_ID { get; set; }
            [Column("NETWORK_ID")] public long? NETWORK_ID { get; set; }
            [Column("NETWORK_NAME")] public string NETWORK_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Column("PARAMETER_ID")] public long? PARAMETER_ID { get; set; }
            [Column("PARENT_ID")] public long? PARENT_ID { get; set; }
            [Column("PLAN_COVERAGE_ID")] public long? PLAN_COVERAGE_ID { get; set; }
            [Key][Column("PLAN_COVERAGE_PARAMETER_ID")] public long? PLAN_COVERAGE_PARAMETER_ID { get; set; }
            [Column("PLAN_ID")] public long? PLAN_ID { get; set; }
            [Column("PLAN_NAME")] public string PLAN_NAME { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SUBPRODUCT_CODE")] public string SUBPRODUCT_CODE { get; set; }
            [Column("SUBPRODUCT_ID")] public long? SUBPRODUCT_ID { get; set; }
            [Column("SUBPRODUCT_NAME")] public string SUBPRODUCT_NAME { get; set; }
            [Column("SYSTEM_TYPE")] public string SYSTEM_TYPE { get; set; }
            [Column("VALUE")] public string VALUE { get; set; }

        }

        [Table("V_PLAN_COVERAGE_VARIATION")]
        public class V_PlanCoverageVariation
        {
            [Column("AGR_AGREEMENT_TYPE")] public string AGR_AGREEMENT_TYPE { get; set; }
            [Column("AGR_COINSURANCE_RATIO")] public decimal? AGR_COINSURANCE_RATIO { get; set; }
            [Column("AGR_CURRENCY_ID")] public string AGR_CURRENCY_ID { get; set; }
            [Column("AGR_DAY_LIMIT")] public decimal? AGR_DAY_LIMIT { get; set; }
            [Column("AGR_EXEMPTION_LIMIT")] public decimal? AGR_EXEMPTION_LIMIT { get; set; }
            [Column("AGR_EXEMPTION_TYPE")] public string AGR_EXEMPTION_TYPE { get; set; }
            [Column("AGR_ID")] public long? AGR_ID { get; set; }
            [Column("AGR_NEW_VERSION_ID")] public long? AGR_NEW_VERSION_ID { get; set; }
            [Column("AGR_NUMBER_LIMIT")] public decimal? AGR_NUMBER_LIMIT { get; set; }
            [Column("AGR_PRICE_FACTOR")] public decimal? AGR_PRICE_FACTOR { get; set; }
            [Column("AGR_PRICE_LIMIT")] public decimal? AGR_PRICE_LIMIT { get; set; }
            [Column("AGR_SESSION_COUNT")] public decimal? AGR_SESSION_COUNT { get; set; }
            [Column("AGR_STATUS")] public string AGR_STATUS { get; set; }
            [Column("BRANCH_ID")] public long? BRANCH_ID { get; set; }
            [Column("BRANCH_NAME")] public string BRANCH_NAME { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("COVERAGE_ID")] public long? COVERAGE_ID { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Column("COVERAGE_TYPE")] public string COVERAGE_TYPE { get; set; }
            [Column("DSP_AGREEMENT_TYPE")] public string DSP_AGREEMENT_TYPE { get; set; }
            [Column("DSP_COINSURANCE_RATIO")] public decimal? DSP_COINSURANCE_RATIO { get; set; }
            [Column("DSP_CURRENCY_ID")] public string DSP_CURRENCY_ID { get; set; }
            [Column("DSP_DAY_LIMIT")] public decimal? DSP_DAY_LIMIT { get; set; }
            [Column("DSP_EXEMPTION_LIMIT")] public decimal? DSP_EXEMPTION_LIMIT { get; set; }
            [Column("DSP_EXEMPTION_TYPE")] public string DSP_EXEMPTION_TYPE { get; set; }
            [Column("DSP_ID")] public long? DSP_ID { get; set; }
            [Column("DSP_NEW_VERSION_ID")] public long? DSP_NEW_VERSION_ID { get; set; }
            [Column("DSP_NUMBER_LIMIT")] public decimal? DSP_NUMBER_LIMIT { get; set; }
            [Column("DSP_PRICE_FACTOR")] public decimal? DSP_PRICE_FACTOR { get; set; }
            [Column("DSP_PRICE_LIMIT")] public decimal? DSP_PRICE_LIMIT { get; set; }
            [Column("DSP_SESSION_COUNT")] public decimal? DSP_SESSION_COUNT { get; set; }
            [Column("DSP_STATUS")] public string DSP_STATUS { get; set; }
            [Column("EMG_AGREEMENT_TYPE")] public string EMG_AGREEMENT_TYPE { get; set; }
            [Column("EMG_COINSURANCE_RATIO")] public decimal? EMG_COINSURANCE_RATIO { get; set; }
            [Column("EMG_CURRENCY_ID")] public string EMG_CURRENCY_ID { get; set; }
            [Column("EMG_DAY_LIMIT")] public decimal? EMG_DAY_LIMIT { get; set; }
            [Column("EMG_EXEMPTION_LIMIT")] public decimal? EMG_EXEMPTION_LIMIT { get; set; }
            [Column("EMG_EXEMPTION_TYPE")] public string EMG_EXEMPTION_TYPE { get; set; }
            [Column("EMG_ID")] public long? EMG_ID { get; set; }
            [Column("EMG_NEW_VERSION_ID")] public long? EMG_NEW_VERSION_ID { get; set; }
            [Column("EMG_NUMBER_LIMIT")] public decimal? EMG_NUMBER_LIMIT { get; set; }
            [Column("EMG_PRICE_FACTOR")] public decimal? EMG_PRICE_FACTOR { get; set; }
            [Column("EMG_PRICE_LIMIT")] public decimal? EMG_PRICE_LIMIT { get; set; }
            [Column("EMG_SESSION_COUNT")] public decimal? EMG_SESSION_COUNT { get; set; }
            [Column("EMG_STATUS")] public string EMG_STATUS { get; set; }
            [Column("IS_MAIN_COVERAGE")] public long? IS_MAIN_COVERAGE { get; set; }
            [Column("MAIN_COVERAGE_ID")] public long? MAIN_COVERAGE_ID { get; set; }
            [Column("NAG_AGREEMENT_TYPE")] public string NAG_AGREEMENT_TYPE { get; set; }
            [Column("NAG_COINSURANCE_RATIO")] public decimal? NAG_COINSURANCE_RATIO { get; set; }
            [Column("NAG_CURRENCY_ID")] public string NAG_CURRENCY_ID { get; set; }
            [Column("NAG_DAY_LIMIT")] public decimal? NAG_DAY_LIMIT { get; set; }
            [Column("NAG_EXEMPTION_LIMIT")] public decimal? NAG_EXEMPTION_LIMIT { get; set; }
            [Column("NAG_EXEMPTION_TYPE")] public string NAG_EXEMPTION_TYPE { get; set; }
            [Column("NAG_ID")] public long? NAG_ID { get; set; }
            [Column("NAG_NEW_VERSION_ID")] public long? NAG_NEW_VERSION_ID { get; set; }
            [Column("NAG_NUMBER_LIMIT")] public decimal? NAG_NUMBER_LIMIT { get; set; }
            [Column("NAG_PRICE_FACTOR")] public decimal? NAG_PRICE_FACTOR { get; set; }
            [Column("NAG_PRICE_LIMIT")] public decimal? NAG_PRICE_LIMIT { get; set; }
            [Column("NAG_SESSION_COUNT")] public decimal? NAG_SESSION_COUNT { get; set; }
            [Column("NAG_STATUS")] public string NAG_STATUS { get; set; }
            [Column("NETWORK_ID")] public long? NETWORK_ID { get; set; }
            [Column("NETWORK_NAME")] public string NETWORK_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Key][Column("PLAN_COVERAGE_ID")] public long? PLAN_COVERAGE_ID { get; set; }
            [Column("PLAN_ID")] public long? PLAN_ID { get; set; }
            [Column("PLAN_NAME")] public string PLAN_NAME { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SUBPRODUCT_CODE")] public string SUBPRODUCT_CODE { get; set; }
            [Column("SUBPRODUCT_ID")] public long? SUBPRODUCT_ID { get; set; }

        }

        [Table("V_COVERAGE")]
        public class V_Coverage
        {
            [Column("BRANCH_ID")] public long? BRANCH_ID { get; set; }
            [Column("BRANCH_NAME")] public string BRANCH_NAME { get; set; }
            [Key][Column("COVERAGE_ID")] public long? COVERAGE_ID { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Column("COVERAGE_TYPE")] public string COVERAGE_TYPE { get; set; }
            [Column("IS_TAX")] public string IS_TAX { get; set; }
            [Column("LOCATION_TYPE")] public string LOCATION_TYPE { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_CLAIM_COVERAGE_PORTSS_PRM")]
        public class V_ClaimCoveragePortTSSParameter
        {
            [Column("CLAIM_ID")] public long CLAIM_ID { get; set; }
            [Key][Column("COVERAGE_ID")] public long COVERAGE_ID { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Column("COVERAGE_TYPE")] public string COVERAGE_TYPE { get; set; }
            [Column("MAIN_COVERAGE_ID")] public long? MAIN_COVERAGE_ID { get; set; }
            [Column("MAIN_COVERAGE_NAME")] public string MAIN_COVERAGE_NAME { get; set; }
            [Column("MAIN_COVERAGE_TYPE")] public string MAIN_COVERAGE_TYPE { get; set; }
            [Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Column("VALUE")] public string VALUE { get; set; }

        }

        [Table("V_COVERAGE_PARAMETER")]
        public class V_CoverageParameter
        {
            [Key]
            [Column("BRANCH_ID")]
            public Int64 BranchId { get; set; }
            [Column("BRANCH_NAME")]
            public string BranchName { get; set; }

            [Column("COVERAGE_ID")]
            public Int64 CoverageId { get; set; }
            [Column("COVERAGE_NAME")]
            public string CoverageName { get; set; }
            [Column("COVERAGE_PARAMETER_ID")]
            public string CoverageParameterId { get; set; }
            [Column("ENVIRONMENT")]
            public string Environment { get; set; }

            [Column("KEY")]
            public string Key { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("PARAMETER_ID")]
            public Int64 ParameterId { get; set; }

            [Column("SP_RESPONSE_ID")]
            public Int64? SpResponseId { get; set; }
            [Column("STATUS")]
            public string Status { get; set; }
            [Column("SYSTEM_TYPE")]
            public string SystemType { get; set; }

            [Column("VALUE")]
            public string Value { get; set; }
        }
        [Table("V_NETWORK")]
        public class V_NETWORK
        {
            [Column("NETWORK_CATEGORY")]
            public string NetworkCategory { get; set; }
            [Column("NETWORK_ID")]
            [Key]
            public Int64? NETWORK_ID { get; set; }
            [Column("NETWORK_NAME")]
            public string NETWORK_NAME { get; set; }
            [Column("NETWORK_TYPE")]
            public string NetworkType { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64? NewVersionId { get; set; }
            [Column("SP_RESPONSE_ID")]
            public Int64? SpResponseId { get; set; }
            [Column("STATUS")]
            public string Status { get; set; }
        }
        [Table("V_EXPORT_NETWORK")]
        public class V_ExportNetwork : CommonEntity
        {

            [Column("NETWORK_NAME")]
            public string NETWORK_NAME { get; set; }
            [Column("NETWORK_TYPE")]
            public string NETWORK_TYPE { get; set; }
            [Column("NETWORK_CATEGORY")]
            public string NetworkCategory { get; set; }

            [Column("NETWORK_ID")]
            [Key]
            public Int64? NETWORK_ID { get; set; }

            //[Column("NEW_VERSION_ID")]
            //public Int64? NewVersionId { get; set; }
            //[Column("STATUS")]
            //public string Status { get; set; }
        }
        [Table("V_INSURED_COVERAGE_RULE")]
        public class V_InsuredCoverageRule
        {
            [Column("AGREEMENT_TYPE")]
            public string AgreementType { get; set; }
            [Column("COINSURANCE_RATIO")]
            public int? CoinsuranceRatio { get; set; }
            [Column("COVERAGE_ID")]
            public Int64 CoverageId { get; set; }
            [Column("COVERAGE_TYPE")]
            public string CoverageType { get; set; }
            [Column("COVERAGE_NAME")]
            [MaxLength(100, ErrorMessage = "")]
            public string CoverageName { get; set; }
            [Column("CURRENCY_TYPE")]
            public string CurrencyType { get; set; }

            [Column("DAY_LIMIT")]
            public int? DayLimit { get; set; }
            [Column("DAY_LIMIT_REMAINING")]
            public int? DayLimitRemaining { get; set; }
            [Column("EVENT_DATE")]
            public DateTime? EventDate { get; set; }
            [Column("EXEMPTION_TYPE")]
            public string ExemptionType { get; set; }
            [Column("EXEMPTION_LIMIT")]
            public Decimal? ExemptionLimit { get; set; }
            [Column("EXEMPTION_LIMIT_REMAINING")]
            public string ExemptionLimitRemaining { get; set; }
            [Key][Column("INSURED_ID")]
            public Int64 InsuredId { get; set; }
            [Column("NETWORK_ID")]
            public Int64 NetworkId { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("NUMBER_LIMIT")]
            public int? NumberLimit { get; set; }
            [Column("NUMBER_LIMIT_REMAINING")]
            public int? NumberLimitRemaining { get; set; }
            [Column("PACKAGE_ID")]
            public Int64? PackageId { get; set; }

            [Column("PLAN_COVERAGE_ID")]
            public Int64 PlanCoverageId { get; set; }
            [Column("PLAN_COVERAGE_VARIATION_ID")]
            public Int64? PlanCoverageVariationId { get; set; }
            [Column("PLAN_COVERAGE_VARIATION_TYPE")]
            public string PlanCoverageVariationType { get; set; }
            [Column("PRICE_FACTOR")]
            public Decimal? PriceFactor { get; set; }
            [Column("PRICE_LIMIT")]
            public Decimal? PriceLimit { get; set; }
            [Column("PRICE_LIMIT_REMAINING")]
            public Decimal? PriceLimitRemaining { get; set; }
            [Column("PROVIDER_ID")]
            public Int64 ProviderId { get; set; }
            [Column("SP_RESPONSE_ID")]
            public Int64? SpResponseId { get; set; }
            [Column("STATUS")]
            public string Status { get; set; }
        }

        [Table("V_RULE_SUBPRODUCT")]
        public class V_RuleSubProduct
        {
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("RESULT")] public string RESULT { get; set; }
            [Column("RESULT_TYPE")] public string RESULT_TYPE { get; set; }
            [Column("RULE_CATEGORY_ID")] public long? RULE_CATEGORY_ID { get; set; }
            [Column("RULE_CATEGORY_TYPE")] public string RULE_CATEGORY_TYPE { get; set; }
            [Column("RULE_DATA")] public string RULE_DATA { get; set; }
            [Column("RULE_GROUP_ID")] public long? RULE_GROUP_ID { get; set; }
            [Column("RULE_GROUP_NAME")] public string RULE_GROUP_NAME { get; set; }
            [Column("RULE_ID")] public long? RULE_ID { get; set; }
            [Column("RULE_NAME")] public string RULE_NAME { get; set; }
            [Key][Column("RULE_SUBPRODUCT_ID")] public long? RULE_SUBPRODUCT_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SUBPRODUCT_CODE")] public string SUBPRODUCT_CODE { get; set; }
            [Column("SUBPRODUCT_ID")] public long? SUBPRODUCT_ID { get; set; }
            [Column("SUBPRODUCT_NAME")] public string SUBPRODUCT_NAME { get; set; }
            [Column("TYPE")] public string TYPE { get; set; }

        }

        [Table("V_RULE_POLICY")]
        public class V_RulePolicy
        {
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("POLICY_ID")] public long? POLICY_ID { get; set; }
            [Column("POLICY_NUMBER")] public Int64? POLICY_NUMBER { get; set; }
            [Column("RENEWAL_NO")] public long? RENEWAL_NO { get; set; }
            [Column("RESULT")] public string RESULT { get; set; }
            [Column("RESULT_TYPE")] public string RESULT_TYPE { get; set; }
            [Column("RULE_CATEGORY_ID")] public long? RULE_CATEGORY_ID { get; set; }
            [Column("RULE_CATEGORY_TYPE")] public string RULE_CATEGORY_TYPE { get; set; }
            [Column("RULE_DATA")] public string RULE_DATA { get; set; }
            [Column("RULE_GROUP_ID")] public long? RULE_GROUP_ID { get; set; }
            [Column("RULE_GROUP_NAME")] public string RULE_GROUP_NAME { get; set; }
            [Column("RULE_ID")] public long? RULE_ID { get; set; }
            [Column("RULE_NAME")] public string RULE_NAME { get; set; }
            [Key][Column("RULE_POLICY_ID")] public long? RULE_POLICY_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("TYPE")] public string TYPE { get; set; }

        }

        [Table("V_RULE_POLICY_GROUP")]
        public class V_RulePolicyGroup
        {
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("POLICY_GROUP_ID")] public long? POLICY_GROUP_ID { get; set; }
            [Column("POLICY_GROUP_NAME")] public string POLICY_GROUP_NAME { get; set; }
            [Column("RESULT")] public string RESULT { get; set; }
            [Column("RESULT_TYPE")] public string RESULT_TYPE { get; set; }
            [Column("RULE_CATEGORY_ID")] public long? RULE_CATEGORY_ID { get; set; }
            [Column("RULE_CATEGORY_TYPE")] public string RULE_CATEGORY_TYPE { get; set; }
            [Column("RULE_DATA")] public string RULE_DATA { get; set; }
            [Column("RULE_GROUP_ID")] public long? RULE_GROUP_ID { get; set; }
            [Column("RULE_GROUP_NAME")] public string RULE_GROUP_NAME { get; set; }
            [Column("RULE_ID")] public long? RULE_ID { get; set; }
            [Column("RULE_NAME")] public string RULE_NAME { get; set; }
            [Key][Column("RULE_POLICY_GROUP_ID")] public long? RULE_POLICY_GROUP_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("TYPE")] public string TYPE { get; set; }

        }

        [Table("V_RULE_PACKAGE")]
        public class V_RulePackage
        {
            [Column("BRANCH_ID")] public long? BRANCH_ID { get; set; }
            [Column("BRANCH_NAME")] public string BRANCH_NAME { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Column("PACKAGE_NAME")] public string PACKAGE_NAME { get; set; }
            [Column("PACKAGE_NO")] public string PACKAGE_NO { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("RESULT")] public string RESULT { get; set; }
            [Column("RESULT_TYPE")] public string RESULT_TYPE { get; set; }
            [Column("RULE_CATEGORY_ID")] public long? RULE_CATEGORY_ID { get; set; }
            [Column("RULE_CATEGORY_TYPE")] public string RULE_CATEGORY_TYPE { get; set; }
            [Column("RULE_DATA")] public string RULE_DATA { get; set; }
            [Column("RULE_GROUP_ID")] public long? RULE_GROUP_ID { get; set; }
            [Column("RULE_GROUP_NAME")] public string RULE_GROUP_NAME { get; set; }
            [Column("RULE_ID")] public long? RULE_ID { get; set; }
            [Column("RULE_NAME")] public string RULE_NAME { get; set; }
            [Key][Column("RULE_PACKAGE_ID")] public long? RULE_PACKAGE_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SUBPRODUCT_CODE")] public string SUBPRODUCT_CODE { get; set; }
            [Column("SUBPRODUCT_ID")] public long? SUBPRODUCT_ID { get; set; }
            [Column("TYPE")] public string TYPE { get; set; }

        }

        [Table("V_RULE_PRODUCT")]
        public class V_RuleProduct
        {
            [Column("BRANCH_ID")] public long? BRANCH_ID { get; set; }
            [Column("BRANCH_NAME")] public string BRANCH_NAME { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_DESCRIPTION")] public string PRODUCT_DESCRIPTION { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("RESULT")] public string RESULT { get; set; }
            [Column("RESULT_TYPE")] public string RESULT_TYPE { get; set; }
            [Column("RULE_CATEGORY_ID")] public long? RULE_CATEGORY_ID { get; set; }
            [Column("RULE_CATEGORY_TYPE")] public string RULE_CATEGORY_TYPE { get; set; }
            [Column("RULE_DATA")] public string RULE_DATA { get; set; }
            [Column("RULE_GROUP_ID")] public long? RULE_GROUP_ID { get; set; }
            [Column("RULE_GROUP_NAME")] public string RULE_GROUP_NAME { get; set; }
            [Column("RULE_ID")] public long? RULE_ID { get; set; }
            [Column("RULE_NAME")] public string RULE_NAME { get; set; }
            [Key] [Column("RULE_PRODUCT_ID")] public long? RULE_PRODUCT_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("TYPE")] public string TYPE { get; set; }

        }

        [Table("V_PRODUCT_NOTE")]
        public class V_ProductNote
        {
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("NOTE_DESCRIPTION")] public string NOTE_DESCRIPTION { get; set; }
            [Column("NOTE_ID")] public long? NOTE_ID { get; set; }
            [Column("NOTE_TYPE")] public string NOTE_TYPE { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_DESCRIPTION")] public string PRODUCT_DESCRIPTION { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Key] [Column("PRODUCT_NOTE_ID")] public long? PRODUCT_NOTE_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_PRODUCT_PARAMETER")]
        public class V_ProductParameter
        {
            [Column("ENVIRONMENT")] public string ENVIRONMENT { get; set; }
            [Column("KEY")] public string KEY { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PARAMETER_ID")] public long? PARAMETER_ID { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_DESCRIPTION")] public string PRODUCT_DESCRIPTION { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Key] [Column("PRODUCT_PARAMETER_ID")] public long? PRODUCT_PARAMETER_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SYSTEM_TYPE")] public string SYSTEM_TYPE { get; set; }
            [Column("VALUE")] public string VALUE { get; set; }

        }

        [Table("V_PRODUCT")]
        public class V_Product
        {
            [Column("BRANCH_ID")] public long? BRANCH_ID { get; set; }
            [Column("BRANCH_NAME")] public string BRANCH_NAME { get; set; }
            [Column("COMPANY_ID")] public long COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("COMPANY_STATUS")] public string COMPANY_STATUS { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_DESCRIPTION")] public string PRODUCT_DESCRIPTION { get; set; }
            [Key][Column("PRODUCT_ID")] public long PRODUCT_ID { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("PRODUCT_TYPE")] public string PRODUCT_TYPE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_PAYROLL")]
        public class V_Payroll
        {
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("EXT_PROVIDER_DATE")] public DateTime? EXT_PROVIDER_DATE { get; set; }
            [Column("EXT_PROVIDER_NO")] public string EXT_PROVIDER_NO { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PAYMENT_DATE")] public DateTime? PAYMENT_DATE { get; set; }
            [Column("DUE_DATE")] public DateTime? DUE_DATE { get; set; }
            [Column("PAYROLL_CATEGORY")] public string PAYROLL_CATEGORY { get; set; }
            [Column("PAYROLL_DATE")] public DateTime? PAYROLL_DATE { get; set; }
            [Key] [Column("PAYROLL_ID")] public long PAYROLL_ID { get; set; }
            [Column("OLD_PAYROLL_ID")] public long? OLD_PAYROLL_ID { get; set; }
            [Column("PAYROLL_TYPE")] public string PAYROLL_TYPE { get; set; }
            [Column("PROVIDER_ID")] public long? PROVIDER_ID { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Column("PROVIDER_GROUP_ID")] public long? PROVIDER_GROUP_ID { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_GROUP_NAME { get; set; }
            [Column("PROVIDER_TITLE")] public string PROVIDER_TITLE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("COMPANY_PAYROLL_NO")] public string COMPANY_PAYROLL_NO { get; set; }
            [Column("INSURED")] public string INSURED { get; set; }
            [Column("COMPANY_PAYROLL_ID")] public long? COMPANY_PAYROLL_ID { get; set; }
            [Column("TOTAL_CLAIM")] public int? TOTAL_CLAIM { get; set; }
            [Column("TOTAL_AMOUNT")] public decimal? TOTAL_AMOUNT { get; set; }
        }
        [Table("V_STAFF")]
        public class V_Staff : CommonEntity
        {
            [Column("BIRTHDATE")]
            public DateTime? BIRTHDATE { get; set; }
            [Column("BIRTHPLACE")] public string BIRTHPLACE { get; set; }
            [Column("NAME_OF_FATHER")] public string NAME_OF_FATHER { get; set; }
            [Column("GENDER")] public string GENDER { get; set; }
            [Column("PASSPORT_NO")] public string PASSPORT_NO { get; set; }
            [Column("TAX_NUMBER")] public Int64? TAX_NUMBER { get; set; }
            [Column("CONTACT_ID")]
            public Int64 CONTACT_ID { get; set; }
            [Column("CONTRACT_TYPE")] public string CONTRACT_TYPE { get; set; }
            [Column("DOCTOR_BRANCH_ID")]
            public Int64? DOCTOR_BRANCH_ID { get; set; }
            [Column("DIPLOMA_NO")]
            [MaxLength(20, ErrorMessage = "")]
            public string DIPLOMA_NO { get; set; }
            [Column("DOCTOR_BRANCH_NAME")]
            public string DOCTOR_BRANCH_NAME { get; set; }
            [Column("EMAIL_ID")]
            public Int64? EMAIL_ID { get; set; }
            [Column("EMAIL_ADDRESS")]
            public string EMAIL_ADDRESS { get; set; }
            [Column("EXTENSION")]
            public string EXTENSION { get; set; }
            [Column("FIRST_NAME")]
            public string FIRST_NAME { get; set; }
            [Column("IDENTITY_NO")] public Int64? IDENTITY_NO { get; set; }
            [Column("IS_GROUP_ADMIN")]
            public string IS_GROUP_ADMIN { get; set; }
            [Column("LAST_NAME")]
            public string LAST_NAME { get; set; }
            [Column("MIDDLE_NAME")]
            public string MIDDLE_NAME { get; set; }
            [Column("MOBILE_PHONE_ID")]
            public Int64? MOBILE_PHONE_ID { get; set; }
            [Column("MOBILE_PHONE_NO")]
            public string MOBILE_PHONE_NO { get; set; }
            [Column("PERSON_ID")]
            public Int64 PERSON_ID { get; set; }
            [Column("PHONE_ID")]
            public Int64 PHONE_ID { get; set; }
            [Column("PHONE_NO")]
            public string PHONE_NO { get; set; }
            [Column("PROVIDER_ID")]
            public Int64 PROVIDER_ID { get; set; }
            [Column("PROVIDER_NAME")]
            public string PROVIDER_NAME { get; set; }
            [Column("PROVIDER_TITLE")]
            public string PROVIDER_TITLE { get; set; }
            [Column("REGISTER_NO")]
            public string REGISTER_NO { get; set; }
            [Column("SP_RESPONSE_ID")]
            public Int64? SP_RESPONSE_ID { get; set; }
            [Key]
            [Column("STAFF_ID")]
            public Int64 STAFF_ID { get; set; }
            [Column("STAFF_IDENTITY_NO")]
            public string STAFF_IDENTITY_NO { get; set; }
            [Column("STAFF_TITLE")]
            public string STAFF_TITLE { get; set; }
            [Column("STAFF_TYPE")]
            public string STAFF_TYPE { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
        }
        [Table("V_DOCTOR")]
        public class V_Doctor : CommonEntity
        {
            [Column("CONTACT_ID")]
            public Int64 CONTACT_ID { get; set; }
            [Column("DOCTOR_BRANCH_ID")]
            public Int64? DOCTOR_BRANCH_ID { get; set; }
            [Column("DIPLOMA_NO")]
            [MaxLength(20, ErrorMessage = "")]
            public string DIPLOMA_NO { get; set; }
            [Column("DOCTOR_BRANCH_NAME")]
            public string DOCTOR_BRANCH_NAME { get; set; }
            [Column("FIRST_NAME")]
            public string FIRST_NAME { get; set; }
            [Column("LAST_NAME")]
            public string LAST_NAME { get; set; }
            [Column("MIDDLE_NAME")]
            public string MIDDLE_NAME { get; set; }
            [Column("PROVIDER_ID")]
            public Int64 PROVIDER_ID { get; set; }
            [Column("REGISTER_NO")]
            public string REGISTER_NO { get; set; }
            [Key]
            [Column("STAFF_ID")]
            public Int64 STAFF_ID { get; set; }
            [Column("STAFF_IDENTITY_NO")]
            public Int64? STAFF_IDENTITY_NO { get; set; }
            [Column("STAFF_TITLE")]
            public string STAFF_TITLE { get; set; }
            [Column("STAFF_TYPE")]
            public string STAFF_TYPE { get; set; }
        }
        [Table("V_RPT_CLAIM_PREMIUM")]
        public class V_RptClaimPremium
        {
            [Column("ACENTEAD")] public string ACENTEAD { get; set; }
            [Column("ACENTENO")] public string ACENTENO { get; set; }
            [Column("ACNILAD")] public string ACNILAD { get; set; }
            [Column("ACNILCE")] public string ACNILCE { get; set; }
            [Column("ACNILKOD")] public long? ACNILKOD { get; set; }
            [Column("AGENCY_ID")] public long? AGENCY_ID { get; set; }
            [Column("BASTAR")] public DateTime? BASTAR { get; set; }
            [Column("BITTAR")] public DateTime? BITTAR { get; set; }
            [Column("BRYTIP")] public string BRYTIP { get; set; }
            [Column("FATTUTARI")] public decimal? FATTUTARI { get; set; }
            [Column("GRUPID")] public long? GRUPID { get; set; }
            [Column("HOLDINGAD")] public string HOLDINGAD { get; set; }
            [Column("HOLDINGID")] public long? HOLDINGID { get; set; }
            [Column("HPORAN")] public decimal? HPORAN { get; set; }
            [Column("INDIVIDUAL_TYPE")] public string INDIVIDUAL_TYPE { get; set; }
            [Column("IPTALTAR")] public string IPTALTAR { get; set; }
            [Column("KABULTUTAR")] public decimal? KABULTUTAR { get; set; }
            [Column("KALANSURE")] public int? KALANSURE { get; set; }
            [Column("PRODUCT_TYPE")] public int? PRODUCT_TYPE { get; set; }
            [Column("KAZPRIM")] public decimal? KAZPRIM { get; set; }
            [Column("KOMISYON")] public string KOMISYON { get; set; }
            [Column("ONAYTUTAR")] public decimal? ONAYTUTAR { get; set; }
            [Column("ORT_YAS")] public long? ORT_YAS { get; set; }
            [Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Column("PAKETAD")] public string PAKETAD { get; set; }
            [Column("PLAN")] public string PLAN { get; set; }
            [Column("POLICEDURUMU")] public string POLICEDURUMU { get; set; }
            [Column("POLICENO")] public string POLICENO { get; set; }
            [Column("POLICETIPI")] public string POLICETIPI { get; set; }
            [Column("POLICY_STATUS")] public string POLICY_STATUS { get; set; }
            [Column("POLICY_TYPE")] public string POLICY_TYPE { get; set; }
            [Column("POLID")] public long? POLID { get; set; }
            [Column("PORTNO")] public long? PORTNO { get; set; }
            [Column("PRIM")] public decimal? PRIM { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("SIGETID")] public long? SIGETID { get; set; }
            [Column("SIGETTIREN")] public string SIGETTIREN { get; set; }
            [Column("SIGILAD")] public string SIGILAD { get; set; }
            [Column("SIGILCE")] public string SIGILCE { get; set; }
            [Column("SIGILKOD")] public long? SIGILKOD { get; set; }
            [Column("SIGORTALI")] public string SIGORTALI { get; set; }
            [Column("SIRKETAD")] public string SIRKETAD { get; set; }
            [Key] [Column("SIRKETKOD")] public long? SIRKETKOD { get; set; }
            [Column("SIRKETTUTAR")] public decimal? SIRKETTUTAR { get; set; }
            [Column("TALEPTUTAR")] public decimal? TALEPTUTAR { get; set; }
            [Column("TANTAR")] public DateTime? TANTAR { get; set; }
            [Column("TCKN")] public string TCKN { get; set; }
            [Column("TOPLAM_SURE")] public long? TOPLAM_SURE { get; set; }
            [Column("URUN")] public string URUN { get; set; }
            [Column("VERGI")] public decimal? VERGI { get; set; }
            [Column("YENIISYENILEME")] public string YENIISYENILEME { get; set; }
            [Column("YENILEMENO")] public long? YENILEMENO { get; set; }
            [Column("ZEYLBASTAR")] public DateTime? ZEYLBASTAR { get; set; }

        }
        [Table("V_RPT_PAYROLL")]
        public class V_RptPayroll
        {
            [Column("CLAIM_COUNT")] public long? CLAIM_COUNT { get; set; }
            [Column("CLAIM_STATUS")] public long? CLAIM_STATUS { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("COMPANY_PAYROLL_ID")] public long? COMPANY_PAYROLL_ID { get; set; }
            [Column("DUE_DATE")] public DateTime? DUE_DATE { get; set; }
            [Column("EXT_PROVIDER_NO")] public string EXT_PROVIDER_NO { get; set; }
            [Column("INSURED_ID")] public long? INSURED_ID { get; set; }
            [Column("OLD_PAYROLL_ID")] public long? OLD_PAYROLL_ID { get; set; }
            [Column("PAID_AMOUNT")] public decimal? PAID_AMOUNT { get; set; }
            [Column("PAYROLL_CATEGORY")] public string PAYROLL_CATEGORY { get; set; }
            [Column("PAYROLL_CATEGORY_TEXT")] public string PAYROLL_CATEGORY_TEXT { get; set; }
            [Key][Column("PAYROLL_ID")] public long PAYROLL_ID { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Column("PROVIDER_GROUP_ID")] public Int64? PROVIDER_GROUP_ID { get; set; }
            [Column("PAYROLL_STATUS")] public string PAYROLL_STATUS { get; set; }
            [Column("PAYROLL_STATUS_TEXT")] public string PAYROLL_STATUS_TEXT { get; set; }
            [Column("PAYROLL_TYPE")] public string PAYROLL_TYPE { get; set; }
            [Column("PAYROLL_TYPE_TEXT")] public string PAYROLL_TYPE_TEXT { get; set; }
            [Column("PROVIDER_ID")] public long? PROVIDER_ID { get; set; }
            [Column("REQUESTED_AMOUNT")] public decimal? REQUESTED_AMOUNT { get; set; }
            [Column("USERNAME")] public string USERNAME { get; set; }
            [Column("VERIFICATION_DATE")] public DateTime? VERIFICATION_DATE { get; set; }
            [Column("PAYMENT_DATE")] public DateTime? PAYMENT_DATE { get; set; }
            [Column("PAYROLL_DATE")] public DateTime? PAYROLL_DATE { get; set; }
            //[Column("PROVIDER_GROUP_NAME")] public string PROVIDER_GROUP_NAME { get; set; }
            //[Column("PROVIDER_GROUP_ID")] public int PROVIDER_GROUP_ID { get; set; }
            //[Column("TAX_NUMBER")] public Int64? TAX_NUMBER { get; set; }
            //[Column("PROVIDER_TITLE")] public string PROVIDER_TITLE { get; set; }
        }

        [Table("T_TABLE_COLUMN_HEADER")]
        public class TableColumnHeader
        {
            [Column("COLUMN_HEADER")] public string COLUMN_HEADER { get; set; }
            [Column("COLUMN_NAME")] public string COLUMN_NAME { get; set; }
            [Key][Column("ID")] public long Id { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("ORDER_NUM")] public int? ORDER_NUM { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("TABLE_HEADER")] public string TABLE_HEADER { get; set; }
            [Column("TABLE_NAME")] public string TABLE_NAME { get; set; }
        }

        [Table("V_RPT_PROVISION_LIST")]
        public class V_RptProvisionList
        {
            [Column("ACENTEBOLGE")] public string ACENTEBOLGE { get; set; }
            [Column("ACENTENO")] public long? ACENTENO { get; set; }
            [Column("BANKAADI")] public string BANKAADI { get; set; }
            [Column("BANK_ACCOUNT_ID")] public long? BANK_ACCOUNT_ID { get; set; }
            [Column("BEYAN")] public string BEYAN { get; set; }
            [Column("BRYTIP")] public string BRYTIP { get; set; }
            [Column("CLAIM_SOURCE_TYPE")] public string CLAIM_SOURCE_TYPE { get; set; }
            [Column("CLAIM_STATUS")] public string CLAIM_STATUS { get; set; }
            [Column("CLAIM_TYPE")] public string CLAIM_TYPE { get; set; }
            [Column("DOKTORADI")] public string DOKTORADI { get; set; }
            [Column("EPIKRIZ_NOTU")] public string EPIKRIZ_NOTU { get; set; }
            [Column("ESKI_ICMALNO")] public long? ESKI_ICMALNO { get; set; }
            [Column("FATURANO")] public string FATURANO { get; set; }
            [Column("FATURATARIHI")] public DateTime? FATURATARIHI { get; set; }
            [Column("FATURATUTAR")] public long? FATURATUTAR { get; set; }
            [Column("GRUPID")] public long? GRUPID { get; set; }
            [Column("HASARTAR")] public DateTime? HASARTAR { get; set; }
            [Column("HESAPADI")] public string HESAPADI { get; set; }
            [Column("HESAPNO")] public string HESAPNO { get; set; }
            [Column("HSRALTSTAT")] public string HSRALTSTAT { get; set; }
            [Column("HSRSTATAD")] public string HSRSTATAD { get; set; }
            [Column("HSRSTATTAR")] public DateTime? HSRSTATTAR { get; set; }
            [Column("IBANNO")] public string IBANNO { get; set; }
            [Column("ICMALNO")] public long? ICMALNO { get; set; }
            [Column("ICMALTAR")] public DateTime? ICMALTAR { get; set; }
            [Column("ID")] public long? ID { get; set; }
            [Column("IHBARTAR")] public DateTime? IHBARTAR { get; set; }
            [Column("ILAC_HIZMET_BILGISI")] public string ILAC_HIZMET_BILGISI { get; set; }
            [Column("ISTISNA")] public string ISTISNA { get; set; }
            [Column("KAYNAKADI")] public string KAYNAKADI { get; set; }
            [Column("KURUMADI")] public string KURUMADI { get; set; }
            [Column("KURUMGRUP")] public long? KURUMGRUP { get; set; }
            [Column("KURUMGRUPAD")] public string KURUMGRUPAD { get; set; }
            [Column("KURUMTICARIUNVAN")] public string KURUMTICARIUNVAN { get; set; }
            [Column("KURUMTIPI")] public string KURUMTIPI { get; set; }
            [Column("KURUMVKN")] public long? KURUMVKN { get; set; }
            [Column("KURUM_ADRES")] public string KURUM_ADRES { get; set; }
            [Column("KURUM_IL")] public string KURUM_IL { get; set; }
            [Column("KURUM_ILCE")] public string KURUM_ILCE { get; set; }
            [Column("MEDULATAKIPNO")] public string MEDULATAKIPNO { get; set; }
            [Column("NORMAL_NOT")] public string NORMAL_NOT { get; set; }
            [Column("NOTLAR")] public string NOTLAR { get; set; }
            [Column("ODEMETARIHI")] public DateTime? ODEMETARIHI { get; set; }
            [Column("ODEMEYERIACIKLAMA")] public string ODEMEYERIACIKLAMA { get; set; }
            [Column("ODENEBILIRTUTAR")] public long? ODENEBILIRTUTAR { get; set; }
            [Column("ODENENTUTAR")] public long? ODENENTUTAR { get; set; }
            [Column("POLBASTAR")] public DateTime? POLBASTAR { get; set; }
            [Column("POLBITTAR")] public DateTime? POLBITTAR { get; set; }
            [Column("POLICENO")] public long? POLICENO { get; set; }
            [Column("POLICETIPI")] public string POLICETIPI { get; set; }
            [Column("POLTANTAR")] public DateTime? POLTANTAR { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("PROVIDER_ID")] public long? PROVIDER_ID { get; set; }
            [Column("PROVIDER_TYPE")] public string PROVIDER_TYPE { get; set; }
            [Column("PROVIZYON_NO")] public long? PROVIZYON_NO { get; set; }
            [Column("SIGETID")] public long? SIGETID { get; set; }
            [Column("SIGORTALIPAYI")] public long? SIGORTALIPAYI { get; set; }
            [Column("SIGORTALI_ADI")] public string SIGORTALI_ADI { get; set; }
            [Column("SIGORTALI_SOYADI")] public string SIGORTALI_SOYADI { get; set; }
            [Column("SIGORTALI_YAS")] public long? SIGORTALI_YAS { get; set; }
            [Column("SIGORTA_ETTIREN")] public string SIGORTA_ETTIREN { get; set; }
            [Column("SIRKETAD")] public string SIRKETAD { get; set; }
            [Column("SIRKETKOD")] public long? SIRKETKOD { get; set; }
            [Column("SUBEADI")] public string SUBEADI { get; set; }
            [Column("SUBPRODUCT_ID")] public long? SUBPRODUCT_ID { get; set; }
            [Column("TANIBILGILERI")] public string TANIBILGILERI { get; set; }
            [Column("TANI_NOTU")] public string TANI_NOTU { get; set; }
            [Column("TCKN")] public long? TCKN { get; set; }
            [Column("TEDAVI_NOTU")] public string TEDAVI_NOTU { get; set; }
            [Column("URUNADI")] public string URUNADI { get; set; }
            [Column("URUNKODU")] public string URUNKODU { get; set; }
            [Column("UZMANLIKALANI")] public string UZMANLIKALANI { get; set; }
            [Column("YENILEMENO")] public long? YENILEMENO { get; set; }

        }

        [Table("V_RPT_PRODUCTION")]
        public class V_RptProduction
        {
            [Column("ACENTEADI")] public string ACENTEADI { get; set; }
            [Column("ACENTEADRES")] public string ACENTEADRES { get; set; }
            [Column("ACENTENO")] public string ACENTENO { get; set; }
            [Column("ACENTETIP")] public string ACENTETIP { get; set; }
            [Column("AILE_NO")] public long? AILE_NO { get; set; }
            [Column("BRYTIP")] public string BRYTIP { get; set; }
            [Column("CINS")] public string CINS { get; set; }
            [Column("CONTACT_ID")] public long? CONTACT_ID { get; set; }
            [Column("DOGUMTEMTAR")] public DateTime? DOGUMTEMTAR { get; set; }
            [Column("ENDORSEMENT_ID")] public long? ENDORSEMENT_ID { get; set; }
            [Column("GENDER")] public string GENDER { get; set; }
            [Column("GIRISTAR")] public DateTime? GIRISTAR { get; set; }
            [Column("GRUPID")] public long? GRUPID { get; set; }
            [Column("GRUPZEYLNO")] public long? GRUPZEYLNO { get; set; }
            [Column("HOLDINGAD")] public string HOLDINGAD { get; set; }
            [Column("HOLDINGID")] public long? HOLDINGID { get; set; }
            [Column("ILKSIGAYKTAR")] public DateTime? ILKSIGAYKTAR { get; set; }
            [Column("ILKSIGORTATAR")] public DateTime? ILKSIGORTATAR { get; set; }
            [Column("INDIVIDUAL_TYPE")] public string INDIVIDUAL_TYPE { get; set; }
            [Column("INSURED_ID")] public long? INSURED_ID { get; set; }
            [Column("INSURED_STATUS")] public string INSURED_STATUS { get; set; }
            [Column("IS_VIP")] public string IS_VIP { get; set; }
            [Column("KAZANILMIS_PRIM")] public decimal? KAZANILMIS_PRIM { get; set; }
            [Column("KOMISYON")] public decimal? KOMISYON { get; set; }
            [Column("MUAFIYET")] public string MUAFIYET { get; set; }
            [Column("NATIONALITY_ID")] public long? NATIONALITY_ID { get; set; }
            [Column("NET_RISK_PRIM")] public decimal? NET_RISK_PRIM { get; set; }
            [Column("OSSTAR")] public string OSSTAR { get; set; }
            [Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Column("PASAPORTNO")] public string PASAPORTNO { get; set; }
            [Column("PERSON_ID")] public long? PERSON_ID { get; set; }
            [Column("POLBASTAR")] public DateTime? POLBASTAR { get; set; }
            [Column("POLBITTAR")] public DateTime? POLBITTAR { get; set; }
            [Column("POLICENO")] public string POLICENO { get; set; }
            [Column("POLICE_DURUMU")] public string POLICE_DURUMU { get; set; }
            [Column("POLICE_TIPI")] public string POLICE_TIPI { get; set; }
            [Column("POLICY_STATUS")] public string POLICY_STATUS { get; set; }
            [Column("POLICY_TYPE")] public string POLICY_TYPE { get; set; }
            [Key][Column("POLID")] public long? POLID { get; set; }
            [Column("POLTANTAR")] public DateTime? POLTANTAR { get; set; }
            [Column("PORTNO")] public long? PORTNO { get; set; }
            [Column("PRIM")] public decimal? PRIM { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("REJITAR")] public DateTime? REJITAR { get; set; }
            [Column("RENEWAL_GUARANTEE_TYPE")] public string RENEWAL_GUARANTEE_TYPE { get; set; }
            [Column("SIGAD")] public string SIGAD { get; set; }
            [Column("SIGAKTIF")] public string SIGAKTIF { get; set; }
            [Column("SIGDOGTAR")] public DateTime? SIGDOGTAR { get; set; }
            [Column("SIGETAD")] public string SIGETAD { get; set; }
            [Column("SIGETID")] public long? SIGETID { get; set; }
            [Column("SIGETTIP")] public string SIGETTIP { get; set; }
            [Column("SIGSOYAD")] public string SIGSOYAD { get; set; }
            [Column("SIGYAS")] public int SIGYAS { get; set; }
            [Column("SIRKETAD")] public string SIRKETAD { get; set; }
            [Column("SIRKETGIRTAR")] public DateTime? SIRKETGIRTAR { get; set; }
            [Column("SIRKETKOD")] public long? SIRKETKOD { get; set; }
            [Column("TAKSIT_SAY")] public long? TAKSIT_SAY { get; set; }
            [Column("TCKN")] public string TCKN { get; set; }
            [Column("TEMPAKETAD")] public string TEMPAKETAD { get; set; }
            [Column("TEMPAKETNO")] public string TEMPAKETNO { get; set; }
            [Column("TPA_PAYI")] public decimal? TPA_PAYI { get; set; }
            [Column("URUN")] public string URUN { get; set; }
            [Column("UYRUK")] public string UYRUK { get; set; }
            [Column("UYRUK_KODU")] public string UYRUK_KODU { get; set; }
            [Column("VERGI")] public decimal? VERGI { get; set; }
            [Column("VERGINO")] public string VERGINO { get; set; }
            [Column("VIP")] public string VIP { get; set; }
            [Column("VIP_TYPE")] public string VIP_TYPE { get; set; }
            [Column("YENIISYENILEME")] public string YENIISYENILEME { get; set; }
            [Column("YENILEMENO")] public long? YENILEMENO { get; set; }
            [Column("YENILEME_GARANTISI_TARIHI")] public DateTime? YENILEME_GARANTISI_TARIHI { get; set; }
            [Column("YGTIP")] public string YGTIP { get; set; }
            [Column("ZEYLBASTAR")] public DateTime? ZEYLBASTAR { get; set; }
            [Column("ZEYLCIKISSEBEBI")] public string ZEYLCIKISSEBEBI { get; set; }
            [Column("ZEYLNO")] public long? ZEYLNO { get; set; }
            [Column("ZEYLTANTAR")] public DateTime? ZEYLTANTAR { get; set; }
            [Column("ZEYLTIPI")] public string ZEYLTIPI { get; set; }
            [Column("SIGETVERGINO")] public string SIGETVERGINO { get; set; }
            [Column("SIGBIRTPLACE")] public string SIGBIRTPLACE { get; set; }
            [Column("SIGFATHERNAME")] public string SIGFATHERNAME { get; set; }
            [Column("SIGADRILNO")] public string SIGADRILNO { get; set; }
            [Column("SIGADRESIL")] public string SIGADRESIL { get; set; }
            [Column("SIGUW")] public string SIGUW { get; set; }
            [Column("POLIPTALTIP")] public string POLIPTALTIP { get; set; }
            [Column("GECIS_BILGI")] public string GECIS_BILGI { get; set; }
            [Column("BEYAN_NOTLAR")] public string BEYAN_NOTLAR { get; set; }
            [Column("BASIM_NOTLAR")] public string BASIM_NOTLAR { get; set; }
            [Column("OZEL_NOTLAR")] public string OZEL_NOTLAR { get; set; }


        }

        public class RptClaimDetails_Sum
        {
            [Column("Poliçe_Adet")] public string POLICY_COUNT { get; set; }
            [Column("Aktif_Sigortalı_Adedi")] public string ACTIVE_INSURED_COUNT { get; set; }
            [Column("Sigortalı_Adedi")] public string INSURED_COUNT { get; set; }
            [Column("Kişi_Sayısı")] public string PERSON_COUNT { get; set; }
            [Column("Zeyil_Adedi")] public string ENDORSEMENT_COUNT { get; set; }
            [Column("Prim")] public long? PREMIUM { get; set; }
            [Column("Net_Risk_Primi")] public string RISK_PREMIUM { get; set; }
            [Column("Komisyon")] public string COMMISSION { get; set; }
            [Column("TPA_Payı")] public string TPA { get; set; }
            [Column("Vergi")] public string TAX { get; set; }
            [Column("Kazanılmış_Prim")] public string TOTAL_PREMIUM { get; set; }
        }
        [Table("V_RPT_ACTIVE_INSURED")]
        public class V_RptActiveInsured
        {
            [Column("ACENTEADI")] public string ACENTEADI { get; set; }
            [Column("ACENTENO")] public long? ACENTENO { get; set; }
            [Column("BEYAN_NOTLAR")] public string BEYAN_NOTLAR { get; set; }
            [Column("BILGI_NOTLAR")] public string BILGI_NOTLAR { get; set; }
            [Column("BRYTIP")] public string BRYTIP { get; set; }
            [Column("CINS")] public string CINS { get; set; }
            [Column("DOGUMTEMTAR")] public DateTime? DOGUMTEMTAR { get; set; }
            [Column("ENDORSEMENT_TYPE")] public string ENDORSEMENT_TYPE { get; set; }
            [Column("GENDER")] public string GENDER { get; set; }
            [Column("GRUPID")] public long? GRUPID { get; set; }
            [Column("GRUPZEYLNO")] public long? GRUPZEYLNO { get; set; }
            [Column("HOLDINGAD")] public string HOLDINGAD { get; set; }
            [Column("HOLDINGID")] public long? HOLDINGID { get; set; }
            [Column("ILKSIGAYKTAR")] public DateTime? ILKSIGAYKTAR { get; set; }
            [Column("ILKSIGORTATAR")] public DateTime? ILKSIGORTATAR { get; set; }
            [Column("INDIVIDUAL_TYPE")] public string INDIVIDUAL_TYPE { get; set; }
            [Column("INSURED_STATUS")] public string INSURED_STATUS { get; set; }
            [Column("IS_VIP")] public string IS_VIP { get; set; }
            [Column("MEDIKAL_NOTLAR")] public string MEDIKAL_NOTLAR { get; set; }
            [Column("MUAFIYET_ACIKLAMA")] public string MUAFIYET_ACIKLAMA { get; set; }
            [Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Column("PASAPORTNO")] public string PASAPORTNO { get; set; }
            [Column("POLBASTAR")] public DateTime? POLBASTAR { get; set; }
            [Column("POLBITTAR")] public DateTime? POLBITTAR { get; set; }
            [Column("POLICENO")] public long? POLICENO { get; set; }
            [Column("POLICE_DURUMU")] public string POLICE_DURUMU { get; set; }
            [Column("POLICE_TIPI")] public string POLICE_TIPI { get; set; }
            [Column("POLICY_STATUS")] public string POLICY_STATUS { get; set; }
            [Column("POLICY_TYPE")] public string POLICY_TYPE { get; set; }
            [Column("POLID")] public long? POLID { get; set; }
            [Column("POLTANTAR")] public DateTime? POLTANTAR { get; set; }
            [Column("PORTNO")] public long? PORTNO { get; set; }
            [Column("PRIM")] public long? PRIM { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("REJITAR")] public DateTime? REJITAR { get; set; }
            [Column("RENEWAL_GUARANTEE_TYPE")] public string RENEWAL_GUARANTEE_TYPE { get; set; }
            [Column("SIGAD")] public string SIGAD { get; set; }
            [Column("SIGAKTIF")] public string SIGAKTIF { get; set; }
            [Column("SIGDOGTAR")] public DateTime? SIGDOGTAR { get; set; }
            [Column("SIGETAD")] public string SIGETAD { get; set; }
            [Column("SIGETID")] public long? SIGETID { get; set; }
            [Column("SIGSOYAD")] public string SIGSOYAD { get; set; }
            [Column("SIRKETAD")] public string SIRKETAD { get; set; }
            [Column("SIRKETGIRTAR")] public DateTime? SIRKETGIRTAR { get; set; }
            [Column("SIRKETKOD")] public long? SIRKETKOD { get; set; }
            [Column("TAZMINAT_NOTLAR")] public string TAZMINAT_NOTLAR { get; set; }
            [Column("TCKN")] public long? TCKN { get; set; }
            [Column("TEMPAKETAD")] public string TEMPAKETAD { get; set; }
            [Column("TEMPAKETNO")] public string TEMPAKETNO { get; set; }
            [Column("URUN")] public string URUN { get; set; }
            [Column("UYRUK")] public string UYRUK { get; set; }
            [Column("UYRUK_KODU")] public string UYRUK_KODU { get; set; }
            [Column("VIP")] public string VIP { get; set; }
            [Column("YENIISYENILEME")] public string YENIISYENILEME { get; set; }
            [Column("YENILEMENO")] public long? YENILEMENO { get; set; }
            [Column("YENILEME_GARANTISI_TARIHI")] public DateTime? YENILEME_GARANTISI_TARIHI { get; set; }
            [Column("YGTIP")] public string YGTIP { get; set; }
            [Column("ZEYLBASTAR")] public DateTime? ZEYLBASTAR { get; set; }
            [Column("ZEYLCIKISSEBEBI")] public string ZEYLCIKISSEBEBI { get; set; }
            [Column("ZEYLTANTAR")] public DateTime? ZEYLTANTAR { get; set; }
            [Column("ZEYLTIPI")] public string ZEYLTIPI { get; set; }

        }
        [Table("V_RPT_CLAIM_DETAILS")]
        public class V_RptClaimDetails
        {
            [Column("ACENTEBOLGE")] public string ACENTEBOLGE { get; set; }
            [Column("ACENTENO")] public string ACENTENO { get; set; }
            [Column("ACENTEADI")] public string ACENTEADI { get; set; }
            [Column("ALTTEMKOD")] public long? ALTTEMKOD { get; set; }
            [Column("ANATEMADI")] public string ANATEMADI { get; set; }
            [Column("ANATEMKOD")] public long? ANATEMKOD { get; set; }
            [Column("ANLASMADISITUTAR")] public decimal? ANLASMADISITUTAR { get; set; }
            [Column("BANKAADI")] public string BANKAADI { get; set; }
            [Column("BRYTIP")] public string BRYTIP { get; set; }
            [Column("BTCLAIMID")] public string BTCLAIMID { get; set; }
            [Column("CLAIM_SOURCE_TYPE")] public string CLAIM_SOURCE_TYPE { get; set; }
            [Column("CLAIM_STATUS")] public string CLAIM_STATUS { get; set; }
            [Column("COUNTRY_ID")] public long? COUNTRY_ID { get; set; }
            [Column("COUNTY_CODE")] public long? COUNTY_CODE { get; set; }
            [Column("COUNTY_ID")] public long? COUNTY_ID { get; set; }
            [Column("DOKTORADI")] public string DOKTORADI { get; set; }
            [Column("EPIKRIZ_NOTU")] public string EPIKRIZ_NOTU { get; set; }

            [Column("FATURANO")] public string FATURANO { get; set; }
            [Column("FATURATARIHI")] public DateTime? FATURATARIHI { get; set; }
            [Column("FATURATUTAR")] public decimal? FATURATUTAR { get; set; }
            [Column("GRUPID")] public long? GRUPID { get; set; }
            [Column("HASARNOTLAR")] public string HASARNOTLAR { get; set; }
            [Column("HASARTAR")] public DateTime? HASARTAR { get; set; }
            [Column("HAVUZ")] public string HAVUZ { get; set; }
            [Column("HESAPADI")] public string HESAPADI { get; set; }
            [Column("HESAPNO")] public string HESAPNO { get; set; }
            [Column("HSRALTSTAT")] public string HSRALTSTAT { get; set; }
            [Column("HSRSLTSTATACK")] public string HSRSLTSTATACK { get; set; }
            [Column("HSRSTAT")] public string HSRSTAT { get; set; }
            [Column("HSRSTATAD")] public string HSRSTATAD { get; set; }
            [Column("HSRSTATTAR")] public DateTime? HSRSTATTAR { get; set; }
            [Column("IBANNO")] public string IBANNO { get; set; }
            [Column("ICMALNO")] public long? ICMALNO { get; set; }
            [Column("ICMALTAR")] public DateTime? ICMALTAR { get; set; }
            [Column("IHBARTAR")] public DateTime? IHBARTAR { get; set; }
            [Column("ILACBILGISI")] public string ILACBILGISI { get; set; }
            [Column("INDIVIDUAL_TYPE")] public string INDIVIDUAL_TYPE { get; set; }
            [Column("KABULTUTAR")] public decimal? KABULTUTAR { get; set; }
            [Column("KAYITTAR")] public DateTime? KAYITTAR { get; set; }
            [Column("KAYITUSR")] public string KAYITUSR { get; set; }
            [Column("KAYNAKADI")] public string KAYNAKADI { get; set; }
            [Column("KISMIRETTUTARI")] public decimal? KISMIRETTUTARI { get; set; }
            [Column("KURUMADI")] public string KURUMADI { get; set; }
            [Column("KURUMGRUP")] public long? KURUMGRUP { get; set; }
            [Column("KURUMGRUPAD")] public string KURUMGRUPAD { get; set; }
            [Column("KURUMICMALNO")] public string KURUMICMALNO { get; set; }
            [Column("KURUMKODU")] public long? KURUMKODU { get; set; }
            [Column("KURUMSOZLESMEBILGISI")] public string KURUMSOZLESMEBILGISI { get; set; }
            [Column("KURUMTICARIUNVAN")] public string KURUMTICARIUNVAN { get; set; }
            [Column("KURUMTIPI")] public string KURUMTIPI { get; set; }
            [Column("KURUMVKN")] public string KURUMVKN { get; set; }
            [Column("KURUM_ADRES")] public string KURUM_ADRES { get; set; }
            [Column("KURUM_IL")] public string KURUM_IL { get; set; }
            [Column("KURUM_ILCE")] public string KURUM_ILCE { get; set; }
            [Key] [Column("MASTERID")] public long? MASTERID { get; set; }
            [Column("MEDULATAKIPNO")] public string MEDULATAKIPNO { get; set; }
            [Column("ODEMETARIHI")] public DateTime? ODEMETARIHI { get; set; }
            [Column("ODEMEYERIACIKLAMA")] public string ODEMEYERIACIKLAMA { get; set; }
            [Column("ODENEBILIRTUTAR")] public decimal? ODENEBILIRTUTAR { get; set; }
            [Column("ODENENTUTAR")] public decimal? ODENENTUTAR { get; set; }
            [Column("ONAYTUTAR")] public decimal? ONAYTUTAR { get; set; }
            [Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Column("OLD_PAYROLL_ID")] public long? OLD_PAYROLL_ID { get; set; }
            [Column("POLBASTAR")] public DateTime? POLBASTAR { get; set; }
            [Column("POLBITTAR")] public DateTime? POLBITTAR { get; set; }
            [Column("POLICENO")] public string POLICENO { get; set; }
            [Column("POLICETIPI")] public string POLICETIPI { get; set; }
            [Column("POLICY_TYPE")] public string POLICY_TYPE { get; set; }
            [Column("POLTANTAR")] public DateTime? POLTANTAR { get; set; }
            [Column("PORTNO")] public long? PORTNO { get; set; }
            [Column("REGION_CODE")] public string REGION_CODE { get; set; }
            [Column("SGKTUTAR")] public decimal? SGKTUTAR { get; set; }
            [Column("SIGETAD")] public string SIGETAD { get; set; }
            [Column("SIGETID")] public long? SIGETID { get; set; }
            [Column("SIGORTALI")] public string SIGORTALI { get; set; }
            [Column("SIGORTALIPAYI")] public decimal? SIGORTALIPAYI { get; set; }
            [Column("SIGORTALI_AD")] public string SIGORTALI_AD { get; set; }
            [Column("SIGORTALI_SOYAD")] public string SIGORTALI_SOYAD { get; set; }
            [Column("SIGYAS")] public long? SIGYAS { get; set; }
            [Column("SIKAYET")] public string SIKAYET { get; set; }
            [Column("SIRKETAD")] public string SIRKETAD { get; set; }
            [Column("SIRKETKOD")] public long? SIRKETKOD { get; set; }
            [Column("SIRKETPAKETID")] public long? SIRKETPAKETID { get; set; }
            [Column("SIRKETPAKETNO")] public string SIRKETPAKETNO { get; set; }
            [Column("SIRKETTUTAR")] public decimal? SIRKETTUTAR { get; set; }
            [Column("SIRKET_HSR_ID")] public long? SIRKET_HSR_ID { get; set; }
            [Column("SIRKET_HSR_NO")] public string SIRKET_HSR_NO { get; set; }
            [Column("SUBEADI")] public string SUBEADI { get; set; }
            [Column("TALEPTUTAR")] public decimal? TALEPTUTAR { get; set; }
            [Column("TANIBILGILERI")] public string TANIBILGILERI { get; set; }
            [Column("TCKN")] public string TCKN { get; set; }
            //[Column("TEDAVI_NOTU")] public string TEDAVI_NOTU { get; set; }
            [Column("TEMINATADI")] public string TEMINATADI { get; set; }
            [Column("TEMPAKETAD")] public string TEMPAKETAD { get; set; }
            [Column("TEMPAKETNO")] public string TEMPAKETNO { get; set; }
            [Column("URUNADI")] public string URUNADI { get; set; }
            [Column("URUNKODU")] public string URUNKODU { get; set; }
            [Column("UZMANLIKALANI")] public string UZMANLIKALANI { get; set; }
            [Column("YENILEMENO")] public int? YENILEMENO { get; set; }

        }
        [Table("V_CLAIM")]
        public class V_Claim
        {
            [Column("ACCEPTED")] public decimal? ACCEPTED { get; set; }
            [Column("AGENCY_ID")] public long? AGENCY_ID { get; set; }
            [Column("AGENCY_NAME")] public string AGENCY_NAME { get; set; }
            [Column("AGENCY_NO")] public string AGENCY_NO { get; set; }
            [Column("AMOUNT")] public decimal? AMOUNT { get; set; }
            [Column("BANK_NAME")] public string BANK_NAME { get; set; }
            [Column("BILL_DATE")] public DateTime? BILL_DATE { get; set; }
            [Column("BILL_EXCHANGE_CURRENCY_TYPE")] public string BILL_EXCHANGE_CURRENCY_TYPE { get; set; }
            [Column("BILL_EXCHANGE_RATE_ID")] public long? BILL_EXCHANGE_RATE_ID { get; set; }
            [Column("CLAIM_BILL_ID")] public long? CLAIM_BILL_ID { get; set; }
            [Column("BILL_NO")] public string BILL_NO { get; set; }
            [Column("BILL_PAYMENT_TYPE")] public string BILL_PAYMENT_TYPE { get; set; }
            [Column("BILL_STATUS")] public string BILL_STATUS { get; set; }
            [Column("BIRTHDATE")] public DateTime? BIRTHDATE { get; set; }
            [Column("CLAIM_DATE")] public DateTime? CLAIM_DATE { get; set; }
            [Column("PAYROLL_DUE_DATE")] public DateTime? PAYROLL_DUE_DATE { get; set; }
            [Key] [Column("CLAIM_ID")] public long CLAIM_ID { get; set; }
            [Column("CLAIM_NO")] public long? CLAIM_NO { get; set; }
            [Column("CLAIM_TYPE")] public string CLAIM_TYPE { get; set; }
            [Column("COMPANY_CLAIM_ID")] public long? COMPANY_CLAIM_ID { get; set; }
            [Column("COMPANY_CLAIM_NO")] public string COMPANY_CLAIM_NO { get; set; }
            [Column("COMPANY_INSURED_NO")] public string COMPANY_INSURED_NO { get; set; }
            [Column("COMPANY_ENTRANCE_DATE")] public DateTime? COMPANY_ENTRANCE_DATE { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("CONFIRMED")] public decimal? CONFIRMED { get; set; }
            [Column("CONTACT_ID")] public long? CONTACT_ID { get; set; }
            [Column("CONTRACT_ID")] public long? CONTRACT_ID { get; set; }
            //[Column("DIPLOMA_NO")] public string DIPLOMA_NO { get; set; }
            // [Column("DOCTOR_BRANCH_ID")] public long? DOCTOR_BRANCH_ID { get; set; }
            // [Column("DOCTOR_BRANCH_NAME")] public string DOCTOR_BRANCH_NAME { get; set; }
            [Column("EVENT_DATE")] public DateTime? EVENT_DATE { get; set; }
            [Column("EVENT_DESCRIPTION")] public string EVENT_DESCRIPTION { get; set; }
            [Column("EVENT_TYPE")] public string EVENT_TYPE { get; set; }
            [Column("EXT_PROVIDER_DATE")] public DateTime? EXT_PROVIDER_DATE { get; set; }
            [Column("EXT_PROVIDER_NO")] public string EXT_PROVIDER_NO { get; set; }
            [Column("FAMILY_NO")] public long? FAMILY_NO { get; set; }
            [Column("FIRST_AMBULANT_COVERAGE_DATE")] public DateTime? FIRST_AMBULANT_COVERAGE_DATE { get; set; }
            [Column("FIRST_INSURED_DATE")] public DateTime? FIRST_INSURED_DATE { get; set; }
            [Column("FIRST_NAME")] public string FIRST_NAME { get; set; }
            [Column("GENDER")] public string GENDER { get; set; }
            [Column("IBAN")] public string IBAN { get; set; }
            [Column("IDENTITY_NO")] public long? IDENTITY_NO { get; set; }
            [Column("INDIVIDUAL_TYPE")] public string INDIVIDUAL_TYPE { get; set; }
            [Column("INSURED_ID")] public long? INSURED_ID { get; set; }
            [Column("INSURER")] public long? INSURER { get; set; }
            [Column("INSURER_NAME")] public string INSURER_NAME { get; set; }
            [Column("IS_SBM_TRANSFER")] public string IS_SBM_TRANSFER { get; set; }
            [Column("LAST_NAME")] public string LAST_NAME { get; set; }
            [Column("MEDULA_NO")] public string MEDULA_NO { get; set; }
            [Column("MIDDLE_NAME")] public string MIDDLE_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("NOTICE_DATE")] public DateTime? NOTICE_DATE { get; set; }
            [Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Column("PACKAGE_NAME")] public string PACKAGE_NAME { get; set; }
            [Column("PACKAGE_NO")] public string PACKAGE_NO { get; set; }
            [Column("PAID")] public decimal? PAID { get; set; }
            [Column("PASSPORT_NO")] public string PASSPORT_NO { get; set; }
            [Column("PAYMENT_METHOD")] public string PAYMENT_METHOD { get; set; }
            [Column("PAYMENT_DATE")] public DateTime? PAYMENT_DATE { get; set; }
            [Column("PAYROLL_CATEGORY")] public string PAYROLL_CATEGORY { get; set; }
            [Column("PAYROLL_DATE")] public DateTime? PAYROLL_DATE { get; set; }
            [Column("PAYROLL_ID")] public long? PAYROLL_ID { get; set; }
            [Column("OLD_PAYROLL_ID")] public long? OLD_PAYROLL_ID { get; set; }
            [Column("PAYROLL_TYPE")] public string PAYROLL_TYPE { get; set; }
            [Column("PAYROLL_STATUS")] public string PAYROLL_STATUS { get; set; }
            [Column("POLICY_END_DATE")] public DateTime? POLICY_END_DATE { get; set; }
            [Column("POLICY_GROUP_ID")] public long? POLICY_GROUP_ID { get; set; }
            [Column("POLICY_GROUP_NAME")] public string POLICY_GROUP_NAME { get; set; }
            [Column("POLICY_ID")] public long? POLICY_ID { get; set; }
            [Column("POLICY_ISSUE_DATE")] public DateTime? POLICY_ISSUE_DATE { get; set; }
            [Column("POLICY_NUMBER")] public Int64? POLICY_NUMBER { get; set; }
            [Column("POLICY_START_DATE")] public DateTime? POLICY_START_DATE { get; set; }
            [Column("POLICY_TYPE")] public string POLICY_TYPE { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("PROVIDER_GROUP_ID")] public long? PROVIDER_GROUP_ID { get; set; }
            [Column("PROVIDER_ID")] public long? PROVIDER_ID { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Column("REASON_DESCRIPTION")] public string REASON_DESCRIPTION { get; set; }
            [Column("REASON_ID")] public long? REASON_ID { get; set; }
            [Column("REASON_ORDINAL")] public string REASON_ORDINAL { get; set; }
           // [Column("REGISTER_NO")] public string REGISTER_NO { get; set; }
            [Column("REGISTRATION_NO")] public string REGISTRATION_NO { get; set; }
            [Column("RENEWAL_GUARANTEE_TYPE")] public string RENEWAL_GUARANTEE_TYPE { get; set; }
            [Column("RENEWAL_NO")] public long? RENEWAL_NO { get; set; }
            [Column("REQUESTED")] public decimal? REQUESTED { get; set; }
            [Column("SBM_NO")] public string SBM_NO { get; set; }
            [Column("SOURCE_TYPE")] public string SOURCE_TYPE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            //[Column("STAFF_CONTRACT_TYPE")] public string STAFF_CONTRACT_TYPE { get; set; }
            //[Column("STAFF_FIRST_NAME")] public string STAFF_FIRST_NAME { get; set; }
            [Column("STAFF_ID")] public long? STAFF_ID { get; set; }
            //[Column("STAFF_IDENTITY_NO")] public string STAFF_IDENTITY_NO { get; set; }
            // [Column("STAFF_LAST_NAME")] public string STAFF_LAST_NAME { get; set; }
            // [Column("STAFF_MIDDLE_NAME")] public string STAFF_MIDDLE_NAME { get; set; }
            //[Column("STAFF_TITLE")] public string STAFF_TITLE { get; set; }
            // [Column("STAFF_TYPE")] public string STAFF_TYPE { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SUBPRODUCT_ID")] public long? SUBPRODUCT_ID { get; set; }
            [Column("TAX_NUMBER")] public Int64? TAX_NUMBER { get; set; }
            [Column("VARIATION")] public string VARIATION { get; set; }


        }
        [Table("V_CLAIM_COVERAGE")]
        public class V_ClaimCoverage : CommonEntity
        {
            [Column("EXEMPTION")]
            public decimal Exemption { get; set; }
            [Column("ACCEPTED")]
            public Decimal Accepted { get; set; }
            [Key]
            [Column("CLAIM_COVERAGE_ID")]
            public Int64 ClaimCoverageId { get; set; }
            [Column("CLAIM_ID")]
            public Int64 ClaimId { get; set; }
            [Column("COINSURANCE")]
            public int Coinsurance { get; set; }
            [Column("CONFIRMED")]
            public Decimal Confirmed { get; set; }
            [Column("COUNT")]
            public int Count { get; set; }
            [Column("COVERAGE_ID")]
            public Int64 CoverageId { get; set; }
            [Column("COVERAGE_NAME")]
            public string CoverageName { get; set; }
            [Column("COVERAGE_TYPE")]
            public string CoverageType { get; set; }
            [Column("DAY")]
            public int Day { get; set; }
            [Column("EXGRACIA")]
            public Decimal Exgracia { get; set; }
            [Column("PAID")]
            public Decimal Paid { get; set; }
            [Column("PROCESS_LIMIT")]
            public Decimal ProcessLimit { get; set; }
            [Column("REASON_DESCRIPTION")]
            public string ReasonDescription { get; set; }
            [Column("REASON_ID")]
            public Int64? ReasonId { get; set; }
            [Column("REQUESTED")]
            public Decimal Requested { get; set; }
            [Column("RESULT")]
            public string Result { get; set; }
            [Column("SESSION_COUNT")]
            public int? SessionCount { get; set; }
            [Column("SGK_AMOUNT")]
            public Decimal SgkAmount { get; set; }

            [Column("SP_RESPONSE_ID")]
            public Int64? SpResponseId { get; set; }
        }
        [Table("T_PROVIDER_WEB")]
        public class ProviderWeb
        {
            [Key]
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("COMPANY_NAME")]
            public string CompanyName { get; set; }
            [Column("COMPANY_ID")]

            public Int64 CompanyId { get; set; }
            [Column("PRODUCT_CODE")]
            [Display(Name = "POLICE_TIPI_KOD")]
            public string ProductCode { get; set; }
            [Column("PRODUCT_NAME")]
            [Display(Name = "POLICE_TIPI_AD")]
            public string ProductName { get; set; }
            [Display(Name = "KURUM_ADI")]
            [Column("PROVIDER_NAME")]
            public string ProviderName { get; set; }
            [Column("PROVIDER_TYPE")]
            public string PROVIDER_TYPE { get; set; }
            [Column("PROVIDER_ADDRESS")]
            [Display(Name = "ADRES")]
            public string ADDRESS { get; set; }
            [Column("PROVIDER_PHONE_NO")]
            [Display(Name = "TELEFON")]
            public string ProviderPhoneNo { get; set; }
            [Column("NEW_VERSION_ID")]
            public Int64 NewVersionId { get; set; }
            [Column("STATUS")]
            public string Status { get; set; }
            [Column("CITY_NAME")]
            [Display(Name = "IL")]
            public string CITY_NAME { get; set; }
            [Column("COUNTY_NAME")]
            [Display(Name = "ILCE")]
            public string COUNTY_NAME { get; set; }
            [Column("COMPANY_WEB_ID")]
            [Display(Name = "SIRKETKOD")]
            public string CompanyWebId { get; set; }
            [Column("PROVIDER_TYPE_NAME")]
            [Display(Name = "KURUM_TIPI")]
            public string ProviderTypeName { get; set; }
        }

        [Table("V_PRIVILEGE")]
        public class V_Privilege
        {
            [Column("ASSET_CODE")] public string ASSET_CODE { get; set; }
            [Column("ASSET_ID")] public long? ASSET_ID { get; set; }
            [Column("ASSET_NAME")] public string ASSET_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PRIVILEGE_CODE")] public string PRIVILEGE_CODE { get; set; }
            [Key][Column("PRIVILEGE_ID")] public long? PRIVILEGE_ID { get; set; }
            [Column("PRIVILEGE_NAME")] public string PRIVILEGE_NAME { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_PERSONNEL_CADRE")]
        public class V_PersonnelCadre
        {
            [Column("ACTIVATION_TOKEN")] public string ACTIVATION_TOKEN { get; set; }
            [Column("BIRTHDATE")] public DateTime? BIRTHDATE { get; set; }
            [Column("CADRE_CODE")] public string CADRE_CODE { get; set; }
            [Column("CADRE_ID")] public long? CADRE_ID { get; set; }
            [Column("CADRE_NAME")] public string CADRE_NAME { get; set; }
            [Column("CONTACT_ID")] public long? CONTACT_ID { get; set; }
            [Column("EMAIL")] public string EMAIL { get; set; }
            [Column("FIRST_NAME")] public string FIRST_NAME { get; set; }
            [Column("GENDER")] public string GENDER { get; set; }
            [Column("IDENTITY_NO")] public Int64? IDENTITY_NO { get; set; }
            [Column("IS_REMEMBER_ME")] public string IS_REMEMBER_ME { get; set; }
            [Column("LAST_NAME")] public string LAST_NAME { get; set; }
            [Column("MOBILE")] public string MOBILE { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Key][Column("PERSONNEL_CADRE_ID")] public long? PERSONNEL_CADRE_ID { get; set; }
            [Column("PERSONNEL_ID")] public long? PERSONNEL_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("TOKEN")] public string TOKEN { get; set; }
            [Column("UNIT_CODE")] public string UNIT_CODE { get; set; }
            [Column("UNIT_ID")] public long? UNIT_ID { get; set; }
            [Column("UNIT_NAME")] public string UNIT_NAME { get; set; }
            [Column("USERNAME")] public string USERNAME { get; set; }
            [Column("USER_ID")] public long? USER_ID { get; set; }

        }

        [Table("V_CADRE")]
        public class V_Cadre
        {
            [Column("CADRE_CODE")] public string CADRE_CODE { get; set; }
            [Key][Column("CADRE_ID")] public long? CADRE_ID { get; set; }
            [Column("CADRE_NAME")] public string CADRE_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("UNIT_CODE")] public string UNIT_CODE { get; set; }
            [Column("UNIT_ID")] public long? UNIT_ID { get; set; }
            [Column("UNIT_NAME")] public string UNIT_NAME { get; set; }

        }

        [Table("V_INTEGRATION_LOG")]
        public class V_IntegrationLog
        {
            [Column("CLAIM_ID")] public long? CLAIM_ID { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("CORRELATION_ID")] public string CORRELATION_ID { get; set; }
            [Column("EXT_PROVIDER_NO")] public string EXT_PROVIDER_NO { get; set; }
            [Column("HTTP_STATUS")] public string HTTP_STATUS { get; set; }
            [Key][Column("ID")] public long ID { get; set; }
            [Column("IDENTITY_NO")] public long? IDENTITY_NO { get; set; }
            [Column("INSURED_FIRST_NAME")] public string INSURED_FIRST_NAME { get; set; }
            [Column("INSURED_ID")] public long? INSURED_ID { get; set; }
            [Column("INSURED_LAST_NAME")] public string INSURED_LAST_NAME { get; set; }
            [Column("INSURED_NATIONALITY_ID")] public long? INSURED_NATIONALITY_ID { get; set; }
            [Column("MEDULA_NO")] public string MEDULA_NO { get; set; }
            [Column("PARENT_ID")] public long? PARENT_ID { get; set; }
            [Column("PAYROLL_ID")] public long? PAYROLL_ID { get; set; }
            [Column("POLICY_ID")] public long? POLICY_ID { get; set; }
            [Column("PROVIDER_ID")] public long? PROVIDER_ID { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Column("REQUEST")] public String REQUEST { get; set; }
            [Column("RESPONSE")] public String RESPONSE { get; set; }
            [Column("RESPONSE_STATUS_CODE")] public string RESPONSE_STATUS_CODE { get; set; }
            [Column("RESPONSE_STATUS_DESC")] public string RESPONSE_STATUS_DESC { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("TYPE")] public string TYPE { get; set; }
            [Column("WS_METHOD")] public string WS_METHOD { get; set; }
            [Column("WS_NAME")] public string WS_NAME { get; set; }
            [Column("WS_REQUEST_DATE")] public DateTime? WS_REQUEST_DATE { get; set; }
            [Column("WS_RESPONSE_DATE")] public DateTime? WS_RESPONSE_DATE { get; set; }
            [Column("INSURED_CONTACT_ID")] public string INSURED_CONTACT_ID { get; set; }
            [Column("POLICY_NUMBER")] public string POLICY_NUMBER { get; set; }
            


        }

        [Table("V_UNIT")]
        public class V_Unit
        {
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("ORGANIZATION_CODE")] public string ORGANIZATION_CODE { get; set; }
            [Column("ORGANIZATION_ID")] public long? ORGANIZATION_ID { get; set; }
            [Column("ORGANIZATION_NAME")] public string ORGANIZATION_NAME { get; set; }
            [Column("PARENT_CODE")] public string PARENT_CODE { get; set; }
            [Column("PARENT_ID")] public long? PARENT_ID { get; set; }
            [Column("PARENT_NAME")] public string PARENT_NAME { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("UNIT_CODE")] public string UNIT_CODE { get; set; }
            [Key][Column("UNIT_ID")] public long? UNIT_ID { get; set; }
            [Column("UNIT_NAME")] public string UNIT_NAME { get; set; }

        }

        [Table("V_ASSET")]
        public class V_Asset
        {
            [Column("ASSET_CODE")] public string ASSET_CODE { get; set; }
            [Key][Column("ASSET_ID")] public long? ASSET_ID { get; set; }
            [Column("ASSET_NAME")] public string ASSET_NAME { get; set; }
            [Column("CATEGORY_CODE")] public string CATEGORY_CODE { get; set; }
            [Column("CATEGORY_ID")] public long? CATEGORY_ID { get; set; }
            [Column("CATEGORY_NAME")] public string CATEGORY_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_CATEGORY")]
        public class V_Category
        {
            [Column("CATEGORY_CODE")] public string CATEGORY_CODE { get; set; }
            [Key][Column("CATEGORY_ID")] public long? CATEGORY_ID { get; set; }
            [Column("CATEGORY_NAME")] public string CATEGORY_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("ORGANIZATION_CODE")] public string ORGANIZATION_CODE { get; set; }
            [Column("ORGANIZATION_ID")] public long? ORGANIZATION_ID { get; set; }
            [Column("ORGANIZATION_NAME")] public string ORGANIZATION_NAME { get; set; }
            [Column("PARENT_CODE")] public string PARENT_CODE { get; set; }
            [Column("PARENT_ID")] public long? PARENT_ID { get; set; }
            [Column("PARENT_NAME")] public string PARENT_NAME { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_RULE_CATEGORY")]
        public class V_RuleCategory
        {
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("RESULT_TYPE")] public string RESULT_TYPE { get; set; }
            [Key][Column("RULE_CATEGORY_ID")] public long? RULE_CATEGORY_ID { get; set; }
            [Column("RULE_CATEGORY_NAME")] public string RULE_CATEGORY_NAME { get; set; }
            [Column("RULE_CATEGORY_TYPE")] public string RULE_CATEGORY_TYPE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_RULE_GROUP")]
        public class V_RuleGroup
        {
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Key][Column("RULE_GROUP_ID")] public long? RULE_GROUP_ID { get; set; }
            [Column("RULE_GROUP_NAME")] public string RULE_GROUP_NAME { get; set; }
            [Column("RULE_GROUP_TYPE")] public string RULE_GROUP_TYPE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_RULE")]
        public class V_Rule
        {
            [Column("CATEGORY_ID")] public long? CATEGORY_ID { get; set; }
            [Column("DATA")] public string DATA { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("RESULT")] public string RESULT { get; set; }
            [Column("RESULT_SECOND")] public string RESULT_SECOND { get; set; }
            [Column("RESULT_TYPE")] public string RESULT_TYPE { get; set; }
            [Column("RULE_CATEGORY_ID")] public long? RULE_CATEGORY_ID { get; set; }
            [Column("RULE_CATEGORY_TYPE")] public string RULE_CATEGORY_TYPE { get; set; }
            [Key] [Column("RULE_ID")] public long? RULE_ID { get; set; }
            [Column("RULE_NAME")] public string RULE_NAME { get; set; }
            [Column("RULE_TYPE")] public string RULE_TYPE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("REASONID")] public long? REASON_ID { get; set; }
            [Column("CLAIMSTATUS")] public string CLAIM_STATUS { get; set; }

        }

        [Table("V_PROVIDER_WEB_SOURCE")]
        public class V_ProviderWebSource
        {

            [Key]
            [Column("COMPANY_ID")]
            public Int64 CompanyId { get; set; }
            [Column("PROVIDER_ID")]
            public Int64 PROVIDER_ID { get; set; }
            [Column("PRODUCT_ID")]
            public Int64 ProductId { get; set; }
            [Column("PRODUCT_NAME")]
            public string ProductName { get; set; }
            [Column("PROVIDER_NAME")]
            public string ProviderName { get; set; }
            [Column("PROVIDER_TYPE")]
            public string ProviderType { get; set; }
            [Column("ADDRESS")]
            public string ADDRESS { get; set; }
            [Column("CITY_NAME")]
            public string CITY_NAME { get; set; }
            [Column("COUNTY_NAME")]
            public string COUNTY_NAME { get; set; }
            [Column("PHONE_NO")]
            public string PhoneNo { get; set; }
            [Column("COMPANY_WEB_ID")]
            public string CompanyWebId { get; set; }
            [Column("TSS_NOTE")]
            public string TSS_NOTE { get; set; }
            [Column("OSS_NOTE")]
            public string OSS_NOTE { get; set; }

        }
        [Table("V_PROVIDER_WEB_EXPORT")]
        public class V_ProviderWebExport
        {
            [Key]
            [Column("SIRKETKOD")]
            [JsonProperty(PropertyName = "SIRKETKOD")]
            public string SirketKod { get; set; }
            [Column("POLICE_TIPI_KOD")]
            [JsonProperty(PropertyName = "POLICE_TIPI_KOD")]
            public string PoliceTipiKod { get; set; }
            [Column("POLICE_TIPI_AD")]
            [JsonProperty(PropertyName = "POLICE_TIPI_AD")]
            public string PoliceTipiAd { get; set; }
            [Column("KURUM_ADI")]
            [JsonProperty(PropertyName = "KURUM_ADI")]
            public string KurumAdi { get; set; }
            [Column("KURUM_TIPI")]
            [JsonProperty(PropertyName = "KURUM_TIPI")]
            public string KurumTipi { get; set; }
            [Column("ADRES")]
            [JsonProperty(PropertyName = "ADRES")]
            public string Adres { get; set; }
            [Column("IL")]
            [JsonProperty(PropertyName = "IL")]
            public string IL { get; set; }
            [Column("ILCE")]
            [JsonProperty(PropertyName = "ILCE")]
            public string Ilce { get; set; }
            [Column("TELEFON")]
            [JsonProperty(PropertyName = "TELEFON")]
            public string Telefon { get; set; }
            [Column("KURUMNOT")]
            [JsonProperty(PropertyName = "KURUMNOT")]
            public string KurumNot { get; set; }
        }
        [Table("V_PLAN_COVERAGE")]
        public class V_PlanCoverage
        {
            [Column("BRANCH_ID")] public long? BRANCH_ID { get; set; }
            [Column("BRANCH_NAME")] public string BRANCH_NAME { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("COVERAGE_ID")] public long? COVERAGE_ID { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Column("COVERAGE_TYPE")] public string COVERAGE_TYPE { get; set; }
            [Column("IS_MAIN_COVERAGE")] public long? IS_MAIN_COVERAGE { get; set; }
            [Column("IS_SBM_TRANSFER")] public string IS_SBM_TRANSFER { get; set; }
            [Column("LOCATION_TYPE")] public string LOCATION_TYPE { get; set; }
            [Column("MAIN_COVERAGE_ID")] public long? MAIN_COVERAGE_ID { get; set; }
            [Column("NETWORK_ID")] public long? NETWORK_ID { get; set; }
            [Column("NETWORK_NAME")] public string NETWORK_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PACKAGE_ID")] public long? PACKAGE_ID { get; set; }
            [Column("PARENT_ID")] public long? PARENT_ID { get; set; }
            [Key] [Column("PLAN_COVERAGE_ID")] public long? PLAN_COVERAGE_ID { get; set; }
            [Column("PLAN_COVERAGE_NAME")] public string PLAN_COVERAGE_NAME { get; set; }
            [Column("PLAN_ID")] public long? PLAN_ID { get; set; }
            [Column("PLAN_NAME")] public string PLAN_NAME { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SUBPRODUCT_CODE")] public string SUBPRODUCT_CODE { get; set; }
            [Column("SUBPRODUCT_ID")] public long? SUBPRODUCT_ID { get; set; }
            [Column("SUBPRODUCT_NAME")] public string SUBPRODUCT_NAME { get; set; }
            [Column("IS_OPEN_TO_PRINT")] public int? IS_OPEN_TO_PRINT { get; set; }
            [Column("IS_OPEN_TO_PROVIDER")] public int? IS_OPEN_TO_PROVIDER { get; set; }

        }

        [Table("V_PLAN")]
        public class V_Plan
        {
            [Column("BRANCH_ID")] public long? BRANCH_ID { get; set; }
            [Column("BRANCH_NAME")] public string BRANCH_NAME { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("IS_SBM_TRANSFER")] public string IS_SBM_TRANSFER { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Key] [Column("PLAN_ID")] public long? PLAN_ID { get; set; }
            [Column("PLAN_NAME")] public string PLAN_NAME { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("PRODUCT_TYPE")] public string PRODUCT_TYPE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SUBPRODUCT_CODE")] public string SUBPRODUCT_CODE { get; set; }
            [Column("SUBPRODUCT_ID")] public long? SUBPRODUCT_ID { get; set; }
            [Column("SUBPRODUCT_NAME")] public string SUBPRODUCT_NAME { get; set; }
            [Column("SUBPRODUCT_TYPE")] public string SUBPRODUCT_TYPE { get; set; }

        }

        [Table("V_SUBPRODUCT_NOTE")]
        public class V_SubProductNote
        {
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("NOTE_DESCRIPTION")] public string NOTE_DESCRIPTION { get; set; }
            [Column("NOTE_ID")] public long? NOTE_ID { get; set; }
            [Column("NOTE_TYPE")] public string NOTE_TYPE { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SUBPRODUCT_CODE")] public string SUBPRODUCT_CODE { get; set; }
            [Column("SUBPRODUCT_ID")] public long? SUBPRODUCT_ID { get; set; }
            [Column("SUBPRODUCT_NAME")] public string SUBPRODUCT_NAME { get; set; }
            [Key] [Column("SUBPRODUCT_NOTE_ID")] public long? SUBPRODUCT_NOTE_ID { get; set; }

        }

        [Table("V_SUBPRODUCT_PARAMETER")]
        public class V_SubProductParameter
        {
            [Column("ENVIRONMENT")] public string ENVIRONMENT { get; set; }
            [Column("KEY")] public string KEY { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PARAMETER_ID")] public long? PARAMETER_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SUBPRODUCT_ID")] public long? SUBPRODUCT_ID { get; set; }
            [Key] [Column("SUBPRODUCT_PARAMETER_ID")] public long? SUBPRODUCT_PARAMETER_ID { get; set; }
            [Column("SYSTEM_TYPE")] public string SYSTEM_TYPE { get; set; }
            [Column("VALUE")] public string VALUE { get; set; }

        }

        [Table("V_SUBPRODUCT")]
        public class V_SubProduct
        {
            [Column("AGREED_AMOUNT")] public long? AGREED_AMOUNT { get; set; }
            [Column("BRANCH_ID")] public long? BRANCH_ID { get; set; }
            [Column("BRANCH_NAME")] public string BRANCH_NAME { get; set; }
            [Column("COMPANY_ID")] public long? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("IS_SBM_TRANSFER")] public string IS_SBM_TRANSFER { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
            [Column("PRODUCT_DESCRIPTION")] public string PRODUCT_DESCRIPTION { get; set; }
            [Column("PRODUCT_ID")] public long? PRODUCT_ID { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("REGISTRY_DATE")] public string REGISTRY_DATE { get; set; }
            [Column("SBM_NO")] public string SBM_NO { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SUBPRODUCT_CODE")] public string SUBPRODUCT_CODE { get; set; }
            [Column("SUBPRODUCT_DESCRIPTION")] public string SUBPRODUCT_DESCRIPTION { get; set; }
            [Key] [Column("SUBPRODUCT_ID")] public long? SUBPRODUCT_ID { get; set; }
            [Column("SUBPRODUCT_NAME")] public string SUBPRODUCT_NAME { get; set; }
            [Column("SUBPRODUCT_TYPE")] public string SUBPRODUCT_TYPE { get; set; }

        }
        [Table("V_INSURED_EXCLUSION")]
        public class V_InsuredExclusion
        {
            [Column("ADDITIONAL_PREMIUM")] public long? ADDITIONAL_PREMIUM { get; set; }
            [Column("DESCRIPTION")] public string DESCRIPTION { get; set; }
            [Column("END_DATE")] public DateTime? END_DATE { get; set; }
            [Column("EXCLUSION_ID")] public long EXCLUSION_ID { get; set; }
            [Key] [Column("INSURED_EXCLUSION")] public long INSURED_EXCLUSION { get; set; }
            [Column("INSURED_ID")] public long INSURED_ID { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("REASON_DESCRIPTION")] public string REASON_DESCRIPTION { get; set; }
            [Column("REASON_ID")] public long? REASON_ID { get; set; }
            [Column("REASON_NAME")] public string REASON_NAME { get; set; }
            [Column("RESULT")] public string RESULT { get; set; }
            [Column("RESULT_SECOND")] public string RESULT_SECOND { get; set; }
            [Column("RULE_CATEGORY_ID")] public long? RULE_CATEGORY_ID { get; set; }
            [Column("RULE_CATEGORY_TYPE")] public string RULE_CATEGORY_TYPE { get; set; }
            [Column("RULE_ID")] public long? RULE_ID { get; set; }
            [Column("RULE_NAME")] public string RULE_NAME { get; set; }
            [Column("START_DATE")] public DateTime? START_DATE { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("IS_OPEN_TO_PRINT")] public int? IS_OPEN_TO_PRINT { get; set; }
        }

        [Table("V_CONTACT_ADDRESS")]
        public class V_ContactAddress
        {
            [Column("ADDRESS_ID")] public long? ADDRESS_ID { get; set; }
            [Column("ADDRESS_TYPE")] public string ADDRESS_TYPE { get; set; }
            [Column("CITY_ID")] public long? CITY_ID { get; set; }
            [Column("CITY_NAME")] public string CITY_NAME { get; set; }
            [Column("CITY_CODE")] public string CITY_CODE { get; set; }
            [Column("COUNTRY_NUM_CODE")] public string COUNTRY_NUM_CODE { get; set; }
            [Column("COUNTY_CODE")] public string COUNTY_CODE { get; set; }
            [Column("CONTACT_ID")] public long? CONTACT_ID { get; set; }
            [Column("COUNTRY_ID")] public long? COUNTRY_ID { get; set; }
            [Column("COUNTRY_NAME")] public string COUNTRY_NAME { get; set; }
            [Column("COUNTY_ID")] public long? COUNTY_ID { get; set; }
            [Column("COUNTY_NAME")] public string COUNTY_NAME { get; set; }
            [Column("DETAILS")] public string DETAILS { get; set; }
            [Key][Column("INSURED_ADDRESS_ID")] public long? INSURED_ADDRESS_ID { get; set; }
            [Column("IS_PRIMARY")] public string IS_PRIMARY { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("ZIP_CODE")] public string ZIP_CODE { get; set; }

        }

        [Table("V_CONTACT_EMAIL")]
        public class V_ContactEmail
        {
            [Key][Column("CONTACT_EMAIL_ID")] public long? CONTACT_EMAIL_ID { get; set; }
            [Column("CONTACT_ID")] public long? CONTACT_ID { get; set; }
            [Column("DETAILS")] public string DETAILS { get; set; }
            [Column("EMAIL_ID")] public long? EMAIL_ID { get; set; }
            [Column("IS_PRIMARY")] public string IS_PRIMARY { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_CONTACT_PHONE")]
        public class V_ContactPhone
        {
            [Column("CONTACT_ID")] public long CONTACT_ID { get; set; }
            [Key][Column("CONTACT_PHONE_ID")] public long CONTACT_PHONE_ID { get; set; }
            [Column("COUNTRY_ID")] public long? COUNTRY_ID { get; set; }
            [Column("COUNTRY_PHONE_CODE")] public string COUNTRY_PHONE_CODE { get; set; }
            [Column("EXTENSION")] public string EXTENSION { get; set; }
            [Column("IS_PRIMARY")] public string IS_PRIMARY { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PHONE_ID")] public long PHONE_ID { get; set; }
            [Column("PHONE_NO")] public string PHONE_NO { get; set; }
            [Column("PHONE_TYPE")] public string PHONE_TYPE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_CONTACT_NOTE")]
        public class V_ContactNote
        {
            [Column("CONTACT_ID")] public long CONTACT_ID { get; set; }
            [Key][Column("CONTACT_NOTE_ID")] public long CONTACT_NOTE_ID { get; set; }
            [Column("DESCRIPTION")] public string DESCRIPTION { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("NOTE_ID")] public long NOTE_ID { get; set; }
            [Column("NOTE_TYPE")] public string NOTE_TYPE { get; set; }
            [Column("SP_RESPONSE_ID")] public long SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
        }

        [Table("V_RESPONSE_USER")]
        public class V_ResponseUser
        {
            [Column("CODE")] public string CODE { get; set; }
            [Column("EMAIL")] public string EMAIL { get; set; }
            [Column("IP")] public string IP { get; set; }
            [Column("MESSAGE")] public string MESSAGE { get; set; }
            [Column("OBJECT_ID")] public string OBJECT_ID { get; set; }
            [Column("PK_ID")] public long? PK_ID { get; set; }
            [Key][Column("RESPONSE_ID")] public long? RESPONSE_ID { get; set; }
            [Column("SP_REQUEST_DATE")] public DateTime? SP_REQUEST_DATE { get; set; }
            [Column("SP_REQUEST_ID")] public string SP_REQUEST_ID { get; set; }
            [Column("SP_RESPONSE_DATE")] public DateTime? SP_RESPONSE_DATE { get; set; }
            [Column("TOKEN")] public string TOKEN { get; set; }
            [Column("USERNAME")] public string USERNAME { get; set; }
            [Column("USER_ID")] public long? USER_ID { get; set; }
        }

        [Table("V_PROVIDER_GROUP_RULE")]
        public class V_ProviderGroupRule
        {
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PROVIDER_GROUP_ID")] public long PROVIDER_GROUP_ID { get; set; }
            [Column("PROVIDER_GROUP_NAME")] public string PROVIDER_GROUP_NAME { get; set; }
            [Key] [Column("PROVIDER_GROUP_RULE_ID")] public long PROVIDER_GROUP_RULE_ID { get; set; }
            [Column("PROVIDER_GROUP_TAX_NUMBER")] public string PROVIDER_GROUP_TAX_NUMBER { get; set; }
            [Column("RULE_CATEGORY_ID")] public long? RULE_CATEGORY_ID { get; set; }
            [Column("RULE_CATEGORY_TYPE")] public string RULE_CATEGORY_TYPE { get; set; }
            [Column("RULE_ID")] public long? RULE_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
        }

        [Table("V_CONTACT_DETAIL_IDS")]
        public class V_ContactDetailIds
        {
            [Key] [Column("CONTACT_ID")] public long CONTACT_ID { get; set; }
            [Column("ADDRESS_ID")] public long? ADDRESS_ID { get; set; }
            [Column("CORPORATE_ID")] public long? CORPORATE_ID { get; set; }
            [Column("IDENTITY_NO")] public long? IDENTITY_NO { get; set; }
            [Column("TAX_NUMBER")] public long? TAX_NUMBER { get; set; }
            [Column("PASSPORT_NO")] public string PASSPORT_NO { get; set; }
            [Column("PERSON_ID")] public long? PERSON_ID { get; set; }
        }

        [Table("V_PROVIDER_NOTE")]
        public class V_ProviderNote
        {
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("NOTE_DESCRIPTION")] public string NOTE_DESCRIPTION { get; set; }
            [Column("NOTE_ID")] public long? NOTE_ID { get; set; }
            [Column("NOTE_TYPE")] public string NOTE_TYPE { get; set; }
            [Column("PROVIDER_ID")] public long? PROVIDER_ID { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Key][Column("PROVIDER_NOTE_ID")] public long? PROVIDER_NOTE_ID { get; set; }
            [Column("PROVIDER_TITLE")] public string PROVIDER_TITLE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_PROVIDER_USER")]
        public class V_ProviderUser
        {
            [Column("EMAIL")] public string EMAIL { get; set; }
            [Column("MOBILE")] public string MOBILE { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PROVIDER_ID")] public long? PROVIDER_ID { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Column("PROVIDER_TYPE")] public string PROVIDER_TYPE { get; set; }
            [Key][Column("PROVIDER_USER_ID")] public long? PROVIDER_USER_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("USERNAME")] public string USERNAME { get; set; }
            [Column("USER_ID")] public long? USER_ID { get; set; }

        }

        [Table("V_PROVIDER_RULE")]
        public class V_ProviderRule
        {
            [Column("CITY_ID")] public long? CITY_ID { get; set; }
            [Column("CITY_NAME")] public string CITY_NAME { get; set; }
            [Column("COUNTY_ID")] public long? COUNTY_ID { get; set; }
            [Column("COUNTY_NAME")] public string COUNTY_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PROVIDER_ID")] public long PROVIDER_ID { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Key] [Column("PROVIDER_RULE_ID")] public long PROVIDER_RULE_ID { get; set; }
            [Column("PROVIDER_TITLE")] public string PROVIDER_TITLE { get; set; }
            [Column("PROVIDER_TYPE")] public string PROVIDER_TYPE { get; set; }
            [Column("RULE_CATEGORY_ID")] public long? RULE_CATEGORY_ID { get; set; }
            [Column("RULE_CATEGORY_TYPE")] public string RULE_CATEGORY_TYPE { get; set; }
            [Column("RULE_ID")] public long? RULE_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_PROCESS_GROUP_PROCESS")]
        public class V_ProcessGroupProcess
        {
            [Column("DECRIPTION")] public string DECRIPTION { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PROCESS_GROUP_ID")] public long? PROCESS_GROUP_ID { get; set; }
            [Column("PROCESS_GROUP_NAME")] public string PROCESS_GROUP_NAME { get; set; }
            [Key] [Column("PROCESS_GROUP_PROCESS_ID")] public long? PROCESS_GROUP_PROCESS_ID { get; set; }
            [Column("PROCESS_ID")] public long? PROCESS_ID { get; set; }
            [Column("PROCESS_LIST_ID")] public long? PROCESS_LIST_ID { get; set; }
            [Column("PROCESS_LIST_NAME")] public string PROCESS_LIST_NAME { get; set; }
            [Column("PROCESS_LIST_TYPE")] public string PROCESS_LIST_TYPE { get; set; }
            [Column("PROCESS_NAME")] public string PROCESS_NAME { get; set; }
            [Column("PROCESS_PARENT_ID")] public long? PROCESS_PARENT_ID { get; set; }
            [Column("PTOCESS_CODE")] public string PTOCESS_CODE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_PROCESS_COVERAGE")]
        public class V_ProcessCoverage
        {
            [Column("COVERAGE_ID")] public long? COVERAGE_ID { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PROCESS_CODE")] public string PROCESS_CODE { get; set; }
            [Key] [Column("PROCESS_COVERAGE_ID")] public long? PROCESS_COVERAGE_ID { get; set; }
            [Column("PROCESS_ID")] public long? PROCESS_ID { get; set; }
            [Column("PROCESS_LIST_ID")] public long? PROCESS_LIST_ID { get; set; }
            [Column("PROCESS_LIST_NAME")] public string PROCESS_LIST_NAME { get; set; }
            [Column("PROCESS_NAME")] public string PROCESS_NAME { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_PROCESS_GROUP")]
        public class V_ProcessGroup
        {
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Key] [Column("PROCESS_GROUP_ID")] public long? PROCESS_GROUP_ID { get; set; }
            [Column("PROCESS_GROUP_NAME")] public string PROCESS_GROUP_NAME { get; set; }
            [Column("PROCESS_LIST_ID")] public long? PROCESS_LIST_ID { get; set; }
            [Column("PROCESS_LIST_NAME")] public string PROCESS_LIST_NAME { get; set; }
            [Column("PROCESS_LIST_TYPE")] public string PROCESS_LIST_TYPE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_PROCESS")]
        public class V_Process
        {
            [Column("AMOUNT")] public decimal? AMOUNT { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PARENT_ID")] public long? PARENT_ID { get; set; }
            [Column("PROCESS_CODE")] public string PROCESS_CODE { get; set; }
            [Key][Column("PROCESS_ID")] public long? PROCESS_ID { get; set; }
            [Column("PROCESS_LIST_ID")] public long? PROCESS_LIST_ID { get; set; }
            [Column("PROCESS_LIST_NAME")] public string PROCESS_LIST_NAME { get; set; }
            [Column("PROCESS_LIST_TYPE")] public string PROCESS_LIST_TYPE { get; set; }
            [Column("PROCESS_NAME")] public string PROCESS_NAME { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_CITY_FACTOR_GROUP")]
        public class V_CityFactorGroup
        {
            [Key][Column("CITY_FACTOR_GROUP_ID")] public long? CITY_FACTOR_GROUP_ID { get; set; }
            [Column("END_DATE")] public DateTime? END_DATE { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PROCESS_LIST_ID")] public long? PROCESS_LIST_ID { get; set; }
            [Column("PROCESS_LIST_NAME")] public string PROCESS_LIST_NAME { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("START_DATE")] public DateTime? START_DATE { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_PROCESS_LIST")]
        public class V_ProcessList
        {
            [Column("IS_FACTOR_CALCULATED")] public string IS_FACTOR_CALCULATED { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PRICE_TYPE")] public string PRICE_TYPE { get; set; }
            [Key][Column("PROCESS_LIST_ID")] public long? PROCESS_LIST_ID { get; set; }
            [Column("PROCESS_LIST_NAME")] public string PROCESS_LIST_NAME { get; set; }
            [Column("PROCESS_LIST_TYPE")] public string PROCESS_LIST_TYPE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_PROCESS_RULE")]
        public class V_ProcessRule
        {
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PROCESS_CODE")] public string PROCESS_CODE { get; set; }
            [Column("PROCESS_ID")] public long? PROCESS_ID { get; set; }
            [Column("PROCESS_LIST_ID")] public long? PROCESS_LIST_ID { get; set; }
            [Column("PROCESS_LIST_NAME")] public string PROCESS_LIST_NAME { get; set; }
            [Column("PROCESS_NAME")] public string PROCESS_NAME { get; set; }
            [Key][Column("PROCESS_RULE_ID")] public long? PROCESS_RULE_ID { get; set; }
            [Column("RULE_CATEGORY_ID")] public long? RULE_CATEGORY_ID { get; set; }
            [Column("RULE_CATEGORY_TYPE")] public string RULE_CATEGORY_TYPE { get; set; }
            [Column("RULE_ID")] public long? RULE_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_PROCESS_GROUP_RULE")]
        public class V_ProcessGroupRule
        {
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("PROCESS_GROUP_ID")] public long PROCESS_GROUP_ID { get; set; }
            [Column("PROCESS_GROUP_NAME")] public string PROCESS_GROUP_NAME { get; set; }
            [Key] [Column("PROCESS_GROUP_RULE_ID")] public long PROCESS_GROUP_RULE_ID { get; set; }
            [Column("PROCESS_LIST_ID")] public long? PROCESS_LIST_ID { get; set; }
            [Column("PROCESS_LIST_NAME")] public string PROCESS_LIST_NAME { get; set; }
            [Column("RULE_CATEGORY_ID")] public long? RULE_CATEGORY_ID { get; set; }
            [Column("RULE_CATEGORY_TYPE")] public string RULE_CATEGORY_TYPE { get; set; }
            [Column("RULE_ID")] public long? RULE_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_COVERAGE_RULE")]
        public class V_CoverageRule
        {
            [Column("COVERAGE_ID")] public long COVERAGE_ID { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Key] [Column("COVERAGE_RULE_ID")] public long COVERAGE_RULE_ID { get; set; }
            [Column("COVERAGE_TYPE")] public string COVERAGE_TYPE { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("RULE_CATEGORY_ID")] public long? RULE_CATEGORY_ID { get; set; }
            [Column("RULE_CATEGORY_TYPE")] public string RULE_CATEGORY_TYPE { get; set; }
            [Column("RULE_ID")] public long? RULE_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }

        }

        [Table("V_SGM_ENDORS_INSURED_PREMIUM")]
        public class V_SgmEndorsInsuredPremium
        {
            [Column("COMPANY_ID")]
            public string CompanyId { get; set; }
            [Column("BRUTPRIM")] public string BrutPrim { get; set; }
            [Column("ZEYLTANZIMTARIHI")] public string ZeylTanzimTarihi { get; set; }
            [Column("ZEYLNO")] public string ZeylNo { get; set; }
            [Column("ZEYLNETPRIMI")] public string ZeylNetPrimi { get; set; }
            [Column("ZEYLBRUTPRIMI")] public string ZeylBrutPrimi { get; set; }
            [Column("ZEYLBASLANGICTARIHI")] public string ZeylBaslangicTarihi { get; set; }
            [Column("YENILEMENO")] public string YenilemeNo { get; set; }
            [Column("SIGORTASIRKETKOD")] public string SigortaSirketKod { get; set; }
            [Column("SBMSAGMERNO")] public string SbmSagmerNo { get; set; }
            [Column("POLICENO")] public string PoliceNo { get; set; }
            [Column("POLICENETPRIMI")] public string PoliceNetPrimi { get; set; }
            [Column("POLICEBRUTPRIMI")] public string PoliceBrutPrimi { get; set; }
            [Column("OTORIZASYONKOD")] public string OtorizasyonKod { get; set; }
            [Column("INSURED_ID")] public string InsuredId { get; set; }
            [Column("ENDORSEMENT_ID")] public string EndorsementId { get; set; }
            [Key]
            [Column("POLICY_ID")] public string PolicyId { get; set; }
            [Column("KIMLIKTIPI")] public string KimlikTipi { get; set; }
            [Column("KIMLIKNO")] public string KimlikNo { get; set; }
            [Column("URUNPLANKOD")] public string UrunPlanKod { get; set; }
            [Column("NETPRIM")] public string NetPrim { get; set; }
        }
        [Table("V_SGM_CLAIM_BILL")]
        public class V_SgmClaimBill
        {
            [Column("PARABIRIMI")] public string ParaBirimi { get; set; }
            [Column("FATURATUTARITLODENEN")] public string FaturaTutariTlOdenen { get; set; }
            [Column("FATURATUTARITL")] public string FaturaTutariTl { get; set; }
            [Column("FATURATARIHI")] public string FaturaTarihi { get; set; }
            [Column("FATURANO")] public string FaturaNo { get; set; }
            [Column("ETTN")] public string Ettn { get; set; }
            [Key]
            [Column("POLICY_ID")] public string PolicyId { get; set; }
            [Column("CLAIM_ID")] public string ClaimId { get; set; }
            [Column("COMPANY_ID")] public string CompanyId { get; set; }

        }
        [Table("V_SGM_CLAIM_COVER")]
        public class V_SgmClaimCover
        {
            [Column("VARYASYONNO")] public string VaryasyonNo { get; set; }
            [Column("TEMINATNO")] public string TeminatNo { get; set; }
            [Column("ODEMETUTARITL")] public string OdemeTutariTL { get; set; }
            [Column("ODEMETIPI")] public string OdemeTipi { get; set; }
            [Column("ODEMESIRANO")] public string OdemeSiraNo { get; set; }
            [Column("MUALLAKTUTARITL")] public string MuallakTutariTL { get; set; }
            [Column("DONEMNO")] public string DonemNo { get; set; }
            [Key]
            [Column("POLICY_ID")] public string PolicyId { get; set; }
            [Column("CLAIM_ID")] public string ClaimId { get; set; }
            [Column("COMPANY_ID")] public string CompanyId { get; set; }

        }
        [Table("V_SGM_CLAIM_ICD")]
        public class V_SgmClaimIcd
        {
            [Column("ISLEMTIPI")] public string IslemTipi { get; set; }
            [Column("VARYASYONNO")] public string VaryasyonNo { get; set; }
            [Column("TEMINATNO")] public string TeminatNo { get; set; }
            [Column("DONEMNO")] public string DonemNo { get; set; }
            [Column("ICDTIP")] public string IcdTip { get; set; }
            [Column("ICDNO")] public string IcdNo { get; set; }
            [Key]
            [Column("POLICY_ID")] public string PolicyId { get; set; }
            [Column("CLAIM_ID")] public string ClaimId { get; set; }
            [Column("COMPANY_ID")] public string CompanyId { get; set; }

        }
        [Table("V_SGM_CLAIM_INSERT")]
        public class V_SgmClaimInsert
        {
            [Column("SAGLIKKURUMNOYENI")] public string SaglikKurumNoYeni { get; set; }
            [Column("SAGLIKKURUMNOTIPIYENI")] public string SaglikKurumNoTipiYeni { get; set; }
            [Column("KURUMULKEYENI")] public string KurumUlkeYeni { get; set; }
            [Column("KURUMILIYENI")] public string KurumIliYeni { get; set; }
            [Column("KURUMILCESIYENI")] public string KurumIlcesiYeni { get; set; }
            [Column("DOSYAGUNCELLEMETARIHI")] public string DosyaGuncellemeTarihi { get; set; }
            [Column("SAGLIKKURUMNOTIPI")] public string SaglikKurumNoTipi { get; set; }
            [Column("SAGLIKKURUMNO")] public string SaglikKurumNo { get; set; }
            [Column("KURUMULKE")] public string KurumUlke { get; set; }
            [Column("KURUMILI")] public string KurumIli { get; set; }
            [Column("KURUMILCESI")] public string KurumIlcesi { get; set; }
            [Column("TEDAVITIPI")] public string TedaviTipi { get; set; }
            [Column("TAZMINATSIRANO")] public string TazminatSiraNo { get; set; }
            [Column("TAZMINATDOSYADURUMU")] public string TazminatDosyaDurumu { get; set; }
            [Column("PROVIZYONTARIHI")] public string ProvizyonTarihi { get; set; }
            [Column("PARABIRIMI")] public string ParaBirimi { get; set; }
            [Column("OTORIZASYONKOD")] public string OtorizasyonKodu { get; set; }
            [Column("OLAYTARIHI")] public string OlayTarihi { get; set; }
            [Column("ODEMEYERI")] public string OdemeYeri { get; set; }
            [Column("ODEMETUTARITL")] public string OdemeTutarTL { get; set; }
            [Column("MUALLAKTUTARITL")] public string MuallakTutariTL { get; set; }
            [Column("MUALLAKODEMETARIHI")] public string MuallakOdemeTarihi { get; set; }
            [Column("KIMLIKTIPKOD")] public string KimlikTipKodu { get; set; }
            [Column("KIMLIKNO")] public string KimlikNo { get; set; }
            [Column("IHBARTARIHI")] public string IhbarTarihi { get; set; }
            [Column("DOSYAREDNEDENI")] public string DosyaRedNedeni { get; set; }
            [Column("TAZMINATDOSYANO")] public string TazminatDosyaNo { get; set; }
            [Column("SBMSAGMERNO")] public string SbmSagmerNo { get; set; }
            [Key]
            [Column("POLICY_ID")] public string PolicyId { get; set; }
            [Column("CLAIM_ID")] public string ClaimId { get; set; }
            [Column("COMPANY_ID")] public string CompanyId { get; set; }

        }
        [Table("V_SGM_POLICY_DELETE")]
        public class V_SgmPolicyDelete
        {

            [Column("YENILEMENO")] public string YenilemeNo { get; set; }
            [Column("SBMSAGMERNO")] public string SbmSagmerNo { get; set; }
            [Column("POLICENO")] public string PoliceNo { get; set; }
            [Column("OTORIZASYONKOD")] public string OtorizasyonKod { get; set; }
            [Column("ENDORSEMENT_ID")] public string EndorsementId { get; set; }
            [Column("COMPANY_ID")] public string CompanyId { get; set; }
            [Key]
            [Column("POLICY_ID")] public string PolicyId { get; set; }

        }
        [Table("V_SGM_ENDORS_POLICY_PREMIUM")]
        public class V_SgmEndorsPolicyPremium
        {
            [Column("COMPANY_ID")]
            public string CompanyId { get; set; }
            [Column("ZEYLNO")] public string ZeylNo { get; set; }
            [Column("ZEYLNETPRIMI")] public string ZeylNetPrimi { get; set; }
            [Column("ZEYLBRUTPRIMI")] public string ZeylBrutPrimi { get; set; }
            [Column("ZEYLBASLANGICTARIHI")] public string ZeylBaslangicTarihi { get; set; }
            [Column("YENILEMENO")] public string YenilemeNo { get; set; }
            [Column("SIGORTASIRKETKOD")] public string SigortaSirketKod { get; set; }
            [Column("SBMSAGMERNO")] public string SbmSagmerNo { get; set; }
            [Column("POLICENO")] public string PoliceNo { get; set; }
            [Column("POLICENETPRIMI")] public string PoliceNetPrimi { get; set; }
            [Column("POLICEBRUTPRIMI")] public string PoliceBrutPrimi { get; set; }
            [Column("OTORIZASYONKOD")] public string OtorizasyonKod { get; set; }
            [Column("ENDORSEMENT_ID")] public string EndorsementId { get; set; }
            [Key]
            [Column("POLICY_ID")] public string PolicyId { get; set; }
            [Column("IPTALZEYLTURU")] public string IptalZeylTuru { get; set; }
            [Column("IPTALNEDENKOD")] public string IptalNedenKod { get; set; }
            [Column("URETIMKAYNAKKOD")] public string UretimKaynakKod { get; set; }
            [Column("ZEYLTIPI")] public string ZeylTipi { get; set; }
            [Column("ZEYLTANZIMTARIHI")] public string ZeylTanzimTarihi { get; set; }

        }
        [Table("V_SGM_ENDORS_POLICY_INSURER")]
        public class V_SgmEndorsPolicyInsurer
        {
            [Column("COMPANY_ID")]
            public string CompanyId { get; set; }
            [Column("DOGUMYERI")] public string DogumYeri { get; set; }
            [Column("DOGUMTARIHI")] public string DogumTarihi { get; set; }
            [Column("CINSIYET")] public string Cinsiyet { get; set; }
            [Column("BABAADI")] public string BabaAdi { get; set; }
            [Column("ADRES")] public string Adres { get; set; }
            [Column("AD")] public string Ad { get; set; }
            [Column("ZEYLNETPRIMI")] public string ZeylNetPrimi { get; set; }
            [Column("ZEYLBRUTPRIMI")] public string ZeylBrutPrimi { get; set; }
            [Column("POLICENETPRIMI")] public string PoliceNetPrimi { get; set; }
            [Column("POLICEBRUTPRIMI")] public string PoliceBrutPrimi { get; set; }
            [Column("ZEYLTANZIMTARIHI")] public string ZeylTanzimTarihi { get; set; }
            [Column("ZEYLNO")] public string ZeylNo { get; set; }
            [Column("ZEYLBASLANGICTARIHI")] public string ZeylBaslangicTarihi { get; set; }
            [Column("YENILEMENO")] public string YenilemeNo { get; set; }
            [Column("SIGORTASIRKETKOD")] public string SigortaSirketKod { get; set; }
            [Column("SBMSAGMERNO")] public string SbmSagmerNo { get; set; }
            [Column("POLICENO")] public string PoliceNo { get; set; }
            [Column("OTORIZASYONKOD")] public string OtorizasyonKod { get; set; }
            [Column("ENDORSEMENT_ID")] public string EndorsementId { get; set; }
            [Key]
            [Column("POLICY_ID")] public string PolicyId { get; set; }
            [Column("ULKEKODU")] public string UlkeKodu { get; set; }
            [Column("TURKOD")] public string TurKod { get; set; }
            [Column("SOYAD")] public string Soyad { get; set; }
            [Column("KURULUSYERI")] public string KurulusYeri { get; set; }
            [Column("KURULUSTARIHI")] public string KurulusTarihi { get; set; }
            [Column("KIMLIKTIPKOD")] public string KimlikTipKod { get; set; }
            [Column("KIMLIKNO")] public string KimlikNo { get; set; }

        }
        [Table("V_SGM_INGRESS_ENDORS_INSURED")]
        public class V_SgmIngressEndorsInsured
        {
            [Column("ADRES")]
            public string Adres { get; set; }
            [Column("AILEBAGNO")]
            public string AileBagNo { get; set; }
            [Column("BABAADI")]
            public string BabaAdi { get; set; }
            [Column("BIREYSIRANO")]
            public string BireySiraNo { get; set; }
            [Column("BIREYTIPKOD")]
            public string BireyTipKod { get; set; }
            [Column("BRUTPRIM")]
            public string BrutPrim { get; set; }
            [Column("CINSIYET")]
            public string Cinsiyet { get; set; }
            [Column("DOGUMTARIHI")]
            public string DogumTarihi { get; set; }
            [Column("DOGUMYERI")]
            public string DogumYeri { get; set; }
            [Column("ENDORSEMENT_ID")]
            public string EndorsementId { get; set; }
            [Column("ESKIPOLICENO")]
            public string EskiPoliceNo { get; set; }
            [Column("ESKIYENILEMENO")]
            public string EskiYenilemeNo { get; set; }
            [Column("ILKOZELSIGORTALILIKTARIHI")]
            public string IlkOzelSigortalilikTarihi { get; set; }
            [Column("ILKOZELSIRKETKOD")]
            public string IlkOzelSirketKod { get; set; }
            [Key]
            [Column("INSURED_ID")]
            public string InsuredId { get; set; }
            [Column("KIMLIKNO")]
            public string KimlikNo { get; set; }
            [Column("KIMLIKTIPKOD")]
            public string KimlikTipKod { get; set; }
            [Column("MESLEKKOD")]
            public string MeslekKod { get; set; }
            [Column("NETPRIM")]
            public string NetPrim { get; set; }
            [Column("POLICY_ID")]
            public string PolicyId { get; set; }
            [Column("SIGORTALIAD")]
            public string SigortaliAd { get; set; }
            [Column("SIGORTALISOYAD")]
            public string SigortaliSoyad { get; set; }
            [Column("SIGORTALIUYRUK")]
            public string SigortaliUyruk { get; set; }
            [Column("ULKEKODU")]
            public string UlkeKodu { get; set; }
            [Column("URUNPLANKOD")]
            public string UrunPlanKod { get; set; }
            [Column("YASADIGIILCEKOD")]
            public string YasadigiIlceKod { get; set; }
            [Column("YASADIGIILKOD")]
            public string YasadigiIlKod { get; set; }
        }
        [Table("V_SGM_INGRESS_EXTRA_PREMIUM")]
        public class V_SgmIngressExtraPremium
        {
            [Column("ACIKLAMA")]
            public string Aciklama { get; set; }
            [Column("ENDORSEMENT_ID")]
            public string EndorsementId { get; set; }
            [Column("INDIRIMEKPRIMDEGERI")]
            public string IndirimEkPrimDegeri { get; set; }
            [Column("INDIRIMEKPRIMKOD")]
            public string IndirimEkPrimKod { get; set; }
            [Key]
            [Column("INSURED_ID")]
            public string InsuredId { get; set; }
            [Column("KAYITNO")]
            public string KayitNo { get; set; }
            [Column("POLICY_ID")]
            public string PolicyId { get; set; }
            [Column("TUTARMIORANMI")]
            public string TutarMiOranMi { get; set; }

        }
        [Table("V_SGM_EGRESS_ENDORSEMENT")]
        public class V_SgmEgressEndorsement
        {
            [Column("COMPANY_ID")]
            public string CompanyId { get; set; }
            [Column("ENDORSEMENT_ID")]
            public string EndorsementId { get; set; }
            [Key]
            [Column("POLICY_ID")]
            public string PolicyId { get; set; }
            [Column("BRUTPRIM")]
            public string BrutPrim { get; set; }
            [Column("KIMLIKNO")]
            public string KimlikNo { get; set; }
            [Column("KIMLIKTIPKOD")]
            public string KimlikTipKod { get; set; }
            [Column("NETPRIM")]
            public string NetPrim { get; set; }
            [Column("OTORIZASYONKOD")]
            public string OtorizasyonKod { get; set; }
            [Column("POLICEBRUTPRIMI")]
            public string PoliceBrutPrimi { get; set; }
            [Column("POLICENETPRIMI")]
            public string PoliceNetPrimi { get; set; }
            [Column("POLICENO")]
            public string PoliceNo { get; set; }
            [Column("SBMSAGMERNO")]
            public string SbmSagmerNo { get; set; }
            [Column("SIGORTASIRKETKOD")]
            public string SigortaSirketKod { get; set; }
            [Column("YENILEMENO")]
            public string YenilemeNo { get; set; }
            [Column("ZEYLBASLANGICTARIHI")]
            public string ZeylBaslangicTarihi { get; set; }
            [Column("ZEYLBRUTPRIMI")]
            public string ZeylBrutPrimi { get; set; }
            [Column("ZEYLNETPRIMI")]
            public string ZeylNetPrimi { get; set; }
            [Column("ZEYLNO")]
            public string ZeylNo { get; set; }
            [Column("ZEYLTANZIMTARIHI")]
            public string ZeylTanzimTarihi { get; set; }
        }
        [Table("V_SGM_INGRESS_INS_HEALTH_INFO")]
        public class V_SgmIngressInsHealthInfo
        {
            [Column("ALKOLKULLANIYORMU")]
            public string AlkolKullaniliyorMu { get; set; }
            [Column("ALKOLKULLANMASURESI")]
            public string AlkolKullanmaSuresi { get; set; }
            [Column("ALKOLPERIYOTTIP")]
            public string AlkolPeriyotTip { get; set; }
            [Column("BOY")]
            public string Boy { get; set; }
            [Column("ENDORSEMENT_ID")]
            public string EndorsementId { get; set; }
            [Key]
            [Column("INSURED_ID")]
            public string InsuredId { get; set; }
            [Column("KILO")]
            public string Kilo { get; set; }
            [Column("PERIYOTTAKIALKOLMIKTARI")]
            public string PeriyottakiAlkolMiktari { get; set; }
            [Column("PERIYOTTAKISIGARAADEDI")]
            public string PeriyottakiSigaraAdedi { get; set; }
            [Column("POLICY_ID")]
            public string PolicyId { get; set; }
            [Column("SIGARAICIYORMU")]
            public string SigaraIciyorMu { get; set; }
            [Column("SIGARAICMESURESI")]
            public string SigaraIcmeSuresi { get; set; }
            [Column("SIGARAPERIYOTTIP")]
            public string SigaraperiyotTip { get; set; }
        }
        [Table("V_SGM_INGRESS_INS_HEALTH_DET")]
        public class V_SgmIngressInsHealthDet
        {
            [Column("AMELIYATVARMI")]
            public string AmeliyatVarMi { get; set; }
            [Column("ENDORSEMENT_ID")]
            public string EndorsementId { get; set; }
            [Column("HASTALIKACIKLAMASI")]
            public string HastalikAciklamasi { get; set; }
            [Column("HASTALIKADI")]
            public string HastalikAdi { get; set; }
            [Column("HASTALIKKOD")]
            public string HastalikKod { get; set; }
            [Column("HASTALIKUYGULAMAKOD")]
            public string HastalikUygulamaKod { get; set; }
            [Key]
            [Column("INSURED_ID")]
            public string InsuredId { get; set; }
            [Column("POLICY_ID")]
            public string PolicyId { get; set; }
            [Column("SONDURUM")]
            public string SonDurum { get; set; }
            [Column("SUREAY")]
            public string SureAy { get; set; }
            [Column("SUREYIL")]
            public string SureYil { get; set; }
            [Column("UYGULAMAACIKLAMASI")]
            public string UygulamaAciklamasi { get; set; }
        }

        [Table("V_SGM_INGRESS_INS_EXEMPTION")]
        public class V_SgmIngressInsExemption
        {
            [Column("ACIKLAMA")]
            public string Aciklama { get; set; }
            [Column("ENDORSEMENT_ID")]
            public string EndorsementId { get; set; }
            [Column("HASTALIKKOD")]
            public string HastalikKod { get; set; }
            [Key]
            [Column("INSURED_ID")]
            public string InsuredId { get; set; }
            [Column("KAYITNO")]
            public string KayitNo { get; set; }
            [Column("MUAFIYETTUTARLIMITI")]
            public string MuafiyetTutarLimiti { get; set; }
            [Column("POLICY_ID")]
            public string PolicyId { get; set; }
            [Column("SIGORTALIUYGULAMAKOD")]
            public string SigortaliUygulamaKod { get; set; }
        }
        [Table("V_SGM_INGRESS_INS_COVER")]
        public class V_SgmIngressInsCover
        {
            [Key]
            [Column("POLICY_ID")]
            public string PolicyId { get; set; }

            [Column("ENDORSEMENT_ID")]
            public string EndorsementId { get; set; }
            [Column("INSURED_ID")]
            public string InsuredId { get; set; }
            [Column("ASO")]
            public string Aso { get; set; }

            [Column("COGRAFIKAPSAMKODU")]
            public string CografiKapsamKodu { get; set; }
            [Column("DONEMBASLANGICTARIHI")]
            public string DonemBaslangicTarihi { get; set; }
            [Column("DONEMBITISTARIHI")]
            public string DonemBitisTarihi { get; set; }
            [Column("DONEMKODU")]
            public string DonemKodu { get; set; }
            [Column("DOVIZCINSI")]
            public string DovizCinsi { get; set; }
            [Column("TEMINATADEDI")]
            public string TeminatAdedi { get; set; }

            [Column("TEMINATKODU")]
            public string TeminatKodu { get; set; }
            [Column("TEMINATLIMITI")]
            public string TeminatLimiti { get; set; }
            [Column("TEMINATLIMITKODU")]
            public string TeminatLimitKodu { get; set; }
            [Column("TEMINATMUAFIYETI")]
            public string TeminatMuafiyeti { get; set; }
            [Column("TEMINATMUAFIYETKODU")]
            public string TeminatMuafiyetKodu { get; set; }
            [Column("TEMINATNETPRIM")]
            public string TeminatNetPrim { get; set; }
            [Column("TEMINATODEMEYUZDESI")]
            public string TeminatOdemeYuzdesi { get; set; }
            [Column("TEMINATVARYASYONKODU")]
            public string TeminatVaryasyonKodu { get; set; }
            [Column("TOPLAMDONEMSAYISI")]
            public string ToplamDonemSayisi { get; set; }
        }
        [Table("V_SGM_INGRESS_ENDORSEMENT")]
        public class V_SgmIngressEndorsement
        {
            [Column("ENDORSEMENT_ID")]
            public string EndorsementId { get; set; }

            [Column("OTORIZASYONKOD")]
            public string OtorizasyonKod { get; set; }
            [Column("POLICEBRUTPRIMI")]
            public string PoliceBrutPrimi { get; set; }
            [Column("POLICENETPRIMI")]
            public string PoliceNetPrimi { get; set; }
            [Column("POLICENO")]
            public string PoliceNo { get; set; }
            [Key]
            [Column("POLICY_ID")]
            public string PolicyId { get; set; }
            [Column("SBMSAGMERNO")]
            public string SbmSagmerNo { get; set; }
            [Column("SIGORTASIRKETKOD")]
            public string SigortaSirketKod { get; set; }
            [Column("YENILEMENO")]
            public string YenilemeNo { get; set; }

            [Column("ZEYLBASLANGICTARIHI")]
            public string ZeylBaslangicTarihi { get; set; }
            [Column("ZEYLBRUTPRIMI")]
            public string ZeylBrutPrimi { get; set; }
            [Column("ZEYLNETPRIMI")]
            public string ZeylNetPrimi { get; set; }
            [Column("ZEYLNO")]
            public string ZeylNo { get; set; }
            [Column("ZEYLTANZIMTARIHI")]
            public string ZeylTanzimTarihi { get; set; }
            [Column("ZEYLTURU")]
            public string ZeylTuru { get; set; }
            [Column("COMPANY_ID")]
            public string CompanyId { get; set; }

        }
        [Table("V_FORM_BILL_APPROVE")]
        public class V_FormBillApprove
        {
            [Column("PAID")] public string PAID { get; set; }
            [Column("REASON_NAME")] public string REASON_NAME { get; set; }
            [Column("COMPANY_FAX")] public string COMPANY_FAX { get; set; }
            [Column("COMPANY_PHONE")] public string COMPANY_PHONE { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("COMPANY_ID")] public string COMPANY_ID { get; set; }
            [Column("AGENCY_NAME")] public string AGENCY_NAME { get; set; }
            [Column("PROCESS_LIST")] public string PROCESS_LIST { get; set; }
            [Column("ICD_LIST")] public string ICD_LIST { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Key] [Column("CLAIM_ID")] public string CLAIM_ID { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("INSURED_REGISTRATION_NO")] public string INSURED_REGISTRATION_NO { get; set; }
            [Column("POLICY_NUMBER")] public Int64? POLICY_NUMBER { get; set; }
            [Column("INSURED_NAME_SURNAME")] public string INSURED_NAME_SURNAME { get; set; }
            [Column("INSURED_CONTACT_ID")] public string INSURED_CONTACT_ID { get; set; }

            [Column("PROVISION_DATE")] public string PROVISION_DATE { get; set; }


        }

        [Table("V_FORM_BILL_APPROVE_LIST")]
        public class V_FormBillApproveList
        {
            [Column("INSURED_AMOUNT")] public string INSURED_AMOUNT { get; set; }
            [Column("PAID")] public string PAID { get; set; }
            [Column("ACCEPTED")] public string ACCEPTED { get; set; }
            [Column("REQUESTED")] public string REQUESTED { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Key] [Column("CLAIM_ID")] public string CLAIM_ID { get; set; }


        }
        [Table("V_FORM_CLAIM_MISSING")]
        public class V_FormClaimMissing
        {
            [Column("BILL_AMOUNT")] public string BILL_AMOUNT { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("COMPANY_ID")] public string COMPANY_ID { get; set; }
            [Column("TEDAVI")] public string TEDAVI { get; set; }
            [Column("TANI")] public string TANI { get; set; }
            [Column("AGENCY_NAME")] public string AGENCY_NAME { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Key]
            [Column("CLAIM_ID")] public string CLAIM_ID { get; set; }
            [Column("COMPANY_CLAIM_NO")] public string COMPANY_CLAIM_NO { get; set; }
            [Column("COMPANY_CLAIM_ID")] public string COMPANY_CLAIM_ID { get; set; }
            //[Column("COMPANY_FAX")] public string COMPANY_FAX { get; set; }
            //[Column("COMPANY_PHONE")] public string COMPANY_PHONE { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("INSURED_REGISTRATION_NO")] public string INSURED_REGISTRATION_NO { get; set; }
            [Column("INSURED_CONTACT_ID")] public string INSURED_CONTACT_ID { get; set; }
            [Column("POLICY_NUMBER")] public Int64? POLICY_NUMBER { get; set; }
            [Column("INSURER_NAME")] public string INSURER_NAME { get; set; }
            [Column("INSURED_NAME_SURNAME")] public string INSURED_NAME_SURNAME { get; set; }
            [Column("MISSING_DATE")] public string MISSING_DATE { get; set; }

        }
        [Table("V_FORM_CLAIM_MISSING_LIST")]
        public class V_FormClaimMissingList
        {
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("COMPANY_ID")] public string COMPANY_ID { get; set; }
            [Column("MISSING_TYPE_TEXT")] public string MISSING_TYPE_TEXT { get; set; }
            [Column("MISSING_TYPE_CODE")] public string MISSING_TYPE_CODE { get; set; }
            [Column("MISSING_TYPE")] public string MISSING_TYPE { get; set; }
            [Column("MISSING_DATE")] public string MISSING_DATE { get; set; }
            [Column("CLAIM_MISSING_ID")] public string CLAIM_MISSING_ID { get; set; }
            [Key] [Column("CLAIM_ID")] public string CLAIM_ID { get; set; }


        }
        [Table("V_FORM_CLAIM_PROVIDER_BACK")]
        public class V_FormClaimProviderBack
        {
            [Column("PAID")] public string PAID { get; set; }
            [Column("CLAIM_NOTE_LIST")] public string CLAIM_NOTE_LIST { get; set; }
            [Column("REASON")] public string REASON { get; set; }
            [Column("BILL_NO")] public string BILL_NO { get; set; }
            [Column("BILL_DATE")] public string BILL_DATE { get; set; }
            [Column("PROVIDER_PACKAGE_NO")] public string PROVIDER_PACKAGE_NO { get; set; }
            [Column("INSURED_NAME_SURNAME")] public string INSURED_NAME_SURNAME { get; set; }
            [Key] [Column("CLAIM_ID")] public string CLAIM_ID { get; set; }
            [Column("PACKAGE_NO")] public string PACKAGE_NO { get; set; }
            [Column("PAYROLL_DATE")] public string PAYROLL_DATE { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("COMPANY_ID")] public string COMPANY_ID { get; set; }


        }
        [Table("V_FORM_CLAIM_PROVIDER_RETURN")]
        public class V_FormClaimProviderReturn
        {
            [Column("REASON_NAME")] public string REASON_NAME { get; set; }
            [Column("COMPANY_FAX")] public string COMPANY_FAX { get; set; }
            [Column("COMPANY_PHONE")] public string COMPANY_PHONE { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("COMPANY_ID")] public string COMPANY_ID { get; set; }
            [Column("REASON_NOTE")] public string REASON_NOTE { get; set; }
            [Column("BILL_NO")] public string BILL_NO { get; set; }
            [Column("BILL_DATE")] public string BILL_DATE { get; set; }
            [Column("PACKAGE_ID")] public string PACKAGE_ID { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Key]
            [Column("CLAIM_ID")] public string CLAIM_ID { get; set; }
            [Column("INSURED_REGISTRATION_NO")] public string INSURED_REGISTRATION_NO { get; set; }
            [Column("INSURED_NAME_SURNAME")] public string INSURED_NAME_SURNAME { get; set; }
            [Column("PROVISION_DATE")] public string PROVISION_DATE { get; set; }
            [Column("COMPANY_CLAIM_ID")]
            public string CompanyClaimId { get; set; }
            [Column("COMPANY_CLAIM_NO")]
            public string CompanyClaimNo { get; set; }
            [Column("PAYROLL_ID")]
            public string PayrollId { get; set; }
            [Column("RETURN_DATE")]
            public string ReturnDate { get; set; }
        }
        [Table("V_FORM_CLAIM_REJECT")]
        public class V_FormClaimReject
        {
            [Column("REASON")] public string REASON { get; set; }
            [Column("COMPANY_FAX")] public string COMPANY_FAX { get; set; }
            [Column("COMPANY_PHONE")] public string COMPANY_PHONE { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("COMPANY_ID")] public string COMPANY_ID { get; set; }
            [Column("PROCESS_LIST")] public string PROCESS_LIST { get; set; }
            [Column("ICD_LIST")] public string ICD_LIST { get; set; }
            [Column("PAID")] public string PAID { get; set; }
            [Column("AGENCY_NAME")] public string AGENCY_NAME { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Key] [Column("CLAIM_ID")] public string CLAIM_ID { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("INSURED_REGISTRATION_NO")] public string INSURED_REGISTRATION_NO { get; set; }
            [Column("INSURED_CONTACT_ID")] public string INSURED_CONTACT_ID { get; set; }
            [Column("POLICY_NUMBER")] public Int64? POLICY_NUMBER { get; set; }
            [Column("INSURED_NAME_SURNAME")] public string INSURED_NAME_SURNAME { get; set; }
            [Column("CLAIM_DATE")] public string CLAIM_DATE { get; set; }


        }
        [Table("V_FORM_CONTACT_CLAIM")]
        public class V_FormContactClaim
        {
            [Column("PAID")] public string PAID { get; set; }
            [Column("REQUESTED")] public string REQUESTED { get; set; }
            [Column("CLAIM_STATUS")] public string CLAIM_STATUS { get; set; }
            [Column("CLAIM_STATUS_ORDINAL")] public int CLAIM_STATUS_ORDINAL { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Column("PROCESS_LIST")] public string PROCESS_LIST { get; set; }
            [Column("ICD_LIST")] public string ICD_LIST { get; set; }
            [Column("CLAIM_NOTE_LIST")] public string CLAIM_NOTE_LIST { get; set; }
            [Column("CLAIM_DATE")] public string CLAIM_DATE { get; set; }
            [Key] [Column("CLAIM_ID")] public string CLAIM_ID { get; set; }
            [Column("POLICY_NO")] public Int64? POLICY_NO { get; set; }
            [Column("PACKAGE_NO")] public string PACKAGE_NO { get; set; }
            [Column("PLAN_NAME")] public string PLAN_NAME { get; set; }
            [Column("PLAN_ID")] public string PLAN_ID { get; set; }
            [Column("CONTACT_ID")] public string CONTACT_ID { get; set; }
            [Column("INSURED_ID")] public string INSURED_ID { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Column("COMPANY_ID")] public string COMPANY_ID { get; set; }


        }
        [Table("V_FORM_CONTACT_POLICY")]
        public class V_FormContactPolicy
        {
            [Column("CLAIM_TOTAL_AMOUNT")] public string CLAIM_TOTAL_AMOUNT { get; set; }
            [Key][Column("CONTACT_ID")] public string CONTACT_ID { get; set; }
            [Column("RENEWAL_GUARANTEE_TYPE")] public string RENEWAL_GUARANTEE_TYPE { get; set; }
            [Column("RENEWAL_GUARANTEE_TEXT")] public string RENEWAL_GUARANTEE_TEXT { get; set; }
            [Column("INSURED_PREMIUM")] public string INSURED_PREMIUM { get; set; }
            [Column("POLICY_END_DATE")] public string POLICY_END_DATE { get; set; }
            [Column("POLICY_START_DATE")] public string POLICY_START_DATE { get; set; }
            [Column("POLICY_NUMBER")] public string POLICY_NUMBER { get; set; }
            [Column("POLICY_ID")] public string POLICY_ID { get; set; }
            [Column("COMPANY_ID")] public string COMPANY_ID { get; set; }

        }
        [Table("V_FORM_INSURED_CLAIM")]
        public class V_FormInsuredClaim
        {
            [Key]
            [Column("INSURED_ID")] public string INSURED_ID { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Column("PAID")] public string PAID { get; set; }
            [Column("REQUESTED")] public string REQUESTED { get; set; }
            [Column("CLAIM_STATUS")] public string CLAIM_STATUS { get; set; }
            [Column("CLAIM_STATUS_ORDINAL")] public int CLAIM_STATUS_ORDINAL { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Column("PROCESS_LIST")] public string PROCESS_LIST { get; set; }
            [Column("ICD_LIST")] public string ICD_LIST { get; set; }
            [Column("CLAIM_NOTE_LIST")] public string CLAIM_NOTE_LIST { get; set; }
            [Column("CLAIM_DATE")] public string CLAIM_DATE { get; set; }
            [Column("CLAIM_ID")] public string CLAIM_ID { get; set; }
            [Column("POLICY_NO")] public string POLICY_NO { get; set; }
            [Column("POLICY_ID")] public string POLICY_ID { get; set; }
            [Column("PACKAGE_NO")] public string PACKAGE_NO { get; set; }
            [Column("PLAN_NAME")] public string PLAN_NAME { get; set; }
            [Column("PLAN_ID")] public string PLAN_ID { get; set; }


        }
        [Table("V_FORM_INSURED_DECLERATION")]
        public class V_FormInsuredDecleration
        {
            [Column("DECLERATION")] public string DECLERATION { get; set; }
            [Key][Column("INSURED_ID")] public string INSURED_ID { get; set; }
        }
        [Table("V_FORM_INSURED_EXCLUSION")]
        public class V_FormInsuredExclusion
        {
            [Column("EXCLUSION")] public string EXCLUSION { get; set; }
            [Key][Column("INSURED_ID")] public string INSURED_ID { get; set; }
        }
        [Table("V_FORM_INSURED_NOTE")]
        public class V_FormInsuredNote
        {
            [Column("NOTE")] public string NOTE { get; set; }
           [Key] [Column("INSURED_ID")] public string INSURED_ID { get; set; }
        }
        [Table("V_FORM_INSURED_PROFILE")]
        public class V_FormInsuredProfile
        {
            [Key]
            [Column("CONTACT_ID")] public string CONTACT_ID { get; set; }
            [Column("INSURED_ID")] public string INSURED_ID { get; set; }
            [Column("COMPANY_ID")] public string COMPANY_ID { get; set; }
            [Column("INSURED_SURPREMIUM")] public string INSURED_SURPREMIUM { get; set; }
            [Column("CLAIM_TOTAL_AMOUNT")] public string CLAIM_TOTAL_AMOUNT { get; set; }
            [Column("INSURED_PREMIUM")] public string INSURED_PREMIUM { get; set; }
            [Column("INSURED_EXIT_DATE")] public string INSURED_EXIT_DATE { get; set; }
            [Column("INSURED_NAME_SURNAME")] public string INSURED_NAME_SURNAME { get; set; }
            [Column("INSURED_IDENTITY_NO")] public string INSURED_IDENTITY_NO { get; set; }
            [Column("INSURED_GENDER")] public string INSURED_GENDER { get; set; }
            [Column("INSURED_BIRTHDATE")] public string INSURED_BIRTHDATE { get; set; }
            [Column("FIRST_INSURED_DATE")] public string FIRST_INSURED_DATE { get; set; }
            [Column("INSURED_COMPANY_START_DATE")] public string INSURED_COMPANY_START_DATE { get; set; }
            [Column("INSURED_START_DATE")] public string INSURED_START_DATE { get; set; }
            [Column("INSURED_REGISTRATION_NO")] public string INSURED_REGISTRATION_NO { get; set; }
            [Column("INSURED_STATUS")] public string INSURED_STATUS { get; set; }
            [Column("POLICY_GROUP_NAME")] public string POLICY_GROUP_NAME { get; set; }
            [Column("INSURER_IDENTITY_NO")] public string INSURER_IDENTITY_NO { get; set; }
            [Column("INSURER_NAME")] public string INSURER_NAME { get; set; }
            [Column("AGENCY_NAME")] public string AGENCY_NAME { get; set; }
            [Column("POLICY_TYPE")] public string POLICY_TYPE { get; set; }
            [Column("POLICY_END_DATE")] public string POLICY_END_DATE { get; set; }
            [Column("POLICY_START_DATE")] public string POLICY_START_DATE { get; set; }
            [Column("POLICY_NUMBER")] public string POLICY_NUMBER { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("RENEWAL_GUARANTEE_TYPE")] public string RENEWAL_GUARANTEE_TYPE { get; set; }
            [Column("RENEWAL_GUARANTEE_TEXT")] public string RENEWAL_GUARANTEE_TEXT { get; set; }
            [Column("POLICY_ID")] public string POLICY_ID { get; set; }

        }
        [Table("V_FORM_PAYROLL")]
        public class V_FormPayroll
        {
            [Column("BANK_BRANCH_NAME")] public string BANK_BRANCH_NAME { get; set; }
            [Column("BANK_BRANCH_ID")] public string BANK_BRANCH_ID { get; set; }
            [Column("BANK_NAME")] public string BANK_NAME { get; set; }
            [Column("BANK_ID")] public string BANK_ID { get; set; }
            [Column("BANK_ACCOUNT_NAME")] public string BANK_ACCOUNT_NAME { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Column("PAYMENT_DATE")] public string PAYMENT_DATE { get; set; }
            [Column("PAYROLL_DATE")] public string PAYROLL_DATE { get; set; }
            [Column("PAYROLL_DUE_DATE")] public string PAYROLL_DUE_DATE { get; set; }
            [Column("PAYROLL_TYPE")] public string PAYROLL_TYPE { get; set; }
            [Column("COMPANY_PAYROLL_NO")] public string COMPANY_PAYROLL_NO { get; set; }
            [Column("EXT_PROVIDER_NO")] public string EXT_PROVIDER_NO { get; set; }
            [Key] [Column("PAYROLL_ID")] public string PAYROLL_ID { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("COMPANY_ID")] public string COMPANY_ID { get; set; }
            [Column("IBAN")] public string IBAN { get; set; }
            [Column("BANK_ACCOUNT_NO")] public string BANK_ACCOUNT_NO { get; set; }
            [Column("TOTAL_AMOUNT")] public string TOTAL_AMOUNT { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }


        }
        [Table("V_FORM_PAYROLL_CLAIM")]
        public class V_FormPayrollClaim
        {
            [Column("MAIN_COVERAGE_NAME")] public string MAIN_COVERAGE_NAME { get; set; }
            [Column("INSURER_NAME")] public string INSURER_NAME { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Column("COMPANY_ID")] public string COMPANY_ID { get; set; }
            [Column("PAID")] public string PAID { get; set; }
            [Column("INSURED_AMOUNT")] public string INSURED_AMOUNT { get; set; }
            [Column("REQUESTED")] public string REQUESTED { get; set; }
            [Column("COVERAGE_TYPE")] public string COVERAGE_TYPE { get; set; }
            [Column("CLAIM_STATUS")] public string CLAIM_STATUS { get; set; }
            [Column("BILL_NO")] public string BILL_NO { get; set; }
            [Column("BILL_DATE")] public string BILL_DATE { get; set; }
            [Column("CLAIM_DATE")] public string CLAIM_DATE { get; set; }
            [Column("ICD")] public string ICD { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Column("INSURED_NAME_LASTNAME")] public string INSURED_NAME_LASTNAME { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("POLICY_NUMBER")] public Int64? POLICY_NUMBER { get; set; }
            [Column("COMPANY_CLAIM_NO")] public string COMPANY_CLAIM_NO { get; set; }
            [Column("COMPANY_CLAIM_ID")] public string COMPANY_CLAIM_ID { get; set; }
            [Column("CLAIM_ID")] public Int64 CLAIM_ID { get; set; }
            [Key] [Column("PAYROLL_ID")] public string PAYROLL_ID { get; set; }


        }
        [Table("V_FORM_ENDORSEMENT_PAYMENT")]
        public class V_FormEndorsementPayment
        {
            [Key]
            [Column("POLICY_ID")]
            public string POLICY_ID { get; set; }
            [Column("ENDORSEMENT_ID")]
            public string ENDORSEMENT_ID { get; set; }
            [Column("PAYMENT_DATE")]
            public string PAYMENT_DATE { get; set; }
            [Column("PAYMENT_TYPE")]
            public string PAYMENT_TYPE { get; set; }
            [Column("COMPANY_PAYMENT_TYPE_TEXT")]
            public string COMPANY_PAYMENT_TYPE_TEXT { get; set; }
            [Column("PAYMENT_AMOUNT")]
            public string PAYMENT_AMOUNT { get; set; }
        }
        [Table("V_FORM_INSURED_COVERAGE")]
        public class V_FormInsuredCoverage
        {
            [Key]
            [Column("ENDORSEMENT_ID")]
            public string ENDORSEMENT_ID { get; set; }
            [Column("INSURED_ID")]
            public string INSURED_ID { get; set; }
            [Column("PACKAGE_ID")]
            public string PACKAGE_ID { get; set; }
            [Column("PACKAGE_NO")]
            public string PACKAGE_NO { get; set; }
            [Column("PACKAGE_NAME")]
            public string PACKAGE_NAME { get; set; }
            [Column("COMPANY_NAME")]
            public string COMPANY_NAME { get; set; }
            [Column("NETWORK_NAME")]
            public string NETWORK_NAME { get; set; }
            [Column("COVERAGE_NAME")]
            public string COVERAGE_NAME { get; set; }
            [Column("AGR_AGREEMENT_TYPE")]
            public string AGR_AGREEMENT_TYPE { get; set; }
            [Column("AGR_CURRENCY_TYPE")]
            public string AGR_CURRENCY_TYPE { get; set; }
            [Column("AGREED_NETWORK_LIMIT")]
            public string AGREED_NETWORK_LIMIT { get; set; }
            [Column("AGREED_NETWORK_COINSURANCE")]
            public string AGREED_NETWORK_COINSURANCE { get; set; }
            [Column("NAG_AGREEMENT_TYPE")]
            public string NOT_AGREEMENT_TYPE { get; set; }
            [Column("NAG_AGREED_NETWORK_LIMIT")]
            public string NOT_AGREED_NETWORK_LIMIT { get; set; }
            [Column("NOT_AGREED_NETWORK_COINSURANCE")]
            public string NOT_AGREED_NETWORK_COINSURANCE { get; set; }
            [Column("NAG_CURRENCY_TYPE")]
            public string NAG_AGREED__CURRENCY_TYPE { get; set; }
            [Column("COVERAGE_ID")]
            public string COVERAGE_ID { get; set; }
            [Column("IS_MAIN_COVERAGE")]
            public string IS_MAIN_COVERAGE { get; set; }
            [Column("IS_OPEN_TO_PRINT")]
            public string IS_OPEN_TO_PRINT { get; set; }
            [Column("MAIN_COVERAGE_ID")]
            public string MAIN_COVERAGE_ID { get; set; }
        }
        [Table("V_FORM_ENDORSEMENT_INSURED")]
        public class V_FormEndorsementInsured
        {
            [Key]
            [Column("POLICY_ID")]
            public string POLICY_ID { get; set; }
            [Column("ENDORSEMENT_ID")]
            public string ENDORSEMENT_ID { get; set; }
            [Column("REGISTRATION_NO")]
            public string REGISTRATION_NO { get; set; }
            [Column("NAME_SURNAME")]
            public string NAME_SURNAME { get; set; }
            [Column("INDIVIDUAL_TYPE")]
            public string INDIVIDUAL_TYPE { get; set; }
            [Column("BIRTHDATE")]
            public string BIRTHDATE { get; set; }
            [Column("IDENTITY_NO")]
            public string IDENTITY_NO { get; set; }
            [Column("GENDER")]
            public string GENDER { get; set; }
            [Column("PLAN_NAME")]
            public string PLAN_NAME { get; set; }
            [Column("FIRST_INSURED_DATE")]
            public string FIRST_INSURED_DATE { get; set; }
            [Column("PREMIUM")]
            public string PREMIUM { get; set; }
            [Column("ANNUAL_PREMIUM")]
            public string ANNUAL_PREMIUM { get; set; }
            [Column("EXTRA_PREMIUM")]
            public string EXTRA_PREMIUM { get; set; }
            [Column("EXEMPTION")]
            public string EXEMPTION { get; set; }
        }
        [Table("V_FORM_POLICY_ENDORSEMENT")]
        public class V_FormPolicyEndorsement
        {
            [Key]
            [Column("AGENCY_NO")]
            public string AGENCY_NO { get; set; }
            [Column("AGENCY_NAME")]
            public string AGENCY_NAME { get; set; }
            [Column("AGENCY_PHONE")]
            public string AGENCY_PHONE { get; set; }
            [Column("AGENCY_FAX")]
            public string AGENCY_FAX { get; set; }
            [Column("AGENCY_EMAIL")]
            public string AGENCY_EMAIL { get; set; }
            [Column("AGENCY_WEB_ADDRESS")]
            public string AGENCY_WEB_ADDRESS { get; set; }
            [Column("AGENCY_TAX_NO")]
            public string AGENCY_TAX_NO { get; set; }
            [Column("AGENCY_PLATE_NO")]
            public string AGENCY_PLATE_NO { get; set; }
            [Column("AGENCY_ADDRESS")]
            public string AGENCY_ADDRESS { get; set; }
            [Column("POLICY_NO")]
            public string POLICY_NO { get; set; }
            [Column("SBM_NO")]
            public string SBM_NO { get; set; }
            [Column("SBM_TARIFF_NO")]
            public string SBM_TARIFF_NO { get; set; }
            [Column("ENDORSEMENT_START_DATE")]
            public string ENDORSEMENT_START_DATE { get; set; }
            [Column("POLICY_START_DATE")]
            public string POLICY_START_DATE { get; set; }
            [Column("POLICY_ISSUE_DATE")]
            public string POLICY_ISSUE_DATE { get; set; }
            [Column("POLICY_END_DATE")]
            public string POLICY_END_DATE { get; set; }
            [Column("POLICY_PAYMENT_TYPE")]
            public string POLICY_PAYMENT_TYPE { get; set; }
            [Column("POLICY_PAYMENT_TYPE_TEXT")]
            public string POLICY_PAYMENT_TYPE_TEXT { get; set; }
            [Column("POLICY_PERIOD")]
            public string POLICY_PERIOD { get; set; }
            [Column("POLICY_PREMIUM")]
            public string POLICY_PREMIUM { get; set; }
            [Column("IS_RENEWAL")]
            public string IS_RENEWAL { get; set; }
            [Column("ENDORSEMENT_DATE_OF_ISSUE")]
            public string ENDORSEMENT_DATE_OF_ISSUE { get; set; }
            [Column("ENDORSEMET_NO")]
            public string ENDORSEMET_NO { get; set; }
            [Column("ENDORSEMENT_PREMIUM")]
            public string ENDORSEMENT_PREMIUM { get; set; }
            [Column("INSURER_NAME")]
            public string INSURER_NAME { get; set; }
            [Column("INSURER_PHONE")]
            public string INSURER_PHONE { get; set; }
            [Column("INSURER_TAX_NO")]
            public string INSURER_TAX_NO { get; set; }
            [Column("INSURER_IDENTITY_NO")]
            public string INSURER_IDENTITY_NO { get; set; }
            [Column("INSURER_ADDRESS")]
            public string INSURER_ADDRESS { get; set; }
            [Column("PLAN_NAME")]
            public string PLAN_NAME { get; set; }
            [Column("POLICY_ID")]
            public string POLICY_ID { get; set; }
            [Column("ENDORSEMENT_ID")]
            public string ENDORSEMENT_ID { get; set; }
            [Column("ENDORSEMENT_TYPE")]
            public string ENDORSEMENT_TYPE { get; set; }
            [Column("PRODUCT_NAME")]
            public string PRODUCT_NAME { get; set; }
            [Column("COMPANY_ID")]
            public string COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")]
            public string COMPANY_NAME { get; set; }
            [Column("SUBPRODUCT_NAME")]
            public string SUBPRODUCT_NAME { get; set; }
        }
        [Table("V_FORM_POLICY_COVERAGE")]
        public class V_FormPolicyCoverage
        {
            [Column("PACKAGE_ID")] public string PACKAGE_ID { get; set; }
            [Key] [Column("INSURED_ID")] public string INSURED_ID { get; set; }
            [Column("POLICY_ID")] public string POLICY_ID { get; set; }
            [Column("POLICY_NO")] public string POLICY_NO { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("PACKAGE_NAME")] public string PACKAGE_NAME { get; set; }
            [Column("PACKAGE_NO")] public string PACKAGE_NO { get; set; }
            [Column("PLAN_NAME")] public string PLAN_NAME { get; set; }
            [Column("PLAN_NO")] public string PLAN_NO { get; set; }
            [Column("COVERAGE_ID")] public long? COVERAGE_ID { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Column("MAIN_COVERAGE_NAME")] public string MAIN_COVERAGE_NAME { get; set; }
            [Column("MAIN_COVERAGE_ID")] public long? MAIN_COVERAGE_ID { get; set; }

        }
        [Table("V_FORM_PROVISION_APPROVE")]
        public class V_FormProvisionApprove
        {
            [Column("PAID")] public string PAID { get; set; }
            [Column("REASON_NAME")] public string REASON_NAME { get; set; }
            [Column("COMPANY_FAX")] public string COMPANY_FAX { get; set; }
            [Column("COMPANY_PHONE")] public string COMPANY_PHONE { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("COMPANY_ID")] public string COMPANY_ID { get; set; }
            [Column("PROCESS_LIST")] public string PROCESS_LIST { get; set; }
            [Column("ICD_LIST")] public string ICD_LIST { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Key] [Column("CLAIM_ID")] public string CLAIM_ID { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("INSURED_REGISTRATION_NO")] public string INSURED_REGISTRATION_NO { get; set; }
            [Column("INSURED_CONTACT_ID")] public string INSURED_CONTACT_ID { get; set; }
            [Column("POLICY_NUMBER")] public Int64? POLICY_NUMBER { get; set; }
            [Column("INSURED_NAME_SURNAME")] public string INSURED_NAME_SURNAME { get; set; }
            [Column("PROVISION_DATE")] public string PROVISION_DATE { get; set; }
            [Column("CLAIM_NOTE_TEDAVI")] public string CLAIM_NOTE_TEDAVI { get; set; }


        }
        [Table("V_FORM_PROVISION_PAYMENT")]
        public class V_FormProvisionPayment
        {
            [Column("IBAN")] public string IBAN { get; set; }
            [Column("BANK_ACCOUNT_NO")] public string BANK_ACCOUNT_NO { get; set; }
            [Column("BANK_BRANCH_NAME")] public string BANK_BRANCH_NAME { get; set; }
            [Column("BANK_BRANCH_ID")] public string BANK_BRANCH_ID { get; set; }
            [Column("BANK_NAME")] public string BANK_NAME { get; set; }
            [Column("BANK_ID")] public string BANK_ID { get; set; }
            [Column("BANK_ACCOUNT_NAME")] public string BANK_ACCOUNT_NAME { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Column("PAYMENT_DATE")] public string PAYMENT_DATE { get; set; }
            [Column("PAYROLL_DATE")] public string PAYROLL_DATE { get; set; }
            [Column("PAYROLL_TYPE")] public string PAYROLL_TYPE { get; set; }
            [Column("COMPANY_PAYROLL_NO")] public string COMPANY_PAYROLL_NO { get; set; }
            [Column("EXT_PROVIDER_NO")] public string EXT_PROVIDER_NO { get; set; }
            [Key] [Column("PAYROLL_ID")] public string PAYROLL_ID { get; set; }


        }
        [Table("V_FORM_PROVISION_PAYMENT_LIST")]
        public class V_FormProvisionPaymentList
        {
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("COMPANY_ID")] public string COMPANY_ID { get; set; }
            [Column("PAID")] public string PAID { get; set; }
            [Column("INSURED_AMOUNT")] public string INSURED_AMOUNT { get; set; }
            [Column("REQUESTED")] public string REQUESTED { get; set; }
            [Column("COVERAGE_TYPE")] public string COVERAGE_TYPE { get; set; }
            [Column("CLAIM_STATUS")] public string CLAIM_STATUS { get; set; }
            [Column("BILL_NO")] public string BILL_NO { get; set; }
            [Column("BILL_DATE")] public string BILL_DATE { get; set; }
            [Column("CLAIM_DATE")] public string CLAIM_DATE { get; set; }
            [Column("ICD")] public string ICD { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Column("INSURED_NAME_LASTNAME")] public string INSURED_NAME_LASTNAME { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("POLICY_NUMBER")] public Int64? POLICY_NUMBER { get; set; }
            [Column("COMPANY_CLAIM_NO")] public string COMPANY_CLAIM_NO { get; set; }
            [Column("COMPANY_CLAIM_ID")] public string COMPANY_CLAIM_ID { get; set; }
            [Key] [Column("CLAIM_ID")] public string CLAIM_ID { get; set; }
            [Column("PAYROLL_ID")] public string PAYROLL_ID { get; set; }


        }
        [Table("V_FORM_PROVISION_REJECT")]
        public class V_FormProvisionReject
        {
            [Column("COMPANY_CLAIM_ID")]
            public string CompanyClaimId { get; set; }
            [Column("COMPANY_CLAIM_NO")]
            public string CompanyClaimNo { get; set; }
            [Column("REASON_NAME")] public string REASON_NAME { get; set; }
            [Column("COMPANY_FAX")] public string COMPANY_FAX { get; set; }
            [Column("COMPANY_PHONE")] public string COMPANY_PHONE { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("COMPANY_ID")] public string COMPANY_ID { get; set; }
            [Column("PROCESS_LIST")] public string PROCESS_LIST { get; set; }
            [Column("ICD_LIST")] public string ICD_LIST { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Key]
            [Column("CLAIM_ID")] public string CLAIM_ID { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("INSURED_REGISTRATION_NO")] public string INSURED_REGISTRATION_NO { get; set; }
            [Column("INSURED_CONTACT_ID")] public string INSURED_CONTACT_ID { get; set; }
            [Column("POLICY_NUMBER")] public Int64? POLICY_NUMBER { get; set; }
            [Column("INSURED_NAME_SURNAME")] public string INSURED_NAME_SURNAME { get; set; }
            [Column("PROVISION_DATE")] public string PROVISION_DATE { get; set; }
        }
        [Table("V_FORM_PROVISION_RELEASE")]
        public class V_FormProvisionRelease
        {
            [Column("COMPANY_FAX")] public string COMPANY_FAX { get; set; }
            [Column("COMPANY_PHONE")] public string COMPANY_PHONE { get; set; }
            [Column("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
            [Column("COMPANY_ID")] public string COMPANY_ID { get; set; }
            [Column("ICD_LIST")] public string ICD_LIST { get; set; }
            [Column("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
            [Column("POLICY_NUMBER")] public Int64? POLICY_NUMBER { get; set; }
            [Column("INSURED_REGISTRATION_NO")] public string INSURED_REGISTRATION_NO { get; set; }
            [Column("INSURED_CONTACT_ID")] public string INSURED_CONTACT_ID { get; set; }
            [Column("INSURED_NAME_SURNAME")] public string INSURED_NAME_SURNAME { get; set; }
            [Column("PROVIDER_NAME")] public string PROVIDER_NAME { get; set; }
            [Key] [Column("CLAIM_ID")] public string CLAIM_ID { get; set; }
            [Column("EVENT_DATE")] public string EVENT_DATE { get; set; }


        }
        [Table("V_FORM_PROVISION_RELEASE_LIST")]
        public class V_FormProvisiomnReleaseList
        {
            [Column("PAID")] public string PAID { get; set; }
            [Column("INSURED_AMOUNT")] public string INSURED_AMOUNT { get; set; }
            [Column("CONFIRMED")] public string CONFIRMED { get; set; }
            [Column("REQUESTED")] public string REQUESTED { get; set; }
            [Column("BILL_NO")] public string BILL_NO { get; set; }
            [Column("BILL_DATE")] public string BILL_DATE { get; set; }
            [Column("COVERAGE_NAME")] public string COVERAGE_NAME { get; set; }
            [Column("COINSURANCE")] public string COINSURANCE { get; set; }
            [Column("COMPANY_CLAIM_NO")] public string COMPANY_CLAIM_NO { get; set; }
            [Column("COMPANY_CLAIM_ID")] public string COMPANY_CLAIM_ID { get; set; }
            [Key] [Column("CLAIM_ID")] public long CLAIM_ID { get; set; }
            [Column("COMPANY_ID")] public long COMPANY_ID { get; set; }
            [Column("COVERAGE_ID")] public long COVERAGE_ID { get; set; }


        }
        [Table("V_SGM_POLICY")]
        public class V_SgmPolicy
        {
            [Column("BRANCH_ID")]
            public string BranchId { get; set; }

            [Column("BRANCH_NAME")]
            public string BranchName { get; set; }
            [Column("BRANSHKOD")]
            public string BranshCode { get; set; }

            [Column("COMPANY_ID")]
            public string CompanyId { get; set; }
            [Column("DOVIZCINSI")]
            public string DovizCinsi { get; set; }

            [Column("ENDIREKTSIGORTASIRKETKOD")]
            public string EndirektSigortaSirketKod { get; set; }
            [Column("ESKIPOLICENO")]
            public string EskiPoliceNo { get; set; }
            [Column("ESKIYENILEMENO")]
            public string EskiYenilemeNo { get; set; }


            [Column("ILKOD")]
            public string IlKod { get; set; }
            [Column("MAIN_BRANCH_ID")]
            public string MainBranchId { get; set; }
            [Column("MAIN_BRANCH_NAME")]
            public string MainBranchName { get; set; }

            [Column("ODEMEKOD")]
            public string OdemeKod { get; set; }
            [Column("OTORIZASYONKOD")]
            public string OtorizasyonKod { get; set; }
            [Column("POLICEBASLAMATARIHI")]
            public string PoliceBaslamaTarihi { get; set; }

            [Column("POLICEBITISTARIHI")]
            public string PoliceBitisTarihi { get; set; }

            [Column("POLICEBRUTPRIMI")]
            public string PoliceBrutPrimi { get; set; }
            [Column("POLICENETPRIMI")]
            public string PoliceNetPrimi { get; set; }
            [Column("POLICENO")]
            public string PoliceNo { get; set; }
            [Column("POLICETANZIMTARIHI")]
            public string PoliceTanzimTarihi { get; set; }
            [Column("POLICETIP")]
            public string PoliceTip { get; set; }
            [Key]
            [Column("POLICY_ID")]
            public string PoliceId { get; set; }
            [Column("SGT_AD")]
            public string SgtAd { get; set; }
            [Column("SGT_ADRES")]
            public string SgtAdres { get; set; }
            [Column("SGT_BABAADI")]
            public string SgtBabaAdi { get; set; }
            [Column("SGT_CINSIYET")]
            public string SgtCinsiyet { get; set; }
            [Column("SGT_DOGUMTARIHI")]
            public string SgtDogumTarihi { get; set; }
            [Column("SGT_DOGUMYERI")]
            public string SgtDogumYeri { get; set; }

            [Column("SGT_KIMLIKNO")]
            public string SgtKimlikNo { get; set; }
            [Column("SGT_TURKOD")]
            public string SgtTurKod { get; set; }

            [Column("SGT_KIMLIKTIPKOD")]
            public string SgtKimlikTipKod { get; set; }
            [Column("SGT_KURULUSTARIHI")]
            public string SgtKurulusTarihi { get; set; }

            [Column("SGT_KURULUSYERI")]
            public string SgtKurulusYeri { get; set; }

            [Column("SGT_SIGORTAETTIRENUYRUK")]
            public string SgtSigortaEttirenUyruk { get; set; }

            [Column("SGT_SOYAD")]
            public string SgtSoyad { get; set; }
            [Column("SGT_ULKEKODU")]
            public string SgtUlkeKodu { get; set; }
            [Column("SIGORTASIRKETKOD")]
            public string SigortaSirketKod { get; set; }
            [Column("TARIFEID")]
            public string TarifeId { get; set; }
            [Column("URETIMKAYNAKKOD")]
            public string UretimKaynakKod { get; set; }
            [Column("URETIMKAYNAKKURUMKOD")]
            public string UretimKaynakKurumKod { get; set; }
            [Column("VADESAYISI")]
            public string VadeSayisi { get; set; }
            [Column("YENILEMENO")]
            public string YenilemeNo { get; set; }
            [Column("YENIYENILEMEGECIS")]
            public string YeniYenilemeGecis { get; set; }
            [Column("ZEYLNO")]
            public string ZeylNo { get; set; }
        }

        [Table("V_INSURED_CLAIM_REASON")]
        public class V_InsuredClaimReason
        {
            [Key] [Column("CLAIM_REASON_ID")] public long? CLAIM_REASON_ID { get; set; }
            [Column("DESCRIPTION")] public string DESCRIPTION { get; set; }
            [Column("INSURED_CLAIM_REASON_ID")] public long? INSURED_CLAIM_REASON_ID { get; set; }
            [Column("INSURED_ID")] public long? INSURED_ID { get; set; }
            [Column("IP")] public string IP { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("SP_REQUEST_DATE")] public DateTime? SP_REQUEST_DATE { get; set; }
            [Column("SP_RESPONSE_DATE")] public DateTime? SP_RESPONSE_DATE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("TYPE")] public string TYPE { get; set; }
            [Column("USERNAME")] public string USERNAME { get; set; }
            [Column("USER_ID")] public long? USER_ID { get; set; }
        }
        [Table("sf_choose_plan_coverage_var")]
        public class SF_PlanCoverageVar
        {
            [Column("PLAN_COVERAGE_VARIATION_ID")] public string PLAN_COVERAGE_VARIATION_ID { get; set; }
            [Column("PLAN_COVERAGE_VARIATION_TYPE")] public string PLAN_COVERAGE_VARIATION_TYPE { get; set; }
            [Column("PLAN_COVERAGE_VARIATION_CODE")] public string PLAN_COVERAGE_VARIATION_CODE { get; set; }
            [Column("PLAN_COVERAGE_VARIATION_TEXT")] public string PLAN_COVERAGE_VARIATION_TEXT { get; set; }
            [Column("PLAN_COVERAGE_ID")] public string PLAN_COVERAGE_ID { get; set; }
            [Column("AGREEMENT_TYPE")] public string AGREEMENT_TYPE { get; set; }
            [Column("AGREEMENT_TYPE_CODE")] public string AGREEMENT_TYPE_CODE { get; set; }
            [Column("AGREEMENT_TYPE_TEXT")] public string AGREEMENT_TYPE_TEXT { get; set; }
            [Column("PRICE_LIMIT")] public string PRICE_LIMIT { get; set; }
            [Column("PRICE_FACTOR")] public string PRICE_FACTOR { get; set; }
            [Column("CURRENCY_TYPE")] public string CURRENCY_TYPE { get; set; }
            [Column("CURRENCY_TYPE_CODE")] public string CURRENCY_TYPE_CODE { get; set; }
            [Column("CURRENCY_TYPE_TEXT")] public string CURRENCY_TYPE_TEXT { get; set; }
            [Column("NUMBER_LIMIT")] public string NUMBER_LIMIT { get; set; }
            [Column("DAY_LIMIT")] public string DAY_LIMIT { get; set; }
            [Column("SESSION_COUNT")] public string SESSION_COUNT { get; set; }
            [Column("COINSURANCE_RATIO")] public string COINSURANCE_RATIO { get; set; }
            [Column("EXEMPTION_TYPE")] public string EXEMPTION_TYPE { get; set; }
            [Column("EXEMPTION_TYPE_CODE")] public string EXEMPTION_TYPE_CODE { get; set; }
            [Column("EXEMPTION_TYPE_TEXT")] public string EXEMPTION_TYPE_TEXT { get; set; }
            [Column("EXEMPTION_LIMIT")] public string EXEMPTION_LIMIT { get; set; }
            [Column("PACKAGE_ID")] public string PACKAGE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("STATUS_CODE")] public string STATUS_CODE { get; set; }
            [Column("STATUS_TEXT")] public string STATUS_TEXT { get; set; }
            [Column("NEW_VERSION_ID")] public string NEW_VERSION_ID { get; set; }
            [Column("SP_RESPONSE_ID")] public string SP_RESPONSE_ID { get; set; }
        }
        [Table("V_POLICY_CLAIM_REASON")]
        public class V_PolicyClaimReason
        {
            [Key] [Column("CLAIM_REASON_ID")] public long? CLAIM_REASON_ID { get; set; }
            [Column("DESCRIPTION")] public string DESCRIPTION { get; set; }
            [Column("INSURED_CLAIM_REASON_ID")] public long? INSURED_CLAIM_REASON_ID { get; set; }
            [Column("IP")] public string IP { get; set; }
            [Column("NEW_VERSION_ID")] public long? NEW_VERSION_ID { get; set; }
            [Column("POLICY_ID")] public long? POLICY_ID { get; set; }
            [Column("SP_REQUEST_DATE")] public DateTime? SP_REQUEST_DATE { get; set; }
            [Column("SP_RESPONSE_DATE")] public DateTime? SP_RESPONSE_DATE { get; set; }
            [Column("SP_RESPONSE_ID")] public long? SP_RESPONSE_ID { get; set; }
            [Column("STATUS")] public string STATUS { get; set; }
            [Column("TYPE")] public string TYPE { get; set; }
            [Column("USERNAME")] public string USERNAME { get; set; }
            [Column("USER_ID")] public long? USER_ID { get; set; }

        }
        [Table("V_PROVIDER_CLAIM_LIST")]
        public class V_ProviderClaimList
        {
            [Column("BANK_NAME")]
            public string BANK_NAME { get; set; }
            [Column("PROVIDER_NAME")]
            public string PROVIDER_NAME { get; set; }
            [Column("BILL_DATE")]
            public DateTime? BILL_DATE { get; set; }
            [Column("BILL_NO")]
            public string BILL_NO { get; set; }
            [Column("MEDULA_NO")]
            public string MEDULA_NO { get; set; }
            [Key]
            [Column("CLAIM_ID")]
            public Int64 CLAIM_ID { get; set; }
            [Column("CLAIM_STATUS")]
            public string CLAIM_STATUS { get; set; }
            [Column("COMPANY_ID")]
            public Int64? COMPANY_ID { get; set; }
            [Column("COMPANY_NAME")]
            public string COMPANY_NAME { get; set; }
            [Column("IBAN")]
            public string IBAN { get; set; }
            [Column("INSURED_NAME_SURNAME")]
            public string INSURED_NAME_SURNAME { get; set; }
            [Column("PAID")]
            public decimal? PAID { get; set; }
            [Column("PAYMENT_DATE")]
            public DateTime? PAYMENT_DATE { get; set; }
            [Column("PAYROLL_ID")]
            public Int64? PAYROLL_ID { get; set; }
            [Column("PROVIDER_GROUP_ID")]
            public Int64? PROVIDER_GROUP_ID { get; set; }
            [Column("PROVIDER_ID")]
            public Int64 PROVIDER_ID { get; set; }
        }
    }//end class
}//end namespace
