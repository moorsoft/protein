﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Entities
{
    public class CommonEntity
    {
        [Column("NEW_VERSION_ID")]
        public Int64? NewVersionId { get; set; }
        [Column("STATUS")]
        public string Status { get; set; }
    }
}
