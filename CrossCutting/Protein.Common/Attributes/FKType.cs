﻿using System;

namespace Protein.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class FKType : System.Attribute
    {
        private string name;
        public string relationType;

        public FKType(string name, string relationType)
        {
            this.name = name;
            this.relationType = relationType;
        }
    }
}
