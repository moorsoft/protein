﻿using System;

namespace Protein.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class EnumValue : System.Attribute
    {
        public string Code { get; set; }
        public string Text { get; set; }

        public EnumValue(string code, string text)
        {
            Code = code;
            Text = text;
        }
    }
}
