﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Resources
{
    public class SimpleCompany
    {
        public Int64 COMPANY_ID { get; set; }
        public string COMPANY_NAME { get; set; }
    }
}
