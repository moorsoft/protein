﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Resources
{
    public abstract class AbstractResource
    {
        public long? Id { get; set; }
        public int Limit { get; set; }
        public int Offset { get; set; }
        public string SearchValue { get; set; }
        public string SortColumnName { get; set; }
        public string SortDirection { get; set; }
    }
}
