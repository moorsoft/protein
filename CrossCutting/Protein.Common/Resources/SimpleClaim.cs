﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Resources
{
    public class SimpleClaim
    {
        public Int64 CLAIM_ID { get; set; }
        public string COVERAGE_NAME { get; set; }
    }
}
