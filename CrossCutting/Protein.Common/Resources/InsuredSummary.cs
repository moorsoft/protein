﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Resources
{
    public class InsuredSummary
    {
        public Int64? IDENTITY_NO { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string BIRTHDATE { get; set; }
        public string GENDER_NAME { get; set; }
        public string INDIVIDUAL_TYPE_TEXT { get; set; }
        public string PASSPORT_NO { get; set; }
        public Int64? TAX_NUMBER { get; set; }
        public string STATUS_TEXT { get; set; }
        public string FIRST_INSURED_DATE { get; set; }
        public string FIRST_AMBULANT_COVERAGE_DATE { get; set; }
        public string BIRTH_COVERAGE_DATE { get; set; }
        public string FIRST_COMPANY_INSURED_DATE { get; set; }
        public string IS_VIP { get; set; }
        public string VIP_TYPE { get; set; }
    }
}
