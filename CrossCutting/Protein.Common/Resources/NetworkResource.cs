﻿using System;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Common.Resources
{
    public class NetworkResource : AbstractResource
    {
        public string Name { get; set; }
        public NetworkType? Type { get; set; }
        public NetworkCategoryType? Category { get; set; }
    }
}
