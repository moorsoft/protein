﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Resources
{
    public class PolicySummary
    {
        public string COMPANY_NAME { get; set; }
        public Int64? POLICY_NUMBER { get; set; }
        public string POLICY_START_DATE { get; set; }
        public string POLICY_END_DATE { get; set; }
        public string POLICY_ISSUE_DATE { get; set; }
        public string STATUS_TEXT { get; set; }
        public string AGENCY_NO { get; set; }
        public string AGENCY_NAME { get; set; }
        public string INSURER_NAME { get; set; }
        public string POLICY_GROUP_NAME { get; set; }
        public string PRODUCT_NAME { get; set; }
        public string LAST_ENDORSEMENT_TYPE { get; set; }
        public string LAST_ENDORSEMENT_NO { get; set; }
        public string LAST_ENDORSEMENT_START_DATE { get; set; }
        public string LAST_ENDORSEMENT_DATE_OF_ISSUE { get; set; }
        
    }
}
