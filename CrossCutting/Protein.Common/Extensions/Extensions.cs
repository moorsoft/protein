﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Protein.Common.Extensions
{
    public static class Extensions
    {
        #region String extensions

        public static int GetWordCount(this string value)
        {
            return value.Split(new char[] { ' ', '.', '?' }, StringSplitOptions.RemoveEmptyEntries).Length;
        }

        public static string UppercaseFirstLetter(this string value)
        {
            if (value.Length > 0)
            {
                char[] array = value.ToCharArray();
                array[0] = char.ToUpper(array[0]);
                for (int i = 1; i < array.Length; i++)
                {
                    array[i] = char.ToLower(array[i]);
                }
                return new string(array);
            }
            return value;
        }

        public static string ToTitleCase(this string value)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            value = textInfo.ToTitleCase(value);
            return value;
        }

        public static string ReplaceTurkishCharacters(this string value)
        {
            value = value.Replace("ğ", "g")
                         .Replace("Ğ", "G")
                         .Replace("ş", "s")
                         .Replace("Ş", "S")
                         .Replace("ı", "i")
                         .Replace("İ", "I")
                         .Replace("ü", "u")
                         .Replace("Ü", "U")
                         .Replace("ö", "o")
                         .Replace("Ö", "O")
                         .Replace("ç", "c")
                         .Replace("Ç", "C");

            return value;
        }

        /// <summary>
        /// Fast replace function
        /// </summary>
        /// <param name="original"></param>
        /// <param name="pattern">text to find</param>
        /// <param name="replacement">replace text</param>
        /// <param name="comparisonType">optional, Comparision Type</param>
        /// <returns></returns>
        public static string ReplaceFast(this string original, string pattern, string replacement, StringComparison comparisonType = StringComparison.Ordinal)
        {
            if (original == null)
            {
                return null;
            }

            if (String.IsNullOrEmpty(pattern))
            {
                return original;
            }

            int lenPattern = pattern.Length;
            int idxPattern = -1;
            int idxLast = 0;

            StringBuilder result = new StringBuilder();

            while (true)
            {
                idxPattern = original.IndexOf(pattern, idxPattern + 1, comparisonType);

                if (idxPattern < 0)
                {
                    result.Append(original, idxLast, original.Length - idxLast);

                    break;
                }

                result.Append(original, idxLast, idxPattern - idxLast);
                result.Append(replacement);

                idxLast = idxPattern + lenPattern;
            }

            return result.ToString();
        }

        public static string ToUrlSlug(this string value)
        {
            //First to lower case
            value = value.ToLowerInvariant();

            //Remove all accents
            var bytes = Encoding.GetEncoding("Cyrillic").GetBytes(value);
            value = Encoding.ASCII.GetString(bytes);

            //Replace spaces
            value = Regex.Replace(value, @"\s", "-", RegexOptions.Compiled);

            //Remove invalid chars
            value = Regex.Replace(value, @"[^a-z0-9\s-_]", "", RegexOptions.Compiled);

            //Trim dashes from end
            value = value.Trim('-', '_');

            //Replace double occurences of - or _
            value = Regex.Replace(value, @"([-_]){2,}", "$1", RegexOptions.Compiled);

            return value;
        }

        public static string CleanInvalidXmlChars(this string value)
        {
            string re = @"[^\x09\x0A\x0D\x20-\xD7FF\xE000-\xFFFD\x10000-x10FFFF]";
            return Regex.Replace(value, re, "");
        }

        public static string RemoveRepeatedWhiteSpace(this string value)
        {
            if (value.IsNull()) return string.Empty;
            
            string[] tmp = value.ToString().Split(new String[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            StringBuilder sb = new StringBuilder();
            foreach (string word in tmp)
            {
                sb.Append(word.Replace("\r", "").Replace("\n", "").Replace("\t", "") + " ");
            }

            return sb.ToString().TrimEnd();
        }


        #endregion

        #region Datetime extensions

        public static string GetMonthName(this DateTime date, string cultureInfoName)
        {
            string monthName = date.ToString("MMMM", CultureInfo.CreateSpecificCulture(cultureInfoName));
            return monthName;
        }

        public static string GetDayName(this DayOfWeek dayOfWeek, string cultureInfoName)
        {
            CultureInfo culture = new CultureInfo(cultureInfoName);
            string dayName = culture.DateTimeFormat.GetDayName(dayOfWeek);
            return dayName;
        }

        public static int GetQuarter(this DateTime date)
        {
            int[] quarters = new int[] { 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4 };
            return quarters[date.Month - 1];
        }

        public static bool isDate(string value)
        {
            try
            {
                DateTime.Parse(value);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        #endregion

        #region Object extensions

        public static string ToJSON<T>(this T value, bool apostropheCheck = false)
        {
            if (value == null) return string.Empty;

            string json = JsonConvert.SerializeObject(value, Newtonsoft.Json.Formatting.None, new JsonSerializerSettings
            { 
                MaxDepth = Int32.MaxValue,
                TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple
            });

            json = apostropheCheck ? json.Replace("'", "\\'").Replace("\"", "\\\"") : json;
            json = json.RemoveRepeatedWhiteSpace();
            json = json.Replace(Environment.NewLine, string.Empty);
            return json;
        }

        public static string ToXML<T>(this T value, bool isCDataValue = false)
        {
            if (value == null) return string.Empty;

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));

            using (StringWriter stringWriter = new StringWriter())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(stringWriter, new XmlWriterSettings { Indent = true }))
                {
                    xmlSerializer.Serialize(xmlWriter, value);
                    return isCDataValue ? $"<![CDATA[{stringWriter.ToString().Replace("&amp;", " ")}]]>" : stringWriter.ToString().Replace("&amp;", " ");
                }
            }
        }

        public static Guid ToGuid<T>(this T value)
        {
            if (value == null) return Guid.Empty;

            byte[] stringbytes = Encoding.UTF8.GetBytes(Convert.ToString(value));
            byte[] hashedBytes = new System.Security.Cryptography
                .SHA1CryptoServiceProvider()
                .ComputeHash(stringbytes);
            Array.Resize(ref hashedBytes, 16);
            return new Guid(hashedBytes);
        }

        public static string ToMD5<T>(this T value)
        {
            if (value == null) return string.Empty;

            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(value.ToString()));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }

            return hash.ToString().ToUpper();
        }
        public static string[] StringToArray(this List<string> value)
        {
            if (value == null) { return new string[] {  }; }
            string[] returnArray = new string[value.Count];

            int i = 0;
            foreach (var item in value)
            {
                returnArray[i] = item;
                i++;
            }

            return returnArray;
        }


        public static bool IsNull(this string value)
        {
            if (value == null)
                return true;
            return String.IsNullOrEmpty(value.Trim());
        }
        public static bool IsNumeric(this string value)
        {
            if (String.IsNullOrEmpty(value)) { return false; }

            decimal convert;
            return decimal.TryParse(value, out convert);
        }
        public static bool IsDateTime(this string value)
        {
            if (String.IsNullOrEmpty(value)) return false;

            DateTime tmp;
            return DateTime.TryParse(value, out tmp);
        }
        public static bool IsInt(this string value)
        {
            if (String.IsNullOrEmpty(value)) return false;

            int tmp;
            return int.TryParse(value, out tmp);
        }
        public static bool IsInt64(this string value)
        {
            if (String.IsNullOrEmpty(value)) return false;

            Int64 tmp;
            return Int64.TryParse(value, out tmp);
        }


        public static Int64? IsIdentityNoLength(this Int64? value)
        {
            if (value == null) return null;
            if (value > 99999999999 && value < 10000000000) return null;
            
            return value;
        }
        public static Int64? IsTaxNumberLength(this Int64? value)
        {
            if (value == null) return null;
            if (value.ToString().Length==9 || value.ToString().Length == 10) return value;
            
            return null;
        }
        #endregion
    }

}
