﻿using Protein.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Enums
{
    public enum CompanyEnum
    {
        [EnumValue("11", "11")]
        AMAN_TAFAKUL = 11,
        [EnumValue("5", "5")]
        DOGA = 5,
        [EnumValue("55", "55")]
        DOGA_TEST = 55,
        [EnumValue("6", "6")]
        ETHICA = 6,
        [EnumValue("66", "66")]
        ETHICA_TEST = 66,
        [EnumValue("98", "98")]
        GUNES_TEST = 98,
        [EnumValue("2", "2")]
        GUNES = 2,
        [EnumValue("1", "1")]
        HALK = 1,
        [EnumValue("10", "10")]
        HDI = 10,
        [EnumValue("95", "95")]
        HDI_TEST = 95,
        [EnumValue("8", "8")]
        KE_TEST = 8,
        [EnumValue("88", "88")]
        KATILIM_EMEKLILIK = 88,
        [EnumValue("96", "96")]
        MOOR = 96,
        [EnumValue("320", "320")]
        NEOVA = 320,
        [EnumValue("321", "321")]
        NEOVA_TEST = 321,
        [EnumValue("313", "313")]
        NIPPON_TEST = 313,
        [EnumValue("323", "323")]
        NN_TEST = 323,
        [EnumValue("322", "322")]
        NN = 322,
        [EnumValue("99", "99")]
        TEST_SIGORTA = 99,
        [EnumValue("3", "3")]
        TURK_NIPPON = 3,
        [EnumValue("4", "4")]
        UNICO = 4,
        [EnumValue("44", "44")]
        UNICO_TEST = 44,
        [EnumValue("9", "9")]
        VAKIF = 9,
        [EnumValue("97", "97")]
        VAKIF_EMEKLILIK = 97,
        [EnumValue("0", "0")]
        IMECE_DESTEK = 0
    }
}
