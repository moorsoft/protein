﻿using Protein.Common.Attributes;

namespace Protein.Common.Enums
{
    public class ProteinEnums
    {
        public enum AddressType
        {
            IS = 0,
            EV = 1
        }
        public enum AgencyStatus
        {
            AKTIF = 0,
            SILINDI = 1,
            FESIH = 2
        }
        public enum AgencyType
        {
            ACENTE = 1,
            BANKA = 2,
            BROKER = 3,
            DIREKT = 4,
            ÇAGRI_MERKEZI = 5,
            WEB = 6,
            ATM = 7,
            SATIS_OFISI = 8,
            DİGER = 9,
            MERKEZ = 10,
            KURUM = 11
        }
        public enum AgreementType
        {

            YILLIK = 0,
            ADETLIK = 1,
            FATURA_BASINA = 2,
            GUNLUK = 3,
            HAVUZ_GRUP = 4,
            HAVUZ_HOLDING = 5,
            RECETE_BASINA = 6,
            SEANS_BASINA = 7,
            VIZITE_BASINA = 8
        }

        public enum WsAgreementType
        {
            
            TazminatMutabakat = 1,
            TazminatMutabakatDetay = 2,
            IcmalMutabakat = 3,
            IcmalMutabakatDetay = 4,
            PoliceMutabakat = 5,
            PoliceDetayMutabakat = 6,
            ZeyilMutabakat = 7,
            ZeyilDetayMutabakat = 8
        }
        public enum WsAgreementStatus
        {

            BASARILI = 0,
            BASARISIZ= 1
           
        }
        public enum BillStatus
        {
            ODEME_BEKLER = 0,
            SILINDI = 1,
            ODENDI = 2,
            RET = 3
        }
        public enum BoolType
        {
            FALSE = 0,
            TRUE = 1
        }
        public enum CashPaymentType
        {
            [EnumValue("0", "0")]
            YUZDE = 0,
            [EnumValue("1", "1")]
            TUTAR = 1
        }
        public enum CityFactorGroupStatus
        {
            AKTIF = 0,
            SILINDI = 1,
            SURESI_DOLMUS = 2

        }
        public enum ClaimCoverageStatusType
        {
            ON_ONAY = 4,
            ONAY = 5,
            RET = 6,
            IPTAL = 7
        }
        public enum ClaimMissingDocType
        {
            ADLI_VAKA_RAPORU = 1,
            ALKOL_TESTI_SONUCU = 2,
            AMBULANS_GOZLEM_FORMU = 3,
            DETAYLI_FATURA_DOKUMU = 4,
            DOGUM_EPIKRIZI = 6,
            DOGUM_RAPORU = 7,
            DOKTOR_ISTEM_BELGESI = 8,
            DOKTOR_KASE_IMZA = 9,
            ECZANE_FISI_KASA_FISI = 10,
            EPIKRIZI = 11,
            FATURA_ASLI = 12,
            FIZIK_TEDAVI_BITIS_EPIKRIZI = 13,
            FIZIK_TEDAVI_HIZMET_DOKUMU = 14,
            GECIS_SIGORTA_SIRKET_PROFILI = 15,
            GORUNUTLEME_SONUCLARI = 16,
            GOZLUK_GARANTI_BELGESI = 17,
            GOZLUK_RECETESI = 18,
            HEMSIRE_GOZLEM_FORMLARI = 19,
            HESAP_BILGISI = 20,
            HIZMETIN_DETAY_DOKUMU = 21,
            ILAC_KUPURLERI = 22,
            ILAC_RAPORU = 23,
            ILETISIM_BILGILERI_ISLAK_IMZA_KARTI = 24,
            KAZA_ZAPTI_TUTANAGI = 25,
            KIMLIK_FOTOKOPISI = 26,
            MALZEMENIN_UBB_KODU = 27,
            MALZEMEYE_AIT_PROFORMA_FATURA = 28,
            OLAYIN_GERCEKLESMESINE_ILISKIN_SIGORTALI_BEYANI = 29,
            OZGECMIS_BILDIRIR_DR_RAPORU = 30,
            PATALOJI_RAPORU = 31,
            RECETE = 32,
            SEVK_BELGESI = 33,
            SIGORTALI_ILETISIM_BILGILERI = 34,
            SIGORTALI_VEYA_BIRINCI_DERECEDEN_YAKININ_IMZASI = 35,
            TAHLIL_TEKKIK_SONUCLARI = 36,
            TANI_SIKAYET_SIKAYET_BASLANGIC_TARIHI_BILDIRIR_DETAYLI_DR_RAPORU = 37,
            TAZMINAT_TALEP_FORMU = 38,
            TEDAVI_PLANI = 39,
            TUM_KURUM_KAYITLARI = 40,
            TUM_POLIKLINIK_KAYITLARI = 41,
            YAPILACAK_TEDAVININ_SUT_KODU = 42,
            YAPILACAK_TEDAVININ_TTB_BIRIMI = 43,
            YENIDOGAN_BEBEK_EPIKRIZI = 44,
            KAYIT_ARASTIRMA_SONUCUNDA_SAPTANAN_BELGE_VE_BILGILER = 45

        }
        public enum ClaimNoteType
        {
            EPIKRIZ = 0,
            TEDAVI = 1,
            NORMAL = 2,
            TANI = 3
        }
        public enum ClaimPaymentMethod
        {
            KURUM = 0,
            SIGORTALI = 1
        }
        public enum ClaimReasonType
        {
            HASARA_KAPALI = 1,
            HASARA_ACIK = 0
        }
        public enum ClaimSourceType
        {
            PROVIZYON_MERKEZI = 0,
            HASTANE = 1,
            ECZANE = 2,
            PORTTSS = 3

        }
        public enum ClaimStatus
        {
            GIRIS = 0,
            SILINDI = 1,
            PROZIYON_ONAY = 2,
            PROVIZYON_BEKLEME = 3,
            RET = 4,
            ODENECEK = 5,
            IPTAL = 6,
            ODENDI = 7,
            TAZMİNAT_BEKLEME = 8,
        }
        public enum ClaimType
        {
            SEY = 2,
            OSS = 1,
            TSS = 0
        }
        public enum ClaimVariationType
        {
            SEVK = 1,
            ACIL = 0
        }
        public enum ContactNoteType
        {
            BILGI = 0,
            TAZMINAT = 1,
            BEYAN = 2,
            MEDDIKAL = 3

        }
        public enum ContactType
        {
            [EnumValue("0", "GERCEK")]
            GERCEK = 0,
            [EnumValue("1", "TUZEL")]
            TUZEL = 1
        }
        public enum ContractStatus
        {
            TEKLIF_ASAMASINDA = 0,
            SILINDI = 1,
            AKTIF = 2,
            RET = 3,
            FESIH = 4
        }
        public enum ContractType
        {
            TSS = 0,
            OSS = 1,
            DOGUM = 2,
            MDP = 3

        }
        public enum CorporateType
        {
            SAHIS = 0,
            LIMITED = 1,
            ANONIM = 2
        }
        public enum CountyType
        {
            MERNIS = 0,
            SBM = 1
        }
        public enum CoverageType
        {
            YATARAK = 1,
            AYAKTA = 2,
            DIGER = 3,
            SEYAHAT = 4
        }
        public enum CurrencyType
        {
            [EnumValue("1", "TRY")]
            TURK_LIRASI = 1,
            [EnumValue("2", "USD")]
            ABD_DOLARI = 2,
            [EnumValue("3", "AUD")]
            AVUSTRALYA_DOLARI = 3,
            [EnumValue("4", "DKK")]
            DANIMARKA_KRONU = 4,
            [EnumValue("5", "EUR")]
            EURO = 5,
            [EnumValue("6", "GBP")]
            INGLIZI_STERLINI = 6,
            [EnumValue("7", "CHF")]
            ISVICRE_FRANGI = 7,
            [EnumValue("8", "SEK")]
            ISVEC_KRONU = 8,
            [EnumValue("9", "CAD")]
            KANADA_DOLARI = 9,
            [EnumValue("10", "KWD")]
            KUVEYT_DINARI = 10,
            [EnumValue("11", "NOK")]
            NORVEC_KRONU = 11,
            [EnumValue("12", "SAR")]
            SUUDI_ARABISTAN_RIYALI = 12,
            [EnumValue("13", "JPY")]
            JAPON_YENI = 13,
            [EnumValue("14", "CN")]
            CIN_YUANI = 14
        }
        public enum EmailType
        {
            IS = 0,
            SAHSI = 1
        }
        public enum CompanyEmailType
        {
            IS = 0,
            SAHSI = 1,
            PROVIZYON_TSS = 2,
            PROVIZYON_OSS = 3
        }
        public enum EndorsementTypeDescriptionType
        {
            [EnumValue("0", "BSL")]
            POLICE_BASLANGICINDAN = 0,
            [EnumValue("1", "GUN")]
            GUN_ESASLI = 1
        }
        public enum EndorsementCategory
        {
            [EnumValue("0", "POL")]
            POLICE = 0,
            [EnumValue("1", "SGR")]
            SIGORTALI = 1
        }
        public enum EndorsementType
        {
            [EnumValue("1", "B")]
            BASLANGIC = 1,
            [EnumValue("2", "I")]
            IPTAL = 2,
            [EnumValue("3", "SG")]
            SIGORTALI_GIRISI = 3,
            [EnumValue("4", "SC")]
            SIGORTALI_CIKISI = 4,
            [EnumValue("5", "TPD")]
            TEMINAT_PLAN_DEGISIKLIGI = 5,
            [EnumValue("6", "UKD")]
            URETIM_KAYNAGI_DEGISIKLIGI = 6,
            [EnumValue("7", "SBD")]
            SIGORTALI_BILGI_DEGISIKLIGI = 7,
            [EnumValue("8", "SED")]
            SE_DEGISIKLIGI = 8,
            [EnumValue("9", "GUD")]
            GIDECEGI_ULKE_DEGISIKLIGI = 9,
            [EnumValue("10", "PFZ")]
            PRIM_FARKI_ZEYLI = 10,
            [EnumValue("11", "KDEI")]
            KISA_DONEM_ESASLI_IPTAL = 11,
            [EnumValue("12", "GEI")]
            GUN_ESASLI_IPTAL = 12,
            [EnumValue("13", "PII")]
            PRIM_IADESIZ_IPTAL = 13,
            [EnumValue("14", "DSI")]
            DONDURMA_SONUCU_IPTAL = 14,
            [EnumValue("15", "MBI")]
            MEBDEINDEN_BASLANGICTAN_IPTAL = 15,
            [EnumValue("16", "MDZ")]
            MERIYETE_DONUS_ZEYLI = 16,
            [EnumValue("17", "BBTD")]
            BASLANGIC_BITIS_TARIHI = 17,
            [EnumValue("18", "PTZ")]
            POLICE_TAHAKKUK_ZEYLI = 18,
            [EnumValue("19", "ST")]
            SIGORTALI_TAHAKKUK = 19,
            [EnumValue("20", "TZ")]
            TEMINAT_ZEYLI = 20

        }
        public enum ErrorType
        {

            VERITABANI_HATASI_BOS_GECILEMEYEN_ALAN = 0,
            VERITABANI_MAKSIMUM_KARAKTER_SAYISI_ASILDI = 1,
            VERITABANI_HATASI_TANIMSIZ_KOLON = 2,
            VERITABANI_HATASI_SYNTAX_ERROR = 4
        }
        public enum EventType
        {
            TRAFIK_KAZASI = 1,
            IS_KAZASI = 2
        }
        public enum ExemptionType
        {
            ADETLIK = 0,
            GUNLUK = 1,
            YILLIK = 2,
            SEANS = 3
        }
        public enum Gender
        {
            [EnumValue("0", "K")]
            KADIN = 0,
            [EnumValue("1", "E")]
            ERKEK = 1

        }
        public enum ICDType
        {
            ON = 1,
            KESIN = 2
        }
        public enum IndividualType
        {
            [EnumValue("1", "F")]
            FERT = 1,
            [EnumValue("2", "E")]
            ES = 2,
            [EnumValue("3", "C")]
            COCUK = 3,
            [EnumValue("4", "A")]
            ANNESI = 4,
            [EnumValue("5", "B")]
            BABASI = 5,
            [EnumValue("6", "D")]
            DIGER = 6

        }
        public enum InsuredNoteType
        {
            OZEL = 0,
            BASIM = 1,
            BEYAN = 2,
            GECİS = 3
        }
        public enum InsuredTransferType
        {
            [EnumValue("0", "0")]
            FERDIDEN_GRUBA = 0,
            [EnumValue("1", "1")]
            GRUPTAN_FERDIYE = 1,
            [EnumValue("2", "2")]
            SIGORTA_SIRKETI = 2
        }
        public enum IntegrationStatus
        {
            [EnumValue("0", "Gönderilmedi")]
            GONDERILMEDI = 0,

            [EnumValue("1", "KontrolServisOk")]
            KONTROL_SERVIS_OK = 1,
            [EnumValue("2", "KontrolServisHata")]
            KONTROL_SERVIS_HATA = 2,

            [EnumValue("3", "CanliServisOk")]
            CANLI_SERVIS_OK = 3,
            [EnumValue("4", "CanliServisHata")]
            CANLI_SERVIS_HATA = 4,

            [EnumValue("5", "Aktarildi")]
            AKTARILDI = 5,

            [EnumValue("6", "AktarimHatasi")]
            AKTARIM_HATASI = 6
        }
        public enum IntervalType
        {

            YAS_ARALIGI = 1,
            YIL_ARALIGI = 2
        }
        public enum LocationType
        {
            [EnumValue("1", "1")]
            YURTICI = 1,
            [EnumValue("2", "2")]
            YURTDISI_TUM_DUNYA = 2,
            [EnumValue("3", "3")]
            ABD_KANADA_HARIC_TUM_DUNYA = 3,
            [EnumValue("4", "4")]
            TUM_AVRUPA_ULKELERI = 4,
            [EnumValue("5", "5")]
            ABD_VE_KANADA = 5,
            [EnumValue("6", "6")]
            SCHENGEN_ULKELERI = 6,
            [EnumValue("7", "7")]
            ABD_KANADA_JAPONYA_HARIC_TUM_DUNYA = 7

        }
        //public enum LogType
        //{
        //    INCOMING = 0,
        //    WAITING = 1,
        //    COMPLETED = 2,
        //    FAILED = 3

        //}
        public enum MaritalStatus
        {
            EVLI = 0,
            BEKAR = 1
        }
        public enum NetworkCategoryType
        {
            [EnumValue("TSS", "TSS")]
            TSS = 0,
            [EnumValue("OSS", "ÖSS")]
            OSS = 1
        }
        public enum NetworkStatus
        {
            AKTIF = 0,
            SILINDI = 1,
            PASIF = 2
        }
        public enum NetworkType
        {
            [EnumValue("1", "TÜM NETWORK")]
            ALL = 1,
            [EnumValue("2", "EKONOMİK NETWORK")]
            ECONOMIC = 2,
            [EnumValue("3", "KISITLI NETWORK")]
            LIMITED = 3
        }
        public enum NoteType
        {
            OZEL = 0,
            BASIM = 1
        }
        public enum PackageStatus
        {
            AKTIF = 0,
            SILINDI = 1,
            SURESI_DOLMUS = 2,
        }
        public enum PackageTpaPriceType
        {
            POLICE_TANZIM_TARIHINE_GORE = 0,
            ZEYL_TANZIM_TARIHINE_GORE = 1
        }
        public enum PackageType
        {
            TSS = 0,
            OSS = 1
        }
        public enum PaymentType
        {
            NAKIT = 1,
            KREDI_KARTI = 2,
            HAVALE = 3,
            EFT = 4,
            CEK = 5,
            SENET = 6,
            HESAPTAN_CEKIM = 7,
            OTOMATIK_ODEME = 8,
            BANKAYA_ODEME = 9,
            DIGER = 10

        }
        public enum PayrollCategory
        {
            TSS = 0,
            OSS = 1
        }
        public enum PayrollStatus
        {
            Onay_Bekler = 0,
            Onay = 1,
            Odendi = 2
        }
        public enum PayrollType
        {
            KURUM = 0,
            SIGORTALI = 1
        }
        public enum PhoneType
        {
            IS = 0,
            MOBIL = 1,
            FAX = 2

        }
        public enum CompanyPhoneType
        {
            IS = 0,
            MOBIL = 1,
            FAX = 2,
            PROVIZYON = 3
        }

        public enum PlanCoverageVariationType
        {
            ANLASMALI_KURUM = 1,
            ANLASMASIZ_KURUM = 2,
            ACIL = 3,
            AILE_HEKIMLIGI = 4,
            VARYASYONSUZ = 5
        }
        public enum PlanStatus
        {
            AKTIF = 0,
            SILINDI = 1,
            PASIF = 2
        }
        public enum PolicyStatus
        {
            [EnumValue("0", "0")]
            TEKLIF = 0,
            [EnumValue("1", "1")]
            SILINDI = 1,
            [EnumValue("2", "2")]
            TANZIMLI = 2,
            [EnumValue("3", "3")]
            IPTAL = 3,
            [EnumValue("4", "4")]
            RET = 4,
            [EnumValue("5", "5")]
            SURESI_DOLMUS = 5,
        }
        public enum PolicyType
        {
            [EnumValue("0", "F")]
            FERDI = 0,
            [EnumValue("1", "G")]
            GRUP = 1
        }
        public enum PriceType
        {

            TL = 0,
            BIRIM = 1,
            HICBIRI = 2
        }
        public enum PricingType
        {
            RAYIC = 0,
            KUPUR = 1,
            BIRIM = 2,
            TTB = 3,
            INDIRIM_ORANI = 4,
            OP_DR_UCRETI = 5,
            SUT = 6,
        }
        public enum ProcessListStatus
        {
            AKTIF = 0,
            SILINDI = 1,
            PASIF = 2
        }
        public enum ProcessListType
        {
            TANI = 0,
            ILAC = 1,
            SUT = 2,
            TTB = 3,
            CARI = 4,
            TDB = 5
        }
        public enum ProductStatus
        {
            AKTIF = 0,
            SILINDI = 1,
            PASIF = 2,

        }
        public enum ProductType
        {
            TSS = 0,
            OSS = 1,
            YBU = 2,
            MDP = 3,
            AVS = 4,
            SEY = 5
        }
        public enum ProfessionType
        {

            TUCCAR_ESNAF_ZANAATKAR = 1,
            CIFTCI_BALIKCI = 2,
            ISCI = 3,
            VETERINER_HEKIM_ = 4,
            HEMSIRE_EBE = 5,
            ECZACI = 6,
            MIMAR_MUHENDIS_MUTEAHHIT = 7,
            AVUKAT_NOTER = 8,
            HAKIM_SAVCI = 9,
            YAZAR_SAIR_SANATCI = 10,
            SPORCU_ANTRENOR = 11,
            AKADEMISYEN_EGITIMCI_OGRETMEN = 12,
            PAZARLAMA_SATIS_HALKLA_ILISKILER_REKLAM = 13,
            BANKA_SIGORTA_FINANS = 14,
            MALI_MUBASIR_MUHASEBE = 15,
            MEDYA_ILETISIM = 16,
            EV_HANIMI = 17,
            KAMU_CALISANI_MEMUR_BUROKRAT = 18,
            ORDU_EMNIYET_MENSUBU = 19,
            ASCI_GARSON_BARMEN_KOMI = 20,
            SOFOR = 21,
            YAZILIMCI_ANALIST = 22,
            IS_ADAMI = 23,
            BURO_OFIS_ELEMANI = 24,
            LABORANT_BIYOLOG_RADYOLOG = 25,
            EMEKLI = 26,
            DANISMAN_DENETCI = 27,
            DIGER = 99
        }
        public enum ProviderNoteType
        {

            GENEL = 0,
            TSS_WEB = 1,
            OSS_WEB = 2
        }
        public enum ProviderType
        {

            HASTANE = 1,
            ECZANE = 2,
            DOKTOR = 3,
            GORUNTULEME_MERKEZI = 4,
            LABORATUAR = 5,
            FIZIK_TEDAVI_MERKEZI = 6,
            DIS_HEKIMI = 7,
            AYAKTA_TEDAVI_MERKEZI = 8,
            DIS_MERKEZI = 9,
            OPTIK = 10,
            AILE_HEKIMLIGI_MERKEZI = 11,
            KLINIK = 12,
            AMBULANS = 13,
            RADYOLOJI = 14,
            EVDE_BAKIM = 15,
            MEDIKAL_MALZEME = 16,
            MUAYENEHANE = 17,
            POLIKLINIK = 18,
            TANI_MERKEZI = 19,
            ASISTANS = 20,
            DIYALIZ_MERKEZI = 21,
            DIGER = 22
        }
        public enum RenewalGuaranteeType
        {

            YENILEME_GARANTISI_OMUR_BOYU = 1,
            YENILEME_GARANTISI_YAS_KISITLI = 2,
            YENILEME_GARANTISI_SARTLI = 3,
            MUAFIYET_HASTALIK = 4,
            MUAFIYET_HASTALIK_TUTAR = 5,
            HASTANE_TEDAVI_TEMINATLARININ_OMUR_BOYU_YENILEME_GARANTISI = 6,
            RISK_DEGERLENDIRMESI_YAPILABILIR_SEKILDE_OMUR_BOYU_YENILEME_GARANTISI = 7
        }
        public enum RuleCategoryResultType
        {
            ODENMEZ = 0,
            KOASURANS_ORAN = 1,
            TUTAR = 2,
            ADETLIK = 3,
            INCELEME = 4,
            HASARA_KAPAMA = 5,
            ISLEM = 6,
            TEMINAT = 7,
            BES_YIL_FAZLASINA_YG_ODENIR = 8,
            UC_YIL_FAZLASINA_YG_ODENIR = 9,
            SIRKETTE_BES_YIL_USTU_ODENIR = 10,
            SIRKETTE_UC_YIL_USTU_ODENIR = 11,
            ONBES_YAS_ALTI_ODENMEZ = 12,
            BEKLEME_SURESI = 13
        }
        public enum RuleCategoryType
        {

            KATILIM_ORAN = 0,
            TEMINAT_LIMIT = 1,
            MUAFIYET_LIMIT = 2,
            TEMINAT_ADET = 3,
            KURUM_HASAR_KAPAMA = 4,
            SIGORTALI_HASAR_KAPAMA = 5,
            TEMINAT_ODENMEZ = 6,
            ISLEM_ODENMEZ = 7,
            HASTALIK_ODENMEZ = 8,
            ILAC_ODENMEZ = 9,
            ISLEMI_ISLEME_YONLENDIRME = 10,
            ISLEMI_TEMINATA_YONLENDIRME = 11
        }
        public enum RuleType
        {
            [EnumValue("0", "0")]
            OZEL_SART = 0,
            [EnumValue("1", "1")]
            EK_PROTOKOL = 1,
            [EnumValue("2", "2")]
            ISTISNA = 2
        }
        public enum SelectionType
        {
            PAKET = 1,
            SECIMLI = 2
        }
        public enum SagmerStatus
        {
            BASARILI = 0,
            BASARISIZ = 1,
            GONDERILMEDI=2  
        }
        public enum StaffType
        {
            DOKTOR = 0,
            YETKILI = 1
        }
        public enum Status
        {
            AKTIF = 0,
            SILINDI = 1,
            PASIF = 2,
            IPTAL = 3
        }
        public enum SubproductType
        {
            FERDI = 0,
            GRUP = 1
        }
        public enum SubproductStatus
        {
            AKTIF = 0,
            SILINDI = 1,
            PASIF = 2

        }
        public enum SystemType
        {
            SBM = 1,
            SAGMER = 2,
            PORTTSS = 3,
            BT = 4,
            PUSULA = 5,
            HDI = 6,
            VAKIF = 7,
            KATILIM_TEST = 9,
            KATILIM = 8,
            WEBID = 10,
            WS = 11
        }
        public enum UnitPrivilegeType
        {
            SADECE_KENDININ = 0,
            SADECE_BUUNDUGU_BIRIMIN = 1,
            BULUNDUGU_BIRIM_VE_ALTINDAKI_BIRIMLERIN = 2,
            BULUNDU_BITIM_VE_KOMSU_BIRIMLERIM = 3,
            TUM_BIRIMLERIN = 4,
            BIRIMDEN_BAGIMSIZ = 5
        }
        public enum WsQueueType
        {
            DOGA_TEK_POL = 1,
            DOGA_HASAR_AKTAR = 2,
            DOGA_ICMAL_AKTAR = 3,
            KATILIM_HASAR_AKTAR = 4,
            KATILIM_HASAR_ONAY = 5,
            KATILIM_HASAR_RED = 6,
            KATILIM_IPTAL = 7,
            KATILIM_POLICE_AKTAR_CEVAP = 8,
            KATILIM_KURUM_TRANSFER = 9,
            KATILIM_HAK_SAHIP_SORGU = 10,
        }

    }
}