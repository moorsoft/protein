﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Protein.Common.Messaging
{
    public class MailSender : IMessaging
    {
        private string SmtpServer, SmtpPort, SmtpUserName, SmtpPassword, SmtpFromMail;

        //public string RuleCode { get; set; }
        //public EmailFrom From { get; set; }
        //public EmailTemplateType Template { get; set; }
        //public string Subject { get; set; }
        //public string Content { get; set; }
        //public MailPriority MailPriority { get;set;}
        //public MailAddress[] TO { get; set; }
        //public MailAddress[] CC { get; set; }
        //public MailAddress[] BCC { get; set; }
        //public bool EnableSsl { get; set; }

        public MailSender()
        {
            SmtpServer = ConfigurationManager.AppSettings["Smtp_Server"];
            SmtpPort = ConfigurationManager.AppSettings["Smtp_Port"];
            SmtpUserName = ConfigurationManager.AppSettings["Smtp_User"];
            SmtpPassword = ConfigurationManager.AppSettings["Smtp_Password"];

            if (string.IsNullOrEmpty(SmtpPassword) || string.IsNullOrEmpty(SmtpServer) || string.IsNullOrEmpty(SmtpPort) || string.IsNullOrEmpty(SmtpUserName))
            {
                var currPath = Environment.CurrentDirectory + "\\app.config";
                var configMap = new ExeConfigurationFileMap { ExeConfigFilename = currPath };
                var configuration = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
                var settings = configuration.AppSettings.Settings;

                SmtpServer = settings["Smtp_Server"].Value;
                SmtpPort = settings["Smtp_Port"].Value;
                SmtpUserName = settings["Smtp_User"].Value;
                SmtpPassword = settings["Smtp_Password"].Value;
            }
        }
        //public bool Send(string ruleCode, EmailFrom from, EmailTemplateType template, string subject, string content, MailPriority priority, MailAddress[] TO, MailAddress[] CC, MailAddress[] BCC)
        //{

        //}
        private delegate void AsyncMethodCaller(IArgs args);
        public void SendMessageAsync(IArgs args)
        {
            AsyncMethodCaller caller = new AsyncMethodCaller(SendMessage);
            AsyncCallback callbackHandler = new AsyncCallback(AsyncCallback);
            caller.BeginInvoke(args, callbackHandler, null);

            //return Task.Run(() =>
            //{
            //    SendMessage(args);
            //});
        }

        private void AsyncCallback(IAsyncResult ar)
        {
            try
            {
                AsyncResult result = (AsyncResult)ar;
                AsyncMethodCaller caller = (AsyncMethodCaller)result.AsyncDelegate;
                caller.EndInvoke(ar);
            }
            catch (Exception e)
            {
            }
        }
        public void SendMessage(IArgs args)
        {

            try
            {
                EmailArgs EmailArgs = args as EmailArgs;
                if (EmailArgs.TO == null || EmailArgs.TO.Length == 0)
                {
                    throw new ArgumentException("En az bir tane email alıcısı olmalıdır!", "TO");
                }

                if (String.IsNullOrEmpty(EmailArgs.Subject) && String.IsNullOrEmpty(EmailArgs.Content))
                {
                    throw new ArgumentException("Email mesajı için konu ve içerik aynı anda boş olamaz!");
                }

                if (EmailArgs.CC == null) { EmailArgs.CCarray = new MailAddress[0]; }
                if (EmailArgs.BCC == null) { EmailArgs.BCCarray = new MailAddress[0]; }
                if (String.IsNullOrEmpty(EmailArgs.RuleCode)) { EmailArgs.RuleCode = "Protein-No-Rule-Code-Found"; }

                SmtpClient client = new SmtpClient(SmtpServer, int.Parse(SmtpPort));
                client.EnableSsl = EmailArgs.EnableSsl;

                MailMessage message = new MailMessage();
                client.UseDefaultCredentials = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                //client.Timeout = 10000;
                client.Credentials = new System.Net.NetworkCredential(SmtpUserName, SmtpPassword);
                message.Headers.Add("Protein-Outlook-Rule", EmailArgs.RuleCode);
                message.IsBodyHtml = EmailArgs.IsHtml;
                message.BodyEncoding = Encoding.UTF8;
                message.SubjectEncoding = Encoding.UTF8;
                message.HeadersEncoding = Encoding.UTF8;
                message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                message.Priority = EmailArgs.MailPriority;
                message.Subject = EmailArgs.Subject;
                //message.ReplyToList.Add(new MailAddress(""));
                foreach (MailAddress item in EmailArgs.TOarray) { message.To.Add(item); }
                foreach (MailAddress item in EmailArgs.CCarray) { message.CC.Add(item); }
                foreach (MailAddress item in EmailArgs.BCCarray) { message.Bcc.Add(item); }

                if (EmailArgs.From == EmailFrom.ProteinInfo)
                {
                    SmtpFromMail = ConfigurationManager.AppSettings["Mail_Info"];
                    message.From = new MailAddress(SmtpFromMail, "Protein");

                }

                if (EmailArgs.Template == EmailTemplateType.Text)
                {
                    message.Body = EmailArgs.Content;
                }


                //TASLAK OLDUĞU ZAMAN KULLANILACAK 

                //else
                //{
                //    message.Body = Properties.Resources.ResourceManager.GetString("taslak_" + (int)template).Replace("%BODYHERE%", content);
                //    message.Body = message.Body.Replace("%SUBJECTHERE%", subject);
                //    message.Body = message.Body.Replace("%TODAYHERE%", DateTime.Today.ToShortDateString());
                //}
                client.Send(message);
            }
            catch (Exception ex)
            {
            }
        }
        public async Task SendMessageAsyncForApp(IArgs args)
        {

            try
            {
                EmailArgs EmailArgs = args as EmailArgs;
                if (EmailArgs.TO == null || EmailArgs.TO.Length == 0)
                {
                    throw new ArgumentException("En az bir tane email alıcısı olmalıdır!", "TO");
                }

                if (String.IsNullOrEmpty(EmailArgs.Subject) && String.IsNullOrEmpty(EmailArgs.Content))
                {
                    throw new ArgumentException("Email mesajı için konu ve içerik aynı anda boş olamaz!");
                }

                if (EmailArgs.CC == null) { EmailArgs.CCarray = new MailAddress[0]; }
                if (EmailArgs.BCC == null) { EmailArgs.BCCarray = new MailAddress[0]; }
                if (String.IsNullOrEmpty(EmailArgs.RuleCode)) { EmailArgs.RuleCode = "Protein-No-Rule-Code-Found"; }

                SmtpClient client = new SmtpClient(SmtpServer, int.Parse(SmtpPort));
                client.EnableSsl = EmailArgs.EnableSsl;

                MailMessage message = new MailMessage();
                client.UseDefaultCredentials = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                //client.Timeout = 10000;
                client.Credentials = new System.Net.NetworkCredential(SmtpUserName, SmtpPassword);
                message.Headers.Add("Protein-Outlook-Rule", EmailArgs.RuleCode);
                message.IsBodyHtml = true;
                message.BodyEncoding = Encoding.UTF8;
                message.SubjectEncoding = Encoding.UTF8;
                message.HeadersEncoding = Encoding.UTF8;
                message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                message.Priority = EmailArgs.MailPriority;
                message.Subject = EmailArgs.Subject;
                //message.ReplyToList.Add(new MailAddress(""));
                foreach (MailAddress item in EmailArgs.TOarray) { message.To.Add(item); }
                foreach (MailAddress item in EmailArgs.CCarray) { message.CC.Add(item); }
                foreach (MailAddress item in EmailArgs.BCCarray) { message.Bcc.Add(item); }

                if (EmailArgs.From == EmailFrom.ProteinInfo)
                {
                    SmtpFromMail = ConfigurationManager.AppSettings["Mail_Info"];
                    message.From = new MailAddress(SmtpFromMail, "Protein");

                }

                if (EmailArgs.Template == EmailTemplateType.Text)
                {
                    message.Body = EmailArgs.Content;
                }


                //TASLAK OLDUĞU ZAMAN KULLANILACAK 

                //else
                //{
                //    message.Body = Properties.Resources.ResourceManager.GetString("taslak_" + (int)template).Replace("%BODYHERE%", content);
                //    message.Body = message.Body.Replace("%SUBJECTHERE%", subject);
                //    message.Body = message.Body.Replace("%TODAYHERE%", DateTime.Today.ToShortDateString());
                //}
                await client.SendMailAsync(message);
            }
            catch (Exception ex)
            {
            }
        }
    }

}
