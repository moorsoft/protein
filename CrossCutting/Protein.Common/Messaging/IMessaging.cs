﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protein.Common.Messaging
{
    public interface IMessaging
    {
        void SendMessage(IArgs args);
        void SendMessageAsync(IArgs args);
         Task SendMessageAsyncForApp(IArgs args);
    }
}
