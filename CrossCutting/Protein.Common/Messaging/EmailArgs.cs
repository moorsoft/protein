﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Protein.Common.Messaging
{
    public class EmailArgs : IArgs
    {
        private string prvToMailAdress;
        private MailAddress[] prvToMailAdressArray;
        private string prvCcMailAdress;
        private MailAddress[] prvCcMailAdressArray;
        private string prvBccMailAdress;
        private MailAddress[] prvBccMailAdressArray;

        public string RuleCode { get; set; }
        public EmailFrom From { get; set; } = EmailFrom.ProteinInfo;
        public EmailTemplateType Template { get; set; } = EmailTemplateType.Text;
        public string Subject { get; set; }
        public string Content { get; set; }
        public bool IsHtml { get; set; } = true;
        public MailPriority MailPriority { get; set; } = MailPriority.Normal;
        protected internal MailAddress[] TOarray { get; set; }
        protected internal MailAddress[] CCarray { get; set; }
        protected internal MailAddress[] BCCarray { get; set; }
        public string TO
        {
            get
            {
                return prvToMailAdress;
            }
            set
            {
                prvToMailAdress = value;
                this.TOarray = this.prvToMailAdressArray = ReviseEmail(value);
            }
        }
        public string CC
        {
            get
            {
                return prvCcMailAdress;
            }
            set
            {
                prvCcMailAdress = value;
                this.CCarray = this.prvCcMailAdressArray = ReviseEmail(value);
            }
        }
        public string BCC
        {
            get
            {
                return prvBccMailAdress;
            }
            set
            {
                prvBccMailAdress = value;
                this.BCCarray = this.prvBccMailAdressArray = ReviseEmail(value);
            }
        }

        public bool EnableSsl { get; set; } = true;
        private static MailAddress[] ReviseEmail(string email)
        {
            if (email == null) { return null; }

            List<MailAddress> list = new List<MailAddress>();

            foreach (string item in email.Split(';'))
            {
                string temp = item.Trim().ToLower();
                temp = temp.Replace("ı", "i");

                if (Regex.IsMatch(temp, @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"))
                {
                    list.Add(new MailAddress(temp));
                }
            }

            return list.ToArray();
        }
    }
}
