﻿using Protein.Common.Dto;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Entities.ProteinEntities;
using Protein.Common.Helpers;

namespace Protein.Business.Workers
{
    public class Token
    {
        private string GenerateToken()
        { return Guid.NewGuid().ToString(); }
        /// <summary>
        /// Token geçerli  mi ?
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="Token"></param>
        /// <returns></returns>
        public bool IsValidToken(int UserID, string Token)
        {
            bool result = false;
            List<TSession> session = new List<TSession>();
            try
            {
                session = new SessionRepository().FindBy(conditions: $"STATUS!='1' AND USER_ID = {UserID} AND TOKEN = {Token} and START_DATE >= {DateTime.Now.ToString()} AND EXPIRED_DATE IS NULL ");
                if (session == null) result = false;
                else if (session.Count < 1) result = false;
                else result = true;
            }
            catch { }
            finally { if (session != null) session = null; }
            return result;
        }
        /// <summary>
        /// Geçerli token'ı al
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public string GetValidToken(int UserID)
        {
            string _token = "";
            List<TSession> session = new List<TSession>();
            try
            {
                session = new SessionRepository().FindBy(conditions: $"STATUS!='1' AND USER_ID = {UserID} AND START_DATE >= {DateTime.Now.ToString()} AND EXPIRED_DATE IS NULL ");
                if (session == null) return _token;
                else if (session.Count < 1) return _token;
                else _token = session.FirstOrDefault().Token;
            }
            catch { }
            finally { if (session != null) session = null; }
            return _token;
        }
        /// <summary>
        /// Login olduğunda token'ı kullanıcıya set et
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public bool SetToken(int UserID)
        {
            bool result = false;
            TSession session = new TSession();
            try
            {
                session.Ip = ClientHelper.GetIpAddress();
                session.StartDate = DateTime.Now;
                session.Id = 0;
                session.Status = "0";
                session.Token = GenerateToken();
                session.UserID = UserID;
                session.ExpiredDate = null;
                SpResponse<TSession> spResponse = new SessionRepository().Insert(session);

                if (spResponse.Code == "100")
                { FindAndDestroyToken(UserID:UserID,CurrentToken:session.Token); session = null; spResponse = null; result = true; }
            }
            catch { }
            finally { session = null; }
            return result;
        }
        /// <summary>
        /// Token oluşurken, mevcut expired_date null olan tokenları pasif duruma düşürmek
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="CurrentToken"></param>
        private void FindAndDestroyToken(int UserID, string CurrentToken)
        {
            List<TSession> sessions = new List<TSession>();

            try
            {
                sessions = new SessionRepository().FindBy(conditions: $"STATUS!='1' AND USER_ID = {UserID} AND TOKEN != '{CurrentToken}' AND EXPIRED_DATE IS NULL");
                if (sessions == null) return;
                if (sessions.Count == 0) return;

                foreach (TSession session in sessions)
                {
                    if (!string.IsNullOrEmpty(session.Token))
                    {
                        session.ExpiredDate = DateTime.Now;
                        SpResponse<TSession> spResponse = new SessionRepository().Insert(session);
                    }
                }
            }
            catch { }
            finally { sessions = null; }
        }
    }
}
