﻿using Dapper;
using Protein.Common.Messaging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Protein.YusufTest.Sagmer;

namespace Protein.YusufTest
{

    public class sasgmr
    {
        public void Sagmer()
        {
            PoliceOnlineServiceClient client = new PoliceOnlineServiceClient();

            policeSonucType sonuc = client.policeKontrol(new policeInputType { });
        }
    }
    public class TestEntitites
    {
        [System.ComponentModel.DataAnnotations.Schema.TableAttribute("T_SP_RESPONSE")]
        public class SpResponse
        {
            [Column("ID")]
            public Int64 Id { get; set; }
            [Column("SP_REQUEST_ID")]
            [MaxLength(100, ErrorMessage = "")]
            public string SpRequestId { get; set; }
            [Column("OBJECT_ID")]
            [MaxLength(100, ErrorMessage = "")]
            public string ObjectId { get; set; }
            [Column("PK_ID")]
            public Int64 PkId { get; set; }

            [Column("CODE")]
            [Display(Name = "CODE", Description = "Code")]
            [MaxLength(10, ErrorMessage = "")]
            public string Code { get; set; }
            [Column("SP_REQUEST_DATE")]
            public DateTime SpRequestDate { get; set; }
            [Column("SP_RESPONSE_DATE")]
            public DateTime SpResponseDate { get; set; }
        }


    }

    public class Print
    {
        public string MethodName { get; set; }
        public Print()
        {
            Printf();

        }
        public void Printf()
        {
            MethodBase methodInfo = System.Reflection.MethodBase.GetCurrentMethod();
            this.MethodName = methodInfo.DeclaringType.FullName + "." + methodInfo.Name;
        }
    }

    public class EmailTest
    {
        //public void test()
        //{

        //    var path = Environment.CurrentDirectory + "\\app.config";
        //    var configMap = new ExeConfigurationFileMap { ExeConfigFilename = path };
        //    var configuration = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
        //    var settings = configuration.AppSettings.Settings;

        //    MailSender msender = new MailSender();

        //    msender.SendMessageAsync(
        //        new EmailArgs
        //        {
        //            TO = "yusuf.yoreli@mooryazilim.com",
        //            IsHtml = false,
        //            Subject = "DövKur içeriye alım başladı...",
        //            Content = "DövKur içeriye alım başladı..."
        //        });

        //    try
        //    {
        //        string today = "http://www.tcmb.gov.tr/kurlar/today.xml";



        //        List<string> currencyTypes = new List<string>
        //                    {
        //                        "USD","AUD","DKK","EUR","GBP","CHF","SEK","CAD","KWD","NOK","SAR","JPY"
        //                    };
        //        var xmlDoc = new XmlDocument();

        //        xmlDoc.Load(today);

        //        if (!string.IsNullOrEmpty(xmlDoc.InnerXml))
        //        {

        //            foreach (string curType in currencyTypes)
        //            {
        //                try
        //                {
        //                    string unit = xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='" + curType + "']/Unit").InnerXml;
        //                    decimal buying = Convert.ToDecimal(xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='" + curType + "']/BanknoteBuying").InnerXml.Replace(".", ","));
        //                    decimal selling = decimal.Parse(xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='" + curType + "']/BanknoteSelling").InnerXml.Replace(".", ","));
        //                    decimal forexBuying = decimal.Parse(xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='" + curType + "']/ForexBuying").InnerXml.Replace(".", ","));
        //                    decimal forexSelling = decimal.Parse(xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='" + curType + "']/ForexSelling").InnerXml.Replace(".", ","));

        //                    string kurTypeOrdinal = "";
        //                    switch (curType)
        //                    {
        //                        case "TRY":
        //                            kurTypeOrdinal = "1";
        //                            break;
        //                        case "USD":
        //                            kurTypeOrdinal = "2";
        //                            break;

        //                        case "AUD":
        //                            kurTypeOrdinal = "3";
        //                            break;
        //                        case "DKK":
        //                            kurTypeOrdinal = "4";
        //                            break;
        //                        case "EUR":
        //                            kurTypeOrdinal = "5";
        //                            break;
        //                        case "GBP":
        //                            kurTypeOrdinal = "6";
        //                            break;
        //                        case "CHF":
        //                            kurTypeOrdinal = "7";
        //                            break;
        //                        case "SEK":
        //                            kurTypeOrdinal = "8";
        //                            break;
        //                        case "CAD":
        //                            kurTypeOrdinal = "9";
        //                            break;
        //                        case "KWD":
        //                            kurTypeOrdinal = "10";
        //                            break;
        //                        case "NOK":
        //                            kurTypeOrdinal = "11";
        //                            break;
        //                        case "SAR":
        //                            kurTypeOrdinal = "12";
        //                            break;
        //                        case "JPY":
        //                            kurTypeOrdinal = "13";
        //                            break;
        //                        default: break;
        //                    }
        //                    Protein.Common.Entities.ProteinEntities.ExchangeRate excRate = new Protein.Common.Entities.ProteinEntities.ExchangeRate();
        //                    excRate.BanknoteSelling = selling;
        //                    excRate.BanknoteBuying = buying;
        //                    excRate.CurrencyType = kurTypeOrdinal;
        //                    excRate.ForexBuying = forexBuying;
        //                    excRate.ForexSelling = forexSelling;
        //                    excRate.Unit = unit;
        //                    excRate.RateDate = DateTime.Now;

        //                    Protein.Common.Entities.ProteinEntities.SpResponse<Protein.Common.Entities.ProteinEntities.ExchangeRate> responseExchangeRate = new Protein.Data.Repositories.ExchangeRateRepository().Insert(excRate);
        //                    if (responseExchangeRate.Code != "100")
        //                        MessageBox.Show(responseExchangeRate.Message);
        //                }
        //                catch (Exception ex)
        //                {
        //                    msender.SendMessageAsync(
        //                        new EmailArgs
        //                        {
        //                            TO = "yusuf.yoreli@mooryazilim.com",
        //                            IsHtml = false,
        //                            Subject = "DövKur içeriye alımda hata...",


        //                            Content = ex.ToString()
        //                        });
        //                }
        //            }
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        msender.SendMessageAsync(
        //              new EmailArgs
        //              {
        //                  TO = "yusuf.yoreli@mooryazilim.com",
        //                  IsHtml = false,
        //                  Subject = "DövKur içeriye alımda hata...",

        //                  Content = ex.ToString()
        //              });
        //    }
        //}
    }
}
