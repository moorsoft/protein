﻿using Protein.Business.Abstract.Service;
using Protein.Business.Concrete.Responses;
using Protein.Business.Concrete.Services;
using Protein.Common.Dto.ImportObjects;
using Protein.Common.Enums;
using Protein.Common.Extensions;
using Protein.Data.ExternalServices.KatilimHealthService;
using Protein.Data.ExternalServices.KatilimSecurityService;
using Protein.Data.ExternalServices.Log;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.YusufTest.QueueTest
{
    public class QTest
    {
        private string Token { get; set; } = "9999999999";
        public void TestQ()
        {
            try
            {
                var currentMethod = MethodBase.GetCurrentMethod();

                List<IntegrationPolicy> IntPolList = new GenericRepository<IntegrationPolicy>().FindBy($" RESPONSE_DATE IS NULL", fetchHistoricRows: true, orderby: "ID ASC");
                if (IntPolList.Count > 0)
                {
                    foreach (IntegrationPolicy itemPol in IntPolList)
                    {
                        V_Policy findPol = new GenericRepository<V_Policy>().FindBy($"POLICY_ID =  {itemPol.PolicyID} AND STATUS = {((int)ProteinEnums.PolicyStatus.TEKLIF)}", orderby: "").FirstOrDefault();

                        if (findPol != null)
                        {
                            if (findPol.IntegrationStatus == ((int)ProteinEnums.IntegrationStatus.KONTROL_SERVIS_OK).ToString())
                            {
                                PolicyDto policyDto = new PolicyDto();
                                policyDto.IsSagmerPolicySendForProd = true;
                                policyDto.PolicyId = itemPol.PolicyID;
                                policyDto.EndorsementId = itemPol.EndorsementId;
                                policyDto.CorrelationId = itemPol.CorrelationId;
                                policyDto.ReqId = itemPol.ReqId;

                                IService<PolicyDto> policyService = new PolicyService();
                                ServiceResponse response = policyService.ServiceInsert(policyDto, this.Token);

                                if (findPol.COMPANY_ID == 5)
                                {
                                    string authKey = "";
                                    SecurityServiceClient securityService = new SecurityServiceClient();
                                    Data.ExternalServices.KatilimSecurityService.ServiceResult result = securityService.GetAuthenticationKey("9CDE40F6327BC1ED744BD1D2D09D79F4", "TPAWS", "TPAWS", ref authKey);

                                    if (response.IsSuccess)
                                    {
                                        if (!string.IsNullOrEmpty(authKey))
                                        {
                                            PolicyStateUpdateInput input = new PolicyStateUpdateInput
                                            {
                                                AuthenticationKey = authKey,
                                                Description = "OK",
                                                PolicyNo = (long)findPol.POLICY_NUMBER,
                                                RenewalNo = long.Parse(findPol.RENEWAL_NO.ToString()),
                                                StateCode = "001",
                                                TransferId = itemPol.ReqId,
                                                TpaCompanyCode = findPol.COMPANY_ID.ToString(),
                                                TPAPolicyNo = itemPol.PolicyID
                                            };
                                            Data.ExternalServices.KatilimHealthService.HealthTPAClient client = new Data.ExternalServices.KatilimHealthService.HealthTPAClient();
                                            PolicyStateUpdateResult StateResult = client.PolicyStateUpdate(input);
                                            if (StateResult.ServiceResult.IsOk)
                                            {
                                                itemPol.ResponseDate = DateTime.Now;
                                                new GenericRepository<IntegrationPolicy>().Insert(itemPol);

                                                Policy updatePol = new GenericRepository<Policy>().FindById(itemPol.PolicyID);
                                                updatePol.Status = ((int)ProteinEnums.PolicyStatus.TANZIMLI).ToString();
                                                updatePol.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.AKTARILDI).ToString();
                                                new GenericRepository<Policy>().Insert(updatePol);

                                                IntegrationLog log = new IntegrationLog();
                                                log.CorrelationId = itemPol.CorrelationId;
                                                log.WsName = currentMethod.DeclaringType.FullName;
                                                log.WsMethod = currentMethod.Name;
                                                log.Request = input.ToXML(true);
                                                log.Response = result.ToXML(true);
                                                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                                                log.WsRequestDate = DateTime.Now;
                                                log.ResponseStatusDesc = "OK";
                                                log.Type = ((int)LogType.COMPLETED).ToString();
                                                IntegrationLogHelper.AddToLog(log);
                                            }
                                            else
                                            {
                                                IntegrationLog log = new IntegrationLog();
                                                log.CorrelationId = itemPol.CorrelationId;
                                                log.WsName = currentMethod.DeclaringType.FullName;
                                                log.WsMethod = currentMethod.Name;
                                                log.Request = input.ToXML(true);
                                                log.Response = result.ToXML(true);
                                                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                                                log.WsRequestDate = DateTime.Now;
                                                log.ResponseStatusDesc = "KatilimServiceNotSuccess";
                                                log.Type = ((int)LogType.FAILED).ToString();
                                                IntegrationLogHelper.AddToLog(log,true);
                                            }
                                        }
                                        else
                                        {
                                            IntegrationLog log = new IntegrationLog();
                                            log.CorrelationId = itemPol.CorrelationId;
                                            log.WsName = currentMethod.DeclaringType.FullName;
                                            log.WsMethod = currentMethod.Name;
                                            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                                            log.ResponseStatusDesc = "KatilimAuthError";
                                            log.WsRequestDate = DateTime.Now;
                                            log.Type = ((int)LogType.FAILED).ToString();
                                            IntegrationLogHelper.AddToLog(log,true);
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(authKey))
                                        {
                                            PolicyStateUpdateInput input = new PolicyStateUpdateInput
                                            {
                                                AuthenticationKey = authKey,
                                                Description = "ERROR",
                                                PolicyNo = (long)findPol.POLICY_NUMBER,
                                                RenewalNo = long.Parse(findPol.RENEWAL_NO.ToString()),
                                                ErrorKeyValueList = new ErrorList[] { new ErrorList { ErrorCode = response.SagmerCode, ErrorDescription = response.SagmerMessage } },
                                                StateCode = "000",
                                                TransferId = itemPol.ReqId,
                                                TpaCompanyCode = findPol.COMPANY_ID.ToString(),
                                                TPAPolicyNo = itemPol.PolicyID
                                            };
                                            Data.ExternalServices.KatilimHealthService.HealthTPAClient client = new Data.ExternalServices.KatilimHealthService.HealthTPAClient();
                                            PolicyStateUpdateResult StateResult = client.PolicyStateUpdate(input);
                                            if (StateResult.ServiceResult.IsOk)
                                            {
                                                itemPol.ResponseDate = DateTime.Now;
                                                new GenericRepository<IntegrationPolicy>().Insert(itemPol);

                                                Policy updatePol = new GenericRepository<Policy>().FindById(itemPol.PolicyID);
                                                updatePol.IntegrationStatus = ((int)ProteinEnums.IntegrationStatus.AKTARIM_HATASI).ToString();
                                                new GenericRepository<Policy>().Insert(updatePol);

                                                IntegrationLog log = new IntegrationLog();
                                                log.CorrelationId = itemPol.CorrelationId;
                                                log.WsName = currentMethod.DeclaringType.FullName;
                                                log.WsMethod = currentMethod.Name;
                                                log.Request = input.ToXML(true);
                                                log.Response = result.ToXML(true);
                                                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                                                log.WsRequestDate = DateTime.Now;
                                                log.ResponseStatusDesc = "SagmerError";
                                                log.Type = ((int)LogType.FAILED).ToString();
                                                IntegrationLogHelper.AddToLog(log);
                                            }
                                            else
                                            {
                                                IntegrationLog log = new IntegrationLog();
                                                log.CorrelationId = itemPol.CorrelationId;
                                                log.WsName = currentMethod.DeclaringType.FullName;
                                                log.WsMethod = currentMethod.Name;
                                                log.Request = input.ToXML(true);
                                                log.Response = result.ToXML(true);
                                                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                                                log.WsRequestDate = DateTime.Now;
                                                log.ResponseStatusDesc = "KatilimServiceNotSuccess";
                                                log.Type = ((int)LogType.FAILED).ToString();
                                                IntegrationLogHelper.AddToLog(log);
                                            }
                                        }
                                        else
                                        {
                                            IntegrationLog log = new IntegrationLog();
                                            log.CorrelationId = itemPol.CorrelationId;
                                            log.WsName = currentMethod.DeclaringType.FullName;
                                            log.WsMethod = currentMethod.Name;
                                            log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                                            log.ResponseStatusDesc = "KatilimAuthError";
                                            log.WsRequestDate = DateTime.Now;
                                            log.Type = ((int)LogType.FAILED).ToString();
                                            IntegrationLogHelper.AddToLog(log);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                IntegrationLog log = new IntegrationLog();
                                log.CorrelationId = itemPol.CorrelationId;
                                log.WsName = currentMethod.DeclaringType.FullName;
                                log.WsMethod = currentMethod.Name;
                                log.HttpStatus = ((int)HttpStatusCode.OK).ToString();
                                log.ResponseStatusDesc = "Poliçe Kuyrukta Ama Sagmer Kontrol Servisinden Geçmemiş";
                                log.WsRequestDate = DateTime.Now;
                                log.Type = ((int)LogType.FAILED).ToString();
                                IntegrationLogHelper.AddToLog(log);
                            }
                        }

                    }
                }
            }
            catch { }
        }

        //public static void SagmerControlTest()
        //{
        //    try
        //    {
        //        IService<PolicyDto> policyService = new PolicyService();
        //        PolicyDto polDto = new PolicyDto();
        //        polDto.PolicyId = 1663;
        //        polDto.EndorsementId = 706;
        //        polDto.IsSagmerPolicySend = true;
        //        ServiceResponse response = policyService.ServiceInsert(polDto, "9999999999");
        //        if (response.IsSucces)
        //        { }
        //        else { }

        //        string responseText = response.Message;
        //    }
        //    catch (Exception ex) { }
        //}
    }
}
