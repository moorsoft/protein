﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Dapper;
using Protein.Data.Repositories;
using static Protein.Common.Entities.ProteinEntities;
using System.Data;
using Oracle.DataAccess.Client;
using Protein.Data.ExternalServices.RxMedia;
using Protein.Data.ExternalServices.RxMedia.Result;
using Protein.Common.Extensions;
using Newtonsoft.Json;
using Protein.Business.Print.ProvisionReject;
using Protein.Data.ExternalServices.InsuranceCompanies;
using static Protein.Data.ExternalServices.Common.Lib.NN.NNInputOutputTypes;
using static Protein.Common.Enums.ProteinEnums;
using Protein.Common.Constants;
using Protein.Common.Enums;
using Protein.Common.Entities;
using Protein.Common.Dto.ImportObjects;
using Protein.Business.Abstract.Service;
using Protein.Business.Concrete.Services;
using Protein.YusufTest.NNHayatTest;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Common.Dto;
using Protein.Data.Helpers;
using static Protein.Common.Constants.Constants;
using System.Reflection;
using Protein.Data.Export.Model;

namespace Protein.YusufTest
{
    class Program
    {
        public static string[] sff { get; set; }
        public static void InnerClass()
        {
            var instance = Activator.CreateInstance(typeof(TestEntitites));

            var findHe = instance.GetType().GetNestedTypes();

            string atrrName = typeof(TableAttribute).Name;

            foreach (var item in findHe)
            {
                var type = item.GetCustomAttributes(false);

                var tableMapping = type.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.TableAttribute));
            }
        }
        public static void BlobTest()
        {

            String SourceLoc = Environment.CurrentDirectory + "\\TamamlayiciSaglikWebServisV3.pdf";

            FileStream fs = new FileStream(SourceLoc, FileMode.Open, FileAccess.Read);


            byte[] ImageData = new byte[fs.Length];


            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));


            fs.Close();


            string dbHost = "185.141.109.26";
            string dbPort = "1521";
            string dbPwd = "prodb";
            string dbUserId = "C##TPA";
            string serviceName = "ORCLTEST";

            string connectionString = $"Data Source=(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = {dbHost})(PORT = {dbPort}))(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = {serviceName})));User Id={dbUserId};Pooling=False;Password={dbPwd}";

            using (OracleConnection cn = new OracleConnection(connectionString))
            {
                cn.Open();

                //OracleCommand ora_cmd = new OracleCommand("SELECT FILE_CONTENT FROM t_media where ID = 6380", cn);
                //var reader = ora_cmd.ExecuteReader();
                //if (reader.Read())
                //{
                //    // Fetch the blob
                //    OracleBlob imgBlob = reader.GetOracleBlob(0);
                //    // Create byte array to read the blob into
                //    byte[] imgBytes = new byte[imgBlob.Length];
                //    // Read the blob into the byte array
                //    imgBlob.Read(imgBytes, 0, imgBytes.Length);

                //    String fileName = Environment.CurrentDirectory + "\\doga23232323.jpg";
                //    using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                //    {
                //        fs.Write(imgBytes, 0, imgBytes.Length);

                //    }
                //}

                //Send XML to SP_EXECUTE Stored Procedure
                OracleCommand ora_cmd = new OracleCommand("SF_MEDIA_UPLOAD", cn);
                ora_cmd.BindByName = true;
                ora_cmd.CommandType = CommandType.StoredProcedure;

                ora_cmd.Parameters.Add("p_FILE_CONTENT", OracleDbType.Blob, ImageData, ParameterDirection.Input);
                ora_cmd.Parameters.Add("p_NAME", OracleDbType.Varchar2, "Name", ParameterDirection.Input);
                ora_cmd.Parameters.Add("P_FILENAME", OracleDbType.Varchar2, "FileName", ParameterDirection.Input);
                ora_cmd.Parameters.Add("v_ID", OracleDbType.Decimal, ParameterDirection.ReturnValue);
                ora_cmd.ExecuteNonQuery();

                object returnID = ora_cmd.Parameters["v_ID"].Value;
                Console.WriteLine(returnID.ToString());

                //6380 return ID
            }


        }
        public static void RxMediaQuery()
        {
            Helper hlp = new Helper();
            MedicineInfo mInfo = hlp.GetMedicineInfoByBarcode("8699546020478");
            var mInfo2 = mInfo;
        }
        public static void AnkurSync()
        {
            List<V_ProviderWebSource> AllList = new GenericRepository<V_ProviderWebSource>().FindAll();

            foreach (V_ProviderWebSource item in AllList)
            {
                ProviderWeb pw = new ProviderWeb
                {
                    ADDRESS = item.ADDRESS,
                    CITY_NAME = item.CITY_NAME,
                    CompanyId = item.CompanyId,
                    CompanyWebId = item.CompanyWebId,
                    COUNTY_NAME = item.COUNTY_NAME,
                    ProductCode = item.ProductId.ToString(),
                    ProviderTypeName = System.Enum.Parse(typeof(ProteinEnums.ProviderType), item.ProviderType).ToString(),
                    ProductName = item.ProductName,
                    ProviderPhoneNo = item.PhoneNo,
                    ProviderName = item.ProviderName,
                    PROVIDER_TYPE = item.ProviderType,
                    Status = "0"
                };
                SpResponse spResponse = new GenericRepository<ProviderWeb>().Insert(pw, "9999999999");
                if (spResponse.Code == "100")
                {

                }
            }
        }
        public static void AnkurToJson()
        {
            List<V_ProviderWebExport> providerWeb = new GenericRepository<V_ProviderWebExport>().FindAll();

            string json = providerWeb.ToJSON();


            //string isim = "Ahmer Refik Cümbüş";

            //if (isim.Split(' ').Count() > 1)
            //{
            //    string soyad = isim.Split(' ')[isim.Split(' ').Count() - 1].ToString().Trim();
            //    string name = isim.Replace(soyad, "").Trim();
            //}

        }
        public static void testPdf()
        {
            PrintProvisionReject re = new PrintProvisionReject();
            re.DoWork(new PrintProvisionRejectReq());
        }
        public static void SagmerTest()
        {
            SagmerSrvWS.SagmerSrvSoapClient client = new SagmerSrvWS.SagmerSrvSoapClient();
            var sonuc = client.police(1537, "HFJF2F876ASDADFDSF1445545LKO0914S00872494037RKFKGH", SagmerSrvWS.SagmerServiceType.KONTROL);

        }
        public static void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                serializer.Serialize(writer, null);
                return;
            }

            // find all properties with type 'int'
            var properties = value.GetType().GetProperties().Where(p => p.PropertyType == typeof(int));

            writer.WriteStartObject();

            foreach (var property in properties)
            {

                writer.WritePropertyName(property.Name);

                serializer.Serialize(writer, property.GetValue(value, null));
            }

            writer.WriteEndObject();
        }
        static void testSet()
        {
            //Installment installment = new Installment
            //{
            //    Status = ((int)ProteinEnums.Status.AKTIF).ToString(),
            //    Id = 554,
            //    PolicyId = 1527
            //};

            //new GenericRepository<Installment>().Insert(instalşl);
        }
        static void DogaServis()
        {
            DoganAgencyServiceConsoleTest.AcenteBilgiServisleriSoapClient dogaAcenteServis = new DoganAgencyServiceConsoleTest.AcenteBilgiServisleriSoapClient();
            DoganAgencyServiceConsoleTest.GeriyePoliceTransferCevap result = dogaAcenteServis.TekPolice("ws_IMECE", "8Qdzp9vpMHkmfNwH", "302217", "303", "37945681", "", "");
        }
        static void CompanyParamEntry()
        {
            var param = new Parameter
            {
                Environment = "P",
                Key = "PASSWORD",
                Value = "AA61454AEB4D4C8G9FC657A113E6B226",
                Status = ((int)Status.AKTIF).ToString(),
                SystemType = ((int)SystemType.PORTTSS).ToString(),
                Id = 0
            };
            var spresponseParam = new GenericRepository<Parameter>().Insert(param);
            if (spresponseParam.Code == "100")
            {
                CompanyParameter comP = new CompanyParameter
                {
                    CompanyId = 3,
                    ParameterId = spresponseParam.PkId,
                    Status = ((int)Status.AKTIF).ToString()
                };
                SpResponse spResponse = new GenericRepository<CompanyParameter>().Insert(comP);

            }
            param = new Parameter
            {
                Environment = "P",
                Key = "PASSWORD",
                Value = "F164810EF2BA4045LC606E33EF90A40F",
                Status = ((int)Status.AKTIF).ToString(),
                SystemType = ((int)SystemType.PORTTSS).ToString(),
                Id = 0
            };
            spresponseParam = new GenericRepository<Parameter>().Insert(param);
            if (spresponseParam.Code == "100")
            {
                CompanyParameter comP = new CompanyParameter
                {
                    CompanyId = 5,
                    ParameterId = spresponseParam.PkId,
                    Status = ((int)Status.AKTIF).ToString()
                };
                SpResponse spResponse = new GenericRepository<CompanyParameter>().Insert(comP);

            }



            //   param = new Parameter
            //   {
            //       Environment = "P",
            //       Key = "ANKUR_ID",
            //       Value = "2",
            //       Status = ((int)Status.AKTIF).ToString(),
            //       SystemType = ((int)SystemType.WS).ToString(),
            //       Id = 0
            //   };
            //  spresponseParam = new GenericRepository<Parameter>().Insert(param);
            //   if (spresponseParam.Code == "100")
            //   {
            //       CompanyParameter comP = new CompanyParameter
            //       {
            //           CompanyId = 2,
            //           ParameterId = spresponseParam.PkId,
            //           Status = ((int)Status.AKTIF).ToString()
            //       };
            //       SpResponse<CompanyParameter> spResponse = new GenericRepository<CompanyParameter>().Insert(comP);
            //   }


            //   param = new Parameter
            //   {
            //       Environment = "P",
            //       Key = "ANKUR_ID",
            //       Value = "4",
            //       Status = ((int)Status.AKTIF).ToString(),
            //       SystemType = ((int)SystemType.WS).ToString(),
            //       Id = 0
            //   };
            //spresponseParam = new GenericRepository<Parameter>().Insert(param);
            //   if (spresponseParam.Code == "100")
            //   {
            //       CompanyParameter comP = new CompanyParameter
            //       {
            //           CompanyId = 3,
            //           ParameterId = spresponseParam.PkId,
            //           Status = ((int)Status.AKTIF).ToString()
            //       };
            //       SpResponse<CompanyParameter> spResponse = new GenericRepository<CompanyParameter>().Insert(comP);
            //   }


            // param = new Parameter
            //   {
            //       Environment = "P",
            //       Key = "ANKUR_ID",
            //       Value = "5",
            //       Status = ((int)Status.AKTIF).ToString(),
            //       SystemType = ((int)SystemType.WS).ToString(),
            //       Id = 0
            //   };
            //  spresponseParam = new GenericRepository<Parameter>().Insert(param);
            //   if (spresponseParam.Code == "100")
            //   {
            //       CompanyParameter comP = new CompanyParameter
            //       {
            //           CompanyId = 4,
            //           ParameterId = spresponseParam.PkId,
            //           Status = ((int)Status.AKTIF).ToString()
            //       };
            //       SpResponse<CompanyParameter> spResponse = new GenericRepository<CompanyParameter>().Insert(comP);
            //   }

            //  param = new Parameter
            //   {
            //       Environment = "P",
            //       Key = "ANKUR_ID",
            //       Value = "1",
            //       Status = ((int)Status.AKTIF).ToString(),
            //       SystemType = ((int)SystemType.WS).ToString(),
            //       Id = 0
            //   };
            //  spresponseParam = new GenericRepository<Parameter>().Insert(param);
            //   if (spresponseParam.Code == "100")
            //   {
            //       CompanyParameter comP = new CompanyParameter
            //       {
            //           CompanyId = 5,
            //           ParameterId = spresponseParam.PkId,
            //           Status = ((int)Status.AKTIF).ToString()
            //       };
            //       SpResponse<CompanyParameter> spResponse = new GenericRepository<CompanyParameter>().Insert(comP);
            //   }

            //  param = new Parameter
            //   {
            //       Environment = "P",
            //       Key = "ANKUR_ID",
            //       Value = "11",
            //       Status = ((int)Status.AKTIF).ToString(),
            //       SystemType = ((int)SystemType.WS).ToString(),
            //       Id = 0
            //   };
            //  spresponseParam = new GenericRepository<Parameter>().Insert(param);
            //   if (spresponseParam.Code == "100")
            //   {
            //       CompanyParameter comP = new CompanyParameter
            //       {
            //           CompanyId = 6,
            //           ParameterId = spresponseParam.PkId,
            //           Status = ((int)Status.AKTIF).ToString()
            //       };
            //       SpResponse<CompanyParameter> spResponse = new GenericRepository<CompanyParameter>().Insert(comP);
            //   }

            //  param = new Parameter
            //   {
            //       Environment = "P",
            //       Key = "ANKUR_ID",
            //       Value = "9",
            //       Status = ((int)Status.AKTIF).ToString(),
            //       SystemType = ((int)SystemType.WS).ToString(),
            //       Id = 0
            //   };
            //  spresponseParam = new GenericRepository<Parameter>().Insert(param);
            //   if (spresponseParam.Code == "100")
            //   {
            //       CompanyParameter comP = new CompanyParameter
            //       {
            //           CompanyId = 9,
            //           ParameterId = spresponseParam.PkId,
            //           Status = ((int)Status.AKTIF).ToString()
            //       };
            //       SpResponse<CompanyParameter> spResponse = new GenericRepository<CompanyParameter>().Insert(comP);
            //   }

            //  param = new Parameter
            //   {
            //       Environment = "P",
            //       Key = "ANKUR_ID",
            //       Value = "88",
            //       Status = ((int)Status.AKTIF).ToString(),
            //       SystemType = ((int)SystemType.WS).ToString(),
            //       Id = 0
            //   };
            //  spresponseParam = new GenericRepository<Parameter>().Insert(param);
            //   if (spresponseParam.Code == "100")
            //   {
            //       CompanyParameter comP = new CompanyParameter
            //       {
            //           CompanyId = 88,
            //           ParameterId = spresponseParam.PkId,
            //           Status = ((int)Status.AKTIF).ToString()
            //       };
            //       SpResponse<CompanyParameter> spResponse = new GenericRepository<CompanyParameter>().Insert(comP);
            //   }
        }
        static void NNhayatTest()
        {
            CommonProxyRes res = new CommonProxyRes();

            NNHayatServiceClientV2 client = new NNHayatServiceClientV2();
            NNHakSahiplikReq req = new NNHakSahiplikReq();
            req.AyaktanYatarak = "A";
            req.Kurum = "1";
            req.OlayTarihi = "2018.07.17";
            req.TcKimlikNo = "43174172166";
            res = client.PolicyState(req);
            Console.WriteLine(res.Description);
            //NNHayatTest.IntegrationWebServicesSoapClient client = new IntegrationWebServicesSoapClient();

            //SigortaliSorgula input = new SigortaliSorgula();
            //input.AyaktanYatarak = "A";
            //input.Kurum = "1";
            //input.ayaktanYatarakSpecified1 = true;
            //input.kurumSpecified1 = true;
            //input.OlayTarihi = "2018.07.17";
            //input.olayTarihiSpecified1 = true;
            //input.Password = "DUWS15CU";
            //input.pasaportNoSpecified1 = true;
            //input.TcKimlikNo = "43174172166";
            //input.tcKimlikNoSpecified1 = true;
            //input.UserName = "USERWSINTEGRATION";
            //input.userNameSpecified1 = true;
            //input.AyaktanYatarak = "A";
            //input.ayaktanYatarakSpecified1 = true;

            //var cevap = client.SigortaliHakSorgula(input);

            //HakSahiplikInput input = new HakSahiplikInput();

            //input.AyaktanYatarak = "A";
            //input.Kurum = "1";
            //input.OlayTarihi = "2018.07.17";
            //input.Password = "FXHMN24XK04";
            //input.TcKimlikNo = "43174172166";
            //input.UserName = "USERWSINTEGRATION";
            //input.AyaktanYatarak = "A";

            ////IcmalGirisInput input = new IcmalGirisInput();
            ////input.IcmalNo = "fdf";
            ////input.Kurum = "fdf";
            ////input.Password = "fdfds";
            ////input.UserName = "fdfd";
            ////input.TazminatBilgileri = new List<TazminatBilgisi>();

            ////TazminatBilgisi tazB = new TazminatBilgisi();
            ////tazB.FaturaNo = "dadffsdf";
            ////tazB.FaturaTarihi = "sdfsdfsd";
            ////tazB.ProvizyonNo = "fdsfsdfsdf";
            ////input.TazminatBilgileri.Add(tazB);

            //NNHayatServiceClient client = new NNHayatServiceClient();

            ////client.icmalGiris(input);
            //client.hakSahipligiSorgula(input);
        }
        static void sagmettst()
        {
            PolicyDto dto = new PolicyDto();
            dto.PolicyId = 224;
            dto.EndorsementId = 113;
            dto.IsSagmerPolicySend = true;
            IService<PolicyDto> policyService = new PolicyService();
            var res = policyService.ServiceInsert(dto);

            var fdf = res;
        }
        static void SpExecute3Test()
        {
            CommonPolicyDto commonPolicyDto = new CommonPolicyDto();
            commonPolicyDto.insurer = new InsurerDto
            {
                contact = new ContactDto
                {
                    identityNo = 15267489547,
                    taxNumber = 5689120456,
                    title = "TEST",
                    type = ContactType.TUZEL
                },
                corporate = new CorporateDto
                {
                    name = "TEST",
                    type = CorporateType.LIMITED
                },
                mobilePhone = new PhoneDto
                {
                    no = "05121212132",
                },
                email = new EmailDto
                {
                    details = "Test",
                    type = EmailType.IS
                }
            };
            PolicyService policyService = new PolicyService();
            policyService.InsertInsurer(commonPolicyDto);

            //Contact con = new Contact();
            //con.IdentityNo = 34324;

            //Corporate cor = new Corporate();
            //cor.Type = "0";
            //cor.Name = "ttest";

            //Address adr = new Address();
            //adr.Type = "0";
            //adr.CityId = 179;
            //adr.CountyId = 354399;
            //adr.Details = "fdssfsd";
            //adr.District = "fdsf";

            //Provider provider = new Provider();
            //provider.OfficialCode = "134";
            //provider.Type = "1";
            //provider.ProviderGroupId = 10;
            //provider.StoppageOverwrite = 12;
            //provider.RevisionDate = DateTime.Now;
            //provider.IsEBill = "1";

            //Phone phone = new Phone();
            //phone.CountryId = 222;
            //phone.No = "12312312";
            //phone.Type = "0";

            //ProviderPhone provPhone = new ProviderPhone();

            //User user = new User();
            //user.Username = "sdfsdf";
            //user.Password = "fdsfdsf";
            //user.Email = "sdfsdf@fdsfs.com";
            //user.IsRememberMe = "0";

            //ProviderUser provUser = new ProviderUser();

            //List<ISpExecute3> lst = new List<ISpExecute3>();
            //lst.Add(con);
            //lst.Add(cor);
            //lst.Add(adr);
            //lst.Add(provider);
            //lst.Add(phone);
            //lst.Add(provPhone);
            //lst.Add(provUser);
            //new GenericRepository<ISpExecute3>().InsertSpExecute3(lst);
        }
        static void SagmerVeriOlgunluk2()
        {
            SagmerVeriOlgunluk.OlgunlukServiceClient client = new SagmerVeriOlgunluk.OlgunlukServiceClient();


            //SagmerVeriOlgunluk.hasarFarkVeriSorguInput
            //SagmerVeriOlgunluk.hasarFarkVeriSorguOutput
            //SagmerVeriOlgunluk.hasarGunBazliOlgunlukDetayInput
            //SagmerVeriOlgunluk.hasarGunBazliOlgunlukDetayOutput

            //SagmerVeriOlgunluk.hasarGunBazliOlgunlukIcmalInput

            //client.hasarGunBazliOlgunlukIcmal()
        }
        static void SagmerTest2()
        {
            Sagmer.PoliceOnlineServiceClient client = new Sagmer.PoliceOnlineServiceClient();
            client.ClientCredentials.UserName.UserName = "017|commer";


            Sagmer.policeInputType input = new Sagmer.policeInputType();
            input.branshKod = 2;
            input.otorizasyonKod = "fdsfsdfsd";

            var result = client.policeKontrol(input);
            var refdjf = result;
        }
        static void PolicyDelete()
        {
            List<int> lst = new List<int> { 291,
                                304,
                                308,
                                312,
                                416,
                                419,
                                422,
                                424,
                                427,
                                430,
                                433,
                                436,
                                439,
                                442,
                                444,
                                448,
                                451,
                                454,
                                457,
                                460,
                                463,
                                465,
                                468,
                                472,
                                475,
                                478,
                                481,
                                483,
                                486,
                                489
                                };

            foreach (var item in lst)
            {
                Policy policy = new GenericRepository<Policy>().FindById(item);
                policy.Status = ((int)ProteinEnums.PolicyStatus.SILINDI).ToString();
                new GenericRepository<Policy>().Insert(policy, "9999999999");
            }
        }


        static void CompanyUserSave()
        {
            User user = new User
            {
                Id = 83,
                Username = "HALK",
                Password = "HALK1234".ToMD5().ToUpper(),
            };
            var spUser = new GenericRepository<User>().Insert(user, "9999999999");
            if (spUser.Code == "100")
            {
                long userId = spUser.PkId;

                CompanyUser companyUser = new CompanyUser
                {
                    CompanyId = 1,
                    UserId = userId
                };
                var spCompanyUser = new GenericRepository<CompanyUser>().Insert(companyUser, "9999999999");

            }
        }

        static void GetIntegrationLog()
        {

            var a = new GenericRepository<IntegrationLog>().FindBy(conditions: $"ID=74072", orderby: "", fetchHistoricRows: true);

        }

        static void Main(string[] args)
        {
            SpExecute3Test();
            //CompanyParamEntry();
            //List<TableColumnHeader> tableColumnHeaderList = new GenericRepository<TableColumnHeader>().FindBy("TABLE_NAME='V_RPT_CLAIM_PREMIUM' OR TABLE_NAME='V_RptClaimPremium' OR TABLE_NAME='V_RPT_CLAIM_DETAILS' OR TABLE_NAME='V_RptClaimDetails' OR TABLE_NAME='V_RPT_PRODUCTION' OR TABLE_NAME='V_RptProduction'");
            //foreach (var item in tableColumnHeaderList)
            //{
            //    item.STATUS = ((int)Status.SILINDI).ToString();
            //    new GenericRepository<TableColumnHeader>().Insert(item);
            //}


            //try
            //{
            //    List<V_Provider> lst = new GenericRepository<V_Provider>().FindBy("provider_id = 235118", fetchDeletedRows: true, fetchHistoricRows: true, orderby: "");
            //    List<V_Provider> v_Providers = new List<V_Provider>();
            //    Type type = v_Providers.GetType();
            //    Type dg = lst[0].GetType();
            //    int i = 0;
            //    foreach (PropertyInfo prop in dg.GetProperties())
            //    {
            //        i++;
            //        var attributes = prop.GetCustomAttributes(false);
            //        var columnMapping = attributes.FirstOrDefault(a => a.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute));
            //        var realColumn = columnMapping as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute;

            //        TableColumnHeader tableColumnHeader = new TableColumnHeader
            //        {
            //            TABLE_NAME = "V_RPT_PAYROLL",
            //            TABLE_HEADER = "İCMAL RAPORU",
            //            COLUMN_NAME = realColumn.Name,
            //            COLUMN_HEADER = realColumn.Name,
            //            ORDER_NUM = i
            //        };
            //        new GenericRepository<TableColumnHeader>().Insert(tableColumnHeader);
            //    }

            //}
            //catch (Exception ex)
            //{

            //    throw;
            //}



            // var spResponse = new GenericRepository<TableColumnHeader>().InsertEntities(tableColumnHeaderList);
            //var spResponse = new TableColumnHeaderRepository().InsertEntitiesExecute(tableColumnHeaderList, "999999999999999");
            //GetIntegrationLog();
            //CompanyUserSave();
            //CompanyParamEntry();
            //PolicyDelete();
            //CompanyEnum obj = (CompanyEnum)5;

            //if (obj == CompanyEnum.DOGA)
            //{
            //    Console.Write("Dogaaa");
            //}
            //NNhayatTest();
            //PolicyDelete();
            //CompanyParamEntry();

            //sagmettst();
            //NNhayatTest();

            //SagmerTest2();

            //SpExecute3Test();

            //IPrint<PolicyInsuredExitReq> print = new PolicyInsuredExit();
            //print.DoWork(new PolicyInsuredExitReq
            //{
            //    PolicyId = 1663,
            //    EndorsementId = 706
            //});


            //AnkurSync();

            //NNhayatTest();
            //DogaServis();

            // CompanyParamEntry();
            //new QTest().TestQ();

            //new gTest().testt();
            //SF_PlanCoverageVarParams sfParam = new SF_PlanCoverageVarParams();
            //sfParam.p_package_id = 80;
            //sfParam.p_plan_coverage_id = "2214";
            //sfParam.p_type = "1";
            //sfParam.p_cid = "";

            //List<SF_PlanCoverageVar> result = new GenericRepository<SF_PlanCoverageVar>().FindBySf(sfParam);

            //foreach (var item in result)
            //{
            //    foreach (var itemProp in item.GetType().GetProperties())
            //    {
            //        Console.WriteLine(itemProp.Name + " ==== " + itemProp.GetValue(item, null));
            //    }
            //}

            //InServiceClient inSclient = new InServiceClient();
            //string varf = inSclient.GetAuthenticationKey();

            //MailSender msender = new MailSender();
            //msender.SendMessageAsync(
            //   new EmailArgs
            //   {
            //       EnableSsl = true,
            //       TO = "yusuf.yoreli@mooryazilim.com",
            //       IsHtml = false,
            //       Subject = "DövKur içeriye alım başladı...",
            //       Content = "DövKur içeriye alım başladı..."
            //   });



            //testPdf();
            //PoliceOnlineServiceClient client = new PoliceOnlineServiceClient();
            //client.ClientCredentials.ClientCertificate.Certificate.
            //client.policeKontrol();
            //Sagmer.PoliceOnlineServiceClient client = new PoliceOnlineServiceClient();

            ////client.tazminatOdeme()

            //string soapResult = "";

            //StringBuilder sb = new StringBuilder();
            //sb.AppendLine("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pts=\"http://pts.sagmer.org.tr\">");
            //sb.AppendLine("<soapenv:Header>");
            //sb.AppendLine("<wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">");
            //sb.AppendLine("<wsse:UsernameToken xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">");
            //sb.AppendLine("<wsse:Username>017|commer</wsse:Username>");
            //sb.AppendLine("<wsse:KurumKod>017</wsse:KurumKod>");
            //sb.AppendLine("</wsse:UsernameToken>");
            //sb.AppendLine("</wsse:Security>");
            //sb.AppendLine("</soapenv:Header>");
            //sb.AppendLine("<soapenv:Body>");
            //sb.AppendLine(" <pts:policeKontrol>");
            //sb.AppendLine("<pts:policeKontrol>");
            //sb.AppendLine(" <otorizasyonKod>a2f522bc5f8ac9ae451a082dba95fe59</otorizasyonKod>");
            //sb.AppendLine(" <sigortaSirketKod>017</sigortaSirketKod>");
            //sb.AppendLine(" <branshKod>7</branshKod>");
            //sb.AppendLine("<dovizCinsi>TRY</dovizCinsi>");
            //sb.AppendLine("  <ilKod>034</ilKod>");
            //sb.AppendLine("    <odemeKod>2</odemeKod>");
            //sb.AppendLine("  <policeBaslamaTarihi>2018-06-11T00:00:00.000Z</policeBaslamaTarihi>");
            //sb.AppendLine(" <policeBitisTarihi>2019-06-11T00:00:00.000Z</policeBitisTarihi>");
            //sb.AppendLine(" <policeBrutPrimi>     195.20</policeBrutPrimi>");
            //sb.AppendLine("    <policeNetPrimi>     195.20</policeNetPrimi>");
            //sb.AppendLine(" <policeNo>49427350</policeNo>");
            //sb.AppendLine("  <policeTanzimTarihi>2018-06-11T00:00:00.000Z</policeTanzimTarihi>");
            //sb.AppendLine("   <policeTip>F</policeTip>");
            //sb.AppendLine(" <sigortaEttirenType>");
            //sb.AppendLine("<ad> AHMED </ad> <adres>.</adres>");
            //sb.AppendLine(" <cinsiyet> E </cinsiyet><dogumTarihi>1989-06-07T00:00:00.000Z</dogumTarihi>");

            //sb.AppendLine("  <dogumYeri>GER</dogumYeri><kimlikNo>F506870</kimlikNo><kimlikTipKod>3</kimlikTipKod>");
            //sb.AppendLine("  <sigortaEttirenUyruk>YABANCI</sigortaEttirenUyruk> <soyad>MKADEM</soyad> <turKod>O</turKod> <ulkeKodu>276</ulkeKodu>");
            //sb.AppendLine("  </sigortaEttirenType> <tarifeID>8201</tarifeID>");

            //sb.AppendLine(" <uretimKaynakKod>1</uretimKaynakKod> <uretimKaynakKurumKod>0521536</uretimKaynakKurumKod>");

            //sb.AppendLine("  <vadeSayisi>1</vadeSayisi><yeniYenilemeGecis>1</yeniYenilemeGecis>");

            //sb.AppendLine(" <yenilemeNo>1</yenilemeNo><zeylNo>0</zeylNo>");

            //sb.AppendLine("  </pts:policeKontrol> </pts:policeKontrol> </soapenv:Body></soapenv:Envelope> ");




            //Stream requestStream = null;
            //Stream responseStream = null;
            //HttpWebRequest webRequest = null;
            //WebResponse webResponse = null;
            //StreamReader rd = null;
            //try
            //{
            //    WebProxy myproxy = new WebProxy("127.0.0.1", 9090);
            //    webRequest = (HttpWebRequest)WebRequest.Create("https://test.sbm.org.tr/SagmerPoliceOnlineWS-test-V1.0/PoliceOnlineService");
            //    webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            //    webRequest.Accept = "text/xml";
            //    webRequest.Proxy = myproxy;
            //    webRequest.Method = "POST";

            //    XmlDocument soapEnvelopeXml = new XmlDocument();
            //    soapEnvelopeXml.LoadXml(sb.ToString());

            //    requestStream = webRequest.GetRequestStream();
            //    soapEnvelopeXml.Save(requestStream);

            //    // begin async call to web request.
            //    IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

            //    // suspend this thread until call is complete. You might want to
            //    // do something usefull here like update your UI.
            //    asyncResult.AsyncWaitHandle.WaitOne();

            //    // get the response from the completed web request.

            //    webResponse = webRequest.EndGetResponse(asyncResult);
            //    rd = new StreamReader(webResponse.GetResponseStream());

            //    soapResult = rd.ReadToEnd();
            //}
            //catch (WebException ex)
            //{
            //    WebResponse errResp = ex.Response;
            //    using (Stream respStream = errResp.GetResponseStream())
            //    {
            //        StreamReader reader = new StreamReader(respStream);
            //        soapResult = reader.ReadToEnd();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    soapResult = ex.Message;
            //}
            //finally
            //{
            //    if (requestStream != null)
            //    {
            //        requestStream.Close();
            //        requestStream = null;
            //    }
            //    if (webResponse != null)
            //    {
            //        webResponse.Close();
            //        webResponse = null;
            //    }
            //    if (responseStream != null)
            //    {
            //        responseStream.Close();
            //        responseStream = null;
            //    }
            //    if (rd != null)
            //    {
            //        rd.Close();
            //        rd = null;
            //    }
            //}



            ////V_SgmPolicy sgmPol = new V_SgmPolicy
            ////{
            ////    OtorizasyonKod = "a2f522bc5f8ac9ae451a082dba95fe59",
            ////    SigortaSirketKod = "017",

            ////}




            ////AnkurSync();

            //string dfd = "0005";

            //Console.WriteLine(int.Parse(dfd).ToString());
            //Print print = new Print();

            //Console.Write(print.MethodName);
            //AnkurToJson();
            //InnerClass();
            //RxMediaQuery();
            //RxMediaQuery();
            //string WSUsername = "84855488";
            //string WSPassword = "G4eRqJhSgcC2MWW";

            //try
            //{
            //    RxMedia rxM = new RxMedia();
            //    rxM.IlacBarkod();
            //    string WSUsername = "84786555";
            //    string WSPassword = "a34BG9uuWnleH3C";

            //    RxMediaPharmaWSClient client = new RxMediaTest.RxMediaPharmaWSClient();

            //    userpass upass = new userpass();
            //    upass.username = WSUsername;
            //    upass.pass = WSPassword;

            //    mustahzarbul_barkod_Request reqIlacBul = new mustahzarbul_barkod_Request();
            //    arr_str ilacBarkodList = new arr_str();
            //    ilacBarkodList.Add("86995dd0419010634");
            //    reqIlacBul.barkodlar = ilacBarkodList;
            //    reqIlacBul.user = upass;

            //    mustahzarbul_barkod_Response respIlacBilgi = client.mustahzarbul_barkod(reqIlacBul);

            //    arr_mustahzar respArray = respIlacBilgi.yanit;
            //}catch(Exception ex){ }

        }
    }
}
