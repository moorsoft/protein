﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSScriptLibrary;
using csscript;
using System.Threading;
using System.Xml;
using Protein.Data;
using Protein.Common;


namespace ScheduleScript
{

    public class Deneme
    {
        public static void Main()
        {

            GetExchangeRatesFromTCMB();
            // Console.WriteLine(JsonConvert.SerializeObject(tst));
            // Console.WriteLine("Besim merhaba");
            Thread.Sleep(3000);
        }


        public static void GetExchangeRatesFromTCMB()
        {
            string today = "http://www.tcmb.gov.tr/kurlar/today.xml";


            List<string> currencyTypes = new List<string>
                                        {
                                            "USD","AUD","DKK","EUR","GBP","CHF","SEK","CAD","KWD","NOK","SAR","JPY","BGN","RON","RUB","IRR","CNY","PKR"
                                        };
            List<Protein.Common.Entities.ProteinEntities.ExchangeRate> lstExcRt = new List<Protein.Common.Entities.ProteinEntities.ExchangeRate>();

            var xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load(today);

                if (!string.IsNullOrEmpty(xmlDoc.InnerXml))
                {
                    foreach (string curType in currencyTypes)
                    {
                        string unit = xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='" + curType + "']/Unit").InnerXml;
                        decimal buying = Convert.ToDecimal(xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='" + curType + "']/BanknoteBuying").InnerXml.Replace(".", ","));
                        decimal selling = decimal.Parse(xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='" + curType + "']/BanknoteSelling").InnerXml.Replace(".", ","));
                        decimal forexBuying = decimal.Parse(xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='" + curType + "']/ForexBuying").InnerXml.Replace(".", ","));
                        decimal forexSelling = decimal.Parse(xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='" + curType + "']/ForexSelling").InnerXml.Replace(".", ","));

                        Protein.Common.Entities.ProteinEntities.ExchangeRate excRate = new Protein.Common.Entities.ProteinEntities.ExchangeRate();
                        //((excRate.BanknoteBuying = buying;
                        excRate.BanknoteSelling = selling;
                        excRate.BanknoteBuying = buying;
                        excRate.CurrencyType = curType;
                        excRate.ForexBuying = forexBuying;
                        excRate.ForexSelling = forexSelling;
                        excRate.Unit = unit;
                        excRate.RateDate = DateTime.Now;

                        try
                        {
                            Protein.Common.Entities.ProteinEntities.SpResponse responseExchangeRate = new Protein.Data.Repositories.ExchangeRateRepository().Insert(excRate);
                            if (responseExchangeRate.Code != "100")
                            {

                            }
                        }
                        catch { }

                    }
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            int s1 = 1;
        }
    }
}
