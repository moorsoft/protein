﻿using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.InsuranceCompanies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Protein.WS.WS.Common
{
    /// <summary>
    /// Summary description for ProxySrv
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ProxySrv : System.Web.Services.WebService
    {
        ProxyServiceClient serviceClient = new ProxyServiceClient();


        //Hasar İhbar
        [WebMethod]
        public CommonProxyRes ClaimTransfer(ClaimStateUpdateReq req)
        {
            return serviceClient.ClaimNotice(req);
        }

        //Hasar Teminat Güncelleme
        [WebMethod]
        public CommonProxyRes ClaimUpdate(ClaimStateUpdateReq req)
        {
            return serviceClient.ClaimUpdate(req);
        }

        //HASAR ONAY
        [WebMethod]
        public CommonProxyRes ClaimApproval(ClaimStateUpdateReq req)
        {
            return serviceClient.ClaimApproval(req);
        }

        //HASAR IPTAL
        [WebMethod]
        public CommonProxyRes ClaimCancellation(ClaimStateUpdateReq req)
        {
            return serviceClient.ClaimCancellation(req);
        }

        //HASAR RED
        [WebMethod]
        public CommonProxyRes ClaimRejection(ClaimStateUpdateReq req)
        {
            return serviceClient.ClaimRejection(req);
        }

        //HASAR BEKLEME
        [WebMethod]
        public CommonProxyRes ClaimWait(ClaimStateUpdateReq req)
        {
            return serviceClient.ClaimWait(req);
        }

        //HAK SAHİPLİĞİ SORGULA
        [WebMethod]
        public CommonProxyRes PolicyState(PolicyStateReq req)
        {
            return serviceClient.PolicyState(req);
        }

        //ICMALTRANSFER
        [WebMethod]
        public CommonProxyRes PayrollTransfer(PayrollTransferReq req)
        {
            return serviceClient.PayrollTransfer(req);
        }
        //KURUMTRANSFER
        [WebMethod]
        public CommonProxyRes ProviderTransfer(ProviderTransferReq req)
        {
            return serviceClient.ProviderTransfer(req);
        }
        //KURUMTRANSFER
        [WebMethod]
        public CommonProxyRes ProviderListTransfer(ProviderListTransferReq req)
        {
            return serviceClient.ProviderListTransfer(req);
        }
    }
}
