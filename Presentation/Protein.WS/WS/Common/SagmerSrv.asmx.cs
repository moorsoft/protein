﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using Protein.Common.Extensions;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.Common.Lib.SBM;
using Protein.Data.ExternalServices.SagmerOnlineService;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;
using static Protein.Data.ExternalServices.SagmerOnlineService.Enums;
using static Protein.Data.ExternalServices.SagmerOnlineService.InputOutputTypes;

namespace Protein.WS.WS.Common
{
    /// <summary>
    /// Summary description for SagmerSrv
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class SagmerSrv : System.Web.Services.WebService
    {
        [WebMethod]
        public SbmServiceRes police(SbmPolicyCreateReq req, SagmerServiceType ServiceType)
        {
            SgmServiceClient sgmServiceClient = new SgmServiceClient();
            return sgmServiceClient.SbmPolicyCreate(req, ServiceType);

            //// Sigortalılar için... giriş zeyli çalışılıyor.
            //serviceClient = new ServiceClient();
            //ServiceResponse<policeSonucType> response = new ServiceResponse<policeSonucType>();

            //#region ApiCode Kontrol
            //if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //#endregion

            //#region IsSagmerSendViaPolicyViaPolicy
            //SagmerHelper sagmerHelper = new SagmerHelper();
            //if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            //{
            //    response.Data.aciklama = "Sagmer Gönderilecek Poliçe Bulunamadı";
            //    response.Data.islemBasarili = false;
            //    return response;
            //}
            //#endregion

            //policeInputType input = new policeInputType();

            //#region FillPolicyDetails
            //V_SgmPolicy sgmPolicy = new GenericRepository<V_SgmPolicy>().FindBy($"POLICY_ID = {PolicyId}", fetchDeletedRows: true, fetchHistoricRows: true, orderby: "").FirstOrDefault();
            //#endregion

            //#region Policy Check
            //if (sgmPolicy == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Poliçe detayı bulunamadı";
            //    return response;
            //}
            //#endregion
            //#region Fill Fields
            //try
            //{
            //    input.branshKod = sgmPolicy.BranshCode.IsInt64() ? long.Parse(sgmPolicy.BranshCode) : 0;
            //    input.dovizCinsi = sgmPolicy.DovizCinsi;
            //    input.endirektSigortaSirketKod = sgmPolicy.EndirektSigortaSirketKod;
            //    input.eskiPoliceNo = sgmPolicy.EskiPoliceNo;
            //    input.eskiYenilemeNo = !sgmPolicy.EskiYenilemeNo.IsInt() ? null : (int?)int.Parse(sgmPolicy.EskiYenilemeNo);
            //    input.ilKod = sgmPolicy.IlKod;
            //    input.odemeKod = !sgmPolicy.OdemeKod.IsInt64() ? 0 : long.Parse(sgmPolicy.OdemeKod);
            //    input.otorizasyonKod = sgmPolicy.OtorizasyonKod;
            //    input.policeBaslamaTarihi = sgmPolicy.PoliceBaslamaTarihi.IsDateTime() ? DateTime.Parse(sgmPolicy.PoliceBaslamaTarihi) : throw new Exception("PoliceBaslamaTarihi Boş!");
            //    input.policeBitisTarihi = sgmPolicy.PoliceBitisTarihi.IsDateTime() ? DateTime.Parse(sgmPolicy.PoliceBitisTarihi) : throw new Exception("PoliceBitisTarihi Boş!");
            //    input.policeBrutPrimi = sgmPolicy.PoliceBrutPrimi.IsNumeric() ? double.Parse(sgmPolicy.PoliceBrutPrimi) : 0;
            //    input.policeNetPrimi = sgmPolicy.PoliceNetPrimi.IsNumeric() ? double.Parse(sgmPolicy.PoliceNetPrimi) : 0;
            //    input.policeNo = sgmPolicy.PoliceNo;
            //    input.policeTanzimTarihi = !sgmPolicy.PoliceTanzimTarihi.IsDateTime() ? null : (DateTime?)DateTime.Parse(sgmPolicy.PoliceTanzimTarihi);
            //    input.policeTip = sgmPolicy.PoliceTip.IsInt() ? (sgmPolicy.PoliceTip == ((int)PolicyType.FERDI).ToString() ? policeTipiType.F : policeTipiType.G) : throw new Exception("PoliceTip Boş!");

            //    input.sigortaEttirenType = new sigortaEttirenInputType();
            //    input.sigortaEttirenType.kimlikTipKod = sgmPolicy.SgtKimlikTipKod;
            //    if (input.sigortaEttirenType.kimlikTipKod != "1" && input.sigortaEttirenType.kimlikTipKod != "2" && input.sigortaEttirenType.kimlikTipKod != "4")
            //    {
            //        input.sigortaEttirenType.ad = sgmPolicy.SgtAd;
            //        input.sigortaEttirenType.soyad = sgmPolicy.SgtSoyad;
            //        input.sigortaEttirenType.adres = sgmPolicy.SgtAdres;
            //        input.sigortaEttirenType.babaAdi = sgmPolicy.SgtBabaAdi;
            //        input.sigortaEttirenType.cinsiyet = !sgmPolicy.SgtCinsiyet.IsInt() ? null : (cinsiyet?)Enum.Parse(typeof(cinsiyet), sgmPolicy.SgtCinsiyet);
            //        input.sigortaEttirenType.dogumTarihi = !sgmPolicy.SgtDogumTarihi.IsDateTime() ? null : (DateTime?)DateTime.Parse(sgmPolicy.SgtDogumTarihi);
            //        input.sigortaEttirenType.dogumYeri = sgmPolicy.SgtDogumYeri;
            //    }
            //    input.sigortaEttirenType.kimlikNo = sgmPolicy.SgtKimlikNo;
            //    input.sigortaEttirenType.sigortaEttirenUyruk = sgmPolicy.SgtSigortaEttirenUyruk == null ? null : (uyrukTuru?)(uyrukTuru)Enum.Parse(typeof(uyrukTuru), sgmPolicy.SgtSigortaEttirenUyruk);

            //    DateTime? kurulusTarihi = null;
            //    if (!string.IsNullOrEmpty(sgmPolicy.SgtKurulusTarihi))
            //        kurulusTarihi = DateTime.Parse(sgmPolicy.SgtKurulusTarihi);

            //    input.sigortaEttirenType.kurulusTarihi = kurulusTarihi;
            //    input.sigortaEttirenType.kurulusYeri = sgmPolicy.SgtKurulusYeri;
            //    input.sigortaEttirenType.turKod = sgmPolicy.SgtTurKod != null ? (turKod)Enum.Parse(typeof(turKod), sgmPolicy.SgtTurKod) : throw new Exception("SgtTurKod Boş");
            //    if (input.sigortaEttirenType.kimlikTipKod != "1")
            //    {
            //        input.sigortaEttirenType.ulkeKodu = !sgmPolicy.SgtUlkeKodu.IsInt() ? null : (int?)int.Parse(sgmPolicy.SgtUlkeKodu);
            //    }
            //    input.sigortaSirketKod = sgmPolicy.SigortaSirketKod;
            //    input.tarifeID = !sgmPolicy.TarifeId.IsInt64() ? 0 : long.Parse(sgmPolicy.TarifeId);
            //    input.uretimKaynakKod = !sgmPolicy.UretimKaynakKod.IsInt64() ? 0 : long.Parse(sgmPolicy.UretimKaynakKod);
            //    input.uretimKaynakKurumKod = sgmPolicy.UretimKaynakKurumKod;
            //    input.vadeSayisi = !sgmPolicy.VadeSayisi.IsInt() ? 0 : int.Parse(sgmPolicy.VadeSayisi);
            //    input.yenilemeNo = !sgmPolicy.YenilemeNo.IsInt() ? 0 : int.Parse(sgmPolicy.YenilemeNo);
            //    input.yeniYenilemeGecis = sgmPolicy.YeniYenilemeGecis;
            //    input.zeylNo = !sgmPolicy.ZeylNo.IsInt() ? 0 : int.Parse(sgmPolicy.ZeylNo);
            //}
            //catch (Exception ex)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = ex.ToString();
            //    return response;
            //}
            //#endregion

            //if (ServiceType == SagmerServiceType.KONTROL)
            //    return serviceClient.policeKontrol(input, long.Parse(sgmPolicy.CompanyId));
            //else return serviceClient.police(input, long.Parse(sgmPolicy.CompanyId));
        }
        [WebMethod]
        public SbmServiceRes sigortaliGirisZeyli(SbmInsuredEntranceReq req, SagmerServiceType ServiceType)
        {
            SgmServiceClient sgmServiceClient = new SgmServiceClient();
            return sgmServiceClient.SbmInsuredEntranceEndorsement(req, ServiceType);
            //serviceClient = new ServiceClient();
            //ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            //#region ApiCode Kontrol
            //if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //#endregion
            //#region IsSagmerSendViaPolicy
            //SagmerHelper sagmerHelper = new SagmerHelper();
            //if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            //{
            //    response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
            //    response.Data.islemBasarili = false;
            //    return response;
            //}
            //#endregion
            //sigortaliGirisInputType input = new sigortaliGirisInputType();
            ////input.policeNetPrimi
            //#region FillEndorsDetails
            //V_SgmIngressEndorsement sgmEndors = new GenericRepository<V_SgmIngressEndorsement>().FindBy($" ENDORSEMENT_ID = {EndorsementId} AND POLICY_ID = {PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            //List<V_SgmIngressEndorsInsured> sgmEndorsInsured = new GenericRepository<V_SgmIngressEndorsInsured>().FindBy($" ENDORSEMENT_ID = {EndorsementId} AND POLICY_ID = {PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            //#endregion
            //#region Endors Check
            //if (sgmEndors == null || sgmEndorsInsured.Count < 1)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Zeyl detayı bulunamadı";
            //    return response;
            //}
            //#endregion
            //#region Fill Fields
            //input.zeylTuru = long.Parse(sgmEndors.ZeylTuru);
            //input.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);
            //input.zeylNo = int.Parse(sgmEndors.ZeylNo);
            //input.zeylNetPrimi = double.Parse(sgmEndors.ZeylNetPrimi);
            //input.zeylBrutPrimi = double.Parse(sgmEndors.ZeylBrutPrimi);
            //input.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
            //input.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
            //input.sigortaSirketKod = sgmEndors.SigortaSirketKod;
            //input.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
            //input.otorizasyonKod = sgmEndors.OtorizasyonKod;
            //input.policeNo = sgmEndors.PoliceNo;
            //input.policeNetPrimi = double.Parse(sgmEndors.PoliceNetPrimi);
            //input.policeBrutPrimi = double.Parse(sgmEndors.PoliceBrutPrimi);
            //List<sigortaliTeminatliInputType> lstInsured = new List<sigortaliTeminatliInputType>();
            //foreach (V_SgmIngressEndorsInsured item in sgmEndorsInsured)
            //{
            //    List<V_SgmIngressInsCover> sgmInsuredCover = new GenericRepository<V_SgmIngressInsCover>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {PolicyId} and ENDORSEMENT_ID = {EndorsementId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            //    V_SgmIngressInsHealthInfo healthInfo = new GenericRepository<V_SgmIngressInsHealthInfo>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {PolicyId} and ENDORSEMENT_ID = {EndorsementId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            //    List<V_SgmIngressInsHealthDet> healthInfoDet = new GenericRepository<V_SgmIngressInsHealthDet>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {PolicyId} and ENDORSEMENT_ID = {EndorsementId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            //    List<V_SgmIngressExtraPremium> ExtraPrm = new GenericRepository<V_SgmIngressExtraPremium>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {PolicyId} and ENDORSEMENT_ID = {EndorsementId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            //    List<V_SgmIngressInsExemption> sgmExemption = new GenericRepository<V_SgmIngressInsExemption>().FindBy($"INSURED_ID = {item.InsuredId} and POLICY_ID = {PolicyId} and ENDORSEMENT_ID = {EndorsementId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

            //    sigortaliSaglikBilgileriInputType sigSagBilg = new sigortaliSaglikBilgileriInputType();
            //    List<sigortaliSaglikBilgileriDetayInputType> sigSagBilgDetay = new List<sigortaliSaglikBilgileriDetayInputType>();

            //    List<sigortaliIndirimEkprimInputType> sigIndEkp = new List<sigortaliIndirimEkprimInputType>();

            //    List<sigortaliTeminatInputType> sigTem = new List<sigortaliTeminatInputType>();
            //    List<sigortaliUygulamaInputType> sigUyg = new List<sigortaliUygulamaInputType>();

            //    if (healthInfo != null)
            //    {
            //        sigSagBilg.alkolKullaniyorMu = (Enums.evetHayirType)int.Parse(healthInfo.AlkolKullaniliyorMu);
            //        sigSagBilg.alkolKullanmaSuresi = healthInfo.AlkolKullanmaSuresi;
            //        sigSagBilg.alkolPeriyotTip = (Enums.aliskanlikPeriyodType)int.Parse(healthInfo.AlkolPeriyotTip);
            //        sigSagBilg.boy = int.Parse(healthInfo.Boy);
            //        sigSagBilg.kilo = int.Parse(healthInfo.Kilo);
            //        sigSagBilg.periyottakiAlkolMiktari = healthInfo.PeriyottakiAlkolMiktari;
            //        sigSagBilg.periyottakiSigaraAdedi = healthInfo.PeriyottakiSigaraAdedi;
            //        sigSagBilg.sigaraIciyorMu = (Enums.evetHayirType)int.Parse(healthInfo.SigaraIciyorMu);
            //        sigSagBilg.sigaraIcmeSuresi = healthInfo.SigaraIcmeSuresi;
            //        sigSagBilg.sigaraPeriyotTip = (Enums.aliskanlikPeriyodType)int.Parse(healthInfo.SigaraperiyotTip);
            //    }

            //    foreach (V_SgmIngressInsHealthDet itemHealthDet in healthInfoDet)
            //    {
            //        sigSagBilgDetay.Add(new sigortaliSaglikBilgileriDetayInputType
            //        {
            //            ameliyatVarmi = itemHealthDet.AmeliyatVarMi,
            //            hastalikAciklamasi = itemHealthDet.HastalikAciklamasi,
            //            hastalikAdi = itemHealthDet.HastalikAdi,
            //            hastalikKod = itemHealthDet.HastalikKod,
            //            hastalikUygulamaKod = itemHealthDet.HastalikUygulamaKod.IsInt64() ? long.Parse(itemHealthDet.HastalikUygulamaKod) : 0,
            //            sonDurum = itemHealthDet.SonDurum,
            //            sureAy = itemHealthDet.SureAy.IsInt() ? int.Parse(itemHealthDet.SureAy) : 0,
            //            sureYil = itemHealthDet.SureYil.IsInt() ? int.Parse(itemHealthDet.SureYil) : 0,
            //            uygulamaAciklamasi = itemHealthDet.UygulamaAciklamasi
            //        });
            //    }
            //    sigSagBilg.saglikDetayBilgileri = sigSagBilgDetay.ToArray();
            //    foreach (V_SgmIngressExtraPremium itemExtraPrm in ExtraPrm)
            //    {
            //        sigIndEkp.Add(new sigortaliIndirimEkprimInputType
            //        {
            //            aciklama = itemExtraPrm.Aciklama,
            //            indirimEkprimDegeri = itemExtraPrm.IndirimEkPrimDegeri.IsNumeric() ? double.Parse(itemExtraPrm.IndirimEkPrimDegeri) : 0,
            //            indirimEkprimKod = itemExtraPrm.IndirimEkPrimKod.IsInt64() ? long.Parse(itemExtraPrm.IndirimEkPrimKod) : 0,
            //            kayitNo = itemExtraPrm.KayitNo.IsInt64() ? long.Parse(itemExtraPrm.KayitNo) : 0,
            //            tutarMiOranMi = itemExtraPrm.TutarMiOranMi
            //        });
            //    }
            //    foreach (V_SgmIngressInsCover itemInsCover in sgmInsuredCover)
            //    {
            //        sigTem.Add(new sigortaliTeminatInputType
            //        {
            //            aso = itemInsCover.Aso.IsInt() ? (Enums.evetHayirType)int.Parse(itemInsCover.Aso) : evetHayirType.H,
            //            cografiKapsamKodu = itemInsCover.CografiKapsamKodu.IsInt64() ? long.Parse(itemInsCover.CografiKapsamKodu) : 0,
            //            donemBaslangicTarihi = itemInsCover.DonemBaslangicTarihi.IsDateTime() ? DateTime.Parse(itemInsCover.DonemBaslangicTarihi) : DateTime.Now,
            //            donemBitisTarihi = itemInsCover.DonemBitisTarihi.IsDateTime() ? DateTime.Parse(itemInsCover.DonemBitisTarihi) : DateTime.Now,
            //            donemKodu = itemInsCover.DonemKodu.IsInt() ? int.Parse(itemInsCover.DonemKodu) : 0,
            //            dovizCinsi = itemInsCover.DovizCinsi,
            //            teminatAdedi = itemInsCover.TeminatAdedi.IsInt() ? int.Parse(itemInsCover.TeminatAdedi) : 0,
            //            teminatKodu = itemInsCover.TeminatKodu.IsInt64() ? long.Parse(itemInsCover.TeminatKodu) : 0,
            //            teminatLimiti = itemInsCover.TeminatLimiti.IsNumeric() ? double.Parse(itemInsCover.TeminatLimiti) : 0,
            //            teminatLimitKodu = itemInsCover.TeminatLimitKodu.IsInt64() ? long.Parse(itemInsCover.TeminatLimitKodu) : 0,
            //            teminatMuafiyeti = itemInsCover.TeminatMuafiyeti.IsNumeric() ? double.Parse(itemInsCover.TeminatMuafiyeti) : 0,
            //            teminatMuafiyetKodu = itemInsCover.TeminatMuafiyetKodu.IsInt64() ? long.Parse(itemInsCover.TeminatMuafiyetKodu) : 0,
            //            teminatNetPrim = itemInsCover.TeminatNetPrim.IsNumeric() ? double.Parse(itemInsCover.TeminatNetPrim) : 0,
            //            teminatOdemeYuzdesi = itemInsCover.TeminatOdemeYuzdesi.IsNumeric() ? double.Parse(itemInsCover.TeminatOdemeYuzdesi) : 0,
            //            teminatVaryasyonKodu = itemInsCover.TeminatVaryasyonKodu.IsInt() ? int.Parse(itemInsCover.TeminatVaryasyonKodu) : 0,
            //            toplamDonemSayisi = itemInsCover.ToplamDonemSayisi.IsInt() ? int.Parse(itemInsCover.ToplamDonemSayisi) : 0,
            //        });
            //    }
            //    foreach (V_SgmIngressInsExemption itemExemption in sgmExemption)
            //    {
            //        sigUyg.Add(new sigortaliUygulamaInputType
            //        {
            //            aciklama = itemExemption.Aciklama,
            //            hastalikKod = itemExemption.HastalikKod,
            //            kayitNo = itemExemption.KayitNo.IsInt64() ? long.Parse(itemExemption.KayitNo) : 0,
            //            muafiyetTutarLimiti = itemExemption.MuafiyetTutarLimiti.IsNumeric() ? double.Parse(itemExemption.MuafiyetTutarLimiti) : 0,
            //            sigortaliUygulamaKod = itemExemption.SigortaliUygulamaKod.IsInt64() ? long.Parse(itemExemption.SigortaliUygulamaKod) : 0,
            //        });
            //    }


            //    var sigInput = new sigortaliTeminatliInputType
            //    {
            //        aileBagNo = item.AileBagNo,
            //        bireySiraNo = item.BireySiraNo,
            //        bireyTipKod = item.BireyTipKod.IsInt64() ? long.Parse(item.BireyTipKod) : 0,
            //        brutPrim = item.BrutPrim.IsNumeric() ? double.Parse(item.BrutPrim) : 0,
            //        eskiPoliceNo = item.EskiPoliceNo,
            //        eskiYenilemeNo = item.EskiYenilemeNo.IsInt() ? int.Parse(item.EskiYenilemeNo) : 0,
            //        ilkOzelSigortalilikTarihi = DateTime.Parse(item.IlkOzelSigortalilikTarihi),
            //        ilkOzelSirketKod = item.IlkOzelSirketKod,
            //        kimlikNo = item.KimlikNo,
            //        kimlikTipKod = item.KimlikTipKod,
            //        meslekKod = item.MeslekKod.IsInt64() ? long.Parse(item.MeslekKod) : 0,
            //        netPrim = item.NetPrim.IsNumeric() ? double.Parse(item.NetPrim) : 0,
            //        sigortaliUyruk = item.SigortaliUyruk == "TC" ? uyrukTuru.TC : uyrukTuru.YABANCI,// (Enums.uyrukTuru)int.Parse(item.SigortaliUyruk),
            //        ulkeKodu = item.UlkeKodu.IsInt() ? int.Parse(item.UlkeKodu) : 0,
            //        urunPlanKod = item.UrunPlanKod.IsInt64() ? long.Parse(item.UrunPlanKod) : 0,
            //        yasadigiIlceKod = item.YasadigiIlceKod,
            //        yasadigiIlKod = item.YasadigiIlKod,
            //        saglikBilgileriInputType = sigSagBilg,
            //        sigortaliIndirimEkprimInputTypeList = sigIndEkp.ToArray(),
            //        sigortaliTeminatInputTypeList = sigTem.ToArray(),
            //        sigortaliUygulamaList = sigUyg.ToArray()
            //    };

            //    if (item.KimlikTipKod != "1" && item.KimlikTipKod != "2" && item.KimlikTipKod != "4")
            //    {
            //        sigInput.adres = item.Adres;
            //        sigInput.babaAdi = item.BabaAdi;
            //        sigInput.sigortaliAd = item.SigortaliAd;
            //        sigInput.sigortaliSoyad = item.SigortaliSoyad;
            //        sigInput.cinsiyet = (Enums.cinsiyet)int.Parse(item.Cinsiyet);
            //        sigInput.dogumTarihi = DateTime.Parse(item.DogumTarihi);
            //        sigInput.dogumYeri = item.DogumYeri;
            //    }

            //    lstInsured.Add(sigInput);

            //}
            //input.sigortali = lstInsured.ToArray();
            //#endregion
            //if (ServiceType == SagmerServiceType.KONTROL)
            //    return serviceClient.sigortaliGirisZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
            //else
            //    return serviceClient.sigortaliGirisZeyli(input, long.Parse(sgmEndors.CompanyId));

        }
        [WebMethod]
        public SbmServiceRes sigortaliCikisZeyli(SbmInsuredExitReq req, SagmerServiceType ServiceType)
        {
            SgmServiceClient sgmServiceClient = new SgmServiceClient();
            return sgmServiceClient.SbmInsuredExitEndorsement(req, ServiceType);
            //serviceClient = new ServiceClient();
            //ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            //#region ApiCode Kontrol
            //if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //#endregion
            //#region IsSagmerSendViaPolicy
            //SagmerHelper sagmerHelper = new SagmerHelper();
            //if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            //{
            //    response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
            //    response.Data.islemBasarili = false;
            //    return response;
            //}
            //#endregion
            //#region FillEndorsDetails
            //V_SgmEgressEndorsement sgmEndors = new GenericRepository<V_SgmEgressEndorsement>().FindBy($"ENDORSEMENT_ID = {EndorsementId} AND POLICY_ID = {PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            //#endregion
            //#region Endors Check
            //if (sgmEndors == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Zeyl detayı bulunamadı";
            //    return response;
            //}
            //#endregion
            //#region FillFields
            //sigortaliCikisInputType input = new sigortaliCikisInputType
            //{
            //    brutPrim = sgmEndors.BrutPrim.IsNumeric() ? double.Parse(sgmEndors.BrutPrim) : 0,
            //    kimlikNo = sgmEndors.KimlikNo,
            //    kimlikTipKod = sgmEndors.KimlikTipKod.IsInt() ? sgmEndors.KimlikTipKod : "",
            //    netPrim = sgmEndors.NetPrim.IsNumeric() ? double.Parse(sgmEndors.NetPrim) : 0,
            //    otorizasyonKod = sgmEndors.OtorizasyonKod,
            //    policeBrutPrimi = sgmEndors.PoliceBrutPrimi.IsNumeric() ? double.Parse(sgmEndors.PoliceBrutPrimi) : 0,
            //    policeNetPrimi = sgmEndors.PoliceNetPrimi.IsNumeric() ? double.Parse(sgmEndors.PoliceNetPrimi) : 0,
            //    policeNo = sgmEndors.PoliceNo,
            //    sbmSagmerNo = sgmEndors.SbmSagmerNo.IsInt64() ? long.Parse(sgmEndors.SbmSagmerNo) : 0,
            //    sigortaSirketKod = sgmEndors.SigortaSirketKod,
            //    yenilemeNo = sgmEndors.YenilemeNo.IsInt() ? int.Parse(sgmEndors.YenilemeNo) : 0,
            //    zeylBaslangicTarihi = sgmEndors.ZeylBaslangicTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylBaslangicTarihi) : DateTime.Now,
            //    zeylBrutPrimi = sgmEndors.ZeylBrutPrimi.IsNumeric() ? double.Parse(sgmEndors.ZeylBrutPrimi) : 0,
            //    zeylNetPrimi = sgmEndors.ZeylNetPrimi.IsNumeric() ? double.Parse(sgmEndors.ZeylNetPrimi) : 0,
            //    zeylNo = sgmEndors.ZeylNo.IsInt() ? int.Parse(sgmEndors.ZeylNo) : 0,
            //    zeylTanzimTarihi = sgmEndors.ZeylTanzimTarihi.IsDateTime() ? DateTime.Parse(sgmEndors.ZeylTanzimTarihi) : DateTime.Now,
            //};

            //#endregion

            //if (ServiceType == SagmerServiceType.KONTROL)
            //    return serviceClient.sigortaliCikisZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
            //else
            //    return serviceClient.sigortaliCikisZeyli(input, long.Parse(sgmEndors.CompanyId));
        }
        [WebMethod]
        public SbmServiceRes sigortaliTahakkukZeyli(SbmInsuredAccrualReq req, SagmerServiceType ServiceType)
        {
            SgmServiceClient sgmServiceClient = new SgmServiceClient();
            return sgmServiceClient.SbmInsuredAccrualEndorsement(req, ServiceType);
            //serviceClient = new ServiceClient();
            //ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            //#region ApiCode Kontrol
            //if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //#endregion
            //#region IsSagmerSendViaPolicy
            //SagmerHelper sagmerHelper = new SagmerHelper();
            //if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            //{
            //    response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
            //    response.Data.islemBasarili = false;
            //    return response;
            //}
            //#endregion
            //sigortaliPrimFarkiInputType input = new sigortaliPrimFarkiInputType();

            //#region FillEndorsDetails
            //V_SgmEndorsInsuredPremium sgmEndors = new GenericRepository<V_SgmEndorsInsuredPremium>().FindBy($" INSURED_ID = '{InsuredId}' and ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            //#endregion
            //#region Endors Check
            //if (sgmEndors == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Zeyl detayı bulunamadı";
            //    return response;
            //}
            //#endregion
            //#region FillFields
            //input.brutPrim = double.Parse(sgmEndors.BrutPrim);
            //input.kimlikNo = sgmEndors.KimlikNo;
            //input.kimlikTipKod = sgmEndors.KimlikTipi;
            //input.netPrim = double.Parse(sgmEndors.NetPrim);
            //input.otorizasyonKod = sgmEndors.OtorizasyonKod;
            //input.policeBrutPrimi = double.Parse(sgmEndors.PoliceBrutPrimi);
            //input.policeNetPrimi = double.Parse(sgmEndors.PoliceNetPrimi);
            //input.policeNo = sgmEndors.PoliceNo;
            //input.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
            //input.sigortaSirketKod = sgmEndors.SigortaSirketKod;
            //input.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
            //input.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
            //input.zeylBrutPrimi = double.Parse(sgmEndors.ZeylBrutPrimi);
            //input.zeylNetPrimi = double.Parse(sgmEndors.ZeylNetPrimi);
            //input.zeylNo = int.Parse(sgmEndors.ZeylNo);
            //input.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);
            //#endregion

            //if (ServiceType == SagmerServiceType.KONTROL)
            //    return serviceClient.sigortaliTahakkukZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
            //else
            //    return serviceClient.sigortaliTahakkukZeyli(input, long.Parse(sgmEndors.CompanyId));
        }
        [WebMethod]
        public SbmServiceRes policeTahakkukVePrimFarkiZeyli(SbmPolicyAccrualReq req, SagmerServiceType ServiceType)
        {
            SgmServiceClient sgmServiceClient = new SgmServiceClient();
            return sgmServiceClient.SbmPolicyAccrualEndorsement(req, ServiceType);
            //serviceClient = new ServiceClient();
            //ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            //#region ApiCode Kontrol
            //if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //#endregion
            //#region IsSagmerSendViaPolicy
            //SagmerHelper sagmerHelper = new SagmerHelper();
            //if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            //{
            //    response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
            //    response.Data.islemBasarili = false;
            //    return response;
            //}
            //#endregion
            //primFarkiInputType input = new primFarkiInputType();

            //#region FillEndorsDetails
            //V_SgmEndorsPolicyPremium sgmPremium = new GenericRepository<V_SgmEndorsPolicyPremium>().FindBy($" ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            //#endregion
            //#region Endors Check
            //if (sgmPremium == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Zeyl detayı bulunamadı";
            //    return response;
            //}
            //#endregion
            //#region FillFields
            //input.otorizasyonKod = sgmPremium.OtorizasyonKod;
            //input.policeBrutPrimi = double.Parse(sgmPremium.PoliceBrutPrimi);
            //input.policeNetPrimi = double.Parse(sgmPremium.PoliceNetPrimi);
            //input.policeNo = sgmPremium.PoliceNo;
            //input.sbmSagmerNo = long.Parse(sgmPremium.SbmSagmerNo);
            //input.sigortaSirketKod = sgmPremium.SigortaSirketKod;
            //input.yenilemeNo = int.Parse(sgmPremium.YenilemeNo);
            //input.zeylBaslangicTarihi = DateTime.Parse(sgmPremium.ZeylBaslangicTarihi);
            //input.zeylBrutPrimi = double.Parse(sgmPremium.ZeylBrutPrimi);
            //input.zeylNetPrimi = double.Parse(sgmPremium.ZeylNetPrimi);
            //input.zeylNo = int.Parse(sgmPremium.ZeylNo);
            //input.zeylTanzimTarihi = DateTime.Parse(sgmPremium.ZeylTanzimTarihi);
            //input.zeylTipi = int.Parse(sgmPremium.ZeylTipi);

            //#endregion

            //if (ServiceType == SagmerServiceType.KONTROL)
            //    return serviceClient.policeTahakkukVePrimFarkiZeyliKontrol(input, long.Parse(sgmPremium.CompanyId));
            //else
            //    return serviceClient.policeTahakkukVePrimFarkiZeyli(input, long.Parse(sgmPremium.CompanyId));
        }
        [WebMethod]
        public SbmServiceRes sigortaEttirenDegisiklikZeyli(SbmInsurerChangeReq req, SagmerServiceType ServiceType)
        {
            SgmServiceClient sgmServiceClient = new SgmServiceClient();
            return sgmServiceClient.SbmInsurerChangeEndorsement(req, ServiceType);
            //serviceClient = new ServiceClient();
            //ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            //#region ApiCode Kontrol
            //if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //#endregion
            //#region IsSagmerSendViaPolicy
            //SagmerHelper sagmerHelper = new SagmerHelper();
            //if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            //{
            //    response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
            //    response.Data.islemBasarili = false;
            //    return response;
            //}
            //#endregion
            //sigortaEttirenZeylInputType input = new sigortaEttirenZeylInputType();
            //#region FillEndorsDetails
            //V_SgmEndorsPolicyInsurer sgmInsurer = new GenericRepository<V_SgmEndorsPolicyInsurer>().FindBy($" ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            //#endregion
            //#region Endors Check
            //if (sgmInsurer == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Zeyl detayı bulunamadı";
            //    return response;
            //}
            //#endregion
            //#region FillFields
            //input.ad = sgmInsurer.Ad;
            //input.adres = sgmInsurer.Adres;
            //input.babaAdi = sgmInsurer.BabaAdi;
            //input.cinsiyet = ((Enums.cinsiyet)int.Parse(sgmInsurer.Cinsiyet));
            //input.dogumTarihi = DateTime.Parse(sgmInsurer.DogumTarihi);
            //input.dogumYeri = sgmInsurer.DogumYeri;
            //input.kimlikNo = sgmInsurer.KimlikNo;
            //input.kimlikTipKod = sgmInsurer.KimlikTipKod;
            //input.kurulusTarihi = DateTime.Parse(sgmInsurer.KurulusTarihi);
            //input.kurulusYeri = sgmInsurer.KurulusYeri;
            //input.otorizasyonKod = sgmInsurer.OtorizasyonKod;
            //input.policeBrutPrimi = double.Parse(sgmInsurer.PoliceBrutPrimi);
            //input.policeNetPrimi = double.Parse(sgmInsurer.PoliceNetPrimi);
            //input.policeNo = sgmInsurer.PoliceNo;
            //input.sbmSagmerNo = long.Parse(sgmInsurer.SbmSagmerNo);
            //input.sigortaSirketKod = sgmInsurer.SigortaSirketKod;
            //input.turKod = ((Enums.turKod)int.Parse(sgmInsurer.TurKod));
            //input.ulkeKodu = int.Parse(sgmInsurer.UlkeKodu);
            //input.yenilemeNo = int.Parse(sgmInsurer.YenilemeNo);
            //input.zeylBaslangicTarihi = DateTime.Parse(sgmInsurer.ZeylBaslangicTarihi);
            //input.zeylBrutPrimi = double.Parse(sgmInsurer.ZeylBrutPrimi);
            //input.zeylNetPrimi = double.Parse(sgmInsurer.ZeylNetPrimi);
            //input.zeylNo = int.Parse(sgmInsurer.ZeylNo);
            //input.zeylTanzimTarihi = DateTime.Parse(sgmInsurer.ZeylTanzimTarihi);
            //#endregion
            //if (ServiceType == SagmerServiceType.KONTROL)
            //    return serviceClient.sigortaEttirenDegisiklikZeyliKontrol(input, long.Parse(sgmInsurer.CompanyId));
            //else
            //    return serviceClient.sigortaEttirenDegisiklikZeyli(input, long.Parse(sgmInsurer.CompanyId));
        }
        [WebMethod]
        public SbmServiceRes uretimKaynakDegisiklikZeyli(SbmAgencyChangeReq req, SagmerServiceType ServiceType)
        {
            SgmServiceClient sgmServiceClient = new SgmServiceClient();
            return sgmServiceClient.SbmAgencyChangeEndorsement(req, ServiceType);

            //serviceClient = new ServiceClient();
            //ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            //#region ApiCode Kontrol
            //if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //#endregion
            //#region IsSagmerSendViaPolicy
            //SagmerHelper sagmerHelper = new SagmerHelper();
            //if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            //{
            //    response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
            //    response.Data.islemBasarili = false;
            //    return response;
            //}
            //#endregion
            //uretimKaynakInputType input = new uretimKaynakInputType();
            //#region FillEndorsDetails
            //V_SgmEndorsPolicyPremium sgmEndors = new GenericRepository<V_SgmEndorsPolicyPremium>().FindBy($" ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            //#endregion
            //#region Endors Check
            //if (sgmEndors == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Zeyl detayı bulunamadı";
            //    return response;
            //}
            //#endregion
            //input.otorizasyonKod = sgmEndors.OtorizasyonKod;
            //input.policeBrutPrimi = double.Parse(sgmEndors.PoliceBrutPrimi);
            //input.policeNetPrimi = double.Parse(sgmEndors.PoliceNetPrimi);
            //input.policeNo = sgmEndors.PoliceNo;
            //input.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
            //input.sigortaSirketKod = sgmEndors.SigortaSirketKod;
            //input.uretimKaynakKod = long.Parse(sgmEndors.UretimKaynakKod);
            //input.uretimKaynakKurumKod = sgmEndors.UretimKaynakKod;
            //input.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
            //input.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
            //input.zeylBrutPrimi = double.Parse(sgmEndors.ZeylBrutPrimi);
            //input.zeylNetPrimi = double.Parse(sgmEndors.ZeylNetPrimi);
            //input.zeylNo = int.Parse(sgmEndors.ZeylNo);
            //input.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);


            //if (ServiceType == SagmerServiceType.KONTROL)
            //    return serviceClient.uretimKaynakDegisiklikZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
            //else
            //    return serviceClient.uretimKaynakDegisiklikZeyli(input, long.Parse(sgmEndors.CompanyId));
        }
        [WebMethod]
        public SbmServiceRes genelIptalZeyli(SbmPolicyCancelReq req, SagmerServiceType ServiceType)
        {
            SgmServiceClient sgmServiceClient = new SgmServiceClient();
            return sgmServiceClient.SbmPolicyCancelEndorsement(req, ServiceType);
            //serviceClient = new ServiceClient();
            //ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            //#region ApiCode Kontrol
            //if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //#endregion
            //#region IsSagmerSendViaPolicy
            //SagmerHelper sagmerHelper = new SagmerHelper();
            //if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            //{
            //    response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
            //    response.Data.islemBasarili = false;
            //    return response;
            //}
            //#endregion
            //genelIptalInputType input = new genelIptalInputType();
            //#region FillEndorsDetails
            //V_SgmEndorsPolicyPremium sgmEndors = new GenericRepository<V_SgmEndorsPolicyPremium>().FindBy($" ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            //#endregion
            //#region Endors Check
            //if (sgmEndors == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Police detayı bulunamadı";
            //    return response;
            //}
            //#endregion
            //input.iptalNedenKod = long.Parse(sgmEndors.IptalNedenKod);
            //input.iptalZeylTuru = long.Parse(sgmEndors.IptalZeylTuru);
            //input.otorizasyonKod = sgmEndors.OtorizasyonKod;
            //input.policeBrutPrimi = double.Parse(sgmEndors.PoliceBrutPrimi);
            //input.policeNetPrimi = double.Parse(sgmEndors.PoliceNetPrimi);
            //input.policeNo = sgmEndors.PoliceNo;
            //input.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
            //input.sigortaSirketKod = sgmEndors.SigortaSirketKod;
            //input.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
            //input.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
            //input.zeylBrutPrimi = double.Parse(sgmEndors.ZeylBrutPrimi);
            //input.zeylNetPrimi = double.Parse(sgmEndors.ZeylNetPrimi);
            //input.zeylNo = int.Parse(sgmEndors.ZeylNo);
            //input.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);

            //if (ServiceType == SagmerServiceType.KONTROL)
            //    return serviceClient.genelIptalZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
            //else
            //    return serviceClient.genelIptalZeyli(input, long.Parse(sgmEndors.CompanyId));
        }
        [WebMethod]
        public SbmServiceRes meriyeteDonusZeyli(SbmPolicyReturnStartReq req, SagmerServiceType ServiceType)
        {
            SgmServiceClient sgmServiceClient = new SgmServiceClient();
            return sgmServiceClient.SbmPolicyReturnStartEndorsement(req, ServiceType);
            //serviceClient = new ServiceClient();
            //ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            //#region ApiCode Kontrol
            //if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //#endregion
            //#region IsSagmerSendViaPolicy
            //SagmerHelper sagmerHelper = new SagmerHelper();
            //if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            //{
            //    response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
            //    response.Data.islemBasarili = false;
            //    return response;
            //}
            //#endregion
            //policeYururlugeAlmaInputType input = new policeYururlugeAlmaInputType();
            //#region FillEndorsDetails
            //V_SgmEndorsPolicyPremium sgmEndors = new GenericRepository<V_SgmEndorsPolicyPremium>().FindBy($" ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            //#endregion
            //#region Endors Check
            //if (sgmEndors == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Police detayı bulunamadı";
            //    return response;
            //}
            //#endregion

            //input.otorizasyonKod = sgmEndors.OtorizasyonKod;
            //input.policeBrutPrimi = double.Parse(sgmEndors.PoliceBrutPrimi);
            //input.policeNetPrimi = double.Parse(sgmEndors.PoliceNetPrimi);
            //input.policeNo = sgmEndors.PoliceNo;
            //input.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
            //input.sigortaSirketKod = sgmEndors.SigortaSirketKod;
            //input.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
            //input.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
            //input.zeylBrutPrimi = double.Parse(sgmEndors.ZeylBrutPrimi);
            //input.zeylNetPrimi = double.Parse(sgmEndors.ZeylNetPrimi);
            //input.zeylNo = int.Parse(sgmEndors.ZeylNo);
            //input.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);
            //if (ServiceType == SagmerServiceType.KONTROL)
            //    return serviceClient.meriyeteDonusZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
            //else
            //    return serviceClient.meriyeteDonusZeyli(input, long.Parse(sgmEndors.CompanyId));
        }
        [WebMethod]
        public SbmServiceRes policeSilme(SbmPolicyDeleteReq req, SagmerServiceType ServiceType)
        {
            SgmServiceClient sgmServiceClient = new SgmServiceClient();
            return sgmServiceClient.SbmPolicyDelete(req, ServiceType);
            //serviceClient = new ServiceClient();
            //ServiceResponse<sonucType> response = new ServiceResponse<sonucType>();
            //#region ApiCode Kontrol
            //if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //#endregion
            //#region IsSagmerSendViaPolicy
            //SagmerHelper sagmerHelper = new SagmerHelper();
            //if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            //{
            //    response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
            //    response.Data.islemBasarili = false;
            //    return response;
            //}
            //#endregion
            //policeSilmeInputType input = new policeSilmeInputType();
            //#region FillEndorsDetails
            //V_SgmPolicyDelete sgmPolDel = new GenericRepository<V_SgmPolicyDelete>().FindBy($" ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            //#endregion
            //#region Endors Check
            //if (sgmPolDel == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Police detayı bulunamadı";
            //    return response;
            //}
            //#endregion
            //input.otorizasyonKod = sgmPolDel.OtorizasyonKod;
            //input.policeNo = sgmPolDel.PoliceNo;
            //input.sbmSagmerNo = long.Parse(sgmPolDel.SbmSagmerNo);
            //input.yenilemeNo = int.Parse(sgmPolDel.YenilemeNo);
            //if (ServiceType == SagmerServiceType.KONTROL)
            //    return serviceClient.policeSilmeKontrol(input, long.Parse(sgmPolDel.CompanyId));
            //else
            //    return serviceClient.policeSilme(input, long.Parse(sgmPolDel.CompanyId));
        }
        [WebMethod]
        public SbmServiceRes teminatPlanDegisiklikZeyli(SbmInsuredPackageChangeReq req, SagmerServiceType ServiceType)
        {
            SgmServiceClient sgmServiceClient = new SgmServiceClient();
            return sgmServiceClient.SbmInsuredPackageChangeEndorsement(req, ServiceType);
            //serviceClient = new ServiceClient();
            //ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            //#region ApiCode Kontrol
            //if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //#endregion
            //#region IsSagmerSendViaPolicy
            //SagmerHelper sagmerHelper = new SagmerHelper();
            //if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            //{
            //    response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
            //    response.Data.islemBasarili = false;
            //    return response;
            //}
            //#endregion
            //#region FillEndorsDetails
            //V_SgmEndorsInsuredPremium sgmEndors = new GenericRepository<V_SgmEndorsInsuredPremium>().FindBy($" INSURED_ID = '{InsuredId}' and ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            //List<V_SgmIngressInsCover> sgmCover = new GenericRepository<V_SgmIngressInsCover>().FindBy($"INSURED_ID '{InsuredId}' and POLICY_ID = '{PolicyId}' and ENDORSEMENT_ID = '{EndorsementId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            //#endregion
            //List<sigortaliTeminatInputType> sigTem = new List<sigortaliTeminatInputType>();

            //foreach (V_SgmIngressInsCover itemInsCover in sgmCover)
            //{
            //    sigTem.Add(new sigortaliTeminatInputType
            //    {
            //        aso = (Enums.evetHayirType)int.Parse(itemInsCover.Aso),
            //        cografiKapsamKodu = long.Parse(itemInsCover.CografiKapsamKodu),
            //        donemBaslangicTarihi = DateTime.Parse(itemInsCover.DonemBaslangicTarihi),
            //        donemBitisTarihi = DateTime.Parse(itemInsCover.DonemBitisTarihi),
            //        donemKodu = int.Parse(itemInsCover.DonemKodu),
            //        dovizCinsi = itemInsCover.DovizCinsi,
            //        teminatAdedi = int.Parse(itemInsCover.TeminatAdedi),
            //        teminatKodu = long.Parse(itemInsCover.TeminatKodu),
            //        teminatLimiti = double.Parse(itemInsCover.TeminatLimiti),
            //        teminatLimitKodu = long.Parse(itemInsCover.TeminatLimitKodu),
            //        teminatMuafiyeti = double.Parse(itemInsCover.TeminatMuafiyeti),
            //        teminatMuafiyetKodu = long.Parse(itemInsCover.TeminatMuafiyetKodu),
            //        teminatNetPrim = double.Parse(itemInsCover.TeminatNetPrim),
            //        teminatOdemeYuzdesi = double.Parse(itemInsCover.TeminatOdemeYuzdesi),
            //        teminatVaryasyonKodu = int.Parse(itemInsCover.TeminatVaryasyonKodu),
            //        toplamDonemSayisi = int.Parse(itemInsCover.ToplamDonemSayisi)
            //    });
            //}
            //#region FillFields
            //teminatInputType input = new teminatInputType();
            //input.kimlikNo = sgmEndors.KimlikNo;
            //input.kimlikTipKod = sgmEndors.KimlikTipi;
            //input.otorizasyonKod = sgmEndors.OtorizasyonKod;
            //input.policeBrutPrimi = double.Parse(sgmEndors.PoliceBrutPrimi);
            //input.policeNetPrimi = double.Parse(sgmEndors.PoliceNetPrimi);
            //input.policeNo = sgmEndors.PoliceNo;
            //input.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
            //input.sigortaSirketKod = sgmEndors.SigortaSirketKod;
            //input.urunPlanKod = long.Parse(sgmEndors.UrunPlanKod);
            //input.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
            //input.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
            //input.zeylBrutPrimi = double.Parse(sgmEndors.ZeylBrutPrimi);
            //input.zeylNetPrimi = double.Parse(sgmEndors.ZeylNetPrimi);
            //input.zeylNo = int.Parse(sgmEndors.ZeylNo);
            //input.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);
            //input.sigortaliTeminatInputTypeList = sigTem.ToArray();
            //#endregion
            //if (ServiceType == SagmerServiceType.KONTROL)
            //    return serviceClient.teminatPlanDegisiklikZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
            //else
            //    return serviceClient.teminatPlanDegisiklikZeyli(input, long.Parse(sgmEndors.CompanyId));
        }
        [WebMethod]
        public SbmServiceRes sigortaliBilgiDegisiklikZeyli(SbmInsuredInfoChangeReq req, SagmerServiceType ServiceType)
        {
            SgmServiceClient sgmServiceClient = new SgmServiceClient();
            return sgmServiceClient.SbmInsuredInfoChangeEndorsement(req, ServiceType);
            //serviceClient = new ServiceClient();
            //ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            //#region ApiCode Kontrol
            //if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //#endregion
            //#region IsSagmerSendViaPolicy
            //SagmerHelper sagmerHelper = new SagmerHelper();
            //if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            //{
            //    response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
            //    response.Data.islemBasarili = false;
            //    return response;
            //}
            //#endregion
            //#region FillEndorsDetails
            //V_SgmIngressEndorsement sgmEndors = new GenericRepository<V_SgmIngressEndorsement>().FindBy($" ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            //#endregion
            //sigortaliBilgiDegisiklikInputType input = new sigortaliBilgiDegisiklikInputType();
            //List<sigortaliBilgileriInputType> insuredInput = new List<sigortaliBilgileriInputType>();
            //List<V_SgmIngressEndorsInsured> sgmInsured = new GenericRepository<V_SgmIngressEndorsInsured>().FindBy($"ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            //foreach (V_SgmIngressEndorsInsured itemInsured in sgmInsured)
            //{
            //    V_SgmIngressInsHealthInfo healthInfo = new GenericRepository<V_SgmIngressInsHealthInfo>().FindBy($"INSURED_ID '{itemInsured.InsuredId}' and POLICY_ID = '{PolicyId}' and ENDORSEMENT_ID = '{EndorsementId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            //    List<V_SgmIngressExtraPremium> ExtraPrm = new GenericRepository<V_SgmIngressExtraPremium>().FindBy($"INSURED_ID '{itemInsured.InsuredId}' and POLICY_ID = '{PolicyId}' and ENDORSEMENT_ID = '{EndorsementId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            //    List<V_SgmIngressInsExemption> sgmExemption = new GenericRepository<V_SgmIngressInsExemption>().FindBy($"INSURED_ID '{itemInsured.InsuredId}' and POLICY_ID = '{PolicyId}' and ENDORSEMENT_ID = '{EndorsementId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

            //    sigortaliSaglikBilgileriInputType sigSagBilg = new sigortaliSaglikBilgileriInputType();
            //    List<sigortaliIndirimEkprimInputType> sigIndEkp = new List<sigortaliIndirimEkprimInputType>();
            //    List<sigortaliUygulamaInputType> sigUyg = new List<sigortaliUygulamaInputType>();

            //    if (ExtraPrm.Count > 0)
            //    {
            //        foreach (V_SgmIngressExtraPremium itemExtraPrm in ExtraPrm)
            //        {
            //            sigIndEkp.Add(new sigortaliIndirimEkprimInputType
            //            {
            //                aciklama = itemExtraPrm.Aciklama,
            //                indirimEkprimDegeri = double.Parse(itemExtraPrm.IndirimEkPrimDegeri),
            //                indirimEkprimKod = long.Parse(itemExtraPrm.IndirimEkPrimKod),
            //                kayitNo = long.Parse(itemExtraPrm.KayitNo),
            //                tutarMiOranMi = itemExtraPrm.TutarMiOranMi
            //            });
            //        }
            //    }
            //    if (healthInfo != null)
            //    {
            //        sigSagBilg.alkolKullaniyorMu = (Enums.evetHayirType)int.Parse(healthInfo.AlkolKullaniliyorMu);
            //        sigSagBilg.alkolKullanmaSuresi = healthInfo.AlkolKullanmaSuresi;
            //        sigSagBilg.alkolPeriyotTip = (Enums.aliskanlikPeriyodType)int.Parse(healthInfo.AlkolPeriyotTip);
            //        sigSagBilg.boy = int.Parse(healthInfo.Boy);
            //        sigSagBilg.kilo = int.Parse(healthInfo.Kilo);
            //        sigSagBilg.periyottakiAlkolMiktari = healthInfo.PeriyottakiAlkolMiktari;
            //        sigSagBilg.periyottakiSigaraAdedi = healthInfo.PeriyottakiSigaraAdedi;
            //        sigSagBilg.sigaraIciyorMu = (Enums.evetHayirType)int.Parse(healthInfo.SigaraIciyorMu);
            //        sigSagBilg.sigaraIcmeSuresi = healthInfo.SigaraIcmeSuresi;
            //        sigSagBilg.sigaraPeriyotTip = (Enums.aliskanlikPeriyodType)int.Parse(healthInfo.SigaraperiyotTip);

            //        List<sigortaliSaglikBilgileriDetayInputType> sigSagBilgDetay = new List<sigortaliSaglikBilgileriDetayInputType>();

            //        List<V_SgmIngressInsHealthDet> healthInfoDet = new GenericRepository<V_SgmIngressInsHealthDet>().FindBy($"INSURED_ID '{itemInsured.InsuredId}' and POLICY_ID = '{PolicyId}' and ENDORSEMENT_ID = '{EndorsementId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            //        foreach (V_SgmIngressInsHealthDet itemHealthDet in healthInfoDet)
            //        {
            //            sigSagBilgDetay.Add(new sigortaliSaglikBilgileriDetayInputType
            //            {
            //                ameliyatVarmi = itemHealthDet.AmeliyatVarMi,
            //                hastalikAciklamasi = itemHealthDet.HastalikAciklamasi,
            //                hastalikAdi = itemHealthDet.HastalikAdi,
            //                hastalikKod = itemHealthDet.HastalikKod,
            //                hastalikUygulamaKod = long.Parse(itemHealthDet.HastalikUygulamaKod),
            //                sonDurum = itemHealthDet.SonDurum,
            //                sureAy = int.Parse(itemHealthDet.SureAy),
            //                sureYil = int.Parse(itemHealthDet.SureYil),
            //                uygulamaAciklamasi = itemHealthDet.UygulamaAciklamasi
            //            });
            //        }
            //        sigSagBilg.saglikDetayBilgileri = sigSagBilgDetay.ToArray();

            //    }
            //    if (sigUyg.Count > 0)
            //    {
            //        foreach (V_SgmIngressInsExemption itemExemption in sgmExemption)
            //        {
            //            sigUyg.Add(new sigortaliUygulamaInputType
            //            {
            //                aciklama = itemExemption.Aciklama,
            //                hastalikKod = itemExemption.HastalikKod,
            //                kayitNo = long.Parse(itemExemption.KayitNo),
            //                muafiyetTutarLimiti = double.Parse(itemExemption.MuafiyetTutarLimiti),
            //                sigortaliUygulamaKod = long.Parse(itemExemption.SigortaliUygulamaKod)
            //            });
            //        }
            //    }

            //    insuredInput.Add(new sigortaliBilgileriInputType
            //    {
            //        brutPrim = double.Parse(itemInsured.BrutPrim),
            //        eskiPoliceNo = itemInsured.EskiPoliceNo,
            //        eskiYenilemeNo = int.Parse(itemInsured.EskiYenilemeNo),
            //        ilkOzelSigortalilikTarihi = DateTime.Parse(itemInsured.IlkOzelSigortalilikTarihi),
            //        ilkOzelSirketKod = itemInsured.IlkOzelSirketKod,
            //        kimlikNo = itemInsured.KimlikNo,
            //        kimlikTipKod = itemInsured.KimlikTipKod,
            //        meslekKod = long.Parse(itemInsured.MeslekKod),
            //        netPrim = double.Parse(itemInsured.NetPrim),
            //        sigortaliUyruk = (Enums.uyrukTuru)int.Parse(itemInsured.SigortaliUyruk),
            //        yasadigiIlceKod = itemInsured.YasadigiIlceKod,
            //        yasadigiIlKod = itemInsured.YasadigiIlKod,
            //        saglikBilgileriInputType = sigSagBilg,
            //        sigortaliIndirimEkprimInputTypeList = sigIndEkp.ToArray(),
            //        sigortaliUygulamaList = sigUyg.ToArray()
            //    });
            //}
            //#region FillFields
            //input.otorizasyonKod = sgmEndors.OtorizasyonKod;
            //input.policeBrutPrimi = double.Parse(sgmEndors.PoliceBrutPrimi);
            //input.policeNetPrimi = double.Parse(sgmEndors.PoliceNetPrimi);
            //input.policeNo = sgmEndors.PoliceNo;
            //input.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
            //input.sigortaSirketKod = sgmEndors.SigortaSirketKod;
            //input.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
            //input.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
            //input.zeylBrutPrimi = double.Parse(sgmEndors.ZeylBrutPrimi);
            //input.zeylNetPrimi = double.Parse(sgmEndors.ZeylNetPrimi);
            //input.zeylNo = int.Parse(sgmEndors.ZeylNo);
            //input.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);
            //input.sigortali = insuredInput.ToArray();
            //#endregion
            //if (ServiceType == SagmerServiceType.KONTROL)
            //    return serviceClient.sigortaliBilgiDegisiklikZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
            //else
            //    return serviceClient.sigortaliBilgiDegisiklikZeyli(input, long.Parse(sgmEndors.CompanyId));
        }









        [WebMethod]
        public SbmServiceRes seyahatPolice(SbmPolicyCreateReq req, SagmerServiceType ServiceType)
        {
            SgmServiceClient sgmServiceClient = new SgmServiceClient();
            return sgmServiceClient.SbmTravelPolicyCreate(req, ServiceType);
            //serviceClient = new ServiceClient();
            //ServiceResponse<policeSonucType> response = new ServiceResponse<policeSonucType>();
            //#region ApiCode Kontrol
            //if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //#endregion
            //#region IsSagmerSendViaPolicy

            //SagmerHelper sagmerHelper = new SagmerHelper();
            //if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            //{
            //    response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
            //    response.Data.islemBasarili = false;
            //    return response;
            //}

            //#endregion
            //seyahatPoliceInputType input = new seyahatPoliceInputType();

            //#region FillPolicyDetails
            //V_SgmPolicy sgmPolicy = new GenericRepository<V_SgmPolicy>().FindBy($"POLICY_ID = {PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            //#endregion
            //#region Policy Check
            //if (sgmPolicy == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Poliçe detayı bulunamadı";
            //    return response;
            //}
            //#endregion
            //#region Fill Fields
            //try
            //{
            //    input.dovizCinsi = sgmPolicy.DovizCinsi;
            //    input.endirektSigortaSirketKod = sgmPolicy.EndirektSigortaSirketKod;
            //    input.eskiPoliceNo = sgmPolicy.EskiPoliceNo;
            //    input.eskiYenilemeNo = string.IsNullOrEmpty(sgmPolicy.EskiYenilemeNo) ? 0 : int.Parse(sgmPolicy.EskiYenilemeNo);
            //    input.ilKod = sgmPolicy.IlKod;
            //    input.odemeKod = string.IsNullOrEmpty(sgmPolicy.OdemeKod) ? 0 : long.Parse(sgmPolicy.OdemeKod);
            //    input.otorizasyonKodField = sgmPolicy.OtorizasyonKod;
            //    input.policeBaslamaTarihi = DateTime.Parse(sgmPolicy.PoliceBaslamaTarihi);
            //    input.policeBitisTarihi = DateTime.Parse(sgmPolicy.PoliceBitisTarihi);
            //    input.policeBrutPrimi = double.Parse(sgmPolicy.PoliceBrutPrimi);
            //    input.policeNetPrimi = double.Parse(sgmPolicy.PoliceNetPrimi);
            //    input.policeNo = sgmPolicy.PoliceNo;
            //    input.policeTanzimTarihi = DateTime.Parse(sgmPolicy.PoliceTanzimTarihi);
            //    input.policeTip = (policeTipiType)System.Enum.Parse(typeof(policeTipiType), sgmPolicy.PoliceTip);
            //    input.sigortaEttirenType = new sigortaEttirenInputType();
            //    input.sigortaEttirenType.ad = sgmPolicy.SgtAd;
            //    input.sigortaEttirenType.adres = sgmPolicy.SgtAdres;
            //    input.sigortaEttirenType.babaAdi = sgmPolicy.SgtBabaAdi;

            //    input.sigortaEttirenType.cinsiyet = (cinsiyet)System.Enum.Parse(typeof(cinsiyet), sgmPolicy.SgtCinsiyet);
            //    input.sigortaEttirenType.dogumTarihi = DateTime.Parse(sgmPolicy.SgtDogumTarihi);
            //    input.sigortaEttirenType.dogumYeri = sgmPolicy.SgtDogumYeri;
            //    input.sigortaEttirenType.kimlikNo = sgmPolicy.SgtKimlikNo;
            //    input.sigortaEttirenType.kimlikTipKod = sgmPolicy.SgtKimlikTipKod;
            //    input.sigortaEttirenType.kurulusTarihi = DateTime.Parse(sgmPolicy.SgtKurulusTarihi);
            //    input.sigortaEttirenType.kurulusYeri = sgmPolicy.SgtKurulusYeri;
            //    input.sigortaEttirenType.sigortaEttirenUyruk = (uyrukTuru)System.Enum.Parse(typeof(uyrukTuru), sgmPolicy.SgtSigortaEttirenUyruk);
            //    input.sigortaEttirenType.soyad = sgmPolicy.SgtSoyad;
            //    input.sigortaEttirenType.turKod = (turKod)System.Enum.Parse(typeof(turKod), sgmPolicy.SgtTurKod);
            //    input.sigortaEttirenType.ulkeKodu = int.Parse(sgmPolicy.SgtUlkeKodu);
            //    input.sigortaSirketKodField = sgmPolicy.SigortaSirketKod;
            //    input.tarifeID = string.IsNullOrEmpty(sgmPolicy.TarifeId) ? 0 : long.Parse(sgmPolicy.TarifeId);
            //    input.uretimKaynakKod = string.IsNullOrEmpty(sgmPolicy.UretimKaynakKod) ? 0 : long.Parse(sgmPolicy.UretimKaynakKod);
            //    input.uretimKaynakKurumKod = sgmPolicy.UretimKaynakKurumKod;
            //    input.vadeSayisi = string.IsNullOrEmpty(sgmPolicy.VadeSayisi) ? 0 : int.Parse(sgmPolicy.VadeSayisi);
            //    input.yenilemeNo = string.IsNullOrEmpty(sgmPolicy.YenilemeNo) ? 0 : int.Parse(sgmPolicy.YenilemeNo);
            //    input.yeniYenilemeGecis = sgmPolicy.YeniYenilemeGecis;
            //    input.zeylNo = string.IsNullOrEmpty(sgmPolicy.ZeylNo) ? 0 : int.Parse(sgmPolicy.ZeylNo);
            //}
            //catch (Exception ex)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = ex.ToString();
            //    return response;
            //}
            //#endregion
            //if (ServiceType == SagmerServiceType.KONTROL)
            //    return serviceClient.seyahatPoliceKontrol(input, long.Parse(sgmPolicy.CompanyId));
            //else return serviceClient.seyahatPolice(input, long.Parse(sgmPolicy.CompanyId));

        }
        [WebMethod]
        public SbmServiceRes seyahatSigortaliGirisZeyli(SbmInsuredEntranceReq req, SagmerServiceType ServiceType)
        {
            SgmServiceClient sgmServiceClient = new SgmServiceClient();
            return sgmServiceClient.SbmTravelInsuredEntranceEndorsement(req, ServiceType);
            //serviceClient = new ServiceClient();
            //ServiceResponse<seyahatSigortaliSonucType> response = new ServiceResponse<seyahatSigortaliSonucType>();
            //#region ApiCode Kontrol
            //if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //#endregion
            //#region IsSagmerSendViaPolicy
            //SagmerHelper sagmerHelper = new SagmerHelper();
            //if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            //{
            //    response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
            //    response.Data.islemBasarili = false;
            //    return response;
            //}
            //#endregion
            //seyahatSigortaliGirisInputType input = new seyahatSigortaliGirisInputType();

            //#region FillEndorsDetails
            //V_SgmIngressEndorsement sgmEndors = new GenericRepository<V_SgmIngressEndorsement>().FindBy($" ENDORSEMENT_ID = {EndorsementId} AND POLICY_ID = {PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            //List<V_SgmIngressEndorsInsured> sgmEndorsInsured = new GenericRepository<V_SgmIngressEndorsInsured>().FindBy($"ENDORSEMENT_ID = {EndorsementId} AND POLICY_ID = {PolicyId}", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            //#endregion

            //#region Fill Fields
            //input.zeylTuru = long.Parse(sgmEndors.ZeylTuru);
            //input.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);
            //input.zeylNo = int.Parse(sgmEndors.ZeylNo);
            //input.zeylNetPrimi = double.Parse(sgmEndors.ZeylNetPrimi);
            //input.zeylBrutPrimi = double.Parse(sgmEndors.ZeylBrutPrimi);
            //input.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
            //input.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
            //input.sigortaSirketKod = sgmEndors.SigortaSirketKod;
            //input.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
            //input.otorizasyonKod = sgmEndors.OtorizasyonKod;
            //input.policeNo = sgmEndors.PoliceNo;
            //input.policeNetPrimi = double.Parse(sgmEndors.PoliceNetPrimi);
            //input.policeBrutPrimi = double.Parse(sgmEndors.PoliceBrutPrimi);
            //List<seyahatSigortaliTeminatliInputType> lstInsured = new List<seyahatSigortaliTeminatliInputType>();
            //foreach (V_SgmIngressEndorsInsured item in sgmEndorsInsured)
            //{
            //    List<V_SgmIngressInsCover> sgmInsuredCover = new GenericRepository<V_SgmIngressInsCover>().FindBy($"INSURED_ID '{item.InsuredId}' and POLICY_ID = '{PolicyId}' and ENDORSEMENT_ID = '{EndorsementId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            //    List<V_SgmIngressExtraPremium> ExtraPrm = new GenericRepository<V_SgmIngressExtraPremium>().FindBy($"INSURED_ID '{item.InsuredId}' and POLICY_ID = '{PolicyId}' and ENDORSEMENT_ID = '{EndorsementId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

            //    List<sigortaliIndirimEkprimInputType> sigIndEkp = new List<sigortaliIndirimEkprimInputType>();
            //    List<seyahatSigortaliTeminatInputType> sigTem = new List<seyahatSigortaliTeminatInputType>();

            //    foreach (V_SgmIngressExtraPremium itemExtraPrm in ExtraPrm)
            //    {
            //        sigIndEkp.Add(new sigortaliIndirimEkprimInputType
            //        {
            //            aciklama = itemExtraPrm.Aciklama,
            //            indirimEkprimDegeri = double.Parse(itemExtraPrm.IndirimEkPrimDegeri),
            //            indirimEkprimKod = long.Parse(itemExtraPrm.IndirimEkPrimKod),
            //            kayitNo = long.Parse(itemExtraPrm.KayitNo),
            //            tutarMiOranMi = itemExtraPrm.TutarMiOranMi
            //        });
            //    }
            //    foreach (V_SgmIngressInsCover itemInsCover in sgmInsuredCover)
            //    {
            //        sigTem.Add(new seyahatSigortaliTeminatInputType
            //        {
            //            cografiKapsamKodu = long.Parse(itemInsCover.CografiKapsamKodu),
            //            dovizCinsi = itemInsCover.DovizCinsi,
            //            teminatAdedi = int.Parse(itemInsCover.TeminatAdedi),
            //            teminatKodu = long.Parse(itemInsCover.TeminatKodu),
            //            teminatLimiti = double.Parse(itemInsCover.TeminatLimiti),
            //            teminatLimitKodu = long.Parse(itemInsCover.TeminatLimitKodu),
            //            teminatMuafiyeti = double.Parse(itemInsCover.TeminatMuafiyeti),
            //            teminatMuafiyetKodu = long.Parse(itemInsCover.TeminatMuafiyetKodu),
            //            teminatNetPrim = double.Parse(itemInsCover.TeminatNetPrim),
            //            teminatOdemeYuzdesi = double.Parse(itemInsCover.TeminatOdemeYuzdesi),
            //            teminatVaryasyonKodu = int.Parse(itemInsCover.TeminatVaryasyonKodu)
            //        });
            //    }

            //    lstInsured.Add(new seyahatSigortaliTeminatliInputType
            //    {
            //        adres = item.Adres,
            //        babaAdi = item.BabaAdi,
            //        bireySiraNo = item.BireySiraNo,
            //        bireyTipKod = long.Parse(item.BireyTipKod),
            //        brutPrim = double.Parse(item.BrutPrim),
            //        cinsiyet = (Enums.cinsiyet)int.Parse(item.Cinsiyet),
            //        dogumTarihi = DateTime.Parse(item.DogumTarihi),
            //        dogumYeri = item.DogumYeri,
            //        eskiPoliceNo = item.EskiPoliceNo,
            //        eskiYenilemeNo = int.Parse(item.EskiYenilemeNo),
            //        kimlikNo = item.KimlikNo,
            //        kimlikTipKod = item.KimlikTipKod,
            //        netPrim = double.Parse(item.NetPrim),
            //        sigortaliAd = item.SigortaliAd,
            //        sigortaliSoyad = item.SigortaliAd,
            //        sigortaliUyruk = (Enums.uyrukTuru)int.Parse(item.SigortaliUyruk),
            //        ulkeKodu = int.Parse(item.UlkeKodu),
            //        urunPlanKod = long.Parse(item.UrunPlanKod),
            //        yasadigiIlceKod = item.YasadigiIlceKod,
            //        yasadigiIlKod = item.YasadigiIlKod,
            //        sigortaliIndirimEkprimInputTypeList = sigIndEkp.ToArray(),
            //        sigortaliTeminatInputTypeListField = sigTem.ToArray()
            //    });
            //}
            //input.sigortali = lstInsured.ToArray();
            //#endregion

            //if (ServiceType == SagmerServiceType.KONTROL)
            //    return serviceClient.seyahatSigortaliGirisZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
            //else
            //    return serviceClient.seyahatSigortaliGirisZeyli(input, long.Parse(sgmEndors.CompanyId));
        }
        [WebMethod]
        public SbmServiceRes seyahatSigortaliCikisZeyli(SbmInsuredExitReq req, SagmerServiceType ServiceType)
        {
            SgmServiceClient sgmServiceClient = new SgmServiceClient();
            return sgmServiceClient.SbmTravelInsuredExitEndorsement(req, ServiceType);
            //serviceClient = new ServiceClient();
            //ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            //#region ApiCode Kontrol
            //if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //#endregion
            //#region IsSagmerSendViaPolicy
            //SagmerHelper sagmerHelper = new SagmerHelper();
            //if (!sagmerHelper.IsSagmerSendViaPolicy(PolicyId))
            //{
            //    response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
            //    response.Data.islemBasarili = false;
            //    return response;
            //}
            //#endregion
            //seyahatSigortaliCikisInputType input = new seyahatSigortaliCikisInputType();

            //#region FillEndorsDetails
            //V_SgmEgressEndorsement sgmEndors = new GenericRepository<V_SgmEgressEndorsement>().FindBy($"ENDORSEMENT_ID = '{EndorsementId}' AND POLICY_ID = '{PolicyId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            //#endregion
            //#region Endors Check
            //if (sgmEndors == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Zeyl detayı bulunamadı";
            //    return response;
            //}
            //#endregion
            //#region FillFields
            //input.brutPrim = double.Parse(sgmEndors.BrutPrim);
            //input.kimlikNo = sgmEndors.KimlikNo;
            //input.kimlikTipKod = sgmEndors.KimlikTipKod;
            //input.netPrim = double.Parse(sgmEndors.NetPrim);
            //input.otorizasyonKod = sgmEndors.OtorizasyonKod;
            //input.policeBrutPrimi = double.Parse(sgmEndors.PoliceBrutPrimi);
            //input.policeNetPrimi = double.Parse(sgmEndors.PoliceNetPrimi);
            //input.policeNo = sgmEndors.PoliceNo;
            //input.sbmSagmerNo = long.Parse(sgmEndors.SbmSagmerNo);
            //input.sigortaSirketKod = sgmEndors.SigortaSirketKod;
            //input.yenilemeNo = int.Parse(sgmEndors.YenilemeNo);
            //input.zeylBaslangicTarihi = DateTime.Parse(sgmEndors.ZeylBaslangicTarihi);
            //input.zeylBrutPrimi = double.Parse(sgmEndors.ZeylBrutPrimi);
            //input.zeylNetPrimi = double.Parse(sgmEndors.ZeylNetPrimi);
            //input.zeylNo = int.Parse(sgmEndors.ZeylNo);
            //input.zeylTanzimTarihi = DateTime.Parse(sgmEndors.ZeylTanzimTarihi);
            //#endregion
            //if (ServiceType == SagmerServiceType.KONTROL)
            //    return serviceClient.seyahatSigortaliCikisZeyliKontrol(input, long.Parse(sgmEndors.CompanyId));
            //else
            //    return serviceClient.seyahatSigortaliCikisZeyli(input, long.Parse(sgmEndors.CompanyId));
        }





        

        [WebMethod]
        public SbmServiceRes tazminatDosyaGiris(SbmClaimReq req, SagmerServiceType ServiceType)
        {
            SgmServiceClient sgmServiceClient = new SgmServiceClient();
            return sgmServiceClient.SbmClaimCreate(req, ServiceType);
            //serviceClient = new ServiceClient();
            //ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            //#region ApiCode Kontrol
            //if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //#endregion
            //#region IsSagmerSendViaPolicy
            //SagmerHelper sagmerHelper = new SagmerHelper();
            //if (!sagmerHelper.IsSagmerSendViaClaim(ClaimId))
            //{
            //    response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
            //    response.Data.islemBasarili = false;
            //    return response;
            //}
            //#endregion
            //dosyaInputType input = new dosyaInputType();
            //#region FillClaim
            //V_SgmClaimInsert claimIns = new GenericRepository<V_SgmClaimInsert>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            //List<faturaType> bill = new List<faturaType>();
            //List<V_SgmClaimBill> claimBill = new GenericRepository<V_SgmClaimBill>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            //List<V_SgmClaimCover> claimCover = new GenericRepository<V_SgmClaimCover>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            //List<V_SgmClaimIcd> claimIcd = new GenericRepository<V_SgmClaimIcd>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

            //List<saglikKurumType> sagKur = new List<saglikKurumType>();
            //List<teminatOdemeType> temOde = new List<teminatOdemeType>();
            //List<icdType> icd = new List<icdType>();

            //foreach (V_SgmClaimIcd _icd in claimIcd)
            //{
            //    icd.Add(new icdType
            //    {
            //        donemNo = int.Parse(_icd.DonemNo),
            //        iCDNo = _icd.IcdNo,
            //        iCDTip = int.Parse(_icd.IcdTip),
            //        teminatNo = int.Parse(_icd.TeminatNo),
            //        varyasyonNo = int.Parse(_icd.VaryasyonNo)
            //    });
            //}
            //foreach (V_SgmClaimCover clmCover in claimCover)
            //{
            //    temOde.Add(new teminatOdemeType
            //    {
            //        donemNo = int.Parse(clmCover.DonemNo),
            //        muallakTutariTL = decimal.Parse(clmCover.MuallakTutariTL),
            //        odemeSiraNo = long.Parse(clmCover.OdemeSiraNo),
            //        odemeTipi = (Enums.odemeTipiType)int.Parse(clmCover.OdemeTipi),
            //        odemeTutariTL = decimal.Parse(clmCover.OdemeTutariTL),
            //        teminatNo = int.Parse(clmCover.TeminatNo),
            //        varyasyonNo = int.Parse(clmCover.VaryasyonNo)
            //    });
            //}
            //sagKur.Add(new saglikKurumType
            //{
            //    kurumIlcesi = claimIns.KurumIlcesi,
            //    kurumIli = claimIns.KurumIli,
            //    kurumUlke = claimIns.KurumUlke,
            //    saglikKurumNo = claimIns.SaglikKurumNo,
            //    saglikKurumNoTipi = claimIns.SaglikKurumNoTipi,
            //    teminatList = temOde.ToArray()
            //});

            //if (claimBill.Count > 0)
            //{
            //    foreach (V_SgmClaimBill fat in claimBill)
            //    {
            //        bill.Add(new faturaType
            //        {
            //            ettn = fat.Ettn,
            //            faturaNo = fat.FaturaNo,
            //            faturaTarihi = DateTime.Parse(fat.FaturaTarihi),
            //            faturaTutariTL = decimal.Parse(fat.FaturaTutariTl),
            //            faturaTutariTLOdenen = decimal.Parse(fat.FaturaTutariTlOdenen),
            //            paraBirimi = fat.ParaBirimi
            //        });
            //    }
            //}
            //#endregion
            //#region  FillFields
            //input.dosyaRedNedeni = long.Parse(claimIns.DosyaRedNedeni);
            //input.ihbarTarihi = DateTime.Parse(claimIns.IhbarTarihi);
            //input.kimlikNo = claimIns.KimlikNo;
            //input.kimlikTipKod = claimIns.KimlikTipKodu;
            //input.muallakOdemeTarihi = DateTime.Parse(claimIns.MuallakOdemeTarihi);
            //input.muallakTutariTL = decimal.Parse(claimIns.MuallakTutariTL);
            //input.odemeTutariTL = decimal.Parse(claimIns.OdemeTutarTL);
            //input.odemeYeri = (Enums.odemeYeriType)int.Parse(claimIns.OdemeYeri);
            //input.olayTarihi = DateTime.Parse(claimIns.OlayTarihi);
            //input.otorizasyonKod = claimIns.OtorizasyonKodu;
            //input.paraBirimi = claimIns.ParaBirimi;
            //input.provizyonTarihi = DateTime.Parse(claimIns.ProvizyonTarihi);
            //input.sbmSagmerNo = long.Parse(claimIns.SbmSagmerNo);
            //input.tazminatDosyaDurumu = long.Parse(claimIns.TazminatDosyaDurumu);
            //input.tazminatDosyaNo = claimIns.TazminatDosyaNo;
            //input.tazminatSiraNo = long.Parse(claimIns.TazminatSiraNo);
            //input.tedaviTipi = (Enums.tedaviTipiType)int.Parse(claimIns.TedaviTipi);
            //input.faturaList = bill.ToArray();
            //input.saglikKurumList = sagKur.ToArray();
            //input.icdList = icd.ToArray();
            //#endregion
            //if (ServiceType == SagmerServiceType.KONTROL)
            //    return serviceClient.tazminatDosyaGirisKontrol(input, long.Parse(claimIns.CompanyId));
            //else
            //    return serviceClient.tazminatDosyaGiris(input, long.Parse(claimIns.CompanyId));
        }
        [WebMethod]
        public SbmServiceRes tazminatDosyaGuncelleme(SbmClaimReq req, SagmerServiceType ServiceType)
        {
            SgmServiceClient sgmServiceClient = new SgmServiceClient();
            return sgmServiceClient.SbmClaimUpdate(req, ServiceType);
            //serviceClient = new ServiceClient();
            //ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            //#region ApiCode Kontrol
            //if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //#endregion
            //#region IsSagmerSendViaPolicy
            //SagmerHelper sagmerHelper = new SagmerHelper();
            //if (!sagmerHelper.IsSagmerSendViaClaim(ClaimId))
            //{
            //    response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
            //    response.Data.islemBasarili = false;
            //    return response;
            //}
            //#endregion
            //dosyaUpdateType input = new dosyaUpdateType();
            //#region FillClaim
            //V_SgmClaimInsert claimIns = new GenericRepository<V_SgmClaimInsert>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            //List<faturaType> bill = new List<faturaType>();
            //List<V_SgmClaimBill> claimBill = new GenericRepository<V_SgmClaimBill>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            //List<V_SgmClaimCover> claimCover = new GenericRepository<V_SgmClaimCover>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            //List<V_SgmClaimIcd> claimIcd = new GenericRepository<V_SgmClaimIcd>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

            //List<saglikKurumUpdateType> sagKur = new List<saglikKurumUpdateType>();
            //List<teminatOdemeType> temOde = new List<teminatOdemeType>();
            //List<icdInputType> icd = new List<icdInputType>();

            //foreach (V_SgmClaimIcd _icd in claimIcd)
            //{
            //    icd.Add(new icdInputType
            //    {
            //        islemTipi = (Enums.islemTipiType)int.Parse(_icd.IslemTipi),
            //        donemNo = int.Parse(_icd.DonemNo),
            //        iCDNo = _icd.IcdNo,
            //        iCDTip = int.Parse(_icd.IcdTip),
            //        teminatNo = int.Parse(_icd.TeminatNo),
            //        varyasyonNo = int.Parse(_icd.VaryasyonNo)
            //    });
            //}
            //foreach (V_SgmClaimCover clmCover in claimCover)
            //{
            //    temOde.Add(new teminatOdemeType
            //    {
            //        donemNo = int.Parse(clmCover.DonemNo),
            //        muallakTutariTL = decimal.Parse(clmCover.MuallakTutariTL),
            //        odemeSiraNo = long.Parse(clmCover.OdemeSiraNo),
            //        odemeTipi = (Enums.odemeTipiType)int.Parse(clmCover.OdemeTipi),
            //        odemeTutariTL = decimal.Parse(clmCover.OdemeTutariTL),
            //        teminatNo = int.Parse(clmCover.TeminatNo),
            //        varyasyonNo = int.Parse(clmCover.VaryasyonNo)
            //    });
            //}
            //sagKur.Add(new saglikKurumUpdateType
            //{
            //    kurumIlcesi = claimIns.KurumIlcesi,
            //    kurumIli = claimIns.KurumIli,
            //    kurumUlke = claimIns.KurumUlke,
            //    saglikKurumNo = claimIns.SaglikKurumNo,
            //    saglikKurumNoTipi = claimIns.SaglikKurumNoTipi,
            //    kurumIlcesiYeni = claimIns.KurumIlcesiYeni,
            //    kurumIliYeni = claimIns.KurumIliYeni,
            //    kurumUlkeYeni = claimIns.KurumUlkeYeni,
            //    saglikKurumNoTipiYeni = claimIns.SaglikKurumNoTipiYeni,
            //    saglikKurumNoYeni = claimIns.SaglikKurumNoYeni
            //});
            //if (claimBill.Count > 0)
            //{
            //    foreach (V_SgmClaimBill fat in claimBill)
            //    {
            //        bill.Add(new faturaType
            //        {
            //            ettn = fat.Ettn,
            //            faturaNo = fat.FaturaNo,
            //            faturaTarihi = DateTime.Parse(fat.FaturaTarihi),
            //            faturaTutariTL = decimal.Parse(fat.FaturaTutariTl),
            //            faturaTutariTLOdenen = decimal.Parse(fat.FaturaTutariTlOdenen),
            //            paraBirimi = fat.ParaBirimi
            //        });
            //    }
            //}
            //#endregion
            //#region  FillFields
            //input.dosyaRedNedeni = long.Parse(claimIns.DosyaRedNedeni);
            //input.ihbarTarihi = DateTime.Parse(claimIns.IhbarTarihi);
            //input.dosyaGuncellemeTarihi = DateTime.Parse(claimIns.DosyaGuncellemeTarihi);
            //input.icdList = icd.ToArray();
            //input.saglikKurumList = sagKur.ToArray();
            //input.odemeYeri = (Enums.odemeYeriType)int.Parse(claimIns.OdemeYeri);
            //input.olayTarihi = DateTime.Parse(claimIns.OlayTarihi);
            //input.otorizasyonKod = claimIns.OtorizasyonKodu;
            //input.paraBirimi = claimIns.ParaBirimi;
            //input.provizyonTarihi = DateTime.Parse(claimIns.ProvizyonTarihi);
            //input.sbmSagmerNo = long.Parse(claimIns.SbmSagmerNo);
            //input.tazminatDosyaDurumu = long.Parse(claimIns.TazminatDosyaDurumu);
            //input.tazminatDosyaNo = claimIns.TazminatDosyaNo;
            //input.tedaviTipi = (Enums.tedaviTipiType)int.Parse(claimIns.TedaviTipi);
            //#endregion
            //if (ServiceType == SagmerServiceType.KONTROL)
            //    return serviceClient.tazminatDosyaGuncellemeKontrol(input, long.Parse(claimIns.CompanyId));
            //else
            //    return serviceClient.tazminatDosyaGuncelleme(input, long.Parse(claimIns.CompanyId));
        }
        [WebMethod]
        public SbmServiceRes tazminatFaturaGiris(SbmClaimReq req, SagmerServiceType ServiceType)
        {
            SgmServiceClient sgmServiceClient = new SgmServiceClient();
            return sgmServiceClient.SbmClaimEntrancePayroll(req, ServiceType);

            //serviceClient = new ServiceClient();
            //ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            //#region ApiCode Kontrol
            //if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //#endregion
            //#region IsSagmerSendViaPolicy
            //SagmerHelper sagmerHelper = new SagmerHelper();
            //if (!sagmerHelper.IsSagmerSendViaClaim(ClaimId))
            //{
            //    response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
            //    response.Data.islemBasarili = false;
            //    return response;
            //}
            //#endregion
            //V_SgmClaimInsert claimIns = new GenericRepository<V_SgmClaimInsert>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();
            //List<V_SgmClaimBill> claimBill = new GenericRepository<V_SgmClaimBill>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            //List<faturaType> bill = new List<faturaType>();
            //if (claimBill.Count > 0)
            //{
            //    foreach (V_SgmClaimBill fat in claimBill)
            //    {
            //        bill.Add(new faturaType
            //        {
            //            ettn = fat.Ettn,
            //            faturaNo = fat.FaturaNo,
            //            faturaTarihi = DateTime.Parse(fat.FaturaTarihi),
            //            faturaTutariTL = decimal.Parse(fat.FaturaTutariTl),
            //            faturaTutariTLOdenen = decimal.Parse(fat.FaturaTutariTlOdenen),
            //            paraBirimi = fat.ParaBirimi
            //        });
            //    }
            //}
            //dosyaFaturaInputType input = new dosyaFaturaInputType();
            //input.sbmSagmerNo = long.Parse(claimIns.SbmSagmerNo);
            //input.tazminatDosyaNo = claimIns.TazminatDosyaNo;
            //input.faturaList = bill.ToArray();
            //if (ServiceType == SagmerServiceType.KONTROL)
            //    return serviceClient.tazminatFaturaGirisKontrol(input, long.Parse(claimIns.CompanyId));
            //else
            //    return serviceClient.tazminatFaturaGiris(input, long.Parse(claimIns.CompanyId));
        }
        [WebMethod]
        public SbmServiceRes tazminatOdemeKontrol(SbmClaimReq req, SagmerServiceType ServiceType)
        {
            SgmServiceClient sgmServiceClient = new SgmServiceClient();
            return sgmServiceClient.SbmClaimPayment(req, ServiceType);
            //serviceClient = new ServiceClient();
            //ServiceResponse<zeylSonucType> response = new ServiceResponse<zeylSonucType>();
            //#region ApiCode Kontrol
            //if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] == null)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //else if (ConfigurationManager.AppSettings[AppKeys.SagmerApiCode] != ApiCode)
            //{
            //    response.Data.islemBasarili = false;
            //    response.Data.aciklama = "Api Code hatası";
            //    return response;
            //}
            //#endregion
            //#region IsSagmerSendViaPolicy
            //SagmerHelper sagmerHelper = new SagmerHelper();
            //if (!sagmerHelper.IsSagmerSendViaClaim(ClaimId))
            //{
            //    response.Data.aciklama = "Bu poliçenin sagmere gönderilme zorunluluğu bulunmamakta";
            //    response.Data.islemBasarili = false;
            //    return response;
            //}
            //#endregion
            //V_SgmClaimInsert claimIns = new GenericRepository<V_SgmClaimInsert>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();

            //dosyaTazminatOdemeInputType input = new dosyaTazminatOdemeInputType();
            //List<saglikKurumType> sagKur = new List<saglikKurumType>();
            //List<V_SgmClaimCover> claimCover = new GenericRepository<V_SgmClaimCover>().FindBy($"CLAIM_ID = '{ClaimId}'", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
            //List<teminatOdemeType> temOde = new List<teminatOdemeType>();

            //foreach (V_SgmClaimCover clmCover in claimCover)
            //{
            //    temOde.Add(new teminatOdemeType
            //    {
            //        donemNo = int.Parse(clmCover.DonemNo),
            //        muallakTutariTL = decimal.Parse(clmCover.MuallakTutariTL),
            //        odemeSiraNo = long.Parse(clmCover.OdemeSiraNo),
            //        odemeTipi = (Enums.odemeTipiType)int.Parse(clmCover.OdemeTipi),
            //        odemeTutariTL = decimal.Parse(clmCover.OdemeTutariTL),
            //        teminatNo = int.Parse(clmCover.TeminatNo),
            //        varyasyonNo = int.Parse(clmCover.VaryasyonNo)
            //    });
            //}
            //sagKur.Add(new saglikKurumType
            //{
            //    kurumIlcesi = claimIns.KurumIlcesi,
            //    kurumIli = claimIns.KurumIli,
            //    kurumUlke = claimIns.KurumUlke,
            //    saglikKurumNo = claimIns.SaglikKurumNo,
            //    saglikKurumNoTipi = claimIns.SaglikKurumNoTipi,
            //    teminatList = temOde.ToArray()
            //});

            //input.dosyaRedNedeni = long.Parse(claimIns.DosyaRedNedeni);
            //input.muallakOdemeTarihi = DateTime.Parse(claimIns.MuallakOdemeTarihi);
            //input.muallakTutariTL = decimal.Parse(claimIns.MuallakTutariTL);
            //input.odemeTutariTL = decimal.Parse(claimIns.OdemeTutarTL);
            //input.otorizasyonKod = claimIns.OtorizasyonKodu;
            //input.sbmSagmerNo = long.Parse(claimIns.SbmSagmerNo);
            //input.tazminatDosyaDurumu = long.Parse(claimIns.TazminatDosyaDurumu);
            //input.tazminatDosyaNo = claimIns.TazminatDosyaNo;
            //input.saglikKurumList = sagKur.ToArray();
            //if (ServiceType == SagmerServiceType.KONTROL)
            //    return serviceClient.tazminatOdemeKontrol(input, long.Parse(claimIns.CompanyId));
            //else
            //    return serviceClient.tazminatOdeme(input, long.Parse(claimIns.CompanyId));
        }
    }
}