﻿using Protein.Common.Extensions;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.InsuranceCompanies;
using Protein.Data.ExternalServices.InsuranceCompanies.Util.Lib;
using Protein.Data.ExternalServices.Log;
using Protein.Data.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Services;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.WS.WS.Common
{
    /// <summary>
    /// Summary description for MutabakatSrv
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MutabakatSrv : System.Web.Services.WebService
    {
        ServiceClient serviceClient = new ServiceClient();

        private void CreateFailedLog(IntegrationLog log, string responseXml, string[] desc)
        {
            log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
            log.Type = ((int)LogType.FAILED).ToString();
            log.Response = responseXml;
            log.ResponseStatusCode = "0";
            log.ResponseStatusDesc = desc != null ? string.Join(" - ", desc) : "HATA";
            log.WsResponseDate = DateTime.Now;
            IntegrationLogHelper.AddToLog(log);
        }

        [WebMethod]
        public ProvizyonMutabakatRes ProvizyonMutabakat(string apiCode, ProvizyonMutabakatReq req)
        {

            if (req != null)
            {
                return serviceClient.ProvizyonMutabakat(apiCode, req);
            }
            else
            {
                var response = new ProvizyonMutabakatRes { ToplamAdet = 0, ToplamSirketTutar = 0, SonucAciklama = "NULL REQUEST", SonucKod = "0" };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST", $"ApiCode={apiCode}" });

                return response;
            }

        }

        [WebMethod]
        public ProvizyonMutabakatDetayRes ProvizyonMutabakatDetay(string apiCode, ProvizyonMutabakatDetayReq req)
        {

            if (req != null)
            {
                return serviceClient.ProvizyonMutabakatDetay(apiCode, req);
            }
            else
            {
                var response = new ProvizyonMutabakatDetayRes { SonucAciklama = "NULL REQUEST", SonucKod = "0" };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST", $"ApiCode={apiCode}" });

                return response;
            }

        }

        [WebMethod]
        public IcmalMutabakatRes TazminatPaketMutabakat(string apiCode, IcmalMutabakatReq req)
        {

            if (req != null)
            {
                return serviceClient.TazminatPaketMutabakat(apiCode, req);
            }
            else
            {
                var response = new IcmalMutabakatRes { SonucAciklama = "NULL REQUEST", SonucKod = "0" };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST", $"ApiCode={apiCode}" });

                return response;
            }
        }

        [WebMethod]
        public IcmalMutabakatDetayRes TazminatPaketMutabakatDetay(string apiCode, IcmalMutabakatDetayReq req)
        {

            if (req != null)
            {
                return serviceClient.TazminatPaketMutabakatDetay(apiCode, req);
            }
            else
            {
                var response = new IcmalMutabakatDetayRes { SonucAciklama = "NULL REQUEST", SonucKod = "0" };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST", $"ApiCode={apiCode}" });

                return response;
            }
        }

        [WebMethod]
        public PoliceMutabakatRes PoliceMutabakat(string apiCode, PoliceMutabakatReq req)
        {

            if (req != null)
            {
                return serviceClient.PoliceMutabakat(apiCode, req);
            }
            else
            {
                var response = new PoliceMutabakatRes { SonucAciklama = "NULL REQUEST", SonucKod = "0" };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                    IS_IMECE_WS = 1
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST", $"ApiCode={apiCode}" });

                return response;
            }

        }

        [WebMethod]
        public PoliceDetayMutabakatRes PoliceDetayMutabakat(string apiCode, PoliceDetayMutabakatReq req)
        {

            if (req != null)
            {
                return serviceClient.PoliceDetayMutabakat(apiCode, req);
            }
            else
            {
                var response = new PoliceDetayMutabakatRes { SonucAciklama = "NULL REQUEST", SonucKod = "0" };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST", $"ApiCode={apiCode}" });

                return response;
            }

        }

        [WebMethod]
        public ZeyilMutabakatRes ZeyilMutabakat(string apiCode, ZeyilMutabakatReq req)
        {

            if (req != null)
            {
                return serviceClient.ZeyilMutabakat(apiCode, req);
            }
            else
            {
                var response = new ZeyilMutabakatRes { SonucAciklama = "NULL REQUEST", SonucKod = "0" };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST", $"ApiCode={apiCode}" });

                return response;
            }

        }

        [WebMethod]
        public ZeyilDetayMutabakatRes ZeyilDetayMutabakat(string apiCode, ZeyilDetayMutabakatReq req)
        {

            if (req != null)
            {
                return serviceClient.ZeyilDetayMutabakat(apiCode, req);
            }
            else
            {
                var response = new ZeyilDetayMutabakatRes { SonucAciklama = "NULL REQUEST", SonucKod = "0" };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST", $"ApiCode={apiCode}" });

                return response;
            }
        }
    }
}
