﻿using Protein.Common.Extensions;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.InsuranceCompanies;
using Protein.Data.ExternalServices.InsuranceCompanies.Util.Lib;
using Protein.Data.ExternalServices.Log;
using Protein.Data.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Services;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.WS.WS.Common
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class ProteinSrv : System.Web.Services.WebService
    {
        ServiceClient serviceClient = new ServiceClient();
        
        //    SirketAuth KE_Live = new SirketAuth("614ECD0447971182E0531A6D8DB9B5BC", 16);
        //    SirketAuth KE_Test = new SirketAuth("44f5f0fd-fac9-4dea-a4b0-823036906ba1", 16);

        //    SirketAuth HDI_Live = new SirketAuth("61500ED135911971E0531A6D8DB949B7", 10);
        //    SirketAuth HDI_Test = new SirketAuth("61500ED135921971E0531A6D8DB949B7", 95);

        //    //  SirketAuth Ethica_Live = new SirketAuth("", null);
        //    SirketAuth Ethica_Test = new SirketAuth("4862b1ab-cc62-48e6-b2b4-63e4ad18cc7a", 14);

        //    SirketAuth Unico_Test = new SirketAuth("6AC0DB95C01B1AFDE0531A6D8DB940EB", 44);
        //    SirketAuth Unico_Live = new SirketAuth("C54CEA76CEB549B5AF1B5A2203A35D13", 4);

        //    SirketAuth Halk_Test = new SirketAuth("1FD5G1D5GDS1F15BD56B1S6B5FS61BDB", 44);
        //    SirketAuth Halk_Live = new SirketAuth("1VF5DV1S1BD6BSBDF15V1V32DV1SSDV5", 5);

        private void CreateFailedLog(IntegrationLog log, string responseXml, string[] desc)
        {
            log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
            log.Type = ((int)LogType.FAILED).ToString();
            log.Response = responseXml;
            log.ResponseStatusCode = "0";
            log.ResponseStatusDesc = desc != null ? string.Join(" - ", desc) : "HATA";
            log.WsResponseDate = DateTime.Now;
            IntegrationLogHelper.AddToLog(log);
        }

        [WebMethod]
        public PoliceAktarRes PoliceAktar(string apiKod, PoliceAktarReq req)
        {
            if (req != null)
            {
                return serviceClient.PoliceAktar(apiKod, req);
            }
            else
            {
                var response = new PoliceAktarRes { SonucAciklama = "NULL REQUEST", SonucKod = "0", ValidationErrors = new string[] { "NULL REQUEST" } };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };
                
                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST",$"ApiCode={apiKod}" });

                return response;
            }
        }
        [WebMethod]
        public ZeyilSigortaliEkleRes ZeyilSigortaliEkle(string apiKod, ZeyilSigortaliEkleReq req)
        {
            if (req != null)
            {
                return serviceClient.ZeyilSigortaliEkle(apiKod, req);
            }
            else
            {
                var response = new ZeyilSigortaliEkleRes { SonucAciklama = "NULL REQUEST", SonucKod = "0", ValidationErrors = new string[] { "NULL REQUEST" } };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST", $"ApiCode={apiKod}" });


                return response;
            }
        }
        [WebMethod]
        public ZeyilSigortaliCikarRes ZeyilSigortaliCikar(string apiKod, ZeyilSigortaliCikarReq req)
        {
            if (req != null)
            {
                return serviceClient.ZeyilSigortaliCikar(apiKod, req);
            }
            else
            {
                var response = new ZeyilSigortaliCikarRes { SonucAciklama = "NULL REQUEST", SonucKod = "0", ValidationErrors = new string[] { "NULL REQUEST" } };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST", $"ApiCode={apiKod}" });

                return response;
            }
        }
        [WebMethod]
        public PoliceIptalRes PoliceIptal(string apiKod, PoliceIptalReq req)
        {
            if (req != null)
            {
                return serviceClient.PoliceIptal(apiKod, req);
            }
            else
            {
                var response = new PoliceIptalRes { SonucAciklama = "NULL REQUEST", SonucKod = "0", ValidationErrors = new string[] { "NULL REQUEST" } };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST", $"ApiCode={apiKod}" });

                return response;
            }
        }
        [WebMethod]
        public PaketOdemeBildirimRes PaketOdemeBildirim(string apiKod, PaketOdemeBildirimReq req)
        {
            if (req != null)
            {
                return serviceClient.PaketOdemeBildirim(apiKod, req);
            }
            else
            {
                var response = new PaketOdemeBildirimRes { SonucAciklama = "NULL REQUEST", SonucKod = "0", ValidationErrors = new string[] { "NULL REQUEST" } };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST", $"ApiCode={apiKod}" });

                return response;
            }
        }
        [WebMethod]
        public PoliceBilgiRes PoliceBilgiSorgula(string apiKod, PoliceBilgiReq req)
        {
            if (req != null)
            {
                return serviceClient.PoliceBilgiSorgula(apiKod, req);
            }
            else
            {

                var response = new PoliceBilgiRes { SonucAciklama = "NULL REQUEST", SonucKod = "0", ValidationErrors = new string[] { "NULL REQUEST" } };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST", $"ApiCode={apiKod}" });

                return response;
            }
        }
        [WebMethod]
        public PoliceSorgulaRes PoliceSorgula(string apiKod, PoliceSorgulaReq req)
        {
            if (req != null)
            {
                return serviceClient.PoliceSorgula(apiKod, req);
            }
            else
            {
                var response = new PoliceSorgulaRes { SonucAciklama = "NULL REQUEST", SonucKod = "0"};

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST", $"ApiCode={apiKod}" });

                return response;
            }
        }
        [WebMethod]
        public AcenteTransferRes AcenteTransfer(string apiKod, AcenteTransferReq req)
        {
            if (req != null)
            {
                return serviceClient.AcenteTransfer(apiKod, req);
            }
            else
            {
                var response = new AcenteTransferRes { SonucAciklama = "NULL REQUEST", SonucKod = "0", ValidationErrors = new string[] { "NULL REQUEST" } };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST", $"ApiCode={apiKod}" });

                return response;
            }
        }
    }
}
