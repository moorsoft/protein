﻿using Protein.Common.Extensions;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.InsuranceCompanies;
using Protein.Data.ExternalServices.InsuranceCompanies.Util.Lib;
using Protein.Data.ExternalServices.Log;
using Protein.Data.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Services;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.WS.WS.Common
{
    /// <summary>
    /// Summary description for AgreementMatchSrv
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AgreementMatchSrv : System.Web.Services.WebService
    {
        ServiceClient serviceClient = new ServiceClient();

        private void CreateFailedLog(IntegrationLog log, string responseXml, string[] desc)
        {
            log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
            log.Type = ((int)LogType.FAILED).ToString();
            log.Response = responseXml;
            log.ResponseStatusCode = "0";
            log.ResponseStatusDesc = desc != null ? string.Join(" - ", desc) : "HATA";
            log.WsResponseDate = DateTime.Now;
            IntegrationLogHelper.AddToLog(log);
        }

        [WebMethod]
        public PolicyAgreementRes PolicyAgreement(PoliceMutabakatReq req)
        {
            if (req != null)
            {
                return serviceClient.PolicyAgreement(req);
            }
            else
            {
                var response = new PolicyAgreementRes { Description = "", SonucAciklama = "NULL REQUEST", SonucKod = "0" };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST" });

                return response;
            }
        }

        [WebMethod]
        public ZeyilAgreementRes ZeyilAGreement(ZeyilMutabakatReq req)
        {
            if (req != null)
            {
                return serviceClient.ZeyilAgreement(req);
            }
            else
            {
                var response = new ZeyilAgreementRes {  SonucAciklama = "NULL REQUEST", SonucKod = "0" };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST" });

                return response;
            }
        }

        [WebMethod]
        public TazminatAgreementRes TazminatAgreement(ProvizyonMutabakatReq req)
        {
            if (req != null)
            {
                return serviceClient.TazminatAgreement(req);
            }
            else
            {
                var response = new TazminatAgreementRes { SonucAciklama = "NULL REQUEST", SonucKod = "0" };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST" });

                return response;
            }
        }

        [WebMethod]
        public IcmalAgreementRes IcmalAgreement(IcmalMutabakatReq req)
        {
            if (req != null)
            {
                return serviceClient.IcmalAgreement(req);
            }
            else
            {
                var response = new IcmalAgreementRes { SonucAciklama = "NULL REQUEST", SonucKod = "0" };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST" });

                return response;
            }
        }

    }
}
