﻿using Newtonsoft.Json;
using Protein.Common.Constants;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.WS.WS.Ankur
{
    public partial class ankurWs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                string ApiCode = ConfigurationManager.AppSettings["AnkurApiCode"];
                if (ApiCode != Request.QueryString["apikod"] || Request.QueryString["sirketkod"] == null)
                {
                    Response.Clear();
                    Response.Write("invalid params");
                    Response.Flush();
                    Response.End();
                    return;
                }

                long companyId = 0;
                var companyParameter = new GenericRepository<V_CompanyParameter>().FindBy($"SYSTEM_TYPE='{((int)SystemType.WS).ToString()}' AND KEY='ANKUR_ID' AND VALUE='{Request.QueryString["sirketkod"].ToString()}'", orderby: "").FirstOrDefault();


                if (companyParameter!=null)
                {
                    companyId = companyParameter.CompanyId;
                    List<V_ProviderWebSource> providerWeb = new GenericRepository<V_ProviderWebSource>().FindBy("COMPANY_ID=:companyId" , parameters: new { companyId }, fetchDeletedRows: true, fetchHistoricRows: true, orderby: "COMPANY_ID");

                
                    List<dynamic> providerList = new List<dynamic>();
                    List<ProviderProduct> ProviderProductlİST = new List<ProviderProduct>();
                    foreach (var item in providerWeb)
                    {
                        if (ProviderProductlİST.Where(x => x.PROVIDER_ID == item.PROVIDER_ID && x.PRODUCT_ID == item.ProductId).FirstOrDefault() == null)
                        {
                            dynamic pr = new System.Dynamic.ExpandoObject();
                            pr.SIRKETKOD = Request.QueryString["sirketkod"].ToString();
                            pr.POLICE_TIPI_KOD = item.ProductId;
                            pr.POLICE_TIPI_AD = item.ProductName;
                            pr.KURUM_ADI = item.ProviderName;
                            pr.KURUM_TIPI = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Provider, item.ProviderType);
                            pr.ADRES = item.ADDRESS;
                            pr.IL = item.CITY_NAME;
                            pr.ILCE = item.COUNTY_NAME;
                            pr.TELEFON = item.PhoneNo;
                            if (item.OSS_NOTE != null && item.TSS_NOTE != null)
                            {
                                pr.KURUMNOT = "ÖSS NOTLARI : " + item.OSS_NOTE + " " + " TSS_NOTLARI: " + item.TSS_NOTE;
                            }
                            if (item.OSS_NOTE != null && item.TSS_NOTE == null)
                            {
                                pr.KURUMNOT = "ÖSS NOTLARI : " + item.OSS_NOTE + " ";
                            }
                            if (item.OSS_NOTE == null && item.TSS_NOTE != null)
                            {
                                pr.KURUMNOT = "TSS NOTLARI : " + item.TSS_NOTE + " ";
                            }
                            ProviderProductlİST.Add(new ProviderProduct
                            {
                                PRODUCT_ID = item.ProductId,
                                PROVIDER_ID = item.PROVIDER_ID
                            });


                            providerList.Add(pr);
                        }
                        else
                        {
                            var a = "";
                        }
                    }

                    string json = JsonConvert.SerializeObject(providerList);

                    Response.Clear();
                    Response.ContentType = "application/json";
                    Response.Write(json);
                    Response.Flush();
                    //Response.End();
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }
    }
    public class ProviderProduct
    {
        public Int64 PROVIDER_ID { get; set; }
        public Int64 PRODUCT_ID { get; set; }
    }
}