﻿using Protein.Common.Dto;
using Protein.Data.Export.Model;
using Protein.Data.Repositories;
using Protein.WS.Models.ExportModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.WS.WS.Ankur
{
    public class ProviderWSController : Controller
    {
        public ActionResult PaymentList(Int64 providerId)
        {
            ExportVM vm = new ExportVM();
            try
            {
                #region Export 

                vm.ViewName = Views.ProviderClaimList;
                vm.SetExportColumns();
                vm.ExportType = ExportType.Excel;
                #endregion
                Provider provider = new ProviderRepository().FindById(providerId);
                if (provider == null)
                {
                    throw new Exception("Kurum kodu sistemde bulunamadı!");
                }
                ViewBag.ProviderId = providerId;
                ViewBag.isFilter = "0";
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View(vm);
        }

        [HttpPost]
        public ActionResult GetPaymentList(FormCollection form)
        {
            #region Export
            ExportVM vm = new ExportVM();
            vm.WhereCondition = "";
            vm.SetExportColumns();
            #endregion

            ViewResultDto<List<V_ProviderClaimList>> PaymentList = new ViewResultDto<List<V_ProviderClaimList>>();
            PaymentList.Data = new List<V_ProviderClaimList>();

            if (form["isFilter"] == "1" && !String.IsNullOrEmpty(form["providerId"]))
            {
                int start = Convert.ToInt32(Request["start"]);
                int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
                string searchValue = Request["search[value]"];
                string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
                string sortDirection = Request["order[0][dir]"];

                long companyId = 1;
                long providerId = 0;
                long payrollId = 0;
                long claimId = 0;
                string paymentStartDate = Request["paymentStartDate"];
                string paymentEndDate = Request["paymentEndDate"];

                string whereConditition = $"COMPANY_ID = {companyId}";
                if (!String.IsNullOrEmpty(form["providerId"]))
                {
                    providerId = Convert.ToInt32(form["providerId"]);
                    whereConditition += $" AND PROVIDER_ID = {providerId}";
                }
                if (!String.IsNullOrEmpty(form["payrollNo"]))
                {
                    payrollId = Convert.ToInt32(form["payrollNo"]);
                    whereConditition += $" AND PAYROLL_ID = {payrollId}";
                }
                if (!String.IsNullOrEmpty(form["claimNo"]))
                {
                    claimId = Convert.ToInt32(form["claimNo"]);
                    whereConditition += $" AND CLAIM_ID = {claimId}";
                }
                if (!String.IsNullOrEmpty(form["paymentStartDate"]))
                {
                    whereConditition += $" AND PAYMENT_DATE >= TO_DATE('{ DateTime.Parse(form["paymentStartDate"])}','DD.MM.YYYY HH24:MI:SS') ";
                }
                if (!String.IsNullOrEmpty(form["paymentEndDate"]))
                {
                    whereConditition += $" AND PAYMENT_DATE <= TO_DATE('{ DateTime.Parse(form["paymentEndDate"])}','DD.MM.YYYY HH24:MI:SS') ";
                }

                PaymentList = new V_ProviderClaimListRepository().FindByPaged(conditions: whereConditition,
                                                                             orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection}" : "CLAIM_ID",
                                                                             fetchDeletedRows: true,
                                                                             fetchHistoricRows: true,
                                                                             pageNumber: start,
                                                                             rowsPerPage: length);

                ViewBag.ProviderId = providerId;
                vm.WhereCondition = whereConditition;
            }
            return Json(new { data = (PaymentList.Data == null ? new List<V_ProviderClaimList>() : PaymentList.Data), draw = Request["draw"], recordsTotal = PaymentList.TotalItemsCount, recordsFiltered = PaymentList.TotalItemsCount, whereConditition = vm.WhereCondition }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ExportGenerator(ExportVM model)
        {
            ExportResponse response = new ExportResponse();
            if (model.selectedExportColumnValue.Count > 0 || model.IsAllColumns)
            {
                model.ExportType = ExportType.Excel;

                model.SetExportColumns();
                response = model.ExportOut();
                if (response.IsDownloaded)
                    TempData[response.Guid] = response.memoryStream.ToArray();
            }
            else
                return new EmptyResult();

            return new JsonResult()
            {
                Data = new { FileGuid = response.Guid, FileName = response.FileName }
            };
        }

        [HttpGet]
        public virtual ActionResult Download(string fileGuid, string fileName)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                TempData[fileGuid] = null;
                return File(data, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            }
            else
                return new EmptyResult();
        }
    }
}