﻿using Protein.Business.Concrete.Services;
using Protein.Common.Dto.ImportObjects;
using Protein.Common.Extensions;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.Log;
using Protein.Data.ExternalServices.Protein;
using Protein.Data.ExternalServices.Protein.Util.Lib;
using Protein.Data.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Services;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.WS.WS.Doga
{
    /// <summary>
    /// Summary description for DogaSigorta
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class DogaSigorta : System.Web.Services.WebService
    {
        DogaServiceClient dogaServiceClient = new DogaServiceClient();
    

        [WebMethod]
        public DogaPoliceAktarRes PoliceAktar(string apiKod, DogaPoliceAktarReq req)
        {
            return dogaServiceClient.PoliceAktar(apiKod, req);
        }

        [WebMethod]
        public DogaHasarAktarRes HasarAktar(string apiKod, long ClaimId)
        {
            return dogaServiceClient.HasarAktar(apiKod, ClaimId);
        }

        [WebMethod]
        public DogaIcmalAktarRes IcmalAktar(string apiKod, long PayrollId)
        {
            return dogaServiceClient.IcmalAktar(apiKod, PayrollId);
        }

        [WebMethod]
        public DogaIcmalAktarRes IcmalAktarList(string apiKod)
        {
            return dogaServiceClient.IcmalAktarList(apiKod);
        }


        Data.ExternalServices.InsuranceCompanies.ServiceClient serviceClient = new Data.ExternalServices.InsuranceCompanies.ServiceClient();

        private void CreateFailedLog(IntegrationLog log, string responseXml, string[] desc)
        {
            log.HttpStatus = ((int)HttpStatusCode.NotFound).ToString();
            log.Type = ((int)LogType.FAILED).ToString();
            log.Response = responseXml;
            log.ResponseStatusCode = "0";
            log.ResponseStatusDesc = desc != null ? string.Join(" - ", desc) : "HATA";
            log.WsResponseDate = DateTime.Now;
            IntegrationLogHelper.AddToLog(log);
        }

        [WebMethod]
        public AcenteTransferRes AcenteTransfer(string apiKod, AcenteTransferReq req)
        {
            if (req != null)
            {
                return serviceClient.AcenteTransfer(apiKod, req);
            }
            else
            {
                var response = new AcenteTransferRes { SonucAciklama = "NULL REQUEST", SonucKod = "0", ValidationErrors = new string[] { "NULL REQUEST" } };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST", $"ApiCode={apiKod}" });

                return response;
            }
        }

        [WebMethod]
        public PaketOdemeBildirimRes PaketOdemeBildirim(string apiKod, PaketOdemeBildirimReq req)
        {
            if (req != null)
            {
                return serviceClient.PaketOdemeBildirim(apiKod, req);
            }
            else
            {
                var response = new PaketOdemeBildirimRes { SonucAciklama = "NULL REQUEST", SonucKod = "0", ValidationErrors = new string[] { "NULL REQUEST" } };

                var currentMethod = MethodBase.GetCurrentMethod();

                IntegrationLog log = new IntegrationLog
                {
                    WsName = currentMethod.DeclaringType.FullName,
                    WsMethod = currentMethod.Name,
                    Request = req.ToXML(true),
                    WsRequestDate = DateTime.Now,
                };

                CreateFailedLog(log, response.ToXML(true), new string[] { "NULL REQUEST", $"ApiCode={apiKod}" });

                return response;
            }
        }


    }
}
