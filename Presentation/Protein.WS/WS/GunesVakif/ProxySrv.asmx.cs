﻿using Protein.Data.ExternalServices.Protein;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using static Protein.Data.ExternalServices.Protein.ProxySrvModels;

namespace Protein.WS.GunesVakif
{
    /// <summary>
    /// Summary description for ProxySrv
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ProxySrv : System.Web.Services.WebService
    {
        ServiceClient serviceClient = new ServiceClient();

        //Protein.Online.WS içerisindeki servisler
        //Çalıştığı link : http://online.imecedestek.com/protein/ws/proxySrv.asmx


        [WebMethod]
        public Sonuc GunesHakSahiplikSorgula(string sirketKod, long GrupPoliceNo, long MusteriId, int olayTarGun, int olayTarAy, int olayTarYil, long PoliceNo, long YenilemeNo)
        {
            return serviceClient.ProvisioningConformityInquiry(sirketKod, GrupPoliceNo, MusteriId, olayTarGun, olayTarAy, olayTarYil, PoliceNo, YenilemeNo);
        }

        [WebMethod]
        public string PusulaPaketSrv(string sirketKod, string cozumOrtagiKodu, long entegrasyonPaketId, string gelisTar_yil, string gelisTar_ay, string gelisTar_gun, long[] provIdList)
        {
            return serviceClient.CreatePackage(sirketKod, cozumOrtagiKodu, entegrasyonPaketId, gelisTar_yil, gelisTar_ay, gelisTar_gun, provIdList);
        }

        [WebMethod]
        public SaglikProvizyonKaydetResult ProvKaydet(string sirketKod, SaglikProvizyonKaydetParam param)
        {
            return serviceClient.ProvisionSave(sirketKod, param);
        }

        [WebMethod]
        public CreateClaimResult CreateClaim(string sirketKod, CreateClaimParam param)
        {
            return serviceClient.CreateClaim(sirketKod, param);
        }
    }
}
