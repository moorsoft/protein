﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Protein.Data.ExternalServices.Protein;
using static Protein.Data.ExternalServices.Protein.ProteinSrvModels;

namespace Protein.WS.GunesVakif
{
    /// <summary>
    /// Summary description for ProteinSrv
    /// </summary>
    [WebService(Namespace = "http://www.imecedestek.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ProteinSrv : WebService
    {
        ServiceClient serviceClient = new ServiceClient();

        //Protein.Online.WS içerisindeki servisler
        //Çalıştığı link : http://online.imecedestek.com/protein/ws/proteinsrv.asmx 


        [WebMethod]
        public MUSTERISTATUS MUSTERICREATEORUPDATE(string apiKod, MUSTERI MUSTERI)
        {
            return serviceClient.CustomerCreateOrUpdate(apiKod, MUSTERI);
        }

        [WebMethod]
        public POLICESTATUS POLICECREATEUPDATE(string apiKod, POLICE POLICE, ZEYIL ZEYIL, SIGORTALILAR SIGORTALILAR)
        {
            return serviceClient.PoliceCreateOrUpdate(apiKod, POLICE, ZEYIL, SIGORTALILAR);
        }

        [WebMethod]
        public ODEMEDURUMUSONUC ODEMEDURUMU(string apiKod, ODEMEDURUMU ODEMEDURUMU1)
        {
            return serviceClient.PaymentStatus(apiKod, ODEMEDURUMU1);
        }

        [WebMethod]
        public UWUPDATESONUC UWUPDATE(string apiKod, UWGUNCELLE UW)
        {
            return serviceClient.UWUpdate(apiKod, UW);
        }

    }
}
