﻿using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.InsuranceCompanies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using static Protein.Data.ExternalServices.Common.Lib.NN.NNInputOutputTypes;
using static Protein.Data.ExternalServices.SagmerOnlineService.InputOutputTypes;

namespace Protein.WS.WS.NNHayat
{
    /// <summary>
    /// Summary description for NNHayatSrv
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class NNHayatSrv : System.Web.Services.WebService
    {
        NNHayatServiceClientV2 serviceClient = new NNHayatServiceClientV2();

        [WebMethod]
        public CommonProxyRes hakSahipligiSorgula(NNHakSahiplikReq Sigortali)
        {
            return serviceClient.PolicyState(Sigortali);
        }

        [WebMethod]
        public CommonProxyRes TazminatGiris(NNTazminatGirisReq Tazminat)
        {
            return serviceClient.TazminatGiris(Tazminat);
        }

        [WebMethod]
        public CommonProxyRes SetProvisionStatus(NNSetProvisionStatusReq provisionStatus)
        {
            return serviceClient.SetProvisionStatus(provisionStatus);
        }

        [WebMethod]
        public CommonProxyRes UpdateSagmerPolicyInfo(NNUpdateSagmerPolicyInfoReq sagmerInfo)
        {
            return serviceClient.UpdateSagmerPolicyInfo(sagmerInfo);
        }

        [WebMethod]
        public CommonProxyRes IcmalGiris(NNIcmalGirisRequest icmalinfo)
        {
            return serviceClient.IcmalGiris(icmalinfo);
        }

        [WebMethod]
        public CommonProxyRes ProviderTransfer(NNProviderTransferRequest providerInfo)
        {
            return serviceClient.ProviderTransfer(providerInfo);
        }

        [WebMethod]
        public CommonProxyRes ProviderListTransfer(NNProviderTransferRequest providerInfo)
        {
            return serviceClient.ProviderListTransfer(providerInfo);
        }

        [WebMethod]
        public CommonProxyRes OldClaimTransfer()
        {
            return serviceClient.OldClaimTransfer();
        }
    }
}
