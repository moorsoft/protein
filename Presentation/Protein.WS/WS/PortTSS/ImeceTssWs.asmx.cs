﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Protein.Data.ExternalServices.PortTSS;
using static Protein.Data.ExternalServices.PortTSS.InputOutputTypes;

namespace Protein.WS.WS.PortTSS
{
    /// <summary>
    /// Summary description for ImeceTssWs
    /// </summary>
    [WebService(Namespace = "http://porttss.bcloud.com.tr/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ImeceTssWs : System.Web.Services.WebService
    {
        ServiceClient serviceClient = new ServiceClient();

        [WebMethod]
        public SigortaliSorgulaCevap[] SigortaliHakSorgula(SigortaliSorgula Sigortali)
        {
            return serviceClient.SigortaliHakSorgula(Sigortali);
        }

        [WebMethod]
        public TazminatHesaplaCevap TamamlayiciSaglikTazminatGiris(TazminatHesapla Tazminat)
        {
            return serviceClient.TamamlayiciSaglikTazminatGiris(Tazminat);
        }

        [WebMethod]
        public ProvizyonIptalCevap TamamlayiciSaglikProvizyonIptal(ProvizyonIptal ProvizyonIptal)
        {
            return serviceClient.TamamlayiciSaglikProvizyonIptal(ProvizyonIptal);
        }

        [WebMethod]
        public RedDurumBildirimCevap RedDurumBildirim(RedDurumBilgisi RedDurum)
        {
            return serviceClient.RedDurumBildirim(RedDurum);
        }

        [WebMethod]
        public RedDurumIptalCevap RedDurumIptal(RedDurumIptalBilgisi RedDurumIptal)
        {
            return serviceClient.RedDurumIptal(RedDurumIptal);
        }

        [WebMethod]
        public IcmalGirisCevap IcmalGiris(IcmalBilgileri Icmal)
        {
            return serviceClient.IcmalGiris(Icmal);
        }

        [WebMethod]
        public IcmalTazminatCikarmaCevap IcmalTazminatCikarma(IcmalTazminatBilgileri IcmalTazminat)
        {
            return serviceClient.IcmalTazminatCikarma(IcmalTazminat);
        }
    }
}