﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Protein.WS.Models.ExportModel
{
    public enum ExportType
    {
       Excel = 0,
       Pdf = 1,
       Print = 2
    }
}