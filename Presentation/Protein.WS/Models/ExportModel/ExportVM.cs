﻿using Protein.Common.Constants;
using Protein.Common.Dto;
using Protein.Data.Export;
using Protein.Data.Export.Model;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.WS.Models.ExportModel
{
    [Serializable]
    public class ExportVM
    {
        public string ViewName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string WhereCondition { get; set; }
        //public  List<SelectListItem> Columns { get; set; }
        [DisplayName("Kolonlar")]
        public List<ExportColumn> ExportColumns { get; set; }
        public List<ExportColumn> SelectedExportColumns { get; set; }
        public List<string> selectedExportColumnValue { get; set; }
        public ExportType ExportType { get; set; }
        [DisplayName("Tümü")]
        public bool IsAllColumns { get; set; } = false;
        public ExportVM()
        {
            ExportColumns = new List<ExportColumn>();
            selectedExportColumnValue = new List<string>();
            SelectedExportColumns = new List<ExportColumn>();

        }
        public void SetExportColumns()
        {
            try
            {
                if (string.IsNullOrEmpty(this.ViewName)) return;

                List<V_TableColumn> resultTableColumnData = new TableColumnRepository().FindBy(conditions: $"TABLE_NAME = '{this.ViewName}' ", fetchDeletedRows: true, fetchHistoricRows: true, orderby: "");
                bool FillSelectedColumn = this.selectedExportColumnValue.Count == 0 ? true : false;
                foreach (var item in resultTableColumnData)
                {
                    string excludeColumnName = item.ColumnName.ToUpper();
                    if (excludeColumnName.Contains("_ID") || excludeColumnName.Contains("_CODE") || excludeColumnName == "STATUS") continue; // ID geçen kolonları es geç

                    this.ExportColumns.Add(new ExportColumn { DisplayName = item.HeaderName, Name = item.ColumnName });
                    if (FillSelectedColumn)
                        this.selectedExportColumnValue.Add(item.ColumnName);
                }

                resultTableColumnData = null;

                if (this.selectedExportColumnValue.Count > 0 && !this.IsAllColumns)
                {
                    foreach (string selectedExpCol in this.selectedExportColumnValue)
                    {
                        ExportColumn matchCol = this.ExportColumns.Where(a => a.Name == selectedExpCol).FirstOrDefault();
                        this.SelectedExportColumns.Add(matchCol);
                    }
                }
                else if (this.IsAllColumns)
                {
                    this.SelectedExportColumns.Clear();
                    this.SelectedExportColumns.AddRange(this.ExportColumns);
                }
            }
            catch (Exception ex) { }
        }
        public ExportResponse ExportOut()
        {
            ExportResponse response = new ExportResponse();
            try
            {
                this.WhereCondition = this.WhereCondition != null ? this.WhereCondition.Replace("WHERE", "") : "";
                switch (this.ViewName)
                {
                    case "T_PROVIDER_GROUP":
                        List<ProviderGroup> returnProvGrp = new GenericRepository<ProviderGroup>().FindBy(this.WhereCondition);
                        IExport<ProviderGroup> exportProvGrp = new ExcelExport<ProviderGroup>();
                        response = exportProvGrp.DoWork(returnProvGrp, this.SelectedExportColumns);
                        break;
                    case Constants.Views.ExportProvider:
                        List<V_ExportProvider> returnProvExport = new GenericRepository<V_ExportProvider>().FindBy(this.WhereCondition, orderby: "");
                        IExport<V_ExportProvider> exportProvExport = new ExcelExport<V_ExportProvider>();
                        response = exportProvExport.DoWork(returnProvExport, this.SelectedExportColumns);
                        break;
                    case Constants.Views.ExportPackage:
                        List<V_ExportPackage> returnPackage = new ExportPackageRepository().FindBy(this.WhereCondition, orderby: "");
                        IExport<V_ExportPackage> exportPackage = new ExcelExport<V_ExportPackage>();
                        response = exportPackage.DoWork(returnPackage, this.SelectedExportColumns);
                        break;
                    case Constants.Views.ExportNetwork:
                        List<V_ExportNetwork> returnNetwork = new GenericRepository<V_ExportNetwork>().FindBy(this.WhereCondition, orderby: "");
                        IExport<V_ExportNetwork> exportNetwork = new ExcelExport<V_ExportNetwork>();
                        response = exportNetwork.DoWork(returnNetwork, this.SelectedExportColumns);
                        break;
                    case Constants.Views.ExportContractList:
                        List<V_ContractList> returnContract = new GenericRepository<V_ContractList>().FindBy(this.WhereCondition, orderby: "");
                        IExport<V_ContractList> exportContract = new ExcelExport<V_ContractList>();
                        response = exportContract.DoWork(returnContract, this.SelectedExportColumns);
                        break;
                    case Constants.Views.Claim:
                        List<V_Claim> returnClaim = new GenericRepository<V_Claim>().FindBy(this.WhereCondition, orderby: "");
                        IExport<V_Claim> exportClaim = new ExcelExport<V_Claim>();
                        response = exportClaim.DoWork(returnClaim, this.SelectedExportColumns);
                        break;
                    case Constants.Views.Payroll:
                        List<V_Payroll> returnPayroll = new GenericRepository<V_Payroll>().FindBy(this.WhereCondition, orderby: "");
                        IExport<V_Payroll> exportPayroll = new ExcelExport<V_Payroll>();
                        response = exportPayroll.DoWork(returnPayroll, this.SelectedExportColumns);
                        break;
                    case Constants.Views.Agency:
                        List<V_Agency> returnAgency = new GenericRepository<V_Agency>().FindBy(this.WhereCondition, orderby: "");
                        IExport<V_Agency> exportAgency = new ExcelExport<V_Agency>();
                        response = exportAgency.DoWork(returnAgency, this.SelectedExportColumns);
                        break;
                    case Constants.Views.RptClaimDetails:
                        List<V_RptClaimDetails> returnClaimDetails = new GenericRepository<V_RptClaimDetails>().FindBy(this.WhereCondition, orderby: "",fetchDeletedRows:true,fetchHistoricRows:true);
                        IExport<V_RptClaimDetails> exportClaimDetails = new ExcelExport<V_RptClaimDetails>();
                        response = exportClaimDetails.DoWork(returnClaimDetails, this.SelectedExportColumns);
                        break;
                    case Constants.Views.RptClaimPremium:
                        List<V_RptClaimPremium> returnClaimPremium = new GenericRepository<V_RptClaimPremium>().FindBy(this.WhereCondition, orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                        IExport<V_RptClaimPremium> exportClaimPremium = new ExcelExport<V_RptClaimPremium>();
                        response = exportClaimPremium.DoWork(returnClaimPremium, this.SelectedExportColumns);
                        break;
                    case Constants.Views.RptProduction:
                        List<V_RptProduction> returnProduction = new GenericRepository<V_RptProduction>().FindBy(this.WhereCondition, orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                        IExport<V_RptProduction> exportProduction = new ExcelExport<V_RptProduction>();
                        response = exportProduction.DoWorkRptProduction(returnProduction, this.SelectedExportColumns);
                        break;
                    case Constants.Views.ProviderClaimList:
                        List<V_ProviderClaimList> returnProviderClaimList = new GenericRepository<V_ProviderClaimList>().FindBy(this.WhereCondition, fetchDeletedRows: true, fetchHistoricRows: true, orderby: "");
                        IExport<V_ProviderClaimList> exportProviderClaimList = new ExcelExport<V_ProviderClaimList>();
                        response = exportProviderClaimList.DoWork(returnProviderClaimList, this.SelectedExportColumns);
                        break;
                    default: break;
                }
            }
            catch (Exception ex) { }
            return response;
        }
        private void WriteColumn(ref string query)
        {
            foreach (ExportColumn col in this.SelectedExportColumns)
            { query += col.Name + ","; }
            query = query.TrimEnd(',');
        }
    }
}