// phone, e-mail, tckn maskes numbers

jQuery(function ($) {
    if ($('.phoneMask').length > 0) {
        $(".phoneMask").mask("(999) 999-9999"); // phone mask
    }

    if ($('.tcknMask').length > 0) {
        $(".tcknMask").mask("99999999999"); // TCKN mask
    }

    if ($('.dateMask').length > 0) {
        $(".dateMask").mask("99-99-9999"); // date mask
    }
});

if ($('.money').length > 0) {
    $('.money').on('keydown', function (e) {
        if (e.key.length == 1 && e.key.match(/[^0-9'".]/)) {
            return false;
        };
    })
}



$(function () {
    $.fn.tooltip.Constructor.DEFAULTS.trigger = 'hover';
    $('body').tooltip({
        selector: "[data-tooltip=tooltip]"
    });
});

$(document).ready(function () {
    //refresh functiom
    $('.mbui-form .mbui-refresh').on('click', function () {
        var element = $(this);
        var mbui = element.parents('.mbui-form');
        var dropdown = element.parents('.dropdown');

        mbui.addClass('refreshing');
        dropdown.trigger('click');

        var t = setTimeout(function () {
            mbui.removeClass('refreshing');
        }, 3000);
    });


    // close function
    $('.mbui-form .mbui-close').on('click', function () {
        var element = $(this);
        var mbui = element.parents('.mbui-form');

        mbui.addClass('closed').fadeOut();
    });

    if ($('.mbui-form').length > 0) {
        //toggle function
        $('.mbui-form .mbui-toggle').on('click', function () {
            var element = $(this);
            var mbui = element.parents('.mbui-form');

            mbui.toggleClass('collapsed');
        });
    }

    if ($('.mbui-inside-form').length > 0) {
        //toggle function inside form
        $('.mbui-inside-form .mbui-inside-toggle').on('click', function () {
            var element = $(this);
            var mbui = element.parents('.mbui-inside-form');

            mbui.toggleClass('collapsed');
        });
    }

    $('.mbui-form .mbui-fullscreen').on('click', function () {
        var element = $(this);
        var tile = element.parents('.mbui-form');
        var dropdown = element.parents('.dropdown');

        screenfull.toggle(tile[0]);
        dropdown.trigger('click');
    });

    if ($('.mbui-form .mbui-fullscreen').length > 0) {
        $(document).on(screenfull.raw.fullscreenchange, function () {
            var element = $(screenfull.element);
            if (screenfull.isFullscreen) {
                element.addClass('isInFullScreen');
                $('.mbui-fullscreen i').removeClass('fa fa-expand');
                $('.mbui-fullscreen i').addClass('fa fa-compress');
            } else {
                $('.mbui-form.isInFullScreen').removeClass('isInFullScreen');
                $('.mbui-fullscreen i').removeClass('fa fa-compress');
                $('.mbui-fullscreen i').addClass('fa fa-expand');
            }
        });
    }

    /*data table panel filter*/
    if ($('.mbui-filter').length > 0) {
        $('.mbui-filter').on('click', function () {
            var element = $(this);
            var mbui = element.parents('.mbui-inside-form');
            mbui.children('.mbui-inside-body').toggleClass('active');
            $(this).toggleClass('active');

        });
    }

    /* datable settings*/
    if ($('.table-md').length > 0) {
        $(document).ready(function () {
            $('.mbui-list .dropdown-menu li a').click(function () {
                var dataShow = $(this).attr('data-value');
                $('.dataTables_length select option').prop('selected', '');
                $(this).closest('.mbui-form').find('.dataTables_length select option[value="' + dataShow + '"]').prop('selected', 'selected').trigger('change');
            });

            $('.table-md').DataTable({
                pageLength: 10,
                "searching": false,
                "aLengthMenu": [[10, 25, 50, 75, 100, -1], [10, 25, 50, 75, 100, "Tümü"]],
                "pagingType": "full_numbers",
                columnDefs: [
                    { type: 'turkish', targets: '_all' }
                ],
                "processing": "true",
                language: {
                    "processing": "Lütfen Bekleyiniz... Kayıtlar Listeleniyor...",
                    "emptyTable": "Kayıt Bulunamadı",
                    "infoEmpty": "Kayıt Bulunamadı",
                    "info": "Gösterilen _START_ ile _END_ arası _TOTAL_ kayıt",
                    oPaginate: {
                        sNext: 'İleri <i class="fa fa-angle-right"></i>',
                        sPrevious: '<i class="fa fa-angle-left"></i> Geri',
                        sFirst: '<i class="fa fa-angle-double-left"></i> İlk',
                        sLast: 'Son <i class="fa fa-angle-double-right"></i>'
                    }
                },

            });
        });
    }

    /* datable-lg settings*/
    if ($('.table-lg').length > 0) {
        $(document).ready(function () {
            $('.mbui-list .dropdown-menu li a').click(function () {
                var dataShow = $(this).attr('data-value');
                $('.dataTables_length select option').prop('selected', '');
                $(this).closest('.mbui-form').find('.dataTables_length select option[value="' + dataShow + '"]').prop('selected', 'selected').trigger('change');
            });

            $('.table-lg').DataTable({
                pageLength: -1,
                language: {
                    "emptyTable": "Kayıt Bulunamadı",
                    "infoEmpty": "Kayıt Bulunamadı"
                },
                "searching": false,
                "aLengthMenu": [[10, 25, 50, 75, 100, -1], [10, 25, 50, 75, 100, "Tümü"]],
                "paging": false,
                "info": false,
                "scrollY": "350px",
                "scrollCollapse": true,
                autoWidth: false,
                scrollX: true,
                columnDefs: [
                    { type: 'turkish', targets: '_all' }
                ]
            });
        });
    }

    /* datable-create settings*/
    if ($('.table-create').length > 0) {
        $(document).ready(function () {
            $('.mbui-list .dropdown-menu li a').click(function () {
                var dataShow = $(this).attr('data-value');
                $('.dataTables_length select option').prop('selected', '');
                $(this).closest('.mbui-form').find('.dataTables_length select option[value="' + dataShow + '"]').prop('selected', 'selected').trigger('change');
            });

            $('.table-create').DataTable({
                pageLength: -1,
                language: {
                    "emptyTable": "Kayıt Bulunamadı",
                    "infoEmpty": "",
                    "search": "Arama: "
                },
                "aLengthMenu": [[10, 25, 50, 75, 100, -1], [10, 25, 50, 75, 100, "Tümü"]],
                "paging": false,
                "info": false
            });
        });
    }

    $(document).on('click', '.parent .tree .open-tree', function () {
        console.log($(this));
        $(this).find('i').toggleClass('fa-chevron-down fa-chevron-right');
        $(this).closest('.parent').toggleClass('tree-open');
    });



    $(document.body).on('click', '.mbui-dropdown li', function (event) {

        var $target = $(event.currentTarget);

        $target.closest('.input-group-btn')
            .find('[data-bind="label"]').text($target.text())
            .end()
            .children('.dropdown-toggle').dropdown('toggle');

        return false;
    });


    $('#contractFormStartDate').datetimepicker({
        format: 'DD-MM-YYYY',
        showTodayButton: true,
        locale: 'tr',
        widgetPositioning: {
            vertical: 'bottom'
        }
    });

    $('#contractFormEndDate').datetimepicker({
        minDate: new Date().setDate(new Date().getDate() + 1),
        useCurrent: false,
        format: 'DD-MM-YYYY',
        showTodayButton: true,
        locale: 'tr',
        widgetPositioning: {
            vertical: 'bottom'
        }
    });

    $('#contractFormStartDate').on('dp.change', function (e) {
        $('#contractFormEndDate').data('DateTimePicker').minDate(e.date);

        //Use moment.js here
        var m = moment(new Date(e.date));
        m.add(1, 'days');
        $('#contractFormEndDate').data('DateTimePicker').minDate(m);
    });

    $('#claimDate').datetimepicker({
        maxDate: moment(),
        locale: 'tr',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        }
    });

    $('#hospitalizationEntranceDate').datetimepicker({
        maxDate: moment(),
        useCurrent: false,
        format: 'DD-MM-YYYY',
        locale: 'tr',
        widgetPositioning: {
            vertical: 'bottom'
        }
    });

    $('#hospitalizationExitDate').datetimepicker({
        maxDate: moment(),
        useCurrent: false,
        format: 'DD-MM-YYYY',
        locale: 'tr',
        widgetPositioning: {
            vertical: 'bottom'
        }
    });

    $('#eventDate').datetimepicker({
        maxDate: moment(),
        useCurrent: false,
        format: 'DD-MM-YYYY',
        locale: 'tr',
        widgetPositioning: {
            vertical: 'bottom'
        }
    });

    $('#hospitalizationEntranceDate').on('dp.change', function (e) {
        $('#hospitalizationExitDate').data('DateTimePicker').minDate(e.date);

        //Use moment.js here
        var m = moment(new Date(e.date));
        m.add(1, 'days');
        $('#hospitalizationExitDate').data('DateTimePicker').minDate(m);
    });

    $('#hospitalizationExitDate').on('dp.change', function (e) {
        $('#hospitalizationEntranceDate').data('DateTimePicker').maxDate(e.date);

        //Use moment.js here
        var m = moment(new Date(e.date));
        m.subtract(1, 'days');
        $('#hospitalizationEntranceDate').data('DateTimePicker').maxDate(m);
    });

    /* date picker time*/
    if ($('.datepicker').length > 0) {
        $('.datepicker').each(function () {
            var element = $(this);
            var format = 'DD-MM-YYYY';
            if (element[0].children[0].id != 'claimDate' && element[0].children[0].id != 'hospitalizationEntranceDate' && element[0].children[0].id != 'hospitalizationExitDate' && element[0].children[0].id != 'eventDate') {
                element.datetimepicker({
                    format: format,
                    locale: 'tr',
                    widgetPositioning: {
                        vertical: 'bottom'
                    }
                });
            } else {
                element.datetimepicker({
                    maxDate: moment(),
                    useCurrent: false,
                    format: format,
                    locale: 'tr',
                    widgetPositioning: {
                        vertical: 'bottom'
                    }
                });
            }
        });

    }


    /*checkbox in info-wrapper*/
    if ($('.info-wrapper').length > 0) {
        // close info-wrapper
        $('.checkbox_close').click(function () {
            var element = $(this);
            var mbui = element.parents('.mbui-inside-form');

            mbui.toggleClass('collapsed');
        });

        // all checkbox unselect
        $('.checkbox_cancel').click(function () {
            $(this).parents('.mbui-inside-form').find('.info-wrapper input[type="checkbox"]').prop('checked', false).change();
            $(this).parents('.tree').css('border-color', '#00acd6');
            $(this).parents('.mbui-inside-form').find('.checkbox-label').removeClass('active');
            $(this).parents('.mbui-inside-form').find('.mbui-inside-header').css('margin-bottom', '0');
            $(this).parents('.mbui-inside-form').change();
        });

        // all checkbox select
        $('.checkbox_check').click(function () {
            $(this).parents('.mbui-inside-form').find('.info-wrapper input[type="checkbox"]').prop('checked', true).change();
            $(this).parents('.tree').css('border-color', '#f59324');
            $(this).parents('.mbui-inside-form').find('.checkbox-label').addClass('active');
            $(this).parents('.mbui-inside-form').find('.mbui-inside-header').css('border-bottom', '1px solid #367fa9');
            $(this).parents('.mbui-inside-form').find('.mbui-inside-header').css('border-bottom-right-radius', '0px');
            $(this).parents('.mbui-inside-form').find('.mbui-inside-header').css('border-bottom-left-radius', '0px');
            $(this).parents('.mbui-inside-form').find('.mbui-inside-header').css('margin-bottom', '10px');
            $(this).parents('.mbui-inside-form').change();
        });

        $(document).ready(function () {
            $(function () {
                $('.tree input[type="checkbox"]').each(function () {
                    var checkedCheckbox = $(this).prop('checked');
                    var idCheckbox = $(this).prop('id');
                    if (checkedCheckbox == true) {
                        $(this).parents('.tree').siblings('ul').find(".tree").css('border-color', '#f59324');
                        $(this).parents('.tree').css('border-color', '#f59324');
                        $('.none-visible').find('.checkbox-label[for="' + idCheckbox + '"]').addClass('active');
                        $(this).parents('.mbui-inside-form').find('.mbui-inside-header').css('border-bottom', '1px solid #367fa9');
                        $(this).parents('.mbui-inside-form').find('.mbui-inside-header').css('border-bottom-right-radius', '0px');
                        $(this).parents('.mbui-inside-form').find('.mbui-inside-header').css('border-bottom-left-radius', '0px');
                    } else {
                        $(this).parents('.tree').siblings('ul').find(".tree").css('border-color', '#00acd6');
                        $(this).parents('.tree').css('border-color', '#00acd6');
                        $('.none-visible').find('.checkbox-label[for="' + idCheckbox + '"]').removeClass('active');
                    }

                });

                $(document).on('click', '.tree input[type="checkbox"]', function () {
                    var checkedCheckbox = $(this).prop('checked');
                    var idCheckbox = $(this).prop('id');
                    if (checkedCheckbox == true) {
                        $(this).parents('.tree').siblings('ul').find("input[type='checkbox']").prop('checked', true);
                        $(this).parents('.tree').siblings('ul').find(".tree").css('border-color', '#f59324');
                        $(this).parents('.tree').css('border-color', '#f59324');
                        $('.none-visible').find('.checkbox-label[for="' + idCheckbox + '"]').addClass('active');
                        $(this).parents('.mbui-inside-form').find('.mbui-inside-header').css('border-bottom', '1px solid #367fa9');
                        $(this).parents('.mbui-inside-form').find('.mbui-inside-header').css('border-bottom-right-radius', '0px');
                        $(this).parents('.mbui-inside-form').find('.mbui-inside-header').css('border-bottom-left-radius', '0px');
                    } else {
                        $(this).parents('.tree').siblings('ul').find("input[type='checkbox']").prop('checked', false);
                        $(this).parents('.tree').siblings('ul').find(".tree").css('border-color', '#00acd6');
                        $(this).parents('.tree').css('border-color', '#00acd6');
                        $('.none-visible').find('.checkbox-label[for="' + idCheckbox + '"]').removeClass('active');
                    }

                });
            });
        });

        // visible checked radio in another div
        $(function () {
            $('.info-wrapper input[type="radio"]').each(function () {
                var checkedRadio = $(this).prop('checked');
                var idRadio = $(this).prop('id');

                var element = $(this);
                var mbui = element.parents('.mbui-inside-form');

                mbui.toggleClass('collapsed');
                if (checkedRadio) {
                    $('.tree').css('border-color', '#00acd6');
                    $(this).parents('.tree').css('border-color', '#f59324');
                    $(this).parents('.mbui-inside-form').children('.none-visible').find('.radio-label').removeClass('active');
                    $(this).parents('.mbui-inside-form').children('.none-visible').find('.radio-label[for="' + idRadio + '"]').addClass('active').css('margin-top', '5px');
                    $(this).parents('.mbui-inside-form').addClass('collapsed');
                    $(this).parents('.mbui-inside-form').find('.mbui-inside-header').css('border-bottom', '1px solid #367fa9');
                    $(this).parents('.mbui-inside-form').find('.mbui-inside-header').css('border-bottom-right-radius', '0px');
                    $(this).parents('.mbui-inside-form').find('.mbui-inside-header').css('border-bottom-left-radius', '0px');
                } else {
                    $(this).parents('.mbui-inside-form').children('.none-visible').find('.radio-label[for="' + idRadio + '"]').removeClass('active').css('margin-top', '0');
                    $(this).parents('.mbui-inside-form').find('.mbui-inside-header').css('border-bottom', 'none');
                    $(this).parents('.mbui-inside-form').removeClass('collapsed');
                }
            });

            $(document).on('click', '.info-wrapper input[type="radio"]', function () {
                var checkedRadio = $(this).prop('checked');
                var idRadio = $(this).prop('id');

                var element = $(this);
                var mbui = element.parents('.mbui-inside-form');

                mbui.toggleClass('collapsed');
                if (checkedRadio) {
                    $('.tree').css('border-color', '#00acd6');
                    $(this).parents('.tree').css('border-color', '#f59324');
                    $(this).parents('.mbui-inside-form').children('.none-visible').find('.radio-label').removeClass('active');
                    $(this).parents('.mbui-inside-form').children('.none-visible').find('.radio-label[for="' + idRadio + '"]').addClass('active').css('margin-top', '5px');
                    $(this).parents('.mbui-inside-form').addClass('collapsed');
                    $(this).parents('.mbui-inside-form').find('.mbui-inside-header').css('border-bottom', '1px solid #367fa9');
                    $(this).parents('.mbui-inside-form').find('.mbui-inside-header').css('border-bottom-right-radius', '0px');
                    $(this).parents('.mbui-inside-form').find('.mbui-inside-header').css('border-bottom-left-radius', '0px');
                } else {
                    $(this).parents('.mbui-inside-form').children('.none-visible').find('.radio-label[for="' + idRadio + '"]').removeClass('active').css('margin-top', '0');
                    $(this).parents('.mbui-inside-form').find('.mbui-inside-header').css('border-bottom', 'none');
                    $(this).parents('.mbui-inside-form').removeClass('collapsed');
                }
            });
        });

        if ($('.tree').length > 0) {
            //toggle function inside form
            $('.tree .inside-toggle').on('click', function () {
                var element = $(this);
                var mbui = element.parent('.tree');

                mbui.toggleClass('active');
            });
        }


        if ($('.inside-checkbox').length > 0) {
            $('.inside-checkbox input[type="checkbox"]').on('click', function () {
                var element = $(this);
                var mbui = element.parents('.mbui-inside-form');

                mbui.toggleClass('collapsed');
            }); $('.inside-checkbox input[type="checkbox"]').prop('checked', '');
        }
    }

    //toggle sidebar right
    $('.toggle-sidebar-right').on('click', function () {
        $('.content').toggleClass('margin');
        $('.content').css('margin-right', '35px');
        $('.content.margin').css('margin-right', '280px');

    });


    /*Right bar vertical tab open*/
    if ($('.control-sidebar').length > 0) {
        if ($('.control-sidebar-lg').length > 0) {
            /*Right bar vertical tab open*/
            $(".vertical-tab-menu>.list-group>a").click(function (event) {
                event.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $(".vertical-tab>.vertical-tab-content").removeClass("active");
                $(".vertical-tab>.vertical-tab-content").eq(index).addClass("active");
                $(".vertical-tab>.vertical-tab-content.active").parents('.control-sidebar-lg').addClass('control-sidebar-open');
                $('.control-sidebar-lg').css('margin-left', '-560px');
            });
        }

        if ($('.control-sidebar-dark').length > 0) {
            /*Right bar vertical tab open*/
            $(".vertical-tab-menu>.list-group>a").click(function (event) {
                event.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $(".vertical-tab>.vertical-tab-content").removeClass("active");
                $(".vertical-tab>.vertical-tab-content").eq(index).addClass("active");
                $(".vertical-tab>.vertical-tab-content.active").parents('.control-sidebar-dark').addClass('control-sidebar-open');
                $('.control-sidebar-dark').css('margin-left', '-280px');
            });
        }

        var $tabs = $(".vertical-tab-menu>.list-group>a").click(function (event) {

            event.preventDefault();

            var $parent = $(this);

            if ($parent.is('.current')) {
                $parent.removeClass('current');
                $tabs.removeClass('current');
                $(".vertical-tab>.vertical-tab-content.active").parents('.control-sidebar').removeClass('control-sidebar-open');
                return;
            }

            if ($('.control-sidebar').hasClass() !== 'control-sidebar-open') {
                $(".vertical-tab-menu>.list-group>a").removeClass('current');
            }

            $tabs.removeClass('current');
            $parent.addClass("current");
        }).parent();
    }

    //touchSpin function
    if ($('.touchspin').length > 0) {
        $('.touchspin').each(function () {
            var element = $(this);
            element.TouchSpin();
        });
    }

    //Chosen select
    if ($('.chosen-select').length > 0) {
        $('.chosen-select').each(function () {
            var element = $(this);
            element.on('chosen:ready', function (e, chosen) {
                var width = element.css("width");
                element.next().find('.chosen-choices').addClass('form-control');
                jQuery(".chosen-single span").css("font-size", "16px");
                jQuery(".chosen-single span").css("font-style", "italic");
                element.next().css("width", width);
                element.next().find('.search-field input').css("width", "125px");
            }).chosen({
                placeholder_text_single: "Seçiniz"
            });
        });

    }

    //table işlemler width
    if ($('.table').length > 0) {
        $('.table thead tr th:last-child').each(function () {
            var islemler = $(this).text();
            if (islemler == 'İşlemler') {
                $(this).css('text-align', 'right');
                $(this).css('padding-right', '25px');
            }
        });
    }

    $(document).on('click', '.plan-list', function () {
        var click = $(this).closest('ul.tree-list').prop('id');
        $('#hdplanCoverageId').val(click);
        $('.plan-list').removeClass('active');
        $(this).addClass('active');
        $('.plan-coverage').addClass('active');

    });

    //plan create teminat popup
    if ($('.switch.teminatCheck').length > 0) {
        $('.switch.teminatCheck input[type="checkbox"]').change(function () {
            if ($(this).is(':checked')) {
                $('.teminatDisabled').addClass('active').prop('disabled', true);
            } else {
                $('.teminatDisabled').removeClass('active').prop("disabled", false);
            }
        }); $('.switch.teminatCheck input[type="checkbox"]').removeClass('active').prop('checked', '');
    }

});


//uppercase text in inputs
$('input[type="text"]').bestupper({
    ln: 'tr'
});
$('textarea').bestupper({
    ln: 'tr'
});


$(document).ready(function () {
    // claim --> insured table row click
    if ($('.table-insured').length > 0) {
        $('table.table-insured tbody tr').click(function () {
            //swal(this.rowIndex + " - " + $(this).find("td").eq(5).html() + " sigortalısı için işlem yapacaksınız");
            $('table.table-insured tbody tr').css('background-color', '#fff');
            $('table.table-insured tbody tr').css('color', '#616f77');
            $(this).css('background-color', '#3c8dbc');
            $(this).css('color', '#fff');
        });
    }


    // claim --> claim table row click
    if ($('.table-claim').length > 0) {
        $('table.table-claim tbody tr').click(function () {
            //swal(this.rowIndex + " - " + $(this).find("td").eq(6).html() + " sigortalısı için işlem yapacaksınız");
            $('table.table-claim tbody tr').css('background-color', '#fff');
            $('table.table-claim tbody tr').css('color', '#616f77');
            $(this).css('background-color', '#3c8dbc');
            $(this).css('color', '#fff');
        });
    }

    //Claim/Create rightbar hasar aç/kapat button
    if ($('.hasarAcKapat').length > 0) {
        $('.control-sidebar .hasarAcKapat').click(function () {
            $(this).toggleClass('bg-danger');
            if ($(this).hasClass('bg-danger')) {
                $(this).text('HASAR KAPAT');
                $(this).css('border-color', '#d9534f !important');
                swal('Hasar Açıldı');
            } else {
                $(this).text('HASAR AÇ');
                $(this).css('border-color', '#f59324 !important');
                swal('Hasar Kapalı');
            }
        });
    }

    // Listlerde arama
    $(".search").on("keyup", function () {
        var value = $(this).val().toUpperCase();
        $(this).parents('.info-wrapper').find(".tree-list li").filter(function () {
            $(this).toggle($(this).text().toUpperCase().indexOf(value) > - 1)
        });
    });
});

$('form').on('status.field.bv', function (e, data) {
    formIsValid = true;
    $('.form-group', $(this)).each(function () {
        formIsValid = formIsValid && $(this).hasClass('has-success');
    });

    if (formIsValid) {
        $('.btn-success', $(this)).attr('disabled', false);
    } else {
        $('.btn-success', $(this)).attr('disabled', true);
    }
});

$.fn.dataTable.ext.errMode = 'none';

$('.modal').on('hidden.bs.modal', function (e) {
    var noresetVal = $(this).data('noreset');
    if (typeof noresetVal == "undefined") {
        $('.modal input[type="hidden"].inputHidden').prop("value", "0").end();
        $(this).find('form').trigger('reset');
        $(this).find('form select').prop('selected', '').trigger('change');
        $(".modal .chosen-select").val('').trigger("chosen:updated");
        $(".modal .chosen-container-single .chosen-single span").text("Seçiniz").end();
        $(".modal .bootstrap-select .btn-default .filter-option").text("Seçiniz").end();
        $(".asyncResult").text('');
        if ($('.modal').hasClass('in')) {
            $('body').addClass('modal-open');
        }
    }
});



