﻿//frm : form id
//hdi : hiddem input control id
function addData(frm, hdi) {
    debugger;
    var formData = $("#" + frm).serializeArray();
    var hiddenInput = $("#" + hdi);
    var jsons = new Array();
    var strJSON = "";
    if (hiddenInput.val() != "") {

        jsons = JSON.parse(hiddenInput.val());

        var clientId = $("#" + frm + "_ClientId").val();
        strJSON = "";

        if (clientId == "0") {

            //Insert new record
            strJSON = "{\"id\":" + (jsons.length + 1) + ",";
            strJSON += getJson(formData);
            strJSON = strJSON.substring(0, strJSON.length - 1) + "}";

            jsons.push(JSON.parse(strJSON));
        }
        else {
            //update record
            $.each(jsons, function (index) {

                var jsonDataItem = jsons[index];

                if (jsonDataItem.id == clientId || jsonDataItem.ID == clientId) {

                    strJSON = "{\"id\":" + (clientId) + ",";
                    strJSON += getJson(formData);
                    strJSON = strJSON.substring(0, strJSON.length - 1) + "}";

                    jsons[index] = (JSON.parse(strJSON));
                }
                var id = "0";
                $("#" + frm + "_ClientId").val(id);
            });
        }

    } else {
        //Insert first record
        strJSON = "{\"id\":1,";
        strJSON += getJson(formData);
        //debugger;
        strJSON = strJSON.substring(0, strJSON.length - 1) + "}";

        jsons.push(JSON.parse(strJSON));
    }
    $("#" + frm)[0].reset();
    //set hidden value

    var hdValue = JSON.stringify(jsons);
    hiddenInput.val(hdValue);
    console.log(hdValue);
};

function getJson(formData, treeData, state) {

    var strJSON = '';
    var resume = true;
    $.each(formData, function (i, frmfield) {
        if (state == "Add" && frmfield.name == "COVERAGE_ID") {
            $.each(treeData, function (j, elm) {
                debugger;
                if (elm.COVERAGE_ID == frmfield.value) {
                    swal({
                        title: "Hata!",
                        text: "Aynı Teminat Birden Fazla Teminat-Network Eşlemesine Kaydedilemez.",
                        icon: "warning"
                    });
                    resume = false;
                    return false;
                }
            });
            if (resume == false) {
                return false;
            }
        }
        fieldValue = frmfield.value;//.replace(/(?:\r\n|\r|\n)/g, '<br />');

        //all html form controls
        strJSON = strJSON + "\"" + frmfield.name + "\":\"" + frmfield.value + "\",";

        var formelement = document.getElementsByName(frmfield.name);

        //select (dropdown)
        if (formelement[0].localName == "select" && frmfield.value != "") {


            var arrayIndex = frmfield.name.toString().indexOf("_Array");
            if (arrayIndex >= 0) {
                strJSON = strJSON + "\"" + frmfield.name + "SelectedId\":[";
                var str = $('#' + frmfield.name + ' option:selected').map(function () { return this.value }).get();
                var i;
                for (i = 0; i < str.length; i++) {
                    if (str[i] == "") continue;
                    strJSON = strJSON + "\"" + str[i] + "\",";
                    // rtnStr += $('#"'+frmfield.name+'" option[value="' + str[i] + '"]').text();
                    //rtnStr += str[i]+"--";
                }
                strJSON = strJSON.substring(0, strJSON.length - 1) + "";
                strJSON = strJSON + "],";

                strJSON = strJSON + "\"" + frmfield.name + "SelectedText\":[";
                for (i = 0; i < str.length; i++) {
                    if (str[i] == "") continue;
                    strJSON = strJSON + "\"" + $('#' + frmfield.name + ' option[value=' + str[i] + ']').data('code') + "\",";
                    //rtnStr += str[i]+"--";
                }
                strJSON = strJSON.substring(0, strJSON.length - 1) + "";
                strJSON = strJSON + "],";
            }
            else if (formelement[0].selectedIndex > 0) {
                strJSON = strJSON + "\"" + frmfield.name + "SelectedText\":\"" + formelement[0].options[formelement[0].selectedIndex].text + "\",";
            }
        }

        //checkbox
        if (formelement[0].type == "checkbox") {
            strJSON = strJSON + "\"" + frmfield.name + "SelectedId\":[";
            for (var i = 0; i < formelement.length; i++) {

                if (formelement[i].checked) {
                    strJSON = strJSON + "\"" + $('input[id=\"' + formelement[i].value + '\"]:checked').data('id') + "\",";
                }
            }
            strJSON = strJSON.substring(0, strJSON.length - 1) + "";
            strJSON = strJSON + "],";

            strJSON = strJSON + "\"" + frmfield.name + "SelectedText\":[";
            for (var i = 0; i < formelement.length; i++) {
                if (formelement[i].checked) {
                    strJSON = strJSON + "\"" + formelement[i].value + "\",";
                }
            }
            strJSON = strJSON.substring(0, strJSON.length - 1) + "";
            strJSON = strJSON + "],";
        }

        //radiobutton
        if (formelement[0].type == "radio") {

            var radioText = $('input[name=\"' + frmfield.name + '\"]:checked').data('text');
            strJSON = strJSON + "\"" + frmfield.name + "SelectedText\":\"" + radioText + "\",";
        }

    });

    return strJSON;
};

function CreateData(frm) {
    //debugger;
    $("#" + frm + "_ClientId").val("0");
    $("#btn" + frm + "Save").html("KAYDET");
    //$('#' + frm).trigger('reset');
}

function OpenListModal(modal) {
    $('#isOpen_' + modal).val('1');
    $('#' + modal).modal('toggle');
}

function deleteData(hdi, id) {

    var hiddenInput = $("#" + hdi);

    var jsons = new Array();
    jsons = JSON.parse(hiddenInput.val());

    $.each(jsons, function (i, item) {

        console.log("item.STATUS: " + item.STATUS);

        if (item.id == id) {
            item.STATUS = "1";
            item.isOpen = "1";
        }
    });

    var hdBanksValue = JSON.stringify(jsons);
    hiddenInput.val(hdBanksValue);
    console.log(hdBanksValue);
}

//Refresh table after row insert | delete
function RefreshTable(tableName, hdControl) {

    var table = $('#' + tableName).DataTable();
    table.rows().remove().draw();

    var jsonValue = $('#' + hdControl).val();
    var data = new Array();

    if (jsonValue != "") {

        var hdData = JSON.parse(jsonValue);

        $.each(hdData, function (index) {
            var status = hdData[index].STATUS;
            if (status == "0") {
                data.push(hdData[index]);
                table.row.add(hdData[index]).draw();
            }
        });
    }
}

//Refresh table after row insert | delete
function RefreshTableAllStatus(tableName, hdControl) {

    var table = $('#' + tableName).DataTable();
    table.rows().remove().draw();

    var jsonValue = $('#' + hdControl).val();
    var data = new Array();

    if (jsonValue != "") {

        var hdData = JSON.parse(jsonValue);

        $.each(hdData, function (index) {
            data.push(hdData[index]);
            table.row.add(hdData[index]).draw();
        });
    }
}

function Success(field) {
    swal({
        title: "İşlem Başarılı!",
        text: field + " başarıyla kaydedildi",
        icon: "success"
    });
}

function FillCoverageByPackage(sourceSelectedBox, targetSelectBox, isName, targetHidden) {
    $("#" + targetSelectBox).html(""); // clear before appending new list 
    $("#" + targetSelectBox).trigger("chosen:updated");
    var selected = $('#' + targetHidden).val();

    var Id = $('#' + sourceSelectedBox).val();
    debugger;
    $.ajax({
        url: '/Common/FillCoverageByPackage',
        type: "POST",
        dataType: "JSON",
        data: { packageId: Id },
        success: function (data) {
            debugger;
            $("#" + targetSelectBox).html(""); // clear before appending new list 
            $.each(JSON.parse(data), function (i, item) {
                if (selected == item.Id) {
                    $("#" + targetSelectBox).append(('<option value="' + item.COVERAGE_ID + '" selected="">' + item.COVERAGE_NAME + '</option>'));
                }
                else {
                    if (isName == 1)
                        $("#" + targetSelectBox).append($('<option></option>').val(item.COVERAGE_NAME).html(item.COVERAGE_NAME));
                    else
                        $("#" + targetSelectBox).append($('<option></option>').val(item.COVERAGE_ID).html(item.COVERAGE_NAME));
                }
            }); $("#" + targetSelectBox).trigger("chosen:updated");

        },
        error: function (xhr, err) {
            alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
            alert("responseText: " + xhr.responseText);
            $.unblockUI();
        }
    });
}

//Fill counties
function FillCounty(sourceSelectedBox, targetSelectBox, isName, targetHidden) {
    $("#" + targetSelectBox).html(""); // clear before appending new list 
    $("#" + targetSelectBox).trigger("chosen:updated");
    var selected = $('#' + targetHidden).val();

    var Id = $('#' + sourceSelectedBox).val();
    $.ajax({
        url: '/Common/FillCounty',
        type: "POST",
        dataType: "JSON",
        data: { cityId: Id },
        success: function (data) {
            $("#" + targetSelectBox).html(""); // clear before appending new list 

            $("#" + targetSelectBox).append('<option value="">TÜMÜ</option>');
            $.each(JSON.parse(data), function (i, item) {
                if (selected == item.Id) {
                    $("#" + targetSelectBox).append(('<option value="' + item.Id + '" selected="">' + item.Name + '</option>'));
                }
                else {
                    if (isName == 1)
                        $("#" + targetSelectBox).append($('<option></option>').val(item.Name).html(item.Name));
                    else
                        $("#" + targetSelectBox).append($('<option></option>').val(item.Id).html(item.Name));
                }
            }); $("#" + targetSelectBox).trigger("chosen:updated");

        }
    });
}

//Fill ProcessGroup
function FillProcessGroup(sourceSelectedBox, targetSelectBox, targetHidden) {
    var Id = $('#' + sourceSelectedBox).val();
    var selected = $('#' + targetHidden).val();
    $.ajax({
        url: '/Common/FillProcessGroup',
        type: "POST",
        dataType: "JSON",
        data: { processListId: Id },
        success: function (data) {
            $("#" + targetSelectBox).html(""); // clear before appending new list 
            $("#" + targetSelectBox).append('<option value="">SEÇİNİZ</option>');

            $.each(JSON.parse(data), function (i, item) {
                //debugger;
                if (selected == item.Id) {
                    $("#" + targetSelectBox).append(('<option value="' + item.Id + '" selected="">' + item.Name + '</option>'));
                }
                else {
                    $("#" + targetSelectBox).append($('<option></option>').val(item.Id).html(item.Name));
                }
            }); $("#" + targetSelectBox).trigger("chosen:updated");
        }
    });
}

//Fill bank branches
function FillBankBranch(sourceSelectedBox, targetSelectBox, targetHidden) {
    debugger;
    var Id = $('#' + sourceSelectedBox).val();
    var selected = $('#' + targetHidden).val();
    $.ajax({
        url: '/Common/FillBankBranch',
        type: "POST",
        dataType: "JSON",
        data: { bankId: Id },
        success: function (data) {
            debugger;
            $("#" + targetSelectBox).html(""); // clear before appending new list 
            $.each(JSON.parse(data), function (i, item) {
                debugger;
                var strValue = item.Name;
                if (item.Code != null && item.Code.length > 0 && item.Code !== "0") {
                    strValue += " - " + item.Code;
                }
                if (selected == item.Id) {
                    $("#" + targetSelectBox).append(('<option value="' + item.Id + '" selected="">' + strValue + '</option>'));
                }
                else {
                    $("#" + targetSelectBox).append($('<option></option>').val(item.Id).html(strValue));
                }
            }); $("#" + targetSelectBox).trigger("chosen:updated");
        }
    });
}
//Fill Product
function FillProduct(sourceSelectedBox, targetSelectBox, isChose, targetTargetSelectedBox, targetTargetTargetSelectedBox) {

    $("#" + targetSelectBox).html(""); // clear before appending new list 
    $("#" + targetSelectBox).selectpicker("refresh");

    $("#" + targetTargetSelectedBox).html("");
    $("#" + targetTargetSelectedBox).selectpicker("refresh");

    $("#" + targetTargetTargetSelectedBox).html("");
    $("#" + targetTargetTargetSelectedBox).selectpicker("refresh");

    var Id = $('#' + sourceSelectedBox).val();
    $.ajax({
        url: '/Common/FillProduct',
        type: "POST",
        dataType: "JSON",
        data: { companyId: Id },
        success: function (data) {
            debugger;
            if (isChose == "1")
                $("#" + targetSelectBox).append('<option value="">Seçiniz</option>');

            $.each(JSON.parse(data), function (i, item) {
                $("#" + targetSelectBox).append($('<option></option>').val(item.Id).html(item.Name));
            }); $("#" + targetSelectBox).selectpicker("refresh");
        }
    });
}
//Fill Product
function FillProductChosen(sourceSelectedBox, targetSelectBox, isChose, targetTargetSelectedBox, targetTargetTargetSelectedBox) {

    $("#" + targetSelectBox).html(""); // clear before appending new list 
    $("#" + targetSelectBox).trigger("chosen:updated");

    $("#" + targetTargetSelectedBox).html("");
    $("#" + targetTargetSelectedBox).selectpicker("refresh");

    $("#" + targetTargetTargetSelectedBox).html("");
    $("#" + targetTargetTargetSelectedBox).selectpicker("refresh");

    var Id = $('#' + sourceSelectedBox).val();
    $.ajax({
        url: '/Common/FillProduct',
        type: "POST",
        dataType: "JSON",
        data: { companyId: Id },
        success: function (data) {
            debugger;
            if (isChose == "1")
                $("#" + targetSelectBox).append('<option value="">Seçiniz</option>');

            $.each(JSON.parse(data), function (i, item) {
                $("#" + targetSelectBox).append($('<option></option>').val(item.Id).html(item.Name));
            }); $("#" + targetSelectBox).trigger("chosen:updated");
        }
    });
}

function FillSagmerCoverage(sourceSelectedBox, sourceHidden, targetSelectBox, targetHidden) {

    $("#" + targetSelectBox).html(""); 
    $("#" + targetSelectBox).trigger("chosen:updated");

    var sagmerTypeId = $('#' + sourceSelectedBox).val();
    var planCoverageId = $('#' + sourceHidden).val();

    var selected = $('#' + targetHidden).val();
    $.ajax({
        url: '/Common/FillSagmerCoverage?sagmerTypeId=' + sagmerTypeId + "&planCoverageId=" + planCoverageId,
        type: "POST",
        dataType: "JSON",
        data: null,
        beforeSend: function (xhr) {
            $.blockUI({ message: $('#domMessage') });
        },
        complete: function() {
            $.unblockUI();
        },
        success: function (data) {
            $.each(JSON.parse(data), function (i, item) {
                if (selected == item.Id)
                    $("#" + targetSelectBox).append(('<option value="' + item.Id + '" selected="">' + item.Description + '</option>'));
                else
                    $("#" + targetSelectBox).append($('<option></option>').val(item.Id).html(item.Description));
            }); $("#" + targetSelectBox).trigger("chosen:updated");
        },
        error: function (xhr, err) {
            $.unblockUI();
        }
    });
}

function FillSagmerPlan(sourceSelectedBox, targetSelectBox, targetHidden) {

    $("#" + targetSelectBox).html(""); // clear before appending new list 
    $("#" + targetSelectBox).trigger("chosen:updated");

    var Id = $('#' + sourceSelectedBox).val();
    var selected = $('#' + targetHidden).val();

    $.ajax({
        url: '/Common/FillSagmerPlan',
        type: "POST",
        dataType: "JSON",
        data: { packageTypeId: Id },

        success: function (data) {
            debugger;
            $.each(JSON.parse(data), function (i, item) {

                if (selected == item.Id)
                    $("#" + targetSelectBox).append(('<option value="' + item.Id + '" selected="">' + item.Description + '</option>'));
                else
                    $("#" + targetSelectBox).append($('<option></option>').val(item.Id).html(item.Description));

            });
            $("#" + targetSelectBox).trigger("chosen:updated").change();
        }
    });
}

//Fill Product Branch
function FillProductBranch(sourceSelectedBox, targetSelectBox, isChose, sourceSelectedBox2) {
    debugger;
    var companyId = $('#' + sourceSelectedBox2).val();
    var Id = $('input[name=\"' + sourceSelectedBox + '\"]:checked').val();
    $.ajax({
        url: '/Common/FillProductBranch?companyId=' + companyId,
        type: "POST",
        dataType: "JSON",
        data: { branchId: Id },
        success: function (data) {
            $("#" + targetSelectBox).html(""); // clear before appending new list 
            if (isChose == "1")
                $("#" + targetSelectBox).append('<option value="">Seçiniz</option>');

            $.each(JSON.parse(data), function (i, item) {
                $("#" + targetSelectBox).append($('<option></option>').val(item.PRODUCT_ID).html(item.PRODUCT_NAME));
            }); $("#" + targetSelectBox).selectpicker("refresh");
        }
    });
}

//Fill Subproduct
function FillSubProduct(sourceSelectedBox, targetSelectBox, isChose, targetTargetSelectedBox) {

    $("#" + targetSelectBox).html(""); // clear before appending new list 
    $("#" + targetSelectBox).selectpicker("refresh");

    $("#" + targetTargetSelectedBox).html("");
    $("#" + targetTargetSelectedBox).selectpicker("refresh");

    var Id = $('#' + sourceSelectedBox).val();
    $.ajax({
        url: '/Common/FillSubProduct',
        type: "POST",
        dataType: "JSON",
        data: { productIdList: Id },
        success: function (data) {
            $("#" + targetSelectBox).html(""); // clear before appending new list 
            if (isChose == "1")
                $("#" + targetSelectBox).append('<option value="">Seçiniz</option>');

            $.each(JSON.parse(data), function (i, item) {
                $("#" + targetSelectBox).append($('<option></option>').val(item.Id).html(item.Name));
            }); $("#" + targetSelectBox).selectpicker("refresh");
        }
    });
}

function FillPlan(sourceSelectedBox, targetSelectBox, isChose) {
    var Id = $('#' + sourceSelectedBox).val();
    $.ajax({
        url: '/Common/FillPlan',
        type: "POST",
        dataType: "JSON",
        data: { subProductIdList: Id },
        success: function (data) {
            $("#" + targetSelectBox).html(""); // clear before appending new list 
            if (isChose == "1")
                $("#" + targetSelectBox).append('<option value="">Seçiniz</option>');

            $.each(JSON.parse(data), function (i, item) {
                $("#" + targetSelectBox).append($('<option></option>').val(item.Id).html(item.Name));
            }); $("#" + targetSelectBox).selectpicker("refresh");
        }
    });
}

function FillPackage(sourceSelectedBox, targetSelectBox, isChose) {
    var Id = $('#' + sourceSelectedBox).val();
    $.ajax({
        url: '/Common/FillPackage',
        type: "POST",
        dataType: "JSON",
        data: { planId: Id },
        success: function (data) {
            debugger;
            $("#" + targetSelectBox).html(""); // clear before appending new list 
            if (isChose == "1")
                $("#" + targetSelectBox).append('<option value="">Seçiniz</option>');

            $.each(JSON.parse(data), function (i, item) {
                $("#" + targetSelectBox).append($('<option></option>').val(item.Id).html(item.Name));
            }); $("#" + targetSelectBox).selectpicker("refresh");
        }
    });
}

function FillAgency(sourceSelectedBox, targetSelectBox, targetTargetSelectedBox, targetTargetTargetSelectedBox) {

    $("#" + targetSelectBox).html(""); // clear before appending new list 
    $("#" + targetSelectBox).trigger("chosen:updated");

    $("#" + targetTargetSelectedBox).html("");
    $("#" + targetTargetSelectedBox).selectpicker("refresh");

    $("#" + targetTargetTargetSelectedBox).html("");
    $("#" + targetTargetTargetSelectedBox).selectpicker("refresh");

    var Id = $('#' + sourceSelectedBox).val();
    $.ajax({
        url: '/Common/FillAgency',
        type: "POST",
        dataType: "JSON",
        data: { companyId: Id },
        success: function (data) {
            $("#" + targetSelectBox).html(""); // clear before appending new list 
            $("#" + targetSelectBox).append('<option value="">Seçiniz</option>');

            $.each(JSON.parse(data), function (i, item) {
                $("#" + targetSelectBox).append($('<option></option>').val(item.Id).html(item.Name));
            }); $("#" + targetSelectBox).trigger("chosen:updated");
        }
    });
}

function GetExchangeRate(cType, hdiExId) {
    var currencyCode = $('#' + cType).val();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Common/GetExchangeRate?Ordn=" + currencyCode,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function (xhr) {
            $.blockUI({ message: $('#domMessage') });
        },
        success: function (data) {
            $.unblockUI();
            if (typeof data != "undefined" && data != "") {
                var responseData = $.parseJSON(data);

                $('#' + hdiExId).val(responseData.Id);
            }
            else {
                $('#' + hdiExId).val("");
            }
        },
        error: function (xhr, err) {
            alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
            alert("responseText: " + xhr.responseText);
            $.unblockUI();
        }
    });
}

function swAlert(title, text, icon) {
    swal({
        title: title,
        text: text,
        icon: icon
    });
}

function Loading() {
    $.unblockUI();
    $.blockUI({ message: $('#domMessage') });
}
function Confirm(title, text, type, href) {
    swal({
        title: title,
        text: text,
        type: type,
        showCancelButton: true,
        cancelButtonText: "Hayır",
        confirmButtonText: "Evet",
        closeOnConfirm: true,
    },
        function (isConfirm) {
            if (isConfirm) {
                Loading();
                document.location.href = href;
                //tempdata alerti siler
                $.getJSON("/Common/ClearTempData?key=Alert", function (data) { });
            }
            else {
                $.unblockUI();

            }
        });
}

function ConfirmDelete(title, text, type, hdi, data, tbl) {

    debugger;

    swal({
        title: title,
        text: text,
        type: type,
        showCancelButton: true,
        cancelButtonText: "Hayır",
        confirmButtonText: "Evet",
        closeOnConfirm: true,
    },
        function (isConfirm) {

            debugger;

            if (isConfirm) {
                deleteData(hdi, data);
                RefreshTable(tbl, hdi);
                //tempdata alerti siler
                $.getJSON("/Common/ClearTempData?key=Alert", function (data) { });
            }
        });
}

function ModalConfirm(title, text, type, modalId) {
    swal({
        title: title,
        text: text,
        type: type,
        showCancelButton: true,
        confirmButtonText: "Evet",
        cancelButtonText: "Hayır",
        closeOnConfirm: true,
    },
        function (isConfirm) {
            if (isConfirm) {
                $('#' + modalId).modal('hide');
                //tempdata alerti siler
                $.getJSON("/Common/ClearTempData?key=Alert", function (data) { });
            }
            else {
                $('#' + modalId).modal('show');
            }
        });
}

function Loading() {
    $.blockUI({ message: $('#domMessage') });
}

function formatdate(date) {
    if (date != null) {
        var dateString = date.substr(6);
        var currentTime = new Date(parseInt(dateString));
        var month = currentTime.getMonth() + 1;
        if (month < 10) {
            month = "0" + month;
        }
        var day = currentTime.getDate();
        if (day < 10) {
            day = "0" + day;
        }
        var year = currentTime.getFullYear();
        var date = day + "-" + month + "-" + year;
        return date;
    }
    else {
        return "-";
    }
}

function DateFormatMask(date) {
    if (date != null) {
        var currentTime = new Date(date);
        var month = currentTime.getMonth();
        if (month < 10) {
            month = "0" + month;
        }
        var day = currentTime.getDate();
        if (day < 10) {
            day = "0" + day;
        }
        var year = currentTime.getFullYear();
        var date = day + "-" + month + "-" + year;
        return date;
    }
    else {
        return "";
    }
}

function DeleteMedia(baseId, mediaId, objectType, objectId) {


    swal({
        title: "Silinecek!",
        text: "Silmek istediginizden emin misiniz ?",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Hayır",
        confirmButtonText: "Evet",
        closeOnConfirm: true,
    },
        function (isConfirm) {
            debugger;
            if (isConfirm) {
                $.blockUI({ message: $('#domMessage') });
                var params = {
                    "MediaId": mediaId,
                    "ObjectType": objectType,
                    "BaseId": baseId,
                    "ObjectId": objectId
                }
                $.get('/Common/DeleteMedia', params, function (data) {
                    FillMedia();
                    $.unblockUI();
                    //tempdata alerti siler
                    $.getJSON("/Common/ClearTempData?key=Alert", function (data) { });
                });
            }
        }
    );
}
function FillMedia() {

    var prevTable = $("#tblMedias").DataTable();
    prevTable.destroy();
    var params = {
        "ObjectId": $("#ObjectId").val(),
        "ObjectType": $("#MediaObjectType").val()
    }
    $.get('/Common/FillMedia', params, function (data) {
        debugger;
        var table = $("#tblMedias").DataTable({
            language: {
                "emptyTable": "Kayıt Bulunamadı",
                "infoEmpty": "",
                "info": "",
                "search": "Arama: "
            },
            "data": data,
            "bSort": false,
            "columns": [
                { "data": "Name" },
                {
                    "data": null,
                    "render": function (data) {
                        debugger;
                        return '<div class="islem">' +
                            '<div class="btn-group fr" name="mediaDownloadButton">' +
                            '<a href="#" class="myIcon icon-warning"  title="İndir" onclick="MediaDownloader(' + data.MediaId + ')">' +
                            '<i class="fa fa-download"></i>' +
                            '</a ></div>' +

                            '<div class="btn-group fr" name="mediaEditButton">' +
                            '<a href="#" class="myIcon icon-warning"  title="Düzenle" onclick="MediaModalRun(' + data.BaseId + ',' + data.MediaId + ',\'' + data.Name + '\');">' +
                            '<i class="fa fa-pencil-square-o"></i>' +
                            '</a ></div >' +
                            '<div class="btn-group fr" name="mediaDeleteButton">' +
                            '<a href="#" class="myIcon icon-danger" onclick="DeleteMedia(\'' + data.BaseId + '\',\'' + data.MediaId + '\',\'' + $("#MediaObjectType").val() + '\',\'' + data.ObjectId + '\');" data-tooltip="tooltip" data-placement="bottom" title="Sil">' +
                            '<i class="fa fa-trash-o"></i>' +
                            '</a ></div></div>';
                    }
                }
            ]
        });
        $("#loadingText").hide();
    });

}

function MediaDownloader(MediaId) {
    debugger;
    $.ajax({
        type: 'POST',
        url: '/Common/DownloadByMediaId/',
        data: '{ "MediaID": ' + MediaId + ' }',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (returnValue) {
            debugger;
            if (returnValue.IsNull == false) {
                window.location = '/Common/Download?fileGuid=' + returnValue.FileGuid
                    + '&filename=' + returnValue.FileName;
            }
        }
    });
}
function MediaModalRun(baseId, mediaId, mediaName) {
    debugger;
    $("#MediaId").val(mediaId);
    $("#BaseId").val(baseId);
    $("#MediaName").val(mediaName);
    $('#Media_Modal').modal('toggle');
    $("#myfrmMedia_Modal").text(mediaName + " Güncelleme");
}

function OpenClaimDetails(claimId) {
    window.open("/Claim/Create?claimId=" + claimId, "_blank");
}