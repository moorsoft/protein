﻿namespace Protein.Console.Jobs
{
    using System;
    using System.Xml;

    public class DailyJob
    {
        /**
         * Method to run each day at 17:00
         */
        public static void GetExchangeRatesFromTCMB()
        {
            string today = "http://www.tcmb.gov.tr/kurlar/today.xml";

            var xmlDoc = new XmlDocument();
            xmlDoc.Load(today);

            Console.WriteLine(DateTime.Now);

            string USD_Birim = xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='USD']/Unit").InnerXml;
            Console.WriteLine("USD Birim : " + USD_Birim);

            string USD_Alis = xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='USD']/BanknoteBuying").InnerXml;
            Console.WriteLine("USD Alış : " + USD_Alis);

            string USD_Satis = xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='USD']/BanknoteSelling").InnerXml;
            Console.WriteLine("USD Satış : " + USD_Satis);

            string USD_Forex_Alis = xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='USD']/ForexBuying").InnerXml;
            Console.WriteLine("USD Forex Alış : " + USD_Alis);

            string USD_Forex_Satis = xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='USD']/ForexSelling").InnerXml;
            Console.WriteLine("USD Forex Satış : " + USD_Satis);

            // Persist data through sp_execute
        }

        void EmailSomeoneForSomeReason()
        {
            //using (var message = new MailMessage("testuser@gmail.com", "testdestinationmail@gmail.com"))
            //{
            //    message.Subject = "Message Subject test";
            //    message.Body = "Message body test at " + DateTime.Now;
            //    using (SmtpClient client = new SmtpClient
            //    {
            //        EnableSsl = true,
            //        Host = "smtp.gmail.com",
            //        Port = 587,
            //        Credentials = new NetworkCredential("testuser@gmail.com", "123546")
            //    })
            //    {
            //        client.Send(message);
            //    }
            //}
        }
    }
}