﻿using Dapper;
using Newtonsoft.Json;
using Protein.Common.Extensions;
using Protein.Common.Resources;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Web.ActionFilter.LoginAttr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Web.Controllers.Production
{
    [LoginControl]
    public class PolicyController : Controller
    {

        public JsonResult Details(Int64 policyId)
        {
            var policy = new PolicySummary();
            var policyItem = new GenericRepository<V_Policy>().FindBy("POLICY_ID=:policyId", orderby: "", parameters: new { policyId }).FirstOrDefault();
            if (policyItem != null)
            {
                policy.COMPANY_NAME = policyItem.COMPANY_NAME;
                policy.POLICY_NUMBER = policyItem.POLICY_NUMBER;
                string POLICY_START_DATE = Convert.ToString(policyItem.POLICY_START_DATE);
                policy.POLICY_START_DATE = POLICY_START_DATE.IsDateTime() ? DateTime.Parse(POLICY_START_DATE).ToString("dd-MM-yyyy") : null;
                string POLICY_END_DATE = Convert.ToString(policyItem.POLICY_END_DATE);
                policy.POLICY_END_DATE = POLICY_END_DATE.IsDateTime() ? DateTime.Parse(POLICY_END_DATE).ToString("dd-MM-yyyy") : null;
                string POLICY_ISSUE_DATE = Convert.ToString(policyItem.POLICY_ISSUE_DATE);
                policy.POLICY_ISSUE_DATE = POLICY_ISSUE_DATE.IsDateTime() ? DateTime.Parse(POLICY_ISSUE_DATE).ToString("dd-MM-yyyy") : null;
                policy.STATUS_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.PolicyStatus, policyItem.STATUS);
                policy.AGENCY_NO = policyItem.AGENCY_NO;
                policy.AGENCY_NAME = policyItem.AGENCY_NAME;
                policy.INSURER_NAME = policyItem.INSURER_NAME;
                policy.POLICY_GROUP_NAME = policyItem.POLICY_GROUP_NAME;
                policy.PRODUCT_NAME = policyItem.PRODUCT_NAME;

                if (policyItem.LastEndorsementId != null && policyItem.LastEndorsementId > 0)
                {
                    Endorsement lastEndorsement = new GenericRepository<Endorsement>().FindById(Convert.ToInt64(policyItem.LastEndorsementId));
                    if (lastEndorsement != null)
                    {
                        policy.LAST_ENDORSEMENT_TYPE = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Endorsement, lastEndorsement.Type);
                        policy.LAST_ENDORSEMENT_NO = lastEndorsement.No.ToString();
                        policy.LAST_ENDORSEMENT_START_DATE = lastEndorsement.StartDate.ToString();
                        policy.LAST_ENDORSEMENT_DATE_OF_ISSUE = lastEndorsement.DateOfIssue.ToString();
                    }
                }
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(policy),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [LoginControl]
        public Int64 GetPrivateTerms(Int64 policyId)
        {
            var policyItem = new GenericRepository<V_Policy>().FindBy("POLICY_ID=:policyId", orderby: "POLICY_ID", parameters: new { policyId }).FirstOrDefault();
            if (policyItem != null)
            {
                var productId = policyItem.PRODUCT_ID;
                if (productId != null)
                {
                    var productMedia = new GenericRepository<V_ProductMedia>().FindBy("MEDIA_NAME =:mediaName AND PRODUCT_ID=:productId", orderby: "PRODUCT_ID", parameters: new { productId, mediaName = new DbString { Value = "ÖZEL ŞART", Length = 200 } }).FirstOrDefault();
                    if (productMedia != null)
                    {
                        if (productMedia != null)
                        {
                            return (long)productMedia.MediaId;
                        }
                    }
                    else
                    {
                        var subproductId = policyItem.SUBPRODUCT_ID;
                        if (subproductId != null)
                        {
                            var subproductMedia = new GenericRepository<V_SubproductMedia>().FindBy("MEDIA_NAME =:mediaName AND SUBPRODUCT_ID=:subproductId", orderby: "SUBPRODUCT_ID", parameters: new { subproductId, mediaName = new DbString { Value = "ÖZEL ŞART", Length = 200 } }).FirstOrDefault();
                            if (subproductMedia != null)
                            {
                                if (subproductMedia != null)
                                {
                                    return subproductMedia.MediaId;
                                }
                            }
                        }
                    }
                }
            }
            return 0;
        }

        [LoginControl]
        public Int64 GetAdditionalTerms(Int64 policyId, Int64 insuredId)
        {
            var policyItem = new GenericRepository<V_Policy>().FindBy("POLICY_ID=:policyId", orderby: "POLICY_ID", parameters: new { policyId }).FirstOrDefault();
            if (policyItem != null)
            {
                var productId = policyItem.PRODUCT_ID;
                if (productId != null)
                {
                    var productMedia = new GenericRepository<V_ProductMedia>().FindBy("MEDIA_NAME =:mediaName AND PRODUCT_ID=:productId", orderby: "PRODUCT_ID", parameters: new { productId, mediaName = new DbString { Value = "EK PROTOKOL", Length = 200 } }).FirstOrDefault();
                    if (productMedia != null)
                    {
                        if (productMedia != null)
                        {
                            return (long)productMedia.MediaId;
                        }
                    }
                    else
                    {
                        var subproductId = policyItem.SUBPRODUCT_ID;
                        if (subproductId != null)
                        {
                            var subproductMedia = new GenericRepository<V_SubproductMedia>().FindBy("MEDIA_NAME =:mediaName AND SUBPRODUCT_ID=:subproductId", orderby: "SUBPRODUCT_ID", parameters: new { subproductId, mediaName = new DbString { Value = "EK PROTOKOL", Length = 200 } }).FirstOrDefault();
                            if (subproductMedia != null)
                            {
                                if (subproductMedia != null)
                                {
                                    return subproductMedia.MediaId;
                                }
                            }
                            else
                            {
                                var insuredItem = new GenericRepository<V_Insured>().FindBy("INSURED_ID=:insuredId", orderby: "", parameters: new { insuredId }).FirstOrDefault();
                                if (insuredItem != null)
                                {
                                    var packageId = insuredItem.PACKAGE_ID;
                                    if (packageId != null)
                                    {
                                        var packageMedia = new GenericRepository<V_SubproductMedia>().FindBy("MEDIA_NAME =:mediaName AND PACKAGE_ID=:packageId", orderby: "PACKAGE_ID", parameters: new { packageId, mediaName = new DbString { Value = "EK PROTOKOL", Length = 200 } }).FirstOrDefault();
                                        if (packageMedia != null)
                                        {
                                            if (packageMedia != null)
                                            {
                                                return (long)packageMedia.MediaId;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return 0;
        }
    }
}