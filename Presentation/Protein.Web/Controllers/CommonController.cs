﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Protein.Data.Export.Model;
using Protein.Common.Extensions;
using Protein.Data.Repositories;
using Protein.Data.Workers;
using Protein.Web.Models.ExportModel;
using Protein.Web.Models.HistoryModel;
using Protein.Web.Models.ImportModel;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Enums.ProteinEnums;
using static Protein.Common.Entities.ProteinEntities;
using Protein.Common.Dto.MediaObjects;
using Protein.Business.Enums.Media;
using Protein.Business.Concrete.Media;
using Protein.Business.Concrete.Responses;
using Protein.Data.Helpers;
using Protein.Common.Constants;
using System.Runtime.Serialization.Formatters.Binary;
using Dapper;
using System.Reflection;
using System.Collections;
using System.Diagnostics;
using Protein.Web.ActionFilter.LoginAttr;
using Protein.Business.Print;

namespace Protein.Web.Controllers
{
    public class CommonController : Controller
    {
        public ActionResult ChangePassword()
        {
            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(FormCollection form)
        {
            dynamic personel = System.Web.HttpContext.Current.Session["Personnel"];

            if (form["newPassword"] != form["newPasswordAgain"])
            {
                TempData["Alert"] = $"swAlert('Uyarı','Şifre Uyumsuzluğu, iki Alanıda Aynı Giriniz!','warning')";
                return View();
            }

            User user = new UserRepository().FindById(long.Parse(Convert.ToString(personel.USER_ID)));
            if (user == null)
            {
                TempData["Alert"] = $"swAlert('Hata','Hatalı E-Posta/Şifre. Lütfen tekrar deneyiniz.','warning')";
                return View();
            }

            user.Password = form["newPassword"].ToMD5().ToUpper();
            SpResponse spResponse = new GenericRepository<User>().Update(user);
            if (spResponse.Code != "100")
            {
                TempData["Alert"] = $"swAlert('Hata','Şifre Değiştirme İşlemi Yapılamadı.','warning')";
                return View();
            }


            TempData["Alert"] = $"swAlert('İşlem Başarılı','Şifre Değiştirme İşlemi Başarıyla Gerçekleştirildi.','success')";

            return RedirectToAction("Index", "Dashboard");
        }
        [HttpPost]
        public virtual ActionResult DownloadByMediaId(long MediaID)
        {
            string handle = Guid.NewGuid().ToString();

            try
            {
                Media media = new GenericRepository<Media>().FindById(MediaID);

                //MemoryStream memStream = new MemoryStream();
                //BinaryFormatter binForm = new BinaryFormatter();
                //memStream.Write(media.FileContent, 0, media.FileContent.Length);
                //memStream.Seek(0, SeekOrigin.Begin);

                //memStream.
                if (media.FileContent != null)
                    TempData[handle] = media.FileContent.ToArray();

                return new JsonResult()
                {
                    Data = new { FileGuid = handle, FileName = media.FileName, IsNull = media.FileContent == null }
                };
            }
            catch (Exception ex) { return new EmptyResult(); }
        }
        [HttpGet]
        public virtual ActionResult Download(string fileGuid, string fileName)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                TempData[fileGuid] = null;
                return File(data, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            }
            else
                return new EmptyResult();
        }
        [HttpPost]
        public ActionResult ExportGenerator(Models.ExportModel.ExportVM model)
        {
            ExportResponse response = new ExportResponse();
            if (model.selectedExportColumnValue.Count > 0 || model.IsAllColumns)
            {
                model.ExportType = ExportType.Excel;
           
                model.SetExportColumns();
                response = model.ExportOut();
                if (response.IsDownloaded)
                    TempData[response.Guid] = response.memoryStream.ToArray();
            }
            else
                return new EmptyResult();

            return new JsonResult()
            {
                Data = new { FileGuid = response.Guid, FileName = response.FileName }
            };
        }
        [HttpPost]
        public ActionResult PrintGenerator(string url,string filename)
        {
            if (!url.IsNull() && url!="null")
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(url);

                TempData[url] = fileBytes;
            }

            return new JsonResult()
            {
                Data = new { FileUrl = url, FileName = filename+".pdf" }
            };
        }
        [HttpGet]
        public JsonResult DeleteMedia(long MediaId, long BaseId, string ObjectType, long ObjectId)
        {

            bool result = new MediaWorker().DeleteMedia(MediaId, ObjectType, BaseId, ObjectId);

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = "OK",
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        [HttpGet]
        public JsonResult FillMedia(long ObjectId, string ObjectType, string DiffrentObjectType = "")
        {
            List<MediaListDto> mediaList = new MediaWorker().FillMedia(ObjectId, ObjectType, DiffrentObjectType);

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = mediaList,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        [HttpGet]
        public JsonResult HistoryDetail(string Id, string ViewName, string HistoryTitle)
        {
            HistoryVM vm = new HistoryVM();
            if (!string.IsNullOrEmpty(ViewName) && int.Parse(Id) > 0)
            {
                vm.ViewName = ViewName;
                vm.HistoryTitle = HistoryTitle;
                vm.Id = int.Parse(Id);
                vm.GetHistory();
                List<HistoryResponse> response = vm.historyList;
            }
            return Json(vm, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult FillCounty(string cityId)
        {
            object result = null;

            if (!string.IsNullOrEmpty(cityId))
            {
                if (!Extensions.IsInt64(cityId))
                {
                    cityId = new GenericRepository<City>().FindBy($"NAME='{cityId.ToUpper().Trim()}'").FirstOrDefault()?.Id.ToString();
                }
                var CountyList = new CountyRepository().FindBy("TYPE=1 AND CITY_ID=" + cityId);
                result = CountyList.ToJSON();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetReason(string claimStatus)
        {
            object result = null;

            if (!string.IsNullOrEmpty(claimStatus))
            {
                var ReasonList = ReasonHelper.GetReasonData("ClaimStatus", claimStatus);

                result = ReasonList.ToJSON();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult FillCoverage(string coverageIdList)
        {
            object result = null;

            if (!string.IsNullOrEmpty(coverageIdList))
            {
                var CoverageList = new CoverageRepository().FindBy(orderby: "NAME");

                result = CoverageList.ToJSON();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FillSagmerCoverageType(string planCoverageId)
        {
            object result = "[]";

            if (planCoverageId.IsInt64())
            {
                V_PlanCoverage v_PlanCoverage = new GenericRepository<V_PlanCoverage>().FindBy($"PLAN_COVERAGE_ID={planCoverageId}", orderby: "").FirstOrDefault();
                if (v_PlanCoverage != null)
                {
                    SagmerCoverageCategory sagmerCategory = new GenericRepository<SagmerCoverageCategory>().FindBy($"BRANCH_ID={v_PlanCoverage.BRANCH_ID} AND COVERAGE_TYPE='{v_PlanCoverage.COVERAGE_TYPE}'").FirstOrDefault();
                    if (sagmerCategory != null)
                    {
                        var sagmerCoverageList = new GenericRepository<SagmerCoverage>().FindBy($"SAGMER_COVERAGE_CATEGORY_ID={sagmerCategory.Id}");

                        List<SagmerCoverageType> sagmerCoverageTypeList = new List<SagmerCoverageType>();

                        string idList = string.Empty;
                        foreach (var sagmerCoverage in sagmerCoverageList)
                        {
                            idList += sagmerCoverage.SagmerCoverageTypeId.ToString() + ", ";
                        }
                        sagmerCoverageTypeList = new GenericRepository<SagmerCoverageType>().FindBy($"ID IN({idList.Substring(0, idList.Length - 2)})");

                        result = sagmerCoverageTypeList.ToJSON();
                    }
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FillSagmerCoverage(string sagmerTypeId, string planCoverageId)
        {
            object result = null;

            if (sagmerTypeId.IsInt64() && planCoverageId.IsInt64())
            {
                V_PlanCoverage v_PlanCoverage = new GenericRepository<V_PlanCoverage>().FindBy($"PLAN_COVERAGE_ID={planCoverageId}", orderby: "").FirstOrDefault();
                if (v_PlanCoverage != null)
                {
                    SagmerCoverageCategory sagmerCategory = new GenericRepository<SagmerCoverageCategory>().FindBy($"BRANCH_ID={v_PlanCoverage.BRANCH_ID} AND COVERAGE_TYPE='{v_PlanCoverage.COVERAGE_TYPE}'").FirstOrDefault();
                    if (sagmerCategory != null)
                    {
                        var sagmerCoverage = new GenericRepository<SagmerCoverage>().FindBy($"SAGMER_COVERAGE_CATEGORY_ID={sagmerCategory.Id} AND SAGMER_COVERAGE_TYPE_ID={sagmerTypeId}");
                        result = sagmerCoverage.ToJSON();
                    }
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FillProcessGroup(string processListId)
        {
            object result = null;

            if (!string.IsNullOrEmpty(processListId))
            {
                var ProcessGroupList = new ProcessGroupRepository().FindBy("PROCESS_LIST_ID=" + processListId);
                result = ProcessGroupList.ToJSON();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FillSagmerPlan(string packageTypeId)
        {
            object result = null;

            if (packageTypeId.IsInt64())
            {
                var SagmerPlanList = new GenericRepository<SagmerPlan>().FindBy("SAGMER_PACKAGE_TYPE_ID=" + packageTypeId);
                result = SagmerPlanList.ToJSON();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetExchangeRate()
        {
            object result = null;
            var ordn = Request["Ordn"];
            if (!string.IsNullOrEmpty(ordn))
            {
                var ExChange = new GenericRepository<ExchangeRate>().FindBy($"CURRENCY_TYPE = '{ordn}'", orderby: "ID DESC").FirstOrDefault();
                result = ExChange.ToJSON();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CalculatePremium(FormCollection form)
        {
            object result = null;
            try
            {
                var premiumStr = form["PREMIUM"].IsNull() ? "0" : form["PREMIUM"].Replace('.', ',');
                var taxStr = form["TAX"].IsNull() ? "0" : form["TAX"].Replace('.', ',');
                var exRateStr = form["hdExchangeRateVal"].IsNull() ? "1,00" : form["hdExchangeRateVal"].Replace('.', ',');


                decimal premium = premiumStr.IsNumeric() ? decimal.Parse(premiumStr) : throw new Exception("Hatalı Prim Tutarı Girişi!!!");
                decimal tax = taxStr.IsNumeric() ? decimal.Parse(taxStr) : throw new Exception("Hatalı Bsmv Tutarı Girişi!!!");
                decimal exRate = exRateStr.IsNumeric() ? decimal.Parse(exRateStr) : throw new Exception("Hatalı Kur Bilgisi!!!");

                decimal totalAmount = (premium + tax) * exRate;
                string totalAmountStr = totalAmount.ToString("#0.00");
                result = totalAmountStr;
            }
            catch (Exception)
            {

                throw;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FillBankBranch(string bankId)
        {
            object result = null;

            if (!string.IsNullOrEmpty(bankId))
            {
                var BankBrachListList = new BankBranchRepository().FindBy("BANK_ID=" + bankId);
                result = BankBrachListList.ToJSON();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetSubproduct(string subproductId)
        {
            object result = null;

            if (subproductId.IsInt64())
            {
                var subProduct = new GenericRepository<Subproduct>().FindById(long.Parse(subproductId));
                result = subProduct.ToJSON();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ClearTempData(string key = "Alert")
        {
            TempData[key] = string.Empty;

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = "OK",
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }
        [HttpPost]
        public JsonResult FillProduct(string companyId)
        {
            object result = null;

            if (!string.IsNullOrEmpty(companyId))
            {
                var ProductList = new ProductRepository().FindBy("COMPANY_ID IN (" + companyId+")", orderby: "NAME");
                result = ProductList.ToJSON();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FillProductbyCompanyList(List<string> companyIdList)
        {
            object result = null;

            if (companyIdList!=null && companyIdList.Count>0)
            {
                string compLst = string.Join(",", companyIdList.ToArray());
                var ProductList = new ProductRepository().FindBy($"COMPANY_ID IN({compLst})", orderby: "NAME");
                result = ProductList.ToJSON();
            }
            else
            {
                var ProductList = new ProductRepository().FindBy(orderby: "NAME");
                result = ProductList.ToJSON();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FillPackage(string productId)
        {
            object result = null;

            if (!string.IsNullOrEmpty(productId))
            {
                var PackageList = new GenericRepository<V_Package>().FindBy("PRODUCT_ID=" + productId, orderby: "PACKAGE_NAME");
                result = PackageList.ToJSON();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]  
        public JsonResult GetPayrollbyId(string payrollId)
        {
            object result = null;

            if (!string.IsNullOrEmpty(payrollId))
            {
                var PackageList = new GenericRepository<V_Payroll>().FindBy("PAYROLL_ID=" + payrollId, orderby: "PAYROLL_ID", fetchDeletedRows: true);
                result = PackageList.ToJSON();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FillProductBranch(string branchId)
        {
            object result = null;
            var companyId = Request["companyId"];
            if (!string.IsNullOrEmpty(branchId) && !string.IsNullOrEmpty(companyId))
            {
                var ProductList = new GenericRepository<V_Product>().FindBy("BRANCH_ID = :branchId AND COMPANY_ID = :companyId", orderby: "PRODUCT_NAME", parameters: new { branchId, companyId });

                if (ProductList != null)
                {
                    List<dynamic> productList = new List<dynamic>();

                    foreach (var item in ProductList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.PRODUCT_NAME = item.PRODUCT_NAME;
                        listItem.PRODUCT_ID = item.PRODUCT_ID.ToString();

                        productList.Add(listItem);
                    }

                    result = productList.ToJSON();
                }

            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FillSubProduct(List<string> productIdList)
        {
            object result = null;
            string query = "PRODUCT_ID IN (";

            if (productIdList.Any())
            {
                foreach (var productId in productIdList)
                {
                    if (!string.IsNullOrEmpty(productId))
                    {
                        query += productId + ", ";
                    }
                }
                query = query.Substring(0, query.Length - 2);
                query += ")";

                var SubProductList = new SubproductRepository().FindBy(query, orderby: "NAME");
                result = SubProductList.ToJSON();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetProviderbyGroup(string providerGroupListId)
        {
            object result = null;

            if (!providerGroupListId.IsNull())
            {
                var resultProvider = new GenericRepository<V_Provider>().FindBy($"PROVIDER_GROUP_ID IN ({providerGroupListId})", orderby: "PROVIDER_ID");
                result = resultProvider.ToJSON();
            }
            else
            {
                var resultProvider = new GenericRepository<V_Provider>().FindBy(orderby: "PROVIDER_ID");
                result = resultProvider.ToJSON();
            }

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            //return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FillPlan(List<string> subProductIdList)
        {
            object result = null;
            string query = "SUBPRODUCT_ID IN (";

            if (subProductIdList.Any())
            {
                foreach (var subProductId in subProductIdList)
                {
                    if (!string.IsNullOrEmpty(subProductId))
                    {
                        query += subProductId + ", ";
                    }
                }
                query = query.Substring(0, query.Length - 2);
                query += ")";

                var PlanList = new PlanRepository().FindBy(query, orderby: "NAME");
                result = PlanList.ToJSON();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FillAgency(string companyId)
        {
            object result = null;

            if (!string.IsNullOrEmpty(companyId))
            {
                var AgencyList = new AgencyRepository().FindBy("COMPANY_ID=" + companyId,
                                                           orderby: "NAME");
                result = AgencyList.ToJSON();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FillCoverageByPackage(string packageId)
        {
            object result = "[]";

            if (packageId.IsInt64())
            {
                var resultCoverage = new GenericRepository<V_PackagePlanCoverage>().FindBy("PACKAGE_ID =:packageId AND IS_MAIN_COVERAGE = :isMainCoverage", orderby: "COVERAGE_ID", parameters: new { packageId, isMainCoverage = "1" });
                if (resultCoverage != null)
                {
                    List<dynamic> coverageList = new List<dynamic>();

                    foreach (var item in resultCoverage)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.COVERAGE_NAME = item.COVERAGE_NAME;
                        listItem.COVERAGE_ID = item.COVERAGE_ID;

                        coverageList.Add(listItem);
                    }

                    result = coverageList.ToJSON();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FillProcessList(string type)
        {
            object result = null;

            if (!string.IsNullOrEmpty(type))
            {
                var ProcessList = new ProcessListRepository().FindBy("TYPE=" + type);
                result = ProcessList.ToJSON();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}