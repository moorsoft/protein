﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Protein.Common.Constants;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Common.Dto;
using static Protein.Common.Entities.ProteinEntities;
using Protein.Common.Extensions;
using System.Text;
using Protein.Web.ActionFilter.LoginAttr;
using static Protein.Common.Constants.Constants;
using Protein.Common.Enums;
using Protein.Common.Messaging;
using Protein.Common.Helpers;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Web.Controllers
{
    [LoginControl]
    public class UserController : Controller
    {
        #region Organization  
        // GET: Organization List
        [LoginControl]
        public ActionResult Organization()
        {
            try
            {
                ViewBag.organizationName = string.Empty;
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var organizationList = new OrganizationRepository().FindBy();
                ViewBag.OrganizationList = organizationList;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        // GET: Organization Filter
        [HttpPost]
        [LoginControl]
        public ActionResult OrganizationFilter(FormCollection form)
        {
            try
            {
                ViewBag.organizationName = form["organizationName"];
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                string whereConditition = !String.IsNullOrEmpty(form["organizationName"]) ? $" NAME LIKE '%{form["organizationName"]}%' AND" : "";
                var OrganizationList = new OrganizationRepository().FindBy(conditions: whereConditition.IsNull() ? "" : whereConditition.Substring(0, whereConditition.Length - 3));
                ViewBag.OrganizationList = OrganizationList;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View("Organization");
        }
        [LoginControl]
        public ActionResult OrganizationDelete(Int64 Id)
        {
            try
            {
                var unitCheck = new UnitRepository().FindBy("ORGANIZATION_ID = :organizationId", parameters: new { organizationId = Id });
                if (!unitCheck.Any())
                {
                    var repo = new OrganizationRepository();
                    var result = repo.FindById(Id);
                    result.Status = "1";
                    var spResponseOrganization = repo.Update(result);
                    if (spResponseOrganization.Code == "100")
                    {
                        TempData["Alert"] = $"swAlert('Silme İşlemi Başarılı','{result.Name} başarıyla silindi.','success')";
                    }
                    else
                    {
                        throw new Exception(spResponseOrganization.Code + " : " + spResponseOrganization.Message);
                    }
                }
                else
                {
                    TempData["Alert"] = $"swAlert('Bilgi','Seçilen Organizasyona ait Aktif Birim Bulunduğundan Silme İşlemi Gerçekleştirilemiyor.','info')";
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Organization");
        }

        // GET: Organization Form
        [LoginControl]
        public ActionResult OrganizationForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                if (id != 0)
                {
                    var organization = new OrganizationRepository().FindById(id);
                    ViewBag.Organization = organization ?? null;
                    if (ViewBag.Organization == null)
                    {
                        TempData["Alert"] = $"swAlert('Hata','Seçilen Organizasyon Bulunamadı.','warning')";
                        return RedirectToAction("Organization");
                    }
                }
                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Organizasyon Güncelle";
                        break;

                    default:
                        ViewBag.Title = "Organizasyon Ekle";
                        break;
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult OrganizationFormSave(FormCollection form)
        {
            try
            {
                var organizationId = form["hdOrganizationId"];
                var name = form["organizationName"];

                var organization = new Organization
                {
                    Id = long.Parse(organizationId),
                    Name = name
                };

                var spResponseOrganization = new OrganizationRepository().Insert(organization);
                if (spResponseOrganization.Code == "100")
                {
                    var resultText = organizationId != "0" ? "güncellendi" : "kaydedildi";
                    TempData["Alert"] = $"swAlert('İşlemi Başarılı','{organization.Name} başarıyla {resultText}.','success')";
                }
                else
                {
                    throw new Exception(spResponseOrganization.Code + " : " + spResponseOrganization.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Organization");
        }
        #endregion

        #region Category
        // GET: Category List
        [LoginControl]
        public ActionResult Category()
        {
            try
            {
                //Filter values
                ViewBag.categoryName = string.Empty;
                ViewBag.categoryParent = string.Empty;
                ViewBag.categoryOrganization = string.Empty;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.ParentList = new List<Category>();
                ViewBag.OrganizationList = new List<Organization>();

                var ParentList = new CategoryRepository().FindBy();
                ViewBag.ParentList = ParentList;

                var OrganizationList = new OrganizationRepository().FindBy();
                ViewBag.OrganizationList = OrganizationList;


                var result =new GenericRepository<V_Category>().FindBy(orderby: "CATEGORY_ID");
                ViewBag.CategoryList = result;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        // GET: Category Filter
        [HttpPost]
        [LoginControl]
        public ActionResult CategoryFilter(FormCollection form)
        {
            try
            {
                //Filter values
                ViewBag.categoryName = form["categoryName"];
                ViewBag.categoryParent = form["categoryParent"];
                ViewBag.categoryOrganization = form["categoryOrganization"];
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                string whereConditition = !String.IsNullOrEmpty(form["categoryName"]) ? $" CATEGORY_NAME LIKE '%{form["categoryName"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["categoryParent"]) ? $" PARENT_ID={long.Parse(form["categoryParent"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["categoryOrganization"]) ? $" ORGANIZATION_ID={long.Parse(form["categoryOrganization"])} AND" : "";

                var result = new GenericRepository<V_Category>().FindBy(string.IsNullOrEmpty(whereConditition) ? string.Empty : whereConditition.Substring(0, whereConditition.Length - 3), orderby: "CATEGORY_ID");
                ViewBag.CategoryList = result;

                var ParentList = new CategoryRepository().FindBy();
                ViewBag.ParentList = ParentList;

                var OrganizationList = new OrganizationRepository().FindBy();
                ViewBag.OrganizationList = OrganizationList;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View("Category");
        }
        [LoginControl]
        public ActionResult CategoryDelete(Int64 Id)
        {
            try
            {
                var assetCheck = new AssetRepository().FindBy("CATEGORY_ID = :categoryId", parameters: new { categoryId = Id });

                if (!assetCheck.Any())
                {
                    var repo = new CategoryRepository();
                    var result = repo.FindById(Id);
                    result.Status = "1";
                    var spResponseCategory = repo.Update(result);
                    if (spResponseCategory.Code == "100")
                    {
                        TempData["Alert"] = $"swAlert('Silme İşlemi Başarılı','{result.Name} başarıyla silindi.','success')";
                    }
                    else
                    {
                        throw new Exception(spResponseCategory.Code + " : " + spResponseCategory.Message);
                    }
                }
                else
                {
                    TempData["Alert"] = $"swAlert('Bilgi','Seçilen Kategoriye ait Aktif Öğe Bulunduğundan Silme İşlemi Gerçekleştirilemiyor.','info')";
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Category");
        }

        // GET: Category Form
        [LoginControl]
        public ActionResult CategoryForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var ParentList = new CategoryRepository().FindBy($"ID != {id}");
                ViewBag.ParentList = ParentList;

                var OrganizationList = new OrganizationRepository().FindBy();
                ViewBag.OrganizationList = OrganizationList;

                if (id != 0)
                {
                    var category = new CategoryRepository().FindById(id);
                    ViewBag.Category = category ?? null;
                    if (ViewBag.Category == null)
                    {
                        TempData["Alert"] = $"swAlert('Hata','Seçilen Kategori Bulunamadı.','warning')";
                        return RedirectToAction("Category");
                    }
                    ViewBag.categoryOrganization = category.OrganizationId.ToString();
                    if (category.ParentId != null)
                        ViewBag.categoryParent = category.ParentId.ToString();
                }
                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Kategori Güncelle";
                        break;

                    default:
                        ViewBag.Title = "Kategori Ekle";
                        break;
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult CategoryFormSave(FormCollection form)
        {
            try
            {
                var categoryId = form["hdCategoryId"];
                var name = form["categoryName"];
                var parent = form["categoryParent"];
                var organization = form["categoryOrganization"];

                if (organization == null)
                {
                    TempData["Alert"] = $"swAlert('Hata','Lütfen Organizasyon seçiniz.','warning')";
                    return RedirectToAction("Category");
                }

                var category = new Category
                {
                    Id = long.Parse(categoryId),
                    Name = name,
                    ParentId = !String.IsNullOrEmpty(parent) ? (long?)long.Parse(parent) : null,
                    OrganizationId = (long?)long.Parse(organization),
                };
                var spResponseCategory = new CategoryRepository().Insert(category);

                if (spResponseCategory.Code == "100")
                {
                    var resultText = form["hdCategoryId"] != "0" ? "güncellendi" : "kaydedildi";
                    TempData["Alert"] = $"swAlert('İşlemi Başarılı','{category.Name} başarıyla {resultText}.','success')";
                }
                else
                {
                    throw new Exception(spResponseCategory.Code + " : " + spResponseCategory.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Category");
        }

        #endregion

        #region Asset
        // GET: Asset List
        [LoginControl]
        public ActionResult Asset()
        {
            try
            {
                //Filter values
                ViewBag.assetName = string.Empty;
                ViewBag.assetCategory = string.Empty;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.CategoryList = new List<Category>();

                var CategoryList = new CategoryRepository().FindBy();
                ViewBag.CategoryList = CategoryList;


                var result =new GenericRepository<V_Asset>().FindBy(orderby: "ASSET_ID");
                ViewBag.AssetList = result;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        // GET: Asset Filter
        [HttpPost]
        [LoginControl]
        public ActionResult AssetFilter(FormCollection form)
        {
            try
            {
                //Filter values
                ViewBag.assetName = form["assetName"];
                ViewBag.assetCategory = form["assetCategory"];
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                string whereConditition = !String.IsNullOrEmpty(form["assetName"]) ? $" ASSET_NAME LIKE '%{form["assetName"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["assetCategory"]) ? $" CATEGORY_ID={long.Parse(form["assetCategory"])} AND" : "";

                var result = new GenericRepository<V_Asset>().FindBy(string.IsNullOrEmpty(whereConditition) ? string.Empty : whereConditition.Substring(0, whereConditition.Length - 3),orderby: "ASSET_ID");
                ViewBag.AssetList = result;

                var CategoryList = new CategoryRepository().FindBy();
                ViewBag.CategoryList = CategoryList;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View("Asset");
        }
        [LoginControl]
        public ActionResult AssetDelete(Int64 Id)
        {
            try
            {
                var privilegeCheck = new PrivilegeRepository().FindBy("ASSET_ID = :assetId", parameters: new { assetId = Id });

                if (!privilegeCheck.Any())
                {
                    var repo = new AssetRepository();
                    var result = repo.FindById(Id);
                    result.Status = "1";
                    var spResponseAsset = repo.Update(result);
                    if (spResponseAsset.Code == "100")
                    {
                        TempData["Alert"] = $"swAlert('Silme İşlemi Başarılı','{result.Name} başarıyla silindi.','success')";
                    }
                    else
                    {
                        throw new Exception(spResponseAsset.Code + " : " + spResponseAsset.Message);
                    }
                }
                else
                {
                    TempData["Alert"] = $"swAlert('Bilgi','Seçilen Öğeye ait Aktif Yetki Bulunduğundan Silme İşlemi Gerçekleştirilemiyor.','info')";
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Asset");
        }

        // GET: Asset Form
        [LoginControl]
        public ActionResult AssetForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var CategoryList = new CategoryRepository().FindBy();
                ViewBag.CategoryList = CategoryList;

                if (id != 0)
                {
                    var asset = new AssetRepository().FindById(id);
                    ViewBag.Asset = asset ?? null;
                    if (ViewBag.Asset == null)
                    {
                        TempData["Alert"] = $"swAlert('Hata','Seçilen Öğe Bulunamadı.','warning')";
                        return RedirectToAction("Asset");
                    }
                    if (asset.CategoryId != null)
                        ViewBag.assetCategory = asset.CategoryId.ToString();
                }
                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Öğe Güncelle";
                        break;

                    default:
                        ViewBag.Title = "Öğe Ekle";
                        break;
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult AssetFormSave(FormCollection form)
        {
            try
            {
                var assetId = form["hdAssetId"];
                var name = form["assetName"];
                var category = form["assetCategory"];

                var asset = new Asset
                {
                    Id = long.Parse(assetId),
                    Name = name,
                    CategoryId = !String.IsNullOrEmpty(category) ? (long?)long.Parse(category) : null,
                };
                var spResponseAsset = new AssetRepository().Insert(asset);

                if (spResponseAsset.Code == "100")
                {
                    var resultText = form["hdAssetId"] != "0" ? "güncellendi" : "kaydedildi";
                    TempData["Alert"] = $"swAlert('İşlemi Başarılı','{asset.Name} başarıyla {resultText}.','success')";
                }
                else
                {
                    throw new Exception(spResponseAsset.Code + " : " + spResponseAsset.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Asset");
        }

        #endregion

        #region Unit
        // GET: Unit List
        [LoginControl]
        public ActionResult Unit()
        {
            try
            {
                //Filter values
                ViewBag.unitName = string.Empty;
                ViewBag.unitParent = string.Empty;
                ViewBag.unitOrganization = string.Empty;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.ParentList = new List<Unit>();
                ViewBag.OrganizationList = new List<Organization>();

                var ParentList = new UnitRepository().FindBy();
                ViewBag.ParentList = ParentList;

                var OrganizationList = new OrganizationRepository().FindBy();
                ViewBag.OrganizationList = OrganizationList;


                var result = new GenericRepository<V_Unit>().FindBy(orderby: "UNIT_ID");
                ViewBag.UnitList = result;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        // GET: Unit Filter
        [HttpPost]
        [LoginControl]
        public ActionResult UnitFilter(FormCollection form)
        {
            try
            {
                //Filter values
                ViewBag.unitName = form["unitName"];
                ViewBag.unitParent = form["unitParent"];
                ViewBag.unitOrganization = form["unitOrganization"];
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                string whereConditition = !String.IsNullOrEmpty(form["unitName"]) ? $" UNIT_NAME LIKE '%{form["unitName"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["unitParent"]) ? $" PARENT_ID={long.Parse(form["unitParent"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["unitOrganization"]) ? $" ORGANIZATION_ID={long.Parse(form["unitOrganization"])} AND" : "";

                var result = new GenericRepository<V_Unit>().FindBy(string.IsNullOrEmpty(whereConditition) ? string.Empty : whereConditition.Substring(0, whereConditition.Length - 3), orderby: "UNIT_ID");
                ViewBag.UnitList = result;

                var ParentList = new UnitRepository().FindBy();
                ViewBag.ParentList = ParentList;

                var OrganizationList = new OrganizationRepository().FindBy();
                ViewBag.OrganizationList = OrganizationList;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View("Unit");
        }
        [LoginControl]
        public ActionResult UnitDelete(Int64 Id)
        {
            try
            {
                var cadreCheck = new CadreRepository().FindBy("UNIT_ID = :unitId", parameters: new { unitId = Id });

                var unitCheck = new UnitRepository().FindBy("PARENT_ID = :parentId", parameters: new { parentId = Id });

                if (!cadreCheck.Any() && !unitCheck.Any())
                {
                    var repo = new UnitRepository();
                    var result = repo.FindById(Id);
                    result.Status = "1";
                    var spResponseUnit = repo.Update(result);
                    if (spResponseUnit.Code == "100")
                    {
                        TempData["Alert"] = $"swAlert('Silme İşlemi Başarılı','{result.Name} başarıyla silindi.','success')";
                    }
                    else
                    {
                        throw new Exception(spResponseUnit.Code + " : " + spResponseUnit.Message);
                    }
                }
                else
                {
                    TempData["Alert"] = $"swAlert('Bilgi','Seçilen Birime ait Aktif Kadro/Alt Birim Bulunduğundan Silme İşlemi Gerçekleştirilemiyor.','info')";
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Unit");
        }

        // GET: Provider Form
        [LoginControl]
        public ActionResult UnitForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var OrganizationList = new OrganizationRepository().FindBy();
                ViewBag.OrganizationList = OrganizationList;

                var ParentList = new UnitRepository().FindBy($"ID!={id}");
                ViewBag.ParentList = ParentList;

                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Birim Güncelle";
                        if (id != 0)
                        {
                            var unit = new UnitRepository().FindById(id);
                            ViewBag.Unit = unit ?? null;
                            if (ViewBag.Unit == null)
                            {
                                TempData["Alert"] = $"swAlert('Hata','Seçilen Birim Bulunamadı.','warning')";
                                return RedirectToAction("Unit");
                            }

                            if (unit.OrganizationId != null)
                                ViewBag.unitOrganization = unit.OrganizationId.ToString();
                            if (unit.ParentId != null)
                                ViewBag.unitParent = unit.ParentId.ToString();
                        }
                        
                        break;

                    default:
                        ViewBag.Title = "Birim Ekle";
                        break;
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult UnitFormSave(FormCollection form)
        {
            try
            {
                var unitId = form["hdUnitId"];
                var name = form["unitName"];
                var parent = form["unitParent"];
                var organization = form["unitOrganization"];

                var unit = new Unit
                {
                    Id = long.Parse(unitId),
                    Name = name,
                    ParentId = !String.IsNullOrEmpty(parent) ? (long?)long.Parse(parent) : null,
                    OrganizationId = !String.IsNullOrEmpty(organization) ? (long?)long.Parse(organization) : null,
                };
                var spResponseUnit = new UnitRepository().Insert(unit);

                if (spResponseUnit.Code == "100")
                {
                    var resultText = form["hdUnitId"] != "0" ? "güncellendi" : "kaydedildi";
                    TempData["Alert"] = $"swAlert('İşlemi Başarılı','{unit.Name} başarıyla {resultText}.','success')";
                }
                else
                {
                    throw new Exception(spResponseUnit.Code + " : " + spResponseUnit.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Unit");
        }

        #endregion

        #region Cadre
        // GET: Cadre List
        [LoginControl]
        public ActionResult Cadre()
        {
            try
            {
                //Filter values
                ViewBag.cadreName = string.Empty;
                ViewBag.cadreUnit = string.Empty;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.UnitList = new List<Unit>();

                var UnitList = new UnitRepository().FindBy();
                ViewBag.UnitList = UnitList;


                var result = new GenericRepository<V_Cadre>().FindBy(orderby: "CADRE_ID");
                ViewBag.CadreList = result;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        // GET: Cadre Filter
        [HttpPost]
        [LoginControl]
        public ActionResult CadreFilter(FormCollection form)
        {
            try
            {
                //Filter values
                ViewBag.cadreName = form["cadreName"];
                ViewBag.cadreUnit = form["cadreUnit"];
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                string whereConditition = !String.IsNullOrEmpty(form["cadreName"]) ? $" CADRE_NAME LIKE '%{form["cadreName"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["cadreUnit"]) ? $" UNIT_ID={long.Parse(form["cadreUnit"])} AND" : "";

                var result = new GenericRepository<V_Cadre>().FindBy(string.IsNullOrEmpty(whereConditition) ? string.Empty : whereConditition.Substring(0, whereConditition.Length - 3),orderby: "CADRE_ID");
                ViewBag.CadreList = result;

                var UnitList = new UnitRepository().FindBy();
                ViewBag.UnitList = UnitList;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View("Cadre");
        }
        [LoginControl]
        public ActionResult CadreDelete(Int64 Id)
        {
            try
            {
                var cadrePrivilegeCheck = new CadrePrivilegeRepository().FindBy("CADRE_ID = :cadreId", parameters: new { cadreId = Id });

                if (!cadrePrivilegeCheck.Any())
                {
                    var repo = new CadreRepository();
                    var result = repo.FindById(Id);
                    result.Status = "1";
                    var spResponseCadre = repo.Update(result);
                    if (spResponseCadre.Code == "100")
                    {
                        TempData["Alert"] = $"swAlert('Silme İşlemi Başarılı','{result.Name} başarıyla silindi.','success')";
                    }
                    else
                    {
                        throw new Exception(spResponseCadre.Code + " : " + spResponseCadre.Message);
                    }
                }
                else
                {
                    TempData["Alert"] = $"swAlert('Bilgi','Seçilen Kadroya ait Aktif Yetki Ataması Bulunduğundan Silme İşlemi Gerçekleştirilemiyor.','info')";
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Cadre");
        }

        // GET: Cadre Form
        [LoginControl]
        public ActionResult CadreForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var UnitList = new UnitRepository().FindBy();
                ViewBag.UnitList = UnitList;

                if (id != 0)
                {
                    var cadre = new CadreRepository().FindById(id);
                    ViewBag.Cadre = cadre ?? null;
                    if (ViewBag.Cadre == null)
                    {
                        TempData["Alert"] = $"swAlert('Hata','Seçilen Kadro Bulunamadı.','warning')";
                        return RedirectToAction("Cadre");
                    }
                    if (cadre.UnitId != null)
                        ViewBag.unit = cadre.UnitId.ToString();
                }
                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Kadro Güncelle";
                        break;

                    default:
                        ViewBag.Title = "Kadro Ekle";
                        break;
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult CadreFormSave(FormCollection form)
        {
            try
            {
                var cadreId = form["hdCadreId"];
                var name = form["cadreName"];
                var unit = form["cadreUnit"];

                var cadre = new Cadre
                {
                    Id = long.Parse(cadreId),
                    Name = name,
                    UnitId = !String.IsNullOrEmpty(unit) ? (long?)long.Parse(unit) : null,
                };
                var spResponseCadre = new CadreRepository().Insert(cadre);

                if (spResponseCadre.Code == "100")
                {
                    var resultText = form["hdCadreId"] != "0" ? "güncellendi" : "kaydedildi";
                    TempData["Alert"] = $"swAlert('İşlemi Başarılı','{cadre.Name} başarıyla {resultText}.','success')";
                }
                else
                {
                    throw new Exception(spResponseCadre.Code + " : " + spResponseCadre.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Cadre");
        }

        // GET: CadreAssignPrivilege
        [LoginControl]
        public ActionResult CadreAssignPrivilege(Int64 id = 0)
        {
            try
            {
                ViewBag.Title = "Kadro Yetki Atama";
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var PrivilegeList = new PrivilegeRepository().FindBy();
                ViewBag.PrivilegeList = PrivilegeList;

                if (id != 0)
                {
                    var cadre = new CadreRepository().FindById(id);
                    ViewBag.Cadre = cadre ?? null;
                    if (ViewBag.Cadre == null)
                    {
                        TempData["Alert"] = $"swAlert('Hata','Seçilen Kadro Bulunamadı.','warning')";
                        return RedirectToAction("Cadre");
                    }
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult CadreAssignPrivilege(FormCollection form)
        {
            try
            {
                var cadreId = form["hdCadreId"];
                var privilegeId = form["privilegeId"];

                List<CadrePrivilege> cadrePrivileges = new CadrePrivilegeRepository().FindBy("CADRE_ID = " + cadreId + " AND PRIVILEGE_ID = " + privilegeId);
                if (cadrePrivileges != null && cadrePrivileges.Count > 0)
                {
                    TempData["Alert"] = $"swAlert('Hata','Girilen Yetki Zaten Kadroya Atanmıştır.','warning')";
                    return RedirectToAction("Cadre");
                }

                var cadrePrivilege = new CadrePrivilege
                {
                    Id = 0,
                    PrivilegeId = long.Parse(privilegeId),
                    CadreId = long.Parse(cadreId),
                };
                var spResponseCadrePrivilege = new CadrePrivilegeRepository().Insert(cadrePrivilege);

                if (spResponseCadrePrivilege.Code != "100")
                {
                    throw new Exception(spResponseCadrePrivilege.Code + " : " + spResponseCadrePrivilege.Message);
                }
                else
                {
                    TempData["Alert"] = $"swAlert('Atama İşlemi Başarılı','','success')";
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Cadre");
        }

        // GET: CadrePrivileges
        [LoginControl]
        public ActionResult CadrePrivileges(Int64 id = 0)
        {
            try
            {
                ViewBag.Title = "Kadro Yetkileri";
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                List<CadrePrivilege> CadrePrivilegeList = new CadrePrivilegeRepository().FindBy("CADRE_ID = " + id);
                ViewBag.CadrePrivilegeList = CadrePrivilegeList;

                if (CadrePrivilegeList == null || CadrePrivilegeList.Count == 0)
                {
                    TempData["Alert"] = $"swAlert('Bilgi','Seçilen Kadroya Ait Yetki Bulunamadı.','information')";
                    return RedirectToAction("Cadre");
                }

                List<Privilege> privilegeList = new List<Privilege>();
                foreach (CadrePrivilege cadrePrivilege in CadrePrivilegeList)
                {
                    var privilege = new PrivilegeRepository().FindById(cadrePrivilege.PrivilegeId);
                    privilegeList.Add(privilege);
                }
                ViewBag.PrivilegeList = privilegeList;

                if (id != 0)
                {
                    var cadre = new CadreRepository().FindById(id);
                    ViewBag.Cadre = cadre ?? null;
                    if (ViewBag.Cadre == null)
                    {
                        TempData["Alert"] = $"swAlert('Hata','Seçilen Kadro Bulunamadı.','warning')";
                        return RedirectToAction("Cadre");
                    }
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        // GET: CadrePrivilegeDelete
        [LoginControl]
        public ActionResult CadrePrivilegeDelete(Int64 id = 0)
        {
            try
            {
                var repo = new CadrePrivilegeRepository();
                var result = repo.FindById(id);
                result.Status = "1";
                var spResponseCadrePrivilege = repo.Update(result);
                if (spResponseCadrePrivilege.Code == "100")
                {
                    TempData["Alert"] = $"swAlert('Silme İşlemi Başarılı','Kadro Yetkisi: {result.Id} başarıyla silindi.','success')";
                }
                else
                {
                    throw new Exception(spResponseCadrePrivilege.Code + " : " + spResponseCadrePrivilege.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Cadre");
        }

        public ActionResult CadrePersonnel(Int64 id = 0)
        {
            try
            {
                if (id != 0)
                {


                    ViewBag.Title = "Kadrodaki Personeller";
                    TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                    var listOfPersonnel = new GenericRepository<V_PersonnelCadre>().FindBy("CADRE_ID =:id", orderby: "CADRE_ID");

                    if (listOfPersonnel.Count == 0)
                    {
                        TempData["Alert"] = $"swAlert('Hata','Seçilen Kadroya Ait Personel Bulunamadı.','warning')";
                        return RedirectToAction("Cadre");
                    }
                    ViewBag.PersonnelList = listOfPersonnel;
                    ViewBag.Cadre = listOfPersonnel[0].CADRE_NAME;
                    
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        #endregion

        #region Personnel

        // GET: Personnel List
        [LoginControl]
        public ActionResult Personnel()
        {
            try
            {
                //Filter values
                ViewBag.contactIdentityNo = string.Empty;
                ViewBag.userUsername = string.Empty;
                ViewBag.userEmail = string.Empty;
                ViewBag.userMobile = string.Empty;
                ViewBag.personFirstName = string.Empty;
                ViewBag.personLastName = string.Empty;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;


                var result =new GenericRepository<V_Personnel>().FindBy(orderby: "PERSONNEL_ID");
                ViewBag.PersonnelList = result;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        // GET: Personnel Filter
        [HttpPost]
        [LoginControl]
        public ActionResult PersonnelFilter(FormCollection form)
        {
            try
            {
                //Filter values
                ViewBag.contactIdentityNo = form["contactIdentityNo"];

                ViewBag.userUsername = form["userUsername"];
                ViewBag.userEmail = form["userEmail"];
                ViewBag.userMobile = form["userMobile"];

                ViewBag.personFirstName = form["personFirstName"];
                ViewBag.personLastName = form["personLastName"];

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                string whereConditition =form["contactIdentityNo"].IsInt64() ? $" IDENTITY_NO = {form["contactIdentityNo"]} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["userUsername"]) ? $" USERNAME LIKE '%{form["userUsername"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["userEmail"]) ? $" EMAIL LIKE '%{form["userEmail"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["userMobile"]) ? $" MOBILE LIKE '%{form["userMobile"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["personFirstName"]) ? $" FIRST_NAME LIKE '%{form["personFirstName"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["personLastName"]) ? $" LAST_NAME LIKE '%{form["personLastName"]}%' AND" : "";

                var result = new GenericRepository<V_Personnel>().FindBy(string.IsNullOrEmpty(whereConditition) ? string.Empty : whereConditition.Substring(0, whereConditition.Length - 3),orderby: "PERSONNEL_ID");
                ViewBag.PersonnelList = result;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View("Personnel");
        }
        [LoginControl]
        public ActionResult PersonnelDelete(Int64 Id)
        {
            try
            {
                var repo = new PersonnelRepository();
                var result = repo.FindById(Id);
                result.Status = ((int)Status.SILINDI).ToString();
                var spResponsePersonnel = repo.Update(result);
                if (spResponsePersonnel.Code == "100")
                {
                    if (result.UserId != null)
                    {
                        var user = new GenericRepository<User>().FindById((long)result.UserId);
                        if (user!=null)
                        {
                            user.Status = ((int)Status.SILINDI).ToString();
                            new GenericRepository<User>().Update(user);
                        }
                    }
                    TempData["Alert"] = $"swAlert('Silme İşlemi Başarılı','Personel: {result.Id} başarıyla silindi.','success')";
                }
                else
                {
                    throw new Exception(spResponsePersonnel.Code + " : " + spResponsePersonnel.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Personnel");
        }

        // GET: Personnel Form
        [LoginControl]
        public ActionResult PersonnelForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var ContactType = LookupHelper.GetLookupData(Constants.LookupTypes.Contact);
                ViewBag.ContactTypeList = ContactType;

                var ProfessionType = LookupHelper.GetLookupData(Constants.LookupTypes.Profession);
                ViewBag.ProfessionTypeList = ProfessionType;

                var Gender = LookupHelper.GetLookupData(Constants.LookupTypes.Gender);
                ViewBag.GenderList = Gender;

                var MaritalStatus = LookupHelper.GetLookupData(Constants.LookupTypes.MaritalStatus);
                ViewBag.MaritalStatusList = MaritalStatus;

                var CountryList = new CountryRepository().FindBy();
                ViewBag.CountryList = CountryList;

                if (id != 0)
                {
                    var personnel = new PersonnelRepository().FindById(id);
                    ViewBag.Personnel = personnel ?? null;
                    if (ViewBag.Personnel == null)
                    {
                        TempData["Alert"] = $"swAlert('Hata','Seçilen Personel Bulunamadı.','warning')";
                        return RedirectToAction("Personnel");
                    }
                    if (personnel.ContactId != null)
                    {
                        var contact = new ContactRepository().FindById(personnel.ContactId.Value);
                        ViewBag.Contact = contact ?? null;

                        List<Person> persons = new PersonRepository().FindBy("CONTACT_ID = " + personnel.ContactId);
                        if (persons != null && persons.Count > 0)
                        {
                            ViewBag.Person = persons[0];
                        }
                    }
                    if (personnel.UserId != null)
                    {
                        var user = new UserRepository().FindById(personnel.UserId.Value);
                        ViewBag.User = user ?? null;
                    }
                }
                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Personel Güncelle";
                        break;

                    default:
                        ViewBag.Title = "Personel Ekle";
                        break;
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult PersonnelFormSave(FormCollection form)
        {
            try
            {
                var personnelId = form["hdPersonnelId"];

                var contactId = form["contact"];
                var contactType = form["contactType"];
                var contactTitle = form["contactTitle"];
                var contactTaxNumber = form["contactTaxNumber"];
                var contactTaxOffice = form["contactTaxOffice"];
                Int64? contactIdentityNo = form["contactIdentityNo"].IsInt64() ? (long?)Convert.ToInt64(form["contactIdentityNo"]) : null;

                var personId = form["person"];
                var personProfessionType = form["personProfessionType"];
                var personFirstName = form["personFirstName"];
                var personMiddleName = form["personMiddleName"];
                var personLastName = form["personLastName"];
                var personBirthdate = form["personBirthdate"];
                var personGender = form["personGender"];
                var personNationality = form["personNationality"];
                var personLicenseNo = form["personLicenseNo"];
                var personBirthplace = form["personBirthplace"];
                var personNameOfFather = form["personNameOfFather"];
                var personPassportNo = form["personPassportNo"];
                var personMaritalStatus = form["personMaritalStatus"];

                var userId = form["user"];
                var userUsername = form["username"];
                var userEmail = form["userEmail"];
                var userMobile = form["userMobile"];

                // username and email check
                List<User> users = null;
                if (userId == null)
                    users = new UserRepository().FindBy("USERNAME = '" + userUsername + "' or EMAIL = '" + userEmail + "'");
                else
                    users = new UserRepository().FindBy("(USERNAME = '" + userUsername + "' or EMAIL = '" + userEmail + "') and ID != " + userId);
                if (users != null && users.Count > 0)
                {
                    TempData["Alert"] = $"swAlert('Hata','Girilen Kullanıcı Adı veya Email, Farklı Bir Kullanıcı Tarafından Kullanılmıştır. Lütfen Tekrar Deneyiniz.','warning')";
                    return RedirectToAction("Personnel");
                }

                var contact = new Contact
                {
                    Id = long.Parse(contactId),
                    Type = contactType,
                    Title = !String.IsNullOrEmpty(contactTitle) ? contactTitle : null,
                    TaxNumber = contactTaxNumber.IsInt64() ? (long?)Convert.ToInt64(contactTaxNumber) : null,
                    TaxOffice = !String.IsNullOrEmpty(contactTaxOffice) ? contactTaxOffice : null,
                    IdentityNo = contactIdentityNo,
                    //CategoryId = !String.IsNullOrEmpty(category) ? (long?)long.Parse(category) : null,
                };
                var spResponseContact = new ContactRepository().Insert(contact);

                if (spResponseContact.Code != "100")
                {
                    throw new Exception(spResponseContact.Code + " : " + spResponseContact.Message);
                }

                var person = new Person
                {
                    Id = long.Parse(personId),
                    ProfessionType = personProfessionType,
                    ContactId = spResponseContact.PkId,
                    FirstName = !String.IsNullOrEmpty(personFirstName) ? personFirstName : null,
                    MiddleName = !String.IsNullOrEmpty(personMiddleName) ? personMiddleName : null,
                    LastName = !String.IsNullOrEmpty(personLastName) ? personLastName : null,
                    Birthdate = !String.IsNullOrEmpty(personBirthdate) ? (DateTime?)DateTime.Parse(personBirthdate) : null,
                    Gender = personGender,
                    NationalityId = !String.IsNullOrEmpty(personNationality) ? (long?)long.Parse(personNationality) : null,
                    Birthplace = !String.IsNullOrEmpty(personBirthplace) ? personBirthplace : null,
                    LicenseNo = !String.IsNullOrEmpty(personLicenseNo) ? personLicenseNo : null,
                    NameOfFather = !String.IsNullOrEmpty(personNameOfFather) ? personNameOfFather : null,
                    PassportNo = !String.IsNullOrEmpty(personPassportNo) ? personPassportNo : null,
                    MaritalStatus = personMaritalStatus,
                };
                var spResponsePerson = new PersonRepository().Insert(person);

                if (spResponsePerson.Code != "100")
                {
                    throw new Exception(spResponsePerson.Code + " : " + spResponsePerson.Message);
                }
                personId = spResponsePerson.PkId.ToString();
                var password = UserHelper.GeneratePassword(personFirstName, personId.ToString());
                var user = new User
                {
                    Id = long.Parse(userId),
                    Username = !String.IsNullOrEmpty(userUsername) ? userUsername : null,
                    Password = password.ToMD5().ToUpper(),
                    Email = !String.IsNullOrEmpty(userEmail) ? userEmail : null,
                    Mobile = !String.IsNullOrEmpty(userMobile) ? userMobile : null,
                    IsRememberMe = "0",
                    Status = "0",
                };
                var spResponseUser = new UserRepository().Insert(user);

                if (spResponseUser.Code != "100")
                {
                    throw new Exception(spResponseUser.Code + " : " + spResponseUser.Message);
                }

                var personnel = new Personnel
                {
                    Id = long.Parse(personnelId),
                    ContactId = spResponseContact.PkId,
                    UserId = spResponseUser.PkId,
                };
                var spResponsePersonnel = new PersonnelRepository().Insert(personnel);
                if (spResponsePersonnel.Code == "100")
                {
                    if (personnelId.Equals("0"))
                    {
                        MailSender msender = new MailSender();

                        msender.SendMessage(
                            new EmailArgs
                            {
                                TO = user.Email,
                                IsHtml = true,
                                Subject = "protein giriş bilgileriniz",
                                Content = $"<b><b> <b>Kullanıcı Adı:</b> {user.Username} <br><br><b>Şifreniz :</b> " + password
                            });
                    }

                    else
                    {
                        TempData["alert"] = $"swalert('işlem başarısız.','Personel bilgileri bulunamadı.','success')";
                    }
                    var resultText = personnelId != "0" ? "güncellendi" : "kaydedildi";

                    TempData["Alert"] = $"swAlert('İşlemi Başarılı','Personel: {spResponsePersonnel.PkId} başarıyla {resultText}.','success')";
                }
                else
                {
                    throw new Exception(spResponsePersonnel.Code + " : " + spResponsePersonnel.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Personnel");
        }

        [LoginControl]
        public  JsonResult RefreshPassword(Int64 personnelId)
        {
            string result = "[]";
            try
            {
                if (personnelId < 0)
                {
                    throw new Exception("Personel Bilgisi Bulunamadı!");
                }

                var vPersonnel= new GenericRepository<V_Personnel>().FindBy(conditions:$"PERSONNEL_ID={personnelId}", orderby: "PERSONNEL_ID").FirstOrDefault();
                if (vPersonnel == null)
                    throw new Exception("Personel Bilgisi Bulunamadı!");

                if (vPersonnel.EMAIL.IsNull())
                    throw new Exception("Kullanıcının mail adresi bulunmamaktadır!");

                var password = UserHelper.GeneratePassword(vPersonnel.FIRST_NAME, vPersonnel.PERSON_ID.ToString());


                var user = new GenericRepository<User>().FindById((long)vPersonnel.USER_ID);
                user.Password = password.ToMD5().ToUpper();
                var spResponseUser = new GenericRepository<User>().Update(user);
                if (spResponseUser.Code!="100")
                {
                    throw new Exception("Şifre Yenilenemedi!");
                }

                MailSender msender = new MailSender();

                msender.SendMessage(
                    new EmailArgs
                    {
                        TO = vPersonnel.EMAIL,
                        IsHtml = true,
                        Subject = "protein giriş bilgileriniz",
                        Content = $"<b><b> <b>Kullanıcı Adı:</b> {vPersonnel.USERNAME} <br><br><b>Şifreniz :</b> " + password
                    });

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(result.ToXML(), JsonRequestBehavior.AllowGet);
        }

        // GET: PersonnelAssignCadre
        [LoginControl]
        public ActionResult PersonnelAssignCadre(Int64 id = 0)
        {
            try
            {
                ViewBag.Title = "Personel Kadro Atama";
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var CadreList = new CadreRepository().FindBy();
                ViewBag.CadreList = CadreList;

                if (id != 0)
                {
                    var personnel = new PersonnelRepository().FindById(id);
                    ViewBag.Personnel = personnel ?? null;
                    if (ViewBag.Personnel == null)
                    {
                        TempData["Alert"] = $"swAlert('Hata','Seçilen Personel Bulunamadı.','warning')";
                        return RedirectToAction("Personnel");
                    }
                    if (personnel.ContactId != null)
                    {
                        var contact = new ContactRepository().FindById(personnel.ContactId.Value);
                        ViewBag.Contact = contact ?? null;

                        List<Person> persons = new PersonRepository().FindBy("CONTACT_ID = " + personnel.ContactId);
                        if (persons != null && persons.Count > 0)
                        {
                            ViewBag.Person = persons[0];
                        }
                    }
                    if (personnel.UserId != null)
                    {
                        var user = new UserRepository().FindById(personnel.UserId.Value);
                        ViewBag.User = user ?? null;
                    }
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult PersonnelAssignCadre(FormCollection form)
        {
            try
            {
                var personnelId = form["hdPersonnelId"];
                var cadreId = form["cadreId"];

                List<PersonnelCadre> personnelCadres = new PersonnelCadreRepository().FindBy("CADRE_ID = " + cadreId + " AND PERSONNEL_ID = " + personnelId);
                if (personnelCadres != null && personnelCadres.Count > 0)
                {
                    TempData["Alert"] = $"swAlert('Hata','Girilen Kadro Zaten Personele Atanmıştır.','warning')";
                    return RedirectToAction("Personnel");
                }

                var personnelCadre = new PersonnelCadre
                {
                    Id = 0,
                    PersonnelId = long.Parse(personnelId),
                    CadreId = long.Parse(cadreId),
                };
                var spResponsePersonnelCadre = new PersonnelCadreRepository().Insert(personnelCadre);

                if (spResponsePersonnelCadre.Code != "100")
                {
                    throw new Exception(spResponsePersonnelCadre.Code + " : " + spResponsePersonnelCadre.Message);
                }
                else
                {
                    TempData["Alert"] = $"swAlert('Atama İşlemi Başarılı','','success')";
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Personnel");
        }

        // GET: PersonnelCadres
        [LoginControl]
        public ActionResult PersonnelCadres(Int64 id = 0)
        {
            try
            {
                ViewBag.Title = "Personel Kadroları";
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                List<PersonnelCadre> PersonnelCadreList = new PersonnelCadreRepository().FindBy("PERSONNEL_ID = " + id);
                ViewBag.PersonnelCadreList = PersonnelCadreList;

                if (PersonnelCadreList == null || PersonnelCadreList.Count == 0)
                {
                    TempData["Alert"] = $"swAlert('Hata','Seçilen Personele Ait Kadro Bulunamadı.','warning')";
                    return RedirectToAction("Personnel");
                }

                List<Cadre> cadreList = new List<Cadre>();
                foreach (PersonnelCadre personnelCadre in PersonnelCadreList)
                {
                    var cadre = new CadreRepository().FindById(personnelCadre.CadreId);
                    cadreList.Add(cadre);
                }
                ViewBag.CadreList = cadreList;

                if (id != 0)
                {
                    var personnel = new PersonnelRepository().FindById(id);
                    ViewBag.Personnel = personnel ?? null;
                    if (ViewBag.Personnel == null)
                    {
                        TempData["Alert"] = $"swAlert('Hata','Seçilen Personel Bulunamadı.','warning')";
                        return RedirectToAction("Personnel");
                    }
                    if (personnel.ContactId != null)
                    {
                        var contact = new ContactRepository().FindById(personnel.ContactId.Value);
                        ViewBag.Contact = contact ?? null;

                        List<Person> persons = new PersonRepository().FindBy("CONTACT_ID = " + personnel.ContactId);
                        if (persons != null && persons.Count > 0)
                        {
                            ViewBag.Person = persons[0];
                        }
                    }
                    if (personnel.UserId != null)
                    {
                        var user = new UserRepository().FindById(personnel.UserId.Value);
                        ViewBag.User = user ?? null;
                    }
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        // GET: PersonnelCadreDelete
        [LoginControl]
        public ActionResult PersonnelCadreDelete(Int64 id = 0)
        {
            try
            {
                var repo = new PersonnelCadreRepository();
                var result = repo.FindById(id);
                result.Status = "1";
                var spResponsePersonnelCadre = repo.Update(result);
                if (spResponsePersonnelCadre.Code == "100")
                {
                    TempData["Alert"] = $"swAlert('Silme İşlemi Başarılı','Personel Kadrosu: {result.Id} başarıyla silindi.','success')";
                }
                else
                {
                    throw new Exception(spResponsePersonnelCadre.Code + " : " + spResponsePersonnelCadre.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Personnel");
        }


        public ActionResult UpdateStatus(Int64 id)
        {

            try
            {
                Personnel personnel = new GenericRepository<Personnel>().FindById(id);

                personnel.Status = (int.Parse(personnel.Status) == (int)ProteinEnums.Status.AKTIF ? (int)ProteinEnums.Status.PASIF : (int)ProteinEnums.Status.AKTIF).ToString();
                //if (personnel.Status == "0")
                //{
                //    personnel.Status = "1";
                //}
                //else
                //{
                //    personnel.Status = "0";
                //}
                var spResponsePersonnel = new PersonnelRepository().Update(personnel);
                if (spResponsePersonnel.Code == "100")
                {
                    TempData["Alert"] = $"swAlert('İşlemi Başarılı','Personel durumu başarıyla değiştirildi.','success')";
                }
                else
                {
                    throw new Exception(spResponsePersonnel.Code + " : " + spResponsePersonnel.Message);
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Personnel");
        }

        #endregion

        #region Privilege
        // GET: Privilege List
        [LoginControl]
        public ActionResult Privilege()
        {
            try
            {
                //Filter values
                ViewBag.privilegeName = string.Empty;
                ViewBag.privilegeAsset = string.Empty;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.AssetList = new List<Asset>();

                var AssetList = new AssetRepository().FindBy();
                ViewBag.AssetList = AssetList;


                var result =new GenericRepository<V_Privilege>().FindBy(orderby: "PRIVILEGE_ID");
                ViewBag.PrivilegeList = result;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        // GET: Privilege Filter
        [HttpPost]
        [LoginControl]
        public ActionResult PrivilegeFilter(FormCollection form)
        {
            try
            {
                //Filter values
                ViewBag.privilegeName = form["privilegeName"];
                ViewBag.privilegeAsset = form["privilegeAsset"];
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                string whereConditition = !String.IsNullOrEmpty(form["privilegeName"]) ? $" PRIVILEGE_NAME LIKE '%{form["privilegeName"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["privilegeAsset"]) ? $" ASSET_ID={long.Parse(form["privilegeAsset"])} AND" : "";

                var result = new GenericRepository<V_Privilege>().FindBy( string.IsNullOrEmpty(whereConditition) ? string.Empty : whereConditition.Substring(0, whereConditition.Length - 3), orderby: "PRIVILEGE_ID");
                ViewBag.PrivilegeList = result;

                var AssetList = new AssetRepository().FindBy();
                ViewBag.AssetList = AssetList;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View("Privilege");
        }
        [LoginControl]
        public ActionResult PrivilegeDelete(Int64 Id)
        {
            try
            {
                var cadrePrivilegeCheck = new CadrePrivilegeRepository().FindBy("PRIVILEGE_ID = :privilegeId", parameters: new { privilegeId = Id });
                if (!cadrePrivilegeCheck.Any())
                {
                    var repo = new PrivilegeRepository();
                    var result = repo.FindById(Id);
                    result.Status = "1";
                    var spResponsePrivilege = repo.Update(result);
                    if (spResponsePrivilege.Code == "100")
                    {
                        TempData["Alert"] = $"swAlert('Silme İşlemi Başarılı','{result.Name} başarıyla silindi.','success')";
                    }
                    else
                    {
                        throw new Exception(spResponsePrivilege.Code + " : " + spResponsePrivilege.Message);
                    }
                }
                else
                {
                    TempData["Alert"] = $"swAlert('Bilgi','Seçilen Yetkiye ait Aktif Yetki Ataması Bulunduğundan Silme İşlemi Gerçekleştirilemiyor.','info')";
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Privilege");
        }

        // GET: Cadre Form
        [LoginControl]
        public ActionResult PrivilegeForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var AssetList = new AssetRepository().FindBy();
                ViewBag.AssetList = AssetList;

                if (id != 0)
                {
                    var privilege = new PrivilegeRepository().FindById(id);
                    ViewBag.Privilege = privilege ?? null;
                    if (ViewBag.Privilege == null)
                    {
                        TempData["Alert"] = $"swAlert('Hata','Seçilen Yetki Bulunamadı.','warning')";
                        return RedirectToAction("Cadre");
                    }
                    if (privilege.AssetId != null)
                        ViewBag.privilegeAsset = privilege.AssetId.ToString();
                }
                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Yetki Güncelle";
                        break;

                    default:
                        ViewBag.Title = "Yetki Ekle";
                        break;
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult PrivilegeFormSave(FormCollection form)
        {
            try
            {
                var privilegeId = form["hdPrivilegeId"];
                var name = form["privilegeName"];
                var asset = form["privilegeAsset"];

                var privilege = new Privilege
                {
                    Id = long.Parse(privilegeId),
                    Name = name,
                    AssetId = !String.IsNullOrEmpty(asset) ? (long?)long.Parse(asset) : null,
                };
                var spResponsePrivilege = new PrivilegeRepository().Insert(privilege);

                if (spResponsePrivilege.Code == "100")
                {
                    var resultText = form["hdPrivilegeId"] != "0" ? "güncellendi" : "kaydedildi";
                    TempData["Alert"] = $"swAlert('İşlemi Başarılı','{privilege.Name} başarıyla {resultText}.','success')";
                }
                else
                {
                    throw new Exception(spResponsePrivilege.Code + " : " + spResponsePrivilege.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Privilege");
        }

        #endregion

        #region Aux functions
        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
        #endregion

    }
}