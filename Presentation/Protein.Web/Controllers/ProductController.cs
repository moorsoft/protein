﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using Newtonsoft.Json;
using Protein.Business.Concrete.Media;
using Protein.Common.Constants;
using Protein.Common.Dto;
using Protein.Common.Entities;
using Protein.Common.Extensions;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Web.ActionFilter.LoginAttr;
using Protein.Web.Helpers;
using Protein.Web.Models.ExportModel;
using Protein.Web.Models.ListView;
using Protein.Web.Models.MediaModel;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Web.Controllers
{
    [LoginControl]
    public class ProductController : Controller
    {
        #region DropDown fill methods 

        private void FillCompanies()
        {
            var v_company = new GenericRepository<V_Company>().FindBy(orderby: "COMPANY_NAME");
            ViewBag.v_company = v_company;
        }

        #endregion

        #region Company

        // GET: Company List /Product/Company
        [LoginControl]
        public ActionResult Company()
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                //Fill data
                ViewBag.companyId = string.Empty;
                ViewBag.companyName = string.Empty;
                var result = new GenericRepository<V_Company>().FindBy(orderby: "COMPANY_ID");
                ViewBag.Result = result;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        // POST: Company Filter /Product/CompanyFilter
        [HttpPost]
        [LoginControl]
        public ActionResult CompanyFilter(FormCollection form)
        {
            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            //Fill data
            ViewBag.companyId = form["companyId"].ToString();
            ViewBag.companyName = form["companyName"].ToString();

            dynamic parameters = new System.Dynamic.ExpandoObject();
            parameters.companyId = form["companyId"];
            parameters.companyName = form["companyName"];

            string whereConditition = form["companyId"].IsInt64() && long.Parse(form["companyId"]) > 0 ? $" COMPANY_ID =:companyId AND" : "";
            whereConditition += !form["companyName"].IsNull() ? $" COMPANY_NAME LIKE '%:companyName%' AND" : "";

            var result = new GenericRepository<V_Company>().FindBy(whereConditition.IsNull() ? "" : whereConditition.Substring(0, whereConditition.Length - 3), orderby: "COMPANY_ID", parameters: parameters);
            ViewBag.Result = result;

            return View("Company");
        }

        // GET: Company Delete /Product/CompanyDelete
        [LoginControl]
        public ActionResult CompanyDelete(Int64 id)
        {
            var repo = new CompanyRepository();
            var result = repo.FindById(id);
            if (result != null)
            {
                result.Status = "1";
                repo.Update(result);
            }
            return RedirectToAction("Company");
        }

        // GET: Company Form  /Product/CompanyForm
        [LoginControl]
        public ActionResult CompanyForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var CityList = new CityRepository().FindBy();
                ViewBag.CityList = CityList;

                var ContactType = LookupHelper.GetLookupData(Constants.LookupTypes.Contact);
                ViewBag.ContactTypeList = ContactType;

                var CurrencyTypes = LookupHelper.GetLookupData(Constants.LookupTypes.Currency);
                ViewBag.CurrencyTypes = CurrencyTypes;

                var PhoneTypes = LookupHelper.GetLookupData(Constants.LookupTypes.CompanyPhone);
                ViewBag.PhoneTypes = PhoneTypes;

                var EmailTypes = LookupHelper.GetLookupData(Constants.LookupTypes.CompanyEmail);
                ViewBag.EmailTypes = EmailTypes;

                var BankList = new BankRepository().FindBy();
                ViewBag.BankList = BankList;

                var NoteTypes = LookupHelper.GetLookupData(Constants.LookupTypes.Note);
                ViewBag.NoteTypeList = NoteTypes;

                var CountryList = new CountryRepository().FindBy();
                ViewBag.CountryList = CountryList;

                ViewBag.isEdit = false;
                ViewBag.isClone = false;

                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Şirket Güncelle";
                        if (id != 0)
                        {

                            var Company = new GenericRepository<V_Company>().FindBy("COMPANY_ID=:companyId", orderby: "COMPANY_ID", parameters: new { companyId = id }).FirstOrDefault();
                            if (Company != null)
                            {
                                ViewBag.Company = Company;

                                var CountyList = new GenericRepository<County>().FindBy("TYPE=:type", parameters: new { type = ((int)CountyType.SBM).ToString() });
                                ViewBag.CountyList = CountyList;

                                //Yetkili Bilgileri
                                var companyContact = new GenericRepository<V_CompanyContact>().FindBy("COMPANY_ID=:id", orderby: "COMPANY_ID", parameters: new { id });

                                if (companyContact != null)
                                {
                                    List<dynamic> contactList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in companyContact)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.AUTHORIZED_COMPANY_CONTACT_ID = Convert.ToString(item.COMPANY_CONTACT_ID);
                                        listItem.AUTHORIZED_CONTACT_ID = Convert.ToString(item.CONTACT_ID);
                                        listItem.AUTHORIZED_PERSON_ID = Convert.ToString(item.PERSON_ID);
                                        listItem.AUTHORIZED_CONTACT_PHONE_ID = Convert.ToString(item.CONTACT_PHONE_ID);
                                        listItem.AUTHORIZED_PHONE_ID = Convert.ToString(item.PHONE_ID);
                                        listItem.AUTHORIZED_CONTACT_MOBILE_PHONE_ID = Convert.ToString(item.CONTACT_MOBILE_PHONE_ID);
                                        listItem.AUTHORIZED_MOBILE_PHONE_ID = Convert.ToString(item.MOBILE_PHONE_ID);
                                        listItem.AUTHORIZED_CONTACT_EMAIL_ID = Convert.ToString(item.CONTACT_EMAIL_ID);
                                        listItem.AUTHORIZED_EMAIL_ID = Convert.ToString(item.EMAIL_ID);


                                        listItem.AUTHORIZED_TITLE = item.TITLE;
                                        listItem.AUTHORIZED_NAME = item.FIRST_NAME;
                                        listItem.AUTHORIZED_SURNAME = item.LAST_NAME;
                                        listItem.AUTHORIZED_PHONE_NO = item.PHONE_NO;
                                        listItem.AUTHORIZED_EMAIL = item.EMAIL;
                                        listItem.AUTHORIZED_IDENTITY_NO = item.IDENTITY_NO;
                                        listItem.AUTHORIZED_MOBILE_PHONE_NO = item.MOBILE_PHONE_NO;
                                        listItem.AUTHORIZED_EXTENSION = item.EXTENSION;
                                        listItem.isOpen = "0";

                                        contactList.Add(listItem);

                                        i++;
                                    }

                                    ViewBag.Staffs = contactList.ToJSON();
                                }

                                //Banka bilgileri
                                var banks = new GenericRepository<V_CompanyBank>().FindBy("COMPANY_ID=:id ", orderby: "COMPANY_ID", parameters: new { id });
                                if (banks != null)
                                {

                                    List<dynamic> bankList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in banks)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.BANK_ACCOUNT_ID = Convert.ToString(item.BANK_ACCOUNT_ID);
                                        listItem.COMPANY_BANK_ID = Convert.ToString(item.COMPANY_BANK_ID);

                                        listItem.BANK_ACCOUNT_NAME = item.BANK_ACCOUNT_NAME;

                                        listItem.BANK_ID = Convert.ToString(item.BANK_ID);
                                        listItem.BANK_IDSelectedText = item.BANK_NAME;

                                        listItem.BANK_BRANCH_ID = Convert.ToString(item.BANK_BRANCH_ID);
                                        listItem.BANK_BRANCH_IDSelectedText = item.BANK_BRANCH_NAME;
                                        listItem.BANK_BRANCH_CODE = item.BANK_BRANCH_CODE;
                                        listItem.BANK_ACCOUNT_NO = item.BANK_ACCOUNT_NO;
                                        listItem.IBAN = item.IBAN;
                                        listItem.CURRENCY_TYPE = item.CURRENCY_TYPE;
                                        listItem.CURRENCY_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Currency, item.CURRENCY_TYPE);
                                        listItem.BANK_IS_PRIMARY = item.BANK_IS_PRIMARY;

                                        listItem.isOpen = "0";
                                        bankList.Add(listItem);

                                        i++;
                                    }

                                    ViewBag.Banks = bankList.ToJSON();
                                }

                                var phones = new GenericRepository<V_CompanyPhone>().FindBy("COMPANY_ID=:id", orderby: "COMPANY_ID", parameters: new { id });
                                if (phones != null)
                                {

                                    List<dynamic> phoneList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in phones)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.PHONE_ID = Convert.ToString(item.PHONE_ID);
                                        listItem.COMPANY_PHONE_ID = Convert.ToString(item.COMPANY_PHONE_ID);

                                        listItem.PHONE_TYPE = item.PHONE_TYPE;
                                        listItem.PHONE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.CompanyPhone, item.PHONE_TYPE);

                                        listItem.COUNTRY_ID = Convert.ToString(item.COUNTRY_ID);
                                        listItem.COUNTRY_IDSelectedText = item.NUM_CODE;

                                        listItem.PHONE_NO = item.PHONE_NO;
                                        listItem.PHONE_IS_PRIMARY = item.IS_PRIMARY;
                                        listItem.isOpen = "0";
                                        phoneList.Add(listItem);

                                        i++;
                                    }

                                    ViewBag.Phones = phoneList.ToJSON();
                                }

                                var mails = new GenericRepository<V_CompanyEmail>().FindBy("COMPANY_ID=:id", orderby: "COMPANY_ID", parameters: new { id });
                                if (mails != null)
                                {

                                    List<dynamic> mailList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in mails)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.EMAIL_ID = Convert.ToString(item.EMAIL_ID);
                                        listItem.COMPANY_EMAIL_ID = Convert.ToString(item.COMPANY_EMAIL_ID);

                                        listItem.EMAIL_TYPE = item.EMAIL_TYPE;
                                        listItem.EMAIL_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.CompanyEmail, item.EMAIL_TYPE);
                                        listItem.EMAIL = item.EMAIL;
                                        listItem.EMAIL_IS_PRIMARY = item.IS_PRIMARY;
                                        listItem.isOpen = "0";
                                        mailList.Add(listItem);

                                        i++;
                                    }

                                    ViewBag.Emails = mailList.ToJSON(true);
                                }

                                List<V_CompanyNote> notes = new GenericRepository<V_CompanyNote>().FindBy("COMPANY_ID=:id", orderby: "COMPANY_ID", parameters: new { id });
                                if (notes != null)
                                {
                                    List<dynamic> notesList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in notes)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                                        listItem.COMPANY_NOTE_ID = Convert.ToString(item.COMPANY_NOTE_ID);

                                        listItem.NOTE_TYPE = item.NOTE_TYPE;
                                        listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Note, item.NOTE_TYPE);
                                        listItem.NOTE_DESCRIPTION = item.NOTE_DESCRIPTION.RemoveRepeatedWhiteSpace();
                                        listItem.isOpen = "0";
                                        notesList.Add(listItem);

                                        i++;
                                    }

                                    ViewBag.Notes = notesList.ToJSON(true);
                                }

                                List<V_CompanyParameter> parameters = new GenericRepository<V_CompanyParameter>().FindBy("COMPANY_ID=:id ", orderby: "COMPANY_ID", parameters: new { id });
                                if (parameters != null)
                                {
                                    foreach (var item in parameters)
                                    {
                                        if (item.SystemType == Convert.ToString((int)SystemType.SBM))
                                        {
                                            if (item.Key == "USER_NAME")
                                            {
                                                ViewBag.SgmUserName = item.Value;
                                            }
                                            else if (item.Key == "CODE")
                                            {
                                                ViewBag.Code = item.Value;
                                            }
                                            else if (item.Key == "PASSWORD")
                                            {
                                                ViewBag.SgmPassword = item.Value;
                                            }
                                        }
                                        else if (item.SystemType == Convert.ToString((int)SystemType.PORTTSS))
                                        {
                                            if (item.Key == "USER_NAME")
                                            {
                                                ViewBag.PortTSSUserName = item.Value;
                                            }
                                            else if (item.Key == "PASSWORD")
                                            {
                                                ViewBag.PortTSSPassword = item.Value;
                                            }
                                        }
                                        else if (item.SystemType == Convert.ToString((int)SystemType.WS))
                                        {
                                            if (item.Key == "ANKUR_ID")
                                            {
                                                ViewBag.AnkurWebServiceId = item.Value;
                                            }

                                        }
                                    }
                                }

                                V_CompanyUser companyUser = new GenericRepository<V_CompanyUser>().FindBy($"COMPANY_ID={id}", orderby: "").FirstOrDefault();
                                if (companyUser != null)
                                {
                                    ViewBag.ProteinUserName = companyUser.USERNAME;
                                    ViewBag.ProteinPassword = companyUser.PASSWORD;
                                    ViewBag.CompanyUserId = companyUser.COMPANY_USER_ID.ToString();
                                }
                            }
                        }

                        ViewBag.isEdit = true;
                        break;

                    default:
                        ViewBag.Title = "Şirket Ekle";
                        ViewBag.FormDisabled = "";
                        break;
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        //GET: Company Clone  /Product/CompanyClone
        [LoginControl]
        public ActionResult CompanyClone(Int64 Id = 0)
        {
            try
            {
                if (Id != 0)
                {
                    var oldCompany = new CompanyRepository().FindById(Id);
                    if (oldCompany != null)
                    {
                        long newCompanyId = 0;
                        #region Clone Company
                        #region Clone Address
                        // Clone Address If Exists
                        long addressId = 0;
                        if (oldCompany.AddressId != null && oldCompany.AddressId > 0)
                        {
                            addressId = CloneHelper.AddressClone((long)oldCompany.AddressId);
                        }
                        #endregion

                        #region Clone Contact
                        // Clone Contact If Exists
                        long contactId = 0;
                        if (oldCompany.ContactId > 0)
                        {
                            contactId = CloneHelper.ContactClone(oldCompany.ContactId);
                        }
                        #endregion

                        #region Clone Company Details
                        Company company = new Company
                        {
                            Id = 0,
                            Name = oldCompany.Name + "-Kopya",
                            Status = oldCompany.Status
                        };
                        if (addressId > 0)
                        {
                            company.AddressId = addressId;
                        }
                        if (contactId > 0)
                        {
                            company.ContactId = contactId;
                        }
                        var spResponseCompany = new CompanyRepository().Insert(company);
                        if (spResponseCompany.Code != "100")
                        {
                            throw new Exception(spResponseCompany.Code + " : " + spResponseCompany.Message);
                        }
                        newCompanyId = spResponseCompany.PkId;
                        company.Id = newCompanyId;
                        #endregion
                        #endregion

                        #region Clone Company's Bank Accounts
                        var companyBankAccountList = new CompanyBankAccountRepository().FindBy("COMPANY_ID=:id", parameters: new { id = oldCompany.Id });

                        foreach (var companyBankAccount in companyBankAccountList)
                        {
                            #region Clone Bank Account
                            // Clone Bank Account If Exists
                            long bankAccountId = 0;
                            if (companyBankAccount.BankAccountId > 0)
                            {
                                bankAccountId = CloneHelper.BankAccountClone(companyBankAccount.BankAccountId);
                            }
                            if (bankAccountId > 0)
                            {
                                #region Clone Company Bank Account Relation
                                // Clone Company Bank Account If Exists
                                long companyBankAccountId = 0;
                                if (companyBankAccount.BankAccountId > 0)
                                {
                                    companyBankAccountId = CloneHelper.CompanyBankAccountClone(companyBankAccount.Id, newCompanyId, bankAccountId);
                                }
                                #endregion
                            }
                            #endregion
                        }
                        #endregion

                        #region Clone Company's Phones
                        var companyPhoneList = new CompanyPhoneRepository().FindBy("COMPANY_ID=:id", parameters: new { id = oldCompany.Id });

                        foreach (var companyPhone in companyPhoneList)
                        {
                            #region Clone Phone
                            // Clone Phone If Exists
                            long phoneId = 0;
                            if (companyPhone.PhoneId > 0)
                            {
                                phoneId = CloneHelper.PhoneClone(companyPhone.PhoneId);
                            }
                            if (phoneId > 0)
                            {
                                #region Clone Company Phone Relation
                                // Clone Company Phone If Exists
                                long companyPhoneId = 0;
                                if (companyPhone.PhoneId > 0)
                                {
                                    companyPhoneId = CloneHelper.CompanyPhoneClone(companyPhone.Id, newCompanyId, phoneId);
                                }
                                #endregion
                            }
                            #endregion
                        }
                        #endregion

                        #region Clone Company's Notes
                        var companyNoteList = new CompanyNoteRepository().FindBy("COMPANY_ID=:id", parameters: new { id = oldCompany.Id });

                        foreach (var companyNote in companyNoteList)
                        {
                            #region Clone Note
                            // Clone Note If Exists
                            long noteId = 0;
                            if (companyNote.NoteId > 0)
                            {
                                noteId = CloneHelper.NoteClone(companyNote.NoteId);
                            }
                            if (noteId > 0)
                            {
                                #region Clone Company Note Relation
                                // Clone Company Note If Exists
                                long companyNoteId = 0;
                                if (companyNote.NoteId > 0)
                                {
                                    companyNoteId = CloneHelper.CompanyNoteClone(companyNote.Id, newCompanyId, noteId);
                                }
                                #endregion
                            }
                            #endregion
                        }
                        #endregion

                        #region  User Clone

                        var companyUserList = new CompanyUserRepository().FindBy("COMPANY_ID=:id", parameters: new { id = oldCompany.Id });
                        foreach (var companyUser in companyUserList)
                        {
                            #region User Clone
                            long userId = 0;
                            if (companyUser.UserId > 0)
                            {
                                userId = CloneHelper.UserClone(companyUser.UserId);
                            }
                            if (userId > 0)
                            {
                                #region Clone Company User Relation
                                long companyUserId = 0;
                                if (companyUser.UserId > 0)
                                {
                                    companyUserId = CloneHelper.CompanUserClone(companyUser.Id, newCompanyId, userId);
                                }
                                #endregion
                            }

                            #endregion
                        }
                        #endregion

                        #region  Contact Clone
                        var companyContactList = new CompanyContactRepository().FindBy("COMPANY_ID=:id", parameters: new { id = oldCompany.Id });
                        foreach (var companyContact in companyContactList)
                        {
                            #region Contact Clone
                            long contactToCloneId = 0;
                            if (companyContact.ContactId > 0)
                            {
                                contactToCloneId = CloneHelper.ContactClone(companyContact.ContactId);
                            }
                            if (contactToCloneId > 0)
                            {
                                #region Clone Company Contact Reletion
                                long companyContactId = 0;
                                if (companyContact.ContactId > 0)
                                {
                                    companyContactId = CloneHelper.CompanyContactClone(companyContact.Id, newCompanyId, contactToCloneId);
                                }
                                #endregion
                            }

                            #endregion
                        }
                        #endregion

                        #region  Parameter Clone
                        var companyParameterList = new CompanyParameterRepository().FindBy("COMPANY_ID=:id", parameters: new { id = oldCompany.Id });
                        foreach (var companyParameter in companyParameterList)
                        {
                            #region Parameter Clone
                            long parameterId = 0;
                            if (companyParameter.ParameterId > 0)
                            {
                                parameterId = CloneHelper.ParameterClone(companyParameter.ParameterId);
                            }
                            if (parameterId > 0)
                            {
                                #region Clone Company Parameter Reletion
                                long companyParameterId = 0;
                                if (companyParameter.ParameterId > 0)
                                {
                                    companyParameterId = CloneHelper.CompanyParameterClone(companyParameter.Id, newCompanyId, parameterId);
                                }

                                #endregion
                            }
                            #endregion
                        }
                        #endregion

                        #region   Email Clone
                        var companyEmailList = new CompanyEmailRepository().FindBy("COMPANY_ID=:id", parameters: new { id = oldCompany.Id });
                        foreach (var companyEmail in companyEmailList)
                        {
                            #region Email Clone
                            long emailId = 0;
                            if (companyEmail.EmailId > 0)
                            {
                                emailId = CloneHelper.EmailClone(companyEmail.EmailId);
                            }
                            if (emailId > 0)
                            {
                                #region Clone Company Email Reletion
                                long companyEmailId = 0;
                                if (companyEmail.EmailId > 0)
                                {
                                    companyEmailId = CloneHelper.CompanyEmailClone(companyEmail.Id, newCompanyId, emailId);
                                }

                                #endregion
                            }
                            #endregion
                        }
                        #endregion
                        TempData["Alert"] = $"swAlert('İşlem Başarılı','Şirket başarıyla kopyalandı.','success')";
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Company");
        }

        // POST: Company Form Save /Product/CompanyFormSave
        [HttpPost]
        public JsonResult CompanySaveStep1(FormCollection form)
        {
            var result = new AjaxResultDto<CompanySaveStep1Result>();
            CompanySaveStep1Result step1Result = new CompanySaveStep1Result();

            try
            {
                #region Contact Creation
                var contactId = string.IsNullOrEmpty(form["hdContactId"]) ? 0 : long.Parse(form["hdContactId"]);
                var companyTitle = form["companyContactTitle"];
                var companyTaxOffice = form["companyTaxOffice"];
                var companyTaxNumber = form["companyTaxNumber"];
                var companyName = form["companyName"];
                long corporateId = long.Parse(form["hdCorporateId"]);

                var contact = new Contact
                {
                    Id = contactId,
                    Type = "1",
                    Title = companyTitle,
                    TaxNumber = companyTaxNumber.IsInt64() ? (long?)Convert.ToInt64(companyTaxNumber) : null,
                    TaxOffice = companyTaxOffice
                };

                var spResponseContact = new ContactRepository().Insert(contact);
                #endregion

                // Continue if contact is created successfully
                if (spResponseContact.Code == "100")
                {
                    contactId = spResponseContact.PkId;
                    step1Result.companyContactId = contactId;

                    var corporate = new Corporate
                    {
                        Id = corporateId,
                        ContactId = contactId,
                        Type = "0",
                        Name = companyName,
                    };
                    var spResponseCorporate = new CorporateRepository().Insert(corporate);
                    if (spResponseCorporate.Code == "100")
                    {
                        step1Result.companyCorporateId = spResponseCorporate.PkId;
                        #region Address Creation

                        var companyAddressId = string.IsNullOrEmpty(form["hdAddressId"]) ? 0 : long.Parse(form["hdAddressId"]);
                        var companyAdress = form["companyAdress"];
                        var companyZipCode = form["companyZipCode"];
                        var companyCityId = form["companyCity"];
                        var companyCountyId = form["companyCounty"];

                        if (!string.IsNullOrEmpty(companyAdress) || companyAddressId > 0)
                        {
                            var adress = new Address
                            {
                                Id = companyAddressId,
                                Type = "0",
                                CityId = string.IsNullOrEmpty(companyCityId) ? null : (long?)long.Parse(companyCityId),
                                CountyId = string.IsNullOrEmpty(companyCountyId) ? null : (long?)long.Parse(companyCountyId),
                                Details = companyAdress,
                                ZipCode = companyZipCode,
                                Status = !string.IsNullOrEmpty(companyAdress) ? "0" : "1"
                            };
                            var spResponseAdress = new AddressRepository().Insert(adress);

                            if (spResponseAdress.Code == "100")
                            {
                                step1Result.companyAdressId = spResponseAdress.PkId;
                                companyAddressId = spResponseAdress.PkId;
                            }
                            else
                            {
                                result.ResultCode = spResponseAdress.Code;
                                result.ResultMessage = spResponseAdress.Message;
                                result.Data = null;
                                throw new Exception("Şirket adresi kaydedililirken hata oluştu!");
                            }
                        }

                        #endregion

                        #region BillAddress Creation

                        //var companyBillAddressId = string.Empty;

                        //var companyBillAdress = form["companyBillAdress"];
                        //var companyBillZipCode = form["companyBillZipCode"];
                        //var billCityId = form["billCity"];
                        //var billCountyId = form["billCounty"];

                        //if (!string.IsNullOrEmpty(companyBillAdress))
                        //{
                        //    var billAdress = new Address
                        //    {
                        //        Id = long.Parse(form["hdBillAddressId"]),
                        //        Type = "0",
                        //        CityId = string.IsNullOrEmpty(billCityId) ? null : (long?)long.Parse(billCityId),
                        //        CountyId = string.IsNullOrEmpty(billCountyId) ? null : (long?)long.Parse(billCountyId),
                        //        Details = companyBillAdress,
                        //        ZipCode = companyBillZipCode
                        //    };
                        //    var spResponseBillAdress = new AddressRepository().Insert(billAdress);

                        //    if (spResponseBillAdress.Code == "100")
                        //    {
                        //        step1Result.companyBillAdressId = spResponseBillAdress.PkId;
                        //        companyBillAddressId = spResponseBillAdress.PkId.ToString();
                        //    }
                        //    else
                        //    {
                        //        result.ResultCode = spResponseBillAdress.Code;
                        //        result.ResultMessage = spResponseBillAdress.Message;
                        //        result.Data = null;
                        //        throw new Exception("Şirket fatura adresi kaydedilirken hata oluştu!");
                        //    }
                        //}

                        #endregion

                        #region Company Creation

                        var companyId = string.IsNullOrEmpty(form["hdCompanyId"]) ? 0 : long.Parse(form["hdCompanyId"]);

                        var company = new Company
                        {
                            Id = companyId,
                            Name = companyName,
                            ContactId = contactId
                        };
                        if (companyAddressId > 0)
                        {
                            company.AddressId = companyAddressId;
                        }
                        var spResponseCompany = new CompanyRepository().Insert(company);
                        if (spResponseCompany.Code == "100")
                        {
                            step1Result.companyId = spResponseCompany.PkId;
                            result.ResultCode = spResponseCompany.Code;
                            result.ResultMessage = spResponseCompany.Message;
                            result.Data = step1Result;
                        }
                        else
                        {
                            result.ResultCode = spResponseCompany.Code;
                            result.ResultMessage = spResponseCompany.Message;
                            result.Data = null;
                        }
                    }
                    #endregion
                }
                else
                {
                    result.ResultCode = spResponseContact.Code;
                    result.ResultMessage = spResponseContact.Message;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                string log = ex.Message;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }

            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CompanySaveStep2(FormCollection form)
        {
            var result = new AjaxResultDto<CompanySaveStep2Result>();

            var authorizeds = form["hdContactList"];

            List<dynamic> authorizedList = new List<dynamic>();
            authorizedList = JsonConvert.DeserializeObject<List<dynamic>>(authorizeds);
            try
            {

                var companyId = long.Parse(form["hdCompanyId"]);
                foreach (var authorized in authorizedList)
                {
                    if (authorized.isOpen == "1")
                    {
                        #region Contact Save
                        var contactId = authorized.AUTHORIZED_CONTACT_ID != null ? authorized.AUTHORIZED_CONTACT_ID : 0;
                        var contact = new Contact
                        {
                            Id = contactId,
                            Type = "0",
                            Title = authorized.AUTHORIZED_TITLE,
                            IdentityNo = authorized.AUTHORIZED_IDENTITY_NO,
                            Status = authorized.STATUS
                        };
                        var spResponseContact = new ContactRepository().Insert(contact);
                        if (spResponseContact.Code != "100")
                        {
                            throw new Exception(spResponseContact.Code + " : " + spResponseContact.Message);
                        }
                        contactId = spResponseContact.PkId;
                        #endregion

                        #region Person Save
                        var personId = authorized.AUTHORIZED_PERSON_ID != null ? authorized.AUTHORIZED_PERSON_ID : 0;
                        var person = new Person
                        {
                            Id = personId,
                            ContactId = contactId,
                            ProfessionType = "99",
                            FirstName = authorized.AUTHORIZED_NAME,
                            LastName = authorized.AUTHORIZED_SURNAME,
                            Status = authorized.STATUS
                        };
                        var spResponsePerson = new PersonRepository().Insert(person);
                        if (spResponsePerson.Code != "100")
                        {
                            throw new Exception(spResponsePerson.Message);
                        }
                        #endregion

                        #region Phone Save
                        if (!string.IsNullOrEmpty(Convert.ToString(authorized.AUTHORIZED_PHONE_NO)))
                        {
                            var phone = new Phone
                            {
                                Id = authorized.AUTHORIZED_PHONE_ID,
                                CountryId = 222,
                                No = authorized.AUTHORIZED_PHONE_NO,
                                Extension = authorized.AUTHORIZED_EXTENSION,
                                Type = "0",
                                Status = authorized.STATUS

                            };
                            var spResponsePhone = new PhoneRepository().Insert(phone);
                            if (spResponsePhone.Code == "100")
                            {
                                #region ContactPhone Save
                                var phoneId = spResponsePhone.PkId;
                                var contactPhone = new ContactPhone
                                {
                                    Id = authorized.AUTHORIZED_CONTACT_PHONE_ID,
                                    ContactId = contactId,
                                    PhoneId = phoneId,
                                    Status = authorized.STATUS

                                };
                                var spResponseContactPhone = new ContactPhoneRepository().Insert(contactPhone);
                                if (spResponseContactPhone.Code != "100")
                                {
                                    throw new Exception(spResponseContactPhone.Message);
                                }
                                #endregion
                            }
                            else
                            {
                                throw new Exception(spResponsePhone.Message);
                            }
                        }
                        #endregion

                        #region MobilePhone Save
                        if (!string.IsNullOrEmpty(Convert.ToString(authorized.AUTHORIZED_MOBILE_PHONE_NO)))
                        {
                            var mobilePhone = new Phone
                            {
                                Id = authorized.AUTHORIZED_MOBILE_PHONE_ID,
                                CountryId = 222,
                                No = authorized.AUTHORIZED_MOBILE_PHONE_NO,
                                Type = "1",
                                Status = authorized.STATUS

                            };
                            var spResponseMobilePhone = new PhoneRepository().Insert(mobilePhone);
                            if (spResponseMobilePhone.Code == "100")
                            {
                                #region ContactMobilePhone Save
                                var mobilePhoneId = spResponseMobilePhone.PkId;
                                var contactMobilePhone = new ContactPhone
                                {
                                    Id = authorized.AUTHORIZED_CONTACT_MOBILE_PHONE_ID,
                                    ContactId = contactId,
                                    PhoneId = mobilePhoneId,
                                    Status = authorized.STATUS

                                };
                                var spResponseContactMobilePhone = new ContactPhoneRepository().Insert(contactMobilePhone);
                                if (spResponseContactMobilePhone.Code != "100")
                                {
                                    throw new Exception(spResponseContactMobilePhone.Message);
                                }
                                #endregion
                            }
                            else
                            {
                                throw new Exception(spResponseMobilePhone.Message);
                            }
                        }
                        #endregion

                        #region Email
                        if (!string.IsNullOrEmpty(Convert.ToString(authorized.AUTHORIZED_EMAIL)))
                        {
                            var email = new Email
                            {
                                Id = authorized.AUTHORIZED_EMAIL_ID,
                                Type = "0",
                                Details = authorized.AUTHORIZED_EMAIL,
                                Status = authorized.STATUS


                            };
                            var spResponseEmail = new EmailRepository().Insert(email);
                            if (spResponseEmail.Code == "100")
                            {
                                var emailId = spResponseEmail.PkId;
                                var contactEmail = new ContactEmail
                                {
                                    Id = authorized.AUTHORIZED_CONTACT_EMAIL_ID,
                                    ContactId = contactId,
                                    EmailId = emailId,
                                    Status = authorized.STATUS

                                };
                                var spResponseContactEmail = new ContactEmailRepository().Insert(contactEmail);
                                if (spResponseContactEmail.Code != "100")
                                {
                                    throw new Exception(spResponseContactEmail.Message);
                                }
                            }
                            else
                            {
                                throw new Exception(spResponseEmail.Code + " : " + spResponseEmail.Message);
                            }
                        }
                        #endregion

                        #region CompanyContact Save
                        var companyContact = new CompanyContact
                        {
                            Id = authorized.AUTHORIZED_COMPANY_CONTACT_ID,
                            CompanyId = companyId,
                            ContactId = contactId,
                            Status = authorized.STATUS

                        };

                        var spResponseCompanyContact = new CompanyContactRepository().Insert(companyContact);
                        if (spResponseCompanyContact.Code != "100")
                        {
                            throw new Exception(spResponseCompanyContact.Message);
                        }
                        else
                        {
                            result.ResultCode = spResponseCompanyContact.Code;
                            result.ResultMessage = spResponseCompanyContact.Message;
                            TempData["Alert"] = $"swAlert('İşlem Başarılı','Yetkili başarıyla kaydedildi.','success')";
                        }
                        #endregion
                    }
                }
                //Yetkili Bilgileri

                var companyContactList = new GenericRepository<V_CompanyContact>().FindBy("COMPANY_ID=:id", orderby: "COMPANY_ID", parameters: new { id = companyId });

                if (companyContactList != null)
                {
                    List<dynamic> contactList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in companyContactList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;


                        listItem.AUTHORIZED_COMPANY_CONTACT_ID = Convert.ToString(item.COMPANY_CONTACT_ID);
                        listItem.AUTHORIZED_CONTACT_ID = Convert.ToString(item.CONTACT_ID);
                        listItem.AUTHORIZED_PERSON_ID = Convert.ToString(item.PERSON_ID);
                        listItem.AUTHORIZED_CONTACT_PHONE_ID = Convert.ToString(item.CONTACT_PHONE_ID);
                        listItem.AUTHORIZED_PHONE_ID = Convert.ToString(item.PHONE_ID);
                        listItem.AUTHORIZED_CONTACT_MOBILE_PHONE_ID = Convert.ToString(item.CONTACT_MOBILE_PHONE_ID);
                        listItem.AUTHORIZED_MOBILE_PHONE_ID = Convert.ToString(item.MOBILE_PHONE_ID);
                        listItem.AUTHORIZED_CONTACT_EMAIL_ID = Convert.ToString(item.CONTACT_EMAIL_ID);
                        listItem.AUTHORIZED_EMAIL_ID = Convert.ToString(item.EMAIL_ID);
                        listItem.AUTHORIZED_TITLE = item.TITLE;
                        listItem.AUTHORIZED_NAME = item.FIRST_NAME;
                        listItem.AUTHORIZED_SURNAME = item.LAST_NAME;
                        listItem.AUTHORIZED_PHONE_NO = item.PHONE_NO;
                        listItem.AUTHORIZED_EXTENSION = item.EXTENSION;
                        listItem.AUTHORIZED_MOBILE_PHONE_NO = item.MOBILE_PHONE_NO;
                        listItem.AUTHORIZED_EMAIL = item.EMAIL;
                        listItem.AUTHORIZED_IDENTITY_NO = item.IDENTITY_NO;
                        listItem.isOpen = "0";

                        contactList.Add(listItem);

                        i++;
                    }
                    result.Data = new CompanySaveStep2Result
                    {
                        jsonData = contactList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                string log = ex.Message;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CompanySaveStep3(FormCollection form)
        {
            var result = new AjaxResultDto<CompanySaveStep3Result>();
            var banks = form["hdBanks"];

            try
            {
                List<dynamic> bankList = new List<dynamic>();
                bankList = JsonConvert.DeserializeObject<List<dynamic>>(banks);
                var companyId = form["hdCompanyId"];

                foreach (var bank in bankList)
                {
                    if (bank.isOpen == "1")
                    {
                        SpResponse spResponseBankAccount = new SpResponse();
                        var bankId = bank.BANK_ID;
                        var bankName = bank.BANK_IDSelectedText;
                        var currenyType = bank.CURRENCY_TYPE;
                        var name = Convert.ToString(bank.BANK_ACCOUNT_NAME);
                        var accountNo = Convert.ToString(bank.BANK_ACCOUNT_NO);
                        var bankBranchId = string.IsNullOrEmpty(Convert.ToString(bank.BANK_BRANCH_ID)) ? 0 : Convert.ToInt64(Convert.ToString(bank.BANK_BRANCH_ID));
                        var ibanNumber = Convert.ToString(bank.IBAN);
                        var isPrimary = string.IsNullOrEmpty(Convert.ToString(bank.BANK_IS_PRIMARY)) ? "0" : Convert.ToString(bank.BANK_IS_PRIMARY);

                        // En son aktif olarak eklenen banka hesabı sadece aktif olmalı önceki aktif olarak eklenen(ler) pasif yapılmalı!
                        if (isPrimary.Equals("1"))
                        {
                            var activeBankAccounts = new GenericRepository<V_CompanyBank>().FindBy("BANK_IS_PRIMARY = '1' AND COMPANY_ID =:companyId ", orderby: "COMPANY_ID", parameters: new { companyId });
                            if (activeBankAccounts != null)
                            {
                                foreach (var activeBankAccount in activeBankAccounts)
                                {
                                    var activeBankAccountToChange = new BankAccount
                                    {
                                        Id = (long)activeBankAccount.BANK_ACCOUNT_ID,
                                        CurrencyType = activeBankAccount.CURRENCY_TYPE,
                                        BankId = (long)activeBankAccount.BANK_ID,
                                        IsPrimary = "0"
                                    };
                                    SpResponse spResponseActiveBankAccount = new SpResponse();
                                    spResponseActiveBankAccount = new BankAccountRepository().Update(activeBankAccountToChange);
                                    if (spResponseActiveBankAccount.Code != "100")
                                    {
                                        throw new Exception("Aktif banka hesabı pasifleştirilirken hata oluştu : " + activeBankAccount.BANK_ACCOUNT_NAME + " (" + activeBankAccount.BANK_ACCOUNT_ID + ")");
                                    }
                                }
                            }
                        }

                        var bankAccount = new BankAccount
                        {
                            Id = bank.BANK_ACCOUNT_ID,
                            Name = name,
                            CurrencyType = currenyType,
                            No = accountNo,
                            Iban = ibanNumber,
                            BankId = bankId,
                            IsPrimary = isPrimary,
                            Status = bank.STATUS
                        };
                        if (bankBranchId > 0)
                        {
                            bankAccount.BankBranchId = bankBranchId;
                        }
                        spResponseBankAccount = new BankAccountRepository().Insert(bankAccount);

                        if (spResponseBankAccount.Code == "100")
                        {
                            var bankAccountId = spResponseBankAccount.PkId;
                            var providerBankAccount = new CompanyBankAccount
                            {
                                Id = bank.COMPANY_BANK_ID,
                                CompanyId = long.Parse(companyId),
                                BankAccountId = bankAccountId,
                                Status = bank.STATUS
                            };
                            var spResponseCompanyBankAccount = new CompanyBankAccountRepository().Insert(providerBankAccount);
                            if (spResponseCompanyBankAccount.Code == "100")
                            {
                                result.ResultCode = spResponseCompanyBankAccount.Code;
                                result.ResultMessage = spResponseCompanyBankAccount.Message;
                                TempData["Alert"] = $"swAlert('İşlem Başarılı','{name} başarıyla kaydedildi.','success')";
                            }
                            else
                            {
                                throw new Exception(spResponseCompanyBankAccount.Code + " : " + spResponseCompanyBankAccount.Message);
                            }
                        }
                        else
                        {
                            throw new Exception(spResponseBankAccount.Code + " : " + spResponseBankAccount.Message);
                        }
                    }
                }


                var bankss = new GenericRepository<V_CompanyBank>().FindBy("COMPANY_ID=:id", orderby: "COMPANY_ID", fetchDeletedRows: true, parameters: new { id = companyId });
                if (bankss != null)
                {
                    List<dynamic> banksList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in bankss)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.BANK_ACCOUNT_ID = Convert.ToString(item.BANK_ACCOUNT_ID);
                        listItem.COMPANY_BANK_ID = Convert.ToString(item.COMPANY_BANK_ID);

                        listItem.BANK_ACCOUNT_NAME = item.BANK_ACCOUNT_NAME;
                        listItem.BANK_ID = Convert.ToString(item.BANK_ID);
                        listItem.BANK_IDSelectedText = item.BANK_NAME;


                        listItem.BANK_BRANCH_ID = Convert.ToString(item.BANK_BRANCH_ID);
                        listItem.BANK_BRANCH_IDSelectedText = item.BANK_BRANCH_NAME;
                        listItem.BANK_BRANCH_CODE = item.BANK_BRANCH_CODE;
                        listItem.BANK_ACCOUNT_NO = item.BANK_ACCOUNT_NO;
                        listItem.IBAN = item.IBAN;
                        listItem.CURRENCY_TYPE = item.CURRENCY_TYPE;
                        listItem.CURRENCY_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Currency, item.CURRENCY_TYPE);
                        listItem.BANK_IS_PRIMARY = item.BANK_IS_PRIMARY;
                        listItem.isOpen = "0";

                        banksList.Add(listItem);

                        i++;
                    }

                    result.Data = new CompanySaveStep3Result
                    {
                        jsonData = banksList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CompanySaveStep4(FormCollection form)
        {
            var result = new AjaxResultDto<CompanySaveStep4Result>();
            CompanySaveStep4Result step4Result = new CompanySaveStep4Result();
            try
            {
                var phones = form["hdPhones"];

                List<dynamic> phoneList = new List<dynamic>();
                phoneList = JsonConvert.DeserializeObject<List<dynamic>>(phones);
                var companyId = form["hdCompanyId"];
                foreach (var phone in phoneList)
                {
                    if (phone.isOpen == "1")
                    {
                        var phoneType = phone.PHONE_TYPE;
                        //var countryCode = phone.COUNTRY_ID.ToString();
                        var no = phone.PHONE_NO;
                        var isPrimary = string.IsNullOrEmpty(Convert.ToString(phone.PHONE_IS_PRIMARY)) ? "0" : Convert.ToString(phone.PHONE_IS_PRIMARY);
                        var Tphone = new Phone
                        {
                            Id = phone.PHONE_ID,
                            CountryId = 222,
                            No = no,
                            Type = phoneType,
                            Status = phone.STATUS,
                            IsPrimary = isPrimary
                        };
                        var spResponsePhone = new PhoneRepository().Insert(Tphone);
                        if (spResponsePhone.Code == "100")
                        {
                            var phoneId = spResponsePhone.PkId;
                            var TcompanyPhone = new CompanyPhone
                            {
                                Id = phone.COMPANY_PHONE_ID,
                                CompanyId = long.Parse(companyId),
                                PhoneId = phoneId,
                                Status = phone.STATUS

                            };
                            var spResponseCompanyPhone = new CompanyPhoneRepository().Insert(TcompanyPhone);
                            if (spResponseCompanyPhone.Code == "100")
                            {
                                result.ResultCode = spResponseCompanyPhone.Code;
                                result.ResultMessage = spResponseCompanyPhone.Message;
                                result.Data = step4Result;
                            }
                            else
                            {
                                result.ResultCode = spResponseCompanyPhone.Code;
                                result.ResultMessage = spResponseCompanyPhone.Message;
                                result.Data = null;
                                throw new Exception($"{no} kaydedilirken hata oluştu.");
                            }
                        }
                        else
                        {
                            result.ResultCode = spResponsePhone.Code;
                            result.ResultMessage = spResponsePhone.Message;
                            result.Data = null;
                            throw new Exception($"{no} kaydedilirken hata oluştu.");
                        }
                    }
                    //*******
                }
                var phoness = new GenericRepository<V_CompanyPhone>().FindBy("COMPANY_ID=:id ", orderby: "COMPANY_ID", parameters: new { id = companyId });
                if (phoness != null)
                {

                    List<dynamic> phonesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in phoness)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.PHONE_ID = Convert.ToString(item.PHONE_ID);
                        listItem.COMPANY_PHONE_ID = Convert.ToString(item.COMPANY_PHONE_ID);

                        listItem.PHONE_TYPE = item.PHONE_TYPE;
                        listItem.PHONE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.CompanyPhone, item.PHONE_TYPE);

                        listItem.COUNTRY_ID = Convert.ToString(item.COUNTRY_ID);
                        listItem.COUNTRY_IDSelectedText = item.NUM_CODE;

                        listItem.PHONE_NO = item.PHONE_NO;
                        listItem.PHONE_IS_PRIMARY = item.IS_PRIMARY;
                        listItem.isOpen = "0";

                        phonesList.Add(listItem);

                        i++;
                    }

                    result.Data = new CompanySaveStep4Result
                    {
                        jsonData = phonesList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CompanySaveStep5(FormCollection form)
        {
            var result = new AjaxResultDto<CompanySaveStep5Result>();
            CompanySaveStep5Result step5Result = new CompanySaveStep5Result();
            var emails = form["hdEmails"];
            try
            {
                List<dynamic> emailList = new List<dynamic>();
                emailList = JsonConvert.DeserializeObject<List<dynamic>>(emails);
                var companyId = form["hdCompanyId"];
                if (!String.IsNullOrEmpty(companyId))
                {
                    foreach (var email in emailList)
                    {
                        if (email.isOpen == "1")
                        {
                            var Temail = new Email
                            {
                                Id = email.EMAIL_ID,
                                Type = email.EMAIL_TYPE,
                                Details = email.EMAIL,
                                IsPrimary = email.EMAIL_IS_PRIMARY ?? "0",
                                Status = email.STATUS
                            };
                            var spResponseEmail = new EmailRepository().Insert(Temail);
                            if (spResponseEmail.Code == "100")
                            {
                                var emailId = spResponseEmail.PkId;
                                var companyEmail = new CompanyEmail
                                {
                                    Id = email.COMPANY_EMAIL_ID,
                                    CompanyId = long.Parse(companyId),
                                    EmailId = emailId,
                                    Status = email.STATUS

                                };
                                var spResponseCompanyEmail = new CompanyEmailRepository().Insert(companyEmail);
                                if (spResponseCompanyEmail.Code == "100")
                                {
                                    //step5Result.emailIdList.Add(spResponseCompanyEmail.PkId);
                                    result.ResultCode = spResponseCompanyEmail.Code;
                                    result.ResultMessage = spResponseCompanyEmail.Message;
                                }
                                else
                                {
                                    throw new Exception($"{email.MAIL} emailin Kurum ile bağlantısı oluşturulamadı.");
                                }
                            }
                            else
                            {
                                throw new Exception($"{email.MAIL} email kaydedilirken hata oluştu.");
                            }

                        }
                    }
                    var mails = new GenericRepository<V_CompanyEmail>().FindBy("COMPANY_ID=:id", orderby: "COMPANY_ID", parameters: new { id = companyId });
                    if (mails != null)
                    {

                        List<dynamic> mailList = new List<dynamic>();

                        int i = 1;

                        foreach (var item in mails)
                        {
                            dynamic listItem = new System.Dynamic.ExpandoObject();

                            listItem.id = i;
                            listItem.ClientId = i;
                            listItem.STATUS = item.STATUS;

                            listItem.EMAIL_ID = Convert.ToString(item.EMAIL_ID);
                            listItem.COMPANY_EMAIL_ID = Convert.ToString(item.COMPANY_EMAIL_ID);

                            listItem.EMAIL_TYPE = item.EMAIL_TYPE;
                            listItem.EMAIL_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.CompanyEmail, item.EMAIL_TYPE);
                            listItem.EMAIL = item.EMAIL;
                            listItem.EMAIL_IS_PRIMARY = item.IS_PRIMARY;

                            mailList.Add(listItem);

                            i++;
                        }

                        result.Data = new CompanySaveStep5Result
                        {
                            jsonData = mailList.ToJSON()
                        };
                    }
                }
                else
                {
                    throw new Exception("Öncelikle Şirket Kaydı Gerçekleştirilmesi Gerekmektedir.");
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CompanySaveStep6(FormCollection form)
        {
            var result = new AjaxResultDto<CompanySaveStep6Result>();
            CompanySaveStep6Result step6Result = new CompanySaveStep6Result();
            var notes = form["hdNotes"];

            List<dynamic> noteList = new List<dynamic>();
            noteList = JsonConvert.DeserializeObject<List<dynamic>>(notes);

            try
            {
                var companyId = form["hdCompanyId"];
                foreach (var note in noteList)
                {

                    if (note.isOpen == "1")
                    {
                        string description = Convert.ToString(note.NOTE_DESCRIPTION);
                        var Tnote = new Note
                        {
                            Id = note.NOTE_ID,
                            Type = note.NOTE_TYPE,
                            Description = description.RemoveRepeatedWhiteSpace(),
                            Status = note.STATUS
                        };
                        var spResponseNote = new NoteRepository().Insert(Tnote);
                        if (spResponseNote.Code == "100")
                        {
                            var noteId = spResponseNote.PkId;
                            var companyNote = new CompanyNote
                            {
                                Id = note.COMPANY_NOTE_ID,
                                CompanyId = long.Parse(companyId),
                                NoteId = noteId,
                                Status = note.STATUS
                            };
                            var spResponseCompanyNote = new CompanyNoteRepository().Insert(companyNote);
                            if (spResponseCompanyNote.Code == "100")
                            {
                                //step6Result.noteIdList.Add(spResponseCompanyNote.PkId);
                                result.ResultCode = spResponseCompanyNote.Code;
                                result.ResultMessage = spResponseCompanyNote.Message;
                            }
                            else
                            {
                                throw new Exception($"{note.NOTE_DESCRIPTION} notun Kurum ile bağlantısı oluşturulamadı.");
                            }
                        }
                        else
                        {
                            throw new Exception($"{note.NOTE_DESCRIPTION} not kaydedilirken hata oluştu.");
                        }
                    }
                }
                List<V_CompanyNote> notess = new GenericRepository<V_CompanyNote>().FindBy("COMPANY_ID=:id", orderby: "COMPANY_ID", parameters: new { id = companyId });
                if (notess != null)
                {
                    List<dynamic> notesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in notess)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                        listItem.COMPANY_NOTE_ID = Convert.ToString(item.COMPANY_NOTE_ID);

                        listItem.NOTE_TYPE = item.NOTE_TYPE;
                        listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Note, item.NOTE_TYPE);
                        listItem.NOTE_DESCRIPTION = item.NOTE_DESCRIPTION.RemoveRepeatedWhiteSpace();

                        notesList.Add(listItem);

                        i++;
                    }

                    result.Data = new CompanySaveStep6Result
                    {
                        jsonData = notesList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CompanySaveStep7(FormCollection form)
        {
            var result = new AjaxResultDto<CompanySaveStep7Result>();
            CompanySaveStep7Result step1Result = new CompanySaveStep7Result();

            var companyId = form["hdCompanyId"];
            try
            {
                var proteinUserName = form["proteinUserName"];
                var proteinPassword = form["proteinPassword"];
                var companyUserId = form["hdCompanyUserId"];
                var ankurWebServiceId = form["ankurWebServiceId"];
                long userId = 0;

                User user = new User();
                if (companyUserId != null && long.Parse(companyUserId) > 0)
                {
                    user.Username = proteinUserName;

                    V_CompanyUser companyUser = new GenericRepository<V_CompanyUser>().FindBy($"COMPANY_USER_ID={long.Parse(companyUserId)} AND COMPANY_ID={long.Parse(companyId)}", orderby: "").FirstOrDefault();
                    if (companyUser != null)
                    {
                        if (proteinPassword != companyUser.PASSWORD)
                        {
                            user.Password = proteinPassword.ToMD5().ToUpper();
                        }
                        userId = companyUser.USER_ID;

                        user.Id = companyUser.USER_ID;
                    }
                    else
                    {
                        user.Password = proteinPassword.ToMD5().ToUpper();
                    }
                }
                else
                {
                    user.Username = proteinUserName;
                    user.Password = proteinPassword.ToMD5().ToUpper();
                }

                var spResponseUser = new GenericRepository<User>().Insert(user);
                if (spResponseUser.Code == "100")
                {
                    if (userId == 0)
                    {
                        CompanyUser companyUser = new CompanyUser
                        {
                            UserId = spResponseUser.PkId,
                            CompanyId = long.Parse(companyId)
                        };
                        var spResponseCompanyUser = new GenericRepository<CompanyUser>().Insert(companyUser);
                        if (spResponseCompanyUser.Code != "100")
                        {
                            throw new Exception("Şirket Protein Giriş Bilgileri Kaydedilemedi!");
                        }
                    }
                    if (ankurWebServiceId != null)
                    {
                        Parameter parameterWs = new Parameter
                        {
                            Environment = "D",
                            Key = "ANKUR_ID",
                            Value = ankurWebServiceId,
                            Status = ((int)Status.AKTIF).ToString(),
                            SystemType = ((int)SystemType.WS).ToString(),
                            Id = 0
                        };
                        var spresponseParam = new GenericRepository<Parameter>().Insert(parameterWs);

                        if (spresponseParam.Code == "100")
                        {
                            CompanyParameter comP = new CompanyParameter
                            {
                                CompanyId = long.Parse(companyId),
                                ParameterId = spresponseParam.PkId,
                                Status = ((int)Status.AKTIF).ToString()
                            };
                            SpResponse spResponse = new GenericRepository<CompanyParameter>().Insert(comP);
                        }
                    }


                }
                else
                {
                    throw new Exception("Şirket Protein Giriş Bilgileri Kaydedilemedi!");
                }

                result.ResultCode = "100";
                result.ResultMessage = "OK";
                //User user = new User
                //{
                //    Id = userId,
                //    Username = proteinUserName,
                //    Password = proteinPassword.ToMD5().ToUpper()
                //};
                //var spResponseUser = new GenericRepository<User>().Insert(user);


                //var portTssUserName = form["portTssUserName"];
                //var portTssUserNameParameterId = long.Parse(form["hdPortTSSUserNameId"]);
                //if (!string.IsNullOrEmpty(portTssUserName))
                //{
                //    var parameter = new Parameter
                //    {
                //        Id = portTssUserNameParameterId,
                //        Environment = "D",
                //        SystemType = Convert.ToString((int)SystemType.PORTTSS),
                //        Key = "USER_NAME",
                //        Value = portTssUserName,
                //    };
                //    var spResponseParameter1 = new ParameterRepository().Insert(parameter);
                //    if (spResponseParameter1.Code == "100")
                //    {
                //        var companyParameter = new CompanyParameter
                //        {
                //            ParameterId = spResponseParameter1.PkId,
                //            CompanyId = long.Parse(companyId)
                //        };
                //        var spResponseCompanyParameter = new CompanyParameterRepository().Insert(companyParameter);
                //        if (string.IsNullOrEmpty(spResponseCompanyParameter.Code) || spResponseCompanyParameter.Code != "100")
                //        {
                //            throw new Exception("Şirket PortTSS Kullanıcı Adı parametresi eklenirken hata oluştu.");
                //        }
                //        else
                //        {

                //        }
                //        result.ResultCode = spResponseCompanyParameter.Code;
                //        result.ResultMessage = spResponseCompanyParameter.Message;
                //    }
                //}
                //else
                //{

                //    if (portTssUserNameParameterId > 0)
                //    {
                //        var parameter = new Parameter
                //        {
                //            Id = portTssUserNameParameterId,
                //            Status = "1"
                //        };
                //        var spResponseParameter1 = new ParameterRepository().Update(parameter);

                //        if (spResponseParameter1.Code == "100")
                //        {
                //            List<CompanyParameter> companyParameters = new CompanyParameterRepository().FindBy("COMPANY_ID = " + companyId + " AND PARAMETER_ID = " + portTssUserNameParameterId);
                //            if (companyParameters.Count > 1)
                //            {
                //                // 987 numaralı hata birden fazla aktif eşleme tablosu olma durumu
                //                throw new Exception("987 : Şirket PortTSS Kullanıcı Adı parametresi silinirken hata oluştu.");
                //            }
                //            else if (companyParameters.Count == 1)
                //            {
                //                var companyParameter = new CompanyParameter
                //                {
                //                    Id = companyParameters.ElementAt(0).Id,
                //                    Status = "1"
                //                };
                //                var spResponseCompanyParameter = new CompanyParameterRepository().Update(companyParameter);
                //                if (string.IsNullOrEmpty(spResponseCompanyParameter.Code) || spResponseCompanyParameter.Code != "100")
                //                {
                //                    throw new Exception("Şirket PortTSS Kullanıcı Adı parametresi silinirken hata oluştu.");
                //                }
                //                result.ResultCode = spResponseCompanyParameter.Code;
                //                result.ResultMessage = spResponseCompanyParameter.Message;
                //            }
                //        }
                //    }
                //}

                //var portTssPassword = form["portTssPassword"];
                //if (!string.IsNullOrEmpty(portTssPassword))
                //{
                //    var parameter = new Parameter
                //    {
                //        Id = long.Parse(form["hdportTssPasswordId"]),
                //        SystemType = Convert.ToString((int)SystemType.PORTTSS),
                //        Key = "PASSWORD",
                //        Value = portTssPassword
                //    };
                //    var spResponseParameter2 = new ParameterRepository().Insert(parameter);
                //    if (spResponseParameter2.Code == "100")
                //    {
                //        var companyParameter = new CompanyParameter
                //        {
                //            ParameterId = spResponseParameter2.PkId,
                //            CompanyId = long.Parse(companyId)
                //        };
                //        var spResponseCompanyParameter = new CompanyParameterRepository().Insert(companyParameter);
                //        if (string.IsNullOrEmpty(spResponseCompanyParameter.Code) || spResponseCompanyParameter.Code != "100")
                //        {
                //            throw new Exception("Şirket PortTSS Şifre parametresi eklenirken hata oluştu.");
                //        }
                //        result.ResultCode = spResponseCompanyParameter.Code;
                //        result.ResultMessage = spResponseCompanyParameter.Message;
                //    }
                //}

                //var sbmCode = form["sbmCode"];
                //if (!string.IsNullOrEmpty(sbmCode))
                //{
                //    var parameter = new Parameter
                //    {
                //        Id = long.Parse(form["hdSgmCodeId"]),
                //        SystemType = Convert.ToString((int)SystemType.SBM),
                //        Key = "CODE",
                //        Value = sbmCode
                //    };
                //    var spResponseParameter3 = new ParameterRepository().Insert(parameter);
                //    if (spResponseParameter3.Code == "100")
                //    {
                //        var companyParameter = new CompanyParameter
                //        {
                //            ParameterId = spResponseParameter3.PkId,
                //            CompanyId = long.Parse(companyId)
                //        };
                //        var spResponseCompanyParameter = new CompanyParameterRepository().Insert(companyParameter);
                //        if (string.IsNullOrEmpty(spResponseCompanyParameter.Code) || spResponseCompanyParameter.Code != "100")
                //        {
                //            throw new Exception("Şirket SBM Kodu parametresi eklenirken hata oluştu.");
                //        }
                //        result.ResultCode = spResponseCompanyParameter.Code;
                //        result.ResultMessage = spResponseCompanyParameter.Message;
                //    }
                //}

                //var sbmUserName = form["sbmUserName"];
                //if (!string.IsNullOrEmpty(sbmUserName))
                //{
                //    var parameter = new Parameter
                //    {
                //        Id = long.Parse(form["hdSgmUserNameId"]),
                //        SystemType = Convert.ToString((int)SystemType.SBM),
                //        Key = "USER_NAME",
                //        Value = sbmUserName
                //    };
                //    var spResponseParameter4 = new ParameterRepository().Insert(parameter);
                //    if (spResponseParameter4.Code == "100")
                //    {
                //        var companyParameter = new CompanyParameter
                //        {
                //            ParameterId = spResponseParameter4.PkId,
                //            CompanyId = long.Parse(companyId)
                //        };
                //        var spResponseCompanyParameter = new CompanyParameterRepository().Insert(companyParameter);
                //        if (string.IsNullOrEmpty(spResponseCompanyParameter.Code) || spResponseCompanyParameter.Code != "100")
                //        {
                //            throw new Exception("Şirket SBM Kullanıcı Adı parametresi eklenirken hata oluştu.");
                //        }
                //        result.ResultCode = spResponseCompanyParameter.Code;
                //        result.ResultMessage = spResponseCompanyParameter.Message;
                //    }
                //}

                //var sbmPassword = form["sbmPassword"];
                //if (!string.IsNullOrEmpty(sbmPassword))
                //{
                //    var parameter = new Parameter
                //    {
                //        Id = long.Parse(form["hdSgmPasswordId"]),
                //        SystemType = Convert.ToString((int)SystemType.SBM),
                //        Key = "PASSWORD",
                //        Value = sbmPassword
                //    };
                //    var spResponseParameter5 = new ParameterRepository().Insert(parameter);
                //    if (spResponseParameter5.Code == "100")
                //    {
                //        var companyParameter = new CompanyParameter
                //        {
                //            ParameterId = spResponseParameter5.PkId,
                //            CompanyId = long.Parse(companyId)
                //        };
                //        var spResponseCompanyParameter = new CompanyParameterRepository().Insert(companyParameter);
                //        if (string.IsNullOrEmpty(spResponseCompanyParameter.Code) || spResponseCompanyParameter.Code != "100")
                //        {
                //            throw new Exception("Şirket SBM Şifre parametresi eklenirken hata oluştu.");
                //        }
                //        result.ResultCode = spResponseCompanyParameter.Code;
                //        result.ResultMessage = spResponseCompanyParameter.Message;
                //    }
                //}
                TempData["Alert"] = $"swAlert('İşlem Başarılı','Tüm veriler başarıyla kaydedildi.','success')";
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Companies()
        {
            List<Company> companies = new GenericRepository<Company>().FindBy(orderby: "NAME");

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = companies.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        #endregion

        #region Product

        // GET: Product List /Product/Product
        [LoginControl]
        public ActionResult Product()
        {
            try
            {

                FillCompanies();

                ViewBag.productCompany = string.Empty;
                ViewBag.productCode = string.Empty;
                ViewBag.productName = string.Empty;
                ViewBag.productDescription = string.Empty;
                ViewBag.productStatus = string.Empty;
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                //Durum - DropDown
                var Status = LookupHelper.GetLookupData(Constants.LookupTypes.Status);
                ViewBag.Status = Status;

                //Fill data

                var result = new GenericRepository<V_Product>().FindBy(orderby: "PRODUCT_ID DESC");
                ViewBag.Result = result;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        // GET: Product Filter /Product/ProductFilter
        [HttpPost]
        [LoginControl]
        public ActionResult ProductFilter(FormCollection form)
        {
            FillCompanies();
            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            var Status = LookupHelper.GetLookupData(LookupTypes.Status);
            ViewBag.Status = Status;
            //Fill data
            ViewBag.productCompany = form["productCompany"];
            ViewBag.productCode = form["productCode"];
            ViewBag.productName = form["productName"];
            ViewBag.productDescription = form["productDescription"];
            ViewBag.productStatus = form["productStatus"];

            dynamic parameters = new System.Dynamic.ExpandoObject();
            parameters.companyId = form["productCompany"];
            parameters.productCode = new DbString { Value = form["productCode"], Length = 20 };
            parameters.productName = new DbString { Value = form["productName"], Length = 100 };
            parameters.productDescription = new DbString { Value = form["productDescription"], Length = 1000 };
            parameters.productStatus = form["productStatus"].IsNull() ? null : form["productStatus"].Split(',');

            string whereConditition = form["productCompany"].IsInt64() ? $" COMPANY_ID=:companyId AND" : "";
            whereConditition += !form["productCode"].IsNull() ? $" PRODUCT_CODE LIKE '%'||:productCode||'%' AND" : "";
            whereConditition += !form["productName"].IsNull() ? $" PRODUCT_NAME LIKE '%'||:productName||'%' AND" : "";
            whereConditition += !form["productDescription"].IsNull() ? $" PRODUCT_DESCRIPTION LIKE '%'||:productDescription||'%' AND" : "";
            whereConditition += !form["productStatus"].IsNull() ? $" STATUS IN :productStatus AND" : "";

            var result = new GenericRepository<V_Product>().FindBy(whereConditition.IsNull() ? "" : whereConditition.Substring(0, whereConditition.Length - 3), orderby: "PRODUCT_ID", parameters: parameters);
            ViewBag.Result = result;

            return View("Product");
        }

        // GET: Product Delete /Product/ProductDelete
        [LoginControl]
        public ActionResult ProductDelete(Int64 id)
        {
            try
            {
                var repo = new ProductRepository();
                var result = repo.FindById(id);
                if (result != null)
                {
                    result.Status = "1";
                    repo.Update(result);
                }
            }
            catch (Exception)
            {
                //
            }
            return RedirectToAction("Product");
        }

        [LoginControl]
        public ActionResult ProductClone(Int64 id = 0)
        {

            try
            {
                if (id != 0)
                {
                    var oldProduct = new ProductRepository().FindById(id);
                    if (oldProduct != null)
                    {
                        #region Product Clone                        
                        long newProductId = 0;
                        #region Clone Product Details
                        Product product = new Product
                        {
                            Id = 0,
                            Name = oldProduct.Name + "-Kopya",
                            Status = oldProduct.Status,
                            Type = oldProduct.Type,
                            Description = oldProduct.Description,
                            CompanyId = oldProduct.CompanyId,
                            BranchId = oldProduct.BranchId,
                            Code = oldProduct.Code
                        };
                        var spResponseProduct = new ProductRepository().Insert(product);
                        if (spResponseProduct.Code != "100")
                        {
                            throw new Exception(spResponseProduct.Code + " : " + spResponseProduct.Message);
                        }
                        newProductId = spResponseProduct.PkId;
                        product.Id = newProductId;
                        #endregion

                        #region Clone Product's Notes
                        var productNoteList = new ProductNoteRepository().FindBy("PRODUCT_ID=:id", parameters: new { id = oldProduct.Id });

                        foreach (var productNote in productNoteList)
                        {
                            #region Clone Note
                            // Clone Note If Exists
                            long noteId = 0;
                            if (productNote.NoteId > 0)
                            {
                                noteId = CloneHelper.NoteClone(productNote.NoteId);
                            }
                            if (noteId > 0)
                            {
                                #region Clone Product Note Relation
                                // Clone product Note If Exists
                                long productNoteId = 0;
                                if (productNote.NoteId > 0)
                                {
                                    productNoteId = CloneHelper.ProductNoteClone(productNote.Id, newProductId, noteId);
                                }
                                #endregion
                            }
                            #endregion
                        }
                        #endregion

                        #region Clone Product's Medias
                        var productMediaList = new ProductMediaRepository().FindBy("PRODUCT_ID=:id", parameters: new { id = oldProduct.Id });
                        foreach (var productMedia in productMediaList)
                        {
                            #region Clone Media
                            // Clone Media If Exists
                            long mediaId = 0;
                            if (productMedia.MediaId > 0)
                            {
                                mediaId = CloneHelper.MediaClone<ProductMedia>(productMedia.MediaId, productMedia.ProductId);
                            }
                            if (mediaId > 0)
                            {
                                #region Clone Product Media Relation
                                // Clone product Media If Exists
                                long productMediaId = 0;
                                if (productMedia.MediaId > 0)
                                {
                                    productMediaId = CloneHelper.ProductMediaClone(productMedia.Id, newProductId, mediaId);
                                }
                                #endregion
                            }
                            #endregion
                        }
                        #endregion

                        #region  Parameter Clone
                        var productParameterList = new ProductParameterRepository().FindBy("PRODUCT_ID=:id", parameters: new { id = oldProduct.Id });
                        foreach (var productParameter in productParameterList)
                        {
                            #region Parameter Clone
                            long parameterId = 0;
                            if (productParameter.ParameterId > 0)
                            {
                                parameterId = CloneHelper.ParameterClone(productParameter.ParameterId);
                            }
                            if (parameterId > 0)
                            {
                                #region Clone Product Parameter Reletion
                                long productParameterId = 0;
                                if (productParameter.ParameterId > 0)
                                {
                                    productParameterId = CloneHelper.ProductParameterClone(productParameter.Id, newProductId, parameterId);
                                }

                                #endregion
                            }
                            #endregion
                        }
                        #endregion

                        #endregion

                        #region Subproduct Clone
                        var subproductList = new SubproductRepository().FindBy("PRODUCT_ID=:id", parameters: new { id = oldProduct.Id });
                        if (subproductList != null)
                        {
                            long newSubproductId = 0;
                            #region Clone Subproduct Details
                            foreach (var subproduct in subproductList)
                            {
                                #region Subproduct Clone
                                Subproduct newSubproduct = new Subproduct
                                {
                                    Id = 0,
                                    Name = subproduct.Name + "-Kopya",
                                    Code = subproduct.Code,
                                    Description = subproduct.Description,
                                    ProductId = newProductId,
                                    Status = subproduct.Status,
                                    Type = subproduct.Type
                                };
                                var spResponseSubproduct = new SubproductRepository().Insert(newSubproduct);
                                if (spResponseSubproduct.Code != "100")
                                {
                                    throw new Exception(spResponseSubproduct.Code + " : " + spResponseSubproduct.Message);
                                }
                                newSubproductId = spResponseSubproduct.PkId;
                                newSubproduct.Id = newSubproductId;
                                #endregion

                                #region Clone Notes
                                var subproductNoteList = new SubproductNoteRepository().FindBy("SUBPRODUCT_ID=:id", parameters: new { id = subproduct.Id });
                                foreach (var subproductNote in subproductNoteList)
                                {
                                    #region Clone Note
                                    // Clone Note If Exists
                                    long noteId = 0;
                                    if (subproductNote.NoteId > 0)
                                    {
                                        noteId = CloneHelper.NoteClone(subproductNote.NoteId);
                                    }
                                    if (noteId > 0)
                                    {
                                        #region Clone Subproduct Note Relationn                                       
                                        long subproductNoteId = 0;
                                        if (subproductNote.NoteId > 0)
                                        {
                                            subproductNoteId = CloneHelper.SubproductNoteClone(subproductNote.Id, newSubproductId, noteId);
                                        }
                                        #endregion
                                    }
                                    #endregion
                                }
                                #endregion

                                #region Clone Subproduct's Medias
                                var subproductMediaList = new SubproductMediaRepository().FindBy("SUBPRODUCT_ID=:id", parameters: new { id = subproduct.Id });
                                foreach (var subproductMedia in subproductMediaList)
                                {
                                    #region Clone Media                                
                                    long mediaId = 0;
                                    if (subproductMedia.MediaId > 0)
                                    {
                                        mediaId = CloneHelper.MediaClone<SubproductMedia>(subproductMedia.MediaId, subproductMedia.SubproductId);
                                    }
                                    if (mediaId > 0)
                                    {
                                        #region Clone Subproduct Media Relation
                                        // Clone product Media If Exists
                                        long subproductMediaId = 0;
                                        if (subproductMedia.MediaId > 0)
                                        {
                                            subproductMediaId = CloneHelper.SubproductMediaClone(subproductMedia.Id, newSubproductId, mediaId);
                                        }
                                        #endregion
                                    }
                                    #endregion
                                }
                                #endregion

                                #region  Parameter Clone
                                var subproductParameterList = new SubproductParameterRepository().FindBy("SUBPRODUCT_ID=:id", parameters: new { id = subproduct.Id });
                                foreach (var subproductParameter in subproductParameterList)
                                {
                                    #region Parameter Clone
                                    long parameterId = 0;
                                    if (subproductParameter.ParameterId > 0)
                                    {
                                        parameterId = CloneHelper.ParameterClone(subproductParameter.ParameterId);
                                    }
                                    if (parameterId > 0)
                                    {
                                        #region Clone Subproduct Parameter Reletion
                                        long subProductParameterId = 0;
                                        if (subproductParameter.ParameterId > 0)
                                        {
                                            subProductParameterId = CloneHelper.SubproductParameterClone(subproductParameter.Id, newSubproductId, parameterId);
                                        }

                                        #endregion
                                    }
                                    #endregion
                                }
                                #endregion

                                #region Plan Clone
                                var subproductPlanList = new PlanRepository().FindBy("SUBPRODUCT_ID=:id", parameters: new { id = subproduct.Id });
                                if (subproductPlanList != null)
                                {
                                    long newPlanId = 0;
                                    #region Clone Plan Details
                                    foreach (var subproductPlan in subproductPlanList)
                                    {
                                        #region Plan Clone
                                        Plan plan = new Plan
                                        {
                                            Id = 0,
                                            Name = subproductPlan.Name + "-Kopya",
                                            SubproductId = newSubproductId,
                                            Status = subproductPlan.Status
                                        };
                                        var spResponsePlan = new PlanRepository().Insert(plan);
                                        if (spResponsePlan.Code != "100")
                                        {
                                            throw new Exception(spResponsePlan.Code + " : " + spResponsePlan.Message);
                                        }
                                        newPlanId = spResponsePlan.PkId;
                                        plan.Id = newPlanId;
                                        #endregion

                                        var planCoverageList = new PlanCoverageRepository().FindBy("PLAN_ID=:id AND PARENT_ID IS NULL", parameters: new { id = subproductPlan.Id });
                                        if (planCoverageList != null)
                                        {
                                            foreach (var planCoverage in planCoverageList)
                                            {
                                                #region Clone Plan Coverage
                                                if (planCoverage.CoverageId > 0)
                                                {
                                                    CloneHelper.PlanCoverageClone(planCoverage.Id, newPlanId, null);
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                #endregion
                            }
                            #endregion
                            TempData["Alert"] = $"swAlert('İşlem Başarılı','Ürün başarıyla kopyalandı.','success')";
                        }
                        #endregion
                    }
                }
            }



            catch (Exception)
            {
                throw;
            }

            return RedirectToAction("Product");

        }

        // GET: Product Form /Product/ProductForm
        [LoginControl]
        public ActionResult ProductForm(Int64 id = 0, string formaction = "")
        {
            ViewBag.isEdit = false;

            MediaVM vm = new MediaVM();
            vm.MediaObjectType = Business.Enums.Media.MediaObjectType.T_PRODUCT_MEDIA;
            vm.ObjectId = id;
            vm.ShowDateInfo = true;
            vm.TabName = "tab3";
            //vm.MediaList = new MediaWorker<V_ProductMedia>().GetAllFindByObjectId(id);

            try
            {

                var CompanyList = new CompanyRepository().FindBy();
                ViewBag.CompanyList = CompanyList;

                var BranchList = new BranchRepository().FindBy();
                ViewBag.BranchList = BranchList;

                var ProductTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Product);
                ViewBag.ProductTypeList = ProductTypeList;

                var NoteTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Note);
                ViewBag.NoteTypeList = NoteTypeList;

                ViewBag.RuleCategoryTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.RuleCategory);

                var RuleList = new GenericRepository<V_Rule>().FindBy(orderby: "RULE_NAME ASC");
                ViewBag.RuleList = RuleList;

                var RuleGroupList = new GenericRepository<RuleGroup>().FindBy();
                ViewBag.RuleGroupList = RuleGroupList;

                ViewBag.SagmerNo = "";

                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Ürün Güncelle";
                        if (id != 0)
                        {

                            var Product = new GenericRepository<V_Product>().FindBy("PRODUCT_ID=:id", orderby: "PRODUCT_ID", fetchDeletedRows: true, parameters: new { id }).FirstOrDefault();
                            if (Product != null)
                            {
                                ViewBag.Product = Product;

                                var parameters = new GenericRepository<V_ProductParameter>().FindBy("PRODUCT_ID=:id AND SYSTEM_TYPE=:systemType AND KEY=:key",
                                                                                    fetchDeletedRows: true,
                                                                                    orderby: "PARAMETER_ID",
                                                                                    parameters: new { id, systemType = new DbString { Value = ((int)SystemType.SAGMER).ToString(), Length = 3 }, key = new DbString { Value = "REGISTRY_DATE", Length = 30 } });
                                if (parameters != null)
                                {
                                    foreach (var item in parameters)
                                    {
                                        ViewBag.SAGMER_REGISTRY_DATE_PRODUCT_PARAMETER_ID = item.PRODUCT_PARAMETER_ID;
                                        ViewBag.SAGMER_REGISTRY_DATE_PARAMETER_ID = item.PARAMETER_ID;
                                        ViewBag.SagmerRegistryDate = item.VALUE;
                                    }
                                }

                                parameters = new GenericRepository<V_ProductParameter>().FindBy("PRODUCT_ID=:id AND SYSTEM_TYPE=:systemType AND KEY=:key",
                                                                                    fetchDeletedRows: true,
                                                                                    orderby: "PARAMETER_ID",
                                                                                    parameters: new { id = id, systemType = new DbString { Value = ((int)SystemType.SAGMER).ToString(), Length = 3 }, key = new DbString { Value = "NO", Length = 30 } });
                                if (parameters != null)
                                {
                                    foreach (var item in parameters)
                                    {
                                        ViewBag.SAGMER_NO_PRODUCT_PARAMETER_ID = item.PRODUCT_PARAMETER_ID;
                                        ViewBag.SAGMER_NO_PARAMETER_ID = item.PARAMETER_ID;
                                        ViewBag.SagmerNo = item.VALUE;
                                    }
                                }

                                var rules = new GenericRepository<V_RuleProduct>().FindBy("PRODUCT_ID=:id", orderby: "PRODUCT_ID", parameters: new { id });
                                if (rules != null)
                                {
                                    List<dynamic> rulesList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in rules)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.RULE_PRODUCT_ID = Convert.ToString(item.RULE_PRODUCT_ID);
                                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                        listItem.RULE_NAME = item.RULE_NAME;
                                        listItem.RESULT = item.RESULT;
                                        listItem.isOpen = "0";
                                        rulesList.Add(listItem);

                                        i++;
                                    }

                                    ViewBag.Rules = rulesList.ToJSON(true);
                                }

                                var notes = new GenericRepository<V_ProductNote>().FindBy("PRODUCT_ID=:id", orderby: "PRODUCT_ID", fetchDeletedRows: true, parameters: new { id });
                                if (notes != null)
                                {
                                    List<dynamic> notesList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in notes)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                                        listItem.PRODUCT_NOTE_ID = Convert.ToString(item.PRODUCT_NOTE_ID);

                                        listItem.NOTE_TYPE = item.NOTE_TYPE;
                                        listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Note, item.NOTE_TYPE);
                                        listItem.NOTE_DESCRIPTION = item.NOTE_DESCRIPTION;
                                        listItem.isOpen = "0";

                                        notesList.Add(listItem);

                                        i++;
                                    }
                                    ViewBag.Notes = notesList.ToJSON();
                                }
                            }
                            ViewBag.isEdit = true;
                        }
                        break;

                    default:
                        ViewBag.Title = "Ürün Ekle";
                        ViewBag.isEdit = false;

                        break;
                }
            }

            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }


            return View(vm);

        }

        // POST: Product Form Save /Product/ProductFormSave
        [HttpPost]
        public JsonResult ProductSaveStep1(FormCollection form)
        {
            var result = new AjaxResultDto<ProductSaveStep1Result>();
            ProductSaveStep1Result step1Result = new ProductSaveStep1Result();
            try
            {
                var productid = form["hdProductId"];
                var companyId = form["productCompany"];
                var branchId = form["productBranch"];
                var productType = form["productType"];
                var productCode = form["productCode"];
                var productName = form["productName"];
                var productDescription = form["productDescription"];

                var product = new Product
                {
                    Id = !String.IsNullOrEmpty(productid) ? long.Parse(productid) : 0,
                    CompanyId = long.Parse(companyId),
                    BranchId = long.Parse(branchId),
                    Type = productType,
                    Code = String.IsNullOrEmpty(productCode) ? "" : productCode,
                    Name = productName,
                    Description = productDescription
                };
                var spResponseProduct = new ProductRepository().Insert(product);
                if (spResponseProduct.Code == "100")
                {
                    var productId = spResponseProduct.PkId;
                    step1Result.productId = productId;
                    var sagmerRegistryDate = form["sagmerRegistryDate"];
                    if (!String.IsNullOrEmpty(sagmerRegistryDate))
                    {
                        var parameter = new Parameter
                        {
                            Id = long.Parse(form["hdSagmerRegistryDateParameterId"]),
                            SystemType = "2",
                            Environment = "D",
                            Key = "REGISTRY_DATE",
                            Value = sagmerRegistryDate
                        };
                        var spResponseParameter = new ParameterRepository().Insert(parameter);
                        if (spResponseParameter.Code == "100")
                        {
                            var parameterId = spResponseParameter.PkId;
                            step1Result.sagmerRegistryDateParameterId = parameterId;

                            var productParameter = new ProductParameter
                            {
                                Id = long.Parse(form["hdSagmerRegistryDateProductParameterId"]),
                                ProductId = productId,
                                ParameterId = parameterId
                            };
                            var spResponseProductParameter = new ProductParameterRepository().Insert(productParameter);
                            if (spResponseProductParameter.Code == "100")
                            {
                                step1Result.sagmerRegistryDateProductParameterId = spResponseProductParameter.PkId;
                                result.Data = step1Result;
                                result.ResultCode = spResponseProductParameter.Code;
                                result.ResultMessage = spResponseProductParameter.Message;
                            }
                        }
                    }
                    else
                    {
                        result.Data = step1Result;
                        result.ResultCode = spResponseProduct.Code;
                        result.ResultMessage = spResponseProduct.Message;
                    }

                    var sagmerNo = form["sagmerNo"];
                    if (!String.IsNullOrEmpty(sagmerNo))
                    {
                        var parameter = new Parameter
                        {
                            Id = long.Parse(form["hdSagmerNoParameterId"]),
                            SystemType = "2",
                            Environment = "D",
                            Key = "NO",
                            Value = sagmerNo
                        };
                        var spResponseParameter = new ParameterRepository().Insert(parameter);
                        if (spResponseParameter.Code == "100")
                        {
                            var parameterId = spResponseParameter.PkId;
                            step1Result.sagmerNoParameterId = parameterId;

                            var productParameter = new ProductParameter
                            {
                                Id = long.Parse(form["hdSagmerNoProductParameterId"]),
                                ProductId = productId,
                                ParameterId = parameterId
                            };
                            var spResponseProductParameter = new ProductParameterRepository().Insert(productParameter);
                            if (spResponseProductParameter.Code == "100")
                            {
                                step1Result.sagmerNoProductParameterId = spResponseProductParameter.PkId;
                                result.Data = step1Result;
                                result.ResultCode = spResponseProductParameter.Code;
                                result.ResultMessage = spResponseProductParameter.Message;
                            }
                        }
                    }
                    else
                    {
                        result.Data = step1Result;
                        result.ResultCode = spResponseProduct.Code;
                        result.ResultMessage = spResponseProduct.Message;
                    }
                }
                else
                {
                    throw new Exception(spResponseProduct.Message);
                }
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }

            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ProductSaveStep2(FormCollection form)
        {
            var result = new AjaxResultDto<ProductSaveStep2Result>();
            var notes = form["hdNotes"];
            try
            {
                List<dynamic> noteList = new List<dynamic>();
                noteList = JsonConvert.DeserializeObject<List<dynamic>>(notes);
                var productId = form["hdProductId"];

                foreach (var note in noteList)
                {
                    if (note.isOpen == "1")
                    {
                        var Tnote = new Note
                        {
                            Id = note.NOTE_ID,
                            Type = note.NOTE_TYPE,
                            Description = note.NOTE_DESCRIPTION,
                            Status = note.STATUS
                        };
                        var spResponseNote = new NoteRepository().Insert(Tnote);
                        if (spResponseNote.Code == "100")
                        {
                            var noteId = spResponseNote.PkId;
                            var productNote = new ProductNote
                            {
                                Id = note.PRODUCT_NOTE_ID,
                                ProductId = long.Parse(productId),
                                NoteId = noteId,
                                Status = note.STATUS
                            };
                            var spResponseProductNote = new ProductNoteRepository().Insert(productNote);
                            if (spResponseProductNote.Code == "100")
                            {
                                result.ResultCode = spResponseProductNote.Code;
                                result.ResultMessage = spResponseProductNote.Message;
                            }
                            else
                            {
                                throw new Exception(spResponseProductNote.Code + " : " + spResponseProductNote.Message);
                            }
                        }
                        else
                        {
                            throw new Exception(spResponseNote.Code + " : " + spResponseNote.Message);
                        }
                    }
                }

                var notess = new GenericRepository<V_ProductNote>().FindBy("PRODUCT_ID=:id", orderby: "PRODUCT_ID", fetchDeletedRows: true, parameters: new { id = productId });
                if (notess != null)
                {
                    List<dynamic> notesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in notess)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                        listItem.PRODUCT_NOTE_ID = Convert.ToString(item.PRODUCT_NOTE_ID);

                        listItem.NOTE_TYPE = Convert.ToString(item.NOTE_TYPE);
                        listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Note, item.NOTE_TYPE);
                        listItem.NOTE_DESCRIPTION = item.NOTE_DESCRIPTION;
                        listItem.isOpen = "0";

                        notesList.Add(listItem);

                        i++;
                    }

                    result.Data = new ProductSaveStep2Result
                    {
                        jsonData = notesList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ProductSaveStep3(FormCollection form)
        {
            var result = new AjaxResultDto<ProductSaveStep3Result>();
            var medias = form["hdMedias"];
            try
            {
                List<dynamic> mediaList = new List<dynamic>();
                mediaList = JsonConvert.DeserializeObject<List<dynamic>>(medias);
                var productId = form["hdProductId"];

                foreach (var media in mediaList)
                {
                    if (media.isOpen == "1")
                    {
                        var Tmedia = new Media
                        {
                            Id = media.MEDIA_ID,
                            Name = media.MEDIA_NAME,
                            FileName = "TEST",
                            Status = media.STATUS
                        };
                        var spResponseMedia = new MediaRepository().Insert(Tmedia);
                        if (spResponseMedia.Code == "100")
                        {
                            var mediaId = spResponseMedia.PkId;
                            var productNote = new ProductMedia
                            {
                                Id = media.PRODUCT_MEDIA_ID,
                                ProductId = long.Parse(productId),
                                MediaId = mediaId,
                                Status = media.STATUS
                            };
                            var spResponseProductMedia = new ProductMediaRepository().Insert(productNote);
                            if (spResponseProductMedia.Code == "100")
                            {
                                result.ResultCode = spResponseProductMedia.Code;
                                result.ResultMessage = spResponseProductMedia.Message;
                            }
                            else
                            {
                                throw new Exception(spResponseProductMedia.Code + " : " + spResponseProductMedia.Message);
                            }
                        }
                        else
                        {
                            throw new Exception(spResponseMedia.Code + " : " + spResponseMedia.Message);
                        }
                    }
                }

                var mediass = new GenericRepository<V_ProductMedia>().FindBy("PRODUCT_ID=:id", orderby: "PRODUCT_ID", fetchDeletedRows: true, parameters: new { id = productId });
                if (mediass != null)
                {
                    List<dynamic> mediasList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in mediass)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.Status;

                        listItem.MEDIA_ID = Convert.ToString(item.MediaId);
                        listItem.PRODUCT_MEDIA_ID = Convert.ToString(item.ProductMediaId);
                        listItem.MEDIA_NAME = item.MediaName;
                        listItem.MEDIA_PATH = item.FileName;
                        listItem.isOpen = "0";
                        mediasList.Add(listItem);

                        i++;
                    }

                    result.Data = new ProductSaveStep3Result
                    {
                        jsonData = mediasList.ToJSON()
                    };
                }

            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ProductSaveStep4(FormCollection form)
        {
            var result = new AjaxResultDto<ProductSaveStep4Result>();
            ProductSaveStep4Result step1Result = new ProductSaveStep4Result();
            var rules = form["hdRules"];
            try
            {
                var productId = form["hdProductId"];
                List<dynamic> ruleList = new List<dynamic>();
                ruleList = JsonConvert.DeserializeObject<List<dynamic>>(rules);
                foreach (var rule in ruleList)
                {
                    if (rule.isOpen == "1")
                    {
                        if (rule.STATUS == "0")
                        {
                            string idList = string.Empty;

                            if (rule.chkRuleSelectedId != null)
                            {
                                idList = rule.chkRuleSelectedId.ToString();

                                foreach (var ruleId in idList.Replace("\r", "").Replace("\n", "").Replace("[", string.Empty).Replace("]", "").Replace("\"", "").Replace(" ", "").Split(','))
                                {
                                    var TruleProduct = new RuleProduct
                                    {
                                        Id = rule.RULE_PRODUCT_ID,
                                        ProductId = long.Parse(productId),
                                        RuleId = long.Parse(ruleId),
                                        Status = Convert.ToString(rule.STATUS),
                                        Type = Convert.ToString(rule.RULE_TYPE)
                                    };
                                    var spResponseRuleProduct = new RuleProductRepository().Insert(TruleProduct);
                                    if (spResponseRuleProduct.Code == "100")
                                    {
                                        result.ResultCode = spResponseRuleProduct.Code;
                                        result.ResultMessage = spResponseRuleProduct.Message;
                                    }
                                    else
                                    {
                                        throw new Exception(spResponseRuleProduct.Message);
                                    }
                                }
                            }
                            else
                            {
                                idList = rule.chkRuleGroupSelectedId.ToString();

                                foreach (var ruleGroupId in idList.Replace("\r", "").Replace("\n", "").Replace("[", string.Empty).Replace("]", "").Replace("\"", "").Replace(" ", "").Split(','))
                                {
                                    List<RuleGroupRule> ruleGroupRuleList = new GenericRepository<RuleGroupRule>().FindBy($"RULE_GROUP_ID={ruleGroupId}");
                                    if (ruleGroupRuleList.Any())
                                    {
                                        foreach (var ruleGroupRule in ruleGroupRuleList)
                                        {
                                            var TruleProduct = new RuleProduct
                                            {
                                                Id = rule.RULE_PRODUCT_ID,
                                                ProductId = long.Parse(productId),
                                                RuleGroupId = long.Parse(ruleGroupId),
                                                RuleId = ruleGroupRule.RuleId,
                                                Status = Convert.ToString(rule.STATUS),
                                                Type = Convert.ToString(rule.RULE_TYPE)
                                            };
                                            var spResponseRuleProduct = new RuleProductRepository().Insert(TruleProduct);
                                            if (spResponseRuleProduct.Code == "100")
                                            {
                                                result.ResultCode = spResponseRuleProduct.Code;
                                                result.ResultMessage = spResponseRuleProduct.Message;
                                            }
                                            else
                                            {
                                                throw new Exception(spResponseRuleProduct.Message);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            RuleProduct ruleProduct = new GenericRepository<RuleProduct>().FindById(long.Parse(Convert.ToString(rule.RULE_PRODUCT_ID)));
                            if (ruleProduct != null)
                            {
                                ruleProduct.Status = ((int)Status.SILINDI).ToString();
                                var spResponseRuleProduct = new RuleProductRepository().Update(ruleProduct);
                                if (spResponseRuleProduct.Code == "100")
                                {
                                    result.ResultCode = spResponseRuleProduct.Code;
                                    result.ResultMessage = spResponseRuleProduct.Message;
                                }
                                else
                                {
                                    throw new Exception(spResponseRuleProduct.Message);
                                }
                            }
                        }
                    }
                }
                var ruless = new GenericRepository<V_RuleProduct>().FindBy("PRODUCT_ID=:id", orderby: "PRODUCT_ID", parameters: new { id = productId });
                if (ruless != null)
                {
                    List<dynamic> rulesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in ruless)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.RULE_PRODUCT_ID = Convert.ToString(item.RULE_PRODUCT_ID);
                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                        listItem.RULE_NAME = item.RULE_NAME;
                        listItem.RESULT = item.RESULT;
                        listItem.isOpen = "0";

                        rulesList.Add(listItem);

                        i++;
                    }

                    result.Data = new ProductSaveStep4Result
                    {
                        jsonData = rulesList.ToJSON()
                    };
                }
                result.ResultCode = "100";
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Products(Int64 companyId)
        {
            List<Product> products = new GenericRepository<Product>().FindBy(conditions: $"COMPANY_ID={companyId}", orderby: "NAME");

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = products.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        #endregion

        #region Subproduct
        [LoginControl]
        public ActionResult Subproduct(Int64 Id = 0)
        {
            if (Id != 0)
            {
                //Fill data

                var result = new GenericRepository<V_SubProduct>().FindBy("PRODUCT_ID=:id",
                                             orderby: "SUBPRODUCT_ID DESC",
                                             parameters: new { id = Id });
                ViewBag.Subproducts = result;
                ViewBag.ProductId = Id;
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                return View();
            }
            else
            {
                return RedirectToAction("Product");
            }
        }

        [LoginControl]
        public ActionResult SubproductClone(Int64 Id = 0)
        {

            var oldSubproduct = new SubproductRepository().FindById(Id);
            var productId = oldSubproduct.ProductId;
            try
            {
                if (Id != 0)
                {

                    if (oldSubproduct != null)
                    {

                        long newSubproductId = 0;
                        #region Clone Subproduct Details                        
                        #region Subproduct Clone
                        Subproduct subproduct = new Subproduct
                        {
                            Id = 0,
                            Name = oldSubproduct.Name + "-Kopya",
                            Description = oldSubproduct.Description,
                            ProductId = oldSubproduct.ProductId,
                            Status = oldSubproduct.Status,
                            Type = oldSubproduct.Type,
                            AgreedAmount = oldSubproduct.AgreedAmount
                        };
                        var spResponseSubproduct = new SubproductRepository().Insert(subproduct);
                        if (spResponseSubproduct.Code != "100")
                        {
                            throw new Exception(spResponseSubproduct.Code + " : " + spResponseSubproduct.Message);
                        }
                        newSubproductId = spResponseSubproduct.PkId;
                        subproduct.Id = newSubproductId;
                        #endregion

                        #region Clone Subproduct's Notes
                        var subproductNoteList = new GenericRepository<SubproductNote>().FindBy("SUBPRODUCT_ID=:id", parameters: new { id = oldSubproduct.Id });

                        foreach (var subproductNote in subproductNoteList)
                        {
                            #region Clone Note
                            long noteId = 0;
                            if (subproductNote.NoteId > 0)
                            {
                                noteId = CloneHelper.NoteClone(subproductNote.NoteId);
                            }
                            if (noteId > 0)
                            {
                                #region Clone Subproduct Note Relation                          
                                long subproductNoteId = 0;
                                if (subproductNote.NoteId > 0)
                                {
                                    subproductNoteId = CloneHelper.SubproductNoteClone(subproductNote.Id, newSubproductId, noteId);
                                }
                                #endregion
                            }
                            #endregion
                        }
                        #endregion

                        #region Clone Subproduct's Medias
                        var subproductMediaList = new SubproductMediaRepository().FindBy("SUBPRODUCT_ID=:id", parameters: new { id = oldSubproduct.Id });

                        foreach (var subproductMedia in subproductMediaList)
                        {
                            #region Clone Media
                            // Clone Media If Exists
                            if (subproductMedia.MediaId > 0)
                            {
                                CloneHelper.MediaClone<SubproductMedia>(subproductMedia.MediaId, newSubproductId);
                            }
                            #endregion
                        }
                        #endregion

                        #region  Clone Subproduct's Parameters
                        var subproductParameterList = new SubproductParameterRepository().FindBy("SUBPRODUCT_ID=:id", parameters: new { id = oldSubproduct.Id });
                        foreach (var subproductParameter in subproductParameterList)
                        {
                            #region Parameter Clone
                            long parameterId = 0;
                            if (subproductParameter.ParameterId > 0)
                            {
                                parameterId = CloneHelper.ParameterClone(subproductParameter.ParameterId);
                            }
                            if (parameterId > 0)
                            {
                                #region Clone Subproduct Parameter Reletion
                                long subProductParameterId = 0;
                                if (subproductParameter.ParameterId > 0)
                                {
                                    subProductParameterId = CloneHelper.SubproductParameterClone(subproductParameter.Id, newSubproductId, parameterId);
                                }

                                #endregion
                            }
                            #endregion
                        }
                        #endregion

                        #region Clone Subproduct's Plans
                        var subproductPlanList = new PlanRepository().FindBy("SUBPRODUCT_ID=:id", parameters: new { id = oldSubproduct.Id });
                        if (subproductPlanList != null)
                        {
                            long newPlanId = 0;
                            #region Clone Plan Details
                            foreach (var subproductPlan in subproductPlanList)
                            {
                                #region Plan Clone
                                Plan plan = new Plan
                                {
                                    Id = 0,
                                    Name = subproductPlan.Name + "-Kopya",
                                    SubproductId = newSubproductId,
                                    Status = subproductPlan.Status
                                };
                                var spResponsePlan = new PlanRepository().Insert(plan);
                                if (spResponsePlan.Code != "100")
                                {
                                    throw new Exception(spResponsePlan.Code + " : " + spResponsePlan.Message);
                                }
                                newPlanId = spResponsePlan.PkId;
                                plan.Id = newPlanId;
                                #endregion

                                var planCoverageList = new PlanCoverageRepository().FindBy("PLAN_ID=:id AND PARENT_ID IS NULL", parameters: new { id = subproductPlan.Id });
                                if (planCoverageList != null)
                                {
                                    foreach (var planCoverage in planCoverageList)
                                    {
                                        #region Clone Plan Coverage
                                        if (planCoverage.Id > 0)
                                        {
                                            CloneHelper.PlanCoverageClone(planCoverage.Id, newPlanId, null);
                                        }
                                        #endregion
                                    }

                                }
                            }
                            #endregion
                        }
                        #endregion

                        #endregion
                        TempData["Alert"] = $"swAlert('İşlem Başarılı','Alt ürün başarıyla kopyalandı.','success')";

                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return RedirectToAction("Subproduct/" + productId);

        }

        [LoginControl]
        public ActionResult SubproductForm(Int64 id = 0, string formaction = "")
        {
            MediaVM vm = new MediaVM();
            vm.MediaObjectType = Business.Enums.Media.MediaObjectType.T_SUBPRODUCT_MEDIA;
            // vm.ObjectId = id;
            vm.TabName = "tab3";

            // id is product.id if a new subproduct form is open, id is subproduct.id if a subproduct form is open for editing
            if (id == 0)
            {
                return RedirectToAction("Product");
            }
            try
            {
                var NoteTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Note);
                ViewBag.NoteTypeList = NoteTypeList;

                ViewBag.RuleCategoryTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.RuleCategory);

                var RuleList = new GenericRepository<V_Rule>().FindBy(orderby: "RULE_NAME ASC");
                ViewBag.RuleList = RuleList;

                var RuleGroupList = new GenericRepository<RuleGroup>().FindBy();
                ViewBag.RuleGroupList = RuleGroupList;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;
                ViewBag.isEdit = false;

                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Alt Ürün Güncelle";
                        vm.ObjectId = id;

                        var Subproduct = new GenericRepository<V_SubProduct>().FindBy("SUBPRODUCT_ID=:subproductId", orderby: "SUBPRODUCT_ID", parameters: new { subproductId = id }).FirstOrDefault();
                        if (Subproduct != null)
                        {
                            ViewBag.Subproduct = Subproduct;
                            ViewBag.ProductId = Subproduct.PRODUCT_ID;

                            var parameters = new GenericRepository<V_SubProductParameter>().FindBy("SUBPRODUCT_ID=:id AND SYSTEM_TYPE=:SYSTEM_TYPE AND KEY=:KEY",
                                orderby: "PARAMETER_ID",
                                parameters: new { id, SYSTEM_TYPE = new DbString { Value = ((int)SystemType.SAGMER).ToString(), Length = 3 }, KEY = new DbString { Value = "REGISTRY_DATE", Length = 30 } });
                            if (parameters != null)
                            {
                                foreach (var item in parameters)
                                {
                                    ViewBag.SAGMER_REGISTRY_DATE_SUBPRODUCT_PARAMETER_ID = item.SUBPRODUCT_PARAMETER_ID;
                                    ViewBag.SAGMER_REGISTRY_DATE_PARAMETER_ID = item.PARAMETER_ID;
                                    ViewBag.SagmerRegistryDate = item.VALUE;
                                }
                            }

                            parameters = new GenericRepository<V_SubProductParameter>().FindBy("SUBPRODUCT_ID=:id AND SYSTEM_TYPE=:SYSTEM_TYPE AND KEY=:KEY",
                                fetchDeletedRows: true,
                                orderby: "PARAMETER_ID",
                                parameters: new { id, SYSTEM_TYPE = new DbString { Value = ((int)SystemType.SAGMER).ToString(), Length = 3 }, KEY = new DbString { Value = "NO", Length = 30 } });
                            if (parameters != null)
                            {
                                foreach (var item in parameters)
                                {
                                    ViewBag.SAGMER_NO_SUBPRODUCT_PARAMETER_ID = item.SUBPRODUCT_PARAMETER_ID;
                                    ViewBag.SAGMER_NO_PARAMETER_ID = item.PARAMETER_ID;
                                    ViewBag.SagmerNo = item.VALUE;
                                }
                            }

                            //RuleSpecial
                            var ruleSpecialList = new GenericRepository<V_RuleSubProduct>().FindBy("SUBPRODUCT_ID=:id AND TYPE=:type", orderby: "SUBPRODUCT_ID", parameters: new { id, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });

                            List<dynamic> ruleDynamicSpecialList = new List<dynamic>();
                            int specialId = 1;

                            if (ruleSpecialList != null)
                            {
                                foreach (var item in ruleSpecialList)
                                {
                                    dynamic listItem = new System.Dynamic.ExpandoObject();

                                    listItem.id = specialId;
                                    listItem.ClientId = specialId;
                                    listItem.STATUS = item.STATUS;

                                    listItem.RULE_SUBPRODUCT_ID = Convert.ToString(item.RULE_SUBPRODUCT_ID);
                                    listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                                    listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                                    listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                    listItem.RULE_NAME = item.RULE_NAME;
                                    listItem.RESULT = item.RESULT;
                                    listItem.INHERITED_NAME = string.Empty;
                                    listItem.INHERITED_CODE = string.Empty;
                                    listItem.isOpen = "0";
                                    listItem.IsInherited = "1";

                                    ruleDynamicSpecialList.Add(listItem);

                                    specialId++;
                                }
                            }

                            var ruleProductSpecialList = new GenericRepository<V_RuleProduct>().FindBy("PRODUCT_ID=:PRODUCT_ID AND TYPE=:type", orderby: "PRODUCT_ID", parameters: new { PRODUCT_ID = Subproduct.PRODUCT_ID, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });
                            if (ruleProductSpecialList != null)
                            {

                                foreach (var item in ruleProductSpecialList)
                                {
                                    dynamic listItem = new System.Dynamic.ExpandoObject();

                                    listItem.id = specialId;
                                    listItem.ClientId = specialId;
                                    listItem.STATUS = item.STATUS;

                                    listItem.RULE_PRODUCT_ID = Convert.ToString(item.RULE_PRODUCT_ID);
                                    listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                                    listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                                    listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                    listItem.RULE_NAME = item.RULE_NAME;
                                    listItem.RESULT = item.RESULT;

                                    listItem.INHERITED_NAME = "ÜRÜN";
                                    listItem.INHERITED_CODE = Convert.ToString(item.PRODUCT_CODE);
                                    listItem.isOpen = "0";
                                    listItem.IsInherited = "0";

                                    ruleDynamicSpecialList.Add(listItem);

                                    specialId++;
                                }
                            }

                            ViewBag.RuleSpecialList = ruleDynamicSpecialList.ToJSON(true);


                            //RuleExtra
                            var ruleExtraList = new GenericRepository<V_RuleSubProduct>().FindBy("SUBPRODUCT_ID=:id AND TYPE=:type", orderby: "SUBPRODUCT_ID", parameters: new { id, type = new DbString { Value = ((int)RuleType.EK_PROTOKOL).ToString(), Length = 3 } });

                            List<dynamic> ruleDynamicExtraList = new List<dynamic>();
                            int extraId = 1;

                            if (ruleExtraList != null)
                            {

                                foreach (var item in ruleExtraList)
                                {
                                    dynamic listItem = new System.Dynamic.ExpandoObject();

                                    listItem.id = extraId;
                                    listItem.ClientId = extraId;
                                    listItem.STATUS = item.STATUS;

                                    listItem.RULE_SUBPRODUCT_ID = Convert.ToString(item.RULE_SUBPRODUCT_ID);
                                    listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                                    listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                                    listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                    listItem.RULE_NAME = item.RULE_NAME;
                                    listItem.RESULT = item.RESULT;
                                    listItem.isOpen = "0";

                                    ruleDynamicExtraList.Add(listItem);

                                    extraId++;
                                }
                            }

                            ViewBag.RuleExtraList = ruleDynamicExtraList.ToJSON(true);

                            var notes = new GenericRepository<V_SubProductNote>().FindBy("SUBPRODUCT_ID=:id", orderby: "SUBPRODUCT_ID", parameters: new { id });
                            if (notes != null)
                            {
                                List<dynamic> notesList = new List<dynamic>();

                                int i = 1;

                                foreach (var item in notes)
                                {
                                    dynamic listItem = new System.Dynamic.ExpandoObject();

                                    listItem.id = i;
                                    listItem.ClientId = i;
                                    listItem.STATUS = item.STATUS;

                                    listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                                    listItem.SUBPRODUCT_NOTE_ID = Convert.ToString(item.SUBPRODUCT_NOTE_ID);

                                    listItem.NOTE_TYPE = item.NOTE_TYPE;
                                    listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Note, item.NOTE_TYPE);
                                    listItem.NOTE_DESCRIPTION = item.NOTE_DESCRIPTION;
                                    listItem.isOpen = "0";

                                    notesList.Add(listItem);

                                    i++;
                                }
                                ViewBag.Notes = notesList.ToJSON();
                            }
                        }
                        ViewBag.isEdit = true;
                        break;

                    default:
                        ViewBag.Title = "Alt Ürün Ekle";
                        ViewBag.ProductId = id;

                        // Alt ürün SAGMER No ve Tescil Tarihi'ni üründen kalıtır.

                        var parameter = new GenericRepository<V_ProductParameter>().FindBy("PRODUCT_ID=:id AND SYSTEM_TYPE=:SYSTEM_TYPE AND KEY=:KEY",
                                orderby: "PARAMETER_ID",
                                parameters: new { id, SYSTEM_TYPE = new DbString { Value = ((int)SystemType.SAGMER).ToString(), Length = 3 }, KEY = new DbString { Value = "REGISTRY_DATE", Length = 30 } });
                        if (parameter != null)
                        {
                            foreach (var item in parameter)
                            {
                                ViewBag.SagmerRegistryDate = item.VALUE;
                            }
                        }

                        parameter = new GenericRepository<V_ProductParameter>().FindBy("PRODUCT_ID=:id AND SYSTEM_TYPE=:SYSTEM_TYPE AND KEY=:KEY",
                            fetchDeletedRows: true,
                            orderby: "PARAMETER_ID",
                            parameters: new { id, SYSTEM_TYPE = new DbString { Value = ((int)SystemType.SAGMER).ToString(), Length = 3 }, KEY = new DbString { Value = "NO", Length = 30 } });
                        if (parameter != null)
                        {
                            foreach (var item in parameter)
                            {
                                ViewBag.SagmerNo = item.VALUE;
                            }
                        }
                        break;
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return View(vm);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult SubproductSaveStep1(FormCollection form)
        {
            var result = new AjaxResultDto<SubproductSaveResult>();
            SubproductSaveResult step1Result = new SubproductSaveResult();

            var subproductType = form["SUBPRODUCT_TYPE"];
            var subproductName = form["SUBPRODUCT_NAME"];
            var subproductDescription = form["SUBPRODUCT_DESCRIPTION"];
            var productId = long.Parse(form["PRODUCT_ID"]);
            var code = form["SUBPRODUCT_CODE"];
            var isSbm = form["subProductIsSagmer"];
            var agreedAmount = form["agreedAmount"];

            try
            {
                var subproduct = new Subproduct
                {
                    Id = long.Parse(form["hdSubproductId"]),
                    ProductId = productId,
                    Code = code,
                    Type = subproductType,
                    Name = subproductName,
                    IsSbmTransfer = isSbm.IsNull() ? "0" : "1",
                    Description = subproductDescription
                };
                if (!string.IsNullOrEmpty(agreedAmount))
                {
                    agreedAmount = agreedAmount.Replace(',', '.');
                    decimal value;
                    if (!decimal.TryParse(agreedAmount, NumberStyles.Any, CultureInfo.InvariantCulture, out value))
                    {
                        throw new Exception("Anlaşma tutarı uygun formatta değil!");
                    }
                    subproduct.AgreedAmount = value;
                }
                var spResponseSubproduct = new SubproductRepository().Insert(subproduct);
                if (spResponseSubproduct.Code == "100")
                {
                    var subproductId = spResponseSubproduct.PkId;
                    step1Result.subproductId = subproductId;
                    step1Result.subproductCode = code;

                    var sagmerRegistryDate = form["sagmerRegistryDate"];
                    if (!String.IsNullOrEmpty(sagmerRegistryDate))
                    {
                        var parameter = new Parameter
                        {
                            Id = long.Parse(form["hdSagmerRegistryDateParameterId"]),
                            SystemType = "2",
                            Environment = "D",
                            Key = "REGISTRY_DATE",
                            Value = sagmerRegistryDate
                        };
                        var spResponseParameter = new ParameterRepository().Insert(parameter);
                        if (spResponseParameter.Code == "100")
                        {
                            var parameterId = spResponseParameter.PkId;
                            step1Result.sagmerRegistryDateParameterId = parameterId;

                            var subproductParameter = new SubproductParameter
                            {
                                Id = long.Parse(form["hdSagmerRegistryDateSubproductParameterId"]),
                                SubproductId = subproductId,
                                ParameterId = parameterId
                            };
                            var spResponseSubproductParameter = new SubproductParameterRepository().Insert(subproductParameter);
                            if (spResponseSubproductParameter.Code == "100")
                            {
                                step1Result.sagmerRegistryDateSubproductParameterId = spResponseSubproductParameter.PkId;
                            }
                        }
                    }

                    var sagmerNo = form["sagmerNo"];
                    if (!String.IsNullOrEmpty(sagmerNo))
                    {
                        var parameter = new Parameter
                        {
                            Id = long.Parse(form["hdSagmerNoParameterId"]),
                            SystemType = "2",
                            Environment = "D",
                            Key = "NO",
                            Value = sagmerNo
                        };
                        var spResponseParameter = new ParameterRepository().Insert(parameter);
                        if (spResponseParameter.Code == "100")
                        {
                            var parameterId = spResponseParameter.PkId;
                            step1Result.sagmerNoParameterId = parameterId;

                            var subproductParameter = new SubproductParameter
                            {
                                Id = long.Parse(form["hdSagmerNoSubproductParameterId"]),
                                SubproductId = subproductId,
                                ParameterId = parameterId
                            };
                            var spResponseSubproductParameter = new SubproductParameterRepository().Insert(subproductParameter);
                            if (spResponseSubproductParameter.Code == "100")
                            {
                                step1Result.sagmerNoSubproductParameterId = spResponseSubproductParameter.PkId;
                            }
                        }
                    }

                    result.Data = step1Result;
                    result.ResultCode = spResponseSubproduct.Code;
                    result.ResultMessage = spResponseSubproduct.Message;
                }
                else
                {
                    throw new Exception(spResponseSubproduct.Message);
                }
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }

            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SubproductSaveStep2(FormCollection form)
        {
            var result = new AjaxResultDto<SubproductSaveStep2Result>();
            var notes = form["hdNotes"];
            try
            {
                List<dynamic> noteList = new List<dynamic>();
                noteList = JsonConvert.DeserializeObject<List<dynamic>>(notes);
                var subproductId = form["hdSubproductId"];

                foreach (var note in noteList)
                {
                    if (note.isOpen == "1")
                    {
                        var Tnote = new Note
                        {
                            Id = note.NOTE_ID,
                            Type = note.NOTE_TYPE,
                            Description = note.NOTE_DESCRIPTION,
                            Status = note.STATUS
                        };
                        var spResponseNote = new NoteRepository().Insert(Tnote);
                        if (spResponseNote.Code == "100")
                        {
                            var noteId = spResponseNote.PkId;
                            var subproductNote = new SubproductNote
                            {
                                Id = note.SUBPRODUCT_NOTE_ID,
                                SubproductId = long.Parse(subproductId),
                                NoteId = noteId,
                                Status = note.STATUS
                            };
                            var spResponseSubproductNote = new SubproductNoteRepository().Insert(subproductNote);
                            if (spResponseSubproductNote.Code == "100")
                            {
                                result.ResultCode = spResponseSubproductNote.Code;
                                result.ResultMessage = spResponseSubproductNote.Message;
                            }
                            else
                            {
                                throw new Exception(spResponseSubproductNote.Message);
                            }
                        }
                        else
                        {
                            throw new Exception(spResponseNote.Message);
                        }
                    }
                }

                var notess = new GenericRepository<V_SubProductNote>().FindBy("SUBPRODUCT_ID=:id", orderby: "SUBPRODUCT_ID", parameters: new { id = subproductId });
                if (notess != null)
                {
                    List<dynamic> notesList = new List<dynamic>();
                    var codeOfSubproduct = "";

                    int i = 1;

                    foreach (var item in notess)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                        listItem.SUBPRODUCT_NOTE_ID = Convert.ToString(item.SUBPRODUCT_NOTE_ID);

                        listItem.NOTE_TYPE = Convert.ToString(item.NOTE_TYPE);
                        listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Note, item.NOTE_TYPE);
                        listItem.NOTE_DESCRIPTION = item.NOTE_DESCRIPTION;
                        listItem.isOpen = "0";

                        if (codeOfSubproduct.Equals(""))
                        {
                            codeOfSubproduct = item.SUBPRODUCT_CODE;
                        }

                        notesList.Add(listItem);

                        i++;
                    }

                    result.Data = new SubproductSaveStep2Result
                    {
                        subproductCode = codeOfSubproduct,
                        jsonData = notesList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SubproductSaveStep3(FormCollection form)
        {
            var result = new AjaxResultDto<SubproductSaveStep3Result>();
            var medias = form["hdMedias"];
            try
            {
                List<dynamic> mediaList = new List<dynamic>();
                mediaList = JsonConvert.DeserializeObject<List<dynamic>>(medias);
                var subproductId = form["hdSubproductId"];

                foreach (var media in mediaList)
                {
                    if (media.isOpen == "1")
                    {
                        var Tmedia = new Media
                        {
                            Id = media.MEDIA_ID,
                            Name = media.MEDIA_NAME,
                            FileName = "TEST",
                            Status = media.STATUS
                        };
                        var spResponseMedia = new MediaRepository().Insert(Tmedia);
                        if (spResponseMedia.Code == "100")
                        {
                            var mediaId = spResponseMedia.PkId;
                            var subproductMedia = new SubproductMedia
                            {
                                Id = media.SUBPRODUCT_MEDIA_ID,
                                SubproductId = long.Parse(subproductId),
                                MediaId = mediaId,
                                Status = media.STATUS
                            };
                            var spResponseSubproductMedia = new SubproductMediaRepository().Insert(subproductMedia);
                            if (spResponseSubproductMedia.Code == "100")
                            {
                                result.ResultCode = spResponseSubproductMedia.Code;
                                result.ResultMessage = spResponseSubproductMedia.Message;
                            }
                            else
                            {
                                throw new Exception(spResponseSubproductMedia.Message);
                            }
                        }
                        else
                        {
                            throw new Exception(spResponseMedia.Message);
                        }
                    }
                }

                var mediass = new GenericRepository<V_SubproductMedia>().FindBy("SUBPRODUCT_ID=:id", orderby: "SUBPRODUCT_ID", parameters: new { id = subproductId });
                if (mediass != null)
                {
                    List<dynamic> mediasList = new List<dynamic>();
                    var codeOfSubproduct = "";

                    int i = 1;

                    foreach (var item in mediass)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.Status;

                        listItem.MEDIA_ID = Convert.ToString(item.MediaId);
                        listItem.SUBPRODUCT_MEDIA_ID = Convert.ToString(item.SubproductMediaId);

                        listItem.MEDIA_NAME = item.MediaName;
                        listItem.MEDIA_PATH = item.FileName;
                        listItem.isOpen = "0";

                        if (codeOfSubproduct.Equals(""))
                        {
                            codeOfSubproduct = item.SubproductCode;
                        }

                        mediasList.Add(listItem);

                        i++;
                    }

                    result.Data = new SubproductSaveStep3Result
                    {
                        subproductCode = codeOfSubproduct,
                        jsonData = mediasList.ToJSON()
                    };
                }

            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SubproductSaveStep4(FormCollection form)
        {
            var result = new AjaxResultDto<ProductSaveStep4Result>();
            ProductSaveStep4Result step1Result = new ProductSaveStep4Result();
            var rules = form["hdRuleSpecial"];
            try
            {
                var subproductId = form["hdSubproductId"];
                var productId = form["hdProductId"];
                List<dynamic> ruleList = new List<dynamic>();
                ruleList = JsonConvert.DeserializeObject<List<dynamic>>(rules);
                foreach (var rule in ruleList)
                {
                    if (rule.isOpen == "1")
                    {
                        if (rule.STATUS == "0")
                        {
                            string idList = string.Empty;

                            if (rule.chkRuleSelectedId != null)
                            {
                                idList = rule.chkRuleSelectedId.ToString();

                                foreach (var ruleId in idList.Replace("\r", "").Replace("\n", "").Replace("[", string.Empty).Replace("]", "").Replace("\"", "").Replace(" ", "").Split(','))
                                {
                                    var TruleSubProduct = new RuleSubproduct
                                    {
                                        Id = rule.RULE_SUBPRODUCT_ID,
                                        SubproductId = long.Parse(subproductId),
                                        RuleId = long.Parse(ruleId),
                                        Status = Convert.ToString(rule.STATUS),
                                        Type = Convert.ToString(rule.RULE_TYPE)
                                    };
                                    var spResponseRuleProduct = new GenericRepository<RuleSubproduct>().Insert(TruleSubProduct);
                                    if (spResponseRuleProduct.Code == "100")
                                    {
                                        result.ResultCode = spResponseRuleProduct.Code;
                                        result.ResultMessage = spResponseRuleProduct.Message;
                                    }
                                    else
                                    {
                                        throw new Exception(spResponseRuleProduct.Message);
                                    }
                                }
                            }
                            else
                            {
                                idList = rule.chkRuleGroupSelectedId.ToString();

                                foreach (var ruleGroupId in idList.Replace("\r", "").Replace("\n", "").Replace("[", string.Empty).Replace("]", "").Replace("\"", "").Replace(" ", "").Split(','))
                                {
                                    List<RuleGroupRule> ruleGroupRuleList = new GenericRepository<RuleGroupRule>().FindBy($"RULE_GROUP_ID={ruleGroupId}");
                                    if (ruleGroupRuleList.Any())
                                    {
                                        foreach (var ruleGroupRule in ruleGroupRuleList)
                                        {
                                            var TruleSubProduct = new RuleSubproduct
                                            {
                                                Id = rule.RULE_SUBPRODUCT_ID,
                                                SubproductId = long.Parse(subproductId),
                                                RuleId = ruleGroupRule.RuleId,
                                                RuleGroupId = ruleGroupRule.RuleGroupId,
                                                Status = Convert.ToString(rule.STATUS),
                                                Type = Convert.ToString(rule.RULE_TYPE)
                                            };
                                            var spResponseRuleProduct = new GenericRepository<RuleSubproduct>().Insert(TruleSubProduct);
                                            if (spResponseRuleProduct.Code == "100")
                                            {
                                                result.ResultCode = spResponseRuleProduct.Code;
                                                result.ResultMessage = spResponseRuleProduct.Message;
                                            }
                                            else
                                            {
                                                throw new Exception(spResponseRuleProduct.Message);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            RuleSubproduct ruleProduct = new GenericRepository<RuleSubproduct>().FindById(long.Parse(Convert.ToString(rule.RULE_SUBPRODUCT_ID)));
                            if (ruleProduct != null)
                            {
                                ruleProduct.Status = ((int)Status.SILINDI).ToString();
                                var spResponseRuleProduct = new GenericRepository<RuleSubproduct>().Update(ruleProduct);
                                if (spResponseRuleProduct.Code == "100")
                                {
                                    result.ResultCode = spResponseRuleProduct.Code;
                                    result.ResultMessage = spResponseRuleProduct.Message;
                                }
                                else
                                {
                                    throw new Exception(spResponseRuleProduct.Message);
                                }
                            }
                        }
                    }
                }

                var ruleSpecialList = new GenericRepository<V_RuleSubProduct>().FindBy("SUBPRODUCT_ID=:id AND TYPE=:type", orderby: "SUBPRODUCT_ID", parameters: new { id = subproductId, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });

                List<dynamic> ruleDynamicSpecialList = new List<dynamic>();
                int specialId = 1;

                if (ruleSpecialList != null)
                {
                    foreach (var item in ruleSpecialList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = specialId;
                        listItem.ClientId = specialId;
                        listItem.STATUS = item.STATUS;

                        listItem.RULE_SUBPRODUCT_ID = Convert.ToString(item.RULE_SUBPRODUCT_ID);
                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                        listItem.RULE_NAME = item.RULE_NAME;
                        listItem.RESULT = item.RESULT;
                        listItem.INHERITED_NAME = string.Empty;
                        listItem.INHERITED_CODE = string.Empty;
                        listItem.isOpen = "0";
                        listItem.IsInherited = "1";

                        ruleDynamicSpecialList.Add(listItem);

                        specialId++;
                    }
                }

                var ruleProductSpecialList = new GenericRepository<V_RuleProduct>().FindBy("PRODUCT_ID=:PRODUCT_ID AND TYPE=:type", orderby: "PRODUCT_ID", parameters: new { PRODUCT_ID = productId, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });
                if (ruleProductSpecialList != null)
                {

                    foreach (var item in ruleProductSpecialList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = specialId;
                        listItem.ClientId = specialId;
                        listItem.STATUS = item.STATUS;

                        listItem.RULE_PRODUCT_ID = Convert.ToString(item.RULE_PRODUCT_ID);
                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                        listItem.RULE_NAME = item.RULE_NAME;
                        listItem.RESULT = item.RESULT;

                        listItem.INHERITED_NAME = "ÜRÜN";
                        listItem.INHERITED_CODE = Convert.ToString(item.PRODUCT_CODE);
                        listItem.isOpen = "0";
                        listItem.IsInherited = "0";

                        ruleDynamicSpecialList.Add(listItem);

                        specialId++;
                    }
                }
                result.Data = new ProductSaveStep4Result
                {
                    jsonData = ruleDynamicSpecialList.ToJSON()
                };
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SubproductSaveStep5(FormCollection form)
        {
            var result = new AjaxResultDto<ProductSaveStep4Result>();
            ProductSaveStep4Result step1Result = new ProductSaveStep4Result();
            var rules = form["hdRuleExtra"];
            try
            {
                var subproductId = form["hdSubproductId"];
                List<dynamic> ruleList = new List<dynamic>();
                ruleList = JsonConvert.DeserializeObject<List<dynamic>>(rules);
                foreach (var rule in ruleList)
                {
                    if (rule.isOpen == "1")
                    {
                        if (rule.STATUS == "0")
                        {
                            string idList = string.Empty;

                            if (rule.chkRuleSelectedId != null)
                            {
                                idList = rule.chkRuleSelectedId.ToString();

                                foreach (var ruleId in idList.Replace("\r", "").Replace("\n", "").Replace("[", string.Empty).Replace("]", "").Replace("\"", "").Replace(" ", "").Split(','))
                                {
                                    var TruleSubProduct = new RuleSubproduct
                                    {
                                        Id = rule.RULE_SUBPRODUCT_ID,
                                        SubproductId = long.Parse(subproductId),
                                        RuleId = long.Parse(ruleId),
                                        Status = Convert.ToString(rule.STATUS),
                                        Type = Convert.ToString(rule.RULE_TYPE)
                                    };
                                    var spResponseRuleProduct = new GenericRepository<RuleSubproduct>().Insert(TruleSubProduct);
                                    if (spResponseRuleProduct.Code == "100")
                                    {
                                        result.ResultCode = spResponseRuleProduct.Code;
                                        result.ResultMessage = spResponseRuleProduct.Message;
                                    }
                                    else
                                    {
                                        throw new Exception(spResponseRuleProduct.Message);
                                    }
                                }
                            }
                            else
                            {
                                idList = rule.chkRuleGroupSelectedId.ToString();

                                foreach (var ruleGroupId in idList.Replace("\r", "").Replace("\n", "").Replace("[", string.Empty).Replace("]", "").Replace("\"", "").Replace(" ", "").Split(','))
                                {
                                    List<RuleGroupRule> ruleGroupRuleList = new GenericRepository<RuleGroupRule>().FindBy($"RULE_GROUP_ID={ruleGroupId}");
                                    if (ruleGroupRuleList.Any())
                                    {
                                        foreach (var ruleGroupRule in ruleGroupRuleList)
                                        {
                                            var TruleSubProduct = new RuleSubproduct
                                            {
                                                Id = rule.RULE_SUBPRODUCT_ID,
                                                SubproductId = long.Parse(subproductId),
                                                RuleId = ruleGroupRule.RuleId,
                                                RuleGroupId = ruleGroupRule.RuleGroupId,
                                                Status = Convert.ToString(rule.STATUS),
                                                Type = Convert.ToString(rule.RULE_TYPE)
                                            };
                                            var spResponseRuleProduct = new GenericRepository<RuleSubproduct>().Insert(TruleSubProduct);
                                            if (spResponseRuleProduct.Code == "100")
                                            {
                                                result.ResultCode = spResponseRuleProduct.Code;
                                                result.ResultMessage = spResponseRuleProduct.Message;
                                            }
                                            else
                                            {
                                                throw new Exception(spResponseRuleProduct.Message);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            RuleSubproduct ruleProduct = new GenericRepository<RuleSubproduct>().FindById(long.Parse(Convert.ToString(rule.RULE_SUBPRODUCT_ID)));
                            if (ruleProduct != null)
                            {
                                ruleProduct.Status = ((int)Status.SILINDI).ToString();
                                var spResponseRuleProduct = new GenericRepository<RuleSubproduct>().Update(ruleProduct);
                                if (spResponseRuleProduct.Code == "100")
                                {
                                    result.ResultCode = spResponseRuleProduct.Code;
                                    result.ResultMessage = spResponseRuleProduct.Message;
                                }
                                else
                                {
                                    throw new Exception(spResponseRuleProduct.Message);
                                }
                            }
                        }
                    }
                }

                var ruless = new GenericRepository<V_RuleSubProduct>().FindBy("SUBPRODUCT_ID=:id AND TYPE=:type", orderby: "SUBPRODUCT_ID", parameters: new { id = subproductId, type = new DbString { Value = ((int)RuleType.EK_PROTOKOL).ToString(), Length = 3 } });
                if (ruless != null)
                {
                    List<dynamic> rulesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in ruless)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.RULE_SUBPRODUCT_ID = Convert.ToString(item.RULE_SUBPRODUCT_ID);
                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                        listItem.RULE_NAME = item.RULE_NAME;
                        listItem.RESULT = item.RESULT;
                        listItem.isOpen = "0";

                        rulesList.Add(listItem);

                        i++;
                    }

                    result.Data = new ProductSaveStep4Result
                    {
                        jsonData = rulesList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [LoginControl]
        public ActionResult SubproductDelete(Int64 id)
        {
            long productId = 0;
            try
            {
                var repo = new SubproductRepository();
                var result = repo.FindById(id);
                if (result != null)
                {
                    productId = result.ProductId;
                    result.Status = "1";
                    var spResponse = repo.Update(result);
                    if (spResponse.Code == "100")
                        TempData["Alert"] = $"swAlert('Silme İşlemi Başarılı','{result.Name} başarıyla silindi.','success')";
                    else
                        throw new Exception(spResponse.Code + " : " + spResponse.Message);
                }
                else
                    TempData["Alert"] = $"swAlert('Silme İşlemi Başarısız','Silinecek Alt Ürün Bulunamadı.','info')";
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Subproduct/" + productId);
        }

        #endregion

        #region Plan
        [LoginControl]
        public ActionResult Plan(Int64 Id = 0)
        {
            if (Id != 0)
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                //Fill data

                var result = new GenericRepository<V_Plan>().FindBy(conditions: "SUBPRODUCT_ID=:id",
                                             orderby: "PLAN_ID",
                                             parameters: new { id = Id });
                ViewBag.Plans = result;
                ViewBag.SubProductId = Id;
                return View();
            }
            else
            {
                return RedirectToAction("Product");
            }
        }

        [LoginControl]
        public ActionResult PlanDelete(Int64 id)
        {
            var repo = new PlanRepository();
            var result = repo.FindById(id);
            try
            {

                if (result != null)
                {
                    result.Status = "1";
                    var spResponse = repo.Update(result);
                    if (spResponse.Code == "100")
                        TempData["Alert"] = $"swAlert('Silme İşlemi Başarılı','{result.Name} başarıyla silindi.','success')";
                    else
                        throw new Exception(spResponse.Code + " : " + spResponse.Message);
                }
                else
                    TempData["Alert"] = $"swAlert('Silme İşlemi Başarısız','Silinecek Plan Bulunamadı.','info')";
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Plan/" + result.SubproductId);
        }

        [LoginControl]
        public ActionResult PlanClone(Int64 Id = 0)
        {
            var oldPlan = new PlanRepository().FindById(Id);
            var subProductId = oldPlan.SubproductId;

            try
            {

                if (Id != 0)
                {
                    #region Plan Clone
                    if (oldPlan != null)
                    {
                        long newPlanId = 0;
                        #region Clone Plan Details                        
                        Plan plan = new Plan
                        {
                            Id = 0,
                            Name = oldPlan.Name + "-Kopya",
                            SubproductId = oldPlan.SubproductId,
                            Status = oldPlan.Status
                        };
                        var spResponsePlan = new PlanRepository().Insert(plan);
                        if (spResponsePlan.Code != "100")
                        {
                            throw new Exception(spResponsePlan.Code + " : " + spResponsePlan.Message);
                        }
                        newPlanId = spResponsePlan.PkId;
                        plan.Id = newPlanId;
                        #endregion

                        var planCoverageParentList = new PlanCoverageRepository().FindBy("PLAN_ID=:id AND PARENT_ID IS NULL", parameters: new { id = oldPlan.Id });
                        if (planCoverageParentList != null)
                        {
                            foreach (var planCoverageParent in planCoverageParentList)
                            {
                                #region Clone Plan Coverage
                                if (planCoverageParent.Id > 0)
                                {
                                    CloneHelper.PlanCoverageClone(planCoverageParent.Id, newPlanId, null);
                                }
                                #endregion
                            }
                        }
                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";

            }
            return RedirectToAction("Plan/" + subProductId);
        }

        [LoginControl]
        public ActionResult PlanForm(Int64 id = 0, string formaction = "")
        {

            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                if (id == 0)
                    return RedirectToAction("Product");


                var CurrencyTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Currency);
                ViewBag.CurrencyTypeList = CurrencyTypeList;

                var AgreementTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Agreement);
                ViewBag.AgreementTypeList = AgreementTypeList;

                var ExemptionTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Exemption, true);
                ViewBag.ExemptionTypeList = ExemptionTypeList;

                var NetworkList = new NetworkRepository().FindBy(orderby: "NAME");
                ViewBag.NetworkList = NetworkList;

                var LocationTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Location);
                ViewBag.LocationTypeList = LocationTypeList;

                ViewBag.CoverageTypeList = LookupHelper.GetLookupData(LookupTypes.Coverage);

                ViewBag.SubproductId = id;
                ViewBag.isEdit = false;

                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Plan Güncelle";
                        if (id != 0)
                        {
                            var Plan = new GenericRepository<V_Plan>().FindBy("PLAN_ID=:id", orderby: "PLAN_ID", parameters: new { id }).FirstOrDefault();
                            if (Plan != null)
                            {
                                ViewBag.Plan = Plan;
                                ViewBag.SubproductId = Plan.SUBPRODUCT_ID;

                                var CoverageListt = new GenericRepository<V_Coverage>().FindBy("BRANCH_ID=:BRANCH_ID", orderby: "COVERAGE_NAME", parameters: new { Plan.BRANCH_ID });
                                ViewBag.CoverageList = CoverageListt;

                                var planId = Plan.PLAN_ID;
                                if (!string.IsNullOrEmpty(planId.ToString()))
                                {
                                    var PlanCoverageList = new GenericRepository<V_PlanCoverage>().FindBy("PLAN_ID =:planId AND PACKAGE_ID IS NULL", orderby: "PLAN_COVERAGE_ID ASC", parameters: new { planId });

                                    if (PlanCoverageList != null && PlanCoverageList.Count > 0)
                                    {
                                        List<dynamic> planCoveragesList = new List<dynamic>();

                                        int i = 1;

                                        foreach (var item in PlanCoverageList)
                                        {
                                            dynamic listItem = new System.Dynamic.ExpandoObject();

                                            listItem.id = i;
                                            listItem.ClientId = i;
                                            listItem.STATUS = item.STATUS;

                                            listItem.PLAN_COVERAGE_ID = Convert.ToString(item.PLAN_COVERAGE_ID);
                                            listItem.COVERAGE_ID = Convert.ToString(item.COVERAGE_ID);
                                            listItem.NETWORK = Convert.ToString(item.NETWORK_ID);

                                            listItem.PARENT_ID = string.IsNullOrEmpty(Convert.ToString(item.PARENT_ID)) ? "-1" : Convert.ToString(item.PARENT_ID);
                                            listItem.PLAN_COVERAGE_NAME = item.PLAN_COVERAGE_NAME;
                                            listItem.LOCATION_TYPE = item.LOCATION_TYPE;
                                            listItem.COVERAGE_IDSelectedText = item.COVERAGE_NAME;
                                            listItem.NETWORKSelectedText = item.NETWORK_NAME;
                                            listItem.isOpen = "0";

                                            planCoveragesList.Add(listItem);

                                            i++;
                                        }
                                        ViewBag.PlanCoverage = planCoveragesList.ToJSON();
                                    }

                                    var CoverageVariation = new GenericRepository<V_PlanCoverageVariation>().FindBy($"PLAN_ID=:planId AND AGR_NEW_VERSION_ID IS NULL AND (AGR_STATUS IS NULL OR AGR_STATUS!=:STATUS) AND EMG_NEW_VERSION_ID IS NULL AND (EMG_STATUS IS NULL OR EMG_STATUS!=:STATUS) AND (NAG_STATUS IS NULL OR NAG_STATUS!=:STATUS) AND NAG_NEW_VERSION_ID IS NULL",
                                                            orderby: "PLAN_COVERAGE_ID",
                                                            parameters: new { planId, STATUS = new DbString { Value = "1", Length = 3 } },
                                                            fetchHistoricRows: true,
                                                            fetchDeletedRows: true);

                                    if (CoverageVariation != null && CoverageVariation.Count > 0)
                                    {
                                        List<dynamic> listContractedVariation = new List<dynamic>();
                                        List<dynamic> listNonAggrementVariation = new List<dynamic>();
                                        List<dynamic> listUrgentVariation = new List<dynamic>();
                                        var list = new List<List<dynamic>>();

                                        foreach (var variation in CoverageVariation)
                                        {
                                            //AGR
                                            dynamic variationListItem = new System.Dynamic.ExpandoObject();

                                            if (variation.AGR_AGREEMENT_TYPE != null)
                                            {
                                                variationListItem.Type = "AGR";
                                                variationListItem.AGR_ID = variation.AGR_ID;
                                                variationListItem.PLAN_COVERAGE_ID = variation.PLAN_COVERAGE_ID;
                                                variationListItem.AGR_AGREEMENT_TYPE = variation.AGR_AGREEMENT_TYPE;
                                                variationListItem.AGR_PRICE_LIMIT = variation.AGR_PRICE_LIMIT;
                                                variationListItem.AGR_PRICE_FACTOR = variation.AGR_PRICE_FACTOR;
                                                variationListItem.AGR_CURRENCY_ID = variation.AGR_CURRENCY_ID;
                                                variationListItem.AGR_NUMBER_LIMIT = variation.AGR_NUMBER_LIMIT;
                                                variationListItem.AGR_DAY_LIMIT = variation.AGR_DAY_LIMIT;
                                                variationListItem.AGR_COINSURANCE_RATIO = variation.AGR_COINSURANCE_RATIO;
                                                variationListItem.AGR_EXEMPTION_TYPE = variation.AGR_EXEMPTION_TYPE;
                                                variationListItem.AGR_EXEMPTION_LIMIT = variation.AGR_EXEMPTION_LIMIT;
                                                variationListItem.AGR_SESSION_COUNT = variation.AGR_SESSION_COUNT;

                                                listContractedVariation.Add(variationListItem);
                                            }
                                            //NAG
                                            variationListItem = new System.Dynamic.ExpandoObject();

                                            if (variation.NAG_AGREEMENT_TYPE != null)
                                            {
                                                variationListItem.Type = "NAG";
                                                variationListItem.NAG_ID = variation.NAG_ID;
                                                variationListItem.PLAN_COVERAGE_ID = variation.PLAN_COVERAGE_ID;
                                                variationListItem.NAG_AGREEMENT_TYPE = variation.NAG_AGREEMENT_TYPE;
                                                variationListItem.NAG_PRICE_LIMIT = variation.NAG_PRICE_LIMIT;
                                                variationListItem.NAG_PRICE_FACTOR = variation.NAG_PRICE_FACTOR;
                                                variationListItem.NAG_CURRENCY_ID = variation.NAG_CURRENCY_ID;
                                                variationListItem.NAG_NUMBER_LIMIT = variation.NAG_NUMBER_LIMIT;
                                                variationListItem.NAG_DAY_LIMIT = variation.NAG_DAY_LIMIT;
                                                variationListItem.NAG_COINSURANCE_RATIO = variation.NAG_COINSURANCE_RATIO;
                                                variationListItem.NAG_EXEMPTION_TYPE = variation.NAG_EXEMPTION_TYPE;
                                                variationListItem.NAG_EXEMPTION_LIMIT = variation.NAG_EXEMPTION_LIMIT;
                                                variationListItem.NAG_SESSION_COUNT = variation.NAG_SESSION_COUNT;

                                                listNonAggrementVariation.Add(variationListItem);
                                            }
                                            //EMG
                                            variationListItem = new System.Dynamic.ExpandoObject();

                                            if (variation.EMG_AGREEMENT_TYPE != null)
                                            {
                                                variationListItem.Type = "EMG";
                                                variationListItem.EMG_ID = variation.EMG_ID;
                                                variationListItem.PLAN_COVERAGE_ID = variation.PLAN_COVERAGE_ID;
                                                variationListItem.EMG_AGREEMENT_TYPE = variation.EMG_AGREEMENT_TYPE;
                                                variationListItem.EMG_PRICE_LIMIT = variation.EMG_PRICE_LIMIT;
                                                variationListItem.EMG_PRICE_FACTOR = variation.EMG_PRICE_FACTOR;
                                                variationListItem.EMG_CURRENCY_ID = variation.EMG_CURRENCY_ID;
                                                variationListItem.EMG_NUMBER_LIMIT = variation.EMG_NUMBER_LIMIT;
                                                variationListItem.EMG_DAY_LIMIT = variation.EMG_DAY_LIMIT;
                                                variationListItem.EMG_COINSURANCE_RATIO = variation.EMG_COINSURANCE_RATIO;
                                                variationListItem.EMG_EXEMPTION_TYPE = variation.EMG_EXEMPTION_TYPE;
                                                variationListItem.EMG_EXEMPTION_LIMIT = variation.EMG_EXEMPTION_LIMIT;
                                                variationListItem.EMG_SESSION_COUNT = variation.EMG_SESSION_COUNT;

                                                listUrgentVariation.Add(variationListItem);
                                            }
                                        }
                                        ViewBag.ContractedVariation = listContractedVariation.ToJSON();
                                        ViewBag.NonAgreementVariation = listNonAggrementVariation.ToJSON();
                                        ViewBag.UrgentVariation = listUrgentVariation.ToJSON();
                                    }
                                }
                            }
                            ViewBag.isEdit = true;
                        }
                        break;

                    default:
                        var result = new GenericRepository<V_SubProduct>().FindBy($"SUBPRODUCT_ID=:id", orderby: "SUBPRODUCT_ID", parameters: new { id }).FirstOrDefault();

                        var CoverageList = new GenericRepository<V_Coverage>().FindBy("BRANCH_ID=:BRANCH_ID", orderby: "COVERAGE_NAME", parameters: new { result.BRANCH_ID });
                        ViewBag.CoverageList = CoverageList;

                        ViewBag.Title = "Plan Ekle";
                        ViewBag.SubproductId = id;
                        ViewBag.isEdit = false;
                        break;
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult CoverageNetworkSave(FormCollection form)
        {
            var result = "[]";
            var PlanCoverages = form["hdTreeData"];
            var planId = string.IsNullOrEmpty(form["hdPlanId"]) ? 0 : long.Parse(form["hdPlanId"]);
            try
            {
                if (planId != 0)
                {
                    List<dynamic> planCoverageList = new List<dynamic>();
                    planCoverageList = JsonConvert.DeserializeObject<List<dynamic>>(PlanCoverages);

                    foreach (var planCoverage in planCoverageList)
                    {
                        if (planCoverage.isOpen == "1")
                        {
                            var TplanCoverage = new PlanCoverage
                            {
                                Id = planCoverage.PLAN_COVERAGE_ID,
                                PlanId = planId,
                                CoverageId = planCoverage.COVERAGE_ID,
                                CoverageName = planCoverage.PLAN_COVERAGE_NAME,
                                LocationType = planCoverage.LOCATION_TYPE,
                                NetworkId = planCoverage.NETWORK,
                                Status = planCoverage.STATUS
                            };
                            if (planCoverage.PARENT_ID != 0 && planCoverage.PARENT_ID != -1)
                            {
                                TplanCoverage.ParentId = planCoverage.PARENT_ID;
                            }
                            var spResponsePlanCoverage = new PlanCoverageRepository().Insert(TplanCoverage);
                            if (spResponsePlanCoverage.Code == "100")
                            {
                                var planCoverageId = spResponsePlanCoverage.PkId;
                                if (Convert.ToString(planCoverage.STATUS) == ((int)Status.SILINDI).ToString())
                                {
                                    //Sub Level Plan Coverage and Variation Delete
                                    var subPlanCoverage = new PlanCoverageRepository().FindBy($"PARENT_ID={planCoverageId}");
                                    foreach (var item in subPlanCoverage)
                                    {
                                        item.Status = ((int)Status.SILINDI).ToString();
                                        spResponsePlanCoverage = new PlanCoverageRepository().Update(item);
                                        if (spResponsePlanCoverage.Code == "100")
                                        {
                                            var subPlanCoverageVariation = new PlanCoverageVariationRepository().FindBy($"PLAN_COVERAGE_ID={spResponsePlanCoverage.PkId}");
                                            foreach (var vari in subPlanCoverageVariation)
                                            {
                                                vari.Status = ((int)Status.SILINDI).ToString();
                                                var spResponsePlanCoverageVar = new PlanCoverageVariationRepository().Update(vari);
                                                if (spResponsePlanCoverageVar.Code != "100")
                                                {
                                                    throw new Exception(spResponsePlanCoverageVar.Code + " : " + spResponsePlanCoverageVar.Message);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            throw new Exception(spResponsePlanCoverage.Code + " : " + spResponsePlanCoverage.Message);
                                        }
                                    }

                                    var subPlanCoverageVar = new PlanCoverageVariationRepository().FindBy($"PLAN_COVERAGE_ID={planCoverageId}");
                                    foreach (var vari in subPlanCoverageVar)
                                    {
                                        vari.Status = ((int)Status.SILINDI).ToString();
                                        var spResponsePlanCoverageVar = new PlanCoverageVariationRepository().Update(vari);
                                        if (spResponsePlanCoverageVar.Code != "100")
                                        {
                                            throw new Exception(spResponsePlanCoverageVar.Code + " : " + spResponsePlanCoverageVar.Message);
                                        }
                                    }
                                    /////---------------//////
                                }
                                else if (planCoverage.PLAN_COVERAGE_ID != "0" && TplanCoverage.ParentId == null)
                                {
                                    var subPlanCoverage = new PlanCoverageRepository().FindBy($"PARENT_ID={planCoverageId}");
                                    foreach (var item in subPlanCoverage)
                                    {
                                        item.NetworkId = planCoverage.NETWORK;
                                        item.LocationType = planCoverage.LOCATION_TYPE;
                                        spResponsePlanCoverage = new PlanCoverageRepository().Update(item);
                                        if (spResponsePlanCoverage.Code != "100")
                                        {
                                            throw new Exception(spResponsePlanCoverage.Code + " : " + spResponsePlanCoverage.Message);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                throw new Exception(spResponsePlanCoverage.Code + " : " + spResponsePlanCoverage.Message);
                            }
                        }
                    }

                    var PlanCoverageList = new GenericRepository<V_PlanCoverage>().FindBy("PLAN_ID = :planId AND PACKAGE_ID IS NULL",
                                                              orderby: "PLAN_COVERAGE_ID ASC", parameters: new { planId });

                    if (PlanCoverageList != null && PlanCoverageList.Count > 0)
                    {
                        List<dynamic> planCoveragesList = new List<dynamic>();

                        int i = 1;

                        foreach (var item in PlanCoverageList)
                        {
                            dynamic listItem = new System.Dynamic.ExpandoObject();

                            listItem.id = i;
                            listItem.ClientId = i;
                            listItem.STATUS = item.STATUS;

                            listItem.PLAN_COVERAGE_ID = Convert.ToString(item.PLAN_COVERAGE_ID);
                            listItem.COVERAGE_ID = Convert.ToString(item.COVERAGE_ID);
                            listItem.NETWORK = Convert.ToString(item.NETWORK_ID);

                            listItem.PARENT_ID = string.IsNullOrEmpty(Convert.ToString(item.PARENT_ID)) ? "-1" : Convert.ToString(item.PARENT_ID);
                            listItem.PLAN_COVERAGE_NAME = item.PLAN_COVERAGE_NAME;
                            listItem.LOCATION_TYPE = item.LOCATION_TYPE;
                            listItem.COVERAGE_IDSelectedText = item.COVERAGE_NAME;
                            listItem.NETWORKSelectedText = item.NETWORK_NAME;
                            listItem.isOpen = "0";

                            planCoveragesList.Add(listItem);

                            i++;
                        }
                        result = planCoveragesList.ToJSON();
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        [LoginControl]
        public ActionResult VariationSave(FormCollection form)
        {
            var result = "[]";

            try
            {
                var planCoverageId = long.Parse(form["selectedPlanCoverage"]);
                var isSubClone = Request["isSubClone"];
                var planId = long.Parse(form["hdPlanId"]);
                var type = Request["Type"];

                GenericRepository<PlanCoverageVariation> repoVar = new GenericRepository<PlanCoverageVariation>();

                if (type == "AGR")
                {
                    string cID = form["AGR_ID"];
                    string cAGREEMENT_TYPE = form["AGR_AGREEMENT_TYPE"];
                    string cPRICE_LIMIT = form["AGR_PRICE_LIMIT"];
                    string cPRICE_FACTOR = form["AGR_PRICE_FACTOR"].Replace('.', ',');
                    string cCURRENCY_TYPE = form["AGR_CURRENCY_ID"];
                    string cNUMBER_LIMIT = form["AGR_NUMBER_LIMIT"];
                    string cDAY_LIMIT = form["AGR_DAY_LIMIT"];
                    string cCOINSURANCE_RATIO = form["AGR_COINSURANCE_RATIO"].Replace('.', ',');
                    string cEXEMPTION_TYPE = form["AGR_EXEMPTION_TYPE"];
                    string cEXEMPTION_LIMIT = form["AGR_EXEMPTION_LIMIT"].Replace('.', ',');
                    string cSESSION_COUNT = form["AGR_SESSION_COUNT"];


                    var TplanCoverageVariation = new PlanCoverageVariation
                    {
                        Id = string.IsNullOrEmpty(cID) ? 0 : long.Parse(cID),
                        Type = ((int)PlanCoverageVariationType.ANLASMALI_KURUM).ToString(),
                        PlanCoverageId = planCoverageId,
                        AgreementType = cAGREEMENT_TYPE,
                        PriceLimit = !cPRICE_LIMIT.IsNumeric() ? null : (decimal?)decimal.Parse(cPRICE_LIMIT),
                        PriceFactor = !cPRICE_FACTOR.IsNumeric() ? null : (decimal?)decimal.Parse(cPRICE_FACTOR),
                        NumberLimit = !cNUMBER_LIMIT.IsInt() ? null : (int?)int.Parse(cNUMBER_LIMIT),
                        DayLimit = !cDAY_LIMIT.IsInt() ? null : (int?)int.Parse(cDAY_LIMIT),
                        CoinsuranceRatio = !cCOINSURANCE_RATIO.IsInt() ? null : (int?)int.Parse(cCOINSURANCE_RATIO),
                        ExemptionType = cEXEMPTION_TYPE,
                        ExemptionLimit = !cEXEMPTION_LIMIT.IsInt() ? null : (int?)int.Parse(cEXEMPTION_LIMIT),
                        CurrencyType = cCURRENCY_TYPE,
                        SessionCount = !cSESSION_COUNT.IsInt() ? null : (int?)int.Parse(cSESSION_COUNT)
                    };

                    var spResponseVariation = repoVar.UpdateForAll(TplanCoverageVariation);
                    if (spResponseVariation.Code == "100")
                    {
                        if (form["checkNAG"] == "1")
                        {
                            TplanCoverageVariation.Id = !form["NAG_ID"].IsInt64() ? 0 : long.Parse(form["NAG_ID"]);
                            TplanCoverageVariation.Type = ((int)PlanCoverageVariationType.ANLASMASIZ_KURUM).ToString();
                            spResponseVariation = repoVar.UpdateForAll(TplanCoverageVariation);
                            if (spResponseVariation.Code != "100")
                                throw new Exception(spResponseVariation.Code + " : " + spResponseVariation.Message);
                        }
                        if (form["checkEMG"] == "1")
                        {
                            TplanCoverageVariation.Id = !form["EMG_ID"].IsInt64() ? 0 : long.Parse(form["EMG_ID"]);
                            TplanCoverageVariation.Type = ((int)PlanCoverageVariationType.ACIL).ToString();
                            spResponseVariation = repoVar.UpdateForAll(TplanCoverageVariation);
                            if (spResponseVariation.Code != "100")
                                throw new Exception(spResponseVariation.Code + " : " + spResponseVariation.Message);
                        }
                    }
                    else
                    {
                        throw new Exception(spResponseVariation.Code + " : " + spResponseVariation.Message);
                    }
                }

                else if (type == "NAG")
                {
                    string nID = form["NAG_ID"];
                    string nAGREEMENT_TYPE = form["NAG_AGREEMENT_TYPE"];
                    string nPRICE_LIMIT = form["NAG_PRICE_LIMIT"];
                    string nPRICE_FACTOR = form["NAG_PRICE_FACTOR"].Replace('.', ',');
                    string nCURRENCY_TYPE = form["NAG_CURRENCY_ID"];
                    string nNUMBER_LIMIT = form["NAG_NUMBER_LIMIT"];
                    string nDAY_LIMIT = form["NAG_DAY_LIMIT"];
                    string nCOINSURANCE_RATIO = form["NAG_COINSURANCE_RATIO"].Replace('.', ',');
                    string nEXEMPTION_TYPE = form["NAG_EXEMPTION_TYPE"];
                    string nEXEMPTION_LIMIT = form["NAG_EXEMPTION_LIMIT"];
                    string nSESSION_COUNT = form["NAG_SESSION_COUNT"];

                    var TplanCoverageVariation = new PlanCoverageVariation
                    {
                        Id = !nID.IsInt64() ? 0 : long.Parse(nID),
                        Type = ((int)PlanCoverageVariationType.ANLASMASIZ_KURUM).ToString(),
                        PlanCoverageId = planCoverageId,
                        AgreementType = nAGREEMENT_TYPE,
                        PriceLimit = !nPRICE_LIMIT.IsNumeric() ? null : (decimal?)decimal.Parse(nPRICE_LIMIT),
                        PriceFactor = !nPRICE_FACTOR.IsNumeric() ? null : (decimal?)decimal.Parse(nPRICE_FACTOR),
                        NumberLimit = !nNUMBER_LIMIT.IsInt() ? null : (int?)int.Parse(nNUMBER_LIMIT),
                        DayLimit = !nDAY_LIMIT.IsInt() ? null : (int?)int.Parse(nDAY_LIMIT),
                        CoinsuranceRatio = !nCOINSURANCE_RATIO.IsInt() ? null : (int?)int.Parse(nCOINSURANCE_RATIO),
                        ExemptionType = nEXEMPTION_TYPE,
                        ExemptionLimit = !nEXEMPTION_LIMIT.IsInt() ? null : (int?)int.Parse(nEXEMPTION_LIMIT),
                        CurrencyType = nCURRENCY_TYPE,
                        SessionCount = !nSESSION_COUNT.IsInt() ? null : (int?)int.Parse(nSESSION_COUNT)

                    };
                    var spResponseVariation = repoVar.UpdateForAll(TplanCoverageVariation);
                    if (spResponseVariation.Code != "100")
                    {
                        throw new Exception(spResponseVariation.Code + " : " + spResponseVariation.Message);
                    }
                }

                else if (type == "EMG")
                {
                    string uID = form["EMG_ID"];
                    string uAGREEMENT_TYPE = form["EMG_AGREEMENT_TYPE"];
                    string uPRICE_LIMIT = form["EMG_PRICE_LIMIT"];
                    string uPRICE_FACTOR = form["EMG_PRICE_FACTOR"].Replace('.', ',');
                    string uCURRENCY_TYPE = form["EMG_CURRENCY_ID"];
                    string uNUMBER_LIMIT = form["EMG_NUMBER_LIMIT"];
                    string uDAY_LIMIT = form["EMG_DAY_LIMIT"];
                    string uCOINSURANCE_RATIO = form["EMG_COINSURANCE_RATIO"].Replace('.', ',');
                    string uEXEMPTION_TYPE = form["EMG_EXEMPTION_TYPE"];
                    string uEXEMPTION_LIMIT = form["EMG_EXEMPTION_LIMIT"];
                    string uSESSION_COUNT = form["EMG_SESSION_COUNT"];


                    var TplanCoverageVariation = new PlanCoverageVariation
                    {
                        Id = !uID.IsInt64() ? 0 : long.Parse(uID),
                        Type = ((int)PlanCoverageVariationType.ACIL).ToString(),
                        PlanCoverageId = planCoverageId,
                        AgreementType = uAGREEMENT_TYPE,
                        PriceLimit = !uPRICE_LIMIT.IsNumeric() ? null : (decimal?)decimal.Parse(uPRICE_LIMIT),
                        PriceFactor = !uPRICE_FACTOR.IsNumeric() ? null : (decimal?)decimal.Parse(uPRICE_FACTOR),
                        NumberLimit = !uPRICE_FACTOR.IsInt() ? null : (int?)int.Parse(uPRICE_FACTOR),
                        DayLimit = !uPRICE_FACTOR.IsInt() ? null : (int?)int.Parse(uPRICE_FACTOR),
                        CoinsuranceRatio = !uPRICE_FACTOR.IsInt() ? null : (int?)int.Parse(uPRICE_FACTOR),
                        ExemptionType = uEXEMPTION_TYPE,
                        ExemptionLimit = !uPRICE_FACTOR.IsInt() ? null : (int?)int.Parse(uPRICE_FACTOR),
                        CurrencyType = uCURRENCY_TYPE,
                        SessionCount = !uSESSION_COUNT.IsInt() ? null : (int?)int.Parse(uSESSION_COUNT)
                    };
                    var spResponseVariation = repoVar.UpdateForAll(TplanCoverageVariation);
                    if (spResponseVariation.Code != "100")
                    {
                        throw new Exception(spResponseVariation.Code + " : " + spResponseVariation.Message);
                    }
                }

                if (isSubClone == "1")
                {
                    List<PlanCoverageVariation> planCoverageVariationList = repoVar.FindBy($"PLAN_COVERAGE_ID={planCoverageId}", orderby: "ID DESC");
                    if (planCoverageVariationList != null && planCoverageVariationList.Count > 0)
                    {
                        List<PlanCoverage> planCoverageList = new GenericRepository<PlanCoverage>().FindBy($"PARENT_ID={planCoverageId}");
                        if (planCoverageList != null && planCoverageList.Count > 0)
                        {
                            var subPlanCoverageIdList = planCoverageList.Select(pc => pc.Id).ToList();
                            var subPlanCoverageIds = string.Join(",", subPlanCoverageIdList.ToArray());

                            var subPlanCovVar = repoVar.FindBy($"PLAN_COVERAGE_ID IN ({subPlanCoverageIds})", orderby: "ID DESC");
                            //subPlanCovVar = subPlanCovVar.Concat(sp => sp.Status = ((int)Status.SILINDI).ToString()).ToList();

                            //foreach (var item in subPlanCovVar)
                            //{
                            //    item.Status = ((int)Status.SILINDI).ToString();
                            //    repoVariation.Update(item);
                            //}

                            SpResponse spResponse = new SpResponse();

                            foreach (var item in planCoverageList)
                            {
                                var rootAgrVariation = planCoverageVariationList.Where(pcv => pcv.Type == ((int)PlanCoverageVariationType.ANLASMALI_KURUM).ToString() && pcv.Status == ((int)Status.AKTIF).ToString().ToString()).FirstOrDefault();
                                var rootNagVariation = planCoverageVariationList.Where(pcv => pcv.Type == ((int)PlanCoverageVariationType.ANLASMASIZ_KURUM).ToString() && pcv.Status == ((int)Status.AKTIF).ToString().ToString()).FirstOrDefault();
                                var rootEmgVariation = planCoverageVariationList.Where(pcv => pcv.Type == ((int)PlanCoverageVariationType.ACIL).ToString() && pcv.Status == ((int)Status.AKTIF).ToString().ToString()).FirstOrDefault();


                                if (rootAgrVariation != null)
                                {
                                    var oldAgrId = subPlanCovVar.Where(spv => spv.PlanCoverageId == item.Id && spv.Type == ((int)PlanCoverageVariationType.ANLASMALI_KURUM).ToString() && spv.Status == ((int)Status.AKTIF).ToString().ToString()).FirstOrDefault()?.Id;
                                    rootAgrVariation.Id = oldAgrId == null ? 0 : (long)oldAgrId;
                                    rootAgrVariation.NewVersionId = null;
                                    rootAgrVariation.PlanCoverageId = item.Id;

                                    spResponse = repoVar.Insert(rootAgrVariation);
                                    if (spResponse.Code != "100")
                                    {
                                        throw new Exception(spResponse.Code + " : " + spResponse.Message);
                                    }
                                }
                                if (rootNagVariation != null)
                                {
                                    var oldNagId = subPlanCovVar.Where(spv => spv.PlanCoverageId == item.Id && spv.Type == ((int)PlanCoverageVariationType.ANLASMASIZ_KURUM).ToString() && spv.Status == ((int)Status.AKTIF).ToString().ToString()).FirstOrDefault()?.Id;
                                    rootAgrVariation.Id = oldNagId == null ? 0 : (long)oldNagId;
                                    rootNagVariation.Id = 0;
                                    rootNagVariation.NewVersionId = null;

                                    rootNagVariation.PlanCoverageId = item.Id;

                                    spResponse = repoVar.Insert(rootNagVariation);
                                    if (spResponse.Code != "100")
                                    {
                                        throw new Exception(spResponse.Code + " : " + spResponse.Message);
                                    }
                                }
                                if (rootEmgVariation != null)
                                {
                                    var oldEmgId = subPlanCovVar.Where(spv => spv.PlanCoverageId == item.Id && spv.Type == ((int)PlanCoverageVariationType.ACIL).ToString() && spv.Status == ((int)Status.AKTIF).ToString().ToString()).FirstOrDefault()?.Id;
                                    rootAgrVariation.Id = oldEmgId == null ? 0 : (long)oldEmgId;
                                    rootEmgVariation.Id = 0;
                                    rootEmgVariation.NewVersionId = null;

                                    rootEmgVariation.PlanCoverageId = item.Id;

                                    spResponse = repoVar.Insert(rootEmgVariation);
                                    if (spResponse.Code != "100")
                                    {
                                        throw new Exception(spResponse.Code + " : " + spResponse.Message);
                                    }
                                }
                            }
                        }
                    }
                }


                var CoverageVariation = new GenericRepository<V_PlanCoverageVariation>().FindBy(
                    "PLAN_ID=:planId AND AGR_NEW_VERSION_ID IS NULL AND (AGR_STATUS IS NULL OR AGR_STATUS!=:STATUS) AND EMG_NEW_VERSION_ID IS NULL AND (EMG_STATUS IS NULL OR EMG_STATUS!=:STATUS) AND (NAG_STATUS IS NULL OR NAG_STATUS!=:STATUS) AND NAG_NEW_VERSION_ID IS NULL", orderby: "PLAN_ID", parameters: new { planId, STATUS = new DbString { Value = ((int)Status.SILINDI).ToString(), Length = 3 } });

                if (CoverageVariation != null && CoverageVariation.Count > 0)
                {
                    List<dynamic> listContractedVariation = new List<dynamic>();
                    List<dynamic> listNonAggrementVariation = new List<dynamic>();
                    List<dynamic> listUrgentVariation = new List<dynamic>();
                    var list = new List<List<dynamic>>();

                    foreach (var variation in CoverageVariation)
                    {
                        //AGR
                        dynamic variationListItem = new System.Dynamic.ExpandoObject();

                        if (variation.AGR_AGREEMENT_TYPE != null)
                        {
                            variationListItem.Type = "AGR";
                            variationListItem.AGR_ID = variation.AGR_ID;
                            variationListItem.PLAN_COVERAGE_ID = variation.PLAN_COVERAGE_ID;
                            variationListItem.AGR_AGREEMENT_TYPE = variation.AGR_AGREEMENT_TYPE;
                            variationListItem.AGR_PRICE_LIMIT = variation.AGR_PRICE_LIMIT;
                            variationListItem.AGR_PRICE_FACTOR = variation.AGR_PRICE_FACTOR;
                            variationListItem.AGR_CURRENCY_ID = variation.AGR_CURRENCY_ID;
                            variationListItem.AGR_NUMBER_LIMIT = variation.AGR_NUMBER_LIMIT;
                            variationListItem.AGR_DAY_LIMIT = variation.AGR_DAY_LIMIT;
                            variationListItem.AGR_COINSURANCE_RATIO = variation.AGR_COINSURANCE_RATIO;
                            variationListItem.AGR_EXEMPTION_TYPE = variation.AGR_EXEMPTION_TYPE;
                            variationListItem.AGR_EXEMPTION_LIMIT = variation.AGR_EXEMPTION_LIMIT;
                            variationListItem.AGR_SESSION_COUNT = variation.AGR_SESSION_COUNT;

                            listContractedVariation.Add(variationListItem);
                        }
                        //NAG
                        variationListItem = new System.Dynamic.ExpandoObject();

                        if (variation.NAG_AGREEMENT_TYPE != null)
                        {
                            variationListItem.Type = "NAG";
                            variationListItem.NAG_ID = variation.NAG_ID;
                            variationListItem.PLAN_COVERAGE_ID = variation.PLAN_COVERAGE_ID;
                            variationListItem.NAG_AGREEMENT_TYPE = variation.NAG_AGREEMENT_TYPE;
                            variationListItem.NAG_PRICE_LIMIT = variation.NAG_PRICE_LIMIT;
                            variationListItem.NAG_PRICE_FACTOR = variation.NAG_PRICE_FACTOR;
                            variationListItem.NAG_CURRENCY_ID = variation.NAG_CURRENCY_ID;
                            variationListItem.NAG_NUMBER_LIMIT = variation.NAG_NUMBER_LIMIT;
                            variationListItem.NAG_DAY_LIMIT = variation.NAG_DAY_LIMIT;
                            variationListItem.NAG_COINSURANCE_RATIO = variation.NAG_COINSURANCE_RATIO;
                            variationListItem.NAG_EXEMPTION_TYPE = variation.NAG_EXEMPTION_TYPE;
                            variationListItem.NAG_EXEMPTION_LIMIT = variation.NAG_EXEMPTION_LIMIT;
                            variationListItem.NAG_SESSION_COUNT = variation.NAG_SESSION_COUNT;

                            listNonAggrementVariation.Add(variationListItem);
                        }
                        //EMG
                        variationListItem = new System.Dynamic.ExpandoObject();

                        if (variation.EMG_AGREEMENT_TYPE != null)
                        {
                            variationListItem.Type = "EMG";
                            variationListItem.EMG_ID = variation.EMG_ID;
                            variationListItem.PLAN_COVERAGE_ID = variation.PLAN_COVERAGE_ID;
                            variationListItem.EMG_AGREEMENT_TYPE = variation.EMG_AGREEMENT_TYPE;
                            variationListItem.EMG_PRICE_LIMIT = variation.EMG_PRICE_LIMIT;
                            variationListItem.EMG_PRICE_FACTOR = variation.EMG_PRICE_FACTOR;
                            variationListItem.EMG_CURRENCY_ID = variation.EMG_CURRENCY_ID;
                            variationListItem.EMG_NUMBER_LIMIT = variation.EMG_NUMBER_LIMIT;
                            variationListItem.EMG_DAY_LIMIT = variation.EMG_DAY_LIMIT;
                            variationListItem.EMG_COINSURANCE_RATIO = variation.EMG_COINSURANCE_RATIO;
                            variationListItem.EMG_EXEMPTION_TYPE = variation.EMG_EXEMPTION_TYPE;
                            variationListItem.EMG_EXEMPTION_LIMIT = variation.EMG_EXEMPTION_LIMIT;
                            variationListItem.EMG_SESSION_COUNT = variation.EMG_SESSION_COUNT;

                            listUrgentVariation.Add(variationListItem);
                        }
                    }
                    list.Add(listContractedVariation);
                    list.Add(listNonAggrementVariation);
                    list.Add(listUrgentVariation);

                    result = list.ToJSON();
                }
            }
            catch (Exception ex)
            {
                result = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [LoginControl]
        public ActionResult PlanSave(FormCollection form)
        {
            var result = "[]";
            try
            {
                var name = form["planName"];
                var planId = long.Parse(form["hdPlanId"]);
                var subproductId = long.Parse(form["hdSubproductId"]);
                var Tplan = new Plan
                {
                    Id = planId,
                    Name = name,
                    SubproductId = subproductId
                };
                var spResponsePlan = new PlanRepository().Insert(Tplan);
                if (spResponsePlan.Code != "100")
                {
                    throw new Exception(spResponsePlan.Code + " : " + spResponsePlan.Message);
                }
                else
                {
                    var list = new List<dynamic>();
                    dynamic rootListItem = new System.Dynamic.ExpandoObject();

                    rootListItem.PLAN_ID = spResponsePlan.PkId;
                    rootListItem.PLAN_NAME = name;

                    list.Add(rootListItem);
                    result = list.ToJSON();
                }
            }
            catch (Exception ex)
            {
                result = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Coverage

        [LoginControl]
        // GET: Coverage List /Product/Coverage
        public ActionResult Coverage()
        {
            try
            {
                //Durum - DropDown
                var v_coverage = LookupHelper.GetLookupData(Constants.LookupTypes.Coverage, showAll: true);
                ViewBag.v_coverage = v_coverage;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return View();
        }

        // GET: Coverage Filter /Product/CoverageFilter
        [HttpPost]
        [LoginControl]
        public ActionResult GetCoverageList(FormCollection form)
        {
            try
            {
                int start = Convert.ToInt32(Request["start"]);
                int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
                string searchValue = Request["search[value]"];
                string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
                string sortDirection = Request["order[0][dir]"];

                string whereConditition = !String.IsNullOrEmpty(form["covarageType"]) ? $"COVERAGE_TYPE = '{form["covarageType"]}' " : "";
                whereConditition += !String.IsNullOrEmpty(form["covarageName"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : "AND") + $"COVERAGE_NAME LIKE '%{form["covarageName"]}%'" : "";

                var Coverages = new GenericRepository<V_Coverage>().FindByPaged(conditions: whereConditition,
                                                            orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},COVERAGE_ID ASC" : "",
                                                            pageNumber: start,
                                                            rowsPerPage: length);

                List<CoverageList> CoverageList = new List<CoverageList>();

                if (Coverages.Data != null)
                {
                    foreach (var item in Coverages.Data)
                    {
                        var listItem = new CoverageList
                        {
                            COVERAGE_ID = Convert.ToString(item.COVERAGE_ID),
                            COVERAGE_NAME = Convert.ToString(item.COVERAGE_NAME),
                            COVERAGE_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Coverage, item.COVERAGE_TYPE)
                        };

                        CoverageList.Add(listItem);
                    }
                }

                return Json(new { data = CoverageList, draw = Request["draw"], recordsTotal = Coverages.TotalItemsCount, recordsFiltered = Coverages.TotalItemsCount }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return RedirectToAction("Coverage");
        }

        // GET: Coverage Delete /Product/CoverageDelete
        [LoginControl]
        public ActionResult CoverageDelete(Int64 id)
        {
            try
            {
                var repo = new CoverageRepository();
                var result = repo.FindById(id);
                result.Status = "1";
                var spResponse = repo.Update(result);
                if (spResponse.Code == "100")
                    TempData["Alert"] = $"swAlert('Silme İşlemi Başarılı','{result.Name} başarıyla silindi.','success')";
                else
                    throw new Exception(spResponse.Code + " : " + spResponse.Message);
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Coverage");
        }

        [LoginControl]
        public ActionResult CoverageClone(Int64 Id = 0)
        {
            try
            {
                if (Id != 0)
                {
                    #region Coverage Clone

                    var oldCoverage = new CoverageRepository().FindById(Id);
                    if (oldCoverage != null)
                    {
                        long newCoverageId = 0;
                        if (oldCoverage.Id > 0)
                        {
                            newCoverageId = CloneHelper.CoverageClone(oldCoverage.Id);
                        }
                        if (newCoverageId > 0)
                        {
                            #region Clone Process of Coverage

                            var coverageProcessList = new ProcessCoverageRepository().FindBy("COVERAGE_ID=:id", parameters: new { id = oldCoverage.Id });
                            foreach (var coverageProcess in coverageProcessList)
                            {
                                long coverageProcessId = 0;
                                if (coverageProcess.Id > 0)
                                {
                                    coverageProcessId = CloneHelper.CoverageProcessClone(coverageProcess.Id, newCoverageId);
                                }
                            }
                            #endregion

                            #region Clone Parameters of Coverage

                            var coverageParameterList = new CoverageParameterRepository().FindBy("COVERAGE_ID=:id", parameters: new { id = oldCoverage.Id });

                            foreach (var coverageParameter in coverageParameterList)
                            {
                                long parameterId = 0;
                                if (coverageParameter.Id > 0)
                                {
                                    parameterId = CloneHelper.ParameterClone(coverageParameter.ParameterId);
                                }
                                if (parameterId > 0)
                                {
                                    #region Clone Coverage Parameter Reletion
                                    long coverageParameterId = 0;
                                    if (coverageParameter.ParameterId > 0)
                                    {
                                        coverageParameterId = CloneHelper.CoverageParameterClone(coverageParameter.Id, newCoverageId, parameterId);
                                    }

                                    #endregion
                                }
                            }
                            #endregion
                        }

                    }


                    #endregion

                    TempData["Alert"] = $"swAlert('İşlem Başarılı','Teminat başarıyla kopyalandı.','success')";
                }
            }


            catch (Exception)
            {

                throw;
            }
            return RedirectToAction("Coverage");
        }

        [HttpPost]
        public JsonResult GetProcesses()
        {
            List<dynamic> listProcess = new List<dynamic>();

            try
            {
                var processId = Request["processId"];
                var coverageId = Request["coverageId"];

                var processes = new GenericRepository<ProteinEntities.Process>().FindBy($"PROCESS_LIST_ID = {processId}", orderby: "NAME"/*, parameters: new { processListId = processId }*/);
                List<ProcessCoverage> ProcessCoverageList = new GenericRepository<ProcessCoverage>().FindBy($"COVERAGE_ID = {coverageId}", orderby: "");
                int i = 1;

                foreach (var item in processes)
                {
                    dynamic listItem = new System.Dynamic.ExpandoObject();

                    listItem.id = i;
                    listItem.ClientId = i;
                    listItem.STATUS = item.Status;

                    listItem.PROCESS_GROUP_PROCESS_ID = "0";
                    listItem.PROCESS_ID = Convert.ToString(item.Id);
                    listItem.PROCESS_LIST_ID = Convert.ToString(item.ProcessListId);

                    listItem.PARENT_ID = Convert.ToString(item.ParentId);
                    listItem.CODE = item.Code;
                    listItem.NAME = item.Name;
                    listItem.isChecked = "";

                    foreach (var ProcessCoverage in ProcessCoverageList)
                    {
                        if (item.Id == ProcessCoverage.ProcessId)
                        {
                            listItem.isChecked = "checked";
                        }
                    }
                    //listItem.AMOUNT = Convert.ToString(item.Amount);

                    listProcess.Add(listItem);

                    i++;
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = listProcess.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            //return Json(listProcess.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteCoverageParameter(Int64 parameterId, Int64 coverageParameterId)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                if (parameterId < 1)
                {
                    throw new Exception("Parametre Bilgisi Bulunamadı.");
                }

                GenericRepository<Parameter> parameterRepository = new GenericRepository<Parameter>();
                GenericRepository<CoverageParameter> coverageParameterRepository = new GenericRepository<CoverageParameter>();

                var Tparameter = parameterRepository.FindById(parameterId);
                if (Tparameter == null)
                {
                    throw new Exception("Parametre Bilgisi Bulunamadı.");
                }

                Tparameter.Status = ((int)Status.SILINDI).ToString();
                var spResponseParameter = parameterRepository.Insert(Tparameter);
                if (spResponseParameter.Code != "100")
                {

                }

                var TCoverageparameter = coverageParameterRepository.FindById(coverageParameterId);
                if (TCoverageparameter == null)
                {
                    throw new Exception("Parametre Bilgisi Bulunamadı.");
                }

                TCoverageparameter.Status = ((int)Status.SILINDI).ToString();
                var spResponseCoverageParameter = coverageParameterRepository.Insert(TCoverageparameter);
                if (spResponseCoverageParameter.Code != "100")
                {

                }

                result.ResultCode = "100";

                var parameters = new GenericRepository<V_CoverageParameter>().FindBy("COVERAGE_ID=:id", orderby: "COVERAGE_ID", parameters: new { id = TCoverageparameter.CoverageId });
                if (parameters != null)
                {
                    List<dynamic> parameterList = new List<dynamic>();
                    int i = 1;

                    foreach (var item in parameters)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.Status;

                        listItem.PARAMETER_ID = Convert.ToString(item.ParameterId);
                        listItem.COVERAGE_PARAMETER_ID = Convert.ToString(item.CoverageParameterId);
                        listItem.SYSTEM_TYPE = item.SystemType;
                        listItem.SYSTEM_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.System, item.SystemType);

                        listItem.KEY = item.Key;
                        listItem.VALUE = item.Value;

                        parameterList.Add(listItem);

                        i++;
                    }
                    result.Data = new JSONResult
                    {
                        jsonData = parameterList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }

            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveCoverageParameter(FormCollection form)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                long coverageId = form["COVERAGE_ID"].IsInt64() ? long.Parse(form["COVERAGE_ID"]) : throw new Exception("Teminat Bilgisi Bulunamadı.");
                long parameterId = form["PARAMETER_ID"].IsInt64() ? long.Parse(form["PARAMETER_ID"]) : 0;
                string systemType = form["SYSTEM_TYPE"].IsInt64() ? form["SYSTEM_TYPE"] : throw new Exception("Sistem Tipi Seçilmesi Gerekmektedir.");
                string key = !form["KEY"].IsNull() ? form["KEY"] : throw new Exception("Alan Girilmesi Gerekmektedir.");
                string value = !form["VALUE"].IsNull() ? form["VALUE"] : throw new Exception("Değer Girilmesi Gerekmektedir.");

                var Tparameter = new Parameter
                {
                    Id = parameterId,
                    SystemType = systemType,
                    Key = key,
                    Value = value
                };
                var spResponseParameter = new GenericRepository<Parameter>().Insert(Tparameter);
                if (spResponseParameter.Code == "100")
                {
                    if (parameterId < 1)
                    {
                        var TcoverageParameter = new CoverageParameter
                        {
                            CoverageId = coverageId,
                            ParameterId = spResponseParameter.PkId,
                        };
                        var spResponseCoverageParameter = new GenericRepository<CoverageParameter>().Insert(TcoverageParameter);
                        if (spResponseCoverageParameter.Code != "100")
                        {
                            throw new Exception(spResponseCoverageParameter.Code + " : " + spResponseCoverageParameter.Message);
                        }
                    }
                }
                else
                {
                    throw new Exception(spResponseParameter.Code + " : " + spResponseParameter.Message);
                }

                result.ResultCode = "100";

                var parameters = new GenericRepository<V_CoverageParameter>().FindBy("COVERAGE_ID=:coverageId", orderby: "COVERAGE_ID", parameters: new { coverageId });
                if (parameters != null)
                {
                    List<dynamic> parameterList = new List<dynamic>();
                    int i = 1;

                    foreach (var item in parameters)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.Status;

                        listItem.PARAMETER_ID = Convert.ToString(item.ParameterId);
                        listItem.COVERAGE_PARAMETER_ID = Convert.ToString(item.CoverageParameterId);
                        listItem.SYSTEM_TYPE = item.SystemType;
                        listItem.SYSTEM_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.System, item.SystemType);

                        listItem.KEY = item.Key;
                        listItem.VALUE = item.Value;

                        parameterList.Add(listItem);

                        i++;
                    }
                    result.Data = new JSONResult
                    {
                        jsonData = parameterList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }

            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddProcess(string processId)
        {
            object result = null;
            try
            {
                var status = Request["Status"];
                if (!string.IsNullOrEmpty(processId))
                {
                    var Process = new GenericRepository<V_Process>().FindBy("PROCESS_ID =: processId", orderby: "PROCESS_ID", parameters: new { processId });
                    List<dynamic> listProcess = new List<dynamic>();
                    if (Process != null)
                    {
                        int i = 1;

                        foreach (var item in Process)
                        {
                            dynamic listItem = new System.Dynamic.ExpandoObject();

                            listItem.id = i;
                            listItem.STATUS = status;

                            listItem.PROCESS_COVERAGE_ID = "0";
                            listItem.PROCESS_LIST_ID = Convert.ToString(item.PROCESS_LIST_ID);
                            listItem.PROCESS_ID = Convert.ToString(item.PROCESS_ID);

                            listProcess.Add(listItem);

                            i++;
                        }
                    }
                    return new JsonResult()
                    {
                        MaxJsonLength = Int32.MaxValue,
                        Data = listProcess.ToJSON(),
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // GET: Coverage Form /Product/CoverageForm
        [LoginControl]
        public ActionResult CoverageForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                var LocationTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Location);
                ViewBag.LocationTypeList = LocationTypeList;

                var CoverageTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Coverage);
                ViewBag.CoverageTypeList = CoverageTypeList;

                var SystemTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.System);
                ViewBag.SystemTypeList = SystemTypeList;

                var BranchList = new BranchRepository().FindBy("PARENT_ID IS NULL");
                ViewBag.BranchList = BranchList;

                var ProcessList = new ProcessListRepository().FindBy(conditions: $"TYPE!={((int)ProcessListType.TANI)}", orderby: "NAME");
                ViewBag.ProcessList = ProcessList;

                ViewBag.CoverageId = 0;
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Teminat Güncelle";
                        if (id != 0)
                        {

                            var Coverage = new GenericRepository<V_Coverage>().FindBy("COVERAGE_ID=:covarageId", orderby: "COVERAGE_ID", parameters: new { covarageId = id }).FirstOrDefault();
                            if (Coverage != null)
                            {
                                ViewBag.Coverage = Coverage;

                                var parameters = new GenericRepository<V_CoverageParameter>().FindBy("COVERAGE_ID=:id", orderby: "COVERAGE_ID", parameters: new { id });
                                if (parameters != null)
                                {
                                    List<dynamic> parameterList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in parameters)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.Status;

                                        listItem.PARAMETER_ID = Convert.ToString(item.ParameterId);
                                        listItem.COVERAGE_PARAMETER_ID = Convert.ToString(item.CoverageParameterId);
                                        listItem.SYSTEM_TYPE = item.SystemType;
                                        listItem.SYSTEM_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.System, item.SystemType);

                                        listItem.KEY = item.Key;
                                        listItem.VALUE = item.Value;

                                        parameterList.Add(listItem);

                                        i++;
                                    }
                                    ViewBag.Parameters = parameterList.ToJSON(true);
                                }

                                //var processs = new GenericRepository<V_ProcessCoverage>().FindBy("COVERAGE_ID=:id", orderby: "COVERAGE_ID", parameters: new { id });
                                //if (processs != null)
                                //{
                                //    List<dynamic> processList = new List<dynamic>();

                                //    int i = 1;

                                //    foreach (var item in processs)
                                //    {
                                //        dynamic listItem = new System.Dynamic.ExpandoObject();

                                //        listItem.id = i;
                                //        listItem.STATUS = item.STATUS;

                                //        listItem.PROCESS_COVERAGE_ID = Convert.ToString(item.PROCESS_COVERAGE_ID);
                                //        listItem.PROCESS_LIST_ID = Convert.ToString(item.PROCESS_LIST_ID);
                                //        listItem.PROCESS_ID = Convert.ToString(item.PROCESS_ID);

                                //        processList.Add(listItem);
                                //        i++;
                                //    }
                                //    ViewBag.Processs = processList.ToJSON();
                                //}
                                ViewBag.isEdit = true;
                            }
                        }
                        break;

                    default:
                        ViewBag.Title = "Teminat Ekle";
                        ViewBag.isEdit = false;
                        break;
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult CoverageSave(FormCollection form)
        {
            try
            {
                var coverageId = long.Parse(form["hdCOVERAGE_ID"]);
                var branchId = long.Parse(form["coverageBranch"]);
                var type = form["COVERAGE_TYPE"];
                var name = form["COVERAGE_NAME"];
                var locationType = form["LOCATION_TYPE"];
                var isTax = form["IS_TAX"];

                if (coverageId == 0)
                {
                    var IsUniqueCoverage = new CoverageRepository().FindBy($"NAME='{name}' AND TYPE={type} AND BRANCH_ID={branchId} " + (string.IsNullOrEmpty(locationType) ? "AND LOCATION_TYPE IS NULL" : $"AND LOCATION_TYPE={locationType}")).FirstOrDefault();
                    if (IsUniqueCoverage != null)
                    {
                        throw new Exception("Aynı Özelliklerde Bir Başka Teminat Kayıtlı, Lütfen Teminat Bilgilerini Değiştirerek Tekrar Deneyiniz.");
                    }
                }

                var coverage = new Coverage
                {
                    Id = coverageId,
                    Name = name,
                    BranchId = branchId,
                    Type = type,
                    LocationType = locationType,
                    IsTax = isTax ?? "0"
                };
                var spResponseCoverage = new CoverageRepository().Insert(coverage);
                if (spResponseCoverage.Code == "100")
                {
                    coverageId = spResponseCoverage.PkId;
                    //var parameters = form["hdCoverageParameters"];
                    //List<dynamic> parameterList = new List<dynamic>();
                    //parameterList = JsonConvert.DeserializeObject<List<dynamic>>(parameters);
                    //foreach (var parameter in parameterList)
                    //{
                    //    if ((parameter.STATUS == "1" && parameter.PARAMETER_ID != 0) || parameter.STATUS == "0")
                    //    {
                    //        var Tparameter = new Parameter
                    //        {
                    //            Id = parameter.PARAMETER_ID,
                    //            SystemType = parameter.SYSTEM_TYPE,
                    //            Key = parameter.KEY,
                    //            Value = parameter.VALUE,
                    //            Status = parameter.STATUS
                    //        };
                    //        var spResponseParameter = new ParameterRepository().Insert(Tparameter);
                    //        if (spResponseParameter.Code == "100")
                    //        {
                    //            var TcoverageParameter = new CoverageParameter
                    //            {
                    //                Id = parameter.COVERAGE_PARAMETER_ID,
                    //                CoverageId = coverageId,
                    //                ParameterId = spResponseParameter.PkId,
                    //                Status = parameter.STATUS,
                    //            };
                    //            var spResponseCoverageParameter = new CoverageParameterRepository().Insert(TcoverageParameter);
                    //            if (spResponseCoverageParameter.Code != "100")
                    //            {
                    //                throw new Exception(spResponseCoverageParameter.Code + " : " + spResponseCoverageParameter.Message);
                    //            }
                    //        }
                    //        else
                    //        {
                    //            throw new Exception(spResponseParameter.Code + " : " + spResponseParameter.Message);
                    //        }
                    //    }
                    //}

                    //ProcessCoverage
                    var processs = form["hdProcess"];
                    List<dynamic> processList = new List<dynamic>();
                    processList = JsonConvert.DeserializeObject<List<dynamic>>(processs);
                    List<ProcessCoverage> ProcessCoverageList = new List<ProcessCoverage>();
                    if (processList != null)
                    {
                        foreach (var process in processList)
                        {
                            if (process != null && ((process.STATUS == "1" && process.PROCESS_COVERAGE_ID != 0) || process.STATUS == "0"))
                            {
                                var TprocessCoverage = new ProcessCoverage
                                {
                                    Id = process.PROCESS_COVERAGE_ID,
                                    ProcessId = process.PROCESS_ID,
                                    CoverageId = coverageId,
                                    Status = process.STATUS
                                };
                                ProcessCoverageList.Add(TprocessCoverage);
                            }
                        }
                        var spResponseProcessList = new GenericRepository<ProcessCoverage>().InsertSpExecute3(ProcessCoverageList);
                        foreach (var spResponseProcess in spResponseProcessList)
                        {
                            if (spResponseProcess.Code != "100")
                            {
                                throw new Exception(spResponseProcess.Code + " : " + spResponseProcess.Message);
                            }
                        }
                    }
                    TempData["Alert"] = $"swAlert('İşlemi Başarılı','{name} başarıyla kaydedildi.','success')";
                }
                else
                {
                    throw new Exception(spResponseCoverage.Code + " : " + spResponseCoverage.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Coverage");
        }
        #endregion

        #region Package

        // GET: Package List /Product/Package
        [LoginControl]
        public ActionResult Package()
        {
            ExportVM vm = new ExportVM();
            try
            {
                vm.ViewName = "V_EXPORT_PACKAGE";
                vm.SetExportColumns();
                vm.ExportType = ExportType.Pdf;
                FillCompanies();
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                //var ProductList = new ProductRepository().FindBy();
                //ViewBag.ProductList = ProductList;
            }

            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View(vm);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetPackageList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            string whereconditition = !String.IsNullOrEmpty(form["company"]) ? $" COMPANY_ID='{form["company"]}' " : "";
            whereconditition += !String.IsNullOrEmpty(form["product"]) ? (string.IsNullOrEmpty(whereconditition) ? "" : "AND") + $" PRODUCT_ID = '{form["product"]}' " : "";
            whereconditition += !String.IsNullOrEmpty(form["subproduct"]) ? (string.IsNullOrEmpty(whereconditition) ? "" : "AND") + $" SUBPRODUCT_ID = '{form["subproduct"]}' " : "";
            whereconditition += !String.IsNullOrEmpty(form["packageNo"]) ? (string.IsNullOrEmpty(whereconditition) ? "" : "AND") + $" PACKAGE_NO = '{form["packageNo"]}' " : "";
            whereconditition += !String.IsNullOrEmpty(form["packageName"]) ? (string.IsNullOrEmpty(whereconditition) ? "" : "AND") + $" PACKAGE_NAME LIKE '%{form["packageName"]}%' " : "";

            var Packages = new GenericRepository<V_Package>().FindByPaged(conditions: whereconditition,
                                                    orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection}" : "PACKAGE_ID",
                                                    pageNumber: start,
                                                    rowsPerPage: length);

            List<PackageList> PackageList = new List<PackageList>();

            if (Packages.Data != null)
            {
                foreach (var item in Packages.Data)
                {
                    PackageList listItem = new PackageList();

                    listItem.PACKAGE_ID = Convert.ToString(item.PACKAGE_ID);
                    listItem.COMPANY_NAME = Convert.ToString(item.COMPANY_NAME);
                    listItem.PACKAGE_NAME = Convert.ToString(item.PACKAGE_NAME);
                    listItem.PACKAGE_NO = Convert.ToString(item.PACKAGE_NO);
                    listItem.PRODUCT_CODE = Convert.ToString(item.PRODUCT_CODE);
                    listItem.SUBPRODUCT_CODE = Convert.ToString(item.SUBPRODUCT_CODE);
                    listItem.IS_OPEN_TO_WS = Convert.ToString(item.IS_OPEN_TO_WS);

                    var CheckInsured = new GenericRepository<Insured>().FindBy($"PACKAGE_ID={item.PACKAGE_ID}");
                    if (CheckInsured.Count > 0)
                        listItem.IS_INSURED_PACKAGE = "1";
                    else
                        listItem.IS_INSURED_PACKAGE = "0";

                    PackageList.Add(listItem);
                }
            }

            return Json(new { data = PackageList, draw = Request["draw"], recordsTotal = Packages.TotalItemsCount, recordsFiltered = Packages.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        // GET: Coverage Delete /Product/PackageDelete
        [LoginControl]
        public ActionResult PackageDelete(Int64 id)
        {
            var repo = new PackageRepository();
            var result = repo.FindById(id);
            if (result != null)
            {
                result.Status = "1";
                repo.Update(result);
            }
            return RedirectToAction("Package");
        }

        public ActionResult PackageClone(Int64 Id = 0)
        {
            try
            {
                if (Id != 0)
                {
                    var oldPackage = new PackageRepository().FindById(Id);
                    if (oldPackage != null)
                    {
                        #region PriceList Clone
                        long priceListId = 0;
                        if (oldPackage.PriceListId != null && oldPackage.PriceListId > 0)
                        {
                            priceListId = CloneHelper.PriceListClone((long)oldPackage.PriceListId);
                        }
                        #endregion

                        #region Package Clone
                        long newPackageId = 0;
                        Package package = new Package
                        {
                            Id = 0,
                            Name = oldPackage.Name + "-Kopya",
                            No = oldPackage.No,
                            Status = oldPackage.Status,
                            Type = oldPackage.Type,
                            ValidFrom = oldPackage.ValidFrom,
                            PriceListId = priceListId,
                            ValidTo = oldPackage.ValidTo
                        };
                        if (priceListId > 0)
                        {
                            package.PriceListId = priceListId;
                        }
                        var spResponsePackage = new PackageRepository().Insert(package);
                        if (spResponsePackage.Code != "100")
                        {
                            throw new Exception(spResponsePackage.Code + " : " + spResponsePackage.Message);
                        }
                        newPackageId = spResponsePackage.PkId;
                        package.Id = newPackageId;

                        #region Clone Medias of Package
                        var packageMediaList = new PackageMediaRepository().FindBy("PACKAGE_ID=:id", parameters: new { id = oldPackage.Id });
                        foreach (var packageMedia in packageMediaList)
                        {
                            #region Clone Media
                            long mediaId = 0;
                            if (packageMedia.MediaId > 0)
                            {
                                mediaId = CloneHelper.MediaClone<PackageMedia>(packageMedia.MediaId, packageMedia.PackageId);
                            }
                            #endregion
                            if (mediaId > 0)
                            {
                                #region Clone Package Media Relation                                
                                long packageMediaId = 0;
                                if (packageMedia.Id > 0)
                                {
                                    packageMediaId = CloneHelper.PackageMediaClone(packageMedia.Id, newPackageId, mediaId);
                                }
                                #endregion
                            }
                        }
                        #endregion


                        #endregion

                        #region Clone Notes of Package
                        List<PackageNote> packageNoteList = new PackageNoteRepository().FindBy("PACKAGE_ID=:id", parameters: new { id = oldPackage.Id });
                        foreach (var packageNote in packageNoteList)
                        {
                            #region Clone Note
                            long noteId = 0;
                            if (packageNote.NoteId > 0)
                            {
                                noteId = CloneHelper.NoteClone(packageNote.NoteId);
                            }
                            #endregion
                            if (noteId > 0)
                            {
                                #region Clone Package Note Relation
                                long newPackageNoteId = 0;
                                if (packageNote.Id > 0)
                                {
                                    newPackageNoteId = CloneHelper.PackageNoteClone(packageNote.Id, newPackageId, noteId);
                                }
                                #endregion
                            }
                        }
                        #endregion

                        #region Clone Parameters of Package
                        var packageParameterList = new PackageParameterRepository().FindBy("PACKAGE_ID=:id", parameters: new { id = oldPackage.Id });
                        foreach (var packageParameter in packageParameterList)
                        {
                            #region Clone Parameter                            
                            long parameterId = 0;
                            if (packageParameter.ParameterId > 0)
                            {
                                parameterId = CloneHelper.ParameterClone(packageParameter.ParameterId);
                            }
                            #endregion
                            if (parameterId > 0)
                            {
                                #region Clone Package Parameter Relation
                                long newPackageParameterId = 0;
                                if (packageParameter.Id > 0)
                                {
                                    newPackageParameterId = CloneHelper.PackageParameterClone(packageParameter.Id, newPackageId, parameterId);
                                }
                                #endregion
                            }
                        }
                        #endregion

                        #region Clone Package TPA Prices
                        var packageTPAPriceList = new PackageTPAPriceRepository().FindBy("PACKAGE_ID=:id ", parameters: new { id = oldPackage.Id });
                        if (packageTPAPriceList != null)
                        {
                            foreach (var packageTPAPrice in packageTPAPriceList)
                            {
                                #region Clone Package TPA Price
                                long packageTPAPriceId = 0;
                                if (packageTPAPrice.Id > 0)
                                {
                                    packageTPAPriceId = CloneHelper.PackageTPAPriceClone(packageTPAPrice.Id, newPackageId);
                                }
                                #endregion
                            }
                        }
                        #endregion                    

                        //#region Clone Rules of Package
                        //var rulePackageList = new RulePackageRepository().FindBy("PACKAGE_ID=:id", parameters: new { id = oldPackage.Id });

                        //foreach (var rulePackage in rulePackageList)
                        //{
                        //    #region Clone Package Rule Relation                           
                        //    long rulePackageId = 0;
                        //    if (rulePackage.Id > 0)
                        //    {
                        //        rulePackageId = CloneHelper.RulePackageClone(rulePackage.Id, newPackageId, rulePackage.RuleId);
                        //    }
                        //    #endregion
                        //}
                        //#endregion

                        #region Clone Plans of Package
                        List<PlanCoverage> planCoverageList = new PlanCoverageRepository().FindBy("PACKAGE_ID=:id AND PARENT_ID IS NULL", parameters: new { id = oldPackage.Id });
                        foreach (PlanCoverage oldPlanCoverage in planCoverageList)
                        {
                            #region Clone Plan Coverage
                            if (oldPlanCoverage.Id > 0)
                            {
                                CloneHelper.PlanCoverageClone(oldPlanCoverage.Id, oldPlanCoverage.PlanId, newPackageId);
                            }
                            #endregion
                        }
                        #endregion

                    }
                    TempData["Alert"] = $"swAlert('İşlem Başarılı','Paket başarıyla kopyalandı.','success')";
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Package");
        }

        [LoginControl]
        public ActionResult PackageForm(Int64 id = 0, string formaction = "")
        {

            MediaVM vm = new MediaVM();
            vm.MediaObjectType = Business.Enums.Media.MediaObjectType.T_PACKAGE_MEDIA;
            vm.ObjectId = id;
            vm.TabName = "tab6";

            try
            {
                dynamic personel = System.Web.HttpContext.Current.Session["Personnel"];
                long userId = long.Parse(Convert.ToString(personel.USER_ID));
                if (userId == 13 ||userId == 11 ||userId == 7||userId == 1)
                {
                    ViewBag.isAdmin = "1";
                }
                else
                {
                    ViewBag.isAdmin = "0";
                }
                
                #region Fill Component
                var CompanyList = new GenericRepository<Company>().FindBy();
                ViewBag.CompanyList = CompanyList;

                var IndividualTypeList = LookupHelper.GetLookupData(LookupTypes.Individual);
                ViewBag.IndividualTypeList = IndividualTypeList;

                var PackageTypeList = LookupHelper.GetLookupData(LookupTypes.Package);
                ViewBag.PackageTypeList = PackageTypeList;

                var SagmerPackageTypeList = new GenericRepository<SagmerPackageType>().FindBy();
                ViewBag.SagmerPackageTypeList = SagmerPackageTypeList;

                var SagmerPlanList = new SagmerPlanRepository().FindBy();
                ViewBag.SagmerPlanList = SagmerPlanList;

                var NetworkList = new GenericRepository<Network>().FindBy(orderby: "NAME");
                ViewBag.NetworkList = NetworkList;

                var LocationTypeList = LookupHelper.GetLookupData(LookupTypes.Location);
                ViewBag.LocationTypeList = LocationTypeList;

                var CurrencyTypeList = LookupHelper.GetLookupData(LookupTypes.Currency);
                ViewBag.CurrencyTypeList = CurrencyTypeList;

                var AgreementTypeList = LookupHelper.GetLookupData(LookupTypes.Agreement);
                ViewBag.AgreementTypeList = AgreementTypeList;

                var ExemptionTypeList = LookupHelper.GetLookupData(LookupTypes.Exemption);
                ViewBag.ExemptionTypeList = ExemptionTypeList;

                var NoteTypes = LookupHelper.GetLookupData(LookupTypes.Note);
                ViewBag.NoteTypeList = NoteTypes;

                var PackageTpaPriceTypeList = LookupHelper.GetLookupData(LookupTypes.PackageTpaPrice);
                ViewBag.PackageTpaPriceTypeList = PackageTpaPriceTypeList;

                var CashPaymentTypeList = LookupHelper.GetLookupData(LookupTypes.CashPayment);
                ViewBag.CashPaymentTypeList = CashPaymentTypeList;

                ViewBag.RuleCategoryTypeList = LookupHelper.GetLookupData(LookupTypes.RuleCategory);

                var RuleList = new GenericRepository<V_Rule>().FindBy(orderby: "RULE_NAME ASC");
                ViewBag.RuleList = RuleList;

                var RuleGroupList = new GenericRepository<RuleGroup>().FindBy();
                ViewBag.RuleGroupList = RuleGroupList;



                TempData["Alert"] = TempData["Alert"] ?? string.Empty;
                ViewBag.isEdit = false;

                #endregion


                switch (formaction.ToLower())
                {
                    case "view":
                    case "edit":
                        if (formaction.ToLower() == "edit")
                            ViewBag.Title = "Paket Güncelle";
                        else
                            ViewBag.Title = "Paket Görüntüle";

                        if (id > 0)
                        {
                            var Packages = new GenericRepository<V_Package>().FindBy("PACKAGE_ID=:id", orderby: "PACKAGE_ID", parameters: new { id }).FirstOrDefault();

                            if (Packages != null)
                            {
                                #region Genel
                                ViewBag.Package = Packages;
                                ViewBag.PackageTPAPrice = null;
                                ViewBag.PackageTypePackageParameter = null;
                                ViewBag.ProductPackageParameterId = null;

                                List<Product> productList = new GenericRepository<Product>().FindBy("COMPANY_ID=:companyId", parameters: new { companyId = Packages.COMPANY_ID });
                                ViewBag.ProductList = productList;

                                List<Subproduct> subproductList = new GenericRepository<Subproduct>().FindBy($"PRODUCT_ID=:productId", parameters: new { productId = Packages.PRODUCT_ID });
                                ViewBag.SubproductList = subproductList;

                                #region Sagmer Parameter
                                var PackageTypePackageParameter = new GenericRepository<V_PackageParameter>().FindBy("SYSTEM_TYPE=:SYSTEM_TYPE AND KEY=:KEY AND PACKAGE_ID=:id", orderby: "PARAMETER_ID", parameters: new { SYSTEM_TYPE = new DbString { Value = ((int)SystemType.SAGMER).ToString(), Length = 3 }, KEY = new DbString { Value = AppKeys.SagmerPackageType, Length = 30 }, id }).FirstOrDefault();
                                if (PackageTypePackageParameter != null)
                                {
                                    ViewBag.PackageTypePackageParameter = PackageTypePackageParameter;
                                }

                                var ProductPackageParameterId = new GenericRepository<V_PackageParameter>().FindBy("SYSTEM_TYPE=:SYSTEM_TYPE AND KEY=:KEY AND PACKAGE_ID=:id", orderby: "PARAMETER_ID", parameters: new { SYSTEM_TYPE = new DbString { Value = ((int)SystemType.SAGMER).ToString(), Length = 3 }, KEY = new DbString { Value = AppKeys.SagmerPlanId, Length = 30 }, id }).FirstOrDefault();

                                //ViewHelper.GetView(Views.PackageParameter, whereCondititon: $"SYSTEM_TYPE={(int)SystemType.SAGMER} AND KEY='{AppKeys.SagmerPackageType}' AND PACKAGE_ID={id}", orderby: "PARAMETER_ID");
                                if (ProductPackageParameterId != null)
                                {
                                    ViewBag.ProductPackageParameterId = ProductPackageParameterId;
                                }
                                #endregion

                                #endregion

                                #region TPA Price
                                var PackageTpaPriceList = new GenericRepository<V_PackageTpaPrice>().FindBy("PACKAGE_ID=:id", orderby: "PACKAGE_ID", parameters: new { id });
                                if (PackageTpaPriceList != null)
                                {
                                    List<dynamic> notesList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in PackageTpaPriceList)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.PACKAGE_TPA_PRICE_ID = Convert.ToString(item.PACKAGE_TPA_PRICE_ID);
                                        listItem.PACKAGE_TPA_PRICE_TYPE = Convert.ToString(item.PACKAGE_TPA_PRICE_TYPE);

                                        listItem.PACKAGE_CASH_PAYMENT_TYPE = Convert.ToString(item.CASH_PAYMENT_TYPE);
                                        listItem.PACKAGE_TPA_PRICE_START_DATE = item.START_DATE;
                                        listItem.PACKAGE_TPA_PRICE_AMOUNT = Convert.ToString(item.AMOUNT);


                                        listItem.isOpen = "0";

                                        notesList.Add(listItem);

                                        i++;
                                    }
                                    ViewBag.PackageTPAPriceList = notesList.ToJSON();
                                }
                                #endregion
                                //if (PackageTpaPriceList != null)
                                //{
                                //    List<dynamic> notesList = new List<dynamic>();

                                //    int i = 1;

                                //    foreach (var item in PackageTpaPriceList)
                                //    {
                                //        dynamic listItem = new System.Dynamic.ExpandoObject();

                                //        listItem.id = i;
                                //        listItem.ClientId = i;
                                //        listItem.STATUS = item.STATUS;

                                //        listItem.PACKAGE_TPA_PRICE_ID = Convert.ToString(item.PACKAGE_TPA_PRICE_ID);
                                //        listItem.PACKAGE_TPA_PRICE_TYPE = Convert.ToString(item.PACKAGE_TPA_PRICE_TYPE);

                                //        listItem.CASH_PAYMENT_TYPE = Convert.ToString(item.CASH_PAYMENT_TYPE);
                                //        listItem.START_DATE = Convert.ToString(item.START_DATE);
                                //        listItem.AMOUNT = Convert.ToString(item.AMOUNT);


                                //        listItem.isOpen = "0";

                                //        notesList.Add(listItem);

                                //        i++;
                                //    }

                                //    ViewBag.PackageTPAPriceList = PackageTpaPriceList.ToJSON(true);
                                //    #endregion
                                //}
                                #region Plan List

                                var PlanCoverageList = new GenericRepository<V_PlanCoverage>().FindBy("PACKAGE_ID=:id",
                                                          orderby: "PLAN_COVERAGE_ID ASC", parameters: new { id });

                                if (PlanCoverageList != null && PlanCoverageList.Count > 0)
                                {
                                    List<dynamic> planCoveragesList = new List<dynamic>();

                                    int i = 1;

                                    List<string> planIdList = new List<string>();

                                    foreach (var item in PlanCoverageList.Where(p => p.PARENT_ID == null))
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.PLAN_COVERAGE_ID = Convert.ToString(item.PLAN_COVERAGE_ID);
                                        listItem.COVERAGE_ID = Convert.ToString(item.COVERAGE_ID);
                                        listItem.NETWORK = Convert.ToString(item.NETWORK_ID);

                                        listItem.PARENT_ID = string.IsNullOrEmpty(Convert.ToString(item.PARENT_ID)) ? "-1" : Convert.ToString(item.PARENT_ID);
                                        listItem.PLAN_COVERAGE_NAME = item.PLAN_COVERAGE_NAME;
                                        listItem.LOCATION_TYPE = item.LOCATION_TYPE;
                                        listItem.COVERAGE_IDSelectedText = item.COVERAGE_NAME;
                                        listItem.NETWORKSelectedText = item.NETWORK_NAME;
                                        listItem.IS_OPEN_TO_PROVIDER = item.IS_OPEN_TO_PROVIDER;
                                        listItem.IS_OPEN_TO_PRINT = item.IS_OPEN_TO_PRINT;
                                        listItem.isOpen = "0";

                                        List<dynamic> childrenList = new List<dynamic>();

                                        foreach (var childrenItem in PlanCoverageList.Where(p => p.PARENT_ID == item.PLAN_COVERAGE_ID))
                                        {
                                            dynamic children = new System.Dynamic.ExpandoObject();

                                            children.id = i;
                                            children.ClientId = i;
                                            children.STATUS = childrenItem.STATUS;

                                            children.PLAN_COVERAGE_ID = Convert.ToString(childrenItem.PLAN_COVERAGE_ID);
                                            children.COVERAGE_ID = Convert.ToString(childrenItem.COVERAGE_ID);
                                            children.NETWORK = Convert.ToString(childrenItem.NETWORK_ID);

                                            children.PARENT_ID = string.IsNullOrEmpty(Convert.ToString(childrenItem.PARENT_ID)) ? "-1" : Convert.ToString(childrenItem.PARENT_ID);
                                            children.PLAN_COVERAGE_NAME = childrenItem.PLAN_COVERAGE_NAME;
                                            children.LOCATION_TYPE = childrenItem.LOCATION_TYPE;
                                            children.COVERAGE_IDSelectedText = childrenItem.COVERAGE_NAME;
                                            children.NETWORKSelectedText = childrenItem.NETWORK_NAME;
                                            children.IS_OPEN_TO_PROVIDER = childrenItem.IS_OPEN_TO_PROVIDER;
                                            children.IS_OPEN_TO_PRINT = childrenItem.IS_OPEN_TO_PRINT;
                                            children.isOpen = "0";

                                            childrenList.Add(children);
                                        }
                                        listItem.children = childrenList;

                                        planIdList.Add(Convert.ToString(item.PLAN_ID));
                                        planCoveragesList.Add(listItem);

                                        i++;
                                    }
                                    ViewBag.PlanCoverageList = planCoveragesList.ToJSON();

                                    List<Plan> planList = new GenericRepository<Plan>().FindBy("SUBPRODUCT_ID=:SUBPRODUCT_ID", parameters: new { Packages.SUBPRODUCT_ID });
                                    List<dynamic> planDynamicList = new List<dynamic>();

                                    foreach (var item in planList)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.Id = item.Id;
                                        listItem.Name = item.Name;
                                        foreach (var it in planIdList)
                                        {
                                            if (it == item.Id.ToString())
                                            {
                                                listItem.IsSelected = "1";
                                                break;
                                            }
                                            else
                                                listItem.IsSelected = "0";
                                        }

                                        planDynamicList.Add(listItem);

                                        i++;
                                    }
                                    ViewBag.PlanList = planDynamicList;

                                    var SagmerPlanCoverageList = new GenericRepository<V_PlanCoverageParameter>().FindBy("PACKAGE_ID=:id AND KEY=:Key",
                                                                orderby: "PLAN_COVERAGE_ID", parameters: new { id, Key = new DbString { Value = AppKeys.SagmerCoverage, Length = 30 } });


                                    if (SagmerPlanCoverageList != null && SagmerPlanCoverageList.Count > 0)
                                    {
                                        List<dynamic> sagmerPlanCoveragesList = new List<dynamic>();

                                        i = 1;
                                        //List<SagmerCoverage> sagmerCoverageList = new GenericRepository<SagmerCoverage>().FindBy();
                                        foreach (var item in SagmerPlanCoverageList)
                                        {
                                            SagmerCoverage sagmerCoverage = new GenericRepository<SagmerCoverage>().FindById(long.Parse(Convert.ToString(item.VALUE)));
                                            dynamic listItem = new System.Dynamic.ExpandoObject();
                                            if (sagmerCoverage != null)
                                            {
                                                listItem.id = i;
                                                listItem.ClientId = i;
                                                listItem.STATUS = item.STATUS;

                                                listItem.PLAN_COVERAGE_ID = Convert.ToString(item.PLAN_COVERAGE_ID);
                                                listItem.PARAMETER_ID = Convert.ToString(item.PARAMETER_ID);
                                                listItem.PLAN_COVERAGE_PARAMETER_ID = Convert.ToString(item.PLAN_COVERAGE_PARAMETER_ID);
                                                listItem.SAGMER_COVERAGE_TYPE = Convert.ToString(sagmerCoverage.SagmerCoverageTypeId);
                                                listItem.SAGMER_COVERAGE = item.VALUE;
                                                listItem.isOpen = "0";

                                                sagmerPlanCoveragesList.Add(listItem);

                                                i++;
                                            }
                                        }
                                        ViewBag.SagmerCoverageTypes = sagmerPlanCoveragesList.ToJSON();
                                    }
                                }

                                var CoverageVariation = new GenericRepository<V_PackagePlanCoverageVar>().FindBy("PACKAGE_ID=:id AND AGR_NEW_VERSION_ID IS NULL AND (AGR_STATUS IS NULL OR AGR_STATUS!=:Status) AND EMG_NEW_VERSION_ID IS NULL AND (EMG_STATUS IS NULL OR EMG_STATUS!=:Status) AND (NAG_STATUS IS NULL OR NAG_STATUS!=:Status) AND NAG_NEW_VERSION_ID IS NULL", orderby: "PACKAGE_ID", parameters: new { id, Status = new DbString { Value = ((int)Status.SILINDI).ToString(), Length = 3 } }, fetchDeletedRows: true, fetchHistoricRows: true);

                                //ViewHelper.GetView(Views.PackagePlanCoverageVariation,
                                //                   whereCondititon: $"PACKAGE_ID={id} AND AGR_NEW_VERSION_ID IS NULL AND (AGR_STATUS IS NULL OR AGR_STATUS!=1) AND EMG_NEW_VERSION_ID IS NULL AND (EMG_STATUS IS NULL OR EMG_STATUS!=1) AND (NAG_STATUS IS /NULL /OR NAG_STATUS!=1) AND NAG_NEW_VERSION_ID IS NULL",
                                //                   fetchHistoricRows: true,
                                //                   fetchDeletedRows: true);

                                if (CoverageVariation != null && CoverageVariation.Count > 0)
                                {
                                    List<dynamic> listContractedVariation = new List<dynamic>();
                                    List<dynamic> listNonAggrementVariation = new List<dynamic>();
                                    List<dynamic> listUrgentVariation = new List<dynamic>();

                                    foreach (var variation in CoverageVariation)
                                    {
                                        //AGR
                                        dynamic variationListItem = new System.Dynamic.ExpandoObject();

                                        if (variation.AGR_AGREEMENT_TYPE != null)
                                        {
                                            variationListItem.Type = "AGR";
                                            variationListItem.AGR_ID = variation.AGR_ID;
                                            variationListItem.PLAN_COVERAGE_ID = variation.PLAN_COVERAGE_ID;
                                            variationListItem.AGR_AGREEMENT_TYPE = variation.AGR_AGREEMENT_TYPE;
                                            variationListItem.AGR_PRICE_LIMIT = variation.AGR_PRICE_LIMIT;
                                            variationListItem.AGR_PRICE_FACTOR = variation.AGR_PRICE_FACTOR;
                                            variationListItem.AGR_CURRENCY_ID = variation.AGR_CURRENCY_ID;
                                            variationListItem.AGR_NUMBER_LIMIT = variation.AGR_NUMBER_LIMIT;
                                            variationListItem.AGR_DAY_LIMIT = variation.AGR_DAY_LIMIT;
                                            variationListItem.AGR_COINSURANCE_RATIO = variation.AGR_COINSURANCE_RATIO;
                                            variationListItem.AGR_EXEMPTION_TYPE = variation.AGR_EXEMPTION_TYPE;
                                            variationListItem.AGR_EXEMPTION_LIMIT = variation.AGR_EXEMPTION_LIMIT;
                                            variationListItem.AGR_SESSION_COUNT = variation.AGR_SESSION_COUNT;

                                            listContractedVariation.Add(variationListItem);
                                        }
                                        //NAG
                                        variationListItem = new System.Dynamic.ExpandoObject();

                                        if (variation.NAG_AGREEMENT_TYPE != null)
                                        {
                                            variationListItem.Type = "NAG";
                                            variationListItem.NAG_ID = variation.NAG_ID;
                                            variationListItem.PLAN_COVERAGE_ID = variation.PLAN_COVERAGE_ID;
                                            variationListItem.NAG_AGREEMENT_TYPE = variation.NAG_AGREEMENT_TYPE;
                                            variationListItem.NAG_PRICE_LIMIT = variation.NAG_PRICE_LIMIT;
                                            variationListItem.NAG_PRICE_FACTOR = variation.NAG_PRICE_FACTOR;
                                            variationListItem.NAG_CURRENCY_ID = variation.NAG_CURRENCY_ID;
                                            variationListItem.NAG_NUMBER_LIMIT = variation.NAG_NUMBER_LIMIT;
                                            variationListItem.NAG_DAY_LIMIT = variation.NAG_DAY_LIMIT;
                                            variationListItem.NAG_COINSURANCE_RATIO = variation.NAG_COINSURANCE_RATIO;
                                            variationListItem.NAG_EXEMPTION_TYPE = variation.NAG_EXEMPTION_TYPE;
                                            variationListItem.NAG_EXEMPTION_LIMIT = variation.NAG_EXEMPTION_LIMIT;
                                            variationListItem.NAG_SESSION_COUNT = variation.NAG_SESSION_COUNT;

                                            listNonAggrementVariation.Add(variationListItem);
                                        }
                                        //EMG
                                        variationListItem = new System.Dynamic.ExpandoObject();

                                        if (variation.EMG_AGREEMENT_TYPE != null)
                                        {
                                            variationListItem.Type = "EMG";
                                            variationListItem.EMG_ID = variation.EMG_ID;
                                            variationListItem.PLAN_COVERAGE_ID = variation.PLAN_COVERAGE_ID;
                                            variationListItem.EMG_AGREEMENT_TYPE = variation.EMG_AGREEMENT_TYPE;
                                            variationListItem.EMG_PRICE_LIMIT = variation.EMG_PRICE_LIMIT;
                                            variationListItem.EMG_PRICE_FACTOR = variation.EMG_PRICE_FACTOR;
                                            variationListItem.EMG_CURRENCY_ID = variation.EMG_CURRENCY_ID;
                                            variationListItem.EMG_NUMBER_LIMIT = variation.EMG_NUMBER_LIMIT;
                                            variationListItem.EMG_DAY_LIMIT = variation.EMG_DAY_LIMIT;
                                            variationListItem.EMG_COINSURANCE_RATIO = variation.EMG_COINSURANCE_RATIO;
                                            variationListItem.EMG_EXEMPTION_TYPE = variation.EMG_EXEMPTION_TYPE;
                                            variationListItem.EMG_EXEMPTION_LIMIT = variation.EMG_EXEMPTION_LIMIT;
                                            variationListItem.EMG_SESSION_COUNT = variation.EMG_SESSION_COUNT;

                                            listUrgentVariation.Add(variationListItem);
                                        }
                                    }

                                    ViewBag.ContractedVariation = listContractedVariation.Any() ? listContractedVariation.ToJSON() : null;
                                    ViewBag.NonAgreementVariation = listNonAggrementVariation.Any() ? listNonAggrementVariation.ToJSON() : null;
                                    ViewBag.UrgentVariation = listUrgentVariation.Any() ? listUrgentVariation.ToJSON() : null;
                                }

                                #endregion

                                #region Price List
                                var pricess = new GenericRepository<V_PackagePrice>().FindBy("PACKAGE_ID =:id", orderby: "PACKAGE_ID", parameters: new { id });
                                if (pricess != null)
                                {
                                    List<dynamic> pricesList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in pricess)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.PRICE_ID = Convert.ToString(item.PRICE_ID);

                                        listItem.AGE_START = Convert.ToString(item.AGE_START);
                                        listItem.AGE_END = Convert.ToString(item.AGE_END);
                                        listItem.GENDER = Convert.ToString(item.GENDER);
                                        listItem.GENDERSelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Gender, item.GENDER);
                                        listItem.INDIVIDUAL_TYPE = Convert.ToString(item.INDIVIDUAL_TYPE);
                                        listItem.INDIVIDUAL_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Individual, item.INDIVIDUAL_TYPE);
                                        listItem.PREMIUM = Convert.ToString(item.PREMIUM);
                                        listItem.isOpen = "0";

                                        pricesList.Add(listItem);

                                        i++;
                                    }

                                    ViewBag.PriceList = pricesList.ToJSON();
                                }
                                #endregion

                                #region Note List
                                var notess = new GenericRepository<V_PackageNote>().FindBy("PACKAGE_ID=:id", orderby: "PACKAGE_ID", parameters: new { id });
                                if (notess != null)
                                {
                                    List<dynamic> notesList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in notess)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                                        listItem.PACKAGE_NOTE_ID = Convert.ToString(item.PACKAGE_NOTE_ID);

                                        listItem.NOTE_TYPE = Convert.ToString(item.PACKAGE_NOTE_TYPE);
                                        listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Note, Convert.ToString(item.PACKAGE_NOTE_TYPE));
                                        listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                                        listItem.isOpen = "0";

                                        notesList.Add(listItem);

                                        i++;
                                    }

                                    ViewBag.NoteList = notesList.ToJSON(true);
                                }
                                #endregion

                                #region Rule List

                                #region RuleSpecial
                                var ruleSpecialList = new GenericRepository<V_RulePackage>().FindBy("PACKAGE_ID=:id AND TYPE=:type", orderby: "PACKAGE_ID", parameters: new { id, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });

                                List<dynamic> ruleDynamicSpecialList = new List<dynamic>();
                                int specialId = 1;

                                if (ruleSpecialList != null)
                                {
                                    foreach (var item in ruleSpecialList)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = specialId;
                                        listItem.ClientId = specialId;
                                        listItem.STATUS = item.STATUS;

                                        listItem.RULE_PACKAGE_ID = Convert.ToString(item.RULE_PACKAGE_ID);
                                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                        listItem.RULE_NAME = item.RULE_NAME;
                                        listItem.RESULT = item.RESULT;
                                        listItem.INHERITED_NAME = string.Empty;
                                        listItem.INHERITED_CODE = string.Empty;
                                        listItem.isOpen = "0";
                                        listItem.IsInherited = "1";

                                        ruleDynamicSpecialList.Add(listItem);

                                        specialId++;
                                    }
                                }

                                var ruleSubProductSpecialList = new GenericRepository<V_RuleSubProduct>().FindBy("SUBPRODUCT_ID=:SUBPRODUCT_ID AND TYPE=:type", orderby: "SUBPRODUCT_ID", parameters: new { Packages.SUBPRODUCT_ID, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });

                                if (ruleSubProductSpecialList != null)
                                {
                                    foreach (var item in ruleSubProductSpecialList)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = specialId;
                                        listItem.ClientId = specialId;
                                        listItem.STATUS = item.STATUS;

                                        listItem.RULE_SUBPRODUCT_ID = Convert.ToString(item.RULE_SUBPRODUCT_ID);
                                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                        listItem.RULE_NAME = item.RULE_NAME;
                                        listItem.RESULT = item.RESULT;
                                        listItem.INHERITED_NAME = "AltÜrün";
                                        listItem.INHERITED_CODE = Convert.ToString(item.SUBPRODUCT_CODE);
                                        listItem.isOpen = "0";
                                        listItem.IsInherited = "0";

                                        ruleDynamicSpecialList.Add(listItem);

                                        specialId++;
                                    }
                                }

                                var ruleProductSpecialList = new GenericRepository<V_RuleProduct>().FindBy("PRODUCT_ID=:PRODUCT_ID AND TYPE=:type", orderby: "PRODUCT_ID", parameters: new { Packages.PRODUCT_ID, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });
                                if (ruleProductSpecialList != null)
                                {

                                    foreach (var item in ruleProductSpecialList)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = specialId;
                                        listItem.ClientId = specialId;
                                        listItem.STATUS = item.STATUS;

                                        listItem.RULE_PRODUCT_ID = Convert.ToString(item.RULE_PRODUCT_ID);
                                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                        listItem.RULE_NAME = item.RULE_NAME;
                                        listItem.RESULT = item.RESULT;

                                        listItem.INHERITED_NAME = "Ürün";
                                        listItem.INHERITED_CODE = Convert.ToString(item.PRODUCT_CODE);
                                        listItem.isOpen = "0";
                                        listItem.IsInherited = "0";

                                        ruleDynamicSpecialList.Add(listItem);

                                        specialId++;
                                    }
                                }

                                ViewBag.RuleSpecialList = ruleDynamicSpecialList.ToJSON(true);
                                #endregion

                                #region RuleExtra
                                var rulePackageExtraList = new GenericRepository<V_RulePackage>().FindBy("PACKAGE_ID=:id AND TYPE=:type", orderby: "PACKAGE_ID", parameters: new { id, type = new DbString { Value = ((int)RuleType.EK_PROTOKOL).ToString(), Length = 3 } });

                                List<dynamic> ruleDynamicExtraList = new List<dynamic>();
                                int extraId = 1;

                                if (rulePackageExtraList != null)
                                {

                                    foreach (var item in rulePackageExtraList)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = extraId;
                                        listItem.ClientId = extraId;
                                        listItem.STATUS = item.STATUS;

                                        listItem.RULE_PACKAGE_ID = Convert.ToString(item.RULE_PACKAGE_ID);
                                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                        listItem.RULE_NAME = item.RULE_NAME;
                                        listItem.RESULT = item.RESULT;
                                        listItem.INHERITED_NAME = string.Empty;
                                        listItem.INHERITED_CODE = string.Empty;
                                        listItem.isOpen = "0";
                                        listItem.IsInherited = "1";

                                        ruleDynamicExtraList.Add(listItem);

                                        extraId++;
                                    }
                                }

                                var ruleSubProductExtraList = new GenericRepository<V_RuleSubProduct>().FindBy("SUBPRODUCT_ID=:SUBPRODUCT_ID AND TYPE=:type", orderby: "SUBPRODUCT_ID", parameters: new { Packages.SUBPRODUCT_ID, type = new DbString { Value = ((int)RuleType.EK_PROTOKOL).ToString(), Length = 3 } });

                                if (ruleSubProductExtraList != null)
                                {

                                    foreach (var item in ruleSubProductExtraList)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = extraId;
                                        listItem.ClientId = extraId;
                                        listItem.STATUS = item.STATUS;

                                        listItem.RULE_SUBPRODUCT_ID = Convert.ToString(item.RULE_SUBPRODUCT_ID);
                                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                        listItem.RULE_NAME = item.RULE_NAME;
                                        listItem.RESULT = item.RESULT;
                                        listItem.INHERITED_NAME = "AltÜrün";
                                        listItem.INHERITED_CODE = Convert.ToString(item.SUBPRODUCT_CODE);
                                        listItem.isOpen = "0";
                                        listItem.IsInherited = "0";

                                        ruleDynamicExtraList.Add(listItem);

                                        extraId++;
                                    }
                                }

                                ViewBag.RuleExtraList = ruleDynamicExtraList.ToJSON(true);

                                #endregion

                                #endregion

                                if (formaction.ToLower() == "edit")
                                    ViewBag.isEdit = true;
                                else
                                    ViewBag.isView = true;
                            }
                        }
                        break;

                    default:
                        ViewBag.Title = "Paket Ekle";
                        ViewBag.isEdit = false;
                        break;
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }


            return View(vm);
        }

        [HttpPost]
        public JsonResult SavePackagePlanCoverage(List<string> planIdList)
        {
            var result = new List<List<dynamic>>();
            try
            {
                var packageId = Request["packageId"];

                if (planIdList.Any())
                {
                    List<Int64> idList = new List<long>();
                    foreach (var item in planIdList)
                    {
                        idList.Add(long.Parse(item));
                    }
                    var PlanCoverageList = new GenericRepository<V_PlanCoverage>().FindBy("PACKAGE_ID IS NULL AND PLAN_ID IN :idList", orderby: "PLAN_COVERAGE_ID ASC", parameters: new { idList = idList.ToArray() });

                    if (PlanCoverageList != null && PlanCoverageList.Count > 0)
                    {
                        List<dynamic> planCoveragesList = new List<dynamic>();
                        var parentIdList = new Dictionary<long, long>();
                        foreach (var item in PlanCoverageList)
                        {
                            var TplanCoverage = new PlanCoverage
                            {
                                Id = 0,
                                PlanId = Convert.ToInt64(item.PLAN_ID),
                                CoverageId = Convert.ToInt64(item.COVERAGE_ID),
                                PackageId = long.Parse(packageId),
                                CoverageName = item.PLAN_COVERAGE_NAME,
                                LocationType = item.LOCATION_TYPE,
                                NetworkId = Convert.ToInt64(item.NETWORK_ID),
                                Status = item.STATUS
                            };
                            if (item.PARENT_ID != 0 && item.PARENT_ID != -1 && item.PARENT_ID != null)
                            {
                                var a = parentIdList.Where(x => x.Key == Convert.ToInt64(item.PARENT_ID)).FirstOrDefault().Value;
                                if (a == 0) continue;
                                TplanCoverage.ParentId = a;
                            }
                            var spResponsePlanCoverage = new PlanCoverageRepository().Insert(TplanCoverage);
                            if (spResponsePlanCoverage.Code == "100")
                            {
                                var planCoverageId = spResponsePlanCoverage.PkId;
                                if (item.PARENT_ID == null)
                                {
                                    parentIdList.Add(Convert.ToInt64(item.PLAN_COVERAGE_ID), planCoverageId);
                                }
                                var CoverageVar = new GenericRepository<V_PlanCoverageVariation>().FindBy("PLAN_COVERAGE_ID=:PLAN_COVERAGE_ID AND AGR_NEW_VERSION_ID IS NULL AND (AGR_STATUS IS NULL OR AGR_STATUS!=:Status) AND EMG_NEW_VERSION_ID IS NULL AND (EMG_STATUS IS NULL OR EMG_STATUS!=:Status) AND (NAG_STATUS IS NULL OR NAG_STATUS!=:Status) AND NAG_NEW_VERSION_ID IS NULL", orderby: "PLAN_COVERAGE_ID", parameters: new { item.PLAN_COVERAGE_ID, Status = new DbString { Value = ((int)Status.SILINDI).ToString(), Length = 3 } });

                                if (CoverageVar != null && CoverageVar.Count > 0)
                                {
                                    var variation = CoverageVar[CoverageVar.Count - 1];

                                    //AGR
                                    dynamic variationListItem = new System.Dynamic.ExpandoObject();

                                    if (variation.AGR_AGREEMENT_TYPE != null)
                                    {
                                        var cID = "0";
                                        var cAGREEMENT_TYPE = Convert.ToString(variation.AGR_AGREEMENT_TYPE);
                                        var cPRICE_LIMIT = Convert.ToString(variation.AGR_PRICE_LIMIT);
                                        var cPRICE_FACTOR = Convert.ToString(variation.AGR_PRICE_FACTOR);
                                        var cCURRENCY_TYPE = Convert.ToString(variation.AGR_CURRENCY_ID);
                                        var cNUMBER_LIMIT = Convert.ToString(variation.AGR_NUMBER_LIMIT);
                                        var cDAY_LIMIT = Convert.ToString(variation.AGR_DAY_LIMIT);
                                        var cCOINSURANCE_RATIO = Convert.ToString(variation.AGR_COINSURANCE_RATIO);
                                        var cEXEMPTION_TYPE = Convert.ToString(variation.AGR_EXEMPTION_TYPE);
                                        var cEXEMPTION_LIMIT = Convert.ToString(variation.AGR_EXEMPTION_LIMIT);
                                        var cSESSION_COUNT = Convert.ToString(variation.AGR_SESSION_COUNT);

                                        var TplanCoverageVariation = new PlanCoverageVariation
                                        {
                                            Id = string.IsNullOrEmpty(cID) ? 0 : long.Parse(cID),
                                            Type = ((int)PlanCoverageVariationType.ANLASMALI_KURUM).ToString(),
                                            PlanCoverageId = planCoverageId,
                                            AgreementType = cAGREEMENT_TYPE,
                                            PriceLimit = String.IsNullOrEmpty(cPRICE_LIMIT) ? null : (decimal?)decimal.Parse(cPRICE_LIMIT),
                                            PriceFactor = String.IsNullOrEmpty(cPRICE_FACTOR) ? null : (decimal?)decimal.Parse(cPRICE_FACTOR),
                                            NumberLimit = String.IsNullOrEmpty(cNUMBER_LIMIT) ? null : (int?)int.Parse(cNUMBER_LIMIT),
                                            DayLimit = String.IsNullOrEmpty(cDAY_LIMIT) ? null : (int?)int.Parse(cDAY_LIMIT),
                                            CoinsuranceRatio = String.IsNullOrEmpty(cCOINSURANCE_RATIO) ? null : (int?)int.Parse(cCOINSURANCE_RATIO),
                                            ExemptionType = cEXEMPTION_TYPE,
                                            ExemptionLimit = String.IsNullOrEmpty(cEXEMPTION_LIMIT) ? null : (int?)int.Parse(cEXEMPTION_LIMIT),
                                            CurrencyType = cCURRENCY_TYPE,
                                            SessionCount = String.IsNullOrEmpty(cSESSION_COUNT) ? null : (int?)int.Parse(cSESSION_COUNT)
                                        };

                                        var spResponseVariation = new PlanCoverageVariationRepository().Insert(TplanCoverageVariation);
                                        if (spResponseVariation.Code != "100")
                                        {
                                            throw new Exception(spResponseVariation.Code + " : " + spResponseVariation.Message);
                                        }
                                    }
                                    //NAG
                                    variationListItem = new System.Dynamic.ExpandoObject();

                                    if (variation.NAG_AGREEMENT_TYPE != null)
                                    {
                                        var nID = "0";
                                        var nAGREEMENT_TYPE = Convert.ToString(variation.NAG_AGREEMENT_TYPE);
                                        var nPRICE_LIMIT = Convert.ToString(variation.NAG_PRICE_LIMIT);
                                        var nPRICE_FACTOR = Convert.ToString(variation.NAG_PRICE_FACTOR);
                                        var nCURRENCY_TYPE = Convert.ToString(variation.NAG_CURRENCY_ID);
                                        var nNUMBER_LIMIT = Convert.ToString(variation.NAG_NUMBER_LIMIT);
                                        var nDAY_LIMIT = Convert.ToString(variation.NAG_DAY_LIMIT);
                                        var nCOINSURANCE_RATIO = Convert.ToString(variation.NAG_COINSURANCE_RATIO);
                                        var nEXEMPTION_TYPE = Convert.ToString(variation.NAG_EXEMPTION_TYPE);
                                        var nEXEMPTION_LIMIT = Convert.ToString(variation.NAG_EXEMPTION_LIMIT);
                                        var nSESSION_COUNT = Convert.ToString(variation.NAG_SESSION_COUNT);

                                        var TplanCoverageVariation = new PlanCoverageVariation
                                        {
                                            Id = string.IsNullOrEmpty(nID) ? 0 : long.Parse(nID),
                                            Type = ((int)PlanCoverageVariationType.ANLASMASIZ_KURUM).ToString(),
                                            PlanCoverageId = planCoverageId,
                                            AgreementType = nAGREEMENT_TYPE,
                                            PriceLimit = String.IsNullOrEmpty(nPRICE_LIMIT) ? null : (decimal?)decimal.Parse(nPRICE_LIMIT),
                                            PriceFactor = String.IsNullOrEmpty(nPRICE_FACTOR) ? null : (decimal?)decimal.Parse(nPRICE_FACTOR),
                                            NumberLimit = String.IsNullOrEmpty(nNUMBER_LIMIT) ? null : (int?)int.Parse(nNUMBER_LIMIT),
                                            DayLimit = String.IsNullOrEmpty(nDAY_LIMIT) ? null : (int?)int.Parse(nDAY_LIMIT),
                                            CoinsuranceRatio = String.IsNullOrEmpty(nCOINSURANCE_RATIO) ? null : (int?)int.Parse(nCOINSURANCE_RATIO),
                                            ExemptionType = nEXEMPTION_TYPE,
                                            ExemptionLimit = String.IsNullOrEmpty(nEXEMPTION_LIMIT) ? null : (int?)int.Parse(nEXEMPTION_LIMIT),
                                            CurrencyType = nCURRENCY_TYPE,
                                            SessionCount = String.IsNullOrEmpty(nSESSION_COUNT) ? null : (int?)int.Parse(nSESSION_COUNT)
                                        };

                                        var spResponseVariation = new PlanCoverageVariationRepository().Insert(TplanCoverageVariation);
                                        if (spResponseVariation.Code != "100")
                                        {
                                            throw new Exception(spResponseVariation.Code + " : " + spResponseVariation.Message);
                                        }

                                    }
                                    //EMG
                                    variationListItem = new System.Dynamic.ExpandoObject();

                                    if (variation.EMG_AGREEMENT_TYPE != null)
                                    {
                                        var nID = "0";
                                        var nAGREEMENT_TYPE = Convert.ToString(variation.EMG_AGREEMENT_TYPE);
                                        var nPRICE_LIMIT = Convert.ToString(variation.EMG_PRICE_LIMIT);
                                        var nPRICE_FACTOR = Convert.ToString(variation.EMG_PRICE_FACTOR);
                                        var nCURRENCY_TYPE = Convert.ToString(variation.EMG_CURRENCY_ID);
                                        var nNUMBER_LIMIT = Convert.ToString(variation.EMG_NUMBER_LIMIT);
                                        var nDAY_LIMIT = Convert.ToString(variation.EMG_DAY_LIMIT);
                                        var nCOINSURANCE_RATIO = Convert.ToString(variation.EMG_COINSURANCE_RATIO);
                                        var nEXEMPTION_TYPE = Convert.ToString(variation.EMG_EXEMPTION_TYPE);
                                        var nEXEMPTION_LIMIT = Convert.ToString(variation.EMG_EXEMPTION_LIMIT);
                                        var nSESSION_COUNT = Convert.ToString(variation.EMG_SESSION_COUNT);

                                        var TplanCoverageVariation = new PlanCoverageVariation
                                        {
                                            Id = string.IsNullOrEmpty(nID) ? 0 : long.Parse(nID),
                                            Type = ((int)PlanCoverageVariationType.ACIL).ToString(),
                                            PlanCoverageId = planCoverageId,
                                            AgreementType = nAGREEMENT_TYPE,
                                            PriceLimit = String.IsNullOrEmpty(nPRICE_LIMIT) ? null : (decimal?)decimal.Parse(nPRICE_LIMIT),
                                            PriceFactor = String.IsNullOrEmpty(nPRICE_FACTOR) ? null : (decimal?)decimal.Parse(nPRICE_FACTOR),
                                            NumberLimit = String.IsNullOrEmpty(nNUMBER_LIMIT) ? null : (int?)int.Parse(nNUMBER_LIMIT),
                                            DayLimit = String.IsNullOrEmpty(nDAY_LIMIT) ? null : (int?)int.Parse(nDAY_LIMIT),
                                            CoinsuranceRatio = String.IsNullOrEmpty(nCOINSURANCE_RATIO) ? null : (int?)int.Parse(nCOINSURANCE_RATIO),
                                            ExemptionType = nEXEMPTION_TYPE,
                                            ExemptionLimit = String.IsNullOrEmpty(nEXEMPTION_LIMIT) ? null : (int?)int.Parse(nEXEMPTION_LIMIT),
                                            CurrencyType = nCURRENCY_TYPE,
                                            SessionCount = String.IsNullOrEmpty(nSESSION_COUNT) ? null : (int?)int.Parse(nSESSION_COUNT)
                                        };

                                        var spResponseVariation = new PlanCoverageVariationRepository().Insert(TplanCoverageVariation);
                                        if (spResponseVariation.Code != "100")
                                        {
                                            throw new Exception(spResponseVariation.Code + " : " + spResponseVariation.Message);
                                        }

                                    }
                                }
                            }
                            else
                            {
                                throw new Exception(spResponsePlanCoverage.Code + " : " + spResponsePlanCoverage.Message);
                            }
                        }
                    }
                    PlanCoverageList = new GenericRepository<V_PlanCoverage>().FindBy("PACKAGE_ID=:packageId", orderby: "PLAN_COVERAGE_ID ASC", parameters: new { packageId });

                    if (PlanCoverageList != null && PlanCoverageList.Count > 0)
                    {
                        List<dynamic> planCoveragesList = new List<dynamic>();

                        int i = 1;

                        foreach (var item in PlanCoverageList)
                        {
                            dynamic listItem = new System.Dynamic.ExpandoObject();

                            listItem.id = i;
                            listItem.ClientId = i;
                            listItem.STATUS = item.STATUS;

                            listItem.PLAN_COVERAGE_ID = Convert.ToString(item.PLAN_COVERAGE_ID);
                            listItem.COVERAGE_ID = Convert.ToString(item.COVERAGE_ID);
                            listItem.NETWORK = Convert.ToString(item.NETWORK_ID);

                            listItem.PARENT_ID = string.IsNullOrEmpty(Convert.ToString(item.PARENT_ID)) ? "-1" : Convert.ToString(item.PARENT_ID);
                            listItem.PLAN_COVERAGE_NAME = item.PLAN_COVERAGE_NAME;
                            listItem.LOCATION_TYPE = item.LOCATION_TYPE;
                            listItem.COVERAGE_IDSelectedText = item.COVERAGE_NAME;
                            listItem.NETWORKSelectedText = item.NETWORK_NAME;
                            listItem.IS_OPEN_TO_PROVIDER = item.IS_OPEN_TO_PROVIDER;
                            listItem.IS_OPEN_TO_PRINT = item.IS_OPEN_TO_PRINT;
                            listItem.isOpen = "0";

                            List<dynamic> childrenList = new List<dynamic>();

                            foreach (var childrenItem in PlanCoverageList.Where(p => p.PARENT_ID == item.PLAN_COVERAGE_ID))
                            {
                                dynamic children = new System.Dynamic.ExpandoObject();

                                children.id = i;
                                children.ClientId = i;
                                children.STATUS = childrenItem.STATUS;

                                children.PLAN_COVERAGE_ID = Convert.ToString(childrenItem.PLAN_COVERAGE_ID);
                                children.COVERAGE_ID = Convert.ToString(childrenItem.COVERAGE_ID);
                                children.NETWORK = Convert.ToString(childrenItem.NETWORK_ID);

                                children.PARENT_ID = string.IsNullOrEmpty(Convert.ToString(childrenItem.PARENT_ID)) ? "-1" : Convert.ToString(childrenItem.PARENT_ID);
                                children.PLAN_COVERAGE_NAME = childrenItem.PLAN_COVERAGE_NAME;
                                children.LOCATION_TYPE = childrenItem.LOCATION_TYPE;
                                children.COVERAGE_IDSelectedText = childrenItem.COVERAGE_NAME;
                                children.NETWORKSelectedText = childrenItem.NETWORK_NAME;
                                children.IS_OPEN_TO_PROVIDER = childrenItem.IS_OPEN_TO_PROVIDER;
                                children.IS_OPEN_TO_PRINT = childrenItem.IS_OPEN_TO_PRINT;
                                children.isOpen = "0";

                                childrenList.Add(children);
                            }
                            listItem.children = childrenList;

                            //planIdList.Add(Convert.ToString(item.PLAN_ID));
                            planCoveragesList.Add(listItem);

                            i++;
                        }
                        result.Add(planCoveragesList);
                    }

                    var CoverageVariation = new GenericRepository<V_PackagePlanCoverageVar>().FindBy("PACKAGE_ID=:packageId AND AGR_NEW_VERSION_ID IS NULL AND (AGR_STATUS IS NULL OR AGR_STATUS!=:Status) AND EMG_NEW_VERSION_ID IS NULL AND (EMG_STATUS IS NULL OR EMG_STATUS!=:Status) AND (NAG_STATUS IS NULL OR NAG_STATUS!=:Status) AND NAG_NEW_VERSION_ID IS NULL", orderby: "PLAN_COVERAGE_ID",
                                            parameters: new { packageId, Status = new DbString { Value = ((int)Status.SILINDI).ToString(), Length = 3 } },
                                           fetchHistoricRows: true,
                                           fetchDeletedRows: true);


                    if (CoverageVariation != null && CoverageVariation.Count > 0)
                    {
                        List<dynamic> listContractedVariation = new List<dynamic>();
                        List<dynamic> listNonAggrementVariation = new List<dynamic>();
                        List<dynamic> listUrgentVariation = new List<dynamic>();
                        var list = new List<List<dynamic>>();

                        foreach (var variation in CoverageVariation)
                        {
                            //AGR
                            dynamic variationListItem = new System.Dynamic.ExpandoObject();

                            if (variation.AGR_AGREEMENT_TYPE != null)
                            {
                                variationListItem.Type = "AGR";
                                variationListItem.AGR_ID = variation.AGR_ID;
                                variationListItem.PLAN_COVERAGE_ID = variation.PLAN_COVERAGE_ID;
                                variationListItem.AGR_AGREEMENT_TYPE = variation.AGR_AGREEMENT_TYPE;
                                variationListItem.AGR_PRICE_LIMIT = variation.AGR_PRICE_LIMIT;
                                variationListItem.AGR_PRICE_FACTOR = variation.AGR_PRICE_FACTOR;
                                variationListItem.AGR_CURRENCY_ID = variation.AGR_CURRENCY_ID;
                                variationListItem.AGR_NUMBER_LIMIT = variation.AGR_NUMBER_LIMIT;
                                variationListItem.AGR_DAY_LIMIT = variation.AGR_DAY_LIMIT;
                                variationListItem.AGR_COINSURANCE_RATIO = variation.AGR_COINSURANCE_RATIO;
                                variationListItem.AGR_EXEMPTION_TYPE = variation.AGR_EXEMPTION_TYPE;
                                variationListItem.AGR_EXEMPTION_LIMIT = variation.AGR_EXEMPTION_LIMIT;
                                variationListItem.AGR_SESSION_COUNT = variation.AGR_SESSION_COUNT;

                                listContractedVariation.Add(variationListItem);
                            }
                            //NAG
                            variationListItem = new System.Dynamic.ExpandoObject();

                            if (variation.NAG_AGREEMENT_TYPE != null)
                            {
                                variationListItem.Type = "NAG";
                                variationListItem.NAG_ID = variation.NAG_ID;
                                variationListItem.PLAN_COVERAGE_ID = variation.PLAN_COVERAGE_ID;
                                variationListItem.NAG_AGREEMENT_TYPE = variation.NAG_AGREEMENT_TYPE;
                                variationListItem.NAG_PRICE_LIMIT = variation.NAG_PRICE_LIMIT;
                                variationListItem.NAG_PRICE_FACTOR = variation.NAG_PRICE_FACTOR;
                                variationListItem.NAG_CURRENCY_ID = variation.NAG_CURRENCY_ID;
                                variationListItem.NAG_NUMBER_LIMIT = variation.NAG_NUMBER_LIMIT;
                                variationListItem.NAG_DAY_LIMIT = variation.NAG_DAY_LIMIT;
                                variationListItem.NAG_COINSURANCE_RATIO = variation.NAG_COINSURANCE_RATIO;
                                variationListItem.NAG_EXEMPTION_TYPE = variation.NAG_EXEMPTION_TYPE;
                                variationListItem.NAG_EXEMPTION_LIMIT = variation.NAG_EXEMPTION_LIMIT;
                                variationListItem.NAG_SESSION_COUNT = variation.NAG_SESSION_COUNT;

                                listNonAggrementVariation.Add(variationListItem);
                            }
                            //EMG
                            variationListItem = new System.Dynamic.ExpandoObject();

                            if (variation.EMG_AGREEMENT_TYPE != null)
                            {
                                variationListItem.Type = "EMG";
                                variationListItem.EMG_ID = variation.EMG_ID;
                                variationListItem.PLAN_COVERAGE_ID = variation.PLAN_COVERAGE_ID;
                                variationListItem.EMG_AGREEMENT_TYPE = variation.EMG_AGREEMENT_TYPE;
                                variationListItem.EMG_PRICE_LIMIT = variation.EMG_PRICE_LIMIT;
                                variationListItem.EMG_PRICE_FACTOR = variation.EMG_PRICE_FACTOR;
                                variationListItem.EMG_CURRENCY_ID = variation.EMG_CURRENCY_ID;
                                variationListItem.EMG_NUMBER_LIMIT = variation.EMG_NUMBER_LIMIT;
                                variationListItem.EMG_DAY_LIMIT = variation.EMG_DAY_LIMIT;
                                variationListItem.EMG_COINSURANCE_RATIO = variation.EMG_COINSURANCE_RATIO;
                                variationListItem.EMG_EXEMPTION_TYPE = variation.EMG_EXEMPTION_TYPE;
                                variationListItem.EMG_EXEMPTION_LIMIT = variation.EMG_EXEMPTION_LIMIT;
                                variationListItem.EMG_SESSION_COUNT = variation.EMG_SESSION_COUNT;

                                listUrgentVariation.Add(variationListItem);
                            }
                        }
                        result.Add(listContractedVariation);
                        result.Add(listNonAggrementVariation);
                        result.Add(listUrgentVariation);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SagmerPlanCoverageSave(FormCollection form)
        {
            var result = new List<List<dynamic>>();
            try
            {
                var packageId = form["hdPackageId"];
                var sagmerData = form["hdSagmerCovereTypeList"];
                List<dynamic> sagmerList = new List<dynamic>();
                sagmerList = JsonConvert.DeserializeObject<List<dynamic>>(sagmerData);

                foreach (var item in sagmerList)
                {
                    if (item.isOpen == "1")
                    {
                        long parameterId = long.Parse(Convert.ToString(item.PARAMETER_ID));
                        var parameter = new Parameter
                        {
                            Id = parameterId,
                            SystemType = ((int)SystemType.SAGMER).ToString(), //Sagmer
                            Key = AppKeys.SagmerCoverage,
                            Value = item.SAGMER_COVERAGE
                        };
                        var spResponseParameter = new ParameterRepository().Insert(parameter);
                        if (spResponseParameter.Code == "100")
                        {
                            parameterId = spResponseParameter.PkId;

                            var planCoverageParameterId = Convert.ToInt64(item.PLAN_COVERAGE_PARAMETER_ID);
                            var planCoverageId = Convert.ToInt64(item.PLAN_COVERAGE_ID);

                            var packageParameter = new PlanCoverageParameter
                            {
                                Id = planCoverageParameterId,
                                PlanCoverageId = planCoverageId,
                                ParameterId = parameterId
                            };
                            var spResponsePlanCoverageParameter = new PlanCoverageParameterRepository().Insert(packageParameter);
                            if (spResponsePlanCoverageParameter.Code != "100")
                                throw new Exception(spResponsePlanCoverageParameter.Code + " : " + spResponsePlanCoverageParameter.Message);
                        }
                        else
                        {
                            throw new Exception(spResponseParameter.Code + " : " + spResponseParameter.Message);
                        }
                    }
                }
                var SagmerPlanCoverageList = new GenericRepository<V_PlanCoverageParameter>().FindBy("PACKAGE_ID=packageId AND KEY=:KEY",
                                                                orderby: "PLAN_COVERAGE_ID", parameters: new { KEY = new DbString { Value = AppKeys.SagmerCoverage, Length = 30 }, packageId });

                if (SagmerPlanCoverageList != null && SagmerPlanCoverageList.Count > 0)
                {
                    List<dynamic> sagmerPlanCoveragesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in SagmerPlanCoverageList)
                    {
                        SagmerCoverage sagmerCoverage = new GenericRepository<SagmerCoverage>().FindById(long.Parse(Convert.ToString(item.VALUE)));
                        dynamic listItem = new System.Dynamic.ExpandoObject();
                        if (sagmerCoverage != null)
                        {
                            listItem.id = i;
                            listItem.ClientId = i;
                            listItem.STATUS = item.STATUS;

                            listItem.PLAN_COVERAGE_ID = Convert.ToString(item.PLAN_COVERAGE_ID);
                            listItem.PARAMETER_ID = Convert.ToString(item.PARAMETER_ID);
                            listItem.PLAN_COVERAGE_PARAMETER_ID = Convert.ToString(item.PLAN_COVERAGE_PARAMETER_ID);

                            listItem.SAGMER_COVERAGE_TYPE = Convert.ToString(sagmerCoverage.SagmerCoverageTypeId);
                            listItem.SAGMER_COVERAGE = item.VALUE;
                            listItem.isOpen = "0";

                            sagmerPlanCoveragesList.Add(listItem);

                            i++;
                        }
                    }
                    result.Add(sagmerPlanCoveragesList);
                }
            }

            catch (Exception ex)
            {
                result = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult PackageVariationSave(FormCollection form)
        {
            var result = "[]";

            try
            {
                var planCoverageId = long.Parse(form["selectedPlanCoverage"]);
                var packageId = Request["packageId"];
                var type = Request["Type"];

                if (type == "AGR")
                {
                    string cID = form["AGR_ID"];
                    string cAGREEMENT_TYPE = form["AGR_AGREEMENT_TYPE"];
                    string cPRICE_LIMIT = string.IsNullOrEmpty(form["AGR_PRICE_LIMIT"]) ? "9999999999" : form["AGR_PRICE_LIMIT"];
                    string cPRICE_FACTOR = form["AGR_PRICE_FACTOR"];
                    string cCURRENCY_TYPE = form["AGR_CURRENCY_ID"];
                    string cNUMBER_LIMIT = form["AGR_NUMBER_LIMIT"];
                    string cDAY_LIMIT = form["AGR_DAY_LIMIT"];
                    string cCOINSURANCE_RATIO = form["AGR_COINSURANCE_RATIO"];
                    string cEXEMPTION_TYPE = form["AGR_EXEMPTION_TYPE"];
                    string cEXEMPTION_LIMIT = form["AGR_EXEMPTION_LIMIT"];
                    string cSESSION_COUNT = form["AGR_SESSION_COUNT"];


                    var TplanCoverageVariation = new PlanCoverageVariation
                    {
                        Id = string.IsNullOrEmpty(cID) ? 0 : long.Parse(cID),
                        Type = ((int)PlanCoverageVariationType.ANLASMALI_KURUM).ToString(),
                        PlanCoverageId = planCoverageId,
                        AgreementType = cAGREEMENT_TYPE,
                        PriceLimit = String.IsNullOrEmpty(cPRICE_LIMIT) ? null : (decimal?)decimal.Parse(cPRICE_LIMIT),
                        PriceFactor = String.IsNullOrEmpty(cPRICE_FACTOR) ? null : (decimal?)decimal.Parse(cPRICE_FACTOR),
                        NumberLimit = String.IsNullOrEmpty(cNUMBER_LIMIT) ? null : (int?)int.Parse(cNUMBER_LIMIT),
                        DayLimit = String.IsNullOrEmpty(cDAY_LIMIT) ? null : (int?)int.Parse(cDAY_LIMIT),
                        CoinsuranceRatio = String.IsNullOrEmpty(cCOINSURANCE_RATIO) ? null : (int?)int.Parse(cCOINSURANCE_RATIO),
                        ExemptionType = cEXEMPTION_TYPE,
                        ExemptionLimit = String.IsNullOrEmpty(cEXEMPTION_LIMIT) ? null : (int?)int.Parse(cEXEMPTION_LIMIT),
                        CurrencyType = cCURRENCY_TYPE,
                        SessionCount = String.IsNullOrEmpty(cSESSION_COUNT) ? null : (int?)int.Parse(cSESSION_COUNT)
                    };

                    var spResponseVariation = new PlanCoverageVariationRepository().Insert(TplanCoverageVariation);
                    if (spResponseVariation.Code == "100")
                    {
                        if (form["checkNAG"] == "1")
                        {
                            TplanCoverageVariation.Id = string.IsNullOrEmpty(form["NAG_ID"]) ? 0 : long.Parse(form["NAG_ID"]);
                            TplanCoverageVariation.Type = ((int)PlanCoverageVariationType.ANLASMASIZ_KURUM).ToString();
                            spResponseVariation = new PlanCoverageVariationRepository().Insert(TplanCoverageVariation);
                            if (spResponseVariation.Code != "100")
                                throw new Exception(spResponseVariation.Code + " : " + spResponseVariation.Message);
                        }
                        if (form["checkEMG"] == "1")
                        {
                            TplanCoverageVariation.Id = string.IsNullOrEmpty(form["EMG_ID"]) ? 0 : long.Parse(form["EMG_ID"]);
                            TplanCoverageVariation.Type = ((int)PlanCoverageVariationType.ACIL).ToString();
                            spResponseVariation = new PlanCoverageVariationRepository().Insert(TplanCoverageVariation);
                            if (spResponseVariation.Code != "100")
                                throw new Exception(spResponseVariation.Code + " : " + spResponseVariation.Message);
                        }
                    }
                    else
                    {
                        throw new Exception(spResponseVariation.Code + " : " + spResponseVariation.Message);
                    }
                }

                else if (type == "NAG")
                {
                    string nID = form["NAG_ID"];
                    string nAGREEMENT_TYPE = form["NAG_AGREEMENT_TYPE"];
                    string nPRICE_LIMIT = string.IsNullOrEmpty(form["NAG_PRICE_LIMIT"]) ? "9999999999" : form["NAG_PRICE_LIMIT"];
                    string nPRICE_FACTOR = form["NAG_PRICE_FACTOR"];
                    string nCURRENCY_TYPE = form["NAG_CURRENCY_ID"];
                    string nNUMBER_LIMIT = form["NAG_NUMBER_LIMIT"];
                    string nDAY_LIMIT = form["NAG_DAY_LIMIT"];
                    string nCOINSURANCE_RATIO = form["NAG_COINSURANCE_RATIO"];
                    string nEXEMPTION_TYPE = form["NAG_EXEMPTION_TYPE"];
                    string nEXEMPTION_LIMIT = form["NAG_EXEMPTION_LIMIT"];
                    string nSESSION_COUNT = form["NAG_SESSION_COUNT"];

                    var TplanCoverageVariation = new PlanCoverageVariation
                    {
                        Id = string.IsNullOrEmpty(nID) ? 0 : long.Parse(nID),
                        Type = ((int)PlanCoverageVariationType.ANLASMASIZ_KURUM).ToString(),
                        PlanCoverageId = planCoverageId,
                        AgreementType = nAGREEMENT_TYPE,
                        PriceLimit = String.IsNullOrEmpty(nPRICE_LIMIT) ? null : (decimal?)decimal.Parse(nPRICE_LIMIT),
                        PriceFactor = String.IsNullOrEmpty(nPRICE_FACTOR) ? null : (decimal?)decimal.Parse(nPRICE_FACTOR),
                        NumberLimit = String.IsNullOrEmpty(nNUMBER_LIMIT) ? null : (int?)int.Parse(nNUMBER_LIMIT),
                        DayLimit = String.IsNullOrEmpty(nDAY_LIMIT) ? null : (int?)int.Parse(nDAY_LIMIT),
                        CoinsuranceRatio = String.IsNullOrEmpty(nCOINSURANCE_RATIO) ? null : (int?)int.Parse(nCOINSURANCE_RATIO),
                        ExemptionType = nEXEMPTION_TYPE,
                        ExemptionLimit = String.IsNullOrEmpty(nEXEMPTION_LIMIT) ? null : (int?)int.Parse(nEXEMPTION_LIMIT),
                        CurrencyType = nCURRENCY_TYPE,
                        SessionCount = String.IsNullOrEmpty(nSESSION_COUNT) ? null : (int?)int.Parse(nSESSION_COUNT)

                    };
                    var spResponseVariation = new PlanCoverageVariationRepository().Insert(TplanCoverageVariation);
                    if (spResponseVariation.Code != "100")
                    {
                        throw new Exception(spResponseVariation.Code + " : " + spResponseVariation.Message);
                    }
                }

                else if (type == "EMG")
                {
                    string uID = form["EMG_ID"];
                    string uAGREEMENT_TYPE = form["EMG_AGREEMENT_TYPE"];
                    string uPRICE_LIMIT = string.IsNullOrEmpty(form["EMG_PRICE_LIMIT"]) ? "9999999999" : form["EMG_PRICE_LIMIT"];
                    string uPRICE_FACTOR = form["EMG_PRICE_FACTOR"];
                    string uCURRENCY_TYPE = form["EMG_CURRENCY_ID"];
                    string uNUMBER_LIMIT = form["EMG_NUMBER_LIMIT"];
                    string uDAY_LIMIT = form["EMG_DAY_LIMIT"];
                    string uCOINSURANCE_RATIO = form["EMG_COINSURANCE_RATIO"];
                    string uEXEMPTION_TYPE = form["EMG_EXEMPTION_TYPE"];
                    string uEXEMPTION_LIMIT = form["EMG_EXEMPTION_LIMIT"];
                    string uSESSION_COUNT = form["EMG_SESSION_COUNT"];


                    var TplanCoverageVariation = new PlanCoverageVariation
                    {
                        Id = string.IsNullOrEmpty(uID) ? 0 : long.Parse(uID),
                        Type = ((int)PlanCoverageVariationType.ACIL).ToString(),
                        PlanCoverageId = planCoverageId,
                        AgreementType = uAGREEMENT_TYPE,
                        PriceLimit = String.IsNullOrEmpty(uPRICE_LIMIT) ? null : (decimal?)decimal.Parse(uPRICE_LIMIT),
                        PriceFactor = String.IsNullOrEmpty(uPRICE_FACTOR) ? null : (decimal?)decimal.Parse(uPRICE_FACTOR),
                        NumberLimit = String.IsNullOrEmpty(uPRICE_FACTOR) ? null : (int?)int.Parse(uPRICE_FACTOR),
                        DayLimit = String.IsNullOrEmpty(uPRICE_FACTOR) ? null : (int?)int.Parse(uPRICE_FACTOR),
                        CoinsuranceRatio = String.IsNullOrEmpty(uPRICE_FACTOR) ? null : (int?)int.Parse(uPRICE_FACTOR),
                        ExemptionType = uEXEMPTION_TYPE,
                        ExemptionLimit = String.IsNullOrEmpty(uPRICE_FACTOR) ? null : (int?)int.Parse(uPRICE_FACTOR),
                        CurrencyType = uCURRENCY_TYPE,
                        SessionCount = String.IsNullOrEmpty(uSESSION_COUNT) ? null : (int?)int.Parse(uSESSION_COUNT)
                    };
                    var spResponseVariation = new PlanCoverageVariationRepository().Insert(TplanCoverageVariation);
                    if (spResponseVariation.Code != "100")
                    {
                        throw new Exception(spResponseVariation.Code + " : " + spResponseVariation.Message);
                    }
                }

                var CoverageVariation = new GenericRepository<V_PackagePlanCoverageVar>().FindBy("PACKAGE_ID=:packageId AND AGR_NEW_VERSION_ID IS NULL AND (AGR_STATUS IS NULL OR AGR_STATUS!=:Status) AND EMG_NEW_VERSION_ID IS NULL AND (EMG_STATUS IS NULL OR EMG_STATUS!=:Status) AND (NAG_STATUS IS NULL OR NAG_STATUS!=:Status) AND NAG_NEW_VERSION_ID IS NULL", orderby: "PLAN_COVERAGE_ID",
                                            parameters: new { packageId, Status = new DbString { Value = ((int)Status.SILINDI).ToString(), Length = 3 } },
                                                            fetchHistoricRows: true,
                                                            fetchDeletedRows: true);

                if (CoverageVariation != null && CoverageVariation.Count > 0)
                {
                    List<dynamic> listContractedVariation = new List<dynamic>();
                    List<dynamic> listNonAggrementVariation = new List<dynamic>();
                    List<dynamic> listUrgentVariation = new List<dynamic>();
                    var list = new List<List<dynamic>>();

                    foreach (var variation in CoverageVariation)
                    {
                        //AGR
                        dynamic variationListItem = new System.Dynamic.ExpandoObject();

                        if (variation.AGR_AGREEMENT_TYPE != null)
                        {
                            variationListItem.Type = "AGR";
                            variationListItem.AGR_ID = variation.AGR_ID;
                            variationListItem.PLAN_COVERAGE_ID = variation.PLAN_COVERAGE_ID;
                            variationListItem.AGR_AGREEMENT_TYPE = variation.AGR_AGREEMENT_TYPE;
                            variationListItem.AGR_PRICE_LIMIT = variation.AGR_PRICE_LIMIT;
                            variationListItem.AGR_PRICE_FACTOR = variation.AGR_PRICE_FACTOR;
                            variationListItem.AGR_CURRENCY_ID = variation.AGR_CURRENCY_ID;
                            variationListItem.AGR_NUMBER_LIMIT = variation.AGR_NUMBER_LIMIT;
                            variationListItem.AGR_DAY_LIMIT = variation.AGR_DAY_LIMIT;
                            variationListItem.AGR_COINSURANCE_RATIO = variation.AGR_COINSURANCE_RATIO;
                            variationListItem.AGR_EXEMPTION_TYPE = variation.AGR_EXEMPTION_TYPE;
                            variationListItem.AGR_EXEMPTION_LIMIT = variation.AGR_EXEMPTION_LIMIT;
                            variationListItem.AGR_SESSION_COUNT = variation.AGR_SESSION_COUNT;

                            listContractedVariation.Add(variationListItem);
                        }
                        //NAG
                        variationListItem = new System.Dynamic.ExpandoObject();

                        if (variation.NAG_AGREEMENT_TYPE != null)
                        {
                            variationListItem.Type = "NAG";
                            variationListItem.NAG_ID = variation.NAG_ID;
                            variationListItem.PLAN_COVERAGE_ID = variation.PLAN_COVERAGE_ID;
                            variationListItem.NAG_AGREEMENT_TYPE = variation.NAG_AGREEMENT_TYPE;
                            variationListItem.NAG_PRICE_LIMIT = variation.NAG_PRICE_LIMIT;
                            variationListItem.NAG_PRICE_FACTOR = variation.NAG_PRICE_FACTOR;
                            variationListItem.NAG_CURRENCY_ID = variation.NAG_CURRENCY_ID;
                            variationListItem.NAG_NUMBER_LIMIT = variation.NAG_NUMBER_LIMIT;
                            variationListItem.NAG_DAY_LIMIT = variation.NAG_DAY_LIMIT;
                            variationListItem.NAG_COINSURANCE_RATIO = variation.NAG_COINSURANCE_RATIO;
                            variationListItem.NAG_EXEMPTION_TYPE = variation.NAG_EXEMPTION_TYPE;
                            variationListItem.NAG_EXEMPTION_LIMIT = variation.NAG_EXEMPTION_LIMIT;
                            variationListItem.NAG_SESSION_COUNT = variation.NAG_SESSION_COUNT;

                            listNonAggrementVariation.Add(variationListItem);
                        }
                        //EMG
                        variationListItem = new System.Dynamic.ExpandoObject();

                        if (variation.EMG_AGREEMENT_TYPE != null)
                        {
                            variationListItem.Type = "EMG";
                            variationListItem.EMG_ID = variation.EMG_ID;
                            variationListItem.PLAN_COVERAGE_ID = variation.PLAN_COVERAGE_ID;
                            variationListItem.EMG_AGREEMENT_TYPE = variation.EMG_AGREEMENT_TYPE;
                            variationListItem.EMG_PRICE_LIMIT = variation.EMG_PRICE_LIMIT;
                            variationListItem.EMG_PRICE_FACTOR = variation.EMG_PRICE_FACTOR;
                            variationListItem.EMG_CURRENCY_ID = variation.EMG_CURRENCY_ID;
                            variationListItem.EMG_NUMBER_LIMIT = variation.EMG_NUMBER_LIMIT;
                            variationListItem.EMG_DAY_LIMIT = variation.EMG_DAY_LIMIT;
                            variationListItem.EMG_COINSURANCE_RATIO = variation.EMG_COINSURANCE_RATIO;
                            variationListItem.EMG_EXEMPTION_TYPE = variation.EMG_EXEMPTION_TYPE;
                            variationListItem.EMG_EXEMPTION_LIMIT = variation.EMG_EXEMPTION_LIMIT;
                            variationListItem.EMG_SESSION_COUNT = variation.EMG_SESSION_COUNT;

                            listUrgentVariation.Add(variationListItem);
                        }
                    }
                    list.Add(listContractedVariation);
                    list.Add(listNonAggrementVariation);
                    list.Add(listUrgentVariation);

                    result = list.ToJSON();
                }
            }
            catch (Exception ex)
            {
                result = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult DeletePackagePlanCoverage(FormCollection form)
        {
            var result = new List<List<dynamic>>();

            var PlanCoverages = form["hdTreeData"];
            var packageId = long.Parse(Request["packageId"]);
            try
            {
                List<dynamic> planCoverageList = new List<dynamic>();
                planCoverageList = JsonConvert.DeserializeObject<List<dynamic>>(PlanCoverages);

                foreach (var planCoverage in planCoverageList)
                {
                    if (planCoverage.isOpen == "1")
                    {
                        var TplanCoverage = new PlanCoverageRepository().FindById(long.Parse(Convert.ToString(planCoverage.PLAN_COVERAGE_ID)));
                        if (TplanCoverage != null)
                        {
                            TplanCoverage.Status = ((int)Status.SILINDI).ToString();

                            var spResponsePlanCoverage = new PlanCoverageRepository().Insert(TplanCoverage);
                            if (spResponsePlanCoverage.Code == "100")
                            {
                                var planCoverageId = spResponsePlanCoverage.PkId;
                                var variations = new PlanCoverageVariationRepository().FindBy($"PLAN_COVERAGE_ID={planCoverage.PLAN_COVERAGE_ID}");
                                if (variations != null)
                                {
                                    foreach (var variation in variations)
                                    {
                                        variation.Status = ((int)Status.SILINDI).ToString();
                                        var spResponseVariation = new PlanCoverageVariationRepository().Update(variation);
                                        if (spResponseVariation.Code != "100")
                                        {
                                            throw new Exception(spResponsePlanCoverage.Code + " : " + spResponsePlanCoverage.Message);
                                        }
                                    }
                                }
                                //Sub Level Plan Coverage and Variation Delete
                                var subPlanCoverage = new PlanCoverageRepository().FindBy($"PARENT_ID={planCoverageId}");
                                foreach (var item in subPlanCoverage)
                                {
                                    item.Status = ((int)Status.SILINDI).ToString();
                                    spResponsePlanCoverage = new PlanCoverageRepository().Update(item);
                                    if (spResponsePlanCoverage.Code == "100")
                                    {
                                        var subPlanCoverageVariation = new PlanCoverageVariationRepository().FindBy($"PLAN_COVERAGE_ID={spResponsePlanCoverage.PkId}");
                                        foreach (var vari in subPlanCoverageVariation)
                                        {
                                            vari.Status = ((int)Status.SILINDI).ToString();
                                            var spResponsePlanCoverageVar = new PlanCoverageVariationRepository().Update(vari);
                                            if (spResponsePlanCoverageVar.Code != "100")
                                            {
                                                throw new Exception(spResponsePlanCoverageVar.Code + " : " + spResponsePlanCoverageVar.Message);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        throw new Exception(spResponsePlanCoverage.Code + " : " + spResponsePlanCoverage.Message);
                                    }
                                }
                            }
                            else
                            {
                                throw new Exception(spResponsePlanCoverage.Code + " : " + spResponsePlanCoverage.Message);
                            }
                        }
                    }
                }

                var PlanCoverageList = new GenericRepository<V_PlanCoverage>().FindBy("PACKAGE_ID=:packageId", orderby: "PLAN_COVERAGE_ID ASC", parameters: new { packageId });

                if (PlanCoverageList != null && PlanCoverageList.Count > 0)
                {
                    List<dynamic> planCoveragesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in PlanCoverageList.Where(p => p.PARENT_ID == null))
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.PLAN_COVERAGE_ID = Convert.ToString(item.PLAN_COVERAGE_ID);
                        listItem.COVERAGE_ID = Convert.ToString(item.COVERAGE_ID);
                        listItem.NETWORK = Convert.ToString(item.NETWORK_ID);

                        listItem.PARENT_ID = string.IsNullOrEmpty(Convert.ToString(item.PARENT_ID)) ? "-1" : Convert.ToString(item.PARENT_ID);
                        listItem.PLAN_COVERAGE_NAME = item.PLAN_COVERAGE_NAME;
                        listItem.LOCATION_TYPE = item.LOCATION_TYPE;
                        listItem.COVERAGE_IDSelectedText = item.COVERAGE_NAME;
                        listItem.NETWORKSelectedText = item.NETWORK_NAME;
                        listItem.IS_OPEN_TO_PROVIDER = item.IS_OPEN_TO_PROVIDER;
                        listItem.IS_OPEN_TO_PRINT = item.IS_OPEN_TO_PRINT;
                        listItem.isOpen = "0";

                        List<dynamic> childrenList = new List<dynamic>();

                        foreach (var childrenItem in PlanCoverageList.Where(p => p.PARENT_ID == item.PLAN_COVERAGE_ID))
                        {
                            dynamic children = new System.Dynamic.ExpandoObject();

                            children.id = i;
                            children.ClientId = i;
                            children.STATUS = childrenItem.STATUS;

                            children.PLAN_COVERAGE_ID = Convert.ToString(childrenItem.PLAN_COVERAGE_ID);
                            children.COVERAGE_ID = Convert.ToString(childrenItem.COVERAGE_ID);
                            children.NETWORK = Convert.ToString(childrenItem.NETWORK_ID);

                            children.PARENT_ID = string.IsNullOrEmpty(Convert.ToString(childrenItem.PARENT_ID)) ? "-1" : Convert.ToString(childrenItem.PARENT_ID);
                            children.PLAN_COVERAGE_NAME = childrenItem.PLAN_COVERAGE_NAME;
                            children.LOCATION_TYPE = childrenItem.LOCATION_TYPE;
                            children.COVERAGE_IDSelectedText = childrenItem.COVERAGE_NAME;
                            children.NETWORKSelectedText = childrenItem.NETWORK_NAME;
                            children.IS_OPEN_TO_PROVIDER = childrenItem.IS_OPEN_TO_PROVIDER;
                            children.IS_OPEN_TO_PRINT = childrenItem.IS_OPEN_TO_PRINT;
                            children.isOpen = "0";

                            childrenList.Add(children);
                        }
                        listItem.children = childrenList;

                        //planIdList.Add(Convert.ToString(item.PLAN_ID));
                        planCoveragesList.Add(listItem);

                        i++;
                    }

                    result.Add(planCoveragesList);
                }

                var CoverageVariation = new GenericRepository<V_PackagePlanCoverageVar>().FindBy("PACKAGE_ID=:packageId AND AGR_NEW_VERSION_ID IS NULL AND (AGR_STATUS IS NULL OR AGR_STATUS!=:Status) AND EMG_NEW_VERSION_ID IS NULL AND (EMG_STATUS IS NULL OR EMG_STATUS!=:Status) AND (NAG_STATUS IS NULL OR NAG_STATUS!=:Status) AND NAG_NEW_VERSION_ID IS NULL", orderby: "PLAN_COVERAGE_ID",
                                            parameters: new { packageId, Status = new DbString { Value = ((int)Status.SILINDI).ToString(), Length = 3 } },
                                       fetchHistoricRows: true,
                                       fetchDeletedRows: true);

                if (CoverageVariation != null && CoverageVariation.Count > 0)
                {
                    List<dynamic> listContractedVariation = new List<dynamic>();
                    List<dynamic> listNonAggrementVariation = new List<dynamic>();
                    List<dynamic> listUrgentVariation = new List<dynamic>();
                    var list = new List<List<dynamic>>();

                    foreach (var variation in CoverageVariation)
                    {
                        //AGR
                        dynamic variationListItem = new System.Dynamic.ExpandoObject();

                        if (variation.AGR_AGREEMENT_TYPE != null)
                        {
                            variationListItem.Type = "AGR";
                            variationListItem.AGR_ID = variation.AGR_ID;
                            variationListItem.PLAN_COVERAGE_ID = variation.PLAN_COVERAGE_ID;
                            variationListItem.AGR_AGREEMENT_TYPE = variation.AGR_AGREEMENT_TYPE;
                            variationListItem.AGR_PRICE_LIMIT = variation.AGR_PRICE_LIMIT;
                            variationListItem.AGR_PRICE_FACTOR = variation.AGR_PRICE_FACTOR;
                            variationListItem.AGR_CURRENCY_ID = variation.AGR_CURRENCY_ID;
                            variationListItem.AGR_NUMBER_LIMIT = variation.AGR_NUMBER_LIMIT;
                            variationListItem.AGR_DAY_LIMIT = variation.AGR_DAY_LIMIT;
                            variationListItem.AGR_COINSURANCE_RATIO = variation.AGR_COINSURANCE_RATIO;
                            variationListItem.AGR_EXEMPTION_TYPE = variation.AGR_EXEMPTION_TYPE;
                            variationListItem.AGR_EXEMPTION_LIMIT = variation.AGR_EXEMPTION_LIMIT;
                            variationListItem.AGR_SESSION_COUNT = variation.AGR_SESSION_COUNT;

                            listContractedVariation.Add(variationListItem);
                        }
                        //NAG
                        variationListItem = new System.Dynamic.ExpandoObject();

                        if (variation.NAG_AGREEMENT_TYPE != null)
                        {
                            variationListItem.Type = "NAG";
                            variationListItem.NAG_ID = variation.NAG_ID;
                            variationListItem.PLAN_COVERAGE_ID = variation.PLAN_COVERAGE_ID;
                            variationListItem.NAG_AGREEMENT_TYPE = variation.NAG_AGREEMENT_TYPE;
                            variationListItem.NAG_PRICE_LIMIT = variation.NAG_PRICE_LIMIT;
                            variationListItem.NAG_PRICE_FACTOR = variation.NAG_PRICE_FACTOR;
                            variationListItem.NAG_CURRENCY_ID = variation.NAG_CURRENCY_ID;
                            variationListItem.NAG_NUMBER_LIMIT = variation.NAG_NUMBER_LIMIT;
                            variationListItem.NAG_DAY_LIMIT = variation.NAG_DAY_LIMIT;
                            variationListItem.NAG_COINSURANCE_RATIO = variation.NAG_COINSURANCE_RATIO;
                            variationListItem.NAG_EXEMPTION_TYPE = variation.NAG_EXEMPTION_TYPE;
                            variationListItem.NAG_EXEMPTION_LIMIT = variation.NAG_EXEMPTION_LIMIT;
                            variationListItem.NAG_SESSION_COUNT = variation.NAG_SESSION_COUNT;

                            listNonAggrementVariation.Add(variationListItem);
                        }
                        //EMG
                        variationListItem = new System.Dynamic.ExpandoObject();

                        if (variation.EMG_AGREEMENT_TYPE != null)
                        {
                            variationListItem.Type = "EMG";
                            variationListItem.EMG_ID = variation.EMG_ID;
                            variationListItem.PLAN_COVERAGE_ID = variation.PLAN_COVERAGE_ID;
                            variationListItem.EMG_AGREEMENT_TYPE = variation.EMG_AGREEMENT_TYPE;
                            variationListItem.EMG_PRICE_LIMIT = variation.EMG_PRICE_LIMIT;
                            variationListItem.EMG_PRICE_FACTOR = variation.EMG_PRICE_FACTOR;
                            variationListItem.EMG_CURRENCY_ID = variation.EMG_CURRENCY_ID;
                            variationListItem.EMG_NUMBER_LIMIT = variation.EMG_NUMBER_LIMIT;
                            variationListItem.EMG_DAY_LIMIT = variation.EMG_DAY_LIMIT;
                            variationListItem.EMG_COINSURANCE_RATIO = variation.EMG_COINSURANCE_RATIO;
                            variationListItem.EMG_EXEMPTION_TYPE = variation.EMG_EXEMPTION_TYPE;
                            variationListItem.EMG_EXEMPTION_LIMIT = variation.EMG_EXEMPTION_LIMIT;
                            variationListItem.EMG_SESSION_COUNT = variation.EMG_SESSION_COUNT;

                            listUrgentVariation.Add(variationListItem);
                        }
                    }
                    result.Add(listContractedVariation);
                    result.Add(listNonAggrementVariation);
                    result.Add(listUrgentVariation);
                }
            }
            catch (Exception ex)
            {
                result = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult PackageSaveStep1(FormCollection form)
        {
            var result = new AjaxResultDto<PackageSaveStep1Result>();
            PackageSaveStep1Result step1Result = new PackageSaveStep1Result();
            try
            {
                var packageId = long.Parse(form["hdPackageId"]);
                var priceListId = long.Parse(form["hdPriceListId"]);
                var packageName = form["PACKAGE_NAME"];
                var packageNo = form["PACKAGE_NO"];
                var packageType = form["PACKAGE_TYPE"];

                var TpriceList = new PriceList
                {
                    Id = priceListId
                };
                var spResponsePriceList = new PriceListRepository().Insert(TpriceList);
                if (spResponsePriceList.Code == "100")
                {
                    step1Result.priceListId = spResponsePriceList.PkId;
                    var Tpackage = new Package
                    {
                        Id = packageId,
                        No = packageNo,
                        Name = packageName,
                        Type = packageType,
                        PriceListId = spResponsePriceList.PkId
                    };

                    var spResponsePackage = new PackageRepository().Insert(Tpackage);
                    if (spResponsePackage.Code == "100")
                    {
                        packageId = spResponsePackage.PkId;
                        step1Result.packageId = packageId;

                        var sagmerPackageType = form["sagmerPackageType"];
                        if (sagmerPackageType.IsInt64())
                        {
                            var packageTypeParameterId = long.Parse(form["hdPackageTypeParameterId"]);
                            var parameter = new Parameter
                            {
                                Id = packageTypeParameterId,
                                SystemType = ((int)SystemType.SAGMER).ToString(), //Sagmer
                                Environment = "D",
                                Key = AppKeys.SagmerPackageType,
                                Value = sagmerPackageType
                            };
                            var spResponseParameter = new ParameterRepository().Insert(parameter);
                            if (spResponseParameter.Code == "100")
                            {
                                var parameterId = spResponseParameter.PkId;
                                step1Result.packageTypeParameterId = parameterId;

                                var packageTypePackageParameterId = long.Parse(form["hdPackageTypePackageParameterId"]);
                                var packageParameter = new PackageParameter
                                {
                                    Id = packageTypePackageParameterId,
                                    PackageId = packageId,
                                    ParameterId = parameterId
                                };
                                var spResponsePackageParameter = new PackageParameterRepository().Insert(packageParameter);
                                if (spResponsePackageParameter.Code == "100")
                                    step1Result.packageTypePackageParameterId = spResponsePackageParameter.PkId;
                            }
                        }

                        var sagmerPlan = form["sagmerProduct"];
                        if (sagmerPlan.IsInt64())
                        {
                            var productParameterId = long.Parse(form["hdProductParameterId"]);

                            var parameter = new Parameter
                            {
                                Id = productParameterId,
                                SystemType = ((int)SystemType.SAGMER).ToString(), //Sagmer
                                Environment = "D",
                                Key = AppKeys.SagmerPlanId,
                                Value = sagmerPlan
                            };
                            var spResponseParameter = new ParameterRepository().Insert(parameter);
                            if (spResponseParameter.Code == "100")
                            {
                                var parameterId = spResponseParameter.PkId;
                                step1Result.productParameterId = parameterId;

                                var productPackageParameterId = long.Parse(form["hdProductPackageParameterId"]);
                                var packageParameter = new PackageParameter
                                {
                                    Id = productPackageParameterId,
                                    PackageId = packageId,
                                    ParameterId = parameterId
                                };
                                var spResponsePackageParameter = new PackageParameterRepository().Insert(packageParameter);
                                if (spResponsePackageParameter.Code == "100")
                                    step1Result.productPackageParameterId = spResponsePackageParameter.PkId;
                            }
                        }

                        var isTPA = form["packageIsTpaPrice"];
                        if (!String.IsNullOrEmpty(isTPA))
                        {
                            var tpaPriceId = long.Parse(form["hdTPAPriceId"]);
                            var cashPaymentType = form["PACKAGE_CASH_PAYMENT_TYPE"];
                            var amount = form["PACKAGE_TPA_PRICE_AMOUNT"];
                            var startDate = form["PACKAGE_TPA_PRICE_START_DATE"];

                            var packageTpaPrice = new PackageTPAPrice
                            {
                                Id = tpaPriceId,
                                PackageId = packageId,
                                Amount = decimal.Parse(amount),
                                Type = ((int)PackageTpaPriceType.ZEYL_TANZIM_TARIHINE_GORE).ToString(),
                                StartDate = DateTime.Parse(startDate),
                                CashPaymentType = cashPaymentType
                            };
                            var spResponseTpaPrice = new PackageTPAPriceRepository().Insert(packageTpaPrice);
                            if (spResponseTpaPrice.Code == "100")
                                step1Result.packageTpaPrice = spResponseTpaPrice.PkId;
                        }

                        result.Data = step1Result;
                        result.ResultCode = spResponsePackage.Code;
                        result.ResultMessage = spResponsePackage.Message;
                    }
                    else
                    {
                        throw new Exception(spResponsePackage.Code + " : " + spResponsePackage.Message);
                    }
                }
                else
                {
                    throw new Exception(spResponsePriceList.Code + " : " + spResponsePriceList.Message);
                }
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }

            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PackageSaveStep2(FormCollection form)
        {
            var result = new AjaxResultDto<PackageSaveStep2Result>();
            try
            {
                var packageId = long.Parse(form["hdPackageId"]);
                var planId = form["plan"];

                var coverages = form["hdTreeData"];
                List<dynamic> coverageList = new List<dynamic>();
                coverageList = JsonConvert.DeserializeObject<List<dynamic>>(coverages);

                var contractedData = form["hdContracted"];
                List<dynamic> contractedList = new List<dynamic>();
                contractedList = JsonConvert.DeserializeObject<List<dynamic>>(contractedData);

                var nonAgreementData = form["hdNonAgreement"];
                List<dynamic> nonAgreementList = new List<dynamic>();
                nonAgreementList = JsonConvert.DeserializeObject<List<dynamic>>(nonAgreementData);

                var urgentData = form["hdUrgent"];
                List<dynamic> urgentList = new List<dynamic>();
                urgentList = JsonConvert.DeserializeObject<List<dynamic>>(urgentData);

                var sagmerData = form["hdSagmerCovereTypeList"];
                List<dynamic> sagmerList = new List<dynamic>();
                sagmerList = JsonConvert.DeserializeObject<List<dynamic>>(sagmerData);

                var rootData = (from t in coverageList
                                where t.parentIndex == -1
                                group t by t.PLAN_ID into g
                                select new { Root = g.Key, Items = g.ToList() }).ToList();
                foreach (var rootItem in rootData)
                {
                    foreach (var item in rootItem.Items)
                    {
                        //Root item props
                        string rCOVERAGE_ID = item.COVERAGE_ID;
                        string rLOCATION_TYPE = item.LOCATION_TYPE;
                        string rCOVERAGE_NAME = item.COVERAGE_NAME;
                        string rNETWORK = item.NETWORK_ID;
                        string rID = item.PLAN_COVERAGE_ID;
                        string rSTATUS = item.STATUS;

                        var TplanCoverage = new PlanCoverage
                        {
                            Id = !String.IsNullOrEmpty(rID) ? long.Parse(rID) : 0,
                            PlanId = item.PLAN_ID,
                            PackageId = packageId,
                            CoverageId = long.Parse(rCOVERAGE_ID),
                            CoverageName = rCOVERAGE_NAME,
                            LocationType = rLOCATION_TYPE,
                            NetworkId = long.Parse(rNETWORK),
                            Status = rSTATUS
                        };
                        var spResponsePlanCoverage = new PlanCoverageRepository().Insert(TplanCoverage);
                        if (spResponsePlanCoverage.Code == "100")
                        {
                            var planCoverageId = spResponsePlanCoverage.PkId;

                            var sagmerCoverageType = (from s in sagmerList
                                                      where s.ID == item.ID && s.STATUS != "1"
                                                      select s).ToList().FirstOrDefault();
                            if (sagmerCoverageType != null)
                            {
                                long parameterId = long.Parse(Convert.ToString(sagmerCoverageType.PARAMETER_ID));
                                var parameter = new Parameter
                                {
                                    Id = parameterId,
                                    SystemType = "2", //Sagmer
                                    Environment = "D",
                                    Key = "COVERAGE_TYPE",
                                    Value = sagmerCoverageType.SAGMER_COVERAGE_TYPE
                                };
                                var spResponseParameter = new ParameterRepository().Insert(parameter);
                                if (spResponseParameter.Code == "100")
                                {
                                    parameterId = spResponseParameter.PkId;

                                    var planCoverageParameterId = long.Parse(Convert.ToString(sagmerCoverageType.PLAN_COVERAGE_PARAMETER_ID));
                                    var packageParameter = new PlanCoverageParameter
                                    {
                                        Id = planCoverageParameterId,
                                        PlanCoverageId = planCoverageId,
                                        ParameterId = parameterId
                                    };
                                    var spResponsePlanCoverageParameter = new PlanCoverageParameterRepository().Insert(packageParameter);
                                    if (spResponsePlanCoverageParameter.Code != "100")
                                        throw new Exception(spResponsePlanCoverageParameter.Code + " : " + spResponsePlanCoverageParameter.Message);
                                }
                                else
                                {
                                    throw new Exception(spResponseParameter.Code + " : " + spResponseParameter.Message);
                                }
                            }

                            var contracted = (from t in contractedList
                                              where t.hdplanCoverageId.ToString() == "root_" + item.index || t.hdplanCoverageId.ToString() == planCoverageId.ToString()
                                              select t).ToList();

                            var PlanCoverageVariationList = new GenericRepository<V_PlanCoverageVariation>().FindBy("PLAN_COVERAGE_ID=:planCoverageId AND AGR_NEW_VERSION_ID IS NULL AND (AGR_STATUS IS NULL OR AGR_STATUS!=:Status) AND EMG_NEW_VERSION_ID IS NULL AND (EMG_STATUS IS NULL OR EMG_STATUS!=:Status) AND (NAG_STATUS IS NULL OR NAG_STATUS!=:Status) AND NAG_NEW_VERSION_ID IS NULL", orderby: "PLAN_COVERAGE_ID", parameters: new { planCoverageId = item.ID, Status = new DbString { Value = ((int)Status.SILINDI).ToString(), Length = 3 } });
                            if (PlanCoverageVariationList.Count > 0)
                            {
                                var variation = PlanCoverageVariationList[PlanCoverageVariationList.Count - 1];

                                if (variation.AGR_AGREEMENT_TYPE != null)
                                {
                                    //Root item props
                                    string cAGREEMENT_TYPE = variation.AGR_AGREEMENT_TYPE != null ? variation.AGR_AGREEMENT_TYPE.ToString() : string.Empty;
                                    string cPRICE_LIMIT = variation.AGR_PRICE_LIMIT != null ? variation.AGR_PRICE_LIMIT.ToString() : string.Empty;
                                    string cPRICE_FACTOR = variation.AGR_PRICE_FACTOR != null ? variation.AGR_PRICE_FACTOR.ToString() : string.Empty;
                                    string cCURRENCY_TYPE = variation.AGR_CURRENCY_ID != null ? variation.AGR_CURRENCY_ID.ToString() : string.Empty;
                                    string cNUMBER_LIMIT = variation.AGR_NUMBER_LIMIT != null ? variation.AGR_NUMBER_LIMIT.ToString() : string.Empty;
                                    string cDAY_LIMIT = variation.AGR_DAY_LIMIT != null ? variation.AGR_DAY_LIMIT.ToString() : string.Empty;
                                    string cCOINSURANCE_RATIO = variation.AGR_COINSURANCE_RATIO != null ? variation.AGR_COINSURANCE_RATIO.ToString() : string.Empty;
                                    string cEXEMPTION_TYPE = variation.AGR_EXEMPTION_TYPE != null ? variation.AGR_EXEMPTION_TYPE.ToString() : string.Empty;
                                    string cEXEMPTION_LIMIT = variation.AGR_EXEMPTION_LIMIT != null ? variation.AGR_EXEMPTION_LIMIT.ToString() : string.Empty;
                                    string cSESSION_COUNT = variation.AGR_SESSION_COUNT != null ? variation.AGR_SESSION_COUNT.ToString() : string.Empty;


                                    var TplanCoverageVariation = new PlanCoverageVariation
                                    {
                                        Id = 0,
                                        Type = "1",
                                        PlanCoverageId = planCoverageId,
                                        AgreementType = cAGREEMENT_TYPE,
                                        PriceLimit = String.IsNullOrEmpty(cPRICE_LIMIT) ? null : (decimal?)decimal.Parse(cPRICE_LIMIT),
                                        PriceFactor = String.IsNullOrEmpty(cPRICE_FACTOR) ? null : (decimal?)decimal.Parse(cPRICE_FACTOR),
                                        NumberLimit = String.IsNullOrEmpty(cNUMBER_LIMIT) ? null : (int?)int.Parse(cNUMBER_LIMIT),
                                        DayLimit = String.IsNullOrEmpty(cDAY_LIMIT) ? null : (int?)int.Parse(cDAY_LIMIT),
                                        CoinsuranceRatio = String.IsNullOrEmpty(cCOINSURANCE_RATIO) ? null : (int?)int.Parse(cCOINSURANCE_RATIO),
                                        ExemptionType = cEXEMPTION_TYPE,
                                        ExemptionLimit = String.IsNullOrEmpty(cEXEMPTION_LIMIT) ? null : (int?)int.Parse(cEXEMPTION_LIMIT),
                                        CurrencyType = cCURRENCY_TYPE,
                                        SessionCount = String.IsNullOrEmpty(cSESSION_COUNT) ? null : (int?)int.Parse(cSESSION_COUNT)
                                    };

                                    var spResponseVariation = new PlanCoverageVariationRepository().Insert(TplanCoverageVariation);
                                    if (spResponseVariation.Code != "100")
                                    {
                                        throw new Exception(spResponseVariation.Code + " : " + spResponseVariation.Message);
                                    }
                                }

                                if (variation.NAG_AGREEMENT_TYPE != null)
                                {
                                    //Root item props
                                    string nAGREEMENT_TYPE = variation.NAG_AGREEMENT_TYPE != null ? variation.NAG_AGREEMENT_TYPE.ToString() : string.Empty;
                                    string nPRICE_LIMIT = variation.NAG_PRICE_LIMIT != null ? variation.NAG_PRICE_LIMIT.ToString() : string.Empty;
                                    string nPRICE_FACTOR = variation.NAG_PRICE_FACTOR != null ? variation.NAG_PRICE_FACTOR.ToString() : string.Empty;
                                    string nCURRENCY_TYPE = variation.NAG_CURRENCY_ID != null ? variation.NAG_CURRENCY_ID.ToString() : string.Empty;
                                    string nNUMBER_LIMIT = variation.NAG_NUMBER_LIMIT != null ? variation.NAG_NUMBER_LIMIT.ToString() : string.Empty;
                                    string nDAY_LIMIT = variation.NAG_DAY_LIMIT != null ? variation.NAG_DAY_LIMIT.ToString() : string.Empty;
                                    string nCOINSURANCE_RATIO = variation.NAG_COINSURANCE_RATIO != null ? variation.NAG_COINSURANCE_RATIO.ToString() : string.Empty;
                                    string nEXEMPTION_TYPE = variation.NAG_EXEMPTION_TYPE != null ? variation.NAG_EXEMPTION_TYPE.ToString() : string.Empty;
                                    string nEXEMPTION_LIMIT = variation.NAG_EXEMPTION_LIMIT != null ? variation.NAG_EXEMPTION_LIMIT.ToString() : string.Empty;
                                    string nSESSION_COUNT = variation.NAG_SESSION_COUNT != null ? variation.NAG_SESSION_COUNT.ToString() : string.Empty;

                                    var TplanCoverageVariation = new PlanCoverageVariation
                                    {
                                        Id = 0,
                                        Type = "2",
                                        PlanCoverageId = planCoverageId,
                                        AgreementType = nAGREEMENT_TYPE,
                                        PriceLimit = String.IsNullOrEmpty(nPRICE_LIMIT) ? null : (decimal?)decimal.Parse(nPRICE_LIMIT),
                                        PriceFactor = String.IsNullOrEmpty(nPRICE_FACTOR) ? null : (decimal?)decimal.Parse(nPRICE_FACTOR),
                                        NumberLimit = String.IsNullOrEmpty(nNUMBER_LIMIT) ? null : (int?)int.Parse(nNUMBER_LIMIT),
                                        DayLimit = String.IsNullOrEmpty(nDAY_LIMIT) ? null : (int?)int.Parse(nDAY_LIMIT),
                                        CoinsuranceRatio = String.IsNullOrEmpty(nCOINSURANCE_RATIO) ? null : (int?)int.Parse(nCOINSURANCE_RATIO),
                                        ExemptionType = nEXEMPTION_TYPE,
                                        ExemptionLimit = String.IsNullOrEmpty(nEXEMPTION_LIMIT) ? null : (int?)int.Parse(nEXEMPTION_LIMIT),
                                        CurrencyType = nCURRENCY_TYPE,
                                        SessionCount = String.IsNullOrEmpty(nSESSION_COUNT) ? null : (int?)int.Parse(nSESSION_COUNT)

                                    };
                                    var spResponseVariation = new PlanCoverageVariationRepository().Insert(TplanCoverageVariation);
                                    if (spResponseVariation.Code != "100")
                                    {
                                        throw new Exception(spResponseVariation.Code + " : " + spResponseVariation.Message);
                                    }
                                }

                                if (variation.EMG_AGREEMENT_TYPE != null)
                                {
                                    //Root item props
                                    string uAGREEMENT_TYPE = variation.EMG_AGREEMENT_TYPE != null ? variation.EMG_AGREEMENT_TYPE.ToString() : string.Empty;
                                    string uPRICE_LIMIT = variation.EMG_PRICE_LIMIT != null ? variation.EMG_PRICE_LIMIT.ToString() : string.Empty;
                                    string uPRICE_FACTOR = variation.EMG_PRICE_FACTOR != null ? variation.EMG_PRICE_FACTOR.ToString() : string.Empty;
                                    string uCURRENCY_TYPE = variation.EMG_CURRENCY_ID != null ? variation.EMG_CURRENCY_ID.ToString() : string.Empty;
                                    string uNUMBER_LIMIT = variation.EMG_NUMBER_LIMIT != null ? variation.EMG_NUMBER_LIMIT.ToString() : string.Empty;
                                    string uDAY_LIMIT = variation.EMG_DAY_LIMIT != null ? variation.EMG_DAY_LIMIT.ToString() : string.Empty;
                                    string uCOINSURANCE_RATIO = variation.EMG_COINSURANCE_RATIO != null ? variation.EMG_COINSURANCE_RATIO.ToString() : string.Empty;
                                    string uEXEMPTION_TYPE = variation.EMG_EXEMPTION_TYPE != null ? variation.EMG_EXEMPTION_TYPE.ToString() : string.Empty;
                                    string uEXEMPTION_LIMIT = variation.EMG_EXEMPTION_LIMIT != null ? variation.EMG_EXEMPTION_LIMIT.ToString() : string.Empty;
                                    string uSESSION_COUNT = variation.EMG_SESSION_COUNT != null ? variation.EMG_SESSION_COUNT.ToString() : string.Empty;


                                    var TplanCoverageVariation = new PlanCoverageVariation
                                    {
                                        Id = 0,
                                        Type = "3",
                                        PlanCoverageId = planCoverageId,
                                        AgreementType = uAGREEMENT_TYPE,
                                        PriceLimit = String.IsNullOrEmpty(uPRICE_LIMIT) ? null : (decimal?)decimal.Parse(uPRICE_LIMIT),
                                        PriceFactor = String.IsNullOrEmpty(uPRICE_FACTOR) ? null : (decimal?)decimal.Parse(uPRICE_FACTOR),
                                        NumberLimit = String.IsNullOrEmpty(uPRICE_FACTOR) ? null : (int?)int.Parse(uPRICE_FACTOR),
                                        DayLimit = String.IsNullOrEmpty(uPRICE_FACTOR) ? null : (int?)int.Parse(uPRICE_FACTOR),
                                        CoinsuranceRatio = String.IsNullOrEmpty(uPRICE_FACTOR) ? null : (int?)int.Parse(uPRICE_FACTOR),
                                        ExemptionType = uEXEMPTION_TYPE,
                                        ExemptionLimit = String.IsNullOrEmpty(uPRICE_FACTOR) ? null : (int?)int.Parse(uPRICE_FACTOR),
                                        CurrencyType = uCURRENCY_TYPE,
                                        SessionCount = String.IsNullOrEmpty(uSESSION_COUNT) ? null : (int?)int.Parse(uSESSION_COUNT)
                                    };
                                    var spResponseVariation = new PlanCoverageVariationRepository().Insert(TplanCoverageVariation);
                                    if (spResponseVariation.Code != "100")
                                    {
                                        throw new Exception(spResponseVariation.Code + " : " + spResponseVariation.Message);
                                    }
                                }
                            }

                            var subData = (from t in coverageList
                                           where t.parentIndex == item.index
                                           group t by t.COVERAGE_NAME into g
                                           select new { Root = g.Key, Items = g.ToList() }).ToList();

                            foreach (var subItem in subData)
                            {
                                foreach (var sitem in subItem.Items)
                                {
                                    //Sub item props
                                    string sCOVERAGE_ID = sitem.COVERAGE_ID;
                                    string sLOCATION_TYPE = sitem.LOCATION_TYPE;
                                    string sCOVERAGE_NAME = sitem.COVERAGE_NAME;
                                    string sNETWORK = sitem.NETWORK_ID;
                                    string sID = sitem.PLAN_COVERAGE_ID;
                                    string sSTATUS = sitem.STATUS;

                                    var TplanCoverageSub = new PlanCoverage
                                    {
                                        Id = !String.IsNullOrEmpty(sID) ? long.Parse(sID) : 0,
                                        ParentId = planCoverageId,
                                        PlanId = sitem.PLAN_ID,
                                        PackageId = packageId,
                                        CoverageId = long.Parse(sCOVERAGE_ID),
                                        CoverageName = sCOVERAGE_NAME,
                                        LocationType = sLOCATION_TYPE,
                                        NetworkId = long.Parse(sNETWORK),
                                        Status = sSTATUS
                                    };
                                    spResponsePlanCoverage = new PlanCoverageRepository().Insert(TplanCoverageSub);
                                    if (spResponsePlanCoverage.Code == "100")
                                    {
                                        planCoverageId = spResponsePlanCoverage.PkId;

                                        sagmerCoverageType = (from s in sagmerList
                                                              where s.ID == sitem.ID && s.STATUS != "1"
                                                              select s).ToList().FirstOrDefault();
                                        if (sagmerCoverageType != null)
                                        {
                                            long parameterId = long.Parse(Convert.ToString(sagmerCoverageType.PARAMETER_ID));
                                            var parameter = new Parameter
                                            {
                                                Id = parameterId,
                                                SystemType = "2", //Sagmer
                                                Environment = "D",
                                                Key = "COVERAGE_TYPE",
                                                Value = sagmerCoverageType.SAGMER_COVERAGE_TYPE
                                            };
                                            var spResponseParameter = new ParameterRepository().Insert(parameter);
                                            if (spResponseParameter.Code == "100")
                                            {
                                                parameterId = spResponseParameter.PkId;

                                                var planCoverageParameterId = long.Parse(Convert.ToString(sagmerCoverageType.PLAN_COVERAGE_PARAMETER_ID));
                                                var packageParameter = new PlanCoverageParameter
                                                {
                                                    Id = planCoverageParameterId,
                                                    PlanCoverageId = planCoverageId,
                                                    ParameterId = parameterId
                                                };
                                                var spResponsePlanCoverageParameter = new PlanCoverageParameterRepository().Insert(packageParameter);
                                                if (spResponsePlanCoverageParameter.Code != "100")
                                                    throw new Exception(spResponsePlanCoverageParameter.Code + " : " + spResponsePlanCoverageParameter.Message);
                                            }
                                            else
                                            {
                                                throw new Exception(spResponseParameter.Code + " : " + spResponseParameter.Message);
                                            }
                                        }

                                        PlanCoverageVariationList = new GenericRepository<V_PlanCoverageVariation>().FindBy("PLAN_COVERAGE_ID=:planCoverageId AND AGR_NEW_VERSION_ID IS NULL AND (AGR_STATUS IS NULL OR AGR_STATUS!=:Status) AND EMG_NEW_VERSION_ID IS NULL AND (EMG_STATUS IS NULL OR EMG_STATUS!=:Status) AND (NAG_STATUS IS NULL OR NAG_STATUS!=:Status) AND NAG_NEW_VERSION_ID IS NULL", orderby: "PLAN_COVERAGE_ID", parameters: new { planCoverageId = sitem.ID, Status = new DbString { Value = ((int)Status.SILINDI).ToString(), Length = 3 } });
                                        if (PlanCoverageVariationList.Count > 0)
                                        {
                                            var variation = PlanCoverageVariationList[PlanCoverageVariationList.Count - 1];

                                            if (variation.AGR_AGREEMENT_TYPE != null)
                                            {
                                                //Root item props
                                                string cAGREEMENT_TYPE = variation.AGR_AGREEMENT_TYPE != null ? variation.AGR_AGREEMENT_TYPE.ToString() : string.Empty;
                                                string cPRICE_LIMIT = variation.AGR_PRICE_LIMIT != null ? variation.AGR_PRICE_LIMIT.ToString() : string.Empty;
                                                string cPRICE_FACTOR = variation.AGR_PRICE_FACTOR != null ? variation.AGR_PRICE_FACTOR.ToString() : string.Empty;
                                                string cCURRENCY_TYPE = variation.AGR_CURRENCY_ID != null ? variation.AGR_CURRENCY_ID.ToString() : string.Empty;
                                                string cNUMBER_LIMIT = variation.AGR_NUMBER_LIMIT != null ? variation.AGR_NUMBER_LIMIT.ToString() : string.Empty;
                                                string cDAY_LIMIT = variation.AGR_DAY_LIMIT != null ? variation.AGR_DAY_LIMIT.ToString() : string.Empty;
                                                string cCOINSURANCE_RATIO = variation.AGR_COINSURANCE_RATIO != null ? variation.AGR_COINSURANCE_RATIO.ToString() : string.Empty;
                                                string cEXEMPTION_TYPE = variation.AGR_EXEMPTION_TYPE != null ? variation.AGR_EXEMPTION_TYPE.ToString() : string.Empty;
                                                string cEXEMPTION_LIMIT = variation.AGR_EXEMPTION_LIMIT != null ? variation.AGR_EXEMPTION_LIMIT.ToString() : string.Empty;
                                                string cSESSION_COUNT = variation.AGR_SESSION_COUNT != null ? variation.AGR_SESSION_COUNT.ToString() : string.Empty;


                                                var TplanCoverageVariation = new PlanCoverageVariation
                                                {
                                                    Id = 0,
                                                    Type = "1",
                                                    PlanCoverageId = planCoverageId,
                                                    AgreementType = cAGREEMENT_TYPE,
                                                    PriceLimit = String.IsNullOrEmpty(cPRICE_LIMIT) ? null : (decimal?)decimal.Parse(cPRICE_LIMIT),
                                                    PriceFactor = String.IsNullOrEmpty(cPRICE_FACTOR) ? null : (decimal?)decimal.Parse(cPRICE_FACTOR),
                                                    NumberLimit = String.IsNullOrEmpty(cNUMBER_LIMIT) ? null : (int?)int.Parse(cNUMBER_LIMIT),
                                                    DayLimit = String.IsNullOrEmpty(cDAY_LIMIT) ? null : (int?)int.Parse(cDAY_LIMIT),
                                                    CoinsuranceRatio = String.IsNullOrEmpty(cCOINSURANCE_RATIO) ? null : (int?)int.Parse(cCOINSURANCE_RATIO),
                                                    ExemptionType = cEXEMPTION_TYPE,
                                                    ExemptionLimit = String.IsNullOrEmpty(cEXEMPTION_LIMIT) ? null : (int?)int.Parse(cEXEMPTION_LIMIT),
                                                    CurrencyType = cCURRENCY_TYPE,
                                                    SessionCount = String.IsNullOrEmpty(cSESSION_COUNT) ? null : (int?)int.Parse(cSESSION_COUNT)
                                                };

                                                var spResponseVariation = new PlanCoverageVariationRepository().Insert(TplanCoverageVariation);
                                                if (spResponseVariation.Code != "100")
                                                {
                                                    throw new Exception(spResponseVariation.Code + " : " + spResponseVariation.Message);
                                                }
                                            }

                                            if (variation.NAG_AGREEMENT_TYPE != null)
                                            {
                                                //Root item props
                                                string nAGREEMENT_TYPE = variation.NAG_AGREEMENT_TYPE != null ? variation.NAG_AGREEMENT_TYPE.ToString() : string.Empty;
                                                string nPRICE_LIMIT = variation.NAG_PRICE_LIMIT != null ? variation.NAG_PRICE_LIMIT.ToString() : string.Empty;
                                                string nPRICE_FACTOR = variation.NAG_PRICE_FACTOR != null ? variation.NAG_PRICE_FACTOR.ToString() : string.Empty;
                                                string nCURRENCY_TYPE = variation.NAG_CURRENCY_ID != null ? variation.NAG_CURRENCY_ID.ToString() : string.Empty;
                                                string nNUMBER_LIMIT = variation.NAG_NUMBER_LIMIT != null ? variation.NAG_NUMBER_LIMIT.ToString() : string.Empty;
                                                string nDAY_LIMIT = variation.NAG_DAY_LIMIT != null ? variation.NAG_DAY_LIMIT.ToString() : string.Empty;
                                                string nCOINSURANCE_RATIO = variation.NAG_COINSURANCE_RATIO != null ? variation.NAG_COINSURANCE_RATIO.ToString() : string.Empty;
                                                string nEXEMPTION_TYPE = variation.NAG_EXEMPTION_TYPE != null ? variation.NAG_EXEMPTION_TYPE.ToString() : string.Empty;
                                                string nEXEMPTION_LIMIT = variation.NAG_EXEMPTION_LIMIT != null ? variation.NAG_EXEMPTION_LIMIT.ToString() : string.Empty;
                                                string nSESSION_COUNT = variation.NAG_SESSION_COUNT != null ? variation.NAG_SESSION_COUNT.ToString() : string.Empty;

                                                var TplanCoverageVariation = new PlanCoverageVariation
                                                {
                                                    Id = 0,
                                                    Type = "2",
                                                    PlanCoverageId = planCoverageId,
                                                    AgreementType = nAGREEMENT_TYPE,
                                                    PriceLimit = String.IsNullOrEmpty(nPRICE_LIMIT) ? null : (decimal?)decimal.Parse(nPRICE_LIMIT),
                                                    PriceFactor = String.IsNullOrEmpty(nPRICE_FACTOR) ? null : (decimal?)decimal.Parse(nPRICE_FACTOR),
                                                    NumberLimit = String.IsNullOrEmpty(nNUMBER_LIMIT) ? null : (int?)int.Parse(nNUMBER_LIMIT),
                                                    DayLimit = String.IsNullOrEmpty(nDAY_LIMIT) ? null : (int?)int.Parse(nDAY_LIMIT),
                                                    CoinsuranceRatio = String.IsNullOrEmpty(nCOINSURANCE_RATIO) ? null : (int?)int.Parse(nCOINSURANCE_RATIO),
                                                    ExemptionType = nEXEMPTION_TYPE,
                                                    ExemptionLimit = String.IsNullOrEmpty(nEXEMPTION_LIMIT) ? null : (int?)int.Parse(nEXEMPTION_LIMIT),
                                                    CurrencyType = nCURRENCY_TYPE,
                                                    SessionCount = String.IsNullOrEmpty(nSESSION_COUNT) ? null : (int?)int.Parse(nSESSION_COUNT)

                                                };
                                                var spResponseVariation = new PlanCoverageVariationRepository().Insert(TplanCoverageVariation);
                                                if (spResponseVariation.Code != "100")
                                                {
                                                    throw new Exception(spResponseVariation.Code + " : " + spResponseVariation.Message);
                                                }
                                            }

                                            if (variation.EMG_AGREEMENT_TYPE != null)
                                            {
                                                //Root item props
                                                string uAGREEMENT_TYPE = variation.EMG_AGREEMENT_TYPE != null ? variation.EMG_AGREEMENT_TYPE.ToString() : string.Empty;
                                                string uPRICE_LIMIT = variation.EMG_PRICE_LIMIT != null ? variation.EMG_PRICE_LIMIT.ToString() : string.Empty;
                                                string uPRICE_FACTOR = variation.EMG_PRICE_FACTOR != null ? variation.EMG_PRICE_FACTOR.ToString() : string.Empty;
                                                string uCURRENCY_TYPE = variation.EMG_CURRENCY_ID != null ? variation.EMG_CURRENCY_ID.ToString() : string.Empty;
                                                string uNUMBER_LIMIT = variation.EMG_NUMBER_LIMIT != null ? variation.EMG_NUMBER_LIMIT.ToString() : string.Empty;
                                                string uDAY_LIMIT = variation.EMG_DAY_LIMIT != null ? variation.EMG_DAY_LIMIT.ToString() : string.Empty;
                                                string uCOINSURANCE_RATIO = variation.EMG_COINSURANCE_RATIO != null ? variation.EMG_COINSURANCE_RATIO.ToString() : string.Empty;
                                                string uEXEMPTION_TYPE = variation.EMG_EXEMPTION_TYPE != null ? variation.EMG_EXEMPTION_TYPE.ToString() : string.Empty;
                                                string uEXEMPTION_LIMIT = variation.EMG_EXEMPTION_LIMIT != null ? variation.EMG_EXEMPTION_LIMIT.ToString() : string.Empty;
                                                string uSESSION_COUNT = variation.EMG_SESSION_COUNT != null ? variation.EMG_SESSION_COUNT.ToString() : string.Empty;


                                                var TplanCoverageVariation = new PlanCoverageVariation
                                                {
                                                    Id = 0,
                                                    Type = "3",
                                                    PlanCoverageId = planCoverageId,
                                                    AgreementType = uAGREEMENT_TYPE,
                                                    PriceLimit = String.IsNullOrEmpty(uPRICE_LIMIT) ? null : (decimal?)decimal.Parse(uPRICE_LIMIT),
                                                    PriceFactor = String.IsNullOrEmpty(uPRICE_FACTOR) ? null : (decimal?)decimal.Parse(uPRICE_FACTOR),
                                                    NumberLimit = String.IsNullOrEmpty(uPRICE_FACTOR) ? null : (int?)int.Parse(uPRICE_FACTOR),
                                                    DayLimit = String.IsNullOrEmpty(uPRICE_FACTOR) ? null : (int?)int.Parse(uPRICE_FACTOR),
                                                    CoinsuranceRatio = String.IsNullOrEmpty(uPRICE_FACTOR) ? null : (int?)int.Parse(uPRICE_FACTOR),
                                                    ExemptionType = uEXEMPTION_TYPE,
                                                    ExemptionLimit = String.IsNullOrEmpty(uPRICE_FACTOR) ? null : (int?)int.Parse(uPRICE_FACTOR),
                                                    CurrencyType = uCURRENCY_TYPE,
                                                    SessionCount = String.IsNullOrEmpty(uSESSION_COUNT) ? null : (int?)int.Parse(uSESSION_COUNT)
                                                };
                                                var spResponseVariation = new PlanCoverageVariationRepository().Insert(TplanCoverageVariation);
                                                if (spResponseVariation.Code != "100")
                                                {
                                                    throw new Exception(spResponseVariation.Code + " : " + spResponseVariation.Message);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        throw new Exception(spResponsePlanCoverage.Code + " : " + spResponsePlanCoverage.Message);
                                    }
                                }
                            }
                        }
                        else
                        {
                            throw new Exception(spResponsePlanCoverage.Message);
                        }
                    }
                }
                result.ResultCode = "100";

            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PackageTPASave(FormCollection form)
        {
            var result = new AjaxResultDto<JSONResult>();
            var tpas = form["hdPackageTpaList"];
            try
            {
                List<dynamic> tpaList = new List<dynamic>();
                tpaList = JsonConvert.DeserializeObject<List<dynamic>>(tpas);
                var packageId = long.Parse(form["hdPackageId"]);

                foreach (var tpa in tpaList)
                {
                    if (tpa.isOpen == "1")
                    {
                        var PACKAGE_TPA_PRICE_ID = !string.IsNullOrEmpty(Convert.ToString(tpa.PACKAGE_TPA_PRICE_ID)) ? tpa.PACKAGE_TPA_PRICE_ID : 0;
                        var PACKAGE_CASH_PAYMENT_TYPE = !string.IsNullOrEmpty(Convert.ToString(tpa.PACKAGE_CASH_PAYMENT_TYPE)) ? tpa.PACKAGE_CASH_PAYMENT_TYPE : "";
                        var PACKAGE_TPA_PRICE_AMOUNT = !string.IsNullOrEmpty(Convert.ToString(tpa.PACKAGE_TPA_PRICE_AMOUNT)) ? tpa.PACKAGE_TPA_PRICE_AMOUNT : 0;
                        string PACKAGE_TPA_PRICE_START_DATE_String = Convert.ToString(tpa.PACKAGE_TPA_PRICE_START_DATE);
                        var PACKAGE_TPA_PRICE_START_DATE = PACKAGE_TPA_PRICE_START_DATE_String.IsDateTime() ? tpa.PACKAGE_TPA_PRICE_START_DATE : DateTime.Now;

                        var packageTpaPrice = new PackageTPAPrice
                        {
                            Id = PACKAGE_TPA_PRICE_ID,
                            PackageId = packageId,
                            Amount =  decimal.Parse(Convert.ToString(PACKAGE_TPA_PRICE_AMOUNT)),
                            Type = ((int)PackageTpaPriceType.ZEYL_TANZIM_TARIHINE_GORE).ToString(),
                            StartDate = (DateTime.Parse(Convert.ToString(PACKAGE_TPA_PRICE_START_DATE))),
                            CashPaymentType = PACKAGE_CASH_PAYMENT_TYPE,
                            Status= Convert.ToString(tpa.STATUS)
                        };

                        var PackageTpaPriceListDateT = new GenericRepository<V_PackageTpaPrice>().FindBy("PACKAGE_ID=:packageId", orderby: "PACKAGE_ID", parameters: new { packageId });
                        foreach (var item in PackageTpaPriceListDateT)
                        {
                            if (item.START_DATE == packageTpaPrice.StartDate && item.PACKAGE_TPA_PRICE_ID!=packageTpaPrice.Id)
                            {
                                throw new Exception("Aynı tarihe sahip TPA eklemesi yapamazsınız...");
                            }
                        }
                        //Veritabanı Gidiş
                        var spResponseTpa = new GenericRepository<PackageTPAPrice>().Insert(packageTpaPrice);
                        if (spResponseTpa.Code != "100")
                        {
                            throw new Exception(spResponseTpa.Code + " : " + spResponseTpa.Message);
                        }
                    }
                }

                var PackageTpaPriceList = new GenericRepository<V_PackageTpaPrice>().FindBy("PACKAGE_ID=:packageId", orderby: "PACKAGE_ID", parameters: new { packageId });
                if (PackageTpaPriceList != null)
                {
                    List<dynamic> notesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in PackageTpaPriceList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.PACKAGE_TPA_PRICE_ID = Convert.ToString(item.PACKAGE_TPA_PRICE_ID);
                        listItem.PACKAGE_TPA_PRICE_TYPE = Convert.ToString(item.PACKAGE_TPA_PRICE_TYPE);

                        listItem.PACKAGE_CASH_PAYMENT_TYPE = Convert.ToString(item.CASH_PAYMENT_TYPE);
                        listItem.PACKAGE_TPA_PRICE_START_DATE = item.START_DATE;
                        listItem.PACKAGE_TPA_PRICE_AMOUNT = Convert.ToString(item.AMOUNT);


                        listItem.isOpen = "0";

                        notesList.Add(listItem);

                        i++;
                    }
                    result.Data = new JSONResult
                    {
                        jsonData = notesList.ToJSON()
                    };
                }
                result.ResultCode = "100";
               
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [LoginControl]
        public ActionResult PackageTPADelete(Int64 id)
        {
            try
            {
                var repo = new PolicyGroupRepository();
                var result = repo.FindById(id);
                result.Status = "1";
                var spResponse = repo.Update(result);
                if (spResponse.Code == "100")
                {
                    TempData["Alert"] = $"swAlert('İşlem Başarılı','Silme İşlemi Başarıyla Gerçekleştirildi.','success')";
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("PolicyGroup");
        }

        [HttpPost]
        public JsonResult PackageSaveStep3(FormCollection form)
        {
            var result = new AjaxResultDto<PackageSaveStep3Result>();
            var prices = form["hdPriceList"];
            try
            {
                List<dynamic> priceList = new List<dynamic>();
                priceList = JsonConvert.DeserializeObject<List<dynamic>>(prices);
                var packageId = form["hdPackageId"];

                foreach (var price in priceList)
                {
                    if (price.isOpen == "1")
                    {
                        var priceId = !string.IsNullOrEmpty(Convert.ToString(price.PRICE_ID)) ? price.PRICE_ID : 0;
                        var ageStart = !string.IsNullOrEmpty(Convert.ToString(price.AGE_START)) ? price.AGE_START : 0;
                        var ageEnd = !string.IsNullOrEmpty(Convert.ToString(price.AGE_END)) ? price.AGE_END : 0;
                        var gender = !string.IsNullOrEmpty(Convert.ToString(price.GENDER)) ? price.GENDER : 0;
                        var individualType = !string.IsNullOrEmpty(Convert.ToString(price.INDIVIDUAL_TYPE)) ? price.INDIVIDUAL_TYPE : 0;
                        var premium = !string.IsNullOrEmpty(Convert.ToString(price.PREMIUM)) ? price.PREMIUM : null;
                        var Tprice = new Price
                        {
                            Id = priceId,
                            PriceListId = long.Parse(form["hdPriceListId"]),
                            AgeStart = ageStart,
                            AgeEnd = ageEnd,
                            Gender = gender,
                            IndividualType = individualType,
                            Status = price.STATUS
                        };
                        if (premium != null)
                        {
                            Tprice.Premium = premium;
                        }
                        var spResponsePrice = new PriceRepository().Insert(Tprice);
                        if (spResponsePrice.Code == "100")
                        {
                            TempData["Alert"] = $"swAlert('İşlem Başarılı','Fiyat Listesi Başarıyla Kaydedildi.','success')";
                            result.ResultCode = "100";
                        }
                        else
                        {
                            throw new Exception(spResponsePrice.Code + " : " + spResponsePrice.Message);
                        }
                    }
                }

                var pricess = new GenericRepository<V_PackagePrice>().FindBy("PACKAGE_ID=:id", orderby: "PACKAGE_ID", parameters: new { id = packageId });
                if (pricess != null)
                {
                    List<dynamic> pricesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in pricess)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.PRICE_ID = Convert.ToString(item.PRICE_ID);

                        listItem.AGE_START = Convert.ToString(item.AGE_START);
                        listItem.AGE_END = Convert.ToString(item.AGE_END);
                        listItem.GENDER = Convert.ToString(item.GENDER);
                        listItem.GENDERSelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Gender, item.GENDER);
                        listItem.INDIVIDUAL_TYPE = Convert.ToString(item.INDIVIDUAL_TYPE);
                        listItem.INDIVIDUAL_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Individual, item.INDIVIDUAL_TYPE);
                        listItem.PREMIUM = Convert.ToString(item.PREMIUM);
                        listItem.isOpen = "0";

                        pricesList.Add(listItem);

                        i++;
                    }

                    result.Data = new PackageSaveStep3Result
                    {
                        jsonData = pricesList.ToJSON()
                    };
                }

            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PackageSaveStep4(FormCollection form)
        {
            var result = new AjaxResultDto<PackageSaveStep4Result>();
            var notes = form["hdNotes"];
            try
            {
                List<dynamic> noteList = new List<dynamic>();
                noteList = JsonConvert.DeserializeObject<List<dynamic>>(notes);
                var packageId = form["hdPackageId"];

                foreach (var note in noteList)
                {
                    if (note.isOpen == "1")
                    {
                        string NOTE_DESCRIPTION = Convert.ToString(note.NOTE_DESCRIPTION);
                        var Tnote = new Note
                        {
                            Id = note.NOTE_ID,
                            Type = note.NOTE_TYPE,
                            Description = NOTE_DESCRIPTION.RemoveRepeatedWhiteSpace(),
                            Status = note.STATUS
                        };
                        var spResponseNote = new NoteRepository().Insert(Tnote);
                        if (spResponseNote.Code == "100")
                        {
                            var noteId = spResponseNote.PkId;
                            var packageNote = new PackageNote
                            {
                                Id = note.PACKAGE_NOTE_ID,
                                PackageId = long.Parse(packageId),
                                NoteId = noteId,
                                Status = note.STATUS
                            };
                            var spResponseProviderNote = new PackageNoteRepository().Insert(packageNote);
                            if (spResponseProviderNote.Code == "100")
                            {
                                result.ResultCode = spResponseProviderNote.Code;
                                result.ResultMessage = spResponseProviderNote.Message;
                            }
                            else
                            {
                                throw new Exception(spResponseProviderNote.Code + " : " + spResponseProviderNote.Message);
                            }
                        }
                        else
                        {
                            throw new Exception(spResponseNote.Code + " : " + spResponseNote.Message);
                        }
                    }
                }

                var notess = new GenericRepository<V_PackageNote>().FindBy("PACKAGE_ID=:id", orderby: "PACKAGE_ID", parameters: new { id = packageId });
                if (notess != null)
                {
                    List<dynamic> notesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in notess)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                        listItem.PACKAGE_NOTE_ID = Convert.ToString(item.PACKAGE_NOTE_ID);

                        listItem.NOTE_TYPE = Convert.ToString(item.PACKAGE_NOTE_TYPE);
                        listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Note, Convert.ToString(item.PACKAGE_NOTE_TYPE));
                        listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                        listItem.isOpen = "0";

                        notesList.Add(listItem);

                        i++;
                    }

                    result.Data = new PackageSaveStep4Result
                    {
                        jsonData = notesList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PackageSaveStep5(FormCollection form)
        {
            var result = new AjaxResultDto<ProductSaveStep4Result>();
            ProductSaveStep4Result step1Result = new ProductSaveStep4Result();
            var rules = form["hdRuleSpecial"];
            try
            {
                var packageId = form["hdPackageId"];
                var productId = form["hdProductId"];
                var subproductId = form["hdSubproductId"];
                List<dynamic> ruleList = new List<dynamic>();
                ruleList = JsonConvert.DeserializeObject<List<dynamic>>(rules);
                foreach (var rule in ruleList)
                {
                    if (rule.isOpen == "1")
                    {
                        if (rule.STATUS == "0")
                        {
                            string idList = string.Empty;

                            if (rule.chkRuleSelectedId != null)
                            {
                                idList = rule.chkRuleSelectedId.ToString();

                                foreach (var ruleId in idList.Replace("\r", "").Replace("\n", "").Replace("[", string.Empty).Replace("]", "").Replace("\"", "").Replace(" ", "").Split(','))
                                {
                                    var TruleSubProduct = new RulePackage
                                    {
                                        Id = rule.RULE_PACKAGE_ID,
                                        PackageId = long.Parse(packageId),
                                        RuleId = long.Parse(ruleId),
                                        Status = Convert.ToString(rule.STATUS),
                                        Type = Convert.ToString(rule.RULE_TYPE)
                                    };
                                    var spResponse = new GenericRepository<RulePackage>().Insert(TruleSubProduct);
                                    if (spResponse.Code == "100")
                                    {
                                        result.ResultCode = spResponse.Code;
                                        result.ResultMessage = spResponse.Message;
                                    }
                                    else
                                    {
                                        throw new Exception(spResponse.Message);
                                    }
                                }
                            }
                            else
                            {
                                idList = rule.chkRuleGroupSelectedId.ToString();

                                foreach (var ruleGroupId in idList.Replace("\r", "").Replace("\n", "").Replace("[", string.Empty).Replace("]", "").Replace("\"", "").Replace(" ", "").Split(','))
                                {
                                    List<RuleGroupRule> ruleGroupRuleList = new GenericRepository<RuleGroupRule>().FindBy($"RULE_GROUP_ID={ruleGroupId}");
                                    if (ruleGroupRuleList.Any())
                                    {
                                        foreach (var ruleGroupRule in ruleGroupRuleList)
                                        {
                                            var TruleSubProduct = new RulePackage
                                            {
                                                Id = rule.RULE_PACKAGE_ID,
                                                PackageId = long.Parse(packageId),
                                                RuleId = ruleGroupRule.RuleId,
                                                RuleGroupId = ruleGroupRule.RuleGroupId,
                                                Status = Convert.ToString(rule.STATUS),
                                                Type = Convert.ToString(rule.RULE_TYPE)
                                            };
                                            var spResponse = new GenericRepository<RulePackage>().Insert(TruleSubProduct);
                                            if (spResponse.Code == "100")
                                            {
                                                result.ResultCode = spResponse.Code;
                                                result.ResultMessage = spResponse.Message;
                                            }
                                            else
                                            {
                                                throw new Exception(spResponse.Message);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            RulePackage ruleProduct = new GenericRepository<RulePackage>().FindById(long.Parse(Convert.ToString(rule.RULE_PACKAGE_ID)));
                            if (ruleProduct != null)
                            {
                                ruleProduct.Status = ((int)Status.SILINDI).ToString();
                                var spResponseRuleProduct = new GenericRepository<RulePackage>().Update(ruleProduct);
                                if (spResponseRuleProduct.Code == "100")
                                {
                                    result.ResultCode = spResponseRuleProduct.Code;
                                    result.ResultMessage = spResponseRuleProduct.Message;
                                }
                                else
                                {
                                    throw new Exception(spResponseRuleProduct.Message);
                                }
                            }
                        }
                    }
                }

                var ruleSpecialList = new GenericRepository<V_RulePackage>().FindBy("PACKAGE_ID=:id AND TYPE=:type", orderby: "PACKAGE_ID", parameters: new { id = packageId, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });

                List<dynamic> ruleDynamicSpecialList = new List<dynamic>();
                int specialId = 1;

                if (ruleSpecialList != null)
                {
                    foreach (var item in ruleSpecialList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = specialId;
                        listItem.ClientId = specialId;
                        listItem.STATUS = item.STATUS;

                        listItem.RULE_PACKAGE_ID = Convert.ToString(item.RULE_PACKAGE_ID);
                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                        listItem.RULE_NAME = item.RULE_NAME;
                        listItem.RESULT = item.RESULT;
                        listItem.INHERITED_NAME = string.Empty;
                        listItem.INHERITED_CODE = string.Empty;
                        listItem.isOpen = "0";
                        listItem.IsInherited = "1";

                        ruleDynamicSpecialList.Add(listItem);

                        specialId++;
                    }
                }

                var ruleSubProductSpecialList = new GenericRepository<V_RuleSubProduct>().FindBy("SUBPRODUCT_ID=:SUBPRODUCT_ID AND TYPE=:type", orderby: "SUBPRODUCT_ID", parameters: new { SUBPRODUCT_ID = subproductId, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });

                if (ruleSubProductSpecialList != null)
                {
                    foreach (var item in ruleSubProductSpecialList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = specialId;
                        listItem.ClientId = specialId;
                        listItem.STATUS = item.STATUS;

                        listItem.RULE_SUBPRODUCT_ID = Convert.ToString(item.RULE_SUBPRODUCT_ID);
                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                        listItem.RULE_NAME = item.RULE_NAME;
                        listItem.RESULT = item.RESULT;
                        listItem.INHERITED_NAME = "AltÜrün";
                        listItem.INHERITED_CODE = Convert.ToString(item.SUBPRODUCT_CODE);
                        listItem.isOpen = "0";
                        listItem.IsInherited = "0";

                        ruleDynamicSpecialList.Add(listItem);

                        specialId++;
                    }
                }

                var ruleProductSpecialList = new GenericRepository<V_RuleProduct>().FindBy("PRODUCT_ID=:PRODUCT_ID AND TYPE=:type", orderby: "PRODUCT_ID", parameters: new { PRODUCT_ID = productId, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });
                if (ruleProductSpecialList != null)
                {

                    foreach (var item in ruleProductSpecialList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = specialId;
                        listItem.ClientId = specialId;
                        listItem.STATUS = item.STATUS;

                        listItem.RULE_PRODUCT_ID = Convert.ToString(item.RULE_PRODUCT_ID);
                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                        listItem.RULE_NAME = item.RULE_NAME;
                        listItem.RESULT = item.RESULT;

                        listItem.INHERITED_NAME = "Ürün";
                        listItem.INHERITED_CODE = Convert.ToString(item.PRODUCT_CODE);
                        listItem.isOpen = "0";
                        listItem.IsInherited = "0";

                        ruleDynamicSpecialList.Add(listItem);

                        specialId++;
                    }
                }
                result.Data = new ProductSaveStep4Result
                {
                    jsonData = ruleDynamicSpecialList.ToJSON()
                };
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PackageSaveStep7(FormCollection form)
        {
            var result = new AjaxResultDto<ProductSaveStep4Result>();
            ProductSaveStep4Result step1Result = new ProductSaveStep4Result();
            var rules = form["hdRuleExtra"];
            try
            {
                var packageId = form["hdPackageId"];
                var subproductId = form["hdSubproductId"];
                List<dynamic> ruleList = new List<dynamic>();
                ruleList = JsonConvert.DeserializeObject<List<dynamic>>(rules);
                foreach (var rule in ruleList)
                {
                    if (rule.isOpen == "1")
                    {
                        if (rule.STATUS == "0")
                        {
                            string idList = string.Empty;

                            if (rule.chkRuleSelectedId != null)
                            {
                                idList = rule.chkRuleSelectedId.ToString();

                                foreach (var ruleId in idList.Replace("\r", "").Replace("\n", "").Replace("[", string.Empty).Replace("]", "").Replace("\"", "").Replace(" ", "").Split(','))
                                {

                                    var TrulePackage = new RulePackage
                                    {
                                        Id = rule.RULE_PACKAGE_ID,
                                        PackageId = long.Parse(packageId),
                                        RuleId = long.Parse(ruleId),
                                        Status = Convert.ToString(rule.STATUS),
                                        Type = Convert.ToString(rule.RULE_TYPE)
                                    };
                                    var spResponseRuleProduct = new GenericRepository<RulePackage>().Insert(TrulePackage);
                                    if (spResponseRuleProduct.Code == "100")
                                    {
                                        result.ResultCode = spResponseRuleProduct.Code;
                                        result.ResultMessage = spResponseRuleProduct.Message;
                                    }
                                    else
                                    {
                                        throw new Exception(spResponseRuleProduct.Message);
                                    }
                                }
                            }
                            else
                            {
                                idList = rule.chkRuleGroupSelectedId.ToString();

                                foreach (var ruleGroupId in idList.Replace("\r", "").Replace("\n", "").Replace("[", string.Empty).Replace("]", "").Replace("\"", "").Replace(" ", "").Split(','))
                                {
                                    List<RuleGroupRule> ruleGroupRuleList = new GenericRepository<RuleGroupRule>().FindBy($"RULE_GROUP_ID={ruleGroupId}");
                                    if (ruleGroupRuleList.Any())
                                    {
                                        foreach (var ruleGroupRule in ruleGroupRuleList)
                                        {
                                            var TrulePackage = new RulePackage
                                            {
                                                Id = rule.RULE_PACKAGE_ID,
                                                PackageId = long.Parse(packageId),
                                                RuleId = ruleGroupRule.RuleId,
                                                RuleGroupId = ruleGroupRule.RuleGroupId,
                                                Status = Convert.ToString(rule.STATUS),
                                                Type = Convert.ToString(rule.RULE_TYPE)
                                            };
                                            var spResponseRuleProduct = new GenericRepository<RulePackage>().Insert(TrulePackage);
                                            if (spResponseRuleProduct.Code == "100")
                                            {
                                                result.ResultCode = spResponseRuleProduct.Code;
                                                result.ResultMessage = spResponseRuleProduct.Message;
                                            }
                                            else
                                            {
                                                throw new Exception(spResponseRuleProduct.Message);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            RulePackage ruleProduct = new GenericRepository<RulePackage>().FindById(long.Parse(Convert.ToString(rule.RULE_PACKAGE_ID)));
                            if (ruleProduct != null)
                            {
                                ruleProduct.Status = ((int)Status.SILINDI).ToString();
                                var spResponseRuleProduct = new GenericRepository<RulePackage>().Update(ruleProduct);
                                if (spResponseRuleProduct.Code == "100")
                                {
                                    result.ResultCode = spResponseRuleProduct.Code;
                                    result.ResultMessage = spResponseRuleProduct.Message;
                                }
                                else
                                {
                                    throw new Exception(spResponseRuleProduct.Message);
                                }
                            }
                        }
                    }
                }

                var rulePackageExtraList = new GenericRepository<V_RulePackage>().FindBy("PACKAGE_ID=:id AND TYPE=:type", orderby: "PRODUCT_ID", parameters: new { id = packageId, type = new DbString { Value = ((int)RuleType.EK_PROTOKOL).ToString(), Length = 3 } });

                List<dynamic> ruleDynamicExtraList = new List<dynamic>();
                int extraId = 1;

                if (rulePackageExtraList != null)
                {

                    foreach (var item in rulePackageExtraList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = extraId;
                        listItem.ClientId = extraId;
                        listItem.STATUS = item.STATUS;

                        listItem.RULE_PACKAGE_ID = Convert.ToString(item.RULE_PACKAGE_ID);
                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                        listItem.RULE_NAME = item.RULE_NAME;
                        listItem.RESULT = item.RESULT;
                        listItem.INHERITED_NAME = string.Empty;
                        listItem.INHERITED_CODE = string.Empty;
                        listItem.isOpen = "0";
                        listItem.IsInherited = "1";

                        ruleDynamicExtraList.Add(listItem);

                        extraId++;
                    }
                }

                var ruleSubProductExtraList = new GenericRepository<V_RuleSubProduct>().FindBy("SUBPRODUCT_ID=:SUBPRODUCT_ID AND TYPE=:type", orderby: "SUBPRODUCT_ID", parameters: new { SUBPRODUCT_ID = subproductId, type = new DbString { Value = ((int)RuleType.EK_PROTOKOL).ToString(), Length = 3 } });

                if (ruleSubProductExtraList != null)
                {

                    foreach (var item in ruleSubProductExtraList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = extraId;
                        listItem.ClientId = extraId;
                        listItem.STATUS = item.STATUS;

                        listItem.RULE_SUBPRODUCT_ID = Convert.ToString(item.RULE_SUBPRODUCT_ID);
                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                        listItem.RULE_NAME = item.RULE_NAME;
                        listItem.RESULT = item.RESULT;
                        listItem.INHERITED_NAME = "AltÜrün";
                        listItem.INHERITED_CODE = Convert.ToString(item.SUBPRODUCT_CODE);
                        listItem.isOpen = "0";
                        listItem.IsInherited = "0";

                        ruleDynamicExtraList.Add(listItem);

                        extraId++;
                    }
                }
                result.Data = new ProductSaveStep4Result
                {
                    jsonData = ruleDynamicExtraList.ToJSON()
                };
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PlanCoverageIsOpenProvider(Int64 PlanCoverageId, int isOpenProvider)
        {
            var result = new AjaxResultDto<PackageSaveStep6Result>();
            try
            {
                var planCoverage = new GenericRepository<PlanCoverage>().FindById(PlanCoverageId);
                if (planCoverage == null)
                    throw new Exception("Teminat Network Eşleşmeniz Bulunamadı!");

                planCoverage.IS_OPEN_TO_PROVIDER = isOpenProvider;
                var spPlanCoverage = new GenericRepository<PlanCoverage>().Insert(planCoverage);

                result.ResultCode = spPlanCoverage.Code;
                result.ResultMessage = spPlanCoverage.Message;
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PlanCoverageIsOpenPrint(Int64 PlanCoverageId, int isOpenPrint)
        {
            var result = new AjaxResultDto<PackageSaveStep6Result>();
            try
            {
                var planCoverage = new GenericRepository<PlanCoverage>().FindById(PlanCoverageId);
                if (planCoverage == null)
                    throw new Exception("Teminat Network Eşleşmeniz Bulunamadı!");

                planCoverage.IS_OPEN_TO_PRINT = isOpenPrint;
                var spPlanCoverage = new GenericRepository<PlanCoverage>().Insert(planCoverage);

                result.ResultCode = spPlanCoverage.Code;
                result.ResultMessage = spPlanCoverage.Message;
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetPackageIdChange(Int64 PackageId, string isOpenWS)
        {
            var result = new AjaxResultDto<PackageSaveStep6Result>();
            try
            {
                var package = new GenericRepository<Package>().FindById(PackageId);
                if (package == null)
                    throw new Exception("Paket Bilgisi Bulunamadı!");

                package.IS_OPEN_TO_WS = isOpenWS;

                var updatePackage = new GenericRepository<Package>().Insert(package);


                result.ResultCode = updatePackage.Code;
                result.ResultMessage = updatePackage.Message;
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;

            }

            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult PackageSaveStep6(FormCollection form)
        {
            var result = new AjaxResultDto<PackageSaveStep6Result>();
            var medias = form["hdMedias"];
            try
            {
                List<dynamic> mediaList = new List<dynamic>();
                mediaList = JsonConvert.DeserializeObject<List<dynamic>>(medias);
                var packageId = form["hdPackageId"];

                foreach (var media in mediaList)
                {
                    if (media.isOpen == "1")
                    {
                        var Tmedia = new Media
                        {
                            Id = media.MEDIA_ID,
                            Name = media.MEDIA_NAME,
                            FileName = "TEST",
                            Status = media.STATUS
                        };
                        var spResponseMedia = new MediaRepository().Insert(Tmedia);
                        if (spResponseMedia.Code == "100")
                        {
                            var mediaId = spResponseMedia.PkId;
                            var packageMedia = new PackageMedia
                            {
                                Id = media.PACKAGE_MEDIA_ID == "" ? 0 : media.PACKAGE_MEDIA_ID,
                                PackageId = long.Parse(packageId),
                                MediaId = mediaId,
                                Status = media.STATUS
                            };
                            var spResponsePackageNote = new PackageMediaRepository().Insert(packageMedia);
                            if (spResponsePackageNote.Code == "100")
                            {
                                result.ResultCode = spResponsePackageNote.Code;
                                result.ResultMessage = spResponsePackageNote.Message;
                                TempData["Alert"] = $"swAlert('İşlemi Başarılı','{media.MEDIA_NAME} başarıyla kaydedildi.','success')";

                            }
                            else
                            {
                                throw new Exception(spResponsePackageNote.Code + " : " + spResponsePackageNote.Message);
                            }
                        }
                        else
                        {
                            throw new Exception(spResponseMedia.Code + " : " + spResponseMedia.Message);
                        }
                    }
                }

                var mediass = new GenericRepository<V_PackageMedia>().FindBy("PACKAGE_ID=:id", orderby: "PACKAGE_ID", parameters: new { id = packageId });
                if (mediass != null)
                {
                    List<dynamic> mediasList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in mediass)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.Status;

                        listItem.MEDIA_ID = Convert.ToString(item.MediaId);
                        listItem.PACKAGE_MEDIA_ID = Convert.ToString(item.PackageMediaId);

                        listItem.MEDIA_NAME = item.MediaName;
                        listItem.MEDIA_PATH = item.FileName;
                        listItem.isOpen = "0";

                        mediasList.Add(listItem);

                        i++;
                    }

                    result.Data = new PackageSaveStep6Result
                    {
                        jsonData = mediasList.ToJSON()
                    };
                }

            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }
       #endregion
    }

}