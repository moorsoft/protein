﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Protein.Common.Constants;
using Protein.Data.Helpers;
using Protein.Data.Repositories;

namespace Protein.Web.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            //var result1 = LookupHelper.GetLookupData(Constants.LookupTypes.DoctorBranch);
            //var repo = new LookupRepository();
            //var list = repo.FindByPaged(1, 10);

            //ViewBag.TableList = list;
            //ViewBag.Liste = result1;
            return View();
        }

        [HttpPost]
        public ActionResult FormPost(FormCollection form)
        {
            var id = form["hdID"];

            var myObject = new MyClass
            {
                ID = Convert.ToInt32(form["hdID"]),
                Name = form["txtName"],
                Comment = form["txtComment"]
            };

            return View("Index");
        }
    }

    public class MyClass
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
    }
}