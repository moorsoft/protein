﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using Protein.Common.Constants;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using static Protein.Common.Entities.ProteinEntities;
using Protein.Common.Extensions;
using Protein.Web.ActionFilter.LoginAttr;
using Protein.Web.Models.ListView;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Enums.ProteinEnums;
using System.Dynamic;
using Protein.Common.Dto;

namespace Protein.Web.Controllers
{
    [LoginControl]
    public class DefinitionController : Controller
    {
        #region Process
        // GET: Process List
        [LoginControl]
        public ActionResult ProcessList()
        {
            try
            {
                var type = Request["Type"] ?? "0";
                ViewBag.Type = type ?? "0";

                switch (type)
                {
                    case "0":
                        ViewBag.Title = "Tanı Listesi";
                        break;
                    case "1":
                        ViewBag.Title = "İlaç Listesi";
                        break;
                    case "2":
                        ViewBag.Title = "İşlem Listesi";
                        ViewBag.ProcessListTypeList = LookupHelper.GetLookupData(LookupTypes.ProcessList, showAll: true);
                        break;
                    default:
                        ViewBag.Title = "";
                        break;
                }

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var PriceType = LookupHelper.GetLookupData(LookupTypes.Price, showAll: true);
                ViewBag.PriceTypeList = PriceType;

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetProcessListList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            dynamic parameters = new ExpandoObject();


            string processListTypeFilter = "PROCESS_LIST_TYPE=" + form["type"];
            if (form["type"].Equals("2"))
            {
                string processListType = form["processListType"];
                if (!string.IsNullOrEmpty(processListType))
                {
                    processListTypeFilter = "PROCESS_LIST_TYPE=" + processListType;
                }
                else
                {
                    processListTypeFilter = "(PROCESS_LIST_TYPE=2 OR PROCESS_LIST_TYPE=3 OR PROCESS_LIST_TYPE=4 OR PROCESS_LIST_TYPE=5)";
                }
            }

            string whereConditition = $"{processListTypeFilter} AND";
            whereConditition += !String.IsNullOrEmpty(form["processListPriceType"]) ? $" PROCESS_LIST_PRICE_TYPE='{form["processListPriceType"]}' AND" : "";
            whereConditition += !String.IsNullOrEmpty(form["processListName"]) ? $" PROCESS_LIST_NAME LIKE '%{form["processListName"]}%' AND" : "";

            var ProcessList = new GenericRepository<V_ProcessList>().FindByPaged(conditions: whereConditition.Substring(0, whereConditition.Length - 3),
                                                orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection}" : "PROCESS_LIST_ID", pageNumber: start, rowsPerPage: length);
            List<ProcessListList> ProcessListList = new List<ProcessListList>();

            if (ProcessList.Data != null)
            {
                foreach (var item in ProcessList.Data)
                {
                    ProcessListList listItem = new ProcessListList
                    {
                        PROCESS_LIST_ID = Convert.ToString(item.PROCESS_LIST_ID),
                        PRICE_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Price, item.PRICE_TYPE),
                        PROCESS_LIST_NAME = Convert.ToString(item.PROCESS_LIST_NAME),
                        PROCESS_LIST_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.ProcessList, item.PROCESS_LIST_TYPE)
                    };
                    ProcessListList.Add(listItem);
                }
            }

            return Json(new { data = ProcessListList, draw = Request["draw"], recordsTotal = ProcessList.TotalItemsCount, recordsFiltered = ProcessList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        // GET: Process Form
        [LoginControl]
        public ActionResult ProcessListForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                var type = Request["Type"] ?? "0";
                ViewBag.Type = type ?? "0";
                switch (type)
                {
                    case "0":
                        ViewBag.Title = "Tanı Listesi";
                        break;
                    case "1":
                        ViewBag.Title = "İlaç Listesi";
                        break;
                    case "2":
                        ViewBag.Title = "İşlem Listesi";
                        ViewBag.ProcessListTypeList = LookupHelper.GetLookupData(LookupTypes.ProcessList, showAll: true);
                        break;

                    default:
                        ViewBag.Title = "";
                        break;
                }

                var PriceType = LookupHelper.GetLookupData(Constants.LookupTypes.Price);
                ViewBag.PriceTypeList = PriceType;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.isEdit = false;
                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title += " Güncelle";
                        ViewBag.isEdit = true;
                        if (id != 0)
                        {
                            var process = new ProcessListRepository().FindById(id);

                            ViewBag.Process = process ?? null;

                            List<Process> listProcess = new GenericRepository<Process>().FindBy(conditions: "PROCESS_LIST_ID = :PROCESS_LIST_ID", parameters: new { PROCESS_LIST_ID = id });
                            if (listProcess != null)
                            {
                                var rootData = (from lp in listProcess
                                                where lp.ParentId == null
                                                select lp).ToList();


                                List<dynamic> list = new List<dynamic>();

                                int index = 0;
                                int i = 0;

                                foreach (var rootItem in rootData)
                                {
                                    //Add root item
                                    dynamic rootListItem = new System.Dynamic.ExpandoObject();

                                    rootListItem.index = index;
                                    rootListItem.parentIndex = "-1";
                                    rootListItem.STATUS = rootItem.Status;

                                    rootListItem.ID = rootItem.Id;
                                    rootListItem.PARENT_ID = rootItem.ParentId == null ? "" : Convert.ToString(rootItem.ParentId);
                                    rootListItem.CODE = rootItem.Code;
                                    rootListItem.NAME = rootItem.Name;
                                    rootListItem.AMOUNT = rootItem.Amount;


                                    rootListItem.hdState = "Update";
                                    rootListItem.hdIndex = "-1";
                                    rootListItem.hdTreeLevel = "";

                                    list.Add(rootListItem);

                                    var subData = (from lp in listProcess
                                                   where lp.ParentId == rootItem.Id
                                                   select lp).ToList();
                                    var rootIndex = index;
                                    foreach (var subItem in subData)
                                    {
                                        index++;

                                        //Add sub item
                                        dynamic subListItem = new System.Dynamic.ExpandoObject();
                                        subListItem.index = index;
                                        subListItem.parentIndex = rootIndex.ToString();
                                        subListItem.STATUS = subItem.Status;

                                        subListItem.ID = subItem.Id;
                                        subListItem.PARENT_ID = subItem.ParentId == null ? "" : Convert.ToString(subItem.ParentId);
                                        subListItem.CODE = subItem.Code;
                                        subListItem.NAME = subItem.Name;
                                        subListItem.AMOUNT = subItem.Amount;

                                        subListItem.hdState = "Update";
                                        subListItem.hdIndex = "-1";
                                        subListItem.hdTreeLevel = "";

                                        list.Add(subListItem);

                                    }

                                    index++;
                                    i++;
                                }
                                ViewBag.TreeData = list.ToJSON(true);
                            }
                        }
                        else
                            TempData["Alert"] = $"swAlert('Bilgi','Aranılan İşlem Listesi Bulunamadı. Lütfen Tekrar Deneyiniz...','information')";
                        break;

                    default:
                        ViewBag.Title += " Ekle";
                        ViewBag.isEdit = false;
                        break;
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        public ActionResult ProcessListCreate(FormCollection form)
        {
            var type = form["type"];
            var processListType = type;
            if (processListType.Equals("2"))
            {
                if (!string.IsNullOrEmpty(form["processListType"]))
                {
                    processListType = form["processListType"];
                }
            }
            try
            {
                var priceType = form["priceType"];
                var processListName = form["processListName"];
                var isFactorCalculeted = form["isFactorCalculeted"];
                var processlistId = form["PROCESS_LIST_ID"];

                var processList = new ProcessList
                {
                    Id = processlistId != "" ? long.Parse(processlistId) : 0,
                    IsFactorCalculated = isFactorCalculeted != null ? "1" : "0",
                    Name = processListName,
                    PriceType = priceType,
                    Type = processListType

                };
                var spResponseProcessList = new ProcessListRepository().Insert(processList);

                if (spResponseProcessList.Code == "100")
                {
                    #region Process Create
                    var processListId = spResponseProcessList.PkId;

                    //hdTreeData
                    var treeData = form["hdTreeData"];

                    List<dynamic> treeList = new List<dynamic>();
                    treeList = JsonConvert.DeserializeObject<List<dynamic>>(treeData);

                    //root items
                    var rootData = (from t in treeList
                                    where t.parentIndex == -1
                                    group t by t.NAME into g
                                    select new { Root = g.Key, Items = g.ToList() }).ToList();

                    foreach (var rootItem in rootData)
                    {
                        foreach (var item in rootItem.Items)
                        {
                            //Root item props
                            string rCODE = item.CODE;
                            string rNAME = item.NAME;
                            string rAMOUNT = item.AMOUNT;
                            string rID = item.ID;
                            string rPARENT_ID = item.PARENT_ID;
                            string rSTATUS = item.STATUS;

                            var rootProcess = new Process
                            {
                                Id = !String.IsNullOrEmpty(rID) ? long.Parse(rID) : 0,
                                ProcessListId = processListId,
                                Code = rCODE,
                                Name = rNAME,
                                Amount = Decimal.Parse(rAMOUNT.Replace('.', ',')),
                                Status = rSTATUS
                            };
                            var spResponseProcess = new ProcessRepository().Insert(rootProcess);
                            if (spResponseProcess.Code == "100")
                            {
                                var rootProcessId = spResponseProcess.PkId;
                                var subData = (from t in treeList
                                               where t.parentIndex == item.index
                                               group t by t.NAME into g
                                               select new { Root = g.Key, Items = g.ToList() }).ToList();

                                foreach (var subItem in subData)
                                {
                                    foreach (var sitem in subItem.Items)
                                    {
                                        //Sub item props
                                        string sCODE = sitem.CODE;
                                        string sNAME = sitem.NAME;
                                        string sAMOUNT = sitem.AMOUNT;
                                        string sID = sitem.ID;
                                        string sPARENT_ID = sitem.PARENT_ID;
                                        string sSTATUS = sitem.STATUS;

                                        var subProcess = new Process
                                        {
                                            Id = !String.IsNullOrEmpty(sID) ? long.Parse(sID) : 0,
                                            ProcessListId = processListId,
                                            ParentId = rootProcessId,
                                            Code = sCODE,
                                            Name = sNAME,
                                            Amount = Decimal.Parse(sAMOUNT.Replace('.', ',')),
                                            Status = sSTATUS
                                        };
                                        spResponseProcess = new ProcessRepository().Insert(subProcess);
                                        if (spResponseProcess.Code != "100")
                                        {
                                            throw new Exception(spResponseProcess.Code + " : " + spResponseProcess.Message);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                throw new Exception(spResponseProcess.Code + " : " + spResponseProcess.Message);
                            }
                        }
                    }
                    TempData["Alert"] = $"swAlert('İşlem Başarılı','{processListName} başarıyla kaydedildi.','success')";

                    #endregion
                }
                else
                {
                    throw new Exception(spResponseProcessList.Code + " : " + spResponseProcessList.Message);
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction($"ProcessList", new { Type = type });
        }


        [LoginControl]
        public ActionResult ProcessListDelete(Int64 id)
        {
            var type = Request["Type"];
            try
            {
                string whereCondition = "PROCESS_LIST_ID =:id ";
                var resultCheckProcess = new GenericRepository<V_Process>().FindBy(whereCondition, orderby: "PROCESS_LIST_ID", parameters: new { id });
                if (resultCheckProcess != null && resultCheckProcess.Count > 0)
                {
                    string message = "İşlem listesinin altında işlem bulunduğundan silinemez!";
                    if (Convert.ToInt32(type) == (int)ProcessListType.TANI)
                    {
                        message = "Tanı listesinin altında hastalık bulunduğundan silinemez!";
                    }
                    else if (Convert.ToInt32(type) == (int)ProcessListType.ILAC)
                    {
                        message = "İlaç listesinin altında ilaç bulunduğundan silinemez!";
                    }
                    throw new Exception(message);
                }
                var repo = new ProcessListRepository();
                var result = repo.FindById(id);
                result.Status = "1";
                var spResponse = repo.Update(result);
                if (spResponse.Code == "100")
                    TempData["Alert"] = $"swAlert('Silme İşlemi Başarılı','{result.Name} başarıyla silindi.','success')";
                else
                    throw new Exception(spResponse.Code + " : " + spResponse.Message);
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("ProcessList", new { Type = type });
        }
        #endregion

        #region CityFactory
        // GET: City Factory Group List
        public ActionResult CityFactorGroup()
        {
            try
            {
                if (Session["Personnel"] == null)
                    return RedirectToAction("Login", "Dashboard");

                ViewBag.cityFactorGroupProcessList = string.Empty;
                ViewBag.cityFactorGroupStatus = string.Empty;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var ProcessListType = new GenericRepository<ProcessList>().FindBy("IS_FACTOR_CALCULATED = 1");
                ViewBag.ProcessListTypeList = ProcessListType;

                var StatusType = LookupHelper.GetLookupData(LookupTypes.Status, showAll: true);
                ViewBag.StatusTypeList = StatusType;

                var CityFactorGroupList = new GenericRepository<V_CityFactorGroup>().FindBy(orderby: "CITY_FACTOR_GROUP_ID");
                ViewBag.CityFactorGroupList = CityFactorGroupList;

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return View();
        }

        [HttpPost]
        public ActionResult CityFactorGroupFilter(FormCollection form)
        {
            ViewBag.cityFactorGroupProcessList = form["cityFactorGroupProcessList"];
            ViewBag.cityFactorGroupStatus = form["cityFactorGroupStatus"];

            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            var ProcessList = new ProcessListRepository().FindBy("IS_FACTOR_CALCULATED = 1");
            ViewBag.ProcessListTypeList = ProcessList;

            var StatusType = LookupHelper.GetLookupData(Constants.LookupTypes.Status, showAll: true);
            ViewBag.StatusTypeList = StatusType;


            string whereConditition = !String.IsNullOrEmpty(form["cityFactorGroupProcessList"]) ? $" PROCESS_LIST_ID={form["cityFactorGroupProcessList"]} AND" : "";
            whereConditition += !String.IsNullOrEmpty(form["cityFactorGroupStatus"]) ? $" STATUS='{form["cityFactorGroupStatus"]}' AND" : "";

            var CityFactorGroupList = new GenericRepository<V_CityFactorGroup>().FindBy(whereConditition.Substring(0, whereConditition.Length - 3), orderby: "CITY_FACTOR_GROUP_ID");
            ViewBag.CityFactorGroupList = CityFactorGroupList;

            return View("CityFactorGroup");
        }

        public ActionResult CityFactorGroupDelete(Int64 Id)
        {
            var type = Request["Type"];
            try
            {
                string whereCondition = "PROCESS_LIST_ID =:processListId ";
                var resultCheckProcess = new GenericRepository<V_Process>().FindBy(whereCondition, orderby: "PROCESS_LIST_ID", parameters: new { processListId = Id });
                if (resultCheckProcess != null && resultCheckProcess.Count > 0)
                {
                    throw new Exception("İl katsayı grubuna bağlı olduğundan silinemez!");
                }
                var repo = new GenericRepository<CityFactorGroup>();
                var result = repo.FindById(Id);
                result.Status = "1";
                var spResponse = repo.Update(result);
                if (spResponse.Code == "100")
                    TempData["Alert"] = $"swAlert('Silme İşlemi Başarılı','Silme İşlemi Başaralı.')";
                else
                    throw new Exception(spResponse.Code + " : " + spResponse.Message);
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("CityFactorGroup");
        }

        // GET: City Factory Group Form
        public ActionResult CityFactorGroupForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                if (Session["Personnel"] == null)
                    return RedirectToAction("Login", "Dashboard");

                var ProcessList = new ProcessListRepository().FindBy("IS_FACTOR_CALCULATED = 1");
                ViewBag.ProcessList = ProcessList;

                var CityList = new CityRepository().FindBy();
                ViewBag.CityList = CityList;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "İl Katsayı Grubu Güncelle";
                        ViewBag.FormMode = "Edit";
                        if (id > 0)
                        {

                            var cityFactor = new CityFactorRepository().FindBy("CITY_FACTOR_GROUP_ID=:cityFactorGroupId",
                                                                               orderby: "CITY_FACTOR_GROUP_ID",
                                                                               parameters: new { cityFactorGroupId = id });
                            if (cityFactor != null)
                            {
                                ViewBag.CityFactor = cityFactor;
                                var cityFactorGroup = new CityFactorGroupRepository().FindById(id);
                                ViewBag.CityFactorGroup = cityFactorGroup ?? null;
                            }
                        }
                        break;

                    default:
                        ViewBag.Title = "İl Katsayı Grubu Ekle";
                        ViewBag.FormMode = "Create";
                        break;
                }


            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }



            return View();
        }

        [HttpPost]
        public ActionResult CityFactorGroupFormSave(FormCollection form)
        {
            try
            {
                var processGroupId = form["processGroup"];
                var cityFactorStartDate = form["cityFactorStartDate"];
                var cityFactorEndDate = form["cityFactorEndDate"];
                var cityFactorGroup = new CityFactorGroup
                {
                    ProcessListId = long.Parse(processGroupId),
                    StartDate = DateTime.Parse(cityFactorStartDate),
                    EndDate = !String.IsNullOrEmpty(cityFactorEndDate) ? (DateTime?)DateTime.Parse(cityFactorEndDate) : null
                };
                var spResponseCityFactorGroup = new CityFactorGroupRepository().Insert(cityFactorGroup);
                if (spResponseCityFactorGroup.Code == "100")
                {
                    var cityFactorGroupId = spResponseCityFactorGroup.PkId;
                    List<CityFactor> cityFactorList = new List<CityFactor>();
                    for (int i = 4; i < form.Count; i++)
                    {
                        var cityFactor = new CityFactor
                        {
                            CityFactorGroupId = cityFactorGroupId,
                            CityId = long.Parse(form.Keys[i]),
                            Factor = decimal.Parse(form[i].Replace('.', ','))
                        };
                        var spResponseCityFactor = new CityFactorRepository().Insert(cityFactor);
                        if (spResponseCityFactor.Code != "100")
                            return RedirectToAction("CityFactorGroupForm");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("CityFactorGroup");
        }

        [HttpPost]
        public ActionResult CityFactorGroupFormUpdate(FormCollection form)
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var cityFactorGroupId = form["hdCityFactorGroupId"];
                var cityIdList = form["hdcityId"];
                var processGroupId = form["processGroup"];
                var cityFactorStartDate = form["cityFactorStartDate"];
                var cityFactorEndDate = form["cityFactorEndDate"];
                var cityFactorGroup = new CityFactorGroup
                {
                    Id = long.Parse(cityFactorGroupId),
                    ProcessListId = long.Parse(processGroupId),
                    StartDate = DateTime.Parse(cityFactorStartDate),
                    EndDate = !String.IsNullOrEmpty(cityFactorEndDate) ? (DateTime?)DateTime.Parse(cityFactorEndDate) : null
                };
                var spResponseCityFactorGroup = new CityFactorGroupRepository().Update(cityFactorGroup);
                if (spResponseCityFactorGroup.Code == "100")
                {
                    List<CityFactor> cityFactorList = new List<CityFactor>();
                    int i = 5;
                    foreach (var cityId in cityIdList.Split(','))
                    {
                        var cityFactor = new CityFactor
                        {
                            Id = long.Parse(form.Keys[i]),
                            CityFactorGroupId = long.Parse(cityFactorGroupId),
                            CityId = long.Parse(cityId),
                            Factor = decimal.Parse(form[i].Replace('.', ','))
                        };
                        var spResponseCityFactor = new CityFactorRepository().Insert(cityFactor);
                        if (spResponseCityFactor.Code != "100")
                            return RedirectToAction("CityFactorGroupForm");
                        i++;
                    }
                    //    var spResponseCityFactor = new CityFactorRepository().UpdateEntities(cityFactorList);
                    //    if (spResponseCityFactor[0].Code == "100")
                    //        return RedirectToAction("CityFactorGroup");
                    //    else
                    //    {
                    //        throw new Exception(spResponseCityFactorGroup.Message);
                    //    }
                }
                else
                {
                    throw new Exception(spResponseCityFactorGroup.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("CityFactorGroup");
        }
        #endregion

        #region ProcessGroup
        // GET: Process Group List
        [LoginControl]
        public ActionResult ProcessGroup()
        {
            try
            {
                var type = Request["Type"] ?? "0";
                ViewBag.Type = type ?? "0";
                switch (type)
                {
                    case "0":
                        ViewBag.Title = "Hastalık Grupları";
                        ViewBag.ProcessListTitle = "Tanı Listesi";
                        break;
                    case "1":
                        ViewBag.Title = "İlaç Grupları";
                        ViewBag.ProcessListTitle = "İlaç Listesi";
                        break;
                    case "2":
                        ViewBag.Title = "Hizmet Grupları";
                        ViewBag.ProcessListTitle = "İşlem Listesi";
                        ViewBag.ProcessListTypeList = LookupHelper.GetLookupData(LookupTypes.ProcessList, showAll: true);
                        break;
                    default:
                        ViewBag.Title = "";
                        ViewBag.ProcessListTitle = "";
                        break;
                }
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                string condition = $"TYPE={type}";
                if (type.Equals("2"))
                {
                    condition = "(TYPE=2 OR TYPE=3 OR TYPE=4)";
                }

                var ProcessListList = new GenericRepository<ProcessList>().FindBy(condition);
                ViewBag.ProcessListList = ProcessListList;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetProcessGroupList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            dynamic parameters = new ExpandoObject();
            var processListType = form["type"];

            string processListTypeFilter = "PROCESS_LIST_TYPE=:processListType";
            if (form["type"].Equals("2"))
            {
                processListType = form["processListType"];
                if (processListType.IsInt())
                {
                    processListTypeFilter = "PROCESS_LIST_TYPE=:processListType";
                }
                else
                {
                    processListType = "2,3,4,5";
                    processListTypeFilter = "PROCESS_LIST_TYPE IN :processListType";
                }
            }
            parameters.processListType = processListType;
            parameters.processGroupName = form["processGroupName"];
            parameters.processListId = form["processList"];

            string whereConditition = $"{processListTypeFilter} AND";
            whereConditition += !form["processGroupName"].IsNull() ? $" PROCESS_GROUP_NAME LIKE '%:processGroupName%' AND" : "";
            whereConditition += (form["processList"].IsInt64() && long.Parse(form["processList"]) > 0) ? $" PROCESS_LIST_ID=:processListId AND" : "";

            var ProcessGroup = new GenericRepository<V_ProcessGroup>().FindByPaged(conditions: !whereConditition.IsNull() ? whereConditition.Substring(0, whereConditition.Length - 3) : "",
                                                  orderby: !sortColumnName.IsNull() ? $"{sortColumnName} {sortDirection}" : "PROCESS_GROUP_ID",
                                                  pageNumber: start,
                                                  rowsPerPage: length,
                                                  parameters: parameters);
            List<ProcessGroupList> ProcessGroupList = new List<ProcessGroupList>();

            if (ProcessGroup.Data != null)
            {
                foreach (var item in ProcessGroup.Data)
                {
                    ProcessGroupList listItem = new ProcessGroupList();

                    listItem.PROCESS_GROUP_ID = Convert.ToString(item.PROCESS_GROUP_ID);
                    listItem.PROCESS_GROUP_NAME = Convert.ToString(item.PROCESS_GROUP_NAME);
                    listItem.PROCESS_LIST_NAME = Convert.ToString(item.PROCESS_LIST_NAME);
                    listItem.PROCESS_LIST_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.ProcessList, item.PROCESS_LIST_TYPE);

                    ProcessGroupList.Add(listItem);
                }
            }

            return Json(new { data = ProcessGroupList, draw = Request["draw"], recordsTotal = ProcessGroup.TotalItemsCount, recordsFiltered = ProcessGroup.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [LoginControl]
        public ActionResult ProcessGroupDelete(Int64 Id)
        {
            var type = Request["Type"];
            try
            {
                var repo = new ProcessGroupRepository();
                var result = repo.FindById(Id);
                result.Status = "1";
                var spResponseProcessGroup = repo.Update(result);
                if (spResponseProcessGroup.Code == "100")
                {
                    TempData["Alert"] = $"swAlert('Silme İşlemi Başarılı','{result.Name} başarıyla silindi.','success')";
                }
                else
                    throw new Exception(spResponseProcessGroup.Code + " : " + spResponseProcessGroup.Message);
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction($"ProcessGroup", new { Type = type });
        }

        [LoginControl]
        public ActionResult ProcessGroupForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                var type = Request["Type"] ?? "0";
                ViewBag.Type = type ?? "0";

                switch (type)
                {
                    case "0":
                        ViewBag.Title = "Hastalık Grubu";
                        break;
                    case "1":
                        ViewBag.Title = "İlaç Grubu";
                        break;
                    case "2":
                        ViewBag.Title = "Hizmet Grubu";
                        break;
                    default:
                        ViewBag.Title = "";
                        break;
                }
                string condition = $"TYPE={type}";
                if (type.Equals("2"))
                {
                    condition = "(TYPE=2 OR TYPE=3 OR TYPE=4)";
                }

                var ProcessListList = new GenericRepository<ProcessList>().FindBy(condition);
                ViewBag.ProcessListList = ProcessListList;

                var ProcessGroupList = new ProcessGroupRepository().FindBy();
                ViewBag.ProcessGroupList = ProcessGroupList;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.isEdit = false;

                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title += " Güncelle";
                        ViewBag.isEdit = true;
                        if (id != 0)
                        {
                            var processGroup = new GenericRepository<V_ProcessGroup>().FindBy("PROCESS_GROUP_ID = :processGroupId", orderby: "PROCESS_GROUP_ID", parameters: new { processGroupId = id }).FirstOrDefault();
                            if (processGroup != null)
                            {
                                ViewBag.ProcessGroup = processGroup;

                                var processGroupId = processGroup.PROCESS_GROUP_ID;
                                var processesSelected = new GenericRepository<V_ProcessGroupProcess>().FindBy("PROCESS_GROUP_ID = :processGroupId", orderby: "PROCESS_GROUP_ID", parameters: new { processGroupId });

                                var listProcessSelectedId = new Dictionary<long, long>();

                                foreach (var item in processesSelected)
                                {
                                    long a = long.Parse(item.PROCESS_ID.ToString());
                                    long b = long.Parse(item.PROCESS_GROUP_PROCESS_ID.ToString());
                                    if (listProcessSelectedId.Keys.Contains(a) || listProcessSelectedId.Values.Contains(b))
                                        break;

                                    listProcessSelectedId.Add(a, b);
                                }
                                var processListId = processGroup.PROCESS_LIST_ID;

                                var processes = new GenericRepository<Process>().FindBy("PROCESS_LIST_ID = :processListId", orderby: "PARENT_ID NULLS FIRST", parameters: new { processListId });

                                int i = 1;
                                List<dynamic> listProcess = new List<dynamic>();

                                foreach (var item in processes)
                                {
                                    dynamic listItem = new System.Dynamic.ExpandoObject();

                                    listItem.id = i;
                                    listItem.ClientId = i;
                                    listItem.STATUS = item.Status;

                                    listItem.PROCESS_GROUP_PROCESS_ID = "0";
                                    listItem.PROCESS_ID = Convert.ToString(item.Id);
                                    listItem.PROCESS_LIST_ID = Convert.ToString(item.ProcessListId);

                                    listItem.PARENT_ID = Convert.ToString(item.ParentId);
                                    listItem.CODE = item.Code;
                                    listItem.NAME = item.Name.Replace("'", "\'");
                                    //listItem.AMOUNT = Convert.ToString(item.Amount);

                                    if (listProcessSelectedId.Keys.Contains(item.Id))
                                        listItem.isChecked = "checked";
                                    else
                                        listItem.isChecked = "";

                                    listProcess.Add(listItem);

                                    i++;
                                }

                                ViewBag.Proceses = listProcess.ToJSON(true);
                            }
                            else
                            {
                                TempData["Alert"] = $"swAlert('Bilgi','Seçilen İslem Grubu Bulunamadı. Lütfen Tekrar Deneyiniz.','warning')";
                            }
                        }
                        break;

                    default:
                        ViewBag.Title += " Ekle";
                        ViewBag.isEdit = false;
                        break;
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        public JsonResult ClearTempData(string key)
        {
            TempData[key] = string.Empty;

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = "OK",
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }
        public JsonResult Processes(string name, string code, string processListId)
        {
            List<Process> parentProcesses = new List<Process>();

            if (processListId.IsInt64())
            {
                var icd10ProcessList = ProcessListHelper.GetProcessList("ICD10");
                if (icd10ProcessList.Id == long.Parse(processListId))
                {
                    //string name = Request["name"];
                    //string code = Request["code"];
                    parentProcesses = ICD10Helper.GetICD10Data();
                    parentProcesses = parentProcesses.Where(p => p.ParentId == null && p.Name.ToUpper().Contains(name.ToUpper()) && p.Code.ToUpper().Contains(code.ToUpper())).ToList();
                    //foreach (var item in )
                    //{
                    //    if (item.Name"%a%")
                    //    {

                    //    }
                    //}
                }
                else
                {
                    GenericRepository<Process> processRepository = new GenericRepository<Process>();

                    parentProcesses = processRepository.FindBy("PROCESS_LIST_ID = :processListId AND PARENT_ID IS NULL", orderby: "CODE", parameters: new { processListId });
                    List<Process> childProcesses = processRepository.FindBy("PROCESS_LIST_ID = :processListId AND PARENT_ID IS NOT NULL", orderby: "CODE", parameters: new { processListId });

                    foreach (Process process in parentProcesses)
                    {
                        foreach (Process childProcess in childProcesses)
                        {
                            if (childProcess.ParentId == process.Id)
                            {
                                process.ChildProcesses.Add(childProcess);
                            }
                        }
                    }
                }
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = parentProcesses.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult Reason(string statusName, Int32 statusOrdinal)
        {
            GenericRepository<Reason> reasonRepository = new GenericRepository<Reason>();

            List<Reason> reasons = reasonRepository.FindBy("STATUS_NAME = :statusName AND STATUS_ORDINAL = :statusOrdinal", orderby: "DESCRIPTION ASC", parameters: new { statusName = new Dapper.DbString { Value = statusName, Length = 400 }, statusOrdinal = new Dapper.DbString { Value = statusOrdinal.ToString(), Length = 3 } });

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = reasons.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        public JsonResult Processes(FormCollection form)
        {
            var processListId = form["processList"];
            var processes = new GenericRepository<Process>().FindBy("PROCESS_LIST_ID = :processListId", orderby: "PARENT_ID NULLS FIRST", parameters: new { processListId });

            int i = 1;
            List<dynamic> listProcess = new List<dynamic>();

            foreach (var item in processes)
            {
                dynamic listItem = new System.Dynamic.ExpandoObject();

                listItem.id = i;
                listItem.ClientId = i;
                listItem.STATUS = item.Status;

                listItem.PROCESS_GROUP_PROCESS_ID = "0";
                listItem.PROCESS_ID = Convert.ToString(item.Id);
                listItem.PROCESS_LIST_ID = Convert.ToString(item.ProcessListId);

                listItem.PARENT_ID = Convert.ToString(item.ParentId);
                listItem.CODE = item.Code;
                listItem.NAME = item.Name;
                //listItem.AMOUNT = Convert.ToString(item.Amount);

                listProcess.Add(listItem);

                i++;
            }

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = listProcess.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        public ActionResult ProcessGroupFormSave(FormCollection form)
        {
            var type = form["type"];
            try
            {
                var processGroupName = form["processGroupName"];
                var processListId = form["hdProcessListID"];
                var parentProcessGroup = form["processGroupList"];
                var processGroupid = form["hdPROCESS_GROUP_ID"];

                var processGroup = new ProcessGroup
                {
                    Id = !String.IsNullOrEmpty(processGroupid) ? long.Parse(processGroupid) : 0,
                    ProcessListId = long.Parse(processListId),
                    Name = processGroupName
                };
                var spResponseProcessGroup = new ProcessGroupRepository().Insert(processGroup);
                if (spResponseProcessGroup.Code == "100")
                {
                    var processGroupId = spResponseProcessGroup.PkId;
                    var processGroupp = new ProcessGroupProcessRepository().FindBy("PROCESS_GROUP_ID = :processGroupId", parameters: new { processGroupId });
                    if (processGroupp.Count > 0)
                    {
                        foreach (var item in processGroupp)
                        {
                            item.Status = "1";
                            var spResponseProcessGroupProcess = new ProcessGroupProcessRepository().Update(item);
                            if (spResponseProcessGroupProcess.Code != "100")
                                throw new Exception(spResponseProcessGroupProcess.Message);
                        }
                    }
                    var rootProcessId = form["rootProcess"];
                    if (!String.IsNullOrEmpty(rootProcessId))
                    {
                        foreach (var item in rootProcessId.Split(','))
                        {
                            var processGroupProcess = new ProcessGroupProcess
                            {
                                ProcessGroupId = processGroupId,
                                ProcessId = long.Parse(item)
                            };
                            var spResponseProcessGroupProcess = new ProcessGroupProcessRepository().Insert(processGroupProcess);
                            if (spResponseProcessGroupProcess.Code != "100")
                                throw new Exception(spResponseProcessGroupProcess.Message);
                        }
                    }
                    var subProcessId = form["subProcess"];
                    if (!String.IsNullOrEmpty(subProcessId))
                    {
                        if (!String.IsNullOrEmpty(subProcessId))
                        {
                            foreach (var item in subProcessId.Split(','))
                            {
                                var processGroupProcess = new ProcessGroupProcess
                                {
                                    ProcessGroupId = processGroupId,
                                    ProcessId = long.Parse(item)
                                };
                                var spResponseProcessGroupProcess = new ProcessGroupProcessRepository().Insert(processGroupProcess);
                                if (spResponseProcessGroupProcess.Code != "100")
                                    throw new Exception(spResponseProcessGroupProcess.Message);
                            }
                        }
                    }
                    TempData["Alert"] = $"swAlert('İşlem Başarılı','{processGroupName} başarıyla kaydedildi.','success')";
                }
                else
                    throw new Exception(spResponseProcessGroup.Message);


            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return RedirectToAction($"ProcessGroup", new { Type = type });

        }
        #endregion

        [LoginControl]
        public ActionResult BankActive(FormCollection form)
        {
            string bankName = form["bankName"];
            int bankCode = Convert.ToInt32(form["bankCode"]);
            return View();
        }

        [HttpPost]
        [LoginControl]
        public JsonResult BankActiveList(FormCollection form)
        {

            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string filter = form["isFilter"];
            string whereConditition = form["bankFilterCode"].IsNull() ? "" : $" CODE='{form["bankFilterCode"]}' AND";
            whereConditition += form["bankFilterName"].IsNull() ? "" : $" NAME LIKE '%{form["bankFilterName"]}%' AND";
            var BankList = new GenericRepository<Bank>().FindByPaged(
                                                                           conditions: whereConditition.IsNull() ? "" : whereConditition.Substring(0, whereConditition.Length - 3),
                                                                           orderby: "ID",
                                                                           pageNumber: start,
                                                                           rowsPerPage: length, fetchHistoricRows: true);


            return Json(new { data = BankList.Data, draw = Request["draw"], recordsTotal = BankList.TotalItemsCount, recordsFiltered = BankList.TotalItemsCount, whereConditition = whereConditition }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult BankSave(FormCollection form)
        {
            string bankName = form["bankName"];
            string bankCode = form["bankCode"];
            int bankId = Convert.ToInt32(form["bank_ID"]);
            int bankIdBranch = Convert.ToInt32(form["bankID"]);
            int bankBranchId = Convert.ToInt32(form["bankBranch_ID"]);
            string bankBranchName = form["bankBranchName"];
            string bankBranchEditName = form["bankBranchEditName"];
            string bankBranchCode = form["bankBranchCode"];
            string bankBranchEditCode = form["bankBranchEditCode"];
            string isFilterBranch = form["isFilterBranch"];
            try
            {
                if (isFilterBranch == "1")
                {
                    BankBranch bankBranch = new BankBranch();
                    bankBranch.Id = bankBranchId;
                    bankBranch.BankId = bankIdBranch;
                    bankBranch.Name = bankBranchEditName;
                    bankBranch.Code = bankBranchEditCode;
            var spResponse = new GenericRepository<BankBranch>().Update(bankBranch);
                    
                }
                else
                {
                    if (bankId > 0)
                    {

                        BankBranch bankBranch = new BankBranch();
                        bankBranch.Name = bankBranchName;
                        bankBranch.Code = bankBranchCode;
                        bankBranch.BankId = bankId;

                        var spReponse = new GenericRepository<BankBranch>().Insert(bankBranch);
                        if (spReponse.Code=="100")
                        {

                        }
                    }
                    else
                    {
                        Bank bank = new Bank();

                        bank.Code = bankCode;
                        bank.Name = bankName;

                        new GenericRepository<Bank>().Insert(bank);
                    }

                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }


            return RedirectToAction("BankActive");
        }


        [HttpPost]
        [LoginControl]
        public ActionResult BankBranchList(FormCollection form)
        {

            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string filter = form["isFilter"];
            string bankID = form["bank_ID"];
            string whereConditition = $"BANK_ID={bankID}";
            var BankList = new GenericRepository<BankBranch>().FindByPaged(
                                                                           conditions: whereConditition,
                                                                           orderby: "ID",
                                                                           pageNumber: start,
                                                                           rowsPerPage: length);


            return Json(new { data = BankList.Data, draw = Request["draw"], recordsTotal = BankList.TotalItemsCount, recordsFiltered = BankList.TotalItemsCount, whereConditition = whereConditition }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        [LoginControl]
        public JsonResult BankBranchDelete(string bankBranchId , string bankId)
        {
            var resultJson = new AjaxResultDto<ExclusionSaveResult>();
            try
            {
                

                BankBranch bankBranch = new BankBranch();
                bankBranch.BankId = Convert.ToInt64(bankId);
                bankBranch.Id = Convert.ToInt64(bankBranchId);
                bankBranch.Status = ((int)Status.SILINDI).ToString();

                var spReponse = new GenericRepository<BankBranch>().Insert(bankBranch);

            }
            catch (Exception ex)
            {
                resultJson.ResultMessage = ex.Message;


            }

            return Json(resultJson.ToJSON(), JsonRequestBehavior.AllowGet);
        }

            #region
            public JsonResult DoctorBranchs()
        {
            List<DoctorBranch> doctorBranchs = new GenericRepository<DoctorBranch>().FindAll();

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = doctorBranchs.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        #endregion
    }
}