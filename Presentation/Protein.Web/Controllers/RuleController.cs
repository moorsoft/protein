﻿using Newtonsoft.Json;
using Protein.Common.Constants;
using Protein.Common.Extensions;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Web.ActionFilter.LoginAttr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Web.Controllers
{
    [LoginControl]
    public class RuleController : Controller
    {
        #region RuleCategory
        [LoginControl]
        public ActionResult RuleCategory()
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var RuleCategoryTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.RuleCategory, showAll: true);
                ViewBag.RuleCategoryTypeList = RuleCategoryTypeList;

                var ResultTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.RuleCategoryResult, showAll: true);
                ViewBag.ResultTypeList = ResultTypeList;


                var result = new GenericRepository<V_RuleCategory>().FindBy(orderby: "RULE_CATEGORY_ID");
                ViewBag.RuleCategoryList = result;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult RuleCategoryFilter(FormCollection form)
        {
            try
            {
                var RuleCategoryTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.RuleCategory, showAll: true);
                ViewBag.RuleCategoryTypeList = RuleCategoryTypeList;

                var ResultTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.RuleCategoryResult, showAll: true);
                ViewBag.ResultTypeList = ResultTypeList;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.RuleCategoryName = form["RULE_CATEGORY_NAME"];
                ViewBag.RuleCategoryType = form["RULE_CATEGORY_TYPE"];
                ViewBag.ResultType = form["RESULT_TYPE"];

                string whereCondititon = !String.IsNullOrEmpty(form["RULE_CATEGORY_NAME"]) ? $" RULE_CATEGORY_NAME LIKE '%{form["RULE_CATEGORY_NAME"]}%' AND" : "";
                whereCondititon += !String.IsNullOrEmpty(form["RULE_CATEGORY_TYPE"]) ? $" RULE_CATEGORY_TYPE = {form["RULE_CATEGORY_TYPE"]} AND" : "";
                whereCondititon += !String.IsNullOrEmpty(form["RESULT_TYPE"]) ? $" RESULT_TYPE = {form["RESULT_TYPE"]} AND" : "";

                var result =new GenericRepository<V_RuleCategory>().FindBy(whereCondititon.IsNull() ? string.Empty : whereCondititon.Substring(0, whereCondititon.Length - 3),
                                                orderby: "RULE_CATEGORY_ID");
                ViewBag.RuleCategoryList = result;

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
                return RedirectToAction("RuleCategory");
            }
            return View("RuleCategory");
        }
        [LoginControl]
        public ActionResult RuleCategoryDelete(Int64 id)
        {
            try
            {
                var repo = new RuleCategoryRepository();
                var result = repo.FindById(id);
                result.Status = "1";
                var spResponse = repo.Update(result);
                if (spResponse.Code == "100")
                    TempData["Alert"] = $"swAlert('İşlem Başarılı','{result.Name} Silme İşlemi Başarıyla Gerçekleştirildi.','success')";
                else
                    throw new Exception(spResponse.Code + " : " + spResponse.Message);
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("RuleCategory");
        }
        [LoginControl]
        public ActionResult RuleCategoryForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var RuleCategoryTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.RuleCategory);
                ViewBag.CategoryTypeList = RuleCategoryTypeList;

                var RuleCategoryResultTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.RuleCategoryResult);
                ViewBag.ResultTypeList = RuleCategoryResultTypeList;

                ViewBag.isEdit = false;
                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Kural Kategorisi Güncelle";
                        if (id > 0)
                        {
                            var ruleCategory = new RuleCategoryRepository().FindById(id);
                            ViewBag.RuleCategory = ruleCategory ?? null;
                            ViewBag.isEdit = false;
                        }
                        break;

                    default:
                        ViewBag.Title = "Kural Kategorisi Ekle";
                        break;
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
                return RedirectToAction("RuleCategory");
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult RuleCategoryFormSave(FormCollection form)
        {
            try
            {
                var ruleCategoryId = long.Parse(form["hdRuleCategoryId"]);
                var name = form["ruleCategoryName"];
                var categoryType = form["ruleCategoryTypeList"];
                var resultType = form["ruleCategoryResultList"];
                var ruleCategory = new RuleCategory()
                {
                    Id = ruleCategoryId,
                    Name = name,
                    Type = categoryType,
                    ResultType = resultType
                };
                var spResponse = new RuleCategoryRepository().Insert(ruleCategory);
                if (spResponse.Code != "100")
                {
                    throw new Exception(spResponse.Code + " : " + spResponse.Message);
                }
                return RedirectToAction("RuleCategory");
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("RuleCategory");
        }
        #endregion

        #region Rule
        [LoginControl]
        public ActionResult Rule()
        {
            try
            {
                ViewBag.RuleCategoryId = string.Empty;
                ViewBag.RuleName = string.Empty;
                ViewBag.Result = string.Empty;
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

              
                var resultCategory = new GenericRepository<V_RuleCategory>().FindBy(orderby: "RULE_CATEGORY_ID");
                ViewBag.RuleCategoryList = resultCategory ?? null;

                var result = new GenericRepository<V_Rule>().FindBy(orderby: "RULE_ID");
                ViewBag.RuleList = result ?? null;

                ViewBag.RuleCategoryTypeList = LookupHelper.GetLookupData(LookupTypes.RuleCategory);
                ViewBag.ResultTypeList = LookupHelper.GetLookupData(LookupTypes.RuleCategoryResult);
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult RuleFilter(FormCollection form)
        {
            try
            {
                ViewBag.RuleCategoryId = form["ruleCategoryList"];
                ViewBag.RuleName = form["ruleName"];
                ViewBag.Result = form["ruleResult"];
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.RuleCategoryTypeList = LookupHelper.GetLookupData(LookupTypes.RuleCategory);
                ViewBag.ResultTypeList = LookupHelper.GetLookupData(LookupTypes.RuleCategoryResult);

                var resultCategory = new GenericRepository<V_RuleCategory>().FindBy(orderby: "RULE_CATEGORY_TYPE");
                ViewBag.RuleCategoryList = resultCategory ?? null;

                string whereCondititon = !String.IsNullOrEmpty(form["ruleCategoryList"]) ? $" RULE_CATEGORY_ID={long.Parse(form["ruleCategoryList"])} AND" : "";
                whereCondititon += !String.IsNullOrEmpty(form["ruleName"]) ? $" RULE_NAME LIKE '%{form["ruleName"]}%' AND" : "";
                whereCondititon += !String.IsNullOrEmpty(form["ruleResult"]) ? $" RESULT LIKE '%{form["ruleResult"]}%' AND" : "";

                var result = new GenericRepository<V_Rule>().FindBy(whereCondititon.IsNull() ? string.Empty : whereCondititon.Substring(0, whereCondititon.Length - 3), orderby: "RULE_ID"); 
                ViewBag.RuleList = result ?? null;

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View("Rule");
        }
        [LoginControl]
        public ActionResult RuleDelete(Int64 id)
        {
            try
            {
                var repo = new RuleRepository();
                var result = repo.FindById(id);
                result.Status = "1";
                var spResponse = repo.Update(result);
                if (spResponse.Code == "100")
                    TempData["Alert"] = $"swAlert('İşlem Başarılı','{result.Name} Silme İşlemi Başarıyla Gerçekleştirildi.','success')";
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Rule");
        }
        [LoginControl]
        public ActionResult RuleForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                ViewBag.isEdit = false;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var resultCategory = new GenericRepository<V_RuleCategory>().FindBy(orderby: "");
                ViewBag.RuleCategoryList = resultCategory;
                ViewBag.ClaimStatusList = LookupHelper.GetLookupData(LookupTypes.ClaimStatus);

                //var resultProvider =new GenericRepository<V_Provider>().FindBy(orderby: "PROVIDER_ID");
                //ViewBag.ProviderList = resultProvider;

                //var resultCoverage = new GenericRepository<V_Coverage>().FindBy( orderby: "COVERAGE_ID");
                //ViewBag.CoverageList = resultCoverage;

                //var resultProcessList =new GenericRepository<V_ProcessList>().FindBy(orderby: "PROCESS_LIST_NAME");
                //ViewBag.ProcessList = resultProcessList;

                //var resultProcessGroupList = new GenericRepository<V_ProcessGroup>().FindBy(orderby: "PROCESS_GROUP_NAME");
                //ViewBag.ProcessGroupList = resultProcessGroupList;

                //var ProviderGroupList = new ProviderGroupRepository().FindBy();
                //ViewBag.ProviderGroupList = ProviderGroupList;


                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Kural Güncelle";
                        if (id > 0)
                        {
                            var rule = new GenericRepository<V_Rule>().FindBy("RULE_ID =:id", orderby: "RULE_ID", parameters: new { id = id }).FirstOrDefault();
                            if (rule != null)
                            {
                                ViewBag.Rule = rule;

                                //var ProviderGroup =new GenericRepository<V_ProviderGroupRule>().FindBy("RULE_ID =:id",orderby: "RULE_ID", parameters: new { id = id });
                                //int i = 1;
                                //List<dynamic> listProviderGroup = new List<dynamic>();

                                //foreach (var item in ProviderGroup)
                                //{
                                //    dynamic listItem = new System.Dynamic.ExpandoObject();

                                //    listItem.id = item.PROVIDER_GROUP_ID;
                                //    listItem.STATUS = item.STATUS;

                                //    listItem.PROVIDER_GROUP_RULE_ID = Convert.ToString(item.PROVIDER_GROUP_RULE_ID);
                                //    listItem.PROVIDER_GROUP_ID = Convert.ToString(item.PROVIDER_GROUP_ID);

                                //    listItem.NAME = Convert.ToString(item.PROVIDER_GROUP_NAME);
                                //    listItem.TAX_NUMBER = item.PROVIDER_GROUP_TAX_NUMBER;

                                //    listProviderGroup.Add(listItem);

                                //    i++;
                                //}
                                //ViewBag.ProviderGroups = listProviderGroup.ToJSON();

                                //var Provider = new GenericRepository<V_ProviderRule>().FindBy("RULE_ID =:id", orderby: "RULE_ID", parameters: new { id = id });
                                //i = 1;
                                //List<dynamic> listProvider = new List<dynamic>();

                                //foreach (var item in Provider)
                                //{
                                //    dynamic listItem = new System.Dynamic.ExpandoObject();

                                //    listItem.id = item.PROVIDER_ID;
                                //    listItem.STATUS = item.STATUS;

                                //    listItem.PROVIDER_RULE_ID = Convert.ToString(item.PROVIDER_RULE_ID);
                                //    listItem.PROVIDER_ID = Convert.ToString(item.PROVIDER_ID);

                                //    listItem.PROVIDER_NAME = item.PROVIDER_NAME;
                                //    listItem.PROVIDER_TITLE = item.PROVIDER_TITLE;
                                //    listItem.PROVIDER_TYPE_TEXT =LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Provider, item.PROVIDER_TYPE);
                                //    listItem.CITY_NAME = item.CITY_NAME;
                                //    listItem.COUNTY_NAME = item.COUNTY_NAME;

                                //    listProvider.Add(listItem);

                                //    i++;
                                //}
                                //ViewBag.Providers = listProvider.ToJSON();

                                //var Coverage = new GenericRepository<V_CoverageRule>().FindBy("RULE_ID =:id", orderby: "RULE_ID", parameters: new { id = id });
                                //i = 1;
                                //List<dynamic> listCoverage = new List<dynamic>();

                                //foreach (var item in Coverage)
                                //{
                                //    dynamic listItem = new System.Dynamic.ExpandoObject();

                                //    listItem.id = item.COVERAGE_ID;
                                //    listItem.STATUS = item.STATUS;

                                //    listItem.COVERAGE_RULE_ID = Convert.ToString(item.COVERAGE_RULE_ID);
                                //    listItem.COVERAGE_ID = Convert.ToString(item.COVERAGE_ID);

                                //    listItem.COVERAGE_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Coverage, item.COVERAGE_TYPE);
                                //    listItem.COVERAGE_NAME = item.COVERAGE_NAME;

                                //    listCoverage.Add(listItem);

                                //    i++;
                                //}
                                //ViewBag.Coverages = listCoverage.ToJSON();

                                //var Process = new GenericRepository<V_ProcessRule>().FindBy("RULE_ID =:id", orderby: "RULE_ID", parameters: new { id = id });
                                //i = 1;
                                //List<dynamic> listProcess = new List<dynamic>();

                                //foreach (var item in Process)
                                //{
                                //    dynamic listItem = new System.Dynamic.ExpandoObject();

                                //    listItem.id = item.PROCESS_ID;
                                //    listItem.STATUS = item.STATUS;

                                //    listItem.PROCESS_RULE_ID = Convert.ToString(item.PROCESS_RULE_ID);
                                //    listItem.PROCESS_ID = Convert.ToString(item.PROCESS_ID);

                                //    listItem.PROCESS_CODE = item.PROCESS_CODE;
                                //    listItem.PROCESS_NAME = item.PROCESS_NAME;

                                //    listProcess.Add(listItem);

                                //    i++;
                                //}
                                //ViewBag.Processs = listProcess.ToJSON();
                                ViewBag.isEdit = true;
                            }
                            else
                            {
                                TempData["Alert"] = $"swAlert('Bilgi','Aranılan Kural Bilgisi Bulunamadı. Lütfen Tekrar Deneyiniz...','warning')";
                                return RedirectToAction("Rule");
                            }
                        }
                        break;

                    default:
                        ViewBag.Title = "Kural Ekle";
                        break;
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        public JsonResult GetProcessbyProcessList()
        {
            try
            {
                var processListId = Request["processListId"];
                var processes = new ProcessRepository().FindBy("PROCESS_LIST_ID = :processListId", orderby: "NAME", parameters: new { processListId });

                int i = 1;
                List<dynamic> listProcess = new List<dynamic>();

                foreach (var item in processes)
                {
                    dynamic listItem = new System.Dynamic.ExpandoObject();

                    listItem.id = Convert.ToString(item.Id);
                    listItem.ClientId = i;
                    listItem.STATUS = item.Status;

                    listItem.PROCESS_RULE_ID = "0";
                    listItem.PROCESS_GROUP_PROCESS_ID = "0";
                    listItem.PROCESS_ID = Convert.ToString(item.Id);
                    listItem.PROCESS_LIST_ID = Convert.ToString(item.ProcessListId);

                    listItem.PARENT_ID = Convert.ToString(item.ParentId);
                    listItem.CODE = item.Code;
                    listItem.NAME = item.Name;
                    listItem.isChecked = "";
                    //listItem.AMOUNT = Convert.ToString(item.Amount);

                    listProcess.Add(listItem);

                    i++;
                }
                return new JsonResult()
                {
                    MaxJsonLength = Int32.MaxValue,
                    Data = listProcess.ToJSON(),
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return null;
        }

        [HttpPost]
        public JsonResult GetProcessbyProcessGroup()
        {
            try
            {
                var processGroupId = Request["processGroupId"];
                var processes =new GenericRepository<V_ProcessGroupProcess>().FindBy("PROCESS_GROUP_ID = :processGroupId",
                                                         orderby: "PROCESS_NAME",
                                                         parameters: new { processGroupId = processGroupId });

                int i = 1;
                List<dynamic> listProcess = new List<dynamic>();

                foreach (var item in processes)
                {
                    dynamic listItem = new System.Dynamic.ExpandoObject();

                    listItem.id = Convert.ToString(item.PROCESS_GROUP_ID);
                    listItem.ClientId = i;
                    listItem.STATUS = item.STATUS;

                    listItem.PROCESS_RULE_ID = "0";
                    listItem.PROCESS_GROUP_PROCESS_ID = "0";
                    listItem.PROCESS_ID = Convert.ToString(item.PROCESS_ID);
                    listItem.PROCESS_GROUP_ID = Convert.ToString(item.PROCESS_GROUP_ID);

                    listItem.PARENT_ID = item.PROCESS_PARENT_ID != null ? Convert.ToString(item.PROCESS_PARENT_ID) : "";
                    listItem.CODE = item.PTOCESS_CODE;
                    listItem.NAME = item.PROCESS_NAME;
                    listItem.isChecked = "";
                    //listItem.AMOUNT = Convert.ToString(item.Amount);

                    listProcess.Add(listItem);

                    i++;
                }
                return new JsonResult()
                {
                    MaxJsonLength = Int32.MaxValue,
                    Data = listProcess.ToJSON(),
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return null;
        }

        [HttpPost]
        public JsonResult AddProviderGroupRule(string providerGroupId)
        {
            object result = null;

            try
            {
                if (!string.IsNullOrEmpty(providerGroupId))
                {
                    var ProviderGroup = new ProviderGroupRepository().FindBy("ID=" + providerGroupId, orderby: "NAME");
                    int i = 1;
                    List<dynamic> listProviderGroup = new List<dynamic>();

                    foreach (var item in ProviderGroup)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = Convert.ToString(item.Id);
                        listItem.STATUS = item.Status;

                        listItem.PROVIDER_GROUP_RULE_ID = "0";
                        listItem.PROVIDER_GROUP_ID = Convert.ToString(item.Id);

                        listItem.NAME = Convert.ToString(item.Name);
                        listItem.TAX_NUMBER = item.TaxNumber;

                        listProviderGroup.Add(listItem);

                        i++;
                    }

                    return new JsonResult()
                    {
                        MaxJsonLength = Int32.MaxValue,
                        Data = listProviderGroup.ToJSON(),
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddProviderRule(string providerId)
        {
            object result = null;
            try
            {
                if (!string.IsNullOrEmpty(providerId))
                {
                    var Provider = new GenericRepository<V_Provider>().FindBy("PROVIDER_ID=:providerId", orderby: "PROVIDER_ID", parameters: new { providerId });
                    int i = 1;
                    List<dynamic> listProviderGroup = new List<dynamic>();

                    foreach (var item in Provider)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = Convert.ToString(item.PROVIDER_ID);
                        listItem.STATUS = item.STATUS;

                        listItem.PROVIDER_RULE_ID = "0";
                        listItem.PROVIDER_ID = Convert.ToString(item.PROVIDER_ID);

                        listItem.PROVIDER_NAME = item.PROVIDER_NAME;
                        listItem.PROVIDER_TITLE = item.PROVIDER_TITLE;
                        listItem.PROVIDER_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Provider, item.PROVIDER_TYPE);
                        listItem.CITY_NAME = item.CITY_NAME;
                        listItem.COUNTY_NAME = item.COUNTY_NAME;

                        listProviderGroup.Add(listItem);

                        i++;
                    }

                    return new JsonResult()
                    {
                        MaxJsonLength = Int32.MaxValue,
                        Data = listProviderGroup.ToJSON(),
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddCoverageRule(string coverageId)
        {
            object result = null;
            try
            {
                if (!string.IsNullOrEmpty(coverageId))
                {
                    var Coverage = new GenericRepository<V_Coverage>().FindBy("COVERAGE_ID=:coverageId", orderby: "COVERAGE_ID", parameters: new { coverageId });
                    int i = 1;
                    List<dynamic> listCoverage = new List<dynamic>();

                    foreach (var item in Coverage)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = Convert.ToString(item.COVERAGE_ID);
                        listItem.STATUS = item.STATUS;

                        listItem.COVERAGE_RULE_ID = "0";
                        listItem.COVERAGE_ID = Convert.ToString(item.COVERAGE_ID);

                        listItem.COVERAGE_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Coverage, item.COVERAGE_TYPE) ;
                        listItem.COVERAGE_NAME = item.COVERAGE_NAME;

                        listCoverage.Add(listItem);

                        i++;
                    }

                    return new JsonResult()
                    {
                        MaxJsonLength = Int32.MaxValue,
                        Data = listCoverage.ToJSON(),
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddProcess(string processId)
        {
            object result = null;
            try
            {
                if (!string.IsNullOrEmpty(processId))
                {
                    var Process = new GenericRepository<V_Process>().FindBy("PROCESS_ID=:processId", orderby: "PROCESS_ID", parameters: new { processId });
                    int i = 1;
                    List<dynamic> listProcess = new List<dynamic>();

                    foreach (var item in Process)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = Convert.ToString(item.PROCESS_ID);
                        listItem.STATUS = item.STATUS;

                        listItem.PROCESS_RULE_ID = "0";
                        listItem.PROCESS_ID = Convert.ToString(item.PROCESS_ID);

                        listItem.PROCESS_CODE = item.PROCESS_CODE;
                        listItem.PROCESS_NAME = item.PROCESS_NAME;

                        listProcess.Add(listItem);

                        i++;
                    }

                    return new JsonResult()
                    {
                        MaxJsonLength = Int32.MaxValue,
                        Data = listProcess.ToJSON(),
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult RuleFormSave(FormCollection form)
        {
            try
            {
                var ruleResult = form["ruleResult"];
                var ruleId = long.Parse(form["hdRuleId"]);
                var ruleName = form["ruleName"];
                var ruleCategoryId = form["ruleCategory"];
                var claimStaus = form["ruleClaimStatus"];
                var reasonId = form["ruleClaimReason"];


                var rule = new Rule
                {
                    Id = ruleId,
                    Name = ruleName,
                    Result = ruleResult,
                    CategoryId = long.Parse(ruleCategoryId),
                    ClaimStatus = claimStaus,
                    ReasonId = reasonId.IsInt64() ? (long?)long.Parse(reasonId) : null
                };
                var spResponseRule = new RuleRepository().Insert(rule);
                if (spResponseRule.Code == "100")
                {
                    ruleId = spResponseRule.PkId;

                    #region Provider - ProviderLisrt - Coverage - Process
                    //var providerGroupRules = form["hdProviderGroupList"];
                    //if (!String.IsNullOrEmpty(providerGroupRules))
                    //{
                    //    List<dynamic> providerGroupRuleList = new List<dynamic>();
                    //    providerGroupRuleList = JsonConvert.DeserializeObject<List<dynamic>>(providerGroupRules);

                    //    foreach (var providerGroupRule in providerGroupRuleList)
                    //    {
                    //        if ((providerGroupRule.STATUS == "1" && providerGroupRule.PROVIDER_GROUP_RULE_ID != 0) || providerGroupRule.STATUS == "0")
                    //        {
                    //            var TproviderGroupRule = new ProviderGroupRule
                    //            {
                    //                Id = providerGroupRule.PROVIDER_GROUP_RULE_ID,
                    //                ProviderGroupId = providerGroupRule.PROVIDER_GROUP_ID,
                    //                RuleId = ruleId
                    //            };
                    //            var spResponseProviderRule = new ProviderGroupRuleRepository().Insert(TproviderGroupRule);
                    //            if (spResponseProviderRule.Code != "100")
                    //                throw new Exception(spResponseProviderRule.Code + " : " + spResponseProviderRule.Message);
                    //        }
                    //    }
                    //}

                    //var providerRules = form["hdProviderList"];
                    //if (!String.IsNullOrEmpty(providerRules))
                    //{
                    //    List<dynamic> providerRuleList = new List<dynamic>();
                    //    providerRuleList = JsonConvert.DeserializeObject<List<dynamic>>(providerRules);

                    //    foreach (var providerRule in providerRuleList)
                    //    {
                    //        if ((providerRule.STATUS == "1" && providerRule.PROVIDER_RULE_ID != 0) || providerRule.STATUS == "0")
                    //        {
                    //            var TproviderRule = new ProviderRule
                    //            {
                    //                Id = long.Parse(providerRule.PROVIDER_RULE_ID.ToString()),
                    //                ProviderId = providerRule.PROVIDER_ID,
                    //                RuleId = ruleId
                    //            };
                    //            var spResponseProviderRule = new ProviderRuleRepository().Insert(TproviderRule);
                    //            if (spResponseProviderRule.Code != "100")
                    //                throw new Exception(spResponseProviderRule.Code + " : " + spResponseProviderRule.Message);
                    //        }
                    //    }
                    //}

                    //var coverageRules = form["hdCoverageList"];
                    //if (!String.IsNullOrEmpty(coverageRules))
                    //{
                    //    List<dynamic> coverageRuleList = new List<dynamic>();
                    //    coverageRuleList = JsonConvert.DeserializeObject<List<dynamic>>(coverageRules);

                    //    foreach (var coverageRule in coverageRuleList)
                    //    {
                    //        if ((coverageRule.STATUS == "1" && coverageRule.COVERAGE_RULE_ID != 0) || coverageRule.STATUS == "0")
                    //        {
                    //            var TcoverageRule = new CoverageRule
                    //            {
                    //                Id = coverageRule.COVERAGE_RULE_ID,
                    //                CoverageId = coverageRule.COVERAGE_ID,
                    //                RuleId = ruleId
                    //            };
                    //            var spResponseCoverageRule = new CoverageRuleRepository().Insert(TcoverageRule);
                    //            if (spResponseCoverageRule.Code != "100")
                    //                throw new Exception(spResponseCoverageRule.Code + " : " + spResponseCoverageRule.Message);
                    //        }
                    //    }
                    //}

                    //var ProcessListRules = form["hdProcessList"];
                    //if (!String.IsNullOrEmpty(ProcessListRules))
                    //{
                    //    List<dynamic> processRuleList = new List<dynamic>();
                    //    processRuleList = JsonConvert.DeserializeObject<List<dynamic>>(coverageRules);

                    //    foreach (var processRule in processRuleList)
                    //    {
                    //        if ((processRule.STATUS == "1" && processRule.PROCESS_RULE_ID != 0) || processRule.STATUS == "0")
                    //        {
                    //            var TprocessRule = new ProcessRule
                    //            {
                    //                Id = processRule.PROCESS_RULE_ID,
                    //                ProcessId = processRule.PROCESS_ID,
                    //                RuleId = ruleId
                    //            };
                    //            var spResponseProcessRule = new ProcessRuleRepository().Insert(TprocessRule);
                    //            if (spResponseProcessRule.Code != "100")
                    //                throw new Exception(spResponseProcessRule.Code + " : " + spResponseProcessRule.Message);
                    //        }
                    //    }
                    //}
                    #endregion
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Rule");
        }
        #endregion

        #region RuleGroup
        [LoginControl]
        public ActionResult RuleGroup()
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.RuleGroupName = string.Empty;

                //var ruleGroupList = new RuleGroupRepository().FindBy();
                var ruleGroupList = new GenericRepository<V_RuleGroup>().FindBy(orderby: "RULE_GROUP_ID");
                ViewBag.RuleGroupList = ruleGroupList;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult RuleGroupFilter(FormCollection form)
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.RuleGroupName = form["ruleGroupName"];

                string whereCondititon = !String.IsNullOrEmpty(form["ruleGroupName"]) ? $"RULE_GROUP_NAME LIKE '%{form["ruleGroupName"]}%' AND" : "";

               // var ruleGroupList = new RuleGroupRepository().FindBy(whereCondititon);
                var ruleGroupList =new GenericRepository<V_RuleGroup>().FindBy( string.IsNullOrEmpty(whereCondititon) ? string.Empty : whereCondititon.Substring(0, whereCondititon.Length - 3),  orderby: "RULE_GROUP_ID");
                ViewBag.RuleGroupList = ruleGroupList ?? null;              
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View("RuleGroup");
        }
        [LoginControl]
        public ActionResult RuleGroupDelete(Int64 id)
        {
            try
            {
                var repo = new RuleGroupRepository();
                var result = repo.FindById(id);
                result.Status = "1";
                var spResponse = repo.Update(result);
                if (spResponse.Code == "100")
                    TempData["Alert"] = $"swAlert('İşlem Başarılı','{result.Name} Silme İşlemi Başarıyla Gerçekleştirildi.','success')";
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("RuleGroup");
        }
        [LoginControl]
        public ActionResult RuleGroupForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var ruleList = new RuleRepository().FindBy();
                ViewBag.RuleList = ruleList;

                ViewBag.SelectRuleId = null;

                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Kural Grubu Güncelle";
                        if (id > 0)
                        {
                            var ruleGroupList = new RuleGroupRepository().FindById(id);
                            if (ruleGroupList != null)
                            {
                                ViewBag.RuleGroup = ruleGroupList;

                                var SelectruleList = new RuleGroupRuleRepository().FindBy($"RULE_GROUP_ID={ruleGroupList.Id}");
                                if (SelectruleList.Any())
                                {
                                    List<long> l = new List<long>();
                                    foreach (var item in SelectruleList)
                                    {
                                        l.Add(item.RuleId);
                                    }
                                    ViewBag.SelectRuleId = l;
                                }
                            }
                        }
                        ViewBag.FormDisabled = "";
                        break;

                    default:
                        ViewBag.Title = "Kural Grubu Ekle";
                        ViewBag.FormDisabled = "";
                        break;
                }

            }

            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";


            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult RuleGroupFormSave(FormCollection form)
        {
            try
            {
                var ruleGroupName = form["ruleGroupName"];
                var ruleGroup = new RuleGroup
                {
                    Name = ruleGroupName
                };
                var spResponseRuleGroup = new RuleGroupRepository().Insert(ruleGroup);
                if (spResponseRuleGroup.Code == "100")
                {
                    var ruleIdList = form["ruleChecked"];
                    foreach (var item in ruleIdList.Split(','))
                    {
                        var ruleGroupRule = new RuleGroupRule
                        {
                            RuleId = long.Parse(item.ToString()),
                            RuleGroupId = spResponseRuleGroup.PkId
                        };
                        var spResponse = new RuleGroupRuleRepository().Insert(ruleGroupRule);
                        if (spResponse.Code != "100")
                            throw new Exception(spResponse.Code + " : " + spResponse.Message);
                    }
                    TempData["Alert"] = $"swAlert('İşlemi Başarılı','{ruleGroup.Name} başarıyla kaydedildi.','success')";
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";

            }
            return RedirectToAction("RuleGroup");
        }

        [HttpPost]
        [LoginControl]
        public ActionResult RuleGroupFormUpdate(FormCollection form)
        {
            try
            {
                var ruleGroupId = form["hdRuleGroupId"];
                var ruleGroupName = form["ruleGroupName"];
                var ruleGroup = new RuleGroup
                {
                    Id = long.Parse(ruleGroupId),
                    Name = ruleGroupName
                };
                var spResponseRuleGroup = new RuleGroupRepository().Update(ruleGroup);
                if (spResponseRuleGroup.Code == "100")
                {
                    var ruleIdList = form["ruleChecked"];
                    foreach (var item in ruleIdList.Split(','))
                    {
                        var ruleGroupRule = new RuleGroupRule
                        {
                            RuleId = long.Parse(item.ToString()),
                            RuleGroupId = spResponseRuleGroup.PkId
                        };
                        var spResponse = new RuleGroupRuleRepository().Insert(ruleGroupRule);
                        if (spResponse.Code != "100")
                            throw new Exception(spResponse.Code + " : " + spResponse.Message);
                    }
                    TempData["Alert"] = $"swAlert('İşlemi Başarılı','{ruleGroup.Name} başarıyla kaydedildi.','success')";
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("RuleGroup");
        }
        #endregion
    }
}