﻿using Protein.Business.Abstract.Import;
using Protein.Business.Concrete.Import.Agency;
using Protein.Business.Concrete.Import.Claim;
using Protein.Business.Concrete.Import.Payroll;
using Protein.Business.Concrete.Media;
using Protein.Business.Concrete.Responses;
using Protein.Business.Enums.Import;
using Protein.Business.Enums.Media;
using Protein.Business.Import.Doctor;
using Protein.Business.Import.Policy;
using Protein.Common.Dto.MediaObjects;
using Protein.Web.Models.ImportModel;
using Protein.Web.Models.MediaModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Web.Controllers
{
    public class AsyncJobController : AsyncController
    {
        private static IDictionary<Guid, double> tasks = new Dictionary<Guid, double>();
        private static IDictionary<Guid, string> ImportErrorList = new Dictionary<Guid, string>();
        private static ImportResponse response = new ImportResponse();
        private static string FormId = "";
        private ImportObjectType ObjectType;
        // GET: AsyncJob
        public ActionResult MediaUploader(MediaVM model)
        {
            if (model.MEDIA_PATH == null && model.BaseId < 1)
            {
                return new JsonResult()
                {
                    Data = new { Message = "Dosya Bulunamadı!", IsSuccess = false }
                };
            }

            if (model.MEDIA_PATH != null)
            {
                if (model.MEDIA_PATH.ContentLength < 1 && model.BaseId < 1)
                {
                    return new JsonResult()
                    {
                        Data = new { Message = "Dosya Bulunamadı!", IsSuccess = false }
                    };
                }
            }

            if (model.BaseId < 1)
            {
                if (model.MEDIA_PATH.ContentLength > MediaRules.FileSizeLimit)
                {
                    return new JsonResult()
                    {
                        Data = new { Message = "Dosya boyutu " + ((MediaRules.FileSizeLimit / 1024) / 1024).ToString() + " MB'dan fazla olamaz", IsSuccess = false }
                    };
                }
            }


            MediaResponse response = new MediaResponse();

            switch (model.MediaObjectType)
            {
                case MediaObjectType.T_PRODUCT_MEDIA:
                    MediaDto productMedia = new MediaDto
                    {
                        Name = model.MediaName,
                        BaseId = model.BaseId,
                        ObjectId = model.ObjectId,
                        MediaId = model.MediaId
                    };
                    if (model.MEDIA_PATH != null)
                    {
                        productMedia.MEDIA_PATH = model.MEDIA_PATH;
                        model.FileName = model.MEDIA_PATH.FileName;
                        productMedia.FileName = model.FileName;
                    }
                    response = new MediaCreator<ProductMedia>().CreateIt(productMedia);

                    break;
                case MediaObjectType.T_CONTRACT_MEDIA:
                    break;

                case MediaObjectType.T_PACKAGE_MEDIA:
                    MediaDto packageMedia = new MediaDto
                    {
                        Name = model.MediaName,
                        BaseId = model.BaseId,
                        ObjectId = model.ObjectId,
                        MediaId = model.MediaId
                        //MediaObject = new ProductMedia()
                    };
                    if (model.MEDIA_PATH != null)
                    {
                        packageMedia.MEDIA_PATH = model.MEDIA_PATH;
                        model.FileName = model.MEDIA_PATH.FileName;
                        packageMedia.FileName = model.FileName;
                    }
                    response = new MediaCreator<PackageMedia>().CreateIt(packageMedia);
                    break;
                case MediaObjectType.T_COMPANY_MEDIA:
                    break;

                case MediaObjectType.T_PROVIDER_MEDIA:
                    MediaDto providerMedia = new MediaDto
                    {
                        Name = model.MediaName,
                        BaseId = model.BaseId,
                        ObjectId = model.ObjectId,
                        MediaId = model.MediaId
                    };
                    if (model.MEDIA_PATH != null)
                    {
                        providerMedia.MEDIA_PATH = model.MEDIA_PATH;
                        model.FileName = model.MEDIA_PATH.FileName;
                        providerMedia.FileName = model.FileName;
                    }
                    response = new MediaCreator<ProviderMedia>().CreateIt(providerMedia);
                    break;
                case MediaObjectType.T_SUBPRODUCT_MEDIA:
                    MediaDto subProductMedia = new MediaDto
                    {
                        Name = model.MediaName,
                        BaseId = model.BaseId,
                        ObjectId = model.ObjectId,
                        MediaId = model.MediaId
                        //MediaObject = new ProductMedia()
                    };
                    if (model.MEDIA_PATH != null)
                    {
                        subProductMedia.MEDIA_PATH = model.MEDIA_PATH;
                        model.FileName = model.MEDIA_PATH.FileName;
                        subProductMedia.FileName = model.FileName;
                    }
                    response = new MediaCreator<SubproductMedia>().CreateIt(subProductMedia);
                    break;
                case MediaObjectType.T_CLAIM_MEDIA:
                    MediaDto claimMedia = new MediaDto
                    {
                        Name = model.MediaName,
                        BaseId = model.BaseId,
                        ObjectId = model.ObjectId,
                        MediaId = model.MediaId
                    };
                    if (model.MEDIA_PATH != null)
                    {
                        claimMedia.MEDIA_PATH = model.MEDIA_PATH;
                        model.FileName = model.MEDIA_PATH.FileName;
                        claimMedia.FileName = model.FileName;
                    }
                    response = new MediaCreator<ClaimMedia>().CreateIt(claimMedia);

                    break;
                default: break;
            }


            return new JsonResult()
            {
                Data = new { Message = response.Message, IsSuccess = response.IsSuccess },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
            };
        }
        public ActionResult Index(ImportVM vm)
        {
            return View();
        }
        public ActionResult ImportProgress(Guid id)
        {
            return new JsonResult()
            {
                Data = new
                {
                    progress = tasks.Keys.Contains(id) ? tasks[id] : 100,
                    ResultMessage = response.Message,
                    Success = response.IsSuccess,
                    errorList = ImportErrorList.Keys.Contains(id) ? ImportErrorList[id] : "",
                    formId = FormId
                }
            };
            //return Json(tasks.Keys.Contains(id) ? tasks[id] : 100);
        }
        public ActionResult Import_FileUpload(ImportVM vm)
        {
            ImportErrorList.Clear();
            #region Validation
            FormId = vm.FormId;
            ObjectType = vm.ImportObjectType;
            if (vm.file == null)
            {
                return new JsonResult()
                {
                    Data = new { Message = "Dosya Bulunamadı!", IsSuccess = false }
                };
            }
            else response.IsSuccess = true;
            if (vm.file.ContentLength < 1)
            {
                return new JsonResult()
                {
                    Data = new { Message = "Dosya Bulunamadı!", IsSuccess = false }
                };
            }
            else response.IsSuccess = true;
            if (vm.file.ContentLength > ImportRules.FileSizeLimit)
            {
                return new JsonResult()
                {
                    Data = new { Message = "Dosya boyutu " + ((ImportRules.FileSizeLimit / 1024) / 1024).ToString() + " MB'dan fazla olamaz", IsSuccess = false }
                };
            }
            else response.IsSuccess = true;

            string extension = Path.GetExtension(vm.file.FileName).ToLower().TrimStart('.');
            if (extension == "xlsx" || extension == "xls")
            {
            }
            else
            {
                return new JsonResult()
                {
                    Data = new { Message = "Sadece *XLSX* veya *XLS* uzantılı dosya yükleyebilirsiniz!", IsSuccess = false }
                };
            }
            if (vm.ShowCompanies)
            {
                if (string.IsNullOrEmpty(vm.SelectedCompany))
                {
                    return new JsonResult()
                    {
                        Data = new { Message = "Şirket seçmelisiniz!", IsSuccess = false }
                    };
                }
            }
            #endregion
            var taskId = Guid.NewGuid();
            tasks.Add(taskId, 0);
            ImportErrorList.Add(taskId, "");
            if (response.IsSuccess)
            {
                switch (vm.ImportObjectType)
                {
                    case ImportObjectType.Policy:
                        IImport import = new PolicyImport();
                        response = import.DoWorkAsync(vm.file.InputStream, (extension == "xlsx" ? ImportFileType.Xlsx : ImportFileType.Xls), ref tasks, taskId, ref ImportErrorList, long.Parse(vm.SelectedCompany));
                        break;

                    case ImportObjectType.ProviderDoctor:
                        IImportForSubContent importDoctor = new DoctorImport();
                        response = importDoctor.DoWork(vm.Id, vm.FormId, vm.file.InputStream, (extension == "xlsx" ? ImportFileType.Xlsx : ImportFileType.Xls), ref tasks, taskId, ref ImportErrorList);
                        break;
                    case ImportObjectType.Agency:
                        IImport importAgency = new AgencyImport();
                        response = importAgency.DoWorkAsync(vm.file.InputStream, (extension == "xlsx" ? ImportFileType.Xlsx : ImportFileType.Xls), ref tasks, taskId, ref ImportErrorList);
                        break;
                    case ImportObjectType.AgencyCompany:
                        IImport importAgencyCompany = new AgencyCompanyImport();
                        response = importAgencyCompany.DoWorkAsync(vm.file.InputStream, (extension == "xlsx" ? ImportFileType.Xlsx : ImportFileType.Xls), ref tasks, taskId, ref ImportErrorList,CompanyId:vm.SelectedCompanyId);
                        break;
                    case ImportObjectType.ClaimStatus:
                        IImport importClaimStatus = new ClaimStatusImport();
                        response = importClaimStatus.DoWorkAsync(vm.file.InputStream, (extension == "xlsx" ? ImportFileType.Xlsx : ImportFileType.Xls), ref tasks, taskId, ref ImportErrorList);
                        break;
                    case ImportObjectType.PayrollStatus:
                        IImport importPayrollStatus = new PayrollStatusImport();
                        response = importPayrollStatus.DoWorkAsync(vm.file.InputStream, (extension == "xlsx" ? ImportFileType.Xlsx : ImportFileType.Xls), ref tasks, taskId, ref ImportErrorList);
                        break;
                    default: break;
                }
            }
            return new JsonResult()
            {
                Data = new { taskId = taskId, Message = response.Message, IsSuccess = response.IsSuccess }
            };
        }
        //public ActionResult PrintForm(,FormObjectType objectType)
        //{
        //    IPrint<PrintProvisionRejectReq> print = new PrintProvisionReject();
        //    print.DoWork()
        //    PrintProvisionReject provisionReject = new PrintProvisionReject();
        //    provisionReject.DoWork(new PrintProvisionRejectReq { ClaimId = 421 });
        //}
    }
}