﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Protein.Common.Constants;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Common.Dto;
using static Protein.Common.Entities.ProteinEntities;
using Protein.Common.Extensions;
using Protein.Web.ActionFilter.LoginAttr;
using Protein.Common.Messaging;
using Protein.Common.Helpers;
using Protein.Web.ActionFilter.ClearAttr;
using Protein.Web.Models.ExportModel;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Enums.ProteinEnums;
using Protein.Web.Models.ContractModel;
using Protein.Business.Enums.Import;
using Protein.Web.Models.ProviderFormModel;
using Dapper;
using Protein.Data.ExternalServices.InsuranceCompanies;
using Protein.Data.ExternalServices.Common.Lib;

namespace Protein.Web.Controllers
{
    public class ProviderController : Controller
    {
        public JsonResult Providers()
        {
            var providerTypeList = LookupHelper.GetLookupData(LookupTypes.Provider);

            List<V_Provider> ProviderList = new GenericRepository<V_Provider>().FindBy(orderby: "PROVIDER_ID");

            List<dynamic> providerListListType = new List<dynamic>();


            foreach (var item in providerTypeList)
            {
                dynamic providerListType = new System.Dynamic.ExpandoObject();

                providerListType.Value = item.Value;
                var ProviderListType = ProviderList.Where(x => x.PROVIDER_TYPE == item.Key).ToList();
                providerListType.provider = ProviderListType;
                providerListListType.Add(providerListType);
            }

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = providerListListType.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }


        #region ProvierGroup
        // GET: Provider Group List
        [LoginControl]
        public ActionResult ProviderGroup()
        {
            #region Export 
            ExportVM vm = new ExportVM();
            vm.ViewName = "T_PROVIDER_GROUP";
            vm.SetExportColumns();
            vm.ExportType = ExportType.Excel;
            #endregion

            try
            {

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View(vm);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetProviderGroupList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];



            string whereConditition = "";
            if (!String.IsNullOrEmpty(form["providerGroupName"]))
            {
                whereConditition += $"NAME LIKE '%{form["providerGroupName"]}%'";
            }
            if (!String.IsNullOrEmpty(form["providerTaxNumber"]))
            {
                if (!string.IsNullOrEmpty(whereConditition))
                {
                    whereConditition += " AND ";
                }
                whereConditition += $"TAX_NUMBER = {form["providerTaxNumber"]} ";
            }
            var ProviderGroupList = new ProviderGroupRepository().FindByPaged(conditions: whereConditition,
                                                                         orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection}" : "ID",
                                                                         pageNumber: start,
                                                                         rowsPerPage: length);
            #region Export
            ExportVM vm = new ExportVM();
            vm.WhereCondition = whereConditition;
            vm.SetExportColumns();
            #endregion
            //ViewBag.ProviderGroupList = ProviderGroupList.Data;
            var json = Json(new { data = ProviderGroupList.Data, draw = Request["draw"], recordsTotal = ProviderGroupList.TotalItemsCount, recordsFiltered = ProviderGroupList.TotalItemsCount, whereConditition = vm.WhereCondition }, JsonRequestBehavior.AllowGet);
            return json;
        }

        [LoginControl]
        public ActionResult ProviderGroupDelete(Int64 Id)
        {
            try
            {
                var providerCheck = new ProviderRepository().FindBy("PROVIDER_GROUP_ID = :providerGroupId", parameters: new { providerGroupId = Id });
                if (!providerCheck.Any())
                {
                    var repo = new ProviderGroupRepository();
                    var result = repo.FindById(Id);
                    result.Status = "1";
                    var spResponseProviderGroup = repo.Update(result);
                    if (spResponseProviderGroup.Code == "100")
                    {
                        TempData["Alert"] = $"swAlert('Silme İşlemi Başarılı','{result.Name} başarıyla silindi.','success')";
                    }
                    else
                    {
                        throw new Exception(spResponseProviderGroup.Code + " : " + spResponseProviderGroup.Message);
                    }
                }
                else
                {
                    TempData["Alert"] = $"swAlert('Bilgi','Seçilen Kurum Grubuna ait Aktif Kurum Bulunduğundan Silme İşlemi Gerçekleştirilemiyor.','info')";
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("ProviderGroup");
        }

        // GET: Provider Group Form
        [LoginControl]
        public ActionResult ProviderGroupForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Kurum Grubu Güncelle";
                        if (id != 0)
                        {
                            var providerGroup = new ProviderGroupRepository().FindById(id);
                            ViewBag.ProviderGroup = providerGroup ?? null;
                            if (ViewBag.ProviderGroup == null)
                            {
                                TempData["Alert"] = $"swAlert('Hata','Seçilen Kurum Grubu Bulunamadı.','warning')";
                                return RedirectToAction("ProviderGroup");
                            }
                        }
                        break;

                    default:
                        ViewBag.Title = "Kurum Grubu Ekle";
                        break;
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult ProviderGroupFormSave(FormCollection form)
        {
            try
            {
                var providerGroupId = form["hdProviderGroupId"];
                var name = form["providerGroupName"];
                var taxNumber = form["providerGroupTaxNumber"];

                var providerGroup = new ProviderGroup
                {
                    Id = long.Parse(providerGroupId),
                    Name = name,
                    TaxNumber = taxNumber.IsInt64() ? (long?)Convert.ToInt64(taxNumber) : null
                };

                var spResponseProviderGroup = new ProviderGroupRepository().Insert(providerGroup);
                if (spResponseProviderGroup.Code == "100")
                {
                    var resultText = providerGroupId != "0" ? "güncellendi" : "kaydedildi";
                    TempData["Alert"] = $"swAlert('İşlemi Başarılı','{providerGroup.Name} başarıyla {resultText}.','success')";
                }
                else
                {
                    throw new Exception(spResponseProviderGroup.Code + " : " + spResponseProviderGroup.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("ProviderGroup");
        }
        #endregion

        #region Provider
        // GET: Provider List
        [ClearTempData(TempDataKey = "Alert")]
        [LoginControl]
        public ActionResult Provider()
        {
            ExportVM vm = new ExportVM();
            try
            {
                #region Export 

                vm.ViewName = Constants.Views.ExportProvider;
                vm.SetExportColumns();
                vm.ExportType = ExportType.Excel;
                #endregion
                ////Filter values
                //ViewBag.providerGroup = string.Empty;
                //ViewBag.providerCode = string.Empty;
                //ViewBag.providerType = string.Empty;
                //ViewBag.providerTaxNumber = string.Empty;
                //ViewBag.providerCity = string.Empty;
                //ViewBag.providerCounty = string.Empty;
                //ViewBag.providerName = string.Empty;
                //ViewBag.providerTitle = string.Empty;
                //ViewBag.revisionDate = string.Empty;
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;
                ViewBag.CountyList = new List<County>();

                var AgreementType = LookupHelper.GetLookupData(Constants.LookupTypes.Agreement, showAll: true);
                ViewBag.AgreementTypeList = AgreementType;

                var ProviderType = LookupHelper.GetLookupData(Constants.LookupTypes.Provider, showAll: true);
                ViewBag.ProviderTypeList = ProviderType;

                var ContractType = LookupHelper.GetLookupData(Constants.LookupTypes.Contract, showAll: true);
                ViewBag.ContractTypeList = ContractType;

                var CityList = new CityRepository().FindBy();
                ViewBag.CityList = CityList;

                var ProviderGroupList = new ProviderGroupRepository().FindBy();
                ViewBag.ProviderGroupList = ProviderGroupList;


                //var result = ViewHelper.GetView(Views.Provider, orderby: "PROVIDER_ID");
                //ViewBag.ProviderList = result.Data;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View(vm);
        }

        //// GET: Provider Group Filter
        //[HttpPost]
        //[LoginControl]
        //public ActionResult ProviderFilter(FormCollection form)
        //{
        //    int start = Convert.ToInt32(Request["start"]);
        //    int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
        //    string searchValue = Request["search[value]"];
        //    string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
        //    string sortDirection = Request["order[0][dir]"];

        //    ExportVM vm = new ExportVM();
        //    vm.ViewName = Constants.Views.ExportProvider;
        //    try
        //    {
        //        //Filter values
        //        ViewBag.providerGroup = form["providerGroup"];
        //        ViewBag.providerCode = form["providerCode"];
        //        ViewBag.providerType = form["providerType"];
        //        ViewBag.providerTaxNumber = form["providerTaxNumber"];
        //        ViewBag.providerCity = form["providerCity"];
        //        ViewBag.providerCounty = form["providerCounty"];
        //        ViewBag.providerName = form["providerName"];
        //        ViewBag.providerTitle = form["providerTitle"];
        //        TempData["Alert"] = TempData["Alert"] ?? string.Empty;
        //        var contractType = form["providerContractType"];
        //        ViewBag.CountyList = new List<County>();

        //        if (!String.IsNullOrEmpty(form["providerCity"]))
        //        {
        //            ViewBag.CountyList = new CountyRepository().FindBy("TYPE=1 AND CITY_ID=" + long.Parse(form["providerCity"]));
        //        }

        //        string whereConditition = !String.IsNullOrEmpty(form["providerGroup"]) ? $" PROVIDER_GROUP_ID={long.Parse(form["providerGroup"])} AND" : "";
        //        whereConditition += !String.IsNullOrEmpty(form["providerCode"]) ? $" PROVIDER_ID={long.Parse(form["providerCode"])} AND" : "";
        //        whereConditition += !String.IsNullOrEmpty(form["providerType"]) ? $" PROVIDER_TYPE='{form["providerType"]}' AND" : "";
        //        whereConditition += !String.IsNullOrEmpty(form["providerTaxNumber"]) ? $" TAX_NUMBER LIKE '%{form["providerTaxNumber"]}%' AND" : "";
        //        whereConditition += !String.IsNullOrEmpty(form["providerCity"]) ? $" CITY_ID={long.Parse(form["providerCity"])} AND" : "";
        //        whereConditition += !String.IsNullOrEmpty(form["providerCounty"]) ? $" COUNTY_ID={long.Parse(form["providerCounty"])} AND" : "";
        //        whereConditition += !String.IsNullOrEmpty(form["providerName"]) ? $" PROVIDER_NAME LIKE '%{form["providerName"]}%' AND" : "";
        //        whereConditition += !String.IsNullOrEmpty(form["providerTitle"]) ? $" PROVIDER_TITLE LIKE '%{form["providerTitle"]}%' AND" : "";
        //        if (!string.IsNullOrEmpty(contractType))
        //        {
        //            whereConditition += " (   ";
        //            foreach (var item in contractType.Split(','))
        //            {
        //                switch (item)
        //                {
        //                    case "0":
        //                        whereConditition += " TSS_CONTRACT_ID IS NOT NULL OR";
        //                        break;
        //                    case "1":
        //                        whereConditition += " OSS_CONTRACT_ID IS NOT NULL OR";
        //                        break;
        //                    case "2":
        //                        whereConditition += " DOGUM_CONTRACT_ID IS NOT NULL OR";
        //                        break;
        //                    default:
        //                        whereConditition += " MDP_CONTRACT_ID IS NOT NULL OR";
        //                        break;
        //                }
        //            }
        //            whereConditition = whereConditition.Substring(0, whereConditition.Length - 3) + ")   ";
        //        }

        //        #region Export

        //        vm.WhereCondition = string.IsNullOrEmpty(whereConditition) ? string.Empty : whereConditition.Substring(0, whereConditition.Length - 3);
        //        vm.SetExportColumns();
        //        #endregion

        //        var result = ViewHelper.GetView(Views.Provider,
        //                                        whereCondititon: string.IsNullOrEmpty(whereConditition) ? string.Empty : whereConditition.Substring(0, whereConditition.Length - 3),
        //                                        orderby: "PROVIDER_ID");
        //        ViewBag.ProviderList = result.Data;

        //        var AgreementType = LookupHelper.GetLookupData(Constants.LookupTypes.Agreement, showAll: true);
        //        ViewBag.AgreementTypeList = AgreementType;

        //        var ProviderType = LookupHelper.GetLookupData(Constants.LookupTypes.Provider, showAll: true);
        //        ViewBag.ProviderTypeList = ProviderType;

        //        var ContractType = LookupHelper.GetLookupData(Constants.LookupTypes.Contract, showAll: true);
        //        ViewBag.ContractTypeList = ContractType;

        //        var CityList = new CityRepository().FindBy();
        //        ViewBag.CityList = CityList;

        //        var ProviderGroupList = new ProviderGroupRepository().FindBy();
        //        ViewBag.ProviderGroupList = ProviderGroupList;
        //    }
        //    catch (Exception ex)
        //    {
        //        TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
        //    }
        //    return View("Provider", vm);
        //}

        [HttpPost]
        [LoginControl]
        public ActionResult GetProviderList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];
            string message = "1";
            ViewResultDto<List<V_Provider>> result = new ViewResultDto<List<V_Provider>>();
            result.Data = new List<V_Provider>();
            ExportVM vm = new ExportVM();

            if (isFilter == "1")
            {
                string whereConditition = !String.IsNullOrEmpty(form["providerGroup"]) ? $" PROVIDER_GROUP_ID={long.Parse(form["providerGroup"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["providerCode"]) ? $" PROVIDER_ID={long.Parse(form["providerCode"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["providerType"]) ? $" PROVIDER_TYPE='{form["providerType"]}' AND" : "";
                whereConditition += form["providerTaxNumber"].IsInt64() ? $" TAX_NUMBER = {form["providerTaxNumber"]} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["providerCity"]) ? $" CITY_ID={long.Parse(form["providerCity"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["providerCounty"]) ? $" COUNTY_ID={long.Parse(form["providerCounty"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["providerName"]) ? $" PROVIDER_NAME LIKE '%{form["providerName"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["providerTitle"]) ? $" PROVIDER_TITLE LIKE '%{form["providerTitle"]}%' AND" : "";

                var contractType = form["providerContractType"];
                if (!string.IsNullOrEmpty(contractType))
                {
                    whereConditition += " (   ";
                    foreach (var item in contractType.Split(','))
                    {
                        switch (item)
                        {
                            case "0":
                                whereConditition += " TSS_CONTRACT_ID IS NOT NULL OR";
                                break;
                            case "1":
                                whereConditition += " OSS_CONTRACT_ID IS NOT NULL OR";
                                break;
                            case "2":
                                whereConditition += " DOGUM_CONTRACT_ID IS NOT NULL OR";
                                break;
                            default:
                                whereConditition += " MDP_CONTRACT_ID IS NOT NULL OR";
                                break;
                        }
                    }
                    whereConditition = whereConditition.Substring(0, whereConditition.Length - 3) + ")   ";
                }

                vm.ViewName = Constants.Views.ExportProvider;
                vm.SetExportColumns();
                if (whereConditition != "")
                {
                    result = new GenericRepository<V_Provider>().FindByPaged(
                                                        conditions: string.IsNullOrEmpty(whereConditition) ? string.Empty : whereConditition.Substring(0, whereConditition.Length - 3),
                                                        orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},PROVIDER_ID" : "PROVIDER_ID",
                                                        pageNumber: start,
                                                        rowsPerPage: length);
                    vm.WhereCondition = string.IsNullOrEmpty(whereConditition) ? string.Empty : whereConditition.Substring(0, whereConditition.Length - 3);
                }
                else
                {
                    message = "Lütfen Filtre alanlarını doldurup filtrelemeyi deneyiniz.";
                }
                
                //var result =  ViewHelper.GetView(Views.Provider,
                //                                whereCondititon: string.IsNullOrEmpty(whereConditition) ? string.Empty : whereConditition.Substring(0, whereConditition.Length - 3),
                //                                orderby: "PROVIDER_ID");
            }
            return Json(new { data = result.Data, draw = Request["draw"], recordsTotal = result.TotalItemsCount , recordsFiltered = result.TotalItemsCount, whereConditition = vm.WhereCondition , message=message , }, JsonRequestBehavior.AllowGet);
        }

        [LoginControl]
        public ActionResult ProviderDelete(Int64 Id)
        {
            try
            {
                var repo = new ProviderRepository();
                var result = repo.FindById(Id);
                result.Status = "1";
                var spResponseProvider = repo.Update(result);
                if (spResponseProvider.Code == "100")
                {
                    TempData["Alert"] = $"swAlert('Silme İşlemi Başarılı','{result.OfficialCode} Kodlu Firma Başarıyla Silindi.','success')";
                }
                else
                {
                    throw new Exception(spResponseProvider.Code + " : " + spResponseProvider.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Provider");
        }

        // GET: Provider Form
        [LoginControl]
        public ActionResult ProviderForm(Int64 id = 0, string formaction = "")
        {
            ProviderFormVM vm = new ProviderFormVM();

            vm.ImportVM.Id = id;
            vm.ImportVM.ImportObjectType = ImportObjectType.ProviderDoctor;
            vm.ImportVM.FormId = "form2";


            vm.MediaVM.MediaObjectType = Business.Enums.Media.MediaObjectType.T_PROVIDER_MEDIA;
            vm.MediaVM.ObjectId = id;
            vm.MediaVM.TabName = "tab6";

            //vm.MediaVM.
            try
            {
                #region Fill dropdowns
                ViewBag.isEdit = false;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var ProviderType = LookupHelper.GetLookupData(Constants.LookupTypes.Provider);
                ViewBag.ProviderTypeList = ProviderType;

                var StaffType = LookupHelper.GetLookupData(Constants.LookupTypes.Staff);
                ViewBag.StaffTypeList = StaffType;

                var ProviderNoteTypes = LookupHelper.GetLookupData(Constants.LookupTypes.ProviderNote);
                ViewBag.ProviderNoteTypeList = ProviderNoteTypes;

                var ProviderGroupList = new ProviderGroupRepository().FindBy(orderby: "NAME ASC");
                ViewBag.ProviderGroupList = ProviderGroupList;

                var CorporateType = LookupHelper.GetLookupData(Constants.LookupTypes.Corporate);
                ViewBag.CorporateTypeList = CorporateType;

                var CurrencyTypes = LookupHelper.GetLookupData(Constants.LookupTypes.Currency);
                ViewBag.CurrencyTypes = CurrencyTypes;

                var CityList = new CityRepository().FindBy();
                ViewBag.CityList = CityList;

                var CountyList = new CountyRepository().FindBy();
                ViewBag.CountyList = CountyList;

                var BankList = new BankRepository().FindBy();
                ViewBag.BankList = BankList;

                var DoctorBranchList = new DoctorBranchRepository().FindBy();
                ViewBag.DoctorBranchList = DoctorBranchList;

                var CadreList = new CadreRepository().FindBy();
                ViewBag.CadreList = CadreList;

                var CountryList = new CountryRepository().FindBy();
                ViewBag.CountryList = CountryList;

                #endregion

                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Kurum Güncelle";

                        #region Fill form

                        if (id != 0)
                        {
                            //Genel Bilgiler

                            var result = new GenericRepository<V_Provider>().FindBy("PROVIDER_ID=:id", orderby: "PROVIDER_ID", parameters: new { id = id }).FirstOrDefault();

                            if (result != null)
                            {
                                ViewBag.Provider = result == null ? null : result;

                                //Doktor bilgileri
                                var doctors = new GenericRepository<V_Staff>().FindBy("PROVIDER_ID=:id AND STAFF_TYPE=0", orderby: "PROVIDER_ID", parameters: new { id = id });

                                if (doctors != null)
                                {
                                    List<dynamic> doctorList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in doctors)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.DOCTOR_CONTACT_ID = Convert.ToString(item.CONTACT_ID);
                                        listItem.DOCTOR_PERSON_ID = Convert.ToString(item.PERSON_ID);
                                        listItem.DOCTOR_STAFF_ID = Convert.ToString(item.STAFF_ID);

                                        listItem.DOCTOR_BRANCH_ID = Convert.ToString(item.DOCTOR_BRANCH_ID);
                                        listItem.DOCTOR_BRANCH_IDSelectedText = item.DOCTOR_BRANCH_NAME;
                                        listItem.STAFF_TITLE = item.STAFF_TITLE;
                                        listItem.DIPLOMA_NO = item.DIPLOMA_NO;
                                        listItem.REGISTER_NO = item.REGISTER_NO;
                                        listItem.FIRST_NAME = item.FIRST_NAME;
                                        listItem.LAST_NAME = item.LAST_NAME;
                                        listItem.isOpen = "0";

                                        doctorList.Add(listItem);

                                        i++;
                                    }

                                    ViewBag.Doctors = doctorList.ToJSON();
                                }

                                //Yetkili Bilgileri
                                var contacts = new GenericRepository<V_Staff>().FindBy("PROVIDER_ID=:id AND STAFF_TYPE=1", orderby: "PROVIDER_ID", parameters: new { id = id });

                                if (contacts != null)
                                {
                                    List<dynamic> contactList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in contacts)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.AUTHORIZED_CONTACT_ID = string.IsNullOrEmpty(Convert.ToString(item.CONTACT_ID)) ? "0" : Convert.ToString(item.CONTACT_ID);
                                        listItem.AUTHORIZED_PERSON_ID = string.IsNullOrEmpty(Convert.ToString(item.PERSON_ID)) ? "0" : Convert.ToString(item.PERSON_ID);
                                        listItem.AUTHORIZED_PHONE_ID = string.IsNullOrEmpty(Convert.ToString(item.PHONE_ID)) ? "0" : Convert.ToString(item.PHONE_ID);
                                        listItem.AUTHORIZED_MOBILE_PHONE_ID = string.IsNullOrEmpty(Convert.ToString(item.MOBILE_PHONE_ID)) ? "0" : Convert.ToString(item.MOBILE_PHONE_ID);
                                        listItem.AUTHORIZED_EMAIL_ID = string.IsNullOrEmpty(Convert.ToString(item.EMAIL_ID)) ? "0" : Convert.ToString(item.EMAIL_ID);
                                        listItem.AUTHORIZED_STAFF_ID = string.IsNullOrEmpty(Convert.ToString(item.STAFF_ID)) ? "0" : Convert.ToString(item.STAFF_ID);

                                        listItem.AUTHORIZED_TITLE = item.STAFF_TITLE.RemoveRepeatedWhiteSpace();
                                        listItem.AUTHORIZED_NAME = item.FIRST_NAME.RemoveRepeatedWhiteSpace();
                                        listItem.AUTHORIZED_SURNAME = item.LAST_NAME.RemoveRepeatedWhiteSpace();
                                        listItem.AUTHORIZED_PHONE_NO = item.PHONE_NO;
                                        listItem.AUTHORIZED_EXTENSION = item.EXTENSION;
                                        listItem.AUTHORIZED_MOBILE_PHONE_NO = item.MOBILE_PHONE_NO;
                                        listItem.AUTHORIZED_EMAIL = item.EMAIL_ADDRESS;
                                        listItem.AUTHORIZED_IS_GROUP_ADMIN = Convert.ToString(item.IS_GROUP_ADMIN);
                                        listItem.isOpen = "0";

                                        contactList.Add(listItem);

                                        i++;
                                    }

                                    ViewBag.Contacts = contactList.ToJSON();
                                }

                                //user bilgileri
                                var users = new GenericRepository<V_ProviderUser>().FindBy("PROVIDER_ID=:id", orderby: "PROVIDER_ID", parameters: new { id = id });
                                if (users != null)
                                {
                                    List<dynamic> userList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in users)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();
                                        listItem.EMAIL = Convert.ToString(item.EMAIL);
                                        listItem.PROVIDER_USER_ID = Convert.ToString(item.PROVIDER_USER_ID);
                                        listItem.USER_ID = Convert.ToString(item.USER_ID);
                                        listItem.ClientId = i;
                                        userList.Add(listItem);
                                        i++;
                                    }
                                    ViewBag.ProviderUser = userList.FirstOrDefault();
                                }

                                //Banka bilgileri
                                var banks = new GenericRepository<V_ProviderBank>().FindBy("PROVIDER_ID=:id", orderby: "PROVIDER_ID", parameters: new { id = id });
                                if (banks != null)
                                {

                                    List<dynamic> bankList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in banks)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.BANK_ACCOUNT_ID = Convert.ToString(item.BANK_ACCOUNT_ID);
                                        listItem.PROVIDER_BANK_ACCOUNT_ID = Convert.ToString(item.PROVIDER_BANK_ACCOUNT_ID);

                                        listItem.BANK_ACCOUNT_NAME = item.BANK_ACCOUNT_NAME;
                                        listItem.BANK_ID = Convert.ToString(item.BANK_ID);
                                        listItem.BANK_IDSelectedText = item.BANK_NAME;

                                        listItem.BANK_BRANCH_ID = Convert.ToString(item.BANK_BRANCH_ID);
                                        listItem.BANK_BRANCH_IDSelectedText = item.BANK_BRANCH_NAME;
                                        listItem.BANK_BRANCH_CODE = item.BANK_BRANCH_CODE;
                                        listItem.BANK_ACCOUNT_NO = item.BANK_ACCOUNT_NO;
                                        listItem.IBAN = item.IBAN;
                                        listItem.CURRENCY_TYPE = item.CURRENCY_TYPE;
                                        listItem.CURRENCY_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Currency, item.CURRENCY_TYPE);
                                        listItem.IS_PRIMARY_BANK = item.IS_PRIMARY;
                                        listItem.isOpen = "0";

                                        bankList.Add(listItem);

                                        i++;
                                    }

                                    ViewBag.Banks = bankList.ToJSON();
                                }

                                //Notlar
                                var notes = new GenericRepository<V_ProviderNote>().FindBy("PROVIDER_ID=:id", orderby: "PROVIDER_ID", parameters: new { id = id });
                                if (notes != null)
                                {
                                    List<dynamic> notesList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in notes)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                                        listItem.PROVIDER_NOTE_ID = Convert.ToString(item.PROVIDER_NOTE_ID);

                                        listItem.NOTE_TYPE = item.NOTE_TYPE;
                                        listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.ProviderNote, item.NOTE_TYPE);
                                        listItem.NOTE_DESCRIPTION = item.NOTE_DESCRIPTION;
                                        listItem.isOpen = "0";
                                        notesList.Add(listItem);

                                        i++;
                                    }

                                    ViewBag.Notes = notesList.ToJSON();
                                }


                                ViewBag.isEdit = true;
                            }
                            else
                            {
                                TempData["Alert"] = $"swAlert('Kayıt Bulunamadı','Seçilen Kuruma ait Kayıt Bulunamadı!','info')";
                                RedirectToAction("Provider");
                            }
                        }
                        else
                        {
                            RedirectToAction("Provider");
                        }

                        #endregion

                        break;

                    default:
                        ViewBag.isEdit = false;
                        ViewBag.Title = "Kurum Ekle";
                        break;
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View(vm);
        }
        [HttpGet]
        [LoginControl]
        public JsonResult RefreshPasword(Int64 providerId = 0)
        {
            var result = new AjaxResultDto<JSONResult>();

            var json = "";
            try
            {
                V_Provider provider = new GenericRepository<V_Provider>().FindBy($"PROVIDER_ID ={providerId}", orderby: "").FirstOrDefault();
                if (provider == null)
                {
                    result.ResultCode = "999";
                    result.ResultMessage = "Kurum Koduna Ait Kurum Bilgisi Bulunamadı. Kurum Kodunu Kontrol Ediniz.";

                    return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
                }

                V_Staff v_Staff = new GenericRepository<V_Staff>().FindBy($"PROVIDER_ID={provider.PROVIDER_ID} AND STAFF_TYPE='{((int)StaffType.YETKILI).ToString()}' AND EMAIL_ADDRESS IS NOT NULL AND IS_GROUP_ADMIN='1'", orderby: "").FirstOrDefault();
                if (v_Staff == null)
                {
                    result.ResultCode = "999";
                    result.ResultMessage = "Kurum Yetkili Bilgisi Bulunamadı.";

                    return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
                }

                string userName = "";
                string clearPassword = UserHelper.GeneratePassword(provider.PROVIDER_NAME, providerId.ToString());

                V_ProviderUser v_ProviderUser = new GenericRepository<V_ProviderUser>().FindBy($"PROVIDER_ID={provider.PROVIDER_ID}", orderby: "").FirstOrDefault();
                if (v_ProviderUser == null)
                {
                    var isCreate = true;
                    userName = UserHelper.GenerateUserName(LookupHelper.GetLookupTextByOrdinal(LookupTypes.Provider, provider.PROVIDER_TYPE), providerId.ToString());
                    var user = new User
                    {
                        Username = userName,
                        Password = CryptoHelper.GetMD5Hash(clearPassword),
                        Email = v_Staff.EMAIL_ADDRESS,
                        IsRememberMe = "1"
                    };
                    var spResponseUser = new UserRepository().Insert(user);
                    if (spResponseUser.Code == "100")
                    {
                        var userId = spResponseUser.PkId;
                        var providerUser = new ProviderUser
                        {
                            ProviderId = providerId,
                            UserId = userId
                        };
                        var spResponseProviderUser = new ProviderUserRepository().Insert(providerUser);
                        if (spResponseProviderUser.Code != "100")
                            isCreate = false;
                    }
                    else
                        isCreate = false;

                    if (!isCreate)
                    {
                        result.ResultCode = "999";
                        result.ResultMessage = "Kurum Giriş Bilgileri Bulunamadı. Lütfen Tekrar Deneyiniz...";

                        return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    userName = v_ProviderUser.USERNAME;

                    User user = new User
                    {
                        Id = (long)v_ProviderUser.USER_ID,
                        Email = v_Staff.EMAIL_ADDRESS,
                        Username = v_ProviderUser.USERNAME,
                        Password = CryptoHelper.GetMD5Hash(clearPassword)
                    };
                    var spResponseUser = new UserRepository().Insert(user);
                }

                MailSender msender = new MailSender();
                msender.SendMessage(
                    new EmailArgs
                    {
                        TO = v_Staff.EMAIL_ADDRESS,
                        IsHtml = true,
                        Subject = "İmece Destek Danışmanlık Hizmetleri A.Ş. E-Protein Kullanıcı Adı ve Şifre Bildirimi",
                        Content = $"<b>Kurum Adı:</b> {provider.PROVIDER_NAME} " +
                        $"<br><br>Sayın Yetkili," +
                        $"<br><br>Siz değerli anlaşmalı kurumlarımızın online sistem üzerinden Halk Sigorta, Türk Nippon Sigorta ve Doğa Sigorta Şirketlerine ait provizyon, icmal, ödeme takibi işlemlerini yapabilmenize olanak sağlayan E-Protein uygulamasına giriş yapabilmeniz için oluşturulan kullanıcı adı ve şifre aşağıda belirtilmiştir." +
                        $"<br><br>E-Protein uygulamasına; http://protein.imecedestek.com:4041 adresinden erişerek kullanıcı adı / şifreniz ile sisteme giriş yaptıktan sonra E-Protein kullanım kılavuzuna ulaşabilirsiniz." +
                        $"<br><br><br><b>Kurum Kodu:</b> {provider.PROVIDER_ID}" +
                        $"<br><b>Kullanıcı Adı:</b> {userName}" +
                        $"<br><b>Şifre:</b> {clearPassword}" +
                        $"<br><br>Kullanıcı bilgilerinizi unutmanız durumunda giriş ekranında bulunan “Şifremi Unuttum” sekmesini kullanarak sistemimizde kayıtlı olan e-mail adresinize yeni şifre gönderimi yapılmaktadır ." +
                        $"<br><br>İşlemlerinizi Online sistem üzerinden yaptığınız için teşekkür eder, iş birliğimizin artarak devam etmesini dileriz."
                    });

                result.ResultCode = "100";
                result.ResultMessage = "Kullanıcı Adı ve Şifreniz Yetkili Mail Adresine Gönderildi.";

                return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }
            return Json(json.ToJSON(), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [LoginControl]
        public JsonResult ProviderSaveStep1(FormCollection form)
        {
            var result = new AjaxResultDto<ProviderSaveStep1Result>();
            ProviderSaveStep1Result step1Result = new ProviderSaveStep1Result();

            try
            {
                var providerTitle = form["providerTitle"];
                var taxOffice = form["taxOffice"];
                var taxNumber = form["taxNumber"];
                Int64? identityNo = form["identityNo"].IsInt64() ? (long?)Convert.ToInt64(form["identityNo"]) : null;

                long providerId = long.Parse(form["hdProviderId"]);
                long contactId = long.Parse(form["hdContactId"]);
                long addressId = long.Parse(form["hdAddressId"]);
                long corporateId = long.Parse(form["hdCorporateId"]);

                long userId = long.Parse(form["hdUserId"]);
                long providerUserId = long.Parse(form["hdProviderUserId"]);
                var providerTypeName = form["providerTypeName"];

                long phoneId = form["hdPhoneId"].IsInt64() ? long.Parse(form["hdPhoneId"]) : 0;
                long providerPhoneId = form["hdProviderPhoneId"].IsInt64() ? long.Parse(form["hdProviderPhoneId"]) : 0; ;
                var phoneNo = form["phoneNo"];

                //Contact Info
                var contact = new Contact
                {
                    Id = contactId,
                    Type = "1",
                    Title = providerTitle,
                    TaxNumber = taxNumber.IsInt64() ? (long?)Convert.ToInt64(taxNumber) : null,
                    TaxOffice = taxOffice,
                    IdentityNo = identityNo
                };
                var providerContactResult = new ContactRepository().Insert(contact);
                if (providerContactResult.Code == "100")
                {
                    step1Result.providerContactId = providerContactResult.PkId;
                    var providerName = form["providerName"];

                    var corporate = new Corporate
                    {
                        Id = corporateId,
                        ContactId = providerContactResult.PkId,
                        Type = "0",
                        Name = providerName,
                    };
                    var spResponseCorporate = new CorporateRepository().Insert(corporate);
                    if (spResponseCorporate.Code == "100")
                    {
                        step1Result.corporateId = spResponseCorporate.PkId;
                        //Address Info
                        var providerAdress = form["providerAdress"];
                        if (!String.IsNullOrEmpty(providerAdress))
                        {
                            var city = form["city"];
                            var county = form["county"];
                            var district = form["district"];
                            var adress = new Address
                            {
                                Id = addressId,
                                Type = "0",
                                CityId = !string.IsNullOrEmpty(city) ? (long?)long.Parse(city) : null,
                                CountyId = !string.IsNullOrEmpty(county) ? (long?)long.Parse(county) : null,
                                Details = providerAdress,
                                District = district
                            };

                            var providerAdressResult = new AddressRepository().Insert(adress);
                            if (providerAdressResult.Code != "100")
                            {
                                throw new Exception(providerAdressResult.Message);
                            }
                            step1Result.providerAdressId = providerAdressResult.PkId;
                        }


                        var providerType = form["providerType"];
                        var providerCode = form["providerCode"];
                        var stoppageValue = form["stoppageValue"];
                        var providerGroupId = form["providerGroup"];
                        var revisionDate = form["revisionDate"];
                        var isEBill = form["IS_E_BILL"] == null ? "0" : "1";
                        var IS_VAT_EXEMPTION = form["IS_VAT_EXEMPTION"] == null ? "0" : "1";
                        //Provider Info
                        var provider = new Provider
                        {
                            Id = providerId,
                            Type = string.IsNullOrEmpty(providerType) ? "0" : providerType,
                            OfficialCode = providerCode,
                            StoppageOverwrite = !string.IsNullOrEmpty(stoppageValue) ? (int?)int.Parse(stoppageValue) : null,
                            ContactId = step1Result.providerContactId,
                            RevisionDate = DateTime.Parse(revisionDate),
                            IsEBill = isEBill,
                            IS_VAT_EXEMPTION = IS_VAT_EXEMPTION,
                            AddressId = step1Result.providerAdressId > 0 ? (long?)step1Result.providerAdressId : null
                        };
                        if (!string.IsNullOrEmpty(providerGroupId))
                        {
                            provider.ProviderGroupId = (long?)long.Parse(providerGroupId);
                        }
                        if (step1Result.providerAdressId != 0)
                        {
                            provider.AddressId = step1Result.providerAdressId;
                        }
                        var spResponseProvider = new ProviderRepository().Insert(provider);

                        if (spResponseProvider.Code == "100")
                        {
                            providerId = spResponseProvider.PkId;

                            if (!string.IsNullOrEmpty(phoneNo))
                            {
                                //Phone  
                                var phone = new Phone
                                {
                                    Id = phoneId,
                                    Type = ((int)PhoneType.IS).ToString(),
                                    CountryId = 222,
                                    No = phoneNo
                                };
                                SpResponse spResponsePhone = new PhoneRepository().Insert(phone);
                                if (spResponsePhone.Code == "100")
                                {
                                    phoneId = spResponsePhone.PkId;
                                    step1Result.phoneId = phoneId;

                                    var providerPhone = new ProviderPhone
                                    {
                                        Id = providerPhoneId,
                                        PhoneId = phoneId,
                                        ProviderId = providerId
                                    };
                                    SpResponse spResponseProviderPhone = new ProviderPhoneRepository().Insert(providerPhone);
                                    if (spResponseProviderPhone.Code != "100")
                                    {
                                        throw new Exception(spResponseProviderPhone.Code + " : " + spResponseProviderPhone.Message);
                                    }
                                    providerPhoneId = spResponseProviderPhone.PkId;
                                    step1Result.providerPhoneId = providerPhoneId;
                                }
                                else
                                {
                                    throw new Exception(spResponsePhone.Code + " : " + spResponsePhone.Message);
                                }
                            }
                            //User
                            var clearPassword = UserHelper.GeneratePassword(providerName, providerId.ToString());
                            string userName = UserHelper.GenerateUserName(providerTypeName, providerId.ToString());
                            var user = new User
                            {
                                Id = userId,
                                Username = userName,
                                Password = CryptoHelper.GetMD5Hash(clearPassword),
                                Email = userName,
                                IsRememberMe = "0"
                            };
                            var spResponseUser = new UserRepository().Insert(user);
                            if (spResponseUser.Code == "100")
                            {
                                step1Result.UserId = spResponseUser.PkId;
                                userId = spResponseUser.PkId;
                                var providerUser = new ProviderUser
                                {
                                    Id = providerUserId,
                                    ProviderId = providerId,
                                    UserId = userId
                                };
                                var spResponseProviderUser = new ProviderUserRepository().Insert(providerUser);
                                if (spResponseProviderUser.Code != "100")
                                {
                                    throw new Exception(spResponseProviderUser.Code + " : " + spResponseProviderUser.Message);
                                }
                                step1Result.providerUserId = spResponseProviderUser.PkId;
                            }
                            else
                            {
                                throw new Exception(spResponseUser.Code + " : " + spResponseUser.Message);
                            }
                        }
                        else
                        {
                            throw new Exception(spResponseProvider.Message);
                        }

                        //Everything is OK
                        result.ResultCode = providerContactResult.Code;
                        result.ResultMessage = $"{providerId} Nolu Kurum başarıyla {(long.Parse(form["hdProviderId"]) > 0 ? "güncellendi" : "kaydedildi")}.";
                        step1Result.providerId = spResponseProvider.PkId;
                        result.Data = step1Result;
                        TempData["Alert"] = $"swAlert('İşlem Başarılı','{providerId} Nolu Kurum başarıyla kaydedildi.','success')";
                    }
                    else
                    {
                        throw new Exception(spResponseCorporate.Message);
                    }
                }
                else
                {
                    throw new Exception(providerContactResult.Message);
                }
                //    ProxyServiceClient proxyServiceClient = new ProxyServiceClient();
                //    ProviderTransferReq stateUpdateReq = new ProviderTransferReq
                //    {
                //        ProviderId = 235122,
                //        CompanyCode = 95
                //    };
                //    proxyServiceClient.ProviderTransfer(stateUpdateReq);

                //List<V_ProviderWebSource> providers = new GenericRepository<V_ProviderWebSource>().FindBy($"COMPANY_ID=10", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);
                //foreach (var item in providers)
                //{
                //    ProxyServiceClient proxyServiceClient = new ProxyServiceClient();
                //    ProviderTransferReq stateUpdateReq = new ProviderTransferReq
                //    {
                //        ProviderId = item.PROVIDER_ID,
                //        CompanyCode = 95
                //    };
                //    proxyServiceClient.ProviderTransfer(stateUpdateReq);
                //}

                var CompanyList = new GenericRepository<V_Company>().FindBy(orderby: "COMPANY_ID").ToList();

                ProxyServiceClient proxyServiceClient = new ProxyServiceClient();

                foreach (var companies in CompanyList)
                {
                    ProviderTransferReq stateUpdateReq = new ProviderTransferReq
                    {
                        ProviderId = step1Result.providerId,
                        CompanyCode = (long)companies.COMPANY_ID
                    };
                    var sonuc = proxyServiceClient.ProviderTransfer(stateUpdateReq);
                }



            }
            catch (Exception ex)
            {
                string log = ex.Message;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [LoginControl]
        public JsonResult ProviderSaveStep2(FormCollection form)
        {
            var result = new AjaxResultDto<ProviderSaveStep2Result>();
            var doctors = form["hdDoctors"];
            var providerId = "0"; providerId = form["hdProviderId"];
            try
            {
                List<dynamic> doctorList = new List<dynamic>();
                doctorList = JsonConvert.DeserializeObject<List<dynamic>>(doctors);

                foreach (var doctor in doctorList)
                {
                    if (doctor.isOpen == "1")
                    {
                        var contact = new Contact
                        {
                            Id = doctor.DOCTOR_CONTACT_ID,
                            Type = "0",
                            Title = doctor.STAFF_TITLE,
                            //IdentityNo = doctor.STAFF_IDENTITY_NO,

                        };
                        var spResponseDoctorContact = new ContactRepository().Insert(contact);
                        if (spResponseDoctorContact.Code == "100")
                        {
                            var contactId = spResponseDoctorContact.PkId;
                            var person = new Person
                            {
                                Id = doctor.DOCTOR_PERSON_ID,
                                ContactId = contactId,
                                ProfessionType = "4",
                                FirstName = doctor.FIRST_NAME,
                                LastName = doctor.LAST_NAME,
                                Status = doctor.STATUS
                            };
                            var spResponsePerson = new PersonRepository().Insert(person);

                            if (spResponsePerson.Code == "100")
                            {
                                var staff = new Staff
                                {
                                    Id = doctor.DOCTOR_STAFF_ID,
                                    ProviderId = long.Parse(providerId),
                                    Type = "0",
                                    ContactId = contactId,
                                    DoctorBranchId = doctor.DOCTOR_BRANCH_ID == "" ? null : doctor.DOCTOR_BRANCH_ID,
                                    DiplomaNo = doctor.DIPLOMA_NO,
                                    RegisterNo = doctor.REGISTER_NO,
                                    Status = doctor.STATUS
                                };
                                var spResponseStaff = new StaffRepository().Insert(staff);
                                if (spResponseStaff.Code != "100")
                                    throw new Exception(spResponseStaff.Code + " : " + spResponseStaff.Message);
                                else
                                {
                                    doctor.DOCTOR_STAFF_ID = spResponseStaff.PkId;
                                    result.ResultCode = spResponseStaff.Code;
                                    result.ResultMessage = spResponseStaff.Message;
                                    TempData["Alert"] = $"swAlert('İşlem Başarılı','{contact.Title} başarıyla kaydedildi.','success')";
                                }
                            }
                            else
                                throw new Exception(spResponsePerson.Code + " : " + spResponsePerson.Message);
                        }
                        else
                            throw new Exception(spResponseDoctorContact.Code + " : " + spResponseDoctorContact.Message);
                    }
                }


                var doctorss = new GenericRepository<V_Staff>().FindBy("PROVIDER_ID=:id AND STAFF_TYPE=0", orderby: "PROVIDER_ID", parameters: new { id = providerId });


                if (doctorss != null)
                {
                    List<dynamic> doctorsList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in doctorss)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.DOCTOR_CONTACT_ID = Convert.ToString(item.CONTACT_ID);
                        listItem.DOCTOR_PERSON_ID = Convert.ToString(item.PERSON_ID);
                        listItem.DOCTOR_STAFF_ID = Convert.ToString(item.STAFF_ID);

                        listItem.DOCTOR_BRANCH_ID = Convert.ToString(item.DOCTOR_BRANCH_ID);
                        listItem.DOCTOR_BRANCH_IDSelectedText = item.DOCTOR_BRANCH_NAME;
                        listItem.STAFF_TITLE = item.STAFF_TITLE;
                        listItem.DIPLOMA_NO = item.DIPLOMA_NO;
                        listItem.REGISTER_NO = item.REGISTER_NO;
                        listItem.FIRST_NAME = item.FIRST_NAME;
                        listItem.LAST_NAME = item.LAST_NAME;
                        //listItem.STAFF_IDENTITY_NO = item.STAFF_IDENTITY_NO;
                        listItem.isOpen = "0";

                        doctorsList.Add(listItem);

                        i++;
                    }
                    result.Data = new ProviderSaveStep2Result
                    {
                        jsonData = doctorsList.ToJSON()
                    };
                }


               

            }
            catch (Exception ex)
            {
                string log = ex.Message;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [LoginControl]
        public JsonResult ProviderSaveStep3(FormCollection form)
        {
            var result = new AjaxResultDto<ProviderSaveStep3Result>();
            var contacts = form["hdContacts"];
            var providerId = form["hdProviderId"];
            try
            {
                List<dynamic> contactList = new List<dynamic>();
                contactList = JsonConvert.DeserializeObject<List<dynamic>>(contacts);

                foreach (var contact in contactList)
                {
                    if (contact.isOpen == "1")
                    {
                        #region Contact Save
                        var contactId = contact.AUTHORIZED_CONTACT_ID != null ? contact.AUTHORIZED_CONTACT_ID : 0;
                        var status = contact.STATUS;
                        var contactObj = new Contact
                        {
                            Id = contactId,
                            Type = "0",
                            Title = contact.AUTHORIZED_TITLE,
                            //IdentityNo = contact.AUTHORIZED_IDENTITY_NO,
                            Status = status
                        };
                        var spResponseStaffContact = new ContactRepository().Insert(contactObj);
                        if (spResponseStaffContact.Code != "100")
                        {
                            throw new Exception(spResponseStaffContact.Message);
                        }
                        contactId = spResponseStaffContact.PkId;
                        contact.AUTHORIZED_CONTACT_ID = contactId;
                        #endregion

                        #region Person Save
                        var personId = contact.AUTHORIZED_PERSON_ID != null ? contact.AUTHORIZED_PERSON_ID : 0;
                        var person = new Person
                        {
                            Id = personId,
                            ContactId = contactId,
                            ProfessionType = "99",
                            FirstName = contact.AUTHORIZED_NAME,
                            LastName = contact.AUTHORIZED_SURNAME,
                            Status = status
                        };
                        var spResponsePerson = new PersonRepository().Insert(person);
                        if (spResponsePerson.Code != "100")
                        {
                            throw new Exception(spResponsePerson.Message);
                        }
                        personId = spResponsePerson.PkId;
                        contact.AUTHORIZED_PERSON_ID = personId;
                        #endregion

                        #region Phone Save
                        long phoneId = string.IsNullOrEmpty(Convert.ToString(contact.AUTHORIZED_PHONE_ID)) ? 0 : Convert.ToInt64(Convert.ToString(contact.AUTHORIZED_PHONE_ID));
                        // Eğer Telefon No boş bırakılmışsa ve önceden kayıt edilmişse silinmeli, boş ise kayıt işlemine hiç girmemeli
                        var phoneNo = Convert.ToString(contact.AUTHORIZED_PHONE_NO);
                        var updateOrSave = true;
                        if (string.IsNullOrEmpty(phoneNo))
                        {
                            if (phoneId > 0)
                            {
                                status = "1";
                            }
                            else
                            {
                                updateOrSave = false;
                            }
                        }
                        if (updateOrSave)
                        {
                            var phone = new Phone
                            {
                                Id = phoneId,
                                CountryId = 222,
                                Extension = contact.AUTHORIZED_EXTENSION,
                                Type = "0",
                                Status = status

                            };
                            if (!string.IsNullOrEmpty(phoneNo))
                            {
                                phone.No = phoneNo;
                            }
                            var spResponsePhone = new PhoneRepository().Insert(phone);
                            if (spResponsePhone.Code != "100")
                            {
                                throw new Exception(spResponsePhone.Message);
                            }
                            phoneId = spResponsePhone.PkId;
                            contact.AUTHORIZED_PHONE_ID = phoneId;
                        }
                        #endregion

                        #region MobilePhone Save
                        long mobilePhoneId = string.IsNullOrEmpty(Convert.ToString(contact.AUTHORIZED_MOBILE_PHONE_ID)) ? 0 : Convert.ToInt64(Convert.ToString(contact.AUTHORIZED_MOBILE_PHONE_ID));
                        // Eğer Mobil Telefon No boş bırakılmışsa ve önceden kayıt edilmişse silinmeli, boş ise kayıt işlemine hiç girmemeli
                        status = contact.STATUS;
                        var mobilePhoneNo = Convert.ToString(contact.AUTHORIZED_MOBILE_PHONE_NO);
                        updateOrSave = true;
                        if (string.IsNullOrEmpty(mobilePhoneNo))
                        {
                            if (mobilePhoneId > 0)
                            {
                                status = "1";
                            }
                            else
                            {
                                updateOrSave = false;
                            }
                        }
                        if (updateOrSave)
                        {
                            var mobilePhone = new Phone
                            {
                                Id = mobilePhoneId,
                                CountryId = 222,
                                Type = "1",
                                Status = status
                            };
                            if (!string.IsNullOrEmpty(mobilePhoneNo))
                            {
                                mobilePhone.No = mobilePhoneNo;
                            }
                            var spResponseMobilePhone = new PhoneRepository().Insert(mobilePhone);
                            if (spResponseMobilePhone.Code != "100")
                            {
                                throw new Exception(spResponseMobilePhone.Message);
                            }
                            mobilePhoneId = spResponseMobilePhone.PkId;
                            contact.AUTHORIZED_MOBILE_PHONE_ID = mobilePhoneId;
                        }
                        #endregion

                        #region Email Save
                        long emailId = string.IsNullOrEmpty(Convert.ToString(contact.AUTHORIZED_EMAIL_ID)) ? 0 : Convert.ToInt64(Convert.ToString(contact.AUTHORIZED_EMAIL_ID));
                        // Eğer Email boş bırakılmışsa ve önceden kayıt edilmişse silinmeli, boş ise kayıt işlemine hiç girmemeli
                        status = contact.STATUS;
                        var emailAddress = Convert.ToString(contact.AUTHORIZED_EMAIL);
                        updateOrSave = true;
                        if (string.IsNullOrEmpty(emailAddress))
                        {
                            if (emailId > 0)
                            {
                                status = "1";
                            }
                            else
                            {
                                updateOrSave = false;
                            }
                        }
                        if (updateOrSave)
                        {
                            var email = new Email
                            {
                                Id = emailId,
                                Type = "0",
                                Status = status
                            };
                            if (!string.IsNullOrEmpty(emailAddress))
                            {
                                email.Details = emailAddress;
                            }
                            var spResponseEmail = new EmailRepository().Insert(email);
                            if (spResponseEmail.Code != "100")
                            {
                                throw new Exception(spResponseEmail.Message);
                            }
                            emailId = spResponseEmail.PkId;
                            contact.AUTHORIZED_EMAIL_ID = emailId;
                        }
                        #endregion

                        #region Staff Save
                        var staffId = contact.AUTHORIZED_STAFF_ID != null ? contact.AUTHORIZED_STAFF_ID : "0";
                        var staff = new Staff
                        {
                            Id = staffId,
                            ProviderId = long.Parse(providerId),
                            Type = "1",
                            ContactId = contactId,
                            IsGroupAdmin = string.IsNullOrEmpty(Convert.ToString(contact.AUTHORIZED_IS_GROUP_ADMIN)) ? "0" : contact.AUTHORIZED_IS_GROUP_ADMIN,
                            Status = status
                        };
                        if (phoneId > 0)
                        {
                            staff.PhoneId = phoneId;
                        }
                        if (mobilePhoneId > 0)
                        {
                            staff.MobilePhoneId = mobilePhoneId;
                        }
                        if (emailId > 0)
                        {
                            staff.EmailId = emailId;
                        }
                        var spResponseStaff = new StaffRepository().Insert(staff);
                        if (spResponseStaff.Code != "100")
                        {
                            throw new Exception(spResponseStaff.Code + " : " + spResponseStaff.Message);
                        }
                        staffId = spResponseStaff.PkId;
                        contact.AUTHORIZED_STAFF_ID = staffId;
                        #endregion

                        result.ResultCode = spResponseStaff.Code;
                        result.ResultMessage = spResponseStaff.Message;
                    }
                }
                var contactListInDB = new GenericRepository<V_Staff>().FindBy("PROVIDER_ID=:id AND STAFF_TYPE=1", orderby: "PROVIDER_ID", parameters: new { id = providerId });

                if (contactListInDB != null)
                {
                    List<dynamic> contactList4Client = new List<dynamic>();

                    int i = 1;

                    foreach (var item in contactListInDB)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.AUTHORIZED_STAFF_ID = Convert.ToString(item.STAFF_ID);
                        listItem.AUTHORIZED_CONTACT_ID = Convert.ToString(item.CONTACT_ID);
                        listItem.AUTHORIZED_PERSON_ID = Convert.ToString(item.PERSON_ID);
                        listItem.AUTHORIZED_PHONE_ID = Convert.ToString(item.PHONE_ID);
                        listItem.AUTHORIZED_MOBILE_PHONE_ID = Convert.ToString(item.MOBILE_PHONE_ID);
                        listItem.AUTHORIZED_EMAIL_ID = Convert.ToString(item.EMAIL_ID);

                        listItem.AUTHORIZED_TITLE = item.STAFF_TITLE;
                        listItem.AUTHORIZED_NAME = item.FIRST_NAME;
                        listItem.AUTHORIZED_SURNAME = item.LAST_NAME;
                        listItem.AUTHORIZED_PHONE_NO = item.PHONE_NO;
                        listItem.AUTHORIZED_EXTENSION = item.EXTENSION;
                        listItem.AUTHORIZED_MOBILE_PHONE_NO = item.MOBILE_PHONE_NO;
                        listItem.AUTHORIZED_EMAIL = item.EMAIL_ADDRESS;
                        //listItem.AUTHORIZED_IDENTITY_NO = item.STAFF_IDENTITY_NO;
                        listItem.AUTHORIZED_IS_GROUP_ADMIN = item.IS_GROUP_ADMIN;
                        listItem.isOpen = "0";

                        contactList4Client.Add(listItem);

                        i++;
                    }

                    result.Data = new ProviderSaveStep3Result
                    {
                        jsonData = contactList4Client.ToJSON()
                    };

                    var CompanyList = new GenericRepository<V_Company>().FindBy(orderby: "COMPANY_ID").ToList();

                    ProxyServiceClient proxyServiceClient = new ProxyServiceClient();

                    foreach (var companies in CompanyList)
                    {
                        ProviderTransferReq stateUpdateReq = new ProviderTransferReq
                        {
                            ProviderId = long.Parse(providerId),
                            CompanyCode = (long)companies.COMPANY_ID
                        };
                        var sonuc = proxyServiceClient.ProviderTransfer(stateUpdateReq);
                    }
                }
            }
            catch (Exception ex)
            {
                string log = ex.Message;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [LoginControl]
        public JsonResult ProviderSaveStep4(FormCollection form)
        {
            var result = new AjaxResultDto<ProviderSaveStep4Result>();
            var banks = form["hdBanks"];
            try
            {
                List<dynamic> bankList = new List<dynamic>();
                bankList = JsonConvert.DeserializeObject<List<dynamic>>(banks);
                var providerId = form["hdProviderId"];

                foreach (var bank in bankList)
                {
                    if (bank.isOpen == "1")
                    {
                        SpResponse spResponseBankAccount = new SpResponse();
                        var bankId = bank.BANK_ID;
                        var bankName = bank.BANK_IDSelectedText;
                        var currenyType = bank.CURRENCY_TYPE;
                        var name = Convert.ToString(bank.BANK_ACCOUNT_NAME);
                        var accountNo = Convert.ToString(bank.BANK_ACCOUNT_NO);
                        var bankBranchId = string.IsNullOrEmpty(Convert.ToString(bank.BANK_BRANCH_ID)) ? 0 : Convert.ToInt64(Convert.ToString(bank.BANK_BRANCH_ID));
                        var ibanNumber = Convert.ToString(bank.IBAN);
                        var isPrimary = string.IsNullOrEmpty(Convert.ToString(bank.IS_PRIMARY_BANK)) ? "0" : Convert.ToString(bank.IS_PRIMARY_BANK);
                        var bankAccount = new BankAccount
                        {
                            Id = bank.BANK_ACCOUNT_ID,
                            Name = name,
                            CurrencyType = currenyType,
                            No = accountNo,
                            Iban = ibanNumber,
                            BankId = bankId,
                            IsPrimary = isPrimary,
                            Status = bank.STATUS
                        };
                        if (bankBranchId > 0)
                        {
                            bankAccount.BankBranchId = bankBranchId;
                        }
                        spResponseBankAccount = new BankAccountRepository().Insert(bankAccount);

                        if (spResponseBankAccount.Code == "100")
                        {
                            var bankAccountId = spResponseBankAccount.PkId;
                            var providerBankAccount = new ProviderBankAccount
                            {
                                Id = bank.PROVIDER_BANK_ACCOUNT_ID,
                                ProviderId = long.Parse(providerId),
                                BankAccountId = bankAccountId,
                                Status = bank.STATUS
                            };
                            var spResponseProviderBankAccount = new ProviderBankAccountRepository().Insert(providerBankAccount);
                            if (spResponseProviderBankAccount.Code == "100")
                            {
                                result.ResultCode = spResponseProviderBankAccount.Code;
                                result.ResultMessage = spResponseProviderBankAccount.Message;
                                TempData["Alert"] = $"swAlert('İşlem Başarılı','{bankAccount.Name} başarıyla kaydedildi.','success')";
                            }
                            else
                            {
                                throw new Exception(spResponseProviderBankAccount.Code + " : " + spResponseProviderBankAccount.Message);
                            }
                        }
                        else
                        {
                            throw new Exception(spResponseBankAccount.Code + " : " + spResponseBankAccount.Message);
                        }
                    }


                }

                var bankss = new GenericRepository<V_ProviderBank>().FindBy("PROVIDER_ID=:id", orderby: "PROVIDER_ID", parameters: new { id = providerId });
                if (bankss != null)
                {

                    List<dynamic> banksList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in bankss)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.BANK_ACCOUNT_ID = Convert.ToString(item.BANK_ACCOUNT_ID);
                        listItem.PROVIDER_BANK_ACCOUNT_ID = Convert.ToString(item.PROVIDER_BANK_ACCOUNT_ID);

                        listItem.BANK_ACCOUNT_NAME = item.BANK_ACCOUNT_NAME;
                        listItem.BANK_ID = Convert.ToString(item.BANK_ID);
                        listItem.BANK_IDSelectedText = item.BANK_NAME;


                        listItem.BANK_BRANCH_ID = Convert.ToString(item.BANK_BRANCH_ID);
                        listItem.BANK_BRANCH_IDSelectedText = item.BANK_BRANCH_NAME;
                        listItem.BANK_BRANCH_CODE = item.BANK_BRANCH_CODE;
                        listItem.BANK_ACCOUNT_NO = item.BANK_ACCOUNT_NO;
                        listItem.IBAN = item.IBAN;
                        listItem.CURRENCY_TYPE = item.CURRENCY_TYPE;
                        listItem.CURRENCY_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Currency, item.CURRENCY_TYPE);
                        listItem.IS_PRIMARY_BANK = item.IS_PRIMARY;
                        listItem.isOpen = "0";

                        banksList.Add(listItem);

                        i++;
                    }


                    result.Data = new ProviderSaveStep4Result
                    {
                        jsonData = banksList.ToJSON()
                    };


                    var CompanyList = new GenericRepository<V_Company>().FindBy(orderby: "COMPANY_ID").ToList();

                    ProxyServiceClient proxyServiceClient = new ProxyServiceClient();

                    foreach (var companies in CompanyList)
                    {
                        ProviderTransferReq stateUpdateReq = new ProviderTransferReq
                        {
                            ProviderId = long.Parse(providerId),
                            CompanyCode = (long)companies.COMPANY_ID
                        };
                        var sonuc = proxyServiceClient.ProviderTransfer(stateUpdateReq);
                    }
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public JsonResult ProviderSaveStep5(FormCollection form)
        {
            var result = new AjaxResultDto<ProviderSaveStep5Result>();
            var notes = form["hdNotes"];
            try
            {
                List<dynamic> noteList = new List<dynamic>();
                noteList = JsonConvert.DeserializeObject<List<dynamic>>(notes);
                var providerId = form["hdProviderId"];

                foreach (var note in noteList)
                {
                    if (note.isOpen == "1" /*(note.STATUS == "1" && note.NOTE_ID != 0) || (note.STATUS == "1" && note.NOTE_ID != 0) || note.STATUS == "0"*/ )
                    {
                        var Tnote = new Note
                        {
                            Id = note.NOTE_ID,
                            Type = note.NOTE_TYPE,
                            Description = note.NOTE_DESCRIPTION,
                            Status = note.STATUS
                        };
                        var spResponseNote = new NoteRepository().Insert(Tnote);
                        if (spResponseNote.Code == "100")
                        {
                            var noteId = spResponseNote.PkId;
                            var providerNote = new ProviderNote
                            {
                                Id = note.PROVIDER_NOTE_ID,
                                ProviderId = long.Parse(providerId),
                                NoteId = noteId,
                                Status = note.STATUS
                            };
                            var spResponseProviderNote = new GenericRepository<ProviderNote>().Insert(providerNote);
                            if (spResponseProviderNote.Code == "100")
                            {
                                result.ResultCode = spResponseProviderNote.Code;
                                result.ResultMessage = spResponseProviderNote.Message;
                                TempData["Alert"] = $"swAlert('İşlem Başarılı','{Tnote.Description} başarıyla kaydedildi.','success')";
                            }
                            else
                            {
                                throw new Exception(spResponseProviderNote.Code + " : " + spResponseProviderNote.Message);
                            }
                        }
                        else
                        {
                            throw new Exception(spResponseNote.Code + " : " + spResponseNote.Message);
                        }
                    }
                }

                var notess = new GenericRepository<V_ProviderNote>().FindBy("PROVIDER_ID=:id", orderby: "PROVIDER_ID", parameters: new { id = providerId });
                if (notess != null)
                {
                    List<dynamic> notesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in notess)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                        listItem.PROVIDER_NOTE_ID = Convert.ToString(item.PROVIDER_NOTE_ID);

                        listItem.NOTE_TYPE = Convert.ToString(item.NOTE_TYPE);
                        listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.ProviderNote, item.NOTE_TYPE);
                        listItem.NOTE_DESCRIPTION = item.NOTE_DESCRIPTION;

                        listItem.isOpen = "0";
                        notesList.Add(listItem);

                        i++;
                    }

                    result.Data = new ProviderSaveStep5Result
                    {
                        jsonData = notesList.ToJSON()
                    };

                    var CompanyList = new GenericRepository<V_Company>().FindBy(orderby: "COMPANY_ID").ToList();

                    ProxyServiceClient proxyServiceClient = new ProxyServiceClient();

                    foreach (var companies in CompanyList)
                    {
                        ProviderTransferReq stateUpdateReq = new ProviderTransferReq
                        {
                            ProviderId = long.Parse(providerId),
                            CompanyCode = (long)companies.COMPANY_ID
                        };
                        var sonuc = proxyServiceClient.ProviderTransfer(stateUpdateReq);
                    }
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public JsonResult ProviderSaveStep6(FormCollection form)
        {
            var result = new AjaxResultDto<ProviderSaveStep6Result>();
            var medias = form["hdMedias"];
            try
            {
                List<dynamic> mediaList = new List<dynamic>();
                mediaList = JsonConvert.DeserializeObject<List<dynamic>>(medias);
                var providerId = form["hdProviderId"];

                foreach (var media in mediaList)
                {
                    if (media.isOpen == "1")
                    {
                        var Tmedia = new Media
                        {
                            Id = media.MEDIA_ID == "" ? 0 : media.MEDIA_ID,
                            Name = media.MEDIA_NAME,
                            FileName = "TEST",
                            Status = media.STATUS
                        };
                        var spResponseMedia = new MediaRepository().Insert(Tmedia);
                        if (spResponseMedia.Code == "100")
                        {
                            var mediaId = spResponseMedia.PkId;
                            var providerNote = new ProviderMedia
                            {
                                Id = media.PROVIDER_MEDIA_ID == "" ? 0 : media.PROVIDER_MEDIA_ID,
                                ProviderId = long.Parse(providerId),
                                MediaId = mediaId,
                                Status = media.STATUS
                            };
                            var spResponseProviderNote = new ProviderMediaRepository().Insert(providerNote);
                            if (spResponseProviderNote.Code == "100")
                            {
                                result.ResultCode = spResponseProviderNote.Code;
                                result.ResultMessage = spResponseProviderNote.Message;
                                TempData["Alert"] = $"swAlert('İşlemi Başarılı','{Tmedia.Name} başarıyla kaydedildi.','success')";

                            }
                            else
                            {
                                throw new Exception(spResponseProviderNote.Code + " : " + spResponseProviderNote.Message);
                            }
                        }
                        else
                        {
                            throw new Exception(spResponseMedia.Code + " : " + spResponseMedia.Message);
                        }
                    }
                }

                var mediass = new GenericRepository<V_ProviderMedia>().FindBy("PROVIDER_ID=:id", orderby: "PROVIDER_ID", parameters: new { id = providerId });
                if (mediass != null)
                {
                    List<dynamic> mediasList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in mediass)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.Status;

                        listItem.MEDIA_ID = Convert.ToString(item.MediaId);
                        listItem.PROVIDER_MEDIA_ID = Convert.ToString(item.ProviderMediaId);

                        listItem.MEDIA_NAME = item.MediaName;
                        listItem.MEDIA_PATH = item.FileName;
                        listItem.isOpen = "0";

                        mediasList.Add(listItem);

                        i++;
                    }

                    result.Data = new ProviderSaveStep6Result
                    {
                        jsonData = mediasList.ToJSON()
                    };
                }
                
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [LoginControl]
        public JsonResult Doctors(Int64 providerId)
        {
            var doctors = new GenericRepository<V_Staff>().FindBy("PROVIDER_ID = :providerId AND STAFF_TYPE=:staffType", orderby: "FIRST_NAME", parameters: new { providerId, staffType = new DbString { Value = ((int)StaffType.DOKTOR).ToString(), Length = 3 } });

            int i = 1;
            List<dynamic> listProcess = new List<dynamic>();

            foreach (var item in doctors)
            {
                dynamic listItem = new System.Dynamic.ExpandoObject();

                listItem.id = i;
                listItem.ClientId = i;
                listItem.STATUS = item.STATUS;

                listItem.STAFF_ID = item.STAFF_ID;
                listItem.FIRST_NAME = item.FIRST_NAME;
                listItem.LAST_NAME = item.LAST_NAME;
                listItem.DOCTOR_BRANCH_ID = item.DOCTOR_BRANCH_ID;
                listItem.DOCTOR_BRANCH_NAME = item.DOCTOR_BRANCH_NAME;
                listItem.DIPLOMA_NO = item.DIPLOMA_NO;
                listItem.REGISTER_NO = item.REGISTER_NO;
                listItem.STAFF_TITLE = item.STAFF_TITLE;
                listItem.STAFF_IDENTITY_NO = item.STAFF_IDENTITY_NO;

                listProcess.Add(listItem);

                i++;
            }

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = listProcess.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        [LoginControl]
        public JsonResult SaveDoctor(Int64 providerId, FormCollection form)
        {
            var result = new AjaxResultDto<ProviderSaveStep2Result>();
            try
            {
                var contractType = form["NEW_DOCTOR_CONTRACT_TYPE"];
                if (string.IsNullOrEmpty(contractType))
                {
                    throw new Exception("Doktor anlşamalı ya da anlaşmasız bilgisini giriniz!");
                }
                var doctorBranchId = form["DOCTOR_BRANCH_LIST"];
                if (string.IsNullOrEmpty(doctorBranchId))
                {
                    throw new Exception("Doktor bölüm/branş bilgisini giriniz!");
                }
                var title = form["NEW_DOCTOR_TITLE"];
                var diplomaNo = form["NEW_DOCTOR_DIPLOMA_NO"];
                var registerNo = form["NEW_DOCTOR_REGISTER_NO"];
                var firstName = form["NEW_DOCTOR_FIRST_NAME"];
                if (string.IsNullOrEmpty(firstName))
                {
                    throw new Exception("Doktor adını giriniz!");
                }
                var lastName = form["NEW_DOCTOR_LAST_NAME"];
                if (string.IsNullOrEmpty(lastName))
                {
                    throw new Exception("Doktor soyadını giriniz!");
                }
                Int64? identityNo = form["NEW_DOCTOR_IDENTITY_NO"].IsInt64() ? (long?)Convert.ToInt64(form["NEW_DOCTOR_IDENTITY_NO"]) : null;

                var staff = new Staff();
                var person = new Person();
                var contact = new Contact
                {
                    Id = 0,
                    Type = "0",
                    Title = title,
                    IdentityNo = identityNo

                };
                var spResponseDoctorContact = new ContactRepository().Insert(contact);
                if (spResponseDoctorContact.Code == "100")
                {
                    var contactId = spResponseDoctorContact.PkId;

                    person.Id = 0;
                    person.ContactId = contactId;
                    person.ProfessionType = "4";
                    person.FirstName = firstName;
                    person.LastName = lastName;

                    var spResponsePerson = new PersonRepository().Insert(person);
                    if (spResponsePerson.Code == "100")
                    {
                        staff.Id = 0;
                        staff.ProviderId = providerId;
                        staff.Type = "0";
                        staff.ContactId = contactId;
                        staff.DoctorBranchId = long.Parse(doctorBranchId);
                        staff.DiplomaNo = diplomaNo;
                        staff.RegisterNo = registerNo;
                        staff.ContractType = contractType;

                        var spResponseStaff = new StaffRepository().Insert(staff);
                        if (spResponseStaff.Code != "100")
                        {
                            throw new Exception(spResponseStaff.Code + " : " + spResponseStaff.Message);
                        }
                        else
                        {
                            staff.Id = spResponseStaff.PkId;
                            result.ResultCode = spResponseStaff.Code;
                            result.ResultMessage = spResponseStaff.Message;
                        }
                    }
                    else
                    {
                        throw new Exception(spResponsePerson.Code + " : " + spResponsePerson.Message);
                    }
                }
                else
                {
                    throw new Exception(spResponseDoctorContact.Code + " : " + spResponseDoctorContact.Message);
                }

                List<dynamic> doctorsList = new List<dynamic>();

                var doctor = new GenericRepository<V_Staff>().FindBy("STAFF_ID=:id", orderby: "STAFF_ID", parameters: new { id = staff.Id }).FirstOrDefault();
                if (doctor != null)
                {
                    if (doctor != null)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.CONTRACT_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Contract, doctor.CONTRACT_TYPE);
                        listItem.STAFF_ID = doctor.STAFF_ID;

                        listItem.DOCTOR_BRANCH_ID = doctor.DOCTOR_BRANCH_ID;
                        listItem.DOCTOR_BRANCH_IDSelectedText = doctor.DOCTOR_BRANCH_NAME;
                        listItem.STAFF_TITLE = doctor.STAFF_TITLE;
                        listItem.DIPLOMA_NO = doctor.DIPLOMA_NO;
                        listItem.REGISTER_NO = doctor.REGISTER_NO;
                        listItem.FIRST_NAME = doctor.FIRST_NAME;
                        listItem.LAST_NAME = doctor.LAST_NAME;
                        listItem.STAFF_IDENTITY_NO = doctor.STAFF_IDENTITY_NO;

                        doctorsList.Add(listItem);
                    }
                }

                result.Data = new ProviderSaveStep2Result
                {
                    jsonData = doctorsList.ToJSON()
                };
            }
            catch (Exception ex)
            {
                string log = ex.Message;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region Contract
        // GET: Contact List
        [LoginControl]
        public ActionResult Contract()
        {
            ContractVM vm = new ContractVM();
            try
            {
                vm.HistoryVM.ViewName = Views.ContractListHist;
                vm.ExportVM.ViewName = Constants.Views.ExportContractList;
                vm.HistoryVM.HistoryTitle = "Sözleşme";
                vm.ExportVM.SetExportColumns();

                ViewBag.contractNo = string.Empty;
                ViewBag.contractProviderList = string.Empty;
                ViewBag.contractStatus = string.Empty;
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;
                var ProviderType = LookupHelper.GetLookupData(Constants.LookupTypes.Provider, showAll: true);
                ViewBag.ProviderTypeList = ProviderType;

                var ProviderList = new GenericRepository<V_Provider>().FindBy(orderby: "PROVIDER_ID");
                ViewBag.ProviderList = ProviderList;

                var ContractStatusType = LookupHelper.GetLookupData(Constants.LookupTypes.ContractStatus, showAll: true);
                ViewBag.ContractStatusTypeList = ContractStatusType;

                ViewBag.ContractTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Contract);

                var ReasonTypeList = new ReasonRepository().FindBy();
                ViewBag.ReasonTypeList = ReasonTypeList;


                //
                ViewBag.CountyList = new List<County>();

                var AgreementType = LookupHelper.GetLookupData(Constants.LookupTypes.Agreement, showAll: true);
                ViewBag.AgreementTypeList = AgreementType;

                var Contract_Type = LookupHelper.GetLookupData(Constants.LookupTypes.Contract, showAll: true);
                ViewBag.ContractTypeList = Contract_Type;

                var CityList = new CityRepository().FindBy();
                ViewBag.CityList = CityList;

                var ProviderGroupList = new ProviderGroupRepository().FindBy();
                ViewBag.ProviderGroupList = ProviderGroupList;
                //



                var ContractList = new GenericRepository<V_ContractList>().FindBy(orderby: "CONTRACT_ID");
                foreach (var item in ContractList)
                {
                    if (item.CONTRACT_TYPE.IsInt())
                    {
                        switch (int.Parse(item.CONTRACT_TYPE))
                        {
                            case ((int)ContractType.TSS):
                                item.CONTRACT_TYPE = "TSS";
                                break;
                            case ((int)ContractType.OSS):
                                item.CONTRACT_TYPE = "ÖSS";
                                break;
                            case ((int)ContractType.MDP):
                                item.CONTRACT_TYPE = "MDP";
                                break;
                            case ((int)ContractType.DOGUM):
                                item.CONTRACT_TYPE = "DOĞUM";
                                break;
                            default:
                                break;
                        }
                    }
                }
                ViewBag.ContractList = null;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View(vm);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult ContractFilter(FormCollection form)
        {
            ContractVM vm = new ContractVM();
            try
            {
                vm.HistoryVM.ViewName = Views.ContractList;
                vm.ExportVM.ViewName = Constants.Views.ExportContractList;
                vm.HistoryVM.HistoryTitle = "Sözleşme";
                vm.ExportVM.SetExportColumns();

                var ProviderType = LookupHelper.GetLookupData(Constants.LookupTypes.Provider, showAll: true);
                ViewBag.ProviderTypeList = ProviderType;

                var ProviderList = new GenericRepository<V_Provider>().FindBy(orderby: "PROVIDER_ID");
                ViewBag.ProviderList = ProviderList;

                var ReasonTypeList = new ReasonRepository().FindBy();
                ViewBag.ReasonTypeList = ReasonTypeList;

                var ContractStatusType = LookupHelper.GetLookupData(Constants.LookupTypes.ContractStatus, showAll: true);
                ViewBag.ContractStatusTypeList = ContractStatusType;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.contractNo = form["contractNo"];
                ViewBag.contractProviderList = form["contractProviderList"];
                ViewBag.contractStatus = form["contractStatus"];




                //
                ViewBag.CountyList = new List<County>();

                var AgreementType = LookupHelper.GetLookupData(Constants.LookupTypes.Agreement, showAll: true);
                ViewBag.AgreementTypeList = AgreementType;

                var ContractType = LookupHelper.GetLookupData(Constants.LookupTypes.Contract, showAll: true);
                ViewBag.ContractTypeList = ContractType;

                var CityList = new CityRepository().FindBy();
                ViewBag.CityList = CityList;

                var ProviderGroupList = new ProviderGroupRepository().FindBy();
                ViewBag.ProviderGroupList = ProviderGroupList;
                //










                string whereConditition = !String.IsNullOrEmpty(form["contractNo"]) ? $" CONTRACT_NO='{form["contractNo"]}' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["contractProviderList"]) ? $" PROVIDER_ID={long.Parse(form["contractProviderList"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["contractStatus"]) ? $" STATUS='{form["contractStatus"]}' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["providerGroup"]) ? $" PROVIDER_GROUP_ID={long.Parse(form["providerGroup"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["providerCode"]) ? $" PROVIDER_ID={long.Parse(form["providerCode"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["providerType"]) ? $" PROVIDER_TYPE='{form["providerType"]}' AND" : "";
                //whereConditition += form["providerTaxNumber"].IsInt64() ? $" TAX_NUMBER = {form["providerTaxNumber"]} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["providerCity"]) ? $" CITY_ID={long.Parse(form["providerCity"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["providerCounty"]) ? $" COUNTY_ID={long.Parse(form["providerCounty"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["providerName"]) ? $" PROVIDER_NAME LIKE '%{form["providerName"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["providerTitle"]) ? $" PROVIDER_TITLE LIKE '%{form["providerTitle"]}%' AND" : "";
                var contractType = form["providerContractType"];
                if (!string.IsNullOrEmpty(contractType))
                {
                    whereConditition += " (   ";
                    foreach (var item in contractType.Split(','))
                    {
                        switch (item)
                        {
                            case "0":
                                whereConditition += " CONTRACT_TYPE=0 OR";
                                break;
                            case "1":
                                whereConditition += " CONTRACT_TYPE=1 OR";
                                break;
                            case "2":
                                whereConditition += " CONTRACT_TYPE=2 OR";
                                break;
                            default:
                                whereConditition += " CONTRACT_TYPE=3 OR";
                                break;
                        }
                    }
                    whereConditition = whereConditition.Substring(0, whereConditition.Length - 3) + ")   ";
                }
                ViewBag.ContractList = null;
                if (whereConditition != "")
                {
                    var ContractList = new GenericRepository<V_ContractList>().FindBy(string.IsNullOrEmpty(whereConditition) ? string.Empty : whereConditition.Substring(0, whereConditition.Length - 3), orderby: "CONTRACT_ID");
                    ViewBag.ContractList = ContractList;
                }
              
                vm.ExportVM.WhereCondition = string.IsNullOrEmpty(whereConditition) ? string.Empty : whereConditition.Substring(0, whereConditition.Length - 3);
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View("Contract", vm);
        }

        // GET: Contact Form
        [LoginControl]
        public ActionResult ContractForm(string Id = "0", string formaction = "")
        {
            try
            {

                #region Fill View

                var result = new GenericRepository<V_Provider>().FindBy(orderby: "PROVIDER_NAME");
                ViewBag.ProviderList = result;

                // Sadece Type'ı İŞLEM (1) olan İşlem listeleri gönderilmeli
                var ProcessListList = new ProcessListRepository().FindBy($"TYPE={(int)ProcessListType.SUT} OR TYPE={(int)ProcessListType.TDB} OR TYPE={(int)ProcessListType.TTB} OR TYPE={(int)ProcessListType.CARI}",
                                                                         orderby: "Name");
                ViewBag.ProcessListList = ProcessListList;

                var CompanyList = new CompanyRepository().FindBy();
                ViewBag.CompanyList = CompanyList;

                var NetworkList = new NetworkRepository().FindBy(orderby: "NAME");
                ViewBag.NetworkList = NetworkList;

                var ContractType = LookupHelper.GetLookupData(Constants.LookupTypes.Contract);
                ViewBag.ContractTypeList = ContractType;

                var PricingType = LookupHelper.GetLookupData(Constants.LookupTypes.Pricing);
                ViewBag.PriceTypeList = PricingType;

                var ReasonTypeList = new ReasonRepository().FindBy();
                ViewBag.ReasonTypeList = ReasonTypeList;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;
                #endregion
                ViewBag.isEdit = false;

                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Sözleşme Güncelle";
                        ViewBag.isEdit = true;

                        var Contract = new V_ContractList();
                        var ProcessGroupList = new ProcessGroupRepository().FindBy();
                        ViewBag.ProcessGroupList = ProcessGroupList;
                        if (Id.IndexOf('-') >= 0)
                        {
                            Contract = new GenericRepository<V_ContractList>().FindBy("PROVIDER_ID = :id AND CONTRACT_TYPE = :type",
                                                        orderby: "CONTRACT_ID",
                                                        parameters: new { id = Id.IndexOf('-') >= 0 ? Id.Split('-')[0] : Id, type = Id.Split('-')[1] }).FirstOrDefault();
                        }
                        else
                        {
                            Contract = new GenericRepository<V_ContractList>().FindBy("CONTRACT_ID = :id",
                                                        orderby: "CONTRACT_ID",
                                                        parameters: new { id = Id }).FirstOrDefault();
                        }
                        if (Contract != null)
                        {
                            ViewBag.Contract = Contract;
                            var contractId = Contract.CONTRACT_ID;
                            var contractNetworks = new GenericRepository<V_ContractNetwork>().FindBy("CONTRACT_ID = :id", orderby: "CONTRACT_ID", parameters: new { id = contractId });

                            if (contractNetworks != null)
                            {
                                int i = 1;

                                var contractNetworksList = contractNetworks;

                                var groupedData = (from t in contractNetworksList
                                                   group t by t.NETWORK_ID into g
                                                   select new { Root = g.Key, Items = g.ToList() }).ToList();


                                List<dynamic> contractNetworkListResult = new List<dynamic>();

                                foreach (var rootItem in groupedData)
                                {
                                    dynamic listItem = new System.Dynamic.ExpandoObject();

                                    var item = rootItem.Items[0];

                                    listItem.id = i;
                                    listItem.ClientId = i;
                                    listItem.STATUS = item.STATUS;

                                    listItem.CONTRACT_NETWORK_ID = Convert.ToString(item.CONTRACT_NETWORK_ID);

                                    List<string> COMPANYSelectedIds = new List<string>();
                                    List<string> COMPANY_IDSelectedTexts = new List<string>();

                                    foreach (var gItem in rootItem.Items)
                                    {
                                        COMPANYSelectedIds.Add(Convert.ToString(gItem.COMPANY_ID));
                                        COMPANY_IDSelectedTexts.Add(Convert.ToString(gItem.COMPANY_NAME));
                                    }

                                    listItem.COMPANY_NAME_ArraySelectedId = COMPANYSelectedIds;
                                    listItem.COMPANY_NAME_ArraySelectedText = COMPANY_IDSelectedTexts;

                                    listItem.NETWORK_ID = Convert.ToString(item.NETWORK_ID);
                                    listItem.NETWORK_IDSelectedText = item.NETWORK_NAME;

                                    contractNetworkListResult.Add(listItem);

                                    i++;
                                }
                                ViewBag.ContractNetwork = contractNetworkListResult.ToJSON();
                            }

                            var contractProcessGroup = new GenericRepository<V_ContractProcessGroup>().FindBy("CONTRACT_ID = :id", orderby: "CONTRACT_ID", parameters: new { id = contractId });

                            if (contractProcessGroup != null)
                            {
                                List<dynamic> contractProcessGroupList = new List<dynamic>();

                                int i = 1;

                                foreach (var item in contractProcessGroup)
                                {

                                    dynamic listItem = new System.Dynamic.ExpandoObject();

                                    listItem.id = i;
                                    listItem.ClientId = i;
                                    listItem.STATUS = item.STATUS;

                                    listItem.CONTRACT_PROCESS_GROUP_ID = Convert.ToString(item.CONTRACT_PROCESS_GROUP_ID);

                                    listItem.PROCESS_LIST_ID = Convert.ToString(item.PROCESS_LIST_ID);
                                    listItem.PROCESS_LIST_IDSelectedText = item.PROCESS_LIST_NAME;
                                    listItem.PROCESS_GROUP_ID = Convert.ToString(item.PROCESS_GROUP_ID);
                                    listItem.PROCESS_GROUP_IDSelectedText = item.PROCESS_GROUP_NAME;

                                    listItem.PRICING_TYPE = item.PRICING_TYPE;
                                    listItem.PRICING_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Pricing, item.PRICING_TYPE);
                                    listItem.AMOUNT = item.AMOUNT;

                                    contractProcessGroupList.Add(listItem);

                                    i++;
                                }

                                ViewBag.ContractProcessGroup = contractProcessGroupList.ToJSON();
                            }
                        }
                        else
                            TempData["Alert"] = $"swAlert('Bilgi','Aranılan Sözleşme Bulunamadı. Lütfen Tekrar Deneyiniz...','information')";
                        break;

                    default:
                        ViewBag.Title = "Sözleşme Ekle";
                        ViewBag.isEdit = false;

                        var contract = new GenericRepository<Contract>().FindBy(orderby: "cast(NO as NUMBER) DESC").FirstOrDefault();
                        ViewBag.AutoNo = contract != null && contract.No.IsInt() ? int.Parse(contract.No) + 1 : 1;
                        if (Id != "0")
                        {
                            ViewBag.SelectedProviderId = Id.Split('-')[0];
                            ViewBag.SelectedContractTypeKey = Id.Split('-')[1];
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult ContractCreate(FormCollection form)
        {
            try
            {
                var contractNo = form["contractNo"];
                var providerId = form["selectProvider"];
                var contractTypeId = form["contractType"];
                var contractPayDate = form["contractPayDate"];
                var contractDiscount = form["contractDiscount"].Replace('.', ',');

                var contractStartDate = form["contractStartDate"];
                var contractRevisionDate = form["revisionDate"];
                var contractEndDate = form["contractEndDate"];
                var contractStatus = form["contractStatus"];
                var sayi = decimal.Parse(contractDiscount);
                var contractid = form["hdCONTRACTID"];

                var contract = new Contract
                {
                    Id = !String.IsNullOrEmpty(contractid) ? long.Parse(contractid) : 0,
                    ProviderId = long.Parse(providerId),
                    No = contractNo,
                    Type = contractTypeId,
                    StartDate = DateTime.Parse(contractStartDate),
                    EndDate = contractEndDate.IsDateTime() ? (DateTime?)DateTime.Parse(contractEndDate) : null,
                    RevisionDate = contractRevisionDate.IsDateTime() ? (DateTime?)DateTime.Parse(contractRevisionDate) : null,
                    DiscountRatio = decimal.Parse(contractDiscount),
                    PaymentDay = int.Parse(contractPayDate),
                    Status = contractStatus
                };
                var spResponseContract = new ContractRepository().Insert(contract);
                if (spResponseContract.Code == "100")
                {
                    var contractId = spResponseContract.PkId;
                    var networks = form["hdNetworks"];

                    List<dynamic> networkList = new List<dynamic>();
                    networkList = JsonConvert.DeserializeObject<List<dynamic>>(networks);

                    foreach (var network in networkList)
                    {
                        if ((network.STATUS == "1" && network.CONTRACT_NETWORK_ID != 0) || network.STATUS == "0")
                        {
                            string idList = network.COMPANY_NAME_ArraySelectedId.ToString();
                            foreach (var companyId in idList.Replace("\r", "").Replace("\n", "").Replace("[", string.Empty).Replace("]", "").Replace("\"", "").Replace(" ", "").Split(','))
                            {
                                var TcontractNetwork = new ContractNetwork
                                {
                                    Id = network.CONTRACT_NETWORK_ID,
                                    ContractId = contractId,
                                    NetworkId = network.NETWORK_ID,
                                    CompanyId = long.Parse(companyId),
                                    Status = Convert.ToString(network.STATUS)
                                };
                                var spResponseContractNetwork = new ContractNetworkRepository().Insert(TcontractNetwork);
                                if (spResponseContractNetwork.Code != "100")
                                {
                                    throw new Exception(spResponseContractNetwork.Message);
                                }
                            }
                        }
                    }

                    var processGroups = form["hdProcessGroups"];
                    List<dynamic> processGroupList = new List<dynamic>();
                    processGroupList = JsonConvert.DeserializeObject<List<dynamic>>(processGroups);

                    foreach (var processGroup in processGroupList)
                    {
                        if ((processGroup.STATUS == "1" && processGroup.CONTRACT_PROCESS_GROUP_ID != 0) || processGroup.STATUS == "0")
                        {
                            var contractProcessGroup = new ContractProcessGroup
                            {
                                Id = String.IsNullOrEmpty(processGroup.CONTRACT_PROCESS_GROUP_ID.ToString()) ? 0 : long.Parse(processGroup.CONTRACT_PROCESS_GROUP_ID.ToString()),
                                ContractId = contractId,
                                ProcessGroupId = processGroup.PROCESS_GROUP_ID,
                                PricingType = processGroup.PRICING_TYPE,
                                Amount = Decimal.Parse(processGroup.AMOUNT.ToString().Replace('.', ',')),
                                Status = Convert.ToString(processGroup.STATUS)
                            };
                            var spResponseContractProcessGroup = new ContractProcessGroupRepository().Insert(contractProcessGroup);
                            if (spResponseContractProcessGroup.Code != "100")
                            {
                                throw new Exception(spResponseContractProcessGroup.Message);
                            }
                        }
                    }
                    TempData["Alert"] = $"swAlert('İşlemi Başarılı','{contract.No} nolu sözleşme başarıyla kaydedildi.','success')";
                }
                else
                {
                    throw new Exception(spResponseContract.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
                //return View("ContractForm");
            }
            return RedirectToAction("Contract");
        }


        [HttpPost]
        [LoginControl]
        public JsonResult ContractUpdateStatus24(UpdateStatus data)
        {
            try
            {
                var contract = new ContractRepository().FindById(data.id);
                contract.Status = data.status;
                var spResponseContract = new ContractRepository().Update(contract);
                if (spResponseContract.Code == "100")
                {
                    TempData["Alert"] = $"swAlert('İşlemi Başarılı','{contract.No} nolu sözleşme durumu başarıyla değiştirildi.','success')";
                }
                else
                {
                    throw new Exception(spResponseContract.Code + " : " + spResponseContract.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(data.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [LoginControl]
        public JsonResult ContractUpdateStatus36(UpdateStatusDetails data)
        {
            try
            {
                var contract = new GenericRepository<Contract>().FindById(data.CONTRACT_ID);
                contract.Status = data.STATUS;
                contract.ReasonId = data.TYPE;
                contract.Description = data.NOTE;
                contract.ResponseDate = data.DATE.IsDateTime() ? (DateTime?)DateTime.Parse(data.DATE) : null;
                var spResponseContract = new ContractRepository().Update(contract);
                if (spResponseContract.Code == "100")
                {
                    TempData["Alert"] = $"swAlert('İşlemi Başarılı','{contract.No} nolu sözleşme durumu başarıyla değiştirildi.','success')";
                }
                else
                {
                    throw new Exception(spResponseContract.Code + " : " + spResponseContract.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(data.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [LoginControl]
        public ActionResult ContractDelete(Int64 Id)
        {
            try
            {
                var repo = new ContractRepository();
                var result = repo.FindById(Id);
                result.Status = "1";
                var spResponseProvider = repo.Update(result);
                if (spResponseProvider.Code == "100")
                {
                    TempData["Alert"] = $"swAlert('Silme İşlemi Başarılı','{result.No} Nolu Sözleşme Başarıyla Silindi.','success')";
                }
                else
                {
                    throw new Exception(spResponseProvider.Code + " : " + spResponseProvider.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Contract");
        }
        #endregion
    }

    public class UpdateStatus
    {
        public int id { get; set; }
        public string status { get; set; }
    }

    public class UpdateStatusDetails
    {
        public int CONTRACT_ID { get; set; }
        public string STATUS { get; set; }
        public int? TYPE { get; set; }
        public string NOTE { get; set; }
        public string DATE { get; set; }
        public string REPLYDATE { get; set; }
        public string TYPETEXT { get; set; }
    }

}