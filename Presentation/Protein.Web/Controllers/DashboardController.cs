﻿using Dapper;
using Protein.Common.Extensions;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Data.Workers;
using Protein.Web.ActionFilter.LoginAttr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Web.Controllers
{
    public class DashboardController : Controller
    {
        // GET: Dashboard

        public ActionResult Index()
        {
            TempData["Alert"] = TempData["Alert"] ?? string.Empty;
            if (Session["Personnel"] == null)
                return RedirectToAction("Login");
            LookupHelper.LoadLookupDataFromDbToCache();
            ReasonHelper.LoadReasonDataFromDbToCache();
            ICD10Helper.LoadICD10DataFromDbToCache();
            ICD10StraightHelper.LoadAppsettingsFromDb();
            ProcessListHelper.LoadProcessListDataFromDbToCache();
            return View();
        }

        #region Login
        // GET: Login
        public ActionResult Login(string returnUrl = "")
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;
                ViewBag.inputEmail = string.Empty;
                ViewBag.inputPassword = string.Empty;
                ViewBag.ReturnUrl = !string.IsNullOrEmpty(returnUrl.Trim()) ? returnUrl.Trim() : string.Empty;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }
        // GET: Login
        [HttpPost]
        public ActionResult Login(FormCollection form)
        {
            string returnUrl = "";
            try
            {
                //CommonPolicyDto commonPolicyDto = new CommonPolicyDto();
                //commonPolicyDto.insurer = new InsurerDto
                //{
                //    contact = new ContactDto
                //    {
                //        identityNo = 15267489547,
                //        taxNumber = 5689120456,
                //        title = "TEST",
                //        type = ContactType.TUZEL
                //    },
                //    corporate = new CorporateDto
                //    {
                //        name = "TEST",
                //        type = CorporateType.LIMITED
                //    },
                //    mobilePhone = new PhoneDto
                //    {
                //        no = "05121212132",
                //    },
                //    email = new EmailDto
                //    {
                //        details = "Test",
                //        type = EmailType.IS
                //    }
                //};
                //PolicyService policyService = new PolicyService();
                //policyService.InsertInsurer(commonPolicyDto);
                //    ProxyServiceClient proxyServiceClient = new ProxyServiceClient();
                //    ProviderTransferReq stateUpdateReq = new ProviderTransferReq
                //    {
                //        ProviderId = 235122,
                //        CompanyCode = 95
                //    };
                //    proxyServiceClient.ProviderTransfer(stateUpdateReq);


                //foreach (var item in providers)
                //{
                //    ProxyServiceClient proxyServiceClient = new ProxyServiceClient();
                //    ProviderTransferReq stateUpdateReq = new ProviderTransferReq
                //    {
                //        ProviderId = item.PROVIDER_ID,
                //        CompanyCode = 10
                //    };
                //    proxyServiceClient.ProviderTransfer(stateUpdateReq);
                //}
                if (HttpContext.Request.UrlReferrer.Query.Contains("returnUrl"))
                    returnUrl = HttpContext.Request.UrlReferrer.Query.ToString().Replace("?returnUrl=", "").ToString();


                //Filter values
                var inputEmail = form["inputEmail"];
                var inputPassword = form["inputPassword"].ToMD5().ToUpper();
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                string whereCondition = $"USERNAME = :username AND PASSWORD = :password";

                User user = new GenericRepository<User>().FindBy(whereCondition,
                                   parameters: new { username = new DbString { Value = inputEmail, Length = 45 }, password = new DbString { Value = inputPassword, Length = 45 } }).FirstOrDefault();
                if (user == null)
                {
                    TempData["Alert"] = $"swAlert('Hata','Hatalı E-Posta/Şifre. Lütfen tekrar deneyiniz.','warning')";
                    return View("Login");
                }

                whereCondition = $"USER_ID = :userId";
                var personnel = new GenericRepository<V_Personnel>().FindBy(whereCondition, orderby: "PERSONNEL_ID", parameters: new { userId = user.Id }).FirstOrDefault();
                if (personnel != null)
                {
                    Session["Personnel"] = personnel;
                    Session.Timeout = 60;

                    var token = new TokenWorker().SetToken(new UserWorker().GetUserID());

                    whereCondition = $"PERSONNEL_ID = :personelId";
                    var personnelCadreList = new GenericRepository<PersonnelCadre>().FindBy(whereCondition, orderby: "PERSONNEL_ID", parameters: new { personelId = personnel.PERSONNEL_ID });
                    if (personnelCadreList.Count > 0)
                    {
                        Session["Cadres"] = personnelCadreList;
                        string cadreIds = string.Join(",", personnelCadreList.Select(pc => pc.CadreId));

                        whereCondition = $"CADRE_ID IN (:cadreIds)";
                        List<CadrePrivilege> cadrePrivilegeList = new GenericRepository<CadrePrivilege>().FindBy(whereCondition, orderby: "CADRE_ID", parameters: new { cadreIds });
                        Session["Privileges"] = cadrePrivilegeList;
                    }
                }
                else
                {
                    CompanyUser companyUser = new GenericRepository<CompanyUser>().FindBy(whereCondition, orderby: "USER_ID", parameters: new { userId = user.Id }).FirstOrDefault();
                    if (companyUser==null)
                    {
                        TempData["Alert"] = $"swAlert('Hata','Giriş Başarısız! Tekrar Deneyiniz...','warning')";
                        return View("Login");
                    }

                    Session["CompanyId"] = companyUser.CompanyId;
                    Session["CompanyUser"] = companyUser;
                    Session.Timeout = 60;

                    var token = new TokenWorker().SetToken(new UserWorker().GetUserID());

                    return  Redirect("~/DashBoardCompany/Index");
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return !string.IsNullOrEmpty(returnUrl) ? Redirect(returnUrl) : Redirect("Index");
            //return View("Index");
        }
        // GET: Logout
        [LoginControl]
        public ActionResult Logout()
        {
            try
            {
                #region Token proc
                TokenWorker token = new TokenWorker();
                string _token = token.GetValidToken(new UserWorker().GetUserID());
                token.FindAndDestroyIncludeToken(new UserWorker().GetUserID(), _token);
                #endregion
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;
                Session["Personnel"] = null;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View("Login");
        }
        #endregion

        

    }
}