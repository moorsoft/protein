﻿using Protein.Common.Dto;
using Protein.Common.Extensions;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Web.ActionFilter.LoginAttr;
using Protein.Web.Models.ExportModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Web.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        [LoginControl]
        public ActionResult Production()
        {
            #region Export 
            ExportVM vm = new ExportVM();
            vm.ViewName = "V_RPT_PRODUCTION";
            vm.SetExportColumns();
            vm.ExportType = ExportType.Excel;
            #endregion
            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            var tableColumnHeaderList = new GenericRepository<TableColumnHeader>().FindBy($"TABLE_NAME='{vm.ViewName}'", orderby: "ORDER_NUM ASC");
            ViewBag.Columns = tableColumnHeaderList;

            var companyList = new GenericRepository<Company>().FindBy();
            ViewBag.CompanyList = companyList;

            var productList = new GenericRepository<Product>().FindBy();
            ViewBag.ProductList = productList;

            var packageList = new GenericRepository<Package>().FindBy();
            ViewBag.PackageList = packageList;

            var policyGroupList = new GenericRepository<PolicyGroup>().FindBy();
            ViewBag.PolicyGroupList = policyGroupList;

            var policyTypeList = LookupHelper.GetLookupData(LookupTypes.Policy, showAll: true);
            ViewBag.PolicyTypeList = policyTypeList;

            var notes = LookupHelper.GetLookupData(LookupTypes.Note, showAll: true);
            ViewBag.notes = notes;

            ViewBag.PolicyStatusList = LookupHelper.GetLookupData(LookupTypes.PolicyStatus, showAll: true);
            ViewBag.EndorsementTypeList = LookupHelper.GetLookupData(LookupTypes.Endorsement, showAll: true);
            ViewBag.ContactTypeList = LookupHelper.GetLookupData(LookupTypes.Contact, showAll: true);
            ViewBag.InsuredStatusList = LookupHelper.GetLookupData(LookupTypes.Status, showAll: true);
            ViewBag.AgencyTypeList = LookupHelper.GetLookupData(LookupTypes.Agency, showAll: true);
            ViewBag.RenewalGuaranteeTypeList = LookupHelper.GetLookupData(LookupTypes.RenewalGuarantee, showAll: true);
            ViewBag.VIPTypeList = LookupHelper.GetLookupData(LookupTypes.VIP, showAll: true);

            return View(vm);
        }

        [LoginControl]
        public ActionResult ClaimDetails()
        {
            #region Export 
            ExportVM vm = new ExportVM();
            vm.ViewName = "V_RPT_CLAIM_DETAILS";
            vm.SetExportColumns();
            vm.ExportType = ExportType.Excel;
            #endregion
            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            ViewBag.claimListProviderTypes = LookupHelper.GetLookupData(LookupTypes.Provider);

            ViewBag.PolicyTypeList = LookupHelper.GetLookupData(LookupTypes.Policy, showAll: true);

            ViewBag.ProductTypeList = LookupHelper.GetLookupData(LookupTypes.Product, showAll: true);

            ViewBag.CoverageTypeList = LookupHelper.GetLookupData(LookupTypes.Coverage, showAll: true);

            ViewBag.ClaimStatusList = LookupHelper.GetLookupData(LookupTypes.ClaimStatus, showAll: true);

            ViewBag.ClaimSourceList = LookupHelper.GetLookupData(LookupTypes.ClaimSource, showAll: true);

            ViewBag.ClaimPaymentTypeList = LookupHelper.GetLookupData(LookupTypes.ClaimPaymentMethod, showAll: true);

            var companyList = new CompanyRepository().FindBy();
            ViewBag.CompanyList = companyList;

            var productList = new GenericRepository<Product>().FindBy();
            ViewBag.ProductList = productList;

            var subproductList = new GenericRepository<Subproduct>().FindBy();
            ViewBag.SubProductList = subproductList;

            var packageList = new GenericRepository<Package>().FindBy();
            ViewBag.PackageList = packageList;

            var providerGroup = new GenericRepository<ProviderGroup>().FindBy();
            ViewBag.ProviderGroupList = providerGroup;

            var policyGroupList = new GenericRepository<PolicyGroup>().FindBy();
            ViewBag.PolicyGroupList = policyGroupList;

            var ProviderList = new GenericRepository<V_Provider>().FindBy(orderby: "PROVIDER_NAME");
            ViewBag.claimListProviders = ProviderList ?? new List<V_Provider>();

            return View(vm);
        }

        [LoginControl]
        public ActionResult ClaimPremium()
        {
            #region Export 
            ExportVM vm = new ExportVM();
            vm.ViewName = "V_RPT_CLAIM_PREMIUM";
            vm.SetExportColumns();
            vm.ExportType = ExportType.Excel;
            #endregion
            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            var companyList = new CompanyRepository().FindBy();
            ViewBag.CompanyList = companyList;

            var policyGroupList = new PolicyGroupRepository().FindBy();
            ViewBag.PolicyGroupList = policyGroupList;
            var policyType = LookupHelper.GetLookupData(LookupTypes.Policy);
            ViewBag.PolicyType = policyType;

            var policyStatus = LookupHelper.GetLookupData(LookupTypes.PolicyStatus);
            ViewBag.PolicyStatusList = policyStatus;

            var productType = LookupHelper.GetLookupData(LookupTypes.Product);
            ViewBag.productType = productType;
            return View(vm);
        }

        [LoginControl]
        public ActionResult ProviderShare()
        {
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetRptProduction(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];
            ViewResultDto<List<V_RptProduction>> claimlist = new ViewResultDto<List<V_RptProduction>>();
            claimlist.Data = new List<V_RptProduction>();
            ExportVM vm = new ExportVM();
            vm.ViewName = "V_RPT_PRODUCTION";

            if (isFilter == "1")
            {
                var notes = form["notes"];

                String whereConditition = !form["policyCompany[]"].IsNull() ? $" SIRKETKOD IN({form["policyCompany[]"]}) AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["product"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " ") + $" PRODUCT_ID = {long.Parse(form["product"])} AND" : "";
                whereConditition += form["package[]"].IsInt() ? $" PACKAGE_ID IN({form["package[]"]}) AND" : "";
                if (notes == "1")
                {
                    whereConditition += form["notes"].IsInt() ? $" BASIM_NOTLAR IS NOT NULL AND" : "";
                }
                else if (notes=="0")
                {
                    whereConditition += form["notes"].IsInt() ? $" OZEL_NOTLAR IS NOT NULL AND" : "";

                }
                else if (notes == "-1")
                {
                    whereConditition += form["notes"].IsInt() ? $" SIGUW IS NOT NULL AND" : "";

                }


                whereConditition += form["insuredFamilyNo"].IsInt() ? $" AILE_NO ={(form["insuredFamilyNo"])} AND" : "";
                whereConditition += form["AgencyNo"].IsInt() ? $" ACENTENO ='{(form["AgencyNo"])}' AND" : "";
                whereConditition += !form["AgencyName"].IsNull() ? $" ACENTEADI LIKE '%{(form["AgencyName"])}%' AND" : "";
                whereConditition += form["policyStatus"].IsInt() ? $" POLICY_STATUS	 = '{(form["policyStatus"])}' AND" : "";
                whereConditition += (form["policyGroup"].IsInt64() && long.Parse(form["policyGroup"]) > 0) ? $" HOLDINGID={form["policyGroup"]} AND" : "";
                whereConditition += form["policyType"].IsInt() ? $" POLICY_TYPE ='{(form["policyType"])}' AND" : "";
                whereConditition += form["endorsementType"].IsInt() ? $" ZEYLTIPI ='{(form["endorsementType"])}' AND" : "";
                whereConditition += form["contactType"].IsInt() ? $" SIGETTIP = '{(form["contactType"])}' AND" : "";
                whereConditition += form["insuredStatus"].IsInt() ? $" INSURED_STATUS = '{(form["insuredStatus"])}' AND" : "";
                whereConditition += form["agencyType"].IsInt() ? $" ACENTETIP = '{(form["agencyType"])}' AND" : "";
                whereConditition += form["renewalGuaranteeType"].IsInt() ? $" RENEWAL_GUARANTEE_TYPE = '{(form["renewalGuaranteeType"])}' AND" : "";
                whereConditition += form["vipType"].IsInt() ? $" VIP_TYPE = '{(form["vipType"])}' AND" : "";
                whereConditition += form["policyNo"].IsInt64() ? $" POLICENO = {(form["policyNo"])} AND" : "";
                whereConditition += form["policyRenewalNo"].IsInt() ? $" YENILEMENO = {(form["policyRenewalNo"])} AND" : "";
                whereConditition += form["endorsementNo"].IsInt() ? $" ZEYLNO = '{(form["endorsementNo"])}' AND" : "";
                whereConditition += form["insuredTCKN"].IsInt64() ? $" (TCKN={form["insuredTCKN"]} OR VERGINO={form["insuredTCKN"]} OR PASAPORTNO='{form["insuredTCKN"]}') AND" : "";


                whereConditition += !form["insuredName"].IsNull() ? $" SIGAD LIKE '%{form["insuredName"]}%' AND" : "";
                whereConditition += !form["insuredSurName"].IsNull() ? $" SIGSOYAD LIKE '%{form["insuredSurName"]}%' AND" : "";
                whereConditition += form["policyStartDateStart"].IsDateTime() ? $" POLBASTAR >= TO_DATE('{ DateTime.Parse(form["policyStartDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyStartDateEnd"].IsDateTime() ? $" POLBASTAR <= TO_DATE('{ DateTime.Parse(form["policyStartDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyEndDateStart"].IsDateTime() ? $" POLBITTAR >= TO_DATE('{ DateTime.Parse(form["policyEndDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyEndDateEnd"].IsDateTime() ? $" POLBITTAR <= TO_DATE('{ DateTime.Parse(form["policyEndDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyIssueDateStart"].IsDateTime() ? $" POLTANTAR >= TO_DATE('{ DateTime.Parse(form["policyIssueDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyIssueDateEnd"].IsDateTime() ? $" POLTANTAR <= TO_DATE('{ DateTime.Parse(form["policyIssueDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["endorsementIssueDateStart"].IsDateTime() ? $" ZEYLTANTAR >= TO_DATE('{ DateTime.Parse(form["endorsementIssueDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["endorsementIssueDateEnd"].IsDateTime() ? $" ZEYLTANTAR <= TO_DATE('{ DateTime.Parse(form["endorsementIssueDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                if (whereConditition != "")
                {
                    claimlist = new GenericRepository<V_RptProduction>().FindByPaged(
                                      conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                      orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},HOLDINGID DESC" : "HOLDINGID DESC",
                                      pageNumber: start,
                                      rowsPerPage: length,
                                      fetchDeletedRows: true,
                                      fetchHistoricRows: true);
                }

                #region Export
                vm.WhereCondition = (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3));
                vm.SetExportColumns();
                #endregion
            }
            return Json(new { data = (claimlist.Data ?? new List<V_RptProduction>()), draw = Request["draw"], recordsTotal = claimlist.TotalItemsCount, recordsFiltered = claimlist.TotalItemsCount, whereConditition = vm.WhereCondition }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetRptClaimPremium(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];
            ViewResultDto<List<V_RptClaimPremium>> claimlist = new ViewResultDto<List<V_RptClaimPremium>>();
            claimlist.Data = new List<V_RptClaimPremium>();
            ExportVM vm = new ExportVM();
            if (isFilter == "1")
            {
                String whereConditition = (form["company"].IsInt64() && long.Parse(form["company"]) > 0) ? $" SIRKETKOD={ long.Parse(form["company"])} AND" : "";
                whereConditition += !form["product[]"].IsNull()? $" PRODUCT_ID IN ({ form["product[]"]}) AND" : "";
                whereConditition += !form["AgencyNo"].IsNull() ? $" ACENTENO= '{form["AgencyNo"]}' AND" : "";
                whereConditition += !form["policyNo"].IsNull() ? $" POLICENO= '{ form["policyNo"]}' AND" : "";
                whereConditition += !form["PolicyType"].IsNull() ? $" POLICETIPI= '{ form["PolicyType"]}' AND" : "";
                whereConditition += !form["policyStatus"].IsNull() ? $" POLICEDURUMU= '{ form["policyStatus"]}' AND" : "";
                whereConditition += !form["productType[]"].IsNull() ? $" PRODUCT_TYPE IN ({form["productType[]"]}) AND" : "";
                whereConditition += !form["insuredName"].IsNull() ? $" SIGORTALI LIKE '%{form["insuredName"]}%' AND" : "";
                whereConditition += form["Tckn"].IsInt64() ? $" TCKN = {form["Tckn"]} AND" : "";
                whereConditition += form["issueDate"].IsDateTime() ? $" TANTAR = TO_DATE('{ DateTime.Parse(form["issueDate"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["startDate"].IsDateTime() ? $" BASTAR >= TO_DATE('{ DateTime.Parse(form["startDate"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["endDate"].IsDateTime() ? $" BITTAR <= TO_DATE('{ DateTime.Parse(form["endDate"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";

                whereConditition += (form["policyGroup"].IsInt64() && long.Parse(form["policyGroup"]) > 0) ? $"  HOLDINGID={long.Parse(form["policyGroup"])} AND" : "";
                if (whereConditition != "")
                {
                    claimlist = new GenericRepository<V_RptClaimPremium>().FindByPaged(
                                        conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                        orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},HOLDINGID DESC" : "HOLDINGID DESC",
                                        pageNumber: start,
                                        rowsPerPage: length,
                                        fetchDeletedRows: true,
                                        fetchHistoricRows: true);
                }

                #region Export
                vm.WhereCondition = (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3));
                vm.SetExportColumns();
                #endregion
            }
            return Json(new { data = (claimlist.Data ?? new List<V_RptClaimPremium>()), draw = Request["draw"], recordsTotal = claimlist.TotalItemsCount, recordsFiltered = claimlist.TotalItemsCount, whereConditition = vm.WhereCondition }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetRptClaimDetails(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];

            ViewResultDto<List<V_RptClaimDetails>> claimlist = new ViewResultDto<List<V_RptClaimDetails>>();
            claimlist.Data = new List<V_RptClaimDetails>();
            ExportVM vm = new ExportVM();
            if (isFilter == "1")
            {
                String whereConditition = !form["Company[]"].IsNull() ? $" SIRKETKOD IN ({ form["Company[]"]}) AND" : "";
                whereConditition += !form["product[]"].IsNull() ? $" PRODUCT_ID IN ({ form["product[]"]}) AND" : "";
                whereConditition += !form["subproduct[]"].IsNull() ? $" SUBPRODUCT_ID IN ({ form["subproduct[]"]}) AND" : "";
                whereConditition += !form["package[]"].IsNull() ? $" PACKAGE_ID IN ({ form["package[]"]}) AND" : "";
                whereConditition += !form["claimListProvider[]"].IsNull() ? $" KURUMKODU IN ({ form["claimListProvider[]"]}) AND" : "";
                whereConditition += (form["providerGroup"].IsInt64() && long.Parse(form["providerGroup"]) > 0) ? $" KURUMGRUP={ long.Parse(form["providerGroup"])} AND" : "";
                whereConditition += !form["providerNo"].IsNull() ? $" KURUMKODU= '{form["providerNo"]}' AND" : "";
                whereConditition += !form["AgencyName"].IsNull() ? $" ACENTEADI LIKE '%{form["AgencyName"]}%' AND" : "";
                whereConditition += !form["productType[]"].IsNull() ? $" URUNTIP IN ({form["productType[]"]}) AND" : "";
                whereConditition += (form["policyType"].IsInt() && int.Parse(form["policyType"]) > 0) ? $" POLICY_TYPE='{form["policyType"]}' AND" : "";
                whereConditition += (form["coverageType"].IsInt() && int.Parse(form["coverageType"]) > 0) ? $" TEMINATTIPI='{form["coverageType"]}' AND" : "";
                whereConditition += !form["claimStatus[]"].IsNull() ? $" CLAIM_STATUS IN ({form["claimStatus[]"]}) AND" : $" CLAIM_STATUS NOT IN ('{((int)ClaimStatus.GIRIS).ToString()}','{((int)ClaimStatus.IPTAL).ToString()}','{((int)ClaimStatus.SILINDI).ToString()}') AND";
                whereConditition += (form["claimSource[]"].IsInt() && int.Parse(form["claimSource[]"]) > 0) ? $" CLAIM_SOURCE_TYPE= '{form["claimSource[]"]}' AND" : "";
                whereConditition += (form["claimPaymentType"].IsInt() && int.Parse(form["claimPaymentType"]) > 0) ? $" CLAIM_PAYMENT_TYPE= '{form["claimPaymentType"]}' AND" : "";
                whereConditition += !form["payrollId"].IsNull() ? $" ICMALNO= '{form["payrollId"]}' AND" : "";
                whereConditition += !form["oldPayrollId"].IsNull() ? $" OLD_PAYROLL_ID= '{form["oldPayrollId"]}' AND" : "";
                whereConditition += !form["extProviderNo"].IsNull() ? $" KURUMICMALNO= '{form["extProviderNo"]}' AND" : "";

                whereConditition += !form["jobType"].IsNull() ? $" ISTIPI= '{form["jobType"]}' AND" : "";
                whereConditition += !form["AgencyNo"].IsNull() ? $" ACENTENO= '{form["AgencyNo"]}' AND" : "";
                whereConditition += !form["policyNo"].IsNull() ? $" POLICENO= '{form["policyNo"]}' AND" : "";
                whereConditition += form["insuredTCKN"].IsInt64() ? $" TCKN= {form["insuredTCKN"]} AND" : "";
                whereConditition += !form["insuredName"].IsNull() ? $" SIGORTALI_AD= '{form["insuredName"]}' AND" : "";
                whereConditition += !form["insuredSurName"].IsNull() ? $" SIGORTALI_SOYAD= '{form["insuredSurName"]}' AND" : "";
                whereConditition += form["policyStartDateStart"].IsDateTime() ? $" POLBASTAR >= TO_DATE('{ DateTime.Parse(form["policyStartDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyStartDateEnd"].IsDateTime() ? $" POLBASTAR <= TO_DATE('{ DateTime.Parse(form["policyStartDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["claimListClaimDateStart"].IsDateTime() ? $" HASARTAR >= TO_DATE('{ DateTime.Parse(form["claimListClaimDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["claimListClaimDateEnd"].IsDateTime() ? $" HASARTAR <= TO_DATE('{ DateTime.Parse(form["claimListClaimDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyIssueDateStart"].IsDateTime() ? $" POLTANTAR >= TO_DATE('{ DateTime.Parse(form["policyIssueDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyIssueDateEnd"].IsDateTime() ? $" POLTANTAR <= TO_DATE('{ DateTime.Parse(form["policyIssueDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["paymentDateStart"].IsDateTime() ? $" ODEMETARIHI >= TO_DATE('{ DateTime.Parse(form["paymentDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["paymentDateEnd"].IsDateTime() ? $" ODEMETARIHI <= TO_DATE('{ DateTime.Parse(form["paymentDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyEntranceDateStart"].IsDateTime() ? $" POLGRSTAR >= TO_DATE('{ DateTime.Parse(form["policyEntranceDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyEntranceDateEnd"].IsDateTime() ? $" POLGRSTAR <= TO_DATE('{ DateTime.Parse(form["policyEntranceDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["claimStatusDateStart"].IsDateTime() ? $" POLGRSTAR >= TO_DATE('{ DateTime.Parse(form["claimStatusDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["claimStatusDateEnd"].IsDateTime() ? $" POLGRSTAR <= TO_DATE('{ DateTime.Parse(form["claimStatusDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";

                if (whereConditition != "")
                {
                    claimlist = new GenericRepository<V_RptClaimDetails>().FindByPaged(
                                  conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                  orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},MASTERID DESC" : "MASTERID DESC",
                                  pageNumber: start,
                                  rowsPerPage: length,
                                  fetchDeletedRows: true,
                                  fetchHistoricRows: true);
                }

                #region Export
                vm.WhereCondition = (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3));
                vm.SetExportColumns();
                #endregion
            }
            return Json(new { data = (claimlist.Data ?? new List<V_RptClaimDetails>()), draw = Request["draw"], recordsTotal = claimlist.TotalItemsCount, recordsFiltered = claimlist.TotalItemsCount, whereConditition = vm.WhereCondition }, JsonRequestBehavior.AllowGet);
        }

        [LoginControl]
        public ActionResult GetPayrollReport(FormCollection form)
        {
            


            #region Export 
            ExportVM vm = new ExportVM();
            vm.ViewName = "V_RPT_PAYROLL";
            vm.SetExportColumns();
            vm.ExportType = ExportType.Excel;
            #endregion
            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            var companyList = new CompanyRepository().FindBy();
            ViewBag.CompanyList = companyList;
            ViewBag.ClaimStatusList = LookupHelper.GetLookupData(LookupTypes.ClaimStatus, showAll: true);
            var PayrollCategory = LookupHelper.GetLookupData(LookupTypes.PayrollCategory, showAll: true);
            ViewBag.PayrollCategory = PayrollCategory;
            var PayrollType = LookupHelper.GetLookupData(LookupTypes.Payroll, showAll: true);
            ViewBag.PayrollType = PayrollType;

            var payrollStatus = LookupHelper.GetLookupData(LookupTypes.PayrollStatus, showAll: true);
            ViewBag.payrollStatus = payrollStatus;

            //var ProviderList = LookupHelper.GetLookupData(LookupTypes.Provider, showAll: true);
            //ViewBag.ProviderList = ProviderList;


            var ProviderList = new GenericRepository<V_Provider>().FindBy(orderby: "PROVIDER_ID");
            ViewBag.ProviderList = ProviderList ?? new List<V_Provider>();

            var providerGroupList = new GenericRepository<ProviderGroup>().FindBy();
            ViewBag.providerGroupList = providerGroupList ?? new List<ProviderGroup>();

            ViewBag.claimListProviderTypes = LookupHelper.GetLookupData(LookupTypes.Provider);
            return View(vm);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetPayrollReportList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];

            ViewResultDto<List<V_RptPayroll>> claimlist = new ViewResultDto<List<V_RptPayroll>>();
            claimlist.Data = new List<V_RptPayroll>();
            ExportVM vm = new ExportVM();
            if (isFilter == "1")
            {
                String whereConditition = (form["providerPayrollNo"].IsInt64() && long.Parse(form["providerPayrollNo"]) > 0) ? $" EXT_PROVIDER_NO={ long.Parse(form["providerPayrollNo"])} AND" : "";
                whereConditition += (form["company"].IsInt64() && long.Parse(form["company"]) > 0) ? $" COMPANY_ID={ long.Parse(form["company"])} AND" : "";
                whereConditition += (form["PayrollNo"].IsInt64() && long.Parse(form["PayrollNo"]) > 0) ? $" PAYROLL_ID={ long.Parse(form["PayrollNo"])} AND" : "";
                whereConditition += (form["providerGroup"].IsInt64() && long.Parse(form["providerGroup"]) > 0) ? $" PROVIDER_GROUP_ID={ long.Parse(form["providerGroup"])} AND" : "";
                whereConditition += (form["ProviderName"].IsInt64() && long.Parse(form["ProviderName"]) > 0) ? $" PROVIDER_ID={ long.Parse(form["ProviderName"])} AND" : "";
                whereConditition += (form["oldPayrollNo"].IsInt64() && long.Parse(form["oldPayrollNo"]) > 0) ? $" OLD_PAYROLL_ID={ long.Parse(form["oldPayrollNo"])} AND" : "";
                whereConditition += (form["claimStatus"].IsInt64() && long.Parse(form["claimStatus"]) > 0) ? $" CLAIM_STATUS={ long.Parse(form["claimStatus"])} AND" : "";
                whereConditition += (form["claimCategory"].IsInt64() && long.Parse(form["claimCategory"]) > 0) ? $" PAYROLL_CATEGORY={ long.Parse(form["claimCategory"])} AND" : "";
                var verificationDateControl = form["payrollChangeVerificationDate"];
                if (!String.IsNullOrEmpty(verificationDateControl))
                {
                    DateTime verificationDate = DateTime.Parse(form["payrollChangeVerificationDate"]);
                    DateTime verificationDateNextDay = verificationDate.AddDays(1);

                    whereConditition += form["payrollChangeVerificationDate"].IsDateTime() ? $" VERIFICATION_DATE BETWEEN TO_DATE('{ verificationDate }','DD.MM.YYYY HH24:MI:SS') AND TO_DATE('{ verificationDateNextDay }','DD.MM.YYYY HH24:MI:SS') AND" : "";
                }

                whereConditition += (form["claimType"].IsInt64() && long.Parse(form["claimType"]) > 0) ? $" PAYROLL_TYPE={ long.Parse(form["claimType"])} AND" : "";
                whereConditition += (form["policyGroup"].IsInt64() && long.Parse(form["policyGroup"]) > 0) ? $" HOLDING_ID={ long.Parse(form["policyGroup"])} AND" : "";

                if (whereConditition != "")

                {
                    claimlist = new GenericRepository<V_RptPayroll>().FindByPaged(
                                         conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                         orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},PAYROLL_ID DESC" : "PAYROLL_ID DESC",
                                         pageNumber: start,
                                         rowsPerPage: length,
                                         fetchDeletedRows: true,
                                         fetchHistoricRows: true);
                }

                #region Export
                vm.WhereCondition = (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3));
                vm.SetExportColumns();
                #endregion
            }
            return Json(new { data = (claimlist.Data ?? new List<V_RptPayroll>()), draw = Request["draw"], recordsTotal = claimlist.TotalItemsCount, recordsFiltered = claimlist.TotalItemsCount, whereConditition = vm.WhereCondition }, JsonRequestBehavior.AllowGet);
        }

        [LoginControl]
        public ActionResult PayrollDetail(FormCollection form)
        {
            
            #region Export 
            ExportVM vm = new ExportVM();
            vm.ViewName = "V_RPT_PAYROLL";
            vm.SetExportColumns();
            vm.ExportType = ExportType.Excel;
            #endregion
            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            var companyList = new CompanyRepository().FindBy();
            ViewBag.CompanyList = companyList;
            ViewBag.ClaimStatusList = LookupHelper.GetLookupData(LookupTypes.ClaimStatus, showAll: true);
            var PayrollCategory = LookupHelper.GetLookupData(LookupTypes.PayrollCategory, showAll: true);
            ViewBag.PayrollCategory = PayrollCategory;
            var PayrollType = LookupHelper.GetLookupData(LookupTypes.Payroll, showAll: true);
            ViewBag.PayrollType = PayrollType;

            var payrollStatus = LookupHelper.GetLookupData(LookupTypes.PayrollStatus, showAll: true);
            ViewBag.payrollStatus = payrollStatus;
            var providerGroup = new GenericRepository<ProviderGroup>().FindBy();
            ViewBag.ProviderGroup = providerGroup;

            //var ProviderList = LookupHelper.GetLookupData(LookupTypes.Provider, showAll: true);
            //ViewBag.ProviderList = ProviderList;


            var ProviderList = new GenericRepository<V_Provider>().FindBy(orderby: "PROVIDER_ID");
            ViewBag.ProviderList = ProviderList ?? new List<V_Provider>();
            ViewBag.claimListProviderTypes = LookupHelper.GetLookupData(LookupTypes.Provider);
            return View(vm);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetPayrollDetailList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];

            ViewResultDto<List<V_RptPayroll>> claimlist = new ViewResultDto<List<V_RptPayroll>>();
            claimlist.Data = new List<V_RptPayroll>();
            ExportVM vm = new ExportVM();
            if (isFilter == "1")
            {
                String whereConditition = (form["providerPayrollNo"].IsInt64() && long.Parse(form["providerPayrollNo"]) > 0) ? $" EXT_PROVIDER_NO={ long.Parse(form["providerPayrollNo"])} AND" : "";
                whereConditition += (form["CompanyPayrollNo"].IsInt64() && long.Parse(form["CompanyPayrollNo"]) > 0) ? $" COMPANY_PAYROLL_ID={ long.Parse(form["CompanyPayrollNo"])} AND" : "";
                whereConditition += (form["company"].IsInt64() && long.Parse(form["company"]) > 0) ? $" COMPANY_ID={ long.Parse(form["company"])} AND" : "";
                whereConditition += (form["PayrollNo"].IsInt64() && long.Parse(form["PayrollNo"]) > 0) ? $" PAYROLL_ID={ long.Parse(form["PayrollNo"])} AND" : "";
                whereConditition += (form["providerNo"].IsInt64() && long.Parse(form["providerNo"]) > 0) ? $" PROVIDER_ID={ long.Parse(form["providerNo"])} AND" : "";

                whereConditition += (form["ProviderName"].IsInt() && long.Parse(form["ProviderName"]) > 0) ? $" PROVIDER_NAME LIKE '%{(form["ProviderName"])}%' AND" : "";

                whereConditition += !(form["providerGroup[]"].IsNull()) ? $" PROVIDER_GROUP_ID IN ({form["providerGroup[]"]}) AND" : "";

                whereConditition += (form["ProviderTax"].IsInt64() && long.Parse(form["ProviderTax"]) > 0) ? $" TAX_NUMBER={ long.Parse(form["ProviderTax"])} AND" : "";

                whereConditition += (form["oldPayrollNo"].IsInt64() && long.Parse(form["oldPayrollNo"]) > 0) ? $" OLD_PAYROLL_ID={ long.Parse(form["oldPayrollNo"])} AND" : "";
                whereConditition += (form["claimStatus"].IsInt64() && long.Parse(form["claimStatus"]) > 0) ? $" CLAIM_STATUS={ long.Parse(form["claimStatus"])} AND" : "";
                whereConditition += (form["claimCategory"].IsInt64() && long.Parse(form["claimCategory"]) > 0) ? $" PAYROLL_CATEGORY={ long.Parse(form["claimCategory"])} AND" : "";


                whereConditition += form["payrollDate"].IsDateTime() ? $" PAYROLL_DATE BETWEEN TO_DATE('{form["payrollDate"]}','DD.MM.YYYY HH24:MI:SS') AND TO_DATE('{form["payrollDate"]}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["payrollDueDate"].IsDateTime() ? $" DUE_DATE BETWEEN TO_DATE('{form["payrollDueDate"]}','DD.MM.YYYY HH24:MI:SS') AND TO_DATE('{form["payrollDueDate"]}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["paymentDate"].IsDateTime() ? $" PAYMENT_DATE BETWEEN TO_DATE('{form["paymentDate"]}','DD.MM.YYYY HH24:MI:SS') AND TO_DATE('{form["paymentDate"]}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["payrollChangeVerificationDate"].IsDateTime() ? $" VERIFICATION_DATE BETWEEN TO_DATE('{form["payrollChangeVerificationDate"]}','DD.MM.YYYY HH24:MI:SS') AND TO_DATE('{form["payrollChangeVerificationDate"]}','DD.MM.YYYY HH24:MI:SS') AND" : "";

                var verificationDateControl = form["payrollChangeVerificationDate"];
                if (!String.IsNullOrEmpty(verificationDateControl))
                {
                    DateTime verificationDate = DateTime.Parse(form["payrollChangeVerificationDate"]);
                    DateTime verificationDateNextDay = verificationDate.AddDays(1);

                    whereConditition += form["payrollChangeVerificationDate"].IsDateTime() ? $" VERIFICATION_DATE BETWEEN TO_DATE('{ verificationDate }','DD.MM.YYYY HH24:MI:SS') AND TO_DATE('{ verificationDateNextDay }','DD.MM.YYYY HH24:MI:SS') AND" : "";
                }

                whereConditition += (form["claimType"].IsInt64() && long.Parse(form["claimType"]) > 0) ? $" PAYROLL_TYPE={ long.Parse(form["claimType"])} AND" : "";
                whereConditition += (form["policyGroup"].IsInt64() && long.Parse(form["policyGroup"]) > 0) ? $" HOLDING_ID={ long.Parse(form["policyGroup"])} AND" : "";

                if (whereConditition != "")

                {
                    claimlist = new GenericRepository<V_RptPayroll>().FindByPaged(
                                         conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                         orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},PAYROLL_ID DESC" : "PAYROLL_ID DESC",
                                         pageNumber: start,
                                         rowsPerPage: length,
                                         fetchDeletedRows: true,
                                         fetchHistoricRows: true);
                }

                #region Export
                vm.WhereCondition = (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3));
                vm.SetExportColumns();
                #endregion
            }
            return Json(new { data = (claimlist.Data ?? new List<V_RptPayroll>()), draw = Request["draw"], recordsTotal = claimlist.TotalItemsCount, recordsFiltered = claimlist.TotalItemsCount, whereConditition = vm.WhereCondition }, JsonRequestBehavior.AllowGet);
        }

        [LoginControl]
        public ActionResult ActiveInsured(FormCollection form)
        {

            #region Export 
            ExportVM vm = new ExportVM();
            vm.ViewName = "V_RPT_PRODUCTION";
            vm.SetExportColumns();
            vm.ExportType = ExportType.Excel;
            #endregion
            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            var tableColumnHeaderList = new GenericRepository<TableColumnHeader>().FindBy($"TABLE_NAME='{vm.ViewName}'", orderby: "ORDER_NUM ASC");
            ViewBag.Columns = tableColumnHeaderList;

            var companyList = new GenericRepository<Company>().FindBy();
            ViewBag.CompanyList = companyList;

            var productList = new GenericRepository<Product>().FindBy();
            ViewBag.ProductList = productList;

            var packageList = new GenericRepository<Package>().FindBy();
            ViewBag.PackageList = packageList;

            var policyGroupList = new GenericRepository<PolicyGroup>().FindBy();
            ViewBag.PolicyGroupList = policyGroupList;

            var policyTypeList = LookupHelper.GetLookupData(LookupTypes.Policy, showAll: true);
            ViewBag.PolicyTypeList = policyTypeList;

            ViewBag.PolicyStatusList = LookupHelper.GetLookupData(LookupTypes.PolicyStatus, showAll: true);
            ViewBag.EndorsementTypeList = LookupHelper.GetLookupData(LookupTypes.Endorsement, showAll: true);
            ViewBag.ContactTypeList = LookupHelper.GetLookupData(LookupTypes.Contact, showAll: true);
            ViewBag.InsuredStatusList = LookupHelper.GetLookupData(LookupTypes.Status, showAll: true);
            ViewBag.AgencyTypeList = LookupHelper.GetLookupData(LookupTypes.Agency, showAll: true);
            ViewBag.RenewalGuaranteeTypeList = LookupHelper.GetLookupData(LookupTypes.RenewalGuarantee, showAll: true);
            ViewBag.VIPTypeList = LookupHelper.GetLookupData(LookupTypes.VIP, showAll: true);

            return View(vm);

        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetActiveInsuredList (FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];
            ViewResultDto<List<V_RptProduction>> claimlist = new ViewResultDto<List<V_RptProduction>>();
            claimlist.Data = new List<V_RptProduction>();
            ExportVM vm = new ExportVM();

            if (isFilter == "1")
            {
                String whereConditition = !form["policyCompany[]"].IsNull() ? $" SIRKETKOD IN({form["policyCompany[]"]}) AND" : "";
                whereConditition += !form["product[]"].IsNull() ? $" PRODUCT_ID IN({form["product[]"]}) AND" : "";
                whereConditition += !form["package[]"].IsNull() ? $" PACKAGE_ID IN({form["package[]"]}) AND" : "";
                whereConditition += form["insuredFamilyNo"].IsInt() ? $" AILE_NO ={(form["insuredFamilyNo"])} AND" : "";
                whereConditition += form["AgencyNo"].IsInt() ? $" ACENTENO ='{(form["AgencyNo"])}' AND" : "";
                whereConditition += !form["AgencyName"].IsNull() ? $" ACENTEADI LIKE '%{(form["AgencyName"])}%' AND" : "";
                whereConditition += form["policyStatus"].IsInt() ? $" POLICY_STATUS = '{(form["policyStatus"])}' AND" : "";
                whereConditition += (form["policyGroup"].IsInt64() && long.Parse(form["policyGroup"]) > 0) ? $" HOLDINGID={form["policyGroup"]} AND" : "";
                whereConditition += form["policyType"].IsInt() ? $" POLICY_TYPE ='{(form["policyType"])}' AND" : "";
                whereConditition += form["endorsementType"].IsInt() ? $" ZEYLTIPI ='{(form["endorsementType"])}' AND" : "";
                whereConditition += form["contactType"].IsInt() ? $" SIGETTIP = '{(form["contactType"])}' AND" : "";
                whereConditition += form["insuredStatus"].IsInt() ? $" INSURED_STATUS = '{(form["insuredStatus"])}' AND" : "";
                whereConditition += form["agencyType"].IsInt() ? $" ACENTETIP = '{(form["agencyType"])}' AND" : "";
                whereConditition += form["renewalGuaranteeType"].IsInt() ? $" RENEWAL_GUARANTEE_TYPE = '{(form["renewalGuaranteeType"])}' AND" : "";
                whereConditition += form["vipType"].IsInt() ? $" VIP_TYPE = '{(form["vipType"])}' AND" : "";
                whereConditition += form["policyNo"].IsInt() ? $" POLICENO = {(form["policyNo"])} AND" : "";
                whereConditition += form["policyRenewalNo"].IsInt() ? $" YENILEMENO = {(form["policyRenewalNo"])} AND" : "";
                whereConditition += form["endorsementNo"].IsInt() ? $" ZEYLNO = '{(form["endorsementNo"])}' AND" : "";
                whereConditition += form["insuredTCKN"].IsInt64() ? $" (TCKN={form["insuredTCKN"]} OR VERGINO={form["insuredTCKN"]} OR PASAPORTNO='{form["insuredTCKN"]}') AND" : ""; 
                whereConditition += !form["insuredName"].IsNull() ? $" SIGAD LIKE '%{form["insuredName"]}%' AND" : "";
                whereConditition += !form["insuredSurName"].IsNull() ? $" SIGSOYAD LIKE '%{form["insuredSurName"]}%' AND" : "";
                whereConditition += form["activeInsuredDateStart"].IsDateTime() ? $" ILKSIGORTATAR >= TO_DATE('{ DateTime.Parse(form["activeInsuredDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["activeInsuredDateEnd"].IsDateTime() ? $" ILKSIGORTATAR <= TO_DATE('{ DateTime.Parse(form["activeInsuredDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyStartDateStart"].IsDateTime() ? $" POLBASTAR >= TO_DATE('{ DateTime.Parse(form["policyStartDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyStartDateEnd"].IsDateTime() ? $" POLBASTAR <= TO_DATE('{ DateTime.Parse(form["policyStartDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyEndDateStart"].IsDateTime() ? $" POLBITTAR >= TO_DATE('{ DateTime.Parse(form["policyEndDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyEndDateEnd"].IsDateTime() ? $" POLBITTAR <= TO_DATE('{ DateTime.Parse(form["policyEndDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyIssueDateStart"].IsDateTime() ? $" POLTANTAR >= TO_DATE('{ DateTime.Parse(form["policyIssueDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyIssueDateEnd"].IsDateTime() ? $" POLTANTAR <= TO_DATE('{ DateTime.Parse(form["policyIssueDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["endorsementIssueDateStart"].IsDateTime() ? $" ZEYLTANTAR >= TO_DATE('{ DateTime.Parse(form["endorsementIssueDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["endorsementIssueDateEnd"].IsDateTime() ? $" ZEYLTANTAR <= TO_DATE('{ DateTime.Parse(form["endorsementIssueDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                if (whereConditition != "")
                {
                    claimlist = new GenericRepository<V_RptProduction>().FindByPaged(
                                      conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                      orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},HOLDINGID DESC" : "HOLDINGID DESC",
                                      pageNumber: start,
                                      rowsPerPage: length,
                                      fetchDeletedRows: true,
                                      fetchHistoricRows: true);
                }

                #region Export
                vm.WhereCondition = (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3));
                vm.SetExportColumns();
                #endregion
            }
            return Json(new { data = (claimlist.Data ?? new List<V_RptProduction>()), draw = Request["draw"], recordsTotal = claimlist.TotalItemsCount, recordsFiltered = claimlist.TotalItemsCount, whereConditition = vm.WhereCondition }, JsonRequestBehavior.AllowGet);
        }
        [LoginControl]
        public ActionResult ClaimDetailsProvision()
        {
            //ClaimDetails Aynısı hiç bir değişiklik yapılmadı.....
            #region Export 
            ExportVM vm = new ExportVM();
            vm.ViewName = "V_RPT_CLAIM_DETAILS";
            vm.SetExportColumns();
            vm.ExportType = ExportType.Excel;
            #endregion
            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            ViewBag.claimListProviderTypes = LookupHelper.GetLookupData(LookupTypes.Provider);

            ViewBag.PolicyTypeList = LookupHelper.GetLookupData(LookupTypes.Policy, showAll: true);

            ViewBag.ProductTypeList = LookupHelper.GetLookupData(LookupTypes.Product, showAll: true);

            ViewBag.CoverageTypeList = LookupHelper.GetLookupData(LookupTypes.Coverage, showAll: true);

            ViewBag.ClaimStatusList = LookupHelper.GetLookupData(LookupTypes.ClaimStatus, showAll: true);

            ViewBag.ClaimSourceList = LookupHelper.GetLookupData(LookupTypes.ClaimSource, showAll: true);

            ViewBag.ClaimPaymentTypeList = LookupHelper.GetLookupData(LookupTypes.ClaimPaymentMethod, showAll: true);

            var companyList = new CompanyRepository().FindBy();
            ViewBag.CompanyList = companyList;

            var productList = new GenericRepository<Product>().FindBy();
            ViewBag.ProductList = productList;

            var subproductList = new GenericRepository<Subproduct>().FindBy();
            ViewBag.SubProductList = subproductList;

            var packageList = new GenericRepository<Package>().FindBy();
            ViewBag.PackageList = packageList;

            var providerGroup = new GenericRepository<ProviderGroup>().FindBy();
            ViewBag.ProviderGroupList = providerGroup;

            var policyGroupList = new GenericRepository<PolicyGroup>().FindBy();
            ViewBag.PolicyGroupList = policyGroupList;

            var ProviderList = new GenericRepository<V_Provider>().FindBy(orderby: "PROVIDER_NAME");
            ViewBag.claimListProviders = ProviderList ?? new List<V_Provider>();

            return View(vm);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetRptClaimDetailsProvision (FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];

            ViewResultDto<List<V_RptProvisionList>> claimlist = new ViewResultDto<List<V_RptProvisionList>>();
            claimlist.Data = new List<V_RptProvisionList>();
            ExportVM vm = new ExportVM();
            if (isFilter == "1")
            {
                String whereConditition = !form["Company[]"].IsNull() ? $" SIRKETKOD IN ({ form["Company[]"]}) AND" : "";
                whereConditition += !form["product[]"].IsNull() ? $" URUNADI IN ({ form["product[]"]}) AND" : "";
                whereConditition += !form["subproduct[]"].IsNull() ? $" SUBPRODUCT_ID IN ({ form["subproduct[]"]}) AND" : "";
                whereConditition += !form["package[]"].IsNull() ? $" PACKAGE_ID IN ({ form["package[]"]}) AND" : "";
                whereConditition += !form["claimListProvider[]"].IsNull() ? $" KURUMKODU IN ({ form["claimListProvider[]"]}) AND" : "";
                whereConditition += (form["providerGroup"].IsInt64() && long.Parse(form["providerGroup"]) > 0) ? $" KURUMGRUP={ long.Parse(form["providerGroup"])} AND" : "";
                whereConditition += !form["providerNo"].IsNull() ? $" PROVIDER_ID= '{form["providerNo"]}' AND" : "";
                whereConditition += !form["AgencyName"].IsNull() ? $" ACENTEADI LIKE '%{form["AgencyName"]}%' AND" : "";
                whereConditition += !form["productType[]"].IsNull() ? $" URUNTIP IN ({form["productType[]"]}) AND" : "";
                whereConditition += (form["policyType"].IsInt() && int.Parse(form["policyType"]) > 0) ? $" POLICY_TYPE='{form["policyType"]}' AND" : "";
                whereConditition += (form["coverageType"].IsInt() && int.Parse(form["coverageType"]) > 0) ? $" TEMINATTIPI='{form["coverageType"]}' AND" : "";
                whereConditition += !form["claimStatus[]"].IsNull() ? $" CLAIM_STATUS IN ({form["claimStatus[]"]}) AND" : $" CLAIM_STATUS NOT IN (0,1) AND";
                whereConditition += (form["claimSource"].IsInt() && int.Parse(form["claimSource"]) > 0) ? $" CLAIM_SOURCE_TYPE= '{form["claimSource"]}' AND" : "";
                whereConditition += (form["claimPaymentType"].IsInt() && int.Parse(form["claimPaymentType"]) > 0) ? $" CLAIM_PAYMENT_TYPE= '{form["claimPaymentType"]}' AND" : "";
                whereConditition += !form["payrollId"].IsNull() ? $" ICMALNO= '{form["payrollId"]}' AND" : "";
                whereConditition += !form["oldPayrollId"].IsNull() ? $" ESKI_ICMALNO= '{form["oldPayrollId"]}' AND" : "";
                whereConditition += !form["extProviderNo"].IsNull() ? $" KURUMICMALNO= '{form["extProviderNo"]}' AND" : "";

                whereConditition += !form["jobType"].IsNull() ? $" ISTIPI= '{form["jobType"]}' AND" : "";
                whereConditition += !form["AgencyNo"].IsNull() ? $" ACENTENO= '{form["AgencyNo"]}' AND" : "";
                whereConditition += !form["policyNo"].IsNull() ? $" POLICENO= '{form["policyNo"]}' AND" : "";
                whereConditition += form["insuredTCKN"].IsInt64() ? $" TCKN= {form["insuredTCKN"]} AND" : "";
                whereConditition += !form["insuredName"].IsNull() ? $" SIGORTALI_ADI= '{form["insuredName"]}' AND" : "";
                whereConditition += !form["insuredSurName"].IsNull() ? $" SIGORTALI_SOYADI= '{form["insuredSurName"]}' AND" : "";
                whereConditition += form["policyStartDateStart"].IsDateTime() ? $" POLBASTAR >= TO_DATE('{ DateTime.Parse(form["policyStartDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyStartDateEnd"].IsDateTime() ? $" POLBASTAR <= TO_DATE('{ DateTime.Parse(form["policyStartDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["claimListClaimDateStart"].IsDateTime() ? $" HASARTAR >= TO_DATE('{ DateTime.Parse(form["claimListClaimDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["claimListClaimDateEnd"].IsDateTime() ? $" HASARTAR <= TO_DATE('{ DateTime.Parse(form["claimListClaimDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyIssueDateStart"].IsDateTime() ? $" POLTANTAR >= TO_DATE('{ DateTime.Parse(form["policyIssueDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyIssueDateEnd"].IsDateTime() ? $" POLTANTAR <= TO_DATE('{ DateTime.Parse(form["policyIssueDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["paymentDateStart"].IsDateTime() ? $" ODEMETARIHI >= TO_DATE('{ DateTime.Parse(form["paymentDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["paymentDateEnd"].IsDateTime() ? $" ODEMETARIHI <= TO_DATE('{ DateTime.Parse(form["paymentDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyEntranceDateStart"].IsDateTime() ? $" POLGRSTAR >= TO_DATE('{ DateTime.Parse(form["policyEntranceDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyEntranceDateEnd"].IsDateTime() ? $" POLGRSTAR <= TO_DATE('{ DateTime.Parse(form["policyEntranceDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["claimStatusDateStart"].IsDateTime() ? $" HSRSTATTAR >= TO_DATE('{ DateTime.Parse(form["claimStatusDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["claimStatusDateEnd"].IsDateTime() ? $" HSRSTATTAR <= TO_DATE('{ DateTime.Parse(form["claimStatusDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";

                if (whereConditition != "")
                {
                    claimlist = new GenericRepository<V_RptProvisionList>().FindByPaged(
                                  conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                  orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},PRODUCT_ID DESC" : "PRODUCT_ID DESC",
                                  pageNumber: start,
                                  rowsPerPage: length,
                                  fetchDeletedRows: true,
                                  fetchHistoricRows: true);
                }

                #region Export
                vm.WhereCondition = (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3));
                vm.SetExportColumns();
                #endregion
            }
            return Json(new { data = (claimlist.Data ?? new List<V_RptProvisionList>()), draw = Request["draw"], recordsTotal = claimlist.TotalItemsCount, recordsFiltered = claimlist.TotalItemsCount, whereConditition = vm.WhereCondition }, JsonRequestBehavior.AllowGet);
        }
    }

}