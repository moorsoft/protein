﻿using Protein.Common.Dto;
using Protein.Common.Extensions;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Web.ActionFilter.LoginAttr;
using Protein.Web.Models.ExportModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Web.Controllers.CompanyScreen.ClaimCompany
{
    public class ReportCompanyController : Controller
    {
        // GET: ReportCompany
        // GET: Report
        [LoginCompanyControl]
        public ActionResult Production()
        {
            ViewBag.Title = "Üretim Raporu";
            #region Export 
            ExportVM vm = new ExportVM();
            vm.ViewName = "V_RPT_PRODUCTION";
            vm.SetExportColumns();
            vm.ExportType = ExportType.Excel;
            #endregion
            var companyId = HttpContext.Session["CompanyId"];

            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            var companyList = new GenericRepository<Company>().FindBy();
            ViewBag.CompanyList = companyList;

            var productList = new GenericRepository<Product>().FindBy($"COMPANY_ID={companyId}");
            ViewBag.ProductList = productList;

            var packageList = new GenericRepository<V_Package>().FindBy($"COMPANY_ID={companyId}", orderby: "COMPANY_ID");
            ViewBag.PackageList = packageList;

            var policyGroupList = new GenericRepository<PolicyGroup>().FindBy($"COMPANY_ID={companyId}");
            ViewBag.PolicyGroupList = policyGroupList;

            var policyTypeList = LookupHelper.GetLookupData(LookupTypes.Policy, showAll: true);
            ViewBag.PolicyTypeList = policyTypeList;

            ViewBag.PolicyStatusList = LookupHelper.GetLookupData(LookupTypes.PolicyStatus, showAll: true);
            ViewBag.EndorsementTypeList = LookupHelper.GetLookupData(LookupTypes.Endorsement, showAll: true);
            ViewBag.ContactTypeList = LookupHelper.GetLookupData(LookupTypes.Contact, showAll: true);
            ViewBag.InsuredStatusList = LookupHelper.GetLookupData(LookupTypes.Status, showAll: true);
            ViewBag.AgencyTypeList = LookupHelper.GetLookupData(LookupTypes.Agency, showAll: true);
            ViewBag.RenewalGuaranteeTypeList = LookupHelper.GetLookupData(LookupTypes.RenewalGuarantee, showAll: true);
            ViewBag.VIPTypeList = LookupHelper.GetLookupData(LookupTypes.VIP, showAll: true);

            return View(vm);
        }

        [LoginCompanyControl]
        public ActionResult ClaimDetails()
        {
            #region Export 
            ExportVM vm = new ExportVM();
            vm.ViewName = "V_RPT_CLAIM_DETAILS";
            vm.SetExportColumns();
            vm.ExportType = ExportType.Excel;
            #endregion
            ViewBag.Title = "Hasar Detay Raporu";
            var companyId = HttpContext.Session["CompanyId"];

            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            ViewBag.claimListProviderTypes = LookupHelper.GetLookupData(LookupTypes.Provider);

            ViewBag.PolicyTypeList = LookupHelper.GetLookupData(LookupTypes.Policy, showAll: true);

            ViewBag.ProductTypeList = LookupHelper.GetLookupData(LookupTypes.Product, showAll: true);

            ViewBag.CoverageTypeList = LookupHelper.GetLookupData(LookupTypes.Coverage, showAll: true);

            ViewBag.ClaimStatusList = LookupHelper.GetLookupData(LookupTypes.ClaimStatus, showAll: true);

            ViewBag.ClaimSourceList = LookupHelper.GetLookupData(LookupTypes.ClaimSource, showAll: true);

            ViewBag.ClaimPaymentTypeList = LookupHelper.GetLookupData(LookupTypes.ClaimPaymentMethod, showAll: true);

            var companyList = new CompanyRepository().FindBy();
            ViewBag.CompanyList = companyList;

            var productList = new GenericRepository<Product>().FindBy($"COMPANY_ID={companyId}");
            ViewBag.ProductList = productList;

            var subproductList = new GenericRepository<V_SubProduct>().FindBy($"COMPANY_ID={companyId}", orderby: "COMPANY_ID");
            ViewBag.SubProductList = subproductList;

            var packageList = new GenericRepository<Package>().FindBy();
            ViewBag.PackageList = packageList;

            var providerGroup = new GenericRepository<ProviderGroup>().FindBy();
            ViewBag.ProviderGroupList = providerGroup;

            var policyGroupList = new GenericRepository<PolicyGroup>().FindBy($"COMPANY_ID={companyId}");
            ViewBag.PolicyGroupList = policyGroupList;

            var ProviderList = new GenericRepository<V_Provider>().FindBy(orderby: "PROVIDER_NAME");
            ViewBag.claimListProviders = ProviderList ?? new List<V_Provider>();

            return View(vm);
        }

        [LoginCompanyControl]
        public ActionResult ClaimPremium()
        {
            ViewBag.Title = "Hasar Prim Raporu";
            TempData["Alert"] = TempData["Alert"] ?? string.Empty;
            var companyId = HttpContext.Session["CompanyId"];

            #region Export 
            ExportVM vm = new ExportVM();
            vm.ViewName = "V_RPT_CLAIM_PREMIUM";
            vm.SetExportColumns();
            vm.ExportType = ExportType.Excel;
            #endregion

            var productList = new GenericRepository<Product>().FindBy(conditions: $"COMPANY_ID={companyId}");
            ViewBag.ProductList = productList;

            var subproductList = new GenericRepository<V_SubProduct>().FindBy($"COMPANY_ID={companyId}", orderby: "COMPANY_ID");
            ViewBag.SubProductList = subproductList;

            var packageList = new GenericRepository<V_Package>().FindBy($"COMPANY_ID={companyId}", orderby: "COMPANY_ID");
            ViewBag.PackageList = packageList;

            var policyGroupList = new GenericRepository<PolicyGroup>().FindBy($"COMPANY_ID={companyId}");
            ViewBag.PolicyGroupList = policyGroupList;

            ViewBag.ProductTypeList = LookupHelper.GetLookupData(LookupTypes.Product, showAll: true);
            ViewBag.ClaimSourceList = LookupHelper.GetLookupData(LookupTypes.ClaimSource, showAll: true);

            return View(vm);
        }

        [LoginCompanyControl]
        public ActionResult ProviderShare()
        {
            return View();
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetRptProduction(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];
            ViewResultDto<List<V_RptProduction>> claimlist = new ViewResultDto<List<V_RptProduction>>();
            claimlist.Data = new List<V_RptProduction>();
            ExportVM vm = new ExportVM();

            if (isFilter == "1")
            {
                var companyId = HttpContext.Session["CompanyId"];
                String whereConditition = companyId != null ? $" SIRKETKOD ={companyId} AND" : "";
                whereConditition += !form["product[]"].IsNull() ? $" PRODUCT_ID IN({form["product[]"]}) AND" : "";
                whereConditition += !form["package[]"].IsNull() ? $" PACKAGE_ID IN({form["package[]"]}) AND" : "";
                whereConditition += form["insuredFamilyNo"].IsInt() ? $" AILE_NO ={(form["insuredFamilyNo"])} AND" : "";
                whereConditition += form["AgencyNo"].IsInt() ? $" ACENTENO ='{(form["AgencyNo"])}' AND" : "";
                whereConditition += form["policyStatus"].IsInt() ? $" POLICY_STATUS	 = '{(form["policyStatus"])}' AND" : "";
                whereConditition += (form["policyGroup"].IsInt64() && long.Parse(form["policyGroup"]) > 0) ? $" HOLDINGID={form["policyGroup"]} AND" : "";
                whereConditition += form["policyType"].IsInt() ? $" POLICY_TYPE ='{(form["policyType"])}' AND" : "";
                whereConditition += form["endorsementType"].IsInt() ? $" ZEYLTIPI ='{(form["endorsementType"])}' AND" : "";
                whereConditition += form["contactType"].IsInt() ? $" SIGETTIP = '{(form["contactType"])}' AND" : "";
                whereConditition += form["insuredStatus"].IsInt() ? $" INSURED_STATUS = '{(form["insuredStatus"])}' AND" : "";
                whereConditition += form["agencyType"].IsInt() ? $" ACENTETIP = '{(form["agencyType"])}' AND" : "";
                whereConditition += form["renewalGuaranteeType"].IsInt() ? $" RENEWAL_GUARANTEE_TYPE = '{(form["renewalGuaranteeType"])}' AND" : "";
                whereConditition += form["vipType"].IsInt() ? $" VIP_TYPE = '{(form["vipType"])}' AND" : "";
                whereConditition += form["policyNo"].IsInt() ? $" POLICENO = {(form["policyNo"])} AND" : "";
                whereConditition += form["insurerTCKN"].IsInt() ? $" SIGETID = '{(form["insurerTCKN"])}' AND" : "";
                whereConditition += form["policyRenewalNo"].IsInt() ? $" YENILEMENO = {(form["policyRenewalNo"])} AND" : "";
                whereConditition += form["endorsementNo"].IsInt() ? $" ZEYLNO = '{(form["endorsementNo"])}' AND" : "";
                whereConditition += !form["insuredTCKN"].IsNull() ? $" (TCKN={form["insuredTCKN"]} OR VERGINO={form["insuredTCKN"]} OR PASAPORTNO='{form["insuredTCKN"]}') AND" : "";
                whereConditition += !form["insuredName"].IsNull() ? $" SIGAD LIKE '%{form["insuredName"]}%' AND" : "";
                whereConditition += !form["insuredSurName"].IsNull() ? $" SIGSOYAD LIKE '%{form["insuredSurName"]}%' AND" : "";
                whereConditition += form["policyStartDateStart"].IsDateTime() ? $" POLBASTAR >= TO_DATE('{ DateTime.Parse(form["policyStartDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyStartDateEnd"].IsDateTime() ? $" POLBASTAR <= TO_DATE('{ DateTime.Parse(form["policyStartDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyEndDateStart"].IsDateTime() ? $" POLBITTAR >= TO_DATE('{ DateTime.Parse(form["policyEndDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyEndDateEnd"].IsDateTime() ? $" POLBITTAR <= TO_DATE('{ DateTime.Parse(form["policyEndDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyIssueDateStart"].IsDateTime() ? $" POLTANTAR >= TO_DATE('{ DateTime.Parse(form["policyIssueDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyIssueDateEnd"].IsDateTime() ? $" POLTANTAR <= TO_DATE('{ DateTime.Parse(form["policyIssueDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["endorsementIssueDateStart"].IsDateTime() ? $" ZEYLTANTAR >= TO_DATE('{ DateTime.Parse(form["endorsementIssueDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["endorsementIssueDateEnd"].IsDateTime() ? $" ZEYLTANTAR <= TO_DATE('{ DateTime.Parse(form["endorsementIssueDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";

                claimlist = new GenericRepository<V_RptProduction>().FindByPaged(
                                      conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                      orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},HOLDINGID DESC" : "HOLDINGID DESC",
                                      pageNumber: start,
                                      rowsPerPage: length,
                                      fetchDeletedRows: true,
                                      fetchHistoricRows: true);
                #region Export
                vm.WhereCondition = (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3));
                vm.SetExportColumns();
                #endregion
            }
            return Json(new { data = (claimlist.Data ?? new List<V_RptProduction>()), draw = Request["draw"], recordsTotal = claimlist.TotalItemsCount, recordsFiltered = claimlist.TotalItemsCount, whereConditition = vm.WhereCondition }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetRptClaimPremium(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];
            ViewResultDto<List<V_RptClaimPremium>> claimlist = new ViewResultDto<List<V_RptClaimPremium>>();
            claimlist.Data = new List<V_RptClaimPremium>();
            ExportVM vm = new ExportVM();
            if (isFilter == "1")
            {
                var companyId = HttpContext.Session["CompanyId"];
                String whereConditition = companyId != null ? $" SIRKETKOD ={companyId} AND" : "";
                whereConditition += !form["product[]"].IsNull() ? $" PRODUCT_ID IN ({ form["product[]"]}) AND" : "";
                whereConditition += !form["subproduct[]"].IsNull() ? $" SUBPRODUCT_ID IN ({ form["product"]}) AND" : "";
                whereConditition += !form["package[]"].IsNull() ? $" PACKAGE_ID  IN ({(form["package[]"])}) AND" : "";

                whereConditition += !form["policyNo"].IsNull() ? $" POLICENO= { form["policyNo"]} AND" : "";
                whereConditition += !form["insuredNo"].IsNull() ? $" PORTNO= '{ form["insuredNo"]}' AND" : "";
                whereConditition += form["policyType"].IsInt() ? $" POLICETIPI ='{(form["policyType"])}' AND" : "";
                whereConditition += form["policyType"].IsInt() ? $" POLICY_TYPE ='{(form["policyType"])}' AND" : "";

                whereConditition += form["AgencyNo"].IsInt() ? $" ACENTENO ='{(form["AgencyNo"])}' AND" : "";
                whereConditition += form["insuredTCKN"].IsInt() ? $" TCKN ={(form["insuredTCKN"])} AND" : "";
                whereConditition += !form["insuredName"].IsNull() ? $" SIGORTALI LIKE '%{form["insuredName"]}%' AND" : "";
                whereConditition += (form["policyGroup"].IsInt64() && long.Parse(form["policyGroup"]) > 0) ? $"  HOLDINGID={long.Parse(form["policyGroup"])} AND" : "";

                claimlist = new GenericRepository<V_RptClaimPremium>().FindByPaged(
                                      conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                      orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},HOLDINGID DESC" : "HOLDINGID DESC",
                                      pageNumber: start,
                                      rowsPerPage: length,
                                      fetchDeletedRows: true,
                                      fetchHistoricRows: true);
                #region Export
                vm.WhereCondition = (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3));
                vm.SetExportColumns();
                #endregion
            }
            return Json(new { data = (claimlist.Data ?? new List<V_RptClaimPremium>()), draw = Request["draw"], recordsTotal = claimlist.TotalItemsCount, recordsFiltered = claimlist.TotalItemsCount, whereConditition = vm.WhereCondition }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetRptClaimDetails(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];

            ViewResultDto<List<V_RptClaimDetails>> claimlist = new ViewResultDto<List<V_RptClaimDetails>>();
            claimlist.Data = new List<V_RptClaimDetails>();
            ExportVM vm = new ExportVM();
            if (isFilter == "1")
            {
                var companyId = HttpContext.Session["CompanyId"];
                String whereConditition = companyId != null ? $" SIRKETKOD ={companyId} AND" : "";
                whereConditition += !form["product[]"].IsNull() ? $" PRODUCT_ID IN ({ form["product[]"]}) AND" : "";
                whereConditition += !form["subproduct[]"].IsNull() ? $" SUBPRODUCT_ID IN ({form["subproduct[]"]}) AND" : "";
                whereConditition += !form["package"].IsNull() ? $" PACKAGE_ID IN ({form["package[]"]}) AND" : "";
                whereConditition += !form["claimListProvider[]"].IsNull() ? $" KURUMKODU IN ({form["claimListProvider[]"]}) AND" : "";
                whereConditition += (form["providerGroup"].IsInt64() && long.Parse(form["providerGroup"]) > 0) ? $" KURUMGRUP={ long.Parse(form["providerGroup"])} AND" : "";
                whereConditition += (form["policyGroup"].IsInt64() && long.Parse(form["policyGroup"]) > 0) ? $" HOLDING_ID={ long.Parse(form["policyGroup"])} AND" : "";
                whereConditition += (form["claimListClaimNo"].IsInt64() && long.Parse(form["claimListClaimNo"]) > 0) ? $" MASTERID={ long.Parse(form["claimListClaimNo"])} AND" : "";

                whereConditition += (form["productType"].IsInt() && int.Parse(form["productType"]) > 0) ? $" URUNTIP='{form["productType"]}' AND" : "";
                whereConditition += (form["policyType"].IsInt() && int.Parse(form["policyType"]) > 0) ? $" POLICY_TYPE='{form["policyType"]}' AND" : "";
                whereConditition += (form["coverageType"].IsInt() && int.Parse(form["coverageType"]) > 0) ? $" TEMINATTIPI='{form["coverageType"]}' AND" : "";
                whereConditition += (form["claimStatus"].IsInt() && int.Parse(form["claimStatus"]) > 0) ? $" CLAIM_STATUS='{form["claimStatus"]}' AND" : "";
                whereConditition += (form["claimSource"].IsInt() && int.Parse(form["claimSource"]) > 0) ? $" CLAIM_SOURCE_TYPE= '{form["claimSource"]}' AND" : "";
                whereConditition += (form["claimPaymentType"].IsInt() && int.Parse(form["claimPaymentType"]) > 0) ? $" CLAIM_PAYMENT_TYPE= '{form["claimPaymentType"]}' AND" : "";

                whereConditition += !form["ProviderID"].IsNull() ? $" KURUMKODU= {form["ProviderID"]} AND" : "";
                whereConditition += !form["portNo"].IsNull() ? $" PORTNO= '{ form["portNo"]}' AND" : "";
                whereConditition += !form["insurerTCKN"].IsNull() ? $" TCKN= '{ form["insurerTCKN"]}' AND" : "";
                whereConditition += !form["insurerName"].IsNull() ? $" SIGORTALI LIKE '%{ form["insurerName"]}%' AND" : "";

                whereConditition += !form["jobType"].IsNull() ? $" ISTIPI= '{form["jobType"]}' AND" : "";
                whereConditition += !form["AgencyNo"].IsNull() ? $" ACENTENO= '{form["AgencyNo"]}' AND" : "";
                whereConditition += !form["policyNo"].IsNull() ? $" POLICENO= '{form["policyNo"]}' AND" : "";
                whereConditition += !form["insurerName"].IsNull() ? $" SIGETAD= '{form["insurerName"]}' AND" : "";
                whereConditition += !form["insuredTCKN"].IsNull() ? $" TCKN= '{form["insuredTCKN"]}' AND" : "";
                whereConditition += !form["insuredName"].IsNull() ? $" SIGORTALI_AD= '{form["insuredName"]}' AND" : "";
                whereConditition += !form["insuredSurName"].IsNull() ? $" SIGORTALI_SOYAD= '{form["insuredSurName"]}' AND" : "";

                whereConditition += form["policyStartDateStart"].IsDateTime() ? $" POLBASTAR >= TO_DATE('{ DateTime.Parse(form["policyStartDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyStartDateEnd"].IsDateTime() ? $" POLBASTAR <= TO_DATE('{ DateTime.Parse(form["policyStartDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["claimListClaimDateStart"].IsDateTime() ? $" HASARTAR >= TO_DATE('{ DateTime.Parse(form["claimListClaimDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["claimListClaimDateEnd"].IsDateTime() ? $" HASARTAR <= TO_DATE('{ DateTime.Parse(form["claimListClaimDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyIssueDateStart"].IsDateTime() ? $" POLTANTAR >= TO_DATE('{ DateTime.Parse(form["policyIssueDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyIssueDateEnd"].IsDateTime() ? $" POLTANTAR <= TO_DATE('{ DateTime.Parse(form["policyIssueDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["paymentDateStart"].IsDateTime() ? $" ODEMETARIHI >= TO_DATE('{ DateTime.Parse(form["paymentDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["paymentDateEnd"].IsDateTime() ? $" ODEMETARIHI <= TO_DATE('{ DateTime.Parse(form["paymentDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyEntranceDateStart"].IsDateTime() ? $" POLGRSTAR >= TO_DATE('{ DateTime.Parse(form["policyEntranceDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["policyEntranceDateEnd"].IsDateTime() ? $" POLGRSTAR <= TO_DATE('{ DateTime.Parse(form["policyEntranceDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["claimStatusDateStart"].IsDateTime() ? $" POLGRSTAR >= TO_DATE('{ DateTime.Parse(form["claimStatusDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["claimStatusDateEnd"].IsDateTime() ? $" POLGRSTAR <= TO_DATE('{ DateTime.Parse(form["claimStatusDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";

                claimlist = new GenericRepository<V_RptClaimDetails>().FindByPaged(
                                      conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                      orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},MASTERID DESC" : "MASTERID DESC",
                                      pageNumber: start,
                                      rowsPerPage: length,
                                      fetchDeletedRows: true,
                                      fetchHistoricRows: true);
                #region Export
                vm.WhereCondition = (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3));
                vm.SetExportColumns();
                #endregion
            }
            return Json(new { data = (claimlist.Data ?? new List<V_RptClaimDetails>()), draw = Request["draw"], recordsTotal = claimlist.TotalItemsCount, recordsFiltered = claimlist.TotalItemsCount, whereConditition = vm.WhereCondition }, JsonRequestBehavior.AllowGet);
        }
    }
}