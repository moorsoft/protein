﻿using Dapper;
using Protein.Business.Abstract.Print;
using Protein.Business.Print.AcquittanceForm;
using Protein.Business.Print.BillAprovalForm;
using Protein.Business.Print.ClaimMissing;
using Protein.Business.Print.ClaimProviderPaymentList;
using Protein.Business.Print.ClaimProviderReturn;
using Protein.Business.Print.ClaimProviderReturnForm;
using Protein.Business.Print.ClaimRejectForm;
using Protein.Business.Print.FamilyBasedInsuredProfile;
using Protein.Business.Print.FamilyBasedTransferProfile;
using Protein.Business.Print.InsuredProfile;
using Protein.Business.Print.InsuredTransfer;
using Protein.Business.Print.MainPolicy;
using Protein.Business.Print.PolicyCertificate;
using Protein.Business.Print.PolicyInsuredExit;
using Protein.Business.Print.PolicyProfile;
using Protein.Business.Print.PolicyTransfer;
using Protein.Business.Print.ProvisionApproval;
using Protein.Business.Print.ProvisionMissingDocForm;
using Protein.Business.Print.ProvisionReject;
using Protein.Common.Extensions;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.InsuranceCompanies;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Data.Workers;
using Protein.Web.ActionFilter.LoginAttr;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Web.Controllers.CompanyScreen
{
    public class DashboardCompanyController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Anasayfa";
            TempData["Alert"] = TempData["Alert"] ?? string.Empty;
            if (Session["CompanyId"] == null)
                return RedirectToAction("Login");
            LookupHelper.LoadLookupDataFromDbToCache();
            ReasonHelper.LoadReasonDataFromDbToCache();
            ICD10Helper.LoadICD10DataFromDbToCache();
            ICD10StraightHelper.LoadAppsettingsFromDb();
            ProcessListHelper.LoadProcessListDataFromDbToCache();
            return View();
        }
    }
}