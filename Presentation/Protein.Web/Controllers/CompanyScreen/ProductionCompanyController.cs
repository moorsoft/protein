﻿using Dapper;
using Newtonsoft.Json;
using Protein.Business.Abstract.Print;
using Protein.Business.Abstract.Service;
using Protein.Business.Concrete.Responses;
using Protein.Business.Concrete.Services;
using Protein.Business.Enums.Import;
using Protein.Business.Print;
using Protein.Business.Print.FamilyBasedInsuredProfile;
using Protein.Business.Print.FamilyBasedTransferProfile;
using Protein.Business.Print.InsuredProfile;
using Protein.Business.Print.InsuredTransfer;
using Protein.Business.Print.MainPolicy;
using Protein.Business.Print.PolicyCertificate;
using Protein.Business.Print.PolicyEndorsment;
using Protein.Business.Print.PolicyInsuredExit;
using Protein.Business.Print.PolicyProfile;
using Protein.Business.Print.PolicyTransfer;
using Protein.Common.Constants;
using Protein.Common.Dto;
using Protein.Common.Dto.ImportObjects;
using Protein.Common.Extensions;
using Protein.Common.Resources;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Web.ActionFilter.LoginAttr;
using Protein.Web.Models.AgencyModel;
using Protein.Web.Models.ExportModel;
using Protein.Web.Models.ImportModel;
using Protein.Web.Models.ListView;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Web.Controllers.CompanyScreen
{
    public class ProductionCompanyController : Controller
    {
        #region Agency
        [LoginCompanyControl]
        public ActionResult Agency()
        {
            ViewBag.Title = "Acente";
            AgencyVM vm = new AgencyVM();
            vm.ImportVM.ImportObjectType = ImportObjectType.AgencyCompany;

            #region Export
            vm.ExportVM.ViewName = Constants.Views.Agency;
            vm.ExportVM.SetExportColumns();
            vm.ExportVM.ExportType = ExportType.Excel;
            #endregion

            try
            {
                var companyId = HttpContext.Session["CompanyId"].ToString();
                if (!companyId.IsInt64())
                {
                    throw new Exception("Sigorta Şirketi bulunamadı, lütfen tekrar giriş yaparak deneyiniz!");
                }
                vm.ImportVM.SelectedCompanyId = long.Parse(companyId);

                var CompanyList = new CompanyRepository().FindBy();
                ViewBag.CompanyList = CompanyList;
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;
                ViewBag.COMPANY_ID = string.Empty;
                ViewBag.AGENCY_NAME = string.Empty;
                ViewBag.AGENCY_NO = string.Empty;

                var CityList = new CityRepository().FindBy(orderby: "NAME");
                ViewBag.CityList = CityList;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View(vm);
        }

        [LoginCompanyControl]
        public ActionResult AgencyForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                var AgencyTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Agency, showChoose: true);
                ViewBag.AgencyTypeList = AgencyTypeList;

                var CompanyList = new CompanyRepository().FindBy();
                ViewBag.CompanyList = CompanyList;

                var CityList = new CityRepository().FindBy(orderby: "CODE");
                ViewBag.CityList = CityList;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.CountyList = null;
                ViewBag.isEdit = false;

                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Acente Görüntüle";
                        if (id > 0)
                        {
                            var agency = new GenericRepository<V_Agency>().FindBy("AGENCY_ID=:id", orderby: "AGENCY_ID", parameters: new { id }).FirstOrDefault();
                            if (agency != null)
                            {
                                ViewBag.Agency = agency;

                                var phone = new GenericRepository<V_AgencyPhone>().FindBy("AGENCY_ID=:id AND PHONE_TYPE=:type", orderby: "AGENCY_ID", parameters: new { id, type = new DbString { Value = ((int)PhoneType.IS).ToString(), Length = 3 } }).FirstOrDefault();
                                if (phone != null)
                                {
                                    ViewBag.Phone = phone;
                                }

                                var mobilePhone = new GenericRepository<V_AgencyPhone>().FindBy("AGENCY_ID=:id AND PHONE_TYPE=:type", orderby: "AGENCY_ID", parameters: new { id, type = new DbString { Value = ((int)PhoneType.MOBIL).ToString(), Length = 3 } }).FirstOrDefault();
                                if (mobilePhone != null)
                                {
                                    ViewBag.MobilePhone = mobilePhone;
                                }

                                var faxPhone = new GenericRepository<V_AgencyPhone>().FindBy("AGENCY_ID=:id AND PHONE_TYPE=:type", orderby: "AGENCY_ID", parameters: new { id, type = new DbString { Value = ((int)PhoneType.FAX).ToString(), Length = 3 } }).FirstOrDefault();
                                if (faxPhone != null)
                                {
                                    ViewBag.FaxPhone = faxPhone;
                                }

                                var CountyList = new CountyRepository().FindBy(orderby: "NAME");
                                ViewBag.CountyList = CountyList;

                                ViewBag.isEdit = true;
                            }
                            else
                            {
                                TempData["Alert"] = $"swAlert('Bilgi','Acente Bilgisi Bulunamadı. Lütfen Tekrar Deneyiniz...','information')";
                                return RedirectToAction("Agency");
                            }
                        }
                        break;

                    default:
                        ViewBag.Title = "Acente Ekle";
                        break;
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }


        [HttpPost]
        [LoginCompanyControl]
        public ActionResult AgencyFilter(FormCollection form)
        {
            AgencyVM vm = new AgencyVM();
            vm.ImportVM.ImportObjectType = ImportObjectType.AgencyCompany;
            vm.ExportVM.ViewName = Views.Agency;
            try
            {
                int start = Convert.ToInt32(Request["start"]);
                int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
                string searchValue = Request["search[value]"];
                string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
                string sortDirection = Request["order[0][dir]"];
                string isFilter = form["isFilter"];

                var companyId = HttpContext.Session["CompanyId"].ToString();
                if (!companyId.IsInt64())
                {
                    throw new Exception("Sigorta Şirketi bulunamadı, lütfen tekrar giriş yaparak deneyiniz!");
                }
                vm.ImportVM.SelectedCompanyId = long.Parse(companyId);

                ViewResultDto<List<V_Agency>> agencyList = new ViewResultDto<List<V_Agency>>();
                agencyList.Data = new List<V_Agency>();
                if (isFilter == "1" || isFilter.IsNull())
                {
                    String whereConditition = $" COMPANY_ID={companyId} AND";
                    whereConditition += !form["agencyCompanyName"].IsNull() ? $" AGENCY_NAME LIKE '%{form["agencyCompanyName"]}%' AND" : "";
                    whereConditition += form["agencyCompanyNo"].IsInt64() ? $" AGENCY_NO ={form["agencyCompanyNo"]} AND" : "";
                    whereConditition += form["agencyCity"].IsInt64() ? $" CITY_ID ={form["agencyCity"]} AND" : "";
                    whereConditition += form["agencyCounty"].IsInt64() ? $" COUNTY_ID ={ form["agencyCounty"]} AND" : "";

                    #region Export
                    vm.ExportVM.WhereCondition = string.IsNullOrEmpty(whereConditition) ? string.Empty : whereConditition;
                    vm.ExportVM.SetExportColumns();
                    #endregion
                    
                    agencyList = new GenericRepository<V_Agency>().FindByPaged(conditions: whereConditition.IsNull() ? "" : whereConditition.Substring(0, whereConditition.Length - 3),
                                            orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection}" : "",
                                            pageNumber: start,
                                            rowsPerPage: length);
                }
                //List<AgencyList> AgencyList = new List<AgencyList>();

                //if (agencyList.Data != null)
                //{
                //    foreach (var item in agencyList.Data)
                //    {
                //        AgencyList listItem = new AgencyList();
                //        listItem.AGENCY_ID = Convert.ToString(item.AGENCY_ID);
                //        listItem.COMPANY_ID = Convert.ToString(item.COMPANY_ID);
                //        listItem.CITY_NAME = item.CITY_NAME;
                //        listItem.COUNTY_NAME = item.COUNTY_NAME;
                //        listItem.STATUS = item.STATUS;
                //        listItem.COMPANY_NAME = Convert.ToString(item.COMPANY_NAME);
                //        listItem.AGENCY_NAME = Convert.ToString(item.AGENCY_NAME);
                //        listItem.AGENCY_NO = Convert.ToString(item.AGENCY_NO);

                //        AgencyList.Add(listItem);
                //    }
                //}

                return Json(new { data = agencyList.Data, draw = Request["draw"], recordsTotal = agencyList.TotalItemsCount, recordsFiltered = agencyList.TotalItemsCount, whereConditition = vm.ExportVM.WhereCondition }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View("Agency", vm);
        }
        #endregion

        #region PolicyGroup
        [LoginCompanyControl]
        public ActionResult PolicyGroup()
        {
            try
            {
                var ParentPolicyGroupList = new PolicyGroupRepository().FindBy();
                ViewBag.ParentPolicyGroupList = ParentPolicyGroupList;

                ViewBag.PARENT_ID = string.Empty;
                ViewBag.POLICY_GROUP_NAME = string.Empty;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;


                var result = new GenericRepository<V_PolicyGroup>().FindBy(orderby: "POLICY_GROUP_NAME");
                ViewBag.PolicyGroupList = result;

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return View();
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult PolicyGroupFilter(FormCollection form)
        {
            try
            {
                var companyId = HttpContext.Session["CompanyId"].ToString();
                var PolicyGroupList = new PolicyGroupRepository().FindBy($"COMPANY_ID={companyId}");
                ViewBag.ParentPolicyGroupList = PolicyGroupList;
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.PARENT_ID = form["PARENT_ID"];
                ViewBag.POLICY_GROUP_NAME = form["POLICY_GROUP_NAME"];

                string whereConditition = !String.IsNullOrEmpty(form["PARENT_ID"]) ? $" PARENT_ID='{form["PARENT_ID"]}' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["POLICY_GROUP_NAME"]) ? $" POLICY_GROUP_NAME LIKE '%{form["POLICY_GROUP_NAME"]}%' AND" : "";

                var result = new GenericRepository<V_PolicyGroup>().FindBy(whereConditition.IsNull() ? "" : whereConditition.Substring(0, whereConditition.Length - 3), orderby: "POLICY_GROUP_NAME");
                ViewBag.PolicyGroupList = result;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View("PolicyGroup");
        }
        #endregion

        #region Policy
        [LoginCompanyControl]
        public ActionResult Policy()
        {
            ViewBag.Title = "Poliçe";
            try
            {
                var companyId = HttpContext.Session["CompanyId"];
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.productList = new GenericRepository<V_Product>().FindBy($"COMPANY_ID={companyId}", orderby: "COMPANY_ID");
                ViewBag.PolicyGroupList = new PolicyGroupRepository().FindBy($"COMPANY_ID={companyId}", orderby: "COMPANY_ID");

                ViewBag.PolicyTypeList = LookupHelper.GetLookupData(LookupTypes.Policy, showAll: true);
                ViewBag.PolicyStatusList = LookupHelper.GetLookupData(Constants.LookupTypes.PolicyStatus, showChoose: true);
                ViewBag.ProductTypeList = LookupHelper.GetLookupData(LookupTypes.Product, showAll: true);
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        #region Table List / Filter
        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetPolicyList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];
            dynamic policyListData = null;
            int policyListTotalItemsCount = 0;
            List<PolicyList> PolicyList = new List<PolicyList>();

            if (isFilter == "1")
            {
                var companyId = HttpContext.Session["CompanyId"];

                if (!String.IsNullOrEmpty(form["insuredFirstName"]) || !String.IsNullOrEmpty(form["insuredLastName"]) || !String.IsNullOrEmpty(form["insuredTCKN"]))
                {
                    if (!String.IsNullOrEmpty(form["policyCompany"]) || !String.IsNullOrEmpty(form["product"]) || !String.IsNullOrEmpty(form["insurerTCKN"]) || !String.IsNullOrEmpty(form["insurerName"]) || !String.IsNullOrEmpty(form["policyType"]) || !String.IsNullOrEmpty(form["productType"]) || !String.IsNullOrEmpty(form["policyGroup"]) || !String.IsNullOrEmpty(form["policyNo"]) || !String.IsNullOrEmpty(form["policyStartDate"]) || !String.IsNullOrEmpty(form["policyRenewalNo"]) || !String.IsNullOrEmpty(form["policyStatus"]))
                    {
                        throw new Exception("Hem sigortalı hem poliçe bilgileriyle filtreleyerek sorgulama yapamazsınız!");
                    }

                    String whereConditition = form["insuredTCKN"].IsInt64() ? $" (INSURED_IDENTITY_NO = {form["insuredTCKN"]} OR INSURED_TAX_NUMBER = {form["insuredTCKN"]} OR INSURED_PASSPORT_NO LIKE '%{form["insuredTCKN"]}%') " : "";
                    whereConditition += companyId != null ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" COMPANY_ID = {companyId} " : "";
                    whereConditition += !String.IsNullOrEmpty(form["insuredFirstName"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" INSURED_FIRST_NAME LIKE '%{form["insuredFirstName"]}%' " : "";
                    whereConditition += !String.IsNullOrEmpty(form["insuredLastName"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" INSURED_LAST_NAME LIKE '%{form["insuredLastName"]}%' " : "";

                    var policyList = new GenericRepository<V_PolicyInsured>().FindByPaged(conditions: whereConditition,
                                                              orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},POLICY_ID DESC" : "POLICY_ID DESC",
                                                              pageNumber: start,
                                                              rowsPerPage: length);
                    policyListData = policyList.Data;
                    policyListTotalItemsCount = policyList.TotalItemsCount;
                }
                else
                {
                    String whereConditition = companyId != null ? $" COMPANY_ID = {companyId} " : "";
                    whereConditition += !String.IsNullOrEmpty(form["product"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" PRODUCT_ID = {long.Parse(form["product"])} " : "";
                    whereConditition += form["insurerTCKN"].IsInt64() ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" (INSURER_IDENTITY_NO = {form["insurerTCKN"]} OR INSURER_TAX_NUMBER  = {form["insurerTCKN"]}) " : "";
                    whereConditition += !String.IsNullOrEmpty(form["insurerName"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" INSURER_NAME LIKE '%{form["insurerName"]}%' " : "";
                    whereConditition += !String.IsNullOrEmpty(form["policyType"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_TYPE = '{(form["policyType"])}' " : "";
                    whereConditition += !String.IsNullOrEmpty(form["productType"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" PRODUCT_TYPE='{form["productType"]}' " : "";
                    whereConditition += !String.IsNullOrEmpty(form["policyGroup"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_GROUP_ID = {(form["policyGroup"])} " : "";
                    whereConditition += form["policyNo"].IsInt64() ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_NUMBER = {form["policyNo"]} " : "";
                    whereConditition += !String.IsNullOrEmpty(form["policyStartDate"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_START_DATE = TO_DATE('{ DateTime.Parse(form["policyStartDate"])}','DD.MM.YYYY HH24:MI:SS') " : "";
                    whereConditition += !String.IsNullOrEmpty(form["policyRenewalNo"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" RENEWAL_NO={long.Parse(form["policyRenewalNo"])} " : "";
                    whereConditition += !String.IsNullOrEmpty(form["policyStatus"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" STATUS='{form["policyStatus"]}' " : "";

                    var policyList = new GenericRepository<V_Policy>().FindByPaged(conditions: whereConditition,
                                                              orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},POLICY_ID DESC" : "POLICY_ID DESC",
                                                              pageNumber: start,
                                                              rowsPerPage: length);
                    policyListData = policyList.Data;
                    policyListTotalItemsCount = policyList.TotalItemsCount;
                }


                if (policyListData != null)
                {
                    foreach (var item in policyListData)
                    {
                        PolicyList listItem = new PolicyList();

                        listItem.POLICY_ID = Convert.ToString(item.POLICY_ID);
                        listItem.POLICY_TYPE_TEXT = LookupHelper.GetLookupCodeByOrdinal(LookupTypes.Policy, item.POLICY_TYPE);
                        listItem.POLICY_NUMBER_RENEWAL_NO = Convert.ToString(item.POLICY_NUMBER) + " - " + Convert.ToString(item.RENEWAL_NO);
                        listItem.POLICY_START_DATE = Convert.ToString(item.POLICY_START_DATE);
                        listItem.POLICY_END_DATE = Convert.ToString(item.POLICY_END_DATE);
                        string startDate = listItem.POLICY_START_DATE.IsDateTime() ? DateTime.Parse(listItem.POLICY_START_DATE).ToString("dd-MM-yyyy") : string.Empty;
                        string endDate = listItem.POLICY_END_DATE.IsDateTime() ? DateTime.Parse(listItem.POLICY_END_DATE).ToString("dd-MM-yyyy") : string.Empty;
                        listItem.POLICY_START_DATE = startDate + " - " + endDate;
                        listItem.INSURER_NAME = Convert.ToString(item.INSURER_NAME);
                        listItem.COMPANY_NAME = Convert.ToString(item.COMPANY_NAME);
                        listItem.PRODUCT_NAME = Convert.ToString(item.PRODUCT_NAME);
                        //listItem.PREMIUM = string.Format("{0:#.00}", Convert.ToDecimal(Convert.ToString(item.PREMIUM).Replace('.',','))) + "₺";
                        string premium = Convert.ToString(item.PREMIUM);
                        if (premium.IsNumeric())
                        {
                            listItem.PREMIUM = Convert.ToDecimal(Convert.ToString(item.PREMIUM).Replace('.', ',')).ToString("C");
                        }
                        else
                            listItem.PREMIUM = "";

                        listItem.IS_OPEN_TO_CLAIM = Convert.ToString(item.IS_OPEN_TO_CLAIM);

                        var endorsmentList = new GenericRepository<Endorsement>().FindBy($"POLICY_ID={listItem.POLICY_ID} AND STATUS={((int)(Status.PASIF))}", fetchDeletedRows: true);
                        if (endorsmentList != null && endorsmentList.Count > 0)
                        {
                            listItem.IS_NOT_ACTIVE_ENDORSEMENT = "0";
                        }
                        else
                        {
                            listItem.IS_NOT_ACTIVE_ENDORSEMENT = "1";
                        }

                        listItem.STATUS = Convert.ToString(item.STATUS);

                        PolicyList.Add(listItem);
                    }
                }
            }
            return Json(new { data = PolicyList, draw = Request["draw"], recordsTotal = policyListTotalItemsCount, recordsFiltered = policyListTotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetParentPolicyList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            var companyId = HttpContext.Session["CompanyId"];

            String whereConditition = companyId != null ? $" COMPANY_ID = {companyId} " : "";
            whereConditition += form["insurerTCKN"].IsInt64() ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" (INSURER_IDENTITY_NO = {form["insurerTCKN"]} OR INSURER_TAX_NUMBER = {form["insurerTCKN"]}) " : "";
            whereConditition += !String.IsNullOrEmpty(form["insurerName"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" INSURER_NAME LIKE '%{form["insurerName"]}%' " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyType"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_TYPE = '{(form["policyType"])}' " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyGroup"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_GROUP_ID = {(form["policyGroup"])} " : "";
            whereConditition += form["policyNo"].IsInt64() ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_NUMBER = {form["policyNo"]} " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyStartDate"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_START_DATE = TO_DATE('{ DateTime.Parse(form["policyStartDate"])}','DD.MM.YYYY HH24:MI:SS') " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyRenewalNo"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" RENEWAL_NO={long.Parse(form["policyRenewalNo"])} " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyStatus"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" STATUS={form["policyStatus"]} " : "";

            var policyList = new GenericRepository<V_Policy>().FindByPaged(conditions: whereConditition,
                                                      orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},POLICY_ID ASC" : "",
                                                      pageNumber: start,
                                                      rowsPerPage: length);
            List<PolicyList> PolicyList = new List<PolicyList>();

            if (policyList.Data != null)
            {
                foreach (var item in policyList.Data)
                {
                    PolicyList listItem = new PolicyList();

                    listItem.POLICY_ID = Convert.ToString(item.POLICY_ID);
                    listItem.POLICY_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Policy, item.POLICY_TYPE);
                    listItem.POLICY_NUMBER = item.POLICY_NUMBER;
                    listItem.POLICY_START_DATE = Convert.ToString(item.POLICY_START_DATE);
                    listItem.POLICY_START_DATE = listItem.POLICY_START_DATE.IsDateTime() ? DateTime.Parse(listItem.POLICY_START_DATE).ToString("dd-MM-yyyy") : string.Empty;
                    listItem.INSURER_NAME = Convert.ToString(item.INSURER_NAME);
                    listItem.COMPANY_NAME = Convert.ToString(item.COMPANY_NAME);
                    listItem.RENEWAL_NO = Convert.ToString(item.RENEWAL_NO);

                    listItem.IS_OPEN_TO_CLAIM = Convert.ToString(item.IS_OPEN_TO_CLAIM);

                    listItem.STATUS = Convert.ToString(item.STATUS);

                    PolicyList.Add(listItem);
                }
            }

            return Json(new { data = PolicyList, draw = Request["draw"], recordsTotal = policyList.TotalItemsCount, recordsFiltered = policyList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetPreviousPolicyList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            var companyId = HttpContext.Session["CompanyId"];

            String whereConditition = companyId != null ? $" COMPANY_ID = {companyId} " : "";
            whereConditition += form["insurerTCKN"].IsInt64() ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" (INSURER_IDENTITY_NO = {form["insurerTCKN"]} OR INSURER_TAX_NUMBER  = {form["insurerTCKN"]}) " : "";
            whereConditition += !String.IsNullOrEmpty(form["insurerName"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" INSURER_NAME LIKE '%{form["insurerName"]}%' " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyType"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_TYPE = '{(form["policyType"])}' " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyGroup"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_GROUP_ID = {(form["policyGroup"])} " : "";
            whereConditition += form["policyNo"].IsInt64() ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_NUMBER = {form["policyNo"]} " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyStartDate"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_START_DATE = TO_DATE('{ DateTime.Parse(form["policyStartDate"])}','DD.MM.YYYY HH24:MI:SS') " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyRenewalNo"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" RENEWAL_NO={long.Parse(form["policyRenewalNo"])} " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyStatus"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" STATUS={form["policyStatus"]} " : "";

            var policyList = new GenericRepository<V_Policy>().FindByPaged(conditions: whereConditition,
                                                      orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},POLICY_ID ASC" : "",
                                                      pageNumber: start,
                                                      rowsPerPage: length);
            List<PolicyList> PolicyList = new List<PolicyList>();

            if (policyList.Data != null)
            {
                foreach (var item in policyList.Data)
                {
                    PolicyList listItem = new PolicyList();

                    listItem.POLICY_ID = Convert.ToString(item.POLICY_ID);
                    listItem.POLICY_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Policy, item.POLICY_TYPE);
                    listItem.POLICY_NUMBER = item.POLICY_NUMBER;
                    listItem.POLICY_START_DATE = Convert.ToString(item.POLICY_START_DATE);
                    listItem.POLICY_START_DATE = listItem.POLICY_START_DATE.IsDateTime() ? DateTime.Parse(listItem.POLICY_START_DATE).ToString("dd-MM-yyyy") : string.Empty;
                    listItem.INSURER_NAME = Convert.ToString(item.INSURER_NAME);
                    listItem.COMPANY_NAME = Convert.ToString(item.COMPANY_NAME);
                    listItem.RENEWAL_NO = Convert.ToString(item.RENEWAL_NO);

                    listItem.IS_OPEN_TO_CLAIM = Convert.ToString(item.IS_OPEN_TO_CLAIM);

                    listItem.STATUS = Convert.ToString(item.STATUS);

                    PolicyList.Add(listItem);
                }
            }

            return Json(new { data = PolicyList, draw = Request["draw"], recordsTotal = policyList.TotalItemsCount, recordsFiltered = policyList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetPortfoyList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isOpen = form["isOpen"];

            var insurerList = new ViewResultDto<List<V_Portfoy>>();
            List<InsurerList> InsurerList = new List<InsurerList>();

            if (isOpen.IsNull() || isOpen == "1")
            {
                String whereConditition = !String.IsNullOrEmpty(form["CONTACT_TITLE"]) ? $" CONTACT_TITLE  LIKE '%{form["CONTACT_TITLE"]}%' " : "";
                whereConditition += form["IDENTITYorTAX"].IsInt64() ? (string.IsNullOrEmpty(whereConditition) ? "" : "AND") + $" (IDENTITY_NO = '{form["IDENTITYorTAX"]}' OR TAX_NUMBER = {form["IDENTITYorTAX"]} ) " : "";

                insurerList = new GenericRepository<V_Portfoy>().FindByPaged(conditions: string.IsNullOrEmpty(whereConditition) ? string.Empty : whereConditition,
                                                     orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},CONTACT_ID ASC" : "",
                                                     pageNumber: start,
                                                     rowsPerPage: length);

                if (insurerList.Data != null)
                {
                    foreach (var item in insurerList.Data)
                    {
                        InsurerList listItem = new InsurerList();

                        listItem.CONTACT_ID = Convert.ToString(item.CONTACT_ID);

                        if (item.CONTACT_TYPE == ((int)ContactType.TUZEL).ToString())
                            listItem.CONTACT_TITLE = Convert.ToString(item.CONTACT_TITLE);
                        else
                            listItem.CONTACT_TITLE = Convert.ToString(item.FIRST_NAME) + " " + Convert.ToString(item.LAST_NAME);

                        listItem.CONTACT_TYPE = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Contact, item.CONTACT_TYPE);
                        listItem.IDENTITY_NO = item.IDENTITY_NO;
                        listItem.TAX_NUMBER = item.TAX_NUMBER;

                        InsurerList.Add(listItem);
                    }
                }
            }
            return Json(new { data = InsurerList, draw = Request["draw"], recordsTotal = insurerList.TotalItemsCount, recordsFiltered = insurerList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetInsuredList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            List<InsuredList> InsuredList = new List<InsuredList>();
            ViewResultDto<List<V_Insured>> insuredList = new ViewResultDto<List<V_Insured>>();
            var policyId = form["POLICY_ID"];
            if (Extensions.IsInt64(policyId) && long.Parse(policyId) > 0)
            {
                dynamic parameters = new ExpandoObject();
                parameters.policyId = policyId;
                parameters.searchValue = searchValue;
                parameters.status = ((int)Status.AKTIF).ToString();

                var whereConditions = "POLICY_ID =:policyId AND STATUS=:status";
                whereConditions += searchValue.IsNull() || searchValue.IsInt64() ? string.Empty : $" AND (FIRST_NAME LIKE '%{searchValue.ToUpper()}%' OR LAST_NAME LIKE '%{searchValue.ToUpper()}%') ";
                whereConditions += !searchValue.IsInt64() ? string.Empty : $" AND (IDENTITY_NO = :searchValue OR FAMILY_NO = :searchValue) ";

                insuredList = new GenericRepository<V_Insured>().FindByPaged(conditions: whereConditions,
                                                                            orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},INSURED_ID ASC" : "INSURED_ID",
                                                                            pageNumber: start,
                                                                            rowsPerPage: length,
                                                                            parameters: parameters);

                if (insuredList.Data != null && insuredList.TotalItemsCount > 0)
                {
                    var insuredEndorsement = new GenericRepository<V_InsuredEndorsement>().FindBy("INSURED_ID IN :insuredIdLst", orderby: "INSURED_ID",
                                                                                                  parameters: new { insuredIdLst = insuredList.Data.Select(i => i.INSURED_ID).ToArray() });

                    foreach (var item in insuredList.Data)
                    {
                        InsuredList listItem = new InsuredList();

                        listItem.INSURED_ID = Convert.ToString(item.INSURED_ID);
                        listItem.INSURER_NAME = item.INSURER_NAME;
                        listItem.FIRST_NAME = item.FIRST_NAME;
                        listItem.LAST_NAME = item.LAST_NAME;
                        listItem.IDENTITY_NO = item.IDENTITY_NO;
                        listItem.INDIVIDUAL_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Individual, item.INDIVIDUAL_TYPE);
                        listItem.FAMILY_NO = Convert.ToString(item.FAMILY_NO);
                        listItem.PREMIUM = Convert.ToString(item.TOTAL_PREMIUM);
                        listItem.IS_OPEN_TO_CLAIM = item.IS_OPEN_TO_CLAIM;

                        listItem.BIRTHDATE = Convert.ToString(item.BIRTHDATE);
                        listItem.BIRTHDATE = listItem.BIRTHDATE.IsDateTime() ? DateTime.Parse(listItem.BIRTHDATE).ToString("dd-MM-yyyy") : string.Empty;

                        if (insuredEndorsement != null && insuredEndorsement.Count > 0)
                        {
                            listItem.ENDORSEMENT_ID_LIST = string.Join(", ", insuredEndorsement.Where(i => i.INSURED_ID == item.INSURED_ID).Select(ie => ie.ENDORSEMENT_NO).ToList());
                        }

                        InsuredList.Add(listItem);
                    }
                }
            }
            return Json(new { data = InsuredList, draw = Request["draw"], recordsTotal = insuredList.TotalItemsCount, recordsFiltered = insuredList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetPersonList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isOpen = form["isOpen"];

            List<InsuredList> PersonList = new List<InsuredList>();
            var personList = new ViewResultDto<List<V_Portfoy>>();

            if (isOpen.IsNull() || isOpen == "1")
            {
                var whereConditions = $"CONTACT_TYPE = {((int)ContactType.GERCEK).ToString()} AND BIRTHDATE IS NOT NULL AND GENDER IS NOT NULL ";
                whereConditions += form["NAME"].IsNull() ? string.Empty : $" AND FIRST_NAME LIKE '%{form["NAME"]}%' ";
                whereConditions += form["LASTNAME"].IsNull() ? string.Empty : $" AND LAST_NAME LIKE '%{form["LASTNAME"]}%' ";
                whereConditions += !form["IDENTITY_NO"].IsInt64() ? string.Empty : $" AND IDENTITY_NO = {form["IDENTITY_NO"]} ";

                personList = new GenericRepository<V_Portfoy>().FindByPaged(conditions: whereConditions,
                                                     orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},CONTACT_ID ASC" : "",
                                                     pageNumber: start,
                                                     rowsPerPage: length);


                if (personList.Data != null)
                {
                    foreach (var item in personList.Data)
                    {
                        InsuredList listItem = new InsuredList();

                        listItem.CONTACT_ID = Convert.ToString(item.CONTACT_ID);
                        listItem.FIRST_NAME = Convert.ToString(item.FIRST_NAME);
                        listItem.LAST_NAME = Convert.ToString(item.LAST_NAME);
                        listItem.IDENTITY_NO = item.IDENTITY_NO;
                        listItem.GENDER = Convert.ToString(item.GENDER);

                        string birtdate = Convert.ToString(item.BIRTHDATE);
                        if (birtdate.IsDateTime())
                        {
                            int age = DateTime.Now.Year - DateTime.Parse(birtdate).Year;
                            listItem.AGE = Convert.ToString(age);
                        }
                        PersonList.Add(listItem);
                    }
                }
            }
            return Json(new { data = PersonList, draw = Request["draw"], recordsTotal = personList.TotalItemsCount, recordsFiltered = personList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetFamilyList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            List<InsuredList> InsuredList = new List<InsuredList>();
            ViewResultDto<List<V_Insured>> insuredList = new ViewResultDto<List<V_Insured>>();
            var policyId = form["POLICY_ID"];
            if (policyId.IsInt64() && long.Parse(policyId) > 0)
            {
                var policy = new GenericRepository<Policy>().FindById(long.Parse(policyId));

                if (policy != null)
                {
                    policyId = policy.ParentId != null ? policy.ParentId.ToString() : policy.Id.ToString();

                    insuredList = new GenericRepository<V_Insured>().FindByPaged(conditions: "POLICY_ID =:policyId AND INDIVIDUAL_TYPE =:INDIVIDUAL_TYPE",
                                                         parameters: new { policyId, INDIVIDUAL_TYPE = new DbString { Value = ((int)IndividualType.FERT).ToString(), Length = 3 } },
                                                         orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},INSURED_ID ASC" : "",
                                                         pageNumber: start,
                                                         rowsPerPage: length);

                    if (insuredList.Data != null)
                    {
                        foreach (var item in insuredList.Data)
                        {
                            InsuredList listItem = new InsuredList();

                            listItem.INSURED_ID = Convert.ToString(item.INSURED_ID);
                            listItem.FIRST_NAME = Convert.ToString(item.FIRST_NAME);
                            listItem.LAST_NAME = Convert.ToString(item.LAST_NAME);
                            listItem.IDENTITY_NO = item.IDENTITY_NO;
                            listItem.INDIVIDUAL_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Individual, item.INDIVIDUAL_TYPE);
                            listItem.FAMILY_NO = Convert.ToString(item.FAMILY_NO);

                            InsuredList.Add(listItem);
                        }
                    }
                }
            }
            return Json(new { data = InsuredList, draw = Request["draw"], recordsTotal = insuredList.TotalItemsCount, recordsFiltered = insuredList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetEndorsementInsuredList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            List<InsuredList> InsuredList = new List<InsuredList>();
            //ViewResultDto<List<V_InsuredEndorsement>> insuredList = new ViewResultDto<List<V_InsuredEndorsement>>();
            var endorsementId = form["ENDORSEMENT_ID"];
            var policyId = form["POLICY_ID"];
            if (Extensions.IsInt64(endorsementId) && long.Parse(endorsementId) > 0)
            {
                dynamic parameters = new ExpandoObject();
                parameters.endorsementId = endorsementId;
                parameters.searchValue = searchValue;

                var whereConditions = "ENDORSEMENT_ID =:endorsementId";
                whereConditions += searchValue.IsNull() || searchValue.IsInt64() ? string.Empty : $" AND (FIRST_NAME LIKE '%:searchValue%' OR LAST_NAME LIKE '%:searchValue%') ";
                whereConditions += !searchValue.IsInt64() ? string.Empty : $" AND (IDENTITY_NO = :searchValue OR FAMILY_NO = :searchValue) ";

                ViewResultDto<List<V_InsuredEndorsement>> insuredEndList = new GenericRepository<V_InsuredEndorsement>().FindByPaged(conditions: whereConditions,
                                                                                orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},INSURED_ID ASC" : "INSURED_ID ASC",
                                                                                pageNumber: start,
                                                                                rowsPerPage: length,
                                                                                parameters: parameters,
                                                                                fetchDeletedRows: true,
                                                                                fetchHistoricRows: true);
                if (insuredEndList.Data != null && insuredEndList.TotalItemsCount > 0)
                {
                    var insuredEndorsement = new GenericRepository<V_InsuredEndorsement>().FindBy("INSURED_ID IN :insuredIdLst", orderby: "INSURED_ID",
                                                                                                      parameters: new { insuredIdLst = insuredEndList.Data.Select(i => i.INSURED_ID).ToArray() });

                    foreach (var item in insuredEndList.Data)
                    {
                        InsuredList listItem = new InsuredList();

                        listItem.INSURED_ID = Convert.ToString(item.INSURED_ID);
                        listItem.FIRST_NAME = Convert.ToString(item.FIRST_NAME);
                        listItem.LAST_NAME = Convert.ToString(item.LAST_NAME);
                        listItem.IDENTITY_NO = item.IDENTITY_NO;
                        listItem.INDIVIDUAL_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Individual, item.INDIVIDUAL_TYPE);
                        listItem.FAMILY_NO = Convert.ToString(item.FAMILY_NO);
                        if (item.ENDORSEMENT_TYPE != ((int)EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI).ToString() && item.ENDORSEMENT_TYPE != ((int)EndorsementType.SE_DEGISIKLIGI).ToString() && item.ENDORSEMENT_TYPE != ((int)EndorsementType.BASLANGIC_BITIS_TARIHI).ToString() && item.ENDORSEMENT_TYPE != ((int)EndorsementType.URETIM_KAYNAGI_DEGISIKLIGI).ToString())
                        {
                            listItem.PREMIUM = Convert.ToString(item.TOTAL_PREMIUM);

                            if (Convert.ToString(item.ENDORSEMENT_TYPE) == ((int)EndorsementType.SIGORTALI_CIKISI).ToString())
                            {
                                listItem.PREMIUM = Convert.ToString(item.PREMIUM);
                                listItem.EXIT_PREMIUM = Convert.ToString(item.TOTAL_PREMIUM);
                                listItem.USE_PREMIUM = Convert.ToString(item.PREMIUM - item.TOTAL_PREMIUM);
                            }
                            else
                            {
                                listItem.EXIT_PREMIUM = "-";
                                listItem.USE_PREMIUM = "-";
                            }
                        }
                        else
                        {
                            listItem.PREMIUM = "0";
                            listItem.EXIT_PREMIUM = "-";
                            listItem.USE_PREMIUM = "-";
                        }

                        listItem.BIRTHDATE = Convert.ToString(item.BIRTHDATE);
                        listItem.BIRTHDATE = listItem.BIRTHDATE.IsDateTime() ? DateTime.Parse(listItem.BIRTHDATE).ToString("dd-MM-yyyy") : string.Empty;

                        if (insuredEndorsement != null && insuredEndorsement.Count > 0)
                        {
                            listItem.ENDORSEMENT_ID_LIST = string.Join(", ", insuredEndorsement.Where(i => i.INSURED_ID == item.INSURED_ID).Select(ie => ie.ENDORSEMENT_NO).ToList());
                        }

                        InsuredList.Add(listItem);
                    }
                }
                if (insuredEndList.TotalItemsCount <= 0 && policyId.IsInt64())
                {
                    parameters = new ExpandoObject();
                    parameters.policyId = policyId;
                    parameters.searchValue = searchValue;

                    whereConditions = "POLICY_ID =:policyId ";
                    whereConditions += searchValue.IsNull() || searchValue.IsInt64() ? string.Empty : $" AND (FIRST_NAME LIKE '%:searchValue%' OR LAST_NAME LIKE '%:searchValue%') ";
                    whereConditions += !searchValue.IsInt64() ? string.Empty : $" AND (IDENTITY_NO = :searchValue OR FAMILY_NO = :searchValue) ";

                    ViewResultDto<List<V_InsuredEndorsement>> insuredList = new GenericRepository<V_InsuredEndorsement>().FindByPaged(conditions: whereConditions,
                                                                                orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},INSURED_ID ASC" : "INSURED_ID",
                                                                                pageNumber: start,
                                                                                rowsPerPage: length,
                                                                                parameters: parameters);

                    if (insuredList.Data != null && insuredList.TotalItemsCount > 0)
                    {
                        var insuredEndorsement = new GenericRepository<V_InsuredEndorsement>().FindBy("INSURED_ID IN :insuredIdLst", orderby: "INSURED_ID",
                                                                                                      parameters: new { insuredIdLst = insuredList.Data.Select(il => il.INSURED_ID).ToArray() });

                        foreach (var item in insuredList.Data)
                        {
                            InsuredList listItem = new InsuredList();

                            listItem.INSURED_ID = Convert.ToString(item.INSURED_ID);
                            listItem.INSURER_NAME = item.INSURER_NAME;
                            listItem.FIRST_NAME = item.FIRST_NAME;
                            listItem.LAST_NAME = item.LAST_NAME;
                            listItem.IDENTITY_NO = item.IDENTITY_NO;
                            listItem.INDIVIDUAL_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Individual, item.INDIVIDUAL_TYPE);
                            listItem.FAMILY_NO = Convert.ToString(item.FAMILY_NO);
                            listItem.PREMIUM = "0";
                            listItem.IS_OPEN_TO_CLAIM = item.INSURED_IS_OPEN_TO_CLAIM;

                            listItem.BIRTHDATE = Convert.ToString(item.BIRTHDATE);
                            listItem.BIRTHDATE = listItem.BIRTHDATE.IsDateTime() ? DateTime.Parse(listItem.BIRTHDATE).ToString("dd-MM-yyyy") : string.Empty;

                            if (insuredEndorsement != null && insuredEndorsement.Count > 0)
                            {
                                listItem.ENDORSEMENT_ID_LIST = string.Join(", ", insuredEndorsement.Where(i => i.INSURED_ID == item.INSURED_ID).Select(ie => ie.ENDORSEMENT_NO).ToList());
                            }

                            InsuredList.Add(listItem);
                        }
                    }
                }
            }
            return Json(new { data = InsuredList, draw = Request["draw"], recordsTotal = InsuredList.Count, recordsFiltered = InsuredList.Count }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetEndorsementList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            List<EndorsementList> EndorsementList = new List<EndorsementList>();
            ViewResultDto<List<V_PolicyEndorsement>> endorsementList = new ViewResultDto<List<V_PolicyEndorsement>>();
            var policyId = form["POLICY_ID"];
            if (Extensions.IsInt64(policyId) && long.Parse(policyId) > 0)
            {
                String whereConditition = $" POLICY_ID = {policyId}";
                whereConditition += !String.IsNullOrEmpty(form["COMPANY_ID"]) ? $" AND COMPANY_ID = {long.Parse(form["COMPANY_ID"])} " : "";
                whereConditition += !String.IsNullOrEmpty(form["INSURER_TCKN"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" (INSURER_IDENTITY_NO = {form["INSURER_TCKN"]} OR INSURER_TAX_NUMBER = {form["INSURER_TCKN"]}) " : "";
                whereConditition += !String.IsNullOrEmpty(form["INSURER_NAME"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" INSURER_NAME LIKE '%{form["INSURER_NAME"]}%' " : "";
                whereConditition += !String.IsNullOrEmpty(form["ENDORSEMENT_TYPE"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" ENDORSEMENT_TYPE = '{(form["ENDORSEMENT_TYPE"])}' " : "";
                whereConditition += !String.IsNullOrEmpty(form["ENDORSEMENT_NO"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" ENDORSEMENT_NO = {long.Parse(form["ENDORSEMENT_NO"])} " : "";

                endorsementList = new GenericRepository<V_PolicyEndorsement>().FindByPaged(conditions: whereConditition,
                                                     orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},ENDORSEMENT_ID ASC" : "",
                                                     pageNumber: start,
                                                     rowsPerPage: length);

                if (endorsementList.Data != null && endorsementList.Data.Count > 0)
                {
                    var endorsement = endorsementList.Data.OrderByDescending(p => p.ENDORSEMENT_ID).ToList()[0]; // new GenericRepository<Endorsement>().FindBy($" POLICY_ID = {policyId}", orderby: "ID DESC").FirstOrDefault();
                    foreach (var item in endorsementList.Data)
                    {
                        var endEndorsementId = endorsement.ENDORSEMENT_ID.ToString();
                        EndorsementList listItem = new EndorsementList();

                        listItem.ENDORSEMENT_ID = Convert.ToString(item.ENDORSEMENT_ID);
                        listItem.POLICY_ID = Convert.ToString(item.POLICY_ID);
                        listItem.ENDORSEMENT_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Endorsement, item.ENDORSEMENT_TYPE);
                        listItem.ENDORSEMENT_TYPE = item.ENDORSEMENT_TYPE;
                        listItem.ENDORSEMENT_NO = Convert.ToString(item.ENDORSEMENT_NO);
                        listItem.COMPANY_NAME = Convert.ToString(item.COMPANY_NAME);
                        listItem.INSURER_NAME = Convert.ToString(item.INSURER_NAME);
                        listItem.ENDORSEMENT_PREMIUM = Convert.ToString(item.ENDORSEMENT_PREMIUM);

                        listItem.ENDORSEMENT_START_DATE = Convert.ToString(item.ENDORSEMENT_START_DATE);
                        listItem.ENDORSEMENT_START_DATE = listItem.ENDORSEMENT_START_DATE.IsDateTime() ? DateTime.Parse(listItem.ENDORSEMENT_START_DATE).ToString("dd-MM-yyyy") : string.Empty;

                        listItem.ENDORSEMENT_DATE_OF_ISSUE = Convert.ToString(item.ENDORSEMENT_ISSUE_DATE);
                        listItem.ENDORSEMENT_DATE_OF_ISSUE = listItem.ENDORSEMENT_DATE_OF_ISSUE.IsDateTime() ? DateTime.Parse(listItem.ENDORSEMENT_DATE_OF_ISSUE).ToString("dd-MM-yyyy") : string.Empty;

                        listItem.STATUS = Convert.ToString(item.STATUS);
                        listItem.IS_BACK_STATUS = "0";

                        if (listItem.STATUS == ((int)Status.AKTIF).ToString())
                        {
                            if (item.SBM_STATUS == null || Convert.ToString(item.SBM_STATUS) == "0")
                            {
                                if (listItem.ENDORSEMENT_ID == endEndorsementId)
                                {
                                    List<EndorsementInsured> endorsementInsured = new GenericRepository<EndorsementInsured>().FindBy($"ENDORSEMENT_ID={listItem.ENDORSEMENT_ID}");
                                    if (endorsementInsured != null && endorsementInsured.Count > 0)
                                    {
                                        string insuredIdList = string.Join(",", endorsementInsured.Select(e => e.InsuredId));
                                        Common.Entities.ProteinEntities.Claim claim = new GenericRepository<Common.Entities.ProteinEntities.Claim>().FindBy($"POLICY_ID={listItem.POLICY_ID} AND INSURED_ID IN ({insuredIdList}) AND CLAIM_DATE >= TO_DATE('{ DateTime.Parse(listItem.ENDORSEMENT_DATE_OF_ISSUE)}','DD.MM.YYYY HH24:MI:SS')", orderby: "ID").FirstOrDefault();
                                        if (claim == null)
                                        {
                                            listItem.IS_BACK_STATUS = "1";
                                        }
                                    }
                                }
                            }
                        }

                        EndorsementList.Add(listItem);
                    }
                }
            }
            return Json(new { data = EndorsementList, draw = Request["draw"], recordsTotal = endorsementList.TotalItemsCount, recordsFiltered = endorsementList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetInstallment(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            List<InstallmentList> InstallmentList = new List<InstallmentList>();

            List<Installment> installmentList = new List<Installment>();
            var policyId = form["POLICY_ID"];

            if (Extensions.IsInt64(policyId) && long.Parse(policyId) > 0)
            {

                installmentList = new GenericRepository<Installment>().FindBy($"POLICY_ID={policyId}");
                if (installmentList != null)
                {
                    int i = 1;
                    foreach (var item in installmentList)
                    {
                        InstallmentList listItem = new InstallmentList();

                        listItem.INSTALLMENT_ID = Convert.ToString(item.Id);
                        listItem.NO = Convert.ToString(i);
                        listItem.AMOUNT = item.Amount;

                        listItem.DUE_DATE = Convert.ToString(item.DueDate);
                        listItem.DUE_DATE = listItem.DUE_DATE.IsDateTime() ? DateTime.Parse(listItem.DUE_DATE).ToString("dd-MM-yyyy") : string.Empty;

                        InstallmentList.Add(listItem);
                        i++;
                    }
                }
            }

            return Json(new { data = InstallmentList, draw = Request["draw"], recordsTotal = installmentList.Count, recordsFiltered = installmentList.Count }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetClaimResonList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            var insuredClaimReasonlist = new ViewResultDto<List<V_InsuredClaimReason>>();
            insuredClaimReasonlist.Data = new List<V_InsuredClaimReason>();

            if (form["INSURED_ID"].IsInt64() && long.Parse(form["INSURED_ID"]) > 0)
            {
                insuredClaimReasonlist = new GenericRepository<V_InsuredClaimReason>().FindByPaged(
                                  conditions: $"INSURED_ID = {long.Parse(form["INSURED_ID"])}",
                                  orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},CLAIM_REASON_ID DESC" : "CLAIM_REASON_ID DESC",
                                  pageNumber: start,
                                  rowsPerPage: length,
                                  fetchDeletedRows: true,
                                  fetchHistoricRows: true);
            }
            else if (form["POLICY_ID"].IsInt64() && long.Parse(form["POLICY_ID"]) > 0)
            {
                var policyClaimReasonlist = new ViewResultDto<List<V_PolicyClaimReason>>();
                policyClaimReasonlist.Data = new List<V_PolicyClaimReason>();
                policyClaimReasonlist = new GenericRepository<V_PolicyClaimReason>().FindByPaged(
                                  conditions: $"POLICY_ID = {long.Parse(form["POLICY_ID"])}",
                                  orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},CLAIM_REASON_ID DESC" : "CLAIM_REASON_ID DESC",
                                  pageNumber: start,
                                  rowsPerPage: length,
                                  fetchDeletedRows: true,
                                  fetchHistoricRows: true);

                return Json(new { data = policyClaimReasonlist.Data, draw = Request["draw"], recordsTotal = policyClaimReasonlist.TotalItemsCount, recordsFiltered = policyClaimReasonlist.TotalItemsCount }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { data = insuredClaimReasonlist.Data, draw = Request["draw"], recordsTotal = insuredClaimReasonlist.TotalItemsCount, recordsFiltered = insuredClaimReasonlist.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        [LoginCompanyControl]
        public JsonResult GetPolicyInsured()
        {
            var result = "[]";
            var PolicyId = Request["PolicyId"].IsInt64() ? long.Parse(Request["PolicyId"]) : 0;
            if (PolicyId > 0)
            {
                var insuredList = new GenericRepository<V_Insured>().FindBy($"POLICY_ID={PolicyId}", orderby: "", fetchDeletedRows: true);
                result = insuredList.ToJSON();
            }

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetProfileReport(FormCollection form)
        {
            var type = form["profileReportType"];
            var response = new PrintResponse();

            if (type == "0")
            {
                IPrint<PolicyProfileReq> printt = new PolicyProfile();
                response = printt.DoWork(new PolicyProfileReq
                {
                    PolicyId = long.Parse(form["hdPolicyId"])
                });
            }
            else if (type == "1")
            {
                IPrint<InsuredProfileReq> printt = new InsuredProfile();
                response = printt.DoWork(new InsuredProfileReq
                {
                    PolicyId = long.Parse(form["hdPolicyId"]),
                    InsuredId = long.Parse(form["insuredList"])
                });
            }
            else if (type == "2")
            {
                IPrint<FamilyBasedInsuredProfileReq> printt = new FamilyBasedInsuredProfile();
                response = printt.DoWork(new FamilyBasedInsuredProfileReq
                {
                    PolicyId = long.Parse(form["hdPolicyId"]),
                    FamilyNo = form["familyNo"],
                });
            }

            return new JsonResult()
            {
                Data = new { FileUrl = response.Url, FileName = response.FileName, Message = response.Message, Code = response.Code }
            };
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetTransferReport(FormCollection form)
        {
            var type = form["transferReportType"];
            var response = new PrintResponse();

            if (type == "0")
            {
                IPrint<PolicyTransferReq> printt = new PolicyTransfer();
                response = printt.DoWork(new PolicyTransferReq
                {
                    PolicyId = long.Parse(form["hdPolicyId"])
                });
            }
            else if (type == "1")
            {
                IPrint<InsuredTransferReq> printt = new InsuredTransferForm();
                response = printt.DoWork(new InsuredTransferReq
                {
                    InsuredId = long.Parse(form["insuredList"])
                });
            }
            else if (type == "2")
            {
                IPrint<FamilyBasedTransferProfileReq> printt = new FamilyBasedTransferProfile();
                response = printt.DoWork(new FamilyBasedTransferProfileReq
                {
                    PolicyId = long.Parse(form["hdPolicyId"]),
                    FamilyNo = form["familyNo"],
                });
            }

            return new JsonResult()
            {
                Data = new { FileUrl = response.Url, FileName = response.FileName, Message = response.Message, Code = response.Code }
            };
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetEndorsementReport()
        {
            var endorsementId = Request["EndorsementId"];
            var policyId = Request["PolicyId"];
            var type = Request["Type"];
            var response = new PrintResponse();

            if (endorsementId.IsInt64() && long.Parse(endorsementId) > 0 && policyId.IsInt64() && long.Parse(policyId) > 0)
            {
                if (type == "0")
                {
                    IPrint<PolicyEndorsmentReq> printt = new PolicyEndorsment();
                    response = printt.DoWork(new PolicyEndorsmentReq
                    {
                        PolicyId = long.Parse(policyId),
                        EndorsementId = long.Parse(endorsementId)
                    });
                }
                else if (type == "1")
                {
                    IPrint<PolicyCertificateReq> printt = new PolicyCertificate();
                    response = printt.DoWork(new PolicyCertificateReq
                    {
                        PolicyId = long.Parse(policyId),
                        EndorsementId = long.Parse(endorsementId)
                    });
                }
            }
            return new JsonResult()
            {
                Data = new { FileUrl = response.Url, FileName = response.FileName, Message = response.Message, Code = response.Code }
            };
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetOtherReport(FormCollection form)
        {
            var type = form["reportType"];
            var response = new PrintResponse();

            if (form["hdPolicyId"].IsInt64() && long.Parse(form["hdPolicyId"]) > 0)
            {
                if (type == "0") //MainPolicy
                {
                    var policy = new GenericRepository<Policy>().FindById(long.Parse(form["hdPolicyId"]));
                    if (policy != null)
                    {
                        IPrint<MainPolicyReq> printt = new MainPolicy();
                        response = printt.DoWork(new MainPolicyReq
                        {
                            PolicyId = long.Parse(form["hdPolicyId"])
                        });
                    }
                }
                else if (type == "1") //PolicyCertificate
                {
                    var policy = new GenericRepository<Policy>().FindById(long.Parse(form["hdPolicyId"]));
                    if (policy != null)
                    {
                        var endorsement = new GenericRepository<Endorsement>().FindBy($"POLICY_ID = {policy.Id} AND TYPE='{((int)EndorsementType.BASLANGIC).ToString()}'").FirstOrDefault();
                        if (endorsement != null)
                        {
                            IPrint<PolicyCertificateReq> printt = new PolicyCertificate();
                            response = printt.DoWork(new PolicyCertificateReq
                            {
                                PolicyId = policy.Id,
                                EndorsementId = endorsement.Id
                            });
                        }
                    }
                }
            }
            return new JsonResult()
            {
                Data = new { FileUrl = response.Url, FileName = response.FileName, Message = response.Message, Code = response.Code }
            };
        }

        [HttpPost]
        [LoginCompanyControl]
        private void FillCompanies()
        {
            var v_company = new GenericRepository<V_Company>().FindBy(orderby: "COMPANY_ID");
            ViewBag.v_company = v_company;
        }

        [HttpPost]
        public JsonResult GetInsurerDetails()
        {
            string id = Request["ContactId"];
            var result = "[]";
            try
            {
                if (id.IsInt64() && long.Parse(id) > 0)
                {
                    var insurer = new GenericRepository<V_Portfoy>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id }).FirstOrDefault();
                    if (insurer != null)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.CONTACT_ID = Convert.ToString(insurer.CONTACT_ID);
                        listItem.CORPORATE_ID = Convert.ToString(insurer.CORPORATE_ID);
                        listItem.PERSON_ID = Convert.ToString(insurer.PERSON_ID);

                        listItem.CONTACT_TYPE = Convert.ToString(insurer.CONTACT_TYPE);
                        listItem.GENDER = Convert.ToString(insurer.GENDER);

                        listItem.FIRST_NAME = Convert.ToString(insurer.FIRST_NAME);
                        listItem.LAST_NAME = Convert.ToString(insurer.LAST_NAME);
                        listItem.IDENTITY_NO = Convert.ToString(insurer.IDENTITY_NO);
                        listItem.PASSPORT_NO = Convert.ToString(insurer.PASSPORT_NO);
                        listItem.NAME_OF_FATHER = Convert.ToString(insurer.NAME_OF_FATHER);
                        string date = Convert.ToString(insurer.BIRTHDATE);
                        listItem.BIRTHDATE = date.IsDateTime() ? DateTime.Parse(date).ToString("dd-MM-yyyy") : null;
                        listItem.BIRTHPLACE = Convert.ToString(insurer.BIRTHPLACE);
                        listItem.NATIONALITY_ID = Convert.ToString(insurer.NATIONALITY_ID);

                        listItem.CONTACT_TITLE = Convert.ToString(insurer.CONTACT_TITLE);
                        listItem.TAX_NUMBER = Convert.ToString(insurer.TAX_NUMBER);
                        listItem.TAX_OFFICE = Convert.ToString(insurer.TAX_OFFICE);


                        var phoneItem = new GenericRepository<V_ContactPhone>().FindBy("CONTACT_ID=:id AND PHONE_TYPE=:phoneType", orderby: "CONTACT_ID", parameters: new { id, phoneType = new DbString { Value = ((int)PhoneType.IS).ToString(), Length = 3 } }).FirstOrDefault();
                        if (phoneItem != null)
                        {
                            listItem.PHONE_ID = Convert.ToString(phoneItem.PHONE_ID);
                            listItem.PHONE_NO = Convert.ToString(phoneItem.PHONE_NO);
                        }

                        var mobilePhoneItem = new GenericRepository<V_ContactPhone>().FindBy("CONTACT_ID=:id AND PHONE_TYPE=:phoneType", orderby: "CONTACT_ID", parameters: new { id, phoneType = new DbString { Value = ((int)PhoneType.MOBIL).ToString(), Length = 3 } }).FirstOrDefault();
                        if (mobilePhoneItem != null)
                        {
                            listItem.MOBILE_PHONE_ID = Convert.ToString(mobilePhoneItem.PHONE_ID);
                            listItem.MOBILE_PHONE_NO = Convert.ToString(mobilePhoneItem.PHONE_NO);
                        }

                        var emailItem = new GenericRepository<V_ContactEmail>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id }).FirstOrDefault();
                        if (emailItem != null)
                        {
                            listItem.EMAIL_ID = Convert.ToString(emailItem.EMAIL_ID);
                            listItem.EMAIL_DETAILS = Convert.ToString(emailItem.DETAILS);
                        }

                        var addressItem = new GenericRepository<V_ContactAddress>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id }).FirstOrDefault();
                        if (addressItem != null)
                        {
                            listItem.ADDRESS_ID = Convert.ToString(addressItem.ADDRESS_ID);
                            listItem.DETAILS = Convert.ToString(addressItem.DETAILS);
                            listItem.ZIP_CODE = Convert.ToString(addressItem.ZIP_CODE);
                            listItem.CITY_ID = Convert.ToString(addressItem.CITY_ID);
                            listItem.COUNTY_ID = Convert.ToString(addressItem.COUNTY_ID);
                        }

                        var bankAccount = new GenericRepository<V_ContactBank>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id }).FirstOrDefault();
                        if (bankAccount != null)
                        {
                            listItem.BANK_ACCOUNT_ID = Convert.ToString(bankAccount.BANK_ACCOUNT_ID);

                            listItem.BANK_ACCOUNT_NAME = bankAccount.BANK_ACCOUNT_NAME;
                            listItem.BANK_ID = Convert.ToString(bankAccount.BANK_ID);

                            listItem.BANK_BRANCH_ID = Convert.ToString(bankAccount.BANK_BRANCH_ID);
                            listItem.BANK_ACCOUNT_NO = bankAccount.BANK_ACCOUNT_NO;
                            listItem.IBAN = bankAccount.IBAN;
                            listItem.BANK_CURRENCY_TYPE = bankAccount.CURRENCY_TYPE;
                        }

                        List<dynamic> insurerList = new List<dynamic>();
                        insurerList.Add(listItem);
                        result = insurerList.ToJSON();
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        public JsonResult GetEndEndorsementNo()
        {
            var result = "[]";
            try
            {
                var policyId = Request["PolicyId"];
                if (policyId.IsInt64() && long.Parse(policyId) > 0)
                {
                    var endorsement = new GenericRepository<Endorsement>().FindBy($"POLICY_ID={policyId}", orderby: "NO DESC").FirstOrDefault();

                    if (endorsement != null)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();
                        listItem.NO = endorsement.No + 1;
                        List<dynamic> insurerList = new List<dynamic>();
                        insurerList.Add(listItem);
                        result = insurerList.ToJSON();
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [LoginCompanyControl]
        public ActionResult PolicyForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                if (id <= 0)
                {
                    return RedirectToAction("Policy");
                }
                var policyTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Policy);
                ViewBag.PolicyTypeList = policyTypeList;
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.PolicyId = id;
                var CityList = new CityRepository().FindBy();
                ViewBag.CityList = CityList;

                var CurrencyTypes = LookupHelper.GetLookupData(Constants.LookupTypes.Currency);
                ViewBag.CurrencyTypes = CurrencyTypes;

                var CountryList = new CountryRepository().FindBy();
                ViewBag.CountryList = CountryList;

                var PolicyGroupList = new PolicyGroupRepository().FindBy();
                ViewBag.PolicyGroupList = PolicyGroupList;

                var LocationTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Location);
                ViewBag.LocationTypeList = LocationTypeList;

                var GenderList = LookupHelper.GetLookupData(Constants.LookupTypes.Gender);
                ViewBag.GenderList = GenderList;

                var ContactTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Contact);
                ViewBag.ContactTypeList = ContactTypeList;

                var CashPaymentTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.CashPayment, showChoose: true);
                ViewBag.CashPaymentTypeList = CashPaymentTypeList;

                var PaymentTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Payment);
                ViewBag.PaymentTypeList = PaymentTypeList;

                var PolicyTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Policy);
                ViewBag.PolicyTypeList = PolicyTypeList;

                var BranchList = new BranchRepository().FindBy("PARENT_ID IS NULL");
                ViewBag.BranchList = BranchList;

                var BankList = new BankRepository().FindBy();
                ViewBag.BankList = BankList;

                ViewBag.InsuredList = string.Empty;
                ViewBag.EndorsementList = string.Empty;

                ViewBag.isEdit = true;
                ViewBag.Title = "Poliçe Görüntüle";

                var Policy = new GenericRepository<V_Policy>().FindBy("POLICY_ID=:id", orderby: "POLICY_ID", parameters: new { id }).FirstOrDefault();
                if (Policy != null)
                {
                    ViewBag.Policy = Policy;

                    var installmentList = new GenericRepository<Installment>().FindBy($"POLICY_ID=:id", orderby: "DUE_DATE", parameters: new { id });
                    ViewBag.InstallmentList = installmentList;

                    var ProductList = new GenericRepository<Product>().FindBy("COMPANY_ID=:COMPANY_ID", parameters: new { Policy.COMPANY_ID }, orderby: "NAME");
                    ViewBag.ProductList = ProductList;

                    var AgencyList = new GenericRepository<Agency>().FindBy("COMPANY_ID=:COMPANY_ID", parameters: new { Policy.COMPANY_ID }, orderby: "NAME");
                    ViewBag.AgencyList = AgencyList;

                    var SubProductList = new GenericRepository<Subproduct>().FindBy(orderby: "NAME");
                    ViewBag.SubProductList = SubProductList;

                    var Insureds = new GenericRepository<V_Insured>().FindBy("POLICY_ID=:id", orderby: "INSURED_ID", parameters: new { id });

                    if (Insureds.Count > 0)
                    {
                        ViewBag.InsuredList = Insureds;
                    }

                    var Endorsements = new GenericRepository<V_PolicyEndorsement>().FindBy("POLICY_ID=:id", orderby: "ENDORSEMENT_TYPE ASC", parameters: new { id });

                    if (Endorsements.Count > 0)
                    {
                        ViewBag.EndorsementList = Endorsements;
                        if (Endorsements[0].STATUS != ((int)Status.AKTIF).ToString())
                        {
                            ViewBag.StartEndorsementId = Endorsements[0].ENDORSEMENT_ID;
                        }
                    }

                    ViewBag.isEdit = true;
                }
                else
                {
                    throw new Exception("Poliçe Bilgisi Bulunamadı!");
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
                return RedirectToAction("Policy");
            }
            return View();
        }
        #endregion

        #region Endorsement
        [LoginCompanyControl]
        public ActionResult EndorsementForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                ViewBag.PolicyId = Request["PolicyId"];
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                #region Lookup Fill
                var EndorsementCategoryList = LookupHelper.GetLookupData(LookupTypes.EndorsementCategory);
                ViewBag.EndorsementCategoryList = EndorsementCategoryList;

                var EndorsementTypeDescriptionTypeList = LookupHelper.GetLookupData(LookupTypes.EndorsementTypeDescription);
                ViewBag.EndorsementTypeDescriptionTypeList = EndorsementTypeDescriptionTypeList;

                var CurrencyTypes = LookupHelper.GetLookupData(Constants.LookupTypes.Currency);
                ViewBag.CurrencyTypes = CurrencyTypes;

                var EndorsementTypeList = LookupHelper.GetLookupData(LookupTypes.Endorsement);
                ViewBag.EndorsementTypeList = EndorsementTypeList;

                var LocationTypeList = LookupHelper.GetLookupData(LookupTypes.Location);
                ViewBag.LocationTypeList = LocationTypeList;

                var GenderList = LookupHelper.GetLookupData(LookupTypes.Gender);
                ViewBag.GenderList = GenderList;

                var ContactTypeList = LookupHelper.GetLookupData(LookupTypes.Contact);
                ViewBag.ContactTypeList = ContactTypeList;

                var IndividualTypeList = LookupHelper.GetLookupData(LookupTypes.Individual);
                ViewBag.IndividualTypeList = IndividualTypeList;
                #endregion

                #region Table List
                var CityList = new CityRepository().FindBy();
                ViewBag.CityList = CityList;

                var CountryList = new CountryRepository().FindBy();
                ViewBag.CountryList = CountryList;

                var policy = new GenericRepository<V_Policy>().FindBy("POLICY_ID=:policyId", orderby: "POLICY_ID", parameters: new { policyId = ViewBag.PolicyId }).FirstOrDefault();

                if (policy == null)
                {
                    TempData["Alert"] = $"swAlert('Hata','Poliçe Bilgisi Bulunamadı.','warning')";
                    return RedirectToAction("Policy");
                }

                ViewBag.SelectAgency = policy.AGENCY_ID;
                if (policy.SUBPRODUCT_ID != null)
                {
                    var PackageList = new GenericRepository<V_Package>().FindBy("SUBPRODUCT_ID=:SUBPRODUCT_ID", orderby: "PACKAGE_NAME", parameters: new { policy.SUBPRODUCT_ID });

                    List<dynamic> packageList = new List<dynamic>();

                    if (PackageList != null)
                    {

                        foreach (var item in PackageList)
                        {
                            dynamic listItem = new System.Dynamic.ExpandoObject();

                            listItem.PACKAGE_NAME = item.PACKAGE_NAME;
                            listItem.PACKAGE_ID = Convert.ToString(item.PACKAGE_ID);

                            packageList.Add(listItem);
                        }

                    }
                    ViewBag.PackageList = packageList;
                }

                var Insureds = new GenericRepository<V_Insured>().FindBy("POLICY_ID=:policyId", orderby: "INSURED_ID", parameters: new { policyId = ViewBag.PolicyId });
                ViewBag.InsuredList = Insureds;

                var agencyList = new GenericRepository<Agency>().FindBy("COMPANY_ID=COMPANY_ID", orderby: "COMPANY_ID", parameters: new { policy.COMPANY_ID });
                ViewBag.AgencyList = agencyList;
                #endregion

                ViewBag.isView = false;

                ViewBag.isEdit = false;

                switch (formaction.ToLower())
                {
                    case "edit":
                    case "view":
                        if (id > 0)
                        {
                            var endorsement = new GenericRepository<Endorsement>().FindById(id);
                            if (endorsement != null)
                            {
                                ViewBag.Endorsement = endorsement;

                                if (endorsement.Type == ((int)EndorsementType.SE_DEGISIKLIGI).ToString())
                                {
                                    ViewBag.InsurerTitle = policy.INSURER_NAME;
                                    ViewBag.InsurerContactId = policy.INSURER;
                                }
                                else if (endorsement.Type == ((int)EndorsementType.POLICE_TAHAKKUK_ZEYLI).ToString() || endorsement.Type == ((int)EndorsementType.PRIM_FARKI_ZEYLI).ToString())
                                {
                                    ViewBag.TAX = policy.TAX;
                                    ViewBag.PREMIUM = policy.PREMIUM;

                                    decimal total = 0;
                                    if (!string.IsNullOrEmpty(Convert.ToString(policy.TAX)))
                                    {
                                        total += decimal.Parse(Convert.ToString(policy.TAX));
                                    }
                                    if (!string.IsNullOrEmpty(Convert.ToString(policy.PREMIUM)))
                                    {
                                        total += decimal.Parse(Convert.ToString(policy.PREMIUM));
                                    }
                                    if (!string.IsNullOrEmpty(Convert.ToString(policy.FOREX_SELLING)))
                                    {
                                        total *= decimal.Parse(Convert.ToString(policy.FOREX_SELLING));
                                    }

                                    ViewBag.TOTAL_AMOUNT = total;
                                }
                                else if (endorsement.Type == ((int)EndorsementType.BASLANGIC_BITIS_TARIHI).ToString())
                                {
                                    string sDate = Convert.ToString(policy.POLICY_START_DATE);
                                    ViewBag.START_DATE = sDate.IsDateTime() ? DateTime.Parse(sDate).ToString("dd-MM-yyyy") : null;

                                    string eDate = Convert.ToString(policy.POLICY_END_DATE);
                                    ViewBag.END_DATE = eDate.IsDateTime() ? DateTime.Parse(eDate).ToString("dd-MM-yyyy") : null;
                                }
                            }
                            ViewBag.isView = true;
                            ViewBag.Title = "Zeyl Görüntüle";
                        }
                        break;
                    default:
                        ViewBag.Title = "Zeyl Ekle";
                        break;
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return View();
        }

        [LoginCompanyControl]
        public ActionResult EndorsementDelete(Int64 id)
        {
            string policyId = null;
            try
            {
                var repo = new GenericRepository<Endorsement>();
                var result = repo.FindById(id);
                if (result == null)
                {
                    throw new Exception("Zeyl Bilgisi Bulunamadı!!!");
                }
                else if (result.Status == ((int)Status.AKTIF).ToString())
                {
                    throw new Exception("Tanzim Edilmiş Zeyl Silinemez!!!");
                }
                policyId = result.PolicyId.ToString();
                Policy policy = new GenericRepository<Policy>().FindById(long.Parse(policyId));
                if (policy == null)
                {
                    throw new Exception("Poliçe Bilgisi Bulunamadı!!!");
                }

                if (result.Type == ((int)EndorsementType.SIGORTALI_CIKISI).ToString())
                {
                    var insuredList = new GenericRepository<Insured>().FindBy($"LAST_ENDORSEMENT_ID={result.Id} AND STATUS IN('{((int)Status.SILINDI)}','{((int)Status.PASIF)}')", fetchDeletedRows: true);
                    if (insuredList != null)
                    {
                        foreach (var insured in insuredList)
                        {
                            insured.Status = ((int)Status.AKTIF).ToString();
                            insured.TotalPremium = insured.InitialPremium == null ? 0 : insured.InitialPremium;
                            new GenericRepository<Insured>().Update(insured);
                        }
                    }
                }

                result.Status = ((int)Status.SILINDI).ToString();
                var spResponse = repo.Update(result);
                if (spResponse.Code != "100")
                {
                    throw new Exception(spResponse.Code + " - " + spResponse.Message);
                }

                policy.LastEndorsementId = new GenericRepository<Endorsement>().FindBy($"POLICY_ID={policyId}", orderby: "NO DESC").FirstOrDefault()?.Id;
                new GenericRepository<Policy>().Update(policy);

                TempData["Alert"] = $"swAlert('İşlem Başarılı','Silme İşlemi Başarıyla Gerçekleştirildi.','success')";
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            if (policyId.IsNull())
                return RedirectToAction("Policy");
            else
                return RedirectToAction("Endorsement/" + policyId);
        }

        [HttpPost]
        [LoginCompanyControl]
        public JsonResult EndorsementCancelClaimControl(FormCollection form)
        {
            object res = "[]";
            try
            {
                var policyId = form["hdPolicyIdForCancel"];

                if (policyId.IsInt64())
                {
                    var searchClaim = new GenericRepository<Common.Entities.ProteinEntities.Claim>().FindBy($"POLICY_ID=:policyId AND STATUS NOT IN :statusList", parameters: new { policyId, statusList = new[] { ((int)ClaimStatus.IPTAL).ToString(), ((int)ClaimStatus.SILINDI).ToString(), ((int)ClaimStatus.RET).ToString() } }).FirstOrDefault();
                    if (searchClaim != null)
                    {
                        throw new Exception("Poliçeye Ait IPTAL Statüsü Dışında Hasar Bulunmakta! Lütfen Hasar Kayıtlarını Kontrol Ediniz...");
                    }
                }
                res = "100";
            }
            catch (Exception ex)
            {
                res = "999";
                //TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(res.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [LoginCompanyControl]
        public ActionResult Endorsement(Int64 id = 0, string formaction = "")
        {
            ViewBag.PolicyId = id;

            if (ViewBag.PolicyId != null)
            {
                try
                {
                    var companyList = new GenericRepository<Company>().FindBy();
                    ViewBag.CompanyList = companyList;

                    var EndorsementTypeList = LookupHelper.GetLookupData(LookupTypes.Endorsement, showAll: true);
                    ViewBag.EndorsementTypeList = EndorsementTypeList;


                    TempData["Alert"] = TempData["Alert"] ?? string.Empty;
                }
                catch (Exception ex)
                {
                    TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
                }
                return View();
            }
            else
            {
                return RedirectToAction("Policy");
            }
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult EndorsementFilter(Int64 id, FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            String whereConditition = $" POLICY_ID = {id}";
            whereConditition += !String.IsNullOrEmpty(form["insurerTCKN"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" (INSURER_IDENTITY_NO = {form["insurerTCKN"]} OR INSURER_TAX_NUMBER = {form["insurerTCKN"]}) " : "";
            whereConditition += !String.IsNullOrEmpty(form["insurerName"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" INSURER_NAME LIKE '%{form["insurerName"]}%' " : "";
            whereConditition += !String.IsNullOrEmpty(form["endorsementType"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" ENDORSEMENT_TYPE = '{(form["endorsementType"])}' " : "";
            whereConditition += !String.IsNullOrEmpty(form["endorsementNo"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" ENDORSEMENT_NO = {long.Parse(form["endorsementNo"])} " : "";

            var endorsementList = new GenericRepository<V_PolicyEndorsement>().FindByPaged(conditions: whereConditition,
                                                      orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection}" : "",
                                                      pageNumber: start,
                                                      rowsPerPage: length);

            ViewBag.Endorsements = endorsementList.Data;

            ViewBag.PolicyId = id;

            var companyList = new CompanyRepository().FindBy();
            ViewBag.CompanyList = companyList;

            var EndorsementTypeList = LookupHelper.GetLookupData(LookupTypes.Endorsement);
            ViewBag.EndorsementTypeList = EndorsementTypeList;

            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            return View("Endorsement");
        }
        #endregion

        #region Insured
        [LoginCompanyControl]
        public ActionResult Insured(Int64 id = 0, string formaction = "")
        {
            ViewBag.EndorsementId = Request["EndorsementId"];

            if (Request["EndorsementId"].IsInt64())
            {
                var endorsement = new GenericRepository<Endorsement>().FindById(long.Parse(Request["EndorsementId"]));
                ViewBag.EndorsementType = endorsement.Type;
            }

            if ((id > 0 && Request["EndorsementId"].IsInt64()) || !formaction.IsNull())
            {
                #region Lookup Fill
                var MaritalStatusList = LookupHelper.GetLookupData(Constants.LookupTypes.MaritalStatus, showChoose: true);
                ViewBag.MaritalStatusList = MaritalStatusList;

                var GenderList = LookupHelper.GetLookupData(Constants.LookupTypes.Gender, showChoose: true);
                ViewBag.GenderList = GenderList;

                var IndividualTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Individual, showChoose: true);
                ViewBag.IndividualTypeList = IndividualTypeList;

                var RenewalGuaranteeTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.RenewalGuarantee, showChoose: true);
                ViewBag.RenewalGuaranteeTypeList = RenewalGuaranteeTypeList;

                var InsuredTransferTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.InsuredTransfer, showChoose: true);
                ViewBag.InsuredTransferTypeList = InsuredTransferTypeList;

                var ContactTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Contact);
                ViewBag.ContactTypeList = ContactTypeList;

                var CurrencyTypes = LookupHelper.GetLookupData(Constants.LookupTypes.Currency);
                ViewBag.CurrencyTypes = CurrencyTypes;

                var VIPTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.VIP, showChoose: true);
                ViewBag.VIPTypeList = VIPTypeList;

                var InsuredNoteTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.InsuredNote, showChoose: true);
                ViewBag.InsuredNoteTypeList = InsuredNoteTypeList;

                #endregion

                #region Table List
                var BankList = new BankRepository().FindBy();
                ViewBag.BankList = BankList;

                var CompanyList = new CompanyRepository().FindBy();
                ViewBag.CompanyList = CompanyList;

                var CityList = new CityRepository().FindBy();
                ViewBag.CityList = CityList;

                var CountryList = new CountryRepository().FindBy(orderby: "NAME");
                ViewBag.CountryList = CountryList;

                var CountyList = new CountyRepository().FindBy();
                ViewBag.CountyList = CountyList;

                var ExclusionList = new ExclusionRepository().FindBy();
                ViewBag.ExclusionList = ExclusionList;
                #endregion

                var resultProvider = new GenericRepository<V_Provider>().FindBy(orderby: "PROVIDER_ID");
                ViewBag.ProviderList = resultProvider.ToJSON(true);

                var resultProcessGroupList = new GenericRepository<V_ProcessGroup>().FindBy("PROCESS_LIST_NAME=:processListName", parameters: new { processListName = new DbString { Value = "ICD10", Length = 100 } }, orderby: "PROCESS_GROUP_NAME");
                ViewBag.ProcessGroupList = resultProcessGroupList;

                var ProviderGroupList = new GenericRepository<ProviderGroup>().FindBy();
                ViewBag.ProviderGroupList = ProviderGroupList;

                var ExclusionStatusReasonList = new GenericRepository<Reason>().FindBy("STATUS_NAME='ExclusionStatus' AND STATUS_ORDINAL=2");
                ViewBag.ExclusionStatusReasonList = ExclusionStatusReasonList;

                switch (formaction.ToLower())
                {
                    case "view":
                    case "edit":
                        if (id > 0)
                        {
                            var insured = new GenericRepository<V_Insured>().FindBy("INSURED_ID=:id", orderby: "INSURED_ID", parameters: new { id }).FirstOrDefault();
                            if (insured != null)
                            {
                                ViewBag.Insured = insured;

                                var PackageList = new GenericRepository<V_Package>().FindBy("SUBPRODUCT_ID=:SUBPRODUCT_ID", orderby: "PACKAGE_NAME", parameters: new { insured.SUBPRODUCT_ID });

                                List<dynamic> packageList = new List<dynamic>();

                                if (PackageList != null)
                                {

                                    foreach (var item in PackageList)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.PACKAGE_NAME = item.PACKAGE_NAME;
                                        listItem.PACKAGE_ID = Convert.ToString(item.PACKAGE_ID);

                                        packageList.Add(listItem);
                                    }

                                }
                                ViewBag.PackageList = packageList.Count > 0 ? packageList.ToJSON(true) : null;

                                var resultCoverage = new GenericRepository<V_PackagePlanCoverage>().FindBy("PACKAGE_ID=:PACKAGE_ID AND IS_MAIN_COVERAGE=1", orderby: "COVERAGE_ID", parameters: new { insured.PACKAGE_ID });
                                ViewBag.CoverageList = resultCoverage;

                                string contactId = Convert.ToString(insured.CONTACT_ID);
                                if (!contactId.IsNull())
                                {
                                    var bankss = new GenericRepository<V_ContactBank>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id = contactId });
                                    if (bankss != null)
                                    {

                                        List<dynamic> banksList = new List<dynamic>();

                                        int i = 1;

                                        foreach (var item in bankss)
                                        {
                                            dynamic listItem = new System.Dynamic.ExpandoObject();

                                            listItem.id = i;
                                            listItem.ClientId = i;
                                            listItem.STATUS = item.STATUS;

                                            listItem.BANK_ACCOUNT_ID = Convert.ToString(item.BANK_ACCOUNT_ID);
                                            listItem.CONTACT_BANK_ACCOUNT_ID = Convert.ToString(item.CONTACT_BANK_ACCOUNT_ID);

                                            listItem.BANK_ACCOUNT_NAME = item.BANK_ACCOUNT_NAME;
                                            listItem.BANK_ID = Convert.ToString(item.BANK_ID);
                                            listItem.BANK_IDSelectedText = item.BANK_NAME;


                                            listItem.BANK_BRANCH_ID = Convert.ToString(item.BANK_BRANCH_ID);
                                            listItem.BANK_BRANCH_IDSelectedText = item.BANK_BRANCH_NAME;
                                            listItem.BANK_BRANCH_CODE = item.BANK_BRANCH_CODE;
                                            listItem.BANK_ACCOUNT_NO = item.BANK_ACCOUNT_NO;
                                            listItem.IBAN = item.IBAN;
                                            listItem.BANK_CURRENCY_TYPE = item.CURRENCY_TYPE;
                                            listItem.CURRENCY_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Currency, item.CURRENCY_TYPE);
                                            listItem.IS_PRIMARY_BANK = item.IS_PRIMARY;
                                            listItem.isOpen = "0";

                                            banksList.Add(listItem);

                                            i++;
                                        }

                                        ViewBag.ContactBankList = banksList.ToJSON();
                                    }

                                    var notess = new GenericRepository<V_ContactNote>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id = contactId });
                                    if (notess != null)
                                    {
                                        List<dynamic> notesList = new List<dynamic>();

                                        int i = 1;

                                        foreach (var item in notess)
                                        {
                                            dynamic listItem = new System.Dynamic.ExpandoObject();

                                            listItem.id = i;
                                            listItem.ClientId = i;
                                            listItem.STATUS = item.STATUS;

                                            listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                                            listItem.CONTACT_NOTE_ID = Convert.ToString(item.CONTACT_NOTE_ID);

                                            listItem.NOTE_TYPE = Convert.ToString(item.NOTE_TYPE);
                                            listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Note, item.NOTE_TYPE);
                                            listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                                            listItem.isOpen = "0";
                                            notesList.Add(listItem);

                                            i++;
                                        }

                                        ViewBag.NoteList = notesList.ToJSON();
                                    }
                                }

                                var insuredExclusions = new GenericRepository<V_InsuredExclusion>().FindBy("INSURED_ID=:id", orderby: "INSURED_ID", parameters: new { id });
                                if (insuredExclusions != null)
                                {

                                    List<dynamic> insuredExclusionList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in insuredExclusions)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        if (item.RULE_CATEGORY_TYPE != null)
                                        {
                                            listItem.RULE_CATEGORY_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                            listItem.RESULT = Convert.ToString(item.RESULT);
                                            listItem.RESULT_SECOND = Convert.ToString(item.RESULT_SECOND);
                                        }
                                        else
                                        {
                                            listItem.RULE_CATEGORY_TYPE_TEXT = "EK PRİM";
                                            listItem.RESULT = Convert.ToString(item.ADDITIONAL_PREMIUM);
                                        }
                                        listItem.EXCLUSION_DESCRIPTION = Convert.ToString(item.DESCRIPTION);

                                        listItem.INSURED_EXCLUSION_ID = Convert.ToString(item.INSURED_EXCLUSION);
                                        listItem.isOpen = "0";

                                        insuredExclusionList.Add(listItem);

                                        i++;
                                    }
                                    ViewBag.Exclusions = insuredExclusionList.ToJSON();
                                }

                                var declarationss = new GenericRepository<V_InsuredNote>().FindBy("INSURED_ID =:id AND NOTE_TYPE=:noteType", orderby: "INSURED_ID", parameters: new { id, noteType = new DbString { Value = ((int)InsuredNoteType.BEYAN).ToString(), Length = 3 } });
                                if (declarationss != null)
                                {
                                    List<dynamic> notesList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in declarationss)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                                        listItem.INSURED_NOTE_ID = Convert.ToString(item.INSURED_NOTE_ID);

                                        listItem.NOTE_TYPE = Convert.ToString(item.NOTE_TYPE);
                                        listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.InsuredNote, item.NOTE_TYPE);
                                        listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                                        listItem.isOpen = "0";
                                        notesList.Add(listItem);

                                        i++;
                                    }

                                    ViewBag.DeclarationList = notesList.ToJSON();
                                }

                                var transferNotes = new GenericRepository<V_InsuredNote>().FindBy("INSURED_ID =:id AND NOTE_TYPE=:noteType", orderby: "INSURED_ID", parameters: new { id, noteType = new DbString { Value = ((int)InsuredNoteType.GECİS).ToString(), Length = 3 } });
                                if (transferNotes != null)
                                {
                                    List<dynamic> notesList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in transferNotes)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                                        listItem.INSURED_NOTE_ID = Convert.ToString(item.INSURED_NOTE_ID);

                                        listItem.NOTE_TYPE = Convert.ToString(item.NOTE_TYPE);
                                        listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.InsuredNote, item.NOTE_TYPE);
                                        listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                                        listItem.isOpen = "0";
                                        notesList.Add(listItem);

                                        i++;
                                    }

                                    ViewBag.TransferNoteList = notesList.ToJSON();
                                }

                                if (formaction.ToLower() == "view")
                                {
                                    ViewBag.Title = "Sigortalı Görüntüle";
                                    ViewBag.IsView = true;
                                }
                                else
                                {
                                    ViewBag.Title = "Sigortalı Güncelle";
                                    ViewBag.IsEdit = true;
                                }
                                return View();

                            }
                        }
                        break;
                    default:
                        ViewBag.PolicyId = id;
                        ViewBag.Title = "Sigortalı Kaydet";

                        var policy = new GenericRepository<V_Policy>().FindBy("POLICY_ID=:policyId", orderby: "POLICY_ID", parameters: new { policyId = ViewBag.PolicyId }).FirstOrDefault();
                        if (policy != null)
                        {
                            ViewBag.Policy = policy;

                            if (policy.SUBPRODUCT_ID != null)
                            {
                                var PackageList = new GenericRepository<V_Package>().FindBy("SUBPRODUCT_ID=:SUBPRODUCT_ID", orderby: "PACKAGE_NAME", parameters: new { policy.SUBPRODUCT_ID });

                                List<dynamic> packageList = new List<dynamic>();

                                if (PackageList != null)
                                {

                                    foreach (var item in PackageList)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.PACKAGE_NAME = item.PACKAGE_NAME;
                                        listItem.PACKAGE_ID = Convert.ToString(item.PACKAGE_ID);

                                        packageList.Add(listItem);
                                    }

                                }
                                ViewBag.PackageList = packageList.Count > 0 ? packageList.ToJSON(true) : null;
                            }
                            if (policy.EXCHANGE_RATE_ID != null)
                            {
                                var code = Convert.ToString(policy.CURRENCY_TYPE);

                                if (!string.IsNullOrEmpty(code))
                                {
                                    var ExChangeRate = new GenericRepository<ExchangeRate>().FindBy("CURRENCY_TYPE =:currencyType", parameters: new { currencyType = new DbString { Value = code, Length = 3 } }, orderby: "ID DESC").FirstOrDefault();
                                    ViewBag.ExChangeRate = ExChangeRate;
                                }
                            }

                            var endFamilyNo = new GenericRepository<V_InsuredEndorsement>().FindBy("FAMILY_NO IS NOT NULL AND POLICY_ID=:policyId AND ENDORSEMENT_ID=:endorsementId", parameters: new { policyId = ViewBag.PolicyId, endorsementId = ViewBag.EndorsementId }, orderby: "FAMILY_NO DESC").FirstOrDefault();
                            if (endFamilyNo != null)
                            {
                                ViewBag.LastFamilyNo = (long.Parse(endFamilyNo.FAMILY_NO.ToString()));
                                ViewBag.FamilyNo = (long.Parse(endFamilyNo.FAMILY_NO.ToString()) + 1);
                            }
                            else
                            {
                                ViewBag.LastFamilyNo = 1;
                                ViewBag.FamilyNo = 1;
                            }
                        }
                        break;
                }

                return View();
            }
            else
            {
                return RedirectToAction("Policy");
            }
        }

        [HttpPost]
        [LoginCompanyControl]
        public JsonResult InsuredDeleteClaimControl(FormCollection form)
        {
            object res = "[]";
            var endorsementId = form["hdEndorsementId"];
            var policyId = form["hdPolicyId"];
            try
            {
                var insuredId = form["hdInsuredId"];
                if (insuredId.IsInt64())
                {
                    var searchClaim = new GenericRepository<Common.Entities.ProteinEntities.Claim>().FindBy($"INSURED_ID={insuredId} AND STATUS NOT IN('{((int)ClaimStatus.IPTAL).ToString()}','{((int)ClaimStatus.SILINDI).ToString()}')").FirstOrDefault();
                    if (searchClaim != null)
                    {
                        throw new Exception("Sigortalıya Ait IPTAL Statüsü Dışında Hasar Bulunmakta! Lütfen Hasar Kayıtlarını Kontrol Ediniz...");
                    }
                }
                res = "100";
            }
            catch (Exception ex)
            {
                res = "999";
                //TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(res.ToJSON(), JsonRequestBehavior.AllowGet);
        }


        [LoginCompanyControl]
        public JsonResult CalculateDeletePremium(FormCollection form)
        {
            object res = "[]";
            var endorsementId = form["hdEndorsementId"];
            var policyId = form["hdPolicyId"];
            try
            {
                var insuredId = form["hdInsuredId"];
                if (!insuredId.IsInt64())
                {
                    throw new Exception("Sigortalı Bilgisi Bulunamadı!!!");
                }
                var calculateType = form["calculateType"];
                if (!calculateType.IsInt())
                {
                    throw new Exception("Hesaplama Tipini Seçiniz");
                }
                decimal policyTotalDay = 0, insuredDay = 0;
                V_Insured insured = new GenericRepository<V_Insured>().FindBy($"INSURED_ID={insuredId}", orderby: "INSURED_ID").FirstOrDefault();
                if (insured != null)
                {
                    TimeSpan time = (DateTime)insured.POLICY_END_DATE - (DateTime)insured.POLICY_START_DATE;
                    policyTotalDay = Math.Abs(time.Days);


                    if (calculateType == "0" && form["insuredDeleteDate"].IsDateTime())
                    {
                        var exitDate = DateTime.Parse(form["insuredDeleteDate"]).AddHours(12);
                        TimeSpan timeSpan = exitDate - (insured.ENTRANCE_DATE.ToString().IsDateTime() ? (DateTime)insured.ENTRANCE_DATE : (DateTime)insured.POLICY_START_DATE);

                        insuredDay = timeSpan.Days;
                        if (insuredDay < 0)
                            insuredDay = 0;
                    }
                    if (calculateType == "1" && form["InsuredDeleteDay"].IsInt())
                    {
                        insuredDay = int.Parse(form["InsuredDeleteDay"]);
                        if (insuredDay < 0)
                            insuredDay = 0;
                    }
                    decimal existPremium = decimal.Parse(((insuredDay / policyTotalDay) * (decimal)(insured.TOTAL_PREMIUM == null ? insured.PREMIUM : insured.TOTAL_PREMIUM)).ToString("#0.00"));

                    if (calculateType == "2" && form["InsuredExitPremium"].IsNumeric())
                    {
                        existPremium = decimal.Parse(form["InsuredExitPremium"].Replace('.', ','));
                    }

                    res = existPremium;
                }
                else
                {
                    throw new Exception("Sigortalı Bilgisi Bulunamadı!!!");
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(res.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetInsuredContactDetails()
        {
            string id = Request["ContactId"];
            var result = "[]";
            try
            {
                if (id.IsInt64() && long.Parse(id) > 0)
                {
                    var item = new GenericRepository<V_Portfoy>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id }).FirstOrDefault();
                    if (item != null)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.CONTACT_ID = Convert.ToString(item.CONTACT_ID);
                        listItem.PERSON_ID = Convert.ToString(item.PERSON_ID);

                        listItem.GENDER = Convert.ToString(item.GENDER);
                        listItem.MARITAL_STATUS = Convert.ToString(item.MARITAL_STATUS);

                        listItem.FIRST_NAME = Convert.ToString(item.FIRST_NAME);
                        listItem.LAST_NAME = Convert.ToString(item.LAST_NAME);
                        listItem.IDENTITY_NO = Convert.ToString(item.IDENTITY_NO);
                        listItem.TAX_NUMBER = Convert.ToString(item.TAX_NUMBER);
                        listItem.PASSPORT_NO = Convert.ToString(item.PASSPORT_NO);
                        listItem.NAME_OF_FATHER = Convert.ToString(item.NAME_OF_FATHER);

                        string BIRTHDATE = Convert.ToString(item.BIRTHDATE);
                        listItem.BIRTHDATE = BIRTHDATE.IsDateTime() ? DateTime.Parse(BIRTHDATE).ToString("dd-MM-yyyy") : null;

                        listItem.AGE = BIRTHDATE.IsDateTime() ? DateTime.Now.Year - DateTime.Parse(BIRTHDATE).Year : 0;

                        listItem.IS_VIP = Convert.ToString(item.IS_VIP);
                        listItem.VIP_TYPE = Convert.ToString(item.VIP_TYPE);
                        listItem.VIP_TEXT = Convert.ToString(item.VIP_TEXT);

                        listItem.BIRTHPLACE = Convert.ToString(item.BIRTHPLACE);
                        listItem.NATIONALITY_ID = Convert.ToString(item.NATIONALITY_ID);



                        var phoneItem = new GenericRepository<V_ContactPhone>().FindBy("CONTACT_ID=:id AND PHONE_TYPE=:phoneType AND IS_PRIMARY='1'", orderby: "CONTACT_ID", parameters: new { id, phoneType = new DbString { Value = ((int)PhoneType.IS).ToString(), Length = 3 } }).FirstOrDefault();
                        if (phoneItem != null)
                        {
                            listItem.PHONE_ID = Convert.ToString(phoneItem.PHONE_ID);
                            listItem.PHONE_NO = Convert.ToString(phoneItem.PHONE_NO);
                        }

                        var mobilePhoneItem = new GenericRepository<V_ContactPhone>().FindBy("CONTACT_ID=:id AND PHONE_TYPE=:phoneType AND IS_PRIMARY='1'", orderby: "CONTACT_ID", parameters: new { id, phoneType = new DbString { Value = ((int)PhoneType.MOBIL).ToString(), Length = 3 } }).FirstOrDefault();
                        if (mobilePhoneItem != null)
                        {
                            listItem.MOBILE_PHONE_ID = Convert.ToString(mobilePhoneItem.PHONE_ID);
                            listItem.MOBILE_PHONE_NO = Convert.ToString(mobilePhoneItem.PHONE_NO);
                        }

                        var emailItem = new GenericRepository<V_ContactEmail>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id }).FirstOrDefault();
                        if (emailItem != null)
                        {
                            listItem.EMAIL_ID = Convert.ToString(emailItem.EMAIL_ID);
                            listItem.EMAIL_DETAILS = Convert.ToString(emailItem.DETAILS);
                        }

                        var addressItem = new GenericRepository<V_ContactAddress>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id }).FirstOrDefault();
                        if (addressItem != null)
                        {
                            listItem.ADDRESS_ID = Convert.ToString(addressItem.ADDRESS_ID);
                            listItem.DETAILS = Convert.ToString(addressItem.DETAILS);
                            listItem.ZIP_CODE = Convert.ToString(addressItem.ZIP_CODE);
                            listItem.CITY_ID = Convert.ToString(addressItem.CITY_ID);
                            listItem.COUNTY_ID = Convert.ToString(addressItem.COUNTY_ID);
                        }
                        List<dynamic> insurerList = new List<dynamic>();
                        insurerList.Add(listItem);
                        result = insurerList.ToJSON();
                    }
                }
            }
            catch (Exception ex)
            {
                result = "[]";
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        public JsonResult GetPremium(FormCollection form)
        {
            var result = "[]";
            try
            {
                var age = form["hdAge"];
                if (!age.IsInt())
                {
                    var birthdate = form["hdBIRTHDATE"];
                    if (birthdate.IsDateTime())
                    {
                        age = (DateTime.Now.Year - DateTime.Parse(birthdate).Year).ToString();
                    }
                }
                var gender = form["hdGender"];
                var individualType = form["INDIVIDUAL_TYPE"];
                if (!age.IsInt() || gender.IsNull() || individualType.IsNull())
                {
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                var packageId = form["package"];
                if (!packageId.IsInt64())
                {
                    packageId = form["InsuredPackageChange"];
                }
                if (packageId.IsInt64() && long.Parse(packageId) > 0)
                {
                    var packagePrice = new GenericRepository<V_PackagePrice>().FindBy($"PACKAGE_ID={packageId} AND AGE_START<={age} AND AGE_END>={age} AND GENDER={gender} AND INDIVIDUAL_TYPE={individualType}", orderby: "INDIVIDUAL_TYPE").FirstOrDefault();
                    dynamic listItem = new System.Dynamic.ExpandoObject();
                    List<dynamic> insurerList = new List<dynamic>();

                    if (form["hdInsuredIdforPackage"].IsInt64())
                    {
                        Insured insured = new GenericRepository<Insured>().FindById(long.Parse(form["hdInsuredIdforPackage"]));
                        listItem.INSURED_PREMIUM = insured.TotalPremium;
                    }
                    if (packagePrice != null)
                    {
                        listItem.PREMIUM = packagePrice.PREMIUM;
                        insurerList.Add(listItem);
                    }
                    else
                    {
                        listItem.PREMIUM = 0;
                        insurerList.Add(listItem);
                    }



                    result = insurerList.ToJSON();
                }
            }
            catch (Exception ex)
            {

            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}