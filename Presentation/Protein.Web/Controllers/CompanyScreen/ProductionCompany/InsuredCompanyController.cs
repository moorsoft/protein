﻿using Dapper;
using Newtonsoft.Json;
using Protein.Common.Constants;
using Protein.Common.Dto;
using Protein.Common.Extensions;
using Protein.Common.Resources;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Web.ActionFilter.LoginAttr;
using Protein.Web.Models.ListView;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Web.Controllers.CompanyScreen.ProductionCompany
{
    public class InsuredCompanyController : Controller
    {
        [LoginCompanyControl]
        public ActionResult Index()
        {
            TempData["Alert"] = TempData["Alert"] ?? string.Empty;
            ViewBag.GenderCodeList = LookupHelper.GetLookupDataCode(LookupTypes.Gender);
            ViewBag.IndividualTypeCodeList = LookupHelper.GetLookupDataCode(LookupTypes.Individual);
            ViewBag.GenderList = LookupHelper.GetLookupData(LookupTypes.Gender);
            ViewBag.StatusList = LookupHelper.GetLookupData(LookupTypes.Status);
            ViewBag.IndividualTypeList = LookupHelper.GetLookupData(LookupTypes.Individual);
            ViewBag.InsuredTransferTypeList = LookupHelper.GetLookupData(LookupTypes.InsuredTransfer);
            ViewBag.CurrencyTypeList = LookupHelper.GetLookupData(LookupTypes.Currency);
            ViewBag.AgreementTypeList = LookupHelper.GetLookupData(LookupTypes.Agreement);
            ViewBag.ExemptionTypeList = LookupHelper.GetLookupData(LookupTypes.Exemption);
            ViewBag.NoteTypeList = LookupHelper.GetLookupData(LookupTypes.Note);
            ViewBag.ClaimOpenReasonList = ReasonHelper.GetReasonData("ClaimReasonType", "0");
            ViewBag.ClaimCloseReasonList = ReasonHelper.GetReasonData("ClaimReasonType", "1");
            ViewBag.PolicyStatusList = LookupHelper.GetLookupData(LookupTypes.PolicyStatus);
            ViewBag.PolicyTypeList = LookupHelper.GetLookupDataCode(LookupTypes.Policy);
            ViewBag.EndorsementTypeList = LookupHelper.GetLookupData(LookupTypes.Endorsement);
            ViewBag.ClaimReasonTypeList = LookupHelper.GetLookupData(LookupTypes.ClaimReason);
            return View();
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetInsuredList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];
            ViewResultDto<List<V_Insured>> insuredList = new ViewResultDto<List<V_Insured>>();
            insuredList.Data = new List<V_Insured>();
            if (isFilter == "1")
            {
                var companyId = HttpContext.Session["CompanyId"];

                String whereConditition = $" POLICY_STATUS!='{((int)PolicyStatus.SILINDI).ToString()}' AND";
                whereConditition += companyId != null ? $" COMPANY_ID={companyId} AND" : "";
                whereConditition += form["insuredListContactId"].IsInt64() ? $" CONTACT_ID={long.Parse(form["insuredListContactId"])} AND" : "";
                whereConditition += form["insuredListTckn"].IsInt64() ? $" (IDENTITY_NO = {form["insuredListTckn"]} OR TAX_NUMBER = {form["insuredListTckn"]} OR PASSPORT_NO = '{form["insuredListTckn"]}') AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["insuredListName"]) ? $" FIRST_NAME LIKE '%{form["insuredListName"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["insuredListSurname"]) ? $" LAST_NAME LIKE '%{form["insuredListSurname"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["insuredListBirthdayDate"]) ? $" BIRTHDATE = TO_DATE('{ DateTime.Parse(form["insuredListBirthdayDate"]) }','DD/MM/YYYY HH24:MI:SS') AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["insuredListGender"]) ? $" GENDER ={form["insuredListGender"]} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["insuredListIndividualType"]) ? $" INDIVIDUAL_TYPE={form["insuredListIndividualType"]} AND" : "";
                whereConditition += form["insuredListPolicyNo"].IsInt64() ? $" POLICY_NUMBER={form["insuredListPolicyNo"]} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["insuredListPolicyStartDate"]) ? $" POLICY_START_DATE >= TO_DATE('{ DateTime.Parse(form["insuredListPolicyStartDate"])}','DD/MM/YYYY HH24:MI:SS') AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["insuredListPolicyStartDate2"]) ? $" POLICY_START_DATE <= TO_DATE('{ DateTime.Parse(form["insuredListPolicyStartDate2"])}','DD/MM/YYYY HH24:MI:SS') AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["insuredListPolicyFinishDate"]) ? $" POLICY_END_DATE >= TO_DATE('{ DateTime.Parse(form["insuredListPolicyFinishDate"])}','DD/MM/YYYY HH24:MI:SS') AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["insuredListPolicyFinishDate2"]) ? $" POLICY_END_DATE <= TO_DATE('{ DateTime.Parse(form["insuredListPolicyFinishDate2"])}','DD/MM/YYYY HH24:MI:SS') AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["insuredListRenewalNo"]) ? $" RENEWAL_NO={long.Parse(form["insuredListRenewalNo"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["PRODUCT_LIST"]) ? $" PRODUCT_ID = {form["PRODUCT_LIST"]} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["insuredListStatus"]) ? $" STATUS={long.Parse(form["insuredListStatus"])} AND" : "";

                insuredList = new GenericRepository<V_Insured>().FindByPaged(
                                  conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                  orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},INSURED_ID DESC" : "INSURED_ID DESC",
                                  pageNumber: start,
                                  rowsPerPage: length, fetchDeletedRows: true);
            }
            return Json(new { data = insuredList.Data, draw = Request["draw"], recordsTotal = insuredList.TotalItemsCount, recordsFiltered = insuredList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetOldClaim(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isOpen = form["isOpen"];

            var insuredId = form["InsuredId"];
            var claimId = form["ClaimId"];
            string claimCondition = "";
            string hint = "";
            if (!string.IsNullOrEmpty(claimId))
            {
                claimCondition = $" AND CLAIM_ID != {claimId}";
                hint = "/*+ USE_HASH(claim_id) ORDERED */";
            }
            var companyId = HttpContext.Session["CompanyId"];

            var oldClaimlist = new ViewResultDto<List<V_Claim>>();
            oldClaimlist.Data = new List<V_Claim>();


            if (isOpen == "1" && insuredId.IsInt64())
            {
                oldClaimlist = new GenericRepository<V_Claim>().FindByPaged(
                                  conditions: $"COMPANY_ID={companyId} AND INSURED_ID={insuredId}" + claimCondition,
                                  orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},CLAIM_ID DESC" : "CLAIM_ID DESC",
                                  pageNumber: start,
                                  rowsPerPage: length,
                                  hint:hint);
            }

            return Json(new { data = oldClaimlist.Data, draw = Request["draw"], recordsTotal = oldClaimlist.TotalItemsCount, recordsFiltered = oldClaimlist.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginCompanyControl]
        public JsonResult GeneralDetails()
        {
            try
            {
                var insuredId = Request["insuredId"];
                var contactId = Request["contactId"];
                Session["insuredId"] = insuredId;
                Session["contactId"] = contactId;
                Console.WriteLine(insuredId);
                Console.WriteLine(contactId);

                var ExclusionList = new GenericRepository<V_InsuredExclusion>().FindBy("INSURED_ID =: insuredId", orderby: "INSURED_ID", parameters: new { insuredId });
                if (ExclusionList.Count > 0)
                {
                    List<dynamic> insuredExclusionList = new List<dynamic>();

                    foreach (var item in ExclusionList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        if (item.RULE_CATEGORY_TYPE != null)
                        {
                            listItem.RULE_CATEGORY_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                            listItem.RESULT = string.IsNullOrEmpty(Convert.ToString(item.RESULT)) ? "-" : Convert.ToString(item.RESULT);
                            listItem.RESULT_SECOND = string.IsNullOrEmpty(Convert.ToString(item.RESULT_SECOND)) ? "-" : Convert.ToString(item.RESULT_SECOND);
                        }
                        else
                        {
                            listItem.RULE_CATEGORY_TYPE_TEXT = "EK PRİM";
                            listItem.RESULT = string.IsNullOrEmpty(Convert.ToString(item.ADDITIONAL_PREMIUM)) ? "-" : Convert.ToString(item.ADDITIONAL_PREMIUM);
                            listItem.RESULT_SECOND = "-";
                        }
                        listItem.INSURED_EXCLUSION = Convert.ToString(item.INSURED_EXCLUSION);
                        listItem.EXCLUSION_DESCRIPTION = string.IsNullOrEmpty(Convert.ToString(item.DESCRIPTION)) ? "-" : Convert.ToString(item.DESCRIPTION);

                        insuredExclusionList.Add(listItem);
                    }
                    ViewBag.exclusionList = insuredExclusionList;
                }

                var NoteList = new GenericRepository<V_ContactNote>().FindBy($"CONTACT_ID=:contactId AND NOTE_TYPE != '{((int)NoteType.OZEL)}'", orderby: "CONTACT_ID", parameters: new { contactId });

                if (NoteList.Count > 0)
                {
                    var spResponseIds = string.Join(",", NoteList.Select(n => n.SP_RESPONSE_ID).ToArray());
                    var UserNote = new GenericRepository<V_ResponseUser>().FindBy($"RESPONSE_ID IN({spResponseIds})", orderby: "RESPONSE_ID", fetchDeletedRows: true, fetchHistoricRows: true);

                    List<dynamic> notesList = new List<dynamic>();

                    foreach (var item in NoteList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();
                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                        listItem.NOTE_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Note, item.NOTE_TYPE);
                        listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();
                        listItem.CREATE_USER = UserNote.Where(u => u.RESPONSE_ID == item.SP_RESPONSE_ID).FirstOrDefault()?.USERNAME;
                        listItem.CREATE_DATE = UserNote.Where(u => u.RESPONSE_ID == item.SP_RESPONSE_ID).FirstOrDefault()?.SP_REQUEST_DATE;
                        notesList.Add(listItem);
                    }
                    ViewBag.noteList = notesList;
                }

                var result = new
                {
                    noteList = ViewBag.noteList,
                    exclusionList = ViewBag.exclusionList
                };
                return new JsonResult()
                {
                    MaxJsonLength = Int32.MaxValue,
                    Data = JsonConvert.SerializeObject(result),
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };

            }
            catch (Exception EX)
            {

            }
            return null;

        }

        [HttpPost]
        [LoginCompanyControl]
        public JsonResult PackageDetails()
        {
            var insuredId = Request["insuredId"];
            var packageId = Request["packageId"];
            Session["insuredId"] = insuredId;
            Session["packageId"] = packageId;
            Console.WriteLine(insuredId);
            Console.WriteLine(packageId);
            var companyId = HttpContext.Session["CompanyId"];

            var PackageList = new GenericRepository<V_InsuredPackage>().FindBy("COMPANY_ID=:companyId AND INSURED_ID =: insuredId AND PACKAGE_ID =: packageId", fetchHistoricRows: true, fetchDeletedRows: true, orderby: "INSURED_ID", parameters: new { companyId, insuredId, packageId });
            if (PackageList != null && PackageList.Count > 0)
            {
                ViewBag.packageList = PackageList;
            }
            else
            {
                throw new Exception("Sigortalıya ait paket bulunamadı!");
            }

            var result = new
            {
                packageList = ViewBag.packageList
            };

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(result),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        [LoginCompanyControl]
        public JsonResult Declerations()
        {
            var insuredId = Request["insuredId"];
            Session["insuredId"] = insuredId;
            Console.WriteLine(insuredId);

            var DeclerationList = new GenericRepository<V_InsuredNote>().FindBy("INSURED_ID=:insuredId AND NOTE_TYPE=:noteType", orderby: "INSURED_ID", parameters: new { insuredId, noteType = new DbString { Value = ((int)InsuredNoteType.BEYAN).ToString(), Length = 3 } });
            if (DeclerationList.Count > 0)
            {
                List<dynamic> notesList = new List<dynamic>();

                foreach (var item in DeclerationList)
                {
                    dynamic listItem = new System.Dynamic.ExpandoObject();

                    listItem.NOTE_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.InsuredNote, item.NOTE_TYPE);
                    listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                    notesList.Add(listItem);
                }

                ViewBag.declerationList = notesList;
            }

            var result = new
            {
                declerationList = ViewBag.declerationList
            };

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(result),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        [LoginCompanyControl]
        public JsonResult Address()
        {
            var insuredId = Request["insuredId"];
            Session["insuredId"] = insuredId;
            Console.WriteLine(insuredId);

            var Address = new GenericRepository<V_InsuredAddress>().FindBy("INSURED_ID =: insuredId", orderby: "INSURED_ID", parameters: new { insuredId }).FirstOrDefault();
            if (Address != null)
                ViewBag.address = Address;

            var result = new
            {
                address = ViewBag.address
            };

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(result),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        [LoginCompanyControl]
        public JsonResult BankAccounts()
        {
            var insuredId = Request["insuredId"];
            Session["insuredId"] = insuredId;
            Console.WriteLine(insuredId);

            var BankAccountList = new GenericRepository<V_InsuredBankAccount>().FindBy("INSURED_ID =: insuredId", orderby: "INSURED_ID", parameters: new { insuredId });
            if (BankAccountList.Count > 0)
                ViewBag.bankAccountList = BankAccountList;

            var result = new
            {
                bankAccountList = ViewBag.bankAccountList
            };

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(result),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        [LoginCompanyControl]
        public JsonResult ContactInfo()
        {
            var insuredId = Request["insuredId"];
            Session["insuredId"] = insuredId;
            Console.WriteLine(insuredId);

            var PhoneList = new GenericRepository<V_InsuredPhone>().FindBy("INSURED_ID =: insuredId", orderby: "INSURED_ID", parameters: new { insuredId });
            if (PhoneList.Count > 0)
                ViewBag.phoneList = PhoneList;

            var MailList = new GenericRepository<V_InsuredEmail>().FindBy("INSURED_ID =: insuredId", orderby: "INSURED_ID", parameters: new { insuredId });
            if (MailList.Count > 0)
                ViewBag.mailList = MailList;

            var result = new
            {
                phoneList = ViewBag.phoneList,
                mailList = ViewBag.mailList
            };

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(result),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        [LoginCompanyControl]
        public JsonResult Edit()
        {
            var insuredId = Request["insuredId"];
            Session["insuredId"] = insuredId;
            Console.WriteLine(insuredId);

            // TODO - Edit başka yerde kullanılmıyorsa Details olarak değiştirilip bölünmeli ilk ekrandaki veriler sadece dönmeli.
            var companyId = HttpContext.Session["CompanyId"];

            var InsuredList = new GenericRepository<V_Insured>().FindBy("COMPANY_ID=:companyId AND INSURED_ID =: id", orderby: "INSURED_ID", parameters: new { companyId, id = insuredId }).FirstOrDefault();
            if (InsuredList != null)
            {
                ViewBag.summary = InsuredList;
            }
            else
            {
                throw new Exception("Sigortalı bulunamadı!");
            }

            var PackageList = new GenericRepository<V_InsuredPackage>().FindBy("COMPANY_ID=:companyId  AND INSURED_ID =: insuredId AND PACKAGE_ID =: packageId", fetchHistoricRows: true, fetchDeletedRows: true, orderby: "INSURED_ID", parameters: new { companyId, insuredId, packageId = InsuredList.PACKAGE_ID });
            if (PackageList != null && PackageList.Count > 0)
            {
                ViewBag.packageList = PackageList;
            }
            else
            {
                throw new Exception("Sigortalıya ait paket bulunamadı!");
            }

            var ExclusionList = new GenericRepository<V_InsuredExclusion>().FindBy("INSURED_ID =: insuredId", orderby: "INSURED_ID", parameters: new { insuredId });
            if (ExclusionList.Count > 0)
            {
                List<dynamic> insuredExclusionList = new List<dynamic>();

                foreach (var item in ExclusionList)
                {
                    dynamic listItem = new System.Dynamic.ExpandoObject();

                    if (item.RULE_CATEGORY_TYPE != null)
                    {
                        listItem.RULE_CATEGORY_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                        listItem.RESULT = Convert.ToString(item.RESULT);
                        string result_second = Convert.ToString(item.RESULT_SECOND);
                        listItem.RESULT_SECOND = result_second.IsNull() ? "-" : result_second;
                    }
                    else
                    {
                        listItem.RULE_CATEGORY_TYPE_TEXT = "EK PRİM";
                        listItem.RESULT = Convert.ToString(item.ADDITIONAL_PREMIUM);
                        listItem.RESULT_SECOND = "-";
                    }
                    listItem.EXCLUSION_DESCRIPTION = Convert.ToString(item.DESCRIPTION);

                    insuredExclusionList.Add(listItem);
                }
                ViewBag.exclusionList = insuredExclusionList;
            }

            var DeclerationList = new GenericRepository<V_InsuredNote>().FindBy("INSURED_ID=:insuredId AND NOTE_TYPE=:noteType", orderby: "INSURED_ID", parameters: new { insuredId, noteType = new DbString { Value = ((int)InsuredNoteType.BEYAN).ToString(), Length = 3 } });
            if (DeclerationList.Count > 0)
            {
                List<dynamic> notesList = new List<dynamic>();

                foreach (var item in DeclerationList)
                {
                    dynamic listItem = new System.Dynamic.ExpandoObject();

                    listItem.NOTE_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.InsuredNote, item.NOTE_TYPE);
                    listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                    notesList.Add(listItem);
                }

                ViewBag.declerationList = notesList;
            }
            var NoteList = new GenericRepository<V_ContactNote>().FindBy("CONTACT_ID=:id", parameters: new { id = InsuredList.CONTACT_ID }, orderby: "CONTACT_ID");
            if (NoteList.Count > 0)
            {
                List<dynamic> notesList = new List<dynamic>();

                foreach (var item in NoteList)
                {
                    dynamic listItem = new System.Dynamic.ExpandoObject();

                    listItem.NOTE_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Note, item.NOTE_TYPE);
                    listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                    notesList.Add(listItem);
                }
                ViewBag.noteList = notesList;
            }
            var BankAccountList = new GenericRepository<V_InsuredBankAccount>().FindBy("INSURED_ID =: insuredId", orderby: "INSURED_ID", parameters: new { insuredId });
            if (BankAccountList.Count > 0)
                ViewBag.bankAccountList = BankAccountList;

            var PhoneList = new GenericRepository<V_InsuredPhone>().FindBy("INSURED_ID =: insuredId", orderby: "INSURED_ID", parameters: new { insuredId });
            if (PhoneList.Count > 0)
                ViewBag.phoneList = PhoneList;

            var MailList = new GenericRepository<V_InsuredEmail>().FindBy("INSURED_ID =: insuredId", orderby: "INSURED_ID", parameters: new { insuredId });
            if (MailList.Count > 0)
                ViewBag.mailList = MailList;

            var Address = new GenericRepository<V_InsuredAddress>().FindBy("INSURED_ID =: insuredId", orderby: "INSURED_ID", parameters: new { insuredId }).FirstOrDefault();
            if (Address != null)
                ViewBag.address = Address;

            V_Policy policy = new GenericRepository<V_Policy>().FindBy($"POLICY_ID={InsuredList.POLICY_ID}", orderby: "POLICY_ID").FirstOrDefault();

            var result = new
            {
                summary = ViewBag.summary,
                packageList = ViewBag.packageList,
                noteList = ViewBag.noteList,
                declerationList = ViewBag.declerationList,
                bankAccountList = ViewBag.bankAccountList,
                phoneList = ViewBag.phoneList,
                mailList = ViewBag.mailList,
                address = ViewBag.address,
                exclusionList = ViewBag.exclusionList,
                specialRuleList = ViewBag.specialRuleList,
                ruleExtraList = ViewBag.ruleExtraList
            };

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(result),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        [LoginCompanyControl]
        public JsonResult GetPolicy()
        {
            try
            {
                var policyId = Request["policyId"];
                var companyId = HttpContext.Session["CompanyId"];

                var PolicyList = new GenericRepository<V_Policy>().FindBy("COMPANY_ID=:companyId AND POLICY_ID =: policyId", orderby: "POLICY_ID", parameters: new { companyId,policyId = long.Parse(policyId) });
                ViewBag.PolicyList = PolicyList;

                if (PolicyList == null || PolicyList.Count == 0)
                {
                    throw new Exception("Poliçe bulunamadı");
                }

                var insuredId = Request["insuredId"];
                var InsuredList = new GenericRepository<V_Insured>().FindBy("COMPANY_ID=:companyId AND INSURED_ID =: insuredId", orderby: "INSURED_ID", parameters: new { companyId,insuredId = long.Parse(insuredId) });
                ViewBag.InsuredList = InsuredList;

                if (InsuredList == null || InsuredList.Count == 0)
                {
                    throw new Exception("Sigortalı bulunamadı");
                }

                var PolicyEndorsementList = new GenericRepository<V_PolicyEndorsement>().FindBy("COMPANY_ID=:companyId AND POLICY_ID =: policyId", orderby: "POLICY_ID", parameters: new { companyId,policyId = long.Parse(policyId) });

                var result = new { policy = PolicyList[0], insured = InsuredList[0], policyEndorsementList = PolicyEndorsementList };

                return new JsonResult()
                {
                    MaxJsonLength = Int32.MaxValue,
                    Data = JsonConvert.SerializeObject(result),
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
                return new JsonResult()
                {
                    MaxJsonLength = Int32.MaxValue,
                    Data = JsonConvert.SerializeObject(ex.Message),
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }

        }

        [HttpPost]
        [LoginCompanyControl]
        public JsonResult GetPlanCoverage()
        {
            var insuredId = Request["insuredId"];
            var result = string.Empty;

            if (!string.IsNullOrEmpty(insuredId))
            {
                var rootPlanCoverageList = new GenericRepository<V_InsuredPackageRemaining>().FindBy(conditions: $"INSURED_ID = {insuredId}", orderby: "IS_MAIN_COVERAGE DESC", fetchDeletedRows: true, fetchHistoricRows: true);
                if (rootPlanCoverageList != null)
                {
                    result = rootPlanCoverageList.ToJSON();
                }
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        [LoginCompanyControl]
        public JsonResult GetPlanCoverageVariation()
        {
            var planCoverageId = Request["planCoverageId"];
            object result = null;

            if (!string.IsNullOrEmpty(planCoverageId))
            {
                var PlanCoverageVariationList = new GenericRepository<V_PlanCoverageVariation>().FindBy("PLAN_COVERAGE_ID =: planCoverageId", orderby: "PLAN_COVERAGE_ID", parameters: new { planCoverageId });
                if (PlanCoverageVariationList != null)
                {
                    List<dynamic> list = new List<dynamic>();

                    foreach (var variation in PlanCoverageVariationList)
                    {
                        //Add root item
                        dynamic variationItem = new System.Dynamic.ExpandoObject();

                        variationItem.ID = variation.PLAN_COVERAGE_ID;
                        variationItem.AGR_AGREEMENT_TYPE = variation.AGR_AGREEMENT_TYPE;
                        variationItem.AGR_COINSURANCE_RATIO = variation.AGR_COINSURANCE_RATIO;
                        variationItem.AGR_CURRENCY_ID = variation.AGR_CURRENCY_ID;
                        variationItem.AGR_DAY_LIMIT = variation.AGR_DAY_LIMIT;
                        variationItem.AGR_EXEMPTION_LIMIT = variation.AGR_EXEMPTION_LIMIT;
                        variationItem.AGR_EXEMPTION_TYPE = variation.AGR_EXEMPTION_TYPE;
                        variationItem.AGR_NUMBER_LIMIT = variation.AGR_NUMBER_LIMIT;
                        variationItem.AGR_PRICE_FACTOR = variation.AGR_PRICE_FACTOR;
                        variationItem.AGR_PRICE_LIMIT = variation.AGR_PRICE_LIMIT;

                        variationItem.NAG_AGREEMENT_TYPE = variation.NAG_AGREEMENT_TYPE;
                        variationItem.NAG_COINSURANCE_RATIO = variation.NAG_COINSURANCE_RATIO;
                        variationItem.NAG_CURRENCY_ID = variation.NAG_CURRENCY_ID;
                        variationItem.NAG_DAY_LIMIT = variation.NAG_DAY_LIMIT;
                        variationItem.NAG_EXEMPTION_LIMIT = variation.NAG_EXEMPTION_LIMIT;
                        variationItem.NAG_EXEMPTION_TYPE = variation.NAG_EXEMPTION_TYPE;
                        variationItem.NAG_NUMBER_LIMIT = variation.NAG_NUMBER_LIMIT;
                        variationItem.NAG_PRICE_FACTOR = variation.NAG_PRICE_FACTOR;
                        variationItem.NAG_PRICE_LIMIT = variation.NAG_PRICE_LIMIT;

                        variationItem.EMG_AGREEMENT_TYPE = variation.EMG_AGREEMENT_TYPE;
                        variationItem.EMG_COINSURANCE_RATIO = variation.EMG_COINSURANCE_RATIO;
                        variationItem.EMG_CURRENCY_ID = variation.EMG_CURRENCY_ID;
                        variationItem.EMG_DAY_LIMIT = variation.EMG_DAY_LIMIT;
                        variationItem.EMG_EXEMPTION_LIMIT = variation.EMG_EXEMPTION_LIMIT;
                        variationItem.EMG_EXEMPTION_TYPE = variation.EMG_EXEMPTION_TYPE;
                        variationItem.EMG_NUMBER_LIMIT = variation.EMG_NUMBER_LIMIT;
                        variationItem.EMG_PRICE_FACTOR = variation.EMG_PRICE_FACTOR;
                        variationItem.EMG_PRICE_LIMIT = variation.EMG_PRICE_LIMIT;
                        list.Add(variationItem);

                    }
                    result = list.ToJSON();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [LoginCompanyControl]
        public JsonResult Details(Int64 insuredId)
        {
            var insured = new InsuredSummary();
            var insuredItem = new GenericRepository<V_Insured>().FindBy("INSURED_ID=:insuredId", orderby: "INSURED_ID", parameters: new { insuredId }).FirstOrDefault();
            if (insuredItem != null)
            {
                insured.IDENTITY_NO = insuredItem.IDENTITY_NO;
                insured.FIRST_NAME = insuredItem.FIRST_NAME;
                insured.LAST_NAME = insuredItem.LAST_NAME;
                string BIRTHDATE = Convert.ToString(insuredItem.BIRTHDATE);
                insured.BIRTHDATE = BIRTHDATE.IsDateTime() ? DateTime.Parse(BIRTHDATE).ToString("dd-MM-yyyy") : null;
                insured.GENDER_NAME = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Gender, insuredItem.GENDER);
                insured.INDIVIDUAL_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Individual, insuredItem.INDIVIDUAL_TYPE);
                insured.PASSPORT_NO = insuredItem.PASSPORT_NO;
                insured.TAX_NUMBER = insuredItem.TAX_NUMBER;
                insured.STATUS_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Status, insuredItem.STATUS);
                string FIRST_INSURED_DATE = Convert.ToString(insuredItem.FIRST_INSURED_DATE);
                insured.FIRST_INSURED_DATE = FIRST_INSURED_DATE.IsDateTime() ? DateTime.Parse(FIRST_INSURED_DATE).ToString("dd-MM-yyyy") : null;
                string FIRST_AMBULANT_COVERAGE_DATE = Convert.ToString(insuredItem.FIRST_AMBULANT_COVERAGE_DATE);
                insured.FIRST_AMBULANT_COVERAGE_DATE = FIRST_AMBULANT_COVERAGE_DATE.IsDateTime() ? DateTime.Parse(FIRST_AMBULANT_COVERAGE_DATE).ToString("dd-MM-yyyy") : null;
                string BIRTH_COVERAGE_DATE = Convert.ToString(insuredItem.BIRTH_COVERAGE_DATE);
                insured.BIRTH_COVERAGE_DATE = BIRTH_COVERAGE_DATE.IsDateTime() ? DateTime.Parse(BIRTH_COVERAGE_DATE).ToString("dd-MM-yyyy") : null;
                string COMPANY_ENTRANCE_DATE = Convert.ToString(insuredItem.COMPANY_ENTRANCE_DATE);
                insured.FIRST_COMPANY_INSURED_DATE = COMPANY_ENTRANCE_DATE.IsDateTime() ? DateTime.Parse(COMPANY_ENTRANCE_DATE).ToString("dd-MM-yyyy") : null;
                insured.IS_VIP = insuredItem.IS_VIP;
                insured.VIP_TYPE = insuredItem.VIP_TYPE;
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(insured),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [LoginCompanyControl]
        public JsonResult Exclusions(Int64 insuredId)
        {
            object result = null;
            var insuredExclusionList = new GenericRepository<V_InsuredExclusion>().FindBy("INSURED_ID=:insuredId", orderby: "", parameters: new { insuredId });
            if (insuredExclusionList != null)
            {
                List<dynamic> list = new List<dynamic>();

                foreach (var insuredExclusion in insuredExclusionList)
                {
                    //Add root item
                    dynamic insuredExclusionItem = new System.Dynamic.ExpandoObject();

                    insuredExclusionItem.DESCRIPTION = insuredExclusion.DESCRIPTION;
                    string END_DATE = Convert.ToString(insuredExclusion.END_DATE);
                    insuredExclusionItem.END_DATE = END_DATE.IsDateTime() ? DateTime.Parse(END_DATE).ToString("dd-MM-yyyy") : "Bulunamadı";
                    insuredExclusionItem.EXCLUSION_ID = insuredExclusion.EXCLUSION_ID;
                    insuredExclusionItem.INSURED_EXCLUSION = insuredExclusion.INSURED_EXCLUSION;
                    insuredExclusionItem.SP_RESPONSE_DATE = "Bulunamadı";
                    string START_DATE = Convert.ToString(insuredExclusion.START_DATE);
                    insuredExclusionItem.START_DATE = START_DATE.IsDateTime() ? DateTime.Parse(START_DATE).ToString("dd-MM-yyyy") : "Bulunamadı";
                    insuredExclusionItem.USERNAME = "Bulunamadı";
                    insuredExclusionItem.STATUS_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Status, insuredExclusion.STATUS);
                    insuredExclusionItem.STATUS = insuredExclusion.STATUS;
                    list.Add(insuredExclusionItem);
                }
                result = list.ToJSON();
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(result),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [LoginCompanyControl]
        public JsonResult Notes(Int64 contactId)
        {
            object result = null;
            var insuredNoteList = new GenericRepository<V_ContactNote>().FindBy("CONTACT_ID=:contactId AND NOTE_TYPE IN(:noteTypeList)", orderby: "CONTACT_ID",
                                    parameters: new { contactId, noteTypeList = ((int)InsuredNoteType.OZEL).ToString() + ", " + ((int)InsuredNoteType.BASIM).ToString() });
            if (insuredNoteList != null)
            {
                List<dynamic> list = new List<dynamic>();

                foreach (var insuredNote in insuredNoteList)
                {
                    //Add root item
                    dynamic insuredNoteItem = new System.Dynamic.ExpandoObject();

                    insuredNoteItem.DESCRIPTION = insuredNote.DESCRIPTION.RemoveRepeatedWhiteSpace();
                    insuredNoteItem.NOTE_ID = insuredNote.NOTE_ID;
                    insuredNoteItem.INSURED_NOTE_ID = insuredNote.CONTACT_NOTE_ID;
                    insuredNoteItem.NOTE_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Note, insuredNote.NOTE_TYPE);
                    string SP_RESPONSE_DATE = "";
                    insuredNoteItem.SP_RESPONSE_DATE = SP_RESPONSE_DATE.IsDateTime() ? DateTime.Parse(SP_RESPONSE_DATE).ToString("dd-MM-yyyy") : "Bulunamadı";
                    insuredNoteItem.USERNAME = "Bulunamadı";
                    insuredNoteItem.STATUS_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Status, insuredNote.STATUS);
                    insuredNoteItem.STATUS = insuredNote.STATUS;
                    list.Add(insuredNoteItem);
                }
                result = list.ToJSON();
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(result),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [LoginCompanyControl]
        public JsonResult TransferExclusions(Int64 insuredId)
        {
            object result = null;
            var insuredTransferExclusionList = new GenericRepository<V_InsuredNote>().FindBy("INSURED_ID=:insuredId AND NOTE_TYPE=:noteType", orderby: "", parameters: new { insuredId, noteType = new DbString { Value = ((int)InsuredNoteType.GECİS).ToString(), Length = 3 } });
            if (insuredTransferExclusionList != null)
            {
                List<dynamic> list = new List<dynamic>();

                foreach (var insuredTransferExclusion in insuredTransferExclusionList)
                {
                    //Add root item
                    dynamic insuredTransferExclusionItem = new System.Dynamic.ExpandoObject();

                    insuredTransferExclusionItem.DESCRIPTION = insuredTransferExclusion.DESCRIPTION.RemoveRepeatedWhiteSpace();
                    insuredTransferExclusionItem.NOTE_ID = insuredTransferExclusion.NOTE_ID;
                    insuredTransferExclusionItem.INSURED_NOTE_ID = insuredTransferExclusion.INSURED_NOTE_ID;
                    string SP_RESPONSE_DATE = "";
                    insuredTransferExclusionItem.SP_RESPONSE_DATE = SP_RESPONSE_DATE.IsDateTime() ? DateTime.Parse(SP_RESPONSE_DATE).ToString("dd-MM-yyyy") : "Bulunamadı";
                    insuredTransferExclusionItem.USERNAME = "Bulunamadı";
                    insuredTransferExclusionItem.STATUS_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Status, insuredTransferExclusion.STATUS);
                    insuredTransferExclusionItem.STATUS = insuredTransferExclusion.STATUS;
                    list.Add(insuredTransferExclusionItem);
                }
                result = list.ToJSON();
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(result),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [LoginCompanyControl]
        public JsonResult Declerations(Int64 insuredId)
        {
            object result = null;
            var insuredDeclerationList = new GenericRepository<V_InsuredNote>().FindBy("INSURED_ID=:insuredId AND NOTE_TYPE=:noteType", orderby: "", parameters: new { insuredId, noteType = new DbString { Value = ((int)InsuredNoteType.BEYAN).ToString(), Length = 3 } });
            if (insuredDeclerationList != null)
            {
                List<dynamic> list = new List<dynamic>();

                foreach (var insuredDecleration in insuredDeclerationList)
                {
                    //Add root item
                    dynamic insuredDeclerationItem = new System.Dynamic.ExpandoObject();

                    insuredDeclerationItem.DESCRIPTION = insuredDecleration.DESCRIPTION.RemoveRepeatedWhiteSpace();
                    insuredDeclerationItem.NOTE_ID = insuredDecleration.NOTE_ID;
                    insuredDeclerationItem.INSURED_NOTE_ID = insuredDecleration.INSURED_NOTE_ID;
                    string SP_RESPONSE_DATE = "";
                    insuredDeclerationItem.SP_RESPONSE_DATE = SP_RESPONSE_DATE.IsDateTime() ? DateTime.Parse(SP_RESPONSE_DATE).ToString("dd-MM-yyyy") : "Bulunamadı";
                    insuredDeclerationItem.USERNAME = "Bulunamadı";
                    insuredDeclerationItem.STATUS_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Status, insuredDecleration.STATUS);
                    insuredDeclerationItem.STATUS = insuredDecleration.STATUS;
                    list.Add(insuredDeclerationItem);
                }
                result = list.ToJSON();
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(result),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult Coverages(Int64 insuredId, Int64 packageId)
        {
            var insuredCoverageResult = new GenericRepository<V_PackageCoverageList>().FindBy("PACKAGE_ID =:packageId", orderby: "PACKAGE_ID", parameters: new { packageId }, fetchHistoricRows: true, fetchDeletedRows: true);
            if (insuredCoverageResult != null)
            {
                List<dynamic> insuredCoverageList = new List<dynamic>();

                foreach (var item in insuredCoverageResult)
                {
                    dynamic listItem = new System.Dynamic.ExpandoObject();

                    listItem.COVERAGE_ID = Convert.ToString(item.COVERAGE_ID);
                    listItem.COVERAGE_NAME = Convert.ToString(item.COVERAGE_NAME);
                    listItem.MAIN_COVERAGE_NAME = Convert.ToString(item.MAIN_COVERAGE_NAME);

                    insuredCoverageList.Add(listItem);
                }


                return new JsonResult()
                {
                    MaxJsonLength = Int32.MaxValue,
                    Data = insuredCoverageList.ToJSON(),
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return null;
        }

        [LoginCompanyControl]
        public JsonResult Check()
        {
            var result = new AjaxResultDto<InsuredCheckResult>();
            try
            {
                var insuredId = Request["insuredId"];
                Session["insuredId"] = insuredId;
                Console.WriteLine(insuredId);

                // Sigortalı HASARA AÇIK mı kontrolü
                V_Insured Insured = new GenericRepository<V_Insured>().FindBy($"INSURED_ID={long.Parse(insuredId)}", orderby: "").FirstOrDefault();
                if (Insured != null)
                {
                    if (Insured.IS_OPEN_TO_CLAIM.Equals("0"))
                    {
                        throw new Exception("Sigortalı HASARA KAPALI, tazminat girişi yapılamaz!");
                    }

                    if (Insured.PRODUCT_TYPE == ((int)ProductType.TSS).ToString())
                    {
                        result.ResultCode = "400";
                    }
                    else
                    {
                        result.ResultCode = "100";
                    }
                }
                else
                {
                    throw new Exception("Sigortalı bulunamadı!");
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        #region DropDown fill methods 

        private void FillCompanies()
        {
            var v_company = new GenericRepository<V_Company>().FindBy(orderby: "COMPANY_ID");
            ViewBag.v_company = v_company;
        }

        private void FillProducts()
        {
            var v_product = new GenericRepository<V_Product>().FindBy(orderby: "PRODUCT_ID");
            ViewBag.v_product = v_product;
        }

        private void FillProducts(Int64 Id)
        {
            var v_product = new GenericRepository<V_Product>().FindBy("COMPANY_ID =:id", orderby: "PRODUCT_ID", parameters: new { id = Id });
            ViewBag.v_product = v_product;
        }

        #endregion
    }
}