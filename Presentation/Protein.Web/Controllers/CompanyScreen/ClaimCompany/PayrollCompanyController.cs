﻿using Protein.Business.Abstract.Print;
using Protein.Business.Enums.Import;
using Protein.Business.Print;
using Protein.Business.Print.ClaimProviderPaymentList;
using Protein.Business.Print.ClaimProviderReturnForm;
using Protein.Common.Constants;
using Protein.Common.Dto;
using Protein.Common.Entities;
using Protein.Common.Extensions;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Web.ActionFilter.LoginAttr;
using Protein.Web.Models.ExportModel;
using Protein.Web.Models.HistoryModel;
using Protein.Web.Models.PayrollModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Web.Controllers.CompanyScreen.ClaimCompany
{
    [LoginCompanyControl]
    public class PayrollCompanyController : Controller
    {
        // GET: Payroll
        [LoginCompanyControl]
        public ActionResult Index()
        {
            ViewBag.Title = "İcmal/Ödeme Listesi";
            PayrollVM vm = new PayrollVM();

            vm.ExportVM.ViewName = "V_PROVIDER_PAYROLL";
            vm.ExportVM.SetExportColumns();
            vm.ExportVM.ExportType = ExportType.Excel;

            TempData["Alert"] = TempData["Alert"] ?? string.Empty;
            
            ViewBag.PayrollTypeList = LookupHelper.GetLookupData(LookupTypes.Payroll);
            ViewBag.PayrollCategoryTypeList = LookupHelper.GetLookupData(LookupTypes.PayrollCategory);
            ViewBag.PaymentTypeList = LookupHelper.GetLookupData(LookupTypes.Payment);
            ViewBag.PayrollStatusList = LookupHelper.GetLookupData(LookupTypes.PayrollStatus);

            var ProviderGroupList = new GenericRepository<ProviderGroup>().FindBy(orderby: "NAME");
            ViewBag.ProviderGroupList = ProviderGroupList;



            return View(vm);
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetPayrollList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];

            PayrollVM vm = new PayrollVM();
            vm.HistoryVM.ViewName = "V_PROVIDER_PAYROLL";
            vm.HistoryVM.HistoryTitle = "Zarf";

            vm.ExportVM.WhereCondition = "";

            vm.ImportVM.ImportObjectType = ImportObjectType.PayrollStatus;

            ViewResultDto<List<V_Payroll>> PaymentList = new ViewResultDto<List<V_Payroll>>();
            PaymentList.Data = new List<V_Payroll>();
            if (isFilter == "1")
            {
                //long companyId = 0;
                long payrollId = 0;
                long claimId = 0;
                long providerId = 0;
                string paymentStartDate = Request["paymentStartDate"];
                string paymentEndDate = Request["paymentEndDate"];

                var companyId = HttpContext.Session["CompanyId"];
                String whereConditition = companyId != null ? $" COMPANY_ID={companyId} AND" : "";
                if (!String.IsNullOrEmpty(form["COMPANY_ID"]))
                {
                    companyId = Convert.ToInt32(form["COMPANY_ID"]);
                    whereConditition += $"COMPANY_ID = {companyId} AND";
                }
                if (!String.IsNullOrEmpty(form["PROVIDER_Group"]))
                {
                    whereConditition += $" PROVIDER_GROUP_ID = {form["PROVIDER_Group"]} AND";
                }
                if (!String.IsNullOrEmpty(form["payrollNo"]))
                {
                    payrollId = Convert.ToInt32(form["payrollNo"]);
                    whereConditition += $" (PAYROLL_ID = {payrollId} OR OLD_PAYROLL_ID={payrollId}) AND";
                }
                if (!String.IsNullOrEmpty(form["providerId"]))
                {
                    providerId = Convert.ToInt32(form["providerId"]);
                    whereConditition += $" PROVIDER_ID = {providerId} AND";
                }
                if (!String.IsNullOrEmpty(form["payrollStatus[]"]))
                {
                    whereConditition += $" STATUS IN ({form["payrollStatus[]"]}) AND";
                }
                if (!String.IsNullOrEmpty(form["paymentStartDate"])) 
                {
                    whereConditition += $" PAYMENT_DATE >= TO_DATE('{DateTime.Parse(form["paymentStartDate"])}', 'DD.MM.YYYY HH24:MI:SS') AND";
                }
                if (!String.IsNullOrEmpty(form["paymentEndDate"]))
                {
                    whereConditition += $" PAYMENT_DATE <= TO_DATE('{DateTime.Parse(form["paymentEndDate"])}', 'DD.MM.YYYY HH24:MI:SS') AND";
                }

                PaymentList = new V_PayrollRepository().FindByPaged(conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                                                             orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection}" : "CLAIM_ID",
                                                                             fetchDeletedRows: true,
                                                                             fetchHistoricRows: true,
                                                                             pageNumber: start,
                                                                             rowsPerPage: length);

                vm.ExportVM.ViewName = "V_PROVIDER_PAYROLL";
                vm.ExportVM.WhereCondition = (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3));
                vm.ExportVM.SetExportColumns();
            }
            return Json(new { data = PaymentList.Data, draw = Request["draw"], recordsTotal = PaymentList.TotalItemsCount, recordsFiltered = PaymentList.TotalItemsCount, whereConditition = vm.ExportVM.WhereCondition }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetReport()
        {
            var type = Request["Type"];
            var response = new PrintResponse();
            var payrollId = Request["PayrollId"];

            if (payrollId.IsInt64() && long.Parse(payrollId) > 0)
            {
                if (type == "0")
                {
                    var claim = new GenericRepository<ProteinEntities.Claim>().FindBy($"PAYROLL_ID={payrollId}");
                    var claimIdList = claim.Select(c => c.Id).ToList();
                    IPrint<PrintClaimProviderReturnFormReq> printt = new PrintClaimProviderReturnForm();
                    response = printt.DoWork(new PrintClaimProviderReturnFormReq
                    {
                        ClaimIdList = claimIdList
                    });
                }
                else if (type == "1")
                {
                    IPrint<PrintClaimProviderPaymentListReq> printt = new PrintClaimProviderPaymentList();
                    response = printt.DoWork(new PrintClaimProviderPaymentListReq
                    {
                        PayrollId = long.Parse(payrollId)
                    });
                }
            }
            return new JsonResult()
            {
                Data = new { FileUrl = response.Url, FileName = response.FileName, Message = response.Message, Code = response.Code }
            };
        }

        #region Bill
        [HttpPost]
        public JsonResult GetBill()
        {
            var payrollId = Request["payrollId"];
            var companyId = HttpContext.Session["CompanyId"];

            var BillList = new GenericRepository<V_Claim>().FindBy($"COMPANY_ID={companyId} AND PAYROLL_ID={payrollId}", orderby: "BILL_NO");
            List<dynamic> listBill = new List<dynamic>();

            if (BillList != null)
            {
                int i = 1;

                foreach (var item in BillList)
                {
                    dynamic listItem = new System.Dynamic.ExpandoObject();
                    listItem.COMPANY_NAME = item.COMPANY_NAME;
                    listItem.PAYROLL_ID = item.PAYROLL_ID;
                    listItem.CLAIM_ID = item.CLAIM_ID;
                    listItem.FIRST_NAME = item.FIRST_NAME + " " + item.LAST_NAME;
                    listItem.IDENTITY_NO = item.IDENTITY_NO;
                    listItem.POLICY_NUMBER = item.POLICY_NUMBER;
                    listItem.POLICY_START_DATE = item.POLICY_START_DATE != null ? ((DateTime)item.POLICY_START_DATE).ToString("dd-MM-yyyy") : string.Empty;
                    listItem.PRODUCT_NAME = item.PRODUCT_NAME;
                    listItem.BILL_NO = item.BILL_NO;
                    listItem.BILL_DATE = item.BILL_DATE != null ? ((DateTime)item.BILL_DATE).ToString("dd-MM-yyyy") : string.Empty;
                    listItem.STATUS_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, item.STATUS);
                    listItem.CLAIM_STATUS = item.STATUS;
                    listItem.PAYROLL_STATUS = item.PAYROLL_STATUS;
                    listItem.PAYMENT_DATE = item.PAYMENT_DATE != null ? ((DateTime)item.PAYMENT_DATE).ToString("dd-MM-yyyy") : string.Empty;
                    listItem.AMOUNT = item.AMOUNT;
                    listItem.BANK_NAME = item.BANK_NAME;
                    listItem.IBAN = item.IBAN;
                    listBill.Add(listItem);

                    i++;
                }

                List<string> claimStatusCount = new List<string>();

                claimStatusCount.Add("BEKLEME - " + BillList.Count(c => c.STATUS == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString() || c.STATUS == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString()));
                claimStatusCount.Add("İPTAL - " + BillList.Count(c => c.STATUS == ((int)ClaimStatus.IPTAL).ToString()));
                claimStatusCount.Add("ÖDENDİ - " + BillList.Count(c => c.STATUS == ((int)ClaimStatus.ODENDI).ToString()));
                claimStatusCount.Add("ÖDENECEK - " + BillList.Count(c => c.STATUS == ((int)ClaimStatus.ODENECEK).ToString()));
                claimStatusCount.Add("PROVIZYON ONAY - " + BillList.Count(c => c.STATUS == ((int)ClaimStatus.PROZIYON_ONAY).ToString()));
                claimStatusCount.Add("RET - " + BillList.Count(c => c.STATUS == ((int)ClaimStatus.RET).ToString()));

                ViewBag.claimStatusCount = claimStatusCount;

                ViewBag.BillList = listBill;
            }

            var result = new
            {
                listBill = ViewBag.BillList,
                claimStatusCount = ViewBag.claimStatusCount
            };

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult FilterBill(FormCollection form)
        {
            object result = null;
            var companyId = HttpContext.Session["CompanyId"];

            var payroll = new GenericRepository<Payroll>().FindBy($"COMPANY_ID={companyId} AND ID=:id", parameters: new { id = form["PayrollId"] }).FirstOrDefault();
            if (payroll != null)
            {
                dynamic parameters = new System.Dynamic.ExpandoObject();
                parameters.paymentMethod = payroll.Type;
                parameters.claimType = payroll.Category;
                parameters.companyId = payroll.CompanyId;

                String whereConditition = $" PAYROLL_ID IS NULL AND CLAIM_BILL_ID IS NOT NULL AND COMPANY_ID=:companyId AND PAYMENT_METHOD=:paymentMethod AND CLAIM_TYPE=:claimType AND STATUS!={((int)ClaimStatus.IPTAL)} AND";
                if (payroll.Type == ((int)PayrollType.KURUM).ToString())
                {
                    parameters.providerId = payroll.ProviderId;
                    whereConditition += " PROVIDER_ID =:providerId AND";
                }
                //else
                //{
                //    parameters.providerId = form["payrollSelectProvider"];
                //    whereConditition += !String.IsNullOrEmpty(form["payrollSelectProvider"]) ? " PROVIDER_ID =:providerId AND" : "";
                //}
                whereConditition += !String.IsNullOrEmpty(form["claimNo"]) ? $" CLAIM_ID={long.Parse(form["claimNo"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["billNo"]) ? $" BILL_NO = {long.Parse(form["billNo"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimInsuredName"]) ? $" FIRST_NAME LIKE '%{form["claimInsuredName"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimInsuredSurname"]) ? $" LAST_NAME LIKE '%{form["claimInsuredSurname"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["billStartDate"]) ? $" BILL_DATE >= TO_DATE('{ DateTime.Parse(form["billStartDate"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["billEndDate"]) ? $" BILL_DATE <= TO_DATE('{ DateTime.Parse(form["billEndDate"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimStartDate"]) ? $" CLAIM_DATE >= TO_DATE('{ DateTime.Parse(form["claimStartDate"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimEndDate"]) ? $" CLAIM_DATE <= TO_DATE('{ DateTime.Parse(form["claimEndDate"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";

                List<V_Claim> resultClaimBill = new GenericRepository<V_Claim>().FindBy((string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)), orderby: "CLAIM_ID", parameters: parameters);
                result = resultClaimBill.ToJSON();
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        #endregion

        #region DropDown fill methods 

        private void FillCompanies()
        {
            var v_company = new GenericRepository<V_Company>().FindBy(orderby: "COMPANY_ID");
            ViewBag.v_company = v_company;
        }

        private void FillProducts()
        {
            var v_product = new GenericRepository<V_Product>().FindBy(orderby: "PRODUCT_ID");
            ViewBag.v_product = v_product;
        }

        private void FillProducts(Int64 Id)
        {
            var v_product = new GenericRepository<V_Product>().FindBy("COMPANY_ID =:id", orderby: "PRODUCT_ID", parameters: new { id = Id });
            ViewBag.v_product = v_product;
        }

        #endregion
    }
}