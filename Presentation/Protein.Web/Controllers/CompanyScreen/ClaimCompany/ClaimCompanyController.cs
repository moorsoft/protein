﻿using Newtonsoft.Json;
using Protein.Business.Abstract.Print;
using Protein.Business.Enums.Import;
using Protein.Business.Print;
using Protein.Business.Print.BillAprovalForm;
using Protein.Business.Print.ClaimMissing;
using Protein.Business.Print.ClaimProviderReturn;
using Protein.Business.Print.ClaimRejectForm;
using Protein.Business.Print.ProvisionApproval;
using Protein.Business.Print.ProvisionMissingDocForm;
using Protein.Business.Print.ProvisionReject;
using Protein.Common.Constants;
using Protein.Common.Dto;
using Protein.Common.Entities;
using Protein.Common.Extensions;
using Protein.Common.Messaging;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.InsuranceCompanies;
using Protein.Data.ExternalServices.RxMedia;
using Protein.Data.ExternalServices.RxMedia.Result;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Web.ActionFilter.LoginAttr;
using Protein.Web.Models.ClaimModel;
using Protein.Web.Models.ExportModel;
using Protein.Web.Models.ListView;
using Protein.Web.Models.MediaModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Web.Controllers.CompanyScreen.ClaimCompany
{
    [LoginCompanyControl]
    public class ClaimCompanyController : Controller
    {
        [LoginCompanyControl]
        public ActionResult Index()
        {
            ViewBag.Title = "Provizyon Listesi";
            #region Export - Import 
            ExportVM vm = new ExportVM();
            vm.ViewName = "V_PROVIDER_CLAIM";
            vm.SetExportColumns();
            vm.ExportType = ExportType.Excel;

          

            #endregion

            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            var insuredId = Request["insuredId"];

            if (insuredId.IsInt64() && long.Parse(insuredId) > 0)
                ViewBag.SelectInsuredId = long.Parse(insuredId);
            else
                ViewBag.SelectInsuredId = 0;

            ViewBag.ClaimStatusList = LookupHelper.GetLookupData(LookupTypes.ClaimStatus);
            ViewBag.ClaimSourceTypeList = LookupHelper.GetLookupData(LookupTypes.ClaimSource);
            ViewBag.claimListClaimTypes = LookupHelper.GetLookupData(LookupTypes.Claim);
            ViewBag.ProviderGroupList = new GenericRepository<ProviderGroup>().FindBy();
            ViewBag.ICDTypeList = LookupHelper.GetLookupData(LookupTypes.ICD);
            ViewBag.ClaimMissingDocTypeList = LookupHelper.GetLookupData(LookupTypes.ClaimMissingDoc);
            ViewBag.PayrollStatusList = LookupHelper.GetLookupData(LookupTypes.PayrollStatus, showChoose: true);
            ViewBag.PayrollTypeList = LookupHelper.GetLookupData(LookupTypes.Payroll);
            ViewBag.PayrollCategoryList = LookupHelper.GetLookupData(LookupTypes.PayrollCategory);
            ViewBag.ClaimSourceTypeList = LookupHelper.GetLookupData(LookupTypes.ClaimSource);
            ViewBag.StaffContractTypeList = LookupHelper.GetLookupData(LookupTypes.StaffContract);
            ViewBag.PaymentTypeList = LookupHelper.GetLookupData(LookupTypes.Payment);
            ViewBag.CurrencyTypeList = LookupHelper.GetLookupData(LookupTypes.Currency);
            ViewBag.AgreementTypeList = LookupHelper.GetLookupData(LookupTypes.Agreement);
            ViewBag.ExemptionTypeList = LookupHelper.GetLookupData(LookupTypes.Exemption);
            ViewBag.EventTypeList = LookupHelper.GetLookupData(LookupTypes.Event);
            ViewBag.ClaimTypeList = LookupHelper.GetLookupData(LookupTypes.Claim);


            return View(vm);
        }

        [LoginCompanyControl]
        public ActionResult MissingDoc()
        {
            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            var claimId = Request["claimId"];
            if (claimId.IsInt64() && long.Parse(claimId) > 0)
            {
                ViewBag.ClaimId = long.Parse(claimId);
            }
            else
            {
                TempData["Alert"] = $"swAlert('Hata','Hasar Bilgisi Bulunamadı','warning')";
                return RedirectToAction("Index");
            }
            ViewBag.ClaimMissingDocTypeList = LookupHelper.GetLookupData(LookupTypes.ClaimMissingDoc);

            return View();
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetClaimProcess(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            var claimlist = new ViewResultDto<List<V_ClaimProcessList>>();
            claimlist.Data = new List<V_ClaimProcessList>();
            if (!form["isOpenProcessModal"].IsNull() && form["isOpenProcessModal"] == "1" && form["ClaimId"].IsInt64() && long.Parse(form["ClaimId"]) > 0)
            {
                String whereConditition = form["ClaimId"].IsInt64() ? $" CLAIM_ID = {long.Parse(form["ClaimId"])} AND" : "";
                whereConditition += !form["ProcessName"].IsNull() ? $" nls_upper(PROCESS_NAME,'NLS_SORT = XTURKISH') LIKE ('%{form["ProcessName"].ToUpper()}%') AND" : "";
                whereConditition += !form["ProcessCode"].IsNull() ? $" PROCESS_CODE LIKE '%{form["ProcessCode"]}%' AND" : "";

                claimlist = new GenericRepository<V_ClaimProcessList>().FindByPaged(
                                  conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                  orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},CLAIM_ID DESC" : "CLAIM_ID DESC",
                                  pageNumber: start,
                                  rowsPerPage: length,
                                  fetchDeletedRows: true,
                                  fetchHistoricRows: true);
            }


            return Json(new { data = claimlist.Data, draw = Request["draw"], recordsTotal = claimlist.TotalItemsCount, recordsFiltered = claimlist.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetIcdList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            string name = form["icdName"];
            string code = form["icdCode"];

            List<Process> icdProcesses = ICD10StraightHelper.GetICD10Data();
            icdProcesses = icdProcesses.Where(p => p.Name.ToUpper().Contains(name.ToUpper()) && p.Code.ToUpper().Contains(code.ToUpper())).ToList();

            return Json(new { data = icdProcesses, draw = Request["draw"], recordsTotal = icdProcesses.Count, recordsFiltered = icdProcesses.Count }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetClaimList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];
            ExportVM vm = new ExportVM();
            vm.WhereCondition = "";
            vm.ViewName = "V_PROVIDER_CLAIM";
            vm.SetExportColumns();
            
            //var draw = "1";// Request["draw"];
            ViewResultDto<List<V_Claim>> claimlist = new ViewResultDto<List<V_Claim>>();
            claimlist.Data = new List<V_Claim>();
            if (isFilter == "1")
            {
                string hint = "";
                var companyId = HttpContext.Session["CompanyId"];
                String whereConditition = companyId!=null ? $" COMPANY_ID={companyId} AND" : "";
                whereConditition += (form["claimListInsuredId"].IsInt64() && long.Parse(form["claimListInsuredId"]) > 0) ? $" INSURED_ID={long.Parse(form["claimListInsuredId"])} AND" : "";
                whereConditition += (form["claimListContactId"].IsInt64() && long.Parse(form["claimListContactId"]) > 0) ? $" CONTACT_ID={long.Parse(form["claimListContactId"])} AND" : "";
                //whereConditition += form["claimListClaimNo"].IsInt64() ? $" CLAIM_ID = {long.Parse(form["claimListClaimNo"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimListMedulaNo"]) ? $" MEDULA_NO = '{form["claimListMedulaNo"]}' AND" : "";
                whereConditition += form["claimListBillNo"].IsInt64() ? $" BILL_NO={long.Parse(form["claimListBillNo"])} AND" : "";
                whereConditition += form["claimListPayrollNo"].IsInt64() ? $" PAYROLL_ID ={long.Parse(form["claimListPayrollNo"])} AND" : "";
                whereConditition += form["claimListOldPayrollNo"].IsInt64() ? $" OLD_PAYROLL_ID ={long.Parse(form["claimListOldPayrollNo"])} AND" : "";
                whereConditition += form["claimListPolicyNo"].IsInt64() ? $" POLICY_NUMBER = {form["claimListPolicyNo"]} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimListClaimType"]) ? $" CLAIM_TYPE= {form["claimListClaimType"]} AND" : "";
                whereConditition += form["claimListClaimDateStart"].IsDateTime() ? $" CLAIM_DATE >= TO_DATE('{ DateTime.Parse(form["claimListClaimDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["claimListClaimDateEnd"].IsDateTime() ? $" CLAIM_DATE <= TO_DATE('{ DateTime.Parse(form["claimListClaimDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["claimListInsuredIdentityNo"].IsInt64() ? $" (IDENTITY_NO={form["claimListInsuredIdentityNo"]} OR TAX_NUMBER={form["claimListInsuredIdentityNo"]} OR  PASSPORT_NO='{form["claimListInsuredIdentityNo"]}') AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimListInsuredName"]) ? $" FIRST_NAME LIKE '%{form["claimListInsuredName"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimListInsuredSurname"]) ? $" LAST_NAME LIKE '%{form["claimListInsuredSurname"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimListProvider"]) ? $" PROVIDER_ID ={(form["claimListProvider"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimListInsurer"]) ? $" INSURER_NAME LIKE '%{form["claimListInsurer"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimListInsurerLastName"]) ? $" INSURER_NAME LIKE '%{form["claimListInsurerLastName"]}%' AND" : "";
                whereConditition += form["claimListProductId"].IsInt64() ? $"  PRODUCT_ID={long.Parse(form["claimListCompany"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimListState[]"]) ? $" STATUS IN({form["claimListState[]"]}) AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimListSourceType[]"]) ? $" SOURCE_TYPE IN({form["claimListSourceType[]"]}) AND" : "";
                whereConditition += form["insurerTCKN"].IsInt64() ?  $" (IDENTITY_NO = {form["insurerTCKN"]} OR TAX_NUMBER = {form["insurerTCKN"]} OR PASSPORT_NO = '{form["insurerTCKN"]}') " : "";

                if (form["claimListClaimNo"].IsInt64())
                {
                    whereConditition += $" CLAIM_ID = {long.Parse(form["claimListClaimNo"])} AND";
                    hint = "/*+ USE_HASH(claim_id) ORDERED */";
                }

                claimlist = new GenericRepository<V_Claim>().FindByPaged(
                                  conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                  orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},CLAIM_ID DESC" : "CLAIM_ID DESC",
                                  pageNumber: start,
                                  rowsPerPage: length,
                                  hint: hint);

                #region Export
                vm.WhereCondition = (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3));
                vm.SetExportColumns();
                #endregion
            }
            return Json(new { data = (claimlist.Data == null ? new List<V_Claim>() : claimlist.Data), draw = Request["draw"], recordsTotal = claimlist.TotalItemsCount, recordsFiltered = claimlist.TotalItemsCount, whereConditition = vm.WhereCondition }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginCompanyControl]
        public ActionResult GetMissingDoc(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            var claimId = form["ClaimId"];
            var claimMissingDoclist = new ViewResultDto<List<ClaimMissingDoc>>();
            if (claimId.IsInt64())
            {
                claimMissingDoclist = new GenericRepository<ClaimMissingDoc>().FindByPaged(
                                            conditions: $"CLAIM_ID={claimId}" + (form["isAll"].IsNull() ? " AND INCOMING_DATE IS NULL" : ""),
                                            orderby: "CLAIM_ID DESC,TYPE ASC",
                                            pageNumber: start,
                                            rowsPerPage: length);
            }

            return Json(new { data = claimMissingDoclist.Data, draw = Request["draw"], recordsTotal = claimMissingDoclist.TotalItemsCount, recordsFiltered = claimMissingDoclist.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public JsonResult GetBankList(Int64 claimId)
        {
            var result = new AjaxResultDto<InsuredBankSaveResult>();
            InsuredBankSaveResult step1Result = new InsuredBankSaveResult();
            try
            {
                if (claimId <= 0)
                    throw new Exception("Hasar Bilgisi Bulunamadı");

                var claim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID={claimId}", orderby: "CLAIM_ID", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                List<dynamic> banksList = new List<dynamic>();

                if (claim.PAYMENT_METHOD == ((int)ClaimPaymentMethod.KURUM).ToString())
                {
                    var providerBankList = new GenericRepository<V_ProviderBank>().FindBy("PROVIDER_ID=:providerId", orderby: "PROVIDER_ID", parameters: new { providerId = claim.PROVIDER_ID });
                    if (providerBankList != null)
                    {
                        foreach (var item in providerBankList)
                        {
                            dynamic listItem = new System.Dynamic.ExpandoObject();

                            listItem.BANK_ACCOUNT_ID = Convert.ToString(item.BANK_ACCOUNT_ID);

                            listItem.BANK_ACCOUNT_NAME = item.BANK_ACCOUNT_NAME;
                            listItem.BANK_NAME = item.BANK_NAME;

                            listItem.BANK_BRANCH_NAME = item.BANK_BRANCH_NAME;
                            listItem.BANK_BRANCH_CODE = item.BANK_BRANCH_CODE;
                            listItem.BANK_ACCOUNT_NO = item.BANK_ACCOUNT_NO;
                            listItem.IBAN = item.IBAN;
                            listItem.CURRENCY_TYPE = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Currency, item.CURRENCY_TYPE);

                            banksList.Add(listItem);
                        }

                        result.Data = new InsuredBankSaveResult
                        {
                            jsonBankData = banksList.ToJSON(),
                            claimAmount = claim.PAID.ToString()
                        };
                    }
                }
                else
                {
                    var contactBankList = new GenericRepository<V_InsuredBankAccount>().FindBy("INSURED_ID=:insuredId", orderby: "CONTACT_ID", parameters: new { insuredId = claim.INSURED_ID });
                    if (contactBankList != null)
                    {
                        foreach (var item in contactBankList)
                        {
                            dynamic listItem = new System.Dynamic.ExpandoObject();

                            listItem.BANK_ACCOUNT_ID = Convert.ToString(item.BANK_ACCOUNT_ID);

                            listItem.BANK_ACCOUNT_NAME = item.BANK_ACCOUNT_NAME;
                            listItem.BANK_NAME = item.BANK_NAME;

                            listItem.BANK_BRANCH_NAME = item.BANK_BRANCH_NAME;
                            listItem.BANK_BRANCH_CODE = "";// item.BANK_BRANCH_CODE;
                            listItem.BANK_ACCOUNT_NO = item.BANK_ACCOUNT_NO;
                            listItem.IBAN = item.IBAN;
                            listItem.CURRENCY_TYPE = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Currency, item.CURRENCY_TYPE);

                            banksList.Add(listItem);
                        }

                        result.Data = new InsuredBankSaveResult
                        {
                            jsonBankData = banksList.ToJSON(),
                            claimAmount = claim.PAID.ToString()
                        };
                    }
                }

                result.ResultCode = "100";
                result.ResultMessage = "";
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }

            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }
        [LoginCompanyControl]
        public JsonResult ICDs(Int64 claimId)
        {
            var result = new AjaxResultDto<ClaimSaveStep2Result>();
            ClaimSaveStep2Result step2Result = new ClaimSaveStep2Result();
            try
            {
                List<V_ClaimIcd> dbIcds = new GenericRepository<V_ClaimIcd>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                if (dbIcds != null)
                {
                    result.ResultCode = "100";
                    result.ResultMessage = "";

                    List<dynamic> icdsList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in dbIcds)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.CLAIM_ICD_ID = Convert.ToString(item.CLAIM_ICD_ID);
                        listItem.ICD_ID = Convert.ToString(item.ICD_ID);
                        listItem.ICD_CODE = item.ICD_CODE;
                        listItem.ICD_NAME = item.ICD_NAME;
                        listItem.ICD_TYPE = item.ICD_TYPE;
                        listItem.ICD_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.ICD, item.ICD_TYPE);

                        icdsList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimSaveStep2Result
                    {
                        jsonData = icdsList.ToJSON(false)
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }
        [LoginCompanyControl]
        public JsonResult Payment(Int64 claimId)
        {
            var result = new AjaxResultDto<ClaimSaveStep2Result>();
            ClaimSaveStep2Result step2Result = new ClaimSaveStep2Result();
            try
            {
                List<V_ClaimPayment> dbPaymentDetails = new GenericRepository<V_ClaimPayment>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                if (dbPaymentDetails != null)
                {
                    result.ResultCode = "100";
                    result.ResultMessage = "";

                    List<dynamic> paymentList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in dbPaymentDetails)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.PAYMENT_TYPE = item.PAYMENT_TYPE;
                        listItem.PAYMENT_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Payment, item.PAYMENT_TYPE);
                        listItem.DUE_DATE = item.DUE_DATE;
                        listItem.PAYMENT_DATE = item.PAYMENT_DATE;
                        listItem.AMOUNT = item.AMOUNT;
                        listItem.BANK_ACCOUNT_NAME = item.BANK_ACCOUNT_NAME;
                        listItem.IBAN = item.IBAN;

                        paymentList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimSaveStep2Result
                    {
                        jsonData = paymentList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }
        [LoginCompanyControl]
        public JsonResult Notes(Int64 claimId)
        {
            var result = new AjaxResultDto<ClaimSaveNoteResult>();
            ClaimSaveNoteResult step6Result = new ClaimSaveNoteResult();
            try
            {
                List<V_ClaimNote> dbNotes = new GenericRepository<V_ClaimNote>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                //ViewHelper.GetView(Views.ClaimNote, "CLAIM_ID=:id", parameters: new { id = claimId });
                if (dbNotes != null)
                {
                    result.ResultCode = "100";
                    result.ResultMessage = "";

                    List<dynamic> notesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in dbNotes)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                        listItem.CLAIM_NOTE_ID = Convert.ToString(item.CLAIM_NOTE_ID);

                        listItem.NOTE_TYPE = item.CLAIM_NOTE_TYPE;
                        listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimNote, item.CLAIM_NOTE_TYPE);
                        listItem.NOTE_DESCRIPTION = item.NOTE_DECRIPTION;

                        notesList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimSaveNoteResult
                    {
                        jsonData = notesList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }
        [LoginCompanyControl]
        public JsonResult Doctor(Int64 claimId)
        {
            var result = new AjaxResultDto<ClaimSaveDoctorResult>();
            try
            {
                List<dynamic> doctorList = new List<dynamic>();
                ProteinEntities.Claim claim = new GenericRepository<ProteinEntities.Claim>().FindById(claimId);
                if (claim != null)
                {
                    result.ResultCode = "100";
                    result.ResultMessage = "";
                    if (claim.StaffId != null)
                    {
                        V_Staff staff = new GenericRepository<V_Staff>().FindBy("STAFF_ID =: id", orderby: "STAFF_ID", parameters: new { id = claim.StaffId }).FirstOrDefault();
                        //ViewHelper.GetView(Views.Claim, whereCondititon: "CLAIM_ID=:claimId", parameters: new { claimId });
                        if (staff != null)
                        {

                            if (staff.STAFF_ID != null)
                            {
                                dynamic listItem = new System.Dynamic.ExpandoObject();

                                listItem.STAFF_ID = staff.STAFF_ID;
                                listItem.FIRST_NAME = staff.FIRST_NAME;
                                listItem.LAST_NAME = staff.LAST_NAME;
                                listItem.DOCTOR_BRANCH_ID = staff.DOCTOR_BRANCH_ID;
                                listItem.DOCTOR_BRANCH_NAME = staff.DOCTOR_BRANCH_NAME;
                                listItem.DIPLOMA_NO = staff.DIPLOMA_NO;
                                listItem.REGISTER_NO = staff.REGISTER_NO;
                                listItem.STAFF_TITLE = staff.STAFF_TITLE;
                                listItem.STAFF_IDENTITY_NO = staff.IDENTITY_NO;
                                listItem.CONTRACT_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.StaffContract, staff.CONTRACT_TYPE);

                                doctorList.Add(listItem);
                            }
                        }
                    }
                }
                result.Data = new ClaimSaveDoctorResult
                {
                    jsonData = doctorList.ToJSON()
                };
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }
        [LoginCompanyControl]
        public JsonResult GetContactPhone(Int64 contactId)
        {
            object phone = "";
            try
            {
                V_ContactPhone contactPhone = new GenericRepository<V_ContactPhone>().FindBy($"CONTACT_ID={contactId} AND IS_PRIMARY='1' AND PHONE_TYPE='{((int)PhoneType.MOBIL).ToString()}'", orderby: "").FirstOrDefault();
                if (contactPhone != null)
                {
                    phone = contactPhone.PHONE_NO;
                }
            }
            catch (Exception)
            {
                phone = "";
            }
            return Json(phone, JsonRequestBehavior.AllowGet);
        }
        [LoginCompanyControl]
        public JsonResult Details(Int64 claimId)
        {
            var result = new AjaxResultDto<ClaimSaveDoctorResult>();
            try
            {
                result.ResultCode = "100";
                result.ResultMessage = "";
                List<dynamic> hospitalizationList = new List<dynamic>();
                ClaimHospitalization claimHospitalization = new GenericRepository<ClaimHospitalization>().FindBy("CLAIM_ID=:claimId", orderby: "CLAIM_ID", parameters: new { claimId }).FirstOrDefault();
                if (claimHospitalization != null)
                {

                    dynamic listItem = new System.Dynamic.ExpandoObject();

                    listItem.CLAIM_HOSPITALIZATION_ID = claimHospitalization.Id;
                    listItem.ENTRANCE_DATE = string.IsNullOrEmpty(Convert.ToString(claimHospitalization.EntranceDate)) ? "" : ((DateTime)claimHospitalization.EntranceDate).ToString("dd-MM-yyyy");
                    listItem.EXIT_DATE = string.IsNullOrEmpty(Convert.ToString(claimHospitalization.ExitDate)) ? "" : ((DateTime)claimHospitalization.ExitDate).ToString("dd-MM-yyyy");

                    hospitalizationList.Add(listItem);
                }
                result.Data = new ClaimSaveDoctorResult
                {
                    jsonData = hospitalizationList.ToJSON()
                };
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [LoginCompanyControl]
        public JsonResult ExclusionAndTransfer(Int64 insuredId)
        {
            var ExclusionList = new GenericRepository<V_InsuredExclusion>().FindBy("INSURED_ID =:insuredId", orderby: "INSURED_ID", parameters: new { insuredId });
            if (ExclusionList.Count > 0)
            {
                List<dynamic> insuredExclusionList = new List<dynamic>();

                foreach (var item in ExclusionList)
                {
                    dynamic listItem = new System.Dynamic.ExpandoObject();

                    if (item.RULE_CATEGORY_TYPE != null)
                    {
                        listItem.RULE_CATEGORY_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                        string resultt = Convert.ToString(item.RESULT);
                        listItem.RESULT = resultt.IsNull() ? "-" : resultt;
                        string result_second = Convert.ToString(item.RESULT_SECOND);
                        listItem.RESULT_SECOND = result_second.IsNull() ? "-" : result_second;
                    }
                    else
                    {
                        listItem.RULE_CATEGORY_TYPE_TEXT = "EK PRİM";
                        listItem.RESULT = Convert.ToString(item.ADDITIONAL_PREMIUM);
                        listItem.RESULT_SECOND = "-";
                    }
                    listItem.EXCLUSION_DESCRIPTION = Convert.ToString(item.DESCRIPTION);

                    insuredExclusionList.Add(listItem);
                }
                ViewBag.insuredExclusionList = insuredExclusionList;
            }

            var TransferExclusionList = new GenericRepository<V_InsuredNote>().FindBy("INSURED_ID =:insuredId AND NOTE_TYPE =':noteType'", orderby: "INSURED_ID",
                                                                                      parameters: new { insuredId, noteType = ((int)InsuredNoteType.GECİS).ToString() });
            //ViewHelper.GetView(Views.InsuredNote, whereCondititon: $"INSURED_ID ={insuredId} AND NOTE_TYPE={((int)InsuredNoteType.GECİS)}");
            if (TransferExclusionList.Count > 0)
            {
                List<dynamic> notesList = new List<dynamic>();

                foreach (var item in TransferExclusionList)
                {
                    dynamic listItem = new System.Dynamic.ExpandoObject();

                    listItem.NOTE_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.InsuredNote, item.NOTE_TYPE);
                    listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                    notesList.Add(listItem);
                }

                ViewBag.insuredTransferExclusionList = notesList;
            }

            var result = new
            {
                insuredExclusionList = ViewBag.insuredExclusionList,
                insuredTransferExclusionList = ViewBag.insuredTransferExclusionList
            };

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(result),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        [LoginCompanyControl]
        public JsonResult NoteAndDecleration(Int64 contactId, Int64 insuredId)
        {
            var NoteList = new GenericRepository<V_ContactNote>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id = contactId });
            if (NoteList.Count > 0)
            {
                List<dynamic> notesList = new List<dynamic>();

                foreach (var item in NoteList)
                {
                    dynamic listItem = new System.Dynamic.ExpandoObject();

                    listItem.NOTE_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Note, item.NOTE_TYPE);
                    listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                    notesList.Add(listItem);
                }
                ViewBag.insuredNoteList = notesList;
            }

            var DeclerationList = new GenericRepository<V_InsuredNote>().FindBy("INSURED_ID=:insuredId AND NOTE_TYPE=':noteType'", orderby: "INSURED_ID",
                                                                                parameters: new { insuredId, noteType = ((int)InsuredNoteType.BEYAN).ToString() });
            //ViewHelper.GetView(Views.InsuredNote, whereCondititon: $"INSURED_ID={insuredId} AND NOTE_TYPE={((int)InsuredNoteType.BEYAN)}");
            if (DeclerationList.Count > 0)
            {
                List<dynamic> notesList = new List<dynamic>();

                foreach (var item in DeclerationList)
                {
                    dynamic listItem = new System.Dynamic.ExpandoObject();

                    listItem.NOTE_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.InsuredNote, item.NOTE_TYPE);
                    listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                    notesList.Add(listItem);
                }

                ViewBag.insuredDeclerationList = notesList;
            }

            var result = new
            {
                insuredNoteList = ViewBag.insuredNoteList,
                insuredDeclerationList = ViewBag.insuredDeclerationList
            };

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(result),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        #region DropDown fill methods 

        private void FillCompanies()
        {
            var v_company = new GenericRepository<V_Company>().FindBy(orderby: "COMPANY_ID");
            //ViewHelper.GetView(Views.Company, orderby: "COMPANY_ID");
            ViewBag.v_company = v_company;
        }

        private void FillProducts()
        {
            var v_product = new GenericRepository<V_Product>().FindBy(orderby: "PRODUCT_ID");
            ViewBag.v_product = v_product;
        }

        private void FillProducts(Int64 Id)
        {
            var v_product = new GenericRepository<V_Product>().FindBy("COMPANY_ID =:id", orderby: "PRODUCT_ID", parameters: new { id = Id });
            ViewBag.v_product = v_product;
        }

        #endregion
        [HttpPost]
        [LoginCompanyControl]
        public JsonResult Medicine(Int64 claimId, Int64 insuredId, Int64 packageId, string barcode)
        {
            var result = new AjaxResultDto<ClaimSaveStep2Result>();
            try
            {
                long medicineProcessListId = 0;
                var medicineProcessList = ProcessListHelper.GetProcessList("İLAÇ");
                if (medicineProcessList != null)
                {
                    medicineProcessListId = medicineProcessList.Id;
                }
                else
                {
                    throw new Exception("İlaç listesi bulunamadı!");
                }

                Process medicine;
                Helper hlp = new Helper();
                MedicineInfo mInfo = hlp.GetMedicineInfoByBarcode(barcode);
                if (mInfo.Success)
                {
                    GenericRepository<Process> medicineRepository = new GenericRepository<Process>();
                    List<Process> medicineList = medicineRepository.FindBy("CODE='" + barcode + "'");
                    if (medicineList == null || medicineList.Count == 0)
                    {
                        // Insert new medicine
                        medicine = new Process()
                        {
                            Id = 0,
                            Code = barcode,
                            ProcessListId = medicineProcessListId,
                            Name = mInfo.Name,
                            Amount = mInfo.TotalPrice
                        };
                        SpResponse spResponse = medicineRepository.Update(medicine);
                        if (spResponse.Code != "100")
                        {
                            throw new Exception("İlaç bilgileri güncellenirken hata oluştu!");
                        }
                        medicine.Id = spResponse.PkId;
                    }
                    else if (medicineList.Count == 1)
                    {
                        medicine = medicineList[0];
                        // Check medicine needs to be updated
                        if (!medicine.Name.Equals(mInfo.Name) || medicine.Amount != mInfo.TotalPrice)
                        {
                            medicine.Name = mInfo.Name;
                            medicine.Amount = mInfo.TotalPrice;
                            SpResponse spResponse = medicineRepository.Update(medicine);
                            if (spResponse.Code != "100")
                            {
                                throw new Exception("İlaç bilgileri güncellenirken hata oluştu!");
                            }
                            result.ResultCode = spResponse.Code;
                            result.ResultMessage = spResponse.Message;
                        }
                    }
                    else
                    {
                        throw new Exception("Aynı barkod ile birden fazla ilaç bulundu! (" + mInfo.Message + ")");
                    }

                    if (medicine != null)
                    {
                        long? coverageId = null;
                        List<V_PackageCoverageList> insuredCoverageResult = new GenericRepository<V_PackageCoverageList>().FindBy(
                            "PACKAGE_ID=:packageId AND COVERAGE_TYPE =':coverageType' AND COVERAGE_NAME LIKE '%:coverageName%'",
                            orderby: "PACKAGE_ID",
                            parameters: new { packageId, coverageType = Convert.ToString((int)CoverageType.AYAKTA).ToString(), coverageName = "İLAÇ" },
                            fetchHistoricRows: true,
                            fetchDeletedRows: true);
                        if (insuredCoverageResult != null)
                        {
                            if (insuredCoverageResult != null && insuredCoverageResult.Count == 1)
                            {
                                string strCoverageId = Convert.ToString(insuredCoverageResult[0].COVERAGE_ID);
                                if (!string.IsNullOrEmpty(strCoverageId))
                                {
                                    coverageId = long.Parse(strCoverageId);
                                }
                            }
                        }

                        var claimMedicine = new ClaimProcess
                        {
                            Id = 0,
                            ClaimId = claimId,
                            ProcessId = medicine.Id,
                            RequestedAmount = medicine.Amount,
                            UnitAmount = medicine.Amount,
                            AgreedAmount = medicine.Amount
                        };
                        if (coverageId != null && coverageId > 0)
                        {
                            claimMedicine.CoverageId = coverageId;
                        }
                        var spResponseClaimMedicine = new ClaimProcessRepository().Insert(claimMedicine);
                        //if (spResponseClaimMedicine.Code == "100")
                        //{
                        result.ResultCode = spResponseClaimMedicine.Code;
                        result.ResultMessage = spResponseClaimMedicine.Message;
                        //}
                        //else
                        //{
                        //    throw new Exception(spResponseClaimMedicine.Message);
                        //}
                    }
                    else
                    {
                        throw new Exception(mInfo.Message);
                    }
                }
                else
                {
                    throw new Exception("İlaç bulunamadı! (" + mInfo.Message + ")");
                }

                List<V_ClaimProcess> dbMedicines = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id AND PROCESS_LIST_ID =: processListId", orderby: "CLAIM_ID", parameters: new { id = claimId, processListId = medicineProcessListId });
                if (dbMedicines != null)
                {
                    List<dynamic> medicinesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in dbMedicines)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.CLAIM_MEDICINE_ID = Convert.ToString(item.CLAIM_PROCESS_ID);
                        listItem.MEDICINE_ID = Convert.ToString(item.PROCESS_ID);
                        listItem.MEDICINE_CODE = item.PROCESS_CODE;
                        listItem.MEDICINE_NAME = item.PROCESS_NAME;
                        listItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(item.UNIT_AMOUNT).ToString("0.00");
                        listItem.REQUESTED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.REQUESTED_AMOUNT)) ? "" : Convert.ToDecimal(item.REQUESTED_AMOUNT).ToString("0.00");
                        listItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(item.AGREED_AMOUNT).ToString("0.00");
                        listItem.CLAIM_MEDICINE_COVERAGE_ID = Convert.ToString(item.COVERAGE_ID);
                        listItem.COVERAGE_NAME = item.COVERAGE_NAME;
                        listItem.PAID = string.IsNullOrEmpty(Convert.ToString(item.PAID)) ? "" : Convert.ToDecimal(item.PAID).ToString("0.00");
                        listItem.RESULT = item.RESULT;
                        if (!string.IsNullOrEmpty(item.PARTLY_REJECT_DESC))
                        {
                            listItem.RESULT += " - " + item.PARTLY_REJECT_DESC;
                        }

                        medicinesList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimSaveStep2Result
                    {
                        jsonData = medicinesList.ToJSON(false)
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ClaimProcess(Int64 claimId, Int64 packageId, FormCollection form)
        {
            var result = new AjaxResultDto<ClaimSaveStep6Result>();
            ClaimSaveStep6Result step6Result = new ClaimSaveStep6Result();
            try
            {
                var processId = form["hdClaimProcessesId"];
                var requestedAmount = form["REQUESTED_AMOUNT_" + processId].Replace('.', ',');
                if (!processId.IsInt64() || !requestedAmount.IsNumeric())
                {
                    throw new Exception("Talep Edilen Tutar Girilmesi Gerekmektedir.");
                }

                var CheckClaimProcess = new GenericRepository<ClaimProcess>().FindBy($"CLAIM_ID={claimId} AND PROCESS_ID={processId}");
                if (CheckClaimProcess.Count > 0)
                {
                    throw new Exception("Bu işlem Daha Önce Seçilmiş. Lütfen Kontrol Ediniz...");
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                //string processIndex = js.Deserialize<string>(processesIndexes);

                V_ClaimProcessList claimProces = new GenericRepository<V_ClaimProcessList>().FindBy("CLAIM_ID =: id AND PROCESS_ID=:processId", orderby: "CLAIM_ID", fetchDeletedRows: true, fetchHistoricRows: true, parameters: new { id = claimId, processId }).FirstOrDefault();
                if (claimProces != null)
                {
                    ClaimProcessList listItem = new ClaimProcessList();

                    listItem.CLAIM_ID = Convert.ToString(claimProces.CLAIM_ID);
                    listItem.PROCESS_ID = Convert.ToString(claimProces.PROCESS_ID);
                    listItem.PROCESS_NAME = claimProces.PROCESS_NAME;
                    listItem.PROCESS_CODE = claimProces.PROCESS_CODE;
                    listItem.PROCESS_AMOUNT = Convert.ToString(claimProces.PROCESS_AMOUNT);
                    listItem.PROCESS_GROUP_ID = Convert.ToString(claimProces.PROCESS_GROUP_ID);
                    listItem.PROCESS_GROUP_NAME = claimProces.PROCESS_GROUP_NAME;
                    listItem.PROCESS_LIST_ID = Convert.ToString(claimProces.PROCESS_LIST_ID);
                    listItem.PROCESS_LIST_NAME = claimProces.PROCESS_LIST_NAME;

                    var claimProcess = new ClaimProcess();
                    claimProcess.Id = 0;
                    claimProcess.ClaimId = claimId;
                    claimProcess.ProcessId = long.Parse(listItem.PROCESS_ID);
                    claimProcess.UnitAmount = decimal.Parse(Convert.ToString(claimProces.PROCESS_AMOUNT));
                    claimProcess.RequestedAmount = decimal.Parse(Convert.ToString(requestedAmount));
                    claimProcess.AgreedAmount = decimal.Parse(Convert.ToString(claimProces.CONTRACT_AMOUNT));

                    List<V_PackageClaimProcess> packageClaimProcesses = new GenericRepository<V_PackageClaimProcess>().FindBy("PACKAGE_ID =: packageId AND PROCESS_ID =: processId", orderby: "PACKAGE_ID", fetchDeletedRows: true, fetchHistoricRows: true, parameters: new { packageId, processId = claimProcess.ProcessId });
                    if (packageClaimProcesses != null && packageClaimProcesses.Count == 1)
                    {
                        claimProcess.CoverageId = long.Parse(Convert.ToString(packageClaimProcesses[0].COVERAGE_ID));
                    }

                    var spResponseClaimProcess = new ClaimProcessRepository().Insert(claimProcess);
                    //if (spResponseClaimProcess.Code == "100")
                    //{
                    result.ResultCode = spResponseClaimProcess.Code;
                    result.ResultMessage = spResponseClaimProcess.Message;
                    //}
                    //else
                    //{
                    //    throw new Exception(spResponseClaimProcess.Message);
                    //}

                    long medicineProcessListId = 0;
                    var medicineProcessList = ProcessListHelper.GetProcessList("İLAÇ");
                    if (medicineProcessList != null)
                    {
                        medicineProcessListId = medicineProcessList.Id;
                    }
                    else
                    {
                        throw new Exception("İlaç listesi bulunamadı!");
                    }

                    List<V_ClaimProcess> dbClaimProcesses = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id AND PROCESS_LIST_ID !=: processListId", orderby: "CLAIM_ID", parameters: new { id = claimId, processListId = medicineProcessListId });
                    if (dbClaimProcesses != null)
                    {
                        List<dynamic> claimProcessList = new List<dynamic>();

                        int i = 1;

                        foreach (var clmProcess in dbClaimProcesses)
                        {
                            dynamic clmProcessItem = new System.Dynamic.ExpandoObject();

                            clmProcessItem.id = i;
                            clmProcessItem.ClientId = i;
                            clmProcessItem.STATUS = clmProcess.STATUS;

                            clmProcessItem.CLAIM_PROCESS_ID = Convert.ToString(clmProcess.CLAIM_PROCESS_ID);
                            clmProcessItem.PROCESS_ID = Convert.ToString(clmProcess.PROCESS_ID);
                            clmProcessItem.PROCESS_CODE = clmProcess.PROCESS_CODE;
                            clmProcessItem.PROCESS_NAME = clmProcess.PROCESS_NAME;
                            clmProcessItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(clmProcess.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(clmProcess.UNIT_AMOUNT).ToString("0.00");
                            clmProcessItem.REQUESTED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(clmProcess.REQUESTED_AMOUNT)) ? "" : Convert.ToDecimal(clmProcess.REQUESTED_AMOUNT).ToString("0.00");
                            clmProcessItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(clmProcess.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(clmProcess.AGREED_AMOUNT).ToString("0.00");
                            clmProcessItem.COINSURANCE = clmProcess.COINSURANCE;
                            clmProcessItem.EXEMPTION = string.IsNullOrEmpty(Convert.ToString(clmProcess.EXEMPTION)) ? "" : Convert.ToDecimal(clmProcess.EXEMPTION).ToString("0.00");
                            clmProcessItem.PAID = string.IsNullOrEmpty(Convert.ToString(clmProcess.PAID)) ? "" : Convert.ToDecimal(clmProcess.PAID).ToString("0.00");
                            clmProcessItem.CLAIM_PROCESS_COVERAGE_ID = Convert.ToString(clmProcess.COVERAGE_ID);
                            clmProcessItem.COVERAGE_NAME = clmProcess.COVERAGE_NAME;
                            clmProcessItem.RESULT = clmProcess.RESULT;
                            if (!string.IsNullOrEmpty(clmProcess.PARTLY_REJECT_DESC))
                            {
                                clmProcessItem.RESULT += " - " + clmProcess.PARTLY_REJECT_DESC;
                            }

                            claimProcessList.Add(clmProcessItem);

                            i++;
                        }

                        result.Data = new ClaimSaveStep6Result
                        {
                            jsonData = claimProcessList.ToJSON(false)
                        };
                    }
                }
                else
                {
                    throw new Exception("Seçilen hasar işlemi bulunamadı!");
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Processes(Int64 claimId)
        {
            var result = new AjaxResultDto<ClaimProcessListResult>();
            try
            {
                long medicineProcessListId = 0;
                var medicineProcessList = ProcessListHelper.GetProcessList("İLAÇ");
                if (medicineProcessList != null)
                {
                    medicineProcessListId = medicineProcessList.Id;
                }
                else
                {
                    throw new Exception("İlaç listesi bulunamadı!");
                }

                List<V_ClaimProcess> dbClaimProcesses = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id AND PROCESS_LIST_ID !=: processListId", orderby: "CLAIM_ID", parameters: new { id = claimId, processListId = medicineProcessListId });
                if (dbClaimProcesses != null && dbClaimProcesses.Count > 0)
                {
                    List<dynamic> claimProcessList = new List<dynamic>();

                    int i = 1;

                    foreach (var clmProcess in dbClaimProcesses)
                    {
                        dynamic clmProcessItem = new System.Dynamic.ExpandoObject();

                        clmProcessItem.id = i;
                        clmProcessItem.ClientId = i;
                        clmProcessItem.STATUS = clmProcess.STATUS;

                        clmProcessItem.CLAIM_PROCESS_ID = Convert.ToString(clmProcess.CLAIM_PROCESS_ID);
                        clmProcessItem.PROCESS_ID = Convert.ToString(clmProcess.PROCESS_ID);
                        clmProcessItem.PROCESS_CODE = clmProcess.PROCESS_CODE;
                        clmProcessItem.PROCESS_NAME = clmProcess.PROCESS_NAME;
                        clmProcessItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(clmProcess.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(clmProcess.UNIT_AMOUNT).ToString("0.00");
                        clmProcessItem.REQUESTED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(clmProcess.REQUESTED_AMOUNT)) ? "" : Convert.ToDecimal(clmProcess.REQUESTED_AMOUNT).ToString("0.00");
                        clmProcessItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(clmProcess.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(clmProcess.AGREED_AMOUNT).ToString("0.00");
                        clmProcessItem.COINSURANCE = clmProcess.COINSURANCE;
                        clmProcessItem.EXEMPTION = string.IsNullOrEmpty(Convert.ToString(clmProcess.EXEMPTION)) ? "" : Convert.ToDecimal(clmProcess.EXEMPTION).ToString("0.00");
                        clmProcessItem.CLAIM_PROCESS_COVERAGE_ID = Convert.ToString(clmProcess.COVERAGE_ID);
                        clmProcessItem.COVERAGE_NAME = clmProcess.COVERAGE_NAME;
                        clmProcessItem.RESULT = clmProcess.RESULT;
                        if (!string.IsNullOrEmpty(clmProcess.PARTLY_REJECT_DESC))
                        {
                            clmProcessItem.RESULT += " - " + clmProcess.PARTLY_REJECT_DESC;
                        }
                        clmProcessItem.PAID = string.IsNullOrEmpty(Convert.ToString(clmProcess.PAID)) ? "" : Convert.ToDecimal(clmProcess.PAID).ToString("0.00");
                        claimProcessList.Add(clmProcessItem);

                        i++;
                    }

                    result.Data = new ClaimProcessListResult
                    {
                        jsonData = claimProcessList.ToJSON(false)
                    };
                    result.ResultCode = "100";
                    result.ResultMessage = "SUCCESS";
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult Medicines(Int64 claimId)
        {
            var result = new AjaxResultDto<ClaimProcessListResult>();
            try
            {
                long medicineProcessListId = 0;
                var medicineProcessList = ProcessListHelper.GetProcessList("İLAÇ");
                if (medicineProcessList != null)
                {
                    medicineProcessListId = medicineProcessList.Id;
                }
                else
                {
                    throw new Exception("İlaç listesi bulunamadı!");
                }

                List<V_ClaimProcess> dbMedicines = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id AND PROCESS_LIST_ID =: processListId", orderby: "CLAIM_ID", parameters: new { id = claimId, processListId = medicineProcessListId });
                if (dbMedicines != null)
                {
                    List<dynamic> medicinesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in dbMedicines)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.CLAIM_MEDICINE_ID = Convert.ToString(item.CLAIM_PROCESS_ID);
                        listItem.MEDICINE_ID = Convert.ToString(item.PROCESS_ID);
                        listItem.MEDICINE_CODE = item.PROCESS_CODE;
                        listItem.MEDICINE_NAME = item.PROCESS_NAME;
                        listItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(item.UNIT_AMOUNT).ToString("0.00");
                        listItem.REQUESTED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.REQUESTED_AMOUNT)) ? "" : Convert.ToDecimal(item.REQUESTED_AMOUNT).ToString("0.00");
                        listItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(item.AGREED_AMOUNT).ToString("0.00");
                        listItem.CLAIM_MEDICINE_COVERAGE_ID = Convert.ToString(item.COVERAGE_ID);
                        listItem.COVERAGE_NAME = item.COVERAGE_NAME;
                        listItem.PAID = string.IsNullOrEmpty(Convert.ToString(item.PAID)) ? "" : Convert.ToDecimal(item.PAID).ToString("0.00");
                        listItem.RESULT = item.RESULT;
                        if (!string.IsNullOrEmpty(item.PARTLY_REJECT_DESC))
                        {
                            listItem.RESULT += " - " + item.PARTLY_REJECT_DESC;
                        }

                        medicinesList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimProcessListResult
                    {
                        jsonData = medicinesList.ToJSON(false)
                    };
                    result.ResultCode = "100";
                    result.ResultMessage = "SUCCESS";
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [LoginCompanyControl]
        public JsonResult Coverages(Int64 claimId)
        {
            object result = null;
            var claimProcessList = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
            //ViewHelper.GetView(Views.ClaimProcess, whereCondititon: $"CLAIM_ID={claimId}");
            if (claimProcessList != null)
            {
                List<dynamic> list = new List<dynamic>();

                int i = 1;

                foreach (var claimProcess in claimProcessList)
                {
                    //Add root item
                    dynamic claimProcessItem = new System.Dynamic.ExpandoObject();

                    claimProcessItem.id = i;
                    claimProcessItem.ClientId = i;
                    claimProcessItem.STATUS = claimProcess.STATUS;

                    claimProcessItem.CLAIM_ID = claimProcess.CLAIM_ID;
                    claimProcessItem.CLAIM_PROCESS_ID = claimProcess.CLAIM_PROCESS_ID;
                    claimProcessItem.PROCESS_ID = claimProcess.PROCESS_ID;
                    claimProcessItem.PROCESS_NAME = claimProcess.PROCESS_NAME;
                    claimProcessItem.COVERAGE_ID = string.IsNullOrEmpty(Convert.ToString(claimProcess.COVERAGE_ID)) ? "" : Convert.ToString(claimProcess.COVERAGE_ID);
                    claimProcessItem.COVERAGE_NAME = claimProcess.COVERAGE_NAME;
                    claimProcessItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(claimProcess.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(claimProcess.UNIT_AMOUNT).ToString("0.00");
                    claimProcessItem.REQUESTED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(claimProcess.REQUESTED_AMOUNT)) ? "" : Convert.ToDecimal(claimProcess.REQUESTED_AMOUNT).ToString("0.00");
                    claimProcessItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(claimProcess.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(claimProcess.AGREED_AMOUNT).ToString("0.00");
                    claimProcessItem.CONTRACT_PROCESS_GROUP_ID = claimProcess.CONTRACT_PROCESS_GROUP_ID;
                    claimProcessItem.PROCESS_CODE = claimProcess.PROCESS_CODE;
                    claimProcessItem.SESSION_COUNT = claimProcess.SESSION_COUNT;
                    claimProcessItem.COUNT = claimProcess.COUNT;
                    claimProcessItem.DAY = claimProcess.DAY;
                    claimProcessItem.PROCESS_LIMIT = string.IsNullOrEmpty(Convert.ToString(claimProcess.PROCESS_LIMIT)) ? "" : Convert.ToDecimal(claimProcess.PROCESS_LIMIT).ToString("0.00");
                    claimProcessItem.REQUESTED = string.IsNullOrEmpty(Convert.ToString(claimProcess.REQUESTED)) ? "" : Convert.ToDecimal(claimProcess.REQUESTED).ToString("0.00");
                    claimProcessItem.STOPPAGE = string.IsNullOrEmpty(Convert.ToString(claimProcess.STOPAJ)) ? "" : Convert.ToDecimal(claimProcess.STOPAJ).ToString("0.00");
                    decimal acceptedAmount = 0;
                    if (claimProcess.REQUESTED != null && Convert.ToDecimal(claimProcess.REQUESTED) > 0)
                    {
                        if (claimProcess.ACCEPTED != null && Convert.ToDecimal(claimProcess.ACCEPTED) > 0)
                        {
                            acceptedAmount = Convert.ToDecimal(claimProcess.REQUESTED) - Convert.ToDecimal(claimProcess.ACCEPTED);
                        }
                        else
                        {
                            acceptedAmount = Convert.ToDecimal(claimProcess.REQUESTED);
                        }
                    }
                    claimProcessItem.ACCEPTED = acceptedAmount.ToString("0.00");
                    claimProcessItem.CONFIRMED = string.IsNullOrEmpty(Convert.ToString(claimProcess.CONFIRMED)) ? "" : Convert.ToDecimal(claimProcess.CONFIRMED).ToString("0.00");
                    claimProcessItem.PAID = string.IsNullOrEmpty(Convert.ToString(claimProcess.PAID)) ? "" : Convert.ToDecimal(claimProcess.PAID).ToString("0.00");
                    claimProcessItem.EXGRACIA = string.IsNullOrEmpty(Convert.ToString(claimProcess.EXGRACIA)) ? "" : Convert.ToDecimal(claimProcess.EXGRACIA).ToString("0.00");
                    claimProcessItem.SGK_AMOUNT = string.IsNullOrEmpty(Convert.ToString(claimProcess.SGK_AMOUNT)) ? "" : Convert.ToDecimal(claimProcess.SGK_AMOUNT).ToString("0.00");
                    decimal insuredAmount = 0;
                    if (claimProcess.REQUESTED != null && Convert.ToDecimal(claimProcess.REQUESTED) > 0)
                    {
                        if (claimProcess.PAID != null && Convert.ToDecimal(claimProcess.PAID) > 0)
                        {
                            insuredAmount = Convert.ToDecimal(claimProcess.REQUESTED) - Convert.ToDecimal(claimProcess.PAID);
                        }
                        else
                        {
                            insuredAmount = Convert.ToDecimal(claimProcess.REQUESTED);
                        }
                    }
                    claimProcessItem.INSURED_AMOUNT = insuredAmount.ToString("0.00");
                    claimProcessItem.COINSURANCE = claimProcess.COINSURANCE;
                    claimProcessItem.EXEMPTION = string.IsNullOrEmpty(Convert.ToString(claimProcess.EXEMPTION)) ? "" : Convert.ToDecimal(claimProcess.EXEMPTION).ToString("0.00");
                    claimProcessItem.RESULT = claimProcess.RESULT;
                    if (!string.IsNullOrEmpty(claimProcess.PARTLY_REJECT_DESC))
                    {
                        claimProcessItem.RESULT += " - " + claimProcess.PARTLY_REJECT_DESC;
                    }
                    claimProcessItem.PROCESS_LIST_ID = claimProcess.PROCESS_LIST_ID;

                    list.Add(claimProcessItem);

                    i++;
                }
                result = list.ToJSON();
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(result),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [LoginCompanyControl]
        public JsonResult ProcessICDs(Int64 claimId, Int64 claimProcessId)
        {
            var IcdList = new GenericRepository<V_ClaimIcd>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
            if (IcdList.Count <= 0)
            {
                throw new Exception("Hasara Ait Tanı Bilgisi Bulunamadı!");
            }

            List<ClaimProcessIcd> claimProcessIcds = new ClaimProcessIcdRepository().FindBy("CLAIM_PROCESS_ID = :claimProcessId", parameters: new { claimProcessId });

            List<dynamic> claimProcessIcdList = new List<dynamic>();

            foreach (var item in IcdList)
            {
                dynamic listItem = new System.Dynamic.ExpandoObject();
                listItem.CLAIM_ICD_ID = Convert.ToString(item.CLAIM_ICD_ID);
                listItem.CLAIM_ID = Convert.ToString(item.CLAIM_ID);
                listItem.ICD_CODE = Convert.ToString(item.ICD_CODE);
                listItem.ICD_ID = Convert.ToString(item.ICD_ID);
                listItem.ICD_NAME = item.ICD_NAME;
                listItem.ICD_TYPE = item.ICD_TYPE;
                listItem.ICD_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.ICD, item.ICD_TYPE);
                listItem.STATUS = item.STATUS;
                listItem.SELECTED = "0";
                foreach (var claimProcessIcd in claimProcessIcds)
                {
                    if (item.ICD_ID == claimProcessIcd.IcdId)
                    {
                        listItem.SELECTED = "1";
                    }
                }

                claimProcessIcdList.Add(listItem);
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = claimProcessIcdList.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        private ClaimProcessGroupList GetClaimProcessGroup(List<ClaimProcessGroupList> claimProcessGroupsList, string processGroupId)
        {
            foreach (ClaimProcessGroupList claimProcessGroup in claimProcessGroupsList)
            {
                if (claimProcessGroup.PROCESS_GROUP_ID.Equals(processGroupId))
                {
                    return claimProcessGroup;
                }
            }
            return null;
        }
    }
}