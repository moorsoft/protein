﻿using Protein.Common.Constants;
using Protein.Common.Resources;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Data.Services;
using Protein.Web.ActionFilter.LoginAttr;
using Protein.Web.Helpers;
using Protein.Web.Models.ExportModel;
using Protein.Web.Models.NetworkModel;
using System;
using System.Web.Mvc;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Web.Controllers
{
    public class NetworkController : Controller
    {
        public NetworkService service;

        public NetworkController()
        {
            service = new NetworkService();
        }

        // GET: Network
        [LoginControl]
        public ActionResult Index()
        {
            NetworkVM vm = new NetworkVM();
            try
            {
                vm.ExportVM.ViewName = Constants.Views.ExportNetwork;
                vm.ExportVM.SetExportColumns();

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var NetworkCategoryType = LookupHelper.GetLookupData(Constants.LookupTypes.NetworkCategory, showAll: true);
                ViewBag.NetworkCategoryTypeList = NetworkCategoryType;

                var NetworkType = LookupHelper.GetLookupData(Constants.LookupTypes.Network, showAll: true);
                ViewBag.NetworkTypeList = NetworkType;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View(vm);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult Index(FormCollection form)
        {
            NetworkVM vm = new NetworkVM();
            try
            {
                vm.ExportVM.ViewName = Constants.Views.ExportNetwork;
                vm.ExportVM.SetExportColumns();

                NetworkResource resource = ResourceHelper.ConvertToNetworkResource(form, Request);
                var result = service.Find(resource);
                vm.ExportVM.WhereCondition = result.Conditions;
                return Json(new { data = result.Data, draw = Request["draw"], recordsTotal = result.TotalItemsCount, recordsFiltered = result.TotalItemsCount, whereConditition = vm.ExportVM.WhereCondition }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
                return RedirectToAction("Index");
            }
        }

        // GET: Network Create
        [LoginControl]
        public ActionResult Create()
        {
            try
            {
                var NetworkCategoryType = LookupHelper.GetLookupData(LookupTypes.NetworkCategory);
                ViewBag.NetworkCategoryTypeList = NetworkCategoryType;

                var NetworkType = LookupHelper.GetLookupData(LookupTypes.Network);
                ViewBag.NetworkTypeList = NetworkType;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.Network = null;
                ViewBag.Title = "Network Ekle";
                ViewBag.FormDisabled = "";
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult Create(FormCollection form)
        {
            try
            {
                Network entity = ResourceHelper.ConvertToNetwork(form);
                var spResponse = service.Create(entity);
                if (spResponse.Code == "100")
                {
                    TempData["Alert"] = $"swAlert('İşlemi Başarılı','{entity.Name} başarıyla kaydedildi.','success')";
                }
                else
                {
                    throw new Exception(spResponse.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Index");
        }

        // GET: Network Create
        [LoginControl]
        public ActionResult Edit(Int64 id)
        {
            try
            {
                var network = service.FindById(id);
                if (network == null || network.Id < 1)
                {
                    throw new Exception($"Network bulunamadı : ID = {id}");
                }

                var NetworkCategoryType = LookupHelper.GetLookupData(LookupTypes.NetworkCategory);
                ViewBag.NetworkCategoryTypeList = NetworkCategoryType;

                var NetworkType = LookupHelper.GetLookupData(LookupTypes.Network);
                ViewBag.NetworkTypeList = NetworkType;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.Network = network;
                ViewBag.Title = "Network Güncelle";
                ViewBag.FormDisabled = "";
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult Edit(FormCollection form)
        {
            try
            {
                Network entity = ResourceHelper.ConvertToNetwork(form);
                var spResponse = service.Update(entity);
                if (spResponse.Code == "100")
                {
                    TempData["Alert"] = $"swAlert('İşlemi Başarılı','{entity.Name} başarıyla kaydedildi.','success')";
                }
                else
                {
                    throw new Exception(spResponse.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Index");
        }

        // GET: Network Delete
        [LoginControl]
        public ActionResult Delete(Int64 Id)
        {
            try
            {
                var spResponse = service.Delete(Id);
                if (spResponse.Code == "100")
                {
                    TempData["Alert"] = $"swAlert('Silme İşlemi Başarılı','Network Bilgisi başarıyla silindi.','success')";
                }
                else
                {
                    throw new Exception(spResponse.Code + " : " + spResponse.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Index");
        }
    }
}