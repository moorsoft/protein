﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using Newtonsoft.Json;
using Protein.Business.Concrete.Media;
using Protein.Common.Constants;
using Protein.Common.Dto;
using Protein.Common.Entities;
using Protein.Common.Extensions;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Web.ActionFilter.LoginAttr;
using Protein.Web.Helpers;
using Protein.Web.Models.ExportModel;
using Protein.Web.Models.ListView;
using Protein.Web.Models.MediaModel;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;
using System.Xml.Serialization;
using Protein.Data.ExternalServices.Common.Lib;
using static Protein.Data.ExternalServices.Common.Lib.NN.NNInputOutputTypes;
using System.Reflection;
using Protein.WS.WS.Common;
using Protein.Data.ExternalServices.Common.Lib.SBM;

namespace Protein.Web.Controllers
{
    public class SystemController : Controller
    {
        // GET: System
        [LoginControl]
        public ActionResult Index()
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                //Fill data
                ViewBag.companyId = string.Empty;
                ViewBag.companyName = string.Empty;
                // var result = new GenericRepository<IntegrationLog>().FindBy(orderby: "ID", fetchHistoricRows: true);
                // ViewBag.Result = result;
                var CompanyList = new CompanyRepository().FindBy();
                ViewBag.CompanyList = CompanyList;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }
        // GET: System
        [LoginControl]
        public ActionResult ExcelColumnHeader(FormCollection form = null)
        {
            try
            {

                List<string> viewNameList = new List<string>();
                ProteinEntities proteinEntities = new ProteinEntities();

                Type a = proteinEntities.GetType();
                var b = a.GetMembers();
                foreach (var item in a.GetMembers())
                {
                    foreach (var c in item.CustomAttributes)
                    {
                        if (c.AttributeType.Name == "TableAttribute")
                        {
                            var d = c.ConstructorArguments[0].Value.ToString();
                            if (d[0] == 'V')
                            {
                                viewNameList.Add(d);
                            }
                        }
                    }
                }
                ViewBag.ViewNameList = viewNameList;

                if (form != null && form.AllKeys.Count() > 0)
                {
                    string viewName = form["viewNameList"].ToUpper();
                    ViewBag.SelectViewName = viewName;

                    var TableColumnHeaderList = new GenericRepository<TableColumnHeader>().FindBy($"TABLE_NAME='{viewName}' AND ORDER_NUM IS NOT NULL", orderby: "ORDER_NUM ASC");
                    if (TableColumnHeaderList.Count() < 1)
                    {
                        Type dg = null;
                        //string namemm = "";
                        //foreach (var item in a.GetMembers())
                        //{
                        //    foreach (var c in item.CustomAttributes)
                        //    {
                        //        if (c.AttributeType.Name == "TableAttribute")
                        //        {
                        //            var d = c.ConstructorArguments[0].Value.ToString();
                        //            if (d == viewName)
                        //            {
                        //                var snamemm = item;//.GetTypeInfo().DeclaredProperties;//.GetTypeInfo().DeclaredProperties;//.MemberType.GetType();
                        //                var fsdgs = item.GetType();
                        //                //dg = item.GetType().GetMembers().GetType();// item.GetType();
                        //            }
                        //        }
                        //    }
                        //}
                        //var sf = a.GetMember(namemm)[0].GetType().GetProperties();//.GetMember(namemm)[0].GetType();
                        //foreach (var item in sf)
                        //{
                        //    if (item.Name== "DeclaredProperties")
                        //    {
                        //        var s = "";
                        //        var ss = item.GetType().GetMembers();
                        //    }
                        //}
                        int i = 0;
                        List<TableColumnHeader> tableColumnHeaders = new List<TableColumnHeader>();

                        //var t = dg.GetProperties();
                        List<V_TableColumn> v_TableColumns = new GenericRepository<V_TableColumn>().FindBy($"TABLE_NAME='{viewName}'", orderby: "COLUMN_ORDER ASC", fetchDeletedRows: true, fetchHistoricRows: true);
                        foreach (var TableColumn in v_TableColumns)
                        {
                            i++;
                            //var attributes = prop.GetCustomAttributes(false);
                            //var columnMapping = attributes.FirstOrDefault(x => x.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute));
                            //if (columnMapping!=null)
                            //{
                            //    var realColumn = columnMapping as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute;
                            //    if (realColumn!=null)
                            //    {
                            if (TableColumn.ColumnName != "NEW_VERSION_ID" && TableColumn.ColumnName != "SP_RESPONSE_ID")
                            {
                                TableColumnHeader tableColumnHeader = new TableColumnHeader
                                {
                                    TABLE_NAME = viewName,
                                    COLUMN_NAME = TableColumn.ColumnName,
                                    COLUMN_HEADER = TableColumn.ColumnName,
                                    ORDER_NUM = i
                                };
                                tableColumnHeaders.Add(tableColumnHeader);
                            }
                            //        }
                            //    }
                            //    //new GenericRepository<TableColumnHeader>().Insert(tableColumnHeader);
                        }
                        var spResponseList = new GenericRepository<TableColumnHeader>().InsertSpExecute3(tableColumnHeaders);

                        TableColumnHeaderList = new GenericRepository<TableColumnHeader>().FindBy($"TABLE_NAME='{viewName}' AND ORDER_NUM IS NOT NULL", orderby: "ORDER_NUM ASC");
                    }
                    ViewBag.TableColumnHeaderList = TableColumnHeaderList;
                    ViewBag.itemIds = string.Join(",", TableColumnHeaderList.Select(x => x.Id));
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }
        private class TableColumn
        {
            public Int64 id { get; set; }
            public string columnHeader { get; set; }
            public string status { get; set; }
        }

        // GET: System
        [HttpPost]
        public ActionResult UpdateTableColumn(FormCollection form)
        {
            try
            {
                List<TableColumn> tableColumnHeaders = new List<TableColumn>();
                foreach (var item in form["hditemIds"].Split(','))
                {
                    if (item.IsInt64())
                    {
                        TableColumn tableColumnHeader = new TableColumn
                        {
                            id = long.Parse(item),
                            columnHeader = form["columnHeader-" + item],
                            status = form["isView_" + item] != null ? ((int)Status.AKTIF).ToString() : ((int)Status.PASIF).ToString()
                        };
                        tableColumnHeaders.Add(tableColumnHeader);
                    }
                }

                List<TableColumn> activeTableColumns = tableColumnHeaders.Where(x => x.status == ((int)Status.AKTIF).ToString()).ToList();
                List<TableColumn> passiveTableColumns = tableColumnHeaders.Where(x => x.status == ((int)Status.PASIF).ToString()).ToList();
                int order_num = 1;

                List<TableColumnHeader> tableColumnHeaderList = new List<TableColumnHeader>();
                foreach (var item in activeTableColumns)
                {
                    TableColumnHeader tableColumnHeader = new TableColumnHeader
                    {
                        Id = item.id,
                        COLUMN_HEADER = item.columnHeader,
                        STATUS = item.status,
                        ORDER_NUM = order_num
                    };
                    tableColumnHeaderList.Add(tableColumnHeader);
                    order_num++;
                }
                foreach (var item in passiveTableColumns)
                {
                    TableColumnHeader tableColumnHeader = new TableColumnHeader
                    {
                        Id = item.id,
                        COLUMN_HEADER = item.columnHeader,
                        STATUS = item.status,
                        ORDER_NUM = order_num
                    };
                    tableColumnHeaderList.Add(tableColumnHeader);
                    order_num++;
                }
                var spResponseList = new GenericRepository<TableColumnHeader>().InsertSpExecute3(tableColumnHeaderList);
                //var TableColumnHeaderList = new GenericRepository<TableColumn>().FindBy("TABLE_NAME='V_RPT_CLAIM_PREMIUM' AND ORDER_NUM IS NOT NULL", orderby:"ORDER_NUM ASC");
                //ViewBag.TableColumnHeaderList = TableColumnHeaderList;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }
        [HttpPost]
        [LoginControl]
        public ActionResult GetLogList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string filter = form["isFilter"];
            string companyid = form["CompanyID"];

            if (filter != "0")
            {
                string whereConditition = form["CorrelationID"].IsNull() ? "" : $" CORRELATION_ID='{form["CorrelationID"].ToLower()}' AND";
                whereConditition += form["CompanyID"].IsNull() ? "" : $" COMPANY_ID={form["CompanyID"]} AND";
                whereConditition += form["PayrollID"].IsNull() ? "" : $" PAYROLL_ID={form["PayrollID"]} AND";
                whereConditition += !form["PolicyNo"].IsInt64() ? "" : $" POLICY_NUMBER = {form["PolicyNo"]} AND";
                whereConditition += form["insuredID"].IsNull() ? "" : $" INSURED_CONTACT_ID={form["insuredID"]} AND";
                whereConditition += form["MedulaNo"].IsNull() ? "" : $" MEDULA_NO='{form["MedulaNo"]}' AND";
                whereConditition += !form["TCKN"].IsInt64() ? "" : $" IDENTITY_NO={form["TCKN"]} AND";
                // whereConditition += form["WS_REQUEST_DATE"].IsDateTime() ? $" WS_REQUEST_DATE >= TO_DATE('{ DateTime.Parse(form["WS_REQUEST_DATE"]).ToShortDateString()}','DD.MM.YYYY') AND" : "";
                // whereConditition += form["WS_REQUEST_DATE"].IsDateTime() ? $" WS_REQUEST_DATE=TO_DATE('{ DateTime.Parse(form["WS_REQUEST_DATE"]).ToShortDateString()}','DD.MM.YYYY') AND" : "";
                whereConditition += form["ProviderPayrollID"].IsNull() ? "" : $" EXT_PROVIDER_NO='{form["ProviderPayrollID"]}' AND";
                whereConditition += form["ClaimID"].IsNull() ? "" : $" CLAIM_ID='{form["ClaimID"]}' AND";
                whereConditition += $" ID in (select max(ID) AS ID from V_INTEGRATION_LOG GROUP BY CORRELATION_ID) AND";

                var requestDateControl = form["WS_REQUEST_DATE"];
                if (requestDateControl.IsDateTime())
                {
                    DateTime requestDate = DateTime.Parse(form["WS_REQUEST_DATE"]);
                    DateTime requestDateNextDay = requestDate.AddDays(1);

                    whereConditition += form["WS_REQUEST_DATE"].IsDateTime() ? $" WS_REQUEST_DATE BETWEEN TO_DATE('{ requestDate }','DD.MM.YYYY HH24:MI:SS') AND TO_DATE('{ requestDateNextDay }','DD.MM.YYYY HH24:MI:SS') AND" : "";
                }


                if (form["serviceType"].IsInt())
                {
                    var serviceType = form["serviceType"];
                    if (form["serviceMethod"].IsNull())
                    {
                        string methodNameList = "";
                        switch (serviceType)
                        {
                            case "1":
                                methodNameList = "('PoliceAktar','ZeyilSigortaliCikar','ZeyilSigortaliEkle','PoliceIptal','AcenteTransfer','ProviderTransfer','PoliceBilgiSorgula','PoliceSorgula','UpdateSagmerPolicyInfo')";
                                whereConditition += $" WS_METHOD IN {methodNameList} AND";
                                break;
                            case "2":
                                methodNameList = "('ClaimTransfer','ClaimUpdate','ClaimApproval','ClaimCancellation','ClaimRejection','ClaimWait','PayrollTransfer','PolicyState','TazminatGiris','SetProvisionStatus','IcmalGiris','hakSahipligiSorgula')";
                                whereConditition += $" WS_METHOD IN {methodNameList} AND";
                                break;
                            case "3":
                                methodNameList = "'Protein.Data.ExternalServices.SagmerOnlineService.SgmServiceClient'";
                                whereConditition += $" WS_NAME = {methodNameList} AND";
                                break;
                            case "4":
                                methodNameList = "('TamamlayiciSaglikTazminatGiris','TamamlayiciSaglikProvizyonIptal','SigortaliHakSorgula','IcmalGiris','IcmalTazminatCikarma','RedDurumBildirim','RedDurumIptal')";
                                whereConditition += $" WS_METHOD IN {methodNameList} AND";
                                break;
                            case "5":
                                methodNameList = "'AgreementService.Form1'";
                                whereConditition += $" WS_NAME = {methodNameList} AND";
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        whereConditition += $" WS_METHOD = '{form["serviceMethod"]}' AND";
                    }
                }
                if (whereConditition != " ID in (select max(ID) AS ID from V_INTEGRATION_LOG GROUP BY CORRELATION_ID) AND")
                {

                    var ProviderGroupList = new GenericRepository<V_IntegrationLog>().FindByPaged(
                                                                                  conditions: whereConditition.IsNull() ? "" : whereConditition.Substring(0, whereConditition.Length - 3),
                                                                                  orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection}, CORRELATION_ID desc" : "CORRELATION_ID desc",
                                                                                  pageNumber: start,
                                                                                  rowsPerPage: length, fetchHistoricRows: true);


                    return Json(new { data = ProviderGroupList.Data, draw = Request["draw"], recordsTotal = ProviderGroupList.TotalItemsCount, recordsFiltered = ProviderGroupList.TotalItemsCount, whereConditition = whereConditition }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { data = 0, draw = Request["draw"], recordsTotal = 0, recordsFiltered = 0, whereConditition = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { data = 0, draw = Request["draw"], recordsTotal = 0, recordsFiltered = 0, whereConditition = "" }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetLogListDetail(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];



            string whereConditition = $"CORRELATION_ID='{form["CORRELATION_ID"]}'";

            var ProviderGroupList = new GenericRepository<IntegrationLog>().FindByPaged(conditions: whereConditition,
                                                                         orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection}" : "ID",
                                                                         pageNumber: start,
                                                                         rowsPerPage: length, fetchHistoricRows: true);




            #region Export
            ExportVM vm = new ExportVM();
            vm.WhereCondition = whereConditition;
            vm.SetExportColumns();
            #endregion
            //ViewBag.ProviderGroupList = ProviderGroupList.Data;
            var json = Json(new { data = ProviderGroupList.Data, draw = Request["draw"], recordsTotal = ProviderGroupList.TotalItemsCount, recordsFiltered = ProviderGroupList.TotalItemsCount, whereConditition = vm.WhereCondition }, JsonRequestBehavior.AllowGet);
            return json;
        }

        [HttpPost]
        [LoginControl]
        public ActionResult LogFilter(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            string CorrelationID = form["CorrelationID"];
            string whereConditition = $"CORRELATION_ID='{CorrelationID}'";

            var result = new GenericRepository<V_IntegrationLog>().FindByPaged(conditions: whereConditition,
                                                                         orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection}" : "ID",
                                                                         pageNumber: start,
                                                                         rowsPerPage: length, fetchHistoricRows: true);
            ViewBag.Result = result;

            return View("Index");

        }

        [HttpPost]
        [LoginControl]
        public JsonResult LogSend(Int64 logId = 0)
        {
            var LogList = new GenericRepository<IntegrationLog>().FindBy(conditions: $"ID='{logId}'", fetchHistoricRows: true, orderby: "ID").FirstOrDefault();

            Protein.Data.ExternalServices.InsuranceCompanies.ProxyServiceClient serviceClient = new Data.ExternalServices.InsuranceCompanies.ProxyServiceClient();
            Protein.Data.ExternalServices.InsuranceCompanies.NNHayatServiceClientV2 NNService = new Data.ExternalServices.InsuranceCompanies.NNHayatServiceClientV2();

            var resultJson = new AjaxResultDto<ExclusionSaveResult>();

            string result = "";

            ClaimStateUpdateReq claimStateUpdateReq = new ClaimStateUpdateReq();
            try
            {
                if (LogList.WsMethod == "ClaimApproval")
                {
                    claimStateUpdateReq.ClaimId = (Int64)LogList.ClaimId;
                    var resultMessage = serviceClient.ClaimApproval(claimStateUpdateReq);
                    if (!resultMessage.IsOk)
                    {
                        result = resultMessage.Error[0];
                    }
                }
                if (LogList.WsMethod == "ClaimCancellation")
                {
                    claimStateUpdateReq.ClaimId = (Int64)LogList.ClaimId;
                    var resultMessage = serviceClient.ClaimCancellation(claimStateUpdateReq);
                    if (!resultMessage.IsOk)
                    {
                        result = resultMessage.Error[0];
                    }
                }
                if (LogList.WsMethod == "ClaimRejection")
                {
                    claimStateUpdateReq.ClaimId = (Int64)LogList.ClaimId;
                    var resultMessage = serviceClient.ClaimRejection(claimStateUpdateReq);
                    if (!resultMessage.IsOk)
                    {
                        result = resultMessage.Error[0];
                    }
                }
                if (LogList.WsMethod == "ClaimWait")
                {
                    claimStateUpdateReq.ClaimId = (Int64)LogList.ClaimId;
                    var resultMessage = serviceClient.ClaimWait(claimStateUpdateReq);
                    if (!resultMessage.IsOk)
                    {
                        result = resultMessage.Error[0];
                    }
                }
                if (LogList.WsMethod == "ClaimWait")
                {
                    claimStateUpdateReq.ClaimId = (Int64)LogList.ClaimId;
                    var resultMessage = serviceClient.ClaimWait(claimStateUpdateReq);
                    if (!resultMessage.IsOk)
                    {
                        result = resultMessage.Error[0];
                    }
                }
                if (LogList.WsMethod == "PolicyState")
                {
                    PolicyStateReq policyStateReq = new PolicyStateReq();
                    policyStateReq.PolicyNo = LogList.PolicyId.ToString();
                    var resultMessage = serviceClient.PolicyState(policyStateReq);
                    if (!resultMessage.IsOk)
                    {
                        result = resultMessage.Error[0];
                    }
                }
                if (LogList.WsMethod == "PayrollTransfer")
                {
                    PayrollTransferReq payrollTransferReq = new PayrollTransferReq();
                    payrollTransferReq.PayrollId = (Int64)LogList.PayrollId;
                    var resultMessage = serviceClient.PayrollTransfer(payrollTransferReq);
                    if (!resultMessage.IsOk)
                    {
                        result = resultMessage.Error[0];
                    }
                }
                if (LogList.WsMethod == "ProviderTransfer")
                {
                    ProviderTransferReq providerTransferReq = new ProviderTransferReq();
                    providerTransferReq.ProviderId = (Int64)LogList.ProviderId;
                    var resultMessage = serviceClient.ProviderTransfer(providerTransferReq);
                    if (!resultMessage.IsOk)
                    {
                        result = resultMessage.Error[0];
                    }
                }
                if (LogList.WsMethod == "ProviderListTransfer")
                {
                    ProviderListTransferReq providerTransferReq = new ProviderListTransferReq();
                    providerTransferReq.CompanyCode = (Int64)LogList.COMPANY_ID;
                    var resultMessage = serviceClient.ProviderListTransfer(providerTransferReq);
                    if (!resultMessage.IsOk)
                    {
                        result = resultMessage.Error[0];
                    }
                }
                if (LogList.WsMethod == "ClaimTransfer")
                {
                    claimStateUpdateReq.ClaimId = (Int64)LogList.ClaimId;

                    var resultMessage = serviceClient.ClaimNotice(claimStateUpdateReq);
                    if (!resultMessage.IsOk)
                    {
                        result = resultMessage.Error[0];
                    }
                }
                if (LogList.WsMethod == "ClaimUpdate")
                {
                    claimStateUpdateReq.ClaimId = (Int64)LogList.ClaimId;
                    var resultMessage = serviceClient.ClaimUpdate(claimStateUpdateReq);
                }
                if (LogList.WsMethod == "TazminatGiris" && LogList.WsName.Contains("Hayat"))
                {
                    NNTazminatGirisReq NNTazminatGirisReq = new NNTazminatGirisReq();
                    NNTazminatGirisReq.claimId = (Int64)LogList.ClaimId;
                    var resultMessage = NNService.TazminatGiris(NNTazminatGirisReq);
                    if (!resultMessage.IsOk)
                    {
                        result = resultMessage.Error[0];
                    }
                }
                if (LogList.WsMethod == "IcmalGiris")
                {
                    NNIcmalGirisRequest nNIcmalGirisRequest = new NNIcmalGirisRequest();
                    nNIcmalGirisRequest.PayrollId = (Int64)LogList.PayrollId;
                    var resultMessage = NNService.IcmalGiris(nNIcmalGirisRequest);
                    if (!resultMessage.IsOk)
                    {
                        result = resultMessage.Error[0];
                    }
                }


                if (result != "")
                {
                    resultJson.ResultCode = "999";
                    resultJson.ResultMessage = result;
                }
                else
                {
                    resultJson.ResultCode = "100";
                    resultJson.ResultMessage = "Başarılı";
                }

            }
            catch (Exception ex)
            {
                resultJson.ResultCode = "999";
                resultJson.ResultMessage = ex.Message;
                resultJson.Data = null;
            }
            return Json(resultJson.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public JsonResult xmlLogList(Int64 logId = 0, string type = "")
        {
            var resultJson = new AjaxResultDto<ExclusionSaveResult>();

            string type1 = Request["type"];

            string whereCondititions = $"ID='{logId}'";


            var LogList = new GenericRepository<IntegrationLog>().FindBy(conditions: whereCondititions, orderby: "ID", fetchHistoricRows: true).FirstOrDefault();

            if (type == "1")
            {
                resultJson.ResultMessage = LogList.Request;
            }
            else
            {
                resultJson.ResultMessage = LogList.Response;
            }
            return Json(resultJson.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        // POST: Company Filter /Product/CompanyFilter
        //[HttpPost]
        //[LoginControl]
        //public ActionResult CompanyFilter(FormCollection form)
        //{
        //    TempData["Alert"] = TempData["Alert"] ?? string.Empty;

        //    //Fill data
        //    ViewBag.companyId = form["companyId"].ToString();
        //    ViewBag.companyName = form["companyName"].ToString();

        //    dynamic parameters = new System.Dynamic.ExpandoObject();
        //    parameters.companyId = form["companyId"];
        //    parameters.companyName = form["companyName"];

        //    string whereConditition = form["companyId"].IsInt64() && long.Parse(form["companyId"]) > 0 ? $" COMPANY_ID =:companyId AND" : "";
        //    whereConditition += !form["companyName"].IsNull() ? $" COMPANY_NAME LIKE '%:companyName%' AND" : "";

        //    var result = new GenericRepository<V_Company>().FindBy(whereConditition.IsNull() ? "" : whereConditition.Substring(0, whereConditition.Length - 3), orderby: "COMPANY_ID", parameters: parameters);
        //    ViewBag.Result = result;

        //    return View("Company");
        //}


        [LoginControl]
        public ActionResult AgreementIndex()
        {

            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                //Fill data
                ViewBag.companyId = string.Empty;
                ViewBag.companyName = string.Empty;
                // var result = new GenericRepository<IntegrationLog>().FindBy(orderby: "ID", fetchHistoricRows: true);
                // ViewBag.Result = result;

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }
        [LoginControl]
        [HttpPost]
        public ActionResult GetAgreementLogList()
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string whereConditition = "";
            var agreementLogList = new GenericRepository<Agreement>().FindByPaged(
                                                                                  conditions: whereConditition.IsNull() ? "" : whereConditition.Substring(0, whereConditition.Length - 3),
                                                                                  orderby: "ID",
                                                                                  pageNumber: start,
                                                                                  rowsPerPage: length, fetchDeletedRows: true, fetchHistoricRows: true);

            foreach (var agreement in agreementLogList.Data)
            {
                var companyname = new GenericRepository<Company>().FindBy($"ID={agreement.CompanyId}").FirstOrDefault();
                agreement.CompanyName = companyname.Name;
                agreement.TypeText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.WsAgreementType, agreement.Type);
                agreement.StatusText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.WsAgreementStatus, agreement.Status);
            }
            return Json(new { data = agreementLogList.Data, draw = Request["draw"], recordsTotal = agreementLogList.TotalItemsCount, recordsFiltered = agreementLogList.TotalItemsCount, whereConditition = whereConditition }, JsonRequestBehavior.AllowGet);
        }


        [LoginControl]
        [HttpPost]
        public ActionResult GetAgreementLogDetail(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string CorrelationID = form["CORRELATION_ID"];
            string whereConditition = $"CORRELATION_ID='{CorrelationID}' AND RESPONSE_STATUS_DESC='OK'";

            var agreementLogList = new GenericRepository<V_IntegrationLog>().FindByPaged(conditions: whereConditition,
                                                                         orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection}" : "ID",
                                                                         pageNumber: start,
                                                                         rowsPerPage: length, fetchHistoricRows: true);

            return Json(new { data = agreementLogList.Data, draw = Request["draw"], recordsTotal = agreementLogList.TotalItemsCount, recordsFiltered = agreementLogList.TotalItemsCount, whereConditition = whereConditition }, JsonRequestBehavior.AllowGet);
        }

        [LoginControl]
        [HttpPost]
        public JsonResult GetAgreementDescription(string agreementId, string corrid)
        {
            var resultJson = new AjaxResultDto<ExclusionSaveResult>();

            string type1 = Request["type"];
            ViewBag.CorrelationId = corrid;
            string whereCondititions = $"ID='{agreementId}'";


            var LogList = new GenericRepository<Agreement>().FindBy(conditions: whereCondititions, orderby: "ID", fetchHistoricRows: true, fetchDeletedRows: true).FirstOrDefault();
            if (LogList.Description != "" && LogList.Description != null)
            {
                string[] descList = LogList.Description.Split('.');
                foreach (var desc in descList)
                {
                    resultJson.ResultMessage += desc + "\n";
                }
            }
            else
            {
                resultJson.ResultMessage += "Mutabıktır.";
            }




            return Json(resultJson.ToJSON(), JsonRequestBehavior.AllowGet);

        }

        [LoginControl]
        public ActionResult SagmerEndorsementTransfer(FormCollection form)
        {

            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;
                var CompanyList = new CompanyRepository().FindBy();
                ViewBag.Product = LookupHelper.GetLookupData(LookupTypes.Product);
                ViewBag.CompanyList = CompanyList;
                //Fill data
                ViewBag.companyId = string.Empty;
                ViewBag.companyName = string.Empty;
                // var result = new GenericRepository<IntegrationLog>().FindBy(orderby: "ID", fetchHistoricRows: true);
                // ViewBag.Result = result;

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }
        [HttpPost]
        [LoginControl]
        public ActionResult GetSagmerEndorsementTransferList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            //string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            dynamic policyListData = null;
            int policyListTotalItemsCount = 0;
            string CorrelationID = form["CORRELATION_ID"];
            string authorizationCodePolicy = form["authorizationCodePolicy"];
            string whereConditition = $"SBM_NO IS NULL AND";
            whereConditition += !String.IsNullOrEmpty(form["renewalNo"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" RENEWAL_NO={long.Parse(form["renewalNo"])} " : "";
            whereConditition += !String.IsNullOrEmpty(form["Product"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" PRODUCT_ID = {long.Parse(form["Product"])} AND" : "";
            whereConditition += form["policyNo"].IsInt64() ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_NUMBER = {form["policyNo"]} " : "";
            List<PolicyList> PolicyList = new List<PolicyList>();

            whereConditition += form["CompanyID"].IsInt64() ? $" COMPANY_ID={form["CompanyID"]} AND" : "";
            if (whereConditition != "SBM_NO IS NULL AND")
            {
                var policyList = new GenericRepository<V_Policy>().FindByPaged(conditions: whereConditition.IsNull() ? "" : whereConditition.Substring(0, whereConditition.Length - 3),
                                                                         orderby: "POLICY_ID",
                                                                         pageNumber: start,
                                                                         rowsPerPage: length, fetchHistoricRows: true);


                policyListData = policyList.Data;
                policyListTotalItemsCount = policyList.TotalItemsCount;

                if (policyListData != null)
                {
                    foreach (var item in policyListData)
                    {
                        PolicyList listItem = new PolicyList();

                        listItem.POLICY_ID = Convert.ToString(item.POLICY_ID);
                        listItem.POLICY_TYPE_TEXT = LookupHelper.GetLookupCodeByOrdinal(LookupTypes.Policy, item.POLICY_TYPE);
                        listItem.POLICY_NUMBER_RENEWAL_NO = Convert.ToString(item.POLICY_NUMBER) + " - " + Convert.ToString(item.RENEWAL_NO);
                        listItem.POLICY_START_DATE = Convert.ToString(item.POLICY_START_DATE);
                        listItem.POLICY_START_DATE = listItem.POLICY_START_DATE.IsDateTime() ? DateTime.Parse(listItem.POLICY_START_DATE).ToString("dd-MM-yyyy") : string.Empty;
                        listItem.INSURER_NAME = Convert.ToString(item.INSURER_NAME);
                        listItem.COMPANY_NAME = Convert.ToString(item.COMPANY_NAME);
                        listItem.PRODUCT_NAME = Convert.ToString(item.PRODUCT_NAME);
                        listItem.PREMIUM = Convert.ToString(item.PREMIUM);
                        listItem.POLICY_END_DATE = Convert.ToString(item.POLICY_END_DATE);
                        listItem.POLICY_END_DATE = listItem.POLICY_END_DATE.IsDateTime() ? DateTime.Parse(listItem.POLICY_END_DATE).ToString("dd-MM-yyyy") : string.Empty;

                        listItem.IS_OPEN_TO_CLAIM = Convert.ToString(item.IS_OPEN_TO_CLAIM);

                        var endorsmentList = new GenericRepository<Endorsement>().FindBy($"POLICY_ID={listItem.POLICY_ID} AND STATUS={((int)(Status.PASIF))}", fetchDeletedRows: true);
                        if (endorsmentList != null && endorsmentList.Count > 0)
                        {
                            listItem.IS_NOT_ACTIVE_ENDORSEMENT = "0";
                        }
                        else
                        {
                            listItem.IS_NOT_ACTIVE_ENDORSEMENT = "1";
                        }

                        listItem.STATUS = Convert.ToString(item.STATUS);

                        PolicyList.Add(listItem);
                    }
                }


                return Json(new { data = PolicyList, draw = Request["draw"], recordsTotal = policyListTotalItemsCount, recordsFiltered = policyListTotalItemsCount }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { data = 0, draw = Request["draw"], recordsTotal = 0, recordsFiltered = 0, whereConditition = "" }, JsonRequestBehavior.AllowGet);

            }
        }
        [HttpPost]
        [LoginControl]
        public ActionResult GetEndorsementList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            //string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            Protein.WS.WS.Common.SagmerSrv sagmerSrv = new SagmerSrv();
            string sortDirection = Request["order[0][dir]"];
            string hdPolicyId = form["hdPolicyId"];
            string hdEndorsementTransferList = form["hdEndorsementTransferList"];
            string hdAuthorizationCode = form["authorizationCode"];
            string whereConditition = $"SBM_NO IS NULL AND";

            whereConditition += form["hdPolicyId"].IsInt64() ? $" POLICY_ID={form["hdPolicyId"]} AND" : "";

            string[] endorsementList = hdEndorsementTransferList.Split(',');
            if (whereConditition != "SBM_NO IS NULL AND")
            {
                var agreementLogList = new GenericRepository<V_PolicyEndorsement>().FindByPaged(conditions: whereConditition.IsNull() ? "" : whereConditition.Substring(0, whereConditition.Length - 3),
                                                            orderby: "ENDORSEMENT_NO",
                                                            pageNumber: start,
                                                            rowsPerPage: length, fetchHistoricRows: true);

                if (!string.IsNullOrEmpty(hdEndorsementTransferList))
                {
                    for (int i = 0; i < endorsementList.Length; i++)
                    {
                        var policySbmTransfer = new GenericRepository<V_PolicyEndorsement>().FindBy($"POLICY_ID={hdPolicyId} AND ENDORSEMENT_NO={endorsementList[i]}", orderby: "POLICY_NUMBER").FirstOrDefault();

                        if (policySbmTransfer.ENDORSEMENT_TYPE == ((int)EndorsementType.BASLANGIC).ToString())
                        {
                            SbmPolicyCreateReq req = new SbmPolicyCreateReq();
                            req.PolicyId = policySbmTransfer.POLICY_ID;
                         
                            var SagmerAktar = sagmerSrv.police(req, Data.ExternalServices.SagmerOnlineService.Enums.SagmerServiceType.CANLI);

                        }

                        if (policySbmTransfer.ENDORSEMENT_TYPE == ((int)EndorsementType.SIGORTALI_CIKISI).ToString())
                        {
                            SbmInsuredExitReq req = new SbmInsuredExitReq();
                            req.PolicyId = policySbmTransfer.POLICY_ID;
                            req.EndorsementId = policySbmTransfer.ENDORSEMENT_ID;
                            req.AuthorizationCode = hdAuthorizationCode;
                            var SagmerAktar = sagmerSrv.sigortaliCikisZeyli(req, Data.ExternalServices.SagmerOnlineService.Enums.SagmerServiceType.CANLI);

                        }
                        if (policySbmTransfer.ENDORSEMENT_TYPE == ((int)EndorsementType.SIGORTALI_GIRISI).ToString())
                        {
                            SbmInsuredEntranceReq req = new SbmInsuredEntranceReq
                            {
                                PolicyId = policySbmTransfer.POLICY_ID,
                                EndorsementId = policySbmTransfer.ENDORSEMENT_ID,
                                Authorization= hdAuthorizationCode
                            };
                            var SagmerAktar = sagmerSrv.sigortaliGirisZeyli(req, Data.ExternalServices.SagmerOnlineService.Enums.SagmerServiceType.CANLI);

                        }
                        if (policySbmTransfer.ENDORSEMENT_TYPE == ((int)EndorsementType.SIGORTALI_TAHAKKUK).ToString())
                        {
                            var insured = new GenericRepository<V_Insured>().FindBy($"ENDORSEMENT_ID={policySbmTransfer.ENDORSEMENT_ID}").FirstOrDefault();
                            SbmInsuredAccrualReq req = new SbmInsuredAccrualReq
                            {
                                PolicyId = policySbmTransfer.POLICY_ID,
                                EndorsementId = policySbmTransfer.ENDORSEMENT_ID,
                                Authorization= hdAuthorizationCode,
                                InsuredId=insured.INSURED_ID
                            };
                            var SagmerAktar = sagmerSrv.sigortaliTahakkukZeyli(req, Data.ExternalServices.SagmerOnlineService.Enums.SagmerServiceType.CANLI);

                        }
                        if (policySbmTransfer.ENDORSEMENT_TYPE == ((int)EndorsementType.TEMINAT_PLAN_DEGISIKLIGI).ToString())
                        {
                            SbmInsuredPackageChangeReq req = new SbmInsuredPackageChangeReq
                            {
                                PolicyId = policySbmTransfer.POLICY_ID,
                                EndorsementId = policySbmTransfer.ENDORSEMENT_ID,
                                Authorization= hdAuthorizationCode
                            };
                            var SagmerAktar = sagmerSrv.teminatPlanDegisiklikZeyli(req,Data.ExternalServices.SagmerOnlineService.Enums.SagmerServiceType.CANLI);

                        }
                        if (policySbmTransfer.ENDORSEMENT_TYPE == ((int)EndorsementType.URETIM_KAYNAGI_DEGISIKLIGI).ToString())
                        {
                            SbmAgencyChangeReq req = new SbmAgencyChangeReq
                            {
                                PolicyId = policySbmTransfer.POLICY_ID,
                                EndorsementId = policySbmTransfer.ENDORSEMENT_ID,
                                Authorization= hdAuthorizationCode
                            };
                            var SagmerAktar = sagmerSrv.uretimKaynakDegisiklikZeyli(req, Data.ExternalServices.SagmerOnlineService.Enums.SagmerServiceType.CANLI);
                        }
                        if (policySbmTransfer.ENDORSEMENT_TYPE == ((int)EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI).ToString())
                        {
                            SbmInsuredInfoChangeReq req = new SbmInsuredInfoChangeReq
                            {
                                PolicyId = policySbmTransfer.POLICY_ID,
                                EndorsementId = policySbmTransfer.ENDORSEMENT_ID,
                                Authorization= hdAuthorizationCode
                            };
                            var SagmerAktar = sagmerSrv.sigortaliBilgiDegisiklikZeyli(req, Data.ExternalServices.SagmerOnlineService.Enums.SagmerServiceType.CANLI);
                        }
                        if (policySbmTransfer.ENDORSEMENT_TYPE == ((int)EndorsementType.PRIM_FARKI_ZEYLI).ToString() || policySbmTransfer.ENDORSEMENT_TYPE == ((int)EndorsementType.POLICE_TAHAKKUK_ZEYLI).ToString())
                        {
                            SbmPolicyAccrualReq req = new SbmPolicyAccrualReq
                            {
                                PolicyId = policySbmTransfer.POLICY_ID,
                                EndorsementId = policySbmTransfer.ENDORSEMENT_ID,
                                Authorization= hdAuthorizationCode
                            };
                            var SagmerAktar = sagmerSrv.policeTahakkukVePrimFarkiZeyli(req, Data.ExternalServices.SagmerOnlineService.Enums.SagmerServiceType.CANLI);
                        }
                        if (policySbmTransfer.ENDORSEMENT_TYPE == ((int)EndorsementType.MERIYETE_DONUS_ZEYLI).ToString())
                        {
                            SbmPolicyReturnStartReq req = new SbmPolicyReturnStartReq
                            {
                                PolicyId = policySbmTransfer.POLICY_ID,
                                EndorsementId = policySbmTransfer.ENDORSEMENT_ID,
                                Authorization= hdAuthorizationCode
                            };
                            var SagmerAktar = sagmerSrv.meriyeteDonusZeyli(req, Data.ExternalServices.SagmerOnlineService.Enums.SagmerServiceType.CANLI);
                        }
                        if (policySbmTransfer.ENDORSEMENT_TYPE == ((int)EndorsementType.IPTAL).ToString() || policySbmTransfer.ENDORSEMENT_TYPE == ((int)EndorsementType.GUN_ESASLI_IPTAL).ToString() || policySbmTransfer.ENDORSEMENT_TYPE == ((int)EndorsementType.DONDURMA_SONUCU_IPTAL).ToString() || policySbmTransfer.ENDORSEMENT_TYPE == ((int)EndorsementType.KISA_DONEM_ESASLI_IPTAL).ToString() || policySbmTransfer.ENDORSEMENT_TYPE == ((int)EndorsementType.MEBDEINDEN_BASLANGICTAN_IPTAL).ToString() || policySbmTransfer.ENDORSEMENT_TYPE == ((int)EndorsementType.PRIM_IADESIZ_IPTAL).ToString())
                        {
                            SbmPolicyCancelReq req = new SbmPolicyCancelReq
                            {
                                PolicyId = policySbmTransfer.POLICY_ID,
                                EndorsementId = policySbmTransfer.ENDORSEMENT_ID,
                                Authorization= hdAuthorizationCode
                            };
                            var SagmerAktar = sagmerSrv.genelIptalZeyli(req, Data.ExternalServices.SagmerOnlineService.Enums.SagmerServiceType.CANLI);
                        }
                        if (policySbmTransfer.ENDORSEMENT_TYPE == ((int)EndorsementType.SE_DEGISIKLIGI).ToString())
                        {
                            SbmInsurerChangeReq req = new SbmInsurerChangeReq
                            {
                                PolicyId = policySbmTransfer.POLICY_ID,
                                EndorsementId = policySbmTransfer.ENDORSEMENT_ID,
                                Authorization= hdAuthorizationCode
                            };
                            var SagmerAktar = sagmerSrv.sigortaEttirenDegisiklikZeyli(req, Data.ExternalServices.SagmerOnlineService.Enums.SagmerServiceType.CANLI);
                        }

                       


                    }
                      

                    
                }

                return Json(new { data = agreementLogList.Data, draw = Request["draw"], recordsTotal = agreementLogList.TotalItemsCount, recordsFiltered = agreementLogList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
            }

            

            return Json(new { data = 0, draw = Request["draw"], recordsTotal = 0, recordsFiltered = 0 }, JsonRequestBehavior.AllowGet);


        }

        [HttpPost]
        [LoginControl]
        public ActionResult SagmerTransfer(string policyId)
        {
            

            var policy = new GenericRepository<V_Policy>().FindBy($"POLICY_ID={policyId}" , orderby:"POLICY_ID").FirstOrDefault();
            SbmPolicyCreateReq sbmPolicyCreateReq = new SbmPolicyCreateReq();
            sbmPolicyCreateReq.PolicyId = policy.POLICY_ID;

            Protein.WS.WS.Common.SagmerSrv sagmerSrv = new SagmerSrv();
          var SagmerAktar = sagmerSrv.police(sbmPolicyCreateReq,Data.ExternalServices.SagmerOnlineService.Enums.SagmerServiceType.KONTROL);

            return View();
        }


    }
}