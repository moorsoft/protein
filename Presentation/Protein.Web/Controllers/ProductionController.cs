﻿using Dapper;
using Newtonsoft.Json;
using Protein.Business.Abstract.Print;
using Protein.Business.Abstract.Service;
using Protein.Business.Concrete.Responses;
using Protein.Business.Concrete.Services;
using Protein.Business.Enums.Import;
using Protein.Business.Print;
using Protein.Business.Print.FamilyBasedInsuredProfile;
using Protein.Business.Print.FamilyBasedTransferProfile;
using Protein.Business.Print.InsuredProfile;
using Protein.Business.Print.InsuredTransfer;
using Protein.Business.Print.MainPolicy;
using Protein.Business.Print.PolicyCertificate;
using Protein.Business.Print.PolicyEndorsment;
using Protein.Business.Print.PolicyInsuredExit;
using Protein.Business.Print.PolicyProfile;
using Protein.Business.Print.PolicyTransfer;
using Protein.Common.Constants;
using Protein.Common.Dto;
using Protein.Common.Dto.ImportObjects;
using Protein.Common.Extensions;
using Protein.Common.Resources;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Web.ActionFilter.LoginAttr;
using Protein.Web.Models.AgencyModel;
using Protein.Web.Models.ExportModel;
using Protein.Web.Models.ImportModel;
using Protein.Web.Models.ListView;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Web.Controllers
{
    [LoginControl]
    public class ProductionController : Controller
    {
        #region Agency
        [LoginControl]
        public ActionResult Agency()
        {
            AgencyVM vm = new AgencyVM();
            vm.ImportVM.ImportObjectType = ImportObjectType.Agency;

            #region Export
            vm.ExportVM.ViewName = Constants.Views.Agency;
            vm.ExportVM.SetExportColumns();
            vm.ExportVM.ExportType = ExportType.Excel;
            #endregion

            try
            {
                var CompanyList = new CompanyRepository().FindBy();
                ViewBag.CompanyList = CompanyList;
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;
                var CityList = new CityRepository().FindBy(orderby: "NAME");
                ViewBag.CityList = CityList;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View(vm);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult AgencyFilter(FormCollection form)
        {
            AgencyVM vm = new AgencyVM();
            vm.ImportVM.ImportObjectType = ImportObjectType.Agency;
            vm.ExportVM.ViewName = Views.Agency;
            try
            {
                int start = Convert.ToInt32(Request["start"]);
                int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
                string searchValue = Request["search[value]"];
                string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
                string sortDirection = Request["order[0][dir]"];
                string isFilter = Request["isFilter"];
                List<AgencyList> AgencyList = new List<AgencyList>();
                ViewResultDto<List<V_Agency>> agencyList = new ViewResultDto<List<V_Agency>>();

                if (isFilter == "1")
                {
                    string whereConditition = !string.IsNullOrEmpty(form["agencyCompanyId"]) ? $" COMPANY_ID = {form["agencyCompanyId"]} " : "";
                    whereConditition += !String.IsNullOrEmpty(form["agencyCompanyName"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" AGENCY_NAME LIKE '%{form["agencyCompanyName"]}%' " : "";
                    whereConditition += !String.IsNullOrEmpty(form["agencyCompanyNo"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" AGENCY_NO LIKE '%{form["agencyCompanyNo"]}%' " : "";

                    #region Export
                    vm.ExportVM.WhereCondition = string.IsNullOrEmpty(whereConditition) ? string.Empty : whereConditition;
                    vm.ExportVM.SetExportColumns();
                    #endregion
                    if (whereConditition != "")
                    {
                        agencyList = new GenericRepository<V_Agency>().FindByPaged(conditions: whereConditition,
                                                                 orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection}" : "",
                                                                 pageNumber: start,
                                                                 rowsPerPage: length);

                    }


                    if (agencyList.Data != null)
                    {
                        string agencyIdList = string.Join(",", agencyList.Data.Select(x => x.AGENCY_ID));
                        var agencyDate = new GenericRepository<SpResponse>().FindBy($"OBJECT_ID = 'T_AGENCY' AND PK_ID IN ({agencyIdList})", orderby: "", fetchDeletedRows: true, fetchHistoricRows: true);

                        foreach (var item in agencyList.Data)
                        {

                            AgencyList listItem = new AgencyList();
                            listItem.AGENCY_ID = Convert.ToString(item.AGENCY_ID);
                            listItem.COMPANY_ID = Convert.ToString(item.COMPANY_ID);
                            listItem.COMPANY_NAME = Convert.ToString(item.COMPANY_NAME);
                            listItem.AGENCY_NAME = Convert.ToString(item.AGENCY_NAME);
                            listItem.AGENCY_NO = Convert.ToString(item.AGENCY_NO);
                            if (agencyDate.Count() > 0)
                            {
                                DateTime createdate = Convert.ToDateTime(agencyDate.Where(x => x.PkId == item.AGENCY_ID).OrderBy(x => x.Id).FirstOrDefault().SpRequestDate);
                                DateTime modificationDate = Convert.ToDateTime(agencyDate.Where(x => x.PkId == item.AGENCY_ID).OrderByDescending(x => x.Id).FirstOrDefault().SpRequestDate);
                                if (createdate.Year > 1)
                                {
                                    listItem.CREATE_DATE = createdate.ToString();
                                    listItem.MODIFICATION_DATE = modificationDate.ToString();
                                }
                                else
                                {
                                    listItem.CREATE_DATE = "-";
                                    listItem.MODIFICATION_DATE = "-";
                                }
                            }
                            else
                            {
                                listItem.CREATE_DATE = "-";
                                listItem.MODIFICATION_DATE = "-";
                            }


                            listItem.STATUS = Convert.ToString(LookupHelper.GetLookupTextByOrdinal(LookupTypes.Status, item.STATUS));

                            AgencyList.Add(listItem);
                        }
                    }
                }
                return Json(new { data = AgencyList, draw = Request["draw"], recordsTotal = agencyList.TotalItemsCount, recordsFiltered = agencyList.TotalItemsCount, whereConditition = vm.ExportVM.WhereCondition }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View("Agency", vm);
        }

        [LoginControl]
        public ActionResult AgencyDelete(Int64 id)
        {
            try
            {
                var repo = new AgencyRepository();
                var result = repo.FindById(id);
                result.Status = "1";
                var spResponse = repo.Update(result);
                if (spResponse.Code == "100")
                    TempData["Alert"] = $"swAlert('İşlem Başarılı','Silme İşlemi Başarıyla Gerçekleştirildi.','success')";
                else
                    throw new Exception(spResponse.Code + " : " + spResponse.Message);
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Agency");
        }

        [LoginControl]
        public ActionResult AgencyForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                var AgencyTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Agency, showChoose: true);
                ViewBag.AgencyTypeList = AgencyTypeList;

                var CompanyList = new CompanyRepository().FindBy();
                ViewBag.CompanyList = CompanyList;

                var CityList = new CityRepository().FindBy(orderby: "CODE");
                ViewBag.CityList = CityList;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.CountyList = null;
                ViewBag.isEdit = false;

                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Acente Güncelle";
                        if (id > 0)
                        {
                            var agency = new GenericRepository<V_Agency>().FindBy("AGENCY_ID=:id", orderby: "AGENCY_ID", parameters: new { id }).FirstOrDefault();
                            if (agency != null)
                            {
                                ViewBag.Agency = agency;

                                var phone = new GenericRepository<V_AgencyPhone>().FindBy("AGENCY_ID=:id AND PHONE_TYPE=:type", orderby: "AGENCY_ID", parameters: new { id, type = new DbString { Value = ((int)PhoneType.IS).ToString(), Length = 3 } }).FirstOrDefault();
                                if (phone != null)
                                {
                                    ViewBag.Phone = phone;
                                }

                                var mobilePhone = new GenericRepository<V_AgencyPhone>().FindBy("AGENCY_ID=:id AND PHONE_TYPE=:type", orderby: "AGENCY_ID", parameters: new { id, type = new DbString { Value = ((int)PhoneType.MOBIL).ToString(), Length = 3 } }).FirstOrDefault();
                                if (mobilePhone != null)
                                {
                                    ViewBag.MobilePhone = mobilePhone;
                                }

                                var faxPhone = new GenericRepository<V_AgencyPhone>().FindBy("AGENCY_ID=:id AND PHONE_TYPE=:type", orderby: "AGENCY_ID", parameters: new { id, type = new DbString { Value = ((int)PhoneType.FAX).ToString(), Length = 3 } }).FirstOrDefault();
                                if (faxPhone != null)
                                {
                                    ViewBag.FaxPhone = faxPhone;
                                }

                                var CountyList = new CountyRepository().FindBy(orderby: "NAME");
                                ViewBag.CountyList = CountyList;

                                ViewBag.isEdit = true;
                            }
                            else
                            {
                                TempData["Alert"] = $"swAlert('Bilgi','Acente Bilgisi Bulunamadı. Lütfen Tekrar Deneyiniz...','information')";
                                return RedirectToAction("Agency");
                            }
                        }
                        break;

                    default:
                        ViewBag.Title = "Acente Ekle";
                        break;
                }

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public JsonResult AgencyFormSave(FormCollection form)
        {
            ServiceResponse response = new ServiceResponse();

            try
            {
                AgencyDto policyDto = new AgencyDto();
                IService<AgencyDto> policyService = new AgencyService();

                #region T_AGENCY
                var agencyId = form["hdAgencyId"];
                if (!Extensions.IsInt64(agencyId))
                {
                    throw new Exception("Acente Kaydedilirken Bir Hata Oluştu. Lütfen Tekrar Deneyiniz...");
                }
                else if (Extensions.IsNull(agencyId) || long.Parse(agencyId) < 0)
                {
                    policyDto.AgencyId = 0;
                }
                else
                {
                    policyDto.AgencyId = long.Parse(agencyId);
                }

                var name = form["agencyName"];
                if (Extensions.IsNull(name))
                {
                    throw new Exception("Acente Adı Girmeniz Gerekmektedir.");
                }
                else
                {
                    policyDto.AgencyTitle = name;
                }

                var companyId = form["agencyCompany"];
                if (!Extensions.IsInt64(companyId) || long.Parse(companyId) < 0)
                {
                    throw new Exception("Şirket Seçimi Yapmanız Gerekmektedir.");
                }
                else
                {
                    policyDto.CompanyId = long.Parse(companyId);
                }

                var addressId = form["hdAddressId"]; //acente
                if (!Extensions.IsInt64(addressId) || long.Parse(addressId) < 1)
                {
                    policyDto.AddressId = 0;
                }
                else
                {
                    policyDto.AddressId = long.Parse(addressId);
                }

                policyDto.Type = Extensions.IsNull(form["AGENCY_TYPE"]) ? null : form["AGENCY_TYPE"];

                policyDto.No = Extensions.IsNull(form["agencyNo"]) ? null : form["agencyNo"];

                policyDto.ContactFirstName = form["CONTACT_FIRST_NAME"].IsNull() ? null : form["CONTACT_FIRST_NAME"];

                policyDto.ContactLastName = form["CONTACT_LAST_NAME"].IsNull() ? null : form["CONTACT_LAST_NAME"];

                policyDto.ContactPhone = form["CONTACT_PHONE"].IsNull() ? null : form["CONTACT_PHONE"];

                policyDto.RegionCode = Extensions.IsNull(form["REGION_CODE"]) ? null : form["REGION_CODE"];

                policyDto.RegionName = Extensions.IsNull(form["REGION_NAME"]) ? null : form["REGION_NAME"];

                policyDto.PlateNo = Extensions.IsNull(form["PLATE_NO"]) ? null : form["PLATE_NO"];

                policyDto.IsOpen = Extensions.IsNull(form["IS_OPEN"]) ? "0" : form["IS_OPEN"];

                policyDto.EstablishmentDate = !Extensions.IsDateTime(form["ESTABLISHMENT_DATE"]) ? null : (DateTime?)DateTime.Parse(form["ESTABLISHMENT_DATE"]);

                policyDto.CloseDownDate = !Extensions.IsDateTime(form["CLOSE_DOWN_DATE"]) ? null : (DateTime?)DateTime.Parse(form["CLOSE_DOWN_DATE"]);

                var contactId = form["hdContactId"]; //acente
                if (!Extensions.IsInt64(contactId) || long.Parse(contactId) < 1)
                {
                    policyDto.ContactId = 0;
                }
                else
                {
                    policyDto.ContactId = long.Parse(contactId);
                }

                #endregion

                #region T_CONTACT

                policyDto.TaxNumber = form["TAX_NUMBER"].IsInt64() ? (long?)Convert.ToInt64(form["TAX_NUMBER"]) : null;

                policyDto.TaxOffice = Extensions.IsNull(form["TAX_OFFICE"]) ? null : form["TAX_OFFICE"];

                #endregion

                #region T_ADDRESS

                policyDto.Address = Extensions.IsNull(form["agencyAdress"]) ? null : form["agencyAdress"];

                policyDto.City = Extensions.IsNull(form["agencyCity"]) ? null : form["agencyCity"];

                policyDto.County = Extensions.IsNull(form["agencyCounty"]) ? null : form["agencyCounty"];

                policyDto.ZipCode = Extensions.IsNull(form["agencyZipCode"]) ? null : form["agencyZipCode"];

                #endregion

                #region T_PHONE

                #region Phone
                policyDto.PhoneId = !Extensions.IsInt64(form["hdPhoneId"]) ? 0 : long.Parse(form["hdPhoneId"]);

                policyDto.AgencyPhoneId = !Extensions.IsInt64(form["hdAgencyPhoneId"]) ? 0 : long.Parse(form["hdAgencyPhoneId"]);

                policyDto.PhoneNo = Extensions.IsNull(form["AgencyPhoneNo"]) ? null : form["AgencyPhoneNo"];
                #endregion

                #region Mobile Phone
                policyDto.MobilePhoneId = !Extensions.IsInt64(form["hdMobilePhoneId"]) ? 0 : long.Parse(form["hdMobilePhoneId"]);

                policyDto.AgencyMobilePhoneId = !Extensions.IsInt64(form["hdAgencyMobilePhoneId"]) ? 0 : long.Parse(form["hdAgencyMobilePhoneId"]);

                policyDto.MobileNo = Extensions.IsNull(form["AgencyGsmPhoneNo"]) ? null : form["AgencyGsmPhoneNo"];
                #endregion

                #region Fax Phone
                policyDto.FaxPhoneId = !Extensions.IsInt64(form["hdFaxPhoneId"]) ? 0 : long.Parse(form["hdFaxPhoneId"]);

                policyDto.AgencyFaxPhoneId = !Extensions.IsInt64(form["hdAgencyFaxPhoneId"]) ? 0 : long.Parse(form["hdAgencyFaxPhoneId"]);

                policyDto.FaxNo = Extensions.IsNull(form["AgencyFaxNo"]) ? null : form["AgencyFaxNo"];
                #endregion

                #endregion

                response = policyService.ServiceInsert(policyDto, Token: "");
                if (!response.IsSuccess)
                {
                    throw new Exception(response.Code + " : " + response.Message);
                }
                TempData["Alert"] = $"swAlert('İşlem Başarılı','Tüm Veriler Başarıyla Kaydedildi.','success')";

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(response.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region PolicyGroup
        [LoginControl]
        public ActionResult PolicyGroup()
        {
            try
            {
                var ParentPolicyGroupList = new PolicyGroupRepository().FindBy();
                ViewBag.ParentPolicyGroupList = ParentPolicyGroupList;

                ViewBag.PARENT_ID = string.Empty;
                ViewBag.POLICY_GROUP_NAME = string.Empty;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;


                var result = new GenericRepository<V_PolicyGroup>().FindBy(orderby: "POLICY_GROUP_NAME");
                ViewBag.PolicyGroupList = result;

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult PolicyGroupFilter(FormCollection form)
        {
            try
            {
                var PolicyGroupList = new PolicyGroupRepository().FindBy();
                ViewBag.ParentPolicyGroupList = PolicyGroupList;
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                ViewBag.PARENT_ID = form["PARENT_ID"];
                ViewBag.POLICY_GROUP_NAME = form["POLICY_GROUP_NAME"];

                string whereConditition = !String.IsNullOrEmpty(form["PARENT_ID"]) ? $" PARENT_ID='{form["PARENT_ID"]}' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["POLICY_GROUP_NAME"]) ? $" POLICY_GROUP_NAME LIKE '%{form["POLICY_GROUP_NAME"]}%' AND" : "";
                var result = new GenericRepository<V_PolicyGroup>().FindBy(whereConditition.IsNull() ? "" : whereConditition.Substring(0, whereConditition.Length - 3), orderby: "POLICY_GROUP_NAME");
                ViewBag.PolicyGroupList = result;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View("PolicyGroup");
        }

        [LoginControl]
        public ActionResult PolicyGroupDelete(Int64 id)
        {
            try
            {
                var repo = new PolicyGroupRepository();
                var result = repo.FindById(id);
                result.Status = "1";
                var spResponse = repo.Update(result);
                if (spResponse.Code == "100")
                {
                    TempData["Alert"] = $"swAlert('İşlem Başarılı','Silme İşlemi Başarıyla Gerçekleştirildi.','success')";
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("PolicyGroup");
        }

        [LoginControl]
        public ActionResult PolicyGroupForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                var PolicyGroupList = new PolicyGroupRepository().FindBy();
                ViewBag.PolicyGroupList = PolicyGroupList;

                ViewBag.RuleCategoryTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.RuleCategory);

                var RuleList = new GenericRepository<V_Rule>().FindBy(orderby: "RULE_NAME ASC");
                ViewBag.RuleList = RuleList;

                var RuleGroupList = new GenericRepository<RuleGroup>().FindBy();
                ViewBag.RuleGroupList = RuleGroupList;

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                switch (formaction.ToLower())
                {
                    case "edit":
                        ViewBag.Title = "Poliçe Grubu Güncelle";
                        if (id != 0)
                        {
                            var policyGroup = new PolicyGroupRepository().FindById(id);
                            if (policyGroup != null)
                            {
                                ViewBag.PolicyGroup = policyGroup;

                                //RuleSpecial
                                var ruleSpecialList = new GenericRepository<V_RulePolicyGroup>().FindBy("POLICY_GROUP_ID=:id AND TYPE=:type", orderby: "", parameters: new { id, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });
                                if (ruleSpecialList != null)
                                {
                                    List<dynamic> rulesList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in ruleSpecialList)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.RULE_POLICY_GROUP_ID = Convert.ToString(item.RULE_POLICY_GROUP_ID);
                                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                        listItem.RULE_NAME = item.RULE_NAME;
                                        listItem.RESULT = item.RESULT;
                                        listItem.INHERITED_NAME = string.Empty;
                                        listItem.INHERITED_CODE = string.Empty;
                                        listItem.isOpen = "0";

                                        rulesList.Add(listItem);

                                        i++;
                                    }

                                    ViewBag.RuleSpecialList = rulesList.ToJSON();
                                }

                                //RuleExtra
                                var ruleExtraList = new GenericRepository<V_RulePolicyGroup>().FindBy("POLICY_GROUP_ID=:id AND TYPE=:type", orderby: "", parameters: new { id, type = new DbString { Value = ((int)RuleType.EK_PROTOKOL).ToString(), Length = 3 } });
                                if (ruleExtraList != null)
                                {
                                    List<dynamic> rulesList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in ruleExtraList)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.RULE_POLICY_GROUP_ID = Convert.ToString(item.RULE_POLICY_GROUP_ID);
                                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                        listItem.RULE_NAME = item.RULE_NAME;
                                        listItem.RESULT = item.RESULT;
                                        listItem.INHERITED_NAME = string.Empty;
                                        listItem.INHERITED_CODE = string.Empty;
                                        listItem.isOpen = "0";

                                        rulesList.Add(listItem);

                                        i++;
                                    }

                                    ViewBag.RuleExtraList = rulesList.ToJSON();
                                }
                            }
                            else
                            {
                                TempData["Alert"] = $"swAlert('Bilgi','Kayıt Bulunamadı. Lütfen Tekrar Deneyiniz.','information')";
                                return RedirectToAction("PolicyGroup");
                            }
                        }
                        ViewBag.isEdit = true;
                        break;

                    default:
                        ViewBag.Title = "Poliçe Grubu Ekle";
                        ViewBag.isEdit = false;
                        break;
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult PolicyGroupSave(FormCollection form)
        {
            var result = new AjaxResultDto<PolicyGroupSaveResult>();
            PolicyGroupSaveResult step1Result = new PolicyGroupSaveResult();
            try
            {
                var parentId = form["PARENT_ID"];
                var name = form["POLICY_GROUP_NAME"];
                var policyGroupId = long.Parse(form["hdPolicyGroupId"]);

                var TpolicyGroup = new PolicyGroup
                {
                    Id = policyGroupId,
                    Name = name,
                    ParentId = string.IsNullOrEmpty(parentId) ? null : (long?)long.Parse(parentId)
                };
                var spResponsePolicyGroup = new PolicyGroupRepository().Insert(TpolicyGroup);
                if (spResponsePolicyGroup.Code == "100")
                {
                    policyGroupId = spResponsePolicyGroup.PkId;
                    step1Result.policyGroupId = policyGroupId;

                    result.Data = step1Result;
                    result.ResultCode = spResponsePolicyGroup.Code;
                    result.ResultMessage = spResponsePolicyGroup.Message;
                }
                else
                {
                    throw new Exception(spResponsePolicyGroup.Code + " : " + spResponsePolicyGroup.Message);
                }
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PolicyGroupSaveStep2(FormCollection form)
        {
            var result = new AjaxResultDto<ProductSaveStep4Result>();
            ProductSaveStep4Result step1Result = new ProductSaveStep4Result();
            var rules = form["hdRuleSpecial"];
            try
            {
                var policyGroupId = form["hdPolicyGroupId"];
                List<dynamic> ruleList = new List<dynamic>();
                ruleList = JsonConvert.DeserializeObject<List<dynamic>>(rules);
                foreach (var rule in ruleList)
                {
                    if (rule.isOpen == "1")
                    {
                        if (rule.STATUS == "0")
                        {
                            string idList = string.Empty;

                            if (rule.chkRuleSelectedId != null)
                            {
                                idList = rule.chkRuleSelectedId.ToString();

                                foreach (var ruleId in idList.Replace("\r", "").Replace("\n", "").Replace("[", string.Empty).Replace("]", "").Replace("\"", "").Replace(" ", "").Split(','))
                                {
                                    var TruleSubProduct = new RulePolicyGroup
                                    {
                                        Id = rule.RULE_POLICY_GROUP_ID,
                                        PolicyGroupId = long.Parse(policyGroupId),
                                        RuleId = long.Parse(ruleId),
                                        Status = Convert.ToString(rule.STATUS),
                                        Type = Convert.ToString(rule.RULE_TYPE)
                                    };
                                    var spResponse = new GenericRepository<RulePolicyGroup>().Insert(TruleSubProduct);
                                    if (spResponse.Code == "100")
                                    {
                                        result.ResultCode = spResponse.Code;
                                        result.ResultMessage = spResponse.Message;
                                    }
                                    else
                                    {
                                        throw new Exception(spResponse.Message);
                                    }
                                }
                            }
                            else
                            {
                                idList = rule.chkRuleGroupSelectedId.ToString();

                                foreach (var ruleGroupId in idList.Replace("\r", "").Replace("\n", "").Replace("[", string.Empty).Replace("]", "").Replace("\"", "").Replace(" ", "").Split(','))
                                {
                                    List<RuleGroupRule> ruleGroupRuleList = new GenericRepository<RuleGroupRule>().FindBy($"RULE_GROUP_ID={ruleGroupId}");
                                    if (ruleGroupRuleList.Any())
                                    {
                                        foreach (var ruleGroupRule in ruleGroupRuleList)
                                        {
                                            var TruleSubProduct = new RulePolicyGroup
                                            {
                                                Id = rule.RULE_POLICY_GROUP_ID,
                                                PolicyGroupId = long.Parse(policyGroupId),
                                                RuleId = ruleGroupRule.RuleId,
                                                RuleGroupId = ruleGroupRule.RuleGroupId,
                                                Status = Convert.ToString(rule.STATUS),
                                                Type = Convert.ToString(rule.RULE_TYPE)
                                            };
                                            var spResponse = new GenericRepository<RulePolicyGroup>().Insert(TruleSubProduct);
                                            if (spResponse.Code == "100")
                                            {
                                                result.ResultCode = spResponse.Code;
                                                result.ResultMessage = spResponse.Message;
                                            }
                                            else
                                            {
                                                throw new Exception(spResponse.Message);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            RulePolicyGroup ruleProduct = new GenericRepository<RulePolicyGroup>().FindById(long.Parse(Convert.ToString(rule.RULE_POLICY_GROUP_ID)));
                            if (ruleProduct != null)
                            {
                                ruleProduct.Status = ((int)Status.SILINDI).ToString();
                                var spResponse = new GenericRepository<RulePolicyGroup>().Update(ruleProduct);
                                if (spResponse.Code == "100")
                                {
                                    result.ResultCode = spResponse.Code;
                                    result.ResultMessage = spResponse.Message;
                                }
                                else
                                {
                                    throw new Exception(spResponse.Message);
                                }
                            }
                        }
                    }
                }
                var ruleSpecialList = new GenericRepository<V_RulePolicyGroup>().FindBy("POLICY_GROUP_ID=:id AND TYPE=:type", orderby: "", parameters: new { id = policyGroupId, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });
                if (ruleSpecialList != null)
                {
                    List<dynamic> rulesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in ruleSpecialList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.RULE_POLICY_GROUP_ID = Convert.ToString(item.RULE_POLICY_GROUP_ID);
                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                        listItem.RULE_NAME = item.RULE_NAME;
                        listItem.RESULT = item.RESULT;
                        listItem.INHERITED_NAME = string.Empty;
                        listItem.INHERITED_CODE = string.Empty;
                        listItem.isOpen = "0";

                        rulesList.Add(listItem);

                        i++;
                    }

                    result.Data = new ProductSaveStep4Result
                    {
                        jsonData = rulesList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PolicyGroupSaveStep3(FormCollection form)
        {
            var result = new AjaxResultDto<ProductSaveStep4Result>();
            ProductSaveStep4Result step1Result = new ProductSaveStep4Result();
            var rules = form["hdRuleExtra"];
            try
            {
                var policyGroupId = form["hdPolicyGroupId"];
                List<dynamic> ruleList = new List<dynamic>();
                ruleList = JsonConvert.DeserializeObject<List<dynamic>>(rules);
                foreach (var rule in ruleList)
                {
                    if (rule.isOpen == "1")
                    {
                        if (rule.STATUS == "0")
                        {
                            string idList = string.Empty;

                            if (rule.chkRuleSelectedId != null)
                            {
                                idList = rule.chkRuleSelectedId.ToString();

                                foreach (var ruleId in idList.Replace("\r", "").Replace("\n", "").Replace("[", string.Empty).Replace("]", "").Replace("\"", "").Replace(" ", "").Split(','))
                                {
                                    var TrulePackage = new RulePolicyGroup
                                    {
                                        Id = rule.RULE_POLICY_GROUP_ID,
                                        PolicyGroupId = long.Parse(policyGroupId),
                                        RuleId = long.Parse(ruleId),
                                        Status = Convert.ToString(rule.STATUS),
                                        Type = Convert.ToString(rule.RULE_TYPE)
                                    };
                                    var spResponseRuleProduct = new GenericRepository<RulePolicyGroup>().Insert(TrulePackage);
                                    if (spResponseRuleProduct.Code == "100")
                                    {
                                        result.ResultCode = spResponseRuleProduct.Code;
                                        result.ResultMessage = spResponseRuleProduct.Message;
                                    }
                                    else
                                    {
                                        throw new Exception(spResponseRuleProduct.Message);
                                    }
                                }
                            }
                            else
                            {
                                idList = rule.chkRuleGroupSelectedId.ToString();

                                foreach (var ruleGroupId in idList.Replace("\r", "").Replace("\n", "").Replace("[", string.Empty).Replace("]", "").Replace("\"", "").Replace(" ", "").Split(','))
                                {
                                    List<RuleGroupRule> ruleGroupRuleList = new GenericRepository<RuleGroupRule>().FindBy($"RULE_GROUP_ID={ruleGroupId}");
                                    if (ruleGroupRuleList.Any())
                                    {
                                        foreach (var ruleGroupRule in ruleGroupRuleList)
                                        {
                                            var TrulePackage = new RulePolicyGroup
                                            {
                                                Id = rule.RULE_POLICY_GROUP_ID,
                                                PolicyGroupId = long.Parse(policyGroupId),
                                                RuleId = ruleGroupRule.RuleId,
                                                RuleGroupId = ruleGroupRule.RuleGroupId,
                                                Status = Convert.ToString(rule.STATUS),
                                                Type = Convert.ToString(rule.RULE_TYPE)
                                            };
                                            var spResponse = new GenericRepository<RulePolicyGroup>().Insert(TrulePackage);
                                            if (spResponse.Code == "100")
                                            {
                                                result.ResultCode = spResponse.Code;
                                                result.ResultMessage = spResponse.Message;
                                            }
                                            else
                                            {
                                                throw new Exception(spResponse.Message);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            RulePolicyGroup ruleProduct = new GenericRepository<RulePolicyGroup>().FindById(long.Parse(Convert.ToString(rule.RULE_POLICY_GROUP_ID)));
                            if (ruleProduct != null)
                            {
                                ruleProduct.Status = ((int)Status.SILINDI).ToString();
                                var spResponse = new GenericRepository<RulePolicyGroup>().Update(ruleProduct);
                                if (spResponse.Code == "100")
                                {
                                    result.ResultCode = spResponse.Code;
                                    result.ResultMessage = spResponse.Message;
                                }
                                else
                                {
                                    throw new Exception(spResponse.Message);
                                }
                            }
                        }
                    }
                }
                var ruleExtraList = new GenericRepository<V_RulePolicyGroup>().FindBy("POLICY_GROUP_ID=:id AND TYPE=:type", orderby: "", parameters: new { id = policyGroupId, type = new DbString { Value = ((int)RuleType.EK_PROTOKOL).ToString(), Length = 3 } });
                if (ruleExtraList != null)
                {
                    List<dynamic> rulesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in ruleExtraList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.RULE_POLICY_GROUP_ID = Convert.ToString(item.RULE_POLICY_GROUP_ID);
                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                        listItem.RULE_NAME = item.RULE_NAME;
                        listItem.RESULT = item.RESULT;
                        listItem.INHERITED_NAME = string.Empty;
                        listItem.INHERITED_CODE = string.Empty;
                        listItem.isOpen = "0";

                        rulesList.Add(listItem);

                        i++;
                    }

                    result.Data = new ProductSaveStep4Result
                    {
                        jsonData = rulesList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Policy
        [LoginControl]
        public ActionResult Policy()
        {
            ImportVM vm = new ImportVM();
            try
            {
                vm.ImportObjectType = ImportObjectType.Policy;
                vm.ShowCompanies = true;

                List<Company> lstCompany = new GenericRepository<Company>().FindBy();
                if (lstCompany != null)
                {
                    if (lstCompany.Count > 0)
                    {
                        foreach (var companyItem in lstCompany)
                        {
                            vm.Companies.Add(new CompanyObjectForImport { CompanyId = companyItem.Id.ToString(), CompanyName = companyItem.Name });
                        }
                    }
                }

                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                var companyList = new CompanyRepository().FindBy();
                ViewBag.CompanyList = companyList;

                var policyGroupList = new PolicyGroupRepository().FindBy();
                ViewBag.PolicyGroupList = policyGroupList;

                var policyTypeList = LookupHelper.GetLookupData(LookupTypes.Policy, showAll: true);
                ViewBag.PolicyTypeList = policyTypeList;

                var PolicyStatusList = LookupHelper.GetLookupData(Constants.LookupTypes.PolicyStatus, showChoose: true);
                ViewBag.PolicyStatusList = PolicyStatusList;

                var EndorsementCategoryList = LookupHelper.GetLookupData(LookupTypes.EndorsementCategory, showChoose: true);
                ViewBag.EndorsementCategoryList = EndorsementCategoryList;

                var EndorsementTypeDescriptionTypeList = LookupHelper.GetLookupData(LookupTypes.EndorsementTypeDescription, showChoose: true);
                ViewBag.EndorsementTypeDescriptionTypeList = EndorsementTypeDescriptionTypeList;

                var EndorsementTypeList = LookupHelper.GetLookupData(LookupTypes.Endorsement, showChoose: true);
                ViewBag.EndorsementTypeList = EndorsementTypeList;

                var ClaimOpenReasonList = new GenericRepository<Reason>().FindBy(conditions: "STATUS_NAME = 'ClaimReasonType' AND STATUS_ORDINAL = 0");
                ViewBag.ClaimOpenReasonList = ClaimOpenReasonList;

                var ClaimCloseReasonList = new GenericRepository<Reason>().FindBy(conditions: "STATUS_NAME = 'ClaimReasonType' AND STATUS_ORDINAL = 1");
                ViewBag.ClaimCloseReasonList = ClaimCloseReasonList;

                ViewBag.ClaimReasonTypeList = LookupHelper.GetLookupData(LookupTypes.ClaimReason);

                ViewBag.ProductTypeList = LookupHelper.GetLookupData(LookupTypes.Product, showAll: true);
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View(vm);
        }

        #region Table List / Filter
        [HttpPost]
        [LoginControl]
        public ActionResult GetPolicyList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];
            dynamic policyListData = null;
            int policyListTotalItemsCount = 0;
            string message = "1";
            List<PolicyList> PolicyList = new List<PolicyList>();

            if (isFilter == "1")
            {
                if (!String.IsNullOrEmpty(form["insuredFirstName"]) || !String.IsNullOrEmpty(form["insuredLastName"]) || !String.IsNullOrEmpty(form["insuredTCKN"]))
                {
                    if (!String.IsNullOrEmpty(form["policyCompany"]) || !String.IsNullOrEmpty(form["product"]) || !String.IsNullOrEmpty(form["insurerTCKN"]) || !String.IsNullOrEmpty(form["insurerName"]) || !String.IsNullOrEmpty(form["policyType"]) || !String.IsNullOrEmpty(form["productType"]) || !String.IsNullOrEmpty(form["policyGroup"]) || !String.IsNullOrEmpty(form["policyNo"]) || !String.IsNullOrEmpty(form["policyStartDate"]) || !String.IsNullOrEmpty(form["policyRenewalNo"]) || !String.IsNullOrEmpty(form["policyStatus"]))
                    {
                        throw new Exception("Hem sigortalı hem poliçe bilgileriyle filtreleyerek sorgulama yapamazsınız!");
                    }

                    String whereConditition = form["insuredTCKN"].IsInt64() ? $" (INSURED_IDENTITY_NO = {form["insuredTCKN"]} OR INSURED_TAX_NUMBER = {form["insuredTCKN"]} OR INSURED_PASSPORT_NO = '{form["insuredTCKN"]}') " : "";
                    whereConditition += !String.IsNullOrEmpty(form["insuredFirstName"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" INSURED_FIRST_NAME LIKE '%{form["insuredFirstName"]}%' " : "";
                    whereConditition += !String.IsNullOrEmpty(form["insuredLastName"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" INSURED_LAST_NAME LIKE '%{form["insuredLastName"]}%' " : "";

                    var policyList = new GenericRepository<V_PolicyInsured>().FindByPaged(conditions: whereConditition,
                                                              orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},POLICY_ID DESC" : "POLICY_ID DESC",
                                                              pageNumber: start,
                                                              rowsPerPage: length);
                    policyListData = policyList.Data;
                    policyListTotalItemsCount = policyList.TotalItemsCount;
                }
                else
                {
                    String whereConditition = !String.IsNullOrEmpty(form["policyCompany"]) ? $" COMPANY_ID = {long.Parse(form["policyCompany"])} " : "";
                    whereConditition += !String.IsNullOrEmpty(form["product"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" PRODUCT_ID = {long.Parse(form["product"])} " : "";
                    whereConditition += form["insurerTCKN"].IsInt64() ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" (INSURER_IDENTITY_NO = {form["insurerTCKN"]} OR INSURER_TAX_NUMBER = {form["insurerTCKN"]}) " : "";
                    whereConditition += !String.IsNullOrEmpty(form["insurerName"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" INSURER_NAME LIKE '%{form["insurerName"]}%' " : "";
                    whereConditition += !String.IsNullOrEmpty(form["policyType"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_TYPE = '{(form["policyType"])}' " : "";
                    whereConditition += !String.IsNullOrEmpty(form["productType"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" PRODUCT_TYPE='{form["productType"]}' " : "";
                    whereConditition += !String.IsNullOrEmpty(form["policyGroup"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_GROUP_ID = {(form["policyGroup"])} " : "";
                    whereConditition += form["policyNo"].IsInt64() ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_NUMBER = {form["policyNo"]} " : "";
                    whereConditition += !String.IsNullOrEmpty(form["policyStartDate"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_START_DATE = TO_DATE('{ DateTime.Parse(form["policyStartDate"])}','DD.MM.YYYY HH24:MI:SS') " : "";
                    whereConditition += !String.IsNullOrEmpty(form["policyEndDate"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_END_DATE = TO_DATE('{ DateTime.Parse(form["policyEndDate"])}','DD.MM.YYYY HH24:MI:SS') " : "";
                    whereConditition += !String.IsNullOrEmpty(form["policyRenewalNo"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" RENEWAL_NO={long.Parse(form["policyRenewalNo"])} " : "";
                    whereConditition += !String.IsNullOrEmpty(form["policyStatus"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" STATUS='{form["policyStatus"]}' " : "";

                    if (whereConditition != "")
                    {
                        var policyList = new GenericRepository<V_Policy>().FindByPaged(conditions: whereConditition,
                                                             orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},POLICY_ID DESC" : "POLICY_ID DESC",
                                                             pageNumber: start,
                                                             rowsPerPage: length);
                        policyListData = policyList.Data;
                        policyListTotalItemsCount = policyList.TotalItemsCount;
                    }
                    else
                    {
                        message = "Lütfen Filtre alanlarını doldurup filtrelemeyi deneyiniz.";
                    }


                }


                if (policyListData != null)
                {
                    foreach (var item in policyListData)
                    {
                        PolicyList listItem = new PolicyList();

                        listItem.POLICY_ID = Convert.ToString(item.POLICY_ID);
                        listItem.POLICY_TYPE_TEXT = LookupHelper.GetLookupCodeByOrdinal(LookupTypes.Policy, item.POLICY_TYPE);
                        listItem.POLICY_NUMBER_RENEWAL_NO = Convert.ToString(item.POLICY_NUMBER) + " - " + Convert.ToString(item.RENEWAL_NO);
                        listItem.POLICY_START_DATE = Convert.ToString(item.POLICY_START_DATE);
                        listItem.POLICY_START_DATE = listItem.POLICY_START_DATE.IsDateTime() ? DateTime.Parse(listItem.POLICY_START_DATE).ToString("dd-MM-yyyy") : string.Empty;
                        listItem.INSURER_NAME = Convert.ToString(item.INSURER_NAME);
                        listItem.COMPANY_NAME = Convert.ToString(item.COMPANY_NAME);
                        listItem.PRODUCT_NAME = Convert.ToString(item.PRODUCT_NAME);
                        listItem.PREMIUM = Convert.ToString(item.PREMIUM);
                        listItem.POLICY_END_DATE = Convert.ToString(item.POLICY_END_DATE);
                        listItem.POLICY_END_DATE = listItem.POLICY_END_DATE.IsDateTime() ? DateTime.Parse(listItem.POLICY_END_DATE).ToString("dd-MM-yyyy") : string.Empty;

                        listItem.IS_OPEN_TO_CLAIM = Convert.ToString(item.IS_OPEN_TO_CLAIM);

                        var endorsmentList = new GenericRepository<Endorsement>().FindBy($"POLICY_ID={listItem.POLICY_ID} AND STATUS={((int)(Status.PASIF))}", fetchDeletedRows: true);
                        if (endorsmentList != null && endorsmentList.Count > 0)
                        {
                            listItem.IS_NOT_ACTIVE_ENDORSEMENT = "0";
                        }
                        else
                        {
                            listItem.IS_NOT_ACTIVE_ENDORSEMENT = "1";
                        }

                        listItem.STATUS = Convert.ToString(item.STATUS);

                        PolicyList.Add(listItem);
                    }
                }
            }
            return Json(new { data = PolicyList, draw = Request["draw"], recordsTotal = policyListTotalItemsCount, recordsFiltered = policyListTotalItemsCount , message=message}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetParentPolicyList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            String whereConditition = !String.IsNullOrEmpty(form["policyCompany"]) ? $" COMPANY_ID = {long.Parse(form["policyCompany"])} " : "";
            whereConditition += form["insurerTCKN"].IsInt64() ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" (INSURER_IDENTITY_NO = {form["insurerTCKN"]} OR INSURER_TAX_NUMBER = {form["insurerTCKN"]}) " : "";
            whereConditition += !String.IsNullOrEmpty(form["insurerName"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" INSURER_NAME LIKE '%{form["insurerName"]}%' " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyType"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_TYPE = '{(form["policyType"])}' " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyGroup"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_GROUP_ID = {(form["policyGroup"])} " : "";
            whereConditition += form["policyNo"].IsInt64() ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_NUMBER = {form["policyNo"]} " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyStartDate"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_START_DATE = TO_DATE('{ DateTime.Parse(form["policyStartDate"])}','DD.MM.YYYY HH24:MI:SS') " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyRenewalNo"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" RENEWAL_NO={long.Parse(form["policyRenewalNo"])} " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyStatus"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" STATUS={form["policyStatus"]} " : "";

            var policyList = new GenericRepository<V_Policy>().FindByPaged(conditions: whereConditition,
                                                      orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},POLICY_ID ASC" : "",
                                                      pageNumber: start,
                                                      rowsPerPage: length);
            List<PolicyList> PolicyList = new List<PolicyList>();

            if (policyList.Data != null)
            {
                foreach (var item in policyList.Data)
                {
                    PolicyList listItem = new PolicyList();

                    listItem.POLICY_ID = Convert.ToString(item.POLICY_ID);
                    listItem.POLICY_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Policy, item.POLICY_TYPE);
                    listItem.POLICY_NUMBER = item.POLICY_NUMBER;
                    listItem.POLICY_START_DATE = Convert.ToString(item.POLICY_START_DATE);
                    listItem.POLICY_START_DATE = listItem.POLICY_START_DATE.IsDateTime() ? DateTime.Parse(listItem.POLICY_START_DATE).ToString("dd-MM-yyyy") : string.Empty;
                    listItem.INSURER_NAME = Convert.ToString(item.INSURER_NAME);
                    listItem.COMPANY_NAME = Convert.ToString(item.COMPANY_NAME);
                    listItem.RENEWAL_NO = Convert.ToString(item.RENEWAL_NO);

                    listItem.IS_OPEN_TO_CLAIM = Convert.ToString(item.IS_OPEN_TO_CLAIM);

                    listItem.STATUS = Convert.ToString(item.STATUS);

                    PolicyList.Add(listItem);
                }
            }

            return Json(new { data = PolicyList, draw = Request["draw"], recordsTotal = policyList.TotalItemsCount, recordsFiltered = policyList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetPreviousPolicyList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            String whereConditition = !String.IsNullOrEmpty(form["policyCompany"]) ? $" COMPANY_ID = {long.Parse(form["policyCompany"])} " : "";
            whereConditition += form["insurerTCKN"].IsInt64() ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" (INSURER_IDENTITY_NO = {form["insurerTCKN"]} OR INSURER_TAX_NUMBER = {form["insurerTCKN"]}) " : "";
            whereConditition += !String.IsNullOrEmpty(form["insurerName"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" INSURER_NAME LIKE '%{form["insurerName"]}%' " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyType"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_TYPE = '{(form["policyType"])}' " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyGroup"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_GROUP_ID = {(form["policyGroup"])} " : "";
            whereConditition += form["policyNo"].IsInt64() ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_NUMBER = {form["policyNo"]} " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyStartDate"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" POLICY_START_DATE = TO_DATE('{ DateTime.Parse(form["policyStartDate"])}','DD.MM.YYYY HH24:MI:SS') " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyRenewalNo"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" RENEWAL_NO={long.Parse(form["policyRenewalNo"])} " : "";
            whereConditition += !String.IsNullOrEmpty(form["policyStatus"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" STATUS={form["policyStatus"]} " : "";

            var policyList = new GenericRepository<V_Policy>().FindByPaged(conditions: whereConditition,
                                                      orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},POLICY_ID ASC" : "",
                                                      pageNumber: start,
                                                      rowsPerPage: length);
            List<PolicyList> PolicyList = new List<PolicyList>();

            if (policyList.Data != null)
            {
                foreach (var item in policyList.Data)
                {
                    PolicyList listItem = new PolicyList();

                    listItem.POLICY_ID = Convert.ToString(item.POLICY_ID);
                    listItem.POLICY_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Policy, item.POLICY_TYPE);
                    listItem.POLICY_NUMBER = item.POLICY_NUMBER;
                    listItem.POLICY_START_DATE = Convert.ToString(item.POLICY_START_DATE);
                    listItem.POLICY_START_DATE = listItem.POLICY_START_DATE.IsDateTime() ? DateTime.Parse(listItem.POLICY_START_DATE).ToString("dd-MM-yyyy") : string.Empty;
                    listItem.INSURER_NAME = Convert.ToString(item.INSURER_NAME);
                    listItem.COMPANY_NAME = Convert.ToString(item.COMPANY_NAME);
                    listItem.RENEWAL_NO = Convert.ToString(item.RENEWAL_NO);

                    listItem.IS_OPEN_TO_CLAIM = Convert.ToString(item.IS_OPEN_TO_CLAIM);

                    listItem.STATUS = Convert.ToString(item.STATUS);

                    PolicyList.Add(listItem);
                }
            }

            return Json(new { data = PolicyList, draw = Request["draw"], recordsTotal = policyList.TotalItemsCount, recordsFiltered = policyList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetPortfoyList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isOpen = form["isOpen"];

            var insurerList = new ViewResultDto<List<V_Portfoy>>();
            List<InsurerList> InsurerList = new List<InsurerList>();

            if (isOpen.IsNull() || isOpen == "1")
            {
                String whereConditition = !String.IsNullOrEmpty(form["CONTACT_TITLE"]) ? $" CONTACT_TITLE  LIKE '%{form["CONTACT_TITLE"]}%' " : "";
                whereConditition += form["IDENTITYorTAX"].IsInt64() ? (string.IsNullOrEmpty(whereConditition) ? "" : "AND") + $" (IDENTITY_NO = {form["IDENTITYorTAX"]} OR TAX_NUMBER = {form["IDENTITYorTAX"]} ) " : "";

                insurerList = new GenericRepository<V_Portfoy>().FindByPaged(conditions: string.IsNullOrEmpty(whereConditition) ? string.Empty : whereConditition,
                                                     orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},CONTACT_ID ASC" : "",
                                                     pageNumber: start,
                                                     rowsPerPage: length);

                if (insurerList.Data != null)
                {
                    foreach (var item in insurerList.Data)
                    {
                        InsurerList listItem = new InsurerList();

                        listItem.CONTACT_ID = Convert.ToString(item.CONTACT_ID);

                        if (item.CONTACT_TYPE == ((int)ContactType.TUZEL).ToString())
                            listItem.CONTACT_TITLE = Convert.ToString(item.CONTACT_TITLE);
                        else
                            listItem.CONTACT_TITLE = Convert.ToString(item.FIRST_NAME) + " " + Convert.ToString(item.LAST_NAME);

                        listItem.CONTACT_TYPE = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Contact, item.CONTACT_TYPE);
                        listItem.IDENTITY_NO = item.IDENTITY_NO;
                        listItem.TAX_NUMBER = item.TAX_NUMBER;

                        InsurerList.Add(listItem);
                    }
                }
            }
            return Json(new { data = InsurerList, draw = Request["draw"], recordsTotal = insurerList.TotalItemsCount, recordsFiltered = insurerList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetInsuredList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            List<InsuredList> InsuredList = new List<InsuredList>();
            ViewResultDto<List<V_Insured>> insuredList = new ViewResultDto<List<V_Insured>>();
            var policyId = form["POLICY_ID"];
            if (Extensions.IsInt64(policyId) && long.Parse(policyId) > 0)
            {
                dynamic parameters = new ExpandoObject();
                parameters.policyId = policyId;
                parameters.searchValue = searchValue;
                parameters.status = ((int)Status.AKTIF).ToString();

                var whereConditions = "POLICY_ID =:policyId AND STATUS=:status";
                whereConditions += searchValue.IsNull() || searchValue.IsInt64() ? string.Empty : $" AND (FIRST_NAME LIKE '%{searchValue.ToUpper()}%' OR LAST_NAME LIKE '%{searchValue.ToUpper()}%') ";
                whereConditions += !searchValue.IsInt64() ? string.Empty : $" AND (IDENTITY_NO = :searchValue OR FAMILY_NO = :searchValue) ";

                insuredList = new GenericRepository<V_Insured>().FindByPaged(conditions: whereConditions,
                                                                            orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},INSURED_ID ASC" : "INSURED_ID",
                                                                            pageNumber: start,
                                                                            rowsPerPage: length,
                                                                            parameters: parameters);

                if (insuredList.Data != null && insuredList.TotalItemsCount > 0)
                {
                    var insuredEndorsement = new GenericRepository<V_InsuredEndorsement>().FindBy("INSURED_ID IN :insuredIdLst", orderby: "INSURED_ID",
                                                                                                  parameters: new { insuredIdLst = insuredList.Data.Select(i => i.INSURED_ID).ToArray() });

                    foreach (var item in insuredList.Data)
                    {
                        InsuredList listItem = new InsuredList();

                        listItem.INSURED_ID = Convert.ToString(item.INSURED_ID);
                        listItem.INSURER_NAME = item.INSURER_NAME;
                        listItem.FIRST_NAME = item.FIRST_NAME;
                        listItem.LAST_NAME = item.LAST_NAME;
                        listItem.IDENTITY_NO = item.IDENTITY_NO;
                        listItem.INDIVIDUAL_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Individual, item.INDIVIDUAL_TYPE);
                        listItem.FAMILY_NO = Convert.ToString(item.FAMILY_NO);
                        listItem.PREMIUM = Convert.ToString(item.TOTAL_PREMIUM);
                        listItem.IS_OPEN_TO_CLAIM = item.IS_OPEN_TO_CLAIM;

                        listItem.BIRTHDATE = Convert.ToString(item.BIRTHDATE);
                        listItem.BIRTHDATE = listItem.BIRTHDATE.IsDateTime() ? DateTime.Parse(listItem.BIRTHDATE).ToString("dd-MM-yyyy") : string.Empty;

                        if (insuredEndorsement != null && insuredEndorsement.Count > 0)
                        {
                            listItem.ENDORSEMENT_ID_LIST = string.Join(", ", insuredEndorsement.Where(i => i.INSURED_ID == item.INSURED_ID).Select(ie => ie.ENDORSEMENT_NO).ToList());
                        }

                        InsuredList.Add(listItem);
                    }
                }
            }
            return Json(new { data = InsuredList, draw = Request["draw"], recordsTotal = insuredList.TotalItemsCount, recordsFiltered = insuredList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetPersonList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isOpen = form["isOpen"];

            List<InsuredList> PersonList = new List<InsuredList>();
            var personList = new ViewResultDto<List<V_Portfoy>>();

            if (isOpen.IsNull() || isOpen == "1")
            {
                var whereConditions = $"CONTACT_TYPE = {((int)ContactType.GERCEK).ToString()} AND BIRTHDATE IS NOT NULL AND GENDER IS NOT NULL ";
                whereConditions += form["NAME"].IsNull() ? string.Empty : $" AND FIRST_NAME LIKE '%{form["NAME"]}%' ";
                whereConditions += form["LASTNAME"].IsNull() ? string.Empty : $" AND LAST_NAME LIKE '%{form["LASTNAME"]}%' ";
                whereConditions += !form["IDENTITY_NO"].IsInt64() ? string.Empty : $" AND IDENTITY_NO = {form["IDENTITY_NO"]} ";

                personList = new GenericRepository<V_Portfoy>().FindByPaged(conditions: whereConditions,
                                                     orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},CONTACT_ID ASC" : "",
                                                     pageNumber: start,
                                                     rowsPerPage: length);


                if (personList.Data != null)
                {
                    foreach (var item in personList.Data)
                    {
                        InsuredList listItem = new InsuredList();

                        listItem.CONTACT_ID = Convert.ToString(item.CONTACT_ID);
                        listItem.FIRST_NAME = Convert.ToString(item.FIRST_NAME);
                        listItem.LAST_NAME = Convert.ToString(item.LAST_NAME);
                        listItem.IDENTITY_NO = item.IDENTITY_NO;
                        listItem.GENDER = Convert.ToString(item.GENDER);

                        string birtdate = Convert.ToString(item.BIRTHDATE);
                        if (birtdate.IsDateTime())
                        {
                            int age = DateTime.Now.Year - DateTime.Parse(birtdate).Year;
                            listItem.AGE = Convert.ToString(age);
                        }
                        PersonList.Add(listItem);
                    }
                }
            }
            return Json(new { data = PersonList, draw = Request["draw"], recordsTotal = personList.TotalItemsCount, recordsFiltered = personList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetFamilyList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            List<InsuredList> InsuredList = new List<InsuredList>();
            ViewResultDto<List<V_Insured>> insuredList = new ViewResultDto<List<V_Insured>>();
            var policyId = form["POLICY_ID"];
            if (policyId.IsInt64() && long.Parse(policyId) > 0)
            {
                var policy = new GenericRepository<Policy>().FindById(long.Parse(policyId));

                if (policy != null)
                {
                    policyId = policy.ParentId != null ? policy.ParentId.ToString() : policy.Id.ToString();

                    insuredList = new GenericRepository<V_Insured>().FindByPaged(conditions: "POLICY_ID =:policyId AND INDIVIDUAL_TYPE =:INDIVIDUAL_TYPE",
                                                         parameters: new { policyId, INDIVIDUAL_TYPE = new DbString { Value = ((int)IndividualType.FERT).ToString(), Length = 3 } },
                                                         orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},INSURED_ID ASC" : "",
                                                         pageNumber: start,
                                                         rowsPerPage: length);

                    if (insuredList.Data != null)
                    {
                        foreach (var item in insuredList.Data)
                        {
                            InsuredList listItem = new InsuredList();

                            listItem.INSURED_ID = Convert.ToString(item.INSURED_ID);
                            listItem.FIRST_NAME = Convert.ToString(item.FIRST_NAME);
                            listItem.LAST_NAME = Convert.ToString(item.LAST_NAME);
                            listItem.IDENTITY_NO = item.IDENTITY_NO;
                            listItem.INDIVIDUAL_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Individual, item.INDIVIDUAL_TYPE);
                            listItem.FAMILY_NO = Convert.ToString(item.FAMILY_NO);

                            InsuredList.Add(listItem);
                        }
                    }
                }
            }
            return Json(new { data = InsuredList, draw = Request["draw"], recordsTotal = insuredList.TotalItemsCount, recordsFiltered = insuredList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetEndorsementInsuredList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            List<InsuredList> InsuredList = new List<InsuredList>();
            //ViewResultDto<List<V_InsuredEndorsement>> insuredList = new ViewResultDto<List<V_InsuredEndorsement>>();
            var endorsementId = form["ENDORSEMENT_ID"];
            var policyId = form["POLICY_ID"];
            if (Extensions.IsInt64(endorsementId) && long.Parse(endorsementId) > 0)
            {
                dynamic parameters = new ExpandoObject();
                parameters.endorsementId = endorsementId;
                parameters.searchValue = searchValue;

                var whereConditions = "ENDORSEMENT_ID =:endorsementId";
                whereConditions += searchValue.IsNull() || searchValue.IsInt64() ? string.Empty : $" AND (FIRST_NAME LIKE '%:searchValue%' OR LAST_NAME LIKE '%:searchValue%') ";
                whereConditions += !searchValue.IsInt64() ? string.Empty : $" AND (IDENTITY_NO = :searchValue OR FAMILY_NO = :searchValue) ";

                ViewResultDto<List<V_InsuredEndorsement>> insuredEndList = new GenericRepository<V_InsuredEndorsement>().FindByPaged(conditions: whereConditions,
                                                                                orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},INSURED_ID ASC" : "INSURED_ID ASC",
                                                                                pageNumber: start,
                                                                                rowsPerPage: length,
                                                                                parameters: parameters,
                                                                                fetchDeletedRows: true,
                                                                                fetchHistoricRows: true);
                if (insuredEndList.Data != null && insuredEndList.TotalItemsCount > 0)
                {
                    var insuredEndorsement = new GenericRepository<V_InsuredEndorsement>().FindBy("INSURED_ID IN :insuredIdLst", orderby: "INSURED_ID",
                                                                                                      parameters: new { insuredIdLst = insuredEndList.Data.Select(i => i.INSURED_ID).ToArray() });

                    foreach (var item in insuredEndList.Data)
                    {
                        InsuredList listItem = new InsuredList();

                        listItem.INSURED_ID = Convert.ToString(item.INSURED_ID);
                        listItem.FIRST_NAME = Convert.ToString(item.FIRST_NAME);
                        listItem.LAST_NAME = Convert.ToString(item.LAST_NAME);
                        listItem.IDENTITY_NO = item.IDENTITY_NO;
                        listItem.INDIVIDUAL_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Individual, item.INDIVIDUAL_TYPE);
                        listItem.FAMILY_NO = Convert.ToString(item.FAMILY_NO);
                        if (item.ENDORSEMENT_TYPE != ((int)EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI).ToString() && item.ENDORSEMENT_TYPE != ((int)EndorsementType.SE_DEGISIKLIGI).ToString() && item.ENDORSEMENT_TYPE != ((int)EndorsementType.BASLANGIC_BITIS_TARIHI).ToString() && item.ENDORSEMENT_TYPE != ((int)EndorsementType.URETIM_KAYNAGI_DEGISIKLIGI).ToString())
                        {
                            listItem.PREMIUM = Convert.ToString(item.TOTAL_PREMIUM);

                            if (Convert.ToString(item.ENDORSEMENT_TYPE) == ((int)EndorsementType.SIGORTALI_CIKISI).ToString())
                            {
                                listItem.PREMIUM = Convert.ToString(item.PREMIUM);
                                listItem.EXIT_PREMIUM = Convert.ToString(item.TOTAL_PREMIUM);
                                listItem.USE_PREMIUM = Convert.ToString(item.PREMIUM - item.TOTAL_PREMIUM);
                            }
                            else
                            {
                                listItem.EXIT_PREMIUM = "-";
                                listItem.USE_PREMIUM = "-";
                            }
                        }
                        else
                        {
                            listItem.PREMIUM = "0";
                            listItem.EXIT_PREMIUM = "-";
                            listItem.USE_PREMIUM = "-";
                        }

                        listItem.BIRTHDATE = Convert.ToString(item.BIRTHDATE);
                        listItem.BIRTHDATE = listItem.BIRTHDATE.IsDateTime() ? DateTime.Parse(listItem.BIRTHDATE).ToString("dd-MM-yyyy") : string.Empty;

                        if (insuredEndorsement != null && insuredEndorsement.Count > 0)
                        {
                            listItem.ENDORSEMENT_ID_LIST = string.Join(", ", insuredEndorsement.Where(i => i.INSURED_ID == item.INSURED_ID).Select(ie => ie.ENDORSEMENT_NO).ToList());
                        }

                        InsuredList.Add(listItem);
                    }
                }
                if (insuredEndList.TotalItemsCount <= 0 && policyId.IsInt64())
                {
                    parameters = new ExpandoObject();
                    parameters.policyId = policyId;
                    parameters.searchValue = searchValue;

                    whereConditions = "POLICY_ID =:policyId ";
                    whereConditions += searchValue.IsNull() || searchValue.IsInt64() ? string.Empty : $" AND (FIRST_NAME LIKE '%:searchValue%' OR LAST_NAME LIKE '%:searchValue%') ";
                    whereConditions += !searchValue.IsInt64() ? string.Empty : $" AND (IDENTITY_NO = :searchValue OR FAMILY_NO = :searchValue) ";

                    ViewResultDto<List<V_Insured>> insuredList = new GenericRepository<V_Insured>().FindByPaged(conditions: whereConditions,
                                                                                orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},INSURED_ID ASC" : "INSURED_ID",
                                                                                pageNumber: start,
                                                                                rowsPerPage: length,
                                                                                parameters: parameters);

                    if (insuredList.Data != null && insuredList.TotalItemsCount > 0)
                    {
                        var insuredEndorsement = new GenericRepository<V_InsuredEndorsement>().FindBy("INSURED_ID IN :insuredIdLst", orderby: "INSURED_ID",
                                                                                                      parameters: new { insuredIdLst = insuredList.Data.Select(il => il.INSURED_ID).ToArray() });

                        foreach (var item in insuredList.Data)
                        {
                            InsuredList listItem = new InsuredList();

                            listItem.INSURED_ID = Convert.ToString(item.INSURED_ID);
                            listItem.INSURER_NAME = item.INSURER_NAME;
                            listItem.FIRST_NAME = item.FIRST_NAME;
                            listItem.LAST_NAME = item.LAST_NAME;
                            listItem.IDENTITY_NO = item.IDENTITY_NO;
                            listItem.INDIVIDUAL_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Individual, item.INDIVIDUAL_TYPE);
                            listItem.FAMILY_NO = Convert.ToString(item.FAMILY_NO);
                            listItem.PREMIUM = "-";
                            listItem.IS_OPEN_TO_CLAIM = item.IS_OPEN_TO_CLAIM;

                            listItem.BIRTHDATE = Convert.ToString(item.BIRTHDATE);
                            listItem.BIRTHDATE = listItem.BIRTHDATE.IsDateTime() ? DateTime.Parse(listItem.BIRTHDATE).ToString("dd-MM-yyyy") : string.Empty;

                            if (insuredEndorsement != null && insuredEndorsement.Count > 0)
                            {
                                listItem.ENDORSEMENT_ID_LIST = string.Join(", ", insuredEndorsement.Where(i => i.INSURED_ID == item.INSURED_ID).Select(ie => ie.ENDORSEMENT_NO).ToList());
                            }

                            InsuredList.Add(listItem);
                        }
                    }
                }
            }
            return Json(new { data = InsuredList, draw = Request["draw"], recordsTotal = InsuredList.Count, recordsFiltered = InsuredList.Count }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetEndorsementList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            List<EndorsementList> EndorsementList = new List<EndorsementList>();
            ViewResultDto<List<V_PolicyEndorsement>> endorsementList = new ViewResultDto<List<V_PolicyEndorsement>>();
            var policyId = form["POLICY_ID"];
            if (Extensions.IsInt64(policyId) && long.Parse(policyId) > 0)
            {
                String whereConditition = $" POLICY_ID = {policyId}";
                whereConditition += !String.IsNullOrEmpty(form["COMPANY_ID"]) ? $" AND COMPANY_ID = {long.Parse(form["COMPANY_ID"])} " : "";
                whereConditition += !String.IsNullOrEmpty(form["INSURER_TCKN"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" (INSURER_IDENTITY_NO = {form["INSURER_TCKN"]} OR INSURER_TAX_NUMBER = {form["INSURER_TCKN"]}) " : "";
                whereConditition += !String.IsNullOrEmpty(form["INSURER_NAME"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" INSURER_NAME LIKE '%{form["INSURER_NAME"]}%' " : "";
                whereConditition += !String.IsNullOrEmpty(form["ENDORSEMENT_TYPE"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" ENDORSEMENT_TYPE = '{(form["ENDORSEMENT_TYPE"])}' " : "";
                whereConditition += !String.IsNullOrEmpty(form["ENDORSEMENT_NO"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" ENDORSEMENT_NO = {long.Parse(form["ENDORSEMENT_NO"])} " : "";

                endorsementList = new GenericRepository<V_PolicyEndorsement>().FindByPaged(conditions: whereConditition,
                                                     orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},ENDORSEMENT_ID ASC" : "ENDORSEMENT_ID",
                                                     pageNumber: start,
                                                     rowsPerPage: length);

                if (endorsementList.Data != null && endorsementList.Data.Count > 0)
                {
                    var endorsement = endorsementList.Data.OrderByDescending(p => p.ENDORSEMENT_ID).ToList()[0]; // new GenericRepository<Endorsement>().FindBy($" POLICY_ID = {policyId}", orderby: "ID DESC").FirstOrDefault();
                    foreach (var item in endorsementList.Data)
                    {
                        var endEndorsementId = endorsement.ENDORSEMENT_ID.ToString();
                        EndorsementList listItem = new EndorsementList();

                        listItem.ENDORSEMENT_ID = Convert.ToString(item.ENDORSEMENT_ID);
                        listItem.POLICY_ID = Convert.ToString(item.POLICY_ID);
                        listItem.ENDORSEMENT_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Endorsement, item.ENDORSEMENT_TYPE);
                        listItem.ENDORSEMENT_TYPE = item.ENDORSEMENT_TYPE;
                        listItem.ENDORSEMENT_NO = Convert.ToString(item.ENDORSEMENT_NO);
                        listItem.COMPANY_NAME = Convert.ToString(item.COMPANY_NAME);
                        listItem.INSURER_NAME = Convert.ToString(item.INSURER_NAME);
                        listItem.ENDORSEMENT_PREMIUM = Convert.ToString(item.ENDORSEMENT_PREMIUM);

                        listItem.ENDORSEMENT_START_DATE = Convert.ToString(item.ENDORSEMENT_START_DATE);
                        listItem.ENDORSEMENT_START_DATE = listItem.ENDORSEMENT_START_DATE.IsDateTime() ? DateTime.Parse(listItem.ENDORSEMENT_START_DATE).ToString("dd-MM-yyyy") : string.Empty;

                        listItem.ENDORSEMENT_DATE_OF_ISSUE = Convert.ToString(item.ENDORSEMENT_ISSUE_DATE);
                        listItem.ENDORSEMENT_DATE_OF_ISSUE = listItem.ENDORSEMENT_DATE_OF_ISSUE.IsDateTime() ? DateTime.Parse(listItem.ENDORSEMENT_DATE_OF_ISSUE).ToString("dd-MM-yyyy") : string.Empty;

                        listItem.STATUS = Convert.ToString(item.STATUS);
                        listItem.IS_BACK_STATUS = "0";

                        if (listItem.STATUS == ((int)Status.AKTIF).ToString())
                        {
                            if (item.SBM_STATUS == null || Convert.ToString(item.SBM_STATUS) == "0")
                            {
                                if (listItem.ENDORSEMENT_ID == endEndorsementId)
                                {
                                    List<EndorsementInsured> endorsementInsured = new GenericRepository<EndorsementInsured>().FindBy($"ENDORSEMENT_ID={listItem.ENDORSEMENT_ID}");
                                    if (endorsementInsured != null && endorsementInsured.Count > 0)
                                    {
                                        string insuredIdList = string.Join(",", endorsementInsured.Select(e => e.InsuredId));
                                        Common.Entities.ProteinEntities.Claim claim = new GenericRepository<Common.Entities.ProteinEntities.Claim>().FindBy($"POLICY_ID={listItem.POLICY_ID} AND INSURED_ID IN ({insuredIdList}) AND CLAIM_DATE >= TO_DATE('{ DateTime.Parse(listItem.ENDORSEMENT_DATE_OF_ISSUE)}','DD.MM.YYYY HH24:MI:SS')", orderby: "ID").FirstOrDefault();
                                        if (claim == null)
                                        {
                                            listItem.IS_BACK_STATUS = "1";
                                        }
                                    }
                                }
                            }
                        }

                        EndorsementList.Add(listItem);
                    }
                }
            }
            return Json(new { data = EndorsementList, draw = Request["draw"], recordsTotal = endorsementList.TotalItemsCount, recordsFiltered = endorsementList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetInstallment(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            List<InstallmentList> InstallmentList = new List<InstallmentList>();

            List<Installment> installmentList = new List<Installment>();
            var policyId = form["POLICY_ID"];

            if (Extensions.IsInt64(policyId) && long.Parse(policyId) > 0)
            {

                installmentList = new GenericRepository<Installment>().FindBy($"POLICY_ID={policyId}");
                if (installmentList != null)
                {
                    int i = 1;
                    foreach (var item in installmentList)
                    {
                        InstallmentList listItem = new InstallmentList();

                        listItem.INSTALLMENT_ID = Convert.ToString(item.Id);
                        listItem.NO = Convert.ToString(i);
                        listItem.AMOUNT = item.Amount;

                        listItem.DUE_DATE = Convert.ToString(item.DueDate);
                        listItem.DUE_DATE = listItem.DUE_DATE.IsDateTime() ? DateTime.Parse(listItem.DUE_DATE).ToString("dd-MM-yyyy") : string.Empty;

                        InstallmentList.Add(listItem);
                        i++;
                    }
                }
            }

            return Json(new { data = InstallmentList, draw = Request["draw"], recordsTotal = installmentList.Count, recordsFiltered = installmentList.Count }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetClaimResonList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            var insuredClaimReasonlist = new ViewResultDto<List<V_InsuredClaimReason>>();
            insuredClaimReasonlist.Data = new List<V_InsuredClaimReason>();

            if (form["INSURED_ID"].IsInt64() && long.Parse(form["INSURED_ID"]) > 0)
            {
                insuredClaimReasonlist = new GenericRepository<V_InsuredClaimReason>().FindByPaged(
                                  conditions: $"INSURED_ID = {long.Parse(form["INSURED_ID"])}",
                                  orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},CLAIM_REASON_ID DESC" : "CLAIM_REASON_ID DESC",
                                  pageNumber: start,
                                  rowsPerPage: length,
                                  fetchDeletedRows: true,
                                  fetchHistoricRows: true);
            }
            else if (form["POLICY_ID"].IsInt64() && long.Parse(form["POLICY_ID"]) > 0)
            {
                var policyClaimReasonlist = new ViewResultDto<List<V_PolicyClaimReason>>();
                policyClaimReasonlist.Data = new List<V_PolicyClaimReason>();
                policyClaimReasonlist = new GenericRepository<V_PolicyClaimReason>().FindByPaged(
                                  conditions: $"POLICY_ID = {long.Parse(form["POLICY_ID"])}",
                                  orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},CLAIM_REASON_ID DESC" : "CLAIM_REASON_ID DESC",
                                  pageNumber: start,
                                  rowsPerPage: length,
                                  fetchDeletedRows: true,
                                  fetchHistoricRows: true);

                return Json(new { data = policyClaimReasonlist.Data, draw = Request["draw"], recordsTotal = policyClaimReasonlist.TotalItemsCount, recordsFiltered = policyClaimReasonlist.TotalItemsCount }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { data = insuredClaimReasonlist.Data, draw = Request["draw"], recordsTotal = insuredClaimReasonlist.TotalItemsCount, recordsFiltered = insuredClaimReasonlist.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        [LoginControl]
        public JsonResult GetPolicyInsured()
        {
            var result = "[]";
            var PolicyId = Request["PolicyId"].IsInt64() ? long.Parse(Request["PolicyId"]) : 0;
            if (PolicyId > 0)
            {
                var insuredList = new GenericRepository<V_Insured>().FindBy($"POLICY_ID={PolicyId}", orderby: "", fetchDeletedRows: true);
                result = insuredList.ToJSON();
            }

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetProfileReport(FormCollection form)
        {
            var type = form["profileReportType"];
            var response = new PrintResponse();

            if (type == "0")
            {
                IPrint<PolicyProfileReq> printt = new PolicyProfile();
                response = printt.DoWork(new PolicyProfileReq
                {
                    PolicyId = long.Parse(form["hdPolicyId"])
                });
            }
            else if (type == "1")
            {
                IPrint<InsuredProfileReq> printt = new InsuredProfile();
                response = printt.DoWork(new InsuredProfileReq
                {
                    PolicyId = long.Parse(form["hdPolicyId"]),
                    InsuredId = long.Parse(form["insuredList"])
                });
            }
            else if (type == "2")
            {
                IPrint<FamilyBasedInsuredProfileReq> printt = new FamilyBasedInsuredProfile();
                response = printt.DoWork(new FamilyBasedInsuredProfileReq
                {
                    PolicyId = long.Parse(form["hdPolicyId"]),
                    FamilyNo = form["familyNo"],
                });
            }

            return new JsonResult()
            {
                Data = new { FileUrl = response.Url, FileName = response.FileName, Message = response.Message, Code = response.Code }
            };
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetTransferReport(FormCollection form)
        {
            var type = form["transferReportType"];
            var response = new PrintResponse();

            if (type == "0")
            {
                IPrint<PolicyTransferReq> printt = new PolicyTransfer();
                response = printt.DoWork(new PolicyTransferReq
                {
                    PolicyId = long.Parse(form["hdPolicyId"])
                });
            }
            else if (type == "1")
            {
                IPrint<InsuredTransferReq> printt = new InsuredTransferForm();
                response = printt.DoWork(new InsuredTransferReq
                {
                    InsuredId = long.Parse(form["insuredList"])
                });
            }
            else if (type == "2")
            {
                IPrint<FamilyBasedTransferProfileReq> printt = new FamilyBasedTransferProfile();
                response = printt.DoWork(new FamilyBasedTransferProfileReq
                {
                    PolicyId = long.Parse(form["hdPolicyId"]),
                    FamilyNo = form["familyNo"],
                });
            }

            return new JsonResult()
            {
                Data = new { FileUrl = response.Url, FileName = response.FileName, Message = response.Message, Code = response.Code }
            };
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetEndorsementReport()
        {
            var endorsementId = Request["EndorsementId"];
            var policyId = Request["PolicyId"];
            var type = Request["Type"];
            var response = new PrintResponse();

            if (endorsementId.IsInt64() && long.Parse(endorsementId) > 0 && policyId.IsInt64() && long.Parse(policyId) > 0)
            {
                if (type == "0")
                {
                    IPrint<PolicyEndorsmentReq> printt = new PolicyEndorsment();
                    response = printt.DoWork(new PolicyEndorsmentReq
                    {
                        PolicyId = long.Parse(policyId),
                        EndorsementId = long.Parse(endorsementId)
                    });
                }
                else if (type == "1")
                {
                    IPrint<PolicyCertificateReq> printt = new PolicyCertificate();
                    response = printt.DoWork(new PolicyCertificateReq
                    {
                        PolicyId = long.Parse(policyId),
                        EndorsementId = long.Parse(endorsementId)
                    });
                }
            }
            return new JsonResult()
            {
                Data = new { FileUrl = response.Url, FileName = response.FileName, Message = response.Message, Code = response.Code }
            };
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetOtherReport(FormCollection form)
        {
            var type = form["reportType"];
            var response = new PrintResponse();

            if (form["hdPolicyId"].IsInt64() && long.Parse(form["hdPolicyId"]) > 0)
            {
                if (type == "0") //MainPolicy
                {
                    var policy = new GenericRepository<Policy>().FindById(long.Parse(form["hdPolicyId"]));
                    if (policy != null)
                    {
                        IPrint<MainPolicyReq> printt = new MainPolicy();
                        response = printt.DoWork(new MainPolicyReq
                        {
                            PolicyId = long.Parse(form["hdPolicyId"])
                        });
                    }
                }
                else if (type == "1") //PolicyCertificate
                {
                    var policy = new GenericRepository<Policy>().FindById(long.Parse(form["hdPolicyId"]));
                    if (policy != null)
                    {
                        var endorsement = new GenericRepository<Endorsement>().FindBy($"POLICY_ID = {policy.Id} AND TYPE='{((int)EndorsementType.BASLANGIC).ToString()}'").FirstOrDefault();
                        if (endorsement != null)
                        {
                            var a = form["insuredList"];

                            IPrint<PolicyCertificateReq> printt = new PolicyCertificate();
                            response = printt.DoWork(new PolicyCertificateReq
                            {
                                PolicyId = policy.Id,
                                EndorsementId = endorsement.Id,
                                InsuredIdList = a
                            });
                        }
                    }
                }
            }
            return new JsonResult()
            {
                Data = new { FileUrl = response.Url, FileName = response.FileName, Message = response.Message, Code = response.Code }
            };
        }

        [HttpPost]
        [LoginControl]
        private void FillCompanies()
        {

            var v_company = new GenericRepository<V_Company>().FindBy(orderby: "COMPANY_ID");
            ViewBag.v_company = v_company;
        }

        [LoginControl]
        public ActionResult PolicyDelete(Int64 id)
        {
            try
            {
                var searchClaim = new GenericRepository<Common.Entities.ProteinEntities.Claim>().FindBy($"POLICY_ID={id} AND STATUS NOT IN({((int)ClaimStatus.IPTAL).ToString()},{((int)ClaimStatus.SILINDI).ToString()})").FirstOrDefault();
                if (searchClaim != null)
                {
                    throw new Exception("Poliçeye Ait IPTAL Statüsü Dışında Hasar Bulunmakta! Lütfen Hasar Kayıtlarını Kontrol Ediniz...");
                }

                var repo = new GenericRepository<Policy>();
                var result = repo.FindById(id);
                result.Status = ((int)PolicyStatus.SILINDI).ToString();
                var spResponse = repo.Update(result);
                if (spResponse.Code == "100")
                    TempData["Alert"] = $"swAlert('İşlem Başarılı','Silme İşlemi Başarıyla Gerçekleştirildi.','success')";
                else
                    throw new Exception(spResponse.Code + " : " + spResponse.Message);
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Policy");
        }

        [HttpPost]
        public JsonResult GetInsurerDetails()
        {
            string id = Request["ContactId"];
            var result = "[]";
            try
            {
                if (id.IsInt64() && long.Parse(id) > 0)
                {
                    var insurer = new GenericRepository<V_Portfoy>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id }).FirstOrDefault();
                    if (insurer != null)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.CONTACT_ID = Convert.ToString(insurer.CONTACT_ID);
                        listItem.CORPORATE_ID = Convert.ToString(insurer.CORPORATE_ID);
                        listItem.PERSON_ID = Convert.ToString(insurer.PERSON_ID);

                        listItem.CONTACT_TYPE = Convert.ToString(insurer.CONTACT_TYPE);
                        listItem.GENDER = Convert.ToString(insurer.GENDER);

                        listItem.FIRST_NAME = Convert.ToString(insurer.FIRST_NAME);
                        listItem.LAST_NAME = Convert.ToString(insurer.LAST_NAME);
                        listItem.IDENTITY_NO = Convert.ToString(insurer.IDENTITY_NO);
                        listItem.PASSPORT_NO = Convert.ToString(insurer.PASSPORT_NO);
                        listItem.NAME_OF_FATHER = Convert.ToString(insurer.NAME_OF_FATHER);
                        string date = Convert.ToString(insurer.BIRTHDATE);
                        listItem.BIRTHDATE = date.IsDateTime() ? DateTime.Parse(date).ToString("dd-MM-yyyy") : null;
                        listItem.BIRTHPLACE = Convert.ToString(insurer.BIRTHPLACE);
                        listItem.NATIONALITY_ID = Convert.ToString(insurer.NATIONALITY_ID);

                        listItem.CONTACT_TITLE = Convert.ToString(insurer.CONTACT_TITLE);
                        listItem.TAX_NUMBER = Convert.ToString(insurer.TAX_NUMBER);
                        listItem.TAX_OFFICE = Convert.ToString(insurer.TAX_OFFICE);


                        var phoneItem = new GenericRepository<V_ContactPhone>().FindBy("CONTACT_ID=:id AND PHONE_TYPE=:phoneType", orderby: "CONTACT_ID", parameters: new { id, phoneType = new DbString { Value = ((int)PhoneType.IS).ToString(), Length = 3 } }).FirstOrDefault();
                        if (phoneItem != null)
                        {
                            listItem.PHONE_ID = Convert.ToString(phoneItem.PHONE_ID);
                            listItem.PHONE_NO = Convert.ToString(phoneItem.PHONE_NO);
                        }

                        var mobilePhoneItem = new GenericRepository<V_ContactPhone>().FindBy("CONTACT_ID=:id AND PHONE_TYPE=:phoneType", orderby: "CONTACT_ID", parameters: new { id, phoneType = new DbString { Value = ((int)PhoneType.MOBIL).ToString(), Length = 3 } }).FirstOrDefault();
                        if (mobilePhoneItem != null)
                        {
                            listItem.MOBILE_PHONE_ID = Convert.ToString(mobilePhoneItem.PHONE_ID);
                            listItem.MOBILE_PHONE_NO = Convert.ToString(mobilePhoneItem.PHONE_NO);
                        }

                        var emailItem = new GenericRepository<V_ContactEmail>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id }).FirstOrDefault();
                        if (emailItem != null)
                        {
                            listItem.EMAIL_ID = Convert.ToString(emailItem.EMAIL_ID);
                            listItem.EMAIL_DETAILS = Convert.ToString(emailItem.DETAILS);
                        }

                        var addressItem = new GenericRepository<V_ContactAddress>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id }).FirstOrDefault();
                        if (addressItem != null)
                        {
                            listItem.ADDRESS_ID = Convert.ToString(addressItem.ADDRESS_ID);
                            listItem.DETAILS = Convert.ToString(addressItem.DETAILS);
                            listItem.ZIP_CODE = Convert.ToString(addressItem.ZIP_CODE);
                            listItem.CITY_ID = Convert.ToString(addressItem.CITY_ID);
                            listItem.COUNTY_ID = Convert.ToString(addressItem.COUNTY_ID);
                        }

                        var bankAccount = new GenericRepository<V_ContactBank>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id }).FirstOrDefault();
                        if (bankAccount != null)
                        {
                            listItem.BANK_ACCOUNT_ID = Convert.ToString(bankAccount.BANK_ACCOUNT_ID);

                            listItem.BANK_ACCOUNT_NAME = bankAccount.BANK_ACCOUNT_NAME;
                            listItem.BANK_ID = Convert.ToString(bankAccount.BANK_ID);

                            listItem.BANK_BRANCH_ID = Convert.ToString(bankAccount.BANK_BRANCH_ID);
                            listItem.BANK_ACCOUNT_NO = bankAccount.BANK_ACCOUNT_NO;
                            listItem.IBAN = bankAccount.IBAN;
                            listItem.BANK_CURRENCY_TYPE = bankAccount.CURRENCY_TYPE;
                        }

                        List<dynamic> insurerList = new List<dynamic>();
                        insurerList.Add(listItem);
                        result = insurerList.ToJSON();
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        public JsonResult GetEndEndorsementNo()
        {
            var result = "[]";
            try
            {
                var policyId = Request["PolicyId"];
                if (policyId.IsInt64() && long.Parse(policyId) > 0)
                {
                    var endorsement = new GenericRepository<Endorsement>().FindBy($"POLICY_ID={policyId}", orderby: "NO DESC").FirstOrDefault();

                    if (endorsement != null)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();
                        listItem.NO = endorsement.No + 1;
                        List<dynamic> insurerList = new List<dynamic>();
                        insurerList.Add(listItem);
                        result = insurerList.ToJSON();
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult PolicySAGMER(FormCollection form)
        {
            try
            {
                if (!form["hdPolicyIdForSAGMER"].IsInt64() && long.Parse(form["hdPolicyIdForSAGMER"]) <= 0)
                {
                    throw new Exception("Poliçe Bilgisi Bulunamadı.");
                }

                var policy = new GenericRepository<Policy>().FindById(long.Parse(form["hdPolicyIdForSAGMER"]));
                if (policy == null)
                {
                    throw new Exception("Seçilen Poliçe Bilgisi Bulunamadı.");
                }

                if (form["SAGMER_CODE"].IsNull())
                {
                    throw new Exception("Lütfen SAGMER Otorizasyon Kodunu giriniz.");
                }

                policy.SbmAuthorizationCode = form["SAGMER_CODE"];
                policy.Status = ((int)PolicyStatus.TEKLIF).ToString();
                SpResponse spPolicy = new GenericRepository<Policy>().Update(policy);
                if (spPolicy.Code != "100")
                {
                    throw new Exception(spPolicy.Code + " : " + spPolicy.Message);
                }
                TempData["Alert"] = $"swAlert('İşlem Başarılı','{policy.No} nolu Poliçenin Durumu Başarıyla Değiştirildi.','success')";


            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Policy");
        }

        [HttpPost]
        public JsonResult InsurerSave(FormCollection form)
        {
            var result = new AjaxResultDto<InsurerSaveResult>();
            try
            {
                result.Data = new InsurerSaveResult();
                ServiceResponse response = new ServiceResponse();
                PolicyDto policyDto = new PolicyDto();
                IService<PolicyDto> policyService = new PolicyService();

                policyDto.isInsurerChange = true;

                policyDto.InsurerContactId = form["CONTACT_ID"].IsInt64() ? long.Parse(form["CONTACT_ID"]) : 0;
                policyDto.InsurerPersonId = form["PERSON_ID"].IsInt64() ? long.Parse(form["PERSON_ID"]) : 0;
                policyDto.InsurerCorporateId = form["CORPORATE_ID"].IsInt64() ? long.Parse(form["CORPORATE_ID"]) : 0;
                policyDto.InsurerPhoneId = form["PHONE_ID"].IsInt64() ? long.Parse(form["PHONE_ID"]) : 0;
                policyDto.InsurerGsmPhoneId = form["MOBILE_PHONE_ID"].IsInt64() ? long.Parse(form["MOBILE_PHONE_ID"]) : 0;
                policyDto.InsurerEmailId = form["EMAIL_ID"].IsInt64() ? long.Parse(form["EMAIL_ID"]) : 0;
                policyDto.InsurerAddressId = form["ADDRESS_ID"].IsInt64() ? long.Parse(form["ADDRESS_ID"]) : 0;

                var contactType = form["contactType"];
                if (Extensions.IsNull(contactType))
                {
                    throw new Exception("Tipi Alanı Seçilmesi Gerekmektedir.");
                }
                else
                {
                    policyDto.InsurerType = contactType;
                }

                if (contactType == ((int)ContactType.GERCEK).ToString())
                {
                    policyDto.InsurerTcNo = form["insurerIdentityNo"].IsInt64() ? (long?)Convert.ToInt64(form["insurerIdentityNo"]) : null;

                    policyDto.InsurerGender = form["insurerGender"].IsNull() ? null : form["insurerGender"];

                    policyDto.InsurerTitle = (form["insurerName"].IsNull() ? "" : form["insurerName"]) + " " + (form["insurerSurName"].IsNull() ? "" : form["insurerSurName"]);

                    policyDto.InsurerName = Extensions.IsNull(form["insurerName"]) ? null : form["insurerName"];

                    policyDto.InsurerLastName = form["insurerSurName"].IsNull() ? null : form["insurerSurName"];

                    policyDto.InsurerNationality = form["insurerNationality"].IsNull() ? null : form["insurerNationality"];

                    policyDto.InsurerNameOfFather = form["insurerFatherofName"].IsNull() ? null : form["insurerFatherofName"];

                    policyDto.InsurerBirthPlace = form["insurerBirthPlace"].IsNull() ? null : form["insurerBirthPlace"];

                    policyDto.InsurerBirthDate = form["insurerBirthDate"].IsDateTime() ? (DateTime?)DateTime.Parse(form["insurerBirthDate"]) : null;

                    policyDto.InsurerPassport = form["insurerPassportNo"].IsNull() ? null : form["insurerPassportNo"];
                }
                else if (contactType == ((int)ContactType.TUZEL).ToString())
                {
                    policyDto.InsurerTitle = Extensions.IsNull(form["insurerTitle"]) ? null : form["insurerTitle"];

                    policyDto.InsurerVkn = form["insurerTaxNumber"].IsInt64() ? (long?)Convert.ToInt64(form["insurerTaxNumber"]) : null;

                    policyDto.InsurerTaxOffice = Extensions.IsNull(form["insurerTaxOffice"]) ? null : form["insurerTaxOffice"];

                    policyDto.InsurerCorporateName = Extensions.IsNull(form["insurerTitle"]) ? null : form["insurerTitle"];
                }

                policyDto.InsurerTelNo = Extensions.IsNull(form["insurerPhoneNo"]) ? null : form["insurerPhoneNo"];

                policyDto.InsurerGsmNo = Extensions.IsNull(form["insurerGsmPhoneNo"]) ? null : form["insurerGsmPhoneNo"];

                policyDto.InsurerEmail = Extensions.IsNull(form["insurerEmail"]) ? null : form["insurerEmail"];


                var adressDetails = form["insurerAdress"];
                if (!Extensions.IsNull(adressDetails))
                {
                    policyDto.InsurerAddress = adressDetails;

                    policyDto.InsurerZipCode = Extensions.IsNull(form["insurerZipCode"]) ? null : form["insurerZipCode"];

                    var cityId = form["city"];
                    if (!Extensions.IsInt64(cityId) || long.Parse(cityId) < 1)
                    {
                        policyDto.InsurerCityId = null;
                    }
                    else
                    {
                        policyDto.InsurerCityId = long.Parse(cityId);
                    }

                    var countyId = form["county"];
                    if (!Extensions.IsInt64(countyId) || long.Parse(countyId) < 1)
                    {
                        policyDto.InsurerCountyId = null;
                    }
                    else
                    {
                        policyDto.InsurerCountyId = long.Parse(countyId);
                    }
                }

                var bankId = form["BANK_ID"];
                if (bankId.IsInt64())
                {
                    var bankAccountId = form["BANK_ACCOUNT_ID"];
                    if (bankAccountId.IsInt64())
                    {
                        policyDto.InsurerBankAccountId = long.Parse(bankAccountId);
                    }

                    policyDto.InsurerBankAccountName = form["BANK_ACCOUNT_NAME"];
                    policyDto.InsurerBankId = long.Parse(bankId);
                    policyDto.InsurerBankBranchId = form["BANK_BRANCH_ID"].IsInt64() ? (long?)long.Parse(form["BANK_BRANCH_ID"]) : null;
                    policyDto.InsurerBankNo = form["BANK_ACCOUNT_NO"];
                    policyDto.InsurerIban = form["IBAN"];
                    policyDto.InsurerBankCurrencyType = form["BANK_CURRENCY_TYPE"];
                }

                response = policyService.ServiceInsert(policyDto, Token: "");
                if (response.IsSuccess)
                {
                    result.Data.ContactTitle = policyDto.InsurerTitle;
                    result.Data.contactId = response.Id;
                    result.ResultCode = response.Code;
                    result.ResultMessage = response.Message;
                    //TempData["Alert"] = $"swAlert('İşlem Başarılı','{spResponsePolicy.Data.No} Başarıyla Kaydedildi.','success')";
                }
                else
                {
                    throw new Exception(response.Code + " : " + response.Message);
                }

            }
            catch (Exception ex)
            {
                result.Data = null;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [LoginControl]
        public ActionResult PolicyForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                if (formaction != "claimopen")
                {
                    var policyTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Policy);
                    ViewBag.PolicyTypeList = policyTypeList;
                    TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                    #region Get Company
                    var CompanyListWithPackage = new GenericRepository<V_Package>().FindBy("COMPANY_ID IS NOT NULL", orderby: "COMPANY_NAME");
                    var CompanyListWithAgency = new GenericRepository<V_Agency>().FindBy("COMPANY_ID IS NOT NULL", orderby: "COMPANY_NAME");

                    var packageCompanyIdList = CompanyListWithPackage.Select(p => p.COMPANY_ID).Distinct().ToList();
                    var agencyCompanyIdList = CompanyListWithAgency.Select(p => p.COMPANY_ID).Distinct().ToList();

                    List<long> simpleCompanyIdList = new List<long>();

                    foreach (var packageCompanyId in packageCompanyIdList)
                    {
                        foreach (var agencyCompanyId in agencyCompanyIdList)
                        {
                            if (packageCompanyId == agencyCompanyId)
                            {
                                simpleCompanyIdList.Add(long.Parse(packageCompanyId.ToString()));
                                break;
                            }
                        }
                    }

                    List<SimpleCompany> CompanyList = new List<SimpleCompany>();
                    if (simpleCompanyIdList.Count > 0)
                    {
                        foreach (var companyId in simpleCompanyIdList)
                        {
                            var agency = CompanyListWithAgency.Where(a => a.COMPANY_ID == companyId).FirstOrDefault();
                            if (agency != null)
                            {
                                SimpleCompany company = new SimpleCompany()
                                {
                                    COMPANY_ID = agency.COMPANY_ID,
                                    COMPANY_NAME = agency.COMPANY_NAME
                                };
                                CompanyList.Add(company);
                            }
                        }
                    }
                    ViewBag.CompanyList = CompanyList;
                    #endregion

                    var CityList = new CityRepository().FindBy();
                    ViewBag.CityList = CityList;

                    var CurrencyTypes = LookupHelper.GetLookupData(Constants.LookupTypes.Currency);
                    ViewBag.CurrencyTypes = CurrencyTypes;

                    var CountryList = new CountryRepository().FindBy();
                    ViewBag.CountryList = CountryList;

                    var PolicyGroupList = new PolicyGroupRepository().FindBy();
                    ViewBag.PolicyGroupList = PolicyGroupList;

                    var LocationTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Location);
                    ViewBag.LocationTypeList = LocationTypeList;

                    var GenderList = LookupHelper.GetLookupData(Constants.LookupTypes.Gender);
                    ViewBag.GenderList = GenderList;

                    var ContactTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Contact);
                    ViewBag.ContactTypeList = ContactTypeList;

                    var CashPaymentTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.CashPayment, showChoose: true);
                    ViewBag.CashPaymentTypeList = CashPaymentTypeList;

                    var PaymentTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Payment);
                    ViewBag.PaymentTypeList = PaymentTypeList;

                    var PolicyTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Policy);
                    ViewBag.PolicyTypeList = PolicyTypeList;

                    var BranchList = new BranchRepository().FindBy("PARENT_ID IS NULL");
                    ViewBag.BranchList = BranchList;

                    ViewBag.RuleCategoryTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.RuleCategory);

                    var RuleList = new GenericRepository<V_Rule>().FindBy(orderby: "RULE_NAME ASC");
                    ViewBag.RuleList = RuleList;

                    var RuleGroupList = new GenericRepository<RuleGroup>().FindBy();
                    ViewBag.RuleGroupList = RuleGroupList;

                    var BankList = new BankRepository().FindBy();
                    ViewBag.BankList = BankList;

                    ViewBag.InsuredList = string.Empty;
                    ViewBag.EndorsementList = string.Empty;

                    ViewBag.isEdit = false;
                }
                switch (formaction.ToLower())
                {
                    case "view":
                    case "edit":
                        if (formaction.ToLower() == "edit")
                            ViewBag.Title = "Poliçe Güncelle";
                        else
                            ViewBag.Title = "Poliçe Görüntüle";

                        if (id > 0)
                        {
                            var Policy = new GenericRepository<V_Policy>().FindBy("POLICY_ID=:id", orderby: "POLICY_ID", parameters: new { id }).FirstOrDefault();
                            if (Policy != null)
                            {
                                ViewBag.Policy = Policy;

                                var installmentList = new GenericRepository<Installment>().FindBy($"POLICY_ID=:id", orderby: "DUE_DATE", parameters: new { id });
                                ViewBag.InstallmentList = installmentList;

                                var ProductList = new GenericRepository<Product>().FindBy("COMPANY_ID=:COMPANY_ID", parameters: new { Policy.COMPANY_ID }, orderby: "NAME");
                                ViewBag.ProductList = ProductList;

                                var AgencyList = new GenericRepository<Agency>().FindBy("COMPANY_ID=:COMPANY_ID", parameters: new { Policy.COMPANY_ID }, orderby: "NAME");
                                ViewBag.AgencyList = AgencyList;

                                var SubProductList = new GenericRepository<Subproduct>().FindBy(orderby: "NAME");
                                ViewBag.SubProductList = SubProductList;

                                var Insureds = new GenericRepository<V_Insured>().FindBy("POLICY_ID=:id", orderby: "INSURED_ID", parameters: new { id });

                                if (Insureds.Count > 0)
                                {
                                    ViewBag.InsuredList = Insureds;
                                }

                                var Endorsements = new GenericRepository<V_PolicyEndorsement>().FindBy("POLICY_ID=:id", orderby: "ENDORSEMENT_TYPE ASC", parameters: new { id });

                                if (Endorsements.Count > 0)
                                {
                                    ViewBag.EndorsementList = Endorsements;
                                    if (Endorsements[0].STATUS != ((int)Status.AKTIF).ToString())
                                    {
                                        ViewBag.StartEndorsementId = Endorsements[0].ENDORSEMENT_ID;
                                    }
                                }

                                //RuleSpecial
                                var ruleSpecialList = new GenericRepository<V_RulePolicy>().FindBy("POLICY_ID=:id AND TYPE=:type", orderby: "POLICY_ID", parameters: new { id, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });

                                List<dynamic> ruleDynamicSpecialList = new List<dynamic>();
                                int specialId = 1;

                                if (ruleSpecialList != null)
                                {
                                    foreach (var item in ruleSpecialList)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = specialId;
                                        listItem.ClientId = specialId;
                                        listItem.STATUS = item.STATUS;

                                        listItem.RULE_POLICY_ID = Convert.ToString(item.RULE_POLICY_ID);
                                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                        listItem.RULE_NAME = item.RULE_NAME;
                                        listItem.RESULT = item.RESULT;
                                        listItem.INHERITED_NAME = string.Empty;
                                        listItem.INHERITED_CODE = string.Empty;
                                        listItem.isOpen = "0";

                                        if (formaction.ToLower() == "edit")
                                            listItem.IsInherited = "1";
                                        else
                                            listItem.IsInherited = "0";

                                        ruleDynamicSpecialList.Add(listItem);

                                        specialId++;
                                    }
                                }

                                var ruleSubProductSpecialList = new GenericRepository<V_RuleSubProduct>().FindBy("SUBPRODUCT_ID=:SUBPRODUCT_ID AND TYPE=:type", orderby: "SUBPRODUCT_ID", parameters: new { Policy.SUBPRODUCT_ID, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });

                                if (ruleSubProductSpecialList != null)
                                {
                                    foreach (var item in ruleSubProductSpecialList)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = specialId;
                                        listItem.ClientId = specialId;
                                        listItem.STATUS = item.STATUS;

                                        listItem.RULE_SUBPRODUCT_ID = Convert.ToString(item.RULE_SUBPRODUCT_ID);
                                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                        listItem.RULE_NAME = item.RULE_NAME;
                                        listItem.RESULT = item.RESULT;
                                        listItem.INHERITED_NAME = "AltÜrün";
                                        listItem.INHERITED_CODE = Convert.ToString(item.SUBPRODUCT_CODE);
                                        listItem.isOpen = "0";
                                        listItem.IsInherited = "0";

                                        ruleDynamicSpecialList.Add(listItem);

                                        specialId++;
                                    }
                                }

                                var ruleProductSpecialList = new GenericRepository<V_RuleProduct>().FindBy("PRODUCT_ID=:PRODUCT_ID AND TYPE=:type", orderby: "PRODUCT_ID", parameters: new { Policy.PRODUCT_ID, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });
                                if (ruleProductSpecialList != null)
                                {

                                    foreach (var item in ruleProductSpecialList)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = specialId;
                                        listItem.ClientId = specialId;
                                        listItem.STATUS = item.STATUS;

                                        listItem.RULE_PRODUCT_ID = Convert.ToString(item.RULE_PRODUCT_ID);
                                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                        listItem.RULE_NAME = item.RULE_NAME;
                                        listItem.RESULT = item.RESULT;

                                        listItem.INHERITED_NAME = "Ürün";
                                        listItem.INHERITED_CODE = Convert.ToString(item.PRODUCT_CODE);
                                        listItem.isOpen = "0";
                                        listItem.IsInherited = "0";

                                        ruleDynamicSpecialList.Add(listItem);

                                        specialId++;
                                    }
                                }

                                if (Policy.POLICY_GROUP_ID != null)
                                {
                                    var rulePolicyGroupExtraList = new GenericRepository<V_RulePolicyGroup>().FindBy("POLICY_GROUP_ID=:POLICY_GROUP_ID AND TYPE=:type", orderby: "POLICY_GROUP_ID", parameters: new { Policy.POLICY_GROUP_ID, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });

                                    if (rulePolicyGroupExtraList != null)
                                    {
                                        foreach (var item in rulePolicyGroupExtraList)
                                        {
                                            dynamic listItem = new System.Dynamic.ExpandoObject();

                                            listItem.id = specialId;
                                            listItem.ClientId = specialId;
                                            listItem.STATUS = item.STATUS;

                                            listItem.RULE_POLICY_GROUP_ID = Convert.ToString(item.RULE_POLICY_GROUP_ID);
                                            listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                                            listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                                            listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                            listItem.RULE_NAME = item.RULE_NAME;
                                            listItem.RESULT = item.RESULT;
                                            listItem.INHERITED_NAME = "Poliçe Grubu";
                                            listItem.INHERITED_CODE = Convert.ToString(item.POLICY_GROUP_NAME);
                                            listItem.isOpen = "0";
                                            listItem.IsInherited = "0";

                                            ruleDynamicSpecialList.Add(listItem);

                                            specialId++;
                                        }
                                    }
                                }

                                ViewBag.RuleSpecialList = ruleDynamicSpecialList.ToJSON(true);

                                //RuleExtra
                                var ruleExtraList = new GenericRepository<V_RulePolicy>().FindBy("POLICY_ID=:id AND TYPE=:type", orderby: "POLICY_ID", parameters: new { id, type = new DbString { Value = ((int)RuleType.EK_PROTOKOL).ToString(), Length = 3 } });

                                List<dynamic> ruleDynamicExtraList = new List<dynamic>();
                                int extraId = 1;

                                if (ruleExtraList != null)
                                {
                                    foreach (var item in ruleExtraList)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = extraId;
                                        listItem.ClientId = extraId;
                                        listItem.STATUS = item.STATUS;

                                        listItem.RULE_POLICY_ID = Convert.ToString(item.RULE_POLICY_ID);
                                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                        listItem.RULE_NAME = item.RULE_NAME;
                                        listItem.RESULT = item.RESULT;
                                        listItem.INHERITED_NAME = string.Empty;
                                        listItem.INHERITED_CODE = string.Empty;
                                        listItem.isOpen = "0";
                                        if (formaction.ToLower() == "edit")
                                            listItem.IsInherited = "1";
                                        else
                                            listItem.IsInherited = "0";

                                        ruleDynamicExtraList.Add(listItem);

                                        extraId++;
                                    }
                                }

                                var ruleSubProductExtraList = new GenericRepository<V_RuleSubProduct>().FindBy("SUBPRODUCT_ID=:SUBPRODUCT_ID AND TYPE=:type", orderby: "SUBPRODUCT_ID", parameters: new { Policy.SUBPRODUCT_ID, type = new DbString { Value = ((int)RuleType.EK_PROTOKOL).ToString(), Length = 3 } });

                                if (ruleSubProductExtraList != null)
                                {

                                    foreach (var item in ruleSubProductExtraList)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = extraId;
                                        listItem.ClientId = extraId;
                                        listItem.STATUS = item.STATUS;

                                        listItem.RULE_SUBPRODUCT_ID = Convert.ToString(item.RULE_SUBPRODUCT_ID);
                                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                        listItem.RULE_NAME = item.RULE_NAME;
                                        listItem.RESULT = item.RESULT;
                                        listItem.INHERITED_NAME = "AltÜrün";
                                        listItem.INHERITED_CODE = Convert.ToString(item.SUBPRODUCT_CODE);
                                        listItem.isOpen = "0";
                                        listItem.IsInherited = "0";

                                        ruleDynamicExtraList.Add(listItem);

                                        extraId++;
                                    }
                                }

                                if (Policy.POLICY_GROUP_ID != null)
                                {
                                    var rulePolicyGroupExtraList = new GenericRepository<V_RulePolicyGroup>().FindBy("POLICY_GROUP_ID=:POLICY_GROUP_ID AND TYPE=:type", orderby: "POLICY_GROUP_ID", parameters: new { Policy.POLICY_GROUP_ID, type = new DbString { Value = ((int)RuleType.EK_PROTOKOL).ToString(), Length = 3 } });

                                    if (rulePolicyGroupExtraList != null)
                                    {
                                        foreach (var item in rulePolicyGroupExtraList)
                                        {
                                            dynamic listItem = new System.Dynamic.ExpandoObject();

                                            listItem.id = extraId;
                                            listItem.ClientId = extraId;
                                            listItem.STATUS = item.STATUS;

                                            listItem.RULE_POLICY_GROUP_ID = Convert.ToString(item.RULE_POLICY_GROUP_ID);
                                            listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                                            listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                                            listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                            listItem.RULE_NAME = item.RULE_NAME;
                                            listItem.RESULT = item.RESULT;
                                            listItem.INHERITED_NAME = "Poliçe Grubu";
                                            listItem.INHERITED_CODE = Convert.ToString(item.POLICY_GROUP_NAME);
                                            listItem.isOpen = "0";
                                            listItem.IsInherited = "0";

                                            ruleDynamicExtraList.Add(listItem);

                                            extraId++;
                                        }
                                    }
                                }

                                ViewBag.RuleExtraList = ruleDynamicExtraList.ToJSON();

                                ViewBag.isEdit = true;
                            }
                        }
                        break;
                    case "claimopen":
                        #region Claim Open / Close

                        if (id > 0)
                        {
                            var isOpen = Request["isOpen"];
                            var policy = new PolicyRepository().FindById(id);
                            if (policy != null)
                            {
                                policy.IsOpenToClaim = isOpen;
                                var spResponse = new PolicyRepository().Update(policy);
                                if (spResponse.Code == "100")
                                {
                                    var responseChange = isOpen == "0" ? "Kapatıldı." : "Açıldı";
                                    TempData["Alert"] = $"swAlert('İşlem Başarılı','{policy.No} lu Poliçe Hasara {responseChange}','success')";
                                }
                                else
                                {
                                    throw new Exception(spResponse.Code + " : " + spResponse.Message);
                                }
                            }
                        }

                        #endregion
                        return RedirectToAction("Policy");
                    case "statuschange":
                        #region Status Change
                        var endorsementId = Request["EndorsementId"];

                        if (id > 0)
                        {
                            var policy = new PolicyRepository().FindById(id);

                            var status = Request["Status"];
                            if (status.Equals(Convert.ToString((int)PolicyStatus.TANZIMLI)))
                            {
                                var insuredList = new GenericRepository<V_Insured>().FindBy("POLICY_ID=:id", orderby: "INSURED_ID", parameters: new { id });
                                if (insuredList == null || insuredList.Count <= 0)
                                {
                                    throw new Exception("Poliçenin tanzim edilebilmesi için başlangıç zeyline en az bir sigortalı ekleyiniz!");
                                }

                                //CheckPayment
                                if (policy.InstallmentCount == null)
                                {
                                    throw new Exception("Poliçenin tanzim edilebilmesi için ödeme bilgisini giriniz!");
                                }

                            }
                            if (policy != null)
                            {
                                // Meriyete Dönüş mü?
                                if (status.Equals(Convert.ToString((int)PolicyStatus.TANZIMLI)) && policy.Status.Equals(Convert.ToString((int)PolicyStatus.IPTAL)))
                                {
                                    var lastCancelEndorsement = new GenericRepository<Endorsement>().FindBy($"POLICY_ID={id} AND (TYPE=11 OR TYPE=12 OR TYPE=13 OR TYPE=14 OR TYPE=15)", orderby: "NO desc").FirstOrDefault();
                                    if (lastCancelEndorsement == null)
                                    {
                                        throw new Exception("Son iptal zeyli bulunamadı!");
                                    }

                                    Endorsement endorsement = new Endorsement()
                                    {
                                        Id = 0,
                                        Category = "0",
                                        DateOfIssue = DateTime.Now,
                                        PolicyId = policy.Id,
                                        RegistrationDate = DateTime.Now,
                                        StartDate = lastCancelEndorsement.StartDate,
                                        Type = "16",
                                        TypeDescriptionType = "1",
                                        No = lastCancelEndorsement.No + 1
                                    };
                                    var spResponseEndorsement = new EndorsementRepository().Insert(endorsement);
                                    if (spResponseEndorsement.Code != "100")
                                    {
                                        throw new Exception(spResponseEndorsement.Code + " : " + spResponseEndorsement.Message);
                                    }

                                    policy.Status = status;
                                    var spResponse = new PolicyRepository().Update(policy);
                                    if (spResponse.Code != "100")
                                    {
                                        throw new Exception(spResponse.Code + " : " + spResponse.Message);
                                    }
                                    TempData["Alert"] = $"swAlert('İşlem Başarılı','{policy.No} nolu Poliçe Başarıyla Meriyete Döndürüldü.','success')";
                                }
                                else
                                {
                                    policy.Status = status;
                                    var spResponse = new PolicyRepository().Update(policy);
                                    if (spResponse.Code == "100")
                                    {
                                        Endorsement endorsement = new Endorsement();
                                        if (!endorsementId.IsInt64() || long.Parse(endorsementId) <= 0)
                                        {
                                            endorsement = new GenericRepository<Endorsement>().FindBy($"POLICY_ID={id} AND TYPE='{((int)EndorsementType.BASLANGIC).ToString()}'").SingleOrDefault();
                                        }
                                        else
                                        {
                                            endorsement = new GenericRepository<Endorsement>().FindById(long.Parse(endorsementId));
                                        }
                                        if (endorsement != null)
                                        {
                                            if (endorsement.Status != ((int)Status.AKTIF).ToString())
                                            {
                                                endorsement.Premium = 0;

                                                if (endorsement.Type != ((int)EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI).ToString() && endorsement.Type != ((int)EndorsementType.SE_DEGISIKLIGI).ToString() && endorsement.Type != ((int)EndorsementType.BASLANGIC_BITIS_TARIHI).ToString() && endorsement.Type != ((int)EndorsementType.URETIM_KAYNAGI_DEGISIKLIGI).ToString())
                                                {
                                                    var insuredList = new GenericRepository<Insured>().FindBy($"LAST_ENDORSEMENT_ID={endorsement.Id}", fetchDeletedRows: (endorsement.Type == ((int)EndorsementType.SIGORTALI_CIKISI).ToString() ? true : false));
                                                    if (insuredList != null && insuredList.Count > 0)
                                                    {
                                                        foreach (var insured in insuredList)
                                                        {
                                                            if (endorsement.Type != ((int)EndorsementType.SIGORTALI_GIRISI).ToString() && endorsement.Type != ((int)EndorsementType.BASLANGIC).ToString())
                                                            {
                                                                EndorsementInsured endorsementInsured = new EndorsementInsured
                                                                {
                                                                    EndorsementId = endorsement.Id,
                                                                    InsuredId = insured.Id,
                                                                    InsuredStatus = insured.Status,
                                                                    PackageId = insured.PackageId,
                                                                    Premium = insured.Premium,
                                                                    TotalPremium = insured.TotalPremium
                                                                };
                                                                new GenericRepository<EndorsementInsured>().Insert(endorsementInsured);
                                                            }

                                                            endorsement.Premium += insured.Premium;
                                                        }
                                                        if (endorsement.Premium != 0)
                                                        {
                                                            var firstEndorsement = endorsement;

                                                            if (firstEndorsement != null)
                                                            {
                                                                var insList = new GenericRepository<Installment>().FindBy($"POLICY_ID=:policyId AND ENDORSEMENT_ID=:endorsementId", parameters: new { policyId = firstEndorsement.PolicyId, endorsementId = endorsement.Id });
                                                                if (insList != null && insList.Count > 0)
                                                                {
                                                                    foreach (var item in insList)
                                                                    {
                                                                        item.Status = ((int)Status.SILINDI).ToString();
                                                                        new GenericRepository<Installment>().Update(item);
                                                                    }
                                                                }

                                                                var instalmentList = new GenericRepository<Installment>().FindBy($"POLICY_ID=:policyId AND ENDORSEMENT_ID=:endorsementId AND DUE_DATE>= TO_DATE('{(DateTime)firstEndorsement.DateOfIssue}','DD.MM.YYYY HH24:MI:SS')", parameters: new { policyId = firstEndorsement.PolicyId, endorsementId = endorsement.Id });
                                                                if (instalmentList != null && instalmentList.Count > 0)
                                                                {
                                                                    foreach (var item in instalmentList)
                                                                    {
                                                                        Installment installment = new Installment
                                                                        {
                                                                            DueDate = item.DueDate,
                                                                            Amount = (decimal)endorsement.Premium / instalmentList.Count,
                                                                            EndorsementId = endorsement.Id,
                                                                            PolicyId = endorsement.PolicyId
                                                                        };
                                                                        new GenericRepository<Installment>().Insert(installment);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    Installment installment = new Installment
                                                                    {
                                                                        DueDate = (DateTime)endorsement.DateOfIssue,
                                                                        Amount = (decimal)endorsement.Premium,
                                                                        EndorsementId = endorsement.Id,
                                                                        PolicyId = endorsement.PolicyId
                                                                    };
                                                                    new GenericRepository<Installment>().Insert(installment);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                new GenericRepository<Endorsement>().Update(endorsement);
                                            }
                                        }
                                    }
                                    TempData["Alert"] = $"swAlert('İşlem Başarılı','{policy.No} nolu Poliçe Durumu Başarıyla Değiştirildi.','success')";
                                }

                            }
                        }

                        #endregion
                        if (!endorsementId.IsInt64() || long.Parse(endorsementId) <= 0)
                        {
                            return RedirectToAction("Policy");
                        }
                        else
                        {
                            return RedirectToAction("Endorsement/" + id);
                        }
                    default:
                        ViewBag.Title = "Poliçe Ekle";
                        break;
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
                return RedirectToAction("Policy");
            }
            return View();
        }

        [HttpPost]
        public ActionResult ClaimOpen(FormCollection form)
        {
            try
            {
                long policyId = 0;
                long insuredId = 0;
                long reasonId = 0;
                long type = -1;
                var isOpen = Request["isOpen"];
                var description = "";
                var date = DateTime.Now;
                if (isOpen.Equals("0"))
                {
                    policyId = long.Parse(form["hdPolicyIdForClaimClose"]);
                    insuredId = long.Parse(form["hdInsuredIdForClaimClose"]);
                    reasonId = long.Parse(form["CLAIM_CLOSE_REASON_ID"]);
                    type = 1;
                    description = form["CLAIM_CLOSE_REASON_DESCRIPTION"];
                    date = string.IsNullOrEmpty(form["CLAIM_CLOSE_START_DATE"]) ? date : DateTime.Parse(form["CLAIM_CLOSE_START_DATE"]);
                }
                else
                {
                    policyId = long.Parse(form["hdPolicyIdForClaimOpen"]);
                    insuredId = long.Parse(form["hdInsuredIdForClaimOpen"]);
                    reasonId = long.Parse(form["CLAIM_OPEN_REASON_ID"]);
                    type = 0;
                    description = form["CLAIM_OPEN_REASON_DESCRIPTION"];
                    date = string.IsNullOrEmpty(form["CLAIM_OPEN_START_DATE"]) ? date : DateTime.Parse(form["CLAIM_OPEN_START_DATE"]);
                }
                #region Claim Open / Close
                if (reasonId > 0 && type > -1)
                {
                    if (policyId > 0)
                    {
                        var policy = new PolicyRepository().FindById(policyId);
                        if (policy != null)
                        {
                            GenericRepository<PolicyClaimReason> policyClaimReasonRepository = new GenericRepository<PolicyClaimReason>();
                            GenericRepository<ClaimReason> claimReasonRepository = new GenericRepository<ClaimReason>();
                            GenericRepository<Policy> policyRepository = new GenericRepository<Policy>();

                            PolicyClaimReason lastPolicyClaimReason = policyClaimReasonRepository.Max($"POLICY_ID = {policyId}");
                            if (lastPolicyClaimReason != null && lastPolicyClaimReason.Id > 0)
                            {
                                lastPolicyClaimReason = policyClaimReasonRepository.FindById(lastPolicyClaimReason.Id);
                                if (lastPolicyClaimReason != null)
                                {
                                    long lastClaimReasonId = lastPolicyClaimReason.ClaimReasonId;

                                    ClaimReason lastClaimReason = claimReasonRepository.FindById(lastClaimReasonId);
                                    if (lastClaimReason != null)
                                    {
                                        lastClaimReason.EndDate = DateTime.Now;

                                        var spResponseLastClaimReason = claimReasonRepository.Update(lastClaimReason);
                                        if (spResponseLastClaimReason.Code != "100")
                                        {
                                            throw new Exception("Hasar Aç/Kapa son kaydı güncellenemedi!");
                                        }
                                    }
                                }
                            }

                            ClaimReason claimReason = new ClaimReason()
                            {
                                Id = 0,
                                Description = description,
                                StartDate = date,
                                ReasonId = reasonId,
                                Type = Convert.ToString(type)
                            };
                            var spResponseClaimReason = claimReasonRepository.Insert(claimReason);
                            if (spResponseClaimReason.Code != "100")
                            {
                                throw new Exception(spResponseClaimReason.Code + " : " + spResponseClaimReason.Message);
                            }
                            var claimReasonId = spResponseClaimReason.PkId;
                            claimReason.Id = claimReasonId;

                            PolicyClaimReason policyClaimReason = new PolicyClaimReason()
                            {
                                Id = 0,
                                PolicyId = policyId,
                                ClaimReasonId = claimReasonId
                            };
                            var spResponsePolicyClaimReason = policyClaimReasonRepository.Insert(policyClaimReason);
                            if (spResponsePolicyClaimReason.Code != "100")
                            {
                                throw new Exception(spResponsePolicyClaimReason.Code + " : " + spResponsePolicyClaimReason.Message);
                            }

                            policy.IsOpenToClaim = isOpen;
                            var spResponse = new PolicyRepository().Update(policy);
                            if (spResponse.Code == "100")
                            {
                                var responseChange = isOpen == "0" ? "Kapatıldı." : "Açıldı";
                                TempData["Alert"] = $"swAlert('İşlem Başarılı','{policy.No} lu Poliçe Hasara {responseChange}','success')";
                            }
                            else
                            {
                                throw new Exception(spResponse.Code + " : " + spResponse.Message);
                            }
                        }
                    }
                    else if (insuredId > 0)
                    {
                        var insured = new GenericRepository<Insured>().FindById(insuredId);
                        if (insured != null)
                        {
                            GenericRepository<InsuredClaimReason> insuredClaimReasonRepository = new GenericRepository<InsuredClaimReason>();
                            GenericRepository<ClaimReason> claimReasonRepository = new GenericRepository<ClaimReason>();
                            GenericRepository<Insured> insuredRepository = new GenericRepository<Insured>();

                            InsuredClaimReason lastInsuredClaimReason = insuredClaimReasonRepository.Max($"INSURED_ID = {insuredId}");
                            if (lastInsuredClaimReason != null && lastInsuredClaimReason.Id > 0)
                            {
                                lastInsuredClaimReason = insuredClaimReasonRepository.FindById(lastInsuredClaimReason.Id);
                                if (lastInsuredClaimReason != null)
                                {
                                    long lastClaimReasonId = lastInsuredClaimReason.ClaimReasonId;

                                    ClaimReason lastClaimReason = claimReasonRepository.FindById(lastClaimReasonId);
                                    if (lastClaimReason != null)
                                    {
                                        lastClaimReason.EndDate = DateTime.Now;

                                        var spResponseLastClaimReason = claimReasonRepository.Update(lastClaimReason);
                                        if (spResponseLastClaimReason.Code != "100")
                                        {
                                            throw new Exception("Hasar Aç/Kapa son kaydı güncellenemedi!");
                                        }
                                    }
                                }
                            }

                            ClaimReason claimReason = new ClaimReason()
                            {
                                Id = 0,
                                Description = description,
                                StartDate = date,
                                ReasonId = reasonId,
                                Type = Convert.ToString(type)
                            };
                            var spResponseClaimReason = claimReasonRepository.Insert(claimReason);
                            if (spResponseClaimReason.Code != "100")
                            {
                                throw new Exception(spResponseClaimReason.Code + " : " + spResponseClaimReason.Message);
                            }
                            var claimReasonId = spResponseClaimReason.PkId;
                            claimReason.Id = claimReasonId;

                            InsuredClaimReason insuredClaimReason = new InsuredClaimReason()
                            {
                                Id = 0,
                                InsuredId = insuredId,
                                ClaimReasonId = claimReasonId
                            };
                            var spResponsePolicyClaimReason = insuredClaimReasonRepository.Insert(insuredClaimReason);
                            if (spResponsePolicyClaimReason.Code != "100")
                            {
                                throw new Exception(spResponsePolicyClaimReason.Code + " : " + spResponsePolicyClaimReason.Message);
                            }

                            insured.IsOpenToClaim = isOpen;
                            var spResponse = new GenericRepository<Insured>().Update(insured);
                            if (spResponse.Code == "100")
                            {
                                var responseChange = isOpen == "0" ? "Kapatıldı." : "Açıldı";
                                TempData["Alert"] = $"swAlert('İşlem Başarılı','Seçilen Sigortalı Hasara {responseChange}','success')";
                            }
                            else
                            {
                                throw new Exception(spResponse.Code + " : " + spResponse.Message);
                            }
                        }
                    }
                }
                else
                {
                    throw new Exception("Hasar açma/kapama gerekçesini giriniz!");
                }
                #endregion
                return RedirectToAction("Policy");
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
                return RedirectToAction("Policy");
            }
        }

        [HttpPost]
        public JsonResult CalculateInstallment(FormCollection form)
        {
            object result = "[]";
            try
            {
                var policyId = form["hdPolicyId"];
                if (policyId.IsInt64() && long.Parse(policyId) > 0)
                {
                    var CASH_PAYMENT_TYPE = form["CASH_PAYMENT_TYPE"];
                    var VALUE = form["VALUE"].IsNumeric() ? form["VALUE"] : "0";
                    VALUE = VALUE.Replace('.', ',');
                    var INSTALLMENT_COUNT = form["INSTALLMENT_COUNT"];
                    var TOTAL = form["TOTAL_AMOUNT"];
                    decimal CASH_AMOUNT = 0;
                    if (!TOTAL.IsNumeric() || decimal.Parse(TOTAL) <= 0)
                    {
                        throw new Exception("Toplam Prim Bilgisi Sıfırdan Büyük Olmalı! Lütfen Sigortalı Girişi Yaptıktan Sonra Tekrar Deneyiniz...");
                    }

                    decimal AMOUNT = 0;
                    if (CASH_PAYMENT_TYPE == "0")
                    {
                        if (decimal.Parse(VALUE) > 100)
                        {
                            throw new Exception("Yüzde Tipinde Değer 100'den Büyük Olamaz!");
                        }
                        CASH_AMOUNT = ((decimal.Parse(TOTAL) * decimal.Parse(VALUE)) / 100);
                        AMOUNT = decimal.Parse(TOTAL) - ((decimal.Parse(TOTAL) * decimal.Parse(VALUE)) / 100);

                        if (AMOUNT == 0)
                        {
                            INSTALLMENT_COUNT = "0";
                        }

                        var policy = new GenericRepository<Policy>().FindById(long.Parse(policyId));
                        if (policy != null)
                        {
                            policy.InstallmentCount = INSTALLMENT_COUNT.IsNull() ? null : (int?)int.Parse(INSTALLMENT_COUNT);
                            policy.CashAmount = CASH_AMOUNT;
                            policy.CashPaymentType = ((int)CashPaymentType.YUZDE).ToString();
                            var spResponse = new GenericRepository<Policy>().Update(policy);
                        }
                    }
                    else
                    {
                        if (decimal.Parse(VALUE) > decimal.Parse(TOTAL))
                        {
                            throw new Exception("Tutar Tipinde Değer Toplam Tutardan Büyük Olamaz!");
                        }
                        CASH_AMOUNT = decimal.Parse(VALUE);
                        AMOUNT = (decimal.Parse(TOTAL) - decimal.Parse(VALUE));

                        if (AMOUNT == 0)
                        {
                            INSTALLMENT_COUNT = "0";
                        }

                        var policy = new GenericRepository<Policy>().FindById(long.Parse(policyId));
                        if (policy != null)
                        {
                            policy.InstallmentCount = INSTALLMENT_COUNT.IsNull() ? null : (int?)int.Parse(INSTALLMENT_COUNT);
                            policy.CashAmount = CASH_AMOUNT;
                            policy.CashPaymentType = ((int)CashPaymentType.TUTAR).ToString();
                            var spResponse = new GenericRepository<Policy>().Update(policy);
                            if (spResponse.Code != "100")
                            {
                                throw new Exception(spResponse.Message);
                            }
                        }
                    }

                    if (INSTALLMENT_COUNT.IsInt() && int.Parse(INSTALLMENT_COUNT) > 0)
                    {
                        var installmentList = new GenericRepository<Installment>().FindBy($"POLICY_ID = {policyId}");
                        foreach (var item in installmentList)
                        {
                            item.Status = ((int)Status.SILINDI).ToString();
                            new GenericRepository<Installment>().Update(item);
                        }
                        for (int i = 0; i < int.Parse(INSTALLMENT_COUNT); i++)
                        {
                            var installment = new Installment
                            {
                                PolicyId = long.Parse(policyId),
                                Amount = AMOUNT / int.Parse(INSTALLMENT_COUNT),
                                DueDate = DateTime.Now.AddMonths(i)
                            };
                            var spResponse = new GenericRepository<Installment>().Insert(installment);
                            if (spResponse.Code != "100")
                            {
                                throw new Exception(spResponse.Message);
                            }
                        }
                    }
                    result = CASH_AMOUNT;
                }
            }
            catch (Exception ex)
            {

            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetInstallment(FormCollection form)
        {
            object result = "[]";
            try
            {
                var policyId = form["hdPolicyId"];
                var duedateList = form["duedate"].Split(',');
                var amountList = form["amount"].Split(',');
                if (policyId.IsInt64() && long.Parse(policyId) > 0)
                {
                    var installmentList = new GenericRepository<Installment>().FindBy($"POLICY_ID = {policyId}");
                    foreach (var item in installmentList)
                    {
                        item.Status = ((int)Status.SILINDI).ToString();
                        new GenericRepository<Installment>().Update(item);
                    }
                    for (int i = 0; i < duedateList.Length; i++)
                    {
                        if (amountList[i].IsNumeric() && duedateList[i].IsDateTime())
                        {
                            var installment = new Installment
                            {
                                PolicyId = long.Parse(policyId),
                                Amount = decimal.Parse(amountList[i].Replace('.', ',')),
                                DueDate = DateTime.Parse(duedateList[i])
                            };
                            var spResponse = new GenericRepository<Installment>().Insert(installment);
                            if (spResponse.Code != "100")
                            {
                                throw new Exception(spResponse.Message);
                            }
                        }
                    }
                    result = "[]";
                }
            }
            catch (Exception)
            {
                result = "[]";
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult PolicyFormSave(FormCollection form)
        {
            var result = new AjaxResultDto<PolicySaveStep1Result>();

            try
            {
                result.Data = new PolicySaveStep1Result();
                // policy service
                ServiceResponse response = new ServiceResponse();
                PolicyDto policyDto = new PolicyDto();
                IService<PolicyDto> policyService = new PolicyService();

                #region T_POLICY
                var policyId = form["hdPolicyId"];
                if (!Extensions.IsInt64(policyId))
                {
                    throw new Exception("Poliçe Kaydedilirken Bir Hata Oluştu Lütfen Tekrar Deneyiniz.");
                }
                else if (Extensions.IsNull(policyId) || long.Parse(policyId) <= 0)
                {
                    policyDto.PolicyId = 0;
                }
                else
                {
                    var policy = new GenericRepository<Policy>().FindById(long.Parse(policyId));
                    if (policy == null)
                    {
                        throw new Exception("Güncellenecek Poliçe Bilgisi Bulunamadı. Lütfen Tekrar Deneyiniz.");
                    }
                    else
                    {
                        if (policy.Status == ((int)PolicyStatus.TANZIMLI).ToString())
                        {
                            throw new Exception("Tanzim Edilen Poliçe Üzerinde Değişiklik Yapılamaz. Zeyl Oluşturarak İstenilen Değişiklikler Yapılabilir.");
                        }
                    }
                    policyDto.PolicyId = long.Parse(policyId);
                }

                var subProductId = form["subProduct"];
                if (Extensions.IsNull(subProductId) || !Extensions.IsInt64(subProductId) || long.Parse(subProductId) < 1)
                {
                    policyDto.SubProductId = null;
                }
                else
                {
                    policyDto.SubProductId = long.Parse(subProductId);
                }

                var agencyId = form["agency"]; //acente
                if (!Extensions.IsInt64(agencyId) || long.Parse(agencyId) < 1)
                {
                    throw new Exception("Acente Seçilmesi Gerekmektedir!!!");
                }
                else
                {
                    policyDto.AgencyId = agencyId;
                }

                var insurerId = form["hdInsurerContactId"]; // sigorta ettiren
                if (!Extensions.IsInt64(insurerId) || long.Parse(insurerId) < 1)
                {
                    throw new Exception("Sigorta Ettiren Bilgisi Gerekmektedir!!!");
                }
                else
                {
                    policyDto.InsurerId = long.Parse(insurerId);
                }

                var ParentId = form["hdConnPolicyID"]; // bağlı bulundugu poliçe
                if (Extensions.IsNull(ParentId) || !Extensions.IsInt64(ParentId) || long.Parse(ParentId) < 1)
                {
                    policyDto.PolicyParentId = null;
                }
                else
                {
                    policyDto.PolicyParentId = long.Parse(ParentId);
                }

                var pGroup = form["policyGroup"]; // poliçe grubu
                if (Extensions.IsNull(pGroup) || !Extensions.IsInt64(pGroup) || long.Parse(pGroup) < 1)
                {
                    policyDto.PolicyGroupId = null;
                }
                else
                {
                    policyDto.PolicyGroupId = long.Parse(pGroup);
                }

                var prevPolicyID = form["hdPrevPolicyID"]; // yenileme poliçesi ise önceki poliçe no 
                if (Extensions.IsNull(prevPolicyID) || !Extensions.IsInt64(prevPolicyID) || long.Parse(prevPolicyID) < 1)
                {
                    policyDto.PolicyPreviousId = null;
                }
                else
                {
                    policyDto.PolicyPreviousId = long.Parse(prevPolicyID);
                }

                var pRenewalNo = form["hdpolicyRenewalNo"]; //poliçe no
                if (!Extensions.IsInt(pRenewalNo))
                {
                    throw new Exception("Yenileme No Kaydedilirken Bir Hata Oluştu Lütfen Tekrar Deneyiniz.");
                }
                if (Extensions.IsNull(pRenewalNo) || int.Parse(pRenewalNo) < 0)
                {
                    throw new Exception("Yenileme No Girmeniz Gerekmektedir.");
                }
                else
                {
                    policyDto.RenewalNo = int.Parse(pRenewalNo);
                }

                var pType = form["policyType"]; //poliçe tipi
                if (!Extensions.IsInt(pType))
                {
                    throw new Exception("Poliçe Tipi Kaydedilirken Bir Hata Oluştu Lütfen Tekrar Deneyiniz.");
                }
                if (Extensions.IsNull(pType) || int.Parse(pType) < 0)
                {
                    throw new Exception("Poliçe Tipi Seçmeniz Gerekmektedir.");
                }
                else
                {
                    policyDto.PolicyType = pType;
                }

                var EndDate = form["policyCreateEndDate"];

                if (Extensions.IsNull(EndDate) || !Extensions.IsDateTime(EndDate))
                {
                    policyDto.EndDate = null;
                }
                else
                {
                    policyDto.EndDate = DateTime.Parse(EndDate);
                }

                policyDto.PolicyPaymentType = form["PAYMENT_TYPE"].IsInt() ? form["PAYMENT_TYPE"] : ((int)PaymentType.NAKIT).ToString();

                policyDto.SbmPoliceNo = Extensions.IsNull(form["policySbmNo"]) ? null : form["policySbmNo"]; // SBM no

                policyDto.LocationType = Extensions.IsNull(form["LOCATION_TYPE"]) ? null : form["LOCATION_TYPE"]; // baglı bulundugu locasyon

                policyDto.PolicyNo = Extensions.IsNull(form["policyCreateNo"]) ? null : form["policyCreateNo"]; //poliçe No

                policyDto.ExchangeRateId = form["hdExchangeRateId"].IsInt64() ? (long?)long.Parse(form["hdExchangeRateId"]) : null;

                policyDto.isPolicyChange = true;
                #endregion
                bool isNew = false;
                if (policyDto.PolicyId == 0)
                {
                    isNew = true;
                }

                response = policyService.ServiceInsert(policyDto, Token: "");
                if (response.IsSuccess)
                {
                    result.Data.policyId = response.Id;

                    if (isNew)
                    {
                        #region T_ENDORSEMENT

                        policyDto.EndorsementNo = 0;

                        policyDto.EndorsementType = ((int)EndorsementType.BASLANGIC).ToString();

                        policyDto.EndorsementStartDate = string.IsNullOrEmpty(form["StartDate"]) ? null : (DateTime?)DateTime.Parse(form["StartDate"]);

                        policyDto.isEndorsementChange = true;
                        policyDto.isPolicyChange = false;

                        response = policyService.ServiceInsert(policyDto, Token: "");
                        if (!response.IsSuccess)
                        {
                            throw new Exception(response.Code + " : " + response.Message);
                        }
                        else
                        {
                            result.Data.endorsementId = response.Id;
                        }
                        #endregion
                    }
                    else
                    {
                        Endorsement endorsement = new GenericRepository<Endorsement>().FindBy($"POLICY_ID={result.Data.policyId} AND NO=0 AND TYPE='{((int)EndorsementType.BASLANGIC).ToString()}'").FirstOrDefault();
                        endorsement.StartDate = string.IsNullOrEmpty(form["StartDate"]) ? null : (DateTime?)DateTime.Parse(form["StartDate"]);
                        SpResponse spResponse = new GenericRepository<Endorsement>().Update(endorsement);
                    }

                    result.ResultCode = response.Code;
                    result.ResultMessage = response.Message;
                    //TempData["Alert"] = $"swAlert('İşlem Başarılı','{spResponsePolicy.Data.No} Başarıyla Kaydedildi.','success')";
                }
                else
                {
                    throw new Exception(response.Code + " : " + response.Message);
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PolicySaveStep5(FormCollection form)
        {
            var result = new AjaxResultDto<ProductSaveStep4Result>();
            ProductSaveStep4Result step1Result = new ProductSaveStep4Result();
            var rules = form["hdRuleSpecial"];
            try
            {
                var policyId = form["hdPolicyId"];
                var policyGroupId = form["hdPolicyGroupId"];
                var productId = form["hdProductId"];
                var subproductId = form["hdSubproductId"];
                List<dynamic> ruleList = new List<dynamic>();
                ruleList = JsonConvert.DeserializeObject<List<dynamic>>(rules);
                foreach (var rule in ruleList)
                {
                    if (rule.isOpen == "1")
                    {
                        if (rule.STATUS == "0")
                        {
                            string idList = string.Empty;

                            if (rule.chkRuleSelectedId != null)
                            {
                                idList = rule.chkRuleSelectedId.ToString();

                                foreach (var ruleId in idList.Replace("\r", "").Replace("\n", "").Replace("[", string.Empty).Replace("]", "").Replace("\"", "").Replace(" ", "").Split(','))
                                {
                                    var TruleSubProduct = new RulePolicy
                                    {
                                        Id = rule.RULE_POLICY_ID,
                                        PolicyId = long.Parse(policyId),
                                        RuleId = long.Parse(ruleId),
                                        Status = Convert.ToString(rule.STATUS),
                                        Type = Convert.ToString(rule.RULE_TYPE)
                                    };
                                    var spResponse = new GenericRepository<RulePolicy>().Insert(TruleSubProduct);
                                    if (spResponse.Code == "100")
                                    {
                                        result.ResultCode = spResponse.Code;
                                        result.ResultMessage = spResponse.Message;
                                    }
                                    else
                                    {
                                        throw new Exception(spResponse.Message);
                                    }
                                }
                            }
                            else
                            {
                                idList = rule.chkRuleGroupSelectedId.ToString();

                                foreach (var ruleGroupId in idList.Replace("\r", "").Replace("\n", "").Replace("[", string.Empty).Replace("]", "").Replace("\"", "").Replace(" ", "").Split(','))
                                {
                                    List<RuleGroupRule> ruleGroupRuleList = new GenericRepository<RuleGroupRule>().FindBy($"RULE_GROUP_ID={ruleGroupId}");
                                    if (ruleGroupRuleList.Any())
                                    {
                                        foreach (var ruleGroupRule in ruleGroupRuleList)
                                        {
                                            var TruleSubProduct = new RulePolicy
                                            {
                                                Id = rule.RULE_POLICY_ID,
                                                PolicyId = long.Parse(policyId),
                                                RuleId = ruleGroupRule.RuleId,
                                                RuleGroupId = ruleGroupRule.RuleGroupId,
                                                Status = Convert.ToString(rule.STATUS),
                                                Type = Convert.ToString(rule.RULE_TYPE)
                                            };
                                            var spResponse = new GenericRepository<RulePolicy>().Insert(TruleSubProduct);
                                            if (spResponse.Code == "100")
                                            {
                                                result.ResultCode = spResponse.Code;
                                                result.ResultMessage = spResponse.Message;
                                            }
                                            else
                                            {
                                                throw new Exception(spResponse.Message);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            RulePolicy ruleProduct = new GenericRepository<RulePolicy>().FindById(long.Parse(Convert.ToString(rule.RULE_POLICY_ID)));
                            if (ruleProduct != null)
                            {
                                ruleProduct.Status = ((int)Status.SILINDI).ToString();
                                var spResponse = new GenericRepository<RulePolicy>().Update(ruleProduct);
                                if (spResponse.Code == "100")
                                {
                                    result.ResultCode = spResponse.Code;
                                    result.ResultMessage = spResponse.Message;
                                }
                                else
                                {
                                    throw new Exception(spResponse.Message);
                                }
                            }
                        }
                    }
                }

                var ruleSpecialList = new GenericRepository<V_RulePolicy>().FindBy("POLICY_ID=:id AND TYPE=:type", orderby: "POLICY_ID", parameters: new { id = policyId, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });

                List<dynamic> ruleDynamicSpecialList = new List<dynamic>();
                int specialId = 1;

                if (ruleSpecialList != null)
                {
                    foreach (var item in ruleSpecialList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = specialId;
                        listItem.ClientId = specialId;
                        listItem.STATUS = item.STATUS;

                        listItem.RULE_POLICY_ID = Convert.ToString(item.RULE_POLICY_ID);
                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                        listItem.RULE_NAME = item.RULE_NAME;
                        listItem.RESULT = item.RESULT;
                        listItem.INHERITED_NAME = string.Empty;
                        listItem.INHERITED_CODE = string.Empty;
                        listItem.isOpen = "0";

                        listItem.IsInherited = "1";

                        ruleDynamicSpecialList.Add(listItem);

                        specialId++;
                    }
                }

                var ruleSubProductSpecialList = new GenericRepository<V_RuleSubProduct>().FindBy("SUBPRODUCT_ID=:SUBPRODUCT_ID AND TYPE=:type", orderby: "SUBPRODUCT_ID", parameters: new { SUBPRODUCT_ID = subproductId, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });

                if (ruleSubProductSpecialList != null)
                {
                    foreach (var item in ruleSubProductSpecialList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = specialId;
                        listItem.ClientId = specialId;
                        listItem.STATUS = item.STATUS;

                        listItem.RULE_SUBPRODUCT_ID = Convert.ToString(item.RULE_SUBPRODUCT_ID);
                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                        listItem.RULE_NAME = item.RULE_NAME;
                        listItem.RESULT = item.RESULT;
                        listItem.INHERITED_NAME = "AltÜrün";
                        listItem.INHERITED_CODE = Convert.ToString(item.SUBPRODUCT_CODE);
                        listItem.isOpen = "0";
                        listItem.IsInherited = "0";

                        ruleDynamicSpecialList.Add(listItem);

                        specialId++;
                    }
                }

                var ruleProductSpecialList = new GenericRepository<V_RuleProduct>().FindBy("PRODUCT_ID=:PRODUCT_ID AND TYPE=:type", orderby: "PRODUCT_ID", parameters: new { PRODUCT_ID = productId, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });
                if (ruleProductSpecialList != null)
                {

                    foreach (var item in ruleProductSpecialList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = specialId;
                        listItem.ClientId = specialId;
                        listItem.STATUS = item.STATUS;

                        listItem.RULE_PRODUCT_ID = Convert.ToString(item.RULE_PRODUCT_ID);
                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                        listItem.RULE_NAME = item.RULE_NAME;
                        listItem.RESULT = item.RESULT;

                        listItem.INHERITED_NAME = "Ürün";
                        listItem.INHERITED_CODE = Convert.ToString(item.PRODUCT_CODE);
                        listItem.isOpen = "0";
                        listItem.IsInherited = "0";

                        ruleDynamicSpecialList.Add(listItem);

                        specialId++;
                    }
                }

                if (policyGroupId.IsInt64())
                {
                    var rulePolicyGroupExtraList = new GenericRepository<V_RulePolicyGroup>().FindBy("POLICY_GROUP_ID=:POLICY_GROUP_ID AND TYPE=:type", orderby: "POLICY_GROUP_ID", parameters: new { POLICY_GROUP_ID = policyGroupId, type = new DbString { Value = ((int)RuleType.OZEL_SART).ToString(), Length = 3 } });

                    if (rulePolicyGroupExtraList != null)
                    {
                        foreach (var item in rulePolicyGroupExtraList)
                        {
                            dynamic listItem = new System.Dynamic.ExpandoObject();

                            listItem.id = specialId;
                            listItem.ClientId = specialId;
                            listItem.STATUS = item.STATUS;

                            listItem.RULE_POLICY_GROUP_ID = Convert.ToString(item.RULE_POLICY_GROUP_ID);
                            listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                            listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                            listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                            listItem.RULE_NAME = item.RULE_NAME;
                            listItem.RESULT = item.RESULT;
                            listItem.INHERITED_NAME = "Poliçe Grubu";
                            listItem.INHERITED_CODE = Convert.ToString(item.POLICY_GROUP_NAME);
                            listItem.isOpen = "0";
                            listItem.IsInherited = "0";

                            ruleDynamicSpecialList.Add(listItem);

                            specialId++;
                        }
                    }
                }

                result.Data = new ProductSaveStep4Result
                {
                    jsonData = ruleDynamicSpecialList.ToJSON()
                };
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PolicySaveStep6(FormCollection form)
        {
            var result = new AjaxResultDto<ProductSaveStep4Result>();
            ProductSaveStep4Result step1Result = new ProductSaveStep4Result();
            var rules = form["hdRuleExtra"];
            try
            {
                var policyId = form["hdPolicyId"];
                var policyGroupId = form["hdPolicyGroupId"];
                var productId = form["hdProductId"];
                var subproductId = form["hdSubproductId"];
                List<dynamic> ruleList = new List<dynamic>();
                ruleList = JsonConvert.DeserializeObject<List<dynamic>>(rules);
                foreach (var rule in ruleList)
                {
                    if (rule.isOpen == "1")
                    {
                        if (rule.STATUS == "0")
                        {
                            string idList = string.Empty;

                            if (rule.chkRuleSelectedId != null)
                            {
                                idList = rule.chkRuleSelectedId.ToString();

                                foreach (var ruleId in idList.Replace("\r", "").Replace("\n", "").Replace("[", string.Empty).Replace("]", "").Replace("\"", "").Replace(" ", "").Split(','))
                                {
                                    var TruleSubProduct = new RulePolicy
                                    {
                                        Id = rule.RULE_POLICY_ID,
                                        PolicyId = long.Parse(policyId),
                                        RuleId = long.Parse(ruleId),
                                        Status = Convert.ToString(rule.STATUS),
                                        Type = Convert.ToString(rule.RULE_TYPE)
                                    };
                                    var spResponse = new GenericRepository<RulePolicy>().Insert(TruleSubProduct);
                                    if (spResponse.Code == "100")
                                    {
                                        result.ResultCode = spResponse.Code;
                                        result.ResultMessage = spResponse.Message;
                                    }
                                    else
                                    {
                                        throw new Exception(spResponse.Message);
                                    }
                                }
                            }
                            else
                            {
                                idList = rule.chkRuleGroupSelectedId.ToString();

                                foreach (var ruleGroupId in idList.Replace("\r", "").Replace("\n", "").Replace("[", string.Empty).Replace("]", "").Replace("\"", "").Replace(" ", "").Split(','))
                                {
                                    List<RuleGroupRule> ruleGroupRuleList = new GenericRepository<RuleGroupRule>().FindBy($"RULE_GROUP_ID={ruleGroupId}");
                                    if (ruleGroupRuleList.Any())
                                    {
                                        foreach (var ruleGroupRule in ruleGroupRuleList)
                                        {
                                            var TruleSubProduct = new RulePolicy
                                            {
                                                Id = rule.RULE_POLICY_ID,
                                                PolicyId = long.Parse(policyId),
                                                RuleId = ruleGroupRule.RuleId,
                                                RuleGroupId = ruleGroupRule.RuleGroupId,
                                                Status = Convert.ToString(rule.STATUS),
                                                Type = Convert.ToString(rule.RULE_TYPE)
                                            };
                                            var spResponse = new GenericRepository<RulePolicy>().Insert(TruleSubProduct);
                                            if (spResponse.Code == "100")
                                            {
                                                result.ResultCode = spResponse.Code;
                                                result.ResultMessage = spResponse.Message;
                                            }
                                            else
                                            {
                                                throw new Exception(spResponse.Message);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            RulePolicy ruleProduct = new GenericRepository<RulePolicy>().FindById(long.Parse(Convert.ToString(rule.RULE_POLICY_ID)));
                            if (ruleProduct != null)
                            {
                                ruleProduct.Status = ((int)Status.SILINDI).ToString();
                                var spResponse = new GenericRepository<RulePolicy>().Update(ruleProduct);
                                if (spResponse.Code == "100")
                                {
                                    result.ResultCode = spResponse.Code;
                                    result.ResultMessage = spResponse.Message;
                                }
                                else
                                {
                                    throw new Exception(spResponse.Message);
                                }
                            }
                        }
                    }
                }

                var ruleExtraList = new GenericRepository<V_RulePolicy>().FindBy("POLICY_ID=:id AND TYPE=:type", orderby: "POLICY_ID", parameters: new { id = policyId, type = new DbString { Value = ((int)RuleType.EK_PROTOKOL).ToString(), Length = 3 } });

                List<dynamic> ruleDynamicExtraList = new List<dynamic>();
                int extraId = 1;

                if (ruleExtraList != null)
                {
                    foreach (var item in ruleExtraList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = extraId;
                        listItem.ClientId = extraId;
                        listItem.STATUS = item.STATUS;

                        listItem.RULE_POLICY_ID = Convert.ToString(item.RULE_POLICY_ID);
                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                        listItem.RULE_NAME = item.RULE_NAME;
                        listItem.RESULT = item.RESULT;
                        listItem.INHERITED_NAME = string.Empty;
                        listItem.INHERITED_CODE = string.Empty;
                        listItem.isOpen = "0";
                        listItem.IsInherited = "1";

                        ruleDynamicExtraList.Add(listItem);

                        extraId++;
                    }
                }

                var ruleSubProductExtraList = new GenericRepository<V_RuleSubProduct>().FindBy("SUBPRODUCT_ID=:SUBPRODUCT_ID AND TYPE=:type", orderby: "SUBPRODUCT_ID", parameters: new { SUBPRODUCT_ID = subproductId, type = new DbString { Value = ((int)RuleType.EK_PROTOKOL).ToString(), Length = 3 } });

                if (ruleSubProductExtraList != null)
                {

                    foreach (var item in ruleSubProductExtraList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = extraId;
                        listItem.ClientId = extraId;
                        listItem.STATUS = item.STATUS;

                        listItem.RULE_SUBPRODUCT_ID = Convert.ToString(item.RULE_SUBPRODUCT_ID);
                        listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                        listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                        listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                        listItem.RULE_NAME = item.RULE_NAME;
                        listItem.RESULT = item.RESULT;
                        listItem.INHERITED_NAME = "AltÜrün";
                        listItem.INHERITED_CODE = Convert.ToString(item.SUBPRODUCT_CODE);
                        listItem.isOpen = "0";
                        listItem.IsInherited = "0";

                        ruleDynamicExtraList.Add(listItem);

                        extraId++;
                    }
                }

                if (policyGroupId.IsInt64())
                {
                    var rulePolicyGroupExtraList = new GenericRepository<V_RulePolicyGroup>().FindBy("POLICY_GROUP_ID=:POLICY_GROUP_ID AND TYPE=:type", orderby: "POLICY_GROUP_ID", parameters: new { POLICY_GROUP_ID = policyGroupId, type = new DbString { Value = ((int)RuleType.EK_PROTOKOL).ToString(), Length = 3 } });

                    if (rulePolicyGroupExtraList != null)
                    {
                        foreach (var item in rulePolicyGroupExtraList)
                        {
                            dynamic listItem = new System.Dynamic.ExpandoObject();

                            listItem.id = extraId;
                            listItem.ClientId = extraId;
                            listItem.STATUS = item.STATUS;

                            listItem.RULE_POLICY_GROUP_ID = Convert.ToString(item.RULE_POLICY_GROUP_ID);
                            listItem.RULE_ID = Convert.ToString(item.RULE_ID);
                            listItem.RULE_GROUP = Convert.ToString(item.RULE_GROUP_NAME);
                            listItem.RULE_CATEGORY = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                            listItem.RULE_NAME = item.RULE_NAME;
                            listItem.RESULT = item.RESULT;
                            listItem.INHERITED_NAME = "Poliçe Grubu";
                            listItem.INHERITED_CODE = Convert.ToString(item.POLICY_GROUP_NAME);
                            listItem.isOpen = "0";
                            listItem.IsInherited = "0";

                            ruleDynamicExtraList.Add(listItem);

                            extraId++;
                        }
                    }
                }

                result.Data = new ProductSaveStep4Result
                {
                    jsonData = ruleDynamicExtraList.ToJSON()
                };
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Endorsement
        [LoginControl]
        public ActionResult EndorsementForm(Int64 id = 0, string formaction = "")
        {
            try
            {
                ViewBag.PolicyId = Request["PolicyId"];
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                #region Lookup Fill
                var EndorsementCategoryList = LookupHelper.GetLookupData(LookupTypes.EndorsementCategory);
                ViewBag.EndorsementCategoryList = EndorsementCategoryList;

                var EndorsementTypeDescriptionTypeList = LookupHelper.GetLookupData(LookupTypes.EndorsementTypeDescription);
                ViewBag.EndorsementTypeDescriptionTypeList = EndorsementTypeDescriptionTypeList;

                var CurrencyTypes = LookupHelper.GetLookupData(Constants.LookupTypes.Currency);
                ViewBag.CurrencyTypes = CurrencyTypes;

                var EndorsementTypeList = LookupHelper.GetLookupData(LookupTypes.Endorsement);
                ViewBag.EndorsementTypeList = EndorsementTypeList;

                var LocationTypeList = LookupHelper.GetLookupData(LookupTypes.Location);
                ViewBag.LocationTypeList = LocationTypeList;

                var GenderList = LookupHelper.GetLookupData(LookupTypes.Gender);
                ViewBag.GenderList = GenderList;

                var ContactTypeList = LookupHelper.GetLookupData(LookupTypes.Contact);
                ViewBag.ContactTypeList = ContactTypeList;

                var IndividualTypeList = LookupHelper.GetLookupData(LookupTypes.Individual);
                ViewBag.IndividualTypeList = IndividualTypeList;
                #endregion

                #region Table List
                var CityList = new CityRepository().FindBy();
                ViewBag.CityList = CityList;

                var CountryList = new CountryRepository().FindBy();
                ViewBag.CountryList = CountryList;

                var policy = new GenericRepository<V_Policy>().FindBy("POLICY_ID=:policyId", orderby: "POLICY_ID", parameters: new { policyId = ViewBag.PolicyId }).FirstOrDefault();

                if (policy == null)
                {
                    TempData["Alert"] = $"swAlert('Hata','Poliçe Bilgisi Bulunamadı.','warning')";
                    return RedirectToAction("Policy");
                }

                ViewBag.SelectAgency = policy.AGENCY_ID;
                if (policy.SUBPRODUCT_ID != null)
                {
                    var PackageList = new GenericRepository<V_Package>().FindBy("SUBPRODUCT_ID=:SUBPRODUCT_ID", orderby: "PACKAGE_NAME", parameters: new { policy.SUBPRODUCT_ID });

                    List<dynamic> packageList = new List<dynamic>();

                    if (PackageList != null)
                    {

                        foreach (var item in PackageList)
                        {
                            dynamic listItem = new System.Dynamic.ExpandoObject();

                            listItem.PACKAGE_NAME = item.PACKAGE_NAME;
                            listItem.PACKAGE_ID = Convert.ToString(item.PACKAGE_ID);

                            packageList.Add(listItem);
                        }

                    }
                    ViewBag.PackageList = packageList;
                }

                var Insureds = new GenericRepository<V_Insured>().FindBy("POLICY_ID=:policyId", orderby: "INSURED_ID", parameters: new { policyId = ViewBag.PolicyId });
                ViewBag.InsuredList = Insureds;

                var agencyList = new GenericRepository<Agency>().FindBy("COMPANY_ID=COMPANY_ID", orderby: "COMPANY_ID", parameters: new { policy.COMPANY_ID });
                ViewBag.AgencyList = agencyList;
                #endregion

                ViewBag.isView = false;

                ViewBag.isEdit = false;

                switch (formaction.ToLower())
                {
                    case "edit":
                    case "view":
                        if (id > 0)
                        {
                            var endorsement = new GenericRepository<Endorsement>().FindById(id);
                            if (endorsement != null)
                            {
                                ViewBag.Endorsement = endorsement;

                                if (endorsement.Type == ((int)EndorsementType.SE_DEGISIKLIGI).ToString())
                                {
                                    ViewBag.InsurerTitle = policy.INSURER_NAME;
                                    ViewBag.InsurerContactId = policy.INSURER;
                                }
                                else if (endorsement.Type == ((int)EndorsementType.POLICE_TAHAKKUK_ZEYLI).ToString() || endorsement.Type == ((int)EndorsementType.PRIM_FARKI_ZEYLI).ToString())
                                {
                                    ViewBag.TAX = policy.TAX;
                                    ViewBag.PREMIUM = policy.PREMIUM;

                                    decimal total = 0;
                                    if (!string.IsNullOrEmpty(Convert.ToString(policy.TAX)))
                                    {
                                        total += decimal.Parse(Convert.ToString(policy.TAX));
                                    }
                                    if (!string.IsNullOrEmpty(Convert.ToString(policy.PREMIUM)))
                                    {
                                        total += decimal.Parse(Convert.ToString(policy.PREMIUM));
                                    }
                                    if (!string.IsNullOrEmpty(Convert.ToString(policy.FOREX_SELLING)))
                                    {
                                        total *= decimal.Parse(Convert.ToString(policy.FOREX_SELLING));
                                    }

                                    ViewBag.TOTAL_AMOUNT = total;
                                }
                                else if (endorsement.Type == ((int)EndorsementType.BASLANGIC_BITIS_TARIHI).ToString())
                                {
                                    string sDate = Convert.ToString(policy.POLICY_START_DATE);
                                    ViewBag.START_DATE = sDate.IsDateTime() ? DateTime.Parse(sDate).ToString("dd-MM-yyyy") : null;

                                    string eDate = Convert.ToString(policy.POLICY_END_DATE);
                                    ViewBag.END_DATE = eDate.IsDateTime() ? DateTime.Parse(eDate).ToString("dd-MM-yyyy") : null;
                                }
                            }
                            if (formaction.ToLower() == "view" || endorsement.Status == ((int)Status.AKTIF).ToString())
                            {
                                ViewBag.isView = true;
                                ViewBag.Title = "Zeyl Görüntüle";
                            }
                            else
                            {
                                ViewBag.isEdit = true;
                                ViewBag.Title = "Zeyl Güncelle";
                            }
                        }
                        break;
                    default:
                        ViewBag.Title = "Zeyl Ekle";
                        break;
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return View();
        }

        [LoginControl]
        public ActionResult EndorsementDelete(Int64 id)
        {
            string policyId = null;
            try
            {
                var repo = new GenericRepository<Endorsement>();
                var result = repo.FindById(id);
                if (result == null)
                {
                    throw new Exception("Zeyl Bilgisi Bulunamadı!!!");
                }
                else if (result.Status == ((int)Status.AKTIF).ToString())
                {
                    throw new Exception("Tanzim Edilmiş Zeyl Silinemez!!!");
                }
                policyId = result.PolicyId.ToString();
                Policy policy = new GenericRepository<Policy>().FindById(long.Parse(policyId));
                if (policy == null)
                {
                    throw new Exception("Poliçe Bilgisi Bulunamadı!!!");
                }

                if (result.Type == ((int)EndorsementType.SIGORTALI_CIKISI).ToString())
                {
                    var insuredList = new GenericRepository<Insured>().FindBy($"LAST_ENDORSEMENT_ID={result.Id} AND STATUS IN('{((int)Status.SILINDI)}','{((int)Status.PASIF)}')", fetchDeletedRows: true);
                    if (insuredList != null)
                    {
                        foreach (var insured in insuredList)
                        {
                            insured.Status = ((int)Status.AKTIF).ToString();
                            insured.TotalPremium = insured.InitialPremium == null ? 0 : insured.InitialPremium;
                            new GenericRepository<Insured>().Update(insured);
                        }
                    }
                }

                result.Status = ((int)Status.SILINDI).ToString();
                var spResponse = repo.Update(result);
                if (spResponse.Code != "100")
                {
                    throw new Exception(spResponse.Code + " - " + spResponse.Message);
                }

                policy.LastEndorsementId = new GenericRepository<Endorsement>().FindBy($"POLICY_ID={policyId}", orderby: "NO DESC").FirstOrDefault()?.Id;
                new GenericRepository<Policy>().Update(policy);

                TempData["Alert"] = $"swAlert('İşlem Başarılı','Silme İşlemi Başarıyla Gerçekleştirildi.','success')";
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            if (policyId.IsNull())
                return RedirectToAction("Policy");
            else
                return RedirectToAction("Endorsement/" + policyId);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult EndorsementSave(FormCollection form)
        {
            try
            {
                var endorsementId = form["hdEndorsementId"];

                ServiceResponse response = new ServiceResponse();
                PolicyDto policyDto = new PolicyDto();
                IService<PolicyDto> policyService = new PolicyService();

                policyDto.isEndorsementChange = true;


                if (!Extensions.IsInt64(endorsementId) || long.Parse(endorsementId) < 0)
                {
                    policyDto.EndorsementId = 0;
                }
                else
                {
                    policyDto.EndorsementId = long.Parse(endorsementId);
                }

                var policyId = form["hdPolicyId"];
                if (!Extensions.IsInt64(policyId) || long.Parse(policyId) < 0)
                {
                    throw new Exception("Zeyl Bağlı Bulunduğu Poliçe Bulunamadı. Lütfen Tekrar Deneyiniz...");
                }
                else
                {
                    policyDto.PolicyId = long.Parse(policyId);
                }

                var CATEGORY = form["CATEGORY"];
                if (Extensions.IsNull(CATEGORY))
                {
                    throw new Exception("Zeyl Kategorisi Seçilmelidir.");
                }
                else
                {
                    policyDto.EndorsementCategory = CATEGORY;
                }

                var TYPE_DESCRIPTION_TYPE = form["TYPE_DESCRIPTION_TYPE"];
                if (Extensions.IsNull(TYPE_DESCRIPTION_TYPE))
                {
                    throw new Exception("Zeyl Tipinin Türü Seçilmelidir.");
                }
                else
                {
                    policyDto.EndorsementTypeDescriptionType = TYPE_DESCRIPTION_TYPE;
                }

                policyDto.EndorsementTypeDescription = form["TYPE_DESCRIPTION"];

                var NO = form["NO"];
                if (!Extensions.IsInt(NO) || int.Parse(NO) < 0)
                {
                    throw new Exception("Zeyl No Bilgisi Girilmelidir.");
                }
                else
                {
                    policyDto.EndorsementNo = int.Parse(NO);
                }

                var TYPE = form["TYPE"];
                if (Extensions.IsNull(TYPE))
                {
                    throw new Exception("Zeyl Tipi Seçilmelidir.");
                }
                else
                {
                    if (TYPE == ((int)EndorsementType.GIDECEGI_ULKE_DEGISIKLIGI).ToString())
                    {
                        V_Policy polcy = new GenericRepository<V_Policy>().FindBy($"POLICY_ID={policyDto.PolicyId}", orderby: "POLICY_ID").FirstOrDefault();
                        if (polcy.BRANCH_ID != 2)
                        {
                            throw new Exception("Seyahat Poliçesi Dışındaki Poliçelere Coğrafi Kapsam Değişikliği Zeyl Eklenemez...");
                        }
                    }
                    policyDto.EndorsementType = TYPE;
                }

                policyDto.EndorsementStartDate = string.IsNullOrEmpty(form["START_DATE"]) ? null : (DateTime?)DateTime.Parse(form["START_DATE"]);

                policyDto.DateOfIssue = string.IsNullOrEmpty(form["DATE_OF_ISSUE"]) ? null : (DateTime?)DateTime.Parse(form["DATE_OF_ISSUE"]);

                policyDto.EndorsementRegistrationDate = string.IsNullOrEmpty(form["REGISTRATION_DATE"]) ? null : (DateTime?)DateTime.Parse(form["REGISTRATION_DATE"]);

                response = policyService.ServiceInsert(policyDto, Token: "");
                if (!response.IsSuccess)
                {
                    throw new Exception(response.Code + " : " + response.Message);
                }
                else
                {
                    endorsementId = response.Id.ToString();
                    var policy = new GenericRepository<Policy>().FindById(policyDto.PolicyId);

                    if (policyDto.EndorsementType == ((int)EndorsementType.URETIM_KAYNAGI_DEGISIKLIGI).ToString())
                    {
                        if (policy != null && form["agency"].IsInt64())
                        {
                            policy.AgencyId = form["agency"].IsInt64() ? (long?)long.Parse(form["agency"]) : null;
                            policy.LastEndorsementId = long.Parse(endorsementId);
                            SpResponse spResponse = new GenericRepository<Policy>().Update(policy);
                            if (spResponse.Code != "100")
                            {
                                throw new Exception("Acente Değişikliği Gerçekleştirilemedi. Lütfen Tekrar Deneyiniz...");
                            }
                        }
                    }
                    else if (policyDto.EndorsementType == ((int)EndorsementType.SE_DEGISIKLIGI).ToString())
                    {
                        if (policy != null && form["hdInsurerContactId"].IsInt64())
                        {
                            policy.Insurer = form["hdInsurerContactId"].IsInt64() ? (long?)long.Parse(form["hdInsurerContactId"]) : null;
                            policy.LastEndorsementId = long.Parse(endorsementId);
                            SpResponse spResponse = new GenericRepository<Policy>().Update(policy);
                            if (spResponse.Code != "100")
                            {
                                throw new Exception("Sigorta Ettiren Değişikliği Gerçekleştirilemedi. Lütfen Tekrar Deneyiniz...");
                            }
                        }
                    }
                    else if (policyDto.EndorsementType == ((int)EndorsementType.GIDECEGI_ULKE_DEGISIKLIGI).ToString())
                    {
                        if (policy != null && form["LOCATION_TYPE"].IsInt64())
                        {
                            policy.LocationType = form["LOCATION_TYPE"].IsInt64() ? form["LOCATION_TYPE"] : null;
                            policy.LastEndorsementId = long.Parse(endorsementId);
                            SpResponse spResponse = new GenericRepository<Policy>().Update(policy);
                            if (spResponse.Code != "100")
                            {
                                throw new Exception("Coğrafi Kapsam Değişikliği Gerçekleştirilemedi. Lütfen Tekrar Deneyiniz...");
                            }
                        }
                    }
                    else if (policyDto.EndorsementType == ((int)EndorsementType.POLICE_TAHAKKUK_ZEYLI).ToString() || policyDto.EndorsementType == ((int)EndorsementType.PRIM_FARKI_ZEYLI).ToString())
                    {
                        if (policy != null && form["PREMIUM"].IsInt64())
                        {
                            var premium = form["PREMIUM"].IsNull() ? null : form["PREMIUM"].Replace('.', ',');
                            var tax = form["TAX"].IsNull() ? null : form["TAX"].Replace('.', ',');

                            policy.Premium = premium.IsNumeric() ? (decimal?)decimal.Parse(premium) : null;
                            policy.Tax = tax.IsNumeric() ? (decimal?)decimal.Parse(tax) : null;
                            policy.LastEndorsementId = long.Parse(endorsementId);
                            SpResponse spResponse = new GenericRepository<Policy>().Update(policy);
                            if (spResponse.Code != "100")
                            {
                                throw new Exception("Poliçe Primi Güncellenemedi. Lütfen Tekrar Deneyiniz...");
                            }
                            else
                            {

                            }
                        }
                    }
                    else if (policyDto.EndorsementType == ((int)EndorsementType.BASLANGIC_BITIS_TARIHI).ToString())
                    {
                        if (policy != null && form["POLICY_START_DATE"].IsDateTime())
                        {
                            var sDate = !form["POLICY_START_DATE"].IsDateTime() ? null : DateTime.Parse(form["POLICY_START_DATE"]).ToString("dd-MM-yyyy");
                            var eDate = !form["POLICY_END_DATE"].IsDateTime() ? null : DateTime.Parse(form["POLICY_END_DATE"]).ToString("dd-MM-yyyy");

                            policy.EndDate = eDate.IsDateTime() ? (DateTime?)DateTime.Parse(eDate) : null;
                            policy.LastEndorsementId = long.Parse(endorsementId);
                            SpResponse spResponse = new GenericRepository<Policy>().Update(policy);
                            if (spResponse.Code != "100")
                            {
                                throw new Exception("Bitiş Tarihi Değişikliği Gerçekleştirilemedi. Lütfen Tekrar Deneyiniz...");
                            }

                            Endorsement endorsement = new GenericRepository<Endorsement>().FindBy($"TYPE = {((int)EndorsementType.BASLANGIC)} AND POLICY_ID={policy.Id}").SingleOrDefault();
                            if (endorsement == null)
                            {
                                throw new Exception("Başlangıç Tarihi Değişikliği Gerçekleştirilemedi. Lütfen Tekrar Deneyiniz...");
                            }
                            endorsement.StartDate = sDate.IsDateTime() ? (DateTime?)DateTime.Parse(sDate) : null;
                            policy.LastEndorsementId = long.Parse(endorsementId);
                            SpResponse spResponseEndorsement = new GenericRepository<Endorsement>().Update(endorsement);
                            if (spResponseEndorsement.Code != "100")
                            {
                                throw new Exception("Başlangıç Tarihi Değişikliği Gerçekleştirilemedi. Lütfen Tekrar Deneyiniz...");
                            }
                        }
                    }

                    if (policyDto.EndorsementStatus == ((int)Status.AKTIF).ToString())
                    {
                        var endorsement = new GenericRepository<Endorsement>().FindById(long.Parse(endorsementId));
                        endorsement.Premium = 0;

                        if (endorsement.Type != ((int)EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI).ToString() && endorsement.Type != ((int)EndorsementType.SE_DEGISIKLIGI).ToString() && endorsement.Type != ((int)EndorsementType.BASLANGIC_BITIS_TARIHI).ToString() && endorsement.Type != ((int)EndorsementType.URETIM_KAYNAGI_DEGISIKLIGI).ToString())
                        {
                            var insuredList = new GenericRepository<Insured>().FindBy($"LAST_ENDORSEMENT_ID={endorsement.Id}", fetchDeletedRows: (endorsement.Type == ((int)EndorsementType.SIGORTALI_CIKISI).ToString() ? true : false));
                            if (insuredList != null && insuredList.Count > 0)
                            {
                                foreach (var insured in insuredList)
                                {
                                    if (endorsement.Type != ((int)EndorsementType.SIGORTALI_GIRISI).ToString() && endorsement.Type != ((int)EndorsementType.BASLANGIC).ToString())
                                    {
                                        EndorsementInsured endorsementInsured = new EndorsementInsured
                                        {
                                            EndorsementId = endorsement.Id,
                                            InsuredId = insured.Id,
                                            InsuredStatus = insured.Status,
                                            PackageId = insured.PackageId,
                                            Premium = insured.Premium,
                                            TotalPremium = insured.TotalPremium
                                        };
                                        new GenericRepository<EndorsementInsured>().Insert(endorsementInsured);
                                    }

                                    endorsement.Premium += insured.Premium;
                                }
                                if (endorsement.Premium != 0)
                                {
                                    var firstEndorsement = new Endorsement();
                                    if (endorsement.Type == ((int)EndorsementType.BASLANGIC).ToString())
                                    {
                                        firstEndorsement = endorsement;
                                    }
                                    else
                                    {
                                        firstEndorsement = new GenericRepository<Endorsement>().FindBy("POLICY_ID=:policyId AND TYPE=:type", orderby: "", parameters: new { policyId = endorsement.PolicyId, type = new Dapper.DbString { Value = ((int)EndorsementType.BASLANGIC).ToString(), Length = 3 } }).FirstOrDefault();
                                    }

                                    if (firstEndorsement != null)
                                    {
                                        var insList = new GenericRepository<Installment>().FindBy($"POLICY_ID=:policyId AND ENDORSEMENT_ID=:endorsementId", parameters: new { policyId = firstEndorsement.PolicyId, endorsementId = endorsement.Id });
                                        if (insList != null && insList.Count > 0)
                                        {
                                            foreach (var item in insList)
                                            {
                                                item.Status = ((int)Status.SILINDI).ToString();
                                                new GenericRepository<Installment>().Update(item);
                                            }
                                        }

                                        var instalmentList = new GenericRepository<Installment>().FindBy($"POLICY_ID=:policyId AND ENDORSEMENT_ID=:endorsementId AND DUE_DATE>= TO_DATE('{ (DateTime)endorsement.DateOfIssue}','DD.MM.YYYY HH24:MI:SS')", parameters: new { policyId = firstEndorsement.PolicyId, endorsementId = endorsement.Id });
                                        if (instalmentList != null && instalmentList.Count > 0)
                                        {
                                            foreach (var item in instalmentList)
                                            {
                                                Installment installment = new Installment
                                                {
                                                    DueDate = item.DueDate,
                                                    Amount = (decimal)endorsement.Premium / instalmentList.Count,
                                                    EndorsementId = endorsement.Id,
                                                    PolicyId = endorsement.PolicyId
                                                };
                                                new GenericRepository<Installment>().Insert(installment);
                                            }
                                        }
                                        else
                                        {
                                            Installment installment = new Installment
                                            {
                                                DueDate = (DateTime)endorsement.DateOfIssue,
                                                Amount = (decimal)endorsement.Premium,
                                                EndorsementId = endorsement.Id,
                                                PolicyId = endorsement.PolicyId
                                            };
                                            new GenericRepository<Installment>().Insert(installment);
                                        }
                                    }
                                }
                            }
                        }
                        new GenericRepository<Endorsement>().Update(endorsement);
                    }
                }
                TempData["Alert"] = $"swAlert('İşlem Başarılı','Zeyl başarıyla kaydedildi.','success')";

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
                return RedirectToAction("Policy");
            }

            return RedirectToAction("Endorsement/" + form["hdPolicyId"]);
        }

        [HttpPost]
        [LoginControl]
        public JsonResult EndorsementCancelClaimControl(FormCollection form)
        {
            object res = "[]";
            try
            {
                var policyId = form["hdPolicyIdForCancel"];

                if (policyId.IsInt64())
                {
                    var searchClaim = new GenericRepository<Common.Entities.ProteinEntities.Claim>().FindBy($"POLICY_ID=:policyId AND STATUS NOT IN :statusList", parameters: new { policyId, statusList = new[] { ((int)ClaimStatus.IPTAL).ToString(), ((int)ClaimStatus.SILINDI).ToString(), ((int)ClaimStatus.RET).ToString() } }).FirstOrDefault();
                    if (searchClaim != null)
                    {
                        throw new Exception("Poliçeye Ait IPTAL Statüsü Dışında Hasar Bulunmakta! Lütfen Hasar Kayıtlarını Kontrol Ediniz...");
                    }
                }
                res = "100";
            }
            catch (Exception ex)
            {
                res = "999";
                //TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(res.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult EndorsementCancel(FormCollection form)
        {
            try
            {
                var endorsementId = form["hdEndorsementId"];

                ServiceResponse response = new ServiceResponse();
                PolicyDto policyDto = new PolicyDto();
                IService<PolicyDto> policyService = new PolicyService();

                policyDto.isEndorsementChange = true;


                if (!Extensions.IsInt64(endorsementId) || long.Parse(endorsementId) < 0)
                {
                    policyDto.EndorsementId = 0;
                }
                else
                {
                    policyDto.EndorsementId = long.Parse(endorsementId);
                }

                var policyId = form["hdPolicyIdForCancel"];
                if (!Extensions.IsInt64(policyId) || long.Parse(policyId) < 0)
                {
                    throw new Exception("Zeyl Bağlı Bulunduğu Poliçe Bulunamadı. Lütfen Tekrar Deneyiniz...");
                }
                else
                {
                    policyDto.PolicyId = long.Parse(policyId);
                }

                Policy policy = new GenericRepository<Policy>().FindById(policyDto.PolicyId);
                if (policy == null)
                {
                    throw new Exception("Zeyl Bağlı Bulunduğu Poliçe Bulunamadı. Lütfen Tekrar Deneyiniz...");
                }


                var CATEGORY = form["CATEGORY"];
                if (Extensions.IsNull(CATEGORY))
                {
                    throw new Exception("Zeyl Kategorisi Seçilmelidir.");
                }
                else
                {
                    policyDto.EndorsementCategory = CATEGORY;
                }

                var TYPE_DESCRIPTION_TYPE = form["TYPE_DESCRIPTION_TYPE"];
                if (Extensions.IsNull(TYPE_DESCRIPTION_TYPE))
                {
                    throw new Exception("Zeyl Tipinin Türü Seçilmelidir.");
                }
                else
                {
                    policyDto.EndorsementTypeDescriptionType = TYPE_DESCRIPTION_TYPE;
                }

                policyDto.EndorsementTypeDescription = form["TYPE_DESCRIPTION"];

                var NO = form["NO_CANCEL"];
                if (!Extensions.IsInt(NO) || int.Parse(NO) < 0)
                {
                    throw new Exception("Zeyl No Bilgisi Girilmelidir.");
                }
                else
                {
                    policyDto.EndorsementNo = int.Parse(NO);
                }

                var TYPE = form["TYPE"];
                if (Extensions.IsNull(TYPE))
                {
                    throw new Exception("Zeyl Tipi Seçilmelidir.");
                }
                else
                {
                    policyDto.EndorsementType = TYPE;
                }

                policyDto.EndorsementStartDate = string.IsNullOrEmpty(form["START_DATE"]) ? null : (DateTime?)DateTime.Parse(form["START_DATE"]);

                var DateOfIssue = form["DATE_OF_ISSUE"];
                if (Extensions.IsNull(DateOfIssue))
                {
                    throw new Exception("Tanzim Tarihi Seçilmelidir.");
                }
                else
                {
                    policyDto.DateOfIssue = (DateTime?)DateTime.Parse(form["DATE_OF_ISSUE"]);
                }

                var remainingPremium = form["hdPolicyPREMIUMREMAINING"].Replace(".", ",");
                if (remainingPremium.IsNumeric())
                {
                    policyDto.PolicyPremium = decimal.Parse(remainingPremium);
                    policyDto.EndorsementPremium = decimal.Parse(remainingPremium) - policy.Premium;
                }
                else
                {
                    policyDto.PolicyPremium = 0;
                    policyDto.EndorsementPremium = -1 * policy.Premium;
                }

                policyDto.EndorsementRegistrationDate = string.IsNullOrEmpty(form["REGISTRATION_DATE"]) ? null : (DateTime?)DateTime.Parse(form["REGISTRATION_DATE"]);

                policyDto.isEndorsementChange = true;
                policyDto.PolicyStatus = Convert.ToString((int)PolicyStatus.IPTAL);

                response = policyService.ServiceInsert(policyDto, Token: "");
                if (!response.IsSuccess)
                {
                    throw new Exception(response.Code + " : " + response.Message);
                }
                endorsementId = response.Id.ToString();

                if (policyDto.EndorsementPremium != 0)
                {
                    var firstEndorsement = new GenericRepository<Endorsement>().FindBy("POLICY_ID=:policyId AND TYPE=:type", orderby: "", parameters: new { policyId = policyDto.PolicyId, type = new Dapper.DbString { Value = ((int)EndorsementType.BASLANGIC).ToString(), Length = 3 } }).FirstOrDefault();

                    if (firstEndorsement != null)
                    {
                        var insList = new GenericRepository<Installment>().FindBy($"POLICY_ID=:policyId AND ENDORSEMENT_ID=:endorsementId", parameters: new { policyId = firstEndorsement.PolicyId, endorsementId = endorsementId });
                        if (insList != null && insList.Count > 0)
                        {
                            foreach (var item in insList)
                            {
                                item.Status = ((int)Status.SILINDI).ToString();
                                new GenericRepository<Installment>().Update(item);
                            }
                        }

                        var instalmentList = new GenericRepository<Installment>().FindBy($"POLICY_ID=:policyId AND ENDORSEMENT_ID=:endorsementId AND DUE_DATE>= TO_DATE('{ (DateTime)policyDto.DateOfIssue}','DD.MM.YYYY HH24:MI:SS')", parameters: new { policyId = firstEndorsement.PolicyId, endorsementId = firstEndorsement.Id });
                        if (instalmentList != null && instalmentList.Count > 0)
                        {
                            foreach (var item in instalmentList)
                            {
                                Installment installment = new Installment
                                {
                                    DueDate = item.DueDate,
                                    Amount = (decimal)policyDto.EndorsementPremium / instalmentList.Count,
                                    EndorsementId = long.Parse(endorsementId),
                                    PolicyId = policyDto.PolicyId
                                };
                                new GenericRepository<Installment>().Insert(installment);
                            }
                        }
                        else
                        {
                            Installment installment = new Installment
                            {
                                DueDate = (DateTime)policyDto.DateOfIssue,
                                Amount = (decimal)policyDto.EndorsementPremium,
                                EndorsementId = long.Parse(endorsementId),
                                PolicyId = policyDto.PolicyId
                            };
                            new GenericRepository<Installment>().Insert(installment);
                        }
                    }
                }

                List<V_Insured> insuredList = new GenericRepository<V_Insured>().FindBy($"POLICY_ID=:policyId", orderby: "INSURED_ID", parameters: new { policyId = policyDto.PolicyId });
                if (insuredList != null && insuredList.Count > 0)
                {
                    List<EndorsementInsured> endorsementInsuredList = new List<EndorsementInsured>();
                    foreach (var item in insuredList)
                    {
                        EndorsementInsured endorsementInsured = new EndorsementInsured
                        {
                            EndorsementId = long.Parse(endorsementId),
                            InsuredId = item.INSURED_ID,
                            InsuredStatus = item.STATUS,
                            PackageId = item.PACKAGE_ID,
                            Premium = item.PREMIUM,
                            TotalPremium = item.TOTAL_PREMIUM
                        };
                        endorsementInsuredList.Add(endorsementInsured);
                    }
                    var a = new GenericRepository<EndorsementInsured>().UpdateEntities(endorsementInsuredList);
                }

                TempData["Alert"] = $"swAlert('İşlem Başarılı','Poliçe Başarıyla İptal Edildi.','success')";

            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
                return RedirectToAction("Policy");
            }

            return RedirectToAction("Policy");
        }

        [LoginControl]
        public ActionResult Endorsement(Int64 id = 0, string formaction = "")
        {
            ViewBag.PolicyId = id;

            if (ViewBag.PolicyId != null)
            {
                try
                {
                    var EndorsementTypeList = LookupHelper.GetLookupData(LookupTypes.Endorsement, showAll: true);
                    ViewBag.EndorsementTypeList = EndorsementTypeList;

                    TempData["Alert"] = TempData["Alert"] ?? string.Empty;
                }
                catch (Exception ex)
                {
                    TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
                }
                return View();
            }
            else
            {
                return RedirectToAction("Policy");
            }
        }

        [HttpPost]
        [LoginControl]
        public ActionResult EndorsementFilter(Int64 id, FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            String whereConditition = $" POLICY_ID = {id}";
            whereConditition += !String.IsNullOrEmpty(form["endorsementCompany"]) ? $" AND COMPANY_ID = {long.Parse(form["endorsementCompany"])} " : "";
            whereConditition += !String.IsNullOrEmpty(form["insurerTCKN"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" (INSURER_IDENTITY_NO = {form["insurerTCKN"]} OR INSURER_TAX_NUMBER = {form["insurerTCKN"]}) " : "";
            whereConditition += !String.IsNullOrEmpty(form["insurerName"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" INSURER_NAME LIKE '%{form["insurerName"]}%' " : "";
            whereConditition += !String.IsNullOrEmpty(form["endorsementType"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" ENDORSEMENT_TYPE = '{(form["endorsementType"])}' " : "";
            whereConditition += !String.IsNullOrEmpty(form["endorsementNo"]) ? (string.IsNullOrEmpty(whereConditition) ? "" : " AND ") + $" ENDORSEMENT_NO = {long.Parse(form["endorsementNo"])} " : "";

            var endorsementList = new GenericRepository<V_PolicyEndorsement>().FindByPaged(conditions: whereConditition,
                                                      orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection}" : "",
                                                      pageNumber: start,
                                                      rowsPerPage: length);

            ViewBag.Endorsements = endorsementList.Data;

            ViewBag.PolicyId = id;

            var companyList = new CompanyRepository().FindBy();
            ViewBag.CompanyList = companyList;

            var EndorsementTypeList = LookupHelper.GetLookupData(LookupTypes.Endorsement);
            ViewBag.EndorsementTypeList = EndorsementTypeList;

            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            return View("Endorsement");
        }

        [LoginControl]
        public ActionResult EndorsementStatusChange(FormCollection form)
        {
            var policyId = form["hdPolicyId"];
            if (!policyId.IsInt64() || long.Parse(policyId) <= 0)
            {
                TempData["Alert"] = $"swAlert('Hata','Poliçe Bilgisi Bulunamadı.','warning')";

                return RedirectToAction("Policy");
            }

            var policy = new PolicyRepository().FindById(long.Parse(policyId));
            if (policy != null)
            {
                Endorsement endorsement = new Endorsement();
                var endorsementId = form["hdEndorsementId_Modal"];
                if (!endorsementId.IsInt64() || long.Parse(endorsementId) <= 0)
                {
                    TempData["Alert"] = $"swAlert('Hata','Zeyl Bilgisi Bulunamadı.','warning')";

                    return RedirectToAction("Endorsement/" + policyId);
                }
                else
                {
                    endorsement = new GenericRepository<Endorsement>().FindById(long.Parse(endorsementId));
                }
                if (endorsement != null)
                {
                    if (endorsement.Type == ((int)EndorsementType.BASLANGIC).ToString())
                    {
                        var insuredList = new GenericRepository<V_Insured>().FindBy("POLICY_ID=:policyId", orderby: "INSURED_ID", parameters: new { policyId });
                        if (insuredList == null || insuredList.Count <= 0)
                        {
                            TempData["Alert"] = $"swAlert('Uyarı','Poliçenin tanzim edilebilmesi için başlangıç zeyline en az bir sigortalı ekleyiniz!','warning')";

                            return RedirectToAction("Endorsement/" + policyId);
                        }
                    }

                    if (endorsement.Status != ((int)Status.AKTIF).ToString())
                    {
                        endorsement.DateOfIssue = !form["DATE_OF_ISSUE"].IsDateTime() ? DateTime.Now : DateTime.Parse(form["DATE_OF_ISSUE"]);
                        endorsement.RegistrationDate = !form["REGISTRATION_DATE"].IsDateTime() ? (!form["DATE_OF_ISSUE"].IsDateTime() ? DateTime.Now : DateTime.Parse(form["DATE_OF_ISSUE"])) : DateTime.Parse(form["REGISTRATION_DATE"]);
                        endorsement.Status = ((int)Status.AKTIF).ToString();
                        var spResponseEndorsement = new GenericRepository<Endorsement>().Update(endorsement);
                        if (spResponseEndorsement.Code != "100")
                        {
                            throw new Exception(spResponseEndorsement.Code + " : " + spResponseEndorsement.Message);
                        }
                        else
                        {
                            endorsement.Premium = 0;

                            if (endorsement.Type != ((int)EndorsementType.SIGORTALI_BILGI_DEGISIKLIGI).ToString() && endorsement.Type != ((int)EndorsementType.SE_DEGISIKLIGI).ToString() && endorsement.Type != ((int)EndorsementType.BASLANGIC_BITIS_TARIHI).ToString() && endorsement.Type != ((int)EndorsementType.URETIM_KAYNAGI_DEGISIKLIGI).ToString())
                            {
                                var insuredList = new GenericRepository<Insured>().FindBy($"LAST_ENDORSEMENT_ID={endorsement.Id}", fetchDeletedRows: (endorsement.Type == ((int)EndorsementType.SIGORTALI_CIKISI).ToString() ? true : false));
                                if (insuredList != null && insuredList.Count > 0)
                                {
                                    foreach (var insured in insuredList)
                                    {
                                        if (endorsement.Type != ((int)EndorsementType.SIGORTALI_GIRISI).ToString() && endorsement.Type != ((int)EndorsementType.BASLANGIC).ToString())
                                        {
                                            EndorsementInsured endorsementInsured = new EndorsementInsured
                                            {
                                                EndorsementId = endorsement.Id,
                                                InsuredId = insured.Id,
                                                InsuredStatus = insured.Status,
                                                PackageId = insured.PackageId,
                                                Premium = insured.Premium,
                                                TotalPremium = insured.TotalPremium
                                            };
                                            new GenericRepository<EndorsementInsured>().Insert(endorsementInsured);
                                        }

                                        endorsement.Premium += insured.Premium;
                                    }
                                    if (endorsement.Premium != 0)
                                    {
                                        var firstEndorsement = new Endorsement();
                                        if (endorsement.Type == ((int)EndorsementType.BASLANGIC).ToString())
                                        {
                                            firstEndorsement = endorsement;
                                        }
                                        else
                                        {
                                            firstEndorsement = new GenericRepository<Endorsement>().FindBy("POLICY_ID=:policyId AND TYPE=:type", orderby: "", parameters: new { policyId = endorsement.PolicyId, type = new Dapper.DbString { Value = ((int)EndorsementType.BASLANGIC).ToString(), Length = 3 } }).FirstOrDefault();
                                        }

                                        if (firstEndorsement != null)
                                        {
                                            var insList = new GenericRepository<Installment>().FindBy($"POLICY_ID=:policyId AND ENDORSEMENT_ID=:endorsementId", parameters: new { policyId = firstEndorsement.PolicyId, endorsementId = endorsement.Id });
                                            if (insList != null && insList.Count > 0)
                                            {
                                                foreach (var item in insList)
                                                {
                                                    item.Status = ((int)Status.SILINDI).ToString();
                                                    new GenericRepository<Installment>().Update(item);
                                                }
                                            }

                                            var instalmentList = new GenericRepository<Installment>().FindBy($"POLICY_ID=:policyId AND ENDORSEMENT_ID=:endorsementId AND DUE_DATE>= TO_DATE('{ (DateTime)endorsement.DateOfIssue}','DD.MM.YYYY HH24:MI:SS')", parameters: new { policyId = firstEndorsement.PolicyId, endorsementId = firstEndorsement.Id });
                                            if (instalmentList != null && instalmentList.Count > 0)
                                            {
                                                foreach (var item in instalmentList)
                                                {
                                                    Installment installment = new Installment
                                                    {
                                                        DueDate = item.DueDate,
                                                        Amount = (decimal)endorsement.Premium / instalmentList.Count,
                                                        EndorsementId = endorsement.Id,
                                                        PolicyId = endorsement.PolicyId
                                                    };
                                                    new GenericRepository<Installment>().Insert(installment);
                                                }
                                            }
                                            else
                                            {
                                                Installment installment = new Installment
                                                {
                                                    DueDate = (DateTime)endorsement.DateOfIssue,
                                                    Amount = (decimal)endorsement.Premium,
                                                    EndorsementId = endorsement.Id,
                                                    PolicyId = endorsement.PolicyId
                                                };
                                                new GenericRepository<Installment>().Insert(installment);
                                            }
                                        }
                                    }
                                }
                            }
                            new GenericRepository<Endorsement>().Update(endorsement);

                        }
                    }
                }
                else
                {
                    TempData["Alert"] = $"swAlert('Hata','Zeyl Bilgisi Bulunamadı.','warning')";

                    return RedirectToAction("Endorsement/" + policyId);
                }
                TempData["Alert"] = $"swAlert('İşlem Başarılı','Zeyl Başarıyla Tanzim Edildi.','success')";
            }
            else
            {
                TempData["Alert"] = $"swAlert('Hata','Poliçe Bilgisi Bulunamadı.','warning')";

                return RedirectToAction("Policy");
            }
            return RedirectToAction("Endorsement/" + policyId);
        }

        [LoginControl]
        public ActionResult EndorsementBackStatus(Int64 id)
        {
            var policyId = Request["PolicyId"];
            try
            {
                if (!policyId.IsInt64() || long.Parse(policyId) <= 0)
                {
                    TempData["Alert"] = $"swAlert('Hata','Poliçe Bilgisi Bulunamadı.','warning')";
                    return RedirectToAction("Policy");
                }

                if (id <= 0)
                {
                    TempData["Alert"] = $"swAlert('Hata','Zeyl Bilgisi Bulunamadı.','warning')";
                    return RedirectToAction("Endorsement/" + policyId);
                }

                var endorsement = new GenericRepository<Endorsement>().FindById(id);
                if (endorsement == null)
                {
                    TempData["Alert"] = $"swAlert('Hata','Zeyl Bilgisi Bulunamadı.','warning')";
                    return RedirectToAction("Endorsement/" + policyId);
                }
                endorsement.Status = ((int)Status.PASIF).ToString();
                endorsement.DateOfIssue = null;
                var spResponse = new GenericRepository<Endorsement>().UpdateForAll(endorsement);
                if (spResponse.Code != "100")
                {
                    TempData["Alert"] = $"swAlert('Hata','Zeyl Bilgisi Güncellenemedi.','warning')";
                    return RedirectToAction("Endorsement/" + policyId);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Endorsement/" + policyId);
        }
        #endregion

        #region Insured
        [LoginControl]
        public ActionResult Insured(Int64 id = 0, string formaction = "")
        {
            ViewBag.EndorsementId = Request["EndorsementId"];

            if (Request["EndorsementId"].IsInt64())
            {
                var endorsement = new GenericRepository<Endorsement>().FindById(long.Parse(Request["EndorsementId"]));
                ViewBag.EndorsementType = endorsement.Type;
            }

            if ((id > 0 && Request["EndorsementId"].IsInt64()) || !formaction.IsNull())
            {
                #region Lookup Fill
                var MaritalStatusList = LookupHelper.GetLookupData(Constants.LookupTypes.MaritalStatus, showChoose: true);
                ViewBag.MaritalStatusList = MaritalStatusList;


                var GenderList = LookupHelper.GetLookupData(Constants.LookupTypes.Gender, showChoose: true);
                ViewBag.GenderList = GenderList;

                var IndividualTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Individual, showChoose: true);
                ViewBag.IndividualTypeList = IndividualTypeList;

                var RenewalGuaranteeTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.RenewalGuarantee, showChoose: true);
                ViewBag.RenewalGuaranteeTypeList = RenewalGuaranteeTypeList;

                var InsuredTransferTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.InsuredTransfer, showChoose: true);
                ViewBag.InsuredTransferTypeList = InsuredTransferTypeList;

                var ContactTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.Contact);
                ViewBag.ContactTypeList = ContactTypeList;

                var CurrencyTypes = LookupHelper.GetLookupData(Constants.LookupTypes.Currency);
                ViewBag.CurrencyTypes = CurrencyTypes;

                var VIPTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.VIP, showChoose: true);
                ViewBag.VIPTypeList = VIPTypeList;

                var InsuredNoteTypeList = LookupHelper.GetLookupData(Constants.LookupTypes.InsuredNote, showChoose: true);
                ViewBag.InsuredNoteTypeList = InsuredNoteTypeList;

                #endregion

                #region Table List
                var BankList = new BankRepository().FindBy();
                ViewBag.BankList = BankList;

                var CompanyList = new CompanyRepository().FindBy();
                ViewBag.CompanyList = CompanyList;

                var CityList = new CityRepository().FindBy();
                ViewBag.CityList = CityList;

                var CountryList = new CountryRepository().FindBy(orderby: "NAME");
                ViewBag.CountryList = CountryList;

                var CountyList = new CountyRepository().FindBy();
                ViewBag.CountyList = CountyList;

                //var ExclusionList = new ExclusionRepository().FindBy();
                //ViewBag.ExclusionList = ExclusionList;
                #endregion

                var resultProvider = new GenericRepository<V_Provider>().FindBy(orderby: "PROVIDER_ID");
                ViewBag.ProviderList = resultProvider.ToJSON(true);

                var resultProcessGroupList = new GenericRepository<V_ProcessGroup>().FindBy("PROCESS_LIST_NAME=:processListName", parameters: new { processListName = new DbString { Value = "ICD10", Length = 100 } }, orderby: "PROCESS_GROUP_NAME");
                ViewBag.ProcessGroupList = resultProcessGroupList;

                var ProviderGroupList = new GenericRepository<ProviderGroup>().FindBy();
                ViewBag.ProviderGroupList = ProviderGroupList;

                var ExclusionStatusReasonList = new GenericRepository<Reason>().FindBy("STATUS_NAME='ExclusionStatus' AND STATUS_ORDINAL=2");
                ViewBag.ExclusionStatusReasonList = ExclusionStatusReasonList;

                switch (formaction.ToLower())
                {
                    case "view":
                    case "edit":
                        if (id > 0)
                        {
                            var insured = new GenericRepository<V_Insured>().FindBy("INSURED_ID=:id", orderby: "INSURED_ID", parameters: new { id }).FirstOrDefault();
                            if (insured != null)
                            {

                                ViewBag.Insured = insured;
                                ViewBag.PolicyDate = "";

                                ViewBag.PolicyDate = insured.POLICY_START_DATE == null ? "" : ((DateTime)insured.POLICY_START_DATE).ToString("dd.MM.yyyy");


                                var PackageList = new GenericRepository<V_Package>().FindBy("SUBPRODUCT_ID=:SUBPRODUCT_ID", orderby: "PACKAGE_NAME", parameters: new { insured.SUBPRODUCT_ID });

                                List<dynamic> packageList = new List<dynamic>();

                                if (PackageList != null)
                                {

                                    foreach (var item in PackageList)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.PACKAGE_NAME = item.PACKAGE_NAME;
                                        listItem.PACKAGE_ID = Convert.ToString(item.PACKAGE_ID);

                                        packageList.Add(listItem);
                                    }

                                }
                                ViewBag.PackageList = packageList.Count > 0 ? packageList.ToJSON(true) : null;

                                var resultCoverage = new GenericRepository<V_PackagePlanCoverage>().FindBy("PACKAGE_ID=:PACKAGE_ID AND IS_MAIN_COVERAGE=1", orderby: "COVERAGE_ID", parameters: new { insured.PACKAGE_ID });
                                ViewBag.CoverageList = resultCoverage;

                                string contactId = Convert.ToString(insured.CONTACT_ID);
                                if (!contactId.IsNull())
                                {
                                    var bankss = new GenericRepository<V_ContactBank>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id = contactId });
                                    if (bankss != null)
                                    {

                                        List<dynamic> banksList = new List<dynamic>();

                                        int i = 1;

                                        foreach (var item in bankss)
                                        {
                                            dynamic listItem = new System.Dynamic.ExpandoObject();

                                            listItem.id = i;
                                            listItem.ClientId = i;
                                            listItem.STATUS = item.STATUS;

                                            listItem.BANK_ACCOUNT_ID = Convert.ToString(item.BANK_ACCOUNT_ID);
                                            listItem.CONTACT_BANK_ACCOUNT_ID = Convert.ToString(item.CONTACT_BANK_ACCOUNT_ID);

                                            listItem.BANK_ACCOUNT_NAME = item.BANK_ACCOUNT_NAME;
                                            listItem.BANK_ID = Convert.ToString(item.BANK_ID);
                                            listItem.BANK_IDSelectedText = item.BANK_NAME;


                                            listItem.BANK_BRANCH_ID = Convert.ToString(item.BANK_BRANCH_ID);
                                            listItem.BANK_BRANCH_IDSelectedText = item.BANK_BRANCH_NAME;
                                            listItem.BANK_BRANCH_CODE = item.BANK_BRANCH_CODE;
                                            listItem.BANK_ACCOUNT_NO = item.BANK_ACCOUNT_NO;
                                            listItem.IBAN = item.IBAN;
                                            listItem.BANK_CURRENCY_TYPE = item.CURRENCY_TYPE;
                                            listItem.CURRENCY_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Currency, item.CURRENCY_TYPE);
                                            listItem.IS_PRIMARY_BANK = item.IS_PRIMARY;
                                            listItem.isOpen = "0";

                                            banksList.Add(listItem);

                                            i++;
                                        }

                                        ViewBag.ContactBankList = banksList.ToJSON();
                                    }

                                    var notess = new GenericRepository<V_ContactNote>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id = contactId });
                                    if (notess != null)
                                    {
                                        List<dynamic> notesList = new List<dynamic>();

                                        int i = 1;

                                        foreach (var item in notess)
                                        {
                                            dynamic listItem = new System.Dynamic.ExpandoObject();

                                            listItem.id = i;
                                            listItem.ClientId = i;
                                            listItem.STATUS = item.STATUS;

                                            listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                                            listItem.CONTACT_NOTE_ID = Convert.ToString(item.CONTACT_NOTE_ID);

                                            listItem.NOTE_TYPE = Convert.ToString(item.NOTE_TYPE);
                                            listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Note, item.NOTE_TYPE);
                                            listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                                            listItem.isOpen = "0";
                                            notesList.Add(listItem);

                                            i++;
                                        }

                                        ViewBag.NoteList = notesList.ToJSON(true);
                                    }
                                }

                                var insuredExclusions = new GenericRepository<V_InsuredExclusion>().FindBy("INSURED_ID=:id", orderby: "INSURED_ID", parameters: new { id });
                                if (insuredExclusions != null)
                                {

                                    List<dynamic> insuredExclusionList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in insuredExclusions)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;
                                        listItem.STATUS_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Status, item.STATUS);

                                        if (item.RULE_CATEGORY_TYPE != null)
                                        {
                                            listItem.RULE_CATEGORY_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                                            listItem.RESULT = Convert.ToString(item.RESULT);
                                            listItem.RESULT_SECOND = Convert.ToString(item.RESULT_SECOND);
                                        }
                                        else
                                        {
                                            listItem.RULE_CATEGORY_TYPE_TEXT = "EK PRİM";
                                            listItem.RESULT = Convert.ToString(item.ADDITIONAL_PREMIUM);
                                        }
                                        listItem.EXCLUSION_DESCRIPTION = Convert.ToString(item.DESCRIPTION).RemoveRepeatedWhiteSpace();

                                        listItem.INSURED_EXCLUSION_ID = Convert.ToString(item.INSURED_EXCLUSION);
                                        listItem.EXCLUSION_ID = Convert.ToString(item.EXCLUSION_ID);
                                        listItem.IS_OPEN_TO_PRINT = Convert.ToString(item.IS_OPEN_TO_PRINT);
                                        listItem.isOpen = "0";

                                        insuredExclusionList.Add(listItem);

                                        i++;
                                    }
                                    ViewBag.Exclusions = insuredExclusionList.ToJSON();
                                }

                                var declarationss = new GenericRepository<V_InsuredNote>().FindBy("INSURED_ID =:id AND NOTE_TYPE=:noteType", orderby: "INSURED_ID", parameters: new { id, noteType = new DbString { Value = ((int)InsuredNoteType.BEYAN).ToString(), Length = 3 } });
                                if (declarationss != null)
                                {
                                    List<dynamic> notesList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in declarationss)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                                        listItem.INSURED_NOTE_ID = Convert.ToString(item.INSURED_NOTE_ID);

                                        listItem.NOTE_TYPE = Convert.ToString(item.NOTE_TYPE);
                                        listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.InsuredNote, item.NOTE_TYPE);
                                        listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                                        listItem.isOpen = "0";
                                        notesList.Add(listItem);

                                        i++;
                                    }

                                    ViewBag.DeclarationList = notesList.ToJSON();
                                }

                                var transferNotes = new GenericRepository<V_InsuredNote>().FindBy("INSURED_ID =:id AND NOTE_TYPE=:noteType", orderby: "INSURED_ID", parameters: new { id, noteType = new DbString { Value = ((int)InsuredNoteType.GECİS).ToString(), Length = 3 } });
                                if (transferNotes != null)
                                {
                                    List<dynamic> notesList = new List<dynamic>();

                                    int i = 1;

                                    foreach (var item in transferNotes)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.id = i;
                                        listItem.ClientId = i;
                                        listItem.STATUS = item.STATUS;

                                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                                        listItem.INSURED_NOTE_ID = Convert.ToString(item.INSURED_NOTE_ID);

                                        listItem.NOTE_TYPE = Convert.ToString(item.NOTE_TYPE);
                                        listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.InsuredNote, item.NOTE_TYPE);
                                        listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                                        listItem.isOpen = "0";
                                        notesList.Add(listItem);

                                        i++;
                                    }

                                    ViewBag.TransferNoteList = notesList.ToJSON();
                                }

                                if (formaction.ToLower() == "view")
                                {
                                    ViewBag.Title = "Sigortalı Görüntüle";
                                    ViewBag.IsView = true;
                                }
                                else
                                {
                                    ViewBag.Title = "Sigortalı Güncelle";
                                    ViewBag.IsEdit = true;
                                }
                                return View();

                            }
                        }
                        break;
                    default:
                        ViewBag.PolicyId = id;
                        ViewBag.Title = "Sigortalı Kaydet";

                        var policy = new GenericRepository<V_Policy>().FindBy("POLICY_ID=:policyId", orderby: "POLICY_ID", parameters: new { policyId = ViewBag.PolicyId }).FirstOrDefault();
                        if (policy != null)
                        {
                            ViewBag.Policy = policy;

                            if (policy.SUBPRODUCT_ID != null)
                            {
                                var PackageList = new GenericRepository<V_Package>().FindBy("SUBPRODUCT_ID=:SUBPRODUCT_ID", orderby: "PACKAGE_NAME", parameters: new { policy.SUBPRODUCT_ID });

                                List<dynamic> packageList = new List<dynamic>();

                                if (PackageList != null)
                                {

                                    foreach (var item in PackageList)
                                    {
                                        dynamic listItem = new System.Dynamic.ExpandoObject();

                                        listItem.PACKAGE_NAME = item.PACKAGE_NAME;
                                        listItem.PACKAGE_ID = Convert.ToString(item.PACKAGE_ID);

                                        packageList.Add(listItem);
                                    }

                                }
                                ViewBag.PackageList = packageList.Count > 0 ? packageList.ToJSON(true) : null;
                            }
                            if (policy.EXCHANGE_RATE_ID != null)
                            {
                                var code = Convert.ToString(policy.CURRENCY_TYPE);

                                if (!string.IsNullOrEmpty(code))
                                {
                                    var ExChangeRate = new GenericRepository<ExchangeRate>().FindBy("CURRENCY_TYPE =:currencyType", parameters: new { currencyType = new DbString { Value = code, Length = 3 } }, orderby: "ID DESC").FirstOrDefault();
                                    ViewBag.ExChangeRate = ExChangeRate;
                                }
                            }

                            var endFamilyNo = new GenericRepository<V_InsuredEndorsement>().FindBy("FAMILY_NO IS NOT NULL AND POLICY_ID=:policyId AND ENDORSEMENT_ID=:endorsementId", parameters: new { policyId = ViewBag.PolicyId, endorsementId = ViewBag.EndorsementId }, orderby: "FAMILY_NO DESC").FirstOrDefault();
                            if (endFamilyNo != null)
                            {
                                ViewBag.LastFamilyNo = (long.Parse(endFamilyNo.FAMILY_NO.ToString()));
                                ViewBag.FamilyNo = (long.Parse(endFamilyNo.FAMILY_NO.ToString()) + 1);
                            }
                            else
                            {
                                ViewBag.LastFamilyNo = 1;
                                ViewBag.FamilyNo = 1;
                            }
                        }
                        break;
                }

                return View();
            }
            else
            {
                return RedirectToAction("Policy");
            }
        }

        [HttpPost]
        public JsonResult ExclusionDelete(FormCollection form)
        {
            var result = new AjaxResultDto<ExclusionSaveResult>();
            try
            {
                var insuredExclusionId = form["hdInsuredExclusionId"];
                if (!insuredExclusionId.IsInt64())
                {
                    throw new Exception("İstisna Bilgisi Bulunamadı...");
                }

                var insuredId = form["hdInsuredIdforExclusion"];
                if (!insuredId.IsInt64())
                {
                    throw new Exception("Sigortalı Bilgisi Bulunamadı...");
                }

                InsuredExclusion insuredExclusion = new GenericRepository<InsuredExclusion>().FindById(long.Parse(insuredExclusionId));
                if (insuredExclusion != null)
                {
                    insuredExclusion.Status = ((int)Status.SILINDI).ToString();
                    insuredExclusion.ReasonId = form["EXCLUSION_STATUS_REASON_ID"].IsInt64() ? (long?)long.Parse(form["EXCLUSION_STATUS_REASON_ID"]) : null;
                    insuredExclusion.EndDate = form["EXCLUSION_END_DATE"].IsDateTime() ? (DateTime?)DateTime.Parse(form["EXCLUSION_END_DATE"]) : null;

                    SpResponse spResponse = new GenericRepository<InsuredExclusion>().Update(insuredExclusion);
                    if (spResponse.Code != "100")
                    {
                        throw new Exception("Silme İşlemi Başarısız!!!");
                    }
                }
                result.Data = new ExclusionSaveResult();
                var insuredExclusions = new GenericRepository<V_InsuredExclusion>().FindBy("INSURED_ID=:insuredId", orderby: "INSURED_ID", parameters: new { insuredId });
                if (insuredExclusions != null)
                {

                    List<dynamic> insuredExclusionList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in insuredExclusions)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;
                        listItem.STATUS_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Status, item.STATUS);

                        if (item.RULE_CATEGORY_TYPE != null)
                        {
                            listItem.RULE_CATEGORY_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                            listItem.RESULT = Convert.ToString(item.RESULT);
                            listItem.RESULT_SECOND = Convert.ToString(item.RESULT_SECOND);
                        }
                        else
                        {
                            listItem.RULE_CATEGORY_TYPE_TEXT = "EK PRİM";
                            listItem.RESULT = Convert.ToString(item.ADDITIONAL_PREMIUM);
                        }
                        listItem.EXCLUSION_DESCRIPTION = Convert.ToString(item.DESCRIPTION).RemoveRepeatedWhiteSpace();

                        listItem.INSURED_EXCLUSION_ID = Convert.ToString(item.INSURED_EXCLUSION);
                        listItem.EXCLUSION_ID = Convert.ToString(item.EXCLUSION_ID);
                        listItem.IS_OPEN_TO_PRINT = Convert.ToString(item.IS_OPEN_TO_PRINT);

                        listItem.isOpen = "0";

                        insuredExclusionList.Add(listItem);

                        i++;
                    }

                    result.Data = new ExclusionSaveResult
                    {
                        jsonExclusionData = insuredExclusionList.ToJSON()
                    };
                }
                result.ResultCode = "100";
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [LoginControl]
        public ActionResult DeleteInsured(Int64 id)
        {
            var endorsementId = Request["EndorsementId"];
            var policyId = Request["PolicyId"];
            try
            {
                var insuredId = id;
                if (insuredId < 1)
                {
                    throw new Exception("Sigortalı Bilgisi Bulunamadı!");
                }

                var insured = new GenericRepository<Insured>().FindById(insuredId);
                if (insured == null)
                {
                    throw new Exception("Sigortalı Bilgisi Bulunamadı!");
                }

                var policy = new GenericRepository<Policy>().FindById(long.Parse(policyId));
                if (policy == null)
                {
                    throw new Exception("Poliçe Bilgisi Bulunamadı!");
                }

                insured.Status = ((int)Status.SILINDI).ToString();
                var spRes = new GenericRepository<Insured>().Update(insured);
                if (spRes.Code != "100")
                {
                    throw new Exception(spRes.Message);
                }

                policy.Premium -= insured.TotalPremium;
                var spResPol = new GenericRepository<Policy>().Update(policy);
                if (spResPol.Code != "100")
                {
                    throw new Exception(spRes.Message);
                }


                TempData["Alert"] = $"swAlert('İşlem Başarılı','Silme İşlemi Başarıyla Gerçekleştirildi.','success')";
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("EndorsementForm/Edit/" + endorsementId, new { PolicyId = policyId });
        }

        [HttpPost]
        public JsonResult ExclusionIsOpenPrint(Int64 ExclusionId, int isOpenPrint)
        {
            var result = new AjaxResultDto<PackageSaveStep6Result>();
            try
            {
                var exclusion = new GenericRepository<Exclusion>().FindById(ExclusionId);
                if (exclusion == null)
                    throw new Exception("İstisna Bilgisi Bulunamadı!");

                exclusion.IS_OPEN_TO_PRINT = isOpenPrint;
                var spExclusion = new GenericRepository<Exclusion>().Insert(exclusion);

                result.ResultCode = spExclusion.Code;
                result.ResultMessage = spExclusion.Message;
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [LoginControl]
        public ActionResult InsuredDelete(FormCollection form)
        {
            var endorsementId = form["hdEndorsementId"];
            var policyId = form["hdPolicyId"];
            try
            {
                var insuredId = form["hdInsuredId"];
                if (!insuredId.IsInt64())
                {
                    throw new Exception("Sigortalı Bilgisi Bulunamadı!");
                }

                if (!form["hdInsuredPREMIUMREMAINING"].IsNumeric())
                {
                    throw new Exception("Çıkış Prim Tutar Giriniz!");
                }

                ServiceResponse response = new ServiceResponse();
                PolicyDto policyDto = new PolicyDto();
                IService<PolicyDto> policyService = new PolicyService();

                policyDto.isInsuredExitChange = true;
                policyDto.InsuredId = long.Parse(insuredId);
                policyDto.InsuredExitPremium = form["hdInsuredPREMIUMREMAINING"];
                policyDto.EndorsementId = long.Parse(endorsementId);
                policyDto.PolicyId = long.Parse(policyId);

                response = policyService.ServiceInsert(policyDto, Token: "");
                if (!response.IsSuccess)
                {
                    throw new Exception(response.Code + " : " + response.Message);
                }
                TempData["Alert"] = $"swAlert('İşlem Başarılı','Silme İşlemi Başarıyla Gerçekleştirildi.','success')";
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("EndorsementForm/Edit/" + endorsementId, new { PolicyId = policyId });
        }

        [HttpPost]
        [LoginControl]
        public JsonResult InsuredDeleteClaimControl(FormCollection form)
        {
            object res = "[]";
            var endorsementId = form["hdEndorsementId"];
            var policyId = form["hdPolicyId"];
            try
            {
                var insuredId = form["hdInsuredId"];
                if (insuredId.IsInt64())
                {
                    var searchClaim = new GenericRepository<Common.Entities.ProteinEntities.Claim>().FindBy($"INSURED_ID={insuredId} AND STATUS NOT IN('{((int)ClaimStatus.IPTAL).ToString()}','{((int)ClaimStatus.SILINDI).ToString()}')").FirstOrDefault();
                    if (searchClaim != null)
                    {
                        throw new Exception("Sigortalıya Ait IPTAL Statüsü Dışında Hasar Bulunmakta! Lütfen Hasar Kayıtlarını Kontrol Ediniz...");
                    }
                }
                res = "100";
            }
            catch (Exception ex)
            {
                res = "999";
                //TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(res.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [LoginControl]
        public JsonResult CalculateEntrancePremium(FormCollection form)
        {
            object res = "[]";
            var policyId = form["hdPolicyId"];
            var InsuredPackagePremium = form["hdInsuredTOTALPREMIUM"];
            try
            {
                if (!InsuredPackagePremium.IsNumeric())
                {
                    throw new Exception("Pakete Ait Primi Bulunamadı!");
                }
                InsuredPackagePremium = InsuredPackagePremium.Replace('.', ',');

                if (!policyId.IsInt64())
                {
                    throw new Exception("Poliçe Bilgisi Bulunamadı!");
                }
                decimal policyTotalDay = 0, insuredDay = 0;
                V_Policy policy = new GenericRepository<V_Policy>().FindBy($"POLICY_ID={policyId}", orderby: "POLICY_ID").FirstOrDefault();
                if (policy != null)
                {
                    TimeSpan time = (DateTime)policy.POLICY_END_DATE - (DateTime)policy.POLICY_START_DATE;
                    policyTotalDay = Math.Abs(time.Days);

                    var entranceDate = DateTime.Parse(form["insuredEntranceDate"]).AddHours(12);
                    TimeSpan timeSpan = entranceDate - (DateTime)(policy.POLICY_START_DATE);

                    insuredDay = timeSpan.Days;
                    if (insuredDay < 0)
                        insuredDay = 0;
                    decimal existPremium = Decimal.Parse(((insuredDay / policyTotalDay) * decimal.Parse(InsuredPackagePremium)).ToString("#0.00"));



                    res = decimal.Parse(InsuredPackagePremium) - existPremium;
                }
                else
                {
                    throw new Exception("Poliçe Bilgisi Bulunamadı!!!");
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(res.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [LoginControl]
        public JsonResult CalculateCancelPremiumPolicy(FormCollection form)
        {
            object res = "[]";
            var policyId = form["hdPolicyIdForCancel"];
            try
            {
                var calculateType = form["calculateType"];
                if (!calculateType.IsInt())
                {
                    throw new Exception("Hesaplama Tipini Seçiniz");
                }
                decimal policyTotalDay = 0, insuredDay = 0;
                V_Policy policy = new GenericRepository<V_Policy>().FindBy($"POLICY_ID={policyId}", orderby: "POLICY_ID").FirstOrDefault();
                if (policy != null)
                {
                    TimeSpan time = (DateTime)policy.POLICY_END_DATE - (DateTime)policy.POLICY_START_DATE;
                    policyTotalDay = Math.Abs(time.Days);


                    if (calculateType == "0" && form["PolicyDeleteDate"].IsDateTime())
                    {
                        var exitDate = DateTime.Parse(form["PolicyDeleteDate"]).AddHours(12);
                        TimeSpan timeSpan = exitDate - (DateTime)(policy.POLICY_START_DATE);

                        insuredDay = timeSpan.Days;
                        if (insuredDay < 0)
                            insuredDay = 0;
                    }
                    if (calculateType == "1" && form["PolicyDeleteDay"].IsInt())
                    {
                        insuredDay = int.Parse(form["PolicyDeleteDay"]);
                        if (insuredDay < 0)
                            insuredDay = 0;
                    }
                    decimal existPremium = Decimal.Parse(((insuredDay / policyTotalDay) * (decimal)policy.PREMIUM).ToString("#0.00"));

                    if (calculateType == "2" && form["PolicyExitPremium"].IsNumeric())
                    {
                        existPremium = decimal.Parse(form["PolicyExitPremium"].Replace('.', ','));
                    }

                    res = existPremium;
                }
                else
                {
                    throw new Exception("Poliçe Bilgisi Bulunamadı!!!");
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(res.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [LoginControl]
        public JsonResult CalculateDeletePremium(FormCollection form)
        {
            object res = "[]";
            var endorsementId = form["hdEndorsementId"];
            var policyId = form["hdPolicyId"];
            try
            {
                var insuredId = form["hdInsuredId"];
                if (!insuredId.IsInt64())
                {
                    throw new Exception("Sigortalı Bilgisi Bulunamadı!!!");
                }
                var calculateType = form["calculateType"];
                if (!calculateType.IsInt())
                {
                    throw new Exception("Hesaplama Tipini Seçiniz");
                }
                decimal policyTotalDay = 0, insuredDay = 0;
                V_Insured insured = new GenericRepository<V_Insured>().FindBy($"INSURED_ID={insuredId}", orderby: "INSURED_ID").FirstOrDefault();
                if (insured != null)
                {
                    TimeSpan time = (DateTime)insured.POLICY_END_DATE - (DateTime)insured.POLICY_START_DATE;
                    policyTotalDay = Math.Abs(time.Days);


                    if (calculateType == "0" && form["insuredDeleteDate"].IsDateTime())
                    {
                        var exitDate = DateTime.Parse(form["insuredDeleteDate"]).AddHours(12);
                        TimeSpan timeSpan = exitDate - (insured.ENTRANCE_DATE.ToString().IsDateTime() ? (DateTime)insured.ENTRANCE_DATE : (DateTime)insured.POLICY_START_DATE);

                        insuredDay = timeSpan.Days;
                        if (insuredDay < 0)
                            insuredDay = 0;
                    }
                    if (calculateType == "1" && form["InsuredDeleteDay"].IsInt())
                    {
                        insuredDay = int.Parse(form["InsuredDeleteDay"]);
                        if (insuredDay < 0)
                            insuredDay = 0;
                    }
                    decimal existPremium = decimal.Parse(((insuredDay / policyTotalDay) * (decimal)(insured.TOTAL_PREMIUM == null ? insured.PREMIUM : insured.TOTAL_PREMIUM)).ToString("#0.00"));

                    if (calculateType == "2" && form["InsuredExitPremium"].IsNumeric())
                    {
                        existPremium = decimal.Parse(form["InsuredExitPremium"].Replace('.', ','));
                    }

                    res = existPremium;
                }
                else
                {
                    throw new Exception("Sigortalı Bilgisi Bulunamadı!!!");
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(res.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public JsonResult InsuredContactSave(FormCollection form)
        {
            var result = new AjaxResultDto<InsuredContactSaveResult>();
            try
            {
                result.Data = new InsuredContactSaveResult();
                ServiceResponse response = new ServiceResponse();
                PolicyDto policyDto = new PolicyDto();
                IService<PolicyDto> policyService = new PolicyService();

                policyDto.isInsuredContactChange = true;

                policyDto.InsuredContactId = Extensions.IsInt64(form["hdInsuredContactId"]) ? long.Parse(form["hdInsuredContactId"]) : 0;
                policyDto.InsuredPersonId = Extensions.IsInt64(form["hdInsuredPersonId"]) ? long.Parse(form["hdInsuredPersonId"]) : 0;
                policyDto.InsuredContactEmailId = Extensions.IsInt64(form["hdInsuredContactEmailId"]) ? long.Parse(form["hdInsuredContactEmailId"]) : 0;
                policyDto.InsuredEmailId = Extensions.IsInt64(form["hdInsuredEmailId"]) ? long.Parse(form["hdInsuredEmailId"]) : 0;
                policyDto.InsuredContactMobilePhoneId = Extensions.IsInt64(form["hdInsuredContactMobilePhoneId"]) ? long.Parse(form["hdInsuredContactMobilePhoneId"]) : 0;
                policyDto.InsuredMobilePhoneId = Extensions.IsInt64(form["hdInsuredMobilePhoneId"]) ? long.Parse(form["hdInsuredMobilePhoneId"]) : 0;
                policyDto.InsuredContactPhoneId = Extensions.IsInt64(form["hdInsuredContactPhoneId"]) ? long.Parse(form["hdInsuredContactPhoneId"]) : 0;
                policyDto.InsuredPhoneId = Extensions.IsInt64(form["hdInsuredPhoneId"]) ? long.Parse(form["hdInsuredPhoneId"]) : 0;

                policyDto.InsuredGender = Extensions.IsNull(form["insuredGender"]) ? null : form["insuredGender"];
                policyDto.InsuredMaritalStatus = Extensions.IsNull(form["insuredMaritalStatus"]) ? null : form["insuredMaritalStatus"];
                policyDto.InsuredName = Extensions.IsNull(form["insuredName"]) ? null : form["insuredName"];
                policyDto.InsuredLastName = Extensions.IsNull(form["insuredSurName"]) ? null : form["insuredSurName"];
                policyDto.InsuredTcNo = form["insuredIdentityNo"].IsInt64() ?  (long?)Convert.ToInt64(form["insuredIdentityNo"]) : null;
                policyDto.InsuredTaxNumber = form["insuredTaxNumber"].IsInt64() ? (long?)Convert.ToInt64(form["insuredTaxNumber"]) :  null;
                policyDto.InsuredPassport = Extensions.IsNull(form["insuredPassportNo"]) ? null : form["insuredPassportNo"];
                policyDto.InsuredGsmNo = Extensions.IsNull(form["insuredGsmPhoneNo"]) ? null : form["insuredGsmPhoneNo"];
                policyDto.InsuredTelNo = Extensions.IsNull(form["insuredPhoneNo"]) ? null : form["insuredPhoneNo"];
                policyDto.InsuredEmail = Extensions.IsNull(form["insuredEmail"]) ? null : form["insuredEmail"];
                policyDto.InsuredNameOfFather = Extensions.IsNull(form["insuredFatherofName"]) ? null : form["insuredFatherofName"];
                policyDto.InsuredBirthDate = !Extensions.IsDateTime(form["insuredBirthDate"]) ? null : (DateTime?)DateTime.Parse(form["insuredBirthDate"]);
                policyDto.InsuredBirthPlace = Extensions.IsNull(form["insuredBirthPlace"]) ? null : form["insuredBirthPlace"];
                policyDto.InsuredNationality = Extensions.IsNull(form["insuredNationality"]) ? null : form["insuredNationality"];
                policyDto.InsuredVip = Extensions.IsNull(form["IS_VIP"]) ? null : form["IS_VIP"];
                policyDto.InsuredVipType = Extensions.IsNull(form["VIP_TYPE"]) ? null : form["VIP_TYPE"];
                policyDto.InsuredVipText = Extensions.IsNull(form["VIP_TYPE_TEXT"]) ? null : form["VIP_TYPE_TEXT"];

                policyDto.InsuredContactAddressId = Extensions.IsInt64(form["hdInsuredContactAddressId"]) ? long.Parse(form["hdInsuredContactAddressId"]) : 0;
                policyDto.InsuredAddressId = Extensions.IsInt64(form["hdInsuredAddressId"]) ? long.Parse(form["hdInsuredAddressId"]) : 0;
                policyDto.InsuredAddress = Extensions.IsNull(form["insuredAdress"]) ? null : form["insuredAdress"];
                policyDto.InsuredZipCode = Extensions.IsNull(form["insuredZipCode"]) ? null : form["insuredZipCode"];
                policyDto.InsuredCityId = !Extensions.IsInt64(form["city"]) ? null : (long?)long.Parse(form["city"]);
                policyDto.InsuredCountyId = !Extensions.IsInt64(form["county"]) ? null : (long?)long.Parse(form["county"]);

                response = policyService.ServiceInsert(policyDto, Token: "");
                if (!response.IsSuccess)
                {
                    result.ResultCode = response.Code;
                    result.ResultMessage = response.Message;
                    throw new Exception(response.Code + " : " + response.Message);
                }
                result.ResultCode = "100";
                result.ResultMessage = response.Message;
                result.Data.insuredContactId = response.Id;
                result.Data.ContactTitle = policyDto.InsuredName + " " + policyDto.InsuredLastName;
                result.Data.AGE = form["insuredBirthDate"].IsDateTime() ? DateTime.Now.Year - DateTime.Parse(form["insuredBirthDate"]).Year : 0;
                result.Data.GENDER = form["insuredGender"].IsNull() ? string.Empty : form["insuredGender"];
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        public JsonResult GetInsuredContactDetails()
        {
            string id = Request["ContactId"];
            var result = "[]";
            try
            {
                if (id.IsInt64() && long.Parse(id) > 0)
                {
                    var item = new GenericRepository<V_Portfoy>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id }).FirstOrDefault();
                    if (item != null)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.CONTACT_ID = Convert.ToString(item.CONTACT_ID);
                        listItem.PERSON_ID = Convert.ToString(item.PERSON_ID);

                        listItem.GENDER = Convert.ToString(item.GENDER);
                        listItem.MARITAL_STATUS = Convert.ToString(item.MARITAL_STATUS);

                        listItem.FIRST_NAME = Convert.ToString(item.FIRST_NAME);
                        listItem.LAST_NAME = Convert.ToString(item.LAST_NAME);
                        listItem.IDENTITY_NO = Convert.ToString(item.IDENTITY_NO);
                        listItem.TAX_NUMBER = Convert.ToString(item.TAX_NUMBER);
                        listItem.PASSPORT_NO = Convert.ToString(item.PASSPORT_NO);
                        listItem.NAME_OF_FATHER = Convert.ToString(item.NAME_OF_FATHER);

                        string BIRTHDATE = Convert.ToString(item.BIRTHDATE);
                        listItem.BIRTHDATE = BIRTHDATE.IsDateTime() ? DateTime.Parse(BIRTHDATE).ToString("dd-MM-yyyy") : null;

                        listItem.AGE = BIRTHDATE.IsDateTime() ? DateTime.Now.Year - DateTime.Parse(BIRTHDATE).Year : 0;

                        listItem.IS_VIP = Convert.ToString(item.IS_VIP);
                        listItem.VIP_TYPE = Convert.ToString(item.VIP_TYPE);
                        listItem.VIP_TEXT = Convert.ToString(item.VIP_TEXT);

                        listItem.BIRTHPLACE = Convert.ToString(item.BIRTHPLACE);
                        listItem.NATIONALITY_ID = Convert.ToString(item.NATIONALITY_ID);



                        var phoneItem = new GenericRepository<V_ContactPhone>().FindBy("CONTACT_ID=:id AND PHONE_TYPE=:phoneType AND IS_PRIMARY='1'", orderby: "CONTACT_ID", parameters: new { id, phoneType = new DbString { Value = ((int)PhoneType.IS).ToString(), Length = 3 } }).FirstOrDefault();
                        if (phoneItem != null)
                        {
                            listItem.PHONE_ID = Convert.ToString(phoneItem.PHONE_ID);
                            listItem.PHONE_NO = Convert.ToString(phoneItem.PHONE_NO);
                        }

                        var mobilePhoneItem = new GenericRepository<V_ContactPhone>().FindBy("CONTACT_ID=:id AND PHONE_TYPE=:phoneType AND IS_PRIMARY='1'", orderby: "CONTACT_ID", parameters: new { id, phoneType = new DbString { Value = ((int)PhoneType.MOBIL).ToString(), Length = 3 } }).FirstOrDefault();
                        if (mobilePhoneItem != null)
                        {
                            listItem.MOBILE_PHONE_ID = Convert.ToString(mobilePhoneItem.PHONE_ID);
                            listItem.MOBILE_PHONE_NO = Convert.ToString(mobilePhoneItem.PHONE_NO);
                        }

                        var emailItem = new GenericRepository<V_ContactEmail>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id }).FirstOrDefault();
                        if (emailItem != null)
                        {
                            listItem.EMAIL_ID = Convert.ToString(emailItem.EMAIL_ID);
                            listItem.EMAIL_DETAILS = Convert.ToString(emailItem.DETAILS);
                        }

                        var addressItem = new GenericRepository<V_ContactAddress>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id }).FirstOrDefault();
                        if (addressItem != null)
                        {
                            listItem.ADDRESS_ID = Convert.ToString(addressItem.ADDRESS_ID);
                            listItem.DETAILS = Convert.ToString(addressItem.DETAILS);
                            listItem.ZIP_CODE = Convert.ToString(addressItem.ZIP_CODE);
                            listItem.CITY_ID = Convert.ToString(addressItem.CITY_ID);
                            listItem.COUNTY_ID = Convert.ToString(addressItem.COUNTY_ID);
                        }
                        List<dynamic> insurerList = new List<dynamic>();
                        insurerList.Add(listItem);
                        result = insurerList.ToJSON();
                    }
                }
            }
            catch (Exception ex)
            {
                result = "[]";
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        public JsonResult GetPremium(FormCollection form)
        {
            var result = "[]";
            try
            {
                var age = form["hdAge"];
                if (!age.IsInt())
                {
                    var birthdate = form["hdBIRTHDATE"];
                    if (birthdate.IsDateTime())
                    {
                        age = (DateTime.Now.Year - DateTime.Parse(birthdate).Year).ToString();
                    }
                }
                var gender = form["hdGender"];
                var individualType = form["INDIVIDUAL_TYPE"];
                if (!age.IsInt() || gender.IsNull() || individualType.IsNull())
                {
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                var packageId = form["package"];
                if (!packageId.IsInt64())
                {
                    packageId = form["InsuredPackageChange"];
                }
                if (packageId.IsInt64() && long.Parse(packageId) > 0)
                {
                    var packagePrice = new GenericRepository<V_PackagePrice>().FindBy($"PACKAGE_ID={packageId} AND AGE_START<={age} AND AGE_END>={age} AND GENDER={gender} AND INDIVIDUAL_TYPE={individualType}", orderby: "INDIVIDUAL_TYPE").FirstOrDefault();
                    dynamic listItem = new System.Dynamic.ExpandoObject();
                    List<dynamic> insurerList = new List<dynamic>();

                    if (form["hdInsuredIdforPackage"].IsInt64())
                    {
                        Insured insured = new GenericRepository<Insured>().FindById(long.Parse(form["hdInsuredIdforPackage"]));
                        listItem.INSURED_PREMIUM = insured.TotalPremium;
                    }
                    if (packagePrice != null)
                    {
                        listItem.PREMIUM = packagePrice.PREMIUM;
                        insurerList.Add(listItem);
                    }
                    else
                    {
                        listItem.PREMIUM = 0;
                        insurerList.Add(listItem);
                    }



                    result = insurerList.ToJSON();
                }
            }
            catch (Exception ex)
            {

            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InsuredSave(FormCollection form)
        {
            var result = new AjaxResultDto<InsuredSaveResult>();
            try
            {
                result.Data = new InsuredSaveResult();
                ServiceResponse response = new ServiceResponse();
                PolicyDto policyDto = new PolicyDto();
                IService<PolicyDto> policyService = new PolicyService();

                policyDto.isInsuredChange = true;
                policyDto.InsuredId = form["hdInsuredId"].IsInt64() ? long.Parse(form["hdInsuredId"]) : 0;
                policyDto.PolicyId = form["hdPolicyId"].IsInt64() ? long.Parse(form["hdPolicyId"]) : throw new Exception("Poliçe Bilgisi Bulunamadı!!!");

                policyDto.EndorsementId = form["hdEndorsementId"].IsInt64() ? long.Parse(form["hdEndorsementId"]) : throw new Exception("Zeyl Bilgisi Bulunamadı!!!");

                var insuredContactId = form["hdInsuredContactId"];
                if (!insuredContactId.IsInt64() || long.Parse(insuredContactId) < 1)
                {
                    throw new Exception("Sigortalı Bilgisi Gerekmektedir!!!");
                }
                else
                {
                    policyDto.InsuredContactId = long.Parse(insuredContactId);
                }

                policyDto.InsuredPackageId = form["package"].IsInt64() ? long.Parse(form["package"]) : 0;

                var individualType = form["INDIVIDUAL_TYPE"];
                if (individualType.IsNull())
                {
                    throw new Exception("Sigortalı Tipi Gerekmektedir!!!");
                }
                else
                {
                    policyDto.InsuredIndividualType = individualType;
                }

                var familyNo = form["hdFamilyNoParent"];
                if (familyNo.IsNull())
                {
                    throw new Exception("Aile No Girilmesi Gerekmektedir!!!");
                }
                else
                {
                    policyDto.InsuredFamilyNo = long.Parse(familyNo);
                }

                policyDto.InsuredRegistorNo = !Extensions.IsNull(form["REGISTRATION_NO"]) ? form["REGISTRATION_NO"] : null;
                policyDto.insuredParentId = form["hdFamilyInsuredId"].IsInt64() ? (long.Parse(form["hdFamilyInsuredId"]) > 0 ? (long?)long.Parse(form["hdFamilyInsuredId"]) : null) : null;

                policyDto.InsuredRenewalGuaranteeType = !Extensions.IsNull(form["RENEWAL_GUARANTEE_TYPE"]) ? form["RENEWAL_GUARANTEE_TYPE"] : null;
                policyDto.InsuredRenewalGuaranteeText = !Extensions.IsNull(form["RENEWAL_GUARANTEE_TYPE_TEXT"]) ? form["RENEWAL_GUARANTEE_TYPE_TEXT"] : null;

                policyDto.InsuredFirstInsuredDate = !form["InsuredFirstInsuredDate"].IsDateTime() ? null : (DateTime?)DateTime.Parse(form["InsuredFirstInsuredDate"]);
                policyDto.InsuredCompanyEntranceDate = !Extensions.IsDateTime(form["InsuredCompanyEntranceDate"]) ? null : (DateTime?)DateTime.Parse(form["InsuredCompanyEntranceDate"]);
                policyDto.InsuredFirstAmbulantCoverageDate = !Extensions.IsDateTime(form["InsuredFirstAmbulantCoverageDate"]) ? null : (DateTime?)DateTime.Parse(form["InsuredFirstAmbulantCoverageDate"]);

                policyDto.InsuredBirthCoverageDate = !Extensions.IsDateTime(form["BIRTH_COVERAGE_DATE"]) ? null : (DateTime?)DateTime.Parse(form["BIRTH_COVERAGE_DATE"]);

                var premium = form["PREMIUM"].IsNull() ? null : form["PREMIUM"].Replace('.', ',');
                var tax = form["TAX"].IsNull() ? null : form["TAX"].Replace('.', ',');

                policyDto.InsuredEndorsementPremium = premium.IsNumeric() ? (decimal?)decimal.Parse(premium) : null;
                policyDto.InsuredInitialPremium = premium.IsNumeric() ? (decimal?)decimal.Parse(premium) : null;
                policyDto.InsuredTotalPremium = premium.IsNumeric() ? (decimal?)decimal.Parse(premium) : null;

                policyDto.InsuredTax = tax.IsNumeric() ? (decimal?)decimal.Parse(tax) : null;
                policyDto.ExchangeRateId = form["hdExchangeRateId"].IsInt64() ? (long?)long.Parse(form["hdExchangeRateId"]) : null;

                response = policyService.ServiceInsert(policyDto, Token: "");
                if (!response.IsSuccess)
                {
                    result.ResultCode = response.Code;
                    result.ResultMessage = response.Message;
                    result.Data = null;
                }
                else
                {
                    result.ResultCode = response.Code;
                    result.ResultMessage = response.Message;
                    result.Data.insuredId = response.Id;
                    result.Data.insuredContactId = form["hdInsuredContactId"].IsInt64() ? long.Parse(form["hdInsuredContactId"]) : 0;
                    policyDto.isInsuredChange = false;
                    policyDto.isInsuredTransferChange = true;

                    policyDto.InsuredTransferType = form["TransferType"].IsNull() ? null : form["TransferType"];
                    policyDto.InsuredTransferCompanyName = form["TrasferCompanyName"].IsNull() ? null : form["TrasferCompanyName"];
                    policyDto.InsuredTransferId = form["hdTransferId"].IsInt64() ? long.Parse(form["hdTransferId"]) : 0;
                    response = policyService.ServiceInsert(policyDto, Token: "");
                    if (!response.IsSuccess)
                    {
                        result.ResultCode = response.Code;
                        result.ResultMessage = response.Message;
                        result.Data = null;
                    }
                    else
                    {
                        result.Data.transferId = response.Id;

                        var birthDate = form["hdContractBirthDate"].IsDateTime() ? (DateTime?)DateTime.Parse(form["hdInsuredId"]) : null;
                        var gender = form["hdContactGender"].IsNull() ? null : form["hdContactGender"];
                        if (policyDto.InsuredPackageId > 0 && policyDto.InsuredIndividualType != null && birthDate != null & gender != null)
                        {
                            var age = DateTime.Now.Year - birthDate.Value.Year;
                            var price = new GenericRepository<V_PackagePrice>().FindBy("PACKAGE_ID=:packageId AND INDIVIDUAL_TYPE=:INDIVIDUAL_TYPE AND AGE_START<:age AND AGE_END>=:age AND GENDER=:gender", orderby: "PACKAGE_ID", parameters: new { packageId = policyDto.InsuredPackageId, INDIVIDUAL_TYPE = policyDto.InsuredIndividualType, age, gender }).FirstOrDefault();
                            if (price != null)
                            {
                                result.Data.Premium = Convert.ToDecimal(price.PREMIUM);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        public JsonResult InsuredRenewalGuaranteeTypeSave(FormCollection form)
        {
            var result = new AjaxResultDto<InsuredSaveResult>();

            try
            {
                if (!form["hdInsuredId"].IsInt64())
                {
                    throw new Exception("Sigortalı Bilgisi Bulunamadı!");
                }

                var insured = new GenericRepository<Insured>().FindById(long.Parse(form["hdInsuredId"]));
                if (insured == null)
                {
                    throw new Exception("Sigortalı Bilgisi Bulunamadı!");
                }

                var InsuredRenewalGuaranteeType = form["RENEWAL_GUARANTEE_TYPE"].IsInt() ? form["RENEWAL_GUARANTEE_TYPE"] : null;
                var InsuredRenewalGuaranteeText = !form["RENEWAL_GUARANTEE_TYPE_TEXT"].IsNull() ? form["RENEWAL_GUARANTEE_TYPE_TEXT"] : null;

                insured.RenewalGuaranteeType = InsuredRenewalGuaranteeType;
                insured.RenewalGuaranteeText = InsuredRenewalGuaranteeText;

                var spResponse = new GenericRepository<Insured>().UpdateForAll(insured);
                if (spResponse.Code != "100")
                {
                    result.ResultCode = spResponse.Code;
                    result.ResultMessage = spResponse.Message;
                    result.Data = null;
                }
                else
                {
                    result.ResultCode = spResponse.Code;
                    result.ResultMessage = spResponse.Message;
                    result.Data = new InsuredSaveResult
                    {
                        insuredId = insured.Id
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        public ActionResult InsuredPremiumSave(FormCollection form)
        {
            var result = new AjaxResultDto<InsuredSaveResult>();
            var endorsementId = form["hdEndorsementId"].IsInt64() ? long.Parse(form["hdEndorsementId"]) : 0;
            var policyId = form["hdPolicyId"].IsInt64() ? long.Parse(form["hdPolicyId"]) : 0;

            try
            {
                result.Data = new InsuredSaveResult();

                var InsuredId = form["hdInsuredIdforPremium"].IsInt64() ? long.Parse(form["hdInsuredIdforPremium"]) : 0;
                var insured = new GenericRepository<Insured>().FindById(InsuredId);
                if (insured != null)
                {
                    Policy policy = new GenericRepository<Policy>().FindById(policyId);
                    policy.Premium -= insured.TotalPremium;
                    var premium = form["InsuredPREMIUM"].IsNull() ? null : form["InsuredPREMIUM"].Replace('.', ',');
                    var tax = form["InsuredTAX"].IsNull() ? null : form["InsuredTAX"].Replace('.', ',');

                    insured.Premium = premium.IsNumeric() ? decimal.Parse(premium) - insured.TotalPremium : null;
                    insured.TotalPremium = premium.IsNumeric() ? (decimal?)decimal.Parse(premium) : null;
                    insured.Tax = tax.IsNumeric() ? (decimal?)decimal.Parse(tax) : null;
                    insured.LastEndorsementId = endorsementId;
                    SpResponse spResponse = new GenericRepository<Insured>().Update(insured);
                    if (spResponse.Code != "100")
                    {
                        result.ResultCode = spResponse.Code;
                        result.ResultMessage = spResponse.Message;
                        result.Data = null;
                    }
                    else
                    {
                        if (policy != null)
                        {
                            policy.Premium += insured.TotalPremium;
                        }
                        result.ResultCode = spResponse.Code;
                        result.ResultMessage = spResponse.Message;
                        result.Data.insuredId = spResponse.PkId;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return RedirectToAction("EndorsementForm/Edit/" + endorsementId, new { PolicyId = policyId });
        }

        [HttpPost]
        public ActionResult InsuredPackageSave(FormCollection form)
        {
            var result = new AjaxResultDto<InsuredSaveResult>();

            var endorsementId = form["hdEndorsementId"].IsInt64() ? long.Parse(form["hdEndorsementId"]) : 0;
            var policyId = form["hdPolicyId"].IsInt64() ? long.Parse(form["hdPolicyId"]) : 0;
            try
            {
                result.Data = new InsuredSaveResult();

                var InsuredId = form["hdInsuredIdforPackage"].IsInt64() ? long.Parse(form["hdInsuredIdforPackage"]) : 0;
                var premium = form["PREMIUM"].IsNumeric() ? decimal.Parse(form["PREMIUM"].Replace('.', ',')) : 0;
                var tax = form["TAX"].IsNumeric() ? decimal.Parse(form["TAX"].Replace('.', ',')) : 0;

                var insured = new GenericRepository<Insured>().FindById(InsuredId);
                if (insured != null)
                {
                    var policy = new GenericRepository<Policy>().FindById(policyId);
                    if (policy != null)
                    {
                        if (policy.Tax != null)
                        {
                            var oldTax = insured.Tax == null ? 0 : insured.Tax;
                            policy.Tax -= oldTax;
                        }
                        if (policy.Premium != null)
                        {
                            var oldPremium = insured.TotalPremium == null ? 0 : insured.TotalPremium;
                            policy.Premium -= oldPremium;
                        }
                    }

                    insured.Premium = premium - insured.TotalPremium;
                    insured.TotalPremium = premium;
                    insured.InitialPremium = premium;
                    insured.Tax = tax;
                    insured.PackageId = form["InsuredPackageChange"].IsInt64() ? (long?)long.Parse(form["InsuredPackageChange"]) : null;
                    insured.LastEndorsementId = endorsementId;
                    SpResponse spResponse = new GenericRepository<Insured>().Update(insured);
                    if (spResponse.Code != "100")
                    {
                        result.ResultCode = spResponse.Code;
                        result.ResultMessage = spResponse.Message;
                        result.Data = null;
                    }
                    else
                    {
                        if (policy.Tax != null)
                        {
                            policy.Tax += tax;
                        }
                        if (policy.Premium != null)
                        {
                            policy.Premium += premium;
                        }
                        new GenericRepository<Policy>().Update(policy);
                        result.ResultCode = spResponse.Code;
                        result.ResultMessage = spResponse.Message;
                        result.Data.insuredId = spResponse.PkId;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }

            return RedirectToAction("EndorsementForm/Edit/" + endorsementId, new { PolicyId = policyId });
        }

        [HttpPost]
        public JsonResult InsuredNoteSave(FormCollection form)
        {
            var result = new AjaxResultDto<InsuredNoteSaveResult>();
            result.Data = new InsuredNoteSaveResult();
            var notes = form["hdNoteList"];
            try
            {
                List<dynamic> noteList = new List<dynamic>();
                noteList = JsonConvert.DeserializeObject<List<dynamic>>(notes == "" ? "[]" : notes);
                var contactId = form["hdInsuredContactId"];

                foreach (var note in noteList)
                {
                    if (note.isOpen == "1")
                    {
                        var Tnote = new Note
                        {
                            Id = note.NOTE_ID,
                            Type = note.NOTE_TYPE,
                            Description = note.NOTE_DESCRIPTION,
                            Status = note.STATUS
                        };
                        var spResponseNote = new NoteRepository().Insert(Tnote);
                        if (spResponseNote.Code == "100")
                        {
                            var noteId = spResponseNote.PkId;
                            var contactNote = new ContactNote
                            {
                                Id = note.CONTACT_NOTE_ID,
                                ContactId = long.Parse(contactId),
                                NoteId = noteId,
                                Status = note.STATUS
                            };
                            var spResponseContactNote = new GenericRepository<ContactNote>().Insert(contactNote);
                            if (spResponseContactNote.Code == "100")
                            {
                                result.ResultCode = spResponseContactNote.Code;
                                result.ResultMessage = spResponseContactNote.Message;
                                TempData["Alert"] = $"swAlert('İşlem Başarılı','{note.NOTE_DESCRIPTION} başarıyla kaydedildi.','success')";
                            }
                            else
                            {
                                throw new Exception(spResponseContactNote.Code + " : " + spResponseContactNote.Message);
                            }
                        }
                        else
                        {
                            throw new Exception(spResponseNote.Code + " : " + spResponseNote.Message);
                        }
                    }
                }

                var notess = new GenericRepository<V_ContactNote>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id = contactId });
                if (notess != null)
                {
                    List<dynamic> notesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in notess)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                        listItem.CONTACT_NOTE_ID = Convert.ToString(item.CONTACT_NOTE_ID);

                        listItem.NOTE_TYPE = Convert.ToString(item.NOTE_TYPE);
                        listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Note, item.NOTE_TYPE);
                        listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                        listItem.isOpen = "0";
                        notesList.Add(listItem);

                        i++;
                    }

                    result.Data.jsonNoteData = notesList.ToJSON();
                }

                var declarations = form["hdDeclarationList"];
                List<dynamic> declarationList = new List<dynamic>();
                declarationList = JsonConvert.DeserializeObject<List<dynamic>>(declarations == "" ? "[]" : declarations);
                var insuredId = form["hdInsuredId"];

                foreach (var note in declarationList)
                {
                    if (note.isOpen == "1")
                    {
                        var Tnote = new Note
                        {
                            Id = note.NOTE_ID,
                            Type = ((int)InsuredNoteType.BEYAN).ToString(),
                            Description = note.NOTE_DESCRIPTION,
                            Status = note.STATUS
                        };
                        var spResponseNote = new NoteRepository().Insert(Tnote);
                        if (spResponseNote.Code == "100")
                        {
                            var noteId = spResponseNote.PkId;
                            var insuredNote = new InsuredNote
                            {
                                Id = note.INSURED_NOTE_ID,
                                InsuredId = long.Parse(insuredId),
                                NoteId = noteId,
                                Status = note.STATUS
                            };
                            var spResponseInsuredNote = new GenericRepository<InsuredNote>().Insert(insuredNote);
                            if (spResponseInsuredNote.Code == "100")
                            {
                                result.ResultCode = spResponseInsuredNote.Code;
                                result.ResultMessage = spResponseInsuredNote.Message;
                                TempData["Alert"] = $"swAlert('İşlem Başarılı','{note.NOTE_DESCRIPTION} başarıyla kaydedildi.','success')";
                            }
                            else
                            {
                                throw new Exception(spResponseInsuredNote.Code + " : " + spResponseInsuredNote.Message);
                            }
                        }
                        else
                        {
                            throw new Exception(spResponseNote.Code + " : " + spResponseNote.Message);
                        }
                    }
                }

                var declarationss = new GenericRepository<V_InsuredNote>().FindBy("INSURED_ID=:insuredId AND NOTE_TYPE=:noteType", orderby: "", parameters: new { insuredId, noteType = new DbString { Value = ((int)InsuredNoteType.BEYAN).ToString(), Length = 3 } });
                if (declarationss != null)
                {
                    List<dynamic> notesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in declarationss)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                        listItem.INSURED_NOTE_ID = Convert.ToString(item.INSURED_NOTE_ID);

                        listItem.NOTE_TYPE = Convert.ToString(item.NOTE_TYPE);
                        listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.InsuredNote, item.NOTE_TYPE);
                        listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                        listItem.isOpen = "0";
                        notesList.Add(listItem);

                        i++;
                    }
                    result.Data.jsonDeclarationData = notesList.ToJSON();
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InsuredTransferNoteSave(FormCollection form)
        {
            var result = new AjaxResultDto<InsuredNoteSaveResult>();
            var notes = form["hdTransferNoteList"];
            try
            {
                List<dynamic> transferNoteList = new List<dynamic>();
                transferNoteList = JsonConvert.DeserializeObject<List<dynamic>>(notes == "" ? "[]" : notes);
                var insuredId = form["hdInsuredId"];

                foreach (var note in transferNoteList)
                {
                    if (note.isOpen == "1")
                    {
                        var Tnote = new Note
                        {
                            Id = note.NOTE_ID,
                            Type = ((int)InsuredNoteType.GECİS).ToString(),
                            Description = note.NOTE_DESCRIPTION,
                            Status = note.STATUS
                        };
                        var spResponseNote = new NoteRepository().Insert(Tnote);
                        if (spResponseNote.Code == "100")
                        {
                            var noteId = spResponseNote.PkId;
                            var insuredNote = new InsuredNote
                            {
                                Id = note.INSURED_NOTE_ID,
                                InsuredId = long.Parse(insuredId),
                                NoteId = noteId,
                                Status = note.STATUS
                            };
                            var spResponseInsuredNote = new GenericRepository<InsuredNote>().Insert(insuredNote);
                            if (spResponseInsuredNote.Code == "100")
                            {
                                result.ResultCode = spResponseInsuredNote.Code;
                                result.ResultMessage = spResponseInsuredNote.Message;
                                TempData["Alert"] = $"swAlert('İşlem Başarılı','{Tnote.Description} başarıyla kaydedildi.','success')";
                            }
                            else
                            {
                                throw new Exception(spResponseInsuredNote.Code + " : " + spResponseInsuredNote.Message);
                            }
                        }
                        else
                        {
                            throw new Exception(spResponseNote.Code + " : " + spResponseNote.Message);
                        }
                    }
                }

                var declarationss = new GenericRepository<V_InsuredNote>().FindBy("INSURED_ID=:insuredId AND NOTE_TYPE=:noteType", orderby: "", parameters: new { insuredId, noteType = new DbString { Value = ((int)InsuredNoteType.GECİS).ToString(), Length = 3 } });
                if (declarationss != null)
                {
                    List<dynamic> notesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in declarationss)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                        listItem.INSURED_NOTE_ID = Convert.ToString(item.INSURED_NOTE_ID);

                        listItem.NOTE_TYPE = Convert.ToString(item.NOTE_TYPE);
                        listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.InsuredNote, item.NOTE_TYPE);
                        listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                        listItem.isOpen = "0";
                        notesList.Add(listItem);

                        i++;
                    }
                    result.Data = new InsuredNoteSaveResult
                    {
                        jsonTransferNoteData = notesList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InsuredBankSave(FormCollection form)
        {
            var result = new AjaxResultDto<InsuredBankSaveResult>();
            var banks = form["hdBanks"];
            try
            {
                List<dynamic> bankList = new List<dynamic>();
                bankList = JsonConvert.DeserializeObject<List<dynamic>>(banks);
                var contactId = form["hdInsuredContactId"];

                foreach (var bank in bankList)
                {
                    if (bank.isOpen == "1")
                    {
                        SpResponse spResponseBankAccount = new SpResponse();
                        var bankId = bank.BANK_ID;
                        var bankName = bank.BANK_IDSelectedText;
                        var currenyType = bank.BANK_CURRENCY_TYPE;
                        var name = Convert.ToString(bank.BANK_ACCOUNT_NAME);
                        var accountNo = Convert.ToString(bank.BANK_ACCOUNT_NO);
                        var bankBranchId = string.IsNullOrEmpty(Convert.ToString(bank.BANK_BRANCH_ID)) ? 0 : Convert.ToInt64(Convert.ToString(bank.BANK_BRANCH_ID));
                        var ibanNumber = Convert.ToString(bank.IBAN);
                        var isPrimary = string.IsNullOrEmpty(Convert.ToString(bank.IS_PRIMARY_BANK)) ? "0" : Convert.ToString(bank.IS_PRIMARY_BANK);

                        var isAgain = false;
                        if (bank.BANK_ACCOUNT_ID == null || long.Parse(Convert.ToString(bank.BANK_ACCOUNT_ID)) < 1)
                        {
                            var contactBank = new GenericRepository<V_ContactBank>().FindBy($"CONTACT_ID={contactId} AND IBAN='{ibanNumber}'", orderby: "").FirstOrDefault();
                            if (contactBank != null) isAgain = true;
                        }

                        if (!isAgain)
                        {
                            var bankAccount = new BankAccount
                            {
                                Id = bank.BANK_ACCOUNT_ID,
                                Name = name,
                                CurrencyType = currenyType,
                                No = accountNo,
                                Iban = ibanNumber,
                                BankId = bankId,
                                IsPrimary = isPrimary,
                                Status = bank.STATUS
                            };
                            if (bankBranchId > 0)
                            {
                                bankAccount.BankBranchId = bankBranchId;
                            }
                            spResponseBankAccount = new BankAccountRepository().Insert(bankAccount);

                            if (spResponseBankAccount.Code == "100")
                            {
                                var bankAccountId = spResponseBankAccount.PkId;
                                var contactBankAccount = new ContactBankAccount
                                {
                                    Id = bank.CONTACT_BANK_ACCOUNT_ID,
                                    ContactId = long.Parse(contactId),
                                    BankAccountId = bankAccountId,
                                    Status = bank.STATUS
                                };
                                var spResponseContactBankAccount = new GenericRepository<ContactBankAccount>().Insert(contactBankAccount);
                                if (spResponseContactBankAccount.Code == "100")
                                {
                                    result.ResultCode = spResponseContactBankAccount.Code;
                                    result.ResultMessage = spResponseContactBankAccount.Message;
                                    TempData["Alert"] = $"swAlert('İşlem Başarılı','{bankAccount.Name} başarıyla kaydedildi.','success')";
                                }
                                else
                                {
                                    throw new Exception(spResponseContactBankAccount.Code + " : " + spResponseContactBankAccount.Message);
                                }
                            }
                            else
                            {
                                throw new Exception(spResponseBankAccount.Code + " : " + spResponseBankAccount.Message);
                            }
                        }
                        else
                        {
                            throw new Exception("Aynı IBAN Numarasına ait Banka Bilgisi Bulunmaktadır! Lütfen Bilgileri Kontrol Ediniz...");
                        }
                    }
                }

                var bankss = new GenericRepository<V_ContactBank>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id = contactId });
                if (bankss != null)
                {

                    List<dynamic> banksList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in bankss)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.BANK_ACCOUNT_ID = Convert.ToString(item.BANK_ACCOUNT_ID);
                        listItem.CONTACT_BANK_ACCOUNT_ID = Convert.ToString(item.CONTACT_BANK_ACCOUNT_ID);

                        listItem.BANK_ACCOUNT_NAME = item.BANK_ACCOUNT_NAME;
                        listItem.BANK_ID = Convert.ToString(item.BANK_ID);
                        listItem.BANK_IDSelectedText = item.BANK_NAME;


                        listItem.BANK_BRANCH_ID = Convert.ToString(item.BANK_BRANCH_ID);
                        listItem.BANK_BRANCH_IDSelectedText = item.BANK_BRANCH_NAME;
                        listItem.BANK_BRANCH_CODE = item.BANK_BRANCH_CODE;
                        listItem.BANK_ACCOUNT_NO = item.BANK_ACCOUNT_NO;
                        listItem.IBAN = item.IBAN;
                        listItem.BANK_CURRENCY_TYPE = item.CURRENCY_TYPE;
                        listItem.CURRENCY_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Currency, item.CURRENCY_TYPE);
                        listItem.IS_PRIMARY_BANK = item.IS_PRIMARY;
                        listItem.isOpen = "0";

                        banksList.Add(listItem);

                        i++;
                    }

                    result.Data = new InsuredBankSaveResult
                    {
                        jsonBankData = banksList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ExclusionSave(FormCollection form)
        {
            var result = new AjaxResultDto<ExclusionSaveResult>();
            try
            {
                var insuredId = form["hdInsuredId"];
                if (!insuredId.IsInt64())
                {
                    throw new Exception("Sigortalı Bilgisi Bulunamadı...");
                }

                var contactId = form["hdInsuredContactId"];
                if (!contactId.IsInt64())
                {
                    throw new Exception("Sigortalı Bilgisi Bulunamadı...");
                }

                var startDate = form["EXCLUSION_startDate"];
                if (!startDate.IsDateTime())
                {
                    throw new Exception("Tarih Bilgisi Girilmesi Gerekmektedir.");
                }

                var processGroupIdList = form["ProcessGroup"];
                if (!processGroupIdList.IsNull())
                {
                    var coverageIdList = form["Coverage"];
                    if (coverageIdList.IsNull())
                    {
                        throw new Exception("Teminat Seçimi Yapılmalıdır!");
                    }
                }

                var v_insured = new GenericRepository<V_Insured>().FindBy("INSURED_ID=:insuredId", orderby: "", parameters: new { insuredId }).FirstOrDefault();
                if (v_insured == null)
                {
                    throw new Exception("Sigortalı Bilgisi Bulunamadı!");
                }
                if (v_insured.POLICY_START_DATE > DateTime.Parse(startDate).AddHours(12))
                {
                    throw new Exception("İstisna Tarihi Poliçe Başlangıç Tarihinde Küçük Olamaz!");
                }
                if (v_insured.POLICY_END_DATE < DateTime.Parse(startDate).AddHours(12))
                {
                    throw new Exception("İstisna Tarihi Poliçe Bitiş Tarihinde Büyük Olamaz!");
                }

                var type = form["type"];

                if (type != "10")
                {
                    var rule = new Rule();
                    rule.Type = ((int)RuleType.ISTISNA).ToString();

                    switch (type)
                    {
                        case "1":
                            rule.Result = form["limit"];
                            rule.CategoryId = new GenericRepository<RuleCategory>().FindBy($"TYPE={((int)RuleCategoryType.TEMINAT_LIMIT).ToString()}", orderby: "ID ASC").FirstOrDefault()?.Id;
                            break;
                        case "6":
                            rule.CategoryId = new GenericRepository<RuleCategory>().FindBy($"TYPE={((int)RuleCategoryType.TEMINAT_ODENMEZ).ToString()}", orderby: "ID ASC").FirstOrDefault()?.Id;
                            break;
                        case "0":
                            rule.Result = form["katilimOrani"];
                            rule.CategoryId = new GenericRepository<RuleCategory>().FindBy($"TYPE={((int)RuleCategoryType.KATILIM_ORAN).ToString()}", orderby: "ID ASC").FirstOrDefault()?.Id;
                            break;
                        case "4":
                            rule.CategoryId = new GenericRepository<RuleCategory>().FindBy($"TYPE={((int)RuleCategoryType.KURUM_HASAR_KAPAMA).ToString()}", orderby: "ID ASC").FirstOrDefault()?.Id;
                            break;
                        case "2":
                            rule.Result = form["muafiyet"];
                            rule.ResultSecond = form["muafiyet2"];
                            rule.CategoryId = new GenericRepository<RuleCategory>().FindBy($"TYPE={((int)RuleCategoryType.MUAFIYET_LIMIT).ToString()}", orderby: "ID ASC").FirstOrDefault()?.Id;
                            break;
                        default:
                            break;
                    }
                    var spResponseRule = new RuleRepository().Insert(rule);
                    if (spResponseRule.Code == "100")
                    {
                        long ruleId = spResponseRule.PkId;

                        var providerGroupRules = form["hdProviderGroupList"];
                        if (!String.IsNullOrEmpty(providerGroupRules) && providerGroupRules != "0")
                        {
                            List<dynamic> providerGroupRuleList = new List<dynamic>();
                            providerGroupRuleList = JsonConvert.DeserializeObject<List<dynamic>>(providerGroupRules);

                            foreach (var providerGroupRule in providerGroupRuleList)
                            {
                                if ((providerGroupRule.STATUS == "1" && providerGroupRule.PROVIDER_GROUP_RULE_ID != 0) || providerGroupRule.STATUS == "0")
                                {
                                    var TproviderGroupRule = new ProviderGroupRule
                                    {
                                        Id = providerGroupRule.PROVIDER_GROUP_RULE_ID,
                                        ProviderGroupId = providerGroupRule.PROVIDER_GROUP_ID,
                                        RuleId = ruleId
                                    };
                                    var spResponseProviderRule = new ProviderGroupRuleRepository().Insert(TproviderGroupRule);
                                    if (spResponseProviderRule.Code != "100")
                                        throw new Exception(spResponseProviderRule.Code + " : " + spResponseProviderRule.Message);
                                }
                            }
                        }

                        var providerRules = form["hdProviderList"];
                        if (!String.IsNullOrEmpty(providerRules) && providerRules != "0")
                        {
                            List<dynamic> providerRuleList = new List<dynamic>();
                            providerRuleList = JsonConvert.DeserializeObject<List<dynamic>>(providerRules);

                            foreach (var providerRule in providerRuleList)
                            {
                                if ((providerRule.STATUS == "1" && providerRule.PROVIDER_RULE_ID != 0) || providerRule.STATUS == "0")
                                {
                                    var TproviderRule = new ProviderRule
                                    {
                                        Id = long.Parse(providerRule.PROVIDER_RULE_ID.ToString()),
                                        ProviderId = providerRule.PROVIDER_ID,
                                        RuleId = ruleId
                                    };
                                    var spResponseProviderRule = new ProviderRuleRepository().Insert(TproviderRule);
                                    if (spResponseProviderRule.Code != "100")
                                        throw new Exception(spResponseProviderRule.Code + " : " + spResponseProviderRule.Message);
                                }
                            }
                        }

                        var coverageIdList = form["Coverage"];
                        if (!coverageIdList.IsNull() && coverageIdList != "0")
                        {
                            foreach (var coverageId in coverageIdList.Split(','))
                            {
                                if (coverageId.IsInt64())
                                {
                                    var TcoverageRule = new CoverageRule
                                    {
                                        CoverageId = long.Parse(coverageId),
                                        RuleId = ruleId
                                    };
                                    var spResponseCoverageRule = new GenericRepository<CoverageRule>().Insert(TcoverageRule);
                                    if (spResponseCoverageRule.Code != "100")
                                        throw new Exception(spResponseCoverageRule.Code + " : " + spResponseCoverageRule.Message);
                                }
                            }
                        }

                        if (!processGroupIdList.IsNull() && processGroupIdList != "0")
                        {
                            foreach (var processGroup in processGroupIdList.Split(','))
                            {
                                if (processGroup.IsInt64())
                                {
                                    var TprocessGroupRule = new ProcessGroupRule
                                    {
                                        ProcessGroupId = long.Parse(processGroup),
                                        RuleId = ruleId
                                    };
                                    var spResponseProcessGroupRule = new GenericRepository<ProcessGroupRule>().Insert(TprocessGroupRule);
                                    if (spResponseProcessGroupRule.Code != "100")
                                        throw new Exception(spResponseProcessGroupRule.Code + " : " + spResponseProcessGroupRule.Message);
                                }
                            }
                        }

                        var exclusion = new Exclusion
                        {
                            RuleId = ruleId,
                            ContactId = long.Parse(contactId),
                            Description = form["exclusionDescription"]
                        };
                        var spResponseExclusion = new GenericRepository<Exclusion>().Insert(exclusion);
                        if (spResponseExclusion.Code != "100")
                            throw new Exception(spResponseExclusion.Code + " : " + spResponseExclusion.Message);

                        var insuredExclusion = new InsuredExclusion
                        {
                            InsuredId = long.Parse(insuredId),
                            ExclusionId = spResponseExclusion.PkId,
                            StartDate = DateTime.Parse(startDate)
                        };
                        var spResponseInsuredExclusion = new GenericRepository<InsuredExclusion>().Insert(insuredExclusion);
                        if (spResponseInsuredExclusion.Code != "100")
                            throw new Exception(spResponseInsuredExclusion.Code + " : " + spResponseInsuredExclusion.Message);
                    }
                    else
                    {
                        throw new Exception(spResponseRule.Code + " : " + spResponseRule.Message);
                    }
                }
                else
                {
                    if (!form["ekprim"].IsInt())
                    {
                        throw new Exception("EkPrim Değeri Giriniz!");
                    }
                    var insured = new GenericRepository<Insured>().FindById(long.Parse(insuredId));
                    if (insured == null)
                    {
                        throw new Exception("Sigortalı Bilgisi Bulunamadı!");
                    }
                    if (insured.InitialPremium == null)
                    {
                        throw new Exception("Sigortalı Prim Bilgisi Bulunamadı!");
                    }
                    decimal extraPremium = ((decimal)insured.InitialPremium * int.Parse(form["ekprim"])) / 100;
                    insured.TotalPremium = insured.TotalPremium == null ? (insured.InitialPremium + extraPremium) : (insured.TotalPremium + extraPremium);
                    var spResponseInsured = new GenericRepository<Insured>().Update(insured);
                    if (spResponseInsured.Code != "100")
                    {
                        throw new Exception("Sigortalı Primi Güncellenemedi!");
                    }
                    if (form["hdPolicyId"].IsInt64())
                    {
                        Policy policy = new GenericRepository<Policy>().FindById(long.Parse(form["hdPolicyId"]));
                        if (policy != null)
                        {
                            policy.Premium += extraPremium;
                            var spResponsePolicy = new GenericRepository<Policy>().Update(policy);
                            if (spResponsePolicy.Code != "100")
                            {
                                throw new Exception("Poliçe Primi Güncellenemedi!");
                            }
                        }
                    }

                    var exclusion = new Exclusion
                    {
                        AdditionalPremium = int.Parse(form["ekprim"]),
                        ContactId = long.Parse(contactId),
                        Description = form["exclusionDescription"]
                    };
                    var spResponseExclusion = new GenericRepository<Exclusion>().Insert(exclusion);
                    if (spResponseExclusion.Code != "100")
                        throw new Exception(spResponseExclusion.Code + " : " + spResponseExclusion.Message);

                    var insuredExclusion = new InsuredExclusion
                    {
                        InsuredId = long.Parse(insuredId),
                        ExclusionId = spResponseExclusion.PkId,
                        StartDate = DateTime.Parse(startDate)
                    };
                    var spResponseInsuredExclusion = new GenericRepository<InsuredExclusion>().Insert(insuredExclusion);
                    if (spResponseInsuredExclusion.Code != "100")
                        throw new Exception(spResponseInsuredExclusion.Code + " : " + spResponseInsuredExclusion.Message);
                }

                var insuredExclusions = new GenericRepository<V_InsuredExclusion>().FindBy("INSURED_ID=:insuredId", orderby: "INSURED_ID", parameters: new { insuredId });
                if (insuredExclusions != null)
                {

                    List<dynamic> insuredExclusionList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in insuredExclusions)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;
                        listItem.STATUS_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Status, item.STATUS);

                        if (item.RULE_CATEGORY_TYPE != null)
                        {
                            listItem.RULE_CATEGORY_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                            listItem.RESULT = Convert.ToString(item.RESULT);
                            listItem.RESULT_SECOND = Convert.ToString(item.RESULT_SECOND);
                        }
                        else
                        {
                            listItem.RULE_CATEGORY_TYPE_TEXT = "EK PRİM";
                            listItem.RESULT = Convert.ToString(item.ADDITIONAL_PREMIUM);
                        }
                        listItem.EXCLUSION_DESCRIPTION = Convert.ToString(item.DESCRIPTION).RemoveRepeatedWhiteSpace();

                        listItem.EXCLUSION_ID = Convert.ToString(item.EXCLUSION_ID);
                        listItem.INSURED_EXCLUSION_ID = Convert.ToString(item.INSURED_EXCLUSION);
                        listItem.IS_OPEN_TO_PRINT = Convert.ToString(item.IS_OPEN_TO_PRINT);
                        listItem.isOpen = "0";

                        insuredExclusionList.Add(listItem);

                        i++;
                    }

                    result.Data = new ExclusionSaveResult
                    {
                        jsonExclusionData = insuredExclusionList.ToJSON()
                    };
                }
                result.ResultCode = "100";
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}