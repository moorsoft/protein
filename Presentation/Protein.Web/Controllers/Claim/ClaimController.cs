﻿using Newtonsoft.Json;
using Protein.Business.Abstract.Print;
using Protein.Business.Enums.Import;
using Protein.Business.Print;
using Protein.Business.Print.BillAprovalForm;
using Protein.Business.Print.ClaimMissing;
using Protein.Business.Print.ClaimProviderReturn;
using Protein.Business.Print.ClaimRejectForm;
using Protein.Business.Print.ProvisionApproval;
using Protein.Business.Print.ProvisionMissingDocForm;
using Protein.Business.Print.ProvisionReject;
using Protein.Common.Constants;
using Protein.Common.Dto;
using Protein.Common.Entities;
using Protein.Common.Extensions;
using Protein.Common.Messaging;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.InsuranceCompanies;
using Protein.Data.ExternalServices.Protein;
using Protein.Data.ExternalServices.RxMedia;
using Protein.Data.ExternalServices.RxMedia.Result;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Web.ActionFilter.LoginAttr;
using Protein.Web.Models.ClaimModel;
using Protein.Web.Models.ExportModel;
using Protein.Web.Models.ListView;
using Protein.Web.Models.MediaModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;
using static Protein.Data.ExternalServices.Common.Lib.NN.NNInputOutputTypes;

namespace Protein.Web.Controllers.Claim
{
    [LoginControl]
    public class ClaimController : Controller
    {
        [LoginControl]
        public ActionResult Index()
        {
            #region Export - Import 
            ClaimVM vm = new ClaimVM();
            vm.ExportVM.ViewName = Views.Claim;
            vm.ExportVM.SetExportColumns();
            vm.ExportVM.ExportType = ExportType.Excel;

            vm.ImportVM.ImportObjectType = ImportObjectType.ClaimStatus;

            #endregion

            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            var insuredId = Request["insuredId"];

            if (insuredId.IsInt64() && long.Parse(insuredId) > 0)
                ViewBag.SelectInsuredId = long.Parse(insuredId);
            else
                ViewBag.SelectInsuredId = 0;

            ViewBag.ClaimStatusList = LookupHelper.GetLookupData(LookupTypes.ClaimStatus);
            ViewBag.ClaimSourceTypeList = LookupHelper.GetLookupData(LookupTypes.ClaimSource);
            ViewBag.claimListClaimTypes = LookupHelper.GetLookupData(LookupTypes.Claim);
            ViewBag.claimListProviderTypes = LookupHelper.GetLookupData(LookupTypes.Provider);
            ViewBag.ICDTypeList = LookupHelper.GetLookupData(LookupTypes.ICD);
            ViewBag.ClaimMissingDocTypeList = LookupHelper.GetLookupData(LookupTypes.ClaimMissingDoc);
            ViewBag.PayrollStatusList = LookupHelper.GetLookupData(LookupTypes.PayrollStatus, showChoose: true);
            ViewBag.PayrollTypeList = LookupHelper.GetLookupData(LookupTypes.Payroll);
            ViewBag.PayrollCategoryList = LookupHelper.GetLookupData(LookupTypes.PayrollCategory);
            ViewBag.ClaimSourceTypeList = LookupHelper.GetLookupData(LookupTypes.ClaimSource);
            ViewBag.StaffContractTypeList = LookupHelper.GetLookupData(LookupTypes.StaffContract);
            ViewBag.PaymentTypeList = LookupHelper.GetLookupData(LookupTypes.Payment);
            ViewBag.CurrencyTypeList = LookupHelper.GetLookupData(LookupTypes.Currency);
            ViewBag.AgreementTypeList = LookupHelper.GetLookupData(LookupTypes.Agreement);
            ViewBag.ExemptionTypeList = LookupHelper.GetLookupData(LookupTypes.Exemption);
            ViewBag.EventTypeList = LookupHelper.GetLookupData(LookupTypes.Event);
            var ProviderList = new GenericRepository<V_Provider>().FindBy(orderby: "PROVIDER_ID");
            ViewBag.claimListProviders = ProviderList ?? new List<V_Provider>();

            return View(vm);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetOtherReport(FormCollection form)
        {
            var type = form["reportType"];
            var response = new PrintResponse();
            ProteinEntities.Claim claim = new GenericRepository<ProteinEntities.Claim>().FindBy("ID=:id", fetchDeletedRows: true, parameters: new { id = form["hdClaimId"] }).FirstOrDefault();
            if (claim != null)
            {
                if (type == "0") //Provizyon Ön Onay Mektubu
                {
                    if (claim.Status == ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                    {
                        IPrint<PrintProvisionApprovalReq> printt = new PrintProvisionApproval();
                        response = printt.DoWork(new PrintProvisionApprovalReq
                        {
                            ClaimId = long.Parse(form["hdClaimId"])
                        });
                    }
                    else
                    {
                        response.Code = "999";
                        response.Message = "Provizyon Ön Onay Mektubu, Hasar Durumu PROVİZYON iken alınabilir.";
                    }
                }
                else if (type == "1") //Fatura Onay Mektubu
                {
                    if (claim.Status == ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                    {
                        IPrint<PrintBillAprovalFormReq> printt = new PrintBillAprovalForm();
                        response = printt.DoWork(new PrintBillAprovalFormReq
                        {
                            ClaimId = long.Parse(form["hdClaimId"])
                        });
                    }
                    else
                    {
                        response.Code = "999";
                        response.Message = "Fatura Onay Mektubu, Hasar Durumu PROVİZYON iken alınabilir.";
                    }
                }
                else if (type == "2") //Provizyon Ret Formu
                {
                    if (claim.Status == ((int)ClaimStatus.RET).ToString())
                    {
                        IPrint<PrintProvisionRejectReq> printt = new PrintProvisionReject();
                        response = printt.DoWork(new PrintProvisionRejectReq
                        {
                            ClaimId = long.Parse(form["hdClaimId"])
                        });
                    }
                    else
                    {
                        response.Code = "999";
                        response.Message = "Provizyon Ret Formu, Hasar Durumu RET iken alınabilir.";
                    }
                }
                else if (type == "3") //Tazminat Ret Formu
                {
                    if (claim.Status == ((int)ClaimStatus.RET).ToString())
                    {
                        IPrint<PrintClaimRejectFormReq> printt = new PrintClaimRejectForm();
                        response = printt.DoWork(new PrintClaimRejectFormReq
                        {
                            ClaimId = long.Parse(form["hdClaimId"])
                        });
                    }
                    else
                    {
                        response.Code = "999";
                        response.Message = "Tazminat Ret Formu, Hasar Durumu RET iken alınabilir.";
                    }
                }
                else if (type == "4") //Provizyon Ret Formu
                {
                    if (claim.Status == ((int)ClaimStatus.RET).ToString())
                    {
                        IPrint<PrintProvisionRejectReq> printt = new PrintProvisionReject();
                        response = printt.DoWork(new PrintProvisionRejectReq
                        {
                            ClaimId = long.Parse(form["hdClaimId"])
                        });
                    }
                    else
                    {
                        response.Code = "999";
                        response.Message = "Provizyon Ret Formu, Hasar Durumu RET iken alınabilir.";
                    }
                }
                else if (type == "5") //Tazminat Eksik Evrak Formu
                {
                    if (claim.Status == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || claim.Status == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                    {
                        IPrint<PrintClaimMissingReq> printt = new PrintClaimMissing();
                        response = printt.DoWork(new PrintClaimMissingReq
                        {
                            ClaimId = long.Parse(form["hdClaimId"])
                        });
                    }
                    else
                    {
                        response.Code = "999";
                        response.Message = "Tazminat Eksik Evrak Formu, Hasar Durumu BEKLEME iken alınabilir.";
                    }
                }
                else if (type == "6") //Tazminat Kurum İade Formu
                {
                    if (claim.Status == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || claim.Status == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                    {
                        IPrint<PrintClaimProviderReturnReq> printt = new PrintClaimProviderReturn();
                        response = printt.DoWork(new PrintClaimProviderReturnReq
                        {
                            ClaimId = long.Parse(form["hdClaimId"])
                        });
                    }
                    else
                    {
                        response.Code = "999";
                        response.Message = "Tazminat Kurum İade Formu, Hasar Durumu BEKLEME iken alınabilir.";
                    }
                }
                else if (type == "7")
                {
                    if (claim.Status == ((int)ClaimStatus.RET).ToString())
                    {
                        IPrint<PrintClaimRejectFormReq> printt = new PrintClaimRejectForm();
                        response = printt.DoWork(new PrintClaimRejectFormReq
                        {
                            ClaimId = long.Parse(form["hdClaimId"])
                        });
                    }
                    else
                    {
                        response.Code = "999";
                        response.Message = "Tazminat Ret Formu, Hasar Durumu RET iken alınabilir.";
                    }
                }
                else if (type == "8")
                {
                    if (claim.Status == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || claim.Status == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                    {
                        IPrint<PrintProvisionMissingDocFormReq> printt = new PrintProvisionMissingDocForm();
                        response = printt.DoWork(new PrintProvisionMissingDocFormReq
                        {
                            ClaimId = long.Parse(form["hdClaimId"])
                        });
                    }
                    else
                    {
                        response.Code = "999";
                        response.Message = "Provizyon Eksik Evrak Formu, Hasar Durumu BEKLEME iken alınabilir.";
                    }
                }
            }
            else
            {
                response.Code = "999";
                response.Message = "Hasar Bulunamadı!";
            }

            return new JsonResult()
            {
                Data = new { FileUrl = response.Url, FileName = response.FileName, Message = response.Message, Code = response.Code }
            };
        }

        [LoginControl]
        public ActionResult MissingDoc()
        {
            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            var claimId = Request["claimId"];
            if (claimId.IsInt64() && long.Parse(claimId) > 0)
            {
                ViewBag.ClaimId = long.Parse(claimId);
            }
            else
            {
                TempData["Alert"] = $"swAlert('Hata','Hasar Bilgisi Bulunamadı','warning')";
                return RedirectToAction("Index");
            }
            ViewBag.ClaimMissingDocTypeList = LookupHelper.GetLookupData(LookupTypes.ClaimMissingDoc);

            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetClaimProcess(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            var claimlist = new ViewResultDto<List<V_ClaimProcessList>>();
            claimlist.Data = new List<V_ClaimProcessList>();
            if (!form["isOpenProcessModal"].IsNull() && form["isOpenProcessModal"] == "1" && form["ClaimId"].IsInt64() && long.Parse(form["ClaimId"]) > 0)
            {
                String whereConditition = form["ClaimId"].IsInt64() ? $" CLAIM_ID = {long.Parse(form["ClaimId"])} AND" : "";
                whereConditition += !form["ProcessName"].IsNull() ? $" nls_upper(PROCESS_NAME,'NLS_SORT = XTURKISH') LIKE ('%{form["ProcessName"].ToUpper()}%') AND" : "";
                whereConditition += !form["ProcessCode"].IsNull() ? $" PROCESS_CODE LIKE '%{form["ProcessCode"]}%' AND" : "";

                claimlist = new GenericRepository<V_ClaimProcessList>().FindByPaged(
                                  conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                  orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},CLAIM_ID DESC" : "CLAIM_ID DESC",
                                  pageNumber: start,
                                  rowsPerPage: length,
                                  fetchDeletedRows: true,
                                  fetchHistoricRows: true);
            }


            return Json(new { data = claimlist.Data, draw = Request["draw"], recordsTotal = claimlist.TotalItemsCount, recordsFiltered = claimlist.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetIcdList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            string name = form["icdName"];
            string code = form["icdCode"];

            List<Process> icdProcesses = ICD10StraightHelper.GetICD10Data();
            icdProcesses = icdProcesses.Where(p => p.Name.ToUpper().Contains(name.ToUpper()) && p.Code.ToUpper().Contains(code.ToUpper())).ToList();
            /*
            List<Process> parentProcesses = new List<Process>();

            List<Process> subProcess = new List<Process>();

            parentProcesses = ICD10Helper.GetICD10Data();
            //var aparentProcesses = parentProcesses.Select(p => p.ChildProcesses).ToList();//.Where(p => p.ParentId != null && p.Name.ToUpper().Contains(name.ToUpper()) && p.Code.ToUpper().Contains(code.ToUpper())).ToList();
            foreach (var item in parentProcesses)
            {
                if (item.ChildProcesses != null && item.ChildProcesses.Count > 0)
                {
                    subProcess.AddRange(item.ChildProcesses);
                }
            }
            subProcess = subProcess.Where(p => p.ParentId != null && p.Name.ToUpper().Contains(name.ToUpper()) && p.Code.ToUpper().Contains(code.ToUpper())).ToList();
            //var bparentProcesses = aparentProcesses.Select(a => a.Select(b => b));//.Where(p => p.ParentId != null && p.Name.ToUpper().Contains(name.ToUpper()) && p.Code.ToUpper().Contains(code.ToUpper())).ToList();
            
            return Json(new { data = subProcess, draw = Request["draw"], recordsTotal = subProcess.Count, recordsFiltered = subProcess.Count }, JsonRequestBehavior.AllowGet);
            */

            return Json(new { data = icdProcesses, draw = Request["draw"], recordsTotal = icdProcesses.Count, recordsFiltered = icdProcesses.Count }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetInsuredBankList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string insuredId = form["hdInsuredId"];
            string claimId = form["hdClaimId"];
            string bankAccountId = form["bankAccountId"];
            string radiobtn = form["radiobtn"];

            ViewResultDto<List<V_InsuredBankAccount>> insuredBankAccount = new ViewResultDto<List<V_InsuredBankAccount>>();
            insuredBankAccount.Data = new List<V_InsuredBankAccount>();

            if (claimId.IsInt64() && long.Parse(claimId) > 0 && insuredId.IsInt64() && long.Parse(insuredId) > 0)
            {

                var claimPayment = new GenericRepository<V_ClaimPayment>().FindBy(conditions: $"CLAIM_ID={claimId}", orderby: "CLAIM_ID").FirstOrDefault();

                string whereConditition = $"INSURED_ID={insuredId} AND";

                insuredBankAccount = new GenericRepository<V_InsuredBankAccount>().FindByPaged(
                                  conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                  orderby: "INSURED_BANK_ACCOUNT_ID",
                                      pageNumber: start,
                                      rowsPerPage: length);
                if (insuredBankAccount.Data != null && insuredBankAccount.Data.Count() > 0 && claimPayment != null && claimPayment.BANK_ACCOUNT_ID != null)
                {
                    var selectInsuredBankAccount = insuredBankAccount.Data.Where(x => x.BANK_ACCOUNT_ID == claimPayment.BANK_ACCOUNT_ID).FirstOrDefault();
                    if (selectInsuredBankAccount != null)
                    {
                        selectInsuredBankAccount.isClaimPayment = "1";
                    }
                }


            }


            return Json(new { data = (insuredBankAccount.Data == null ? new List<V_InsuredBankAccount>() : insuredBankAccount.Data), draw = Request["draw"], recordsTotal = insuredBankAccount.TotalItemsCount, recordsFiltered = insuredBankAccount.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetClaimList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];
            string message = "1";
            ClaimVM vm = new ClaimVM();
            vm.ExportVM.WhereCondition = "";
            vm.ImportVM.ImportObjectType = ImportObjectType.ClaimStatus;

            //var draw = "1";// Request["draw"];
            ViewResultDto<List<V_Claim>> claimlist = new ViewResultDto<List<V_Claim>>();
            claimlist.Data = new List<V_Claim>();
            if (isFilter == "1")
            {
                string hint = "";
                String whereConditition = !String.IsNullOrEmpty(form["claimListCompany"]) ? $" COMPANY_ID={long.Parse(form["claimListCompany"])} AND" : "";
                whereConditition += (form["claimListInsuredId"].IsInt64() && long.Parse(form["claimListInsuredId"]) > 0) ? $" INSURED_ID={long.Parse(form["claimListInsuredId"])} AND" : "";
                whereConditition += (form["claimListContactId"].IsInt64() && long.Parse(form["claimListContactId"]) > 0) ? $" CONTACT_ID={long.Parse(form["claimListContactId"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimListMedulaNo"]) ? $" MEDULA_NO = '{form["claimListMedulaNo"]}' AND" : "";
                whereConditition += form["claimListBillNo"].IsInt64() ? $" BILL_NO={long.Parse(form["claimListBillNo"])} AND" : "";
                whereConditition += form["claimListPayrollNo"].IsInt64() ? $" PAYROLL_ID ={long.Parse(form["claimListPayrollNo"])} AND" : "";
                whereConditition += form["claimListOldPayrollNo"].IsInt64() ? $" OLD_PAYROLL_ID ={long.Parse(form["claimListOldPayrollNo"])} AND" : "";
                whereConditition += form["claimListPolicyNo"].IsInt64() ? $" POLICY_NUMBER = {long.Parse(form["claimListPolicyNo"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimListClaimType"]) ? $" CLAIM_TYPE= {form["claimListClaimType"]} AND" : "";
                whereConditition += form["claimListClaimDateStart"].IsDateTime() ? $" CLAIM_DATE >= TO_DATE('{ DateTime.Parse(form["claimListClaimDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["claimListClaimDateEnd"].IsDateTime() ? $" CLAIM_DATE <= TO_DATE('{ DateTime.Parse(form["claimListClaimDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["claimListInsuredIdentityNo"].IsInt64() ? $" (IDENTITY_NO={form["claimListInsuredIdentityNo"]} OR TAX_NUMBER={form["claimListInsuredIdentityNo"]} OR  PASSPORT_NO='{form["claimListInsuredIdentityNo"]}') AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimListInsuredName"]) ? $" FIRST_NAME LIKE '%{form["claimListInsuredName"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimListInsuredSurname"]) ? $" LAST_NAME LIKE '%{form["claimListInsuredSurname"]}%' AND" : "";
                whereConditition += form["claimListProvider"] == "-1" ? "" : $" PROVIDER_ID ={(form["claimListProvider"])} AND";
                whereConditition += !String.IsNullOrEmpty(form["claimListInsurer"]) ? $" INSURER_NAME LIKE '%{form["claimListInsurer"]}%' AND" : "";
                whereConditition += form["claimListProductId"].IsInt64() ? $"  PRODUCT_ID={long.Parse(form["claimListCompany"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimListState[]"]) ? $" STATUS IN({form["claimListState[]"]}) AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimListSourceType[]"]) ? $" SOURCE_TYPE IN({form["claimListSourceType[]"]}) AND" : "";
                if (form["claimListClaimNo"].IsInt64())
                {
                    whereConditition += $" CLAIM_ID = {long.Parse(form["claimListClaimNo"])} AND";
                    hint = "/*+ USE_HASH(claim_id) ORDERED */";
                }

                if (whereConditition != "")
                {
                    claimlist = new GenericRepository<V_Claim>().FindByPaged(
                                                     conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                                     orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},CLAIM_ID DESC" : "CLAIM_ID DESC",
                                                     pageNumber: start,
                                                     rowsPerPage: length,
                                                     hint: hint);
                }
                else
                {
                    message = "Lütfen Filtre alanlarını doldurup filtrelemeyi deneyiniz.";
                }

                #region Export
                vm.ExportVM.ViewName = Views.Claim;
                vm.ExportVM.WhereCondition = (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3));
                vm.ExportVM.SetExportColumns();
                #endregion
            }
            return Json(new { data = (claimlist.Data == null ? new List<V_Claim>() : claimlist.Data), draw = Request["draw"], recordsTotal = claimlist.TotalItemsCount, recordsFiltered = claimlist.TotalItemsCount, whereConditition = vm.ExportVM.WhereCondition, message = message }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetMissingDoc(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            var claimId = form["ClaimId"];
            var claimMissingDoclist = new ViewResultDto<List<ClaimMissingDoc>>();
            if (claimId.IsInt64())
            {
                claimMissingDoclist = new GenericRepository<ClaimMissingDoc>().FindByPaged(
                                            conditions: $"CLAIM_ID={claimId}" + (form["isAll"].IsNull() ? " AND INCOMING_DATE IS NULL" : ""),
                                            orderby: "CLAIM_ID DESC,TYPE ASC",
                                            pageNumber: start,
                                            rowsPerPage: length);
            }

            return Json(new { data = claimMissingDoclist.Data, draw = Request["draw"], recordsTotal = claimMissingDoclist.TotalItemsCount, recordsFiltered = claimMissingDoclist.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [LoginControl]
        public ActionResult NoteList(FormCollection form)
        {


            return View();
        }
        [HttpPost]
        [LoginControl]
        public JsonResult SetClaimMissingDoc(FormCollection form)
        {
            var result = new AjaxResultDto<ExclusionSaveResult>();
            try
            {
                var claimId = form["hdClaimId"];
                if (!claimId.IsInt64() || long.Parse(claimId) < 1)
                {
                    throw new Exception("Hasar Bilgisi Bulunamadı...");
                }

                ProteinEntities.Claim claim = new GenericRepository<ProteinEntities.Claim>().FindById(long.Parse(claimId));
                if (claim == null)
                {
                    throw new Exception("Hasar Bilgisi Bulunamadı...");
                }
                int i = 0;
                List<ClaimMissingDoc> claimMissingDocList = new GenericRepository<ClaimMissingDoc>().FindBy($"CLAIM_ID={claimId} AND INCOMING_DATE IS NULL", orderby: "CLAIM_ID DESC,TYPE ASC");
                foreach (var claimMissingDoc in claimMissingDocList)
                {
                    if (form["INCOMING_DATE"].Split(',')[i].IsDateTime())
                    {
                        claimMissingDoc.INCOMING_DATE = DateTime.Parse(form["INCOMING_DATE"].Split(',')[i]);
                        var returnDate = new DateTime();
                        if (DateTime.Parse(form["INCOMING_DATE"].Split(',')[i]).AddDays(1).DayOfWeek == DayOfWeek.Saturday)
                        {
                            returnDate = DateTime.Parse(form["INCOMING_DATE"].Split(',')[i]).AddDays(3);
                        }
                        else if (DateTime.Parse(form["INCOMING_DATE"].Split(',')[i]).AddDays(1).DayOfWeek == DayOfWeek.Sunday)
                        {
                            returnDate = DateTime.Parse(form["INCOMING_DATE"].Split(',')[i]).AddDays(2);
                        }
                        else
                        {
                            returnDate = DateTime.Parse(form["INCOMING_DATE"].Split(',')[i]).AddDays(1);
                        }
                        claimMissingDoc.RETURN_DATE = returnDate;
                        SpResponse spResponseClaimMissingDoc = new GenericRepository<ClaimMissingDoc>().Update(claimMissingDoc);
                        if (spResponseClaimMissingDoc.Code != "100")
                        {
                            throw new Exception("Eksik Evrak Bilgisi Oluşturulamadı");
                        }
                    }
                    i++;
                }
                result.ResultCode = "100";
                result.ResultMessage = "Hasar Durumu Başarıyla Değiştirildi.";
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public JsonResult AddMissingDoc(FormCollection form)
        {
            var result = new AjaxResultDto<ExclusionSaveResult>();
            try
            {
                var claimId = form["hdClaimId"];
                if (!claimId.IsInt64() || long.Parse(claimId) < 1)
                {
                    throw new Exception("Hasar Bilgisi Bulunamadı...");
                }

                ProteinEntities.Claim claim = new GenericRepository<ProteinEntities.Claim>().FindById(long.Parse(claimId));
                if (claim == null)
                {
                    throw new Exception("Hasar Bilgisi Bulunamadı...");
                }

                if (form["DOCUMENT_LIST"].IsNull())
                {
                    throw new Exception("Eksik Evrakların Seçilmesi Gerekmektedir");
                }
                var missingDoc = form["DOCUMENT_LIST"].Split(',');
                foreach (var item in missingDoc)
                {
                    var claimMissingDocFetch = new GenericRepository<ClaimMissingDoc>().FindBy($"CLAIM_ID={claimId} AND TYPE={item}", orderby: "CLAIM_ID");
                    if (claimMissingDocFetch == null || claimMissingDocFetch.Count <= 0)
                    {
                        ClaimMissingDoc claimMissingDoc = new ClaimMissingDoc
                        {
                            CLAIM_ID = long.Parse(claimId),
                            MISSING_DATE = DateTime.Now,
                            TYPE = item
                        };
                        SpResponse spResponseClaimMissingDoc = new GenericRepository<ClaimMissingDoc>().Insert(claimMissingDoc);
                        if (spResponseClaimMissingDoc.Code != "100")
                        {
                            throw new Exception("Eksik Evrak Bilgisi Oluşturulamadı");
                        }
                    }
                }

                result.ResultCode = "100";
                result.ResultMessage = "Eksik Evraklar Başarıyla Eklendi";
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public JsonResult DeleteMissingDoc(string ClaimMisssingDocId)
        {
            var result = new AjaxResultDto<ExclusionSaveResult>();
            try
            {
                var claimMissingDoc = new GenericRepository<ClaimMissingDoc>().FindById(long.Parse(ClaimMisssingDocId));
                if (claimMissingDoc != null)
                {
                    claimMissingDoc.STATUS = ((int)Status.SILINDI).ToString();
                    SpResponse spResponse = new GenericRepository<ClaimMissingDoc>().Update(claimMissingDoc);
                    if (spResponse.Code != "100")
                        throw new Exception(spResponse.Code + " / " + spResponse.Message);
                }
                result.ResultCode = "100";
                result.ResultMessage = "Silme İşlemi Başarıyla Gerçekleştirildi";
                // TempData["Alert"] = $"swAlert('İşlem Başarılı','Silme İşlemi Başarıyla Gerçekleştirildi','success')";
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public JsonResult UpdateMissingDocDate(FormCollection form)
        {
            var result = new AjaxResultDto<ExclusionSaveResult>();
            try
            {
                var claimMissingDoc = new GenericRepository<ClaimMissingDoc>().FindById(long.Parse(form["hdClaimMisssingDocId"]));
                if (claimMissingDoc == null)
                {
                    throw new Exception("Eksik Dosya Bilgisi Bulunamadı!");
                }
                if (claimMissingDoc != null)
                {
                    claimMissingDoc.INCOMING_DATE = form["INCOMING_DATE"].IsDateTime() ? (DateTime?)DateTime.Parse(form["INCOMING_DATE"]) : null;
                    if (form["INCOMING_DATE"].IsDateTime())
                    {
                        var returnDate = new DateTime();
                        if (DateTime.Parse(form["INCOMING_DATE"]).AddDays(1).DayOfWeek == DayOfWeek.Saturday)
                        {
                            returnDate = DateTime.Parse(form["INCOMING_DATE"]).AddDays(3);
                        }
                        else if (DateTime.Parse(form["INCOMING_DATE"]).AddDays(1).DayOfWeek == DayOfWeek.Sunday)
                        {
                            returnDate = DateTime.Parse(form["INCOMING_DATE"]).AddDays(2);
                        }
                        else
                        {
                            returnDate = DateTime.Parse(form["INCOMING_DATE"]).AddDays(1);
                        }
                        claimMissingDoc.RETURN_DATE = returnDate;
                    }
                    SpResponse spResponse = new GenericRepository<ClaimMissingDoc>().UpdateForAll(claimMissingDoc);
                    if (spResponse.Code != "100")
                        throw new Exception(spResponse.Code + " / " + spResponse.Message);
                }
                result.ResultCode = "100";
                result.ResultMessage = "Güncelleme İşlemi Başarıyla Gerçekleştirildi";
                // TempData["Alert"] = $"swAlert('İşlem Başarılı','Silme İşlemi Başarıyla Gerçekleştirildi','success')";
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public JsonResult StatusRejectorWait(FormCollection form)
        {
            var result = new AjaxResultDto<StatusRejectorWaitSaveResult>();
            try
            {
                var claimId = form["hdClaimId"];
                if (!claimId.IsInt64() || long.Parse(claimId) < 1)
                {
                    throw new Exception("Hasar Bilgisi Bulunamadı...");
                }

                ProxyServiceClient proxyService = new ProxyServiceClient();
                ClaimStateUpdateReq stateUpdateReq = new ClaimStateUpdateReq { ClaimId = long.Parse(claimId) };

                var status = form["hdStatus"];
                if (!status.IsInt() || int.Parse(status) < 0)
                {
                    throw new Exception("Status Bilgisi Bulunamadı...");
                }

                ProteinEntities.Claim claim = new GenericRepository<ProteinEntities.Claim>().FindById(long.Parse(claimId));
                if (claim == null)
                {
                    throw new Exception("Hasar Bilgisi Bulunamadı...");
                }

                var reason = form["REASON_LIST"];
                if (!reason.IsInt())
                {
                    throw new Exception("Gerekçe Seçilmesi Gerekmektedir...");
                }

                if (status == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || status == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                {
                    if (claim.PayrollId != null)
                    {
                        var payroll = new GenericRepository<V_Payroll>().FindBy($"PAYROLL_ID={(long)claim.PayrollId}", orderby: "PAYROLL_ID", fetchDeletedRows: true).FirstOrDefault();
                        if (payroll.STATUS != ((int)PayrollStatus.Onay_Bekler).ToString())
                        {
                            throw new Exception("Onay Bekler Durumundaki Zarf Haricinde, Onay veya Ödendi Durumundaki Zarftaki Hasar Durumu BEKLEME'ye Çekilemez!");
                        }
                        status = ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString();
                    }
                    else
                    {
                        status = ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString();
                    }
                    if (claim.Status != ((int)ClaimStatus.PROZIYON_ONAY).ToString() && claim.Status != ((int)ClaimStatus.RET).ToString() && claim.Status != ((int)ClaimStatus.IPTAL).ToString() && claim.Status != ((int)ClaimStatus.ODENECEK).ToString())
                    {
                        throw new Exception("Hasar Durumu Uygun Değil");
                    }
                    if (reason == "3") //ClaimMissingDoc
                    {
                        if (form["DOCUMENT_LIST"].IsNull())
                        {
                            throw new Exception("Eksik Evrakların Seçilmesi Gerekmektedir");
                        }
                        var missingDoc = form["DOCUMENT_LIST"].Split(',');
                        foreach (var item in missingDoc)
                        {
                            ClaimMissingDoc claimMissingDoc = new ClaimMissingDoc
                            {
                                CLAIM_ID = long.Parse(claimId),
                                MISSING_DATE = DateTime.Now,
                                TYPE = item
                            };
                            SpResponse spResponseClaimMissingDoc = new GenericRepository<ClaimMissingDoc>().Insert(claimMissingDoc);
                            if (spResponseClaimMissingDoc.Code != "100")
                            {
                                throw new Exception("Eksik Evrak Bilgisi Oluşturulamadı");
                            }
                        }
                    }
                }
                else if (status == ((int)ClaimStatus.RET).ToString())
                {
                    if (claim.PayrollId != null)
                    {
                        var payroll = new GenericRepository<V_Payroll>().FindBy($"PAYROLL_ID={(long)claim.PayrollId}", orderby: "PAYROLL_ID", fetchDeletedRows: true).FirstOrDefault();
                        if (payroll.STATUS != ((int)PayrollStatus.Onay_Bekler).ToString())
                        {
                            throw new Exception("Onay Bekler Durumundaki Zarf Haricinde, Onay veya Ödendi Durumundaki Zarftaki Hasar Durumu BEKLEME'ye Çekilemez!");
                        }
                    }
                    if (claim.Status != ((int)ClaimStatus.PROZIYON_ONAY).ToString() && claim.Status != ((int)ClaimStatus.GIRIS).ToString() && claim.Status != ((int)ClaimStatus.IPTAL).ToString())
                    {
                        throw new Exception("Hasar Durumu Uygun Değil");
                    }
                }
                else if (status == ((int)ClaimStatus.IPTAL).ToString())
                {
                    if (claim.PayrollId != null)
                    {
                        var payroll = new GenericRepository<Payroll>().FindById((long)claim.PayrollId);
                        if (payroll.Status != ((int)PayrollStatus.Onay_Bekler).ToString())
                        {
                            throw new Exception("Onay Bekler Durumundaki Zarf Haricinde, Onay veya Ödendi Durumundaki Zarftaki Hasar Durumu PROVIZYON'a Çekilemez!");
                        }
                    }
                    if (claim.Status != ((int)ClaimStatus.RET).ToString() && claim.Status != ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                    {
                        throw new Exception("Hasar Durumu Uygun Değil");
                    }
                }

                claim.Status = status;
                claim.ReasonId = long.Parse(reason);
                SpResponse spResponse = new GenericRepository<ProteinEntities.Claim>().Update(claim);
                if (spResponse.Code != "100")
                {
                    throw new Exception(spResponse.Code + " : " + spResponse.Message);
                }

                result.Data = new StatusRejectorWaitSaveResult
                {
                    reason = ReasonHelper.GetReasonbyId(long.Parse(reason)).Description,
                    reasonId = long.Parse(reason)
                };
                result.ResultCode = spResponse.Code;
                result.ResultMessage = "Hasar Durumu Başarıyla Değiştirildi.";

                if (claim.CompanyId != null && (claim.CompanyId == 95 || claim.CompanyId == 10))
                {
                    if (claim.Status == ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                    {
                        proxyService.ClaimApproval(stateUpdateReq);
                    }
                    else if (claim.Status == ((int)ClaimStatus.RET).ToString())
                    {
                        proxyService.ClaimRejection(stateUpdateReq);
                    }
                    else if (claim.Status == ((int)ClaimStatus.IPTAL).ToString())
                    {
                        proxyService.ClaimCancellation(stateUpdateReq);
                    }
                    else if (claim.Status == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || claim.Status == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                    {
                        proxyService.ClaimWait(stateUpdateReq);
                    }
                }
                else if (claim.CompanyId == 322)
                {
                    string newStatus = "";
                    if (claim.Status == ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                    {
                        newStatus = "1";
                    }
                    else if (claim.Status == ((int)ClaimStatus.RET).ToString())
                    {
                        newStatus = "4";
                    }
                    else if (claim.Status == ((int)ClaimStatus.IPTAL).ToString())
                    {
                        newStatus = "2";
                    }
                    else if (claim.Status == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || claim.Status == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                    {
                        newStatus = "3";
                    }

                    if (newStatus.IsInt())
                    {
                        NNSetProvisionStatusReq nNSetProvisionStatusReq = new NNSetProvisionStatusReq
                        {
                            ProvisionNumber = claimId.ToString(),
                            ProvisionNumberSpecified = true,
                            NewStatu = newStatus,
                            NewStatuSpecified = true,
                            ReasonDesc = "",
                            ReasonDescSpecified = true
                        };
                        new NNHayatServiceClientV2().SetProvisionStatus(nNSetProvisionStatusReq);
                    }
                }
                else if (claim.CompanyId == 5)
                {
                    DogaServiceClient dogaServiceClient = new DogaServiceClient();
                    dogaServiceClient.HasarAktar("78cc5f34-42fe-40c3-9baf-8c45d8aada97", claim.Id);
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public JsonResult StatusChange(string claimId)
        {
            var result = new AjaxResultDto<ExclusionSaveResult>();
            try
            {

                bool isMailSend = false;
                if (!claimId.IsInt64() || long.Parse(claimId) < 1)
                {
                    throw new Exception("Hasar Bilgisi Bulunamadı...");
                }

                ProxyServiceClient proxyService = new ProxyServiceClient();
                ClaimStateUpdateReq stateUpdateReq = new ClaimStateUpdateReq { ClaimId = long.Parse(claimId) };
                var status = Request["Status"];
                if (!status.IsInt() || int.Parse(status) < 0)
                {
                    throw new Exception("Hasar durum bilgisi bulunamadı...");
                }

                ProteinEntities.Claim claim = new GenericRepository<ProteinEntities.Claim>().FindById(long.Parse(claimId));

                if (claim == null)
                {
                    throw new Exception("Hasar Bilgisi Bulunamadı...");
                }

                if (status == ((int)ClaimStatus.GIRIS).ToString())
                {
                    if (claim.PayrollId != null)
                    {
                        throw new Exception("Zarfa Eklenmiş Bir Hasar Giriş Durumuna Getirilemez! Zarftan Çıkardıktan Sonra Tekrar Deneyiniz...");
                    }

                    if (claim.Status != ((int)ClaimStatus.PROZIYON_ONAY).ToString() && claim.Status != ((int)ClaimStatus.RET).ToString() && claim.Status != ((int)ClaimStatus.IPTAL).ToString() && claim.Status != ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString())
                    {
                        throw new Exception("Hasar Durumu Uygun Değil");
                    }

                    if (claim.Status == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || claim.ReasonId == 3)
                    {
                        var missingDoc = new GenericRepository<ClaimMissingDoc>().FindBy($"CLAIM_ID={claimId} AND INCOMING_DATE IS NULL");
                        if (missingDoc.Count > 0)
                        {
                            throw new Exception("Tüm Eksik Evrakların Geliş Tarihlerinin Girilmesi Gerekmektedir.");
                        }
                    }
                }
                else if (status == ((int)ClaimStatus.SILINDI).ToString())
                {
                    if (claim.PayrollId != null)
                    {
                        throw new Exception("Zarfa Eklenmiş Bir Hasar Silinemez! Zarftan Çıkardıktan Sonra Tekrar Deneyiniz...");
                    }
                    if (claim.Status != ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                    {
                        throw new Exception("Hasar Durumu Uygun Değil");
                    }
                }
                else if (status == ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                {
                    if (claim.PayrollId != null)
                    {
                        Payroll payroll = new GenericRepository<Payroll>().FindBy($"ID={(long)claim.PayrollId}", fetchDeletedRows: true).FirstOrDefault();
                        if (payroll.Status != ((int)PayrollStatus.Onay_Bekler).ToString())
                        {
                            throw new Exception("Onay Bekler Durumundaki Zarf Haricinde, Onay veya Ödendi Durumundaki Zarftaki Hasar Durumu PROVIZYON'a Çekilemez!");
                        }
                    }
                    if (claim.Status != ((int)ClaimStatus.GIRIS).ToString() && claim.Status != ((int)ClaimStatus.RET).ToString() && claim.Status != ((int)ClaimStatus.ODENECEK).ToString() && claim.Status != ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString() && claim.Status != ((int)ClaimStatus.IPTAL).ToString() && claim.Status != ((int)ClaimStatus.ODENDI).ToString())
                    {
                        throw new Exception("Hasar Durumu Uygun Değil");
                    }

                    var vClaim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID={claim.Id}", orderby: "CLAIM_ID", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                    if (vClaim.PAID == null || vClaim.PAID <= 0)
                    {
                        throw new Exception("Hasarın Şirket Tutarı 0'dan Büyük Olmalıdır!");
                    }

                    if (claim.PaymentMethod == ((int)ClaimPaymentMethod.KURUM).ToString())
                    {
                        var IcdList = new GenericRepository<V_ClaimIcd>().FindBy("CLAIM_ID=:claimId", orderby: "CLAIM_ID", parameters: new { claimId = claim.Id });
                        if (IcdList.Count <= 0)
                            throw new Exception("Hasara Ait Tanı Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                        if (claim.StaffId == null)
                        {
                            throw new Exception("Hasara Ait Doktor Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                        }
                    }
                    else if (claim.PaymentMethod != ((int)ClaimPaymentMethod.SIGORTALI).ToString())
                    {
                        throw new Exception("Hasar Ödeme Tipi Uygun Değil");
                    }

                    var processList = new GenericRepository<V_ClaimProcess>().FindBy($"CLAIM_ID ={claim.Id}", orderby: "CLAIM_ID");
                    if (processList == null || processList.Count <= 0)
                    {
                        throw new Exception("Hasara Ait İşlem ya da Teminat Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                    }
                    else
                    {
                        foreach (V_ClaimProcess claimProcess in processList)
                        {
                            if (claimProcess.STATUS != Convert.ToString((int)Status.AKTIF))
                            {
                                throw new Exception("Hasara Ait İşlem/İlaç/Teminatlardan PROVİZYON Durumunda Olmayanlar Var. Lütfen Hasarı Güncelleyiniz...");
                            }
                        }
                    }

                    foreach (var process in processList)
                    {
                        if (process.COVERAGE_ID == null)
                        {
                            throw new Exception("Hasara Ait Tüm İşlemlerin Teminat Eşlemesi Yapılması Gerekmektedir. Lütfen Hasarı Güncelleyiniz...");
                        }
                        if (process.COVERAGE_TYPE == ((int)CoverageType.YATARAK).ToString())
                        {
                            var hospitalization = new GenericRepository<ClaimHospitalization>().FindBy($"CLAIM_ID={claim.Id}").FirstOrDefault();
                            if (hospitalization == null || hospitalization.EntranceDate == null)
                            {
                                throw new Exception("Yatarak Teminatı Bulunan Hasarın Yatış Tarihi Girilmesi Gerekmektedir.");
                            }
                        }
                    }

                    if (vClaim.PAID != null && vClaim.PAID >= 10000)
                    {
                        isMailSend = true;
                    }
                }
                else if (status == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || status == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                {
                    throw new Exception("Lütfen Gerekçe Giriniz!!!");
                }
                else if (status == ((int)ClaimStatus.RET).ToString())
                {
                    throw new Exception("Lütfen Gerekçe Giriniz!!!");
                }
                else if (status == ((int)ClaimStatus.ODENECEK).ToString())
                {
                    if (claim.Status != ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                    {
                        throw new Exception("Hasar Durumu Uygun Değil");
                    }
                    var claimBillList = new GenericRepository<V_ClaimBill>().FindBy($"CLAIM_ID ={claim.Id}", orderby: "CLAIM_ID");
                    if (claimBillList == null || claimBillList.Count <= 0)
                    {
                        throw new Exception("Hasara Ait Fatura Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                    }
                    if (claim.PayrollId == null || claim.PayrollId <= 0)
                    {
                        throw new Exception("Hasara Ait Zarf Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                    }
                    else
                    {
                        var payrollList = new GenericRepository<V_Payroll>().FindBy($"PAYROLL_ID ={claim.PayrollId}", orderby: "PAYROLL_ID");
                        if (payrollList == null || payrollList.Count <= 0)
                        {
                            throw new Exception("Hasara Ait Zarf Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                        }
                    }

                    if (claim.PaymentMethod == ((int)ClaimPaymentMethod.SIGORTALI).ToString())
                    {
                        var insuredId = Request["insuredId"];
                        if (!insuredId.IsInt64())
                        {
                            throw new Exception("Hasara Ait Sigortalı Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                        }
                        var insuredBankAccount = new GenericRepository<V_InsuredBankAccount>().FindBy($"INSURED_ID={insuredId}", orderby: "");
                        if (insuredBankAccount == null || insuredBankAccount.Count <= 0)
                        {
                            throw new Exception("Sigortalıya Ait Banka Hesap Bilgisi Bulunamadı. Lütfen Sigortalı Bilgilerini Güncelleyiniz...");
                        }
                    }
                    else if (claim.PaymentMethod == ((int)ClaimPaymentMethod.KURUM).ToString())
                    {
                        var providerankAccount = new GenericRepository<V_ProviderBankAccount>().FindBy($"PROVIDER_ID={claim.ProviderId}", orderby: "");
                        if (providerankAccount == null || providerankAccount.Count <= 0)
                        {
                            throw new Exception("Kuruma Ait Banka Hesap Bilgisi Bulunamadı. Lütfen Kurum Bilgilerini Güncelleyiniz...");
                        }
                    }

                }
                else if (status == ((int)ClaimStatus.IPTAL).ToString())
                {
                    if (claim.PayrollId != null)
                    {
                        var payroll = new GenericRepository<Payroll>().FindById((long)claim.PayrollId);
                        if (payroll.Status != ((int)PayrollStatus.Onay_Bekler).ToString())
                        {
                            throw new Exception("Onay Bekler Durumundaki Zarf Haricinde, Onay veya Ödendi Durumundaki Zarftaki Hasar Durumu PROVIZYON'a Çekilemez!");
                        }
                    }
                    if (claim.Status != ((int)ClaimStatus.RET).ToString() && claim.Status != ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                    {
                        throw new Exception("Hasar Durumu Uygun Değil");
                    }
                }
                else if (status == ((int)ClaimStatus.ODENDI).ToString())
                {
                    if (claim.Status != ((int)ClaimStatus.ODENECEK).ToString())
                    {
                        throw new Exception("Hasar Durumu Uygun Değil");
                    }
                    var claimBillList = new GenericRepository<ClaimBill>().FindBy($"CLAIM_ID ={claim.Id}", orderby: "CLAIM_ID");
                    if (claimBillList == null || claimBillList.Count <= 0)
                    {
                        throw new Exception("Hasara Ait Fatura Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                    }
                    else
                    {
                        foreach (ClaimBill claimBill in claimBillList)
                        {
                            if (claimBill.Status != Convert.ToString((int)BillStatus.ODENDI))
                            {
                                claimBill.Status = ((int)BillStatus.ODENDI).ToString();
                                new GenericRepository<ClaimBill>().Update(claimBill);
                            }
                        }
                    }
                    if (claim.PayrollId == null || claim.PayrollId <= 0)
                    {
                        throw new Exception("Hasara Ait Zarf Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                    }
                    else
                    {
                        var payrollList = new GenericRepository<V_Payroll>().FindBy($"PAYROLL_ID ={claim.PayrollId}", orderby: "PAYROLL_ID", fetchDeletedRows: true);
                        if (payrollList == null || payrollList.Count <= 0)
                        {
                            throw new Exception("Hasara Ait Zarf Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                        }
                        else
                        {
                            foreach (V_Payroll payroll in payrollList)
                            {
                                if (payroll.STATUS != Convert.ToString((int)PayrollStatus.Onay))
                                {
                                    throw new Exception("Hasara Ait Zarf Durumu Onay olmadan hasar durumunu ÖDENDİ'ye çekemezsiniz. Lütfen Hasarı Güncelleyiniz...");
                                }
                            }
                        }
                    }
                }
                else
                {
                    throw new Exception("Belirtilen Status Değeri Bulunamadı");
                }

                claim.Status = status;
                claim.ReasonId = null;
                SpResponse spResponse = new GenericRepository<ProteinEntities.Claim>().UpdateForAll(claim);
                result.ResultCode = spResponse.Code;
                result.ResultMessage = spResponse.Message;
                if (spResponse.Code == "100")
                {
                    result.ResultCode = "100";
                    result.ResultMessage = "Hasar Durumu Başarıyla Değiştirildi.";

                    if (claim.CompanyId != null && (claim.CompanyId == 95 || claim.CompanyId == 10))
                    {
                        if (claim.Status == ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                        {
                            proxyService.ClaimApproval(stateUpdateReq);
                        }
                        else if (claim.Status == ((int)ClaimStatus.RET).ToString())
                        {
                            proxyService.ClaimRejection(stateUpdateReq);
                        }
                        else if (claim.Status == ((int)ClaimStatus.IPTAL).ToString())
                        {
                            proxyService.ClaimCancellation(stateUpdateReq);
                        }
                        else if (claim.Status == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || claim.Status == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                        {
                            proxyService.ClaimWait(stateUpdateReq);
                        }
                    }
                    else if (claim.CompanyId == 322)
                    {
                        string newStatus = "";
                        if (claim.Status == ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                        {
                            newStatus = "1";
                        }
                        else if (claim.Status == ((int)ClaimStatus.RET).ToString())
                        {
                            newStatus = "4";
                        }
                        else if (claim.Status == ((int)ClaimStatus.IPTAL).ToString())
                        {
                            newStatus = "2";
                        }
                        else if (claim.Status == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || claim.Status == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                        {
                            newStatus = "3";
                        }

                        if (newStatus.IsInt())
                        {
                            NNSetProvisionStatusReq nNSetProvisionStatusReq = new NNSetProvisionStatusReq
                            {
                                ProvisionNumber = claimId.ToString(),
                                ProvisionNumberSpecified = true,
                                NewStatu = newStatus,
                                NewStatuSpecified = true,
                                ReasonDesc = "",
                                ReasonDescSpecified = true
                            };
                            new NNHayatServiceClientV2().SetProvisionStatus(nNSetProvisionStatusReq);
                        }
                    }
                    else if (claim.CompanyId == 5)
                    {
                        DogaServiceClient dogaServiceClient = new DogaServiceClient();
                        dogaServiceClient.HasarAktar("78cc5f34-42fe-40c3-9baf-8c45d8aada97", claim.Id);
                    }

                    if (status == ((int)ClaimStatus.ODENDI).ToString())
                    {
                        var claimPayment = new GenericRepository<ClaimPayment>().FindBy($"CLAIM_ID ={claim.Id}").FirstOrDefault();
                        if (claimPayment != null)
                        {
                            claimPayment.PaymentDate = DateTime.Now;
                            new GenericRepository<ClaimPayment>().Update(claimPayment);
                        }

                        var otherClaim = new GenericRepository<ProteinEntities.Claim>().FindBy($"PAYROLL_ID={claim.PayrollId}");
                        if (otherClaim.Where(c => c.Status != ((int)ClaimStatus.ODENDI).ToString()).FirstOrDefault() == null)
                        {
                            var payroll = new GenericRepository<Payroll>().FindBy($"ID ={claim.PayrollId}", orderby: "ID", fetchDeletedRows: true).FirstOrDefault();
                            payroll.Status = ((int)PayrollStatus.Odendi).ToString();
                            payroll.PaymentDate = DateTime.Now;
                            SpResponse spResponsePayroll = new GenericRepository<Payroll>().Update(payroll);
                            if (spResponsePayroll.Code == "100")
                            {
                                result.ResultMessage = "Hasar ödeme işlemi başarıyla tamamlandı. Hasar Durumu Ödendi'ye Alındı. Bağlı Bulunduğu Zarf İçerisindeki Tüm Hasarlar Ödendiği İçin Zarf Ödendi Durumuna Alındı.";
                            }
                        }
                    }

                    if (isMailSend)
                    {
                        var vClaim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID={claim.Id}", orderby: "", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();

                        var dbIcds = new GenericRepository<V_ClaimIcd>().FindBy($"CLAIM_ID={claim.Id}", orderby: "");// ViewHelper.GetView(Views.ClaimICD, "CLAIM_ID =: id", parameters: new { id = claimId });
                        var icdNameList = "";
                        if (dbIcds != null && dbIcds.Count > 0)
                        {
                            icdNameList = string.Join(", ", dbIcds.Select(i => i.ICD_NAME));
                        }

                        long medicineProcessListId = 0;
                        var medicineProcessList = ProcessListHelper.GetProcessList("İLAÇ");
                        if (medicineProcessList != null)
                        {
                            medicineProcessListId = medicineProcessList.Id;
                        }

                        var dbClaimProcesses = new GenericRepository<V_ClaimProcess>().FindBy($"CLAIM_ID ={claim.Id} AND PROCESS_LIST_ID !={medicineProcessListId}"); //ViewHelper.GetView(Views.ClaimProcess, "", parameters: new { id = claimId, processListId = medicineProcessListId });
                        var processNameList = "";
                        if (dbClaimProcesses != null && dbClaimProcesses.Count > 0)
                        {
                            processNameList = string.Join(", ", dbIcds.Select(i => i.ICD_NAME));
                        }

                        MailSender msender = new MailSender();
                        msender.SendMessage(
                            new EmailArgs
                            {
                                TO = "fulya@imecedestek.com",
                                IsHtml = true,
                                Subject = $"Yüksek Tutarlı Hasar Bilgilendirme",
                                Content = $"{vClaim.CLAIM_ID} nolu provizyon yuksek hasarlidir. Kontrol ediniz! </br></br> Hasar Statusu : PROV </br> Tutar : {vClaim.PAID} TL </br> Şirket : {vClaim.COMPANY_NAME} </br> Policeno : {vClaim.POLICY_NUMBER} </br> Provizyon No : {vClaim.CLAIM_ID} </br> Sigortali : {vClaim.FIRST_NAME} {vClaim.LAST_NAME} </br> Sigorta Ettiren : {vClaim.INSURER_NAME} </br> Kurum : {vClaim.PROVIDER_NAME} </br> Hasar Tarihi : {vClaim.CLAIM_DATE} </br> İşlemler : {processNameList} </br> Tanılar : {icdNameList}",
                            });
                        msender.SendMessage(
                            new EmailArgs
                            {
                                TO = "deniz@imecedestek.com",
                                IsHtml = true,
                                Subject = $"Yüksek Tutarlı Hasar Bilgilendirme",
                                Content = $"{vClaim.CLAIM_ID} nolu provizyon yuksek hasarlidir. Kontrol ediniz! </br></br> Hasar Statusu : PROV </br> Tutar : {vClaim.PAID} TL </br> Şirket : {vClaim.COMPANY_NAME} </br> Policeno : {vClaim.POLICY_NUMBER} </br> Provizyon No : {vClaim.CLAIM_ID} </br> Sigortali : {vClaim.FIRST_NAME} {vClaim.LAST_NAME} </br> Sigorta Ettiren : {vClaim.INSURER_NAME} </br> Kurum : {vClaim.PROVIDER_NAME} </br> Hasar Tarihi : {vClaim.CLAIM_DATE} </br> İşlemler : {processNameList} </br> Tanılar : {icdNameList}",
                            });
                        msender.SendMessage(
                            new EmailArgs
                            {
                                TO = "bilgiprovizyon@imecedestek.com",
                                IsHtml = true,
                                Subject = $"Yüksek Tutarlı Hasar Bilgilendirme",
                                Content = $"{vClaim.CLAIM_ID} nolu provizyon yuksek hasarlidir. Kontrol ediniz! </br></br> Hasar Statusu : PROV </br> Tutar : {vClaim.PAID} TL </br> Şirket : {vClaim.COMPANY_NAME} </br> Policeno : {vClaim.POLICY_NUMBER} </br> Provizyon No : {vClaim.CLAIM_ID} </br> Sigortali : {vClaim.FIRST_NAME} {vClaim.LAST_NAME} </br> Sigorta Ettiren : {vClaim.INSURER_NAME} </br> Kurum : {vClaim.PROVIDER_NAME} </br> Hasar Tarihi : {vClaim.CLAIM_DATE} </br> İşlemler : {processNameList} </br> Tanılar : {icdNameList}",
                            });
                    }
                }


            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [LoginControl]
        public JsonResult AllProcessICDs(Int64 claimId, List<string> icdIds)
        {
            var IcdList = new GenericRepository<V_ClaimIcd>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
            if (IcdList.Count <= 0)
            {
                throw new Exception("Hasara Ait Tanı Bilgisi Bulunamadı!");
            }

            //List<ClaimProcessIcd> claimProcessIcds = new ClaimProcessIcdRepository().FindBy(conditions:);

            List<dynamic> claimProcessIcdList = new List<dynamic>();

            foreach (var item in IcdList)
            {
                dynamic listItem = new System.Dynamic.ExpandoObject();
                listItem.CLAIM_ICD_ID = Convert.ToString(item.CLAIM_ICD_ID);
                listItem.CLAIM_ID = Convert.ToString(item.CLAIM_ID);
                listItem.ICD_CODE = Convert.ToString(item.ICD_CODE);
                listItem.ICD_ID = Convert.ToString(item.ICD_ID);
                listItem.ICD_NAME = item.ICD_NAME;
                listItem.ICD_TYPE = item.ICD_TYPE;
                listItem.ICD_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.ICD, item.ICD_TYPE);
                listItem.STATUS = item.STATUS;
                listItem.SELECTED = "0";
                //foreach (var claimProcessIcd in claimProcessIcds)
                //{
                //    if (item.ICD_ID == claimProcessIcd.IcdId)
                //    {
                //        listItem.SELECTED = "1";
                //    }
                //}

                claimProcessIcdList.Add(listItem);
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = claimProcessIcdList.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }


        [HttpPost]
        [LoginControl]
        public JsonResult SaveAllClaimProcessICD(Int64 claimProcessId, List<string> icdIds, Int64 claimId)
        {
            var result = new AjaxResultDto<ClaimProcessICDResult>();

            var IcdList = new GenericRepository<V_ClaimIcd>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
            if (IcdList.Count <= 0)
            {
                throw new Exception("Hasara Ait Tanı Bilgisi Bulunamadı!");
            }

            List<ClaimProcessIcd> claimProcessIcds = new ClaimProcessIcdRepository().FindBy("CLAIM_PROCESS_ID = :claimProcessId", parameters: new { claimProcessId });

            List<dynamic> claimProcessIcdList = new List<dynamic>();

            foreach (var item in IcdList)
            {
                dynamic listItem = new System.Dynamic.ExpandoObject();
                listItem.CLAIM_ICD_ID = Convert.ToString(item.CLAIM_ICD_ID);
                listItem.CLAIM_ID = Convert.ToString(item.CLAIM_ID);
                listItem.ICD_CODE = Convert.ToString(item.ICD_CODE);
                listItem.ICD_ID = Convert.ToString(item.ICD_ID);
                listItem.ICD_NAME = item.ICD_NAME;
                listItem.ICD_TYPE = item.ICD_TYPE;
                listItem.ICD_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.ICD, item.ICD_TYPE);
                listItem.STATUS = item.STATUS;
                listItem.SELECTED = "0";
                foreach (var claimProcessIcd in claimProcessIcds)
                {
                    if (item.ICD_ID == claimProcessIcd.IcdId)
                    {
                        listItem.SELECTED = "1";
                    }
                }

                claimProcessIcdList.Add(listItem);
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = claimProcessIcdList.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [LoginControl]
        public JsonResult SaveClaimProcessICD(string claimProcessId, List<string> icdIds)
        {
            var result = new AjaxResultDto<ClaimProcessICDResult>();
            try
            {

                if (claimProcessId.IsNull())
                {
                    throw new Exception("Seçilen işlem bulunamadı!");
                }
                List<Int64> claimProcessIdList = new List<long>();
                foreach (var item in claimProcessId.Split(','))
                {
                    if (item.IsInt64())
                    {
                        claimProcessIdList.Add(long.Parse(item));
                    }
                }

                if (icdIds == null)
                {
                    icdIds = new List<string>();
                }
                foreach (var item in claimProcessIdList)
                {
                    // Find firstly claimProcessIcds that should be excluded
                    List<ClaimProcessIcd> claimProcessIcdList = new GenericRepository<ClaimProcessIcd>().FindBy($"CLAIM_PROCESS_ID={item}");
                    if (claimProcessIcdList != null && claimProcessIcdList.Count > 0)
                    {
                        foreach (ClaimProcessIcd claimProcessIcd in claimProcessIcdList)
                        {
                            bool isFound = false;
                            foreach (string strIcdId in icdIds)
                            {
                                if (!string.IsNullOrEmpty(strIcdId))
                                {
                                    long icdId = long.Parse(strIcdId);
                                    if (claimProcessIcd.IcdId == icdId)
                                    {
                                        isFound = true;
                                    }
                                }
                            }
                            if (!isFound)
                            {
                                claimProcessIcd.Status = Convert.ToString((int)Status.SILINDI);
                                SpResponse spResponse = new GenericRepository<ClaimProcessIcd>().Update(claimProcessIcd);
                                result.ResultCode = spResponse.Code;
                                result.ResultMessage = spResponse.Message;
                                if (spResponse.Code != "100")
                                {
                                    result.ResultCode = "198";
                                    result.ResultMessage = "Hasar tanı işlem eşleştirmesi sırasında hata oluştu! : (ClaimProcessIcdId=" + claimProcessIcd.Id + ")";
                                }
                            }
                        }
                    }

                    // Add new icds to claimProcesses secondly
                    claimProcessIcdList = new GenericRepository<ClaimProcessIcd>().FindBy($"CLAIM_PROCESS_ID={item}");
                    foreach (string strIcdId in icdIds)
                    {
                        if (!string.IsNullOrEmpty(strIcdId))
                        {
                            long icdId = long.Parse(strIcdId);
                            bool isFound = false;
                            foreach (ClaimProcessIcd claimProcessIcd in claimProcessIcdList)
                            {
                                if (claimProcessIcd.IcdId == icdId)
                                {
                                    isFound = true;
                                }
                            }
                            if (!isFound)
                            {
                                var vClaimIcd = new GenericRepository<V_ClaimIcd>().FindBy($"ICD_ID={icdId}", orderby: "CLAIM_ID").FirstOrDefault();
                                ClaimProcessIcd claimProcessIcd = new ClaimProcessIcd()
                                {
                                    Id = 0,
                                    ClaimProcessId = item,
                                    IcdId = icdId,
                                    IcdType = vClaimIcd.ICD_TYPE,
                                    Status = Convert.ToString((int)Status.AKTIF)
                                };
                                SpResponse spResponse = new GenericRepository<ClaimProcessIcd>().Insert(claimProcessIcd);
                                result.ResultCode = spResponse.Code;
                                result.ResultMessage = spResponse.Message;
                                if (spResponse.Code != "100")
                                {
                                    result.ResultCode = "199";
                                    result.ResultMessage = "Hasar tanı işlem eşleştirmesi sırasında hata oluştu! : (ClaimProcessId=" + claimProcessIcd + ",IcdId=" + icdId + ")";
                                }
                            }
                        }
                    }
                }
                result.ResultCode = "100";
                result.ResultMessage = "Tanı Eşleştirme İşlemi Başarıyla Gerçekleştirildi.";
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [LoginControl]
        public ActionResult Create(Int64 claimId = 0, Int64 insuredId = 0, Int64 policyId = 0, Int64 packageId = 0, Int64 contactId = 0)
        {
            MediaVM vm = new MediaVM();
            vm.MediaObjectType = Business.Enums.Media.MediaObjectType.T_CLAIM_MEDIA;
            vm.ObjectId = claimId;
            vm.ShowDateInfo = true;
            vm.TabName = "tab10";

            // Edit View
            if (claimId > 0)
            {
                var vClaim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID={claimId}", orderby: "CLAIM_ID DESC", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                if (vClaim != null)
                {
                    ViewBag.CompanyId = vClaim.COMPANY_ID;
                    ViewBag.ProviderId = vClaim.PROVIDER_ID;
                    ViewBag.InsuredId = vClaim.INSURED_ID;
                    ViewBag.ContactId = vClaim.CONTACT_ID;
                    ViewBag.PolicyId = vClaim.POLICY_ID;
                    ViewBag.PackageId = vClaim.PACKAGE_ID;
                    ViewBag.ClaimPaymentMethod = vClaim.PAYMENT_METHOD;
                    ViewBag.ClaimType = vClaim.CLAIM_TYPE;
                    ViewBag.InsuredName = vClaim.FIRST_NAME + " " + vClaim.LAST_NAME;
                    ViewBag.NoticeDate = ((DateTime)vClaim.NOTICE_DATE).ToString("dd-MM-yyyy HH:mm");
                    ViewBag.ClaimDate = ((DateTime)vClaim.CLAIM_DATE).ToString("dd-MM-yyyy HH:mm");
                    ViewBag.ClaimStatus = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, vClaim.STATUS);
                    ViewBag.ClaimStatusOrdinal = vClaim.STATUS;
                    ViewBag.MedulaNo = vClaim.MEDULA_NO;
                    ViewBag.CompanyClaimId = vClaim.COMPANY_CLAIM_ID;
                    ViewBag.CompanyClaimNo = vClaim.COMPANY_CLAIM_NO;
                    ViewBag.Reason = vClaim.REASON_DESCRIPTION; // ReasonHelper.GetReasonbyId(long.Parse(vClaim.REASON_ID)).Description;
                    ViewBag.ReasonId = vClaim.REASON_ID; // ReasonHelper.GetReasonbyId(long.Parse(vClaim.REASON_ID)).Description;
                    ViewBag.EventDate = string.IsNullOrEmpty(Convert.ToString(vClaim.EVENT_DATE)) ? "" : ((DateTime)vClaim.EVENT_DATE).ToString("dd-MM-yyyy");
                    ViewBag.EventType = vClaim.EVENT_TYPE;
                    ViewBag.EventDescription = vClaim.EVENT_DESCRIPTION;
                    ViewBag.BillId = vClaim.CLAIM_BILL_ID;
                    ViewBag.BillNo = vClaim.BILL_NO;
                    ViewBag.BillDate = string.IsNullOrEmpty(Convert.ToString(vClaim.BILL_DATE)) ? "" : ((DateTime)vClaim.BILL_DATE).ToString("dd-MM-yyyy");
                    ViewBag.BillPaymentType = vClaim.BILL_PAYMENT_TYPE;
                    ViewBag.BillCurrencyType = vClaim.BILL_EXCHANGE_CURRENCY_TYPE;
                    ViewBag.BillStatus = vClaim.BILL_STATUS;
                    ViewBag.PayrollId = vClaim.PAYROLL_ID;

                    ViewBag.InsuredIdentityNo = vClaim.IDENTITY_NO;
                    ViewBag.PolicyNumber = vClaim.POLICY_NUMBER;
                    ViewBag.PolicyStartDate = vClaim.POLICY_START_DATE;
                    ViewBag.PolicyEndDate = vClaim.POLICY_END_DATE;
                    ViewBag.FirstInsuredDate = vClaim.FIRST_INSURED_DATE;

                    if (vClaim.PAYROLL_ID != null && vClaim.PAYROLL_ID > 0)
                    {
                        string payrollNo = string.IsNullOrEmpty(Convert.ToString(vClaim.PAYROLL_ID)) ? "-" : Convert.ToString(vClaim.PAYROLL_ID);
                        string payrollDate = string.IsNullOrEmpty(Convert.ToString(vClaim.PAYROLL_DATE)) ? "-" : ((DateTime)vClaim.PAYROLL_DATE).ToString("dd-MM-yyyy");
                        string payrollType = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Payroll, vClaim.PAYROLL_TYPE);
                        payrollType = string.IsNullOrEmpty(payrollType) ? "-" : payrollType;
                        string payrollCategory = LookupHelper.GetLookupTextByOrdinal(LookupTypes.PayrollCategory, vClaim.PAYROLL_CATEGORY);
                        payrollCategory = string.IsNullOrEmpty(payrollCategory) ? "-" : payrollCategory;
                        ViewBag.PayrollDescription = $"No : {payrollNo} / Tarih : {payrollDate} / Tip : {payrollType} / Kategori : {payrollCategory}";
                    }
                    ViewBag.ClaimId = claimId;
                }
                else
                {
                    return RedirectToAction("Index", "Insured");
                }
            }
            // Create View
            else
            {
                if (insuredId <= 0 || policyId <= 0)
                {
                    return RedirectToAction("Index", "Insured");
                }
                ViewBag.ProviderId = 0;
                ViewBag.InsuredId = insuredId;
                ViewBag.ContactId = contactId;
                ViewBag.PolicyId = policyId;
                ViewBag.PackageId = packageId;
                ViewBag.ClaimPaymentMethod = Convert.ToString((int)ClaimPaymentMethod.KURUM);

                var insuredClaimInfo = new GenericRepository<V_InsuredClaim>().FindBy("INSURED_ID=:insuredId", parameters: new { insuredId }, orderby: "INSURED_ID").FirstOrDefault();
                if (insuredClaimInfo != null)
                {
                    ViewBag.CompanyId = insuredClaimInfo.COMPANY_ID;
                    ViewBag.ClaimType = insuredClaimInfo.PACKAGE_TYPE;
                    ViewBag.InsuredName = insuredClaimInfo.FIRST_NAME + " " + insuredClaimInfo.LAST_NAME;
                    ViewBag.InsuredIdentityNo = insuredClaimInfo.IDENTITY_NO;
                    ViewBag.PolicyNumber = insuredClaimInfo.POLICY_NUMBER;
                    ViewBag.PolicyStartDate = insuredClaimInfo.POLICY_START_DATE;
                    ViewBag.PolicyEndDate = insuredClaimInfo.POLICY_END_DATE;
                    ViewBag.FirstInsuredDate = insuredClaimInfo.FIRST_INSURED_DATE;
                }


                //var package = new GenericRepository<V_Package>().FindBy($"PACKAGE_ID={packageId}", orderby: "").FirstOrDefault();
                //if (package != null)
                //{
                //    if (package.PRODUCT_TYPE == ((int)ProductType.TSS).ToString() || package.PRODUCT_TYPE == ((int)ProductType.MDP).ToString())
                //        ViewBag.ClaimType = Convert.ToString((int)ClaimType.TSS);
                //    else
                //        ViewBag.ClaimType = Convert.ToString((int)ClaimType.OSS);
                //}
                //else
                //    ViewBag.ClaimType = Convert.ToString((int)ClaimType.OSS);

                ViewBag.EventType = "";
                ViewBag.BillCurrencyType = Convert.ToString((int)CurrencyType.TURK_LIRASI);
                ViewBag.BillPaymentType = Convert.ToString((int)PaymentType.BANKAYA_ODEME);
            }

            ViewBag.PricingTypeList = LookupHelper.GetLookupData(LookupTypes.Pricing);

            ViewBag.ProviderTypeList = LookupHelper.GetLookupData(LookupTypes.Provider);
            var ProviderList = new GenericRepository<V_Provider>().FindBy(orderby: "PROVIDER_ID");
            ViewBag.ProviderList = ProviderList;

            ViewBag.ClaimPaymentMethodList = LookupHelper.GetLookupData(LookupTypes.ClaimPaymentMethod);

            ViewBag.ClaimTypeList = LookupHelper.GetLookupData(LookupTypes.Claim);

            ViewBag.NoteTypeList = LookupHelper.GetLookupData(LookupTypes.ClaimNote);
            ViewBag.ICDTypeList = LookupHelper.GetLookupData(LookupTypes.ICD);

            var ICD10ProcessList = ProcessListHelper.GetProcessList("ICD10");
            if (ICD10ProcessList != null)
            {
                ViewBag.ICD10ProcessListId = ICD10ProcessList.Id;
            }
            else
            {
                ViewBag.ICD10ProcessListId = 0;
            }

            ViewBag.StaffContractTypeList = LookupHelper.GetLookupData(LookupTypes.StaffContract);

            ViewBag.EventTypeList = LookupHelper.GetLookupData(LookupTypes.Event);

            ViewBag.PaymentTypeList = LookupHelper.GetLookupData(LookupTypes.Payment);

            ViewBag.CurrencyTypeList = LookupHelper.GetLookupData(LookupTypes.Currency);
            ViewBag.BillCurrencyType = "";

            ViewBag.BillStatusList = LookupHelper.GetLookupData(LookupTypes.BillStatus);

            ViewBag.PayrollStatusList = LookupHelper.GetLookupData(LookupTypes.PayrollStatus, showChoose: true);
            ViewBag.PayrollTypeList = LookupHelper.GetLookupData(LookupTypes.Payroll);
            ViewBag.PayrollCategoryList = LookupHelper.GetLookupData(LookupTypes.PayrollCategory);

            ViewBag.ClaimMissingDocTypeList = LookupHelper.GetLookupData(LookupTypes.ClaimMissingDoc);

            ViewBag.AgreementTypeList = LookupHelper.GetLookupData(LookupTypes.Agreement);
            ViewBag.ExemptionTypeList = LookupHelper.GetLookupData(LookupTypes.Exemption);

            return View(vm);
        }

        [HttpPost]
        [LoginControl]
        public JsonResult Create(Int64 claimId, Int64 insuredId, Int64 policyId, Int64 packageId, Int64 contactId, Int64 companyId, FormCollection form)
        {
            MediaVM vm = new MediaVM
            {
                MediaObjectType = Business.Enums.Media.MediaObjectType.T_CLAIM_MEDIA,
                ObjectId = claimId,
                ShowDateInfo = true,
                TabName = "tab10"
            };

            var result = new AjaxResultDto<ClaimSaveStep1Result>();
            ClaimSaveStep1Result step1Result = new ClaimSaveStep1Result();
            try
            {
                var claim = new ProteinEntities.Claim();

                if (form["claimType"] != form["hdDefaultClaimType"])
                {
                    throw new Exception("Hasar Tipi ile Bağlı olduğu ürünün tipi Uyumsuz!");
                }
                // Create
                if (claimId <= 0)
                {
                    var insured = new GenericRepository<Insured>().FindById(insuredId);
                    if (insured == null)
                    {
                        throw new Exception("Sigortalı bilgisi bulunamadı!");
                    }
                    var providerId = form["providerId"];
                    if (string.IsNullOrEmpty(providerId) || long.Parse(providerId) <= 0)
                    {
                        throw new Exception("Lütfen kurumu seçiniz!");
                    }
                    var policy = new GenericRepository<V_Policy>().FindBy($"POLICY_ID={policyId}", orderby: "").FirstOrDefault();
                    if (policy == null)
                    {
                        throw new Exception("Aktif Poliçe Bilgisi Bulunamadı!");
                    }
                    else if (policy.POLICY_END_DATE < DateTime.Parse(form["claimDate"]) || policy.POLICY_START_DATE > DateTime.Parse(form["claimDate"]))
                    {
                        throw new Exception("Poliçe Girilen Hasar Tarihinde Geçerli Değildir! Lütfen Hasar Tarihini Kontrol Ediniz...");
                    }
                    var paymentMethod = form["claimPaymentMethod"];
                    // Hasar Tipi PortTSS'den gelirse Medula No set edilmiş olacağı için claimType TSS olacaktır.
                    // Ekrandan girişlerde default ÖSS seçili gelecek, güncellemede değiştirilebilecek.
                    claim.Id = 0;
                    claim.InsuredId = insuredId;
                    claim.ProviderId = long.Parse(providerId);
                    claim.NoticeDate = DateTime.Now;
                    claim.ClaimDate = DateTime.Parse(form["claimDate"]);
                    claim.PaymentMethod = paymentMethod;
                    claim.Type = form["claimType"];
                    claim.SourceType = Convert.ToString((int)ClaimSourceType.PROVIZYON_MERKEZI);
                    claim.PolicyId = policyId;
                    claim.PackageId = packageId;
                    claim.CompanyId = policy.COMPANY_ID;
                    if (companyId == 95 || companyId == 10)
                    {
                        ProxyServiceClient serviceClient = new ProxyServiceClient();
                        PolicyStateReq policyStateReq = new PolicyStateReq
                        {
                            CompanyCode = companyId.ToString(),
                            IncidentDate = DateTime.Parse(form["claimDate"]),
                            InsuredNo = insured.CompanyInsuredNo,
                            PolicyNo = policy.POLICY_NUMBER.ToString(),
                            RenewalNo = policy.RENEWAL_NO,
                        };
                        var policyStateRes = serviceClient.PolicyState(policyStateReq);
                        if (!policyStateRes.IsOk)
                        {
                            throw new Exception($"Şirket Tarafından ({(policyStateRes.Error.Length > 0 ? (string.Join(", ", policyStateRes.Error)) : policyStateRes.Description)}) bu nedenlerden dolayı hasar oluşturulamadı!");
                        }
                    }
                }
                // Edit
                else
                {
                    claim.Id = claimId;
                    claim.Type = form["claimType"];
                }

                var spResponseClaim = new GenericRepository<ProteinEntities.Claim>().Insert(claim);
                if (spResponseClaim.Code == "100")
                {
                    claimId = spResponseClaim.PkId;
                    step1Result.claimId = claimId;
                    step1Result.claimStatus = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, Convert.ToString((int)ClaimStatus.GIRIS));
                    step1Result.claimStatusOrdinal = (int)ClaimStatus.GIRIS;
                    step1Result.provisionStep = 0;

                    var vClaim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID={claimId}", orderby: "CLAIM_ID DESC", fetchDeletedRows: true, hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                    if (vClaim != null)
                    {
                        if (vClaim.PAYMENT_METHOD == ((int)ClaimPaymentMethod.SIGORTALI).ToString() && vClaim.CONTRACT_ID == null)
                        {
                            result.ResultCode = "860";
                        }
                        else
                        {
                            result.ResultCode = spResponseClaim.Code;
                        }
                        step1Result.CompanyId = (long)vClaim.COMPANY_ID;
                        step1Result.CompanyName = vClaim.COMPANY_NAME;
                        step1Result.providerName = vClaim.PROVIDER_NAME;
                        step1Result.InsuredId = (long)vClaim.INSURED_ID;
                        step1Result.paymentMethod = vClaim.PAYMENT_METHOD;
                    }

                    result.Data = step1Result;
                    result.ResultMessage = spResponseClaim.Message;
                }
                else
                {
                    if (spResponseClaim.PkId > 0)
                    {
                        claimId = spResponseClaim.PkId;
                        step1Result.claimId = claimId;

                        step1Result.provisionStep = 0;

                        var vClaim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID={claimId}", orderby: "CLAIM_ID DESC", fetchDeletedRows: true, hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                        if (vClaim != null)
                        {
                            step1Result.claimStatus = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, vClaim.STATUS);
                            step1Result.claimStatusOrdinal = int.Parse(vClaim.STATUS);
                            step1Result.CompanyId = (long)vClaim.COMPANY_ID;
                            step1Result.CompanyName = vClaim.COMPANY_NAME;
                            step1Result.ReasonDesc = vClaim.REASON_DESCRIPTION;
                            step1Result.providerName = vClaim.PROVIDER_NAME;
                            step1Result.InsuredId = (long)vClaim.INSURED_ID;
                            step1Result.paymentMethod = vClaim.PAYMENT_METHOD;
                        }

                        result.Data = step1Result;
                        result.ResultCode = "760";
                        result.ResultMessage = spResponseClaim.Message + " - Bu Sebeple Hasar İptal Statüsünde Kaydedilmiştir.";
                    }
                    else
                    {
                        throw new Exception(spResponseClaim.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }

            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ClaimCancel(Int64 claimId)
        {
            var result = new AjaxResultDto<ClaimSaveStep1Result>();
            ClaimSaveStep1Result step1Result = new ClaimSaveStep1Result();
            try
            {
                var claim = new GenericRepository<ProteinEntities.Claim>().FindById(claimId);
                if (claim != null)
                {
                    claim.Status = ((int)ClaimStatus.IPTAL).ToString();
                    claim.ReasonId = 175;
                    var spResponseClaim = new GenericRepository<ProteinEntities.Claim>().Insert(claim);
                    if (spResponseClaim.Code == "100")
                    {
                        claimId = spResponseClaim.PkId;
                        step1Result.claimId = claimId;
                        step1Result.claimStatus = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, Convert.ToString((int)ClaimStatus.IPTAL));
                        step1Result.claimStatusOrdinal = (int)ClaimStatus.IPTAL;
                        step1Result.provisionStep = 0;

                        var vClaim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID={claimId}", orderby: "CLAIM_ID DESC", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                        if (vClaim != null)
                        {
                            step1Result.CompanyId = (long)vClaim.COMPANY_ID;
                            step1Result.CompanyName = vClaim.COMPANY_NAME;
                            step1Result.providerName = vClaim.PROVIDER_NAME;
                            step1Result.ReasonDesc = vClaim.REASON_DESCRIPTION;
                            step1Result.InsuredId = (long)vClaim.INSURED_ID;
                            step1Result.paymentMethod = vClaim.PAYMENT_METHOD;
                        }
                        result.ResultCode = spResponseClaim.Code;
                        result.Data = step1Result;
                        result.ResultMessage = spResponseClaim.Message;
                    }
                    else
                    {
                        if (spResponseClaim.PkId > 0)
                        {
                            claimId = spResponseClaim.PkId;
                            step1Result.claimId = claimId;

                            step1Result.provisionStep = 0;

                            var vClaim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID={claimId}", orderby: "CLAIM_ID DESC", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                            if (vClaim != null)
                            {
                                step1Result.claimStatus = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, vClaim.STATUS);
                                step1Result.claimStatusOrdinal = int.Parse(vClaim.STATUS);
                                step1Result.CompanyId = (long)vClaim.COMPANY_ID;
                                step1Result.CompanyName = vClaim.COMPANY_NAME;
                                step1Result.ReasonDesc = vClaim.REASON_DESCRIPTION;
                                step1Result.providerName = vClaim.PROVIDER_NAME;
                                step1Result.InsuredId = (long)vClaim.INSURED_ID;
                                step1Result.paymentMethod = vClaim.PAYMENT_METHOD;
                            }

                            result.Data = step1Result;
                            result.ResultCode = "100";
                            result.ResultMessage = spResponseClaim.Message + " - Bu Sebeple Hasar İptal Statüsünde Kaydedilmiştir.";
                        }
                        else
                        {
                            throw new Exception(spResponseClaim.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }

            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetBankList(Int64 claimId)
        {
            var result = new AjaxResultDto<InsuredBankSaveResult>();
            InsuredBankSaveResult step1Result = new InsuredBankSaveResult();
            try
            {
                if (claimId <= 0)
                    throw new Exception("Hasar Bilgisi Bulunamadı");

                var claim = new GenericRepository<ProteinEntities.Claim>().FindBy($"ID=:claimId", orderby: "ID", parameters: new { claimId }).FirstOrDefault();
                if (claim == null)
                    throw new Exception("Hasar Bilgisi Bulunamadı");

                List<dynamic> banksList = new List<dynamic>();

                var claimPayment = new GenericRepository<V_ClaimPayment>().FindBy(conditions: "CLAIM_ID=:claimId", orderby: "CLAIM_ID", parameters: new { claimId }).FirstOrDefault();

                if (claim.PaymentMethod == ((int)ClaimPaymentMethod.KURUM).ToString())
                {
                    var providerBankList = new GenericRepository<V_ProviderBank>().FindBy("PROVIDER_ID=:providerId", orderby: "BANK_ACCOUNT_ID DESC", parameters: new { providerId = claim.ProviderId });
                    if (providerBankList != null)
                    {
                        foreach (var item in providerBankList)
                        {
                            dynamic listItem = new System.Dynamic.ExpandoObject();

                            listItem.BANK_ACCOUNT_ID = Convert.ToString(item.BANK_ACCOUNT_ID);
                            listItem.BANK_NAME = item.BANK_NAME;

                            listItem.BANK_BRANCH_NAME = item.BANK_BRANCH_NAME;
                            listItem.BANK_ACCOUNT_NO = item.BANK_ACCOUNT_NO;
                            listItem.IBAN = item.IBAN;

                            if (claimPayment != null && claimPayment.BANK_ACCOUNT_ID != null)
                            {
                                if (item.BANK_ACCOUNT_ID == claimPayment.BANK_ACCOUNT_ID)
                                {
                                    listItem.isClaimPayment = "1";
                                }
                            }
                            else if (item.IS_PRIMARY == "1" && claim.Status != ((int)ClaimStatus.IPTAL).ToString() && claim.Status != ((int)ClaimStatus.RET).ToString())
                            {
                                V_Claim v_Claim = new GenericRepository<V_Claim>().FindBy("CLAIM_ID=:claimId", orderby: "", parameters: new { claimId }, hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                                if (v_Claim != null)
                                {
                                    ClaimPayment TclaimPayment = new ClaimPayment
                                    {
                                        BankAccountId = item.BANK_ACCOUNT_ID,
                                        ClaimId = claim.Id,
                                        Amount = v_Claim.PAID,
                                        PaymentType = v_Claim.BILL_PAYMENT_TYPE.IsInt() ? v_Claim.BILL_PAYMENT_TYPE : ((int)PaymentType.BANKAYA_ODEME).ToString(),
                                        DueDate = v_Claim.PAYROLL_DUE_DATE
                                    };
                                    SpResponse spResClaimPay = new GenericRepository<ClaimPayment>().Insert(TclaimPayment);
                                    if (spResClaimPay.Code == "100")
                                    {
                                        listItem.isClaimPayment = "1";
                                        claimPayment = new GenericRepository<V_ClaimPayment>().FindBy(conditions: "CLAIM_ID=:claimId", orderby: "CLAIM_ID", parameters: new { claimId }).FirstOrDefault();
                                    }
                                }
                            }

                            banksList.Add(listItem);
                        }

                        result.Data = new InsuredBankSaveResult
                        {
                            jsonBankData = banksList.ToJSON(),
                        };
                    }
                }
                else
                {
                    var contactBankList = new GenericRepository<V_InsuredBankAccount>().FindBy("INSURED_ID=:insuredId", orderby: "BANK_ACCOUNT_ID DESC", parameters: new { insuredId = claim.InsuredId });
                    if (contactBankList != null)
                    {
                        foreach (var item in contactBankList)
                        {
                            dynamic listItem = new System.Dynamic.ExpandoObject();

                            listItem.BANK_ACCOUNT_ID = Convert.ToString(item.BANK_ACCOUNT_ID);
                            listItem.BANK_NAME = item.BANK_NAME;

                            listItem.BANK_BRANCH_NAME = item.BANK_BRANCH_NAME;
                            listItem.BANK_ACCOUNT_NO = item.BANK_ACCOUNT_NO;
                            listItem.IBAN = item.IBAN;

                            if (claimPayment != null && claimPayment.BANK_ACCOUNT_ID != null)
                            {
                                if (item.BANK_ACCOUNT_ID == claimPayment.BANK_ACCOUNT_ID)
                                {
                                    listItem.isClaimPayment = "1";
                                }
                            }
                            else if (item.IS_PRIMARY == "1" && claim.Status != ((int)ClaimStatus.IPTAL).ToString() && claim.Status != ((int)ClaimStatus.RET).ToString())
                            {
                                V_Claim v_Claim = new GenericRepository<V_Claim>().FindBy("CLAIM_ID=:claimId", orderby: "CLAIM_ID", parameters: new { claimId }, hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                                if (v_Claim != null)
                                {
                                    ClaimPayment TclaimPayment = new ClaimPayment
                                    {
                                        BankAccountId = item.BANK_ACCOUNT_ID,
                                        ClaimId = claim.Id,
                                        Amount = v_Claim.PAID,
                                        PaymentType = v_Claim.BILL_PAYMENT_TYPE.IsInt() ? v_Claim.BILL_PAYMENT_TYPE : ((int)PaymentType.BANKAYA_ODEME).ToString(),
                                        DueDate = v_Claim.PAYROLL_DUE_DATE
                                    };
                                    SpResponse spResClaimPay = new GenericRepository<ClaimPayment>().Insert(TclaimPayment);
                                    if (spResClaimPay.Code == "100")
                                    {
                                        listItem.isClaimPayment = "1";
                                        claimPayment = new GenericRepository<V_ClaimPayment>().FindBy(conditions: "CLAIM_ID=:claimId", orderby: "CLAIM_ID", parameters: new { claimId }).FirstOrDefault();
                                    }
                                }
                            }

                            banksList.Add(listItem);
                        }

                        result.Data = new InsuredBankSaveResult
                        {
                            jsonBankData = banksList.ToJSON(),
                        };
                    }
                }

                result.ResultCode = "100";
                result.ResultMessage = "";
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }

            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveBill(Int64 claimId, FormCollection form)
        {
            var result = new AjaxResultDto<BillSaveResult>();
            try
            {
                if (claimId < 1)
                    throw new Exception("Hasar Bilgisi Bulunamadı");
                if (!form["billDate"].IsDateTime())
                    throw new Exception("Fatura Tarihi Girilmesi Gerekmektedir");
                if (form["billNo"].IsNull())
                    throw new Exception("Fatura No Girilmesi Gerekmektedir");
                //if (form["billStatus"].IsNull())
                //    throw new Exception("Fatura Durumu Seçilmesi Gerekmektedir");

                var claimRepository = new GenericRepository<ProteinEntities.Claim>();
                ProteinEntities.Claim claim = claimRepository.FindById(claimId);
                if (claim != null)
                {

                    if (!string.IsNullOrEmpty(form["billDate"]))
                    {
                        claim.ClaimDate = DateTime.Parse(((DateTime)claim.ClaimDate).ToString("dd.MM.yyyy"));
                        DateTime billDateTime = DateTime.Parse(form["billDate"]);
                        if (DateTime.Compare((DateTime)claim.ClaimDate, billDateTime) > 0)
                        {
                            throw new Exception("Fatura tarihi hasar tarihinden küçük olamaz!");
                        }
                    }
                    ClaimBill claimBill = new ClaimBill
                    {
                        Id = (form["ClaimBillId"].IsInt64() && long.Parse(form["ClaimBillId"]) > 0) ? long.Parse(form["ClaimBillId"]) : 0,
                        ClaimId = claimId,
                        BillNo = form["billNo"],
                        BillDate = DateTime.Parse(form["billDate"]),
                        PaymentType = form["billPaymentType"],
                        // Status = form["billStatus"],
                        Status = "0",
                        ExchangeRateId = (form["hdExchangeRateId"].IsInt64() && long.Parse(form["hdExchangeRateId"]) > 0) ? (long?)long.Parse(form["hdExchangeRateId"]) : null
                    };
                    SpResponse spResponse = new GenericRepository<ClaimBill>().Insert(claimBill);
                    if (spResponse.Code != "100")
                        throw new Exception("Fatura Bilgisi Oluşturulamadı");

                    result.ResultCode = spResponse.Code;
                    result.ResultMessage = spResponse.Message;
                    result.Data = new BillSaveResult
                    {
                        claimBillId = spResponse.PkId.ToString()
                    };
                }
                else
                {
                    throw new Exception($"{claimId} nolu hasar bulunamadı!");
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveClaimPayment(long claimId, string bankAccountId)
        {
            var result = new AjaxResultDto<BillSaveResult>();
            try
            {
                var claim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID={claimId}", orderby: "", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();

                if (claim.STATUS != ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                {
                    throw new Exception("Hasar Durumu Uygun Değil");
                }

                var claimBillList = new GenericRepository<V_ClaimBill>().FindBy($"CLAIM_ID ={claimId}", orderby: "CLAIM_ID");
                if (claimBillList == null || claimBillList.Count <= 0)
                {
                    throw new Exception("Hasara Ait Fatura Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                }

                V_Payroll Vpayroll = new V_Payroll();
                if (claim.PAYROLL_ID == null || claim.PAYROLL_ID <= 0)
                {
                    throw new Exception("Hasara Ait Zarf Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                }
                else
                {
                    Vpayroll = new GenericRepository<V_Payroll>().FindBy($"PAYROLL_ID ={claim.PAYROLL_ID}", orderby: "PAYROLL_ID", fetchDeletedRows: true).FirstOrDefault();
                    if (Vpayroll == null)
                    {
                        throw new Exception("Hasara Ait Zarf Bilgisi Bulunamadı. Lütfen Hasarı Güncelleyiniz...");
                    }
                }

                ClaimPayment claimPayment = new GenericRepository<ClaimPayment>().FindBy($"CLAIM_ID ={claimId}").FirstOrDefault();

                claimPayment = new ClaimPayment
                {
                    Id = claimPayment != null ? claimPayment.Id : 0,
                    ClaimId = claimId,
                    DueDate = Vpayroll.DUE_DATE,
                    PaymentDate = null,
                    BankAccountId = bankAccountId.IsInt64() ? (long?)long.Parse(bankAccountId) : null,
                    PaymentType = claim.BILL_PAYMENT_TYPE.IsInt() ? claim.BILL_PAYMENT_TYPE : ((int)PaymentType.BANKAYA_ODEME).ToString(),
                    Amount = claim.PAID
                };
                SpResponse spResponse = new GenericRepository<ClaimPayment>().Insert(claimPayment);
                if (spResponse.Code != "100")
                    throw new Exception("Banka Bilgisi Kayıt Edilemedi. Lütfen Tekrar Deneyiniz...");

                result.ResultCode = "100";
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PayrollExit(Int64 claimId)
        {
            var result = new AjaxResultDto<PayrollExitResult>();
            try
            {
                if (claimId < 1)
                    throw new Exception("Hasar Bilgisi Bulunamadı");

                var claim = new GenericRepository<ProteinEntities.Claim>().FindById(claimId);
                if (claim == null)
                    throw new Exception("Hasar Bilgisi Bulunamadı");

                claim.PayrollId = null;
                if (claim.Status == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                {
                    if (claim.ReasonId != null)
                    {
                        // TODO - Provizyon Bekleme durumu için bekleme gerekçesi olan hasarların tamamı statik olarak Eksik evrak nedenine bağlandı
                        //        Veritabanından eşlenmesi daha uygun olacaktır.
                        Reason reason = ReasonHelper.GetReasonData("ClaimStatus", ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString(), "3");
                        claim.ReasonId = reason.Id;
                    }
                    claim.Status = ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString();
                }
                SpResponse spResponseClaim = new GenericRepository<ProteinEntities.Claim>().UpdateForAll(claim);
                if (spResponseClaim.Code != "100")
                    throw new Exception("Hasarı Zarftan Çıkarma İşlemi Gerçekleştirilemedi");

                result.ResultCode = spResponseClaim.Code;
                result.ResultMessage = "Hasar Zarftan Başarıyla Çıkartıldı.";
                result.Data = new PayrollExitResult
                {
                    payrollId = spResponseClaim.PkId.ToString()
                };
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult FilterPayroll(FormCollection form)
        {
            object result = "[]";

            var claim = new GenericRepository<ProteinEntities.Claim>().FindBy("ID=:id", parameters: new { id = form["hdClaimId"] }).FirstOrDefault();
            if (claim != null)
            {
                dynamic parameters = new System.Dynamic.ExpandoObject();
                parameters.companyId = claim.CompanyId;
                parameters.payrollType = claim.PaymentMethod;
                parameters.payrollCategory = claim.Type;

                String whereConditition = " COMPANY_ID=:companyId AND PAYROLL_TYPE =:payrollType AND PAYROLL_CATEGORY =:payrollCategory AND ";

                if (claim.PaymentMethod == ((int)ClaimPaymentMethod.KURUM).ToString())
                {
                    parameters.providerId = claim.ProviderId;
                    whereConditition += " PROVIDER_ID =:providerId AND";
                }
                //else
                //{
                //    parameters.providerId = form["payrollSelectProvider"];
                //    whereConditition += !String.IsNullOrEmpty(form["payrollSelectProvider"]) ? $" PROVIDER_ID =:providerId AND" : "";
                //}

                whereConditition += !String.IsNullOrEmpty(form["payrollSelectNo"]) ? $" PAYROLL_ID={long.Parse(form["payrollSelectNo"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["payrollSelectDate"]) ? $" PAYROLL_DATE = TO_DATE('{ DateTime.Parse(form["payrollSelectDate"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";

                var resultClaimBill = new GenericRepository<V_Payroll>().FindBy((string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)), orderby: "PAYROLL_ID", parameters: parameters);
                result = resultClaimBill;
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        public JsonResult SelectPayroll(FormCollection form)
        {
            var result = new AjaxResultDto<PayrollSaveResult>();
            try
            {
                if (!form["radioPayroll"].IsInt64() || long.Parse(form["radioPayroll"]) < 1)
                    throw new Exception("Zarf Bilgisi Bulunamadı");

                if (!form["hdClaimId"].IsInt64() || long.Parse(form["hdClaimId"]) < 1)
                    throw new Exception("Hasar Bilgisi Bulunamadı");

                var payroll = new GenericRepository<Payroll>().FindById(long.Parse(form["radioPayroll"]));
                if (payroll == null)
                    throw new Exception("Zarf Bilgisi Bulunamadı");

                if (payroll.Status == ((int)PayrollStatus.Odendi).ToString())
                    throw new Exception("Ödenen Zarf içerisine yeni Hasar Eklenemez!");

                var claimlist = new GenericRepository<V_Claim>().FindBy($"(CLAIM_ID = {long.Parse(form["hdClaimId"])} OR PAYROLL_ID={payroll.Id})", orderby: "", hint: "/*+ USE_HASH(claim_id) ORDERED */");
                if (claimlist == null && claimlist.Count < 1)
                    throw new Exception("Hasar Bilgisi Bulunamadı");

                var selectClaim = claimlist.Where(c => (c.CLAIM_ID == long.Parse(form["hdClaimId"]))).FirstOrDefault();
                if (selectClaim == null)
                    throw new Exception("Hasar Bilgisi Bulunamadı");

                if (selectClaim.STATUS != ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() && selectClaim.STATUS != ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString() && selectClaim.STATUS != ((int)ClaimStatus.PROZIYON_ONAY).ToString() && selectClaim.STATUS != ((int)ClaimStatus.RET).ToString() && selectClaim.STATUS != ((int)ClaimStatus.ODENECEK).ToString())
                {
                    throw new Exception("Hasar Durumu Uygun Değil.");
                }

                if (selectClaim.PAID == null || selectClaim.PAID < 1)
                    throw new Exception("Hasara ait Şirket Tutarı Boş ya da 0 olamaz. Lütfen Teminat bilgilerini Kontrol Ediniz...");
                if (selectClaim.CLAIM_BILL_ID == null)
                    throw new Exception("Hasara ait Fatura Bilgisi Bulunamadı. Fatura Bilgilerini Kontrol Ediniz...");

                if ((payroll.Category == ((int)PayrollCategory.OSS).ToString() && selectClaim.CLAIM_TYPE != ((int)ClaimType.OSS).ToString()))
                    throw new Exception("Kategorisi OSS olan bir zarfa farklı tipte bir hasar eklenemez");

                if ((payroll.Category == ((int)PayrollCategory.TSS).ToString() && selectClaim.CLAIM_TYPE != ((int)ClaimType.TSS).ToString()))
                    throw new Exception("Kategorisi TSS olan bir zarfa farklı tipte bir hasar eklenemez");

                if (payroll.Type == ((int)PayrollType.KURUM).ToString())
                {
                    if (selectClaim.PAYMENT_METHOD != ((int)ClaimPaymentMethod.KURUM).ToString())
                        throw new Exception("Tipi KURUM olan bir zarfa sadece ödeme yöntemi KURUM olan hasarlar eklenebilir!");

                    if (selectClaim.PROVIDER_ID != payroll.ProviderId)
                        throw new Exception("Tipi KURUM olan bir zarfa farklı farklı kurumdan bir hasar eklenemez");
                }
                if (payroll.Type == ((int)PayrollType.SIGORTALI).ToString())
                {
                    if (selectClaim.PAYMENT_METHOD != ((int)ClaimPaymentMethod.SIGORTALI).ToString())
                        throw new Exception("Tipi SİGORTALI olan bir zarfa sadece ödeme yöntemi SİGORTALI olan hasarlar eklenebilir!");

                    var insuredBankAccount = new GenericRepository<V_InsuredBankAccount>().FindBy($"INSURED_ID={selectClaim.INSURED_ID}", orderby: "");
                    if (insuredBankAccount == null || insuredBankAccount.Count <= 0)
                    {
                        throw new Exception("Sigortalıya Ait Banka Hesap Bilgisi Bulunamadı. Lütfen Sigortalı Bilgilerini Güncelleyiniz...");
                    }

                    var differentClaim = claimlist.Where(c => c.CLAIM_ID != selectClaim.CLAIM_ID && c.INSURED_ID == selectClaim.INSURED_ID).FirstOrDefault();
                    if (differentClaim == null)
                    {
                        differentClaim = claimlist.Where(c => c.CLAIM_ID != selectClaim.CLAIM_ID && c.FAMILY_NO == selectClaim.FAMILY_NO).FirstOrDefault();
                        if (differentClaim == null)
                        {
                            throw new Exception("Tipi SIGORTALI olan bir zarfa farklı sigortalı hasarı eklenemez.");
                        }
                    }
                }
                if (selectClaim.MEDULA_NO != null)
                {
                    var payrollClaimList = claimlist.Where(c => (c.CLAIM_ID != long.Parse(form["hdClaimId"]))).ToList();
                    foreach (var item in payrollClaimList)
                    {
                        if (item.MEDULA_NO != null)
                        {
                            if (item.MEDULA_NO == selectClaim.MEDULA_NO)
                            {
                                throw new Exception("Aynı Medula No'ya ait hasarlar aynı zarfın içinde bulanamaz.");
                            }
                        }
                    }
                }
                var claim = new GenericRepository<ProteinEntities.Claim>().FindById(long.Parse(form["hdClaimId"]));

                claim.PayrollId = long.Parse(form["radioPayroll"]);
                SpResponse spResponseClaim = new GenericRepository<ProteinEntities.Claim>().Update(claim);
                if (spResponseClaim.Code != "100")
                    throw new Exception("Hasara Zarf Ekleme İşlemi Gerçekleştirilemedi");


                result.ResultCode = spResponseClaim.Code;
                result.ResultMessage = "Hasara Zarf Ekleme İşlemi Başarıyla Gerçekleştirildi.";
                result.Data = new PayrollSaveResult
                {
                    payrollId = payroll.Id.ToString(),
                    payrollDescription = $"No:{payroll.Id.ToString()} - Tarih:{form["payrollDate"]} - Tip:{LookupHelper.GetLookupTextByOrdinal(LookupTypes.Payroll, payroll.Type)} - Kategori:{LookupHelper.GetLookupTextByOrdinal(LookupTypes.PayrollCategory, payroll.Category)}"
                };
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SavePayroll(Int64 claimId, FormCollection form)
        {
            var result = new AjaxResultDto<PayrollSaveResult>();
            try
            {
                if (claimId < 1)
                    throw new Exception("Hasar Bilgisi Bulunamadı");

                if (form["PayrollId"].IsInt64() && long.Parse(form["PayrollId"]) > 0)
                {
                    var oldPayroll = new GenericRepository<Payroll>().FindById(long.Parse(form["PayrollId"]));
                    if (oldPayroll == null)
                        throw new Exception("Zarf Bilgisi Bulunamadı");
                    if (oldPayroll.Status != ((int)PayrollStatus.Onay_Bekler).ToString())
                        throw new Exception("Zarf Durumu Güncellemek İçin Uygun Değil!");
                }

                var Vclaim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID={claimId}", orderby: "", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                if (Vclaim == null)
                    throw new Exception("Hasar Bilgisi Bulunamadı");
                if (Vclaim.PAID == null || Vclaim.PAID < 1)
                    throw new Exception("Hasara ait Şirket Tutarı Boş ya da 0 olamaz. Lütfen Teminat bilgilerini Kontrol Ediniz...");
                if (Vclaim.CLAIM_BILL_ID == null)
                    throw new Exception("Hasara ait Fatura Bilgisi Bulunamadı. Fatura Bilgilerini Kontrol Ediniz...");
                if (Vclaim.STATUS != ((int)ClaimStatus.PROZIYON_ONAY).ToString() && Vclaim.STATUS != ((int)ClaimStatus.RET).ToString() && Vclaim.STATUS != ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() && Vclaim.STATUS != ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString() && Vclaim.STATUS != ((int)ClaimStatus.ODENECEK).ToString())
                {
                    throw new Exception("Hasar Durumu Uygun Değil! Lütfen Hasar Durumunu Kontrol Ediniz...");
                }



                if (form["payrollCategory"].IsInt())
                {
                    if (Vclaim.CLAIM_TYPE != form["payrollCategory"])
                    {
                        throw new Exception("Hasara Tipi ile Zarf Kategorisi Uyumsuz! Zarf Bilgilerini kontrol Ediniz...");
                    }
                }
                var payrollType = form["payrollType"];
                if (payrollType.IsInt())
                {
                    if (Vclaim.PAYMENT_METHOD != payrollType)
                    {
                        throw new Exception("Hasar Ödeme Tipi ile Zarf Tipi Uyumsuz! Zarf Bilgilerini kontrol Ediniz...");
                    }

                    if (payrollType == ((int)PayrollType.KURUM).ToString() && Vclaim.PROVIDER_ID != long.Parse(form["ProviderId"]))
                    {
                        throw new Exception("Farklı Kurum Bilgisi ile Zarf Oluşturulamaz! Zarf Bilgilerini kontrol Ediniz...");
                    }
                }

                Payroll payroll = new Payroll
                {
                    Id = (form["PayrollId"].IsInt64() && long.Parse(form["PayrollId"]) > 0) ? long.Parse(form["PayrollId"]) : 0,
                    CompanyId = (form["CompanyId"].IsInt64() && long.Parse(form["CompanyId"]) > 0) ? (long?)long.Parse(form["CompanyId"]) : null,
                    PayrollDate = form["payrollDate_Modal"].IsDateTime() ? (DateTime?)DateTime.Parse(form["payrollDate_Modal"]) : null,
                    Type = !form["payrollType"].IsNull() ? form["payrollType"] : null,
                    Category = !form["payrollCategory"].IsNull() ? form["payrollCategory"] : null,
                    ProviderId = (form["ProviderId"].IsInt64() && long.Parse(form["ProviderId"]) > 0) ? (long?)long.Parse(form["ProviderId"]) : null,
                    ExtProviderNo = !form["payrollProviderNo"].IsNull() ? form["payrollProviderNo"] : null,
                    ExtProviderDate = form["payrollProviderDate"].IsDateTime() ? (DateTime?)DateTime.Parse(form["payrollProviderDate"]) : null,
                    Due_Date = form["payrollPaymentDate"].IsDateTime() ? (DateTime?)DateTime.Parse(form["payrollPaymentDate"]) : null,
                    Status = !form["payrollStatus"].IsNull() ? form["payrollStatus"] : null,
                    CompanyPayrollNo = !form["CompanyPayrollNo"].IsNull() ? form["CompanyPayrollNo"] : null,
                };
                SpResponse spResponse = new GenericRepository<Payroll>().Insert(payroll);
                if (spResponse.Code != "100")
                    throw new Exception("Zarf Bilgisi Oluşturulamadı");

                var claim = new GenericRepository<ProteinEntities.Claim>().FindById(claimId);

                claim.PayrollId = spResponse.PkId;
                SpResponse spResponseClaim = new GenericRepository<ProteinEntities.Claim>().Update(claim);
                if (spResponse.Code != "100")
                    throw new Exception("Hasara Zarf Ekleme İşlemi Gerçekleştirilemedi");

                result.ResultCode = spResponse.Code;
                result.ResultMessage = "Zarf Başarıyla Oluşturuldu";
                result.Data = new PayrollSaveResult
                {
                    payrollId = spResponse.PkId.ToString(),
                    payrollDescription = $"No:{spResponse.PkId.ToString()} - Tarih:{form["payrollDate"]} - Tip:{LookupHelper.GetLookupTextByOrdinal(LookupTypes.Payroll, form["payrollType"])} - Kategori:{LookupHelper.GetLookupTextByOrdinal(LookupTypes.PayrollCategory, form["payrollCategory"])}"
                };
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ClaimSaveStep2(Int64 claimId, FormCollection form)
        {
            var result = new AjaxResultDto<ClaimSaveStep2Result>();
            ClaimSaveStep2Result step2Result = new ClaimSaveStep2Result();
            try
            {
                var claimIcdId = form["hdClaimIcdId"];
                var icds = form["hdIcdIds"];
                var icdTypes = form["hdIcdType"];

                JavaScriptSerializer js = new JavaScriptSerializer();
                string icdType = js.Deserialize<string>(icdTypes);

                if (!string.IsNullOrEmpty(claimIcdId) && int.Parse(claimIcdId) > 0)
                {
                    if (!string.IsNullOrEmpty(icdType))
                    {
                        var claimIcd = new ClaimIcd
                        {
                            Id = long.Parse(claimIcdId),
                            IcdType = icdType
                        };
                        var spResponseClaimIcd = new ClaimIcdRepository().Insert(claimIcd);
                        if (spResponseClaimIcd.Code == "100")
                        {
                            result.ResultCode = spResponseClaimIcd.Code;
                            result.ResultMessage = spResponseClaimIcd.Message;
                        }
                        else
                        {
                            throw new Exception(spResponseClaimIcd.Message);
                        }
                    }
                }
                else
                {
                    string[] icdIdArray = js.Deserialize<string[]>(icds);

                    foreach (string icdId in icdIdArray)
                    {
                        if (!string.IsNullOrEmpty(icdId))
                        {
                            if (!string.IsNullOrEmpty(icdType))
                            {
                                var claimIcd = new ClaimIcd
                                {
                                    Id = 0,
                                    ClaimId = claimId,
                                    IcdId = long.Parse(icdId),
                                    IcdType = icdType
                                };
                                var spResponseClaimIcd = new ClaimIcdRepository().Insert(claimIcd);
                                if (spResponseClaimIcd.Code == "100")
                                {
                                    result.ResultCode = spResponseClaimIcd.Code;
                                    result.ResultMessage = spResponseClaimIcd.Message;
                                }
                                else
                                {
                                    throw new Exception(spResponseClaimIcd.Message);
                                }
                            }
                        }
                    }
                }

                List<V_ClaimIcd> dbIcds = new GenericRepository<V_ClaimIcd>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                if (dbIcds != null)
                {
                    List<dynamic> icdsList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in dbIcds)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.CLAIM_ICD_ID = Convert.ToString(item.CLAIM_ICD_ID);
                        listItem.ICD_ID = Convert.ToString(item.ICD_ID);
                        listItem.ICD_CODE = item.ICD_CODE;
                        listItem.ICD_NAME = item.ICD_NAME;
                        listItem.ICD_TYPE = item.ICD_TYPE;
                        listItem.ICD_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.ICD, item.ICD_TYPE);

                        icdsList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimSaveStep2Result
                    {
                        jsonData = icdsList.ToJSON(false)
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ClaimIcdSave(Int64 claimId, FormCollection form)
        {
            var result = new AjaxResultDto<ClaimSaveStep2Result>();
            ClaimSaveStep2Result step2Result = new ClaimSaveStep2Result();
            try
            {
                if (claimId < 1)
                {
                    result.ResultCode = "999";
                    result.ResultMessage = "Hasar Bilgisi Bulunamadı!";
                }
                else
                {
                    var icdId = form["selectIcdId"];
                    var icdtype = form["ICD_TYPE_" + icdId];

                    if (!icdId.IsInt64())
                    {
                        result.ResultCode = "999";
                        result.ResultMessage = "Tanı Bilgisi Bulunamadı!";
                    }
                    else if (!icdtype.IsInt())
                    {
                        result.ResultCode = "999";
                        result.ResultMessage = "Tanı Tipi Seçiniz!";
                    }
                    else
                    {
                        var claimIcd = new ClaimIcd
                        {
                            Id = 0,
                            ClaimId = claimId,
                            IcdId = long.Parse(icdId),
                            IcdType = icdtype
                        };
                        var spResponseClaimIcd = new ClaimIcdRepository().Insert(claimIcd);
                        if (spResponseClaimIcd.Code == "100")
                        {
                            result.ResultCode = spResponseClaimIcd.Code;
                            result.ResultMessage = spResponseClaimIcd.Message;
                        }
                        else
                        {
                            throw new Exception(spResponseClaimIcd.Message);
                        }

                        List<V_ClaimIcd> dbIcds = new GenericRepository<V_ClaimIcd>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                        if (dbIcds != null)
                        {
                            List<dynamic> icdsList = new List<dynamic>();

                            int i = 1;

                            foreach (var item in dbIcds)
                            {
                                dynamic listItem = new System.Dynamic.ExpandoObject();

                                listItem.id = i;
                                listItem.ClientId = i;
                                listItem.STATUS = item.STATUS;

                                listItem.CLAIM_ICD_ID = Convert.ToString(item.CLAIM_ICD_ID);
                                listItem.ICD_ID = Convert.ToString(item.ICD_ID);
                                listItem.ICD_CODE = Convert.ToString(item.ICD_CODE);
                                listItem.ICD_NAME = Convert.ToString(item.ICD_NAME);
                                listItem.ICD_TYPE = Convert.ToString(item.ICD_TYPE);
                                listItem.ICD_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.ICD, item.ICD_TYPE);

                                icdsList.Add(listItem);

                                i++;
                            }

                            result.Data = new ClaimSaveStep2Result
                            {
                                jsonData = icdsList.ToJSON(false)
                            };
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }


        public JsonResult ICDs(Int64 claimId)
        {
            var result = new AjaxResultDto<ClaimSaveStep2Result>();
            ClaimSaveStep2Result step2Result = new ClaimSaveStep2Result();
            try
            {
                List<V_ClaimIcd> dbIcds = new GenericRepository<V_ClaimIcd>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                if (dbIcds != null)
                {
                    result.ResultCode = "100";
                    result.ResultMessage = "";

                    List<dynamic> icdsList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in dbIcds)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.CLAIM_ICD_ID = Convert.ToString(item.CLAIM_ICD_ID);
                        listItem.ICD_ID = Convert.ToString(item.ICD_ID);
                        listItem.ICD_CODE = item.ICD_CODE;
                        listItem.ICD_NAME = item.ICD_NAME;
                        listItem.ICD_TYPE = item.ICD_TYPE;
                        listItem.ICD_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.ICD, item.ICD_TYPE);

                        icdsList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimSaveStep2Result
                    {
                        jsonData = icdsList.ToJSON(false)
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Payment(Int64 claimId)
        {
            var result = new AjaxResultDto<ClaimSaveStep2Result>();
            ClaimSaveStep2Result step2Result = new ClaimSaveStep2Result();
            try
            {
                List<V_ClaimPayment> dbPaymentDetails = new GenericRepository<V_ClaimPayment>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                if (dbPaymentDetails != null)
                {
                    result.ResultCode = "100";
                    result.ResultMessage = "";

                    List<dynamic> paymentList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in dbPaymentDetails)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.PAYMENT_TYPE = item.PAYMENT_TYPE;
                        listItem.PAYMENT_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Payment, item.PAYMENT_TYPE);
                        listItem.DUE_DATE = item.DUE_DATE;
                        listItem.PAYMENT_DATE = item.PAYMENT_DATE;
                        listItem.AMOUNT = item.AMOUNT;
                        listItem.BANK_ACCOUNT_NAME = item.BANK_ACCOUNT_NAME;
                        listItem.IBAN = item.IBAN;

                        paymentList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimSaveStep2Result
                    {
                        jsonData = paymentList.ToJSON(false)
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [LoginControl]
        public JsonResult CreateContactNote(FormCollection form)
        {
            var contactId = form["hdContactId"];

            var Tnote = new Note
            {
                Type = form["NOTE_TYPE"],
                Description = form["NOTE_DESCRIPTION"].RemoveRepeatedWhiteSpace()
            };
            var spResponseNote = new NoteRepository().Insert(Tnote);
            if (spResponseNote.Code == "100")
            {
                var noteId = spResponseNote.PkId;
                var contactNote = new ContactNote
                {
                    ContactId = long.Parse(contactId),
                    NoteId = noteId
                };
                var spResponseContactNote = new GenericRepository<ContactNote>().Insert(contactNote);
            }


            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject("[]"),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }
        [HttpPost]
        public JsonResult MedicalNotes(Int64 contactId)
        {
            object result = null;
            var insuredNoteList = new GenericRepository<V_ContactNote>().FindBy("CONTACT_ID=:contactId", orderby: "CONTACT_ID",
                                    parameters: new { contactId });
            if (insuredNoteList != null)
            {
                List<dynamic> list = new List<dynamic>();

                foreach (var insuredNote in insuredNoteList)
                {
                    //Add root item
                    dynamic insuredNoteItem = new System.Dynamic.ExpandoObject();

                    insuredNoteItem.DESCRIPTION = insuredNote.DESCRIPTION.RemoveRepeatedWhiteSpace();
                    insuredNoteItem.NOTE_ID = insuredNote.NOTE_ID;
                    insuredNoteItem.INSURED_NOTE_ID = insuredNote.CONTACT_NOTE_ID;
                    insuredNoteItem.NOTE_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Note, insuredNote.NOTE_TYPE);
                    string SP_RESPONSE_DATE = "";
                    insuredNoteItem.SP_RESPONSE_DATE = SP_RESPONSE_DATE.IsDateTime() ? DateTime.Parse(SP_RESPONSE_DATE).ToString("dd-MM-yyyy") : "Bulunamadı";
                    insuredNoteItem.USERNAME = "Bulunamadı";
                    insuredNoteItem.STATUS_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.Status, insuredNote.STATUS);
                    insuredNoteItem.STATUS = insuredNote.STATUS;
                    list.Add(insuredNoteItem);
                }
                result = list.ToJSON();
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(result),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult Notes(Int64 claimId)
        {
            var result = new AjaxResultDto<ClaimSaveNoteResult>();
            ClaimSaveNoteResult step6Result = new ClaimSaveNoteResult();
            try
            {
                List<V_ClaimNote> dbNotes = new GenericRepository<V_ClaimNote>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                //ViewHelper.GetView(Views.ClaimNote, "CLAIM_ID=:id", parameters: new { id = claimId });
                if (dbNotes != null)
                {
                    result.ResultCode = "100";
                    result.ResultMessage = "";

                    List<dynamic> notesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in dbNotes)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                        listItem.CLAIM_NOTE_ID = Convert.ToString(item.CLAIM_NOTE_ID);

                        listItem.NOTE_TYPE = item.CLAIM_NOTE_TYPE;
                        listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimNote, item.CLAIM_NOTE_TYPE);
                        listItem.NOTE_DESCRIPTION = item.NOTE_DECRIPTION;

                        notesList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimSaveNoteResult
                    {
                        jsonData = notesList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Doctor(Int64 claimId)
        {
            var result = new AjaxResultDto<ClaimSaveDoctorResult>();
            try
            {
                List<dynamic> doctorList = new List<dynamic>();
                ProteinEntities.Claim claim = new GenericRepository<ProteinEntities.Claim>().FindById(claimId);
                if (claim != null)
                {
                    result.ResultCode = "100";
                    result.ResultMessage = "";
                    if (claim.StaffId != null)
                    {
                        V_Doctor staff = new GenericRepository<V_Doctor>().FindBy("STAFF_ID =: id", orderby: "STAFF_ID", parameters: new { id = claim.StaffId }).FirstOrDefault();
                        //ViewHelper.GetView(Views.Claim, whereCondititon: "CLAIM_ID=:claimId", parameters: new { claimId });
                        if (staff != null)
                        {
                            dynamic listItem = new System.Dynamic.ExpandoObject();

                            listItem.STAFF_ID = staff.STAFF_ID;
                            listItem.FIRST_NAME = staff.FIRST_NAME;
                            listItem.LAST_NAME = staff.LAST_NAME;
                            listItem.DOCTOR_BRANCH_ID = staff.DOCTOR_BRANCH_ID;
                            listItem.DOCTOR_BRANCH_NAME = staff.DOCTOR_BRANCH_NAME;
                            listItem.DIPLOMA_NO = staff.DIPLOMA_NO;
                            listItem.REGISTER_NO = staff.REGISTER_NO;
                            listItem.STAFF_TITLE = staff.STAFF_TITLE;
                            listItem.STAFF_IDENTITY_NO = staff.STAFF_IDENTITY_NO;
                            listItem.CONTRACT_TYPE_TEXT = "DOKTOR";

                            doctorList.Add(listItem);
                        }
                    }
                }
                result.Data = new ClaimSaveDoctorResult
                {
                    jsonData = doctorList.ToJSON()
                };
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetContactPhone(Int64 contactId)
        {
            object phone = "";
            try
            {
                V_ContactPhone contactPhone = new GenericRepository<V_ContactPhone>().FindBy($"CONTACT_ID={contactId} AND IS_PRIMARY='1' AND PHONE_TYPE='{((int)PhoneType.MOBIL).ToString()}'", orderby: "").FirstOrDefault();
                if (contactPhone != null)
                {
                    phone = contactPhone.PHONE_NO;
                }
            }
            catch (Exception)
            {
                phone = "";
            }
            return Json(phone, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Details(Int64 claimId)
        {
            var result = new AjaxResultDto<ClaimSaveDoctorResult>();
            try
            {
                result.ResultCode = "100";
                result.ResultMessage = "";
                List<dynamic> hospitalizationList = new List<dynamic>();
                ClaimHospitalization claimHospitalization = new GenericRepository<ClaimHospitalization>().FindBy("CLAIM_ID=:claimId", orderby: "CLAIM_ID", parameters: new { claimId }).FirstOrDefault();
                if (claimHospitalization != null)
                {

                    dynamic listItem = new System.Dynamic.ExpandoObject();

                    listItem.CLAIM_HOSPITALIZATION_ID = claimHospitalization.Id;
                    listItem.ENTRANCE_DATE = string.IsNullOrEmpty(Convert.ToString(claimHospitalization.EntranceDate)) ? "" : ((DateTime)claimHospitalization.EntranceDate).ToString("dd-MM-yyyy");
                    listItem.EXIT_DATE = string.IsNullOrEmpty(Convert.ToString(claimHospitalization.ExitDate)) ? "" : ((DateTime)claimHospitalization.ExitDate).ToString("dd-MM-yyyy");

                    hospitalizationList.Add(listItem);
                }
                result.Data = new ClaimSaveDoctorResult
                {
                    jsonData = hospitalizationList.ToJSON()
                };
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }
        [LoginControl]
        public JsonResult ExclusionAndTransfer(Int64 insuredId)
        {
            var ExclusionList = new GenericRepository<V_InsuredExclusion>().FindBy("INSURED_ID =:insuredId", orderby: "INSURED_ID", parameters: new { insuredId });
            if (ExclusionList.Count > 0)
            {
                List<dynamic> insuredExclusionList = new List<dynamic>();

                foreach (var item in ExclusionList)
                {
                    dynamic listItem = new System.Dynamic.ExpandoObject();

                    if (item.RULE_CATEGORY_TYPE != null)
                    {
                        listItem.RULE_CATEGORY_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.RuleCategory, item.RULE_CATEGORY_TYPE);
                        string resultt = Convert.ToString(item.RESULT);
                        listItem.RESULT = resultt.IsNull() ? "-" : resultt;
                        string result_second = Convert.ToString(item.RESULT_SECOND);
                        listItem.RESULT_SECOND = result_second.IsNull() ? "-" : result_second;
                    }
                    else
                    {
                        listItem.RULE_CATEGORY_TYPE_TEXT = "EK PRİM";
                        listItem.RESULT = Convert.ToString(item.ADDITIONAL_PREMIUM);
                        listItem.RESULT_SECOND = "-";
                    }
                    listItem.EXCLUSION_DESCRIPTION = Convert.ToString(item.DESCRIPTION);

                    insuredExclusionList.Add(listItem);
                }
                ViewBag.insuredExclusionList = insuredExclusionList;
            }

            var TransferExclusionList = new GenericRepository<V_InsuredNote>().FindBy("INSURED_ID =:insuredId AND NOTE_TYPE =':noteType'", orderby: "INSURED_ID",
                                                                                      parameters: new { insuredId, noteType = ((int)InsuredNoteType.GECİS).ToString() });
            //ViewHelper.GetView(Views.InsuredNote, whereCondititon: $"INSURED_ID ={insuredId} AND NOTE_TYPE={((int)InsuredNoteType.GECİS)}");
            if (TransferExclusionList.Count > 0)
            {
                List<dynamic> notesList = new List<dynamic>();

                foreach (var item in TransferExclusionList)
                {
                    dynamic listItem = new System.Dynamic.ExpandoObject();

                    listItem.NOTE_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.InsuredNote, item.NOTE_TYPE);
                    listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                    notesList.Add(listItem);
                }

                ViewBag.insuredTransferExclusionList = notesList;
            }

            var result = new
            {
                insuredExclusionList = ViewBag.insuredExclusionList,
                insuredTransferExclusionList = ViewBag.insuredTransferExclusionList
            };

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(result),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult NoteAndDecleration(Int64 contactId, Int64 insuredId)
        {
            var NoteList = new GenericRepository<V_ContactNote>().FindBy("CONTACT_ID=:id", orderby: "CONTACT_ID", parameters: new { id = contactId });
            if (NoteList.Count > 0)
            {
                List<dynamic> notesList = new List<dynamic>();

                foreach (var item in NoteList)
                {
                    dynamic listItem = new System.Dynamic.ExpandoObject();

                    listItem.NOTE_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Note, item.NOTE_TYPE);
                    listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                    notesList.Add(listItem);
                }
                ViewBag.insuredNoteList = notesList;
            }

            var DeclerationList = new GenericRepository<V_InsuredNote>().FindBy("INSURED_ID=:insuredId AND NOTE_TYPE=':noteType'", orderby: "INSURED_ID",
                                                                                parameters: new { insuredId, noteType = ((int)InsuredNoteType.BEYAN).ToString() });
            //ViewHelper.GetView(Views.InsuredNote, whereCondititon: $"INSURED_ID={insuredId} AND NOTE_TYPE={((int)InsuredNoteType.BEYAN)}");
            if (DeclerationList.Count > 0)
            {
                List<dynamic> notesList = new List<dynamic>();

                foreach (var item in DeclerationList)
                {
                    dynamic listItem = new System.Dynamic.ExpandoObject();

                    listItem.NOTE_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.InsuredNote, item.NOTE_TYPE);
                    listItem.NOTE_DESCRIPTION = item.DESCRIPTION.RemoveRepeatedWhiteSpace();

                    notesList.Add(listItem);
                }

                ViewBag.insuredDeclerationList = notesList;
            }

            var result = new
            {
                insuredNoteList = ViewBag.insuredNoteList,
                insuredDeclerationList = ViewBag.insuredDeclerationList
            };

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(result),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult DeleteClaimCoverage(Int64 claimId, Int64 claimCoverageId)
        {
            var result = new AjaxResultDto<ClaimSaveStep8Result>();
            ClaimSaveStep8Result step8Result = new ClaimSaveStep8Result();
            try
            {
                var claimCoverage = new ClaimProcess
                {
                    Id = claimCoverageId,
                    Status = Convert.ToString((int)Status.SILINDI)
                };
                var spResponseClaimProcess = new ClaimProcessRepository().Insert(claimCoverage);
                if (spResponseClaimProcess.Code == "100")
                {
                    result.ResultCode = spResponseClaimProcess.Code;
                    result.ResultMessage = spResponseClaimProcess.Message;
                }
                else
                {
                    throw new Exception(spResponseClaimProcess.Message);
                }

                List<V_ClaimProcess> dbClaimCoverages = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                if (dbClaimCoverages != null)
                {
                    List<dynamic> claimCoverageList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in dbClaimCoverages)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.CLAIM_ID = item.CLAIM_ID;
                        listItem.CLAIM_PROCESS_ID = item.CLAIM_PROCESS_ID;
                        listItem.PROCESS_ID = item.PROCESS_ID;
                        listItem.PROCESS_NAME = item.PROCESS_NAME;
                        listItem.COVERAGE_ID = string.IsNullOrEmpty(Convert.ToString(item.COVERAGE_ID)) ? "" : Convert.ToString(item.COVERAGE_ID);
                        listItem.COVERAGE_NAME = item.COVERAGE_NAME;
                        listItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(item.UNIT_AMOUNT).ToString("0.00");
                        listItem.REQUESTED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.REQUESTED_AMOUNT)) ? "" : Convert.ToDecimal(item.REQUESTED_AMOUNT).ToString("0.00");
                        listItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(item.AGREED_AMOUNT).ToString("0.00");
                        listItem.CONTRACT_PROCESS_GROUP_ID = item.CONTRACT_PROCESS_GROUP_ID;
                        listItem.PROCESS_CODE = item.PROCESS_CODE;
                        listItem.SESSION_COUNT = item.SESSION_COUNT;
                        listItem.COUNT = item.COUNT;
                        listItem.DAY = item.DAY;
                        listItem.PROCESS_LIMIT = string.IsNullOrEmpty(Convert.ToString(item.PROCESS_LIMIT)) ? "" : Convert.ToDecimal(item.PROCESS_LIMIT).ToString("0.00");
                        listItem.REQUESTED = string.IsNullOrEmpty(Convert.ToString(item.REQUESTED)) ? "" : Convert.ToDecimal(item.REQUESTED).ToString("0.00");
                        listItem.STOPPAGE = string.IsNullOrEmpty(Convert.ToString(item.STOPAJ)) ? "" : Convert.ToDecimal(item.STOPAJ).ToString("0.00");
                        decimal acceptedAmount = 0;
                        if (item.REQUESTED != null && Convert.ToDecimal(item.REQUESTED) > 0)
                        {
                            if (item.ACCEPTED != null && Convert.ToDecimal(item.ACCEPTED) > 0)
                            {
                                acceptedAmount = Convert.ToDecimal(item.REQUESTED) - Convert.ToDecimal(item.ACCEPTED);
                            }
                            else
                            {
                                acceptedAmount = Convert.ToDecimal(item.REQUESTED);
                            }
                        }
                        listItem.ACCEPTED = acceptedAmount.ToString("0.00");
                        listItem.CONFIRMED = string.IsNullOrEmpty(Convert.ToString(item.CONFIRMED)) ? "" : Convert.ToDecimal(item.CONFIRMED).ToString("0.00");
                        listItem.PAID = string.IsNullOrEmpty(Convert.ToString(item.PAID)) ? "" : Convert.ToDecimal(item.PAID).ToString("0.00");
                        listItem.EXGRACIA = string.IsNullOrEmpty(Convert.ToString(item.EXGRACIA)) ? "" : Convert.ToDecimal(item.EXGRACIA).ToString("0.00");
                        listItem.SGK_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.SGK_AMOUNT)) ? "" : Convert.ToDecimal(item.SGK_AMOUNT).ToString("0.00");
                        decimal insuredAmount = 0;
                        if (item.REQUESTED != null && Convert.ToDecimal(item.REQUESTED) > 0)
                        {
                            if (item.PAID != null && Convert.ToDecimal(item.PAID) > 0)
                            {
                                insuredAmount = Convert.ToDecimal(item.REQUESTED) - Convert.ToDecimal(item.PAID);
                            }
                            else
                            {
                                insuredAmount = Convert.ToDecimal(item.REQUESTED);
                            }
                        }
                        listItem.INSURED_AMOUNT = insuredAmount.ToString("0.00");
                        listItem.COINSURANCE = item.COINSURANCE;
                        listItem.EXEMPTION = string.IsNullOrEmpty(Convert.ToString(item.EXEMPTION)) ? "" : Convert.ToDecimal(item.EXEMPTION).ToString("0.00");
                        listItem.RESULT = item.RESULT;
                        if (!string.IsNullOrEmpty(item.PARTLY_REJECT_DESC))
                        {
                            listItem.RESULT += " - " + item.PARTLY_REJECT_DESC;
                        }
                        listItem.PROCESS_LIST_ID = item.PROCESS_LIST_ID;

                        claimCoverageList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimSaveStep8Result
                    {
                        jsonData = claimCoverageList.ToJSON(false)
                    };
                }
                else
                {
                    throw new Exception("Hasar teminatları getirilirken hata oluştu!");
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteICD(Int64 claimId, Int64 claimIcdId)
        {
            var result = new AjaxResultDto<ClaimSaveStep2Result>();
            ClaimSaveStep2Result step2Result = new ClaimSaveStep2Result();
            try
            {
                List<V_ClaimIcd> dbIcds = new GenericRepository<V_ClaimIcd>().FindBy("CLAIM_ID =:id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                if (dbIcds == null || dbIcds.Count == 1)
                {
                    List<V_ClaimProcess> dbClaimCoverages = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                    if (dbClaimCoverages.Count > 0)
                    {
                        throw new Exception("Tüm Tanıları Silmek İçin Lütfen Öncelikle İşlemleri Siliniz.");
                    }
                }

                var claimIcd = new GenericRepository<ClaimIcd>().FindById(claimIcdId);
                if (claimIcd == null)
                {
                    throw new Exception("Tanı Bilgisi Bulunamadı!");
                }
                claimIcd.Status = ((int)Status.SILINDI).ToString();
                var spResponseClaimIcd = new ClaimIcdRepository().Insert(claimIcd);
                if (spResponseClaimIcd.Code == "100")
                {
                    result.ResultCode = spResponseClaimIcd.Code;
                    result.ResultMessage = spResponseClaimIcd.Message;
                }
                else
                {
                    throw new Exception(spResponseClaimIcd.Message);
                }

                dbIcds = new GenericRepository<V_ClaimIcd>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                //ViewHelper.GetView(Views.ClaimICD, "CLAIM_ID =: id", parameters: new { id = claimId });
                if (dbIcds != null)
                {
                    List<dynamic> icdsList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in dbIcds)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.CLAIM_ICD_ID = Convert.ToString(item.CLAIM_ICD_ID);
                        listItem.ICD_ID = Convert.ToString(item.ICD_ID);
                        listItem.ICD_CODE = item.ICD_CODE;
                        listItem.ICD_NAME = item.ICD_NAME;
                        listItem.ICD_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.ICD, item.ICD_TYPE);

                        icdsList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimSaveStep2Result
                    {
                        jsonData = icdsList.ToJSON(false)
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = new ClaimSaveStep2Result();
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteMedicine(Int64 claimId, Int64 claimMedicineId)
        {
            var result = new AjaxResultDto<ClaimSaveStep2Result>();
            ClaimSaveStep2Result step2Result = new ClaimSaveStep2Result();
            try
            {
                long medicineProcessListId = 0;
                var medicineProcessList = ProcessListHelper.GetProcessList("İLAÇ");
                if (medicineProcessList != null)
                {
                    medicineProcessListId = medicineProcessList.Id;
                }
                else
                {
                    throw new Exception("İlaç listesi bulunamadı!");
                }

                var claimProcess = new ClaimProcess
                {
                    Id = claimMedicineId,
                    Status = Convert.ToString((int)Status.SILINDI)
                };
                var spResponseClaimProcess = new ClaimProcessRepository().Insert(claimProcess);
                if (spResponseClaimProcess.Code == "100")
                {
                    result.ResultCode = spResponseClaimProcess.Code;
                    result.ResultMessage = spResponseClaimProcess.Message;
                }
                else
                {
                    throw new Exception(spResponseClaimProcess.Message);
                }

                List<V_ClaimProcess> dbProcesses = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id AND PROCESS_LIST_ID =: processListId", orderby: "CLAIM_ID", parameters: new { id = claimId, processListId = medicineProcessListId });
                if (dbProcesses != null)
                {
                    List<dynamic> processesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in dbProcesses)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.CLAIM_MEDICINE_ID = Convert.ToString(item.CLAIM_PROCESS_ID);
                        listItem.MEDICINE_ID = Convert.ToString(item.PROCESS_ID);
                        listItem.MEDICINE_CODE = item.PROCESS_CODE;
                        listItem.MEDICINE_NAME = item.PROCESS_NAME;
                        listItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(item.UNIT_AMOUNT).ToString("0.00");
                        listItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(item.AGREED_AMOUNT).ToString("0.00");
                        listItem.PAID = string.IsNullOrEmpty(Convert.ToString(item.PAID)) ? "" : Convert.ToDecimal(item.PAID).ToString("0.00");
                        listItem.RESULT = item.RESULT;
                        if (!string.IsNullOrEmpty(item.PARTLY_REJECT_DESC))
                        {
                            listItem.RESULT += " - " + item.PARTLY_REJECT_DESC;
                        }

                        processesList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimSaveStep2Result
                    {
                        jsonData = processesList.ToJSON(false)
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteClaimProcess(Int64 claimId, Int64 claimProcessId)
        {
            var result = new AjaxResultDto<ClaimSaveStep2Result>();
            ClaimSaveStep2Result step2Result = new ClaimSaveStep2Result();
            try
            {
                long medicineProcessListId = 0;
                var medicineProcessList = ProcessListHelper.GetProcessList("İLAÇ");
                if (medicineProcessList != null)
                {
                    medicineProcessListId = medicineProcessList.Id;
                }
                else
                {
                    throw new Exception("İlaç listesi bulunamadı!");
                }

                var claimProcess = new ClaimProcess
                {
                    Id = claimProcessId,
                    Status = Convert.ToString((int)Status.SILINDI)
                };
                var spResponseClaimProcess = new ClaimProcessRepository().Insert(claimProcess);
                if (spResponseClaimProcess.Code == "100")
                {
                    result.ResultCode = spResponseClaimProcess.Code;
                    result.ResultMessage = spResponseClaimProcess.Message;
                }
                else
                {
                    throw new Exception(spResponseClaimProcess.Message);
                }

                List<V_ClaimProcess> dbProcesses = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id AND PROCESS_LIST_ID !=: processListId", orderby: "CLAIM_ID",
                                                                                            parameters: new { id = claimId, processListId = medicineProcessListId });
                if (dbProcesses != null)
                {
                    List<dynamic> processesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in dbProcesses)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.CLAIM_PROCESS_ID = Convert.ToString(item.CLAIM_PROCESS_ID);
                        listItem.PROCESS_ID = Convert.ToString(item.PROCESS_ID);
                        listItem.PROCESS_CODE = item.PROCESS_CODE;
                        listItem.PROCESS_NAME = item.PROCESS_NAME;
                        listItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(item.UNIT_AMOUNT).ToString("0.00");
                        listItem.REQUESTED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.REQUESTED_AMOUNT)) ? "" : Convert.ToDecimal(item.REQUESTED_AMOUNT).ToString("0.00");
                        listItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(item.AGREED_AMOUNT).ToString("0.00");
                        listItem.COINSURANCE = item.COINSURANCE;
                        listItem.EXEMPTION = string.IsNullOrEmpty(Convert.ToString(item.EXEMPTION)) ? "" : Convert.ToDecimal(item.EXEMPTION).ToString("0.00");
                        listItem.CLAIM_PROCESS_COVERAGE_ID = Convert.ToString(item.COVERAGE_ID);
                        listItem.COVERAGE_NAME = item.COVERAGE_NAME;
                        listItem.RESULT = item.RESULT;
                        if (!string.IsNullOrEmpty(item.PARTLY_REJECT_DESC))
                        {
                            listItem.RESULT += " - " + item.PARTLY_REJECT_DESC;
                        }
                        listItem.PAID = string.IsNullOrEmpty(Convert.ToString(item.PAID)) ? "" : Convert.ToDecimal(item.PAID).ToString("0.00");
                        processesList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimSaveStep2Result
                    {
                        jsonData = processesList.ToJSON(false)
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ClaimSaveNote(FormCollection form)
        {
            var result = new AjaxResultDto<ClaimSaveNoteResult>();
            ClaimSaveNoteResult step6Result = new ClaimSaveNoteResult();
            var notes = form["hdNotes"];

            List<dynamic> noteList = new List<dynamic>();
            noteList = JsonConvert.DeserializeObject<List<dynamic>>(notes);

            try
            {
                var claimId = form["hdClaimId"];
                foreach (var note in noteList)
                {

                    if (note.isOpen == "1")
                    {
                        var Tnote = new Note
                        {
                            Id = note.NOTE_ID,
                            Type = note.NOTE_TYPE,
                            Description = note.NOTE_DESCRIPTION,
                            Status = note.STATUS
                        };
                        var spResponseNote = new NoteRepository().Insert(Tnote);
                        if (spResponseNote.Code == "100")
                        {
                            var noteId = spResponseNote.PkId;
                            var claimNote = new ClaimNote
                            {
                                Id = note.CLAIM_NOTE_ID,
                                ClaimId = long.Parse(claimId),
                                NoteId = noteId,
                                Type = "0",
                                Status = note.STATUS
                            };
                            var spResponseClaimNote = new ClaimNoteRepository().Insert(claimNote);
                            if (spResponseClaimNote.Code == "100")
                            {
                                result.ResultCode = spResponseClaimNote.Code;
                                result.ResultMessage = spResponseClaimNote.Message;
                            }
                            else
                            {
                                throw new Exception($"{note.NOTE_DESCRIPTION} notun Hasar ile bağlantısı oluşturulamadı.");
                            }
                        }
                        else
                        {
                            throw new Exception($"{note.NOTE_DESCRIPTION} not kaydedilirken hata oluştu.");
                        }
                    }
                }
                List<V_ClaimNote> notess = new GenericRepository<V_ClaimNote>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID",
                                                                                            parameters: new { id = claimId });
                if (notess != null)
                {
                    List<dynamic> notesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in notess)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.NOTE_ID = Convert.ToString(item.NOTE_ID);
                        listItem.CLAIM_NOTE_ID = Convert.ToString(item.CLAIM_NOTE_ID);

                        listItem.NOTE_TYPE = item.CLAIM_NOTE_TYPE;
                        listItem.NOTE_TYPESelectedText = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.ClaimNote, item.CLAIM_NOTE_TYPE);
                        listItem.NOTE_DESCRIPTION = item.NOTE_DECRIPTION;

                        notesList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimSaveNoteResult
                    {
                        jsonData = notesList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveDoctor(Int64 claimId, Int64 doctorId)
        {
            var result = new AjaxResultDto<ClaimSaveDoctorResult>();
            try
            {
                var repo = new GenericRepository<ProteinEntities.Claim>();
                var res = repo.FindById(claimId);
                if (res != null)
                {
                    res.StaffId = doctorId;
                    var spResponseClaim = repo.Update(res);
                    if (spResponseClaim.Code == "100")
                    {
                        result.ResultCode = spResponseClaim.Code;
                        result.ResultMessage = spResponseClaim.Message;

                        List<dynamic> doctorList = new List<dynamic>();
                        V_Doctor staff = new GenericRepository<V_Doctor>().FindBy("STAFF_ID=:doctorId", orderby: "STAFF_ID", parameters: new { doctorId }).FirstOrDefault();
                        if (staff != null)
                        {
                            dynamic listItem = new System.Dynamic.ExpandoObject();

                            listItem.STAFF_ID = staff.STAFF_ID;
                            listItem.FIRST_NAME = staff.FIRST_NAME;
                            listItem.LAST_NAME = staff.LAST_NAME;
                            listItem.DOCTOR_BRANCH_ID = staff.DOCTOR_BRANCH_ID;
                            listItem.DOCTOR_BRANCH_NAME = staff.DOCTOR_BRANCH_NAME;
                            listItem.DIPLOMA_NO = staff.DIPLOMA_NO;
                            listItem.REGISTER_NO = staff.REGISTER_NO;
                            listItem.STAFF_TITLE = staff.STAFF_TITLE;
                            listItem.STAFF_IDENTITY_NO = staff.STAFF_IDENTITY_NO;
                            listItem.CONTRACT_TYPE_TEXT = "DOKTOR";

                            doctorList.Add(listItem);
                        }
                        result.Data = new ClaimSaveDoctorResult
                        {
                            jsonData = doctorList.ToJSON()
                        };
                    }
                    else
                    {
                        throw new Exception(spResponseClaim.Code + " : " + spResponseClaim.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveClaimProcessCoverage(Int64 claimProcessId, Int64 claimProcessCoverageId, int isForMedicine = 0)
        {
            var result = new AjaxResultDto<ClaimSaveDoctorResult>();
            try
            {
                long medicineProcessListId = 0;
                var medicineProcessList = ProcessListHelper.GetProcessList("İLAÇ");
                if (medicineProcessList != null)
                {
                    medicineProcessListId = medicineProcessList.Id;
                }
                else
                {
                    throw new Exception("İlaç listesi bulunamadı!");
                }

                var repo = new ClaimProcessRepository();
                var res = repo.FindById(claimProcessId);
                if (res != null)
                {
                    res.CoverageId = claimProcessCoverageId;
                    var spResponseClaim = new ClaimProcessRepository().Update(res);
                    //if (spResponseClaim.Code == "100")
                    //{
                    result.ResultCode = spResponseClaim.Code;
                    result.ResultMessage = spResponseClaim.Message;

                    if (isForMedicine == 1)
                    {
                        List<V_ClaimProcess> dbMedicines = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id AND PROCESS_LIST_ID =: processListId", orderby: "CLAIM_ID",
                                                                                                    parameters: new { id = res.ClaimId, processListId = medicineProcessListId });
                        if (dbMedicines != null)
                        {
                            List<dynamic> medicinesList = new List<dynamic>();

                            int i = 1;

                            foreach (var item in dbMedicines)
                            {
                                dynamic listItem = new System.Dynamic.ExpandoObject();

                                listItem.id = i;
                                listItem.ClientId = i;
                                listItem.STATUS = item.STATUS;

                                listItem.CLAIM_MEDICINE_ID = Convert.ToString(item.CLAIM_PROCESS_ID);
                                listItem.MEDICINE_ID = Convert.ToString(item.PROCESS_ID);
                                listItem.MEDICINE_CODE = item.PROCESS_CODE;
                                listItem.MEDICINE_NAME = item.PROCESS_NAME;
                                listItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(item.UNIT_AMOUNT).ToString("0.00");
                                listItem.REQUESTED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.REQUESTED_AMOUNT)) ? "" : Convert.ToDecimal(item.REQUESTED_AMOUNT).ToString("0.00");
                                listItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(item.AGREED_AMOUNT).ToString("0.00");
                                listItem.CLAIM_MEDICINE_COVERAGE_ID = Convert.ToString(item.COVERAGE_ID);
                                listItem.COVERAGE_NAME = item.COVERAGE_NAME;
                                listItem.PAID = string.IsNullOrEmpty(Convert.ToString(item.PAID)) ? "" : Convert.ToDecimal(item.PAID).ToString("0.00");
                                listItem.RESULT = item.RESULT;
                                if (!string.IsNullOrEmpty(item.PARTLY_REJECT_DESC))
                                {
                                    listItem.RESULT += " - " + item.PARTLY_REJECT_DESC;
                                }

                                medicinesList.Add(listItem);

                                i++;
                            }

                            result.Data = new ClaimSaveDoctorResult
                            {
                                jsonData = medicinesList.ToJSON(false)
                            };
                        }
                    }
                    else
                    {
                        List<V_ClaimProcess> dbClaimProcesses = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id AND PROCESS_LIST_ID !=: processListId", orderby: "CLAIM_ID",
                                                                                                    parameters: new { id = res.ClaimId, processListId = medicineProcessListId });
                        //ViewHelper.GetView(Views.ClaimProcess, "CLAIM_ID =: id AND PROCESS_LIST_ID !=: processListId", parameters: new { id = res.ClaimId, processListId = medicineProcessListId });
                        if (dbClaimProcesses != null)
                        {
                            List<dynamic> claimProcessList = new List<dynamic>();

                            int i = 1;

                            foreach (var clmProcess in dbClaimProcesses)
                            {
                                dynamic clmProcessItem = new System.Dynamic.ExpandoObject();

                                clmProcessItem.id = i;
                                clmProcessItem.ClientId = i;
                                clmProcessItem.STATUS = clmProcess.STATUS;

                                clmProcessItem.CLAIM_PROCESS_ID = Convert.ToString(clmProcess.CLAIM_PROCESS_ID);
                                clmProcessItem.PROCESS_ID = Convert.ToString(clmProcess.PROCESS_ID);
                                clmProcessItem.PROCESS_CODE = clmProcess.PROCESS_CODE;
                                clmProcessItem.PROCESS_NAME = clmProcess.PROCESS_NAME;
                                clmProcessItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(clmProcess.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(clmProcess.UNIT_AMOUNT).ToString("0.00");
                                clmProcessItem.REQUESTED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(clmProcess.REQUESTED_AMOUNT)) ? "" : Convert.ToDecimal(clmProcess.REQUESTED_AMOUNT).ToString("0.00");
                                clmProcessItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(clmProcess.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(clmProcess.AGREED_AMOUNT).ToString("0.00");
                                clmProcessItem.COINSURANCE = clmProcess.COINSURANCE;
                                clmProcessItem.EXEMPTION = string.IsNullOrEmpty(Convert.ToString(clmProcess.EXEMPTION)) ? "" : Convert.ToDecimal(clmProcess.EXEMPTION).ToString("0.00");
                                clmProcessItem.CLAIM_PROCESS_COVERAGE_ID = Convert.ToString(clmProcess.COVERAGE_ID);
                                clmProcessItem.COVERAGE_NAME = clmProcess.COVERAGE_NAME;
                                clmProcessItem.RESULT = clmProcess.RESULT;
                                if (!string.IsNullOrEmpty(clmProcess.PARTLY_REJECT_DESC))
                                {
                                    clmProcessItem.RESULT += " - " + clmProcess.PARTLY_REJECT_DESC;
                                }
                                clmProcessItem.PAID = string.IsNullOrEmpty(Convert.ToString(clmProcess.PAID)) ? "" : Convert.ToDecimal(clmProcess.PAID).ToString("0.00");
                                claimProcessList.Add(clmProcessItem);

                                i++;
                            }

                            result.Data = new ClaimSaveDoctorResult
                            {
                                jsonData = claimProcessList.ToJSON(false)
                            };
                        }
                    }
                    //}
                    //else
                    //{
                    //    throw new Exception(spResponseClaim.Code + " : " + spResponseClaim.Message);
                    //}
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveEvent(Int64 claimId, FormCollection form)
        {
            var result = new AjaxResultDto<ClaimSaveStep5Result>();
            ClaimSaveStep5Result step5Result = new ClaimSaveStep5Result();
            try
            {
                var eventDate = form["eventDate"];
                var eventType = form["eventType"];
                var eventDescription = form["eventDescription"];

                var claimRepository = new GenericRepository<ProteinEntities.Claim>();
                ProteinEntities.Claim claim = claimRepository.FindById(claimId);
                if (claim != null)
                {
                    if (!string.IsNullOrEmpty(eventDate))
                    {
                        claim.EventDate = DateTime.Parse(eventDate);
                    }
                    if (!string.IsNullOrEmpty(eventDescription))
                    {
                        claim.EventDescription = eventDescription;
                    }
                    if (!string.IsNullOrEmpty(eventType))
                    {
                        claim.EventType = eventType;
                    }

                    var spResponseClaim = claimRepository.Update(claim);
                    if (spResponseClaim.Code != "100")
                    {
                        throw new Exception(spResponseClaim.Code + " : " + spResponseClaim.Message);
                    }
                    result.ResultCode = spResponseClaim.Code;
                    result.ResultMessage = spResponseClaim.Message;
                }
                else
                {
                    throw new Exception($"{claimId} nolu hasar bulunamadı!");
                }

            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveHospitalization(Int64 claimId, FormCollection form)
        {
            var result = new AjaxResultDto<ClaimSaveHospitalizationResult>();
            ClaimSaveHospitalizationResult claimSaveHospitalizationResult = new ClaimSaveHospitalizationResult();
            try
            {
                var hospitalizationEntranceDate = form["hospitalizationEntranceDate"];
                var hospitalizationExitDate = form["hospitalizationExitDate"];
                var claimHospitalizationId = form["hdClaimHospitalizationId"];

                var claimRepository = new GenericRepository<ProteinEntities.Claim>();
                ProteinEntities.Claim claim = claimRepository.FindById(claimId);
                if (claim != null)
                {
                    if (!string.IsNullOrEmpty(hospitalizationEntranceDate) || !string.IsNullOrEmpty(hospitalizationExitDate))
                    {
                        var claimHospitalization = new ClaimHospitalization
                        {
                            Id = long.Parse(claimHospitalizationId),
                            ClaimId = claimId
                        };
                        if (!string.IsNullOrEmpty(hospitalizationEntranceDate))
                        {
                            DateTime hospitalizationEntranceDateTime = DateTime.Parse(hospitalizationEntranceDate);
                            if (DateTime.Compare((DateTime)claim.ClaimDate, hospitalizationEntranceDateTime) > 0)
                            {
                                throw new Exception("Yatış tarihi hasar tarihinden küçük olamaz!");
                            }
                            claimHospitalization.EntranceDate = hospitalizationEntranceDateTime;
                        }
                        if (!string.IsNullOrEmpty(hospitalizationExitDate))
                        {
                            DateTime hospitalizationExitDateTime = DateTime.Parse(hospitalizationExitDate);
                            if (DateTime.Compare((DateTime)claim.ClaimDate, hospitalizationExitDateTime) > 0)
                            {
                                throw new Exception("Çıkış tarihi hasar tarihinden küçük olamaz!");
                            }
                            if (!string.IsNullOrEmpty(hospitalizationEntranceDate))
                            {
                                DateTime hospitalizationEntranceDateTime = DateTime.Parse(hospitalizationEntranceDate);
                                if (DateTime.Compare(hospitalizationEntranceDateTime, hospitalizationExitDateTime) > 0)
                                {
                                    throw new Exception("Çıkış tarihi yatış tarihinden küçük olamaz!");
                                }
                            }
                            else
                            {
                                throw new Exception("Lütfen yatış tarihini giriniz!");
                            }
                            claimHospitalization.ExitDate = hospitalizationExitDateTime;
                        }
                        var spResponseClaimHospitalization = new ClaimHospitalizationRepository().Insert(claimHospitalization);
                        if (spResponseClaimHospitalization.Code != "100")
                        {
                            throw new Exception(spResponseClaimHospitalization.Code + " : " + spResponseClaimHospitalization.Message);
                        }
                        claimSaveHospitalizationResult.claimHospitalizationId = spResponseClaimHospitalization.PkId;
                        result.ResultCode = spResponseClaimHospitalization.Code;
                        result.ResultMessage = spResponseClaimHospitalization.Message;
                        result.Data = claimSaveHospitalizationResult;
                    }
                }
                else
                {
                    throw new Exception($"{claimId} nolu hasar bulunamadı!");
                }

            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveClaimCoverage(Int64 claimId, FormCollection form)
        {
            var result = new AjaxResultDto<ClaimSaveStep8Result>();
            ClaimSaveStep5Result step5Result = new ClaimSaveStep5Result();
            try
            {
                if (claimId <= 0)
                {
                    throw new Exception("Hasar numarasını giriniz!");
                }
                var claimProcessId = form["CLAIM_PROCESS_ID"];
                var coverageId = form["CLAIM_COVERAGE_LIST"];
                var count = form["COUNT"];
                var day = form["DAY"];
                var sessionCount = form["SESSION_COUNT"];
                var requested = form["REQUESTED"];
                var accepted = form["ACCEPTED"];
                var sgkAmount = form["SGK_AMOUNT"];
                var exgracia = form["EXGRACIA"];
                var stoppage = form["STOPPAGE"];
                var partlyRejectDesc = form["PARTLY_REJECT_DESC"];

                var claimProcessRepository = new GenericRepository<ClaimProcess>();
                ClaimProcess claimProcess = new ClaimProcess();
                claimProcess.ClaimId = claimId;
                if (!string.IsNullOrEmpty(claimProcessId))
                {
                    claimProcess.Id = long.Parse(claimProcessId);
                    if (claimProcess.Id == 0)
                    {
                        claimProcess.UnitAmount = 0;
                        claimProcess.AgreedAmount = 0;
                        claimProcess.RequestedAmount = 0;
                    }
                }
                if (!string.IsNullOrEmpty(coverageId))
                {
                    claimProcess.CoverageId = long.Parse(coverageId);
                }
                else
                {
                    throw new Exception("Sigortalı teminatını giriniz!");
                }
                if (!string.IsNullOrEmpty(count))
                {
                    claimProcess.Count = int.Parse(count);
                }
                if (!string.IsNullOrEmpty(day))
                {
                    claimProcess.Day = int.Parse(day);
                }
                if (!string.IsNullOrEmpty(sessionCount))
                {
                    claimProcess.SessionCount = int.Parse(sessionCount);
                }
                if (!string.IsNullOrEmpty(requested))
                {
                    requested = requested.Replace(".", string.Empty);
                    requested = requested.Replace(',', '.');
                    decimal value;
                    if (!decimal.TryParse(requested, NumberStyles.Any, CultureInfo.InvariantCulture, out value))
                    {
                        throw new Exception("Teminatın talep tutarı uygun formatta değil!");
                    }
                    claimProcess.Requested = value;
                }
                else
                {
                    throw new Exception("Teminatın talep tutarını giriniz!");
                }
                if (!string.IsNullOrEmpty(stoppage))
                {
                    stoppage = stoppage.Replace(".", string.Empty);
                    stoppage = stoppage.Replace(',', '.');
                    decimal value;
                    if (!decimal.TryParse(stoppage, NumberStyles.Any, CultureInfo.InvariantCulture, out value))
                    {
                        throw new Exception("Teminatın stopaj değeri uygun formatta değil!");
                    }
                    claimProcess.Stopaj = value;
                }
                if (!string.IsNullOrEmpty(accepted))
                {
                    accepted = accepted.Replace(".", string.Empty);
                    accepted = accepted.Replace(',', '.');
                    decimal value;
                    if (!decimal.TryParse(accepted, NumberStyles.Any, CultureInfo.InvariantCulture, out value))
                    {
                        throw new Exception("Teminatın kısmi ret tutarı uygun formatta değil!");
                    }
                    if (claimProcess.Requested != null && claimProcess.Requested >= 0)
                    {
                        claimProcess.Accepted = claimProcess.Requested - value;
                    }
                }
                if (!string.IsNullOrEmpty(sgkAmount))
                {
                    sgkAmount = sgkAmount.Replace(".", string.Empty);
                    sgkAmount = sgkAmount.Replace(',', '.');
                    decimal value;
                    if (!decimal.TryParse(sgkAmount, NumberStyles.Any, CultureInfo.InvariantCulture, out value))
                    {
                        throw new Exception("Teminatın sgk tutarı uygun formatta değil!");
                    }
                    claimProcess.SgkAmount = value;

                    ProteinEntities.Claim claim = new GenericRepository<ProteinEntities.Claim>().FindBy($"ID={claimId}").FirstOrDefault();
                    if (claim.Type != ((int)ClaimType.TSS).ToString())
                    {
                        if (claimProcess.Accepted != null)
                        {
                            claimProcess.Accepted -= claimProcess.SgkAmount;
                        }
                        else
                        {
                            claimProcess.Accepted = claimProcess.Requested - claimProcess.SgkAmount;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(exgracia))
                {
                    exgracia = exgracia.Replace(".", string.Empty);
                    exgracia = exgracia.Replace(',', '.');
                    decimal value;
                    if (!decimal.TryParse(exgracia, NumberStyles.Any, CultureInfo.InvariantCulture, out value))
                    {
                        throw new Exception("Teminatın jest ödeme uygun formatta değil!");
                    }
                    claimProcess.Exgracia = value;
                }
                if (partlyRejectDesc == null)
                {
                    partlyRejectDesc = "";
                }
                claimProcess.PartlyRejectDesc = partlyRejectDesc;

                var spResponseClaimProcess = claimProcessRepository.Insert(claimProcess);
                //if (spResponseClaimProcess.Code != "100")
                //{
                //    throw new Exception(spResponseClaimProcess.Code + " : " + spResponseClaimProcess.Message);
                //}
                result.ResultCode = spResponseClaimProcess.Code;
                result.ResultMessage = spResponseClaimProcess.Message;

                List<V_ClaimProcess> dbClaimCoverages = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                if (dbClaimCoverages != null)
                {
                    List<dynamic> claimCoverageList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in dbClaimCoverages)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.CLAIM_ID = item.CLAIM_ID;
                        listItem.CLAIM_PROCESS_ID = item.CLAIM_PROCESS_ID;
                        listItem.PROCESS_ID = item.PROCESS_ID;
                        listItem.PROCESS_NAME = item.PROCESS_NAME;
                        listItem.COVERAGE_ID = string.IsNullOrEmpty(Convert.ToString(item.COVERAGE_ID)) ? "" : Convert.ToString(item.COVERAGE_ID);
                        listItem.COVERAGE_NAME = item.COVERAGE_NAME;
                        listItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(item.UNIT_AMOUNT).ToString("0.00");
                        listItem.REQUESTED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.REQUESTED_AMOUNT)) ? "" : Convert.ToDecimal(item.REQUESTED_AMOUNT).ToString("0.00");
                        listItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(item.AGREED_AMOUNT).ToString("0.00");
                        listItem.CONTRACT_PROCESS_GROUP_ID = item.CONTRACT_PROCESS_GROUP_ID;
                        listItem.PROCESS_CODE = item.PROCESS_CODE;
                        listItem.SESSION_COUNT = item.SESSION_COUNT;
                        listItem.COUNT = item.COUNT;
                        listItem.DAY = item.DAY;
                        listItem.PROCESS_LIMIT = string.IsNullOrEmpty(Convert.ToString(item.PROCESS_LIMIT)) ? "" : Convert.ToDecimal(item.PROCESS_LIMIT).ToString("0.00");
                        listItem.REQUESTED = string.IsNullOrEmpty(Convert.ToString(item.REQUESTED)) ? "" : Convert.ToDecimal(item.REQUESTED).ToString("0.00");
                        listItem.STOPPAGE = string.IsNullOrEmpty(Convert.ToString(item.STOPAJ)) ? "" : Convert.ToDecimal(item.STOPAJ).ToString("0.00");
                        decimal acceptedAmount = 0;
                        if (item.REQUESTED != null && Convert.ToDecimal(item.REQUESTED) > 0)
                        {
                            if (item.ACCEPTED != null && Convert.ToDecimal(item.ACCEPTED) > 0)
                            {
                                acceptedAmount = Convert.ToDecimal(item.REQUESTED) - Convert.ToDecimal(item.ACCEPTED);
                            }
                            else
                            {
                                acceptedAmount = Convert.ToDecimal(item.REQUESTED);
                            }
                        }
                        listItem.ACCEPTED = acceptedAmount.ToString("0.00");
                        listItem.CONFIRMED = string.IsNullOrEmpty(Convert.ToString(item.CONFIRMED)) ? "" : Convert.ToDecimal(item.CONFIRMED).ToString("0.00");
                        listItem.PAID = string.IsNullOrEmpty(Convert.ToString(item.PAID)) ? "" : Convert.ToDecimal(item.PAID).ToString("0.00");
                        listItem.EXGRACIA = string.IsNullOrEmpty(Convert.ToString(item.EXGRACIA)) ? "" : Convert.ToDecimal(item.EXGRACIA).ToString("0.00");
                        listItem.SGK_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.SGK_AMOUNT)) ? "" : Convert.ToDecimal(item.SGK_AMOUNT).ToString("0.00");
                        decimal insuredAmount = 0;
                        if (item.REQUESTED != null && Convert.ToDecimal(item.REQUESTED) > 0)
                        {
                            if (item.PAID != null && Convert.ToDecimal(item.PAID) > 0)
                            {
                                insuredAmount = Convert.ToDecimal(item.REQUESTED) - Convert.ToDecimal(item.PAID);
                            }
                            else
                            {
                                insuredAmount = Convert.ToDecimal(item.REQUESTED);
                            }
                        }
                        listItem.INSURED_AMOUNT = insuredAmount.ToString("0.00");
                        listItem.COINSURANCE = item.COINSURANCE;
                        listItem.EXEMPTION = string.IsNullOrEmpty(Convert.ToString(item.EXEMPTION)) ? "" : Convert.ToDecimal(item.EXEMPTION).ToString("0.00");
                        listItem.RESULT = item.RESULT;
                        if (!string.IsNullOrEmpty(item.PARTLY_REJECT_DESC))
                        {
                            listItem.RESULT += " - " + item.PARTLY_REJECT_DESC;
                        }
                        listItem.PROCESS_LIST_ID = item.PROCESS_LIST_ID;

                        claimCoverageList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimSaveStep8Result
                    {
                        jsonData = claimCoverageList.ToJSON(false)
                    };
                }
                else
                {
                    throw new Exception("Hasar teminatları getirilirken hata oluştu!");
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ClaimSaveStep9(FormCollection form)
        {
            var result = new AjaxResultDto<ClaimSaveStep9Result>();
            var medias = form["hdMedias"];
            try
            {
                List<dynamic> mediaList = new List<dynamic>();
                mediaList = JsonConvert.DeserializeObject<List<dynamic>>(medias);
                var claimId = form["hdClaimId"];

                foreach (var media in mediaList)
                {
                    if (media.isOpen == "1")
                    {
                        var Tmedia = new Media
                        {
                            Id = media.MEDIA_ID,
                            Name = media.MEDIA_NAME,
                            FileName = "TEST",
                            Status = media.STATUS
                        };
                        var spResponseMedia = new MediaRepository().Insert(Tmedia);
                        if (spResponseMedia.Code == "100")
                        {
                            var mediaId = spResponseMedia.PkId;
                            var claimMedia = new ClaimMedia
                            {
                                Id = media.PRODUCT_MEDIA_ID,
                                ClaimId = long.Parse(claimId),
                                MediaId = mediaId,
                                Status = media.STATUS
                            };
                            var spResponseClaimMedia = new ClaimMediaRepository().Insert(claimMedia);
                            if (spResponseClaimMedia.Code == "100")
                            {
                                result.ResultCode = spResponseClaimMedia.Code;
                                result.ResultMessage = spResponseClaimMedia.Message;
                            }
                            else
                            {
                                throw new Exception(spResponseClaimMedia.Code + " : " + spResponseClaimMedia.Message);
                            }
                        }
                        else
                        {
                            throw new Exception(spResponseMedia.Code + " : " + spResponseMedia.Message);
                        }
                    }
                }

                List<V_ClaimMedia> mediass = new GenericRepository<V_ClaimMedia>().FindBy("CLAIM_ID=:id", orderby: "CLAIM_ID", fetchDeletedRows: true, parameters: new { id = claimId });
                if (mediass != null)
                {
                    List<dynamic> mediasList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in mediass)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.Status;

                        listItem.MEDIA_ID = Convert.ToString(item.MediaId);
                        listItem.CLAIM_MEDIA_ID = Convert.ToString(item.ClaimMediaId);
                        listItem.MEDIA_NAME = item.MediaName;
                        listItem.MEDIA_PATH = item.FileName;
                        listItem.isOpen = "0";
                        mediasList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimSaveStep9Result
                    {
                        jsonData = mediasList.ToJSON()
                    };
                }

            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [LoginControl]
        public ActionResult Delete(int id)
        {
            var repo = new ClaimRepository();
            var result = repo.FindById(id);
            if (result != null)
            {
                result.Status = "1";
                repo.Update(result);
            }
            return RedirectToAction("Claim");
        }

        #region DropDown fill methods 

        private void FillCompanies()
        {
            var v_company = new GenericRepository<V_Company>().FindBy(orderby: "COMPANY_ID");
            //ViewHelper.GetView(Views.Company, orderby: "COMPANY_ID");
            ViewBag.v_company = v_company;
        }

        private void FillProducts()
        {
            var v_product = new GenericRepository<V_Product>().FindBy(orderby: "PRODUCT_ID");
            ViewBag.v_product = v_product;
        }

        private void FillProducts(Int64 Id)
        {
            var v_product = new GenericRepository<V_Product>().FindBy("COMPANY_ID =:id", orderby: "PRODUCT_ID", parameters: new { id = Id });
            ViewBag.v_product = v_product;
        }

        #endregion
        [HttpPost]
        [LoginControl]
        public JsonResult Medicine(Int64 claimId, Int64 insuredId, Int64 packageId, string barcode)
        {
            var result = new AjaxResultDto<ClaimSaveStep2Result>();
            try
            {
                long medicineProcessListId = 0;
                var medicineProcessList = ProcessListHelper.GetProcessList("İLAÇ");
                if (medicineProcessList != null)
                {
                    medicineProcessListId = medicineProcessList.Id;
                }
                else
                {
                    throw new Exception("İlaç listesi bulunamadı!");
                }

                Process medicine;
                Helper hlp = new Helper();
                MedicineInfo mInfo = hlp.GetMedicineInfoByBarcode(barcode);
                if (mInfo.Success)
                {
                    GenericRepository<Process> medicineRepository = new GenericRepository<Process>();
                    List<Process> medicineList = medicineRepository.FindBy("CODE='" + barcode + "'");
                    if (medicineList == null || medicineList.Count == 0)
                    {
                        // Insert new medicine
                        medicine = new Process()
                        {
                            Id = 0,
                            Code = barcode,
                            ProcessListId = medicineProcessListId,
                            Name = mInfo.Name,
                            Amount = mInfo.TotalPrice
                        };
                        SpResponse spResponse = medicineRepository.Update(medicine);
                        if (spResponse.Code != "100")
                        {
                            throw new Exception("İlaç bilgileri güncellenirken hata oluştu!");
                        }
                        medicine.Id = spResponse.PkId;
                    }
                    else if (medicineList.Count == 1)
                    {
                        medicine = medicineList[0];
                        // Check medicine needs to be updated
                        if (!medicine.Name.Equals(mInfo.Name) || medicine.Amount != mInfo.TotalPrice)
                        {
                            medicine.Name = mInfo.Name;
                            medicine.Amount = mInfo.TotalPrice;
                            SpResponse spResponse = medicineRepository.Update(medicine);
                            if (spResponse.Code != "100")
                            {
                                throw new Exception("İlaç bilgileri güncellenirken hata oluştu!");
                            }
                            result.ResultCode = spResponse.Code;
                            result.ResultMessage = spResponse.Message;
                        }
                    }
                    else
                    {
                        throw new Exception("Aynı barkod ile birden fazla ilaç bulundu! (" + mInfo.Message + ")");
                    }

                    if (medicine != null)
                    {
                        long? coverageId = null;
                        List<V_PackageCoverageList> insuredCoverageResult = new GenericRepository<V_PackageCoverageList>().FindBy(
                            $"PACKAGE_ID={packageId} AND COVERAGE_TYPE ={((int)CoverageType.AYAKTA).ToString()} AND COVERAGE_NAME LIKE '%İLAÇ%'",
                            orderby: "PACKAGE_ID",
                            fetchHistoricRows: true,
                            fetchDeletedRows: true);
                        if (insuredCoverageResult != null)
                        {
                            if (insuredCoverageResult != null && insuredCoverageResult.Count == 1)
                            {
                                string strCoverageId = Convert.ToString(insuredCoverageResult[0].COVERAGE_ID);
                                if (!string.IsNullOrEmpty(strCoverageId))
                                {
                                    coverageId = long.Parse(strCoverageId);
                                }
                            }
                        }

                        var claimMedicine = new ClaimProcess
                        {
                            Id = 0,
                            ClaimId = claimId,
                            ProcessId = medicine.Id,
                            RequestedAmount = medicine.Amount,
                            UnitAmount = medicine.Amount,
                            AgreedAmount = medicine.Amount
                        };
                        if (coverageId != null && coverageId > 0)
                        {
                            claimMedicine.CoverageId = coverageId;
                        }
                        var spResponseClaimMedicine = new ClaimProcessRepository().Insert(claimMedicine);
                        //if (spResponseClaimMedicine.Code == "100")
                        //{
                        result.ResultCode = spResponseClaimMedicine.Code;
                        result.ResultMessage = spResponseClaimMedicine.Message;
                        //}
                        //else
                        //{
                        //    throw new Exception(spResponseClaimMedicine.Message);
                        //}
                    }
                    else
                    {
                        throw new Exception(mInfo.Message);
                    }
                }
                else
                {
                    throw new Exception("İlaç bulunamadı! (" + mInfo.Message + ")");
                }

                List<V_ClaimProcess> dbMedicines = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id AND PROCESS_LIST_ID =: processListId", orderby: "CLAIM_ID", parameters: new { id = claimId, processListId = medicineProcessListId });
                if (dbMedicines != null)
                {
                    List<dynamic> medicinesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in dbMedicines)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.CLAIM_MEDICINE_ID = Convert.ToString(item.CLAIM_PROCESS_ID);
                        listItem.MEDICINE_ID = Convert.ToString(item.PROCESS_ID);
                        listItem.MEDICINE_CODE = item.PROCESS_CODE;
                        listItem.MEDICINE_NAME = item.PROCESS_NAME;
                        listItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(item.UNIT_AMOUNT).ToString("0.00");
                        listItem.REQUESTED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.REQUESTED_AMOUNT)) ? "" : Convert.ToDecimal(item.REQUESTED_AMOUNT).ToString("0.00");
                        listItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(item.AGREED_AMOUNT).ToString("0.00");
                        listItem.CLAIM_MEDICINE_COVERAGE_ID = Convert.ToString(item.COVERAGE_ID);
                        listItem.COVERAGE_NAME = item.COVERAGE_NAME;
                        listItem.PAID = string.IsNullOrEmpty(Convert.ToString(item.PAID)) ? "" : Convert.ToDecimal(item.PAID).ToString("0.00");
                        listItem.RESULT = item.RESULT;
                        if (!string.IsNullOrEmpty(item.PARTLY_REJECT_DESC))
                        {
                            listItem.RESULT += " - " + item.PARTLY_REJECT_DESC;
                        }

                        medicinesList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimSaveStep2Result
                    {
                        jsonData = medicinesList.ToJSON(false)
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ClaimProcess(Int64 claimId, Int64 packageId, FormCollection form)
        {
            var result = new AjaxResultDto<ClaimSaveStep6Result>();
            ClaimSaveStep6Result step6Result = new ClaimSaveStep6Result();
            try
            {
                var processId = form["hdClaimProcessesId"];
                var requestedAmount = form["REQUESTED_AMOUNT_" + processId].Replace('.', ',');
                if (!processId.IsInt64() || !requestedAmount.IsNumeric())
                {
                    throw new Exception("Talep Edilen Tutar Girilmesi Gerekmektedir.");
                }

                var CheckClaimProcess = new GenericRepository<ClaimProcess>().FindBy($"CLAIM_ID={claimId} AND PROCESS_ID={processId}");
                if (CheckClaimProcess.Count > 0)
                {
                    throw new Exception("Bu işlem Daha Önce Seçilmiş. Lütfen Kontrol Ediniz...");
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                //string processIndex = js.Deserialize<string>(processesIndexes);

                V_ClaimProcessList claimProces = new GenericRepository<V_ClaimProcessList>().FindBy("CLAIM_ID =: id AND PROCESS_ID=:processId", orderby: "CLAIM_ID", fetchDeletedRows: true, fetchHistoricRows: true, parameters: new { id = claimId, processId }).FirstOrDefault();
                if (claimProces != null)
                {
                    ClaimProcessList listItem = new ClaimProcessList();

                    listItem.CLAIM_ID = Convert.ToString(claimProces.CLAIM_ID);
                    listItem.PROCESS_ID = Convert.ToString(claimProces.PROCESS_ID);
                    listItem.PROCESS_NAME = claimProces.PROCESS_NAME;
                    listItem.PROCESS_CODE = claimProces.PROCESS_CODE;
                    listItem.PROCESS_AMOUNT = Convert.ToString(claimProces.PROCESS_AMOUNT);
                    listItem.PROCESS_GROUP_ID = Convert.ToString(claimProces.PROCESS_GROUP_ID);
                    listItem.PROCESS_GROUP_NAME = claimProces.PROCESS_GROUP_NAME;
                    listItem.PROCESS_LIST_ID = Convert.ToString(claimProces.PROCESS_LIST_ID);
                    listItem.PROCESS_LIST_NAME = claimProces.PROCESS_LIST_NAME;

                    var claimProcess = new ClaimProcess();
                    claimProcess.Id = 0;
                    claimProcess.ClaimId = claimId;
                    claimProcess.ProcessId = long.Parse(listItem.PROCESS_ID);
                    claimProcess.UnitAmount = decimal.Parse(Convert.ToString(claimProces.PROCESS_AMOUNT));
                    claimProcess.RequestedAmount = decimal.Parse(Convert.ToString(requestedAmount));
                    claimProcess.AgreedAmount = decimal.Parse(Convert.ToString(claimProces.CONTRACT_AMOUNT));

                    List<V_PackageClaimProcess> packageClaimProcesses = new GenericRepository<V_PackageClaimProcess>().FindBy("PACKAGE_ID =: packageId AND PROCESS_ID =: processId", orderby: "PACKAGE_ID", fetchDeletedRows: true, fetchHistoricRows: true, parameters: new { packageId, processId = claimProcess.ProcessId });
                    if (packageClaimProcesses != null && packageClaimProcesses.Count == 1)
                    {
                        claimProcess.CoverageId = long.Parse(Convert.ToString(packageClaimProcesses[0].COVERAGE_ID));
                    }

                    var spResponseClaimProcess = new ClaimProcessRepository().Insert(claimProcess);
                    //if (spResponseClaimProcess.Code == "100")
                    //{
                    result.ResultCode = spResponseClaimProcess.Code;
                    result.ResultMessage = spResponseClaimProcess.Message;
                    //}
                    //else
                    //{
                    //    throw new Exception(spResponseClaimProcess.Message);
                    //}

                    long medicineProcessListId = 0;
                    var medicineProcessList = ProcessListHelper.GetProcessList("İLAÇ");
                    if (medicineProcessList != null)
                    {
                        medicineProcessListId = medicineProcessList.Id;
                    }
                    else
                    {
                        throw new Exception("İlaç listesi bulunamadı!");
                    }

                    List<V_ClaimProcess> dbClaimProcesses = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id AND PROCESS_LIST_ID !=: processListId", orderby: "CLAIM_ID", parameters: new { id = claimId, processListId = medicineProcessListId });
                    if (dbClaimProcesses != null)
                    {
                        List<dynamic> claimProcessList = new List<dynamic>();

                        int i = 1;

                        foreach (var clmProcess in dbClaimProcesses)
                        {
                            dynamic clmProcessItem = new System.Dynamic.ExpandoObject();

                            clmProcessItem.id = i;
                            clmProcessItem.ClientId = i;
                            clmProcessItem.STATUS = clmProcess.STATUS;

                            clmProcessItem.CLAIM_PROCESS_ID = Convert.ToString(clmProcess.CLAIM_PROCESS_ID);
                            clmProcessItem.PROCESS_ID = Convert.ToString(clmProcess.PROCESS_ID);
                            clmProcessItem.PROCESS_CODE = clmProcess.PROCESS_CODE;
                            clmProcessItem.PROCESS_NAME = clmProcess.PROCESS_NAME;
                            clmProcessItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(clmProcess.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(clmProcess.UNIT_AMOUNT).ToString("0.00");
                            clmProcessItem.REQUESTED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(clmProcess.REQUESTED_AMOUNT)) ? "" : Convert.ToDecimal(clmProcess.REQUESTED_AMOUNT).ToString("0.00");
                            clmProcessItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(clmProcess.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(clmProcess.AGREED_AMOUNT).ToString("0.00");
                            clmProcessItem.COINSURANCE = clmProcess.COINSURANCE;
                            clmProcessItem.EXEMPTION = string.IsNullOrEmpty(Convert.ToString(clmProcess.EXEMPTION)) ? "" : Convert.ToDecimal(clmProcess.EXEMPTION).ToString("0.00");
                            clmProcessItem.PAID = string.IsNullOrEmpty(Convert.ToString(clmProcess.PAID)) ? "" : Convert.ToDecimal(clmProcess.PAID).ToString("0.00");
                            clmProcessItem.CLAIM_PROCESS_COVERAGE_ID = Convert.ToString(clmProcess.COVERAGE_ID);
                            clmProcessItem.COVERAGE_NAME = clmProcess.COVERAGE_NAME;
                            clmProcessItem.RESULT = clmProcess.RESULT;
                            if (!string.IsNullOrEmpty(clmProcess.PARTLY_REJECT_DESC))
                            {
                                clmProcessItem.RESULT += " - " + clmProcess.PARTLY_REJECT_DESC;
                            }

                            claimProcessList.Add(clmProcessItem);

                            i++;
                        }

                        result.Data = new ClaimSaveStep6Result
                        {
                            jsonData = claimProcessList.ToJSON(false)
                        };
                    }
                }
                else
                {
                    throw new Exception("Seçilen hasar işlemi bulunamadı!");
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Processes(Int64 claimId)
        {
            var result = new AjaxResultDto<ClaimProcessListResult>();
            try
            {
                long medicineProcessListId = 0;
                var medicineProcessList = ProcessListHelper.GetProcessList("İLAÇ");
                if (medicineProcessList != null)
                {
                    medicineProcessListId = medicineProcessList.Id;
                }
                else
                {
                    throw new Exception("İlaç listesi bulunamadı!");
                }

                List<V_ClaimProcess> dbClaimProcesses = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id AND PROCESS_LIST_ID !=: processListId", orderby: "CLAIM_ID", parameters: new { id = claimId, processListId = medicineProcessListId });
                if (dbClaimProcesses != null)
                {
                    List<dynamic> claimProcessList = new List<dynamic>();

                    int i = 1;

                    foreach (var clmProcess in dbClaimProcesses)
                    {
                        dynamic clmProcessItem = new System.Dynamic.ExpandoObject();

                        clmProcessItem.id = i;
                        clmProcessItem.ClientId = i;
                        clmProcessItem.STATUS = clmProcess.STATUS;

                        clmProcessItem.CLAIM_PROCESS_ID = Convert.ToString(clmProcess.CLAIM_PROCESS_ID);
                        clmProcessItem.PROCESS_ID = Convert.ToString(clmProcess.PROCESS_ID);
                        clmProcessItem.PROCESS_CODE = clmProcess.PROCESS_CODE;
                        clmProcessItem.PROCESS_NAME = clmProcess.PROCESS_NAME;
                        clmProcessItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(clmProcess.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(clmProcess.UNIT_AMOUNT).ToString("0.00");
                        clmProcessItem.REQUESTED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(clmProcess.REQUESTED_AMOUNT)) ? "" : Convert.ToDecimal(clmProcess.REQUESTED_AMOUNT).ToString("0.00");
                        clmProcessItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(clmProcess.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(clmProcess.AGREED_AMOUNT).ToString("0.00");
                        clmProcessItem.COINSURANCE = clmProcess.COINSURANCE;
                        clmProcessItem.EXEMPTION = string.IsNullOrEmpty(Convert.ToString(clmProcess.EXEMPTION)) ? "" : Convert.ToDecimal(clmProcess.EXEMPTION).ToString("0.00");
                        clmProcessItem.CLAIM_PROCESS_COVERAGE_ID = Convert.ToString(clmProcess.COVERAGE_ID);
                        clmProcessItem.COVERAGE_NAME = clmProcess.COVERAGE_NAME;
                        clmProcessItem.RESULT = clmProcess.RESULT;
                        if (!string.IsNullOrEmpty(clmProcess.PARTLY_REJECT_DESC))
                        {
                            clmProcessItem.RESULT += " - " + clmProcess.PARTLY_REJECT_DESC;
                        }
                        clmProcessItem.PAID = string.IsNullOrEmpty(Convert.ToString(clmProcess.PAID)) ? "" : Convert.ToDecimal(clmProcess.PAID).ToString("0.00");
                        claimProcessList.Add(clmProcessItem);

                        i++;
                    }

                    result.Data = new ClaimProcessListResult
                    {
                        jsonData = claimProcessList.ToJSON(false)
                    };
                    result.ResultCode = "100";
                    result.ResultMessage = "SUCCESS";
                }
                else
                {
                    ClaimRepository claimRepository = new ClaimRepository();
                    ProteinEntities.Claim claim = claimRepository.FindById(claimId);
                    if (claim != null)
                    {
                        if (claim.PaymentMethod.Equals(Convert.ToString((int)ClaimPaymentMethod.SIGORTALI)))
                        {
                            throw new Exception("Bu sigortalı için bu kurumdan işlem seçilememekte, lütfen teminat seçiniz!");
                        }
                        else
                        {
                            throw new Exception("Bu kurumun dahil olduğu network'lerden sigortalı için paketinde tanımlı hizmet alabileceği işlem bulunamamıştır!");
                        }
                    }
                    else
                    {
                        throw new Exception("Hasar bulunamadı!");
                    }
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult Medicines(Int64 claimId)
        {
            var result = new AjaxResultDto<ClaimProcessListResult>();
            try
            {
                long medicineProcessListId = 0;
                var medicineProcessList = ProcessListHelper.GetProcessList("İLAÇ");
                if (medicineProcessList != null)
                {
                    medicineProcessListId = medicineProcessList.Id;
                }
                else
                {
                    throw new Exception("İlaç listesi bulunamadı!");
                }

                List<V_ClaimProcess> dbMedicines = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id AND PROCESS_LIST_ID =: processListId", orderby: "CLAIM_ID", parameters: new { id = claimId, processListId = medicineProcessListId });
                if (dbMedicines != null)
                {
                    List<dynamic> medicinesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in dbMedicines)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.CLAIM_MEDICINE_ID = Convert.ToString(item.CLAIM_PROCESS_ID);
                        listItem.MEDICINE_ID = Convert.ToString(item.PROCESS_ID);
                        listItem.MEDICINE_CODE = item.PROCESS_CODE;
                        listItem.MEDICINE_NAME = item.PROCESS_NAME;
                        listItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(item.UNIT_AMOUNT).ToString("0.00");
                        listItem.REQUESTED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.REQUESTED_AMOUNT)) ? "" : Convert.ToDecimal(item.REQUESTED_AMOUNT).ToString("0.00");
                        listItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(item.AGREED_AMOUNT).ToString("0.00");
                        listItem.CLAIM_MEDICINE_COVERAGE_ID = Convert.ToString(item.COVERAGE_ID);
                        listItem.COVERAGE_NAME = item.COVERAGE_NAME;
                        listItem.PAID = string.IsNullOrEmpty(Convert.ToString(item.PAID)) ? "" : Convert.ToDecimal(item.PAID).ToString("0.00");
                        listItem.RESULT = item.RESULT;
                        if (!string.IsNullOrEmpty(item.PARTLY_REJECT_DESC))
                        {
                            listItem.RESULT += " - " + item.PARTLY_REJECT_DESC;
                        }

                        medicinesList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimProcessListResult
                    {
                        jsonData = medicinesList.ToJSON(false)
                    };
                    result.ResultCode = "100";
                    result.ResultMessage = "SUCCESS";
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [LoginControl]
        public JsonResult Coverages(Int64 claimId)
        {
            object result = null;
            var claimProcessList = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
            //ViewHelper.GetView(Views.ClaimProcess, whereCondititon: $"CLAIM_ID={claimId}");
            if (claimProcessList != null)
            {
                List<dynamic> list = new List<dynamic>();

                int i = 1;

                foreach (var claimProcess in claimProcessList)
                {
                    //Add root item
                    dynamic claimProcessItem = new System.Dynamic.ExpandoObject();

                    claimProcessItem.id = i;
                    claimProcessItem.ClientId = i;
                    claimProcessItem.STATUS = claimProcess.STATUS;

                    claimProcessItem.CLAIM_ID = claimProcess.CLAIM_ID;
                    claimProcessItem.CLAIM_PROCESS_ID = claimProcess.CLAIM_PROCESS_ID;
                    claimProcessItem.PROCESS_ID = claimProcess.PROCESS_ID;
                    claimProcessItem.PROCESS_NAME = claimProcess.PROCESS_NAME;
                    claimProcessItem.COVERAGE_ID = string.IsNullOrEmpty(Convert.ToString(claimProcess.COVERAGE_ID)) ? "" : Convert.ToString(claimProcess.COVERAGE_ID);
                    claimProcessItem.COVERAGE_NAME = claimProcess.COVERAGE_NAME;
                    claimProcessItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(claimProcess.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(claimProcess.UNIT_AMOUNT).ToString("0.00");
                    claimProcessItem.REQUESTED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(claimProcess.REQUESTED_AMOUNT)) ? "" : Convert.ToDecimal(claimProcess.REQUESTED_AMOUNT).ToString("0.00");
                    claimProcessItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(claimProcess.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(claimProcess.AGREED_AMOUNT).ToString("0.00");
                    claimProcessItem.CONTRACT_PROCESS_GROUP_ID = claimProcess.CONTRACT_PROCESS_GROUP_ID;
                    claimProcessItem.PROCESS_CODE = claimProcess.PROCESS_CODE;
                    claimProcessItem.SESSION_COUNT = claimProcess.SESSION_COUNT;
                    claimProcessItem.COUNT = claimProcess.COUNT;
                    claimProcessItem.DAY = claimProcess.DAY;
                    claimProcessItem.PROCESS_LIMIT = string.IsNullOrEmpty(Convert.ToString(claimProcess.PROCESS_LIMIT)) ? "" : Convert.ToDecimal(claimProcess.PROCESS_LIMIT).ToString("0.00");
                    claimProcessItem.REQUESTED = string.IsNullOrEmpty(Convert.ToString(claimProcess.REQUESTED)) ? "" : Convert.ToDecimal(claimProcess.REQUESTED).ToString("0.00");
                    claimProcessItem.STOPPAGE = string.IsNullOrEmpty(Convert.ToString(claimProcess.STOPAJ)) ? "" : Convert.ToDecimal(claimProcess.STOPAJ).ToString("0.00");
                    decimal acceptedAmount = 0;
                    if (claimProcess.REQUESTED != null && Convert.ToDecimal(claimProcess.REQUESTED) > 0)
                    {
                        if (claimProcess.ACCEPTED != null && Convert.ToDecimal(claimProcess.ACCEPTED) > 0)
                        {
                            acceptedAmount = Convert.ToDecimal(claimProcess.REQUESTED) - Convert.ToDecimal(claimProcess.ACCEPTED);
                        }
                        else
                        {
                            acceptedAmount = Convert.ToDecimal(claimProcess.REQUESTED);
                        }
                    }
                    claimProcessItem.ACCEPTED = acceptedAmount.ToString("0.00");
                    claimProcessItem.CONFIRMED = string.IsNullOrEmpty(Convert.ToString(claimProcess.CONFIRMED)) ? "" : Convert.ToDecimal(claimProcess.CONFIRMED).ToString("0.00");
                    claimProcessItem.PAID = string.IsNullOrEmpty(Convert.ToString(claimProcess.PAID)) ? "" : Convert.ToDecimal(claimProcess.PAID).ToString("0.00");
                    claimProcessItem.EXGRACIA = string.IsNullOrEmpty(Convert.ToString(claimProcess.EXGRACIA)) ? "" : Convert.ToDecimal(claimProcess.EXGRACIA).ToString("0.00");
                    claimProcessItem.SGK_AMOUNT = string.IsNullOrEmpty(Convert.ToString(claimProcess.SGK_AMOUNT)) ? "" : Convert.ToDecimal(claimProcess.SGK_AMOUNT).ToString("0.00");
                    decimal insuredAmount = 0;
                    if (claimProcess.REQUESTED != null && Convert.ToDecimal(claimProcess.REQUESTED) > 0)
                    {
                        if (claimProcess.PAID != null && Convert.ToDecimal(claimProcess.PAID) > 0)
                        {
                            insuredAmount = Convert.ToDecimal(claimProcess.REQUESTED) - Convert.ToDecimal(claimProcess.PAID);
                        }
                        else
                        {
                            insuredAmount = Convert.ToDecimal(claimProcess.REQUESTED);
                        }
                    }
                    claimProcessItem.INSURED_AMOUNT = insuredAmount.ToString("0.00");
                    claimProcessItem.COINSURANCE = claimProcess.COINSURANCE;
                    claimProcessItem.EXEMPTION = string.IsNullOrEmpty(Convert.ToString(claimProcess.EXEMPTION)) ? "" : Convert.ToDecimal(claimProcess.EXEMPTION).ToString("0.00");
                    claimProcessItem.RESULT = claimProcess.RESULT;
                    if (!string.IsNullOrEmpty(claimProcess.PARTLY_REJECT_DESC))
                    {
                        claimProcessItem.RESULT += " - " + claimProcess.PARTLY_REJECT_DESC;
                    }
                    claimProcessItem.PROCESS_LIST_ID = claimProcess.PROCESS_LIST_ID;

                    list.Add(claimProcessItem);

                    i++;
                }
                result = list.ToJSON();
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(result),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [LoginControl]
        public JsonResult ProcessICDs(Int64 claimId, Int64 claimProcessId)
        {
            var IcdList = new GenericRepository<V_ClaimIcd>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
            if (IcdList.Count <= 0)
            {
                throw new Exception("Hasara Ait Tanı Bilgisi Bulunamadı!");
            }

            List<ClaimProcessIcd> claimProcessIcds = new ClaimProcessIcdRepository().FindBy("CLAIM_PROCESS_ID = :claimProcessId", parameters: new { claimProcessId });

            List<dynamic> claimProcessIcdList = new List<dynamic>();

            foreach (var item in IcdList)
            {
                dynamic listItem = new System.Dynamic.ExpandoObject();
                listItem.CLAIM_ICD_ID = Convert.ToString(item.CLAIM_ICD_ID);
                listItem.CLAIM_ID = Convert.ToString(item.CLAIM_ID);
                listItem.ICD_CODE = Convert.ToString(item.ICD_CODE);
                listItem.ICD_ID = Convert.ToString(item.ICD_ID);
                listItem.ICD_NAME = item.ICD_NAME;
                listItem.ICD_TYPE = item.ICD_TYPE;
                listItem.ICD_TYPE_TEXT = LookupHelper.GetLookupTextByOrdinal(Constants.LookupTypes.ICD, item.ICD_TYPE);
                listItem.STATUS = item.STATUS;
                listItem.SELECTED = "0";
                foreach (var claimProcessIcd in claimProcessIcds)
                {
                    if (item.ICD_ID == claimProcessIcd.IcdId)
                    {
                        listItem.SELECTED = "1";
                    }
                }

                claimProcessIcdList.Add(listItem);
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = claimProcessIcdList.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [LoginControl]
        public JsonResult EvaluateStatus(Int64 claimId, Int64 companyId)
        {
            var result = new AjaxResultDto<ClaimStatusResult>();
            ClaimStatusResult step1Result = new ClaimStatusResult();
            try
            {
                List<V_ClaimIcd> dbIcds = new GenericRepository<V_ClaimIcd>().FindBy("CLAIM_ID =:id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                if (dbIcds == null || dbIcds.Count < 1)
                {
                    throw new Exception("En az Bir Tanı Bilgisi Girilmesi Gerekmektedir.");
                }

                ProxyServiceClient proxyServiceClient = new ProxyServiceClient();
                NNHayatServiceClientV2 nNHayatServiceClient = new NNHayatServiceClientV2();
                ClaimStateUpdateReq stateUpdateReq = new ClaimStateUpdateReq
                {
                    ClaimId = claimId
                };

                if (companyId == 95 || companyId == 10)
                {
                    proxyServiceClient.ClaimNotice(stateUpdateReq);
                }
                else if (companyId == 322)
                {
                    NNTazminatGirisReq nNTazminatGirisReq = new NNTazminatGirisReq
                    {
                        claimId = claimId
                    };
                    nNHayatServiceClient.TazminatGiris(nNTazminatGirisReq);
                }

                ClaimRepository claimRepository = new ClaimRepository();
                string[] spResult = claimRepository.ClaimStatusResult(claimId);
                if (!spResult[0].Equals("1"))
                {
                    throw new Exception(spResult[0] + " : " + spResult[1] + " - HASAR DURUMU GÜNCELLENEMEDİ!");
                }

                var vClaim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID={claimId}", orderby: "CLAIM_ID DESC", hint: "/*+ USE_HASH(claim_id) ORDERED */").FirstOrDefault();
                if (vClaim != null)
                {
                    step1Result.claimStatus = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, vClaim.STATUS);
                    step1Result.claimStatusOrdinal = int.Parse(vClaim.STATUS);
                    step1Result.claimReason = vClaim.REASON_DESCRIPTION;
                    step1Result.companyClaimId = vClaim.COMPANY_CLAIM_ID.ToString();

                    if (companyId == 95 || companyId == 10)
                    {
                        CommonProxyRes commonProxyRes = new CommonProxyRes();
                        if (vClaim.STATUS == ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                        {
                            commonProxyRes = proxyServiceClient.ClaimApproval(stateUpdateReq);
                        }
                        else if (vClaim.STATUS == ((int)ClaimStatus.RET).ToString())
                        {
                            commonProxyRes = proxyServiceClient.ClaimRejection(stateUpdateReq);
                        }
                        else if (vClaim.STATUS == ((int)ClaimStatus.IPTAL).ToString())
                        {
                            commonProxyRes = proxyServiceClient.ClaimCancellation(stateUpdateReq);
                        }
                        else if (vClaim.STATUS == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || vClaim.STATUS == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                        {
                            commonProxyRes = proxyServiceClient.ClaimWait(stateUpdateReq);
                        }

                    }
                    else if (companyId == 322)
                    {
                        string newStatus = "";
                        if (vClaim.STATUS == ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                        {
                            newStatus = "1";
                        }
                        else if (vClaim.STATUS == ((int)ClaimStatus.RET).ToString())
                        {
                            newStatus = "4";
                        }
                        else if (vClaim.STATUS == ((int)ClaimStatus.IPTAL).ToString())
                        {
                            newStatus = "2";
                        }
                        else if (vClaim.STATUS == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || vClaim.STATUS == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                        {
                            newStatus = "3";
                        }

                        if (newStatus.IsInt())
                        {
                            NNSetProvisionStatusReq nNSetProvisionStatusReq = new NNSetProvisionStatusReq
                            {
                                ProvisionNumber = claimId.ToString(),
                                ProvisionNumberSpecified = true,
                                NewStatu = newStatus,
                                NewStatuSpecified = true,
                                ReasonDesc = vClaim.REASON_DESCRIPTION,
                                ReasonDescSpecified = true
                            };
                            nNHayatServiceClient.SetProvisionStatus(nNSetProvisionStatusReq);
                        }
                    }
                    else if (companyId == 5)
                    {
                        DogaServiceClient dogaServiceClient = new DogaServiceClient();
                        dogaServiceClient.HasarAktar("78cc5f34-42fe-40c3-9baf-8c45d8aada97", vClaim.CLAIM_ID);
                    }

                    result.Data = step1Result;
                    result.ResultCode = "100";
                    result.ResultMessage = "SUCCESS";
                }
                else
                {
                    throw new Exception("Hasar Bulunamadı!");
                }
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }

            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public JsonResult Reject(Int64 claimId, Int64 reasonId)
        {
            var result = new AjaxResultDto<ClaimRejectResult>();
            try
            {
                GenericRepository<ProteinEntities.Claim> claimRepository = new GenericRepository<ProteinEntities.Claim>();
                ProteinEntities.Claim claim = claimRepository.FindById(claimId);
                if (claim != null)
                {
                    claim.ReasonId = reasonId;
                    claim.Status = Convert.ToString((int)ClaimStatus.RET);
                    var spResponseClaim = claimRepository.Update(claim);
                    result.ResultCode = spResponseClaim.Code;
                    result.ResultMessage = spResponseClaim.Message;
                    if (spResponseClaim.Code == "100")
                    {
                        result.Data = new ClaimRejectResult
                        {
                            reason = ReasonHelper.GetReasonbyId(reasonId).Description,
                            reasonId = reasonId
                        };

                        ProxyServiceClient proxyService = new ProxyServiceClient();
                        ClaimStateUpdateReq stateUpdateReq = new ClaimStateUpdateReq { ClaimId = claimId };
                        if (claim.CompanyId != null && (claim.CompanyId == 95 || claim.CompanyId == 10))
                        {
                            if (claim.Status == ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                            {
                                proxyService.ClaimApproval(stateUpdateReq);
                            }
                            else if (claim.Status == ((int)ClaimStatus.RET).ToString())
                            {
                                proxyService.ClaimRejection(stateUpdateReq);
                            }
                            else if (claim.Status == ((int)ClaimStatus.IPTAL).ToString())
                            {
                                proxyService.ClaimCancellation(stateUpdateReq);
                            }
                            else if (claim.Status == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || claim.Status == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                            {
                                proxyService.ClaimWait(stateUpdateReq);
                            }
                        }
                        else if (claim.CompanyId == 322)
                        {
                            string newStatus = "";
                            if (claim.Status == ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                            {
                                newStatus = "1";
                            }
                            else if (claim.Status == ((int)ClaimStatus.RET).ToString())
                            {
                                newStatus = "4";
                            }
                            else if (claim.Status == ((int)ClaimStatus.IPTAL).ToString())
                            {
                                newStatus = "2";
                            }
                            else if (claim.Status == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || claim.Status == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                            {
                                newStatus = "3";
                            }

                            if (newStatus.IsInt())
                            {
                                NNSetProvisionStatusReq nNSetProvisionStatusReq = new NNSetProvisionStatusReq
                                {
                                    ProvisionNumber = claimId.ToString(),
                                    ProvisionNumberSpecified = true,
                                    NewStatu = newStatus,
                                    NewStatuSpecified = true,
                                    ReasonDesc = "",
                                    ReasonDescSpecified = true
                                };
                                new NNHayatServiceClientV2().SetProvisionStatus(nNSetProvisionStatusReq);
                            }
                        }
                        else if (claim.CompanyId == 5)
                        {
                            DogaServiceClient dogaServiceClient = new DogaServiceClient();
                            dogaServiceClient.HasarAktar("78cc5f34-42fe-40c3-9baf-8c45d8aada97", claim.Id);
                        }
                    }
                }
                else
                {
                    throw new Exception("Hasar bulunmadı!");
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(result),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        private ClaimProcessGroupList GetClaimProcessGroup(List<ClaimProcessGroupList> claimProcessGroupsList, string processGroupId)
        {
            foreach (ClaimProcessGroupList claimProcessGroup in claimProcessGroupsList)
            {
                if (claimProcessGroup.PROCESS_GROUP_ID.Equals(processGroupId))
                {
                    return claimProcessGroup;
                }
            }
            return null;
        }
    }
}
