﻿using Protein.Business.Abstract.Print;
using Protein.Business.Enums.Import;
using Protein.Business.Print;
using Protein.Business.Print.ClaimProviderPaymentList;
using Protein.Business.Print.ClaimProviderReturnForm;
using Protein.Common.Constants;
using Protein.Common.Dto;
using Protein.Common.Entities;
using Protein.Common.Extensions;
using Protein.Common.Helpers;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.InsuranceCompanies;
using Protein.Data.ExternalServices.Protein;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Web.ActionFilter.LoginAttr;
using Protein.Web.Models.ExportModel;
using Protein.Web.Models.HistoryModel;
using Protein.Web.Models.PayrollModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;
using static Protein.Data.ExternalServices.Common.Lib.NN.NNInputOutputTypes;

namespace Protein.Web.Controllers.Claim
{
    [LoginControl]
    public class PayrollController : Controller
    {
        // GET: Payroll
        [LoginControl]
        public ActionResult Index()
        {
            PayrollVM vm = new PayrollVM();
            vm.HistoryVM.ViewName = Views.Payroll;
            vm.HistoryVM.HistoryTitle = "Zarf";

            vm.ExportVM.ViewName = Views.Payroll;
            vm.ExportVM.SetExportColumns();
            vm.ExportVM.ExportType = ExportType.Excel;

            vm.ImportVM.ImportObjectType = ImportObjectType.PayrollStatus;

            FillCompanies();
            TempData["Alert"] = TempData["Alert"] ?? string.Empty;

            ViewBag.ProviderTypeList = LookupHelper.GetLookupData(LookupTypes.Provider);
            ViewBag.PayrollTypeList = LookupHelper.GetLookupData(LookupTypes.Payroll);
            ViewBag.PayrollCategoryTypeList = LookupHelper.GetLookupData(LookupTypes.PayrollCategory);
            ViewBag.PaymentTypeList = LookupHelper.GetLookupData(LookupTypes.Payment);
            ViewBag.PayrollStatusList = LookupHelper.GetLookupData(LookupTypes.PayrollStatus);
            var ProviderList = new GenericRepository<V_Provider>().FindBy(orderby: "PROVIDER_ID");
            ViewBag.ProviderList = ProviderList;
            var ProviderGroupList = new GenericRepository<ProviderGroup>().FindBy(orderby: "NAME");
            ViewBag.ProviderGroupList = ProviderGroupList;



            return View(vm);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetPayrollList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];
            string message = "1";
            PayrollVM vm = new PayrollVM();
            vm.HistoryVM.ViewName = Views.Payroll;
            vm.HistoryVM.HistoryTitle = "Zarf";

            vm.ExportVM.WhereCondition = "";

            vm.ImportVM.ImportObjectType = ImportObjectType.PayrollStatus;

            ViewResultDto<List<V_Payroll>> PayrollList = new ViewResultDto<List<V_Payroll>>();
            PayrollList.Data = new List<V_Payroll>();
            if (isFilter == "1")
            {
                String whereConditition = form["payrollCompany"].IsInt64() ? $" COMPANY_ID={long.Parse(form["payrollCompany"])} AND" : "";
                if (form["product"].IsInt64())
                {
                    whereConditition += $" PAYROLL_ID IN (SELECT PAYROLL_ID FROM V_CLAIM WHERE PRODUCT_ID={form["product"]}) AND";

                }

                whereConditition += form["payrollNo"].IsInt64() ? $" PAYROLL_ID = {long.Parse(form["payrollNo"])} AND" : "";
                whereConditition += form["oldPayrollNo"].IsInt64() ? $" OLD_PAYROLL_ID = {long.Parse(form["oldPayrollNo"])} AND" : "";
                whereConditition += !string.IsNullOrEmpty(form["providerPayrollNo"]) ? $" EXT_PROVIDER_NO = '{form["providerPayrollNo"]}' AND" : "";
                whereConditition += !string.IsNullOrEmpty(form["companyPayrollNo"]) ? $" COMPANY_PAYROLL_NO = '{form["companyPayrollNo"]}' AND" : "";
                whereConditition += !string.IsNullOrEmpty(form["payrollStatus"]) ? $" STATUS = '{form["payrollStatus"]}' AND" : "";
                whereConditition += form["payrollProvider"].IsInt64() ? $" PROVIDER_ID ={long.Parse(form["payrollProvider"])} AND" : "";
                whereConditition += form["payrollProviderGroup"].IsInt64() ? $" PROVIDER_GROUP_ID ={long.Parse(form["payrollProviderGroup"])} AND" : "";
                whereConditition += form["payrollDate"].IsDateTime() ? $" PAYROLL_DATE >= TO_DATE('{ DateTime.Parse(form["payrollDate"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["payrollPaymentDateStart"].IsDateTime() ? $" PAYMENT_DATE >= TO_DATE('{ DateTime.Parse(form["payrollPaymentDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["payrollPaymentDateEnd"].IsDateTime() ? $" PAYMENT_DATE <= TO_DATE('{ DateTime.Parse(form["payrollPaymentDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";

                if (whereConditition != "")
                {
                    PayrollList = new GenericRepository<V_Payroll>().FindByPaged(
                             conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                             orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},PAYROLL_ID DESC" : "PAYROLL_ID DESC",
                             pageNumber: start,
                             rowsPerPage: length,
                             fetchDeletedRows: true);
                }
                else
                {
                    message = "Lütfen Filtre alanlarını doldurup filtrelemeyi deneyiniz.";
                }
           

                vm.ExportVM.ViewName = Views.Payroll;
                vm.ExportVM.WhereCondition = (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3));
                vm.ExportVM.SetExportColumns();
            }
            return Json(new { data = PayrollList.Data, draw = Request["draw"], recordsTotal = PayrollList.TotalItemsCount, recordsFiltered = PayrollList.TotalItemsCount, whereConditition = vm.ExportVM.WhereCondition, message = message }, JsonRequestBehavior.AllowGet);
        }

        // GET: Payroll/Create
        [LoginControl]
        public ActionResult Create(Int64 id = 0, string formaction = "")
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;
                //Şirket
                FillCompanies();

                //Zarf Tipi
                var PayrollTypeList = LookupHelper.GetLookupData(LookupTypes.Payroll);
                ViewBag.PayrollTypeList = PayrollTypeList;

                //Zarf Kategori
                var EnvelopeCategoryList = LookupHelper.GetLookupData(LookupTypes.PayrollCategory);
                ViewBag.EnvelopeCategoryList = EnvelopeCategoryList;

                //Kurum
                var ProviderList = new GenericRepository<V_Provider>().FindBy(orderby: "PROVIDER_ID");
                ViewBag.ProviderList = ProviderList;

                ViewBag.ProviderTypeList = LookupHelper.GetLookupData(LookupTypes.Provider);

                ViewBag.PayrollStatusList = LookupHelper.GetLookupData(LookupTypes.PayrollStatus);

                ViewBag.CompanyNotChange = false;
                if (id > 0)
                {
                    var payroll = new GenericRepository<V_Payroll>().FindBy($"PAYROLL_ID={id}", orderby: "PAYROLL_ID").FirstOrDefault();
                    ViewBag.payroll = payroll ?? null;

                    var claimList = new GenericRepository<ProteinEntities.Claim>().FindBy($"PAYROLL_ID={id}", orderby: "PAYROLL_ID");
                    if (claimList.Count > 0)
                    {
                        ViewBag.CompanyNotChange = true;
                    }
                }
                switch (formaction.ToLower())
                {
                    case "view":
                        ViewBag.Title = "Zarfı Görüntüle";
                        ViewBag.FormDisabled = "disabled";
                        break;

                    case "edit":
                        ViewBag.Title = "Zarfı Güncelle";
                        ViewBag.FormDisabled = "";
                        break;

                    default:
                        ViewBag.Title = "Zarf Oluştur";
                        ViewBag.FormDisabled = "";
                        break;
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return View();
        }

        [HttpPost]
        [LoginControl]
        public JsonResult GetDueDate(string payrollDate,string type, string category, string providerId)
        {
            var result = "";

            try
            {
                if (payrollDate.IsDateTime())
                {
                    if (type == ((int)PayrollType.SIGORTALI).ToString())
                    {
                        result = DateTime.Parse(payrollDate).AddDays(15).ToString("dd-MM-yyyy");
                    }
                    else if (type == ((int)PayrollType.KURUM).ToString())
                    {
                        if (category.IsInt() && providerId.IsInt64())
                        {
                            var contract = new GenericRepository<V_ContractList>().FindBy($"PROVIDER_ID={providerId} AND CONTRACT_TYPE={category}", orderby: "").FirstOrDefault();
                            if (contract != null && contract.PAYMENT_DAY != null)
                            {
                                result = DateTime.Parse(payrollDate).AddDays((int)contract.PAYMENT_DAY).ToString("dd-MM-yyyy");
                            }
                        }
                    }
                    if (!result.IsDateTime())
                    {
                        result = DateTime.Parse(payrollDate).AddDays(30).ToString("dd-MM-yyyy");
                    }
                }
                else
                {
                    result = DateTime.Now.AddDays(30).ToString("dd-MM-yyyy");

                }
            }
            catch (Exception ex)
            {
                result = "";
            }

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        // POST: Payroll/Create
        [HttpPost]
        [LoginControl]
        public ActionResult Create(FormCollection form)
        {
            try
            {
                if (form["type"]==((int)PayrollType.KURUM).ToString() && !form["provider"].IsInt64())
                {
                    throw new Exception("Kurum tipinde zarf için Kurum bilgisi seçili olmalı...");
                }
                var payroll = new Payroll
                {
                    Id = form["hdPayrollId"].IsInt64() ? long.Parse(form["hdPayrollId"]) : 0,
                    PayrollDate = DateTime.Parse(form["payrollDate"]),
                    Type = form["type"],
                    Category = form["category"],
                    ProviderId = form["provider"].IsInt64() ? (long?)long.Parse(form["provider"]) : null,
                    CompanyPayrollNo = form["companyPayrollNo"],
                    ExtProviderNo = form["extProviderNo"]
                };

                var companyId = form["company"];
                if (companyId.IsInt64())
                {
                    payroll.CompanyId = long.Parse(companyId);
                }
                var extProviderDate = form["extProviderDate"];
                if (extProviderDate.IsDateTime())
                {
                    payroll.ExtProviderDate = DateTime.Parse(extProviderDate);
                }

                var dueDate = form["dueDate"];
                if (dueDate.IsDateTime())
                {
                    payroll.Due_Date = DateTime.Parse(dueDate);
                }
                else
                {
                    if (form["type"] == ((int)PayrollType.SIGORTALI).ToString())
                    {
                        payroll.Due_Date = DateTime.Parse(form["payrollDate"]).AddDays(15);
                    }
                    else if (form["type"] == ((int)PayrollType.KURUM).ToString())
                    {
                        if (form["category"].IsInt() && form["provider"].IsInt64())
                        {
                            var contract = new GenericRepository<V_ContractList>().FindBy($"PROVIDER_ID={form["provider"]} AND CONTRACT_TYPE={form["category"]}", orderby: "").FirstOrDefault();
                            if (contract != null && contract.PAYMENT_DAY != null)
                            {
                                payroll.Due_Date = DateTime.Parse(form["payrollDate"]).AddDays((int)contract.PAYMENT_DAY);
                            }
                        }
                    }
                    else
                    {
                        payroll.Due_Date = DateTime.Now.AddDays(30);
                    }
                }

                var spResponse = new PayrollRepository().Insert(payroll);
                if (spResponse.Code == "100")
                {
                    TempData["Alert"] = $"swAlert('İşlemi Başarılı','{spResponse.PkId} numaralı zarf başarıyla kaydedildi.','success')";
                }
                else
                {
                    throw new Exception(spResponse.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return RedirectToAction("Index");
        }

        // POST: Payroll/StatusChange/?PayrollId='5'&Status='1'
        [HttpPost]
        [LoginControl]
        public JsonResult StatusChange(string PayrollId, string Status)
        {
            var result = new AjaxResultDto<ExclusionSaveResult>();
            try
            {
                if (!PayrollId.IsInt64())
                {
                    throw new Exception("Zarf Bilgisisi Bulunamadı");
                }
                Payroll payroll = new GenericRepository<Payroll>().FindBy($"ID={long.Parse(PayrollId)}", fetchDeletedRows: true).FirstOrDefault();
                if (payroll == null)
                {
                    throw new Exception("Zarf Bilgisisi Bulunamadı");
                }

                var changeStatus = (Status == ((int)PayrollStatus.Onay).ToString()) ? ((int)PayrollStatus.Onay_Bekler).ToString() : ((int)PayrollStatus.Onay).ToString();
                if (payroll.Status == changeStatus)
                {
                    throw new Exception("Zarf Durumu Daha Önce Değiştirilmiş. Lütfen Kontrol Ediniz...");
                }
                if (changeStatus == ((int)PayrollStatus.Odendi).ToString())
                {
                    throw new Exception("Zarf Kullanıcı Tarafından Ödendi Durumuna Getirilemez. İçerisindeki Hasarların Tümü Ödendiğinde Otomatik Olarak Zarf Durumu Ödendi Yapılır.");
                }
                if (changeStatus == ((int)PayrollStatus.Onay_Bekler).ToString())
                {
                    var claim = new GenericRepository<ProteinEntities.Claim>().FindBy($"STATUS = '{(int)ClaimStatus.ODENDI}' AND PAYROLL_ID={PayrollId}", fetchDeletedRows: true);
                    if (claim.Count > 0)
                    {
                        throw new Exception("Zarf içerisinde ODENECEK veya ODENDI Durumunda Hasar Bulunmaktadır. Lütfen Hasar Durumlarını Kontrol Ediniz...");
                    }
                    //TO_DO: Yetki Kontrolü
                    //throw new Exception("Yetkili Kullanıcılar Dışında Zarfın Durumunu Onay Bekler'e Çekilemez. Lütfen Yetkilinize Danışın...");
                }
                if (changeStatus == ((int)PayrollStatus.Onay).ToString())
                {
                    if (payroll.CompanyId == 10 || payroll.CompanyId == 17)
                    {
                        var claim = new GenericRepository<ProteinEntities.Claim>().FindBy($"STATUS NOT IN('{(int)ClaimStatus.ODENECEK}','{(int)ClaimStatus.ODENDI}','{(int)ClaimStatus.RET}') AND PAYROLL_ID={PayrollId}", fetchDeletedRows: true);
                        if (claim.Count > 0)
                        {
                            //TO_DO: Güneş Ret Hasarların Klonlanması
                            throw new Exception("Zarfı Onaylamak için İçerisindeki Hasarların ODENECEK veya RET Durumunda Olması Gerekmektedir. Lütfen Hasar Durumlarını Kontrol Ediniz...");
                        }
                    }
                    else
                    {
                        var claim = new GenericRepository<ProteinEntities.Claim>().FindBy($"STATUS NOT IN('{(int)ClaimStatus.ODENECEK}','{(int)ClaimStatus.ODENDI}','{(int)ClaimStatus.RET}') AND PAYROLL_ID={PayrollId}", fetchDeletedRows: true);
                        if (claim.Count > 0)
                        {
                            throw new Exception("Zarfı Onaylamak için İçerisindeki Hasarların ODENECEK veya RET Durumunda Olması Gerekmektedir. Lütfen Hasar Durumlarını Kontrol Ediniz...");
                        }
                    }
                    payroll.VerificationDate = DateTime.Now;

                    var _Company = new CompanyHelper().WhichCompany((long)payroll.CompanyId);
                    if (_Company == Common.Enums.CompanyEnum.HDI || _Company == Common.Enums.CompanyEnum.HDI_TEST)
                    {
                        ProxyServiceClient serviceClient = new ProxyServiceClient();
                        PayrollTransferReq payrollTransferReq = new PayrollTransferReq
                        {
                            PayrollId = payroll.Id
                        };
                        serviceClient.PayrollTransfer(payrollTransferReq);
                    }
                    else if (_Company == Common.Enums.CompanyEnum.NN || _Company == Common.Enums.CompanyEnum.NN_TEST)
                    {
                        NNHayatServiceClientV2 serviceClient = new NNHayatServiceClientV2();
                        NNIcmalGirisRequest nNIcmalGirisRequest = new NNIcmalGirisRequest
                        {
                            PayrollId= payroll.Id
                        };
                        serviceClient.IcmalGiris(nNIcmalGirisRequest);
                    }
                    else if (_Company == Common.Enums.CompanyEnum.DOGA || _Company == Common.Enums.CompanyEnum.DOGA_TEST)
                    {
                        DogaServiceClient dogaServiceClient = new DogaServiceClient();
                        NNIcmalGirisRequest nNIcmalGirisRequest = new NNIcmalGirisRequest
                        {
                            PayrollId= payroll.Id
                        };
                        dogaServiceClient.IcmalAktar(apiKod: ConfigurationManager.AppSettings["ApiCode" + payroll.CompanyId], PayrollId: payroll.Id);
                    }
                }

                payroll.Status = changeStatus;
                SpResponse spResponse = new GenericRepository<Payroll>().Update(payroll);
                if (spResponse.Code != "100")
                {
                    throw new Exception("Zarf Durumu Değiştirilemedi");
                }
                result.ResultCode = spResponse.Code;
                result.ResultMessage = "Zarf Durumu Başarıyla Değiştirildi.";
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetReport()
        {
            var type = Request["Type"];
            var response = new PrintResponse();
            var payrollId = Request["PayrollId"];

            var payroll = new GenericRepository<Payroll>().FindBy($"STATUS != '{((int)PayrollStatus.Onay_Bekler).ToString()}' AND ID={payrollId}",orderby:"",fetchDeletedRows:true);

            if (payroll.Count() > 0 && payroll != null)
            {
            if (payrollId.IsInt64() && long.Parse(payrollId) > 0)
            {
                if (type == "0")
                {
                    var claim = new GenericRepository<ProteinEntities.Claim>().FindBy($"PAYROLL_ID={payrollId}");
                    var claimIdList = claim.Select(c => c.Id).ToList();
                    IPrint<PrintClaimProviderReturnFormReq> printt = new PrintClaimProviderReturnForm();
                    response = printt.DoWork(new PrintClaimProviderReturnFormReq
                    {
                        ClaimIdList = claimIdList
                    });
                }
                else if (type == "1")
                {
                    IPrint<PrintClaimProviderPaymentListReq> printt = new PrintClaimProviderPaymentList();
                    response = printt.DoWork(new PrintClaimProviderPaymentListReq
                    {
                        PayrollId = long.Parse(payrollId)
                    });
                }
            }
            }
            else
            {
                response.Message = "Zarf Onayı Olmayan Zarfların Bordro Basımı Yapılamamaktadır.";
                response.Code = "999";
            }
            return new JsonResult()
            {
                Data = new { FileUrl = response.Url, FileName = response.FileName, Message = response.Message, Code = response.Code }
            };
        }

        // GET: Payroll/Delete/5
        [LoginControl]
        public ActionResult Delete(int id)
        {
            var repo = new PayrollRepository();
            var result = repo.FindById(id);
            if (result != null)
            {
                result.Status = "1";
                repo.Update(result);
            }
            return View("Payroll");
        }

        [HttpPost]
        public JsonResult GetPayrollHistory()
        {
            var result = "[]";
            var payrollId = Request["payrollId"];

            var payrollList = new GenericRepository<Payroll>().FindBy($"(NEW_VERSION_ID = {payrollId} OR ID = {payrollId})", fetchHistoricRows: true, fetchDeletedRows: true);
            int i = 0;
            List<dynamic> listBill = new List<dynamic>();

            foreach (var item in payrollList)
            {
                var ResponseUser = new GenericRepository<V_ResponseUser>().FindBy($"OBJECT_ID='T_PAYROLL' AND PK_ID={item.Id}", orderby: "RESPONSE_ID ASC", fetchDeletedRows: true, fetchHistoricRows: true).FirstOrDefault();

                if (ResponseUser != null)
                {
                    if (i == 0)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.PROCESS = "Oluşturma";
                        listItem.PROCESS_USER = ResponseUser.USERNAME;
                        listItem.PROCESS_DATE = ((DateTime)ResponseUser.SP_RESPONSE_DATE).ToString("dd-MM-yyy");

                        listBill.Add(listItem);
                    }
                    else
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();
                        
                            listItem.PROCESS = "Onaylama";
                            listItem.PROCESS_USER = ResponseUser.USERNAME;
                            listItem.PROCESS_DATE = ((DateTime)ResponseUser.SP_RESPONSE_DATE).ToString("dd-MM-yyy");

                        listBill.Add(listItem);
                    }
                    i++;
                }
            }
            result = listBill.ToJSON();

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        #region Bill
        [HttpPost]
        public JsonResult GetBill()
        {
            var payrollId = Request["payrollId"];

            var BillList = new GenericRepository<V_Claim>().FindBy($"PAYROLL_ID={payrollId}", orderby: "BILL_NO");
            List<dynamic> listBill = new List<dynamic>();

            if (BillList != null)
            {
                int i = 1;

                foreach (var item in BillList)
                {
                    dynamic listItem = new System.Dynamic.ExpandoObject();
                    listItem.COMPANY_NAME = item.COMPANY_NAME;
                    listItem.PAYROLL_ID = item.PAYROLL_ID;
                    listItem.CLAIM_ID = item.CLAIM_ID;
                    listItem.FIRST_NAME = item.FIRST_NAME + " " + item.LAST_NAME;
                    listItem.IDENTITY_NO = item.IDENTITY_NO;
                    listItem.POLICY_NUMBER = item.POLICY_NUMBER;
                    listItem.POLICY_START_DATE = item.POLICY_START_DATE != null ? ((DateTime)item.POLICY_START_DATE).ToString("dd-MM-yyyy") : string.Empty;
                    listItem.PRODUCT_NAME = item.PRODUCT_NAME;
                    listItem.BILL_NO = item.BILL_NO;
                    listItem.BILL_DATE = item.BILL_DATE != null ? ((DateTime)item.BILL_DATE).ToString("dd-MM-yyyy") : string.Empty;
                    listItem.STATUS_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, item.STATUS);
                    listItem.CLAIM_STATUS = item.STATUS;
                    listItem.PAYROLL_STATUS = item.PAYROLL_STATUS;
                    listItem.PAYMENT_DATE = item.PAYMENT_DATE != null ? ((DateTime)item.PAYMENT_DATE).ToString("dd-MM-yyyy") : string.Empty;
                    listItem.AMOUNT = item.AMOUNT;
                    listItem.BANK_NAME = item.BANK_NAME;
                    listItem.IBAN = item.IBAN;
                    listBill.Add(listItem);

                    i++;
                }

                List<string> claimStatusCount = new List<string>();

                claimStatusCount.Add("BEKLEME - " + BillList.Count(c => c.STATUS == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || c.STATUS == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString()));
                claimStatusCount.Add("İPTAL - " + BillList.Count(c => c.STATUS == ((int)ClaimStatus.IPTAL).ToString()));
                claimStatusCount.Add("ÖDENDİ - " + BillList.Count(c => c.STATUS == ((int)ClaimStatus.ODENDI).ToString()));
                claimStatusCount.Add("ÖDENECEK - " + BillList.Count(c => c.STATUS == ((int)ClaimStatus.ODENECEK).ToString()));
                claimStatusCount.Add("PROVIZYON ONAY - " + BillList.Count(c => c.STATUS == ((int)ClaimStatus.PROZIYON_ONAY).ToString()));
                claimStatusCount.Add("RET - " + BillList.Count(c => c.STATUS == ((int)ClaimStatus.RET).ToString()));

                ViewBag.claimStatusCount = claimStatusCount;

                ViewBag.BillList = listBill;
            }

            var result = new
            {
                listBill = ViewBag.BillList,
                claimStatusCount = ViewBag.claimStatusCount
            };

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult FilterBill(FormCollection form)
        {
            object result = null;

            var payroll = new GenericRepository<Payroll>().FindBy("ID=:id",parameters:new {id= form["PayrollId"] }).FirstOrDefault();
            if (payroll != null)
            {
                dynamic parameters = new System.Dynamic.ExpandoObject();
                parameters.paymentMethod = payroll.Type;
                parameters.claimType = payroll.Category;
                parameters.companyId = payroll.CompanyId;
                
                String whereConditition = $" PAYROLL_ID IS NULL AND CLAIM_BILL_ID IS NOT NULL AND COMPANY_ID=:companyId AND PAYMENT_METHOD=:paymentMethod AND CLAIM_TYPE=:claimType AND STATUS!={((int)ClaimStatus.IPTAL)} AND";
                if (payroll.Type==((int)PayrollType.KURUM).ToString())
                {
                    parameters.providerId = payroll.ProviderId;
                    whereConditition += " PROVIDER_ID =:providerId AND";
                }
                //else
                //{
                //    parameters.providerId = form["payrollSelectProvider"];
                //    whereConditition += !String.IsNullOrEmpty(form["payrollSelectProvider"]) ? " PROVIDER_ID =:providerId AND" : "";
                //}
                whereConditition += !String.IsNullOrEmpty(form["claimNo"]) ? $" CLAIM_ID={long.Parse(form["claimNo"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["billNo"]) ? $" BILL_NO = {long.Parse(form["billNo"])} AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimInsuredName"]) ? $" FIRST_NAME LIKE '%{form["claimInsuredName"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimInsuredSurname"]) ? $" LAST_NAME LIKE '%{form["claimInsuredSurname"]}%' AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["billStartDate"]) ? $" BILL_DATE >= TO_DATE('{ DateTime.Parse(form["billStartDate"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["billEndDate"]) ? $" BILL_DATE <= TO_DATE('{ DateTime.Parse(form["billEndDate"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimStartDate"]) ? $" CLAIM_DATE >= TO_DATE('{ DateTime.Parse(form["claimStartDate"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += !String.IsNullOrEmpty(form["claimEndDate"]) ? $" CLAIM_DATE <= TO_DATE('{ DateTime.Parse(form["claimEndDate"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";

                List<V_Claim> resultClaimBill = new GenericRepository<V_Claim>().FindBy((string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)), orderby: "CLAIM_ID",parameters:parameters);
                result = resultClaimBill.ToJSON();
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        public JsonResult SelectClaim(FormCollection form)
        {
            var result = new AjaxResultDto<PayrollSaveResult>();
            try
            {
                if (!form["PayrollId"].IsInt64() || long.Parse(form["PayrollId"]) < 1)
                    throw new Exception("Zarf Bilgisi Bulunamadı");

                var payroll = new GenericRepository<Payroll>().FindById(long.Parse(form["PayrollId"]));
                if (payroll == null)
                    throw new Exception("Zarf Bilgisi Bulunamadı");

                if (!form["radioClaim"].IsInt64() || long.Parse(form["radioClaim"]) < 1)
                    throw new Exception("Hasar Bilgisi Bulunamadı");

                var claimlist = new GenericRepository<V_Claim>().FindBy($"(CLAIM_ID = {long.Parse(form["radioClaim"])} OR PAYROLL_ID={payroll.Id})", orderby: "", hint: "/*+ USE_HASH(claim_id) ORDERED */");
                if (claimlist == null && claimlist.Count < 1)
                    throw new Exception("Hasar Bilgisi Bulunamadı");

                var selectClaim = claimlist.Where(c => (c.CLAIM_ID == long.Parse(form["radioClaim"]))).FirstOrDefault();
                if (selectClaim == null)
                    throw new Exception("Hasar Bilgisi Bulunamadı");

                if (selectClaim.STATUS != ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() && selectClaim.STATUS != ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString() && selectClaim.STATUS != ((int)ClaimStatus.PROZIYON_ONAY).ToString() && selectClaim.STATUS != ((int)ClaimStatus.RET).ToString() && selectClaim.STATUS != ((int)ClaimStatus.ODENECEK).ToString())
                {
                    throw new Exception("Hasar Durumu Uygun Değil.");
                }

                if (selectClaim.PAID == null || selectClaim.PAID < 1)
                    throw new Exception("Hasara ait Şirket Tutarı Boş ya da 0 olamaz. Lütfen Teminat bilgilerini Kontrol Ediniz...");
                if (selectClaim.CLAIM_BILL_ID == null)
                    throw new Exception("Hasara ait Fatura Bilgisi Bulunamadı. Fatura Bilgilerini Kontrol Ediniz...");

                if ((payroll.Category == ((int)PayrollCategory.OSS).ToString() && selectClaim.CLAIM_TYPE != ((int)ClaimType.OSS).ToString()))
                    throw new Exception("Kategorisi OSS olan bir zarfa farklı tipte bir hasar eklenemez");

                if ((payroll.Category == ((int)PayrollCategory.TSS).ToString() && selectClaim.CLAIM_TYPE != ((int)ClaimType.TSS).ToString()))
                    throw new Exception("Kategorisi TSS olan bir zarfa farklı tipte bir hasar eklenemez");

                if (payroll.Type == ((int)PayrollType.KURUM).ToString())
                {
                    if (selectClaim.PAYMENT_METHOD != ((int)ClaimPaymentMethod.KURUM).ToString())
                        throw new Exception("Tipi KURUM olan bir zarfa sadece ödeme yöntemi KURUM olan hasarlar eklenebilir!");

                    if (selectClaim.PROVIDER_ID != payroll.ProviderId)
                        throw new Exception("Tipi KURUM olan bir zarfa farklı farklı kurumdan bir hasar eklenemez");
                }
                if (payroll.Type == ((int)PayrollType.SIGORTALI).ToString())
                {
                    if (selectClaim.PAYMENT_METHOD != ((int)ClaimPaymentMethod.SIGORTALI).ToString())
                        throw new Exception("Tipi SİGORTALI olan bir zarfa sadece ödeme yöntemi SİGORTALI olan hasarlar eklenebilir!");

                    var insuredBankAccount = new GenericRepository<V_InsuredBankAccount>().FindBy($"INSURED_ID={selectClaim.INSURED_ID}", orderby: "");
                    if (insuredBankAccount == null || insuredBankAccount.Count <= 0)
                    {
                        throw new Exception("Sigortalıya Ait Banka Hesap Bilgisi Bulunamadı. Lütfen Sigortalı Bilgilerini Güncelleyiniz...");
                    }

                    var differentClaim = claimlist.Where(c => c.CLAIM_ID != selectClaim.CLAIM_ID && c.INSURED_ID == selectClaim.INSURED_ID).FirstOrDefault();
                    if (differentClaim == null)
                    {
                        differentClaim = claimlist.Where(c => c.CLAIM_ID != selectClaim.CLAIM_ID && c.FAMILY_NO == selectClaim.FAMILY_NO).FirstOrDefault();
                        if (differentClaim == null)
                        {
                            throw new Exception("Tipi SIGORTALI olan bir zarfa farklı sigortalı hasarı eklenemez.");
                        }
                    }
                }

                if (selectClaim.MEDULA_NO != null)
                {
                    var payrollClaimList = claimlist.Where(c => (c.CLAIM_ID != long.Parse(form["hdClaimId"]))).ToList();
                    foreach (var item in payrollClaimList)
                    {
                        if (item.MEDULA_NO != null)
                        {
                            if (item.MEDULA_NO == selectClaim.MEDULA_NO)
                            {
                                throw new Exception("Aynı Medula No'ya ait hasarlar aynı zarfın içinde bulanamaz.");
                            }
                        }
                    }
                }

                //var claim = new GenericRepository<ProteinEntities.Claim>().FindById(long.Parse(form["radioClaim"]));
                //if (claim == null)
                //    throw new Exception("Hasar Bilgisi Bulunamadı");

                //if (claim.Status != ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() && claim.Status != ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString() && claim.Status != ((int)ClaimStatus.PROZIYON_ONAY).ToString() && claim.Status != ((int)ClaimStatus.RET).ToString() && claim.Status != ((int)ClaimStatus.ODENECEK).ToString())
                //{
                //    throw new Exception("Hasar Durumu Uygun Değil.");
                //}

                //if ((payroll.Category == ((int)PayrollCategory.OSS).ToString() && claim.Type != ((int)ClaimType.OSS).ToString()))
                //    throw new Exception("Kategorisi OSS olan bir zarfa farklı tipte bir hasar eklenemez");

                //if ((payroll.Category == ((int)PayrollCategory.TSS).ToString() && claim.Type != ((int)ClaimType.TSS).ToString()))
                //    throw new Exception("Kategorisi TSS olan bir zarfa farklı tipte bir hasar eklenemez");

                //if (payroll.Type == ((int)PayrollType.KURUM).ToString())
                //{
                //    if (claim.PaymentMethod != ((int)ClaimPaymentMethod.KURUM).ToString())
                //        throw new Exception("Tipi KURUM olan bir zarfa farklı ödeme şekli olan bir hasar eklenemez");

                //    if (claim.ProviderId!=payroll.ProviderId)
                //        throw new Exception("Tipi KURUM olan bir zarfa farklı bir Kuruma ait hasar eklenemez");
                //}
                //if ((payroll.Type == ((int)PayrollType.SIGORTALI).ToString() && claim.PaymentMethod != ((int)ClaimPaymentMethod.SIGORTALI).ToString()))
                //    throw new Exception("Tipi SIGORTALI olan bir zarfa farklı ödeme şekli olan bir hasar eklenemez");


                var claim = new GenericRepository<ProteinEntities.Claim>().FindById(long.Parse(form["radioClaim"]));
                claim.PayrollId = long.Parse(form["PayrollId"]);
                SpResponse spResponseClaim = new GenericRepository<ProteinEntities.Claim>().Update(claim);
                if (spResponseClaim.Code != "100")
                    throw new Exception("Zarfa Hasar Ekleme İşlemi Gerçekleştirilemedi");

                result.ResultCode = spResponseClaim.Code;
                result.ResultMessage = "Zarfa Hasar Ekleme İşlemi Başarıyla Gerçekleştirildi.";
                result.Data = new PayrollSaveResult
                {
                    payrollId = spResponseClaim.PkId.ToString(),
                    payrollDescription = $"No:{spResponseClaim.PkId.ToString()} - Tarih:{form["payrollDate"]} - Tip:{LookupHelper.GetLookupTextByOrdinal(LookupTypes.Payroll, form["payrollType"])} - Kategori:{LookupHelper.GetLookupTextByOrdinal(LookupTypes.PayrollCategory, form["payrollCategory"])}"
                };
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult PayrollExit(Int64 claimId)
        {
            var result = new AjaxResultDto<PayrollExitResult>();
            try
            {
                if (claimId < 1)
                    throw new Exception("Hasar Bilgisi Bulunamadı");

                var claim = new GenericRepository<ProteinEntities.Claim>().FindById(claimId);

                if (claim == null)
                    throw new Exception("Hasar Bilgisi Bulunamadı");

                if (claim.Status == ((int)ClaimStatus.ODENECEK).ToString() || claim.Status == ((int)ClaimStatus.ODENDI).ToString())
                    throw new Exception("ODENDI veya ODENECEK durumundaki Hasar Zarftan Çıkarılamaz!");

                var payrollId = claim.PayrollId;
                claim.PayrollId = null;

                if (claim.Status == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                {
                    if (claim.ReasonId != null)
                    {
                        // TODO - Provizyon Bekleme durumu için bekleme gerekçesi olan hasarların tamamı statik olarak Eksik evrak nedenine bağlandı
                        //        Veritabanından eşlenmesi daha uygun olacaktır.
                        Reason reason = ReasonHelper.GetReasonData("ClaimStatus", ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString(), "3");
                        claim.ReasonId = reason.Id;
                    }
                    claim.Status = ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString();
                }

                SpResponse spResponseClaim = new GenericRepository<ProteinEntities.Claim>().UpdateForAll(claim);
                if (spResponseClaim.Code != "100")
                    throw new Exception("Hasarı Zarftan Çıkarma İşlemi Gerçekleştirilemedi");

                var BillList = new GenericRepository<V_Claim>().FindBy($"PAYROLL_ID={payrollId}", orderby: "BILL_NO");
                List<dynamic> listBill = new List<dynamic>();

                if (BillList != null)
                {
                    int i = 1;

                    foreach (var item in BillList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();
                        listItem.COMPANY_NAME = item.COMPANY_NAME;
                        listItem.PAYROLL_ID = item.PAYROLL_ID;
                        listItem.CLAIM_ID = item.CLAIM_ID;
                        listItem.FIRST_NAME = item.FIRST_NAME + " " + item.LAST_NAME;
                        listItem.IDENTITY_NO = item.IDENTITY_NO;
                        listItem.POLICY_NUMBER = item.POLICY_NUMBER;
                        listItem.POLICY_START_DATE = item.POLICY_START_DATE != null ? ((DateTime)item.POLICY_START_DATE).ToString("dd-MM-yyyy") : string.Empty;
                        listItem.PRODUCT_NAME = item.PRODUCT_NAME;
                        listItem.BILL_NO = item.BILL_NO;
                        listItem.BILL_DATE = item.BILL_DATE != null ? ((DateTime)item.BILL_DATE).ToString("dd-MM-yyyy") : string.Empty;
                        listItem.STATUS_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, item.STATUS);
                        listItem.CLAIM_STATUS = item.STATUS;
                        listItem.PAYROLL_STATUS = item.PAYROLL_STATUS;
                        listItem.PAYMENT_DATE = item.PAYMENT_DATE != null ? ((DateTime)item.PAYMENT_DATE).ToString("dd-MM-yyyy") : string.Empty;
                        listItem.AMOUNT = item.AMOUNT;
                        listItem.BANK_NAME = item.BANK_NAME;
                        listItem.IBAN = item.IBAN;
                        listBill.Add(listItem);

                        i++;
                    }
                }
                result.ResultCode = spResponseClaim.Code;
                result.ResultMessage = "Hasar Zarftan Başarıyla Çıkartıldı.";
                result.Data = new PayrollExitResult
                {
                    jsonData = listBill.ToJSON()
                };
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region DropDown fill methods 

        private void FillCompanies()
        {
            var v_company = new GenericRepository<V_Company>().FindBy(orderby: "COMPANY_ID");
            ViewBag.v_company = v_company;
        }

        private void FillProducts()
        {
            var v_product =new GenericRepository<V_Product>().FindBy(orderby: "PRODUCT_ID");
            ViewBag.v_product = v_product;
        }

        private void FillProducts(Int64 Id)
        {
            var v_product = new GenericRepository<V_Product>().FindBy("COMPANY_ID =:id", orderby: "PRODUCT_ID", parameters: new { id = Id });
            ViewBag.v_product = v_product;
        }

        #endregion
    }
}
