﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Protein.Web.ActionFilter.LoginAttr
{
    public class LoginControl : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Session["Personnel"] == null)
            {

                filterContext.HttpContext.Response.Redirect("/Dashboard/Login?returnUrl=" + filterContext.HttpContext.Request.RawUrl);
            }

            base.OnActionExecuting(filterContext);
        }
    }
}