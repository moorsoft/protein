﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Protein.Web.ActionFilter.LoginAttr
{
    public class LoginCompanyControl : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Session["CompanyId"] == null)
            {
                filterContext.HttpContext.Response.Redirect("/Dashboard/Login");
            }

            var companyId = filterContext.HttpContext.Session["CompanyId"];

            base.OnActionExecuting(filterContext);
        }
    }
}