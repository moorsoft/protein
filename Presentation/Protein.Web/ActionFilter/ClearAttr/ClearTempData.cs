﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Protein.Web.ActionFilter.ClearAttr
{
    public class ClearTempData : ActionFilterAttribute
    {
        public string TempDataKey { get; set; }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.Controller.TempData[TempDataKey] = string.Empty;
            base.OnActionExecuting(filterContext);
        }
    }
}