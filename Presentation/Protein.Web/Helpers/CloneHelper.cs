﻿using Protein.Business.Concrete.Media;
using Protein.Common.Dto.MediaObjects;
using Protein.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Web.Helpers
{
    public class CloneHelper
    {
        public static long AddressClone(long addressId)
        {
            var oldAddress = new AddressRepository().FindById(addressId);
            Address address = new Address()
            {
                Id = 0,
                CityId = oldAddress.CityId,
                CountyId = oldAddress.CountyId,
                Details = oldAddress.Details,
                District = oldAddress.District,
                IsPrimary = oldAddress.IsPrimary,
                Type = oldAddress.Type,
                ZipCode = oldAddress.ZipCode,
                Status = oldAddress.Status
            };
            var spResponseAddress = new AddressRepository().Insert(address);
            if (spResponseAddress.Code != "100")
            {
                throw new Exception(spResponseAddress.Code + " : " + spResponseAddress.Message);
            }
            return spResponseAddress.PkId;
        }

        public static long ContactClone(long contactId)
        {
            var oldContact = new ContactRepository().FindById(contactId);
            Contact contact = new Contact()
            {
                Id = 0,
                Type = oldContact.Type,
                Title = oldContact.Title,
                IdentityNo = oldContact.IdentityNo,
                TaxOffice = oldContact.TaxOffice,
                TaxNumber = oldContact.TaxNumber,
                Status = oldContact.Status
            };
            var spResponseContact = new ContactRepository().Insert(contact);
            if (spResponseContact.Code != "100")
            {
                throw new Exception(spResponseContact.Code + " : " + spResponseContact.Message);
            }
            var newContactId = spResponseContact.PkId;
            if (contact.Type.Equals(((int)ContactType.GERCEK).ToString()))
            {
                List<Person> personList = new PersonRepository().FindBy(conditions: "CONTACT_ID = " + oldContact.Id);
                if (personList != null && personList.Count > 0)
                {
                    if (personList.Count == 1)
                    {
                        var oldPerson = personList.FirstOrDefault();
                        long personId = 0;
                        if (oldPerson.Id > 0)
                        {
                            personId = PersonClone(oldPerson.Id, newContactId);
                        }
                    }
                    else
                    {
                        throw new Exception("Klonlanacak varlık için birden fazla Gerçek Kişilik bilgisi bulundu!");
                    }
                }
                else
                {
                    throw new Exception("Klonlanacak varlık için Gerçek Kişilik bilgileri bulunamadı!");
                }
            }
            else if (contact.Type.Equals(((int)ContactType.TUZEL).ToString()))
            {
                List<Corporate> corporateList = new CorporateRepository().FindBy(conditions: "CONTACT_ID = " + oldContact.Id);
                if (corporateList != null && corporateList.Count > 0)
                {
                    if (corporateList.Count == 1)
                    {
                        var oldCorporate = corporateList.FirstOrDefault();
                        long corporateId = 0;
                        if (oldCorporate.Id > 0)
                        {
                            corporateId = CorporateClone(oldCorporate.Id, newContactId);
                        }
                    }
                    else
                    {
                        throw new Exception("Klonlanacak varlık için birden fazla Tüzel Kişilik bilgisi bulundu!");
                    }
                }
                else
                {
                    throw new Exception("Klonlanacak varlık için Tüzel Kişilik bilgileri bulunamadı!");
                }
            }
            else
            {
                throw new Exception("Klonlanacak varlık için Kişilik tipi tanımı gerçerli değildir!");
            }
            return newContactId;
        }

        private static long CorporateClone(long corporateId, long newContactId)
        {
            var oldCorporate = new CorporateRepository().FindById(corporateId);
            Corporate corporate = new Corporate()
            {
                Id = 0,
                ContactId = newContactId,
                Name = oldCorporate.Name,
                Type = oldCorporate.Type,
                Status = oldCorporate.Status
            };
            var spResponseCorporate = new CorporateRepository().Insert(corporate);
            if (spResponseCorporate.Code != "100")
            {
                throw new Exception(spResponseCorporate.Code + " : " + spResponseCorporate.Message);
            }
            return spResponseCorporate.PkId;
        }

        private static long PersonClone(long personId, long newContactId)
        {
            var oldPerson = new PersonRepository().FindById(personId);
            Person person = new Person()
            {
                Id = 0,
                Birthdate = oldPerson.Birthdate,
                Birthplace = oldPerson.Birthplace,
                ContactId = newContactId,
                FirstName = oldPerson.FirstName,
                Gender = oldPerson.Gender,
                LastName = oldPerson.LastName,
                LicenseNo = oldPerson.LicenseNo,
                MaritalStatus = oldPerson.MaritalStatus,
                MiddleName = oldPerson.MiddleName,
                NameOfFather = oldPerson.NameOfFather,
                NationalityId = oldPerson.NationalityId,
                PassportNo = oldPerson.PassportNo,
                Photo = oldPerson.Photo,
                ProfessionType = oldPerson.ProfessionType,
                Status = oldPerson.Status
            };
            var spResponsePerson = new PersonRepository().Insert(person);
            if (spResponsePerson.Code != "100")
            {
                throw new Exception(spResponsePerson.Code + " : " + spResponsePerson.Message);
            }
            return spResponsePerson.PkId;
        }

        public static long BankAccountClone(long bankAccountId)
        {
            var oldBankAccount = new BankAccountRepository().FindById(bankAccountId);
            BankAccount bankAccount = new BankAccount()
            {
                Id = 0,
                BankBranchId = oldBankAccount.BankBranchId,
                BankId = oldBankAccount.BankId,
                CurrencyType = oldBankAccount.CurrencyType,
                Iban = oldBankAccount.Iban,
                IsPrimary = oldBankAccount.IsPrimary,
                Name = oldBankAccount.Name,
                No = oldBankAccount.No,
                Status = oldBankAccount.Status
            };
            var spResponseBankAccount = new BankAccountRepository().Insert(bankAccount);
            if (spResponseBankAccount.Code != "100")
            {
                throw new Exception(spResponseBankAccount.Code + " : " + spResponseBankAccount.Message);
            }
            return spResponseBankAccount.PkId;
        }

        public static long CompanyBankAccountClone(long companyBankAccountId, long newCompanyId, long newBankAccountId)
        {
            var oldCompanyBankAccount = new CompanyBankAccountRepository().FindById(companyBankAccountId);
            CompanyBankAccount companyBankAccount = new CompanyBankAccount()
            {
                Id = 0,
                BankAccountId = newBankAccountId,
                CompanyId = newCompanyId,
                Status = oldCompanyBankAccount.Status
            };
            var spResponseCompanyBankAccount = new CompanyBankAccountRepository().Insert(companyBankAccount);
            if (spResponseCompanyBankAccount.Code != "100")
            {
                throw new Exception(spResponseCompanyBankAccount.Code + " : " + spResponseCompanyBankAccount.Message);
            }
            return spResponseCompanyBankAccount.PkId;
        }

        public static long PhoneClone(long phoneId)
        {
            var oldPhone = new PhoneRepository().FindById(phoneId);
            Phone phone = new Phone()
            {
                Id = 0,
                Type = oldPhone.Type,
                IsPrimary = oldPhone.IsPrimary,
                No = oldPhone.No,
                CountryId = oldPhone.CountryId,
                Extension = oldPhone.Extension,
                Status = oldPhone.Status
            };
            var spResponsePhone = new PhoneRepository().Insert(phone);
            if (spResponsePhone.Code != "100")
            {
                throw new Exception(spResponsePhone.Code + " : " + spResponsePhone.Message);
            }
            return spResponsePhone.PkId;
        }

        public static long CompanyPhoneClone(long companyPhoneId, long newCompanyId, long newPhoneId)
        {
            var oldCompanyPhone = new CompanyPhoneRepository().FindById(companyPhoneId);
            CompanyPhone companyPhone = new CompanyPhone()
            {
                Id = 0,
                PhoneId = newPhoneId,
                CompanyId = newCompanyId,
                Status = oldCompanyPhone.Status
            };
            var spResponseCompanyPhone = new CompanyPhoneRepository().Insert(companyPhone);
            if (spResponseCompanyPhone.Code != "100")
            {
                throw new Exception(spResponseCompanyPhone.Code + " : " + spResponseCompanyPhone.Message);
            }
            return spResponseCompanyPhone.PkId;
        }

        public static long NoteClone(long noteId)
        {
            var oldNote = new NoteRepository().FindById(noteId);
            Note note = new Note()
            {
                Id = 0,
                Type = oldNote.Type,
                Description = oldNote.Description,
                Status = oldNote.Status
            };
            var spResponseNote = new NoteRepository().Insert(note);
            if (spResponseNote.Code != "100")
            {
                throw new Exception(spResponseNote.Code + " : " + spResponseNote.Message);
            }
            return spResponseNote.PkId;
        }

        public static long CompanyNoteClone(long companyNoteId, long newCompanyId, long newNoteId)
        {
            var oldCompanyNote = new CompanyNoteRepository().FindById(companyNoteId);
            CompanyNote companyNote = new CompanyNote()
            {
                Id = 0,
                NoteId = newNoteId,
                CompanyId = newCompanyId,
                Status = oldCompanyNote.Status
            };
            var spResponseCompanyNote = new CompanyNoteRepository().Insert(companyNote);
            if (spResponseCompanyNote.Code != "100")
            {
                throw new Exception(spResponseCompanyNote.Code + " : " + spResponseCompanyNote.Message);
            }
            return spResponseCompanyNote.PkId;
        }

        public static long UserClone(long userId)
        {
            var oldUser = new UserRepository().FindById(userId);
            User user = new User()
            {
                Id = 0,
                Username = oldUser.Username,
                Password = oldUser.Password,
                Email = oldUser.Email,
                Mobile = oldUser.Mobile,
                IsRememberMe = oldUser.IsRememberMe,
                Status = oldUser.Status
            };
            var spResponseUser = new UserRepository().Insert(user);
            if (spResponseUser.Code != "100")
            {
                throw new Exception(spResponseUser.Code + " : " + spResponseUser.Message);
            }
            return spResponseUser.PkId;
        }

        public static long CompanUserClone(long companyUserId, long newCompanyId, long userId)
        {

            var oldCompanyUser = new CompanyUserRepository().FindById(companyUserId);
            CompanyUser companyUser = new CompanyUser()
            {
                Id = 0,
                UserId = userId,
                CompanyId = newCompanyId,
                Status = oldCompanyUser.Status
            };
            var spResponseCompanyUser = new CompanyUserRepository().Insert(companyUser);
            if (spResponseCompanyUser.Code != "100")
            {
                throw new Exception(spResponseCompanyUser.Code + " : " + spResponseCompanyUser.Message);
            }
            return spResponseCompanyUser.PkId;
        }

        public static long CompanyContactClone(long companyContactId, long newCompanyId, long contactId)
        {
            var oldCompanyContact = new CompanyContactRepository().FindById(companyContactId);
            CompanyContact companyContact = new CompanyContact()
            {
                Id = 0,
                CompanyId = newCompanyId,
                ContactId = contactId,
                Status = oldCompanyContact.Status
            };
            var spResponseCompanyContact = new CompanyContactRepository().Insert(companyContact);
            if (spResponseCompanyContact.Code != "100")
            {
                throw new Exception(spResponseCompanyContact.Code + " : " + spResponseCompanyContact.Message);
            }
            return spResponseCompanyContact.PkId;
        }

        public static long ParameterClone(long parameterId)
        {
            var oldParameter = new ParameterRepository().FindById(parameterId);
            Parameter parameter = new Parameter()
            {
                Id = 0,
                Environment = oldParameter.Environment,
                Key = oldParameter.Key,
                Value = oldParameter.Value,
                Status = oldParameter.Status,
                SystemType = oldParameter.SystemType
            };
            var spResponseParameter = new ParameterRepository().Insert(parameter);
            if (spResponseParameter.Code != "100")
            {
                throw new Exception(spResponseParameter.Code + " : " + spResponseParameter.Message);
            }
            return spResponseParameter.PkId;

        }

        public static long CompanyParameterClone(long companyParameterId, long newCompanyId, long parameterId)
        {
            var oldCompanyParameter = new CompanyParameterRepository().FindById(companyParameterId);
            CompanyParameter companyParameter = new CompanyParameter()
            {
                Id = 0,
                CompanyId = newCompanyId,
                ParameterId = parameterId,
                Status = oldCompanyParameter.Status

            };
            var spResponseCompanyParameter = new CompanyParameterRepository().Insert(companyParameter);
            if (spResponseCompanyParameter.Code != "100")
            {
                throw new Exception(spResponseCompanyParameter.Code + " : " + spResponseCompanyParameter.Message);
            }
            return spResponseCompanyParameter.PkId;
        }

        public static long EmailClone(long emailId)
        {
            var oldEmail = new EmailRepository().FindById(emailId);
            Email email = new Email()
            {
                Id = 0,
                Type = oldEmail.Type,
                Details = oldEmail.Details,
                IsPrimary = oldEmail.IsPrimary,
                Status = oldEmail.Status
            };
            var spResponseEmail = new EmailRepository().Insert(email);
            if (spResponseEmail.Code != "100")
            {
                throw new Exception(spResponseEmail.Code + " : " + spResponseEmail.Message);
            }
            return spResponseEmail.PkId;
        }

        public static long CompanyEmailClone(long companyEmailId, long newCompanyId, long emailId)
        {
            var oldCompanyEmail = new CompanyEmailRepository().FindById(companyEmailId);
            CompanyEmail companyEmail = new CompanyEmail()
            {
                Id = 0,
                CompanyId = newCompanyId,
                EmailId = emailId,
                Status = oldCompanyEmail.Status

            };
            var spResponseCompanyEmail = new CompanyEmailRepository().Insert(companyEmail);
            if (spResponseCompanyEmail.Code != "100")
            {
                throw new Exception(spResponseCompanyEmail.Code + " : " + spResponseCompanyEmail.Message);
            }
            return spResponseCompanyEmail.PkId;
        }

        public static long ProductNoteClone(long productNoteId, long newProductId, long noteId)
        {
            var oldProductNote = new ProductNoteRepository().FindById(productNoteId);
            ProductNote productNote = new ProductNote()
            {
                Id = 0,
                NoteId = noteId,
                ProductId = newProductId,
                Status = oldProductNote.Status
            };
            var spResponseProductNote = new ProductNoteRepository().Insert(productNote);
            if (spResponseProductNote.Code != "100")
            {
                throw new Exception(spResponseProductNote.Code + " : " + spResponseProductNote.Message);
            }
            return spResponseProductNote.PkId;
        }

        public static long RuleProductClone(long ruleProductId, long newProductId, long ruleId)
        {
            var oldRuleProduct = new RuleProductRepository().FindById(ruleProductId);
            RuleProduct ruleProduct = new RuleProduct()
            {
                Id = 0,
                RuleId = ruleId,
                ProductId = newProductId,
                Status = oldRuleProduct.Status
            };
            var spResponseRuleProduct = new RuleProductRepository().Insert(ruleProduct);
            if (spResponseRuleProduct.Code != "100")
            {
                throw new Exception(spResponseRuleProduct.Code + " : " + spResponseRuleProduct.Message);
            }
            return spResponseRuleProduct.PkId;
        }

        public static long MediaClone<T>(long mediaId, long ObjectId) where T : class, new()
        {
            T instance = Activator.CreateInstance<T>();
            MediaDto cMedia = new MediaDto
            {
                MediaId = mediaId,
                ObjectId = ObjectId
            };

            new MediaCreator<T>().Clone(cMedia);
            return 0;
            #region OldCode
            //var oldMedia = new MediaRepository().FindById(mediaId);
            //Media media = new Media()
            //{
            //    Id = 0,
            //    FileContent = oldMedia.FileContent,
            //    FileName = oldMedia.FileName,
            //    Name = oldMedia.Name,
            //    Status = oldMedia.Status
            //};
            //var spResponseMedia = new MediaRepository().Insert(media);
            //if (spResponseMedia.Code != "100")
            //{
            //    throw new Exception(spResponseMedia.Code + " : " + spResponseMedia.Message);
            //}
            //return spResponseMedia.PkId;
            #endregion
        }

        public static long ProductMediaClone(long productMediaId, long newProductId, long mediaId)
        {
            var oldProductMedia = new ProductMediaRepository().FindById(productMediaId);
            ProductMedia productMedia = new ProductMedia()
            {
                Id = 0,
                MediaId = mediaId,
                ProductId = newProductId,
                Status = oldProductMedia.Status

            };
            var spResponseProductMedia = new ProductMediaRepository().Insert(productMedia);
            if (spResponseProductMedia.Code != "100")
            {
                throw new Exception(spResponseProductMedia.Code + " : " + spResponseProductMedia.Message);
            }
            return spResponseProductMedia.PkId;
        }

        public static long ProductParameterClone(long productParameterId, long newProductId, long parameterId)
        {
            var oldProductParameter = new ProductParameterRepository().FindById(productParameterId);
            ProductParameter productParameter = new ProductParameter()
            {
                Id = 0,
                ProductId = newProductId,
                ParameterId = parameterId,
                Status = oldProductParameter.Status
            };
            var spResponseProductParameter = new ProductParameterRepository().Insert(productParameter);
            if (spResponseProductParameter.Code != "100")
            {
                throw new Exception(spResponseProductParameter.Code + " : " + spResponseProductParameter.Message);
            }
            return spResponseProductParameter.PkId;
        }

        public static long SubproductNoteClone(long subproductNoteId, long newSubproductId, long noteId)
        {
            var oldSubproductNote = new SubproductNoteRepository().FindById(subproductNoteId);
            SubproductNote subproductNote = new SubproductNote()
            {
                Id = 0,
                NoteId = noteId,
                SubproductId = newSubproductId,
                Status = oldSubproductNote.Status
            };
            var spResponseSubproductNote = new SubproductNoteRepository().Insert(subproductNote);
            if (spResponseSubproductNote.Code != "100")
            {
                throw new Exception(spResponseSubproductNote.Code + " : " + spResponseSubproductNote.Message);
            }
            return spResponseSubproductNote.PkId;
        }

        public static long SubproductParameterClone(long subproductParameterId, long newSubproductId, long parameterId)
        {
            var oldSubproductParameter = new SubproductParameterRepository().FindById(subproductParameterId);
            SubproductParameter subproductParameter = new SubproductParameter()
            {
                Id = 0,
                SubproductId = newSubproductId,
                ParameterId = parameterId,
                Status = oldSubproductParameter.Status
            };
            var spResponseSubproductParameter = new SubproductParameterRepository().Insert(subproductParameter);
            if (spResponseSubproductParameter.Code != "100")
            {
                throw new Exception(spResponseSubproductParameter.Code + " : " + spResponseSubproductParameter.Message);
            }
            return spResponseSubproductParameter.PkId;
        }

        public static long SubproductMediaClone(long subproductMediaId, long newSubproductId, long mediaId)
        {
            var oldSubproductMedia = new SubproductMediaRepository().FindById(subproductMediaId);
            SubproductMedia subproductMedia = new SubproductMedia()
            {
                Id = 0,
                MediaId = mediaId,
                SubproductId = newSubproductId,
                Status = oldSubproductMedia.Status

            };
            var spResponseSubproductMedia = new SubproductMediaRepository().Insert(subproductMedia);
            if (spResponseSubproductMedia.Code != "100")
            {
                throw new Exception(spResponseSubproductMedia.Code + " : " + spResponseSubproductMedia.Message);
            }
            return spResponseSubproductMedia.PkId;
        }

        public static long CoverageClone(long coverageId)
        {
            var oldCoverage = new CoverageRepository().FindById(coverageId);
            Coverage coverage = new Coverage()
            {
                Id = 0,
                Name = oldCoverage.Name + "-Kopya",
                BranchId = oldCoverage.BranchId,
                IsTax = oldCoverage.IsTax,
                LocationType = oldCoverage.LocationType,
                Status = oldCoverage.Status,
                Type = oldCoverage.Type

            };
            var spResponseCoverage = new CoverageRepository().Insert(coverage);
            if (spResponseCoverage.Code != "100")
            {
                throw new Exception(spResponseCoverage.Code + " : " + spResponseCoverage.Message);
            }
            return spResponseCoverage.PkId;
        }

        public static void PlanCoverageClone(long planCoverageId, long newPlanId, long? newPackageId)
        {
            #region Clone Parent Plan Coverage
            var parentPlanCoverageId = PlanCoverageClone(planCoverageId, newPlanId, newPackageId, null);
            #endregion

            if (parentPlanCoverageId > 0)
            {
                #region Clone Children of Parent Plan Coverage
                var childrenPlanCoverageList = new PlanCoverageRepository().FindBy("PARENT_ID=:id", parameters: new { id = planCoverageId });
                if (childrenPlanCoverageList != null)
                {
                    foreach (var childPlanCoverage in childrenPlanCoverageList)
                    {
                        #region Clone Child Plan Coverage
                        if (childPlanCoverage.Id > 0)
                        {
                            PlanCoverageClone(childPlanCoverage.Id, newPlanId, newPackageId, parentPlanCoverageId);
                        }
                        #endregion
                    }
                }
                #endregion
            }
        }

        private static long PlanCoverageClone(long oldPlanCoverageId, long newPlanId, long? newPackageId, long? newParentId)
        {
            #region Clone Plan Coverage
            var oldPlanCoverage = new PlanCoverageRepository().FindById(oldPlanCoverageId);
            PlanCoverage planCoverage = new PlanCoverage()
            {
                Id = 0,
                CoverageId = oldPlanCoverage.CoverageId,
                CoverageName = oldPlanCoverage.CoverageName,
                LocationType = oldPlanCoverage.LocationType,
                PlanId = newPlanId,
                NetworkId = oldPlanCoverage.NetworkId,
                Status = oldPlanCoverage.Status
            };
            if (newPackageId != null && newPackageId > 0)
            {
                planCoverage.PackageId = newPackageId;
            }
            if (newParentId != null && newParentId > 0)
            {
                planCoverage.ParentId = newParentId;
            }
            var spResponsePlanCoverage = new PlanCoverageRepository().Insert(planCoverage);
            if (spResponsePlanCoverage.Code != "100")
            {
                throw new Exception(spResponsePlanCoverage.Code + " : " + spResponsePlanCoverage.Message);
            }
            var newPlanCoverageId = spResponsePlanCoverage.PkId;
            #endregion
            #region Clone Plan Coverage Variation
            var planCoverageVariationList = new PlanCoverageVariationRepository().FindBy("PLAN_COVERAGE_ID=:id", parameters: new { id = oldPlanCoverage.Id });
            if (planCoverageVariationList != null)
            {
                foreach (var planCoverageVariation in planCoverageVariationList)
                {
                    if (planCoverageVariation.Id > 0)
                    {
                        PlanCoverageVariationClone(planCoverageVariation.Id, newPlanCoverageId);
                    }
                }
            }
            #endregion

            #region Clone Plan Coverage Parameter
            var planCoverageParameterList = new PlanCoverageParameterRepository().FindBy("PLAN_COVERAGE_ID=:id", parameters: new { id = oldPlanCoverage.Id });
            if (planCoverageParameterList != null)
            {
                foreach (var planCoverageParameter in planCoverageParameterList)
                {
                    #region Parameter Clone
                    long parameterId = 0;
                    if (planCoverageParameter.ParameterId > 0)
                    {
                        parameterId = ParameterClone(planCoverageParameter.ParameterId);
                    }
                    if (parameterId > 0)
                    {
                        #region Clone PlanCoverage Parameter Relation
                        if (planCoverageParameter.Id > 0)
                        {
                            PlanCoverageParameterClone(planCoverageParameter.Id, newPlanCoverageId, parameterId);
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            #endregion
            return newPlanCoverageId;
        }

        public static void PlanCoverageVariationClone(long planCoverageVariationId, long newPlanCoverageId)
        {
            var oldPlanCoverageVariation = new PlanCoverageVariationRepository().FindById(planCoverageVariationId);
            PlanCoverageVariation planCoverageVariation = new PlanCoverageVariation()
            {
                Id = 0,
                AgreementType = oldPlanCoverageVariation.AgreementType,
                CoinsuranceRatio = oldPlanCoverageVariation.CoinsuranceRatio,
                CurrencyType = oldPlanCoverageVariation.CurrencyType,
                DayLimit = oldPlanCoverageVariation.DayLimit,
                ExemptionLimit = oldPlanCoverageVariation.ExemptionLimit,
                ExemptionType = oldPlanCoverageVariation.ExemptionType,
                NumberLimit = oldPlanCoverageVariation.NumberLimit,
                PlanCoverageId = newPlanCoverageId,
                PriceFactor = oldPlanCoverageVariation.PriceFactor,
                PriceLimit = oldPlanCoverageVariation.PriceLimit,
                SessionCount = oldPlanCoverageVariation.SessionCount,
                Status = oldPlanCoverageVariation.Status,
                Type = oldPlanCoverageVariation.Type

            };
            var spResponsePlanCoverageVariation = new PlanCoverageVariationRepository().Insert(planCoverageVariation);
            if (spResponsePlanCoverageVariation.Code != "100")
            {
                throw new Exception(spResponsePlanCoverageVariation.Code + " : " + spResponsePlanCoverageVariation.Message);
            }
        }

        public static void PlanCoverageParameterClone(long planCoverageParameterId, long newPlanCoverageId, long newParameterId)
        {
            var oldPlanCoverageParameter = new PlanCoverageParameterRepository().FindById(planCoverageParameterId);
            PlanCoverageParameter planCoverageParameter = new PlanCoverageParameter()
            {
                Id = 0,
                ParameterId = newParameterId,
                PlanCoverageId = newPlanCoverageId,
                Status = oldPlanCoverageParameter.Status
            };
            var spResponsePlanCoverageParameter = new PlanCoverageParameterRepository().Insert(planCoverageParameter);
            if (spResponsePlanCoverageParameter.Code != "100")
            {
                throw new Exception(spResponsePlanCoverageParameter.Code + " : " + spResponsePlanCoverageParameter.Message);
            }
        }

        public static long PackageNoteClone(long packageNoteId, long newPackageId, long noteId)
        {
            var oldPackageNote = new PackageNoteRepository().FindById(packageNoteId);
            PackageNote packageNote = new PackageNote()
            {
                Id = 0,
                NoteId = noteId,
                PackageId = newPackageId,
                Status = oldPackageNote.Status
            };
            var spResponsePackagetNote = new PackageNoteRepository().Insert(packageNote);
            if (spResponsePackagetNote.Code != "100")
            {
                throw new Exception(spResponsePackagetNote.Code + " : " + spResponsePackagetNote.Message);
            }
            return spResponsePackagetNote.PkId;
        }

        public static long PackageParameterClone(long packageParameterId, long newPackageId, long parameterId)
        {
            var oldPackageParameter = new PackageParameterRepository().FindById(packageParameterId);
            PackageParameter packageParameter = new PackageParameter()
            {
                Id = 0,
                PackageId = newPackageId,
                ParameterId = parameterId,
                Status = oldPackageParameter.Status

            };
            var spResponsePackageParameter = new PackageParameterRepository().Insert(packageParameter);
            if (spResponsePackageParameter.Code != "100")
            {
                throw new Exception(spResponsePackageParameter.Code + " : " + spResponsePackageParameter.Message);
            }
            return spResponsePackageParameter.PkId;
        }

        public static long PackageTPAPriceClone(long packageTPAPriceId, long newPackageId)
        {
            var oldPackageTPAPrice = new PackageTPAPriceRepository().FindById(packageTPAPriceId);
            PackageTPAPrice packageTPAPrice = new PackageTPAPrice()
            {
                Id = 0,
                Amount = oldPackageTPAPrice.Amount,
                CashPaymentType = oldPackageTPAPrice.CashPaymentType,
                PackageId = newPackageId,
                StartDate = oldPackageTPAPrice.StartDate,
                Status = oldPackageTPAPrice.Status,
                Type = oldPackageTPAPrice.Type
            };
            var spResponsePackageTPAPrice = new PackageTPAPriceRepository().Insert(packageTPAPrice);
            if (spResponsePackageTPAPrice.Code != "100")
            {
                throw new Exception(spResponsePackageTPAPrice.Code + " : " + spResponsePackageTPAPrice.Message);
            }
            return spResponsePackageTPAPrice.PkId;
        }

        public static long PackageMediaClone(long packageMediaId, long newPackageId, long mediaId)
        {
            var oldPackageMedia = new PackageMediaRepository().FindById(packageMediaId);
            PackageMedia packageMedia = new PackageMedia()
            {
                Id = 0,
                MediaId = mediaId,
                PackageId = newPackageId,
                Status = oldPackageMedia.Status

            };
            var spResponsePackageMedia = new PackageMediaRepository().Insert(packageMedia);
            if (spResponsePackageMedia.Code != "100")
            {
                throw new Exception(spResponsePackageMedia.Code + " : " + spResponsePackageMedia.Message);
            }
            return spResponsePackageMedia.PkId;
        }

        public static long ProcessClone(long processId)
        {
            var oldProcess = new ProcessRepository().FindById(processId);
            Process process = new Process()
            {
                Id = 0,
                Amount = oldProcess.Amount,
                Code = oldProcess.Code,
                Name = oldProcess.Name,
                ParentId = oldProcess.ParentId,
                Status = oldProcess.Status,
                ProcessListId = oldProcess.ProcessListId

            };
            var spResponseProcess = new ProcessRepository().Insert(process);
            if (spResponseProcess.Code != "100")
            {
                throw new Exception(spResponseProcess.Code + " : " + spResponseProcess.Message);
            }
            return spResponseProcess.PkId;
        }

        public static long CoverageProcessClone(long coverageProcessId, long newCoverageId)
        {
            var oldCoverageProcess = new ProcessCoverageRepository().FindById(coverageProcessId);
            ProcessCoverage processCoverage = new ProcessCoverage()
            {
                Id = 0,
                CoverageId = newCoverageId,
                ProcessId = oldCoverageProcess.ProcessId,
                Status = oldCoverageProcess.Status

            };
            var spResponseCoverageProcess = new ProcessCoverageRepository().Insert(processCoverage);
            if (spResponseCoverageProcess.Code != "100")
            {
                throw new Exception(spResponseCoverageProcess.Code + " : " + spResponseCoverageProcess.Message);
            }
            return spResponseCoverageProcess.PkId;
        }

        public static long CoverageParameterClone(long coverageParameterId, long newCoverageId, long parameterId)
        {
            var oldCoverageParameter = new CoverageParameterRepository().FindById(coverageParameterId);
            CoverageParameter coverageParameter = new CoverageParameter()
            {
                Id = 0,
                CoverageId = newCoverageId,
                ParameterId = parameterId,
                Status = oldCoverageParameter.Status

            };
            var spResponseCoverageParameter = new CoverageParameterRepository().Insert(coverageParameter);
            if (spResponseCoverageParameter.Code != "100")
            {
                throw new Exception(spResponseCoverageParameter.Code + " : " + spResponseCoverageParameter.Message);
            }
            return spResponseCoverageParameter.PkId;
        }

        public static long PriceListClone(long priceListId)
        {
            PriceList oldPriceList = new GenericRepository<PriceList>().FindById(priceListId);
            long newPriceListId = 0;
            if (oldPriceList != null)
            {
                #region Clone PriceList
                PriceList priceList = new PriceList()
                {
                    Id = 0,
                    Status = oldPriceList.Status,
                    ValidFrom = oldPriceList.ValidFrom,
                    ValidTo = oldPriceList.ValidTo
                };
                SpResponse spResponsePriceList = new GenericRepository<PriceList>().Insert(priceList);
                if (spResponsePriceList.Code != "100")
                {
                    throw new Exception(spResponsePriceList.Code + ":" + spResponsePriceList.Message);
                }
                newPriceListId = spResponsePriceList.PkId;
                priceList.Id = newPriceListId;
                #endregion
                if (newPriceListId > 0)
                {
                    #region Clone Prices of PriceList
                    List<Price> prices = new GenericRepository<Price>().FindBy("PRICE_LIST_ID=:id", parameters: new { id = oldPriceList.Id });
                    foreach (var price in prices)
                    {
                        #region Clone Price
                        long priceId = 0;
                        if (price.Id > 0)
                        {
                            Price newPrice = new Price()
                            {
                                Id = 0,
                                AgeStart = price.AgeStart,
                                AgeEnd = price.AgeEnd,
                                Gender = price.Gender,
                                DayCount = price.DayCount,
                                IndividualType = price.IndividualType,
                                Premium = price.Premium,
                                PriceListId = newPriceListId,
                                Status = price.Status
                            };
                            SpResponse spResponsePrice = new GenericRepository<Price>().Insert(newPrice);
                            if (spResponsePrice.Code != "100")
                            {
                                throw new Exception(spResponsePrice.Code + ":" + spResponsePrice.Message);
                            }
                            priceId = spResponsePrice.PkId;
                            newPrice.Id = priceId;
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            return newPriceListId;
        }
    }
}