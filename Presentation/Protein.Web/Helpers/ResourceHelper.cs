﻿using Protein.Common.Entities;
using Protein.Common.Resources;
using System;
using System.Web;
using System.Web.Mvc;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Web.Helpers
{
    public static class ResourceHelper
    {
        public static NetworkResource ConvertToNetworkResource(FormCollection Form, HttpRequestBase Request)
        {
            NetworkResource networkResource = new NetworkResource();
            if (Request != null)
            {
                networkResource.Offset = Convert.ToInt32(Request["start"]);
                networkResource.Limit = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
                networkResource.SearchValue = Request["search[value]"];
                networkResource.SortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
                networkResource.SortDirection = Request["order[0][dir]"];
            }
            if (Form != null)
            {
                networkResource.Name = Form["networkName"];
                if (!string.IsNullOrEmpty(Form["networkType"]))
                {
                    networkResource.Type = (NetworkType)Convert.ToInt32(Form["networkType"]);
                }
                if (!string.IsNullOrEmpty(Form["networkCategory"]))
                {
                    networkResource.Category = (NetworkCategoryType)Convert.ToInt32(Form["networkCategory"]);
                }
            }
            return networkResource;
        }

        public static Network ConvertToNetwork(FormCollection Form)
        {
            var networkId = long.Parse(Form["hdNetworkId"]);
            var network = new Network
            {
                Id = networkId,
                Name = Form["networkName"].ToUpper(),
                Category = Form["networkCategory"],
                Type = Form["networkType"]
            };
            return network;
        }
    }
}