﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Protein.Web.Models.ImportModel;
using Protein.Web.Models.MediaModel;

namespace Protein.Web.Models.ProviderFormModel
{
    public class ProviderFormVM
    {
        public ImportVM ImportVM { get; set; }
        public MediaVM MediaVM { get; set; }

        public ProviderFormVM()
        {
            ImportVM = new ImportVM();
            MediaVM = new MediaVM();
        }
    }
}