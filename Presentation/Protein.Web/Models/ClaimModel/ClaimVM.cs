﻿using Protein.Web.Models.ExportModel;
using Protein.Web.Models.ImportModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Protein.Web.Models.ClaimModel
{
    public class ClaimVM
    {
        public ImportVM ImportVM { get; set; }
        public ExportVM ExportVM { get; set; }

        public ClaimVM()
        {
            ImportVM = new ImportVM();
            ExportVM = new ExportVM();
        }
    }
}