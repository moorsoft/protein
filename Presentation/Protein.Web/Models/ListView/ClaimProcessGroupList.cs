﻿using System.Collections.Generic;

namespace Protein.Web.Models.ListView
{
    public class ClaimProcessGroupList
    {
        public string CLAIM_ID { get; set; }
        public string COVERAGE_ID { get; set; }
        public string COVERAGE_NAME { get; set; }
        public string PROCESS_GROUP_ID { get; set; }
        public string PROCESS_GROUP_NAME { get; set; }
        public string PROCESS_LIST_ID { get; set; }
        public string PROCESS_LIST_NAME { get; set; }

        public List<ClaimProcessList> CLAIM_PROCESSES { get; set; } 
    }
}