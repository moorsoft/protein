﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Protein.Web.Models.ListView
{
    public class EndorsementList
    {
        [DisplayName("Id")]
        public string ENDORSEMENT_ID { get; set; }
        public string POLICY_ID { get; set; }
        public string COMPANY_NAME { get; set; }
        public string INSURER_NAME { get; set; }
        public string ENDORSEMENT_PREMIUM { get; set; }
        public string ENDORSEMENT_NO { get; set; }
        public string ENDORSEMENT_TYPE_TEXT { get; set; }
        public string ENDORSEMENT_TYPE { get; set; }
        public string ENDORSEMENT_START_DATE { get; set; }
        public string ENDORSEMENT_DATE_OF_ISSUE { get; set; }
        public string STATUS { get; set; }
        public string IS_BACK_STATUS { get; set; }
    }
}