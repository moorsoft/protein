﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Protein.Web.Models.ListView
{
    public class NetworkList
    {
        public string NETWORK_ID { get; set; }
        public string NETWORK_NAME { get; set; }
        public string NETWORK_CATEGORY_TEXT { get; set; }
        public string NETWORK_TYPE_TEXT { get; set; }
    }
}