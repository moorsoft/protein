﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Protein.Web.Models.ListView
{
    public class ProcessGroupList
    {
        public string PROCESS_GROUP_ID { get; set; }
        public string PROCESS_GROUP_NAME { get; set; }
        public string PROCESS_LIST_NAME { get; set; }
        public string PROCESS_LIST_TYPE_TEXT { get; set; }

    }
}