﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Protein.Web.Models.ListView
{
    public class ProcessListList
    {
        public string PROCESS_LIST_ID { get; set; }
        public string PROCESS_LIST_NAME { get; set; }
        public string PRICE_TYPE_TEXT { get; set; }
        public string PROCESS_LIST_TYPE_TEXT { get; set; }
    }
}