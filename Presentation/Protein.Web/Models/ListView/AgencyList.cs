﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Protein.Web.Models.ListView
{
    public class AgencyList
    {
        public string AGENCY_ID { get; set; }
        public string COMPANY_ID { get; set; }
        public string COMPANY_NAME { get; set; }
        public string AGENCY_NAME { get; set; }      
        public string AGENCY_NO { get; set; }
        public string STATUS { get; set; }
        public string CREATE_DATE{ get; set; }
        public string MODIFICATION_DATE { get; set; }
        public string CITY_NAME { get; set; }
        public string COUNTY_NAME { get; set; }
        public string IS_OPEN { get; set; }

    }
}