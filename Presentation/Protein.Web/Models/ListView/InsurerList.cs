﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Protein.Web.Models.ListView
{
    public class InsurerList
    {
        public string CONTACT_ID { get; set; }
        public string CONTACT_TYPE { get; set; }
        public string CONTACT_TITLE { get; set; }
        public Int64? IDENTITY_NO { get; set; }
        public Int64? TAX_NUMBER { get; set; }
    }
}