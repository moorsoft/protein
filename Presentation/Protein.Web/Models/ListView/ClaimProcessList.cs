﻿namespace Protein.Web.Models.ListView
{
    public class ClaimProcessList
    {
        public string INDEX { get; set; }
        public string CLAIM_ID { get; set; }
        public string COVERAGE_ID { get; set; }
        public string COVERAGE_NAME { get; set; }
        public string PROCESS_ID { get; set; }      
        public string PROCESS_NAME { get; set; }
        public string PROCESS_CODE { get; set; }
        public string PROCESS_AMOUNT { get; set; }
        public string PROCESS_GROUP_ID { get; set; }
        public string PROCESS_GROUP_NAME { get; set; }
        public string PROCESS_LIST_ID { get; set; }
        public string PROCESS_LIST_NAME { get; set; }
        public int? COVERAGE_COUNT { get; set; }
    }
}