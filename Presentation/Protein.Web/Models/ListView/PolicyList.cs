﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Protein.Web.Models.ListView
{
    public class PolicyList
    {
        [DisplayName("Id")]
        public string POLICY_ID { get; set; }
        public string COMPANY_NAME { get; set; }
        public string PRODUCT_NAME { get; set; }
        public string POLICY_TYPE_TEXT { get; set; }
        public string POLICY_NUMBER_RENEWAL_NO { get; set; }
        public Int64? POLICY_NUMBER { get; set; }
        public string RENEWAL_NO { get; set; }
        public string INSURER_NAME { get; set; }
        public string POLICY_START_DATE { get; set; }
        public string POLICY_END_DATE { get; set; }
        public string PREMIUM { get; set; }
        public string IS_OPEN_TO_CLAIM { get; set; }
        public string IS_NOT_ACTIVE_ENDORSEMENT { get; set; }
        public string STATUS { get; set; }
    }
}