﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Protein.Web.Models.ListView
{
    public class InstallmentList
    {
        [DisplayName("Id")]
        public string INSTALLMENT_ID { get; set; }
        public string NO { get; set; }
        public decimal AMOUNT { get; set; }
        public string DUE_DATE { get; set; }
    }
}