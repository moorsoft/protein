﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Protein.Web.Models.ListView
{
    public class CoverageList
    {
        public string COVERAGE_ID { get; set; }
        public string COVERAGE_NAME { get; set; }
        public string COVERAGE_TYPE_TEXT { get; set; }
    }
}