﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Protein.Web.Models.ListView
{
    public class InsuredList
    {
        public string INSURED_ID { get; set; }
        public string CONTACT_ID { get; set; }
        public string INSURER_NAME { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public Int64? IDENTITY_NO { get; set; }
        public string INDIVIDUAL_TYPE_TEXT { get; set; }
        public string FAMILY_NO { get; set; }
        public string BIRTHDATE { get; set; }
        public string PREMIUM { get; set; }
        public string USE_PREMIUM { get; set; }
        public string EXIT_PREMIUM { get; set; }
        public string GENDER { get; set; }
        public string AGE { get; set; }
        public string IS_OPEN_TO_CLAIM { get; set; }
        public string ENDORSEMENT_ID_LIST { get; set; }
    }
}