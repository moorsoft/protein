﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Protein.Web.Models.ListView
{
    public class PackageList
    {
        public string PACKAGE_ID { get; set; }
        public string COMPANY_NAME { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string SUBPRODUCT_CODE { get; set; }
        public string PACKAGE_NO { get; set; }
        public string PACKAGE_NAME { get; set; }
        public string IS_INSURED_PACKAGE { get; set; }
        public string IS_OPEN_TO_WS { get; set; }
    }
}