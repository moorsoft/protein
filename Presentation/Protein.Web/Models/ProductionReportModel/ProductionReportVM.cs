﻿using Protein.Web.Models.ExportModel;
using Protein.Web.Models.ImportModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Protein.Web.Models.ProductionReportModel
{
    public class ProductionReportVM
    {
        public ExportVM ExportVM { get; set; }
        public ExportVM SumExportVM { get; set; }

        public ProductionReportVM()
        {
            ExportVM = new ExportVM();
            SumExportVM = new ExportVM();
        }
    }
}