﻿using Protein.Business.Concrete.Responses;
using Protein.Business.Enums.Import;
using Protein.Business.Import;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Protein.Web.Models.ImportModel
{
    [Serializable]
    public class ImportVM
    {
        public HttpPostedFileBase file { get; set; }
        public ImportResponse Response { get; set; }
        public ImportObjectType ImportObjectType { get; set; }
        public Int64 Id { get; set; }
        public string FormId { get; set; }
        public bool ShowCompanies { get; set; } = false;
        public List<CompanyObjectForImport> Companies { get; set; }
        public string SelectedCompany { get; set; }
        public long SelectedCompanyId { get; set; }

        public ImportVM()
        {
            Response = new ImportResponse();
            Companies = new List<CompanyObjectForImport>();
        }
    }
    public class CompanyObjectForImport
    {
        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
    }
}