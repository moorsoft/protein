﻿using Protein.Common.Entities;
using Protein.Data.Repositories;
using Protein.Data.Workers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;

namespace Protein.Web.Models.HistoryModel
{
    public class HistoryVM
    {
        public string ViewName { get; set; }
        public int Id { get; set; }
        public string HistoryTitle { get; set; }

        public List<HistoryResponse> historyList { get; set; }
        public HistoryVM()
        {
            historyList = new List<HistoryResponse>();
        }

        public void GetHistory()
        {

            switch (this.ViewName)
            {
                case Views.ContractListHist:

                    History<V_ContractListHist> history = new History<V_ContractListHist>();
                    this.historyList = history.GetHistory(Id);
                    break;
                case Views.Payroll:

                    History<V_Payroll> historyPayroll = new History<V_Payroll>();
                    this.historyList = historyPayroll.GetHistory(Id);
                    break;
                default: break;
            }
        }
    }
}