﻿using Protein.Business.Enums.Media;
using Protein.Common.Dto.MediaObjects;
using Protein.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Web.Models.MediaModel
{

    public class MediaVM
    {
        public HttpPostedFileBase MEDIA_PATH { get; set; }
        public string FileName { get; set; }
        [DisplayName("Adı")]
        public string MediaName { get; set; }
        public string TabName { get; set; }
        /// <summary>
        /// örn: T_PRODUCT_MEDIA tablosu için  ID kolonu
        /// </summary>
        public long BaseId { get; set; } = 0;
        /// <summary>
        /// örn: T_PRODUCT_MEDIA için  CONTRACT_ID
        /// </summary>
        public long ObjectId { get; set; } = 0;
        /// <summary>
        /// örn: T_PRODUCT_MEDIA için MEDIA_ID
        /// </summary>
        public long MediaId { get; set; } = 0;
        public MediaObjectType MediaObjectType { get; set; }
        public List<MediaListDto> MediaList { get; set; }
        public bool ShowDateInfo { get; set; } = false;
        public MediaVM()
        {
            MediaList = new List<MediaListDto>();
        }

    }
}