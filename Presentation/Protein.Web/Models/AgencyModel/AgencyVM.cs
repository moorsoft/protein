﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Protein.Web.Models.ExportModel;
using Protein.Web.Models.ImportModel;

namespace Protein.Web.Models.AgencyModel
{
    public class AgencyVM
    {
        public ImportVM ImportVM { get; set; }
        public ExportVM ExportVM { get; set; }

        public AgencyVM()
        {
            ImportVM = new ImportVM();
            ExportVM = new ExportVM();
        }
    }
}