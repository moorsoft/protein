﻿using Protein.Web.Models.ExportModel;
using Protein.Web.Models.HistoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Protein.Web.Models.ContractModel
{
    public class ContractVM
    {
        public ExportVM ExportVM { get; set; }
        public HistoryVM HistoryVM { get; set; }
        public ContractVM()
        {
            ExportVM = new ExportVM();
            HistoryVM = new HistoryVM();
        }
    }
}