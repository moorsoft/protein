﻿using Protein.Web.Models.ExportModel;
using Protein.Web.Models.HistoryModel;
using Protein.Web.Models.ImportModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Protein.Web.Models.PayrollModel
{
    public class PayrollVM
    {
        public ExportVM ExportVM { get; set; }
        public HistoryVM HistoryVM { get; set; }
        public ImportVM ImportVM { get; set; }
        public PayrollVM()
        {
            ImportVM = new ImportVM();
            ExportVM = new ExportVM();
            HistoryVM = new HistoryVM();
        }
    }
}