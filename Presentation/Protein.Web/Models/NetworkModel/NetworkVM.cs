﻿using Protein.Web.Models.ExportModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Protein.Web.Models.NetworkModel
{
    public class NetworkVM
    {
        public Int64 Id { get; set; }
        public string Type { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public Int64 NewVersionId { get; set; }
        public string Status { get; set; }
        public ExportVM ExportVM { get; set; }
        public NetworkVM()
        {
            ExportVM = new ExportVM();
        }
    }
}