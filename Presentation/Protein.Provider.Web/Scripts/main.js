
(function($) {
  "use strict"; // Start of use strict

  // Toggle the side navigation
  $("#sidebarToggle").click(function(e) {
    e.preventDefault();
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
  });

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
    if ($window.width() > 768) {
      var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    }
  });

  // Scroll to top button appear
  $(document).scroll(function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  // Smooth scrolling using jQuery easing
  $(document).on('click', 'a.scroll-to-top', function(event) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    event.preventDefault();
  });

})(jQuery); // End of use strict

function Loading() {
    $.unblockUI();
    $.blockUI({ message: $('#domMessage') });
}

if( $('.table-long').length > 0 ) {
      $(document).ready(function() {

          $('.table-long').DataTable({
              pagingType: 'input',
              "order": [[ 0, "desc" ]],
              pageLength: 10,
              language: {
                  "search": "Arama:",
                  "sLengthMenu":" _MENU_  Adet Kayıt Listelendi",
                  "zeroRecords":"Kayıt Bulunamadı !",
                  "emptyTable": "Kayıt Bulunamadı !",
                  "infoEmpty": "Kayıt Bulunamadı !",
                  "info": "_TOTAL_ Adet Kayıttan _START_  ile _END_ Arası Sonuç Listelendi.",
                  oPaginate: {
                      sNext: 'İleri <i class="fa fa-angle-right"></i>',
                      sPrevious: '<i class="fa fa-angle-left"></i> Geri',
                      sFirst: '<i class="fa fa-angle-double-left"></i> İlk Sayfa',
                      sLast: 'Son Sayfa <i class="fa fa-angle-double-right"></i>'
                  }
              },
              "searching": true,
              "aLengthMenu": [[10, 25, 50, 75, 100, -1], [10, 25, 50, 75, 100, "Alles"]],
              "pagingType": "full_numbers"
          });
      });
  }


  if( $('.table-short').length > 0 ) {
        $(document).ready(function() {
            $('.table-short').DataTable({
                pagingType: 'input',
                "order": [[ 0, "desc" ]],
                pageLength: 5,
                language: {
                    "search": "Arama:",
                    "sLengthMenu":" _MENU_  Adet Kayıt Listelendi",
                    "zeroRecords":"Kayıt Bulunamadı !",
                    "emptyTable": "Kayıt Bulunamadı !",
                    "infoEmpty": "Kayıt Bulunamadı !",
                    "info": "_TOTAL_ Adet Kayıttan _START_  ile _END_ Arası Sonuç Listelendi.",
                    oPaginate: {
                        sNext: 'İleri <i class="fa fa-angle-right"></i>',
                        sPrevious: '<i class="fa fa-angle-left"></i> Geri',
                        sFirst: '<i class="fa fa-angle-double-left"></i> İlk Sayfa',
                        sLast: 'Son Sayfa <i class="fa fa-angle-double-right"></i>'
                    }
                },
                "searching": true,
                "aLengthMenu": [[5, 15, 50, 75, 100, -1], [5, 15, 50, 75, 100, "Alles"]],
                "pagingType": "full_numbers"
            });
        });
    }

    if ($('.chosen-select').length > 0) {
    $('.chosen-select').each(function() {
      var element = $(this);
      element.on('chosen:ready', function(e, chosen) {
        var width = element.css("width");
        element.next().find('.chosen-choices').addClass('form-control');
        element.next().css("width", width);
        element.next().find('.search-field input').css("width", "125px");
      }).chosen();
    });
  }

  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

$('.modal').on('hidden.bs.modal', function (e) {
    var noresetVal = $(this).data('noreset');
    if (typeof noresetVal == "undefined") {
        $('.modal input[type="hidden"].inputHidden').prop("value", "0").end();
        $(this).find('form').trigger('reset');
        $(this).find('form select').prop('selected', '').trigger('change');
        $(".modal .chosen-select").val('').trigger("chosen:updated");
        $(".modal .chosen-container-single .chosen-single span").text("Seçiniz").end();
        $(".modal .bootstrap-select .btn-default .filter-option").text("Seçiniz").end();
        $(".asyncResult").text('');
        if ($('.modal').hasClass('in')) {
            $('body').addClass('modal-open');
        }
    }
});

$('.prevTab').click(function () {
    $('#' + $('.nav-item > .active').parent().prev().find('a').attr('id')).tab('show');
});

function getErrorMessage(errorMessageType, errorMessage) {
    if (errorMessageType == 'success') {
        toastr.options = {
            "closeButton": true,
            "progressBar": true,
            "timeOut": "10000"
        }
        toastr[errorMessageType](errorMessage);
    } else {
        swal({
            title: "Hata!",
            text: errorMessage,
            icon: errorMessageType
        });
    }
}
//$('.filter').click(function(){
//getErrorMessage("success","Başarılı Bilgilendirme Mesajı");
//getErrorMessage("info","Bilgi Bilgilendirme Mesajı");
//getErrorMessage("warning","Uyarı Bilgilendirme Mesajı");
//getErrorMessage("error","Hata Bilgilendirme Mesajı");
//});

function CreateData(frm) {
    $('#' + frm + "_Modal").modal("toggle");
    $("#" + frm + "_ClientId").val("0");
    $("#btn" + frm + "Save").html("KAYDET");
}

function RefreshTable(tableName, jsonData) {

    var table = $('#' + tableName).DataTable();
    table.rows().remove().draw();

    var data = new Array();

    if (jsonData != null) {

        $.each(jsonData, function (index) {
            data.push(jsonData[index]);
            table.row.add(jsonData[index]).draw();
        });
    }
}

function formatdate(date) {
    if (date != null) {
        var dateString = date.substr(6);
        var currentTime = new Date(parseInt(dateString));
        var month = currentTime.getMonth() + 1;
        if (month < 10) {
            month = "0" + month;
        }
        var day = currentTime.getDate();
        if (day < 10) {
            day = "0" + day;
        }
        var year = currentTime.getFullYear();
        var date = day + "-" + month + "-" + year;
        return date;
    }
    else {
        return "-";
    }
}