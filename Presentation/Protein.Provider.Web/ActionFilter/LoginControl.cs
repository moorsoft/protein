﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Protein.Provider.Web.ActionFilter
{
    public class LoginControl : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Session["ProviderId"] == null)
            {
                filterContext.HttpContext.Response.Redirect("/Login/Index?returnUrl=" + filterContext.HttpContext.Request.RawUrl);
            }

            base.OnActionExecuting(filterContext);
        }
    }
}