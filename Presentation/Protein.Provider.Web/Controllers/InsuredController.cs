﻿using Protein.Common.Dto;
using Protein.Common.Extensions;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Provider.Web.ActionFilter;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Provider.Web.Controllers
{
    [LoginControl]
    public class InsuredController : Controller
    {
        // GET: Insured
        public ActionResult Index()
        {
            ViewBag.ProviderId = Session["ProviderId"];
            ViewBag.ProviderName = Session["ProviderName"];

            var v_company = new GenericRepository<V_Company>().FindBy(orderby: "COMPANY_ID");
            ViewBag.v_company = v_company;

            //ViewBag.IndividualTypeCodeList = LookupHelper.GetLookupDataCode(LookupTypes.Individual);
            ViewBag.GenderList = LookupHelper.GetLookupDataText(LookupTypes.Gender);
            return View();
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetInsuredList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];


            ViewResultDto<List<V_Insured>> insuredList = new ViewResultDto<List<V_Insured>>();
            insuredList.Data = new List<V_Insured>();
            if (isFilter == "1")
            {
                String whereConditition = $" (POLICY_STATUS='{((int)PolicyStatus.TANZIMLI).ToString()}' OR POLICY_STATUS='{((int)PolicyStatus.SURESI_DOLMUS).ToString()}') AND";
                whereConditition += form["COMPANY_ID"].IsInt64() ? $" COMPANY_ID = {long.Parse(form["COMPANY_ID"])} AND" : "";
                whereConditition += form["IDENTITY_NO"].IsInt64() ? $" IDENTITY_NO = {form["IDENTITY_NO"]} AND" : "";
                whereConditition += form["TAX_NUMBER"].IsInt64() ? $" TAX_NUMBER = {form["TAX_NUMBER"]} AND" : "";
                whereConditition += !form["Pasaport_NO"].IsNull() ? $" PASSPORT_NO LIKE '%{form["Pasaport_NO"]}%' AND" :""; 
                whereConditition += !form["FIRST_NAME"].IsNull() ? $" FIRST_NAME LIKE '%{form["FIRST_NAME"]}%' AND" : "";
                whereConditition += !form["LAST_NAME"].IsNull() ? $" LAST_NAME LIKE '%{form["LAST_NAME"]}%' AND" : "";
                whereConditition += form["BIRTHDATE"].IsDateTime() ? $" BIRTHDATE = TO_DATE('{DateTime.Parse(form["BIRTHDATE"])}','DD/MM/YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["POLICY_NUMBER"].IsInt64() ? $" POLICY_NUMBER={form["POLICY_NUMBER"]} AND" : "";
                whereConditition += $" POLICY_START_DATE < TO_DATE('{DateTime.Parse(DateTime.Now.ToString())}','DD/MM/YYYY HH24:MI:SS') AND";
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();
                insuredList = new GenericRepository<V_Insured>().FindByPaged(
                                  conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                  orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},INSURED_ID DESC" : "INSURED_ID DESC",
                                  pageNumber: start,
                                  rowsPerPage: length, fetchDeletedRows: true);
               stopwatch.Stop();
                var a = stopwatch.Elapsed;
                
            }
            return Json(new { data = insuredList.Data, draw = Request["draw"], recordsTotal = insuredList.TotalItemsCount, recordsFiltered = insuredList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public JsonResult GetExclusion(Int64 insuredId = 0)
        {
            var result = "[]";
            try
            {
                List<V_InsuredExclusion> exclusionList = new GenericRepository<V_InsuredExclusion>().FindBy($"INSURED_ID={insuredId}", orderby: "");
                if (exclusionList!=null)
                {
                    result = exclusionList.ToJSON();
                }
            }
            catch (Exception)
            {
                result = "[]";
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }
    }
}