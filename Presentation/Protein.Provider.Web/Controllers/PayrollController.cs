﻿using Protein.Common.Dto;
using Protein.Common.Extensions;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Provider.Web.ActionFilter;
using Protein.Web.Models.ExportModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Provider.Web.Controllers
{
    [LoginControl]
    public class PayrollController : Controller
    {
        // GET: Payroll
        [LoginControl]
        public ActionResult Index()
        {
            ViewBag.ProviderId = Session["ProviderId"];
            ViewBag.ProviderName = Session["ProviderName"];

            ExportVM vm = new ExportVM();
            vm.ViewName = "V_FORM_PAYROLL";
            vm.SetExportColumns();
            vm.ExportType = ExportType.Excel;

            var v_company = new GenericRepository<V_Company>().FindBy(orderby: "COMPANY_ID");
            ViewBag.v_company = v_company;

            ViewBag.PayrollStatusList = LookupHelper.GetLookupData(LookupTypes.PayrollStatus);
            ViewBag.ClaimStatusList = LookupHelper.GetLookupData(LookupTypes.ClaimStatus);
            ViewBag.PayrollCategoryTypeList = LookupHelper.GetLookupData(LookupTypes.PayrollCategory);

            return View(vm);
        }

        [HttpPost]
        public ActionResult GetPaymentList(FormCollection form)
        {
            #region Export
            ExportVM vm = new ExportVM();
            vm.ViewName = "V_FORM_PAYROLL";
            vm.WhereCondition = "";
            vm.SetExportColumns();
            #endregion

            ViewResultDto<List<V_FormPayroll>> PaymentList = new ViewResultDto<List<V_FormPayroll>>();
            PaymentList.Data = new List<V_FormPayroll>();
            long providerId = long.Parse(Session["ProviderId"].ToString());

            if (form["isFilter"] == "1" && providerId>0)
            {
                int start = Convert.ToInt32(Request["start"]);
                int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
                string searchValue = Request["search[value]"];
                string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
                string sortDirection = Request["order[0][dir]"];

                long companyId = 0;
                long payrollId = 0;
                long claimId = 0;
                string paymentStartDate = Request["paymentStartDate"];
                string paymentEndDate = Request["paymentEndDate"];

                string whereConditition = $"PROVIDER_ID = {providerId} AND PAYROLL_TYPE='{((int)PayrollType.KURUM).ToString()}'";
                if (!String.IsNullOrEmpty(form["COMPANY_ID"]))
                {
                    companyId = Convert.ToInt32(form["COMPANY_ID"]);
                    whereConditition += $" AND COMPANY_ID = {companyId} ";
                }
                if (!String.IsNullOrEmpty(form["PROVIDER_Group"]))
                {
                    whereConditition += $" AND PROVIDER_GROUP_ID = {form["PROVIDER_Group"]} ";
                }
                if (!String.IsNullOrEmpty(form["payrollNo"]))
                {
                    payrollId = Convert.ToInt32(form["payrollNo"]);
                    whereConditition += $" AND (PAYROLL_ID = {payrollId} OR OLD_PAYROLL_ID={payrollId}) ";
                }
                if (!String.IsNullOrEmpty(form["extpayrollNo"]))
                {
                    whereConditition += $" AND EXT_PROVIDER_NO = '{form["extpayrollNo"]}' ";
                }
                if (!String.IsNullOrEmpty(form["providerId"]))
                {
                    providerId = Convert.ToInt32(form["providerId"]);
                    whereConditition += $" AND PROVIDER_ID = {providerId} ";
                }
                if (!String.IsNullOrEmpty(form["payrollStatus"]))
                {
                    whereConditition += $" AND STATUS ={form["payrollStatus"]} ";
                }
                if (!String.IsNullOrEmpty(form["paymentStartDate"]))
                {
                    whereConditition += $" AND PAYMENT_DATE >= TO_DATE('{DateTime.Parse(form["paymentStartDate"])}', 'DD.MM.YYYY HH24:MI:SS') ";
                }
                if (!String.IsNullOrEmpty(form["paymentEndDate"]))
                {
                    whereConditition += $" AND PAYMENT_DATE <= TO_DATE('{DateTime.Parse(form["paymentEndDate"])}', 'DD.MM.YYYY HH24:MI:SS') ";
                }

                PaymentList = new GenericRepository<V_FormPayroll>().FindByPaged(conditions: whereConditition,
                                                                             orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection}" : "CLAIM_ID",
                                                                             fetchDeletedRows: true,
                                                                             fetchHistoricRows: true,
                                                                             pageNumber: start,
                                                                             rowsPerPage: length);
                if (PaymentList.Data!=null )
                {
                    foreach (var item in PaymentList.Data)
                    {
                        if (item.PAYMENT_DATE.IsDateTime())
                        {
                            item.PAYMENT_DATE = DateTime.Parse(item.PAYMENT_DATE).ToShortDateString();
                        }
                        if (item.PAYROLL_DATE.IsDateTime())
                        {
                            item.PAYROLL_DATE = DateTime.Parse(item.PAYROLL_DATE).ToShortDateString();
                        }
                        if (item.PAYROLL_DUE_DATE.IsDateTime())
                        {
                            item.PAYROLL_DUE_DATE = DateTime.Parse(item.PAYROLL_DUE_DATE).ToShortDateString();
                        }
                    }
                }
                
                vm.WhereCondition = whereConditition;
            }
            return Json(new { data = (PaymentList.Data == null ? new List<V_FormPayroll>() : PaymentList.Data), draw = Request["draw"], recordsTotal = PaymentList.TotalItemsCount, recordsFiltered = PaymentList.TotalItemsCount, whereConditition = vm.WhereCondition}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PayrollExit(Int64 claimId)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                if (claimId < 1)
                    throw new Exception("Hasar Bilgisi Bulunamadı");

                var claim = new GenericRepository<Claim>().FindById(claimId);

                if (claim == null)
                    throw new Exception("Hasar Bilgisi Bulunamadı");

                if (claim.Status == ((int)ClaimStatus.ODENECEK).ToString() || claim.Status == ((int)ClaimStatus.ODENDI).ToString())
                    throw new Exception("ODENDI veya ODENECEK durumundaki Hasar Zarftan Çıkarılamaz!");

                var payrollId = claim.PayrollId;
                claim.PayrollId = null;
                SpResponse spResponseClaim = new GenericRepository<Claim>().UpdateForAll(claim);
                if (spResponseClaim.Code != "100")
                    throw new Exception("Hasarı Zarftan Çıkarma İşlemi Gerçekleştirilemedi");

                var BillList = new GenericRepository<V_Claim>().FindBy($"PAYROLL_ID={payrollId}", orderby: "BILL_NO");
                if (BillList != null)
                {
                    result.ResultMessage = "Hasar Zarftan Başarıyla Çıkartıldı.";
                    result.ResultCode = "100";
                    result.Data = new JSONResult
                    {
                        jsonData = BillList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }
    }
}