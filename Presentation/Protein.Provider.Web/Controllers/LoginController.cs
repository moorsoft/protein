﻿using Dapper;
using Protein.Common.Dto;
using Protein.Common.Entities;
using Protein.Common.Extensions;
using Protein.Common.Helpers;
using Protein.Common.Messaging;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Data.Workers;
using Protein.Provider.Web.ActionFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Provider.Web.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index(string returnUrl = "")
        {
            try
            {
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;
                ViewBag.inputEmail = string.Empty;
                ViewBag.inputPassword = string.Empty;
                ViewBag.ReturnUrl = !string.IsNullOrEmpty(returnUrl.Trim()) ? returnUrl.Trim() : string.Empty;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = "swal({title: 'Uyarı',text: '" + ex.Message + "'',icon:'warning'});";
            }
            return View();
        }

        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            string returnUrl = "";
            try
            {
                var inputEmail = form["inputEmail"];
                var inputPassword = form["inputPassword"].ToMD5().ToUpper();
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;

                string whereCondition = $"USERNAME = :username AND PASSWORD = :password";

                User user = new GenericRepository<User>().FindBy(whereCondition,
                                   parameters: new { username = new DbString { Value = inputEmail, Length = 45 }, password = new DbString { Value = inputPassword, Length = 45 } }).FirstOrDefault();
                if (user == null)
                {
                    TempData["Alert"] = "swal({title: 'Uyarı',text: 'Hatalı Kullanıcı Adı/Şifre Girdiniz! Lütfen Tekrar Deneyin yada Şifre Unuttum ile şifrenizi yenileyiniz...',icon:'warning'}); ";
                    return View("Index");
                }

                whereCondition = $"USER_ID = :userId";
                V_ProviderUser providerUser = new GenericRepository<V_ProviderUser>().FindBy(whereCondition, orderby: "USER_ID", parameters: new { userId = user.Id }).FirstOrDefault();
                if (providerUser == null)
                {
                    TempData["Alert"] = "swal({title: 'Uyarı',text: 'Giriş Başarısız! Tekrar Deneyiniz...',icon:'warning'});";
                    return View("Index");
                }

                Session["ProviderId"] = providerUser.PROVIDER_ID;
                Session["ProviderName"] = providerUser.PROVIDER_NAME;
                Session["ProviderType"] = providerUser.PROVIDER_TYPE;
                Session.Timeout = 60;

                var token = new TokenWorker().SetToken(new UserWorker().GetUserID());

                return Redirect("~/Dashboard/Index");
            }
            catch (Exception ex)
            {
                TempData["Alert"] = "swal({title: 'Uyarı',text: '" + ex.Message + "'',icon:'warning'});";
            }
            return !string.IsNullOrEmpty(returnUrl) ? Redirect(returnUrl) : Redirect("Index");
        }

        [HttpGet]
        public JsonResult RefreshPasword(Int64 providerId = 0)
        {
            var result = new AjaxResultDto<JSONResult>();

            var json = "";
            try
            {
                V_Provider provider = new GenericRepository<V_Provider>().FindBy($"PROVIDER_ID ={providerId}", orderby: "").FirstOrDefault();
                if (provider == null)
                {
                    result.ResultCode = "999";
                    result.ResultMessage = "Kurum Koduna Ait Kurum Bilgisi Bulunamadı. Kurum Kodunu Kontrol Ediniz.";

                    return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
                }

                V_Staff v_Staff = new GenericRepository<V_Staff>().FindBy($"PROVIDER_ID={provider.PROVIDER_ID} AND STAFF_TYPE='{((int)StaffType.YETKILI).ToString()}' AND EMAIL_ADDRESS IS NOT NULL AND IS_GROUP_ADMIN='1'", orderby: "").FirstOrDefault();
                if (v_Staff == null)
                {
                    result.ResultCode = "999";
                    result.ResultMessage = "Kurum Yetkili Bilgisi Bulunamadı.";

                    MailSender msender1 = new MailSender();
                    msender1.SendMessage(
                        new EmailArgs
                        {
                            TO = "basak@imecedestek.com",
                            IsHtml = true,
                            Subject = "Kurum Yetkili Bilgisi Eksik",
                            Content = $"<b>{provider.PROVIDER_ID}</b> Nolu <b>{provider.PROVIDER_NAME}</b> Kurumunun Aktif Yetkili Bilgisi Sistemde Bulunmamaktadır. Kurumun Şifre Yenilemesi için Yetkili Bilgisinin Siteme Eklenmesi Gerekmektedir."
                        });

                    return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
                }

                string userName = "";
                string clearPassword = UserHelper.GeneratePassword(provider.PROVIDER_NAME, providerId.ToString());

                V_ProviderUser v_ProviderUser = new GenericRepository<V_ProviderUser>().FindBy($"PROVIDER_ID={provider.PROVIDER_ID}", orderby: "").FirstOrDefault();
                if (v_ProviderUser == null)
                {
                    var isCreate = true;
                    userName = UserHelper.GenerateUserName(LookupHelper.GetLookupTextByOrdinal(LookupTypes.Provider, provider.PROVIDER_TYPE), providerId.ToString());
                    var user = new User
                    {
                        Username = userName,
                        Password = CryptoHelper.GetMD5Hash(clearPassword),
                        Email = v_Staff.EMAIL_ADDRESS,
                        IsRememberMe = "1"
                    };
                    var spResponseUser = new UserRepository().Insert(user);
                    if (spResponseUser.Code == "100")
                    {
                        var userId = spResponseUser.PkId;
                        var providerUser = new ProviderUser
                        {
                            ProviderId = providerId,
                            UserId = userId
                        };
                        var spResponseProviderUser = new ProviderUserRepository().Insert(providerUser);
                        if (spResponseProviderUser.Code != "100")
                            isCreate = false;
                    }
                    else
                        isCreate = false;

                    if (!isCreate)
                    {
                        result.ResultCode = "999";
                        result.ResultMessage = "Kurum Giriş Bilgileri Bulunamadı. Lütfen Tekrar Deneyiniz...";

                        return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    userName = v_ProviderUser.USERNAME;

                    User user = new User
                    {
                        Id = (long)v_ProviderUser.USER_ID,
                        Email = v_Staff.EMAIL_ADDRESS,
                        Username = v_ProviderUser.USERNAME,
                        Password = CryptoHelper.GetMD5Hash(clearPassword)
                    };
                    var spResponseUser = new UserRepository().Insert(user);
                }

                MailSender msender = new MailSender();
                msender.SendMessage(
                    new EmailArgs
                    {
                        TO = v_Staff.EMAIL_ADDRESS,
                        IsHtml = true,
                        Subject = "İmece Destek Danışmanlık Hizmetleri A.Ş. E-Protein Kullanıcı Adı ve Şifre Bildirimi",
                        Content = $"<b>Kurum Adı:</b> {provider.PROVIDER_NAME} " +
                        $"<br><br>Sayın Yetkili," +
                        $"<br><br>Siz değerli anlaşmalı kurumlarımızın online sistem üzerinden Halk Sigorta, Türk Nippon Sigorta ve Doğa Sigorta Şirketlerine ait provizyon, icmal, ödeme takibi işlemlerini yapabilmenize olanak sağlayan E-Protein uygulamasına giriş yapabilmeniz için oluşturulan kullanıcı adı ve şifre aşağıda belirtilmiştir." +
                        $"<br><br>E-Protein uygulamasına; http://protein.imecedestek.com:4041 adresinden erişerek kullanıcı adı / şifreniz ile sisteme giriş yaptıktan sonra E-Protein kullanım kılavuzuna ulaşabilirsiniz." +
                        $"<br><br><br><b>Kurum Kodu:</b> {provider.PROVIDER_ID}" +
                        $"<br><b>Kullanıcı Adı:</b> {userName}" +
                        $"<br><b>Şifre:</b> {clearPassword}" +
                        $"<br><br>Kullanıcı bilgilerinizi unutmanız durumunda giriş ekranında bulunan “Şifremi Unuttum” sekmesini kullanarak sistemimizde kayıtlı olan e-mail adresinize yeni şifre gönderimi yapılmaktadır ." +
                        $"<br><br>İşlemlerinizi Online sistem üzerinden yaptığınız için teşekkür eder, iş birliğimizin artarak devam etmesini dileriz."
                    });

                result.ResultCode = "100";
                result.ResultMessage = $"Kullanıcı Adı ve Şifreniz {v_Staff.EMAIL_ADDRESS} Mail Adresine Gönderildi.";

                return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }
            return Json(json.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [LoginControl]
        public ActionResult Logout()
        {
            try
            {
                #region Token proc
                TokenWorker token = new TokenWorker();
                string _token = token.GetValidToken(new UserWorker().GetUserID());
                token.FindAndDestroyIncludeToken(new UserWorker().GetUserID(), _token);
                #endregion
                TempData["Alert"] = TempData["Alert"] ?? string.Empty;
                Session["ProviderId"] = null;
                Session["ProviderName"] = null;
            }
            catch (Exception ex)
            {
                TempData["Alert"] = "swal({title: 'Uyarı',text: '" + ex.Message + "'',icon:'warning'});";
            }
            return View("Index");
        }
    }
}