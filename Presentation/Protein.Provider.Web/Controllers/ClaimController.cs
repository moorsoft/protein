﻿using Newtonsoft.Json;
using Protein.Business.Abstract.Print;
using Protein.Business.Print;
using Protein.Business.Print.AcquittanceForm;
using Protein.Business.Print.BillAprovalForm;
using Protein.Business.Print.ClaimMissing;
using Protein.Business.Print.ClaimProviderReturn;
using Protein.Business.Print.ClaimRejectForm;
using Protein.Business.Print.ProvisionApproval;
using Protein.Business.Print.ProvisionMissingDocForm;
using Protein.Business.Print.ProvisionReject;
using Protein.Common.Dto;
using Protein.Common.Extensions;
using Protein.Common.Helpers;
using Protein.Data.ExternalServices.Common.Lib;
using Protein.Data.ExternalServices.InsuranceCompanies;
using Protein.Data.ExternalServices.Protein;
using Protein.Data.ExternalServices.RxMedia;
using Protein.Data.ExternalServices.RxMedia.Result;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Provider.Web.ActionFilter;
using Protein.Web.Models.ExportModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;
using static Protein.Data.ExternalServices.Common.Lib.NN.NNInputOutputTypes;

namespace Protein.Provider.Web.Controllers
{
    [LoginControl]
    public class ClaimController : Controller
    {
        // GET: Claim
        public ActionResult Index()
        {
            ViewBag.ProviderId = Session["ProviderId"];
            ViewBag.ProviderName = Session["ProviderName"];


            ExportVM vm = new ExportVM();
            vm.ViewName = "V_PROVIDER_CLAIM";
            vm.SetExportColumns();
            vm.ExportType = ExportType.Excel;

            var v_company = new GenericRepository<V_Company>().FindBy(orderby: "COMPANY_ID");
            ViewBag.v_company = v_company;

            ViewBag.PayrollCategoryTypeList = LookupHelper.GetLookupData(LookupTypes.PayrollCategory);
            ViewBag.ClaimStatusList = LookupHelper.GetLookupData(LookupTypes.ClaimStatus);
            ViewBag.ClaimTypeList = LookupHelper.GetLookupData(LookupTypes.Claim);
            ViewBag.ClaimSourceList = LookupHelper.GetLookupData(LookupTypes.ClaimSource);
            ViewBag.PaymentTypeList = LookupHelper.GetLookupData(LookupTypes.Payment);
            ViewBag.EventTypeList = LookupHelper.GetLookupData(LookupTypes.Event);
            ViewBag.PayrollCategoryList = LookupHelper.GetLookupData(LookupTypes.PayrollCategory);
            ViewBag.PayrollTypeList = LookupHelper.GetLookupData(LookupTypes.Payroll);
            ViewBag.IcdTypeList = LookupHelper.GetLookupData(LookupTypes.ICD);
            ViewBag.ClaimReasonList = ReasonHelper.GetReasonData("ClaimStatus", ((int)ClaimStatus.IPTAL).ToString());

            return View(vm);
        }

        public JsonResult ClaimCancellation(long claimId, long reasonId)
        {
            var result = new AjaxResultDto<ClaimSaveStep2Result>();

            if (claimId > 0)
            {
                Claim claim = new GenericRepository<Claim>().FindById(claimId);
                if (claim != null)
                {
                    claim.Status = ((int)ClaimStatus.IPTAL).ToString();
                    claim.ReasonId = reasonId;
                    new GenericRepository<Claim>().Update(claim);
                    result.ResultCode = "100";
                }
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);

        }

        public JsonResult DeleteMedicine(Int64 claimId, Int64 claimMedicineId)
        {
            var result = new AjaxResultDto<ClaimSaveStep2Result>();
            ClaimSaveStep2Result step2Result = new ClaimSaveStep2Result();
            try
            {
                long medicineProcessListId = 0;
                var medicineProcessList = ProcessListHelper.GetProcessList("İLAÇ");
                if (medicineProcessList != null)
                {
                    medicineProcessListId = medicineProcessList.Id;
                }
                else
                {
                    throw new Exception("İlaç listesi bulunamadı!");
                }

                var claimProcess = new ClaimProcess
                {
                    Id = claimMedicineId,
                    Status = Convert.ToString((int)Status.SILINDI)
                };
                var spResponseClaimProcess = new ClaimProcessRepository().Insert(claimProcess);
                if (spResponseClaimProcess.Code == "100")
                {
                    result.ResultCode = spResponseClaimProcess.Code;
                    result.ResultMessage = spResponseClaimProcess.Message;
                }
                else
                {
                    throw new Exception(spResponseClaimProcess.Message);
                }

                List<V_ClaimProcess> dbProcesses = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id AND PROCESS_LIST_ID =: processListId", orderby: "CLAIM_ID", parameters: new { id = claimId, processListId = medicineProcessListId });
                if (dbProcesses != null)
                {
                    List<dynamic> processesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in dbProcesses)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.CLAIM_MEDICINE_ID = Convert.ToString(item.CLAIM_PROCESS_ID);
                        listItem.MEDICINE_ID = Convert.ToString(item.PROCESS_ID);
                        listItem.MEDICINE_CODE = item.PROCESS_CODE;
                        listItem.MEDICINE_NAME = item.PROCESS_NAME;
                        listItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(item.UNIT_AMOUNT).ToString("0.00");
                        listItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(item.AGREED_AMOUNT).ToString("0.00");
                        listItem.PAID = string.IsNullOrEmpty(Convert.ToString(item.PAID)) ? "" : Convert.ToDecimal(item.PAID).ToString("0.00");
                        listItem.RESULT = item.RESULT;
                        if (!string.IsNullOrEmpty(item.PARTLY_REJECT_DESC))
                        {
                            listItem.RESULT += " - " + item.PARTLY_REJECT_DESC;
                        }

                        processesList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimSaveStep2Result
                    {
                        jsonData = processesList.ToJSON(false)
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public JsonResult Medicine(Int64 claimId, Int64 insuredId, Int64 packageId, string barcode)
        {
            var result = new AjaxResultDto<ClaimSaveStep2Result>();
            try
            {
                long medicineProcessListId = 0;
                var medicineProcessList = ProcessListHelper.GetProcessList("İLAÇ");
                if (medicineProcessList != null)
                {
                    medicineProcessListId = medicineProcessList.Id;
                }
                else
                {
                    throw new Exception("İlaç listesi bulunamadı!");
                }

                Process medicine;
                Helper hlp = new Helper();
                MedicineInfo mInfo = hlp.GetMedicineInfoByBarcode(barcode);
                if (mInfo.Success)
                {
                    GenericRepository<Process> medicineRepository = new GenericRepository<Process>();
                    List<Process> medicineList = medicineRepository.FindBy("CODE='" + barcode + "'");
                    if (medicineList == null || medicineList.Count == 0)
                    {
                        // Insert new medicine
                        medicine = new Process()
                        {
                            Id = 0,
                            Code = barcode,
                            ProcessListId = medicineProcessListId,
                            Name = mInfo.Name,
                            Amount = mInfo.TotalPrice
                        };
                        SpResponse spResponse = medicineRepository.Update(medicine);
                        if (spResponse.Code != "100")
                        {
                            throw new Exception("İlaç bilgileri güncellenirken hata oluştu!");
                        }
                        medicine.Id = spResponse.PkId;
                    }
                    else if (medicineList.Count == 1)
                    {
                        medicine = medicineList[0];
                        // Check medicine needs to be updated
                        if (!medicine.Name.Equals(mInfo.Name) || medicine.Amount != mInfo.TotalPrice)
                        {
                            medicine.Name = mInfo.Name;
                            medicine.Amount = mInfo.TotalPrice;
                            SpResponse spResponse = medicineRepository.Update(medicine);
                            if (spResponse.Code != "100")
                            {
                                throw new Exception("İlaç bilgileri güncellenirken hata oluştu!");
                            }
                            result.ResultCode = spResponse.Code;
                            result.ResultMessage = spResponse.Message;
                        }
                    }
                    else
                    {
                        throw new Exception("Aynı barkod ile birden fazla ilaç bulundu! (" + mInfo.Message + ")");
                    }

                    if (medicine != null)
                    {
                        long? coverageId = null;
                        List<V_PackageCoverageList> insuredCoverageResult = new GenericRepository<V_PackageCoverageList>().FindBy(
                            $"PACKAGE_ID={packageId} AND COVERAGE_TYPE ={((int)CoverageType.AYAKTA).ToString()} AND COVERAGE_NAME LIKE '%İLAÇ%'",
                            orderby: "PACKAGE_ID",
                            fetchHistoricRows: true,
                            fetchDeletedRows: true);
                        if (insuredCoverageResult != null)
                        {
                            if (insuredCoverageResult != null && insuredCoverageResult.Count == 1)
                            {
                                string strCoverageId = Convert.ToString(insuredCoverageResult[0].COVERAGE_ID);
                                if (!string.IsNullOrEmpty(strCoverageId))
                                {
                                    coverageId = long.Parse(strCoverageId);
                                }
                            }
                        }

                        var claimMedicine = new ClaimProcess
                        {
                            Id = 0,
                            ClaimId = claimId,
                            ProcessId = medicine.Id,
                            RequestedAmount = medicine.Amount,
                            UnitAmount = medicine.Amount,
                            AgreedAmount = medicine.Amount
                        };
                        if (coverageId != null && coverageId > 0)
                        {
                            claimMedicine.CoverageId = coverageId;
                        }
                        var spResponseClaimMedicine = new ClaimProcessRepository().Insert(claimMedicine);
                        //if (spResponseClaimMedicine.Code == "100")
                        //{
                        result.ResultCode = spResponseClaimMedicine.Code;
                        result.ResultMessage = spResponseClaimMedicine.Message;
                        //}
                        //else
                        //{
                        //    throw new Exception(spResponseClaimMedicine.Message);
                        //}
                    }
                    else
                    {
                        throw new Exception(mInfo.Message);
                    }
                }
                else
                {
                    throw new Exception("İlaç bulunamadı! (" + mInfo.Message + ")");
                }

                List<V_ClaimProcess> dbMedicines = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id AND PROCESS_LIST_ID =: processListId", orderby: "CLAIM_ID", parameters: new { id = claimId, processListId = medicineProcessListId });
                if (dbMedicines != null)
                {
                    List<dynamic> medicinesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in dbMedicines)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.CLAIM_MEDICINE_ID = Convert.ToString(item.CLAIM_PROCESS_ID);
                        listItem.MEDICINE_ID = Convert.ToString(item.PROCESS_ID);
                        listItem.MEDICINE_CODE = item.PROCESS_CODE;
                        listItem.MEDICINE_NAME = item.PROCESS_NAME;
                        listItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(item.UNIT_AMOUNT).ToString("0.00");
                        listItem.REQUESTED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.REQUESTED_AMOUNT)) ? "" : Convert.ToDecimal(item.REQUESTED_AMOUNT).ToString("0.00");
                        listItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(item.AGREED_AMOUNT).ToString("0.00");
                        listItem.CLAIM_MEDICINE_COVERAGE_ID = Convert.ToString(item.COVERAGE_ID);
                        listItem.COVERAGE_NAME = item.COVERAGE_NAME;
                        listItem.PAID = string.IsNullOrEmpty(Convert.ToString(item.PAID)) ? "" : Convert.ToDecimal(item.PAID).ToString("0.00");
                        listItem.RESULT = item.RESULT;
                        if (!string.IsNullOrEmpty(item.PARTLY_REJECT_DESC))
                        {
                            listItem.RESULT += " - " + item.PARTLY_REJECT_DESC;
                        }

                        medicinesList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimSaveStep2Result
                    {
                        jsonData = medicinesList.ToJSON(false)
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Medicines(Int64 claimId)
        {
            var result = new AjaxResultDto<ClaimProcessListResult>();
            try
            {
                long medicineProcessListId = 0;
                var medicineProcessList = ProcessListHelper.GetProcessList("İLAÇ");
                if (medicineProcessList != null)
                {
                    medicineProcessListId = medicineProcessList.Id;
                }
                else
                {
                    throw new Exception("İlaç listesi bulunamadı!");
                }

                List<V_ClaimProcess> dbMedicines = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id AND PROCESS_LIST_ID =: processListId", orderby: "CLAIM_ID", parameters: new { id = claimId, processListId = medicineProcessListId });
                if (dbMedicines != null)
                {
                    List<dynamic> medicinesList = new List<dynamic>();

                    int i = 1;

                    foreach (var item in dbMedicines)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();

                        listItem.id = i;
                        listItem.ClientId = i;
                        listItem.STATUS = item.STATUS;

                        listItem.CLAIM_MEDICINE_ID = Convert.ToString(item.CLAIM_PROCESS_ID);
                        listItem.MEDICINE_ID = Convert.ToString(item.PROCESS_ID);
                        listItem.MEDICINE_CODE = item.PROCESS_CODE;
                        listItem.MEDICINE_NAME = item.PROCESS_NAME;
                        listItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(item.UNIT_AMOUNT).ToString("0.00");
                        listItem.REQUESTED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.REQUESTED_AMOUNT)) ? "" : Convert.ToDecimal(item.REQUESTED_AMOUNT).ToString("0.00");
                        listItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(item.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(item.AGREED_AMOUNT).ToString("0.00");
                        listItem.CLAIM_MEDICINE_COVERAGE_ID = Convert.ToString(item.COVERAGE_ID);
                        listItem.COVERAGE_NAME = item.COVERAGE_NAME;
                        listItem.PAID = string.IsNullOrEmpty(Convert.ToString(item.PAID)) ? "" : Convert.ToDecimal(item.PAID).ToString("0.00");
                        listItem.RESULT = item.RESULT;
                        if (!string.IsNullOrEmpty(item.PARTLY_REJECT_DESC))
                        {
                            listItem.RESULT += " - " + item.PARTLY_REJECT_DESC;
                        }

                        medicinesList.Add(listItem);

                        i++;
                    }

                    result.Data = new ClaimProcessListResult
                    {
                        jsonData = medicinesList.ToJSON(false)
                    };
                    result.ResultCode = "100";
                    result.ResultMessage = "SUCCESS";
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult CheckBill(string ClaimIdList)
        {
            var result = new AjaxResultDto<ClaimProcessListResult>();
            try
            {
                List<Claim> claimList = new GenericRepository<Claim>().FindBy($"ID IN ({ClaimIdList})");
                foreach (var item in claimList)
                {
                    foreach (var claim in claimList)
                    {
                        if (claim.Type != item.Type)
                            throw new Exception("İcmale Alınacak Provizyonların Tipleri Aynı Olmalıdır.");
                        if (claim.PaymentMethod != item.PaymentMethod)
                            throw new Exception("İcmale Alınacak Provizyonların Ödeme Yöntemi Aynı Olmalıdır.");
                        if (claim.PaymentMethod != item.PaymentMethod)
                            throw new Exception("İcmale Alınacak Provizyonların Ödeme Yöntemi Aynı Olmalıdır.");
                        if (claim.CompanyId != item.CompanyId)
                            throw new Exception("İcmale Alınacak Provizyonlar Aynı Sigorta Şirketine Ait Olmalıdır.");
                    }
                }
                result.ResultCode = "100";
                result.ResultMessage = " ";
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        [HttpGet]
        [LoginControl]
        public ActionResult Create(Int64 claimId = 0, Int64 insuredId = 0, Int64 policyId = 0, Int64 packageId = 0, Int64 contactId = 0)
        {
            ViewBag.ProviderId = Session["ProviderId"];
            ViewBag.ProviderName = Session["ProviderName"];
            ViewBag.ProviderType = Session["ProviderType"];
            ViewBag.ClaimReasonList = ReasonHelper.GetReasonData("ClaimStatus", ((int)ClaimStatus.IPTAL).ToString());

            // Edit View
            if (claimId > 0)
            {
                var vClaim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID={claimId}", orderby: "CLAIM_ID DESC").FirstOrDefault();
                if (vClaim != null)
                {
                    ViewBag.CompanyId = vClaim.COMPANY_ID;
                    ViewBag.InsuredId = vClaim.INSURED_ID;
                    ViewBag.ContactId = vClaim.CONTACT_ID;
                    ViewBag.PolicyId = vClaim.POLICY_ID;
                    ViewBag.PackageId = vClaim.PACKAGE_ID;
                    ViewBag.ClaimPaymentMethod = vClaim.PAYMENT_METHOD;
                    ViewBag.ClaimType = vClaim.CLAIM_TYPE;
                    ViewBag.ClaimTypeText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Claim, vClaim.CLAIM_TYPE);

                    ViewBag.InsuredName = vClaim.FIRST_NAME + " " + vClaim.LAST_NAME;
                    ViewBag.NoticeDate = ((DateTime)vClaim.NOTICE_DATE).ToString("yyyy-MM-ddTHH:mm");
                    ViewBag.ClaimDate = ((DateTime)vClaim.CLAIM_DATE).ToString("yyyy-MM-ddTHH:mm");
                    ViewBag.ClaimStatus = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, vClaim.STATUS);
                    ViewBag.ClaimStatusOrdinal = vClaim.STATUS;
                    ViewBag.MedulaNo = vClaim.MEDULA_NO;
                    ViewBag.CompanyClaimId = vClaim.COMPANY_CLAIM_ID;
                    ViewBag.CompanyClaimNo = vClaim.COMPANY_CLAIM_NO;
                    ViewBag.Reason = vClaim.REASON_DESCRIPTION; // ReasonHelper.GetReasonbyId(long.Parse(vClaim.REASON_ID)).Description;
                    ViewBag.ReasonId = vClaim.REASON_ID; // ReasonHelper.GetReasonbyId(long.Parse(vClaim.REASON_ID)).Description;
                    ViewBag.EventDate = string.IsNullOrEmpty(Convert.ToString(vClaim.EVENT_DATE)) ? "" : ((DateTime)vClaim.EVENT_DATE).ToString("yyyy-MM-ddTHH:mm");
                    ViewBag.EventType = vClaim.EVENT_TYPE;
                    ViewBag.EventDescription = vClaim.EVENT_DESCRIPTION;
                    ViewBag.BillId = vClaim.CLAIM_BILL_ID;
                    ViewBag.BillNo = vClaim.BILL_NO;
                    ViewBag.BillDate = string.IsNullOrEmpty(Convert.ToString(vClaim.BILL_DATE)) ? "" : ((DateTime)vClaim.BILL_DATE).ToString("yyyy-MM-ddTHH:mm");
                    ViewBag.BillPaymentType = vClaim.BILL_PAYMENT_TYPE;
                    ViewBag.BillCurrencyType = vClaim.BILL_EXCHANGE_CURRENCY_TYPE;
                    ViewBag.BillStatus = vClaim.BILL_STATUS;
                    ViewBag.PayrollId = vClaim.PAYROLL_ID;

                    ViewBag.InsuredIdentityNo = vClaim.IDENTITY_NO;
                    ViewBag.PolicyNumber = vClaim.POLICY_NUMBER;
                    ViewBag.PolicyStartDate = vClaim.POLICY_START_DATE;
                    ViewBag.PolicyEndDate = vClaim.POLICY_END_DATE;
                    ViewBag.FirstInsuredDate = vClaim.FIRST_INSURED_DATE;

                    if (vClaim.PAYROLL_ID != null && vClaim.PAYROLL_ID > 0)
                    {
                        string payrollNo = string.IsNullOrEmpty(Convert.ToString(vClaim.PAYROLL_ID)) ? "-" : Convert.ToString(vClaim.PAYROLL_ID);
                        string payrollDate = string.IsNullOrEmpty(Convert.ToString(vClaim.PAYROLL_DATE)) ? "-" : ((DateTime)vClaim.PAYROLL_DATE).ToString("dd-MM-yyyy");
                        string payrollType = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Payroll, vClaim.PAYROLL_TYPE);
                        payrollType = string.IsNullOrEmpty(payrollType) ? "-" : payrollType;
                        string payrollCategory = LookupHelper.GetLookupTextByOrdinal(LookupTypes.PayrollCategory, vClaim.PAYROLL_CATEGORY);
                        payrollCategory = string.IsNullOrEmpty(payrollCategory) ? "-" : payrollCategory;
                        ViewBag.PayrollDescription = $"No : {payrollNo} / Tarih : {payrollDate} / Tip : {payrollType} / Kategori : {payrollCategory}";
                    }
                    ViewBag.ClaimId = claimId;
                }
                else
                {
                    return RedirectToAction("Index", "Insured");
                }
            }
            // Create View
            else
            {
                if (insuredId <= 0 || policyId <= 0)
                {
                    return RedirectToAction("Index", "Insured");
                }
                ViewBag.InsuredId = insuredId;
                ViewBag.ContactId = contactId;
                ViewBag.PolicyId = policyId;
                ViewBag.PackageId = packageId;
                ViewBag.ClaimPaymentMethod = Convert.ToString((int)ClaimPaymentMethod.KURUM);

                var insuredClaimInfo = new GenericRepository<V_InsuredClaim>().FindBy("INSURED_ID=:insuredId", parameters: new { insuredId }, orderby: "INSURED_ID").FirstOrDefault();
                if (insuredClaimInfo != null)
                {
                    ViewBag.CompanyId = insuredClaimInfo.COMPANY_ID;
                    ViewBag.ClaimType = insuredClaimInfo.PACKAGE_TYPE;
                    ViewBag.ClaimTypeText = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Claim, insuredClaimInfo.PACKAGE_TYPE);

                    ViewBag.InsuredName = insuredClaimInfo.FIRST_NAME + " " + insuredClaimInfo.LAST_NAME;
                    ViewBag.InsuredIdentityNo = insuredClaimInfo.IDENTITY_NO;
                    ViewBag.PolicyNumber = insuredClaimInfo.POLICY_NUMBER;
                    ViewBag.PolicyStartDate = insuredClaimInfo.POLICY_START_DATE;
                    ViewBag.PolicyEndDate = insuredClaimInfo.POLICY_END_DATE;
                    ViewBag.FirstInsuredDate = insuredClaimInfo.FIRST_INSURED_DATE;
                }

                ViewBag.EventType = "";
                ViewBag.BillCurrencyType = Convert.ToString((int)CurrencyType.TURK_LIRASI);
                ViewBag.BillPaymentType = Convert.ToString((int)PaymentType.BANKAYA_ODEME);
            }

            ViewBag.PricingTypeList = LookupHelper.GetLookupData(LookupTypes.Pricing);

            ViewBag.ClaimPaymentMethodList = LookupHelper.GetLookupData(LookupTypes.ClaimPaymentMethod);

            ViewBag.ClaimTypeList = LookupHelper.GetLookupData(LookupTypes.Claim);

            ViewBag.NoteTypeList = LookupHelper.GetLookupData(LookupTypes.ClaimNote);
            ViewBag.ICDTypeList = LookupHelper.GetLookupData(LookupTypes.ICD);

            var ICD10ProcessList = ProcessListHelper.GetProcessList("ICD10");
            if (ICD10ProcessList != null)
            {
                ViewBag.ICD10ProcessListId = ICD10ProcessList.Id;
            }
            else
            {
                ViewBag.ICD10ProcessListId = 0;
            }

            ViewBag.StaffContractTypeList = LookupHelper.GetLookupData(LookupTypes.StaffContract);

            ViewBag.EventTypeList = LookupHelper.GetLookupData(LookupTypes.Event);

            ViewBag.PaymentTypeList = LookupHelper.GetLookupData(LookupTypes.Payment);
            ViewBag.IcdTypeList = LookupHelper.GetLookupData(LookupTypes.ICD);

            ViewBag.CurrencyTypeList = LookupHelper.GetLookupData(LookupTypes.Currency);
            ViewBag.BillCurrencyType = "";

            ViewBag.BillStatusList = LookupHelper.GetLookupData(LookupTypes.BillStatus);

            ViewBag.PayrollStatusList = LookupHelper.GetLookupData(LookupTypes.PayrollStatus, showChoose: true);
            ViewBag.PayrollTypeList = LookupHelper.GetLookupData(LookupTypes.Payroll);
            ViewBag.PayrollCategoryList = LookupHelper.GetLookupData(LookupTypes.PayrollCategory);

            ViewBag.ClaimMissingDocTypeList = LookupHelper.GetLookupData(LookupTypes.ClaimMissingDoc);

            ViewBag.AgreementTypeList = LookupHelper.GetLookupData(LookupTypes.Agreement);
            ViewBag.ExemptionTypeList = LookupHelper.GetLookupData(LookupTypes.Exemption);

            var DoctorBranchList = new GenericRepository<DoctorBranch>().FindBy();
            ViewBag.DoctorBranchList = DoctorBranchList;

            return View();
        }

        [HttpPost]
        [LoginControl]
        public JsonResult Create(Int64 claimId, Int64 insuredId, Int64 policyId, Int64 packageId, Int64 contactId, Int64 companyId, FormCollection form)
        {
            var result = new AjaxResultDto<ClaimSaveStep1Result>();
            ClaimSaveStep1Result step1Result = new ClaimSaveStep1Result();
            try
            {
                var claim = new Claim();

                if (claimId <= 0)
                {
                    if (form["CLAIM_TYPE"] != ((int)ClaimType.OSS).ToString())
                    {
                        throw new Exception("Tipi TSS olan Provizyon Kaydı Yapılamaz!");
                    }
                    var insured = new GenericRepository<Insured>().FindById(insuredId);
                    if (insured == null)
                    {
                        throw new Exception("Sigortalı bilgisi bulunamadı!");
                    }
                    var providerId = Session["ProviderId"].ToString();
                    if (string.IsNullOrEmpty(providerId) || long.Parse(providerId) <= 0)
                    {
                        throw new Exception("Lütfen kurumu seçiniz!");
                    }
                    var claimDate = form["claimDate"];
                    if (!claimDate.IsDateTime())
                    {
                        throw new Exception("Hasar Tarihi Girilmesi Gerekmektedir!");
                    }
                    if (DateTime.Now.AddDays(-7) > DateTime.Parse(form["claimDate"]))
                    {
                        throw new Exception("Yalnızca 7 gün öncesine kadar hasar girişi yapabilirsiniz! Hasar Tarihini Kontrol Ediniz.");
                    }
                    if (DateTime.Now < DateTime.Parse(form["claimDate"]))
                    {
                        throw new Exception("İleri Tarihli Hasar Girişi Yapılamaz! Hasar Tarihini Kontrol Ediniz.");
                    }
                    var policy = new GenericRepository<V_Policy>().FindBy($"POLICY_ID={policyId}", orderby: "").FirstOrDefault();
                    if (policy == null)
                    {
                        throw new Exception("Aktif Poliçe Bilgisi Bulunamadı!");
                    }
                    else if (policy.POLICY_END_DATE < DateTime.Parse(form["claimDate"]) || policy.POLICY_START_DATE > DateTime.Parse(form["claimDate"]))
                    {
                        throw new Exception("Poliçe Girilen Hasar Tarihinde Geçerli Değildir! Lütfen Hasar Tarihini Kontrol Ediniz...");
                    }
                    var paymentMethod = ((int)ClaimPaymentMethod.KURUM).ToString();

                    claim.Id = 0;
                    claim.InsuredId = insuredId;
                    claim.ProviderId = long.Parse(providerId);
                    claim.NoticeDate = DateTime.Now;
                    claim.ClaimDate = DateTime.Parse(form["claimDate"]);
                    claim.PaymentMethod = paymentMethod;
                    claim.Type = form["CLAIM_TYPE"];
                    string providerType = Session["ProviderType"].ToString();
                    if(providerType== ((int)ProviderType.ECZANE).ToString())
                    {
                        claim.SourceType = Convert.ToString((int)ClaimSourceType.ECZANE);
                    }
                    else
                    {
                        claim.SourceType = Convert.ToString((int)ClaimSourceType.HASTANE);
                    }
                    claim.PolicyId = policyId;
                    claim.PackageId = packageId;

                    if (companyId == 95 || companyId == 10)
                    {
                        ProxyServiceClient serviceClient = new ProxyServiceClient();
                        PolicyStateReq policyStateReq = new PolicyStateReq
                        {
                            CompanyCode = companyId.ToString(),
                            IncidentDate = DateTime.Parse(form["claimDate"]),
                            InsuredNo = insured.CompanyInsuredNo,
                            PolicyNo = policy.POLICY_NUMBER.ToString(),
                            RenewalNo = policy.RENEWAL_NO,
                        };
                        var policyStateRes = serviceClient.PolicyState(policyStateReq);
                        if (!policyStateRes.IsOk)
                        {
                            throw new Exception($"Şirket Tarafından ({(policyStateRes.Error.Length > 0 ? (string.Join(", ", policyStateRes.Error)) : policyStateRes.Description)}) bu nedenlerden dolayı hasar oluşturulamadı!");
                        }
                    }

                    var spResponseClaim = new GenericRepository<Claim>().Insert(claim);
                    if (spResponseClaim.Code == "100")
                    {
                        claimId = spResponseClaim.PkId;
                    }
                    else
                    {
                        if (spResponseClaim.PkId > 0)
                        {
                            claimId = spResponseClaim.PkId;
                            step1Result.claimId = claimId;

                            step1Result.provisionStep = 0;

                            var v_Claim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID=:id", parameters: new { id = claimId }, orderby: "CLAIM_ID DESC", fetchDeletedRows: true).FirstOrDefault();
                            if (v_Claim != null)
                            {
                                step1Result.claimStatus = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, v_Claim.STATUS);
                                step1Result.claimStatusOrdinal = int.Parse(v_Claim.STATUS);
                                step1Result.CompanyId = (long)v_Claim.COMPANY_ID;
                                step1Result.CompanyName = v_Claim.COMPANY_NAME;
                                step1Result.ReasonDesc = v_Claim.REASON_DESCRIPTION;
                                step1Result.providerName = v_Claim.PROVIDER_NAME;
                                step1Result.InsuredId = (long)v_Claim.INSURED_ID;
                                step1Result.paymentMethod = v_Claim.PAYMENT_METHOD;
                            }

                            result.Data = step1Result;
                            result.ResultCode = "760";
                            result.ResultMessage = spResponseClaim.Message + " - Bu Sebeple Hasar İptal Statüsünde Kaydedilmiştir.";

                            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            throw new Exception(spResponseClaim.Message);
                        }
                    }
                }
                else
                {
                    claim.Id = claimId;
                    var claimDate = form["claimDate"];
                    if (!claimDate.IsDateTime())
                    {
                        throw new Exception("Hasar Tarihi Girilmesi Gerekmektedir!");
                    }
                    if (DateTime.Now.AddDays(-7) > DateTime.Parse(form["claimDate"]))
                    {
                        throw new Exception("Yalnızca 7 gün öncesine kadar hasar girişi yapabilirsiniz! Hasar Tarihini Kontrol Ediniz.");
                    }
                    if (DateTime.Now < DateTime.Parse(form["claimDate"]))
                    {
                        throw new Exception("İleri Tarihli Hasar Girişi Yapılamaz! Hasar Tarihini Kontrol Ediniz.");
                    }
                    claim.ClaimDate = DateTime.Parse(claimDate);
                    var spResponseClaim = new GenericRepository<Claim>().Insert(claim);
                    if (spResponseClaim.Code == "100")
                    {
                        claimId = spResponseClaim.PkId;
                    }
                }

                step1Result.claimId = claimId;
                step1Result.claimStatus = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, Convert.ToString((int)ClaimStatus.GIRIS));
                step1Result.claimStatusOrdinal = (int)ClaimStatus.GIRIS;
                step1Result.provisionStep = 0;

                var vClaim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID=:id", parameters: new { id = claimId }, orderby: "CLAIM_ID DESC", fetchDeletedRows: true).FirstOrDefault();
                if (vClaim != null)
                {
                    result.ResultCode = "100";

                    step1Result.claimStatus = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, vClaim.STATUS);
                    step1Result.claimStatusOrdinal = int.Parse(vClaim.STATUS);
                    step1Result.CompanyId = (long)vClaim.COMPANY_ID;
                    step1Result.CompanyName = vClaim.COMPANY_NAME;
                    step1Result.ReasonDesc = vClaim.REASON_DESCRIPTION;
                    step1Result.providerName = vClaim.PROVIDER_NAME;
                    step1Result.InsuredId = (long)vClaim.INSURED_ID;
                    step1Result.paymentMethod = vClaim.PAYMENT_METHOD;
                }

                result.Data = step1Result;
                result.ResultMessage = "Giriş Sekmesi Başarıyla Kayıt Edildi.";
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }

            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ICDs(Int64 claimId)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                List<V_ClaimIcd> dbIcds = new GenericRepository<V_ClaimIcd>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                if (dbIcds != null)
                {
                    result.ResultCode = "100";

                    result.Data = new JSONResult
                    {
                        jsonData = dbIcds.ToJSON(false)
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Notes(Int64 claimId)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                List<V_ClaimNote> dbIcds = new GenericRepository<V_ClaimNote>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                if (dbIcds != null)
                {
                    result.ResultCode = "100";

                    result.Data = new JSONResult
                    {
                        jsonData = dbIcds.ToJSON(false)
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetIcdList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            string name = form["icdName"];
            string code = form["icdCode"];

            List<Process> icdProcesses = ICD10StraightHelper.GetICD10Data();
            icdProcesses = icdProcesses.Where(p => p.Name.ToUpper().Contains(name.ToUpper()) && p.Code.ToUpper().Contains(code.ToUpper())).ToList();

            return Json(new { data = icdProcesses, draw = Request["draw"], recordsTotal = icdProcesses.Count, recordsFiltered = icdProcesses.Count }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetClaimProcess(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];

            var claimlist = new ViewResultDto<List<V_ClaimProcessList>>();
            claimlist.Data = new List<V_ClaimProcessList>();
            //if (!form["isOpenProcessModal"].IsNull() && form["isOpenProcessModal"] == "1" && form["ClaimId"].IsInt64() && long.Parse(form["ClaimId"]) > 0)
            //{
            String whereConditition = form["ClaimId"].IsInt64() ? $" CLAIM_ID = {long.Parse(form["ClaimId"])} AND" : "";
            whereConditition += !form["ProcessName"].IsNull() ? $" nls_upper(PROCESS_NAME,'NLS_SORT = XTURKISH') LIKE ('%{form["ProcessName"].ToUpper()}%') AND" : "";
            whereConditition += !form["ProcessCode"].IsNull() ? $" PROCESS_CODE LIKE '%{form["ProcessCode"]}%' AND" : "";

            claimlist = new GenericRepository<V_ClaimProcessList>().FindByPaged(
                              conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                              orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},CLAIM_ID DESC" : "CLAIM_ID DESC",
                              pageNumber: start,
                              rowsPerPage: length,
                              fetchDeletedRows: true,
                              fetchHistoricRows: true);
            //}


            return Json(new { data = claimlist.Data, draw = Request["draw"], recordsTotal = claimlist.TotalItemsCount, recordsFiltered = claimlist.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public JsonResult ClaimIcdSave(Int64 claimId, Int64 icdId, Int64 claimIcdId, FormCollection form)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                if (claimId < 1)
                {
                    result.ResultCode = "999";
                    result.ResultMessage = "Hasar Bilgisi Bulunamadı!";
                }
                else
                {
                    if (claimIcdId < 1)
                    {
                        var icdtype = form["ICD_TYPE_" + icdId];

                        if (icdId < 0)
                        {
                            result.ResultCode = "999";
                            result.ResultMessage = "Tanı Bilgisi Bulunamadı!";
                        }
                        else if (!icdtype.IsInt())
                        {
                            result.ResultCode = "999";
                            result.ResultMessage = "Tanı Tipi Seçiniz!";
                        }
                        else
                        {
                            var claimIcd = new ClaimIcd
                            {
                                Id = 0,
                                ClaimId = claimId,
                                IcdId = icdId,
                                IcdType = icdtype
                            };
                            var spResponseClaimIcd = new ClaimIcdRepository().Insert(claimIcd);
                            if (spResponseClaimIcd.Code == "100")
                            {
                                result.ResultCode = spResponseClaimIcd.Code;
                                result.ResultMessage = "Tanı Başarıyla Eklendi.";
                            }
                            else
                            {
                                throw new Exception(spResponseClaimIcd.Message);
                            }
                        }
                    }
                    else
                    {
                        ClaimIcd claimIcd = new GenericRepository<ClaimIcd>().FindById(claimIcdId);
                        if (claimIcd == null)
                        {
                            result.ResultCode = "999";
                            result.ResultMessage = "Tanı Bilgisi Bulunamadı!";
                        }
                        claimIcd.IcdType = form["ICD_TYPE_Edit"];
                        SpResponse spResponseClaimIcd = new GenericRepository<ClaimIcd>().Insert(claimIcd);
                        if (spResponseClaimIcd.Code == "100")
                        {
                            result.ResultCode = "100";
                            result.ResultMessage = "Tanı Tipi Başarıyla Güncellendi.";
                        }
                    }

                    List<V_ClaimIcd> dbIcds = new GenericRepository<V_ClaimIcd>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                    if (dbIcds != null)
                    {
                        result.Data = new JSONResult
                        {
                            jsonData = dbIcds.ToJSON(false)
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public JsonResult ClaimNoteSave(Int64 claimId, FormCollection form, Int64 noteId = 0)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                if (claimId < 1)
                {
                    result.ResultCode = "999";
                    result.ResultMessage = "Hasar Bilgisi Bulunamadı!";
                }
                else
                {
                    var noteType = ((int)ClaimNoteType.EPIKRIZ).ToString();
                    var noteDescription = form["noteDescription"];

                    if (noteId > 0)
                    {
                        Note note = new GenericRepository<Note>().FindById(noteId);
                        if (note == null)
                        {
                            throw new Exception("Not Bilgisi Bulunamadı");
                        }
                        note.Description = noteDescription;
                        SpResponse spResponseClaimNote = new GenericRepository<Note>().Insert(note);
                        if (spResponseClaimNote.Code == "100")
                        {
                            result.ResultCode = spResponseClaimNote.Code;
                            result.ResultMessage = "Not Başarıyla Güncellendi.";
                        }
                        else
                        {
                            throw new Exception(spResponseClaimNote.Message);
                        }
                    }
                    else
                    {
                        Note note = new Note
                        {
                            Id = 0,
                            Type = noteType,
                            Description = noteDescription.ToUpper().RemoveRepeatedWhiteSpace(),
                        };
                        var spResponseNote = new GenericRepository<Note>().Insert(note);
                        if (spResponseNote.Code == "100")
                        {
                            noteId = spResponseNote.PkId;
                            ClaimNote claimNote = new ClaimNote
                            {
                                ClaimId = claimId,
                                NoteId = noteId,
                                Type = noteType
                            };
                            var spResClaimNote = new GenericRepository<ClaimNote>().Insert(claimNote);
                            if (spResClaimNote.Code != "100")
                            {
                                throw new Exception(spResClaimNote.Message);
                            }
                            else
                            {
                                result.ResultCode = spResponseNote.Code;
                                result.ResultMessage = "Not Başarıyla Kayıt Edildi.";
                            }
                        }
                        else
                        {
                            throw new Exception(spResponseNote.Message);
                        }
                    }

                    List<V_ClaimNote> dbClaimNotes = new GenericRepository<V_ClaimNote>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                    if (dbClaimNotes != null)
                    {
                        result.Data = new JSONResult
                        {
                            jsonData = dbClaimNotes.ToJSON(false)
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteICD(Int64 claimId, Int64 claimIcdId)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                List<V_ClaimIcd> dbIcds = new GenericRepository<V_ClaimIcd>().FindBy("CLAIM_ID =:id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                if (dbIcds == null || dbIcds.Count == 1)
                {
                    List<V_ClaimProcess> dbClaimCoverages = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                    if (dbClaimCoverages.Count > 0)
                    {
                        throw new Exception("Tüm Tanıları Silmek İçin Lütfen Öncelikle İşlemleri Siliniz.");
                    }
                }
                
                var claimIcd = new ClaimIcd
                {
                    Id = claimIcdId,
                    Status = Convert.ToString((int)Status.SILINDI)
                };
                var spResponseClaimIcd = new ClaimIcdRepository().Insert(claimIcd);
                if (spResponseClaimIcd.Code == "100")
                {
                    result.ResultCode = spResponseClaimIcd.Code;
                    result.ResultMessage = "Tanı Bilgisi Başarıyla Silindi.";
                }
                else
                {
                    throw new Exception(spResponseClaimIcd.Message);
                }

                dbIcds = new GenericRepository<V_ClaimIcd>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                if (dbIcds != null)
                {
                    result.Data = new JSONResult
                    {
                        jsonData = dbIcds.ToJSON(false)
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetClaimList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == 1000 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];

            ExportVM vm = new ExportVM();
            vm.WhereCondition = "";
            vm.SetExportColumns();

            ViewResultDto<List<V_Claim>> insuredList = new ViewResultDto<List<V_Claim>>();
            insuredList.Data = new List<V_Claim>();

            if (isFilter == "1")
            {
                string whereConditition = $"PROVIDER_ID={Session["ProviderId"]} AND STATUS!='{((int)ClaimStatus.SILINDI).ToString()}' AND PAYMENT_METHOD='{((int) ClaimPaymentMethod.KURUM).ToString()}' AND";
                whereConditition += form["CLAIM_ID"].IsInt64() ? $" CLAIM_ID={long.Parse(form["CLAIM_ID"])} AND" : "";
                whereConditition += !form["MEDULA_NO"].IsNull() ? $" MEDULA_NO='{form["MEDULA_NO"]}' AND" : "";
                whereConditition += form["COMPANY_ID"].IsInt64() ? $" COMPANY_ID={long.Parse(form["COMPANY_ID"])} AND" : "";
                whereConditition += !form["BILL_NO"].IsNull() ? $" BILL_NO='{form["BILL_NO"]}' AND" : "";
                whereConditition += form["PAYROLL_ID"].IsInt64() ? $" PAYROLL_ID ={long.Parse(form["PAYROLL_ID"])} AND" : "";
                whereConditition += form["POLICY_NUMBER"].IsInt64() ? $" POLICY_NUMBER = {form["POLICY_NUMBER"]} AND" : "";
                whereConditition += form["CLAIM_TYPE"].IsInt() ? $" CLAIM_TYPE= '{form["CLAIM_TYPE"]}' AND" : "";
                whereConditition += form["IDENTITY_NO"].IsInt64() ? $" IDENTITY_NO = {form["IDENTITY_NO"]} AND" : "";
                whereConditition += form["TAX_NUMBER"].IsInt64() ? $" TAX_NUMBER = {form["TAX_NUMBER"]} AND" : "";
                whereConditition += !form["Pasaport_NO"].IsNull() ? $" PASSPORT_NO LIKE '%{form["Pasaport_NO"]}%' AND" : "";
                //whereConditition += form["IDENTITY_NO"].IsInt64() ? $" (IDENTITY_NO={form["IDENTITY_NO"]} OR TAX_NUMBER={form["IDENTITY_NO"]} OR PASSPORT_NO='{form["IDENTITY_NO"]}') AND" : "";
                whereConditition += !form["FIRST_NAME"].IsNull() ? $" FIRST_NAME LIKE '%{form["FIRST_NAME"]}%' AND" : "";
                whereConditition += !form["LAST_NAME"].IsNull() ? $" LAST_NAME LIKE '%{form["LAST_NAME"]}%' AND" : "";
                whereConditition += form["CLAIM_DATE_START"].IsDateTime() ? $" CLAIM_DATE >= TO_DATE('{ DateTime.Parse(form["CLAIM_DATE_START"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["CLAIM_DATE_END"].IsDateTime() ? $" CLAIM_DATE <= TO_DATE('{ DateTime.Parse(form["CLAIM_DATE_END"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += !form["INSURER"].IsNull() ? $" INSURER_NAME LIKE '%{form["INSURER"]}%' AND" : "";
                whereConditition += form["CLAIM_STATUS"].IsInt() ? $" STATUS = '{form["CLAIM_STATUS"]}' AND" : "";
                whereConditition += form["PAYMENT_DATE_START"].IsDateTime() ? $" PAYMENT_DATE >= TO_DATE('{ DateTime.Parse(form["PAYMENT_DATE_START"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
                whereConditition += form["PAYMENT_DATE_END"].IsDateTime() ? $" PAYMENT_DATE <= TO_DATE('{ DateTime.Parse(form["PAYMENT_DATE_END"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";

                string hint = "/*+ USE_HASH(claim_id) ORDERED */";
                insuredList = new GenericRepository<V_Claim>().FindByPaged(
                                  conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                  orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},INSURED_ID DESC" : "INSURED_ID DESC",
                                  pageNumber: start,
                                  rowsPerPage: length, fetchDeletedRows: true
                                  , hint: hint);

                vm.WhereCondition = (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3));
            }
            return Json(new { data = insuredList.Data, draw = Request["draw"], recordsTotal = insuredList.TotalItemsCount, recordsFiltered = insuredList.TotalItemsCount, whereConditition = vm.WhereCondition }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetNoteList(Int64 claimId = 0)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                if (claimId < 1) throw new Exception("Hasar Bilgisi Bulunamadı!");

                var BillList = new GenericRepository<V_ClaimNote>().FindBy($"CLAIM_ID={claimId}", orderby: "CLAIM_ID");
                if (BillList != null)
                {
                    result.ResultCode = "100";
                    result.Data = new JSONResult
                    {
                        jsonData = BillList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        public JsonResult GetDoctorList()
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                var DoctorList = new GenericRepository<V_Doctor>().FindBy($"PROVIDER_ID={Session["ProviderId"]}", orderby: "");
                if (DoctorList != null)
                {
                    result.ResultCode = "100";
                    result.Data = new JSONResult
                    {
                        jsonData = DoctorList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult Doctor(Int64 claimId)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                List<dynamic> doctorList = new List<dynamic>();
                Claim claim = new GenericRepository<Claim>().FindById(claimId);
                if (claim != null)
                {
                    result.ResultCode = "100";
                    result.ResultMessage = "";
                    if (claim.StaffId != null)
                    {
                        V_Doctor DoctorList = new GenericRepository<V_Doctor>().FindBy("STAFF_ID =: id", orderby: "STAFF_ID", parameters: new { id = claim.StaffId }).FirstOrDefault();
                        //ViewHelper.GetView(Views.Claim, whereCondititon: "CLAIM_ID=:claimId", parameters: new { claimId });
                        if (DoctorList != null)
                        {
                            result.ResultCode = "100";
                            result.Data = new JSONResult
                            {
                                jsonData = DoctorList.ToJSON()
                            };
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveDoctor(Int64 claimId, Int64 doctorId)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                var repo = new GenericRepository<Claim>();
                var res = repo.FindById(claimId);
                if (res != null)
                {
                    res.StaffId = doctorId;
                    var spResponseClaim = repo.Insert(res);
                    if (spResponseClaim.Code == "100")
                    {
                        V_Doctor staff = new GenericRepository<V_Doctor>().FindBy("STAFF_ID=:doctorId", orderby: "STAFF_ID", parameters: new { doctorId }).FirstOrDefault();
                        if (staff != null)
                        {
                            result.ResultCode = "100";
                            result.ResultMessage = "Doktor Bilgisi Başarıyla Kayıt Edildi.";
                            result.Data = new JSONResult
                            {
                                jsonData = staff.ToJSON()
                            };
                        }

                    }
                    else
                    {
                        throw new Exception(spResponseClaim.Code + " : " + spResponseClaim.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ClaimProcess(Int64 claimId, Int64 packageId, FormCollection form)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                var processId = Request["processId"];
                if (!processId.IsInt64())
                {
                    throw new Exception("İşlem Bilgisi Bulunamadı!");
                }

                var CheckClaimProcess = new GenericRepository<ClaimProcess>().FindBy($"CLAIM_ID={claimId} AND PROCESS_ID={processId}");
                if (CheckClaimProcess.Count > 0)
                {
                    throw new Exception("Bu işlem Daha Önce Seçilmiş. Lütfen Kontrol Ediniz...");
                }

                V_ClaimProcessList claimProces = new GenericRepository<V_ClaimProcessList>().FindBy("CLAIM_ID =: id AND PROCESS_ID=:processId", orderby: "CLAIM_ID", fetchDeletedRows: true, fetchHistoricRows: true, parameters: new { id = claimId, processId }).FirstOrDefault();
                if (claimProces != null)
                {
                    var claimProcess = new ClaimProcess();
                    claimProcess.Id = 0;
                    claimProcess.ClaimId = claimId;
                    claimProcess.ProcessId = claimProces.PROCESS_ID;
                    claimProcess.UnitAmount = decimal.Parse(Convert.ToString(claimProces.PROCESS_AMOUNT));
                    claimProcess.RequestedAmount = decimal.Parse(Convert.ToString(claimProces.CONTRACT_AMOUNT));
                    claimProcess.AgreedAmount = decimal.Parse(Convert.ToString(claimProces.CONTRACT_AMOUNT));

                    List<V_PackageClaimProcess> packageClaimProcesses = new GenericRepository<V_PackageClaimProcess>().FindBy("PACKAGE_ID =: packageId AND PROCESS_ID =: processId", orderby: "PACKAGE_ID", fetchDeletedRows: true, fetchHistoricRows: true, parameters: new { packageId, processId = claimProcess.ProcessId });
                    if (packageClaimProcesses != null && packageClaimProcesses.Count == 1)
                    {
                        claimProcess.CoverageId = long.Parse(Convert.ToString(packageClaimProcesses[0].COVERAGE_ID));
                    }

                    var spResponseClaimProcess = new ClaimProcessRepository().Insert(claimProcess);
                    result.ResultCode = spResponseClaimProcess.Code;
                    result.ResultMessage = "İşlem Başarıyla Kayıt Edildi";

                    long medicineProcessListId = 0;
                    var medicineProcessList = ProcessListHelper.GetProcessList("İLAÇ");
                    if (medicineProcessList != null)
                    {
                        medicineProcessListId = medicineProcessList.Id;
                    }
                    else
                    {
                        throw new Exception("İlaç listesi bulunamadı!");
                    }

                    List<V_ClaimProcess> dbClaimProcesses = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id AND PROCESS_LIST_ID !=: processListId", orderby: "CLAIM_ID", parameters: new { id = claimId, processListId = medicineProcessListId });
                    if (dbClaimProcesses != null)
                    {
                        result.Data = new JSONResult
                        {
                            jsonData = dbClaimProcesses.ToJSON(false)
                        };
                    }
                }
                else
                {
                    throw new Exception("Seçilen hasar işlemi bulunamadı!");
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteClaimProcess(Int64 claimId, Int64 claimProcessId)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                long medicineProcessListId = 0;
                var medicineProcessList = ProcessListHelper.GetProcessList("İLAÇ");
                if (medicineProcessList != null)
                {
                    medicineProcessListId = medicineProcessList.Id;
                }
                else
                {
                    throw new Exception("İlaç listesi bulunamadı!");
                }

                var claimProcess = new ClaimProcess
                {
                    Id = claimProcessId,
                    Status = Convert.ToString((int)Status.SILINDI)
                };
                var spResponseClaimProcess = new ClaimProcessRepository().Insert(claimProcess);
                if (spResponseClaimProcess.Code == "100")
                {
                    result.ResultCode = spResponseClaimProcess.Code;
                    result.ResultMessage = "İşlem Silme İşlemi Başarıyla Gerçekleştirildi.";
                }
                else
                {
                    throw new Exception(spResponseClaimProcess.Message);
                }

                List<V_ClaimProcess> dbClaimProcesses = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id AND PROCESS_LIST_ID !=: processListId", orderby: "CLAIM_ID", parameters: new { id = claimId, processListId = medicineProcessListId });
                if (dbClaimProcesses != null)
                {
                    result.Data = new JSONResult
                    {
                        jsonData = dbClaimProcesses.ToJSON(false)
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Processes(Int64 claimId)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                long medicineProcessListId = 0;
                var medicineProcessList = ProcessListHelper.GetProcessList("İLAÇ");
                if (medicineProcessList != null)
                {
                    medicineProcessListId = medicineProcessList.Id;
                }
                else
                {
                    throw new Exception("İlaç listesi bulunamadı!");
                }

                List<V_ClaimProcess> dbClaimProcesses = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id AND PROCESS_LIST_ID !=: processListId", orderby: "CLAIM_ID", parameters: new { id = claimId, processListId = medicineProcessListId });
                if (dbClaimProcesses != null)
                {
                    result.Data = new JSONResult
                    {
                        jsonData = dbClaimProcesses.ToJSON(false)
                    };
                    result.ResultCode = "100";
                    result.ResultMessage = "";
                }
                else
                {
                    ClaimRepository claimRepository = new ClaimRepository();
                    Claim claim = claimRepository.FindById(claimId);
                    if (claim != null)
                    {
                        if (claim.PaymentMethod.Equals(Convert.ToString((int)ClaimPaymentMethod.SIGORTALI)))
                        {
                            throw new Exception("Bu sigortalı için bu kurumdan işlem seçilememekte, lütfen teminat seçiniz!");
                        }
                        else
                        {
                            throw new Exception("Bu kurumun dahil olduğu network'lerden sigortalı için paketinde tanımlı hizmet alabileceği işlem bulunamamıştır!");
                        }
                    }
                    else
                    {
                        throw new Exception("Hasar bulunamadı!");
                    }
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [LoginControl]
        public JsonResult Coverages(Int64 claimId)
        {
            object result = null;
            var claimProcessList = new GenericRepository<V_ClaimProcess>().FindBy("CLAIM_ID =: id", orderby: "CLAIM_ID", parameters: new { id = claimId });
            var claim = new GenericRepository<Claim>().FindBy(conditions: $"ID={claimId}").FirstOrDefault();

            if (claimProcessList != null)
            {
                List<dynamic> list = new List<dynamic>();

                int i = 1;

                foreach (var claimProcess in claimProcessList)
                {
                    //Add root item
                    dynamic claimProcessItem = new System.Dynamic.ExpandoObject();

                    claimProcessItem.id = i;
                    claimProcessItem.ClientId = i;
                    claimProcessItem.STATUS = claimProcess.STATUS;

                    claimProcessItem.CLAIM_ID = claimProcess.CLAIM_ID;
                    claimProcessItem.CLAIM_PROCESS_ID = claimProcess.CLAIM_PROCESS_ID;
                    claimProcessItem.PROCESS_ID = claimProcess.PROCESS_ID;
                    claimProcessItem.PROCESS_NAME = claimProcess.PROCESS_NAME;
                    claimProcessItem.COVERAGE_ID = string.IsNullOrEmpty(Convert.ToString(claimProcess.COVERAGE_ID)) ? "" : Convert.ToString(claimProcess.COVERAGE_ID);
                    claimProcessItem.COVERAGE_NAME = claimProcess.COVERAGE_NAME;
                    claimProcessItem.UNIT_AMOUNT = string.IsNullOrEmpty(Convert.ToString(claimProcess.UNIT_AMOUNT)) ? "" : Convert.ToDecimal(claimProcess.UNIT_AMOUNT).ToString("0.00");
                    claimProcessItem.REQUESTED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(claimProcess.REQUESTED_AMOUNT)) ? "" : Convert.ToDecimal(claimProcess.REQUESTED_AMOUNT).ToString("0.00");
                    claimProcessItem.AGREED_AMOUNT = string.IsNullOrEmpty(Convert.ToString(claimProcess.AGREED_AMOUNT)) ? "" : Convert.ToDecimal(claimProcess.AGREED_AMOUNT).ToString("0.00");
                    claimProcessItem.CONTRACT_PROCESS_GROUP_ID = claimProcess.CONTRACT_PROCESS_GROUP_ID;
                    claimProcessItem.PROCESS_CODE = claimProcess.PROCESS_CODE;
                    claimProcessItem.SESSION_COUNT = claimProcess.SESSION_COUNT;
                    claimProcessItem.COUNT = claimProcess.COUNT;
                    claimProcessItem.DAY = claimProcess.DAY;
                    claimProcessItem.PROCESS_LIMIT = string.IsNullOrEmpty(Convert.ToString(claimProcess.PROCESS_LIMIT)) ? "" : Convert.ToDecimal(claimProcess.PROCESS_LIMIT).ToString("0.00");
                    claimProcessItem.REQUESTED = string.IsNullOrEmpty(Convert.ToString(claimProcess.REQUESTED)) ? "0" : Convert.ToDecimal(claimProcess.REQUESTED).ToString("0.00");
                    claimProcessItem.STOPPAGE = string.IsNullOrEmpty(Convert.ToString(claimProcess.STOPAJ)) ? "" : Convert.ToDecimal(claimProcess.STOPAJ).ToString("0.00");
                    decimal acceptedAmount = 0;
                    if (claimProcess.REQUESTED != null && Convert.ToDecimal(claimProcess.REQUESTED) > 0)
                    {
                        if (claimProcess.ACCEPTED != null && Convert.ToDecimal(claimProcess.ACCEPTED) > 0)
                        {
                            acceptedAmount = Convert.ToDecimal(claimProcess.REQUESTED) - Convert.ToDecimal(claimProcess.ACCEPTED);
                        }
                        else
                        {
                            acceptedAmount = Convert.ToDecimal(claimProcess.REQUESTED);
                        }
                    }
                    claimProcessItem.ACCEPTED = acceptedAmount.ToString("0.00");
                    claimProcessItem.CONFIRMED = string.IsNullOrEmpty(Convert.ToString(claimProcess.CONFIRMED)) ? "" : Convert.ToDecimal(claimProcess.CONFIRMED).ToString("0.00");
                    claimProcessItem.PAID = string.IsNullOrEmpty(Convert.ToString(claimProcess.PAID)) ? "" : Convert.ToDecimal(claimProcess.PAID).ToString("0.00");
                    claimProcessItem.EXGRACIA = string.IsNullOrEmpty(Convert.ToString(claimProcess.EXGRACIA)) ? "" : Convert.ToDecimal(claimProcess.EXGRACIA).ToString("0.00");
                    claimProcessItem.SGK_AMOUNT = string.IsNullOrEmpty(Convert.ToString(claimProcess.SGK_AMOUNT)) ? "" : Convert.ToDecimal(claimProcess.SGK_AMOUNT).ToString("0.00");
                    decimal insuredAmount = 0;
                    if (claimProcess.REQUESTED != null && Convert.ToDecimal(claimProcess.REQUESTED) > 0)
                    {
                        if (claimProcess.PAID != null && Convert.ToDecimal(claimProcess.PAID) > 0)
                        {
                            insuredAmount = Convert.ToDecimal(claimProcess.REQUESTED) - Convert.ToDecimal(claimProcess.PAID);
                        }
                        else
                        {
                            insuredAmount = Convert.ToDecimal(claimProcess.REQUESTED);
                        }
                    }
                    claimProcessItem.INSURED_AMOUNT = insuredAmount.ToString("0.00");
                    claimProcessItem.COINSURANCE = claimProcess.COINSURANCE;
                    claimProcessItem.EXEMPTION = string.IsNullOrEmpty(Convert.ToString(claimProcess.EXEMPTION)) ? "" : Convert.ToDecimal(claimProcess.EXEMPTION).ToString("0.00");
                    claimProcessItem.RESULT = claimProcess.RESULT;
                    if (!string.IsNullOrEmpty(claimProcess.PARTLY_REJECT_DESC))
                    {
                        claimProcessItem.RESULT += " - " + claimProcess.PARTLY_REJECT_DESC;
                    }

                    string processResult = Convert.ToString(claimProcessItem.RESULT);
                    if ((claim.Status == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || claim.Status == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString()) && processResult.ToUpper().Contains("KURAL"))
                    {
                        string phone = "";
                        if (claim.CompanyId != null)
                        {
                            var _Company = new CompanyHelper().WhichCompany((long)claim.CompanyId);
                            if (_Company == Common.Enums.CompanyEnum.HALK)
                            {
                                phone = "0212 978 14 40";
                            }
                            else if (_Company == Common.Enums.CompanyEnum.DOGA)
                            {
                                phone = "0212 978 14 64";
                            }
                            else if (_Company == Common.Enums.CompanyEnum.TURK_NIPPON)
                            {
                                phone = "0212 978 14 60";
                            }
                        }
                        if (!phone.IsNull())
                        {
                            claimProcessItem.RESULT = $"İŞLEME DEVAM EDEBİLMEK İÇİN LÜTFEN {phone} NUMARALI PROVİZYON MERKEZİNİ ARAYINIZ";
                        }
                        else
                        {
                            claimProcessItem.RESULT = $"İŞLEME DEVAM EDEBİLMEK İÇİN LÜTFEN PROVİZYON MERKEZİNİ ARAYINIZ";
                        }
                    }

                    claimProcessItem.PROCESS_LIST_ID = claimProcess.PROCESS_LIST_ID;

                    list.Add(claimProcessItem);

                    i++;
                }
                result = list.ToJSON();
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = JsonConvert.SerializeObject(result),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        [HttpPost]
        [LoginControl]
        public ActionResult GetOtherReport(long claimId)
        {
            var response = new PrintResponse();
            Claim claim = new GenericRepository<Claim>().FindBy("ID=:id", fetchDeletedRows: true, parameters: new { id = claimId }).FirstOrDefault();
            if (claim != null)
            {
                IPrint<PrintAcquittanceFormReq> printt = new AcquittanceForm();
                response = printt.DoWork(new PrintAcquittanceFormReq
                {
                    ClaimId = claimId
                });
            }
            else
            {
                response.Code = "999";
                response.Message = "Hasar Bulunamadı!";
            }

            return new JsonResult()
            {
                Data = new { FileUrl = response.Url, FileName = response.FileName, Message = response.Message, Code = response.Code }
            };
        }

        [LoginControl]
        public JsonResult EvaluateStatus(Int64 claimId, Int64 companyId)
        {
            var result = new AjaxResultDto<ClaimStatusResult>();
            ClaimStatusResult step1Result = new ClaimStatusResult();
            try
            {
                List<V_ClaimIcd> dbIcds = new GenericRepository<V_ClaimIcd>().FindBy("CLAIM_ID =:id", orderby: "CLAIM_ID", parameters: new { id = claimId });
                if (dbIcds == null || dbIcds.Count < 1)
                {
                    throw new Exception("En az Bir Tanı Bilgisi Girilmesi Gerekmektedir.");
                }

                ProxyServiceClient proxyServiceClient = new ProxyServiceClient();
                NNHayatServiceClientV2 nNHayatServiceClient = new NNHayatServiceClientV2();
                ClaimStateUpdateReq stateUpdateReq = new ClaimStateUpdateReq
                {
                    ClaimId = claimId
                };

                if (companyId == 95 || companyId == 10)
                {
                    proxyServiceClient.ClaimNotice(stateUpdateReq);
                }
                else if (companyId == 322)
                {
                    NNTazminatGirisReq nNTazminatGirisReq = new NNTazminatGirisReq
                    {
                        claimId = claimId
                    };
                    nNHayatServiceClient.TazminatGiris(nNTazminatGirisReq);
                }

                ClaimRepository claimRepository = new ClaimRepository();
                string[] spResult = claimRepository.ClaimStatusResult(claimId);
                if (!spResult[0].Equals("1"))
                {
                    throw new Exception(spResult[0] + " : " + spResult[1] + " - HASAR DURUMU GÜNCELLENEMEDİ!");
                }

                var vClaim = new GenericRepository<V_Claim>().FindBy($"CLAIM_ID={claimId}", orderby: "CLAIM_ID DESC").FirstOrDefault();
                if (vClaim != null)
                {

                    step1Result.claimStatus = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, vClaim.STATUS);
                    step1Result.claimStatusOrdinal = int.Parse(vClaim.STATUS);
                    step1Result.claimReason = vClaim.REASON_DESCRIPTION;
                    step1Result.companyClaimId = vClaim.COMPANY_CLAIM_ID.ToString();

                    if (companyId == 95 || companyId == 10)
                    {
                        CommonProxyRes commonProxyRes = new CommonProxyRes();
                        if (vClaim.STATUS == ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                        {
                            commonProxyRes = proxyServiceClient.ClaimApproval(stateUpdateReq);
                        }
                        else if (vClaim.STATUS == ((int)ClaimStatus.RET).ToString())
                        {
                            commonProxyRes = proxyServiceClient.ClaimRejection(stateUpdateReq);
                        }
                        else if (vClaim.STATUS == ((int)ClaimStatus.IPTAL).ToString())
                        {
                            commonProxyRes = proxyServiceClient.ClaimCancellation(stateUpdateReq);
                        }
                        else if (vClaim.STATUS == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || vClaim.STATUS == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                        {
                            commonProxyRes = proxyServiceClient.ClaimWait(stateUpdateReq);
                        }

                    }
                    else if (companyId == 322)
                    {
                        string newStatus = "";
                        if (vClaim.STATUS == ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                        {
                            newStatus = "1";
                        }
                        else if (vClaim.STATUS == ((int)ClaimStatus.RET).ToString())
                        {
                            newStatus = "4";
                        }
                        else if (vClaim.STATUS == ((int)ClaimStatus.IPTAL).ToString())
                        {
                            newStatus = "2";
                        }
                        else if (vClaim.STATUS == ((int)ClaimStatus.PROVIZYON_BEKLEME).ToString() || vClaim.STATUS == ((int)ClaimStatus.TAZMİNAT_BEKLEME).ToString())
                        {
                            newStatus = "3";
                        }

                        if (newStatus.IsInt())
                        {
                            NNSetProvisionStatusReq nNSetProvisionStatusReq = new NNSetProvisionStatusReq
                            {
                                ProvisionNumber = vClaim.COMPANY_CLAIM_ID.ToString(),
                                ProvisionNumberSpecified = true,
                                NewStatu = newStatus,
                                NewStatuSpecified = true,
                                ReasonDesc = vClaim.REASON_DESCRIPTION,
                                ReasonDescSpecified = true
                            };
                            nNHayatServiceClient.SetProvisionStatus(nNSetProvisionStatusReq);
                        }
                    }
                    else if (companyId == 5)
                    {
                        DogaServiceClient dogaServiceClient = new DogaServiceClient();
                        dogaServiceClient.HasarAktar("78cc5f34-42fe-40c3-9baf-8c45d8aada97", vClaim.CLAIM_ID);
                    }

                    result.Data = step1Result;
                    if (vClaim.STATUS == ((int)ClaimStatus.PROZIYON_ONAY).ToString())
                    {
                        result.ResultCode = "100";
                        result.ResultMessage = "Hasar PROVİZYON Durumuna Getirildi.";
                    }
                    else
                    {
                        result.ResultCode = "360";
                        result.ResultMessage = $"Hasar {(step1Result.claimReason.IsNull() ? "" : $"{step1Result.claimReason} Sebebiyle")} {step1Result.claimStatus} Olarak Kayıt Edildi.";
                    }
                }
                else
                {
                    throw new Exception("Hasar Bulunamadı!");
                }
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }

            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }
    }
}