﻿using Protein.Common.Dto;
using Protein.Common.Extensions;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Provider.Web.ActionFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Provider.Web.Controllers
{
    [LoginControl]
    public class ProviderController : Controller
    {
        // GET: Provider
        public ActionResult Index()
        {
            var providerId = Session["ProviderId"];
            string whereCondition = $"PROVIDER_ID = :providerId";
            V_Provider provider = new GenericRepository<V_Provider>().FindBy(whereCondition, orderby: "PROVIDER_ID", parameters: new { providerId }).FirstOrDefault();
            if (provider == null)
            {
                TempData["Alert"] = $"swAlert('Hata','Kurum bilgilerine ulaşılamadı, lütfen tekrar giriş yapınız!','warning')";
                return RedirectToAction("Index", "Login");
            }
            ViewBag.Provider = provider;

            ViewBag.ProviderId = providerId;
            ViewBag.ProviderName = Session["ProviderName"];
            if (!string.IsNullOrEmpty(provider.PROVIDER_TYPE))
            {
                ViewBag.ProviderType = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Provider, provider.PROVIDER_TYPE);
            }
            else
            {
                ViewBag.ProviderType = " ";
            }

            var DoctorBranchList = new DoctorBranchRepository().FindBy();
            ViewBag.DoctorBranchList = DoctorBranchList;

            //Doktor bilgileri
            List<V_Staff> staffList = new GenericRepository<V_Staff>().FindBy($"PROVIDER_ID=:providerId AND STAFF_TYPE IN('{((int)StaffType.DOKTOR).ToString()}','{((int)StaffType.YETKILI).ToString()}')", orderby: "PROVIDER_ID", parameters: new { providerId });
            if (staffList != null && staffList.Count() > 0)
            {
                List<V_Staff> doctorList = staffList.Where(s => s.STAFF_TYPE == ((int)StaffType.DOKTOR).ToString()).ToList();
                if (doctorList != null)
                {
                    ViewBag.Doctors = doctorList.ToJSON();
                }

                //Yetkili Bilgileri
                var contacts = staffList.Where(s => s.STAFF_TYPE == ((int)StaffType.YETKILI).ToString()).ToList();
                if (contacts != null)
                {
                    ViewBag.Contacts = contacts.ToJSON();
                }
            }
            //Banka bilgileri
            var banks = new GenericRepository<V_ProviderBank>().FindBy("PROVIDER_ID=:providerId", orderby: "PROVIDER_ID", parameters: new { providerId });
            if (banks != null)
            {
                ViewBag.Banks = banks;
            }

            // Dosya bilgileri
            var medias = new GenericRepository<V_ProviderMedia>().FindBy("PROVIDER_ID=:providerId", orderby: "PROVIDER_ID", parameters: new { providerId });
            if (medias != null)
            {
                ViewBag.Medias = medias;
            }

            return View();
        }

        [HttpPost]
        [LoginControl]
        public JsonResult StaffSave(FormCollection form)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                var STAFF_TITLE = form["STAFF_TITLE"];
                var STAFF_FIRST_NAME = form["STAFF_FIRST_NAME"];
                var STAFF_LAST_NAME = form["STAFF_LAST_NAME"];
                var STAFF_PHONE_NO = form["STAFF_PHONE_NO"];
                var STAFF_EXTENSION = form["STAFF_EXTENSION"];
                var STAFF_MOBILE_PHONE_NO = form["STAFF_MOBILE_PHONE_NO"];
                var STAFF_EMAIL = form["STAFF_EMAIL"];
                var STAFF_IS_GROUP_ADMIN = form["STAFF_IS_GROUP_ADMIN"];
                long PROVIDER_ID = HttpContext.Session["ProviderId"] != null ? Convert.ToInt64(HttpContext.Session["ProviderId"]) : throw new Exception("Bağlı Bulunduğu Kurum Bilgisi Bulunamadı!");

                Contact contact = new Contact
                {
                    Type = ((int)ContactType.GERCEK).ToString(),
                    Title = STAFF_TITLE,
                };
                SpResponse spResponseContact = new GenericRepository<Contact>().Insert(contact);
                if (spResponseContact.Code == "100" && spResponseContact.PkId > 0)
                {
                    long contactId = spResponseContact.PkId;
                    Person person = new Person
                    {
                        ContactId = contactId,
                        ProfessionType = ((int)ProfessionType.DIGER).ToString(),
                        FirstName = STAFF_FIRST_NAME,
                        LastName = STAFF_LAST_NAME
                    };
                    SpResponse spResponsePerson = new GenericRepository<Person>().Insert(person);
                    if (spResponsePerson.Code == "100" && spResponsePerson.PkId > 0)
                    {
                        long personId = spResponsePerson.PkId;

                        long? phoneId = 0, mobilePhoneId = 0, emailId = 0;
                        if (!STAFF_PHONE_NO.IsNull())
                        {
                            Phone phone = new Phone
                            {
                                CountryId = 222,
                                Extension = STAFF_EXTENSION,
                                Type = ((int)PhoneType.IS).ToString(),
                                No = STAFF_PHONE_NO
                            };
                            SpResponse spResponsePhone = new GenericRepository<Phone>().Insert(phone);
                            if (spResponsePhone.Code == "100" && spResponsePhone.PkId > 0)
                            {
                                phoneId = spResponsePhone.PkId;
                            }
                        }

                        if (!STAFF_MOBILE_PHONE_NO.IsNull())
                        {
                            Phone phone = new Phone
                            {
                                CountryId = 222,
                                Type = ((int)PhoneType.MOBIL).ToString(),
                                No = STAFF_MOBILE_PHONE_NO
                            };
                            SpResponse spResponseMobilePhone = new GenericRepository<Phone>().Insert(phone);
                            if (spResponseMobilePhone.Code == "100" && spResponseMobilePhone.PkId > 0)
                            {
                                mobilePhoneId = spResponseMobilePhone.PkId;
                            }
                        }

                        if (!STAFF_EMAIL.IsNull())
                        {
                            Email email = new Email
                            {
                                Type = ((int)EmailType.IS).ToString(),
                                Details = STAFF_EMAIL
                            };
                            SpResponse spResponseEmail = new GenericRepository<Email>().Insert(email);
                            if (spResponseEmail.Code == "100" && spResponseEmail.PkId > 0)
                            {
                                emailId = spResponseEmail.PkId;
                            }
                        }

                        Staff staff = new Staff
                        {
                            ProviderId = PROVIDER_ID,
                            Type = ((int)StaffType.YETKILI).ToString(),
                            ContactId = contactId,
                            PhoneId = phoneId > 0 ? phoneId : null,
                            MobilePhoneId = mobilePhoneId > 0 ? mobilePhoneId : null,
                            EmailId = emailId > 0 ? emailId : null,
                            IsGroupAdmin = STAFF_IS_GROUP_ADMIN.IsNull() ? "0" : "1"
                        };
                        SpResponse spResponseStaff = new GenericRepository<Staff>().Insert(staff);
                        if (spResponseStaff.Code == "100" && spResponseStaff.PkId > 0)
                        {
                            result.ResultCode = "100";
                            result.ResultMessage = "Yetkili Bilgileri Başarıyla Kaydedildi.";
                        }
                    }
                }

                var staffList = new GenericRepository<V_Staff>().FindBy($"PROVIDER_ID=:id AND STAFF_TYPE='{((int)StaffType.YETKILI).ToString()}'", orderby: "PROVIDER_ID", parameters: new { id = PROVIDER_ID });
                if (staffList != null)
                {
                    result.Data = new JSONResult
                    {
                        jsonData = staffList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                string log = ex.Message;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}