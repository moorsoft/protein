﻿using Protein.Common.Dto;
using Protein.Common.Extensions;
using Protein.Data.Export.Model;
using Protein.Data.Repositories;
using Protein.Provider.Web.ActionFilter;
using Protein.Web.Models.ExportModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Provider.Web.Controllers
{
    public class CommonController : Controller
    {
        // GET: Common
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public virtual ActionResult DownloadByMediaId(long MediaID)
        {
            string handle = Guid.NewGuid().ToString();

            try
            {
                Media media = new GenericRepository<Media>().FindById(MediaID);

                //MemoryStream memStream = new MemoryStream();
                //BinaryFormatter binForm = new BinaryFormatter();
                //memStream.Write(media.FileContent, 0, media.FileContent.Length);
                //memStream.Seek(0, SeekOrigin.Begin);

                //memStream.
                if (media.FileContent != null)
                    TempData[handle] = media.FileContent.ToArray();

                return new JsonResult()
                {
                    Data = new { FileGuid = handle, FileName = media.FileName, IsNull = media.FileContent == null }
                };
            }
            catch (Exception ex) { return new EmptyResult(); }
        }
        [HttpPost]
        public ActionResult PrintGenerator(string url, string filename)
        {
            if (!url.IsNull() && url != "null")
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(url);

                TempData[url] = fileBytes;
            }

            return new JsonResult()
            {
                Data = new { FileUrl = url, FileName = filename + ".pdf" }
            };
        }
        public virtual ActionResult Download(string fileGuid, string fileName)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                TempData[fileGuid] = null;
                return File(data, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            }
            else
                return new EmptyResult();
        }

        [HttpPost]
        [LoginControl]
        public JsonResult DoctorSave(FormCollection form)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                var DOCTOR_BRANCH_ID = form["DOCTOR_BRANCH_ID"].IsInt64() ? long.Parse(form["DOCTOR_BRANCH_ID"]) : throw new Exception("Bölüm/Doktor Branş Seçilmesi Gerekmektedir!");
                var STAFF_TITLE = form["DOCTOR_TITLE"];
                var DIPLOMA_NO = form["DIPLOMA_NO"];
                var REGISTER_NO = form["REGISTER_NO"];
                var FIRST_NAME = form["FIRST_NAME"];
                var LAST_NAME = form["LAST_NAME"];
                long PROVIDER_ID = HttpContext.Session["ProviderId"] != null ? Convert.ToInt64(HttpContext.Session["ProviderId"]) : throw new Exception("Bağlı Bulunduğu Kurum Bilgisi Bulunamadı!");
                long contactId = 0;
                long personId = 0;
                long staffId = 0;

                if (!REGISTER_NO.IsNull())
                {
                    V_Staff doctor = new GenericRepository<V_Staff>().FindBy($"PROVIDER_ID=:id AND DOCTOR_BRANCH_ID={DOCTOR_BRANCH_ID} AND STAFF_TYPE='{((int)StaffType.DOKTOR).ToString()}' AND REGISTER_NO={REGISTER_NO}", orderby: "PROVIDER_ID", parameters: new { id = PROVIDER_ID }).FirstOrDefault();
                    if (doctor != null)
                    {
                        contactId = doctor.CONTACT_ID;
                        personId = doctor.PERSON_ID;
                        staffId = doctor.STAFF_ID;
                    }
                }
                

                Contact contact = new Contact
                {
                    Id=contactId,
                    Type = ((int)ContactType.GERCEK).ToString(),
                    Title = STAFF_TITLE,
                };
                SpResponse spResponseContact = new GenericRepository<Contact>().Insert(contact);
                if (spResponseContact.Code == "100" && spResponseContact.PkId > 0)
                {
                    contactId = spResponseContact.PkId;
                    Person person = new Person
                    {
                        Id=personId,
                        ContactId=contactId,
                        ProfessionType=((int)ProfessionType.DANISMAN_DENETCI).ToString(),
                        FirstName=FIRST_NAME,
                        LastName=LAST_NAME
                    };
                    SpResponse spResponsePerson = new GenericRepository<Person>().Insert(person);
                    if (spResponsePerson.Code=="100" && spResponsePerson.PkId>0)
                    {
                        personId = spResponsePerson.PkId;
                        Staff staff = new Staff
                        {
                            Id=staffId,
                            ProviderId=PROVIDER_ID,
                            Type=((int)StaffType.DOKTOR).ToString(),
                            ContactId=contactId,
                            DoctorBranchId=DOCTOR_BRANCH_ID,
                            DiplomaNo=DIPLOMA_NO,
                            RegisterNo=REGISTER_NO
                        };
                        SpResponse spResponseStaff = new GenericRepository<Staff>().Insert(staff);
                        if (spResponseStaff.Code == "100" && spResponseStaff.PkId > 0)
                        {
                            result.ResultCode = "100";
                            result.ResultMessage = "Doktor Bilgileri Başarıyla Kaydedildi.";
                        }
                    }
                }

                var doctorList = new GenericRepository<V_Staff>().FindBy($"PROVIDER_ID=:id AND STAFF_TYPE='{((int)StaffType.DOKTOR).ToString()}'", orderby: "PROVIDER_ID", parameters: new { id = PROVIDER_ID });
                if (doctorList != null)
                {
                    result.Data = new JSONResult
                    {
                        jsonData = doctorList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                string log = ex.Message;
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
                TempData["Alert"] = $"swAlert('Hata','{ex.Message}','warning')";
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ExportGenerator(ExportVM model)
        {
            ExportResponse response = new ExportResponse();
            if (model.selectedExportColumnValue.Count > 0 || model.IsAllColumns)
            {
                model.ExportType = ExportType.Excel;

                model.SetExportColumns();
                response = model.ExportOut();
                if (response.IsDownloaded)
                    TempData[response.Guid] = response.memoryStream.ToArray();
            }
            else
                return new EmptyResult();

            return new JsonResult()
            {
                Data = new { FileGuid = response.Guid, FileName = response.FileName }
            };
        }

        [HttpPost]
        [LoginControl]
        public JsonResult PayrollCreateOrUpdate(FormCollection form)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                var PAYROLL_ID = form["hdPayrollId"].IsInt64() ? long.Parse(form["hdPayrollId"]) :0;
                var EXT_PROVIDER_NO = form["payrollProviderNo"];
                var COMPANY_ID = form["payrollCompany"].IsInt64() ? long.Parse(form["payrollCompany"]) : throw new Exception("Sigorta Şirketi Seçilmesi Gerekmektedir!");
                var PAYROLL_CATEGORY = form["payrollCategory"];
                var PAYROLL_DATE = form["payrollDate"].IsDateTime() ? DateTime.Parse(form["payrollDate"]) : throw new Exception("Bordrolama Tarihi Girilmesi Gerekmektedir!");
                long PROVIDER_ID = HttpContext.Session["ProviderId"] != null ? Convert.ToInt64(HttpContext.Session["ProviderId"]) : throw new Exception("Bağlı Bulunduğu Kurum Bilgisi Bulunamadı!");

                Payroll payroll = new Payroll
                {
                    Id = PAYROLL_ID,
                    ExtProviderNo = EXT_PROVIDER_NO,
                    ExtProviderDate = DateTime.Now,
                    Category = PAYROLL_CATEGORY,
                    ProviderId = PROVIDER_ID,
                    CompanyId = COMPANY_ID,
                    Type = ((int)PayrollType.KURUM).ToString(),
                    PayrollDate = PAYROLL_DATE
                };
                var contract = new GenericRepository<V_ContractList>().FindBy($"PROVIDER_ID={PROVIDER_ID} AND CONTRACT_TYPE='{PAYROLL_CATEGORY}'", orderby: "").FirstOrDefault();
                if (contract != null && contract.PAYMENT_DAY != null)
                {
                    payroll.Due_Date = DateTime.Parse(form["payrollDate"]).AddDays((int)contract.PAYMENT_DAY);
                }
                else
                {
                    payroll.Due_Date = DateTime.Parse(form["payrollDate"]).AddDays(30);
                }
                SpResponse spResponsePayroll = new GenericRepository<Payroll>().Insert(payroll);
                if (spResponsePayroll.Code == "100" && spResponsePayroll.PkId > 0)
                {
                    result.ResultCode = "100";
                    result.ResultMessage = $"Zarf Başarıyla {(PAYROLL_ID > 0 ? "Güncellendi" : "Kayıt Edildi.")}";
                }
                else throw new Exception(spResponsePayroll.Message);
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetPayrollList(FormCollection form)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]) == -1 ? int.MaxValue : Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            string isFilter = form["isFilter"];


            ViewResultDto<List<V_Payroll>> PayrollList = new ViewResultDto<List<V_Payroll>>();
            PayrollList.Data = new List<V_Payroll>();
            String whereConditition = form["payrollCompany"].IsInt64() ? $" COMPANY_ID={long.Parse(form["payrollCompany"])} AND" : "";
            whereConditition += $" PROVIDER_ID ={Session["ProviderId"]} AND";
            whereConditition += $" PAYROLL_TYPE ='{((int)PayrollType.KURUM).ToString()}' AND";
            whereConditition += form["payrollNo"].IsInt64() ? $" PAYROLL_ID = {long.Parse(form["payrollNo"])} AND" : "";
            whereConditition += !form["providerPayrollNo"].IsNull() ? $" EXT_PROVIDER_NO = '{form["providerPayrollNo"]}' AND" : "";
            whereConditition += !form["payrollStatus"].IsNull() ? $" STATUS = '{form["payrollStatus"]}' AND" : "";
            whereConditition += form["payrollDate"].IsDateTime() ? $" PAYROLL_DATE >= TO_DATE('{ DateTime.Parse(form["payrollDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
            whereConditition += form["payrollDate"].IsDateTime() ? $" PAYROLL_DATE <= TO_DATE('{ DateTime.Parse(form["payrollDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
            whereConditition += form["payrollPaymentDateStart"].IsDateTime() ? $" PAYMENT_DATE >= TO_DATE('{ DateTime.Parse(form["payrollPaymentDateStart"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";
            whereConditition += form["payrollPaymentDateEnd"].IsDateTime() ? $" PAYMENT_DATE <= TO_DATE('{ DateTime.Parse(form["payrollPaymentDateEnd"])}','DD.MM.YYYY HH24:MI:SS') AND" : "";

            PayrollList = new GenericRepository<V_Payroll>().FindByPaged(
                              conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                              orderby: !String.IsNullOrEmpty(sortColumnName) ? $"{sortColumnName} {sortDirection},PAYROLL_ID DESC" : "PAYROLL_ID DESC",
                              pageNumber: start,
                              rowsPerPage: length,
                              fetchDeletedRows: true);
            return Json(new { data = PayrollList.Data, draw = Request["draw"], recordsTotal = PayrollList.TotalItemsCount, recordsFiltered = PayrollList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetBillList(Int64 payrollId = 0)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                if (payrollId < 1) throw new Exception("Zarf Bilgisi Bulunamadı!");
                long PROVIDER_ID = HttpContext.Session["ProviderId"] != null ? Convert.ToInt64(HttpContext.Session["ProviderId"]) : throw new Exception("Bağlı Bulunduğu Kurum Bilgisi Bulunamadı!");

                var BillList = new GenericRepository<V_Claim>().FindBy($"PAYROLL_ID={payrollId} AND PROVIDER={PROVIDER_ID}", orderby: "BILL_NO");
                if (BillList != null)
                {
                    result.ResultCode = "100";
                    result.Data = new JSONResult
                    {
                        jsonData = BillList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        public JsonResult GetOldClaimList(Int64 insuredId = 0)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                if (insuredId < 1) throw new Exception("Sigortalı Bilgisi Bulunamadı!");
                long PROVIDER_ID = HttpContext.Session["ProviderId"] != null ? Convert.ToInt64(HttpContext.Session["ProviderId"]) : throw new Exception("Bağlı Bulunduğu Kurum Bilgisi Bulunamadı!");

                var claimList = new GenericRepository<V_Claim>().FindBy($"INSURED_ID={insuredId} AND PROVIDER_ID={PROVIDER_ID}", orderby: "BILL_NO");
                if (claimList != null)
                {
                    result.ResultCode = "100";
                    result.Data = new JSONResult
                    {
                        jsonData = claimList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        public JsonResult GetRemainingCoverageList(Int64 insuredId = 0)
        {
            var result = new AjaxResultDto<JSONResult>();
            try
            {
                if (insuredId < 1) throw new Exception("Sigortalı Bilgisi Bulunamadı!");

                var rootPlanCoverageList = new GenericRepository<V_InsuredPackageRemaining>().FindBy(conditions: $"INSURED_ID = {insuredId} AND IS_MAIN_COVERAGE='1'", orderby: "INSURED_ID DESC", fetchDeletedRows: true, fetchHistoricRows: true);
                if (rootPlanCoverageList != null)
                {
                    result.ResultCode = "100";
                    result.Data = new JSONResult
                    {
                        jsonData = rootPlanCoverageList.ToJSON()
                    };
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
            }

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}