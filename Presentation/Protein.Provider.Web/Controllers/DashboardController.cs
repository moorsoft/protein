﻿using Protein.Data.Helpers;
using Protein.Provider.Web.ActionFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Protein.Provider.Web.Controllers
{
    [LoginControl]
    public class DashboardController : Controller
    {
        // GET: Dashboard
        public ActionResult Index()
        {
            LookupHelper.LoadLookupDataFromDbToCache();
            ReasonHelper.LoadReasonDataFromDbToCache();
            ICD10Helper.LoadICD10DataFromDbToCache();
            ICD10StraightHelper.LoadAppsettingsFromDb();
            ProcessListHelper.LoadProcessListDataFromDbToCache();

            ViewBag.ProviderId = Session["ProviderId"];
            ViewBag.ProviderName = Session["ProviderName"];

            return View();
        }
    }
}