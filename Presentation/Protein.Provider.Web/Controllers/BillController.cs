﻿using Protein.Business.Abstract.Print;
using Protein.Business.Print;
using Protein.Business.Print.ProviderPayroll;
using Protein.Common.Dto;
using Protein.Common.Extensions;
using Protein.Data.Helpers;
using Protein.Data.Repositories;
using Protein.Provider.Web.ActionFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Protein.Common.Constants.Constants;
using static Protein.Common.Entities.ProteinEntities;
using static Protein.Common.Enums.ProteinEnums;

namespace Protein.Provider.Web.Controllers
{
    public class BillController : Controller
    {
        // GET: Bill
        public ActionResult Create()
        {
            ViewBag.ProviderId = Session["ProviderId"];
            ViewBag.ProviderName = Session["ProviderName"];
            ViewBag.ClaimIdList = Request["ClaimIdList"];
            ViewBag.ClaimTypeList = LookupHelper.GetLookupData(LookupTypes.Claim);

            return View();
        }
        // GET: Bill
        public ActionResult Index(Int64 payrollId)
        {
            ViewBag.ProviderId = Session["ProviderId"];
            ViewBag.ProviderName = Session["ProviderName"];

            Payroll payroll = new GenericRepository<Payroll>().FindBy($"ID = {payrollId}", fetchDeletedRows: true).FirstOrDefault();
            if (payroll != null)
            {
                ViewBag.PayrollId = payroll.Id;
                ViewBag.PayrollDate = ((DateTime)payroll.PayrollDate).ToString("dd.MM.yyyy");
                ViewBag.PayrollStatus = LookupHelper.GetLookupTextByOrdinal(LookupTypes.PayrollStatus, payroll.Status);


                var BillList = new GenericRepository<V_Claim>().FindBy($"PAYROLL_ID={payrollId}", orderby: "BILL_NO");
                List<dynamic> listBill = new List<dynamic>();

                if (BillList != null)
                {
                    int i = 1;

                    foreach (var item in BillList)
                    {
                        dynamic listItem = new System.Dynamic.ExpandoObject();
                        listItem.NO = i.ToString();
                        listItem.BILL_NO = item.BILL_NO;
                        listItem.BILL_DATE = item.BILL_DATE != null ? ((DateTime)item.BILL_DATE).ToString("dd-MM-yyyy") : string.Empty;
                        listItem.CLAIM_ID = item.CLAIM_ID;
                        listItem.CLAIM_TYPE = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Claim, item.CLAIM_TYPE);
                        listItem.MEDULA_NO = item.MEDULA_NO;
                        listItem.CLAIM_DATE = item.CLAIM_DATE != null ? ((DateTime)item.CLAIM_DATE).ToString("dd-MM-yyyy") : string.Empty;
                        listItem.STATUS_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, item.STATUS);
                        listItem.CLAIM_STATUS = item.STATUS;
                        listItem.COMPANY_NAME = item.COMPANY_NAME;
                        listItem.INSURED_NAME = item.FIRST_NAME + " " + item.LAST_NAME;
                        listItem.IDENTITY_NO = item.IDENTITY_NO;
                        listItem.REQUESTED = item.REQUESTED == null ? "0" : Convert.ToDecimal(item.REQUESTED).ToString("0.00");
                        listItem.PAID = item.PAID == null ? "0" : Convert.ToDecimal(item.PAID).ToString("0.00");
                        listItem.INSURED_AMOUNT = "0";// string.IsNullOrEmpty(Convert.ToString(item.PAID)) ? "" : Convert.ToDecimal(item.PAID).ToString("0.00");
                        if (item.REQUESTED != null && item.PAID == null)
                        {
                            listItem.INSURED_AMOUNT = Convert.ToDecimal(item.REQUESTED).ToString("0.00");
                        }
                        else if (item.REQUESTED != null && item.PAID != null)
                        {
                            listItem.INSURED_AMOUNT = (Convert.ToDecimal(item.REQUESTED) - Convert.ToDecimal(item.PAID)).ToString("0.00");
                        }

                        listBill.Add(listItem);

                        i++;
                    }

                    ViewBag.BillList = listBill.ToJSON();
                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult SaveBillList(FormCollection form)
        {
            var result = new AjaxResultDto<PayrollSaveResult>();
            try
            {
                var ClaimIdList = form["ClaimIdList"];
                if (ClaimIdList.IsNull())
                    throw new Exception("Hasar Bilgisi Bulunamadı");

                var claimRepository = new GenericRepository<Claim>();
                List<Claim> claimList = claimRepository.FindBy($"ID IN ({ClaimIdList})");

                List<ClaimBill> claimBillList = new List<ClaimBill>();
                foreach (var item in ClaimIdList.Split(','))
                {
                    if(!item.IsInt64())
                        throw new Exception("Hasar Bilgisi Bulunamadı");

                    Claim claim = claimList.Where(x => x.Id == long.Parse(item)).FirstOrDefault();
                    if (claim != null)
                    {
                        string billNo = form["billNo_" + item];
                        string billDate = form["billDate_" + item];

                        if (!billDate.IsDateTime())
                            throw new Exception("Fatura Tarihi Girilmesi Gerekmektedir");
                        if (billNo.IsNull())
                            throw new Exception("Fatura No Girilmesi Gerekmektedir");

                        claim.ClaimDate = DateTime.Parse(((DateTime)claim.ClaimDate).ToString("dd.MM.yyyy"));
                        DateTime billDateTime = DateTime.Parse(billDate);
                        if (DateTime.Compare((DateTime)claim.ClaimDate, billDateTime) > 0)
                        {
                            throw new Exception("Fatura tarihi hasar tarihinden küçük olamaz!");
                        }

                        ClaimBill claimBill = new ClaimBill
                        {
                            Id = 0,
                            ClaimId = claim.Id,
                            BillNo = billNo,
                            BillDate = DateTime.Parse(billDate),
                            PaymentType = ((int)PaymentType.BANKAYA_ODEME).ToString(),
                        };
                        claimBillList.Add(claimBill);
                    }
                    else
                    {
                        throw new Exception($"{item} nolu hasar bulunamadı!");
                    }
                }
                var spResponseClaimBill = new GenericRepository<ClaimBill>().InsertSpExecute3(claimBillList);

                Payroll payroll = new Payroll
                {
                    CompanyId = claimList.Where(c => c.CompanyId != null).FirstOrDefault().CompanyId,
                    PayrollDate = DateTime.Now,
                    Due_Date = DateTime.Now.AddDays(30),
                    Category = claimList[0].Type,
                    ProviderId = long.Parse(Session["ProviderId"].ToString()),
                    Type = claimList[0].PaymentMethod
                };
                var spResponsePayroll = new GenericRepository<Payroll>().Insert(payroll);
                if (spResponsePayroll.Code!="100")
                {
                    throw new Exception("İcmal Oluşturulamadı!");
                }
                long payrollId = spResponsePayroll.PkId;

                List<Claim> claims = new List<Claim>();
                foreach (var item in ClaimIdList.Split(','))
                {
                    Claim claim = claimList.Where(x => x.Id == long.Parse(item)).FirstOrDefault();
                    claim.PayrollId = payrollId;
                    claims.Add(claim);
                }
                var spResponseClaim = new GenericRepository<Claim>().InsertSpExecute3(claims);


                result.ResultCode = "100";
                result.ResultMessage = "İcmal Başarıyla Oluşturuldu.";
                result.Data = new PayrollSaveResult
                {
                    payrollId = payrollId.ToString()
                };
            }
            catch (Exception ex)
            {
                result.ResultCode = "999";
                result.ResultMessage = ex.Message;
                result.Data = null;
            }
            return Json(result.ToJSON(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetBill()
        {
            var result = new AjaxResultDto<JSONResult>();

            var payrollId = Request["payrollId"].ToString();

            var BillList = new GenericRepository<V_Claim>().FindBy($"PAYROLL_ID={payrollId}", orderby: "BILL_NO");
            List<dynamic> listBill = new List<dynamic>();

            if (BillList != null)
            {
                int i = 1;

                foreach (var item in BillList)
                {
                    dynamic listItem = new System.Dynamic.ExpandoObject();
                    listItem.NO = i.ToString();
                    listItem.BILL_NO = item.BILL_NO;
                    listItem.BILL_DATE = item.BILL_DATE != null ? ((DateTime)item.BILL_DATE).ToString("dd-MM-yyyy") : string.Empty;
                    listItem.CLAIM_ID = item.CLAIM_ID;
                    listItem.CLAIM_TYPE = LookupHelper.GetLookupTextByOrdinal(LookupTypes.Claim, item.CLAIM_TYPE);
                    listItem.MEDULA_NO = item.MEDULA_NO;
                    listItem.CLAIM_DATE = item.CLAIM_DATE != null ? ((DateTime)item.CLAIM_DATE).ToString("dd-MM-yyyy") : string.Empty;
                    listItem.STATUS_TEXT = LookupHelper.GetLookupTextByOrdinal(LookupTypes.ClaimStatus, item.STATUS);
                    listItem.CLAIM_STATUS = item.STATUS;
                    listItem.COMPANY_NAME = item.COMPANY_NAME;
                    listItem.INSURED_NAME = item.FIRST_NAME + " " + item.LAST_NAME;
                    listItem.IDENTITY_NO = item.IDENTITY_NO;
                    listItem.REQUESTED = item.REQUESTED==null ? "0" : Convert.ToDecimal(item.REQUESTED).ToString("0.00");
                    listItem.PAID = item.PAID==null ? "0" : Convert.ToDecimal(item.PAID).ToString("0.00");
                    listItem.INSURED_AMOUNT = "0";// string.IsNullOrEmpty(Convert.ToString(item.PAID)) ? "" : Convert.ToDecimal(item.PAID).ToString("0.00");
                    if (item.REQUESTED != null && item.PAID==null)
                    {
                        listItem.INSURED_AMOUNT = Convert.ToDecimal(item.REQUESTED).ToString("0.00");
                    }
                    else if (item.REQUESTED != null && item.PAID != null)
                    {
                        listItem.INSURED_AMOUNT = (Convert.ToDecimal(item.REQUESTED)- Convert.ToDecimal(item.PAID)).ToString("0.00");
                    }

                    listBill.Add(listItem);

                    i++;
                }

                ViewBag.BillList = listBill;
            }

            result = new AjaxResultDto<JSONResult>
            {
                Data = new JSONResult
                {
                    jsonData = listBill.ToJSON(),
                },
                ResultCode = "100",
                ResultMessage = "OK"
            };

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = result.ToJSON(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetPayrollReport(long payrollId)
        {
            var response = new PrintResponse();
            Payroll claim = new GenericRepository<Payroll>().FindBy("ID=:id", fetchDeletedRows: true, parameters: new { id = payrollId }).FirstOrDefault();
            if (claim != null)
            {
                IPrint<PrintProviderPayrollReq> printt = new PrintProviderPayroll();
                response = printt.DoWork(new PrintProviderPayrollReq
                {
                    PayrollId = payrollId
                });
            }
            else
            {
                response.Code = "999";
                response.Message = "Hasar Bulunamadı!";
            }

            return new JsonResult()
            {
                Data = new { FileUrl = response.Url, FileName = response.FileName, Message = response.Message, Code = response.Code }
            };
        }

        [HttpPost]
        [LoginControl]
        public ActionResult GetClaimBill(FormCollection form)
        {
            ViewResultDto<List<V_Claim>> insuredList = new ViewResultDto<List<V_Claim>>();
            insuredList.Data = new List<V_Claim>();

            if (!form["ClaimIdList"].IsNull())
            {
                string whereConditition = $"PROVIDER_ID={Session["ProviderId"]} AND STATUS!='1' AND";
                whereConditition += !form["ClaimIdList"].IsNull() ? $" CLAIM_ID IN ({form["ClaimIdList"]}) AND" : "";

                string hint = "/*+ USE_HASH(claim_id) ORDERED */";

                insuredList = new GenericRepository<V_Claim>().FindByPaged(
                                  conditions: (string.IsNullOrEmpty(whereConditition) ? "" : whereConditition.Substring(0, whereConditition.Length - 3)),
                                  orderby: "CLAIM_ID DESC",
                                  hint: hint);
            }

            return Json(new { data = insuredList.Data, draw = Request["draw"], recordsTotal = insuredList.TotalItemsCount, recordsFiltered = insuredList.TotalItemsCount }, JsonRequestBehavior.AllowGet);
        }
    }
}